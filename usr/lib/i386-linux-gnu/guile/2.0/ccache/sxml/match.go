GOOF----LE-4-2.0#�     ]� 4     h��      ] g  guile�	 �	g  define-module*�	 �	 �	g  sxml�	g  match�	 �		g  filenameS�	
f  sxml/match.scm�	g  importsS�	g  srfi�	g  srfi-1�	 �	 �	g  srfi-11�	 �	 �	g  ice-9�	g  control�	 �	 �	 �	g  exportsS�	g  
sxml-match�	g  sxml-match-let�	g  sxml-match-let*�	 �	g  set-current-module�	 �	 �	 g  make-syntax-transformer�	!  �	"  �	#g  syntax-object->datum�	$g  macro�	%g  $sc-dispatch�	&% �	'% �	(g  any�	)(( �	*g  syntax-object�	+g  syntax->datum�	,g  top�	-, �	.g  ribcage�	/g  dummy�	0g  stx�	1/0 �	2g  m-gxFMqUq6swe52Ta9yFs47x-223�	32, �	43- �	5f  l-gxFMqUq6swe52Ta9yFs47x-228�	6f  l-gxFMqUq6swe52Ta9yFs47x-229�	756 �	8.147 �	9. �	:g  x�	;: �	<3 �	=f  l-gxFMqUq6swe52Ta9yFs47x-225�	>= �	?.;<> �	@-89? �	Ag  hygiene�	BA �	C*+@B �	Dg  syntax-violation�	ED �	FD �	Gf  -source expression failed to match any pattern�	Hg  void�	I( �	Jg  *unspecified*�	K/ �	Lg  m-gxFMqUq6swe52Ta9yFs47x-233�	ML, �	NM �	Of  l-gxFMqUq6swe52Ta9yFs47x-238�	PO �	Q.KNP �	Rf  l-gxFMqUq6swe52Ta9yFs47x-235�	SR �	T.;NS �	U-Q9T �	V*JUB �	Wg  throw�	Xg  sxml-match-error�	Yg  raise-syntax-error�	Zg  module�	[g  free-id�	\g  provide�	]g  m-gxFMqUq6swe52Ta9yFs47x-248�	^], �	_^ �	`f  l-gxFMqUq6swe52Ta9yFs47x-250�	a` �	b.;_a �	c-9b �	d*\cB �	e[d �	fg  each-any�	gef��	hg  require�	i*hcB �	j[i �	kjf��	lkf��	mgl��	n(m��	o(n��	p(o��	qg  begin�	rg  name�	sg  lang�	tg  p_�	ug  r_�	vg  body�	w/rstuv �	x^----- �	yf  l-gxFMqUq6swe52Ta9yFs47x-253�	zf  l-gxFMqUq6swe52Ta9yFs47x-254�	{f  l-gxFMqUq6swe52Ta9yFs47x-255�	|f  l-gxFMqUq6swe52Ta9yFs47x-256�	}f  l-gxFMqUq6swe52Ta9yFs47x-257�	~f  l-gxFMqUq6swe52Ta9yFs47x-258�	yz{|}~ � �.wx � �-�9b � �*q�B � �g  nodeset?� �g  error� �g  xml-element-tag� �f  expected an xml-element, given� �g  xml-element-attributes� �g  
fold-right� �g  @� �g  filter� �g  xml-element-contents� �g  match-xml-attribute� �g  member� �g  filter-attributes� �g  compile-clause� �g  map� �f( � �g  
let-values� �g  cata-binding� �g  body-stx� ��� � �-- � �f  l-gxFMqUq6swe52Ta9yFs47x-399� �f  l-gxFMqUq6swe52Ta9yFs47x-400� ��� � �.��� � �g  	cata-defs� ��v � �f  l-gxFMqUq6swe52Ta9yFs47x-394� �f  l-gxFMqUq6swe52Ta9yFs47x-395� ��� � �.��� � �g  sxml-match-syntax-error� �g  	ellipsis?� �g  literal?� �g  keyword?� �g  extract-cata-fun� �g  add-pat-var� �g  add-cata-def� �g  process-cata-exp� �g  process-cata-defs� �g  cata-defs->pvar-lst� �g  process-output-action� �g  compile-element-pat� �g  compile-end-element� �g  compile-attr-list� �g  compile-item-list� �g  compile-dotted-pattern-list� �g  compile-item� ������������������ � �----------------- � �f  l-gxFMqUq6swe52Ta9yFs47x-271� �f  l-gxFMqUq6swe52Ta9yFs47x-272� �f  l-gxFMqUq6swe52Ta9yFs47x-273� �f  l-gxFMqUq6swe52Ta9yFs47x-274� �f  l-gxFMqUq6swe52Ta9yFs47x-275� �f  l-gxFMqUq6swe52Ta9yFs47x-276� �f  l-gxFMqUq6swe52Ta9yFs47x-277� �f  l-gxFMqUq6swe52Ta9yFs47x-278� �f  l-gxFMqUq6swe52Ta9yFs47x-279� �f  l-gxFMqUq6swe52Ta9yFs47x-280� �f  l-gxFMqUq6swe52Ta9yFs47x-281� �f  l-gxFMqUq6swe52Ta9yFs47x-282� �f  l-gxFMqUq6swe52Ta9yFs47x-283� �f  l-gxFMqUq6swe52Ta9yFs47x-284� �f  l-gxFMqUq6swe52Ta9yFs47x-285� �f  l-gxFMqUq6swe52Ta9yFs47x-286� �f  l-gxFMqUq6swe52Ta9yFs47x-287� ������������������ � �.��� � �0 � �- � �f  l-gxFMqUq6swe52Ta9yFs47x-270� �� � �.��� � �-9�9��9� � �*��B � �g  identifier?� �((�� �(ό� �g  ...� �g  quote� �g  lst� �� � �f  l-gxFMqUq6swe52Ta9yFs47x-476� �� � �.��� � �g  process-quasiquote� �g  expand-quasiquote-body� �g  expand-dotted-item� �g  select-dotted-vars� �g  merge-pvars� �g  dotted-var?� �g  member-var?� �g  
expand-lst� �g  finite-lst?� ���������� 	� �--------- 	� �f  l-gxFMqUq6swe52Ta9yFs47x-446� �f  l-gxFMqUq6swe52Ta9yFs47x-444� �f  l-gxFMqUq6swe52Ta9yFs47x-442� �f  l-gxFMqUq6swe52Ta9yFs47x-440� �f  l-gxFMqUq6swe52Ta9yFs47x-438� �f  l-gxFMqUq6swe52Ta9yFs47x-436� �f  l-gxFMqUq6swe52Ta9yFs47x-434� �f  l-gxFMqUq6swe52Ta9yFs47x-432� �f  l-gxFMqUq6swe52Ta9yFs47x-430� ���������� 	� �.��� � �g  action� �g  dotted-vars� ��� � �f  l-gxFMqUq6swe52Ta9yFs47x-428� �f  l-gxFMqUq6swe52Ta9yFs47x-429� ��� � �.��� � �-9����9� � �*��B � �� � �g  cons� �g  exp-lft� �g  exp-rgt� ��� � �f  l-gxFMqUq6swe52Ta9yFs47x-512� �f  l-gxFMqUq6swe52Ta9yFs47x-513� ��� � �.��� � g  fst�g  rst�  �f  l-gxFMqUq6swe52Ta9yFs47x-505�f  l-gxFMqUq6swe52Ta9yFs47x-506� �.� �-9�9����9� �*�B �	g  append�
f  l-gxFMqUq6swe52Ta9yFs47x-500�f  l-gxFMqUq6swe52Ta9yFs47x-501�
 �.�� �g  dots�  �--- �f  l-gxFMqUq6swe52Ta9yFs47x-491�f  l-gxFMqUq6swe52Ta9yFs47x-492�f  l-gxFMqUq6swe52Ta9yFs47x-493� �. �-99����9� �*	B �g  bound-identifier=?�g  free-identifier=?�g  unquote�g  y� �f  l-gxFMqUq6swe52Ta9yFs47x-539� �.� � g  walk-quasi-body�!  �"- �#f  l-gxFMqUq6swe52Ta9yFs47x-536�$# �%.!"$ �&f  l-gxFMqUq6swe52Ta9yFs47x-535�'& �(.;�' �)-9%(���9� 
�**)B �+[* �,+( �-,(��.g  unquote-splicing�/*.)B �0[/ �10( �21(��3-%(���9� �4*�3B �5[4 �65(��7g  
quasiquote�8*73B �9[8 �:9(��;g  dv�<; �=f  l-gxFMqUq6swe52Ta9yFs47x-594�>= �?.<�> �@f  l-gxFMqUq6swe52Ta9yFs47x-589�A@ �B.;�A �Cg  dvars�DC �Ef  l-gxFMqUq6swe52Ta9yFs47x-581�FE �G.D�F �Hg  item�IH �Jf  l-gxFMqUq6swe52Ta9yFs47x-580�KJ �L.I�K �M-9?B9G9L���9� �N*�MB �Og  lambda�P*OMB �Qf  l-gxFMqUq6swe52Ta9yFs47x-599�RQ �S.;�R �T-9S���9� �U*7TB �V[U �WV(��X*TB �Y[X �ZY( �[g  expanded-item�\[ �]f  l-gxFMqUq6swe52Ta9yFs47x-609�^] �_.\�^ �`f  l-gxFMqUq6swe52Ta9yFs47x-605�a` �b.I�a �c-9_b9S���9� �d*cB �e*.TB �f[e �gf( �hf  l-gxFMqUq6swe52Ta9yFs47x-616�ih �j.\�i �kf  l-gxFMqUq6swe52Ta9yFs47x-612�lk �m.I�l �n-9jm9S���9� �o*.nB �pZό�qH �rf  l-gxFMqUq6swe52Ta9yFs47x-645�sf  l-gxFMqUq6swe52Ta9yFs47x-646�tf  l-gxFMqUq6swe52Ta9yFs47x-647�urst �v.qu �w-v9S���9� 	�x*7wB �yg  expanded-rst�z[y �{f  l-gxFMqUq6swe52Ta9yFs47x-654�|f  l-gxFMqUq6swe52Ta9yFs47x-655�}{| �~.z�} �-9~v9S���9� ��*.B ��f  l-gxFMqUq6swe52Ta9yFs47x-634��f  l-gxFMqUq6swe52Ta9yFs47x-635���� ��.z�� ��f  l-gxFMqUq6swe52Ta9yFs47x-625��f  l-gxFMqUq6swe52Ta9yFs47x-626��f  l-gxFMqUq6swe52Ta9yFs47x-627����� ��.q� ��-9��9S���9� ��*.�B ��f  bad quasiquote-form��g  s��� ��f  l-gxFMqUq6swe52Ta9yFs47x-313��� ��.��� ��g  msg��g  exp��g  sub����� ��f  l-gxFMqUq6swe52Ta9yFs47x-308��f  l-gxFMqUq6swe52Ta9yFs47x-309��f  l-gxFMqUq6swe52Ta9yFs47x-310����� ��.�� ��-9�9��9� ��*�B ��-���9� ��*��B ��[� ���(���*7�B ��[� ���(���g  apply��f  l-gxFMqUq6swe52Ta9yFs47x-708��f  l-gxFMqUq6swe52Ta9yFs47x-709���� ��.��� ��f  l-gxFMqUq6swe52Ta9yFs47x-694��f  l-gxFMqUq6swe52Ta9yFs47x-695���� ��.�� ��-9�����9� 	��*��B ��g  ele��g  nextp��g  fail-k��g  pvar-lst��g  depth��g  cata-fun����������� 	��--------- 	��f  l-gxFMqUq6swe52Ta9yFs47x-723��f  l-gxFMqUq6swe52Ta9yFs47x-724��f  l-gxFMqUq6swe52Ta9yFs47x-725��f  l-gxFMqUq6swe52Ta9yFs47x-726��f  l-gxFMqUq6swe52Ta9yFs47x-727��f  l-gxFMqUq6swe52Ta9yFs47x-728��f  l-gxFMqUq6swe52Ta9yFs47x-729��f  l-gxFMqUq6swe52Ta9yFs47x-730��f  l-gxFMqUq6swe52Ta9yFs47x-731����������� 	��.��� ��-9��9� ��*��B ��[� ���(����(���(ƌ��g  generate-temporaries��(((( ��g  if��g  bx��g  fail-to��:�v� ��---- ��f  l-gxFMqUq6swe52Ta9yFs47x-795��f  l-gxFMqUq6swe52Ta9yFs47x-796��f  l-gxFMqUq6swe52Ta9yFs47x-797��f  l-gxFMqUq6swe52Ta9yFs47x-798������ ��.��� ��g  tests��g  new-pvar-lst��g  new-cata-defs��g  new-dotted-vars������ ��f  l-gxFMqUq6swe52Ta9yFs47x-788��f  l-gxFMqUq6swe52Ta9yFs47x-789��f  l-gxFMqUq6swe52Ta9yFs47x-790��f  l-gxFMqUq6swe52Ta9yFs47x-791������ ��.��� ��g  body-exp��� ��f  l-gxFMqUq6swe52Ta9yFs47x-781��� ��.��� ��g  tag��g  items���� ��f  l-gxFMqUq6swe52Ta9yFs47x-777��f  l-gxFMqUq6swe52Ta9yFs47x-778���� ��.��� ��-9�9�99��9��9� ��*��B ��g  and��*��B ��g  pair?��*��B ��g  eq?��*��B ��*��B ��*��B ��g  let��*��B ��*��B ��((((( ��g  ax��:��v� ��----- ��f  l-gxFMqUq6swe52Ta9yFs47x-762��f  l-gxFMqUq6swe52Ta9yFs47x-763��f  l-gxFMqUq6swe52Ta9yFs47x-764� f  l-gxFMqUq6swe52Ta9yFs47x-765�f  l-gxFMqUq6swe52Ta9yFs47x-766����  �.�� �f  l-gxFMqUq6swe52Ta9yFs47x-755�f  l-gxFMqUq6swe52Ta9yFs47x-756�f  l-gxFMqUq6swe52Ta9yFs47x-757�f  l-gxFMqUq6swe52Ta9yFs47x-758� �	.�� �
g  attr-exp�
� �f  l-gxFMqUq6swe52Ta9yFs47x-746�f  l-gxFMqUq6swe52Ta9yFs47x-747� �.� �g  
attr-items��� �f  l-gxFMqUq6swe52Ta9yFs47x-740�f  l-gxFMqUq6swe52Ta9yFs47x-741�f  l-gxFMqUq6swe52Ta9yFs47x-742� �. �-99	999��9� �*�B �*�B �*�B �*�B �*�B �*�B �*�B �*�B � *�B �!g  attr-lst�"g  body-lst�#g  attr-key-lst�$!"
�#������� �%------------ �&f  l-gxFMqUq6swe52Ta9yFs47x-845�'f  l-gxFMqUq6swe52Ta9yFs47x-846�(f  l-gxFMqUq6swe52Ta9yFs47x-847�)f  l-gxFMqUq6swe52Ta9yFs47x-848�*f  l-gxFMqUq6swe52Ta9yFs47x-849�+f  l-gxFMqUq6swe52Ta9yFs47x-850�,f  l-gxFMqUq6swe52Ta9yFs47x-851�-f  l-gxFMqUq6swe52Ta9yFs47x-852�.f  l-gxFMqUq6swe52Ta9yFs47x-853�/f  l-gxFMqUq6swe52Ta9yFs47x-854�0f  l-gxFMqUq6swe52Ta9yFs47x-855�1f  l-gxFMqUq6swe52Ta9yFs47x-856�2&'()*+,-./01 �3.$%2 �4-93�9� �5*4B �6[5 �76( �8g  ->�9*84B �:[9 �;:f��<(;��=6< �>=( �?(> �@?(��A6f �BA( �C(B �DC(��E7( �F(E �GF(��H(= �IH(��J(A �KJ(��L(7 �ML(��N(f �ON(��P)(��Qg  string?�Rg  char?�Sg  number?�Tg  boolean?�U((( �V�v� �Wf  l-gxFMqUq6swe52Ta9yFs47x-1167�Xf  l-gxFMqUq6swe52Ta9yFs47x-1168�Yf  l-gxFMqUq6swe52Ta9yFs47x-1169�ZWXY �[.VZ �\f  l-gxFMqUq6swe52Ta9yFs47x-1160�]f  l-gxFMqUq6swe52Ta9yFs47x-1161�^f  l-gxFMqUq6swe52Ta9yFs47x-1162�_f  l-gxFMqUq6swe52Ta9yFs47x-1163�`\]^_ �a.��` �bg  atag�cg  literal�dbc �ef  l-gxFMqUq6swe52Ta9yFs47x-1149�ff  l-gxFMqUq6swe52Ta9yFs47x-1150�gf  l-gxFMqUq6swe52Ta9yFs47x-1151�hefg �i.dh �j-9[9ai93�9� �k*�jB �lg  binding�m*ljB �n*�jB �o*�jB �p*�jB �qg  equal?�r*qjB �sg  cadr�t*sjB �utm �vg  kwd�wg  i�xbw �yf  l-gxFMqUq6swe52Ta9yFs47x-1134�zf  l-gxFMqUq6swe52Ta9yFs47x-1135�{f  l-gxFMqUq6swe52Ta9yFs47x-1136�|yz{ �}.x| �~-}93�9� �*v~B ��f  bad attribute pattern��f  l-gxFMqUq6swe52Ta9yFs47x-1118��f  l-gxFMqUq6swe52Ta9yFs47x-1119��f  l-gxFMqUq6swe52Ta9yFs47x-1120����� ��.x� ��-�93�9� ��*v�B ��f  l-gxFMqUq6swe52Ta9yFs47x-1105��f  l-gxFMqUq6swe52Ta9yFs47x-1106��f  l-gxFMqUq6swe52Ta9yFs47x-1107����� ��.V� ��f  l-gxFMqUq6swe52Ta9yFs47x-1098��f  l-gxFMqUq6swe52Ta9yFs47x-1099��f  l-gxFMqUq6swe52Ta9yFs47x-1100��f  l-gxFMqUq6swe52Ta9yFs47x-1101������ ��.��� ��g  var��b� ��f  l-gxFMqUq6swe52Ta9yFs47x-1087��f  l-gxFMqUq6swe52Ta9yFs47x-1088��f  l-gxFMqUq6swe52Ta9yFs47x-1089����� ��.�� ��-9�9��93�9� ��*��B ��*l�B ��*��B ��*��B ��*��B ��*s�B ���� ��� ��f  <sxml-match pattern: catamorphism not allowed in this context��g  ct����v� ��f  l-gxFMqUq6swe52Ta9yFs47x-1070��f  l-gxFMqUq6swe52Ta9yFs47x-1071��f  l-gxFMqUq6swe52Ta9yFs47x-1072��f  l-gxFMqUq6swe52Ta9yFs47x-1073������ ��.��� ��f  l-gxFMqUq6swe52Ta9yFs47x-1063��f  l-gxFMqUq6swe52Ta9yFs47x-1064��f  l-gxFMqUq6swe52Ta9yFs47x-1065��f  l-gxFMqUq6swe52Ta9yFs47x-1066������ ��.��� ��g  ctemp��� ��f  l-gxFMqUq6swe52Ta9yFs47x-1053��� ��.��� ��g  cvar��b� ��f  l-gxFMqUq6swe52Ta9yFs47x-1047��f  l-gxFMqUq6swe52Ta9yFs47x-1048��f  l-gxFMqUq6swe52Ta9yFs47x-1049����� ��.�� ��-9�9�9��93�9� ��*��B ��*l�B ��*��B ��*��B ��*��B ��*s�B ���� ��� ��f  l-gxFMqUq6swe52Ta9yFs47x-1032��f  l-gxFMqUq6swe52Ta9yFs47x-1033��f  l-gxFMqUq6swe52Ta9yFs47x-1034��f  l-gxFMqUq6swe52Ta9yFs47x-1035������ ��.��� ��f  l-gxFMqUq6swe52Ta9yFs47x-1025��f  l-gxFMqUq6swe52Ta9yFs47x-1026��f  l-gxFMqUq6swe52Ta9yFs47x-1027��f  l-gxFMqUq6swe52Ta9yFs47x-1028������ ��.��� ��f  l-gxFMqUq6swe52Ta9yFs47x-1016��� ��.��� ��g  cata��b�� ��f  l-gxFMqUq6swe52Ta9yFs47x-1008��f  l-gxFMqUq6swe52Ta9yFs47x-1009��f  l-gxFMqUq6swe52Ta9yFs47x-1010��f  l-gxFMqUq6swe52Ta9yFs47x-1011������ ��.��� ��-9�9�99��93�9� ��*��B ��*l�B ��*��B ��*��B ��*��B ��*s�B ���� ��� ���v ��f  l-gxFMqUq6swe52Ta9yFs47x-995��f  l-gxFMqUq6swe52Ta9yFs47x-996���� ��.��� ��f  l-gxFMqUq6swe52Ta9yFs47x-988��f  l-gxFMqUq6swe52Ta9yFs47x-989��f  l-gxFMqUq6swe52Ta9yFs47x-990��f  l-gxFMqUq6swe52Ta9yFs47x-991������ ��.��� ��g  default��b�� ��f  l-gxFMqUq6swe52Ta9yFs47x-975��f  l-gxFMqUq6swe52Ta9yFs47x-976��f  l-gxFMqUq6swe52Ta9yFs47x-977��f  l-gxFMqUq6swe52Ta9yFs47x-978������ ��.��� ��-9�9��93�9� ��*��B ��*l�B ��*��B ��*��B ��*��B � *s�B � � ���v �f  l-gxFMqUq6swe52Ta9yFs47x-958�f  l-gxFMqUq6swe52Ta9yFs47x-959�f  l-gxFMqUq6swe52Ta9yFs47x-960� �. �f  l-gxFMqUq6swe52Ta9yFs47x-951�	f  l-gxFMqUq6swe52Ta9yFs47x-952�
f  l-gxFMqUq6swe52Ta9yFs47x-953�f  l-gxFMqUq6swe52Ta9yFs47x-954�	
 �.�� �f  l-gxFMqUq6swe52Ta9yFs47x-941� �.�� �b�� �f  l-gxFMqUq6swe52Ta9yFs47x-933�f  l-gxFMqUq6swe52Ta9yFs47x-934�f  l-gxFMqUq6swe52Ta9yFs47x-935�f  l-gxFMqUq6swe52Ta9yFs47x-936� �.� �-99993�9� �*�B �*lB �*�B �*�B �*�B �*sB � � f  l-gxFMqUq6swe52Ta9yFs47x-918�!f  l-gxFMqUq6swe52Ta9yFs47x-919�"f  l-gxFMqUq6swe52Ta9yFs47x-920�# !" �$.# �%f  l-gxFMqUq6swe52Ta9yFs47x-911�&f  l-gxFMqUq6swe52Ta9yFs47x-912�'f  l-gxFMqUq6swe52Ta9yFs47x-913�(f  l-gxFMqUq6swe52Ta9yFs47x-914�)%&'( �*.��) �+f  l-gxFMqUq6swe52Ta9yFs47x-902�,+ �-.��, �.b��� �/f  l-gxFMqUq6swe52Ta9yFs47x-892�0f  l-gxFMqUq6swe52Ta9yFs47x-893�1f  l-gxFMqUq6swe52Ta9yFs47x-894�2f  l-gxFMqUq6swe52Ta9yFs47x-895�3f  l-gxFMqUq6swe52Ta9yFs47x-896�4/0123 �5..�4 �6-9$9*99-593�9� �7*�6B �8*l6B �9*�6B �:*�6B �;*�6B �<*s6B �=<8 �>g  matched-attrs�?�>v �@f  l-gxFMqUq6swe52Ta9yFs47x-875�Af  l-gxFMqUq6swe52Ta9yFs47x-876�Bf  l-gxFMqUq6swe52Ta9yFs47x-877�C@AB �D.?C �Ef  l-gxFMqUq6swe52Ta9yFs47x-868�Ff  l-gxFMqUq6swe52Ta9yFs47x-869�Gf  l-gxFMqUq6swe52Ta9yFs47x-870�Hf  l-gxFMqUq6swe52Ta9yFs47x-871�IEFGH �J.��I �K� �Lf  l-gxFMqUq6swe52Ta9yFs47x-861�ML �N.K�M �O-9D9JN93�9� �P*�OB �Q*�OB �R*�OB �S:v� �Tf  l-gxFMqUq6swe52Ta9yFs47x-827�Uf  l-gxFMqUq6swe52Ta9yFs47x-828�Vf  l-gxFMqUq6swe52Ta9yFs47x-829�WTUV �X.SW �Yg  
next-tests�ZY��� �[f  l-gxFMqUq6swe52Ta9yFs47x-820�\f  l-gxFMqUq6swe52Ta9yFs47x-821�]f  l-gxFMqUq6swe52Ta9yFs47x-822�^f  l-gxFMqUq6swe52Ta9yFs47x-823�_[\]^ �`.Z�_ �a������ �b------ �cf  l-gxFMqUq6swe52Ta9yFs47x-809�df  l-gxFMqUq6swe52Ta9yFs47x-810�ef  l-gxFMqUq6swe52Ta9yFs47x-811�ff  l-gxFMqUq6swe52Ta9yFs47x-812�gf  l-gxFMqUq6swe52Ta9yFs47x-813�hf  l-gxFMqUq6swe52Ta9yFs47x-814�icdefgh �j.abi �k-9X9`99j�9� �l*�kB �mg  null?�n*mkB �og  ellipsis-allowed?�p����o����� 
�q---------- 
�rf  l-gxFMqUq6swe52Ta9yFs47x-1184�sf  l-gxFMqUq6swe52Ta9yFs47x-1185�tf  l-gxFMqUq6swe52Ta9yFs47x-1186�uf  l-gxFMqUq6swe52Ta9yFs47x-1187�vf  l-gxFMqUq6swe52Ta9yFs47x-1188�wf  l-gxFMqUq6swe52Ta9yFs47x-1189�xf  l-gxFMqUq6swe52Ta9yFs47x-1190�yf  l-gxFMqUq6swe52Ta9yFs47x-1191�zf  l-gxFMqUq6swe52Ta9yFs47x-1192�{f  l-gxFMqUq6swe52Ta9yFs47x-1193�|rstuvwxyz{ 
�}.pq| �~-9}�9� �*~B ��[ ���( ��*8~B ��[� ���f���(������ ���:v ��f  l-gxFMqUq6swe52Ta9yFs47x-1238��f  l-gxFMqUq6swe52Ta9yFs47x-1239��f  l-gxFMqUq6swe52Ta9yFs47x-1240����� ��.�� ��f  l-gxFMqUq6swe52Ta9yFs47x-1231��f  l-gxFMqUq6swe52Ta9yFs47x-1232��f  l-gxFMqUq6swe52Ta9yFs47x-1233��f  l-gxFMqUq6swe52Ta9yFs47x-1234������ ��.Z�� ��f  l-gxFMqUq6swe52Ta9yFs47x-1222��� ��.��� ���� ��f  l-gxFMqUq6swe52Ta9yFs47x-1218��f  l-gxFMqUq6swe52Ta9yFs47x-1219���� ��.��� ��-9�9�99��9}�9� ��*��B ��f  1improper list pattern not allowed in this context��-�9}�9� ��*�B ���f ��f  l-gxFMqUq6swe52Ta9yFs47x-1264��f  l-gxFMqUq6swe52Ta9yFs47x-1265��f  l-gxFMqUq6swe52Ta9yFs47x-1266����� ��.�� ��f  l-gxFMqUq6swe52Ta9yFs47x-1257��f  l-gxFMqUq6swe52Ta9yFs47x-1258��f  l-gxFMqUq6swe52Ta9yFs47x-1259��f  l-gxFMqUq6swe52Ta9yFs47x-1260������ ��.Z�� ��f  l-gxFMqUq6swe52Ta9yFs47x-1247��� ��.��� ��� ��f  l-gxFMqUq6swe52Ta9yFs47x-1245��� ��.��� ��-9�9�9��9}�9� ��*��B ��f  $ellipses not allowed in this context��:v ��f  l-gxFMqUq6swe52Ta9yFs47x-1213��f  l-gxFMqUq6swe52Ta9yFs47x-1214���� ��.��� ��f  l-gxFMqUq6swe52Ta9yFs47x-1206��f  l-gxFMqUq6swe52Ta9yFs47x-1207��f  l-gxFMqUq6swe52Ta9yFs47x-1208��f  l-gxFMqUq6swe52Ta9yFs47x-1209������ ��.Z�� ��f  l-gxFMqUq6swe52Ta9yFs47x-1199��� ��.K�� ��-9�9��9}�9� ��*��B ��-�9}�9� ��*�B ��g  tail��H��������� 
��f  l-gxFMqUq6swe52Ta9yFs47x-1306��f  l-gxFMqUq6swe52Ta9yFs47x-1307��f  l-gxFMqUq6swe52Ta9yFs47x-1308��f  l-gxFMqUq6swe52Ta9yFs47x-1309��f  l-gxFMqUq6swe52Ta9yFs47x-1310��f  l-gxFMqUq6swe52Ta9yFs47x-1311��f  l-gxFMqUq6swe52Ta9yFs47x-1312��f  l-gxFMqUq6swe52Ta9yFs47x-1313��f  l-gxFMqUq6swe52Ta9yFs47x-1314��f  l-gxFMqUq6swe52Ta9yFs47x-1315������������ 
��.�q� ��-999��9� ��*��B ��g  values��g  npv��� ��f  l-gxFMqUq6swe52Ta9yFs47x-1334��� ��.��� ����� ��f  l-gxFMqUq6swe52Ta9yFs47x-1328��f  l-gxFMqUq6swe52Ta9yFs47x-1329��f  l-gxFMqUq6swe52Ta9yFs47x-1330����� ��.�� ��-9�9�999��9� ��*��B ��*�B ��g  fail��*��B ��g  t-1316��g  t-1317��g  t-1318��g  t-1319������ ��g  m-gxFMqUq6swe52Ta9yFs47x-1324���, ������ ��f  l-gxFMqUq6swe52Ta9yFs47x-1341��f  l-gxFMqUq6swe52Ta9yFs47x-1342��f  l-gxFMqUq6swe52Ta9yFs47x-1343��f  l-gxFMqUq6swe52Ta9yFs47x-1344������ ��.��� ��-99�99��9� 
��*��B ��f  l-gxFMqUq6swe52Ta9yFs47x-1356��� ��.��� ��g  new-exp������ ��f  l-gxFMqUq6swe52Ta9yFs47x-1349��f  l-gxFMqUq6swe52Ta9yFs47x-1350� f  l-gxFMqUq6swe52Ta9yFs47x-1351�f  l-gxFMqUq6swe52Ta9yFs47x-1352���  �.�� �-9�999�99��9� �*�B �*B �g  cdr�*B �	*�B �
	 �*��B �w �f  l-gxFMqUq6swe52Ta9yFs47x-1437� �.� �g  temp-item-pvar-lst� �f  l-gxFMqUq6swe52Ta9yFs47x-1392� �.� �g  final-tests�g  final-pvar-lst�g  final-cata-defs�g  final-dotted-vars� �f  l-gxFMqUq6swe52Ta9yFs47x-1388�f  l-gxFMqUq6swe52Ta9yFs47x-1389�f  l-gxFMqUq6swe52Ta9yFs47x-1390�f  l-gxFMqUq6swe52Ta9yFs47x-1391� �.� � g  
item-tests�!g  item-pvar-lst�"g  item-cata-defs�#g  item-dotted-vars�$g  
tail-tests�%g  tail-pvar-lst�&g  tail-cata-defs�'g  tail-dotted-vars�( !"#$%&' �)-------- �*f  l-gxFMqUq6swe52Ta9yFs47x-1367�+f  l-gxFMqUq6swe52Ta9yFs47x-1368�,f  l-gxFMqUq6swe52Ta9yFs47x-1369�-f  l-gxFMqUq6swe52Ta9yFs47x-1370�.f  l-gxFMqUq6swe52Ta9yFs47x-1371�/f  l-gxFMqUq6swe52Ta9yFs47x-1372�0f  l-gxFMqUq6swe52Ta9yFs47x-1373�1f  l-gxFMqUq6swe52Ta9yFs47x-1374�2*+,-./01 �3.()2 �4g  t-1320�5g  t-1321�6g  t-1322�7g  t-1323�84567 �9f  l-gxFMqUq6swe52Ta9yFs47x-1363�:f  l-gxFMqUq6swe52Ta9yFs47x-1364�;f  l-gxFMqUq6swe52Ta9yFs47x-1365�<f  l-gxFMqUq6swe52Ta9yFs47x-1366�=9:;< �>.8�= �?-9999939>9�99��9� �@*H?B �A@ �Bf  l-gxFMqUq6swe52Ta9yFs47x-1439�CB �D.�C �E-9D999939>9�99��9� �F*HEB �GF �Hf  l-gxFMqUq6swe52Ta9yFs47x-1441�IH �J.�I �K-9J999939>9�99��9� �L*�KB �ML �Ng  xa�Og  xb�PNO �Qf  l-gxFMqUq6swe52Ta9yFs47x-1449�Rf  l-gxFMqUq6swe52Ta9yFs47x-1450�SQR �T.P�S �Ug  a�Vg  b�WUV �Xf  l-gxFMqUq6swe52Ta9yFs47x-1444�Yf  l-gxFMqUq6swe52Ta9yFs47x-1445�ZXY �[.W�Z �\-9T99[999939>9�99��9� �]*�\B �^(((((fffffff �_g  letrec�`g  	tail-body�ag  	item-body�bg  
final-body�cg  ipv�dg  gpv�eg  tpv�fg  	item-void�gg  	tail-void�hg  	item-null�ig  	item-cons�j:�`abcdefghi �kf  l-gxFMqUq6swe52Ta9yFs47x-1397�lf  l-gxFMqUq6swe52Ta9yFs47x-1398�mf  l-gxFMqUq6swe52Ta9yFs47x-1399�nf  l-gxFMqUq6swe52Ta9yFs47x-1400�of  l-gxFMqUq6swe52Ta9yFs47x-1401�pf  l-gxFMqUq6swe52Ta9yFs47x-1402�qf  l-gxFMqUq6swe52Ta9yFs47x-1403�rf  l-gxFMqUq6swe52Ta9yFs47x-1404�sf  l-gxFMqUq6swe52Ta9yFs47x-1405�tf  l-gxFMqUq6swe52Ta9yFs47x-1406�uf  l-gxFMqUq6swe52Ta9yFs47x-1407�vf  l-gxFMqUq6swe52Ta9yFs47x-1408�wklmnopqrstuv �x.j%w �y-9x999939>9�99��9� �z*_yB �{g  
match-tail�|*{yB �}*OyB �~*�yB �*�yB ��~ ��g  
match-item��*�yB ��~ ��*�yB ��*�yB ��*yB ��g  match-dotted��*�yB ��*�yB ��g  tail-res��*�yB ��*�yB ��	 ��	 ��g  res��*�yB ��g  new-x��*�yB ���� ��� ��g  last-tail-res��*�yB ��H�������� 	��f  l-gxFMqUq6swe52Ta9yFs47x-1462��f  l-gxFMqUq6swe52Ta9yFs47x-1463��f  l-gxFMqUq6swe52Ta9yFs47x-1464��f  l-gxFMqUq6swe52Ta9yFs47x-1465��f  l-gxFMqUq6swe52Ta9yFs47x-1466��f  l-gxFMqUq6swe52Ta9yFs47x-1467��f  l-gxFMqUq6swe52Ta9yFs47x-1468��f  l-gxFMqUq6swe52Ta9yFs47x-1469��f  l-gxFMqUq6swe52Ta9yFs47x-1470����������� 	��.��� ��-9��9� ��*�B ��[� ���( ��*8�B ��[� ���f���(������ ��g  nx��:��v� ��f  l-gxFMqUq6swe52Ta9yFs47x-1522��f  l-gxFMqUq6swe52Ta9yFs47x-1523��f  l-gxFMqUq6swe52Ta9yFs47x-1524��f  l-gxFMqUq6swe52Ta9yFs47x-1525��f  l-gxFMqUq6swe52Ta9yFs47x-1526������� ��.��� ��f  l-gxFMqUq6swe52Ta9yFs47x-1515��f  l-gxFMqUq6swe52Ta9yFs47x-1516��f  l-gxFMqUq6swe52Ta9yFs47x-1517��f  l-gxFMqUq6swe52Ta9yFs47x-1518������ ��.Z�� ���� ��f  l-gxFMqUq6swe52Ta9yFs47x-1504��f  l-gxFMqUq6swe52Ta9yFs47x-1505���� ��.��� ��f  l-gxFMqUq6swe52Ta9yFs47x-1500��f  l-gxFMqUq6swe52Ta9yFs47x-1501���� ��.��� ��-9�9�99��9��9� ��*��B ��*��B ��*��B ��*�B ��g  car��*��B ���f ��f  l-gxFMqUq6swe52Ta9yFs47x-1554��f  l-gxFMqUq6swe52Ta9yFs47x-1555��f  l-gxFMqUq6swe52Ta9yFs47x-1556��f  l-gxFMqUq6swe52Ta9yFs47x-1557��f  l-gxFMqUq6swe52Ta9yFs47x-1558������� ��.��� ��f  l-gxFMqUq6swe52Ta9yFs47x-1547��f  l-gxFMqUq6swe52Ta9yFs47x-1548��f  l-gxFMqUq6swe52Ta9yFs47x-1549��f  l-gxFMqUq6swe52Ta9yFs47x-1550������ ��.Z�� ��f  l-gxFMqUq6swe52Ta9yFs47x-1535��f  l-gxFMqUq6swe52Ta9yFs47x-1536���� ��.��� ��f  l-gxFMqUq6swe52Ta9yFs47x-1533��� ��.��� ��-9�9�9��9��9� ��*��B ��*��B ��*��B ��*�B ��*��B ��(f���f  +bad pattern syntax (not an element pattern)��f  7bad pattern syntax (symbol not allowed in this context)��:�v� ��f  l-gxFMqUq6swe52Ta9yFs47x-1650��f  l-gxFMqUq6swe52Ta9yFs47x-1651��f  l-gxFMqUq6swe52Ta9yFs47x-1652��f  l-gxFMqUq6swe52Ta9yFs47x-1653������ ��.��� ��f  l-gxFMqUq6swe52Ta9yFs47x-1643��f  l-gxFMqUq6swe52Ta9yFs47x-1644��f  l-gxFMqUq6swe52Ta9yFs47x-1645��f  l-gxFMqUq6swe52Ta9yFs47x-1646������ ��.Z�� ��� ��f  l-gxFMqUq6swe52Ta9yFs47x-1636��� ��.��� ��c ��f  l-gxFMqUq6swe52Ta9yFs47x-1634��� ��.��� ��-9�9�99��9��9� ��*��B � *��B �*��B �*q�B �*��B �*��B �*�B �f  l-gxFMqUq6swe52Ta9yFs47x-1579� �.;� �	f  l-gxFMqUq6swe52Ta9yFs47x-1573�
	 �.��
 ��H �f  l-gxFMqUq6swe52Ta9yFs47x-1569�f  l-gxFMqUq6swe52Ta9yFs47x-1570� �.� �-99999��9� �*�B �:�v �f  l-gxFMqUq6swe52Ta9yFs47x-1599�f  l-gxFMqUq6swe52Ta9yFs47x-1600�f  l-gxFMqUq6swe52Ta9yFs47x-1601� �. �f  l-gxFMqUq6swe52Ta9yFs47x-1592�f  l-gxFMqUq6swe52Ta9yFs47x-1593�f  l-gxFMqUq6swe52Ta9yFs47x-1594�f  l-gxFMqUq6swe52Ta9yFs47x-1595� �.Z� �g  more-pvar-lst� g  more-cata-defs�!g  more-dotted-vars�" ! �#f  l-gxFMqUq6swe52Ta9yFs47x-1584�$f  l-gxFMqUq6swe52Ta9yFs47x-1585�%f  l-gxFMqUq6swe52Ta9yFs47x-1586�&#$% �'."& �(-9999'9999��9� �)*�(B �**(B �+f  l-gxFMqUq6swe52Ta9yFs47x-1616�,f  l-gxFMqUq6swe52Ta9yFs47x-1617�-f  l-gxFMqUq6swe52Ta9yFs47x-1618�.+,- �/.S. �0g  after-tests�1g  after-pvar-lst�2g  after-cata-defs�3g  after-dotted-vars�40123 �5f  l-gxFMqUq6swe52Ta9yFs47x-1609�6f  l-gxFMqUq6swe52Ta9yFs47x-1610�7f  l-gxFMqUq6swe52Ta9yFs47x-1611�8f  l-gxFMqUq6swe52Ta9yFs47x-1612�95678 �:.4�9 �;-9/9:999��9� �<*�;B �=*�;B �>f  l-gxFMqUq6swe52Ta9yFs47x-1491�?f  l-gxFMqUq6swe52Ta9yFs47x-1492�@f  l-gxFMqUq6swe52Ta9yFs47x-1493�Af  l-gxFMqUq6swe52Ta9yFs47x-1494�B>?@A �C.��B �Df  l-gxFMqUq6swe52Ta9yFs47x-1484�Ef  l-gxFMqUq6swe52Ta9yFs47x-1485�Ff  l-gxFMqUq6swe52Ta9yFs47x-1486�Gf  l-gxFMqUq6swe52Ta9yFs47x-1487�HDEFG �I.Z�H �Jf  l-gxFMqUq6swe52Ta9yFs47x-1477�KJ �L.��K �Mf  l-gxFMqUq6swe52Ta9yFs47x-1475�NM �O.K�N �P-9C9I99LO9��9� �Q*�PB �R*�PB �S*�PB �T*PB �U*�PB �Vf  &duplicate pattern variable not allowed�Wg  nct�X�Wv �Yf  l-gxFMqUq6swe52Ta9yFs47x-386�Zf  l-gxFMqUq6swe52Ta9yFs47x-387�[f  l-gxFMqUq6swe52Ta9yFs47x-388�\YZ[ �].X\ �^g  	new-ctemp�_^ �`f  l-gxFMqUq6swe52Ta9yFs47x-381�a` �b._�a �cg  cfun�d�c� �ef  l-gxFMqUq6swe52Ta9yFs47x-371�ff  l-gxFMqUq6swe52Ta9yFs47x-372�gf  l-gxFMqUq6swe52Ta9yFs47x-373�hefg �i.dh �j-9]99b9i�9� �k*�jB �l*OjB �m� �nf  l-gxFMqUq6swe52Ta9yFs47x-1658�on �p.m�o �q-9p9�9� �r*qB �s[r �ts( �ug  guard�v*uqB �w[v �xwf��yx挤zty��{(z((( �|*8qB �}[| �~}f��(~���s ���y���(�((( ��g  gexp��g  action0��g  fail-exp����������� 	��f  l-gxFMqUq6swe52Ta9yFs47x-1697��f  l-gxFMqUq6swe52Ta9yFs47x-1698��f  l-gxFMqUq6swe52Ta9yFs47x-1699��f  l-gxFMqUq6swe52Ta9yFs47x-1700��f  l-gxFMqUq6swe52Ta9yFs47x-1701��f  l-gxFMqUq6swe52Ta9yFs47x-1702��f  l-gxFMqUq6swe52Ta9yFs47x-1703��f  l-gxFMqUq6swe52Ta9yFs47x-1704��f  l-gxFMqUq6swe52Ta9yFs47x-1705����������� 	��.��� ��-�9p9�9� ��*��B ��*��B ��*��B ��*q�B ��sf ���y���(�((( ��g  atom��� ���������� ��f  l-gxFMqUq6swe52Ta9yFs47x-1719��f  l-gxFMqUq6swe52Ta9yFs47x-1720��f  l-gxFMqUq6swe52Ta9yFs47x-1721��f  l-gxFMqUq6swe52Ta9yFs47x-1722��f  l-gxFMqUq6swe52Ta9yFs47x-1723��f  l-gxFMqUq6swe52Ta9yFs47x-1724��f  l-gxFMqUq6swe52Ta9yFs47x-1725��f  l-gxFMqUq6swe52Ta9yFs47x-1726���������� ��.�)� ��-�9p9�9� ��*��B ��*��B ��*��B ��*q�B ��t挤�(�((( ���挤�(�((( ���������� ��f  l-gxFMqUq6swe52Ta9yFs47x-1770��f  l-gxFMqUq6swe52Ta9yFs47x-1771��f  l-gxFMqUq6swe52Ta9yFs47x-1772��f  l-gxFMqUq6swe52Ta9yFs47x-1773��f  l-gxFMqUq6swe52Ta9yFs47x-1774��f  l-gxFMqUq6swe52Ta9yFs47x-1775��f  l-gxFMqUq6swe52Ta9yFs47x-1776��f  l-gxFMqUq6swe52Ta9yFs47x-1777���������� ��.�)� ��-�9p9�9� ��*��B ���挤�(�((( ��������� ��------- ��f  l-gxFMqUq6swe52Ta9yFs47x-1789��f  l-gxFMqUq6swe52Ta9yFs47x-1790��f  l-gxFMqUq6swe52Ta9yFs47x-1791��f  l-gxFMqUq6swe52Ta9yFs47x-1792��f  l-gxFMqUq6swe52Ta9yFs47x-1793��f  l-gxFMqUq6swe52Ta9yFs47x-1794��f  l-gxFMqUq6swe52Ta9yFs47x-1795��������� ��.��� ��-�9p9�9� ��*��B ���y���(�((( ���挤�(�((( ��fy���(�((( ��f挤�(�((( ��(y���(�((( ��(挤�(�((( ���c����� ��f  l-gxFMqUq6swe52Ta9yFs47x-2206��f  l-gxFMqUq6swe52Ta9yFs47x-2207��f  l-gxFMqUq6swe52Ta9yFs47x-2208��f  l-gxFMqUq6swe52Ta9yFs47x-2209��f  l-gxFMqUq6swe52Ta9yFs47x-2210��f  l-gxFMqUq6swe52Ta9yFs47x-2211��f  l-gxFMqUq6swe52Ta9yFs47x-2212��������� ��.��� ��-�9p9�9� ��*��B ��*q�B ��*q�B ���c������ ��f  l-gxFMqUq6swe52Ta9yFs47x-2173��f  l-gxFMqUq6swe52Ta9yFs47x-2174��f  l-gxFMqUq6swe52Ta9yFs47x-2175��f  l-gxFMqUq6swe52Ta9yFs47x-2176��f  l-gxFMqUq6swe52Ta9yFs47x-2177��f  l-gxFMqUq6swe52Ta9yFs47x-2178��f  l-gxFMqUq6swe52Ta9yFs47x-2179��f  l-gxFMqUq6swe52Ta9yFs47x-2180���������� ��.�)� ��-�9p9�9� ��*��B ��*��B ��*q�B ��*q�B ��f  l-gxFMqUq6swe52Ta9yFs47x-2041��f  l-gxFMqUq6swe52Ta9yFs47x-2042��f  l-gxFMqUq6swe52Ta9yFs47x-2043����� ��.�� ��� ����� ��f  l-gxFMqUq6swe52Ta9yFs47x-2021��f  l-gxFMqUq6swe52Ta9yFs47x-2022��f  l-gxFMqUq6swe52Ta9yFs47x-2023��f  l-gxFMqUq6swe52Ta9yFs47x-2024� f  l-gxFMqUq6swe52Ta9yFs47x-2025�f  l-gxFMqUq6swe52Ta9yFs47x-2026�f  l-gxFMqUq6swe52Ta9yFs47x-2027�f  l-gxFMqUq6swe52Ta9yFs47x-2028�����  �.�) �-9�99p9�9� �*qB �g  failure�	-9�9� �
*	B ��v �f  l-gxFMqUq6swe52Ta9yFs47x-2056�f  l-gxFMqUq6swe52Ta9yFs47x-2057� �.� �g  result���� �f  l-gxFMqUq6swe52Ta9yFs47x-2049�f  l-gxFMqUq6swe52Ta9yFs47x-2050�f  l-gxFMqUq6swe52Ta9yFs47x-2051�f  l-gxFMqUq6swe52Ta9yFs47x-2052� �.� �-9999p9�9� �*�B �f  l-gxFMqUq6swe52Ta9yFs47x-1977�f  l-gxFMqUq6swe52Ta9yFs47x-1978�f  l-gxFMqUq6swe52Ta9yFs47x-1979� �.� �� ������ 	� f  l-gxFMqUq6swe52Ta9yFs47x-1955�!f  l-gxFMqUq6swe52Ta9yFs47x-1956�"f  l-gxFMqUq6swe52Ta9yFs47x-1957�#f  l-gxFMqUq6swe52Ta9yFs47x-1958�$f  l-gxFMqUq6swe52Ta9yFs47x-1959�%f  l-gxFMqUq6swe52Ta9yFs47x-1960�&f  l-gxFMqUq6swe52Ta9yFs47x-1961�'f  l-gxFMqUq6swe52Ta9yFs47x-1962�(f  l-gxFMqUq6swe52Ta9yFs47x-1963�) !"#$%&'( 	�*.�) �+-99*9p9�9� �,*q+B �-v� �.f  l-gxFMqUq6swe52Ta9yFs47x-1983�/f  l-gxFMqUq6swe52Ta9yFs47x-1984�0./ �1.-�0 �2-9199*9p9�9� �3*�2B �4*�2B �5f  l-gxFMqUq6swe52Ta9yFs47x-2000�6f  l-gxFMqUq6swe52Ta9yFs47x-2001�756 �8.�7 �9f  l-gxFMqUq6swe52Ta9yFs47x-1993�:f  l-gxFMqUq6swe52Ta9yFs47x-1994�;f  l-gxFMqUq6swe52Ta9yFs47x-1995�<f  l-gxFMqUq6swe52Ta9yFs47x-1996�=9:;< �>.�= �?-9899>*9p9�9� �@*�?B �Ag  list�Bf  l-gxFMqUq6swe52Ta9yFs47x-1917�Cf  l-gxFMqUq6swe52Ta9yFs47x-1918�Df  l-gxFMqUq6swe52Ta9yFs47x-1919�EBCD �F.�E �G������� �Hf  l-gxFMqUq6swe52Ta9yFs47x-1897�If  l-gxFMqUq6swe52Ta9yFs47x-1898�Jf  l-gxFMqUq6swe52Ta9yFs47x-1899�Kf  l-gxFMqUq6swe52Ta9yFs47x-1900�Lf  l-gxFMqUq6swe52Ta9yFs47x-1901�Mf  l-gxFMqUq6swe52Ta9yFs47x-1902�Nf  l-gxFMqUq6swe52Ta9yFs47x-1903�Of  l-gxFMqUq6swe52Ta9yFs47x-1904�PHIJKLMNO �Q.G)P �R-9F9Q9p9�9� �S*qRB �Tf  l-gxFMqUq6swe52Ta9yFs47x-1932�Uf  l-gxFMqUq6swe52Ta9yFs47x-1933�VTU �W.-�V �Xf  l-gxFMqUq6swe52Ta9yFs47x-1925�Yf  l-gxFMqUq6swe52Ta9yFs47x-1926�Zf  l-gxFMqUq6swe52Ta9yFs47x-1927�[f  l-gxFMqUq6swe52Ta9yFs47x-1928�\XYZ[ �].�\ �^-9W99]Q9p9�9� �_*�^B �`*�^B �a*�^B �bf  l-gxFMqUq6swe52Ta9yFs47x-1850�cf  l-gxFMqUq6swe52Ta9yFs47x-1851�df  l-gxFMqUq6swe52Ta9yFs47x-1852�ebcd �f.�e �g�������� 	�hf  l-gxFMqUq6swe52Ta9yFs47x-1828�if  l-gxFMqUq6swe52Ta9yFs47x-1829�jf  l-gxFMqUq6swe52Ta9yFs47x-1830�kf  l-gxFMqUq6swe52Ta9yFs47x-1831�lf  l-gxFMqUq6swe52Ta9yFs47x-1832�mf  l-gxFMqUq6swe52Ta9yFs47x-1833�nf  l-gxFMqUq6swe52Ta9yFs47x-1834�of  l-gxFMqUq6swe52Ta9yFs47x-1835�pf  l-gxFMqUq6swe52Ta9yFs47x-1836�qhijklmnop 	�r.g�q �s-9f9r9p9�9� �t*qsB �ug  exp-body�vu� �wf  l-gxFMqUq6swe52Ta9yFs47x-1856�xf  l-gxFMqUq6swe52Ta9yFs47x-1857�ywx �z.v�y �{-9z9f9r9p9�9� �|*�{B �}*�{B �~f  l-gxFMqUq6swe52Ta9yFs47x-1873�f  l-gxFMqUq6swe52Ta9yFs47x-1874��~ ��.�� ��f  l-gxFMqUq6swe52Ta9yFs47x-1866��f  l-gxFMqUq6swe52Ta9yFs47x-1867��f  l-gxFMqUq6swe52Ta9yFs47x-1868��f  l-gxFMqUq6swe52Ta9yFs47x-1869������ ��.�� ��-9�99�r9p9�9� ��*��B ��*��B ��*��B ��������� ��f  l-gxFMqUq6swe52Ta9yFs47x-1754��f  l-gxFMqUq6swe52Ta9yFs47x-1755��f  l-gxFMqUq6swe52Ta9yFs47x-1756��f  l-gxFMqUq6swe52Ta9yFs47x-1757��f  l-gxFMqUq6swe52Ta9yFs47x-1758��f  l-gxFMqUq6swe52Ta9yFs47x-1759��f  l-gxFMqUq6swe52Ta9yFs47x-1760��������� ��.��� ��-�9p9�9� ��*��B ���������� ��f  l-gxFMqUq6swe52Ta9yFs47x-1678��f  l-gxFMqUq6swe52Ta9yFs47x-1679��f  l-gxFMqUq6swe52Ta9yFs47x-1680��f  l-gxFMqUq6swe52Ta9yFs47x-1681��f  l-gxFMqUq6swe52Ta9yFs47x-1682��f  l-gxFMqUq6swe52Ta9yFs47x-1683��f  l-gxFMqUq6swe52Ta9yFs47x-1684��f  l-gxFMqUq6swe52Ta9yFs47x-1685���������� ��.�)� ��-�9p9�9� ��*��B ��*��B ��*��B ��*q�B ��g  sxml-match1��g  clause��/��� ��g  m-gxFMqUq6swe52Ta9yFs47x-2221���, ���--- ��f  l-gxFMqUq6swe52Ta9yFs47x-2226��f  l-gxFMqUq6swe52Ta9yFs47x-2227��f  l-gxFMqUq6swe52Ta9yFs47x-2228��f  l-gxFMqUq6swe52Ta9yFs47x-2229������ ��.��� ��� ��f  l-gxFMqUq6swe52Ta9yFs47x-2223��� ��.;�� ��-�9� ��*��B ��*O�B ��*��B ��*��B ��*�B ���� ��f  no matching clause found��*��B ����� ���� ��� ��(֌��(Č��g  let/ec��g  clause0��/���� ���---- ��f  l-gxFMqUq6swe52Ta9yFs47x-2235��f  l-gxFMqUq6swe52Ta9yFs47x-2236��f  l-gxFMqUq6swe52Ta9yFs47x-2237��f  l-gxFMqUq6swe52Ta9yFs47x-2238��f  l-gxFMqUq6swe52Ta9yFs47x-2239������� ��.��� ��-�9� ��*��B ��g  escape��*��B ��*��B ��*O�B ��g  call-with-values��*��B ��*��B ��� ��g  val��/��� ��g  m-gxFMqUq6swe52Ta9yFs47x-2246���, ���--- ��f  l-gxFMqUq6swe52Ta9yFs47x-2251��f  l-gxFMqUq6swe52Ta9yFs47x-2252��f  l-gxFMqUq6swe52Ta9yFs47x-2253��f  l-gxFMqUq6swe52Ta9yFs47x-2254������ ��.��� ��� ��f  l-gxFMqUq6swe52Ta9yFs47x-2248��� ��.;�� ��-�9� ��*_�B ��*c�B ��*O�B ��*��B ��� ��*��B ��g  sxml-match-let1��挤�(��(��(��g  syntag��g  synform��g  body0��/���v ��g  m-gxFMqUq6swe52Ta9yFs47x-2260���, ���---- ��f  l-gxFMqUq6swe52Ta9yFs47x-2265��f  l-gxFMqUq6swe52Ta9yFs47x-2266��f  l-gxFMqUq6swe52Ta9yFs47x-2267� f  l-gxFMqUq6swe52Ta9yFs47x-2268�f  l-gxFMqUq6swe52Ta9yFs47x-2269����  �.�� �� �f  l-gxFMqUq6swe52Ta9yFs47x-2262� �.; �-9 �	*�B �
) �
挤(��(��(��g  pat�/����v ��------ �f  l-gxFMqUq6swe52Ta9yFs47x-2277�f  l-gxFMqUq6swe52Ta9yFs47x-2278�f  l-gxFMqUq6swe52Ta9yFs47x-2279�f  l-gxFMqUq6swe52Ta9yFs47x-2280�f  l-gxFMqUq6swe52Ta9yFs47x-2281�f  l-gxFMqUq6swe52Ta9yFs47x-2282�f  l-gxFMqUq6swe52Ta9yFs47x-2283� �. �-9 �*�B �*�B �*B �*OB � *�B �!*�B �"f  could not match pattern ~s�#*"B �$g  each�%$) �&)%��'&挤(('��)((��*()��+g  pat0�,g  exp0�-/��+,��v 	�.�-------- 	�/f  l-gxFMqUq6swe52Ta9yFs47x-2293�0f  l-gxFMqUq6swe52Ta9yFs47x-2294�1f  l-gxFMqUq6swe52Ta9yFs47x-2295�2f  l-gxFMqUq6swe52Ta9yFs47x-2296�3f  l-gxFMqUq6swe52Ta9yFs47x-2297�4f  l-gxFMqUq6swe52Ta9yFs47x-2298�5f  l-gxFMqUq6swe52Ta9yFs47x-2299�6f  l-gxFMqUq6swe52Ta9yFs47x-2300�7f  l-gxFMqUq6swe52Ta9yFs47x-2301�8/01234567 	�9.-.8 �:-99 �;*�:B �<*�:B �=� �>� �?*:B �@*O:B �A*�:B �B*�:B �C*":B �Dg  sxml-match-let-help�E%挤F(E��G(F��H(G��Ig  	temp-name�JI �Kf  l-gxFMqUq6swe52Ta9yFs47x-2335�LK �M.J�L �ND����v �Of  l-gxFMqUq6swe52Ta9yFs47x-2318�Pf  l-gxFMqUq6swe52Ta9yFs47x-2319�Qf  l-gxFMqUq6swe52Ta9yFs47x-2320�Rf  l-gxFMqUq6swe52Ta9yFs47x-2321�Sf  l-gxFMqUq6swe52Ta9yFs47x-2322�Tf  l-gxFMqUq6swe52Ta9yFs47x-2323�Uf  l-gxFMqUq6swe52Ta9yFs47x-2324�VOPQRSTU �W.N�V �Xf  l-gxFMqUq6swe52Ta9yFs47x-2315�YX �Z.��Y �[-9MW9Z �\*�[B �]*�[B �^� �_f  l-gxFMqUq6swe52Ta9yFs47x-2359�`_ �a.^�` �b��v �cf  l-gxFMqUq6swe52Ta9yFs47x-2347�df  l-gxFMqUq6swe52Ta9yFs47x-2348�ef  l-gxFMqUq6swe52Ta9yFs47x-2349�ff  l-gxFMqUq6swe52Ta9yFs47x-2350�gf  l-gxFMqUq6swe52Ta9yFs47x-2351�hcdefg �i.b�h �jf  l-gxFMqUq6swe52Ta9yFs47x-2344�kj �l.��k �m-9ai9l �n*DmB �o�v �pf  l-gxFMqUq6swe52Ta9yFs47x-2368�qf  l-gxFMqUq6swe52Ta9yFs47x-2369�rf  l-gxFMqUq6swe52Ta9yFs47x-2370�spqr �t.os �uf  l-gxFMqUq6swe52Ta9yFs47x-2365�vu �w.��v �x-t9w �y*�xB �zf  l-gxFMqUq6swe52Ta9yFs47x-2392�{z �|.^�{ �}+,��v �~f  l-gxFMqUq6swe52Ta9yFs47x-2376�f  l-gxFMqUq6swe52Ta9yFs47x-2377��f  l-gxFMqUq6swe52Ta9yFs47x-2378��f  l-gxFMqUq6swe52Ta9yFs47x-2379��f  l-gxFMqUq6swe52Ta9yFs47x-2380��f  l-gxFMqUq6swe52Ta9yFs47x-2381��f  l-gxFMqUq6swe52Ta9yFs47x-2382��~����� ��.}�� ��-9|�9w ��*D�B �C 5      h��  �   ]4	
5 4 >  "  G   4"#$')C  h   >   ] C      6       g  dummy
		
 g  stx		
  		
	   CFG   h(   �   ]	4 5$  @ 6      �       g  x
		" g  tmp		"  g  filenamef  sxml/match.scm�
	+
�� 		"  g  
macro-typeg  syntax-rules�g  patternsg  stx   C5#R4"H$'IV h   -   ]C   %       g  dummy
		  		   CFG    h(   �   ]	4 5$  @ 6      �       g  x
		" g  tmp		"  g  filenamef  sxml/match.scm�
	.
�� 		"  g  
macro-typeg  syntax-rules�g  patterns  C5HRWX    h   �   ] 6�       g  x
		 g  msg		 g  obj			 g  sub			  g  filenamef  sxml/match.scm�
	1
��		2		��		2	�� 			  g  nameg  raise-syntax-error� CYR4"Z$'p� h   �   ]�C{       g  dummy
		 g  name		 g  lang			 g  p_			 g  r_			 g  body			  			   CFG      h(   �   ]	4 5$  @ 6      �       g  x
		" g  tmp		"  g  filenamef  sxml/match.scm�
	5	�� 		"  g  
macro-typeg  syntax-rules�g  patternsg  nameg  langg  provideg  p_g  ... g  requireg  r_g  ... g  bodyg  ...   C5ZR  h(   �   ]	 �$  	 ���"  $  C �C      �       g  x
		" g  t		"  g  filenamef  sxml/sxml-match.ss�
		��			��				��			%��			��			��			��	!		0�� 			"  g  nameg  nodeset?� C�R���   h0   �   ]"  
 6 �$   ��$   �C"���"���     �       g  s
		+  g  filenamef  sxml/sxml-match.ss�
		��	
		��			 ��			��			��			��			��			 ��			��			��	"		�� 		+  g  nameg  xml-element-tag� C�R�������        h   �   ] �$  
 ��"  �C  }       g  i
		  g  filenamef  sxml/sxml-match.ss�
		&��			<��				7��			K��			N��			F��			2�� 		   C��   h(   �   ]"   �C �$   �&  C"���"��� �       g  c
		' g  d		'  g  filenamef  sxml/sxml-match.ss�
		*��		!	0��			,��			5��			,��			D��			G��			0�� 			'	   C h8   �   ] �$  & �&  (  	 �6 �6CC      �       g  a
		2 g  b		2  g  filenamef  sxml/sxml-match.ss�
		��			��				��			.��			1��			��			��			Z��	!		��	*	"	,��	,		�� 		2	   C h8   �   ]"  
 6 �$   ��$  
 �6"���"���       �       g  s
		1  g  filenamef  sxml/sxml-match.ss�
		��	
	&	��		&	'��		&	��			��			��			��			 ��			��			��	$	$	��	'	%	��	)		�� 		1  g  nameg  xml-element-attributes� C�R�����      h   �   ] �$  
 ��"  �C  }       g  i
		  g  filenamef  sxml/sxml-match.ss�
	*	��		+	��			+	��		+	+��		+	.��		+	&��		+	�� 		   C        h0   �   ]"  
 6 �$   ��$  	 �6"���"����       g  s
		0  g  filenamef  sxml/sxml-match.ss�
	(	��	
	-	��		-	%��		-	��		)	��		)	��		)	��		)	 ��		)	��		)	��	&	,	��	(	*	�� 		0  g  nameg  xml-element-contents� C�R� h(   �   ]�$  �� &  �C �6C      �       g  key
		" g  l		"  g  filenamef  sxml/sxml-match.ss�
	/	��		0	��			0	��		2	��		2	��		2	��		3	��		4	%��	 	4	�� 
		"	  g  nameg  match-xml-attribute� C�R��       h8   �   ](  C4�� 5$  	 �6�4 �5�C       �       g  keys
		1 g  lst		1  g  filenamef  sxml/sxml-match.ss�
	6	��		7	��			8	��		9	��		9	��		9	��		9	��	 	:	$��	"	:	��	%	;	��	&	;	��	-	;	4��	/	;	��	0	;	�� 		1	  g  nameg  filter-attributes� C�R4"�$�')    h   >   ]  C      6       g  bvar
		
 g  bval		
  		
	   CFG   h@   �   ] ��4L  � ��� ����5 45$  @6  �       g  def
		> g  tmp		> g  tmp		'	>  g  filenamef  sxml/sxml-match.ss�
	v	&��		w	<��		x	<��		x	N��		y	N��		z	N��		x	<��		w	(�� 			>   C'��      h   J   ]  C    B       g  cata-binding
		 g  body-stx		  			   CFG       h@   �   ] (  C4L O  5 45$  @6�       g  	cata-defs
		@ g  body		@ g  tmp			@ g  tmp		)	@  g  filenamef  sxml/sxml-match.ss�
	r	��		s	��		v	!��		u	�� 		@	  g  nameg  process-cata-defs� C� h   Z   ] 6R       g  item
		  g  filenamef  sxml/sxml-match.ss�	 �	�� 		   C    h   ,   ]C    $       g  item
		  		   C'h      ] C           		
   C�� h   j   ]L 6b       g  fst
		 g  rst		  g  filenamef  sxml/sxml-match.ss�	 �	�� 			   CFG��+        h    �   ]45$  45�CC      �       g  fst
		 g  dots		 g  rst			  g  filenamef  sxml/sxml-match.ss�		E	��		E	��		E	*��		E	/��		E	%�� 			   C   h   N   ]C    F       g  fst
		 g  dots		 g  rst			  			   C       h�   �   ]!  4?$  @4 5$  @4 5"  %4 5$  L O @	 6$  4
?$  @"���"���     �       g  lst
	 � g  tmp	 � g  tmp		" � g  tmp		9 � g  tmp		H	d  g  filenamef  sxml/sxml-match.ss�
 �	�� 	 �  g  nameg  finite-lst?� C'�       h      ] C          		
   C� h   Z   ] 6R       g  item
		  g  filenamef  sxml/sxml-match.ss�	 �	�� 		   C    h   ,   ] C   $       g  item
		  		   C��')    h   D   ]  C    <       g  exp-lft
		 g  exp-rgt		  			   CFG     h8   �   ]4L  L54L5 45$  @6 �       g  fst
		7 g  rst		7 g  tmp			7 g  tmp		 	7  g  filenamef  sxml/sxml-match.ss�	 �	*��	 �	*��	 �	�� 		7	   CFG��+    h    �   ]45$  45�CC      �       g  fst
		 g  dots		 g  rst			  g  filenamef  sxml/sxml-match.ss�		E	��		E	��		E	*��		E	/��		E	%�� 			   C') h   D   ]  C    <       g  exp-lft
		 g  exp-rgt		  			   CFG     h@   �   ]4L 4L L554L5 45$  @6    �       g  fst
		< g  dots		< g  rst			< g  tmp			< g  tmp		%	<  g  filenamef  sxml/sxml-match.ss�	 �	*��	 �	+��	 �	*��	 �	*��	 �	�� 		<	   C   h�   �   ]!4 5$  @  4?$  @4 5"  )4 5$  LLLO @	 6$   4
?$  L LLLO @"���"���      �       g  lst
	 � g  tmp	
 � g  tmp		 � g  tmp		9 � g  tmp		H	h  g  filenamef  sxml/sxml-match.ss�
 �	�� 	 �  g  nameg  
expand-lst� C       hP     ]"  ;(  C4 �5$  "  
4 �5$  C�"���"���             g  var
		I g  lst		I g  lst			A g  t			2  g  filenamef  sxml/sxml-match.ss�
 �	��	 �	��	 �	��	 �	��	 �	6��	 �	��	 �	��	& �	��	- �	5��	/ �	��	6 �	��	; �	 ��	A �	��	A �	�� 		I	  g  nameg  member-var?� Ch   y   ]L L 6      q       g  var
		
  g  filenamef  sxml/sxml-match.ss�
 �	��	
 �	�� 		
  g  nameg  dotted-var?� C     h8   �   ] (  C4L  �5$   � "��� �4L �5�C   �       g  lst1
		5 g  lst2		5  g  filenamef  sxml/sxml-match.ss�
 �	��	 �	��	 �	��	 �	%��	 �	��	 �	��	 �	%��	& �	��	) �	��	* �	)��	/ �	6��	3 �	)��	4 �	�� 		5	  g  nameg  merge-pvars� C'-h   z   ]L4L 54L 56    r       g  a
		 g  rst		  g  filenamef  sxml/sxml-match.ss�	 �	"��	 �	"��	 �	�� 			   C2 h   z   ]L4L 54L 56    r       g  a
		 g  rst		  g  filenamef  sxml/sxml-match.ss�	 �	"��	 �	"��	 �	�� 			   C�  h   |   ]L4L  54L 56    t       g  fst
		 g  rst		  g  filenamef  sxml/sxml-match.ss�	 �	"��	 �	"��	 �	�� 			   C hh   �   ]4 5$  LLL O @4 5$  LLL O @4 5$  LL O @C      �       g  y
		b g  tmp		b g  tmp		+	b g  tmp		K	b  g  filenamef  sxml/sxml-match.ss�
 �	��	a �	�� 		b  g  nameg  walk-quasi-body� C'6    h   Y   ]C    Q       g  rst
		  g  filenamef  sxml/sxml-match.ss�	 �	!�� 		   C:  h   Y   ]L  6Q       g  rst
		  g  filenamef  sxml/sxml-match.ss�	 �	&�� 		   C�   h   |   ]L4L  54L 56    t       g  fst
		 g  rst		  g  filenamef  sxml/sxml-match.ss�	 �	 ��	 �	 ��	 �	�� 			   C�      h   l   ]4 5$  L  6C   d       g  item
		  g  filenamef  sxml/sxml-match.ss�	 �	��	 �	��	 �	�� 		   C  h   Z   ]  CR       g  item
		  g  filenamef  sxml/sxml-match.ss�	 �	�� 		   C   h�   �   ])O LLQ 4 5$  @4 5$  O @4 5$  LLO @  4	L O ?$  
@C   �       g  x
	 � g  walk-quasi-body	 � g  tmp		 � g  tmp		5 � g  tmp		Q � g  tmp		k �  g  filenamef  sxml/sxml-match.ss�
 �	�� � �	�� 	 �  g  nameg  select-dotted-vars� C�   h   W   ] 6O       g  x
		  g  filenamef  sxml/sxml-match.ss�	 �	�� 		   C       h   )   ] C   !       g  x
		  		   C'fNP h   *   ] L   ��C      "       g  dv
		  		   CFG       hH   �   ]4L  5  4?$  @45$   O @6�       g  item
		H g  dvars			H g  tmp			H g  tmp		,	H  g  filenamef  sxml/sxml-match.ss�
 �	��	 �	��		 �	�� 		H  g  nameg  expand-dotted-item� C'W  h   Y   ]LL 6Q       g  rst
		  g  filenamef  sxml/sxml-match.ss�	 �	&�� 		   CZd     h   s   ]	4L  L5 C   k       g  item
		 g  tmp		  g  filenamef  sxml/sxml-match.ss�	 �	0��	 �	�� 		   Cgo   h   s   ]	4L  L5 C   k       g  item
		 g  tmp		  g  filenamef  sxml/sxml-match.ss�	 �	0��	 �	�� 		   Cp��')       h   N   ] �CF       g  expanded-fst
		 g  expanded-rst		  			   CFG   h8   �   ]4L  54L 5 45$  @6   �       g  fst
		5 g  rst		5 g  tmp			5 g  tmp			5  g  filenamef  sxml/sxml-match.ss�	 �	/��	
 �	/��	 �	�� 		5	   C��+        h    �   ]45$  45�CC      �       g  item
		 g  dots		 g  rst			  g  filenamef  sxml/sxml-match.ss�		E	��		E	��		E	*��		E	/��		E	%�� 			   Cx')�     h   O   ]  �C   G       g  expanded-item
		 g  expanded-rst		  			   CFG  hH   �   ]4L 4L  L554L5 45$  @6       �       g  item
		A g  dots		A g  rst			A g  tmp			A g  tmp		*	A  g  filenamef  sxml/sxml-match.ss�	 �	0��	 �	1��	 �	0��	 �	/��	 �	�� 		A	   C��+   h    �   ]45$  45�CC      �       g  item
		 g  dots		 g  rst			  g  filenamef  sxml/sxml-match.ss�		E	��		E	��		E	*��		E	/��		E	%�� 			   C')�        h   O   ]  �C   G       g  expanded-item
		 g  expanded-rst		  			   CFG  h@   �   ]4L 4L L554L5 45$  @6    �       g  item
		< g  dots		< g  rst			< g  tmp			< g  tmp		%	<  g  filenamef  sxml/sxml-match.ss�	 �	0��	 �	1��	 �	0��	 �	/��	 �	�� 		<	   C  h�   �   ]14 5$   LO @4 5$  L LO @4 5$  L LO @4 5"  X4 	5"  4 
5$  LO @ C$   4?$  LLL LO @"���"���$   4?$  LLL LO @"���"��~   �       g  x
	 � g  tmp	 � g  tmp		) � g  tmp		G � g  tmp		e � g  tmp		t � g  tmp	 � �  g  filenamef  sxml/sxml-match.ss�
 �	�� 	 �  g  nameg  expand-quasiquote-body� C')  h   �   ]
4L 5  C     �       g  
quasiquote
		 g  term		 g  tmp				  g  filenamef  sxml/sxml-match.ss�	 �	B��		 �	%�� 			   CY�� h0   �   ]	4 5$  LO @L   6  �       g  x
		. g  tmp		.  g  filenamef  sxml/sxml-match.ss�
 �	��	# �	1��	.	B	�� 		.  g  nameg  process-quasiquote� C'�   h   +   ]L C   #       g  rst
		  		   C�h   Y   ]LL 6Q       g  rst
		  g  filenamef  sxml/sxml-match.ss�	 	$�� 		   C�')      h   D   ] �C<       g  exp-lft
		 g  exp-rgt		  			   CFG� h   D   ]  C    <       g  exp-lft
		 g  exp-rgt		  			   C h�   �   ]4LL5$  74L L54LL5 45$  @64L L54L 5 45$  @6       �       g  fst
		y g  rst		y g  tmp		"	D g  tmp		-	D g  tmp		W	y g  tmp		b	y  g  filenamef  sxml/sxml-match.ss�		!��		��		8��		8��	"	!��	E	8��	N	8��	W	!�� 			y	   C        h�   �  ]bO O O O O O O 		O 
Q LQ Q Q Q Q L	
Q 	L 	Q 
4
 5$   O @4
 5$   
O @4
 5$  L O @ C     �      g  action
	 � g  dotted-vars	 � g  finite-lst?		R � g  
expand-lst		R � g  member-var?		R � g  dotted-var?		R � g  merge-pvars		R � g  select-dotted-vars		R � g  expand-dotted-item		R � g  expand-quasiquote-body			R � g  process-quasiquote	
	R � g  tmp	 � � g  tmp	 � � g  tmp	 � �  g  filenamef  sxml/sxml-match.ss�
 �	�� 	 �	  g  nameg  process-output-action� C'��FG�   h   l   ] 6d       g  tag
		 g  items		  g  filenamef  sxml/sxml-match.ss�	,	�� 			   C�'��������� 	      hH   `   ]  L             C      X       g  x
		B g  bx		B g  body			B g  fail-to			B  		B	   CFG h�   Q  ]:4L  5�4LLLLLLLL>
  G L L 45$  4 O ?"  
45D    I      g  tag
		| g  items		| g  body-exp			| g  tests		+	| g  new-pvar-lst		+	| g  new-cata-defs		+	| g  new-dotted-vars		+	| g  tmp		A	t g  tmp		L	q  	g  filenamef  sxml/sxml-match.ss�	-	&��	
-	<��	-	&��	-	!��	-	��	/	!��	..	��	A9	��	|9	�� 
		|	   C� h   �   ] 6|       g  tag
		 g  
attr-items		 g  items			  g  filenamef  sxml/sxml-match.ss�		�� 			   C�'�  
   hP   p   ]  L         	       C  h       g  x
		N g  ax		N g  bx			N g  body			N g  fail-to			N  		N	   CFG h�   �  ]C4L  5�4L  5�4LLLLLLLL>  G L L 	4	5

$  4 O 
?"  
4	5
	D �      g  tag
	 � g  
attr-items	 � g  items		 � g  attr-exp		 � g  body-exp		 � g  tests		< � g  new-pvar-lst		< � g  new-cata-defs		< � g  new-dotted-vars		< � g  tmp			T � g  tmp	
	_ �  g  filenamef  sxml/sxml-match.ss�		&��	
	<��		&��		!��		&��		<��		&��		!��		��		!��	(	4��	;	!��	?	��	T	�� �	�� 	 �	   C 
  h�   L  , 	3 4 5	"  L4 5
"  	 6
$  *4
?$  L O 	
@"���"���	$  *4	?$  	LO 		@"���"���   D      g  ele
	 � g  exp	 � g  nextp		 � g  fail-k		 � g  pvar-lst		 � g  depth		 � g  cata-fun		 � g  	cata-defs		 � g  dotted-vars		 � g  tmp			 � g  tmp	
		a  g  filenamef  sxml/sxml-match.ss�
	�� 	 �		  g  nameg  compile-element-pat� C'7@DGIKMOP    h    G   ] L	LLLLLLLLL 6
       ?       g  filenamef  sxml/sxml-match.ss�	F	�� 		
   CFG�+QRST hH   �   ]4 5$  64545$  C45$  C45$  C6C   �       g  atag
		E g  literal		E g  rst			E g  x			C  g  filenamef  sxml/sxml-match.ss�	+	��	+	��		H	��		H	��		I	��	!	I	��	"	I	��	$	J	��	.	I	��	/	J	��	1	K	��	;	I	��	<	K	��	C	L	�� 		E	   C'Ukmnopru h@   Q   ]L     L       C I       g  ax
		? g  body		? g  fail-to			?  		?	   CFG        hx   4  ]34LL
L	L L�LLLLLLL >  G L	L 45$  4 O ?"  
45D ,      g  atag
		w g  literal		w g  rst			w g  tests		&	w g  new-pvar-lst		&	w g  new-cata-defs		&	w g  new-dotted-vars		&	w g  tmp		:	o g  tmp		E	l  	g  filenamef  sxml/sxml-match.ss�	-	��	1	2��	%-	��	),	��	:9	��	w9	�� 		w	   C�      h   �   ]4 5$  6C   �       g  atag
		 g  i		 g  rst			  g  filenamef  sxml/sxml-match.ss�	&	��	&	��	&	2�� 			   CY��     h    �   ] L  6     �       g  atag
		 g  i		 g  rst			 g  sub				  g  filenamef  sxml/sxml-match.ss�		'	��	'	*��		B	�� 			   C�      h   z   ] 6r       g  atag
		 g  i		 g  rst			  g  filenamef  sxml/sxml-match.ss�	!	�� 			   C�Y��       h    �   ] L  6     �       g  atag
		 g  i		 g  rst			 g  sub				  g  filenamef  sxml/sxml-match.ss�		"	��	"	*��		B	�� 			   C�      h   �   ]4 5$  6C   �       g  atag
		 g  var		 g  rst			  g  filenamef  sxml/sxml-match.ss�		��		��		2�� 			   C'U������      h@   Q   ]L     L �     C       I       g  ax
		9 g  body		9 g  fail-to			9  		9	   CFG        h�   9  ]34LLL
L	 L�LL4LL5LLLL >  G L
L 45$  4 O ?"  
45D  1      g  atag
		~ g  var		~ g  rst			~ g  tests		-	~ g  new-pvar-lst		-	~ g  new-cata-defs		-	~ g  new-dotted-vars		-	~ g  tmp		A	v g  tmp		L	s  	g  filenamef  sxml/sxml-match.ss�			��		2��		2��	,		��	0	��	A	��	~	�� 		~	   C� h   }   ] 6u       g  atag
		 g  cvar		 g  rst			  g  filenamef  sxml/sxml-match.ss�	�	�� 			   C�Y��'�������    h@   a   ]L      �     C       Y       g  ax
		9 g  ct		9 g  body			9 g  fail-to			9  		9	   CFG 
       h�   �  ];4 5�L$  "  4L >  "  G  4L LLL L�LL4LL5L	LL	L L
�L>  G LL 45		$  4 O 	?"  
4	5	D   �      g  atag
	 � g  cvar	 � g  rst		 � g  ctemp		 � g  tests		l � g  new-pvar-lst		l � g  new-cata-defs		l � g  new-dotted-vars		l � g  tmp	 � � g  tmp		 � �  
g  filenamef  sxml/sxml-match.ss�	�	#��	�	��	�	��	�	��		B	��	�	0��	-	B	��	6�	!��	E�	4��	J�	4��	a	e	��	d	e	��	k�	!��	o�	�� ��	�� ��	�� 	 �	   C� h   �   ] 6�       g  atag
		 g  cata		 g  cvar			 g  rst			  g  filenamef  sxml/sxml-match.ss�	�	�� 			   C�'�������  h@   a   ]L      �     C       Y       g  ax
		9 g  ct		9 g  body			9 g  fail-to			9  		9	   CFG        h�   �  ]<4 5�4LLL
L	 L�LL4LL5LLL L�L >  G L
L 	4	5

$  4 O 
?"  
4	5
	D �      g  atag
	 � g  cata	 � g  cvar		 � g  rst		 � g  ctemp		 � g  tests		F � g  new-pvar-lst		F � g  new-cata-defs		F � g  new-dotted-vars		F � g  tmp			\ � g  tmp	
	g �  g  filenamef  sxml/sxml-match.ss�	�	#��	�	��	�	��	�	!��	�	4��	$�	4��	;	e	��	>	e	��	E�	!��	I�	��	\�	�� ��	�� 	 �	   C�   h   �   ]4 5$  6C   �       g  atag
		 g  var		 g  default			 g  rst			  g  filenamef  sxml/sxml-match.ss�	�	��	�	��	�	2�� 			   C')�����  h8   <   ]L     L L     C4       g  ax
		8 g  body		8  		8	   CFG     h�   N  ]44LLL
L	 L�LL4LL5LLLL >  G L
 45		$  4 O 	?"  
45	D  F      g  atag
		~ g  var		~ g  default			~ g  rst			~ g  tests		-	~ g  new-pvar-lst		-	~ g  new-cata-defs		-	~ g  new-dotted-vars		-	~ g  tmp		?	v g  tmp			J	s  
g  filenamef  sxml/sxml-match.ss�	�	��	�	2��	�	2��	,�	��	0�	��	?�	��	~�	�� 		~	   C�    h   �   ] 6�       g  atag
		 g  cvar		 g  default			 g  rst			  g  filenamef  sxml/sxml-match.ss�	�	�� 			   C�Y��'U       h8   L   ]L     L      CD       g  ax
		8 g  ct		8 g  body			8  		8	   CFG 
    h�   �  ]<4 5�L$  "  4L >  "  G  4L LLL L�LL4LL5L	LL	L L
�L>  G L 	4	5

$  4 O 
?"  
4		5
	D   �      g  atag
	 � g  cvar	 � g  default		 � g  rst		 � g  ctemp		 � g  tests		l � g  new-pvar-lst		l � g  new-cata-defs		l � g  new-dotted-vars		l � g  tmp		 � � g  tmp	
 � �  g  filenamef  sxml/sxml-match.ss�	�	#��	�	��	�	��	�	��		B	��	�	0��	-	B	��	6�	!��	E�	4��	J�	4��	a	e	��	d	e	��	k�	!��	o�	�� ��	�� ��	�� 	 �	   C�   h   �   ] 6�       g  atag
		 g  cata		 g  cvar			 g  default			 g  rst			  g  filenamef  sxml/sxml-match.ss�	h	�� 			   C�'U789:;=     h8   L   ]L     L      CD       g  ax
		8 g  ct		8 g  body			8  		8	   CFG     h�   �  ]=4 5�4LLL
L	 L�LL4LL5LLL L�L >  G 	L
 
4
5$  4 O ?"  
4
5
	D �      g  atag
	 � g  cata	 � g  cvar		 � g  default		 � g  rst		 � g  ctemp		 � g  tests		F � g  new-pvar-lst		F � g  new-cata-defs		F � g  new-dotted-vars			F � g  tmp	
	Z � g  tmp		e �  g  filenamef  sxml/sxml-match.ss�	i	#��	i	��	i	��	k	!��	o	4��	$r	4��	;	e	��	>	e	��	Ek	!��	Ij	��	Z{	�� �{	�� 	 �	   C�     h   Y   ] 6Q       g  var
		  g  filenamef  sxml/sxml-match.ss�	S	�� 		   C'UPQR    h    W   ]L       CO       g  ax
		  g  matched-attrs		  g  body			   		 	   CFG  hx     ]14LLL
L	L4L L5LLLL>
  G LL  45$  4 O ?"  
45D          g  var
		t g  tests	%	t g  new-pvar-lst		%	t g  new-cata-defs		%	t g  new-dotted-vars		%	t g  tmp		9	l g  tmp		D	i  g  filenamef  sxml/sxml-match.ss�	U	��	Z	2��	$U	��	(T	��	9_	��	t_	�� 		t   C "h   C  , 3 4 5" �4 5" `4 5" 4 5" �4 5" }4 5" 04 5"  �4 	5"  �4 
5"  4 
5"  64 5$  
	LO 
@ 6$  04?$  !
	L O @"���"���$  4?$  LO @"��a"��]$  4?$  LO @"��."��*$  24?$  #
	LL O @"���"���$  44?$  %L L
L	O @"���"���$  24?$  #
	LL O @"��K"��G$  24?$  #
	LL O @"�� "���$  44?$  %L L
L	O @"���"���$  24?$  #
	LL O @"��h"��d$  24 ?$  #!
	LLO @"��"��    ;      g  attr-lst
	� g  body-lst	� g  attr-exp		� g  body-exp		� g  attr-key-lst		� g  nextp		� g  fail-k		� g  pvar-lst		� g  depth		� g  cata-fun			� g  	cata-defs	
	� g  dotted-vars		� g  tmp		� g  tmp		� g  tmp		-� g  tmp		<F g  tmp		K
 g  tmp		Z� g  tmp		i� g  tmp		xT g  tmp	 �0 g  tmp	 � g  tmp	 � �  g  filenamef  sxml/sxml-match.ss�
P	�� 	�	  g  nameg  compile-attr-list� C''Uln  h   P   ]    C  H       g  x
		 g  body		 g  fail-to			  			   CFG h`   �   ]04LLLL>  G  L L  45$  4?"  
45D     �       g  
next-tests
		[ g  new-pvar-lst		[ g  new-cata-defs			[ g  new-dotted-vars			[ g  tmp		%	S g  tmp		0	P  g  filenamef  sxml/sxml-match.ss�	G	��	F	��	%H	��	[H	�� 		[
   C���'U�    h   K   ]    C    C       g  ct
		 g  x		 g  body			  			   CFGY���   h�   �  ]:L$  y4 5�4L4LL5L  L�L>  G L 45$  4?"  
45D	L  
6       �      g  cata
	 � g  cvar	 � g  ctemp		 � g  
next-tests		7 � g  new-pvar-lst		7 � g  new-cata-defs		7 � g  new-dotted-vars		7 � g  tmp		K	y g  tmp		V	v  	g  filenamef  sxml/sxml-match.ss�	c	��		g	'��	g	"��	g	��	i	%��	i	,��	,	e	��	/	e	��	6i	%��	:h	��	Kp	!�� �p	�� �d	.�� �	B	�� 	 �	   C��Y��'U�     h   K   ]    C    C       g  ct
		 g  x		 g  body			  			   CFG 
     h�   |  ]94  5�L$  "  4L  >  "  G  4L 4LL5L L L�L>  G L 45$  4?"  
4	5D       t      g  cvar
	 � g  ctemp	 � g  
next-tests		W � g  new-pvar-lst		W � g  new-cata-defs		W � g  new-dotted-vars		W � g  tmp		k � g  tmp		v �  g  filenamef  sxml/sxml-match.ss�	x	#��	x	��	x	��	y	��		B	��	z	0��	-	B	��	6~	!��	9~	(��	L	e	��	O	e	��	V~	!��	Z}	��	k�	�� ��	�� 	 �   C��     h    �   ]LL LLLLL 6
      �       g  new-exp
		 g  new-pvar-lst		 g  new-cata-defs			 g  new-dotted-vars			  g  filenamef  sxml/sxml-match.ss�
�	��	�	!�� 			   C      h0   k   ]L
 L	LLLLLLO LLLLLL 6	       c       g  item
		) g  rst		)  g  filenamef  sxml/sxml-match.ss�	)�	�� 		)	   CFG��+       h    �   ]45$  45�CC      �       g  item
		 g  dots		 g  rst			  g  filenamef  sxml/sxml-match.ss�		E	��		E	��		E	*��		E	/��		E	%�� 			   CY��        h0   �   ]L
$  L LLLLLLLL	6
L  6�       g  item
		0 g  dots		0 g  rst			0  g  filenamef  sxml/sxml-match.ss�	�	��	 �	��	%�	.��	0	B	�� 		0	   C�   h   Y   ] 6Q       g  var
		  g  filenamef  sxml/sxml-match.ss�	U	�� 		   C')�   h   ;   ]L     C    3       g  x
		 g  body		  			   CFGY��� 
  h�   +  ]1L$  c4L4L L5LL>  G L 45$  4 O ?"  
45DL  	6     #      g  var
		{ g  
next-tests		k g  new-pvar-lst			k g  new-cata-defs			k g  new-dotted-vars			k g  tmp		0	c g  tmp		;	`  g  filenamef  sxml/sxml-match.ss�	V	��		[	#��	[	*��	[	#��	!Z	��	0\	��	k\	��	pW	.��	{	B	�� 
		{   C  hH  �  , 
3 4 5

$  	O 
@4 5"  �4 5$  LL 	O 	@4 5$  L 	LO 	@4 5"  94 	5$  
	LLO @ 6$  .4?$  LL	O @"���"���$  (4?$  LL 	O @"���"���    �      g  lst
	D g  exp	D g  nextp		D g  fail-k		D g  ellipsis-allowed?		D g  pvar-lst		D g  depth		D g  cata-fun		D g  	cata-defs		D g  dotted-vars			D g  tmp	
	D g  tmp		4D g  tmp		C g  tmp		o g  tmp	 � g  tmp	 � �  g  filenamef  sxml/sxml-match.ss�
Q	�� 	D	
  g  nameg  compile-item-list� C�'f��        h   +   ] ��C     #       g  npv
		  		   CFG      h8   �   ]4 5$  4?"  
4 5 D     �       g  new-pvar-lst
		3 g  new-cata-defs		3 g  new-dotted-vars			3 g  tmp			+  g  filenamef  sxml/sxml-match.ss�
�	/��	3�	1�� 		3	   C��'f
     h   +   ] ���C  #       g  npv
		  		   CFG      h8   �   ]45$  4?"  
45D     �       g  new-exp
		3 g  new-pvar-lst		3 g  new-cata-defs			3 g  new-dotted-vars			3 g  tmp			+  g  filenamef  sxml/sxml-match.ss�
�	*��	3�	,�� 		3	   C	��A      h   V   ]C   N       g  i
		  g  filenamef  sxml/sxml-match.ss�
�	0�� 		   CG   h   V   ]C   N       g  i
		  g  filenamef  sxml/sxml-match.ss�
�	0�� 		   CM   h   V   ]C   N       g  i
		  g  filenamef  sxml/sxml-match.ss�
�	0�� 		   C')]      h   :   ]  C    2       g  xa
		 g  xb		  			   CFG       h0   �   ]  45$  @6     �       g  a
		+ g  b		+ g  tmp				+ g  tmp			+  g  filenamef  sxml/sxml-match.ss�
�	0��		�	2�� 		+	   C'^z|}������~���������   h     , 3   	
���        � 	
	��    	4
5���    45� 	45� �    	4
5�        45      C           g  x
	 g  fail-to	 g  	tail-body		 g  	item-body		 g  
final-body		 g  ipv		 g  gpv		 g  tpv		 g  	item-void		 g  	tail-void			 g  	item-null	
	 g  	item-cons		  		   CFG h  �  , 
3 4L	>
  G 
4L  �	>	  G 4454544L5	5>  G 45
4	
54	54	54	5 45$  4?"  
45D     �      g  item
	 g  tail	 g  exp		 g  nextp		 g  fail-k		 g  pvar-lst		 g  depth		 g  cata-fun		 g  	cata-defs		 g  dotted-vars			 g  t-1316	
	 g  t-1317		 g  t-1318		 g  t-1319		 g  t-1320		D g  t-1321		D g  t-1322		D g  t-1323		D g  final-tests	 � g  final-pvar-lst	 � g  final-cata-defs	 � g  final-dotted-vars	 � g  temp-item-pvar-lst	 � g  tmp	 � � g  tmp	 � �  g  filenamef  sxml/sxml-match.ss�
�	��	�	��	�	/��	�	/��	�	/��	�	��	"�	��	+�	��	6�	*��	9�	*��	<�	*��	C�	��	G�	��	P�	��	S�	%��	^�	%��	i�	%��	n�	-��	z�	%��	�	�� ��	�� ��	,�� ��	�� ��	+�� ��	+�� ��	+�� ��	+�� ��	���	�� 		
  g  nameg  compile-dotted-pattern-list� C'���'������        h8   p   ]            C  h       g  x
		6 g  nx		6 g  ct			6 g  body			6 g  fail-to			6  		6	   CFG h�   �  ]B4L 5�4 5�4L4LL5L  L�L>  G LL  45		$  4	?"  
45	D  �      g  cata
	 � g  cvar	 � g  new-exp		 � g  ctemp		 � g  
next-tests		@ � g  new-pvar-lst		@ � g  new-cata-defs		@ � g  new-dotted-vars		@ � g  tmp		X � g  tmp			c �  
g  filenamef  sxml/sxml-match.ss�	/	%��	
/	;��	/	%��	/	 ��	0	#��	0	��	/	��	2	!��	"3	(��	5	e	��	8	e	��	?2	!��	C1	��	X:	�� �:	�� 	 �	   C��Y��'������ h8   p   ]            C  h       g  x
		6 g  nx		6 g  ct			6 g  body			6 g  fail-to			6  		6	   CFG 
h�   �  ]A4L 5�4  5�L	$  "  4L  >  "  G  4L 4LL5L L	 L�L>  G LL 45$  4?"  
4	5D    �      g  cvar
	 � g  new-exp	 � g  ctemp		 � g  
next-tests		f � g  new-pvar-lst		f � g  new-cata-defs		f � g  new-dotted-vars		f � g  tmp		~ � g  tmp	 � �  	g  filenamef  sxml/sxml-match.ss�	I	%��	
I	;��	I	%��	I	 ��	J	#��	J	��	I	��	"K	��	'	B	��	,L	0��	:	B	��	CP	!��	HQ	(��	[	e	��	^	e	��	eP	!��	iO	��	~X	�� �X	�� 	 �   C�fY��        h   _   ]L   6      W       g  i
		  g  filenamef  sxml/sxml-match.ss�	�	*��		B	�� 		   C�   h   W   ] 6O       g  i
		  g  filenamef  sxml/sxml-match.ss�	�	�� 		   CY��     h   _   ]L   6      W       g  i
		  g  filenamef  sxml/sxml-match.ss�	�	*��		B	�� 		   C+QRST       h8   �   ]	4 545$  C45$  C45$  C6�       g  literal
		8 g  x			8  g  filenamef  sxml/sxml-match.ss�		H	��			H	��		I	��		I	��		I	��		J	��	#	I	��	$	J	��	&	K	��	0	I	��	1	K	��	8	L	�� 		8   C�'��     h@   `   ]  L            C   X       g  x
		= g  nx		= g  body			= g  fail-to			=  		=	   CFG hx   E  ]94L 5�4LLLL>  G LL  45$  4 O ?"  
45D       =      g  literal
		q g  new-exp		q g  
next-tests		 	q g  new-pvar-lst		 	q g  new-cata-defs		 	q g  new-dotted-vars		 	q g  tmp		6	i g  tmp		A	f  g  filenamef  sxml/sxml-match.ss�	�	%��	
�	;��	�	%��	�	 ��	�	��	�	!��	#�	��	6�	��	q�	�� 
		q   CFG� h   k   ] 6c       g  tag
		 g  item		  g  filenamef  sxml/sxml-match.ss�	f	�� 			   C�'U)*      h    K   ]     C       C       g  x
		 g  nx		 g  body			  			   CFG      h`   L  ]34LL >  G L L 45$  4?"  
45D   D      g  more-pvar-lst
		] g  more-cata-defs		] g  more-dotted-vars			] g  
next-tests			] g  new-pvar-lst			] g  new-cata-defs			] g  new-dotted-vars			] g  tmp		'	U g  tmp		2	R  	g  filenamef  sxml/sxml-match.ss�
l	6��	p	F��	m	8��	't	B��	]t	:�� 		]	   C'U<=     h   P   ]    C  H       g  x
		 g  body		 g  fail-to			  			   CFG 	h�   d  ]:4L 5�4L �L LLO LLLLLL >	  G LL 45$  4?"  
45D   \      g  tag
	 � g  item	 � g  new-exp		 � g  after-tests		; � g  after-pvar-lst		; � g  after-cata-defs		; � g  after-dotted-vars		; � g  tmp		O	} g  tmp		Z	z  	g  filenamef  sxml/sxml-match.ss�	g	%��	
g	;��	g	%��	g	 ��	g	��	i	!��	>h	��	O�	�� ��	�� 
	 �	   C�      h   Y   ] 6Q       g  var
		  g  filenamef  sxml/sxml-match.ss�		�� 		   C�'�QRSTU   h8   `   ]     L        C  X       g  x
		6 g  nx		6 g  body			6 g  fail-to			6  		6	   CFG hx   S  ]94L 5�4L4L L5LL>  G LL  45$  4 O ?"  
45DK      g  var
		x g  new-exp		x g  
next-tests		'	x g  new-pvar-lst		'	x g  new-cata-defs		'	x g  new-dotted-vars		'	x g  tmp		=	p g  tmp		H	m  g  filenamef  sxml/sxml-match.ss�		%��	
	;��		%��		 ��		��	 	!��	 	0��	& 	!��	*	��	=!	��	x!	�� 		x   C  hP  �  , 	3 4 5	" 4 5

$  LO 
@4 5$  LLO 
@4 5"  i4 5$  	LO @  4
?$  LO @  4?$  O @ 6$  *4?$  L O 	@"��g"��c	$  &4	?$  LO 	@"���"���       �      g  item
	I g  exp	I g  nextp		I g  fail-k		I g  pvar-lst		I g  depth		I g  cata-fun		I g  	cata-defs		I g  dotted-vars		I g  tmp			I g  tmp	
	 g  tmp		H g  tmp		v g  tmp	 � � g  tmp	 � � g  tmp	 � �  g  filenamef  sxml/sxml-match.ss�
	�� 	I		  g  nameg  compile-item� CYV�    h8   �   ] (  C4 �L 5$  L L 6 � "���       �       g  lst
		1  g  filenamef  sxml/sxml-match.ss�
	Y	��		Z	��		\	��		\	,��		\	��		\	��		]	1��	(	B	��	+	`	$��	1	`	�� 		1  g  nameg  
check-pvar� C       h0   �   ]
O  L Q 4>  "  G   �C   �       g  pvar
		- g  pvar-lst		- g  
check-pvar			-  g  filenamef  sxml/sxml-match.ss�
	X	��		a	��	,	b	�� 		-	  g  nameg  add-pat-var� C')   h   :   ]  C      2       g  cf
		
 g  ct		
  		
	   CFG�Ukl  h   M   ]    C  E       g  ct
		 g  nct		 g  body			  			   C 	       hx   V  ] 
�$  ) 45$  @64 5�4L  �5 45$  @6   N      g  depth
		u g  cfun		u g  ctemp			u g  tmp			3 g  tmp			3 g  	new-ctemp		>	u g  tmp		S	u g  tmp		^	u  g  filenamef  sxml/sxml-match.ss�
	g	��		h	��	
	h	��		i	��	4	l	(��	;	l	>��	=	l	(��	>	l	#��	>	l	��	E	o	(��	J	o	:��	P	o	(��	S	m	�� 		u	  g  nameg  process-cata-exp� C'    h   P   ] LL �6       H       g  filenamef  sxml/sxml-match.ss�	 �	/��		 �	�� 			
   C�    h   s   ] 4L 5�C   k       g  fst
		 g  rst		  g  filenamef  sxml/sxml-match.ss�	 �	6��	 �	#�� 			   CFG      hH   �   ]4 5$  LLO @4 5$  L O @ 6    �       g  items
		D g  tmp	
	D g  tmp		(	D  g  filenamef  sxml/sxml-match.ss�
 �	�� 		D  g  nameg  iter� C h(   �   ]	 (  CO L  Q  ���6  �       g  lst
		& g  iter		&  g  filenamef  sxml/sxml-match.ss�
 �	��	 �	��		 �	��	 �	��	" �	*��	# �	$��	& �	�� 		&  g  nameg  cata-defs->pvar-lst� C'{����� h8   �   , 	3 	�   ��   C      �       g  compile-clause
		2 g  cata		2 g  cvar			2 g  gexp			2 g  action0			2 g  action			2 g  exp			2 g  cata-fun			2 g  fail-exp			2  			2		   C�'�   h      ] C           		
   C����Y��        hh     , 3 	45$  4?"  $  ,�   ��   C	
L  6       g  compile-clause
		g g  cvar		g g  gexp			g g  action0			g g  action			g g  exp			g g  cata-fun			g g  fail-exp			g g  tmp			'  	g  filenamef  sxml/sxml-match.ss�	+�	��	\�	*��	g	B	�� 		g	   C���   h    �   , 3    ���C �       g  compile-clause
		 g  cata		 g  cvar			 g  action0			 g  action			 g  exp			 g  cata-fun			 g  fail-exp			  			   C�'�     h      ] C           		
   C�Y�� hP   �   ]45$  4?"  $     ���CL  6�       g  compile-clause
		P g  cvar		P g  action0			P g  action			P g  exp			P g  cata-fun			P g  fail-exp			P g  tmp			#  g  filenamef  sxml/sxml-match.ss�	'�	��	E�	*��	P	B	�� 		P	   C���Y��     h   �   , 3 L  6  �       g  compile-clause
		 g  i		 g  gexp			 g  action0			 g  action			 g  exp			 g  cata-fun			 g  fail-exp			  g  filenamef  sxml/sxml-match.ss�	B	&��		B	�� 			   C�Y��      h   �   ]L  6      �       g  compile-clause
		 g  i		 g  action0			 g  action			 g  exp			 g  cata-fun			 g  fail-exp			  g  filenamef  sxml/sxml-match.ss�	F	&��		B	�� 			   C��FG+QRST   h8   E  ]4545$  C45$  C45$  C6=      g  compile-clause
		8 g  literal		8 g  action0			8 g  action			8 g  exp			8 g  cata-fun			8 g  fail-exp			8 g  x				8  g  filenamef  sxml/sxml-match.ss�		H	��			H	��		I	��		I	��		I	��		J	��	#	I	��	$	J	��	&	K	��	0	I	��	1	K	��	8	L	�� 		8	   C���      h    �   ] ��  C  �       g  compile-clause
		 g  literal		 g  action0			 g  action			 g  exp			 g  cata-fun			 g  fail-exp			  			   C+QRST     h@   W  , 3 	4545$  C45$  C45$  C6    O      g  compile-clause
		< g  literal		< g  gexp			< g  action0			< g  action			< g  exp			< g  cata-fun			< g  fail-exp			< g  x			<  	g  filenamef  sxml/sxml-match.ss�		H	��		H	��		I	��		I	��		I	��		J	��	'	I	��	(	J	��	*	K	��	4	I	��	5	K	��	<	L	�� 		<	   C���� h0   �   , 3  � ��  C    �       g  compile-clause
		, g  literal		, g  gexp			, g  action0			, g  action			, g  exp			, g  cata-fun			, g  fail-exp			,  		,	   C�       h   �   ]6�       g  compile-clause
		 g  pat		 g  action0			 g  action			 g  exp			 g  cata-fun			 g  fail-exp			  g  filenamef  sxml/sxml-match.ss�	O	�� 			   CY�� h   �   ]L  6      �       g  compile-clause
		 g  pat		 g  action0			 g  action			 g  exp			 g  cata-fun			 g  fail-exp			  g  filenamef  sxml/sxml-match.ss�	P	&��		B	�� 			   C�       h   �   , 3 6    �       g  compile-clause
		 g  pat		 g  gexp			 g  action0			 g  action			 g  exp			 g  cata-fun			 g  fail-exp			  g  filenamef  sxml/sxml-match.ss�	J	�� 			   CY��       h   �   , 3 L  6  �       g  compile-clause
		 g  pat		 g  gexp			 g  action0			 g  action			 g  exp			 g  cata-fun			 g  fail-exp			  g  filenamef  sxml/sxml-match.ss�	K	&��		B	�� 			   C�     h   �   , 3 6    �       g  compile-clause
		 g  fst		 g  rst			 g  action0			 g  action			 g  exp			 g  cata-fun			 g  fail-exp			  g  filenamef  sxml/sxml-match.ss�	*	�� 			   C    h    �   ]4L4L LL��55 D�       g  new-pvar-lst
		  g  new-cata-defs		  g  new-dotted-vars			   g  filenamef  sxml/sxml-match.ss�
.	0��	/	:��	0	M��	/	:��	 /	2�� 		 	   C
'� h      ] C           		
   C)      h   A   ] L    C    9       g  fail-to
		 g  body		  			   CFG 
       h�   �  , 3 4L�LL O 
45$  4?"  >	  G 
	 45$  O @	6     �      g  compile-clause
	 � g  fst	 � g  rst		 � g  action0		 � g  action		 � g  exp		 � g  cata-fun		 � g  fail-exp		 � g  tmp		*	B g  result		J � g  pvar-lst			J � g  	cata-defs	
	J � g  dotted-vars		J � g  tmp		\ � g  tmp		g �  g  filenamef  sxml/sxml-match.ss�	,	��	 8	0��	C;	0��	D<	0��	I,	��	M+	��	\=	�� 	 �	   C�    h   �   , 	3 	6    �       g  compile-clause
		 g  fst		 g  rst			 g  gexp			 g  action0			 g  action			 g  exp			 g  cata-fun			 g  fail-exp			  	g  filenamef  sxml/sxml-match.ss�		�� 				   C,
')34    h   A   ]L �   C    9       g  body
		 g  fail-to		  			   CFG        hX   �   ]4L 4LLL��55 45$  4LO ?"  
45 D�       g  new-pvar-lst
		X g  new-cata-defs		X g  new-dotted-vars			X g  tmp			P g  tmp		(	M  g  filenamef  sxml/sxml-match.ss�
	0��		>��		Q��		>��		3��	X	2�� 		X	   C
'�      h      ] C           		
   C)@      h   A   ] L    C    9       g  fail-to
		 g  body		  			   CFG 
       h�   �  , 	3 4L�LL O 
45		$  4	?"  	>	  G 
		 45$  O @	6   �      g  compile-clause
	 � g  fst	 � g  rst		 � g  gexp		 � g  action0		 � g  action		 � g  exp		 � g  cata-fun		 � g  fail-exp		 � g  tmp			,	D g  result			L � g  pvar-lst	
	L � g  	cata-defs		L � g  dotted-vars		L � g  tmp		^ � g  tmp		i �  g  filenamef  sxml/sxml-match.ss�		��	" 	0��	E#	0��	F$	0��	K	��	O	��	^%	�� 	 �		   C�A+    h      , 3 45$  45�CC        g  compile-clause
		 g  lst		 g  rst			 g  action0			 g  action			 g  exp			 g  cata-fun			 g  fail-exp			  g  filenamef  sxml/sxml-match.ss�	�	��	�	��	�	2��	�	8��	�	-�� 			   CS        h    �   ]4L4L LL��55 D�       g  new-pvar-lst
		  g  new-cata-defs		  g  new-dotted-vars			   g  filenamef  sxml/sxml-match.ss�
�	.��	�	8��	�	K��	�	8��	 �	0�� 		 	   C
'� h      ] C           		
   C)_`a        h(   A   ]L  L      C   9       g  body
		% g  fail-to		%  		%	   CFG 
       h�   �  , 3 4LLL O 
45$  4?"  >
  G 
	 45$  O @	6     �      g  compile-clause
	 � g  lst	 � g  rst		 � g  action0		 � g  action		 � g  exp		 � g  cata-fun		 � g  fail-exp		 � g  tmp		(	@ g  result		H � g  pvar-lst			H � g  	cata-defs	
	H � g  dotted-vars		H � g  tmp		Z � g  tmp		e �  g  filenamef  sxml/sxml-match.ss�	�	��		.��	A	.��	B	.��	G�	��	K�	��	Z	�� 	 �	   C�A+       h    "  , 	3 	45$  45�CC        g  compile-clause
		 g  lst		 g  rst			 g  gexp			 g  action0			 g  action			 g  exp			 g  cata-fun			 g  fail-exp			  	g  filenamef  sxml/sxml-match.ss�	�	��	�	��	�	2��	�	8��	�	-�� 				   Ct
')|}        h   E   ]L �   C    =       g  exp-body
		 g  fail-to		  			   CFG    hX   �   ]4L 4LLL��55 45$  4LO ?"  
45 D�       g  new-pvar-lst
		X g  new-cata-defs		X g  new-dotted-vars			X g  tmp			P g  tmp		(	M  g  filenamef  sxml/sxml-match.ss�
�	.��	�	@��	�	S��	�	@��	�	1��	X�	0�� 		X	   C
'�      h      ] C           		
   C)���        h(   A   ] L  L      C   9       g  fail-to
		% g  body		%  		%	   CFG 
       h�   �  , 	3 4LLL O 
45		$  4	?"  	>
  G 
		 45$  O @	6   �      g  compile-clause
	 � g  lst	 � g  rst		 � g  gexp		 � g  action0		 � g  action		 � g  exp		 � g  cata-fun		 � g  fail-exp		 � g  tmp			*	B g  result			J � g  pvar-lst	
	J � g  	cata-defs		J � g  dotted-vars		J � g  tmp		\ � g  tmp		g �  g  filenamef  sxml/sxml-match.ss�	�	��	 �	.��	C�	.��	D�	.��	I�	��	M�	��	\�	�� 	 �		   C� h   �   ]6�       g  compile-clause
		 g  var		 g  action0			 g  action			 g  exp			 g  cata-fun			 g  fail-exp			  g  filenamef  sxml/sxml-match.ss�	�	�� 			   C�      h   �   ]  ���C  �       g  compile-clause
		 g  var		 g  action0			 g  action			 g  exp			 g  cata-fun			 g  fail-exp			  			   C�     h   �   , 3 6    �       g  compile-clause
		 g  var		 g  gexp			 g  action0			 g  action			 g  exp			 g  cata-fun			 g  fail-exp			  g  filenamef  sxml/sxml-match.ss�	�	�� 			   C����   h0   �   , 3   ���   C   �       g  compile-clause
		- g  var		- g  gexp			- g  action0			- g  action			- g  exp			- g  cata-fun			- g  fail-exp			-  		-	   C 4     hP  �  ]�O O O O O O O O 	O 	
O 
	Q  Q Q  Q  Q 
Q  Q  Q 	Q 	
Q 
4 5" s4 5$  @4 5$   O @4 5" 4 5$  @4 5$   O @4 5" �4 5" q4 5" :4 5" 4 5$   O @4 5$   O @4 5"  �4 5"  e4 5"  74 5"  	 6$  4 ?$  !@"���"���$  4"?$  #@"���"���$  4$?$  % O @"��{"��w$  4&?$  ' O @"��H"��D$  4(?$  )O @"���"���$  4*?$  +O @"���"���$  4,?$  -O @"��k"��g$  4.?$  /O @"��4"��0$  40?$  1@"���"���$  42?$  3@"��r"��n    �      g  stx
	L g  process-cata-defs	_L g  process-output-action		_L g  compile-element-pat		_L g  compile-attr-list		_L g  compile-item-list		_L g  compile-dotted-pattern-list		_L g  compile-item		_L g  add-pat-var		_L g  process-cata-exp			_L g  cata-defs->pvar-lst	
	_L g  tmp	 �L g  tmp	 �- g  tmp	 �- g  tmp	 �- g  tmp	 g  tmp	 g  tmp	8 g  tmp	G� g  tmp	V� g  tmp	e� g  tmp	tn g  tmp	�n g  tmp	�n g  tmp	�J g  tmp	�& g  tmp	�  g  filenamef  sxml/sxml-match.ss�
	>	��	
	?	�� 	L   C5�R4"�$'���     h   h   ]����C       `       g  dummy
		 g  exp		 g  cata-fun			 g  clause			  			   C��������    h8   }   ]��� ��   C      u       g  dummy
		2 g  exp		2 g  cata-fun			2 g  clause0			2 g  clause			2  		2	   CFG    h@   �   ]4 5$  @4 5$  @ 6       �       g  x
		9 g  tmp		9 g  tmp		"	9  g  filenamef  sxml/sxml-match.ss�
_	�� 		9  g  
macro-typeg  syntax-rules�g  patternsg  expg  cata-fung  clause g  expg  cata-fung  clause0g  clauseg  ...   C5�R4"$'�������        h0   g   ]����     C    _       g  dummy
		, g  val		, g  clause0			, g  clause			,  		,	   CFG  h(   �   ]	4 5$  @ 6      �       g  x
		" g  tmp		"  g  filenamef  sxml/sxml-match.ss�
l	�� 		"  g  
macro-typeg  syntax-rules�g  patternsg  valg  clause0g  clauseg  ...   C5R4"�$'�	    h   {   ]���C   s       g  dummy
		 g  syntag		 g  synform			 g  body0			 g  body			  			   C !# h8   �   ]���      C  �       g  dummy
		6 g  syntag		6 g  synform			6 g  pat			6 g  exp			6 g  body0			6 g  body			6  		6	   C*;<>A?@ABC 
hP   �   , 	3 	45�����  	    C      �       g  dummy
		J g  syntag		J g  synform			J g  pat0			J g  exp0			J g  pat			J g  exp			J g  body0			J g  body			J  			J		   CFG 
       hP   �  ]4 5$  @4 5$  @4 5$  @	 6�      g  x
		P g  tmp		P g  tmp		"	P g  tmp		9	P  g  filenamef  sxml/sxml-match.ss�
s	�� 		P  g  
macro-typeg  syntax-rules�g  patternsg  syntagg  synformg  body0g  bodyg  ... g  syntagg  synformg  patg  exp  g  body0g  bodyg  ... g  syntagg  synformg  pat0g  exp0 g  patg  exp g  ... g  body0g  bodyg  ...   C5�R4"D$'H�'f\>A]   h0   1   ]4 L5L L4L 5LL����� C   )       g  	temp-name
		-  		-   CFG        h@     ]4545$  O @6      �       g  sxml-match-let-help
		: g  syntag		: g  synform			: g  pat			: g  exp			: g  body0			: g  body			: g  tmp				: g  tmp			:  	g  filenamef  sxml/sxml-match.ss�	�	(��		�		�� 		:	   CFG     h(   h   ]	4 5$  @ 6      `       g  stx
		" g  tmp		"  g  filenamef  sxml/sxml-match.ss�
�	�� 		"   C5DR4"$'Fn>A        h    }   ] L 45�����C   u       g  sxml-match-let
		 g  pat		 g  exp			 g  body0			 g  body			  			   CFG    h(   h   ]	4 5$   O @ 6 `       g  stx
		' g  tmp		'  g  filenamef  sxml/sxml-match.ss�
�	�� 		'   C5R4"$'�y       h   \   ]���C   T       g  sxml-match-let*
		 g  body0		 g  body			  			   C(�>A    h0   �   ] L    45��� C      �       g  sxml-match-let*
		* g  pat0		* g  exp0			* g  pat			* g  exp			* g  body0			* g  body			*  		*	   CFG       h@   y   ]4 5$  @4 5$   O @ 6  q       g  stx
		> g  tmp		> g  tmp		"	>  g  filenamef  sxml/sxml-match.ss�
�	�� 		>   C5RC�       g  m
		,  g  filenamef  sxml/match.scm�		
��e	1
��g  filenamef  sxml/sxml-match.ss�		��$		��
�		���	(	���	/	���	6	�� 
	      ��
   C6 