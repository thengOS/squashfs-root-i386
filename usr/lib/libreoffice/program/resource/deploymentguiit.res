  �     �   �Stai per installare la versione $NEW dell'estensione '$NAME'.
La versione più recente $DEPLOYED è già installata.
Fai clic su 'OK' per sostituire l'estensione installata.
Fai clic su 'Annulla' per interrompere l'installazione.   �      Stai per installare la versione $NEW dell'estensione '$NAME'.
La versione più recente $DEPLOYED, denominata '$OLDNAME', è già installata.
Fai clic su 'OK' per sostituire l'estensione installata.
Fai clic su 'Annulla' per interrompere l'installazione.   �     �   �Stai per installare la versione $NEW dell'estensione '$NAME'.
Tale versione è già installata.
Fai clic su 'OK' per sostituire l'estensione installata.
Fai clic su 'Annulla' per interrompere l'installazione.    �     �   �Stai per installare la versione $NEW dell'estensione '$NAME'.
Tale versione, denominata '$OLDNAME', è già installata.
Fai clic su 'OK' per sostituire l'estensione installata.
Fai clic su 'Annulla' per interrompere l'installazione.    �     �   �Stai per installare la versione $NEW dell'estensione '$NAME'.
La vecchia versione $DEPLOYED è già installata.
Fai clic su 'OK' per sostituire l'estensione installata.
Fai clic su 'Annulla' per interrompere l'installazione.    �    
  
Stai per installare la versione $NEW dell'estensione '$NAME'.
La vecchia versione $DEPLOYED, denominata '$OLDNAME', è già installata.
Fai clic su 'OK' per sostituire l'estensione installata.
Fai clic su 'Annulla' per interrompere l'installazione.    �     �   �Stai per installare l'estensione '%NAME'.
Fai clic su 'OK' per procedere.
Fai clic su 'Annulla' per interrompere.   �     :   :Non sono disponibili nuovi aggiornamenti.   �     �   �Non sono disponibili aggiornamenti da installare. Per visualizzare gli aggiornamenti ignorati o disabilitati, seleziona la casella di controllo 'Mostra tutti gli aggiornamenti'.   �     ,   ,Si è verificato un errore:   �     $   $Errore sconosciuto.   �     N   NNon sono disponibili altri dettagli per questo aggiornamento.   �     :   :L'estensione non può essere installata:    �     H   HLa versione di %PRODUCTNAME richiesta non corrisponde:    �     *   *Hai %PRODUCTNAME %VERSION   �     .   .aggiornamento tramite browser   �        Versione    �     ,   ,Ignora questo aggiornamento   �     &   &Abilita aggiornamenti   �     0   0Ignora tutti gli aggiornamenti    �     6   6Questo aggiornamento sarà ignorato.
   �     <   <Installazione delle estensioni in corso...    �     *   *Installazione completata    �          Nessun errore.    �     *   *Il messaggio di errore:     �     H   HErrore durante lo scaricamento dell'estensione %NAME.     �     H   HErrore durante l'installazione dell'estensione %NAME.     �     T   TIl contratto di licenza per l'estensione %NAME è stato rifiutato.    �     4   4L'estensione non verrà installata.        &   &Aggiungi estensione/i            ~Rimuovi    !        ~Abilita    "        ~Disabilita   #        ~Aggiorna...    $        ~Opzioni...   %     ,   ,Aggiunta di %EXTENSION_NAME   &     .   .Rimozione di %EXTENSION_NAME    '     0   0Attivazione di %EXTENSION_NAME    (     2   2Disattivazione di %EXTENSION_NAME   )     8   8Accetto la licenza per %EXTENSION_NAME    ,     F   FErrore: lo stato di questa estensione è sconosciuto    -        Chiudi    .        Esci    /    �  �%PRODUCTNAME è stato aggiornato a una nuova versione. Alcune estensioni condivise di %PRODUCTNAME non sono compatibili con questa versione e devono essere aggiornate prima che %PRODUCTNAME possa essere avviato.

L'aggiornamento di estensioni condivise richiede i privilegi di amministratore. Contatta l'amministratore di sistema per aggiornare le seguenti estensioni condivise:    0     l   lImpossibile abilitare l'estensione. Le seguenti dipendenze di sistema non sono soddisfatte:   1     `   `Questa estensione è disabilitata perché non hai ancora accettato la licenza.
   2          Mostra licenza    5     N   NL'estensione '%Name' non è supportata nel computer in uso.     7     �   �Stai per rimuovere l'estensione '%NAME'.
Fai clic su 'OK' per rimuovere l'estensione.
Fai clic su 'Annulla' per interrompere la rimozione.    8      Durante il cambiamento di estensioni condivise in un ambiente multi utente, accertati che altri utenti non stiano lavorando con la medesima copia di %PRODUCTNAME.
Fai clic su 'OK' per rimuovere l'estensione.
Fai clic su 'Annulla' per interrompere la rimozione.    9      Durante il cambiamento di estensioni condivise in un ambiente multi utente, accertati che altri utenti non stiano lavorando con la medesima copia di %PRODUCTNAME.
Fai clic su 'OK' per attivare l'estensione.
Fai clic su 'Annulla' per interrompere l'attivazione.    :      Durante il cambiamento di estensioni condivise in un ambiente multi utente, accertati che altri utenti non stiano lavorando con la medesima copia di %PRODUCTNAME.
Fai clic su 'OK' per disattivare l'estensione.
Fai clic su 'Annulla' per interrompere la disattivazione.   �  #   H   H            4   4       desktop/res/caution_12.png      #   H   H            4   4      desktop/res/caution_16.png    
  #   D   D            0   0      desktop/res/lock_16.png     #   J   J            6   6      desktop/res/extension_32.png      #   F   F            2   2      desktop/res/shared_16.png   �  #   V   V            (   (      res/sx03256.png             ��  ��      �  #   V   V            (   (      res/im30820.png             ��  ��      �  #   ^   ^            0   0      res/dialogfolder_16.png             ��  ��      �  #   V   V            (   (      res/xml_16.png              ��  ��      |  #   \   \            .   .      	res/component_16.png              ��  ��      ~  #   `   `            2   2      
res/javacomponent_16.png              ��  ��      �  #   Z   Z            ,   ,      res/library_16.png              ��  ��      �  #   ^   ^            0   0      res/javalibrary_16.png              ��  ��      X  #   ^   ^            0   0      res/sc_helperdialog.png             ��  ��       �           C     �  �    �        �   �    �      �  �    �  �    �  �    �  �    �  ^    �  �    �  Z    �  �    �  �    �  �    �  2    �  z    �  �    �  �    �  �    �  	    �  	>    �  	n    �  	�    �  	�    �  

    �  
*    �  
T    �  
�    �  
�    �  8      l       �    !  �    "  �    #  �    $       %      &  H    '  v    (  �    )  �    ,      -  V    .  n    /  �    0      1  |    2  �    5  �    7  J    8  �    9  �    :    #  �  .  #    v  #  
  �  #      #    L  #  �  �  #  �  �  #  �  >  #  �  �  #  |  �  #  ~  N  #  �  �  #  �    #  X  f  (