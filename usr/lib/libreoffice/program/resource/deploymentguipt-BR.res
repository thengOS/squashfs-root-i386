  �     �   �Você está prestes a instalar a versão $NEW da extensão '$NAME'.
A versão mais nova $DEPLOYED já está instalada.
Clique em 'OK' para substituir a extensão instalada.
Clique em 'Cancelar' para anular a instalação.   �      Você está prestes a instalar a versão $NEW da extensão '$NAME'.
A versão mais nova $DEPLOYED, com nome '$OLDNAME', já está instalada.
Clique em 'OK' para substituir a extensão instalada.
Clique em 'Cancelar' para anular a instalação.   �     �   �Você está prestes a instalar a versão $NEW da extensão '$NAME'.
Essa versão já está instalada.
Clique em 'OK' para substituir a extensão instalada.
Clique em 'Cancelar' para anular a instalação.    �     �   �Você está prestes a instalar a versão $NEW da extensão '$NAME'.
Essa versão, com nome '$OLDNAME', já está instalada.
Clique em 'OK' para substituir a extensão instalada.
Clique em 'Cancelar' para anular a instalação.    �     �   �Você está prestes a instalar a versão $NEW da extensão '$NAME'.
A versão mais antiga $DEPLOYED já está instalada.
Clique em 'OK' para substituir a extensão instalada.
Clique em 'Cancelar' para anular a instalação.   �      Você está prestes a instalar a versão $NEW da extensão '$NAME'.
A versão antiga $DEPLOYED, com nome '$OLDNAME', já está instalada.
Clique em 'OK' para substituir a extensão instalada.
Clique em 'Cancelar' para anular a instalação.    �     �   �Você está prestes a instalar a extensão '%NAME'.
Clique em 'OK' para prosseguir com a instalação.
Clique em 'Cancelar' para interromper a instalação.    �     >   >Nenhuma nova atualização está disponível.   �     �   �Não estão disponíveis novas atualizações. Para ver as atualizações ignoradas ou inativas, marque a caixa "Mostrar todas as atualizações".    �     "   "Ocorreu um erro:    �     $   $Erro desconhecido.    �     N   NNão há mais detalhes disponíveis para esta atualização.    �     @   @A extensão não pode ser atualizada devido a:    �     H   HA versão necessária do %PRODUCTNAME não é adequada:   �     2   2Você tem o %PRODUCTNAME %VERSION   �     .   .atualização pelo navegador    �        Versão   �     ,   ,Ignorar essa atualização    �     &   &Ativar atualizações   �     0   0Ignorar todas as atualizações   �     4   4Essa atualização será ignorada.
   �     *   *Instalando extensões...    �     (   (Instalação concluída   �        Nenhum erro.    �     (   (A mensagem de erro é:    �     B   BErro ao executar o download da extensão %NAME.     �     6   6Erro ao instalar a extensão %NAME.     �     J   JO contrato de licença da extensão %NAME foi recusado.     �     2   2A extensão não será instalada.        *   *Adicionar extensão(ões)            ~Remover    !        ~Ativar   "        ~Desativar    #        At~ualizar...   $        ~Opções...    %     ,   ,Adicionando %EXTENSION_NAME   &     *   *Removendo %EXTENSION_NAME   '     *   *Ativando %EXTENSION_NAME    (     ,   ,Desativando %EXTENSION_NAME   )     8   8Aceitar a licença para %EXTENSION_NAME   ,     @   @Erro: o status desta extensão é desconhecido    -        Fechar    .        Sair    /    �  �O %PRODUCTNAME foi atualizado para uma nova versão. Algumas extensões compartilhadas do %PRODUCTNAME não são compatíveis com esta versão e precisam ser atualizadas antes de iniciar o %PRODUCTNAME.

Para atualizar extensões compartilhadas é necessário possuir privilégios de administrador. Entre em contato com seu administrador de sistemas para atualizar as seguintes extensões:   0     x   xA extensão não pode ser instalada porque as seguintes dependências de sistema não são satisfeitas:   1     ^   ^Essa extensão está desativada porque você ainda não aceitou a licença.
    2     $   $Mostrar a licença    5     D   DA extensão '%Name' não funciona neste computador.   7     �   �Você está prestes a remover a extensão '%NAME'.
Clique em 'OK' para remover a extensão.
Clique em 'Cancelar' para interromper a remoção da extensão.   8        Tenha certeza que os demais usuários não estejam trabalhando no mesmo %PRODUCTNAME, ao alterar extensões compartilhadas em um ambiente multiusuário.
Clique em 'OK' para remover a extensão.
Clique em 'Cancelar' para manter a extensão.   9    
  
Tenha certeza que os demais usuários não estejam trabalhando no mesmo %PRODUCTNAME ao alterar extensões compartilhadas em um ambiente multiusuário.
Clique em 'OK' para ativar a extensão.
Clique em 'Cancelar' para manter a extensão desativada.    :    
  
Tenha certeza que os demais usuários não estejam trabalhando no mesmo %PRODUCTNAME ao alterar extensões compartilhadas em um ambiente multiusuário.
Clique em 'OK' para desativar a extensão.
Clique em 'Cancelar' para manter a extensão ativada.    �  #   H   H            4   4       desktop/res/caution_12.png      #   H   H            4   4      desktop/res/caution_16.png    
  #   D   D            0   0      desktop/res/lock_16.png     #   J   J            6   6      desktop/res/extension_32.png      #   F   F            2   2      desktop/res/shared_16.png   �  #   V   V            (   (      res/sx03256.png             ��  ��      �  #   V   V            (   (      res/im30820.png             ��  ��      �  #   ^   ^            0   0      res/dialogfolder_16.png             ��  ��      �  #   V   V            (   (      res/xml_16.png              ��  ��      |  #   \   \            .   .      	res/component_16.png              ��  ��      ~  #   `   `            2   2      
res/javacomponent_16.png              ��  ��      �  #   Z   Z            ,   ,      res/library_16.png              ��  ��      �  #   ^   ^            0   0      res/javalibrary_16.png              ��  ��      X  #   ^   ^            0   0      res/sc_helperdialog.png             ��  ��       �           C     �  L    �        �   �    �  �    �  �    �  �    �  �    �  �    �  d    �  �    �  F    �  h    �  �    �  �    �      �  b    �  �    �  �    �  �    �  	    �  	,    �  	\    �  	�    �  	�    �  	�    �  
     �  
(    �  
j    �  
�    �  
�             F    !  `    "  x    #  �    $  �    %  �    &  �    '  &    (  P    )  |    ,  �    -  �    .      /  "    0  �    1  0    2  �    5  �    7  �    8  �    9  �    :  �  #  �  �  #    �  #  
  F  #    �  #    �  #  �    #  �  p  #  �  �  #  �  $  #  |  z  #  ~  �  #  �  6  #  �  �  #  X  �  (