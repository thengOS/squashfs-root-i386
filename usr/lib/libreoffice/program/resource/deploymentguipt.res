  �     �   �Está prestes a instalar a versão $NEW da extensão '$NAME'.
Já está instalada a versão $DEPLOYED.
Clique em 'Aceitar' para substituir a extensão instalada.
Clique em 'Cancelar' para cancelar a instalação.    �     �   �Está prestes a instalar a versão $NEW da extensão '$NAME'.
Já está instalada a versão $DEPLOYED, denominada '$OLDNAME'.
Clique em 'Aceitar' para substituir a extensão instalada.
Clique em 'Cancelar' para cancelar a instalação.   �     �   �Está prestes a instalar a versão $NEW da extensão '$NAME'.
Esta versão já está instalada.
Clique em 'Aceitar' para substituir a extensão instalada.
Clique em 'Cancelar' para cancelar a instalação.   �     �   �Está prestes a instalar a versão $NEW da extensão '$NAME'.
Já está instalada esta versão, denominada '$OLDNAME'.
Clique em 'Aceitar' para substituir a extensão instalada.
Clique em 'Cancelar' para cancelar a instalação.    �     �   �Está prestes a instalar a versão $NEW da extensão '$NAME'.
Já está instalada a versão $DEPLOYED.
Clique em 'Aceitar' para substituir a extensão instalada.
Clique em 'Cancelar' para cancelar a instalação.    �     �   �Está prestes a instalar a versão $NEW da extensão '$NAME'.
Já está instalada a versão $DEPLOYED, denominada '$OLDNAME'.
Clique em 'Aceitar' para substituir a extensão instalada.
Clique em 'Cancelar' para cancelar a instalação.   �     �   �Está prestes a instalar a extensão '%NAME'.
Clique em 'Aceitar' para continuar a instalação.
Clique em 'Cancelar' para cancelar a instalação.   �     @   @Não estão disponíveis novas atualizações.    �     �   �Não estão disponíveis novas atualizações. Para ver as atualizações ignoradas ou desativadas, marque a caixa "Mostrar todas as atualizações".   �     "   "Ocorreu um erro:    �     $   $Erro desconhecido.    �     @   @Não existem detalhes para esta atualização.    �     @   @Não é possível atualizar a extensão porque:   �     >   >A versão do %PRODUCTNAME não coincide com:    �     2   2Você tem o %PRODUCTNAME %VERSION   �     4   4atualização baseada no navegador    �        Versão   �     ,   ,Ignorar esta atualização    �     &   &Ativar atualizações   �     0   0Ignorar todas as atualizações   �     4   4Esta atualização será ignorada.
   �     *   *A instalar extensões...    �     (   (Instalação concluída   �        Sem erros.    �     (   (A mensagem de erro é:    �     8   8Erro ao descarregar a extensão %NAME.    �     6   6Erro ao instalar a extensão %NAME.     �     N   NO contrato de licenciamento da extensão %NAME foi recusado.    �     2   2A extensão não será instalada.        &   &Adicionar extensões             ~Remover    !        A~tivar   "        ~Desativar    #        At~ualizar...   $        ~Opções...    %     *   *Adicionar %EXTENSION_NAME   &     (   (Remover %EXTENSION_NAME   '     (   (Ativar %EXTENSION_NAME    (     *   *Desativar %EXTENSION_NAME   )     4   4Aceitar licença de %EXTENSION_NAME   ,     @   @Erro: o estado desta extensão é desconhecido    -        Fechar    .        Sair    /    |  |O %PRODUCTNAME foi atualizado para a nova versão. Algumas extensões partilhadas do %PRODUCTNAME não são compatíveis com esta versão e devem ser atualizadas antes de iniciar o %PRODUCTNAME.

A atualização de extensões partilhadas requer privilégios de administrador. Contacte o administrador do sistema para atualizar as seguintes extensões partilhadas:   0     t   tNão é possível ativar a extensão, uma vez que as seguintes dependências não foram cumpridas:    1     X   XEsta extensão está desativada porque ainda não aceitou a licença.
    2     "   "Mostrar licença    5     D   DA extensão '%Name' não funciona neste computador.   7     �   �Está prestes a remover a extensão '%NAME'.
Clique em 'Aceitar' para remover a extensão.
Clique em 'Cancelar' para cancelar a remoção da extensão.   8      Certifique-se de que não existem mais utilizadores a trabalhar no mesmo %PRODUCTNAME, ao alterar as extensões partilhadas num ambiente multi-utilizador.
Clique em 'Aceitar' para remover a extensão.
Clique em 'Cancelar' para cancelar a remoção da extensão.   9      Certifique-se de que não existem mais utilizadores a trabalhar no mesmo %PRODUCTNAME, ao alterar as extensões partilhadas num ambiente multi-utilizador.
Clique em 'Aceitar' para ativar a extensão.
Clique em 'Cancelar' para cancelar a ativação da extensão.   :      Certifique-se de que não existem mais utilizadores a trabalhar no mesmo %PRODUCTNAME, ao alterar as extensões partilhadas num ambiente multi-utilizador.
Clique em 'Aceitar' para desativar a extensão.
Clique em 'Cancelar' para cancelar a desativação da extensão.   �  #   H   H            4   4       desktop/res/caution_12.png      #   H   H            4   4      desktop/res/caution_16.png    
  #   D   D            0   0      desktop/res/lock_16.png     #   J   J            6   6      desktop/res/extension_32.png      #   F   F            2   2      desktop/res/shared_16.png   �  #   V   V            (   (      res/sx03256.png             ��  ��      �  #   V   V            (   (      res/im30820.png             ��  ��      �  #   ^   ^            0   0      res/dialogfolder_16.png             ��  ��      �  #   V   V            (   (      res/xml_16.png              ��  ��      |  #   \   \            .   .      	res/component_16.png              ��  ��      ~  #   `   `            2   2      
res/javacomponent_16.png              ��  ��      �  #   Z   Z            ,   ,      res/library_16.png              ��  ��      �  #   ^   ^            0   0      res/javalibrary_16.png              ��  ��      X  #   ^   ^            0   0      res/sc_helperdialog.png             ��  ��       �           C     �      �        �   �    �  �    �  �    �  �    �  �    �  �    �  <    �  |    �  "    �  D    �  h    �  �    �  �    �  &    �  X    �  �    �  �    �  �    �  �    �  	&    �  	Z    �  	�    �  	�    �  	�    �  	�    �  
(    �  
^    �  
�      
�           !      "  6    #  R    $  p    %  �    &  �    '  �    (      )  2    ,  f    -  �    .  �    /  �    0  P    1  �    2      5  >    7  �    8  *    9  @    :  V  #  �  r  #    �  #  
    #    F  #    �  #  �  �  #  �  ,  #  �  �  #  �  �  #  |  6  #  ~  �  #  �  �  #  �  L  #  X  �  (