  �    #�     *�        Latin de base   *�        Latin-1   *�          Latin étendu A   *�          Latin étendu B   *�     4   4Alphabet phonétique international    *�     2   2Lettres modificatives avec chasse   *�        Diacritiques    *�        Grec    *�        Grec et copte   *�        Cyrillique    *�        Arménien   *�        Hébreu   *�          Hébreu étendu   *�        Arabe   *�        Arabe étendu   *�        Dévanagari   *�        Bengali   *�        Gurmukhi    *�        Gujarati    *�        Odia    *�        Tamoul    *�        Télugu   *�        Kannada   *�        Malayalam   *�        Thaï   *�        Laotien   *�        Géorgien   *�     "   "Géorgien étendu   *�        Hangul Jamo   *�     *   *Latin étendu additionnel   *�        Grec étendu    *�     (   (Ponctuation générale    *�     &   &Exposants et indices    *�     &   &Symboles monétaires    *�     4   4Signes combinatoires pour symboles    *�     (   (Symboles de type lettre   *�     "   "Formes numérales   *�        Flèches    *�     ,   ,Opérateurs mathématiques    *�     (   (Signes technique divers   *�     *   *Pictogrammes de commande    *�     6   6Reconnaissance optique de caractères   *�     *   *Alphanumériques cerclés   *�        Filets    *�        Pavés    *�     &   &Formes géométriques   *�          Symboles divers   *�        Casseau   *�     ,   ,Ponctuation et symboles CJC   *�        Hiragana    *�        Katakana    *�        Bopomofo    *�     (   (Hangul compatible Jamo    *�        CJC divers    *�     .   .Lettres et mois CJC cerclés    *�     $   $Compatibilité CJC    *�        Hangul    *�     *   *Idéogrammes unifiés CJC   +N     :   :Extension d'idéogrammes unifiés CJC - A   *�     &   &Zone à usage privé    *�     4   4Idéogrammes de compatibilité CJC    *�     8   8Formes de présentation alphabétiques    *�     4   4Formes de présentation arabes - A    *�     *   *Demi-signes combinatoires   *�     .   .Formes de compatibilité CJC    *�     ,   ,Petites variantes de formes   *�     4   4Formes de présentation arabes - B    *�     0   0Formes de demi et pleine chasse   *�     &   &Caractères spéciaux   *�          Syllabaires Yi    *�        Radicaux Yi   *�        Italic ancien   *�        Gothique    *�        Deseret   *�     ,   ,Symboles musicaux byzantins   *�     "   "Symboles musicaux   *�     :   :Symboles mathématiques alphanumériques    *�     :   :Extension d'idéogrammes unifiés CJC - B   *�     :   :Extension d'idéogrammes unifiés CJC - C   *�     :   :Extension d'idéogrammes unifiés CJC - D   *�     @   @Supplément d'idéogramme de compatibilité CJC   *�        Balises   *�     (   (Supplément cyrillique    *�     *   *Sélecteurs de variation    *�     @   @Zone d'utilisation privée supplémentaire - A    *�     @   @Zone d'utilisation privée supplémentaire - B    *�        Limbu   *�        Thaï Le    *�          Symboles khmer    *�     (   (Extensions phonétiques   *�     ,   ,Symboles et flèches divers   *�     .   .Symboles d'hexagramme Yijing    *�     (   (Syllabaire linéaire B    *�     *   *Idéogrammes linéaires B   *�     "   "Nombres égéens    *�        Ougaritique   *�        Shavian   *�        Osmanya   +Z        Sinhala   +[        Tibétain   +\        Myanmar   +]        Khmer   +^        Ogham   +f        Runique   +g        Syriaque    +_        Thaana    +h        Éthiopien    +i        Cherokee    +`     2   2Syllabaires autochtones canadiens   +j        Mongol    +k     4   4Symboles mathématiques divers - A    +l     ,   ,Supplément de flèches - A   +a          Motifs Braille    +m     ,   ,Supplément de flèches - B   +n     4   4Symboles mathématiques divers - B    +b     ,   ,Supplément de radicaux CJC   +o          Radicaux Kangxi   +p     :   :Caractères de description idéographique   +q        Tagalog   +r        Hanounóo   +c        Tagbanwa    +t        Bouhid    +s        Kanboun   +d     "   "Bopomofo étendu    +e     &   &Phonétiques Katakana   *�        Traits CJC    *�     &   &Syllabaire chypriote    *�     (   (Symboles Tai Xuan Jing    *�     8   8Supplément de sélecteurs de variation   *�     .   .Notation musicale grec ancien   *�     &   &Nombres grecs anciens   *�     "   "Supplément arabe   *�        Bugis   +      D   DSupplément de combinaisons de marques diacritiques   +        Copte   +     $   $Éthiopien étendu    +     (   (Supplément éthiopien    +     &   &Supplément géorgien   +        Glagolitique    +        Kharoshthi    +     4   4Lettres modificatrices de tonalité   +          Tai Lue nouveau   +	        Perse ancien    +
     6   6Supplément d'extensions phonétiques   +     ,   ,Ponctuation supplémentaire   +        Syloti Nagri    +        Tifinagh    +     "   "Formes verticales   +        Nko   +        Balinais    +     "   "Latin étendu - C   +     "   "Latin étendu - D   +        Phags-Pa    +        Phénicien    +        Cunéiforme   +     4   4Nombres et ponctuation cunéiformes   +     (   (Numérations à bâtons   +        Soudanais   +        Lepcha    +        Ol Chiki    +     (   (Cyrillique étendu - A    +        Vai   +     (   (Cyrillique étendu - B    +        Saurashtra    +        Kayah Li    +         Rejang    +!        Cham    +"     "   "Symboles anciens    +#     $   $Disque de Phaistos    +$        Lycien    +%        Carien    +&        Lydien    +'     "   "Tuiles de Mahjong   +(     "   "Tuiles de dominos   +)        Samaritain    +*     8   8Syllabaires canadien autochtone étendu   ++        Tai Tham    +,     $   $Extension védiques   +-        Lisu    +.        Bamum   +/     4   4Formes de nombres indiques communes   +0     $   $Devanagari étendu    +1     (   (Hangul Jamo étendu - A   +2        Javanais    +3     $   $Myanmar étendu - A   +4        Thaï Viet    +5        Meetei Mayek    +6     (   (Hangul Jamo étendu - B   +7     $   $Araméen impérial    +8     &   &Arable du Sud ancien    +9        Avestan   +:     *   *Parthe des inscriptions     +;     *   *Pehlevi des inscriptions    +<        Turc ancien   +=     *   *Symboles numériques rumi   +>        Kaithi    +?     *   *Hiéroglyphes égyptiens    +@     4   4Supplément alphanumérique inclus    +A     2   2Supplément idéographique inclus   +C        Mandéen    +D        Batak   +E     (   (Éthiopien étendu - A    +F        Brahmi    +G     "   "Supplément Bamum   +H     "   "Supplément Kana    +I          Cartes à jouer   +J     0   0Symboles et pictogrammes divers   +K        Frimousses    +L     2   2Symboles de transports et cartes    +M     &   &Symboles alchimiques    +O     "   "Arabe étendu - A   +P     >   >Symboles alphabétiques mathématiques arabes   +Q        Chakma    +R     (   (Extension Meetei Mayek    +S     &   &Cursive méroïtique    +T     ,   ,Hiéroglyphes méroïtiques   +U        Miao    +V        Sharada   +W        Sora Sompeng    +X     &   &Supplément soudanais   +Y        Takri   +u        Bassa Vah   +v     $   $Albanais caucasien    +w        Épacte copte   +x     :   :Marques diacritiques jonctives avancées    +y        Duployan    +z        Elbasan   +{     .   .formes géométriques avancé   +|        Grantha   +}        Khojki    +~        Khudawadi   +     "   "Latin étendu - E   +�        Linéaire A   +�        Mahajani    +�        Manichéen    +�        Mende Kikakui   +�        Modi    +�        Mro   +�     $   $Myanmar étendu - B   +�        Nabatéen   +�     &   &Ancien Arabe du nord    +�        Ancien Permic   +�     &   &Dingbats décoratifs    +�        Pahawh Hmong    +�        Palmyrénien    +�        Pau Cin Hau   +�     "   "Psautier Pahlavi    +�     4   4contrôles de format sténographie    +�        Siddham   +�     ,   ,Nombres cinghalais anciens    +�     ,   ,Supplément de flèches - C   +�        Tirhuta   +�        Warang Citi   +�        Âhom   +�     *   *Hiéroglyphes anatoliens    +�     &   &Supplément Cherokee    +�     :   :Extension d'idéogrammes unifiés CJC - E   +�     2   2Cunéiforme dynastique archaïque   +�        Hatran    +�        Multani   +�          Hongrois ancien   +�     6   6Symboles et pictogrammes additionnels   +�     $   $Sutton Signwriting    (1    �              Gauche             Intérieur             Droite             Extérieur     !        Centre             De gauche              De l'intérieur         $   $Zone de paragraphe          ,   ,Zone de texte de paragraphe    	     (   (Bordure de page gauche          ,   ,Bordure de page intérieure    
     (   (Bordure de page droite          ,   ,Bordure de page extérieure         .   .Bordure de paragraphe gauche          2   2Bordure de paragraphe intérieure         .   .Bordure de paragraphe droite          2   2Bordure de paragraphe extérieure         $   $Bord de la feuille          0   0Zone de délimitation du texte             Haut             Bas             Centre     "        Du haut    #        Du bas     $        En dessous     %        De droite    &     ,   ,Bordure de page supérieure    '     ,   ,Bordure de page inférieure    (     2   2Bordure de paragraphe supérieure    )     2   2Bordure de paragraphe inférieure         2   2Texte du paragraphe + espacements         $   $Texte du paragraphe         (   (Bordure de cadre gauche         .   .Bordure de cadre intérieure          (   (Bordure de cadre droite         .   .Bordure de cadre extérieure             Cadre entier          (   (Zone de texte de cadre             Ligne de base            Caractère             Ligne    *          Ligne de texte    (n    	V          �   �Aucun dictionnaire des synonymes n'est disponible pour la langue active.
Vérifiez la configuration installée et installez la langue souhaitée.          $(ARG1) n'est pas pris en charge par la vérification ou n'est actuellement pas actif.
Vérifiez l'installation et installez le cas échéant le module de langue requis,
ou activez-le sous 'Outils - Options - Paramètres linguistiques - Linguistique'.        H   HLa vérification de l'orthographe n'est pas disponible.        :   :La coupure des mots n'est pas disponible.        J   JImpossible de lire le dictionnaire personnalisé $(ARG1).        @   @Impossible d'installer le dictionnaire $(ARG1).        8   8Impossible de trouver l'image $(ARG1).         @   @Une image non liée n'a pas pu être chargée.    	     R   RAucune langue n'a été spécifiée pour le terme sélectionné.    
     �   �Impossible de charger la mise en page du formulaire, car les services IO requis (com.sun.star.io.*) n'ont pas pu être instanciés.   
     �   �Impossible d'écrire la mise en page du formulaire, car les services IO requis (com.sun.star.io.*) n'ont pas pu être instanciés.         ~   ~Erreur lors de la lecture des contrôles de formulaire. La mise en page du formulaire n'a pas été chargée.        �   �Erreur lors de l'écriture des contrôles de formulaire. La mise en page du formulaire n'a pas été enregistrée.         x   xUne erreur s'est produite pendant la lecture d'une puce. Certaines puces n'ont pas pu être chargées.         �   �Toutes les modifications apportées au code Basic sont perdues. Le code de macro VBA d'origine va être enregistré à la place.         ^   ^Le code VBA basic d'origine contenu dans le document ne sera pas enregistré.         H   HMot de passe incorrect. Impossible d'ouvir le document.        �   �La méthode de chiffrement de ce document n'est pas prise en charge. Seul le chiffrement de mot de passe compatible Microsoft Office 97/2000 est pris en charge.         v   vLes présentations Microsoft PowerPoint protégées par mot de passe ne peuvent pas être chargées.         �   �Les fichiers enregistrés au format Microsoft Office ne peuvent pas être protégés par mot de passe.
Voulez-vous enregistrer le document sans le protéger par mot de passe ?   (o    �           L   L$(ERR) lors de l'exécution du dictionnaire des synonymes.          R   R$(ERR) lors de l'exécution de la vérification de l'orthographe.         <   <$(ERR) lors de l'exécution de la césure.          @   @$(ERR) lors de la création d'un dictionnaire.          L   L$(ERR) lors du paramétrage de l'attribut d'arrière-plan.          :   :$(ERR) lors du chargement d'un graphique.   *d    |        y  �  �   	Paramètre de la bordure      Ligne de bordure gauche    Ligne de bordure droite    Ligne de bordure supérieure     Ligne de bordure inférieure     Ligne de bordure horizontale     Ligne de bordure verticale     Ligne de bordure diagonale de l'angle supérieur gauche à l'angle inférieur droit    Ligne de bordure diagonale de l'angle inférieur gauche à l'angle supérieur droit         y  �  �   	Paramètre de la bordure      Ligne de bordure gauche    Ligne de bordure droite    Ligne de bordure supérieure     Ligne de bordure inférieure     Ligne de bordure horizontale     Ligne de bordure verticale     Ligne de bordure diagonale de l'angle supérieur gauche à l'angle inférieur droit    Ligne de bordure diagonale de l'angle inférieur gauche à l'angle supérieur droit            ,   ,       svx/res/frmsel.bmp    FP     T              Table            Requête             SQL   FQ    L              LIKE             NOT            EMPTY            TRUE             FALSE            IS             BETWEEN            OR     	        AND    
        Moyenne            NB             Maximum            Minimum            Somme            Toutes les             Tous             Certains             STDDEV_POP             STDDEV_SAMP            VAR_SAMP             VAR_POP            Rassembler             Fusionner            Intersection          L   L$(WIDTH) x $(HEIGHT) ($(WIDTH_IN_PX) x $(HEIGHT_IN_PX) px)         4   4$(WIDTH) x $(HEIGHT) à $(DPI) DPI              $(CAPACITY) kio           Image Gif           Image Jpeg            Image PNG           Image TIFF            Image WMF           Image MET   	        Image PCT   
        Image SVG           Image BMP           Inconnu   
           objet de dessin   
     "   "objets de dessin    
          objet de groupe   
     "   "objets de groupe    
     &   &objet de groupe vide    
     (   (Objets de groupe vides    
        Tableau   
        Tableaux    
        Ligne   
	     "   "ligne horizontale   

          Ligne verticale   
          ligne diagonale   
        Lignes    
        Rectangle   
        Rectangles    
        Carré    
        Carrés   
     "   "Parallélogramme    
     "   "Parallélogrammes   
        Rhombe    
        Rhombes   
     "   "Rectangle arrondi   
     $   $Rectangles arrondis   
          carré arrondi    
     "   "Carrés arrondis    
     *   *Parallélogramme arrondi    
     ,   ,Parallélogrammes arrondis    
          rhombe arrondi    
     "   "Rhombes arrondis    
        Cercle    
        Cercles   
     "   "Secteur de cercle   
      $   $Secteurs de cercle    
!        Arc de cercle   
"          Arcs de cercles   
#     "   "Segment de cercle   
$     $   $Segments de cercle    
%        Ellipse   
&        Ellipses    
'     "   "Secteur d'ellipse   
(     $   $Secteurs d'ellipse    
)        Arc d'ellipse   
*          Arcs d'ellipse    
+     "   "Segment d'ellipse   
,     $   $Segments d'ellipse    
-        Polygone    
.     (   (Polygone avec %2 angles   
/        Polygones   
0        Polyligne   
1     *   *Polyligne avec %2 angles    
2        Polylignes    
3     "   "Courbe de Bézier   
4     $   $Courbes de Bézier    
5     "   "Courbe de Bézier   
6     $   $Courbes de Bézier    
7     &   &Ligne à main levée    
8     &   &Lignes à main levée   
9     &   &Ligne à main levée    
:     &   &Lignes à main levée   
;          Objet incurvé    
<          Objets courbes    
=          Objet incurvé    
>          Objets courbes    
?     "   "Spline naturelle    
@     $   $Splines naturelles    
A     $   $Spline périodique    
B     &   &Splines périodiques    
C          Cadre de texte    
D          Cadre de texte    
E     $   $Cadre de texte lié   
F     &   &Cadres de texte liés   
G     6   6Ajuster la taille de l'objet au texte   
H     6   6Ajuster la taille des objets au texte   
I     6   6Ajuster la taille de l'objet au texte   
J     6   6Ajuster la taille des objets au texte   
K          Texte de titre    
L          Textes de titre   
M        Texte de plan   
N          Textes de plan    
O        image   
P        images    
Q        Image liée   
R        Images liées   
S     &   &Objet graphique vide    
T     (   (Objets graphiques vides   
U     *   *Objet graphique lié vide   
V     .   .Objets graphiques liés vides   
W        Métafichier    
X        Métafichiers   
Y     "   "Métafichier lié   
Z     $   $Métafichiers liés   
[        Bitmap    
\        Bitmaps   
]        Bitmap lié   
^        Bitmaps liés   
_        Image Mac   
`        Images Mac    
a     "   "Images Mac liées   
b     "   "Images Mac liées   
c     (   (objet incorporé (OLE)    
d     *   *Objets incorporés (OLE)    
e     0   0objet lié et incorporé (OLE)    
f     2   2Objets liés et incorporés (OLE)   
g        Objet   
h        Cadre   
i        Cadres    
j        Cadre   
k     &   &Connecteurs d'objets    
l     &   &Connecteurs d'objets    
m        Légende    
n        Légendes   
o          Objet d'aperçu   
p     "   "Objets d'aperçus   
q     "   "Ligne de cotation   
r     $   $Objets de cotation    
s     "   "objets de dessin    
t     &   &Aucun objet de dessin   
u        et    
v     $   $objet(s) de dessin    
w        Contrôle   
x        Contrôles    
y        Cube 3D   
z        Cubes 3D    
{     "   "Objet d'extrusion   
|     $   $Objets d'extrusion    
}        Texte 3D    
~        Textes 3D   
     "   "objet de rotation   
�     $   $objets de rotation    
�        Objet 3D    
�        Objets 3D   
�        Polygones 3D    
�        Séquence 3D    
�        Séquences 3D   
�        sphère   
�        sphères    
�     *   *Bitmap avec transparence    
�     .   .Bitmap lié avec transparence   
�     *   *Bitmaps avec transparence   
�     0   0Bitmaps liés avec transparence   
�        Forme   
�        Formes    
�          Objet de média   
�     "   "Objets de média    
�        font work   
�        font works    
�        SVG   
�        SVG   
�        avec copie    
�     8   8Définir la position et la taille de %1   
�        Supprimer %1    
�     *   *Déplacer %1 vers l'avant   
�     .   .Déplacer %1 vers l'arrière    
�     (   (Déplacer %1 à l'avant   
�     ,   ,Déplacer %1 à l'arrière    
�     (   (Inverser l'ordre de %1    
�        Déplacer %1    
�     "   "Redimensionner %1   
�        Tourner de %1   
�     .   .Retourner %1 horizontalement    
�     ,   ,Retourner %1 verticalement    
�     ,   ,Retourner %1 diagonalement    
�     (   (Retourner %1 librement    
�     (   (Déformer %1 (incliner)   
�     &   &Disposer %1 en cercle   
�     &   &Courber %1 en cercle    
�        Déformer %1    
�        Annuler %1    
�     8   8Modifier les propriétés Bézier de %1   
�     8   8Modifier les propriétés Bézier de %1   
�        Fermer %1   
�     8   8Définir la direction de sortie pour %1   
�     4   4Définir l'attribut relatif pour %1   
�     :   :Définir un point de référence pour %1    
�        Grouper %1    
�        Dissocier %1    
�     .   .Appliquer les attributs à %1   
�     ,   ,Appliquer les styles à %1    
�     2   2Retirer la feuille de style de %1   
�     *   *Convertir %1 en polygone    
�     *   *Convertir %1 en polygones   
�     (   (Convertir %1 en courbe    
�     (   (Convertir %1 en courbes   
�        Aligner %1    
�     $   $Aligner %1 en haut    
�     "   "Aligner %1 en bas   
�     ,   ,Centrer %1 horizontalement    
�     &   &Aligner %1 à gauche    
�     &   &Aligner %1 à droite    
�     *   *Centrer %1 verticalement    
�        Centrer %1    
�          Transformer %1    
�        Combiner %1   
�        Combiner %1   
�        Scinder %1    
�        Scinder %1    
�        Scinder %1    
�     4   4StarDraw Dos Zeichnung importieren    
�     "   "HPGL importieren    
�          DXF importieren   
�     (   (Convertir %1 en contour   
�     *   *Convertir %1 en contours    
�        Fusionner %1    
�        Soustraire %1   
�          Intersecter %1    
�     4   4Répartir les objets sélectionnés   
�     *   *Égaliser les largeurs %1   
�     *   *Égaliser les hauteurs %1   
�     (   (Insérer le(s) objet(s)   
�        Couper %1   
�     4   4Coller le contenu du presse-papiers   
�     $   $Glisser-Déposer %1   
�     .   .Insérer le Glisser-Déposer    
�     *   *Insérer un point dans %1   
�     6   6Insérer un point de collage dans %1    
�     2   2Déplacer le point de référence   
�     .   .Modifier la géométrie de %1   
�        Déplacer %1    
�     "   "Redimensionner %1   
�        Tourner de %1   
�     .   .Retourner %1 horizontalement    
�     ,   ,Retourner %1 verticalement    
�     ,   ,Retourner %1 diagonalement    
�     (   (Retourner %1 librement    
�     (   (Déformer %1 (incliner)   
�     &   &Disposer %1 en cercle   
�     &   &Courber %1 en cercle    
�        Déformer %1    
�     (   (Modifier le rayon de %1   
�        Modifier %1   
�     "   "Redimensionner %1   
�        Déplacer %1    
�     4   4Déplacer le point de limite de %1    
�     &   &Ajuster l'angle de %1   
�        Modifier %1   
�     .   .Dégradé interactif pour %1    
�     2   2Transparence interactive pour %1    
�        Rogner %1   
�     F   FÉdition texte : Paragraphe %1, Ligne %2, Colonne %3    
�     (   (%1 sélectionné(e)(s)    
�        Point de %1   
�          %2 points de %1   
�     (   (Point de collage de %1    
�     ,   ,%2 points de collage de %1    
�     *   *Sélectionner des objets    
�     :   :Sélectionner des objets supplémentaires   
�     *   *Sélectionner des points    
�     :   :Sélectionner des points supplémentaires   
�     4   4Sélectionner les points de collage   
�     F   FSélectionner des points de collage supplémentaires    
�        Créer %1   
�        Insérer %1   
�        Copier %1   
�     2   2Modifier l'ordre de l'objet de %1   
�     (   (Éditer le texte de %1          "   "Insérer une page        "   "Supprimer la page             Copier la page         *   *Changer l'ordre des pages        2   2Assigner une page d'arrière-plan        D   DSupprimer les attributs de la page en arrière-plan        D   DDéplacer les attributs de la page en arrière-plan        D   DModifier les attributs de la page en arrière-plan         &   &Insérer un document    	     $   $Insérer une couche   
     $   $Supprimer la couche        ,   ,Changer l'ordre des couches        2   2Modifier le nom d'objet de %1 en         0   0Modifier le titre d'objet de %1        6   6Modifier la description d'objet de %1           Standard            activé           désactivé           oui           Non           Type 1            Type 2            Type 3            Type 4            Horizontal            Vertical            Automatique           Désactivé           Proportionnel        >   >Adapter à la taille (lignes individuelles)          .   .Utiliser des attributs fixes            Haut             Centre    !        Bas   "     .   .Utiliser la hauteur entière    #        Élargi   $        Gauche    %        Centre    &        Droite    '     .   .Utiliser la largeur entière    (        Élargi   )        désactivé   *        clignoter   +        Défiler    ,        alternant   -          Défiler dedans   .          vers la gauche    /          vers la droite    0        vers le haut    1        vers le bas   2     $   $Connecteur standard   3     &   &Connecteur linéaire    4     "   "Connecteur direct   5     $   $Connecteur incurvé   6        Standard    7        Rayon   8        automatique   9          extrême gauche   :     *   *à l'intérieur (centré)   ;          extrême droite   <        automatique   =        sur la ligne    >     "   "ligne interrompue   ?     (   (en dessous de la ligne    @        centré   A        cercle entier   B     "   "Secteur de cercle   C     "   "Segment de cercle   D        Arc de cercle   E        Ombre   F          Couleur d'ombre   G     0   0Décalage horizontal de l'ombre   H     .   .Décalage vertical de l'ombre   I     &   &Transparence d'ombre    J        Ombre 3D    K     &   &Ombre en perspective    R     "   "Type de légende    S        Angle donné    T        Angle   U        Espace    V     $   $Direction de sortie   W     ,   ,Position de sortie relative   X     $   $Position de sortie    Y     $   $Position de sortie    Z     "   "Longueur de ligne   [     .   .Longueur de ligne automatique   c        Rayon d'angle   d     *   *Espacement du bord gauche   e     *   *Espacement du bord droit    f     .   .Espacement du bord supérieur   g     0   0Espacement inférieur du cadre    h     :   :Adaptation autom. de la hauteur du cadre    i     *   *Hauteur minimale du cadre   j     *   *Hauteur maximale du cadre   k     :   :Adaptation autom. de la largeur du cadre    l     *   *Largeur minimale du cadre   m     *   *Largeur maximale du cadre   n     *   *Ancrage vertical du texte   o     ,   ,Ancrage horizontal du texte   p     *   *Adapter le texte au cadre   q     $   $Proportion de rouge   r     $   $Proportion de vert    s     $   $Proportion de bleu    t        Luminosité   u        Contraste   v        Gamma   w        Transparence    x        Invertir    y          Mode Graphique    z            {            |            }            ~                        �     "   "Attributs divers    �     $   $Position protégée   �     (   (Protection de la taille   �          Ne pas imprimer   �     &   &Indicateur de couche    �        ~Niveau   �          Nom de l'objet    �     "   "Angle de départ    �     "   "Angle d'arrivée    �        Position X    �        Position Y    �        Largeur   �        Hauteur   �     "   "Angle de rotation   �     &   &Angle de cisaillement   �     "   "Attribut inconnu    �          Style de ligne    �          Motif de ligne    �     $   $Épaisseur de trait   �     "   "Couleur de ligne    �          Début de ligne   �     &   &Extrémité de ligne    �     ,   ,Largeur du début de ligne    �     2   2Largeur de l'extrémité de ligne   �     (   (Début de ligne centré   �     (   (Extrémités centrées    �     &   &Transparence de ligne   �     $   $Jointure de lignes    �     (   (Réservé pour ligne 2    �     (   (Réservé pour ligne 3    �     (   (Réservé pour ligne 4    �     (   (Réservé pour ligne 5    �     (   (Réservé pour ligne 6    �     $   $Attributs de lignes   �     &   &Style de remplissage    �     (   (Couleur de remplissage    �        Dégradé   �        Hachure   �     &   &Bitmap de remplissage   �        Transparence    �     .   .Finesse du grain du dégradé   �     *   *Remplissage du carrelage    �     2   2Position du bitmap de remplissage   �     2   2Largeur du bitmap de remplissage    �     2   2Hauteur du bitmap de remplissage    �     &   &Dégradé transparent   �     .   .Réservé pour remplissage 2    �     4   4Taille de carrelage absente dans %    �     0   0Décalage de carrelage X dans %   �     0   0Décalage de carrelage Y dans %   �     $   $Extension de bitmap   �     (   (Réservé pour bitmap 3   �     (   (Réservé pour bitmap 4   �     (   (Réservé pour bitmap 5   �     (   (Réservé pour bitmap 6   �     (   (Réservé pour bitmap 7   �     (   (Réservé pour bitmap 8   �     0   0Position de carrelage X dans %    �     0   0Position de carrelage Y dans %    �     ,   ,Remplissage d'arrière-plan   �     .   .Réservé pour remplissage 10   �     .   .Réservé pour remplissage 11   �     .   .Réservé pour remplissage 12   �     *   *Attributs de remplissage    �          Style Fontwork    �     $   $Alignement Fontwork   �     $   $Espacement Fontwork   �          Début Fontwork   �          Reflet Fontwork   �     "   "Contour Fontwork    �          Ombre Fontwork    �     *   *Couleur d'ombre Fontwork    �     .   .Décalage d'ombre Fontwork X    �     .   .Décalage d'ombre Fontwork Y    �     ,   ,Masquer le contour Fontwork   �     .   .Transparence d'ombre Fontwork   �     *   *Réservé pour Fontwork 2   �     *   *Réservé pour Fontwork 3   �     *   *Réservé pour Fontwork 4   �     *   *Réservé pour Fontwork 5   �     *   *Réservé pour Fontwork 6   �        Ombre   �          Couleur d'ombre   �     &   &Espacement d'ombre X    �     &   &Espacement d'ombre Y    �     &   &Transparence d'ombre    �        Ombre 3D    �     &   &Ombre en perspective    �     "   "Type de légende    �     ,   ,Angle de la légende fixé    �     &   &Angle de la légende    �     2   2Espacement des lignes de légende   �     4   4Direction de la sortie de légende    �     0   0Sortie relative de la légende    �     0   0Sortie relative de la légende    �     .   .Sortie absolue de la légende   �     4   4Longueur des lignes de la légende    �     P   PAdaptation automatique de la longueur des lignes de la légende   �        Rayon d'angle   �     *   *Hauteur de cadre minimale   �     ,   ,Adapter la hauteur au texte   �     *   *Adapter le texte au cadre   �     2   2Espacement gauche du cadre texte    �     0   0Espacement droit du cadre texte   �     6   6Espacement supérieur du cadre texte    �     6   6Espacement inférieur du cadre texte    �     *   *Ancrage vertical du texte   �     *   *Hauteur de cadre maximale   �     *   *Largeur de cadre minimale   �     *   *Largeur de cadre maximale   �     ,   ,Adapter la largeur au texte   �     ,   ,Ancrage horizontal du texte         &   &Défilement de texte         .   .Sens de défilement du texte         *   *Texte visible au départ         *   *Texte visible à l'arrêt        &   &Cycles de défilement        (   (Cadence de défilement         2   2Pas de progression du défilement        ,   ,Contours des enchaînements        &   &Ajustement des formes   	     &   &Attributs utilisateur   
     B   BUtiliser un interligne indépendant de la police         <   <Renvoyer le texte à la ligne dans la forme        *   *Adapter la forme au texte        *   *Réservé pour SvDraw 18         *   *Réservé pour SvDraw 19         $   $Type de connecteur         .   .Espacement horizontal objet 1        ,   ,Espacement vertical objet 1        .   .Espacement horizontal objet 2        ,   ,Espacement vertical objet 2        0   0Espacement d'adhésion objet 1         0   0Espacement d'adhésion objet 2         0   0Nombre de lignes déplaçables         &   &Ligne de décalage 1         &   &Ligne de décalage 2         &   &Ligne de décalage 3    $     "   "Type de cotation    %     4   4Valeur de la cote à l'horizontale    &     2   2Valeur de la cote à la verticale   '     *   *Écart des lignes de cote   (     ,   ,Saillie des repères cotés   )     0   0Espacement des repères cotés    *     4   4Dépassement des repères cotés 1    +     4   4Dépassement des repères cotés 2    ,     2   2Cotation de l'arête inférieure    -     F   FValeur de la cote perpendiculaire à la ligne de cote   .     <   <Pivoter la valeur de la cote de 180 degrés   /     ,   ,Saillie des lignes de cote    0     "   "Unité de mesure    1     4   4Facteur supplémentaire d'échelle    2     0   0Affichage de l'unité de mesure   3     8   8Format de la valeur de la ligne de cote   4     L   LPositionnement automatique de la valeur de la ligne de cote   5     V   VAngle de positionnement automatique de la valeur de la ligne de cote    6     H   HDéfinition de l'angle de la valeur de la ligne de cote   7     8   8Angle de la valeur de la ligne de cote    8        Décimales    9     *   *Réservé pour cotation 5   :     *   *Réservé pour cotation 6   ;     *   *Réservé pour cotation 7   =          Type de cercle    >     "   "Angle de départ    ?        Angle final   @     (   (Réservé pour cercle 0   A     (   (Réservé pour cercle 1   B     (   (Réservé pour cercle 2   C     (   (Réservé pour cercle 3   D          Objet, visible    E     ,   ,Position d'objet protégée   F     *   *Taille d'objet protégée   G     "   "Objet, imprimable   H     *   *Identification de niveau    I        Couche    J          Nom de l'objet    K     &   &Position X pour tout    L     &   &Position Y pour tout    M          Largeur totale    N          Hauteur totale    O     (   (Position X individuelle   P     (   (Position Y individuelle   Q     &   &Largeur individuelle    R     &   &Hauteur individuelle    S          Largeur logique   T          Hauteur logique   U     .   .Angle de rotation individuel    V     2   2Angle de cisaillement individuel    W     *   *Déplacer horizontalement   X     (   (Déplacer verticalement   Y     ,   ,Redimensionner uniquement X   Z     ,   ,Redimensionner uniquement Y   [     &   &Rotation individuelle   \     0   0Cisaillement horiz. individuel    ]     .   .Cisaillement vert. individuel   ^     ,   ,Redimensionner X pour tout    _     ,   ,Redimensionner Y pour tout    `     $   $Rotation pour tout    a     .   .Cisaillement horiz. pour tout   b     .   .Cisaillement vert. pour tout    c     *   *Point de référence 1 X    d     *   *Point de référence 1 Y    e     *   *Point de référence 2 X    f     *   *Point de référence 2 Y    g     "   "Coupure des mots    h     $   $Afficher les puces    i     .   .Retraits de la numérotation    j     (   (Niveau de numérotation   k     (   (Puces et numérotations   l        Retraits    m     2   2Espacement entre les paragraphes    n        Interligne    o     *   *Alignement de paragraphe    p        Tabulations   q     "   "Couleur de police   r     $   $Jeu de caractères    s     $   $Taille de la police   t     &   &Largeur de la police    u     "   "Graisse de police   v        Soulignage    w        Surlignage    x        Barré    y        Italique    z        Contour   {        Ombre   |          Exposant/Indice   }        Crénage    ~          Crénage manuel        .   .Aucun soulignage des espaces    �        Tabulation    �     .   .Renvoi à la ligne facultatif   �     ,   ,Caractère non convertible    �        Champs    �     $   $Proportion de rouge   �     $   $Proportion de vert    �     $   $Proportion de bleu    �        Luminosité   �        Contraste   �        Gamma   �        Transparence    �        Invertir    �          Mode Graphique    �        Rogner    �            �            �            �            �     4   4Appliquer les attributs du tableau    �     &   &AutoFormat de tableau   �     &   &Insérer une colonne    �     $   $Insérer une ligne    �     &   &Supprimer la colonne    �     $   $Supprimer la ligne    �     &   &Scinder les cellules    �     (   (Fusionner les cellules    �     $   $Formater la cellule   �     &   &Répartir les lignes    �     (   (Répartir les colonnes    �     "   "Style de tableau    �     2   2Paramétrages du style de tableau   �     4   4Supprimer le contenu de la cellule    �     8   8Prochain lien dans la chaîne de texte    '        [Toutes]    'W     ,   ,Fin de la feuille atteinte    'Y        Favori    'Z        X   '[        Y   '\        Z   ']        R :   '^        V :   '_     *   *Fin du document atteinte    '`     &   &Y compris les styles    'a        (Rechercher)    'b        (Remplacer)   'c     6   6Rechercher les styles de ~paragraphe    'd        B :   'e     2   2Rechercher les styles de ~cellule   'h     .   .Terme recherché introuvable    'i     n   nÊtes-vous sûr de vouloir annuler les données de récupération du document %PRODUCTNAME ?    'j     .   .Arrivé au début du document   '�        Continu   '�        Dégradé   '�        Bitmap    '�          Style de ligne    '�        Aucun(e)    '�        Couleur   '�        Hachure   '�        - aucun(e)-   '�     "   "Aucun remplissage   '�        Motif   '�        Bordures    '�     $   $Couleur de bordure    '�     "   "Style de bordure    '�     .   .Couleur de mise en évidence    '�     &   &Effacer le formatage    '�     "   "Plus d'options...   '�     ^   ^Nom de police. La police active n'est pas disponible et va être substituée.   '�        Nom de police   '�     "   "Couleur de ligne    '�     "   "Plus de styles...   '�     (   (Couleur de remplissage    '�     *   *Plus de numérotation...    '�     "   "Plus de puces...    '�     $   $Palette par défaut   '�     &   &Couleurs du document    '�     $   $Couleur du document   '�     L   LMode insérer. Cliquez pour changer pour le mode écraser.    '�     L   LMode écraser. Cliquez pour changer pour le mode insérer.    '�        Écraser    '�     N   NSignature numérique : la signature du document est correcte.   '�     |   |Signature numérique : la signature du document est correcte, mais le certificat n'a pas pu être validé.    '�     �   �Signature numérique : la signature du document ne correspond pas à son contenu. Il est vivement recommandé de ne pas le considérer comme un document de confiance.    '�     F   FSignature numérique : le document n'est pas signé.    '�     �   �Signature numérique : la signature du document et le certificat sont corrects, mais toutes les parties du document ne sont pas signées.   '�     (   (Extrémités de flèche   (        Gauche    (        Droite    (        Centre    (        Décimal    (
        Utilisateur   (     &   &Thème de la Gallery    (     &   &Éléments du thème    (        Aperçu   (        Fermer    (#        Noir    ($        Bleu    (%        Vert    (&        Cyan    ('        Rouge   ((        Magenta   (*        Gris    (1        Jaune   (2        Blanc   (3        Gris bleu   (4        Orange    (5        Turquoise   (6        Turquoise   (7          Bleu classique    (8        Blue classic    (<        Flèche   (=        Carré    (>        Cercle    (A        Transparence    (B        Centré   (C        Non centré   (D        Liste   (E        Filtrer   (�        Transparent   (�     "   "Couleur d'origine   (�     $   $Palette de couleurs   (�        Tolérance    (�        Remplacer par   (�     (   (Insérer le(s) objet(s)   (�     0   0Créer un objet de rotation 3D    (�     $   $Nombre de segments    (�     &   &Profondeur de l'objet   (�     $   $Longueur de focale    (�     (   (Position de la caméra    (�     (   (Rotation de l'objet 3D    )      ,   ,Créer un objet d'extrusion   )     0   0Créer un corps de révolution    )     (   (Fractionner l'objet 3D    )        Attributs 3D    )        Par défaut   )          Niveaux de gris   )        Noir/Blanc    )        Filigrane   )     *   *Intel Indeo Video (*.ivf)   )     ,   ,Vidéo pour Windows (*.avi)   )     (   (QuickTime Movie (*.mov)   )     J   JMPEG - Motion Pictures Experts Group (*.mpe;*.mpeg;*.mpg)   )         <Tous>    )!     *   *Insérer un fichier audio   )"     $   $Insérer une vidéo   )#        Arrière-plan   ),        Violet    )-        Bordeaux    ).        Jaune pâle   )/        Vert pâle    )0        Violet foncé   )1        Saumon    )2        Azur    )4     :   :Vert 1 (couleur principale %PRODUCTNAME)    )5        Accent vert   )6        Accent bleu   )7        Accent orange   )8        Mauve   )9        Accent mauve    ):        Accent jaune    )@        3D    )A        Noir 1    )B        Noir 2    )C        Bleu    )D        Marron    )E        Monnaie   )F        Monnaie 3D    )G        Monnaie grise   )H          Monnaie lavande   )I     "   "Monnaie turquoise   )J        Gris    )K        Vert    )L        Lavande   )M        Rouge   )N        Turquoise   )O        Jaune   )Z     &   &Coiffe de ligne plate   )[     &   &Coiffe de ligne ronde   )\     (   (Coiffe de ligne carrée   )]     .   .Jointure de lignes moyennée    )^     .   .Jointure de lignes biseautée   )_     ,   ,Jointure de lignes sécante   )`     *   *Jointure de lignes ronde    )c        Black   )d        Blue    )e        Green   )f        Cyan    )g        Red   )h        Magenta   )j        Gray    )q        Yellow    )r        White   )s        Blue gray   )t        Orange    )u        Violet    )v        Bordeaux    )w        Pale yellow   )x        Pale green    )y        Dark violet   )z        Salmon    ){        Sea blue    )|        Sun   )}        Diagramme   )~        Chart   )        Mauve   )�        Purple    )�        Bleu ciel   )�        Sky blue    )�        Vert jaune    )�        Yellow green    )�        Rose    )�        Pink    )�     2   2Green 1 (%PRODUCTNAME Main Color)   )�        Green Accent    )�        Blue Accent   )�        Orange Accent   )�        Purple    )�        Purple Accent   )�        Yellow Accent   )�          Tango : beurre    )�          Tango : orange    )�     "   "Tango : chocolat    )�     $   $Tango : caméléon    )�     "   "Tango : bleu ciel   )�        Tango : prune   )�     (   (Tango : rouge écarlate   )�     "   "Tango : aluminium   )�        Tango: Butter   )�        Tango: Orange   )�     "   "Tango: Chocolate    )�     "   "Tango: Chameleon    )�          Tango: Sky Blue   )�        Tango: Plum   )�     $   $Tango: Scarlet Red    )�     "   "Tango: Aluminium    )�     &   &Black 45 Degrees Wide   )�     "   "Black 45 Degrees    )�     "   "Black -45 Degrees   )�     "   "Black 90 Degrees    )�     (   (Red Crossed 45 Degrees    )�     &   &Red Crossed 0 Degrees   )�     (   (Blue Crossed 45 Degrees   )�     (   (Blue Crossed 0 Degrees    )�     (   (Blue Triple 90 Degrees    )�          Black 0 Degrees   )�        Hatching    )�     ,   ,Noir 45 degrés de largeur    )�          Noir 45 degrés   )�     "   "Noir -45 degrés    )�          Noir 90 degrés   )�     2   2Rouge double incliné 45 degrés    )�     0   0Rouge double incliné 0 degré    )�     0   0Bleu double incliné 45 degrés   )�     .   .Bleu double incliné 0 degré   )�     0   0Bleu triple incliné 90 degrés   )�        Noir 0 degré   )�        Hachure   )�        Empty   )�        Sky   )�        Aqua    )�        Coarse    )�        Space Metal   )�        Space   )�        Metal   )�        Wet   )�        Marble    )�        Linen   )�        Stone   )�        Pebbles   )�        Wall    )�        Red Wall    )�        Pattern   )�        Leaves    )�          Lawn Artificial   )�        Daisy   )�        Orange    )�        Fiery   )�        Roses   )�        Bitmap    )�        Vide    )�        Ciel    )�        Eau   )�        Gros grain    )�        Mercure   )�        Cosmos    )�        Métal    )�        Gouttelettes    )�        Marbre    )�        Lin   )�        Pierre    )�        Gravier   )�        Muraille    )�        Briques   )�        Tresses   )�        Vigne vierge    )�     "   "Gazon artificiel    )�        Marguerites   )�        Orange    )�        Flamme    )�        Roses   )�        Bitmap    )�     "   "Ultrafine Dashed    )�        Fine Dashed   )�     *   *Ultrafine 2 Dots 3 Dashes   )�        Fine Dotted   )�     $   $Line with Fine Dots   )�     "   "Fine Dashed (var)   )�     &   &3 Dashes 3 Dots (var)   )�     (   (Ultrafine Dotted (var)    )�        Line Style 9    )�        2 Dots 1 Dash   )�        Dashed (var)    )�        Dash    *         Line Style    *          Tiret ultra-fin   *        Tiret fin   *     "   "3 tirets 2 points   *        Point fin   *     (   (Ligne avec points fins    *        Tiret fin   *     "   "3 tirets 3 points   *          Tiret ultra-fin   *	     "   "Style de ligne 9    *
     "   "1 Tiret 2 points    *        Tirets    *        Tirets    *          Style de ligne    *2     ,   ,Impression de la sélection   *3     Z   ZSouhaitez vous imprimer uniquement la sélection ou le document entier ?    *4        ~Tout   *5        ~Sélection   *D        Rogner    *E        Mode Image    *F        Rouge   *G        Vert    *H        Bleu    *I        Luminosité   *J        Contraste   *K        Gamma   *L        Transparence    *Y        Automatique   *^     .   .Actions à annuler : $(ARG1)    *_     0   0Actions à rétablir : $(ARG1)    *`     .   .Actions à annuler : $(ARG1)    *a     0   0Actions à rétablir : $(ARG1)    *b        Transparency    *c        Transparence    *m     (   (Couleur de matériau 3D   *n     "   "Couleur de police   *o     (   (Couleur d'arrière-plan   *p        Aucun   *q        Plein   *r        Hachuré    *s        Dégradé   *t        Bitmap    *u        avec    *v        Style   *w        et    *�     0   0Élément de contrôle de coin    *�     4   4Sélection d'un point d'inflexion.    *�     0   0Élément de contrôle d'angle    *�     2   2Sélection d'un angle principal.    *�     "   "En haut à gauche   *�     "   "En haut au milieu   *�     "   "En haut à droite   *�     $   $Au centre à gauche   *�        Centre    *�     $   $Au centre à droite   *�     "   "En bas à gauche    *�     "   "En bas au milieu    *�     "   "En bas à droite    *�        0 degré    *�        45 degrés    *�        90 degrés    *�        135 degrés   *�        180 degrés   *�        225 degrés   *�        270 degrés   *�        315 degrés   *�     &   &Contrôle du contour    *�     &   &Édition du contour.    *�     2   2Sélection de caractère spécial   *�     $   $Code de caractère    *�     >   >Zone de sélection des caractères spéciaux.   *�          Export d'image    *�        Extrusion   *�        Fontwork    *�     $   $Couleur d'extrusion   *�        ~0 cm   *�        ~1 cm   *�        ~2,5 cm   *�        ~5 cm   *�        10 ~cm    *�        0 pouce   *�        0,~5 pouce    *�        ~1 pouce    *�        ~2 pouces   *�        ~4 pouces   *�        Pages   +n     .   .Appliquer une forme Fontwork    +o     J   JAppliquer une hauteur identique pour les lettres Fontwork   +p     2   2Appliquer un alignement Fontwork    +r     @   @Appliquer l'espacement des caractères Fontwork   +s     <   <(Dés)activer l'application de l'extrusion    +t     &   &Incliner vers le bas    +u     &   &Incliner vers le haut   +v     (   (Incliner vers la gauche   +w     (   (Incliner vers la droite   +x     4   4Modifier la profondeur d'extrusion    +y     (   (Modifier l'orientation    +z     0   0Modifier le type de projection    +{     &   &Modifier l'éclairage   +|     (   (Modifier la luminosité   +}     0   0Modifier la surface d'extrusion   +~     0   0Modifier la couleur d'extrusion   +�     2   2Puces circulaires petites pleines   +�     2   2Puces circulaires grandes pleines   +�     (   (Puces losanges pleines    +�     0   0Puces carrées grandes pleines    +�     @   @Puces flèche pointant vers la droite remplies    +�     6   6Puces flèche pointant vers la droite   +�     (   (Puces en forme de coche   +�     8   8Puces en forme de marque de graduation    +�     "   "Numéro 1) 2) 3)    +�     "   "Numéro 1. 2. 3.    +�     $   $Numéro (1) (2) (3)   +�     D   DNuméro en chiffres romains majuscules I. II. III.    +�     *   *Lettre majuscule A) B) C)   +�     *   *Lettre minuscule a) b) c)   +�     .   .Lettre minuscule (a) (b) (c)    +�     D   DNuméro en chiffres romains minuscules i. ii. iii.    +�     V   VPetite puce circulaire pleine, en minuscules, numérique, numérique    +�     J   JPetite puce circulaire pleine, en minuscules, numérique    +�     p   pPetite puce circulaire pleine, en majuscules, en minuscules romaines, en minuscules, numérique   +�        Numérique    +�     |   |Petite puce circulaire pleine, en minuscules, en minuscules romaines, en majuscules, en majuscules romaines   +�     |   |Petite puce circulaire pleine, en minuscules romaines, en minuscules, en majuscules romaines, en majuscules   +�     6   6Numérique avec tous les sous-niveaux   +�     �   �Petite puce circulaire pleine, puce pleine en forme de losange, puce en forme de flèche vers la droite, puce pointant à droite    +�     .   .Styles d'ébauche de tableau    +�     "   "Couleur de police   +�     @   @Chercher un contenu de cellule tel qu'affiché    +�     $   $Respecter la casse    +�        Rechercher    +�     b   bLe document a été modifié. Faites un double-clic pour enregistrer le document.   +�     V   VLe document n'a pas été modifié depuis le dernier enregistrement.    +�     *   *Chargement du document...   +�     (   (Mot de passe incorrect    +�     8   8Les mots de passe ne correspondent pas    +�     8   8Adapter la diapo à la fenêtre active    ,     *   *Récupération terminée    ,     0   0Document d'origine récupéré    ,     ,   ,Échec de la récupération   ,     (   (Récupération en cours   ,     .   .Récupération non terminée    ,     �   �%PRODUCTNAME %PRODUCTVERSION va débuter la récupération des documents. En fonction de la taille des documents, ce procédé peut prendre du temps.   ,     n   nLa récupération des documents est terminée.
Clique sur 'Terminer' pour voir vos documents.   ,        ~Terminer   ,     �   �Niveau de zoom. Faites un clic avec le bouton droit pour modifier le niveau de zoom ou cliquez pour ouvrir la boîte de dialogue Zoom.    ,     "   "Agrandir le zoom    ,     "   "Réduire le zoom    ,     "   "~Personnalisé...   ,         ~Infini   ,!        ~Fil de fer   ,"        ~Mat    ,#        ~Plastique    ,$        Mé~tal   ,%        ~Très serré   ,&        ~Serré   ,'        ~Normal   ,(        ~Large    ,)        ~Très large    ,*     "   "~Personnalisé...   ,+     4   4~Créner les paires de caractères    ,8     &   &Extrusion nord-ouest    ,9          Extrusion nord    ,:     $   $Extrusion nord-est    ,;          Extrusion ouest   ,<     (   (Extrusion vers le haut    ,=        Extrusion est   ,>     $   $Extrusion sud-ouest   ,?        Extrusion sud   ,@     "   "Extrusion sud-est   ,B        ~Perspective    ,C        P~arallèle   ,D        ~Clair    ,E        ~Normal   ,F        ~Estompé   ,h     $   $A~ligner à gauche    ,i        ~Centrer    ,j     $   $Aligne~r à droite    ,k     "   "~Mots justifiés    ,l     &   &É~tirement justifié   ,v        25 %    ,w        50 %    ,x        75 %    ,y        100 %   ,z        150 %   ,{        200 %   ,|        Page entière   ,}          Largeur de page   ,~     "   "Affichage optimal   .�        Gradient    .�     "   "Linear blue/white   .�     &   &Linear magenta/green    .�     $   $Linear yellow/brown   .�     $   $Radial green/black    .�     "   "Radial red/yellow   .�     &   &Rectangular red/white   .�     $   $Square yellow/white   .�     0   0Ellipsoid blue grey/light blue    .�     &   &Axial light red/white   .�        Diagonal 1l   .�        Diagonal 1r   .�        Diagonal 2l   .�        Diagonal 2r   .�        Diagonal 3l   .�        Diagonal 3r   .�        Diagonal 4l   .�        Diagonal 4r   .�        Diagonal Blue   .�          Diagonal Green    .�          Diagonal Orange   .�        Diagonal Red    .�     $   $Diagonal Turquoise    .�          Diagonal Violet   .�        From a Corner   .�     $   $From a Corner, Blue   .�     &   &From a Corner, Green    .�     &   &From a Corner, Orange   .�     $   $From a Corner, Red    .�     *   *From a Corner, Turquoise    .�     &   &From a Corner, Violet   .�          From the Middle   /      &   &From the Middle, Blue   /     (   (From the Middle, Green    /     (   (From the Middle, Orange   /     &   &From the Middle, Red    /     ,   ,From the Middle, Turquoise    /     (   (From the Middle, Violet   /        Horizontal    /          Horizontal Blue   /     "   "Horizontal Green    /	     "   "Horizontal Orange   /
          Horizontal Red    /     &   &Horizontal Turquoise    /     "   "Horizontal Violet   /        Radial    /        Radial Blue   /        Radial Green    /        Radial Orange   /        Radial Red    /     "   "Radial Turquoise    /        Radial Violet   /        Vertical    /        Vertical Blue   /          Vertical Green    /          Vertical Orange   /        Vertical Red    /     $   $Vertical Turquoise    /          Vertical Violet   /        Gray Gradient   /          Yellow Gradient   /          Orange Gradient   /        Red Gradient    /        Pink Gradient   /         Sky   /!        Cyan Gradient   /"        Blue Gradient   /#        Purple Pipe   /$        Night   /%          Green Gradient    /&        Tango Green   /'     $   $Subtle Tango Green    /(        Tango Purple    /)        Tango Red   /*        Tango Blue    /+        Tango Yellow    /,        Tango Orange    /-        Tango Gray    /.        Argile    //        Olive Green   /0        Grison    /1        Sunburst    /2        Brownie   /3        Sun-Set   /4        Deep Green    /5        Deep Orange   /6        Deep Blue   /7        Purple Haze   /D        Dégradé   /E     &   &Linéaire bleu/blanc    /F     &   &Linéaire violet/vert   /G     (   (Linéaire jaune/marron    /H     "   "Radial vert/noir    /I     $   $Radial rouge/jaune    /J     *   *Rectangulaire rouge/blanc   /K     $   $Carré jaune/blanc    /L     2   2Ellipsoïde gris bleu/bleu clair    /M     (   (Axial rouge clair/blanc   /N        1g diagonale    /O        1d diagonale    /P        2g diagonales   /Q        2d diagonales   /R        3g diagonales   /S        3d diagonales   /T        4g diagonales   /U        4d diagonales   /V          Bleu diagonale    /W          Vert diagonale    /X     "   "Orange diagonale    /Y          Rouge diagonale   /Z     $   $Turquoise diagonale   /[     "   "Violet diagonale    /\     $   $À partir d'un coin   /]     *   *À partir d'un coin, bleu   /^     *   *À partir d'un coin, vert   /_     ,   ,À partir d'un coin, orange   /`     ,   ,À partir d'un coin, rouge    /a     0   0À partir d'un coin, turquoise    /b     ,   ,À partir d'un coin, violet   /c     $   $À partir du milieu   /d     *   *À partir du milieu, bleu   /e     *   *À partir du milieu, vert   /f     ,   ,À partir du milieu, orange   /g     ,   ,À partir du milieu, rouge    /h     0   0À partir du milieu, turquoise    /i     ,   ,À partir du milieu, violet   /j        Horizontal    /k          Bleu horizontal   /l          Vert horizontal   /m     "   "Orange horizontal   /n     "   "Rouge horizontal    /o     &   &Turquoise horizontal    /p     "   "Violet horizontal   /q        Radial    /r        Bleu radial   /s        Vert radial   /t        Orange radial   /u        Rouge radial    /v     "   "Turquoise radial    /w        Violet radial   /x        Vertical    /y        Bleu vertical   /z        Vert vertical   /{          Orange vertical   /|          Rouge vertical    /}     $   $Turquoise vertical    /~          Violet vertical   /          Dégradé gris    /�          Dégradé jaune   /�     "   "Dégradé orange    /�          Dégradé rouge   /�          Dégradé rose    /�        Ciel    /�          Dégradé cyan    /�          Dégradé bleu    /�        Tube violet   /�        Nuit    /�          Dégradé vert    /�        Vert Tango    /�          Vert doux Tango   /�        Violet Tango    /�        Rouge Tango   /�        Bleu Tango    /�        Jaune Tango   /�        Orange Tango    /�        Gris Tango    /�        Terre   /�        Vert olive    /�        Argent    /�        Flamboyant    /�        Brownie   /�     "   "Coucher de soleil   /�        Vert profond    /�          Orange profong    /�        Bleu profond    /�          Brume violette    1�        Arrow concave   1�        Square 45   1�        Small Arrow   1�          Dimension Lines   1�        Double Arrow    1�     $   $Rounded short Arrow   1�          Symmetric Arrow   1�        Line Arrow    1�     $   $Rounded large Arrow   1�        Circle    1�        Square    1�        Arrow   1�     "   "Short line Arrow    1�     "   "Triangle unfilled   1�     "   "Diamond unfilled    1�        Diamond   1�          Circle unfilled   1�     $   $Square 45 unfilled    1�          Square unfilled   1�     &   &Half Circle unfilled    1�        Arrowhead   1�          Flèche concave   1�        Carré 45   1�     "   "Flèche étroite    1�     $   $Lignes de cotation    1�          Double flèche    1�     (   (Flèche courte arrondie   1�     $   $Flèche symétrique   1�     "   "Ligne de flèche    1�     (   (Flèche longue arrondie   2         Cercle    2        Carré    2        Flèche   2     &   &Flèche ligne courte    2     $   $Triangle non rempli   2     $   $Losange non rempli    2        Losange   2     "   "Cercle non rempli   2     &   &Carré 45 non rempli    2	     "   "Carré non rempli   2
     (   (Demi cercle non rempli    2     (   (Extrémité de flèche    :h     "   "Couleur de police   :p        Rechercher    :q          Tout rechercher   :r        Remplacer   :s          Tout remplacer    :t     $   $Style de caractère   :u     $   $Style de paragraphe   :v          Style de cadre    :w        Style de page   :z        Formule   :{        Valeur    :|        Note    :        StarWriter    :�        StarCalc    :�        StarDraw    :�        StarBase    :�        Aucun(e)    :�        Plein   :�        Horizontal    :�        Vertical    :�        Grille    :�        Losange   :�     (   (Diagonale vers le haut    :�     &   &Diagonale vers le bas   :�        25%   :�        50%   :�        75%   :�        Image   <      (   (Orientation par défaut   <          Du haut en bas    <          Du bas en haut    <        Empilé   <!        Table   <"        Pas de table    <#     $   $Espacement activé    <$     (   (Espacement désactivé    <%     4   4Conserver l'intervalle d'espacement   <&     ,   ,Espace inférieur autorisé   <F          Marge gauche :    <G     &   &Marge supérieure :     <H          Marge droite :    <I     &   &Marge inférieure :     <X     *   *Description de la page :    <Y        Majuscules    <Z        Minuscules    <[     $   $Majuscules romaines   <\     $   $Minuscules romaines   <]        Arabe   <^        Aucun(e)    <_        Paysage   <`        Portrait    <a        À gauche   <b        À droite   <c        Tout    <d     $   $Pages en vis-à-vis   <o        Auteur :    <p        Date :    <q        Texte :     <r     ,   ,Couleur d'arrière-plan :     <s     $   $Couleur de motif :    <u     ,   ,Arrière-plan de caractère   C     $   $Palette de couleurs   C        Changer   FQ     R   RLe nom %1 n'est pas valide au format XML. Saisissez un autre nom.   FR     \   \Le préfixe %1 n'est pas valide au format XML. Saisissez un autre préfixe.   FS     L   LLe nom '%1' existe déjà. Veuillez saisir un nouveau nom.    FT     2   2La soumission doit avoir un nom.    FU     �   �La suppression de la liaison "$BINDINGNAME" aura une incidence sur tous les contrôles actuellement liés à cette liaison.
 
Voulez-vous vraiment la supprimer ?   FV     �   �La suppression de la soumission '$SUBMISSIONNAME' aura une incidence sur tous les contrôles actuellement liés à cette soumission.

Voulez-vous vraiment la supprimer ?   FW     N   NVoulez-vous vraiment supprimer l'attribut '$ATTRIBUTENAME' ?    FX     �   �La suppression de l'élément '$ELEMENTNAME' aura une incidence sur tous les contrôles actuellement liés à cet élément.
Voulez-vous vraiment supprimer ce élément ?    FY     �   �La suppression de l'instance "$INSTANCENAME" aura une incidence sur tous les contrôles actuellement liés à cette instance.
Voulez-vous vraiment la supprimer ?   F[        Formulaire    F\          Enregistrement    F]        de    F^     ,   ,Définir la propriété '#'   F_     ,   ,Insérer dans le container    F`        Supprimer #   Fk     $   $Supprimer # objets    Fl     4   4Remplacer un élément de container   Fn     (   (Supprimer la structure    Fo     (   (Remplacer le contrôle    Fp     $   $Barre de navigation   Ft        Formulaire    Fu     $   $Ajouter un champ :    Fv     .   .Aucun contrôle sélectionné   Fw          Propriétés :    Fx     ,   ,Propriétés du formulaire    Fy     *   *Navigateur de formulaires   Fz        Formulaires   F{     R   RErreur lors de l'écriture des données dans la base de données    F|     F   FVous avez l'intention de supprimer 1 enregistrement.    F}     ~   ~Si vous choisissez Oui, l'opération ne peut plus être annulée !
Souhaitez-vous tout de même poursuivre ?    F~     $   $Élément de cadre    F        Navigation    F�        Col   F�        Date    F�        Heure   F�     $   $Barre de navigation   F�        Bouton    F�        Bouton radio    F�          Case à cocher    F�     $   $Champ d'étiquette    F�          Zone de groupe    F�        Zone de texte   F�        Zone de liste   F�          Zone combinée    F�        Bouton picto    F�          Contrôle picto   F�     &   &Sélection de fichier   F�        Champ de date   F�        Champ horaire   F�     "   "Champ numérique    F�     "   "Champ monétaire    F�        Champ masqué   F�     $   $Contrôle de table    F�     $   $Sélection multiple   F�     :   : # enregistrements vont être supprimés.   F�        Contrôle   F�         (Date)   F�         (Heure)    F�     P   PAucun contrôle lié à des données dans le formulaire actif !   F�     &   &Navigateur de filtres   F�        Filtre pour   F�        Ou    F�          Champ formaté    F�     D   DErreur de syntaxe dans l'expression de la requête    F�     ~   ~La suppression de '$MODELNAME' aura une incidence sur tous les contrôles actuellement liés à ce modèle.
    F�     �   �Le formulaire actif ne contient aucun contrôle correctement lié et donc susceptible d'être utilisé pour l'affichage de la table.    F�        <AutoChamp>   F�     8   8Erreur de syntaxe dans l'expression SQL   F�     >   >Impossible d'utiliser la valeur #1 avec LIKE.   F�     @   @LIKE ne peut pas être utilisé avec ce champ.    F�     N   NImpossible de comparer le critère spécifié avec ce champ.    F�     H   HImpossible de comparer le champ avec un nombre entier.    F�     v   vLa valeur saisie n'est pas une date valide. Utilisez un format valide pour la date, par ex. JJ/MM/AA.   F�     V   VImpossible de comparer le champ avec un nombre à virgule flottante.    F�     L   LLa base de données ne contient pas une table nommée "#".    F�     B   BLa colonne "#1" est inconnue dans la table "#2".    F�     &   &Barre de défilement    F�        Compteur    F�     "   "Contrôle masqué   F�        Élément   F�        Attribut    F�        Liaison   F�     &   &Expression de liaison   F�        Envoyer   F�        Mettre    F�        Récupérer   F�        Aucun   F�        Instance    F�        Document    F�     (   (Navigateur de données    F�        Envoi :     F�        ID :    F�        Action :    F�        Méthode :    F�          Référence :     F�        Liaison :     F�        Remplacer :     F�     &   &Ajouter un élément    F�     $   $Éditer l'élément   F�     &   &Supprimer l'élément   F�     $   $Ajouter un attribut   F�     $   $Éditer l'attribut    F�     &   &Supprimer l'attribut    F�     $   $Ajouter une liaison   F�     $   $Éditer la liaison    F�     &   &Supprimer la liaison    F�     "   "Ajouter un envoi    F�          Éditer l'envoi   F�     "   "Supprimer l'envoi   F�     Z   ZLa base de données ne contient ni une table ni une requête nommée "#".   F�     V   VLa base de données contient déjà une table ou une vue nommée "#".   F�     N   NLa base de données contient déjà une requête nommée "#".   F�     "   " (lecture seule)    F�     8   8Le fichier existe déjà. L'écraser ?    F�     (   (Étiquette de #object#    H�     <   <Erreur lors de la création d'un formulaire   H�     L   LCette entrée existe déjà.
Veuillez choisir un autre nom.   H�     J   JSaisie requise dans le champ "#". Saisissez-y une valeur.   H�     v   vSélectionnez une entrée de liste ou saisissez le texte concordant avec l'un des éléments listés.   (G  y   �   �   Millimètre    Centimètre    Mètre     Kilomètre     Pouce    Pied     	Miles    
Pica     Point    Car    Ligne      (H  y  
  
   NEurope occidentale (Windows-1252/WinLatin 1)     Europe occidentale (Apple Macintosh)     Europe occidentale (DOS/OS2-850/International)     Europe occidentale (DOS/OS2-437/US)    Europe occidentale (DOS/OS2-860/Portugais)     Europe occidentale (DOS/OS2-861/Islandais)     Europe occidentale (DOS/OS2-863/Français canadien)    Europe occidentale (DOS/OS2-865/Nordique)    Europe occidentale (ASCII/US)    Europe occidentale (ISO-8859-1)    Europe de l'Est (ISO-8859-2)     Latin 3 (ISO-8859-3)     Balte (ISO-8859-4)     Cyrillique (ISO-8859-5)    Arabe (ISO-8859-6)     Grec (ISO-8859-7)    Hébreu (ISO-8859-8)     Turc (ISO-8859-9)    Europe occidentale (ISO-8859-14)     Europe occidentale (ISO-8859-15/EURO)    Grec (DOS/OS2-737)     Balte (DOS/OS2-775)    Europe de l'Est (DOS/OS2-852)    Cyrillique (DOS/OS2-855)     Turc (DOS/OS2-857)     Hébreu (DOS/OS2-862)    Arabe (DOS/OS2-864)    Cyrillique (DOS/OS2-866/Russe)     Grec (DOS/OS2-869/Moderne)     Europe de l'Est (Windows-1250/WinLatin 2)    !Cyrillique (Windows-1251)    "Grec (Windows-1253)    #Turc (Windows-1254)    $Hébreu (Windows-1255)     %Arabe (Windows-1256)     &Balte (Windows-1257)     'Vietnamien (Windows-1258)    (Europe de l'Est (Apple Macintosh)    *Europe de l'Est (Apple Macintosh/Croate)     +Cyrillique (Apple Macintosh)     ,Grec (Apple Macintosh)     /Europe occidentale (Apple Macintosh/Islandais)     3Europe de l'Est (Apple Macintosh/Roumain)    4Turc (Apple Macintosh)     6Cyrillique (Apple Macintosh/Ukrainien)     7Chinois simplifié (Apple Macintosh)     8Chinois traditionnel (Apple Macintosh)     9Japonais (Apple Macintosh)     :Coréen (Apple Macintosh)    ;Japonais (Windows-932)     <Chinois simplifié (Windows-936)     =Coréen (Windows-949)    >Chinois traditionnel (Windows-950)     ?Japonais (Shift-JIS)     @Chinois simplifié (GB-2312)     AChinois simplifié (GB-18030)    UChinois traditionnel (GBT-12345)     BChinois simplifié (GBK/GB-2312-80)    CChinois traditionnel (Big5)    DChinois traditionnel (BIG5-HKSCS)    VJaponais (EUC-JP)    EChinois simplifié (EUC-CN)    FChinois traditionnel (EUC-TW)    GJaponais (ISO-2022-JP)     HChinois simplifié (ISO-2022-CN)     ICyrillique (KOI8-R)    JUnicode (UTF-7)    KUnicode (UTF-8)    LEurope de l'Est (ISO-8859-10)    MEurope de l'Est (ISO-8859-13)    NCoréen (EUC-KR)     OCoréen (ISO-2022-KR)    PCoréen (Windows-Johab-1361)     TUnicode (UTF-16)    ��Thaï (ISO-8859-11/TIS-620)    WThaï (Windows-874)     Cyrillique (KOI8-U)    XCyrillique (PT154)     ]  <v  y  L  L   ;Échelle    'Pinceau   'Espace entre les tabulations    'Caractère    'Police    'Inclinaison   'Graisse   'Ombré    'Mot par mot   'Contour   'Barré    'Soulignage    'Taille de la police   'Taille relative   ' Couleur de police   '!Crénage    '"Effets    '#Langue    '$Position    '%Clignotement de caractère    'SCouleur de jeu de caractères   *}Surlignage    -0Paragraphe    '*Alignement    '+Interligne    '1Saut de page    '5Coupure des mots    '6Ne pas scinder le paragraphe    '7Orphelines    '8Veuves    '9Espacement entre les paragraphes    ':Retrait de paragraphe   ';Retrait   '@Espacement    'APage    'BStyle de page   'QConserver avec le paragraphe suivant    'RClignotant    (�Contrôle de repérage    (�Arrière-plan de caractère   )_Police asiatique    *�Taille de la police asiatique   *�Langue de la police asiatique   *�Inclinaison de la police asiatique    *�Graisse de la police asiatique    *�Scripts complexes   *�Taille des scripts complexes    *�Langue des scripts complexes    *�Inclinaison des scripts complexes   *�Taille des scripts complexes    *�Deux lignes en une    *�Caractères d'accentuation    *�Espacement du texte   *�Ponctuation en retrait    *�Caractères non permis    *�Rotation    *�Mise à l'échelle des caractères    *�Relief    *�Alignement vertical du texte    *�  �     *   *      res/grafikei.png    �     *   *      res/grafikde.png    �     ,   ,      svx/res/markers.png   �     4   4      svx/res/pageshadow35x35.png   �     (   (      res/oleobj.png    �     0   0      svx/res/cropmarkers.png   '�     .   .      svx/res/rectbtns.png    '�     ,   ,      svx/res/objects.png   '�     (   (      	svx/res/ole.png   '�     ,   ,      
svx/res/graphic.png   'S  #   f   f            8   8      svx/res/slidezoombutton_10.png              ��  ��      'T  #   b   b            4   4      svx/res/slidezoomout_10.png             ��  ��      'U  #   b   b            4   4      svx/res/slidezoomin_10.png              ��  ��      'e  #   V   V            (   (      res/sc10223.png             ��  ��      'f  #   V   V            (   (      res/sc10224.png             ��  ��      'g  #   `   `            2   2      svx/res/signet_11x16.png              ��  ��      'i  #   `   `            2   2      svx/res/caution_11x16.png             ��  ��      'k  #   d   d            6   6      svx/res/notcertificate_16.png             ��  ��      '�  #   Z   Z            ,   ,      svx/res/lighton.png             ��  ��      '�  #   X   X            *   *      svx/res/light.png             ��  ��      '�  #   \   \            .   .      svx/res/colordlg.png              ��  ��      '�  #   H   H            4   4      svx/res/selection_10x22.png   (  #   \   \            .   .      svx/res/notcheck.png              ��  ��      (  #   \   \            .   .      svx/res/lngcheck.png              ��  ��      (�  #   V   V            (   (      res/sc10865.png             ��  ��      (�  #   V   V            (   (      res/sc10866.png             ��  ��      (�  #   V   V            (   (      res/sc10867.png             ��  ��      (�  #   V   V            (   (      res/sc10863.png             ��  ��      (�  #   V   V            (   (      res/sc10864.png             ��  ��      (�  #   V   V            (   (      res/sc10868.png             ��  ��      (�  #   V   V            (   (      res/sc10869.png             ��  ��      (�  #   V   V            (   (       res/reload.png              ��  ��      (�  #   Z   Z            ,   ,      !svx/res/reloads.png             ��  ��      (�  #   h   h            :   :      "svx/res/extrusioninfinity_16.png              ��  ��      (�  #   d   d            6   6      #svx/res/extrusion0inch_16.png             ��  ��      (�  #   f   f            8   8      $svx/res/extrusion05inch_16.png              ��  ��      (�  #   d   d            6   6      %svx/res/extrusion1inch_16.png             ��  ��      (�  #   d   d            6   6      &svx/res/extrusion2inch_16.png             ��  ��      (�  #   d   d            6   6      'svx/res/extrusion4inch_16.png             ��  ��      )$  #   `   `            2   2      (svx/res/wireframe_16.png              ��  ��      )%  #   \   \            .   .      )svx/res/matte_16.png              ��  ��      )&  #   ^   ^            0   0      *svx/res/plastic_16.png              ��  ��      )'  #   \   \            .   .      +svx/res/metal_16.png              ��  ��      +�  #   f   f            8   8      ,svx/res/doc_modified_yes_14.png             ��  ��      +�  #   f   f            8   8      -svx/res/doc_modified_no_14.png              ��  ��      +�  #   h   h            :   :      .svx/res/doc_modified_feedback.png             ��  ��      +�  #   f   f            8   8      /svx/res/zoom_page_statusbar.png             ��  ��      +�  #   J   J            6   6      0svx/res/symphony/spacing3.png   +�  #   P   P            <   <      1svx/res/symphony/Indent_Hanging.png   +�  #   H   H            4   4      2svx/res/symphony/blank.png    +�  #   H   H            4   4      3svx/res/symphony/width1.png   ,   #   H   H            4   4      4svx/res/symphony/width2.png   ,  #   H   H            4   4      5svx/res/symphony/width3.png   ,  #   H   H            4   4      6svx/res/symphony/width4.png   ,  #   H   H            4   4      7svx/res/symphony/width5.png   ,  #   H   H            4   4      8svx/res/symphony/width6.png   ,  #   H   H            4   4      9svx/res/symphony/width7.png   ,  #   H   H            4   4      :svx/res/symphony/width8.png   ,  #   H   H            4   4      ;svx/res/symphony/axial.png    ,  #   L   L            8   8      <svx/res/symphony/ellipsoid.png    ,	  #   L   L            8   8      =svx/res/symphony/Quadratic.png    ,
  #   H   H            4   4      >svx/res/symphony/radial.png   ,  #   H   H            4   4      ?svx/res/symphony/Square.png   ,  #   H   H            4   4      @svx/res/symphony/linear.png   ,  #   N   N            :   :      Asvx/res/symphony/rotate_left.png    ,  #   N   N            :   :      Bsvx/res/symphony/rotate_right.png   ,  #   >   >            *   *      Csvx/res/nu01.png    ,  #   >   >            *   *      Dsvx/res/nu04.png    ,  #   >   >            *   *      Esvx/res/nu02.png    ,,  #   h   h            :   :      Fsvx/res/directionnorthwest_22.png             ��  ��      ,-  #   d   d            6   6      Gsvx/res/directionnorth_22.png             ��  ��      ,.  #   h   h            :   :      Hsvx/res/directionnortheast_22.png             ��  ��      ,/  #   d   d            6   6      Isvx/res/directionwest_22.png              ��  ��      ,0  #   h   h            :   :      Jsvx/res/directionstraight_22.png              ��  ��      ,1  #   d   d            6   6      Ksvx/res/directioneast_22.png              ��  ��      ,2  #   h   h            :   :      Lsvx/res/directionsouthwest_22.png             ��  ��      ,3  #   d   d            6   6      Msvx/res/directionsouth_22.png             ��  ��      ,4  #   h   h            :   :      Nsvx/res/directionsoutheast_22.png             ��  ��      ,6  #   b   b            4   4      Osvx/res/perspective_16.png              ��  ��      ,7  #   ^   ^            0   0      Psvx/res/parallel_16.png             ��  ��      ,G  #   j   j            <   <      Qsvx/res/lightofffromtopleft_22.png              ��  ��      ,H  #   f   f            8   8      Rsvx/res/lightofffromtop_22.png              ��  ��      ,I  #   j   j            <   <      Ssvx/res/lightofffromtopright_22.png             ��  ��      ,J  #   f   f            8   8      Tsvx/res/lightofffromleft_22.png             ��  ��      ,L  #   h   h            :   :      Usvx/res/lightofffromright_22.png              ��  ��      ,M  #   l   l            >   >      Vsvx/res/lightofffrombottomleft_22.png             ��  ��      ,N  #   h   h            :   :      Wsvx/res/lightofffrombottom_22.png             ��  ��      ,O  #   n   n            @   @      Xsvx/res/lightofffrombottomright_22.png              ��  ��      ,Q  #   h   h            :   :      Ysvx/res/lightonfromtopleft_22.png             ��  ��      ,R  #   d   d            6   6      Zsvx/res/lightonfromtop_22.png             ��  ��      ,S  #   j   j            <   <      [svx/res/lightonfromtopright_22.png              ��  ��      ,T  #   f   f            8   8      \svx/res/lightonfromleft_22.png              ��  ��      ,V  #   f   f            8   8      ]svx/res/lightonfromright_22.png             ��  ��      ,W  #   l   l            >   >      ^svx/res/lightonfrombottomleft_22.png              ��  ��      ,X  #   h   h            :   :      _svx/res/lightonfrombottom_22.png              ��  ��      ,Y  #   l   l            >   >      `svx/res/lightonfrombottomright_22.png             ��  ��      ,[  #   f   f            8   8      asvx/res/lightfromtopleft_22.png             ��  ��      ,\  #   b   b            4   4      bsvx/res/lightfromtop_22.png             ��  ��      ,]  #   h   h            :   :      csvx/res/lightfromtopright_22.png              ��  ��      ,^  #   d   d            6   6      dsvx/res/lightfromleft_22.png              ��  ��      ,_  #   d   d            6   6      esvx/res/lightfromfront_22.png             ��  ��      ,`  #   d   d            6   6      fsvx/res/lightfromright_22.png             ��  ��      ,a  #   j   j            <   <      gsvx/res/lightfrombottomleft_22.png              ��  ��      ,b  #   f   f            8   8      hsvx/res/lightfrombottom_22.png              ��  ��      ,c  #   j   j            <   <      isvx/res/lightfrombottomright_22.png             ��  ��      ,e  #   `   `            2   2      jsvx/res/brightlit_16.png              ��  ��      ,f  #   `   `            2   2      ksvx/res/normallit_16.png              ��  ��      ,g  #   \   \            .   .      lsvx/res/dimlit_16.png             ��  ��      ,m  #   h   h            :   :      msvx/res/fontworkalignleft_16.png              ��  ��      ,n  #   l   l            >   >      nsvx/res/fontworkaligncentered_16.png              ��  ��      ,o  #   h   h            :   :      osvx/res/fontworkalignright_16.png             ��  ��      ,p  #   l   l            >   >      psvx/res/fontworkalignjustified_16.png             ��  ��      ,q  #   j   j            <   <      qsvx/res/fontworkalignstretch_16.png             ��  ��      ,r  #   >   >            *   *      rsvx/res/fw018.png   ,s  #   >   >            *   *      ssvx/res/fw019.png   ,t  #   >   >            *   *      tsvx/res/fw016.png   ,u  #   >   >            *   *      usvx/res/fw017.png      $  �  �   id              ��  ��       svx/res/id01.png     svx/res/id02.png     svx/res/id03.png     svx/res/id04.png     svx/res/id05.png     svx/res/id06.png     svx/res/id07.png     svx/res/id08.png     svx/res/id030.png    svx/res/id031.png    svx/res/id032.png     svx/res/id033.png    !svx/res/id040.png    (svx/res/id041.png    )svx/res/id016.png    svx/res/id018.png    svx/res/id019.png       'Q  $  @  @   fr              ��  ��       svx/res/fr01.png     svx/res/fr02.png     svx/res/fr03.png     svx/res/fr04.png     svx/res/fr05.png     svx/res/fr06.png     svx/res/fr07.png     svx/res/fr08.png     svx/res/fr09.png     	svx/res/fr010.png    
svx/res/fr011.png    svx/res/fr012.png       'R  $   �   �   da              ��  ��       res/da01.png     res/da02.png     res/da03.png     res/da04.png     res/da05.png     res/da06.png        FP  $  �  �   sx              �   �        res/sx10594.png   )bres/sx10595.png   )cres/sx10596.png   )dres/sx10597.png   )eres/sx10598.png   )fres/sx10599.png   )gres/sx10600.png   )hres/sx10601.png   )ires/sx10607.png   )ores/sx10144.png   '�res/sx10593.png   )ares/sx18013.png   F]res/sx18002.png   FRres/sx18003.png   FSres/sx10604.png   )lres/sx10605.png   )mres/sx10704.png   )�res/sx10705.png   )�res/sx10706.png   )�res/sx10707.png   )�res/sx10708.png   )�res/sx18022.png   Ffres/sx10710.png   )�res/sx10603.png   )kres/sx10715.png   )�res/sx10728.png   )�res/sx10757.png   *res/sx18027.png   Fkres/sx10768.png   *res/sx10769.png   *   FQ  $   �   �   tb              ��  ��       res/tb01.png     res/tb02.png     res/tb03.png     res/tb04.png     res/tb05.png        (K  D     (   8            ?   �     D   h   h  @�    �  SVX_HID_PPROPERTYPANEL_TEXT_UNDERLINE_VS                 9   xUnderline      F   ^   ^  @�       SVX_HID_UNDERLINE_BTN          }      9   ~Plus d'options...     
  #   H   H            4   4      vsvx/res/symphony/line1.png       #   H   H            4   4      wsvx/res/symphony/line2.png       #   H   H            4   4      xsvx/res/symphony/line3.png       #   H   H            4   4      ysvx/res/symphony/line4.png       #   H   H            4   4      zsvx/res/symphony/line5.png       #   H   H            4   4      {svx/res/symphony/line6.png       #   H   H            4   4      |svx/res/symphony/line7.png       #   H   H            4   4      }svx/res/symphony/line8.png       #   H   H            4   4      ~svx/res/symphony/line9.png       #   H   H            4   4      svx/res/symphony/line10.png    (  #   P   P            <   <      �svx/res/symphony/selected-line1.png    )  #   P   P            <   <      �svx/res/symphony/selected-line2.png    *  #   P   P            <   <      �svx/res/symphony/selected-line3.png    +  #   P   P            <   <      �svx/res/symphony/selected-line4.png    ,  #   P   P            <   <      �svx/res/symphony/selected-line5.png    -  #   P   P            <   <      �svx/res/symphony/selected-line6.png    .  #   P   P            <   <      �svx/res/symphony/selected-line7.png    /  #   P   P            <   <      �svx/res/symphony/selected-line8.png    0  #   P   P            <   <      �svx/res/symphony/selected-line9.png    1  #   R   R            >   >      �svx/res/symphony/selected-line10.png     2        (sans)             Simple             Double             Gras             Pointillé          "   "Pointillé (gras)            Tiret            Tiret long             Point tiret         "   "Point point tiret            Ondulé   (M  D  H   (   8            V   �     D   n   n  @�    �  SVX_HID_PPROPERTYPANEL_TEXT_SPACING_VS                 P   ZCharacter Spacing      W   D   D   �      @          _      H   Personnalisé :      W   R   R   �      @          n      >   Espacement de ~caractères :       U   ~   ~  @?     SVX_HID_SPACING_CB_KERN          x      >   P��   Par défaut     Étentdu      Condensé          W   D   D   �      @          �      >   Modifier ~par :    	  d   h   h  @?     `SVX_HID_SPACING_MB_KERN          �      >          '             
     #   P   P            <   <      �svx/res/symphony/spacing_normal.png       #   T   T            @   @      �svx/res/symphony/spacing_very tight.png    !  #   P   P            <   <      �svx/res/symphony/spacing_tight.png     "  #   P   P            <   <      �svx/res/symphony/spacing_loose.png     #  #   T   T            @   @      �svx/res/symphony/spacing_very loose.png    3  #   R   R            >   >      �svx/res/symphony/spacing_normal_s.png    4  #   V   V            B   B      �svx/res/symphony/spacing_very tight_s.png    5  #   R   R            >   >      �svx/res/symphony/spacing_tight_s.png     6  #   R   R            >   >      �svx/res/symphony/spacing_loose_s.png     7  #   V   V            B   B      �svx/res/symphony/spacing_very loose_s.png    $  #   T   T            @   @      �svx/res/symphony/last_custom_common.png    %  #   Z   Z            F   F      �svx/res/symphony/last_custom_common_grey.png     =        Très serré     >        Serré     ?        Normal     @        Large    A        Très large    B     0   0Dernière valeur personnalisée    C     2   2 Espacement : condensé de : 3 pt    D     4   4 Espacement : condensé de : 1,5 pt    E     &   & Espacement : normal     F     0   0 Espacement : étendu de : 3 pt    G     0   0 Espacement : étendu de : 3 pt    H     .   . Espacement : condensé de :     I     ,   , Espacement : étendu de :     J        pt    (P  D  T                	  W   4   4   �              -   	Centre ~X :    
  d        B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_CENTER_X       -   Spécifiez le pourcentage de décalage horizontal à partir du centre pour le style d'ombrage du dégradé. 50% est le centre horizontal.         d             d        W   4   4   �              4   	Centre ~Y :      d   �   �  B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_CENTER_Y       -   Spécifiez le pourcentage de décalage vertical à partir du centre pour le style d'ombrage du dégradé. 50% est le centre vertical.         d             d        W   2   2   �              d   	~Angle :       d   �   �  B8     ` SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_ANGLE        -   Spécifiez l'angle de rotation du style d'ombrage de dégradé.      ����  '       degrés             W   >   >   �              -   	Valeur de ~départ :       d      B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_SVALUE       -   Saisissez une valeur de transparence pour le point de départ du dégradé, où 0% est l'opacité complète et 100% la transparence complète.          d             d        W   :   :   �              4   	Valeur de ~fin :       d   �   �  B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_EVALUE       -   Saisissez une valeur de transparence pour le point final du dégradé, où 0% est l'opacité complète et 100% la transparence complète.         d             d        W   4   4   �              d   	~Bordure :       d   �   �  B8     `SVX_HID_PPROPERTYPANEL_AREA_MTR_TRGR_BORDER       -   Spécifiez la valeur de bordure de la transparence du dégradé.          d             d        q   �   �  @�      @SVX_HID_PPROPERTYPANEL_AREA_BTN_LEFT_SECOND Rotate Left        �         p           Rotate Left      q   �   �  @�      @SVX_HID_PPROPERTYPANEL_AREA_BTN_RIGHT_FIRST Rotate Right         �         p   "   "   Rotate Right          B   BPivoter de 45 degrés dans le sens anti-horaire.          <   <Pivoter de 45 degrés dans le sens horaire.   (T  D  �   (   8            V   �     D   `   `  @�    �  SVX_HID_PPROPERTYPANEL_LINE_VS_WIDTH                 P   lWidth      W   D   D   �                 q      P   Personnalisé :      W   H   H   �                 �      @   ~Largeur de ligne :      d   �   �  B?     aSVX_HID_PPROPERTYPANEL_LINE_MTR_WIDTH          �      (   Spécifiez la largeur de ligne.      6  �                �   
     #   T   T            @   @      �svx/res/symphony/last_custom_common.png      #   Z   Z            F   F      �svx/res/symphony/last_custom_common_grey.png          0   0Dernière valeur personnalisée    	        pt    (W  D   �                    W   �   �   �                       d   Les propriétés de la tâche que vous réalisez ne sont pas disponibles pour la sélection active    '�  T   D   D  @8    �  SVX_HID_STYLE_LISTBOX       <   V        'b    �  �               B   B   J   Page entière SVX_HID_MNU_ZOOM_WHOLE_PAGE          D   D   J   Largeur de page SVX_HID_MNU_ZOOM_PAGE_WIDTH          D   D   J   Affichage optimal SVX_HID_MNU_ZOOM_OPTIMAL           2   2   J   50 %  SVX_HID_MNU_ZOOM_50          2   2   J   75 %  SVX_HID_MNU_ZOOM_75          4   4   J   100 % SVX_HID_MNU_ZOOM_100           4   4   J   150 % SVX_HID_MNU_ZOOM_150           0   0   H200 % SVX_HID_MNU_ZOOM_200    'c    �  �               2   2   HMoyenne SVX_HID_MNU_FUNC_AVG           6   6   J   NBVAL SVX_HID_MNU_FUNC_COUNT2          4   4   J   NB  SVX_HID_MNU_FUNC_COUNT           6   6   J   Maximum SVX_HID_MNU_FUNC_MAX           6   6   J   Minimum SVX_HID_MNU_FUNC_MIN           4   4   J   	Somme SVX_HID_MNU_FUNC_SUM           R   R   J   Compte de la sélection SVX_HID_MNU_FUNC_SELECTION_COUNT           4   4   J   Aucun SVX_HID_MNU_FUNC_NONE   'd     Z   Z               B   B   HSignatures numériques... SVX_HID_XMLSEC_CALL   'l    �  �               $   $      Millimètre          (   (         Centimètre          $   $         Mètre           (   (         Kilomètre           "   "         Pouce          "   "      	   Pied           "   "      
   Miles          "   "         Point          "   "         Pica                         Car          "   "         Trait   '�     �   �               J   J   
  '�Mettre à jour pour correspondre à la sélection          ,   ,   
  '�Éditer le style...   '�     �   �               ,   ,      Sélection standard          0   0         Sélection étendue          2   2         Sélection avec ajout          0   0         Sélection par bloc   (�    �  �               $   $   Description...           "   "   
   
~Macro...                
   	Actif                          �   �  
   Disposer           �   �               ,   ,   
   Envoyer à l'avant           .   .   
   Envoyer v~ers l'avant          2   2   
   Envoyer ~vers l'arrière           0   0   
   Envoyer à l'a~rrière                           ,   ,   
   ~Tout sélectionner          $   $   
   ~Supprimer    FP    &  &               �   �  J  )�~Nouveau  SVX_HID_FM_NEW           �   �               8   8   J  )�Formulaire  SVX_HID_FM_NEW_FORM          @   @   J  )pContrôle masqué SVX_HID_FM_NEW_HIDDEN          >   >   J  )�Remplacer par .uno:ChangeControlType           *   *  
  N~Couper .uno:Cut           *   *  
  O~Copier .uno:Copy          ,   ,  
  P~Coller .uno:Paste           6   6   J  )�~Supprimer  SVX_HID_FM_DELETE          B   B   J  )wSéquence d'activation... .uno:TabDialog           <   <   J  )q~Renommer SVX_HID_FM_RENAME_OBJECT           @   @   J  )�~Propriétés .uno:ShowPropertyBrowser           B   B   J  )�Ouvrir en mode Ébauche .uno:OpenReadOnly          X   X   J  *Focalisation automatique sur le contrôle .uno:AutoControlFocus   FQ     �   �               D   D   J  )rSupprimer des lignes  SVX_HID_FM_DELETEROWS          D   D   J  )�Enregistrer l'enregistrement  .uno:RecSave           D   D   J  )�Annuler : saisie de données  .uno:RecUndo    FR    �  �              �  �  J  )s~Insérer une colonne SVX_HID_FM_INSERTCOL          �  �               0   0   J  )gZone de texte .uno:Edit          6   6   J  )dCase à cocher  .uno:CheckBox          6   6   J  )iZone combinée  .uno:ComboBox          4   4   J  )hZone de liste .uno:ListBox           6   6   J  )�Champ de date .uno:DateField           6   6   J  )�Champ horaire .uno:TimeField           <   <   J  )�Champ numérique  .uno:NumericField          >   >   J  )�Champ monétaire  .uno:CurrencyField           8   8   J  )�Champ masqué .uno:PatternField          <   <   J  )�Champ formaté  .uno:FormattedField          P   P  J  *Champ de date et heure  SVX_HID_CONTROLS_DATE_N_TIME            >   >   J  )n~Remplacer par  SVX_HID_FM_CHANGECOL           D   D   J  )tSupprimer la colonne  SVX_HID_FM_DELETECOL           @   @   J  *~Masquer la colonne SVX_HID_FM_HIDECOL           �   �  J  *~Afficher les colonnes  SVX_HID_FM_SHOWCOLS          �   �               <   <   J  *~Plus...  SVX_HID_FM_SHOWCOLS_MORE                           6   6   J  *~Tout SVX_HID_FM_SHOWALLCOLS           >   >   J  )�Colonne...  .uno:ShowPropertyBrowser    FS     B   B               *   *  
  O~Copier .uno:Copy   FT    �  �               P   P  J  )�Zone de ~texte  .uno:ConvertToEdit  .uno:ConvertToEdit           L   L  J  )�~Bouton .uno:ConvertToButton  .uno:ConvertToButton           T   T  J  )�~Champ d'étiquette .uno:ConvertToFixed .uno:ConvertToFixed          P   P  J  )�Zone de ~groupe .uno:ConvertToGroup .uno:ConvertToGroup          P   P  J  )�Zone de ~liste  .uno:ConvertToList  .uno:ConvertToList           X   X  J  )�~Case à cocher .uno:ConvertToCheckBox  .uno:ConvertToCheckBox           N   N  J  )�Bouton ~radio .uno:ConvertToRadio .uno:ConvertToRadio          P   P  J  )�Zone ~combinée .uno:ConvertToCombo .uno:ConvertToCombo          V   V  J  )�Bouton ~picto .uno:ConvertToImageBtn  .uno:ConvertToImageBtn           d   d  J  )�~Sélection de fichier  .uno:ConvertToFileControl .uno:ConvertToFileControl          P   P  J  )�Champ de ~date  .uno:ConvertToDate  .uno:ConvertToDate           P   P  J  )�Champ ~horaire  .uno:ConvertToTime  .uno:ConvertToTime           V   V  J  )�Champ ~numérique .uno:ConvertToNumeric .uno:ConvertToNumeric          Z   Z  J  )�Champ ~monétaire .uno:ConvertToCurrency  .uno:ConvertToCurrency           T   T  J  )�Champ ~masqué  .uno:ConvertToPattern .uno:ConvertToPattern          b   b  J  )�Contrôle ~picto  .uno:ConvertToImageControl  .uno:ConvertToImageControl           X   X  J  )�~Champ formaté .uno:ConvertToFormatted .uno:ConvertToFormatted          ^   ^  J  *Barre de défilement  .uno:ConvertToScrollBar .uno:ConvertToScrollBar          V   V  J  *Compteur  .uno:ConvertToSpinButton  .uno:ConvertToSpinButton           d   d  J  *Barre de navigation .uno:ConvertToNavigationBar .uno:ConvertToNavigationBar   FU     �   �               6   6   J  )�~Supprimer  SVX_HID_FM_DELETE          2   2   J  *~Éditer  SVX_HID_FM_EDIT          <   <   J  *~Est vide SVX_HID_FM_FILTER_IS_NULL          @   @   J  *~N'est pas vide SVX_HID_FM_IS_NOT_NULL    FV    �  �               2   2  
  'Police  .uno:CharFontName          0   0  
  'Taille  .uno:FontHeight         "  "  J  FWSt~yle  SVX_HID_MENU_FM_TEXTATTRIBUTES_STYLE          �  �      
         (   (  
  'Gras  .uno:Bold          .   .  
  'Italique  .uno:Italic          2   2  
  -0Surlignage  .uno:Overline          4   4  
  'Soulignage  .uno:Underline           0   0  
  'Barré  .uno:Strikeout           ,   ,  
  'Ombre .uno:Shadowed          2   2  
  'Contour .uno:OutlineFont                           4   4  
  (6Ex~posant .uno:SuperScript           0   0  
  (7In~dice .uno:SubScript          >  >  N  FX   Ali~gnement SVX_HID_MENU_FM_TEXTATTRIBUTES_ALIGNMENT           �   �               2   2    ',   ~Gauche .uno:LeftPara          4   4    '-   D~roite .uno:RightPara           6   6    '.   ~Centré  .uno:CenterPara          8   8    '/   Justifié .uno:JustifyPara              J  FYInter~ligne SVX_HID_MENU_FM_TEXTATTRIBUTES_SPACING           �   �               4   4    '2   Simple  .uno:SpacePara1          8   8    '3   1,5 ligne .uno:SpacePara15           4   4    '4   ~Double .uno:SpacePara2   FZ    �  �               N   N   J   
Ajouter un élément  SVX_HID_XFORMS_TOOLBOX_ITEM_ADD          V   V   J   Ajouter un élément  SVX_HID_XFORMS_TOOLBOX_ITEM_ADD_ELEMENT          V   V   J   Ajouter un attribut SVX_HID_XFORMS_TOOLBOX_ITEM_ADD_ATTRIBUTE          B   B   J   Éditer SVX_HID_XFORMS_TOOLBOX_ITEM_EDIT                           F   F   J   Supprimer SVX_HID_XFORMS_TOOLBOX_ITEM_REMOVE     �          �     � �,    �        (1  #�    (n  )�    (o  2�    *d  4�    FP  8    FQ  8d       :�      :�      ;0      ;P      ;j      ;�      ;�      ;�      ;�    	  ;�    
  <
      <$      <>    
   <V    
  <v    
  <�    
  <�    
  <�    
  =     
  =(    
  =@    
  =Z    
	  =p    

  =�    
  =�    
  =�    
  =�    
  >    
  >     
  >8    
  >P    
  >r    
  >�    
  >�    
  >�    
  >�    
  ?
    
  ?*    
  ?L    
  ?v    
  ?�    
  ?�    
  ?�    
  ?�    
  @    
   @6    
!  @Z    
"  @x    
#  @�    
$  @�    
%  @�    
&  @�    
'  A    
(  A2    
)  AV    
*  At    
+  A�    
,  A�    
-  A�    
.  A�    
/  B    
0  B6    
1  BP    
2  Bz    
3  B�    
4  B�    
5  B�    
6  B�    
7  C"    
8  CH    
9  Cn    
:  C�    
;  C�    
<  C�    
=  C�    
>  D    
?  D:    
@  D\    
A  D�    
B  D�    
C  D�    
D  D�    
E  E
    
F  E.    
G  ET    
H  E�    
I  E�    
J  E�    
K  F,    
L  FL    
M  Fl    
N  F�    
O  F�    
P  F�    
Q  F�    
R  F�    
S  G    
T  G8    
U  G`    
V  G�    
W  G�    
X  G�    
Y  G�    
Z  H    
[  H:    
\  HR    
]  Hj    
^  H�    
_  H�    
`  H�    
a  H�    
b  H�    
c  I    
d  IF    
e  Ip    
f  I�    
g  I�    
h  I�    
i  I�    
j  J    
k  J,    
l  JR    
m  Jx    
n  J�    
o  J�    
p  J�    
q  J�    
r  K    
s  K4    
t  KV    
u  K|    
v  K�    
w  K�    
x  K�    
y  K�    
z  L    
{  L    
|  L>    
}  Lb    
~  L|    
  L�    
�  L�    
�  L�    
�  L�    
�  M    
�  M.    
�  ML    
�  Mj    
�  M�    
�  M�    
�  M�    
�  M�    
�  N    
�  NN    
�  Nd    
�  N|    
�  N�    
�  N�    
�  N�    
�  N�    
�  O    
�  O    
�  O8    
�  Op    
�  O�    
�  O�    
�  O�    
�  P    
�  P:    
�  Pb    
�  P�    
�  P�    
�  P�    
�  P�    
�  Q    
�  QF    
�  Qn    
�  Q�    
�  Q�    
�  Q�    
�  R     
�  R    
�  RT    
�  R�    
�  R�    
�  R�    
�  S    
�  SL    
�  Sh    
�  S�    
�  S�    
�  S�    
�  T    
�  T<    
�  Tf    
�  T�    
�  T�    
�  T�    
�  T�    
�  U    
�  UD    
�  Uj    
�  U�    
�  U�    
�  U�    
�  U�    
�  V    
�  V.    
�  VJ    
�  Vf    
�  V�    
�  V�    
�  V�    
�  V�    
�  W     
�  WJ    
�  Wh    
�  W�    
�  W�    
�  W�    
�  X    
�  X.    
�  XV    
�  Xp    
�  X�    
�  X�    
�  X�    
�  Y     
�  YV    
�  Y�    
�  Y�    
�  Y�    
�  Y�    
�  Z    
�  ZB    
�  Zn    
�  Z�    
�  Z�    
�  Z�    
�  [    
�  [6    
�  [T    
�  [|    
�  [�    
�  [�    
�  [�    
�  \    
�  \2    
�  \N    
�  \|    
�  \�    
�  \�    
�  ]    
�  ]6    
�  ]R    
�  ]r    
�  ]�    
�  ]�    
�  ]�    
�  ^*    
�  ^T    
�  ^�    
�  ^�    
�  _    
�  _"    
�  _>    
�  _X    
�  _�       _�      _�      _�      `      `@      `r      `�      `�      a>    	  ad    
  a�      a�      a�      b
      b:      bp      b�      b�      b�      b�      b�      b�      c      c.      cF      cb      c|      c�      c�      c�      d      d>       dT    !  dl    "  d�    #  d�    $  d�    %  d�    &  d�    '  e    (  e<    )  eT    *  ep    +  e�    ,  e�    -  e�    .  e�    /  e�    0  f    1  f<    2  fX    3  f|    4  f�    5  f�    6  f�    7  g    8  g    9  g4    :  gT    ;  g~    <  g�    =  g�    >  g�    ?  g�    @  h"    A  h:    B  hX    C  hz    D  h�    E  h�    F  h�    G  h�    H  i     I  iN    J  it    K  i�    R  i�    S  i�    T  i�    U  j
    V  j"    W  jF    X  jr    Y  j�    Z  j�    [  j�    c  k
    d  k(    e  kR    f  k|    g  k�    h  k�    i  l    j  l>    k  lh    l  l�    m  l�    n  l�    o  m     p  mL    q  mv    r  m�    s  m�    t  m�    u  m�    v  n    w  n.    x  nL    y  nf    z  n�    {  n�    |  n�    }  n�    ~  n�      n�    �  n�    �  o    �  o8    �  o`    �  o�    �  o�    �  o�    �  o�    �  p     �  p"    �  p>    �  pZ    �  pr    �  p�    �  p�    �  p�    �  p�    �  q    �  q4    �  qX    �  qz    �  q�    �  q�    �  q�    �  r    �  rF    �  rn    �  r�    �  r�    �  r�    �  s    �  s0    �  sX    �  s�    �  s�    �  s�    �  s�    �  t    �  t$    �  tJ    �  th    �  t�    �  t�    �  t�    �  u$    �  uV    �  u|    �  u�    �  u�    �  v    �  v>    �  vb    �  v�    �  v�    �  v�    �  w    �  w*    �  wR    �  w�    �  w�    �  w�    �  x    �  x:    �  xh    �  x�    �  x�    �  x�    �  x�    �  y    �  y:    �  y\    �  y|    �  y�    �  y�    �  z    �  z.    �  z\    �  z�    �  z�    �  z�    �  {    �  {.    �  {D    �  {d    �  {�    �  {�    �  {�    �  {�    �  |    �  |8    �  |d    �  |�    �  |�    �  |�    �  }     �  }P    �  }~    �  }�    �  ~    �  ~     �  ~J    �  ~v    �  ~�    �  ~�    �      �  8    �  n    �  �    �  �    �  �    �  �    �  �B       �n      ��      ��      ��      �      �<      �d      ��      ��    	  ��    
  �      �P      ��      ��      ��      �
      �.      �\      ��      ��      ��      �      �B      �r      ��      ��    $  ��    %  �    &  �:    '  �l    (  ��    )  ��    *  ��    +  �&    ,  �Z    -  ��    .  ��    /  �    0  �:    1  �\    2  ��    3  ��    4  ��    5  �D    6  ��    7  ��    8  �    9  �6    :  �`    ;  ��    =  ��    >  ��    ?  ��    @  �    A  �:    B  �b    C  ��    D  ��    E  ��    F  ��    G  �(    H  �J    I  �t    J  ��    K  ��    L  ��    M  ��    N  �    O  �8    P  �`    Q  ��    R  ��    S  ��    T  ��    U  �    V  �B    W  �t    X  ��    Y  ��    Z  ��    [  �    \  �D    ]  �t    ^  ��    _  ��    `  ��    a  �    b  �L    c  �z    d  ��    e  ��    f  ��    g  �"    h  �D    i  �h    j  ��    k  ��    l  ��    m  �     n  �2    o  �N    p  �x    q  ��    r  ��    s  ��    t  ��    u  �$    v  �F    w  �b    x  �~    y  ��    z  ��    {  ��    |  ��    }  ��    ~  �      �8    �  �f    �  ��    �  ��    �  ��    �  ��    �  �    �  �<    �  �`    �  �|    �  ��    �  ��    �  ��    �  ��    �  �    �  �    �  �.    �  �@    �  �R    �  �d    �  ��    �  ��    �  ��    �  �    �  �.    �  �R    �  �x    �  ��    �  ��    �  ��    �  �    �  �4    �  �f    �  ��    '  ��    'W  ��    'Y  �    'Z  �0    '[  �B    '\  �T    ']  �f    '^  �z    '_  ��    '`  ��    'a  ��    'b  ��    'c  �    'd  �N    'e  �b    'h  ��    'i  ��    'j  �0    '�  �^    '�  �v    '�  ��    '�  ��    '�  ��    '�  ��    '�  ��    '�  �    '�  �.    '�  �P    '�  �f    '�  ��    '�  ��    '�  ��    '�  ��    '�  �    '�  �<    '�  ��    '�  ��    '�  ��    '�  ��    '�  �$    '�  �N    '�  �p    '�  ��    '�  ��    '�  ��    '�  �*    '�  �v    '�  ��    '�  ��    '�  �Z    '�  �    '�  �X    '�  ��    (  �    (  �2    (  �J    (  �b    (
  �|    (  ��    (  ��    (  ��    (  ��    (#  �    ($  �*    (%  �@    (&  �V    ('  �l    ((  ��    (*  ��    (1  ��    (2  ��    (3  ��    (4  ��    (5  �    (6  �(    (7  �B    (8  �b    (<  ��    (=  ��    (>  ��    (A  ��    (B  ��    (C  ��    (D  �    (E  �0    (�  �H    (�  �d    (�  ��    (�  ��    (�  ��    (�  ��    (�  �    (�  �<    (�  �`    (�  ��    (�  ��    (�  ��    )   ��    )  �&    )  �V    )  �~    )  ��    )  ��    )  ��    )  ��    )  �    )  �8    )  �d    )  ��    )   ��    )!  ��    )"  �    )#  �<    ),  �Z    )-  �r    ).  ��    )/  ��    )0  ��    )1  ��    )2  ��    )4  �    )5  �J    )6  �f    )7  ��    )8  ��    )9  ��    ):  ��    )@  ��    )A  �    )B  �    )C  �6    )D  �L    )E  �d    )F  �|    )G  ��    )H  ��    )I  ��    )J  ��    )K  �    )L  �$    )M  �<    )N  �R    )O  �l    )Z  ��    )[  ��    )\  ��    )]  ��    )^  �$    )_  �R    )`  �~    )c  ��    )d  ��    )e  ��    )f  ��    )g  �     )h  �    )j  �,    )q  �B    )r  �Z    )s  �p    )t  ��    )u  ��    )v  ��    )w  ��    )x  ��    )y  �    )z  �(    ){  �@    )|  �Z    )}  �n    )~  ��    )  ��    )�  ��    )�  ��    )�  ��    )�  �     )�  �    )�  �:    )�  �P    )�  �f    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �&    )�  �D    )�  �d    )�  ��    )�  ��    )�  ��    )�  ��    )�  �
    )�  �2    )�  �T    )�  �r    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �4    )�  �V    )�  �|    )�  ��    )�  ��    )�  ��    )�  �
    )�  �0    )�  �X    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �.    )�  �P    )�  �p    )�  ��    )�  ��    )�  �    )�  �0    )�  �`    )�  �~    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �
    )�  �     )�  �6    )�  �J    )�  �b    )�  �x    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �&    )�  �<    )�  �T    )�  �j    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �$    )�  �<    )�  �Z    )�  �r    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �     )�  �    )�  �@    )�  �\    )�  �t    )�  ��    )�  ��    )�  ��    )�  ��    )�  ��    )�  �"    )�  �>    )�  �b    )�  ��    )�  ��    )�  ��    )�  ��    )�  �    )�  �,    *   �B    *  �^    *  �~    *  ��    *  ��    *  ��    *  ��    *  �    *  �8    *	  �X    *
  �z    *  ��    *  ��    *  ��    *2  ��    *3  �    *4  �r    *5  ��    *D  ��    *E  ��    *F  ��    *G  ��    *H  �    *I  �    *J  �6    *K  �P    *L  �f    *Y  ��    *^  ��    *_  ��    *`  ��    *a  �,    *b  �\    *c  �z    *m  ��    *n  ��    *o  ��    *p  �
    *q  �     *r  �6    *s  �P    *t  �j    *u  ��    *v  ��    *w  ��    *�  ��    *�  ��    *�  �&    *�  �V    *�  ��    *�  ��    *�  ��    *�  ��    *�  �    *�  �*    *�  �N    *�  �p    *�      *�  ´    *�  ��    *�  ��    *�  �    *�  �"    *�  �>    *�  �Z    *�  �v    *�  Ò    *�  ø    *�  ��    *�  �    *�  �4    *�  �r    *�  Ē    *�  Ĭ    *�  ��    *�  ��    *�  �     *�  �    *�  �.    *�  �D    *�  �\    *�  �t    *�  Ő    *�  Ū    *�  ��    *�  ��    +n  ��    +o  �"    +p  �l    +r  ƞ    +s  ��    +t  �    +u  �@    +v  �f    +w  ǎ    +x  Ƕ    +y  ��    +z  �    +{  �B    +|  �h    +}  Ȑ    +~  ��    +�  ��    +�  �"    +�  �T    +�  �|    +�  ɬ    +�  ��    +�  �"    +�  �J    +�  ʂ    +�  ʤ    +�  ��    +�  ��    +�  �.    +�  �X    +�  ˂    +�  ˰    +�  ��    +�  �J    +�  ̔    +�  �    +�  �     +�  ͜    +�  �    +�  �N    +�  ��    +�  �    +�  �0    +�  �p    +�  ϔ    +�  ϰ    +�  �    +�  �h    +�  В    +�  к    +�  ��    ,  �*    ,  �T    ,  ф    ,  Ѱ    ,  ��    ,  �    ,  Ҭ    ,  �    ,  �4    ,  ��    ,  ��    ,  �    ,   �2    ,!  �J    ,"  �f    ,#  �|    ,$  Ԙ    ,%  ԰    ,&  ��    ,'  ��    ,(  ��    ,)  �    ,*  �4    ,+  �V    ,8  Պ    ,9  հ    ,:  ��    ,;  ��    ,<  �    ,=  �<    ,>  �Z    ,?  �~    ,@  ֜    ,B  ־    ,C  ��    ,D  ��    ,E  �    ,F  �(    ,h  �B    ,i  �f    ,j  ׀    ,k  פ    ,l  ��    ,v  ��    ,w  �    ,x  �    ,y  �.    ,z  �D    ,{  �Z    ,|  �p    ,}  ؎    ,~  خ    .�  ��    .�  ��    .�  �    .�  �2    .�  �V    .�  �z    .�  ٜ    .�  ��    .�  ��    .�  �    .�  �<    .�  �X    .�  �t    .�  ڐ    .�  ڬ    .�  ��    .�  ��    .�  �     .�  �    .�  �:    .�  �Z    .�  �z    .�  ۘ    .�  ۼ    .�  ��    .�  ��    .�  �    .�  �D    .�  �j    .�  ܎    .�  ܸ    .�  ��    /   ��    /  �$    /  �L    /  �t    /  ݚ    /  ��    /  ��    /  �
    /  �*    /	  �L    /
  �n    /  ގ    /  ޴    /  ��    /  ��    /  �
    /  �(    /  �F    /  �b    /  ߄    /  ߢ    /  ߼    /  ��    /  ��    /  �    /  �8    /  �\    /  �|    /  ��    /  �    /  ��    /  ��    /   �    /!  �*    /"  �H    /#  �f    /$  �    /%  �    /&  �    /'  ��    /(  ��    /)  �    /*  �0    /+  �L    /,  �j    /-  �    /.  �    //  �    /0  ��    /1  ��    /2  �
    /3  �"    /4  �:    /5  �V    /6  �r    /7  �    /D  �    /E  ��    /F  ��    /G  �    /H  �6    /I  �X    /J  �|    /K  �    /L  ��    /M  ��    /N  �$    /O  �B    /P  �`    /Q  �~    /R  �    /S  �    /T  ��    /U  ��    /V  �    /W  �4    /X  �T    /Y  �v    /Z  �    /[  �    /\  ��    /]  �     /^  �*    /_  �T    /`  �    /a  �    /b  ��    /c  �    /d  �,    /e  �V    /f  �    /g  �    /h  ��    /i  �    /j  �4    /k  �P    /l  �p    /m  �    /n  �    /o  ��    /p  ��    /q  �    /r  �4    /s  �P    /t  �l    /u  �    /v  �    /w  ��    /x  ��    /y  �    /z  �     /{  �>    /|  �^    /}  �~    /~  �    /  ��    /�  ��    /�  �    /�  �$    /�  �D    /�  �d    /�  �z    /�  �    /�  �    /�  ��    /�  ��    /�  �    /�  �(    /�  �H    /�  �f    /�  �    /�  �    /�  ��    /�  ��    /�  ��    /�  �
    /�  �&    /�  �>    /�  �Z    /�  �r    /�  �    /�  �    /�  ��    /�  ��    1�  �    1�  �.    1�  �H    1�  �d    1�  �    1�  �    1�  ��    1�  ��    1�  �    1�  �&    1�  �>    1�  �V    1�  �l    1�  ��    1�  �    1�  ��    1�  ��    1�  �
    1�  �.    1�  �N    1�  �t    1�  �    1�  �    1�  ��    1�  ��    1�  �    1�  �.    1�  �V    1�  �z    1�  �    2   ��    2  ��    2  ��    2  �    2  �2    2  �V    2  �z    2  �    2  �    2	  ��    2
  ��    2  �$    :h  �L    :p  �n    :q  �    :r  ��    :s  ��    :t  ��    :u  �    :v  �,    :w  �L    :z  �j    :{  ��    :|  ��    :  ��    :�  ��    :�  ��    :�  �     :�  �    :�  �4    :�  �J    :�  �f    :�  ��    :�  ��    :�  ��    :�  ��    :�  ��    :�  �    :�  �&    :�  �:    <   �P    <  �x    <  ��    <  ��    <!  ��    <"  ��    <#  �    <$  �(    <%  �P    <&  ��    <F  ��    <G  ��    <H  ��    <I  �    <X  �<    <Y  �f    <Z  ��    <[  ��    <\  ��    <]  ��    <^  ��    <_  �    <`  �.    <a  �H    <b  �b    <c  �|    <d  ��    <o  ��    <p  ��    <q  ��    <r  �    <s  �.    <u  �R    C  �~    C  ��    FQ  ��    FR  �    FS  �h    FT  ��    FU  ��    FV  ��    FW  �R    FX  ��    FY  �\    F[      F\  *    F]  J    F^  ^    F_  �    F`  �    Fk  �    Fl  �    Fn *    Fo R    Fp z    Ft �    Fu �    Fv �    Fw     Fx ,    Fy X    Fz �    F{ �    F| �    F} 6    F~ �    F �    F� �    F�     F�     F� 4    F� X    F� p    F� �    F� �    F� �    F� �    F�     F� .    F� N    F� l    F� �    F� �    F� �    F� �    F�     F� 2    F� P    F� t    F� �    F� �    F� �    F�     F�     F� n    F� �    F� �    F� �    F� �    F� (    F� �    F� 	<    F� 	X    F� 	�    F� 	�    F� 
    F� 
\    F� 
�    F�     F� p    F� �    F� �    F� $    F� >    F� `    F� z    F� �    F� �    F� �    F� �    F�     F�     F� 4    F� N    F� h    F� �    F� �    F� �    F� �    F� �    F�     F� 2    F� P    F� v    F� �    F� �    F� �    F�     F� .    F� R    F� v    F� �    F� �    F� �    F�      F� Z    F� �    F� �    F�      F� X    H� �    H� �    H�     H� R    � "�    � "�    � #    � #:    � #n    � #�    '� #�    '� #�    '� $     '� $H    'b m�    'c o�    'd qn    'l q�    '� sd    '� s�    (� t�    FP v�    FQ y�    FR z�    FS �    FT �    FU ��    FV ��    FZ �|  #  'S $t  #  'T $�  #  'U %<  #  'e %�  #  'f %�  #  'g &J  #  'i &�  #  'k '
  #  '� 'n  #  '� '�  #  '� (   #  '� (|  #  ( (�  #  ( )   #  (� )|  #  (� )�  #  (� *(  #  (� *~  #  (� *�  #  (� +*  #  (� +�  #  (� +�  #  (� ,,  #  (� ,�  #  (� ,�  #  (� -R  #  (� -�  #  (� .  #  (� .�  #  )$ .�  #  )% /D  #  )& /�  #  )' /�  #  +� 0Z  #  +� 0�  #  +� 1&  #  +� 1�  #  +� 1�  #  +� 2>  #  +� 2�  #  +� 2�  #  ,  3  #  , 3f  #  , 3�  #  , 3�  #  , 4>  #  , 4�  #  , 4�  #  , 5  #  , 5^  #  ,	 5�  #  ,
 5�  #  , 6>  #  , 6�  #  , 6�  #  , 7  #  , 7j  #  , 7�  #  , 7�  #  ,, 8$  #  ,- 8�  #  ,. 8�  #  ,/ 9X  #  ,0 9�  #  ,1 :$  #  ,2 :�  #  ,3 :�  #  ,4 ;T  #  ,6 ;�  #  ,7 <  #  ,G <|  #  ,H <�  #  ,I =L  #  ,J =�  #  ,L >  #  ,M >�  #  ,N >�  #  ,O ?X  #  ,Q ?�  #  ,R @.  #  ,S @�  #  ,T @�  #  ,V Ab  #  ,W A�  #  ,X B4  #  ,Y B�  #  ,[ C  #  ,\ Cn  #  ,] C�  #  ,^ D8  #  ,_ D�  #  ,` E   #  ,a Ed  #  ,b E�  #  ,c F4  #  ,e F�  #  ,f F�  #  ,g G^  #  ,m G�  #  ,n H"  #  ,o H�  #  ,p H�  #  ,q Ib  #  ,r I�  #  ,s J
  #  ,t JH  #  ,u J�  $    J�  $  'Q Lr  $  'R M�  $  FP NV  $  FQ P�  D  (K Qx  D  (M Y|  D  (P a�  D  (T j  D  (W l�  T  '� mr  y  (G �  y  (H \  y  <v n  N4