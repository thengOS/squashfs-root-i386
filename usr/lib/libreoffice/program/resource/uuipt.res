  8�    �            B   BA operação executada em $(ARG1) foi cancelada.          ,   ,Acesso a $(ARG1) recusado.          $   $$(ARG1) já existe.    1     &   &O destino já existe.    :    �  �Vai guardar/exportar uma biblioteca básica, protegida por palavra-passe e que contém o(s) módulo(s)
$(ARG1)
que são muito grandes para guardar no formato binário. Se quiser que utilizadores que não possuam a palavra-passe da biblioteca executem macros nestes módulos, tem que os dividir em módulos mais pequenos. Pretende continuar a guardar/exportar esta biblioteca?         N   NA soma de verificação dos dados de $(ARG1) está incorreta.         P   PNão é possível criar o objeto $(ARG1) no diretório $(ARG2).         <   <Não foi possível ler os dados de $(ARG1).         L   LNão foi possível executar a operação "seek" em $(ARG1).         L   LNão foi possível executar a operação "tell" em $(ARG1).         D   DNão foi possível escrever os dados para $(ARG1).     	     D   DAção impossível: $(ARG1) é o diretório atual.     
     .   .$(ARG1) não está preparado.         `   `Ação impossível: $(ARG1) e $(ARG2) são dispositivos diferentes (unidades).          B   BErro geral de entrada/saída ao aceder a $(ARG1).         V   VA tentativa de acesso a $(ARG1) foi executada de uma forma inválida.         8   8$(ARG1) contém caracteres inválidos.          >   >O dispositivo (unidade) $(ARG1) é inválido.         D   DOs dados de $(ARG1) têm um comprimento inválido.          R   RA operação em $(ARG1) foi iniciada com um parâmetro inválido.         h   hNão é possível executar a operação, porque $(ARG1) contém caracteres universais.          <   <Erro durante o acesso partilhado a $(ARG1).         >   >$(ARG1) contém caracteres mal posicionados.          :   :O nome $(ARG1) contém muitos caracteres.         &   &$(ARG1) não existe.          0   0O caminho $(ARG1) não existe.          V   VEste sistema operativo não tem suporte para a operação em $(ARG1).         0   0$(ARG1) não é um diretório.          .   .$(ARG1) não é um ficheiro.          B   BNão existe espaço livre no dispositivo $(ARG1).         p   pNão é possível executar a operação em $(ARG1) porque já existem muitos ficheiros abertos.         l   lNão é possível executar a operação em $(ARG1) porque não existe memória disponível.         d   dNão é possível executar a operação em $(ARG1) porque existem dados pendentes.          D   DNão é possível copiar $(ARG1) para si próprio.           J   JErro desconhecido de entrada/saída ao aceder a $(ARG1).     !     8   8$(ARG1) está protegido contra escrita.    "     8   8$(ARG1) não está no formato correto.     #     6   6A versão de $(ARG1) está incorreta.    $     0   0A unidade $(ARG1) não existe.     %     .   .A pasta $(ARG1) não existe.     &     <   <A versão Java instalada não é suportada.    '     D   DA versão Java instalada $(ARG1) não é suportada.    (     l   lA versão Java instalada não é suportada pois a versão mínima necessária é a $(ARG1).    )     r   rA versão Java instalada $(ARG1) não é suportada pois a versão mínima necessária é $(ARG2).    *     D   DOs dados associados à parceria estão danificados.    +     L   LOs dados associados à parceria $(ARG1) estão danificados.    ,     8   8O volume $(ARG1) não está preparado.     -     X   X$(ARG1) não está preparado; introduza outro suporte de armazenamento.    .     b   bA unidade $(ARG1) não está preparada; introduza outro suporte de armazenamento.    /     ,   ,Introduza o disco $(ARG1).     0     H   HNão é possível criar o objeto no diretório $(ARG1).    2     �   �Ao utilizar este protocolo de transmissão, o %PRODUCTNAME não consegue impedir que os ficheiros sejam substituídos. Continuar?    3    �  �O ficheiro "$(ARG1)" está danificado e não pode ser aberto. O %PRODUCTNAME pode tentar reparar o ficheiro.

O ficheiro pode ter ficado danificado devido à manipulação do documento ou a danos estruturais durante a transmissão de dados.

Não deve confiar no conteúdo dos documentos reparados.
A execução de macros está desativada para este documento.

Deseja que o %PRODUCTNAME repare o ficheiro?
     4     `   `Não foi possível reparar o ficheiro "$(ARG1)", pelo que não pode ser aberto.    5     �   �Os dados de configuração em "$(ARG1)" estão danificados. Sem estes dados, algumas funções poderão não funcionar corretamente.
Pretende continuar com a inicialização do %PRODUCTNAME sem os dados de configuração danificados?    6        O ficheiro de configuração "$(ARG1)" está danificado e tem de ser eliminado para continuar. Pode perder algumas das suas definições.
Pretende continuar com a inicialização do %PRODUCTNAME sem os dados de configuração danificados?     7     �   �A origem de dados de configuração "$(ARG1)" não está disponível. Sem estes dados, algumas funções poderão não funcionar corretamente.     8      A origem de dados de configuração "$(ARG1)" não está disponível. Sem estes dados, algumas funções poderão não funcionar corretamente.
Pretende continuar com a inicialização do %PRODUCTNAME sem os dados de configuração em falta?     9     L   LO formulário contém dados inválidos. Pretende continuar?    ;     �   �O ficheiro $(ARG1) está bloqueado por outro utilizador. Atualmente, não é possível conceder acesso de escrita a este ficheiro.     <     �   �O ficheiro $(ARG1) está bloqueado por si. Atualmente, não é possível conceder outro acesso de escrita a este ficheiro.     =     J   JO ficheiro $(ARG1) não está bloqueado pelo utilizador.     >        O anterior bloqueio do ficheiro $(ARG1) terminou.
Isto pode ocorrer devido a problemas no servidor que gere o bloqueio do ficheiro. Não é possível garantir que as operações de escrita neste ficheiro não substituirão as alterações feitas por outros utilizadores!    a�     �   �Não é possível verificar a identidade do sítio $(ARG1).

Antes de aceitar este certificado, deve-o examinar cuidadosamente. Está disposto a aceitar este certificado para efeitos de identificação do sítio web $(ARG1)?    a�     �   �$(ARG1) é um sítio que utiliza um certificado de segurança para encriptar dados durante a transmissão, mas o respetivo certificado expirou em $(ARG2).

Verifique se a hora do seu computador está correta.    a�    �  �Tentou estabelecer uma ligação a $(ARG1). Contudo, o certificado de segurança existente pertence a $(ARG2). É possível, embora improvável, que alguém possa estar a tentar intercetar a comunicação com este sítio web.

Se desconfia que o certificado apresentado não pertence a $(ARG1), cancele a ligação e notifique o administrador do sítio.

Pretende continuar?    a�     �   �Não foi possível validar o certificado. Deve examinar cuidadosamente o certificado deste sítio.

Se desconfia do certificado apresentado, cancele a ligação e notifique o administrador do sítio.   a�     P   PAviso de segurança: existe uma disparidade no nome do domínio   a�     H   HAviso de segurança: o certificado de servidor expirou    a�     L   LAviso de segurança: o certificado de servidor é inválido    ?     �   �O componente não foi carregado, possivelmente por estar danificado ou erro na instalação.
Mensagem de erro:

$(ARG1).    8�     @   @Memori~zar palavra-passe até ao fim da sessão   8�     *   *Memori~zar palavra-passe    8�     @   @A palavra-passe de confirmação não é igual.   8�     8   8A palavra-passe principal está errada.   8�     .   .A palavra-passe está errada.   8�     R   RA palavra-passe está errada. Não é possível abrir o ficheiro.   8�     V   VA palavra-passe está errada. Não é possível modificar o ficheiro.   8�     (   (Utilizador desconhecido   8�     *   *Documento em utilização   8�     �   �O ficheiro do documento "$(ARG1)" está bloqueado para edição por:

$(ARG2)

Abra o documento no modo de leitura ou abra uma cópia do documento para edição.

   8�     ,   ,Abrir como só de leitu~ra    8�        Abrir ~cópia   8�     8   8O documento foi alterado por terceiros    8�     �   �O ficheiro foi alterado desde que foi aberto para edição no %PRODUCTNAME. Se guardar a sua versão do documento, irá substituir as alterações efetuadas por terceiros.

Pretende continuar?

    8�        ~Sim, guardar   8�     *   *Documento em utilização   8�     �   �Desde $(ARG2) que o documento "$(ARG1)" está bloqueado para edição por si mas num sistema diferente.

Abra o documento como só de leitura ou ignore o bloqueio do ficheiro e abra o documento para edição.

    8�     ,   ,Abrir como só de leitu~ra    8�        ~Abrir    8�     2   2Documento não pode ser bloqueado   8�     �   �O ficheiro não pôde ser bloqueado pelo %PRODUCTNAME para acesso exclusivo, pois falta a permissão para criar um ficheiro de bloqueio no local do ficheiro.   8�     6   6Não voltar a mostrar e~sta mensagem    8�     *   *Documento em utilização   8�     �   �O ficheiro de documento "$(ARG1)" está bloqueado para edição por:

$(ARG2)

Tente guardar o documento mais tarde ou crie uma cópia desse documento.

   8�     "   "Tenta~r novamente   8�     "   "~Guardar como...    8�     �   �Desde $(ARG2) que o ficheiro de documento "$(ARG1)" está bloqueado para edição por si, mas num sistema diferente

Feche o documento no outro sistema e tente guardar ou ignore o bloqueio do ficheiro e guarde o documento.

    8�     "   "Tenta~r novamente   8�        ~Guardar    8�     (   (Fluxos não encriptados   8�     2   2Assinatura de documento inválida   8�     D   DIntroduza a palavra-passe para abrir o ficheiro: 
    8�     H   HIntroduza a palavra-passe para modificar o ficheiro: 
    8�     �   �Já existe um ficheiro com o nome "%NAME" na localização "%FOLDER".
Escolha Substituir para substituir o ficheiro existente ou introduza um novo nome.    8�     x   xJá existe um ficheiro com o nome "%NAME" na localização "%FOLDER".
Por favor introduza um novo nome.   8�     8   8Por favor, introduza um nome diferente!   8�     ,   ,Introduza a palavra-passe:    8�     ,   ,Confirme a palavra-passe:     8�     &   &Definir palavra-passe   8�     *   *Introduza a palavra-passe   8�     �   �A palavra-passe de confirmação não é igual à palavra-passe. Defina a palavra-passe, introduzindo a mesma palavra-passe em ambas as caixas.    �           +     �  *     8�        8�  �    8�  �    8�  "    8�  b    8�  �    8�  �    8�      8�  p    8�  �    8�  �    8�   v    8�   �    8�   �    8�   �    8�  !�    8�  !�    8�  "    8�  "�    8�  #$    8�  #<    8�  #n    8�  $    8�  $R    8�  $|    8�  %&    8�  %H    8�  %j    8�  &\    8�  &~    8�  &�    8�  &�    8�  &�    8�  '6    8�  '~    8�  ((    8�  (�    8�  (�    8�  )    8�  )0    8�  )V    8�  )�  