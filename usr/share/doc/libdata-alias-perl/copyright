Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Data-Alias
Upstream-Contact: Andrew Main (Zefram) <zefram@fysh.org>
Source: https://metacpan.org/release/Data-Alias

Files: *
Copyright: 2010-2015, Andrew Main (Zefram) <zefram@fysh.org>
 2003-2007, Matthijs van Duin
 Parts from perl, which is Copyright (C) 1991-2013 Larry Wall and others
License: Artistic or GPL-1+

Files: inc/Module/*
Copyright: 2002-2011, Adam Kennedy <adamk@cpan.org>
 2002-2011, Audrey Tang <autrijus@autrijus.org>
 2002-2011, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

Files: t/lib/Test/More.pm t/lib/Test/Simple.pm
Copyright: 2001-2008, Michael G Schwern <schwern@pobox.com>
License: Artistic or GPL-1+

Files: t/lib/Test/Builder.pm
Copyright: 2002-2008, chromatic <chromatic@wgz.org>
 2002-2008, Michael G Schwern <schwern@pobox.com>
License: Artistic or GPL-1+

Files: debian/*
Copyright: 2010-2011, Jonathan Yu <jawnsy@cpan.org>
 2009, Ryan Niebur <ryan@debian.org>
 2011-2015, gregor herrmann <gregoa@debian.org>
 2011, Jotam Jr. Trejo <jotamjr@debian.org.sv>
 2011, Fabrizio Regalli <fabreg@fabreg.it>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
