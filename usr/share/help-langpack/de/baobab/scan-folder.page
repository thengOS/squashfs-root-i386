<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="scan-folder" xml:lang="de">

  <info>
    <link type="guide" xref="index#scan"/>
    <revision version="0.1" date="2011-12-19" status="stub"/>
    <revision pkgversion="3.4" date="2012-02-20" status="review"/>
    <revision pkgversion="3.8" date="2013-07-20" status="review"/>

    <credit type="author copyright">
      <name>Julita Inca</name>
      <email its:translate="no">yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Einen lokalen Ordner einschließlich dessen Unterordnern einlesen.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2008, 2012, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Einen Ordner einlesen</title>

  <p>Das Einlesen individueller Ordner benötigt weniger Zeit als für das gesamte Dateisystem. Das ist effizienter, falls Sie lediglich Informationen zu einem bestimmten Teil Ihres Dateisystems benötigen.</p>

  <steps>
    <item>
      <p>Klicken Sie auf den Knopf in der oberen rechten Ecke des Hauptfensters und wählen Sie <gui style="menuitem">Ordner einlesen …</gui>.</p>
    </item>
    <item>
      <p>Daraufhin öffnet sich ein Dateiöffner-Dialog. Wählen Sie den Ordner, den Sie einlesen möchten.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Öffnen</gui>, um mit dem Einlesen zu beginnen.</p>
    </item>
  </steps>

  <p>Der Ordner, den Sie soeben eingelesen haben, wird nun zu Ihrer Liste der <gui>Geräte und Orte</gui> hinzugefügt. Wenn der Ordner umbenannt oder gelöscht wird, so wird er beim nächsten Start der <app>Festplattenbelegungsanalyse</app> aus der Liste entfernt.</p>

</page>
