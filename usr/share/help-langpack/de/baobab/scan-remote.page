<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="scan-remote" xml:lang="de">

  <info>
    <link type="guide" xref="index#scan"/>
    <revision version="0.1" date="2011-12-19" status="stub"/>
    <revision pkgversion="3.4" date="2012-02-20" status="review"/>
    <revision pkgversion="3.10" date="2013-07-20" status="review"/>

    <credit type="author copyright">
      <name>Julita Inca</name>
      <email its:translate="no">yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Einen nicht auf Ihrem lokalen Rechner befindlichen Ordner einlesen.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2008, 2012, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2013, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Einen entfernten Ordner einlesen</title>

  <p>Die <app>Festplattenbelegungsanalyse</app> kann entfernte Speichergeräte einlesen. So lesen Sie das gesamte Dateisystem oder einen bestimmten Ordner ein:</p>

  <steps>
    <item>
      <p>Klicken Sie auf den Knopf oben rechts im Hauptfenster und wählen Sie <gui style="menuitem">Entfernten Ordner einlesen …</gui>.</p>
    </item>
    <item>
      <p>Geben Sie die Adresse im Feld <gui>Server-Adresse</gui> ein. Sie enthält normalerweise einen Protokollnamen, gefolgt von einem Doppelpunkt und zwei Schrägstrichen. Sie hängt vom jeweils verwendeten <link xref="help:gnome-help/nautilus-connect#types">Protokoll</link> ab.</p>
    </item>
    <item>
      <p>Klicken Sie zum Fortsetzen auf <gui>Verbinden</gui>. Sie werden gegebenenfalls nach weiteren Details gefragt, wie dem Benutzernamen und dem Passwort, bevor der Einlesevorgang beginnt.</p>
    </item>
  </steps>

  <note>
    <p>Das Einlesen über ein Netzwerk kann langsamer sein als innerhalb des lokalen Dateisystems.</p>
  </note>

  <p>Sie können auch einen zuletzt verwendeten Server wählen, anstatt eine neue Adresse einzutippen. Wenn Sie eine ungültige Adresse eingeben, können Sie nicht auf <gui>Fortsetzen</gui> klicken. Wenn die Adresse zwar gültig, aber nicht richtig ist, so wird die Verbindung ohne Warnungen fehlschlagen.</p>

</page>
