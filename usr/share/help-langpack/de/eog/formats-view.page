<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="formats-view" xml:lang="de">

  <info>
    <link type="guide" xref="index"/>
    <link type="seealso" xref="formats-save"/>
    <desc>Der Bildbetrachter deckt einen weiten Bereich von Grafikformaten ab, wie PNG, JPEG und TIFF.</desc>

    <revision pkgversion="3.2" version="0.1" date="2011-09-05" status="final"/>
  <credit type="author">
    <name>Tiffany Antopolski</name>
    <email>tiffany.antopolski@gmail.com</email>
  </credit>
  <license>
   <p>Creative Commons Share Alike 3.0</p>
  </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jan Arne Petersen</mal:name>
      <mal:email>jpetersen@jpetersen.org</mal:email>
      <mal:years>2006</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2008, 2010, 2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Felix Riemann</mal:name>
      <mal:email>friemann@svn.gnome.org</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>Christian.Kirbach@googlemail.com</mal:email>
      <mal:years>2011, 2012, 2014, 2015</mal:years>
    </mal:credit>
  </info>

 <title>Unterstützte Bildformate</title>

  <p>Der Bildbetrachter kann Bilder in den folgenden Formaten öffnen:</p>
  <list>
   <item><p>ANI - Animation</p></item>
   <item><p>BMP - Bitmap</p></item>
   <item><p>GIF - Graphics Interchange Format</p></item>
   <item><p>ICO - Windows Icon</p></item>
   <item><p>JPEG/JPG - Joint Photographic Experts Group</p></item>
   <item><p>PCX - PC Paintbrush</p></item>
   <item><p>PNG - Portable Network Graphics</p></item>
   <item><p>PNM - Portable Anymap from the PPM Toolkit</p></item>
   <item><p>RAS - Sun Raster</p></item>
   <item><p>SVG - Scalable Vector Graphics</p></item>
   <item><p>TGA - Targa</p></item>
   <item><p>TIFF - Tagged Image File Format</p></item>
   <item><p>WBMP - Wireless Bitmap</p></item>
   <item><p>XBM - X Bitmap</p></item>
   <item><p>XPM - X Pixmap</p></item>
  </list>
  <p>Falls Sie versuchen, ein nicht unterstütztes Format zu öffnen, erhalten Sie die Fehlermeldung <gui>Bild »Bildname« konnte nicht geöffnet werden</gui>.</p>
</page>
