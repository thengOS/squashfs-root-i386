<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-what" xml:lang="de">

  <info>
    <link type="guide" xref="backup-why"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Sichern Sie alles, dessen Verlust Sie nicht verschmerzen können, falls etwas schief geht.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2015.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>

  <title>Was gesichert werden sollte</title>

  <p>Vorzugsweise sollten Sie sowohl die <link xref="backup-thinkabout">wichtigsten Dateien</link> als auch diejenigen sichern, die schwer wiederherzustellen sind. Ein Beispiel, gewichtet von den wichtigsten bis zu den unwichtigsten Dateien:</p>

<terms>
 <item>
  <title>Ihre persönlichen Dateien</title>
   <p>Dies könnten Dokumente, Tabellenblätter, E-Mail-Nachrichten, Kalendereinträge, Finanzdaten, Familienfotos oder sonstige persönliche Daten sein, die für Sie als unersetzlich gelten.</p>
 </item>

 <item>
  <title>Ihre persönlichen Einstellungen</title>
   <p>Dies schließt Änderungen der Farben, Hintergründe, der Bildschirmauflösung und Mauseinstellungen in Ihrer Arbeitsumgebung ein. Weiterhin können dies Anwendungseinstellungen sein, zum Beispiel für <app>LibreOffice</app>, für Ihr Musikwiedergabeprogramm oder Ihr E-Mail-Programm. Diese Einstellungen lassen sich zwar wiederherstellen, doch das kann unter Umständen viel Zeit in Anspruch nehmen.</p>
 </item>

 <item>
  <title>Systemeinstellungen</title>
   <p>Die meisten Leute ändern die während der Installation erzeugten Systemeinstellungen nie. Falls Sie aus irgendwelchen Gründen Ihre Systemeinstellungen anpassen wollen oder es sich bei Ihrem Rechner um einen Server handelt, sollten Sie diese Einstellungen vorher sichern.</p>
 </item>

 <item>
  <title>Installierte Software</title>
   <p>Die von Ihnen verwendete Software kann üblicherweise nach einem ernsthaften Rechnerproblem wiederhergestellt werden, indem Sie diese erneut installieren.</p>
 </item>
</terms>

  <p>Allgemein sollten Sie alle Dateien sichern, die unersetzlich sind, sowie Dateien, deren Wiederherstellung ohne vorherige Sicherung sehr viel Zeit erfordert. Andererseits sollten Sie für leicht zu ersetzende Dinge keinen Plattenplatz für die Sicherung opfern.</p>

</page>
