<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-hidden" xml:lang="de">

  <info>
    <link type="guide" xref="files#faq"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-29" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Eine Datei unsichtbar machen, so dass sie in der Dateiverwaltung nicht angezeigt wird.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2015.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>

<title>Eine Datei verbergen</title>

  <p>The <app>Files</app> file manager gives you the ability to hide and unhide
  files at your discretion. When a file is hidden, it is not displayed by the
  file manager, but it is still there in its folder.</p>

  <p>Um eine Datei zu verbergen, <link xref="files-rename">benennen Sie diese um</link> mit einem <key>.</key> am Anfang des Dateinamens. Um zum Beispiel die Datei <file>Beispiel.txt</file> zu verbergen, benennen Sie sie um zu <file>.Beispiel.txt</file>.</p>

<note>
  <p>Sie können Ordner auf die gleiche Weise verbergen wie Dateien. Benennen Sie den Ordner mit einem <key>.</key> am Anfang des Ordnernamens um, um ihn zu verbergen.</p>
</note>

<section id="show-hidden">
 <title>Alle verborgenen Dateien anzeigen</title>

  <p>Wenn Sie alle verborgenen Dateien in einem Ordner anzeigen wollen, wechseln Sie in diesen Ordner und klicken Sie entweder auf den <gui><media its:translate="no" type="image" src="figures/go-down.png"><span its:translate="yes">Ansichtseinstellungen</span></media></gui> Knopf in der Werkzeugleiste und wählen Sie <gui>Verborgene Dateien anzeigen</gui>, oder drücken Sie <keyseq><key>Strg</key><key>H</key></keyseq>. Alle verborgenen Dateien werden dann gemeinsam mit den normalen, nicht verborgenen Dateien angezeigt.</p>

  <p>Um diese Dateien wieder zu verbergen, klicken Sie entweder noch einmal auf den Knopf <gui><media its:translate="no" type="image" src="figures/go-down.png"><span its:translate="yes">Ansichtseinstellungen</span></media></gui> in der Werkzeugleiste und wählen Sie <gui>Verborgene Dateien anzeigen</gui>, oder drücken Sie wieder <keyseq><key>Strg</key><key>H</key></keyseq>.</p>

</section>

<section id="unhide">
 <title>Eine Datei anzeigen</title>

  <p>To unhide a file, go to the folder containing the hidden file and click
  the
  <!-- FIXME: Get a tooltip added for "View options" -->
  view options button in the toolbar and pick <gui>Show Hidden Files</gui>.
  Then, find the hidden file and rename it so that it does not have a
  <file>.</file> in front of its name. For example, to unhide a file called
  <file>.example.txt</file>, you should rename it to
  <file>example.txt</file>.</p>

  <p>Once you have renamed the file, you can either click the
  <!-- FIXME: Get a tooltip added for "View options" -->
  view options button in the toolbar and pick <gui>Show Hidden Files</gui>, or
  press <keyseq><key>Ctrl</key><key>H</key></keyseq> to hide any other hidden
  files again.</p>

  <note><p>Standardmäßig werden in der Dateiverwaltung verborgene Dateien nur so lange angezeigt, bis Sie die Dateiverwaltung schließen. Um diese Einstellung so zu ändern, dass die Dateiverwaltung verborgene Dateien immer anzeigt, siehe <link xref="nautilus-views"/>.</p></note>

  <note><p>Die meisten verborgenen Dateien haben ein <key>.</key> am Anfang ihres Namens. Andere können stattdessen ein <key>~</key> am Namensende haben. Dabei handelt es sich um Sicherheitskopien. Lesen Sie auch <link xref="files-tilde"/> für weitergehende Informationen.</p></note>

</section>

</page>
