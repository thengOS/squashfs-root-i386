<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-sort" xml:lang="de">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-25" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ordnen Sie Dateien nach Name, Größe, Typ oder Änderungsdatum an.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2015.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>

<title>Dateien und Ordner sortieren</title>

<p>Sie können Dateien in einem Ordner auf verschiedene Weise sortieren, zum Beispiel nach ihrem Datum oder der Dateigröße. Bitte lesen Sie weiter unten <link xref="#ways"/> für eine Liste der üblichen Sortierweisen. Lesen Sie <link xref="nautilus-views"/> für weitere Informationen, wie Sie die voreingestellte Sortierreihenfolge ändern.</p>

<p>Wie Sie Dateien sortieren können, hängt von der <em>Ordneransicht</em> ab, die Sie verwenden. Sie können die aktuelle Ansicht mit Hilfe des Listen- und Symbol-Knopfes in der Werkzeugleiste ändern.</p>

<section id="icon-view">
  <title>Symbolansicht</title>

  <p>To sort files in a different order, click the view options button in the
  <!-- FIXME: Get a tooltip added for "View options" -->
  toolbar and choose <gui>By Name</gui>, <gui>By Size</gui>, <gui>By
  Type</gui>, <gui>By Modification Date</gui>, or <gui>By Access
  Date</gui>.</p>

  <p>Ein Beispiel: Wenn Sie <gui>Nach Name</gui> wählen, so werden die Dateien in alphabetischer Reihenfolge nach ihren Namen sortiert. Lesen Sie <link xref="#ways"/> für weitere Optionen.</p>

  <p>Sie können in umgekehrter Reihenfolge sortieren, indem Sie <gui>Umgekehrte Reihenfolge</gui> aus dem Menü wählen.</p>

</section>

<section id="list-view">
  <title>Listenansicht</title>

  <p>Um die Dateien in einer anderen Reihenfolge anzuordnen, klicken Sie auf einen Spaltenkopf in der Dateiverwaltung. Klicken Sie zum Beispiel auf <gui>Typ</gui>, um sie nach Dateityp zu sortieren. Klicken Sie noch einmal auf den Spaltenkopf, um die Reihenfolge umzudrehen.</p>
  <p>In list view, you can show columns with more attributes and sort on those
  columns. Click the view options button in the toolbar, pick <gui>Visible
  <!-- FIXME: Get a tooltip added for "View options" -->
  Columns…</gui> and select the columns that you want to be visible. You will
  then be able to sort by those columns. See <link xref="nautilus-list"/> for
  descriptions of available columns.</p>

</section>

<section id="ways">
  <title>Möglichkeiten zum Sortieren von Dateien</title>

  <terms>
    <item>
      <title>Name</title>
      <p>Sortiert alphabetisch nach dem Namen der Datei.</p>
    </item>
    <item>
      <title>Größe</title>
      <p>Sortiert nach der Größe der Datei (dem belegten Speicherplatz). Die Sortierung erfolgt standardmäßig von der kleinsten zur größten Datei.</p>
    </item>
    <item>
      <title>Typ</title>
      <p>Sortiert alphabetisch nach dem Dateityp. Datei des gleichen Typs werden zunächst zu Gruppen zusammengefasst und dann nach Namen sortiert.</p>
    </item>
    <item>
      <title>Last Modified</title>
      <p>Sortiert nach Datum und Zeit der letzten Änderung der Datei. Die Sortierung erfolgt standardmäßig von der ältesten zur neuesten Datei. </p>
    </item>
  </terms>

</section>

</page>
