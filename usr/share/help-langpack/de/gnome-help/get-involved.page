<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="get-involved" xml:lang="de">

  <info>
    <link type="guide" xref="more-help"/>
    <desc>Hier erfahren Sie, wo und wie Sie Probleme mit dieser Hilfe melden können.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2015.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>
  <title>Mithelfen, dieses Handbuch zu verbessern</title>

  <section id="bug-report">
   <title>Einen Fehlerbericht oder einen Verbesserungsvorschlag einreichen</title>
   <p>Dieses Hilfesystem wurde von einer Gemeinschaft aus Freiwilligen erstellt. Sie sind herzlich eingeladen, sich daran zu beteiligen. Wenn Sie ein Problem mit diesen Hilfeseiten feststellen (wie Tippfehler, falsche Anleitungen oder Themen, die behandelt werden sollten, aber nicht werden), können Sie einen <em>Fehlerbericht</em> einreichen. Um einen Fehler zu berichten, gehen Sie zu <link href="https://bugzilla.gnome.org/">bugzilla.gnome.org</link>.</p>
   <p>Sie müssen sich registrieren, damit Sie Fehler berichten und Aktualisierungen über Ihren Status per E-Mail erhalten können. Wenn Sie noch keinen Zugang haben, können Sie auf <gui>New Account</gui> klicken und ein Konto einrichten.</p>
   <p>Sobald Sie ein Konto haben, melden Sie sich an, klicken Sie auf <guiseq><gui>File a Bug</gui><gui>Core</gui><gui>gnome-user-docs</gui></guiseq>. Lesen Sie bitte vor dem Berichten eines Fehlers die <link href="https://bugzilla.gnome.org/page.cgi?id=bug-writing.html">Richtlinien zum Melden von Fehlern (englisch)</link> und <link href="https://bugzilla.gnome.org/browse.cgi?product=gnome-user-docs">durchsuchen</link> Sie bitte die Datenbank, ob nicht ein ähnlicher Fehler bereits gemeldet worden ist.</p>
   <p>Zum Melden des Fehlers wählen Sie bitte die entsprechende Komponente im Menü <gui>Component</gui>. Wenn Sie einen Fehler in der Dokumentation melden, sollten Sie die Komponente <gui>gnome-help</gui> wählen. Wenn Sie sich nicht sicher sind, wählen Sie einfach <gui>general</gui>.</p>
   <p>Wenn Sie um eine Erweiterung bitten, so wählen Sie <gui>enhancement</gui> (Erweiterung) im Menü <gui>Severity</gui> (Schweregrad). Füllen Sie die Abschnitte »summary« (Zusammenfassung) und »description« (Beschreibung) aus und klicken Sie auf <gui>Commit</gui>.</p>
   <p>Ihr Bericht bekommt eine Kennung (ID) zugewiesen und der Status wird mit laufender Bearbeitung stets aktualisiert. Danke für Ihre Mithilfe bei der Verbesserung der GNOME-Hilfe!</p>
   </section>

   <section id="contact-us">
   <title>Mit uns in Kontakt treten</title>
   <p>Sie können eine <link href="mailto:gnome-doc-list@gnome.org">E-Mail</link> an die GNOME-docs-Mailingliste senden, um zu erfahren, wie Sie sich in das Dokumentationsteam einbringen können. <em>E-Mail:</em> <link href="mailto:gnome-doc-list@gnome.org"/>.</p>
   </section>
</page>
