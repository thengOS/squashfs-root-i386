<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="keyboard-shortcuts-set" xml:lang="de">

  <info>
    <link type="guide" xref="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-23" status="review"/>
    <revision pkgversion="3.9.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.17.90" date="2015-08-30" status="candidate"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email its:translate="no">yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Juanjo Marín</name>
      <email its:translate="no">juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email its:translate="no">tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <desc>Festlegen und Ändern von Tastenkombination in den <gui>Tastatur</gui>-Einstellungen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2015.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>

  <title>Tastenkombinationen einrichten</title>

<p>So ändern Sie für eine Tastenkombination die zu drückenden Tasten:</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Keyboard</gui>.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Tastatur</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Wählen Sie den Reiter <gui>Tastenkombinationen</gui>.</p>
    </item>
    <item>
      <p>Wählen Sie eine Kategorie in der linken Leiste und die Zeile für die gewünschte Aktion auf der rechten Seite. Die aktuelle Definition der Tastenkombination ändert sich zu <gui>Neue Tastenkombination …</gui></p>
    </item>
    <item>
      <p>Halten Sie die gewünschte Tastenkombination gedrückt oder drücken Sie die <key>Löschtaste</key> zum Aufheben der Festlegung.</p>
    </item>
  </steps>


<section id="defined">
<title>Voreingestellte Tastenkürzel</title>
  <p>Es gibt eine Anzahl voreingestellter Tastenkürzel, die geändert werden können, gruppiert in die folgenden Kategorien:</p>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Starter</title>
  <tr>
	<td><p>Hilfe-Browser starten</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
  <tr>
	<td><p>Taschenrechner öffnen</p></td>
	<td><p>Taschenrechner</p></td>
  </tr>
  <tr>
	<td><p>E-Mail-Programm öffnen</p></td>
	<td><p>E-Mail</p></td>
  </tr>
  <tr>
	<td><p>Webbrowser starten</p></td>
	<td><p>WWW</p></td>
  </tr>
  <tr>
	<td><p>Persönlicher Ordner</p></td>
	<td><p>Explorer</p></td>
  </tr>
  <tr>
	<td><p>Suchen</p></td>
	<td><p>Suchen</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Navigation</title>
  <tr>
	<td><p>Fenster auf Arbeitsfläche 1 verschieben</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
  <tr>
	<td><p>Fenster auf Arbeitsfläche 2 verschieben</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
  <tr>
	<td><p>Fenster auf Arbeitsfläche 3 verschieben</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
  <tr>
	<td><p>Fenster auf Arbeitsfläche 4 verschieben</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
  <tr>
	<td><p>Fenster eine Arbeitsfläche nach links verschieben</p></td>
	<td><p><keyseq><key>Umschalttaste</key><key>Strg</key><key>Alt</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Fenster eine Arbeitsfläche nach rechts verschieben</p></td>
	<td><p><keyseq><key>Umschalttaste</key><key>Strg</key><key>Alt</key><key>→</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Fenster eine Arbeitsfläche nach oben verschieben</p></td>
	<td><p><keyseq><key>Umschalttaste</key><key xref="keyboard-key-super">Super</key> <key>Bild↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Fenster eine Arbeitsfläche nach unten verschieben</p></td>
	<td><p><keyseq><key>Umschalttaste</key><key xref="keyboard-key-super">Super</key> <key>Bild↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Anwendungen wechseln</p></td>
	<td><p><keyseq><key>Super</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
  <td><p>Fenster wechseln</p></td>
  <td><p>Deaktiviert</p></td>
  </tr>
  <tr>
	<td><p>Zwischen den Fenstern einer Anwendung wechseln</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
  <tr>
	<td><p>Systemsteuerungen umschalten</p></td>
	<td><p><keyseq><key>Strg</key><key>Alt</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Fenster sofort wechseln</p></td>
	<td><p><keyseq><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Sofort zwischen den Fenstern einer Anwendung wechseln</p></td>
	<td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Systemsteuerungen sofort umschalten</p></td>
	<td><p><keyseq><key>Strg</key><key>Alt</key><key>Esc</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Alle normalen Fenster verbergen</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
  <tr>
	<td><p>Zur Arbeitsfläche 1 wechseln</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
  <tr>
	<td><p>Zur Arbeitsfläche 2 wechseln</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
  <tr>
	<td><p>Zur Arbeitsfläche 3 wechseln</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
  <tr>
	<td><p>Zur Arbeitsfläche 4 wechseln</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
  <tr>
	<td><p>Auf Arbeitsfläche links verschieben</p></td>
	<td><p><keyseq><key>Strg</key><key>Alt</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Auf Arbeitsfläche rechts verschieben</p></td>
	<td><p><keyseq><key>Strg</key><key>Alt</key><key>→</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Auf Arbeitsfläche darüber verschieben</p></td>
	<td><p><keyseq><key>Super</key><key>Bild↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Auf Arbeitsfläche darunter verschieben</p></td>
	<td><p><keyseq><key>Super</key><key>Bild↓</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Bildschirmfotos</title>
  <tr>
	<td><p>Ein Bildschirmfoto aufnehmen</p></td>
	<td><p><key>Druck</key></p></td>
  </tr>
  <tr>
	<td><p>Ein Bildschirmfoto eines Fensters aufnehmen</p></td>
	<td><p><keyseq><key>Alt</key><key>Druck</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Ein Bildschirmfoto eines Bereichs aufnehmen</p></td>
	<td><p><keyseq><key>Umschalttaste</key><key>Druck</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Ein Bildschirmfoto in die Zwischenablage kopieren</p></td>
	<td><p><keyseq><key>Strg</key><key>Druck</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Ein Bildschirmfoto eines Fensters in die Zwischenablage kopieren</p></td>
	<td><p><keyseq><key>Strg</key><key>Alt</key><key>Druck</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Ein Bildschirmfoto eines Bereichs in die Zwischenablage kopieren</p></td>
	<td><p><keyseq><key>Umschalttaste</key><key>Strg</key><key>Druck</key></keyseq></p></td>
  </tr>
  <tr>
  <td><p>Eine Bildschirmaufzeichnung erstellen</p></td>
  <td><p><keyseq><key>Umschalttaste</key><key>Strg</key><key>Alt</key><key>R</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Ton und Medien</title>
  <tr>
	<td><p>Stumm schalten</p></td>
	<td><p>Stumm schalten (Audio)</p></td>
  </tr>
  <tr>
	<td><p>Leiser</p></td>
	<td><p>Lautstärke verringern</p></td>
  </tr>
  <tr>
	<td><p>Lauter</p></td>
	<td><p>Lautstärke erhöhen</p></td>
  </tr>
  <tr>
	<td><p>Medienwiedergabeprogramm starten</p></td>
	<td><p>Tonmedien</p></td>
  </tr>
  <tr>
	<td><p>Wiedergabe (bzw. Wiedergabe/Unterbrechen)</p></td>
	<td><p>Wiedergabe (Audio)</p></td>
  </tr>
  <tr>
	<td><p>Wiedergabe unterbrechen</p></td>
	<td><p>Pause (Audio)</p></td>
  </tr>
  <tr>
	<td><p>Wiedergabe anhalten</p></td>
	<td><p>Stopp (Audio)</p></td>
  </tr>
  <tr>
	<td><p>Vorheriger Titel</p></td>
	<td><p>Zurück (Audio)</p></td>
  </tr>
  <tr>
	<td><p>Nächster Titel</p></td>
	<td><p>Vor (Audio)</p></td>
  </tr>
  <tr>
	<td><p>Auswerfen</p></td>
	<td><p>Auswerfen</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>System</title>
  <tr>
	<td><p>Den »Befehl ausführen«-Dialog anzeigen</p></td>
	<td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Die Aktivitäten-Übersicht anzeigen</p></td>
	<td><p><keyseq><key>Alt</key><key>F1</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Abmelden</p></td>
	<td><p><keyseq><key>Strg</key><key>Alt</key><key>Löschtaste</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Bildschirm sperren</p></td>
	<td><p><keyseq><key>Super</key><key>L</key></keyseq></p></td>
  </tr>
  <tr>
  <td><p>Das Benachrichtigungsfeld anzeigen</p></td>
  <td><p><keyseq><key>Super</key><key>M</key></keyseq></p></td>
  </tr>
  <tr>
  <td><p>Auf aktive Benachrichtigungen ausrichten</p></td>
  <td><p><keyseq><key>Super</key><key>N</key></keyseq></p></td>
  </tr>
  <tr>
  <td><p>Alle Anwendungen anzeigen</p></td>
  <td><p><keyseq><key>Super</key><key>A</key></keyseq></p></td>
  </tr>
  <tr>
  <td><p>Das Anwendungsmenü öffnen</p></td>
  <td><p><keyseq><key>Super</key><key>F10</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Texteingabe</title>
  <tr>
  <td><p>Zur nächsten Eingabequelle wechseln</p></td>
  <td><p><keyseq><key>Super</key><key>Leertaste</key></keyseq></p></td>
  </tr>

  <tr>
  <td><p>Zur vorhergehenden Eingabequelle wechseln</p></td>
  <td><p>Deaktiviert</p></td>
  </tr>
  <tr>
  <td><p>Nur Zusatztasten wechseln zur nächsten Quelle</p></td>
  <td><p>Deaktiviert</p></td>
  </tr>
  <tr>
  <td><p>Compose-Taste</p></td>
  <td><p>Deaktiviert</p></td>
  </tr>
  <tr>
  <td><p>Taste für alternative Zeichen</p></td>
  <td><p>Deaktiviert</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Barrierefreiheit</title>
  <tr>
	<td><p>Vergrößerung ein-/ausschalten</p></td>
	<td><p><keyseq><key>Alt</key><key>Super</key><key>8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Ansicht vergrößern</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>=</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Ansicht verkleinern</p></td>
  <td><p><keyseq><key>Alt</key><key>Super</key><key>-</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Bildschirmleser ein-/ausschalten</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
  <tr>
	<td><p>Bildschirmtastatur ein-/ausschalten</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
  <tr>
	<td><p>Schrift vergrößern</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
  <tr>
	<td><p>Schrift verkleinern</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
  <tr>
	<td><p>Hohen Kontrast ein-/ausschalten</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Fenster</title>
  <tr>
	<td><p>Das Fenstermenü aktivieren</p></td>
	<td><p><keyseq><key>Alt</key><key>Leertaste</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Vollbildmodus ein-/ausschalten</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
  <tr>
	<td><p>Maximierungszustand ein-/ausschalten</p></td>
	<td><p><keyseq><key>Alt</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Fenster maximieren</p></td>
	<td><p><keyseq><key>Super</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Fenstergröße wiederherstellen</p></td>
  <td><p><keyseq><key>Super</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Fenster ein-/ausrollen</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
  <tr>
	<td><p>Fenster schließen</p></td>
	<td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
  </tr>
  <tr>
  <td><p>Fenster verbergen</p></td>
  <td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Fenster verschieben</p></td>
	<td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Fenstergröße ändern</p></td>
	<td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Festlegen, ob das Fenster auf allen oder nur einer Arbeitsfläche sichtbar ist</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
  <tr>
	<td><p>Fenster anheben, falls es verdeckt ist, andernfalls absenken</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
  <tr>
	<td><p>Fenster vor die anderen Fenster anheben</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
  <tr>
	<td><p>Fenster hinter die anderen Fenster absenken</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
  <tr>
	<td><p>Fenster vertikal maximieren</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
  <tr>
	<td><p>Fenster horizontal maximieren</p></td>
	<td><p>Deaktiviert</p></td>
  </tr>
  <tr>
  <td><p>Ansicht links teilen</p></td>
  <td><p><keyseq><key>Super</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
  <td><p>Ansicht rechts teilen</p></td>
  <td><p><keyseq><key>Super</key><key>→</key></keyseq></p></td>
  </tr>
</table>

</section>

<section id="custom">
<title>Eigene Tastenkombinationen</title>

  <p>To create your own application keyboard shortcut in the
  <app>Keyboard</app> settings:</p>

  <steps>
    <item>
      <p>Click the <gui style="button">+</gui> button. The <gui>Custom
      Shortcut</gui> window will appear.</p>
    </item>
    <item>
      <p>Type a <gui>Name</gui> to identify the shortcut, and a
      <gui>Command</gui> to run an application, then click <gui>Add</gui>.
      For example, if you wanted the shortcut to open <app>Rhythmbox</app>, you
      could name it <input>Music</input> and use the <input>rhythmbox</input>
      command.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Deaktiviert</gui> in der soeben hinzugefügten Zeile. Wenn es sich zu <gui>Neue Tastenkombination …</gui> ändert, halten Sie die gewünschte Tastenkombination gedrückt.</p>
    </item>
  </steps>

  <p>Der von Ihnen eingegebene Befehl sollte ein gültiger Befehl des Systems sein. Sie können überprüfen, ob der Befehl funktioniert, indem Sie ihn in einem Terminal eingeben. Der zum Öffnen einer Anwendung verwendete Befehl muss nicht zwangsläufig dem Anwendungsnamen entsprechen.</p>

  <p>Wenn Sie den Befehl ändern wollen, der einer bestimmten Tastenkombination zugeordnet ist, klicken Sie doppelt auf den <em>Namen</em> des Kürzels. Das Fenster <gui>Individuelle Tastenkombination</gui> wird geöffnet, in welchem Sie den Befehl ändern können.</p>

</section>

</page>
