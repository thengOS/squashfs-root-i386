<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-background" xml:lang="de">

  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email its:translate="no">gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>April Gonzales</name>
      <email its:translate="no">loonycookie@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email its:translate="no">nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email its:translate="no">ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email its:translate="no">tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>So verwenden Sie ein Bild, eine Farbe oder einen Farbverlauf als Hintergrund des Schreibtischs oder Sperrbildschirms.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2015.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>

  <title>Anpassen des Hintergrundes für den Schreibtisch und den Sperrbildschirm</title>

  <p>Sie können die Bilder für Ihre Hintergründe ändern oder eine einfache Farbe verwenden.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Background</gui>.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Hintergrund</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Wählen Sie den <gui>Hintergrund</gui> oder <gui>Sperrbildschirm</gui>.</p>
    </item>
    <item>
      <p>Es werden drei Möglichkeiten oben angezeigt:</p>
      <list>
        <item>
          <p>Wählen Sie <gui>Hintergrundbilder</gui>, um eines der vielen professionellen Hintergrundbilder zu verwenden, die mit GNOME ausgeliefert werden. Manche Hintergrundbilder ändern sich im Laufe des Tages. Für diese Hintergrundbilder wird rechts unten eine kleine Uhr angezeigt.</p>
        </item>
        <item>
          <p>Wählen Sie <gui>Bilder</gui>, um eines Ihrer eigenen Fotos aus dem <file>Bilder</file>ordner zu verwenden. Die meisten Anwendungen zur Bilderverwaltung speichern Bilder dort. Falls Sie ein Bild verwenden wollen, das sich nicht in Ihrem Bilder-Ordner befindet, gehen Sie wie folgt vor: In <app>Dateien</app> klicken Sie mit der rechten Maustaste auf die Bilddatei und wählen <gui>Als Hintergrund verwenden</gui>, oder öffnen Sie im <app>Bildbetrachter</app> die Bilddatei, klicken Sie auf den Menüknopf in der Titelleiste und wählen Sie <gui>Als Hintergrund festlegen</gui>.</p>
        </item>
        <item>
          <p>Wählen Sie <gui>Farben</gui>, um nur eine einzige Farbe zu verwenden.</p>
        </item>
      </list>
    </item>
    <item>
      <p>Die Einstellungen werden sofort angewendet.</p>
    </item>
    <item>
      <p><link xref="shell-workspaces-switch">Wechseln Sie zu einer leeren Arbeitsfläche</link>, um den gesamten Schreibtisch zu sehen.</p>
    </item>
  </steps>

</page>
