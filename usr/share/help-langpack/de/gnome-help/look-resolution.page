<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-resolution" xml:lang="de">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="seealso" xref="look-display-fuzzy"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email its:translate="no">gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email its:translate="no">nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email its:translate="no">tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Die Auflösung des Bildschirms und seine Ausrichtung (Drehung) ändern.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2015.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>

  <title>Ändern der Auflösung und der Drehung des Bildschirms</title>

  <p>Sie können ändern, wie groß (oder wie detailliert) Dinge auf dem Bildschirm erscheinen, indem Sie die <em>Bildschirmauflösung</em> ändern. Sie können ändern, wie herum die Dinge erscheinen (zum Beispiel wenn Sie einen drehbaren Monitor haben), indem Sie die <em>Drehung</em> ändern.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Displays</gui>.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Bildschirme</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>Wenn Sie mehrere Bildschirme verwenden und diese nicht gespiegelt sind, können Sie für jeden Bildschirm eigene Einstellungen festlegen. Wählen Sie einen Bildschirm im Vorschaubereich aus.</p>
    </item>
    <item>
      <p>Wählen Sie die Auflösung und Drehung.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Anwenden</gui>. Die geänderten Einstellungen werden für 20 Sekunden angewendet und dann wieder rückgängig gemacht. Auf diese Weise werden Ihre alten Einstellungen wiederhergestellt, falls Sie mit den neuen Einstellungen nichts sehen können. Wenn Sie mit den neuen Einstellungen zufrieden sind, klicken Sie auf <gui>beibehalten</gui>.</p>
    </item>
  </steps>

<section id="resolution">
  <title>Auflösung</title>

  <p>Die Auflösung wird als Anzahl der Pixel (Punkte auf dem Bildschirm) angegeben, in jeder darstellbaren Richtung. Jede Auflösung hat ein <em>Seitenverhältnis</em>, das Verhältnis der Breite zur Höhe. Breitbildschirme verwenden ein Seitenverhältnis von 16:9, während traditionelle Bildschirme 4:3 verwenden. Wenn Sie eine Auflösung wählen, die nicht dem Seitenverhältnis Ihres Bildschirms entspricht, werden schwarze Balken hinzugefügt, um Verzerrungen zu vermeiden.</p>

  <p>Sie können aus der Auswahlliste die <gui>Auflösung</gui> auswählen, die Sie bevorzugen. Beachten Sie, dass wenn Sie eine Auflösung wählen, die für Ihren Bildschirm ungeeignet ist, das Bild <link xref="look-display-fuzzy">unscharf oder verpixelt</link> aussehen kann.</p>

</section>

<section id="rotation">
  <title>Drehung</title>

  <p>Bei manchen Laptops kann der Bildschirm physikalisch in viele Richtungen gedreht werden. Ssomit ist es nützlich, die Bildschirmdrehung ändern zu können. Sie können durch Klicken der Knöpfe mit den Pfeilen wählen, was Sie auf Ihrem Bildschirm sehen.</p>

</section>

</page>
