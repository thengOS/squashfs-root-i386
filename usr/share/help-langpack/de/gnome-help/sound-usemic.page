<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usemic" xml:lang="de">

  <info>
    <link type="guide" xref="media#sound"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Ein analoges oder USB-Mikrofon verwenden und das vorgegebene Eingabegerät wählen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2015.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>

  <title>Ein anderes Mikrofon verwenden</title>

  <p>Sie können ein externes Mikrofon für Unterhaltungen mit Freunden, Gespräche mit Kollegen, Aufzeichnen von Sprache oder für andere Multimedia-Zwecke verwenden. Selbst wenn der Rechner bereits über ein eingebautes Mikrofon verfügen sollte, bietet ein externes Mikrofon meist eine bessere Klangqualität.</p>

  <p>If your microphone has a circular plug, just plug it into the appropriate
  audio socket on your computer. Most computers have two sockets: one for
  microphones and one for speakers. This socket is usually light red in color
  or is accompanied by a picture of a microphone. Microphones plugged
  into the appropriate socket are usually used by default. If not, see the
  instructions below for selecting a default input device.</p>

  <p>Falls Sie ein USB-Mikrofon haben, stecken Sie es an einen USB-Port Ihres Rechners. USB-Mikrofone funktionieren als separate Audiogeräte, daher müssen Sie angeben, welches Mikrofon als Standard verwendet werden soll.</p>

  <steps>
    <title>Vorgegebenes Audio-Eingabegerät wählen</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Sound</gui>.</p>
    </item>
    <item>
      <p>Klicken Sie auf <gui>Audio</gui>, um das Panel zu öffnen.</p>
    </item>
    <item>
      <p>In the <gui>Input</gui> tab, select the device that you want to use.
      The input level indicator should respond when you speak.</p>
    </item>
  </steps>

  <p>Sie können in diesem Panel die Lautstärke anpassen und das Mikrofon ausschalten.</p>

</page>
