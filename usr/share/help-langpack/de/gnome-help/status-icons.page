<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="status-icons" xml:lang="de">

  <info>
    <link type="guide" xref="shell-overview#apps"/>

    <revision version="0.1" date="2013-02-23" status="review"/>

    <credit type="author copyright">
      <name>Monica Kochofar</name>
      <email>monicakochofar@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Erläutert die Bedeutung der Symbole an der rechten Seite des oberen Panels.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2015.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>

  <title>Was bedeuten die Symbole im oberen Panel?</title>
<p>Dieser Abschnitt erklärt die Symbole oben rechts auf dem Bildschirm. Speziell werden die verschiedenen möglichen Variationen in der GNOME-Benutzeroberfläche erklärt.</p>

<if:choose>
<if:when test="!platform:gnome-classic">
<media type="image" src="figures/top-bar-icons.png" style="floatend">
  <p>Oberes Panel der GNOME Shell</p>
</media>
</if:when>
<if:when test="platform:gnome-classic">
<media type="image" src="figures/top-bar-icons-classic.png" width="395" height="70" style="floatend">
  <p>Oberes Panel der GNOME Shell</p>
</media>
</if:when>
</if:choose>

<links type="section"/>

<section id="universalicons">
<title>Symbole im Barrierefreiheitsmenü</title>

 <table shade="rows">
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/preferences-desktop-accessibility-symbolic.svg"/></td>
      <td><p>Führt zu einem Menü der Barrierefreiheitseinstellungen.</p></td>
    </tr>
    
  </table>
</section>


<section id="audioicons">
<title>Symbole der Lautstärkeregelung</title>

 <table shade="rows">
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/audio-volume-high-symbolic.svg"/></td>
      <td><p>Die Lautstärke ist auf hoch gesetzt.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/audio-volume-medium-symbolic.svg"/></td>
      <td><p>Die Lautstärke ist auf mittel gesetzt.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/audio-volume-low-symbolic.svg"/></td>
      <td><p>Die Lautstärke ist auf niedrig gesetzt.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/audio-volume-muted-symbolic.svg"/></td>
      <td><p>Die Lautstärke ist auf stumm geschaltet.</p></td>
    </tr>
  </table>
</section>


<section id="bluetoothicons">
<title>Symbole der Bluetooth-Verwaltung</title>

 <table shade="rows">
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/bluetooth-active-symbolic.svg"/></td>
      <td><p>Bluetooth wurde aktiviert.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/bluetooth-disabled-symbolic.svg"/></td>
      <td><p>Bluetooth wurde deaktiviert.</p></td>
    </tr>
  </table>
</section>

<section id="networkicons">
<title>Symbole der Netzwerkverwaltung</title>
<p/>
<p><app>Mobilfunkverbindungen</app></p>
 <table shade="rows">

    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-3g-symbolic.svg"/></td>
      <td><p>Mit einem 3G-Netzwerk verbunden.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-4g-symbolic.svg"/></td>
      <td><p>Mit einem 4G-Netzwerk verbunden.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-edge-symbolic.svg"/></td>
      <td><p>Mit einem EDGE-Netzwerk verbunden.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-gprs-symbolic.svg"/></td>
      <td><p>Mit einem GPRS-Netzwerk verbunden.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-umts-symbolic.svg"/></td>
      <td><p>Mit einem UMTS-Netzwerk verbunden.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-connected-symbolic.svg"/></td>
      <td><p>Mit einem Mobilfunk-Netzwerk verbunden.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-acquiring-symbolic.svg"/></td>
      <td><p>Verbindung mit einem Mobilfunknetzwerk wird aufgebaut.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-signal-excellent-symbolic.svg"/></td>
      <td><p>Sehr hohe Signalstärke.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-signal-good-symbolic.svg"/></td>
      <td><p>Hohe Signalstärke.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-signal-ok-symbolic.svg"/></td>
      <td><p>Mittlere Signalstärke.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-signal-weak-symbolic.svg"/></td>
      <td><p>Geringe Signalstärke.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-cellular-signal-none-symbolic.svg"/></td>
      <td><p>Sehr geringe Signalstärke.</p></td>
    </tr></table>




<p><app>Kabelgebundene Netzwerkverbindungen</app></p>
 <table shade="rows">
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-error-symbolic.svg"/></td>
      <td><p>Beim Suchen des Netzwerks trat ein Fehler auf.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-idle-symbolic.svg"/></td>
      <td><p>Das Netzwerk ist untätig.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-no-route-symbolic.svg"/></td>
      <td><p>Es wurde keine Route für das Netzwerk gefunden.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-offline-symbolic.svg"/></td>
      <td><p>Das Netzwerk ist abgemeldet.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-receive-symbolic.svg"/></td>
      <td><p>Das Netzwerk empfängt Daten.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-transmit-receive-symbolic.svg"/></td>
      <td><p>Das Netzwerk sendet und empfängt Daten.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-transmit-symbolic.svg"/></td>
      <td><p>Das Netzwerk sendet Daten.</p></td>
    </tr>
</table>



<p><app>Virtuelle Private Netzwerkverbindung</app></p>
 <table shade="rows">
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-vpn-acquiring-symbolic.svg"/></td>
      <td><p>Netzwerkverbindung wird aufgebaut.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-vpn-symbolic.svg"/></td>
      <td><p>Mit einem VPN-Netzwerk verbunden.</p></td>
    </tr>
</table>


<p><app>Kabelgebundene Verbindung</app></p>
 <table shade="rows">
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wired-acquiring-symbolic.svg"/></td>
      <td><p>Netzwerkverbindung wird aufgebaut.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wired-disconnected-symbolic.svg"/></td>
      <td><p>Die Netzwerkverbindung wurde getrennt.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wired-symbolic.svg"/></td>
      <td><p>Mit einem kabelgebundenen Netzwerk verbunden.</p></td>
    </tr>
</table>


<p><app>Funknetzwerkverbindung</app></p>
 <table shade="rows">
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-acquiring-symbolic.svg"/></td>
      <td><p>Funketzwerkverbindung wird aufgebaut.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-encrypted-symbolic.svg"/></td>
      <td><p>Das Funknetzwerk ist verschlüsselt.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-connected-symbolic.svg"/></td>
      <td><p>Mit einem Funknetzwerk verbunden.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-excellent-symbolic.svg"/></td>
      <td><p>Sehr hohe Signalstärke.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-good-symbolic.svg"/></td>
      <td><p>Hohe Signalstärke.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-ok-symbolic.svg"/></td>
      <td><p>Mittlere Signalstärke.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-weak-symbolic.svg"/></td>
      <td><p>Geringe Signalstärke.</p></td>
    </tr>
<tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-none-symbolic.svg"/></td>
      <td><p>Sehr geringe Signalstärke.</p></td>
    </tr>

  </table>
</section>

<section id="batteryicons">
<title>Symbole der Energieverwaltung</title>

 <table shade="rows">
   <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-full-symbolic.svg"/></td>
      <td><p>Der Akku ist vollständig geladen.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-good-symbolic.svg"/></td>
      <td><p>Der Akku ist teilweise entladen.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-low-symbolic.svg"/></td>
      <td><p>Ladezustand des Akkus ist niedrig.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-caution-symbolic.svg"/></td>
      <td><p>Achtung: Der Ladezustand des Akkus ist sehr niedrig.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-empty-symbolic.svg"/></td>
      <td><p>Ladezustand des Akkus ist sehr niedrig.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-missing-symbolic.svg"/></td>
      <td><p>Der Akku wurde entfernt.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-full-charged-symbolic.svg"/></td>
      <td><p>Der Akku ist vollständig geladen.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-full-charging-symbolic.svg"/></td>
      <td><p>Der Akku ist voll und lädt.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-good-charging-symbolic.svg"/></td>
      <td><p>Der Akku ist teilweise geladen und lädt.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-low-charging-symbolic.svg"/></td>
      <td><p>Der Akku ist wenig geladen und lädt.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-caution-charging-symbolic.svg"/></td>
      <td><p>Der Akku ist sehr wenig geladen und lädt.</p></td>
    </tr>
    <tr>
      <td><media its:translate="no" type="image" mime="image/svg" src="figures/battery-empty-charging-symbolic.svg"/></td>
      <td><p>Der Akku ist leer und lädt.</p></td>
    </tr>
  </table>
</section>


</page>
