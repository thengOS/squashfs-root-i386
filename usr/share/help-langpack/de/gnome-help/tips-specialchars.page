<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="tips-specialchars" xml:lang="de">

  <info>
    <link type="guide" xref="tips"/>
    <link type="seealso" xref="keyboard-layouts"/>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-18" status="review"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Geben Sie Zeichen ein, die nicht auf Ihrer Tastatur verfügbar sind, beispielsweise fremde Alphabete, mathematische Zeichen, Symbole und Piktogramme.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Hendrik Knackstedt</mal:name>
      <mal:email>hendrik.knackstedt@t-online.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Gabor Karsay</mal:name>
      <mal:email>gabor.karsay@gmx.at</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011-2015.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011-2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>

  <title>Sonderzeichen eingeben</title>

  <p>Sie können Tausende von Zeichen aus den meisten Schriftsystemen der Welt eingeben und betrachten, auch wenn diese auf Ihrer Tastatur nicht verfügbar sind. In diesem Abschnitt finden Sie einige Wege, wie Sie spezielle Zeichen eingeben können.</p>

  <links type="section">
    <title>Methoden zum Eingeben von Zeichen</title>
  </links>

  <section id="charmap">
    <title>Zeichentabelle</title>
    <p>GNOME liefert eine Zeichentabelle, die Ihnen das Durchsuchen aller Unicode-Zeichen ermöglicht. Verwenden Sie diese Zeichentabelle, um die gewünschten Zeichen zu finden, und fügen Sie sie anschließend ein, wo immer Sie wollen.</p>

    <p>Sie finden die <app>Zeichentabelle</app> in der <gui>Aktivitäten</gui>-Übersicht. Weitere Informationen zur Zeichentabelle finden Sie im <link href="help:gucharmap">Zeichentabelle-Handbuch</link>.</p>

  </section>

  <section id="compose">
    <title>Compose-Taste</title>
    <p>Eine Compose-Taste ist eine spezielle Taste, die Ihnen das Drücken mehrerer Tasten auf einmal ermöglicht, um ein spezielles Zeichen zu erzeugen. Um beispielsweise den akzentuierten Buchstaben <em>é</em> einzugeben, drücken Sie die <key>Compose-Taste</key>, dann <key>'</key> und dann <key>e</key>.</p>
    <p>Tastaturen haben keine festen Compose-Tasten. Stattdessen können Sie eine der vorhandenen Tasten Ihrer Tastatur als Compose-Taste festlegen.</p>

    <steps>
      <title>Eine Compose-Taste festlegen</title>
      <item>
        <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
        start typing <gui>Keyboard</gui>.</p>
      </item>
      <item>
        <p>Klicken Sie auf <gui>Tastatur</gui>, um das Panel zu öffnen.</p>
      </item>
      <item><p>Select the <gui>Shortcuts</gui> tab and click
      <gui>Typing</gui>.</p></item>
      <item><p>Klicken Sie auf <gui>Compose-taste</gui> im rechten Teilfenster.</p></item>
      <item><p>Click on <gui>Disabled</gui> and select the key you would like to
      behave as a compose key from the drop-down menu. You can choose either of
      the <key>Ctrl</key> keys, the right <key>Alt</key> key, the right
      <key>Win</key> or <key xref="keyboard-key-super">Super</key> key if
      you have one, the <key xref="keyboard-key-menu">Menu</key> key or
      <key>Caps Lock</key>. Any key you select will then only work as a compose
      key, and will no longer work for its original purpose.</p></item>
    </steps>
    <p>Sie können viele häufig vorkommende Zeichen mit der Compose-Taste eingeben, zum Beispiel:</p>
    <list>
      <item><p>Drücken Sie die <key>Compose-Taste</key>, dann <key>'</key> und dann den Buchstaben, über den Sie ein Acute-Akzentzeichen setzen wollen, zum Beispiel <em>é</em>.</p></item>
      <item><p>Drücken Sie die <key>Compose-Taste</key>, dann <key>`</key> und dann den Buchstaben, über den Sie ein Grave-Akzentzeichen setzen wollen, zum Beispiel <em>è</em>.</p></item>
      <item><p>Drücken Sie die <key>Compose-Taste</key>, dann <key>"</key> und dann den Buchstaben, über den Sie Umlautpunkte setzen wollen, zum Beispiel <em>ë</em>.</p></item>
      <item><p>Drücken Sie die <key>Compose-Taste</key>, dann <key>-</key> und dann den Buchstaben, über den Sie ein Makron setzen wollen, zum Beispiel <em>ē</em>.</p></item>
    </list>
    <p>Weitere Tastaturfolgen für die Nutzung der Compose-Taste finden Sie auf der <link href="http://de.wikipedia.org/wiki/Compose-Taste">entsprechenden Seite in Wikipedia</link>.</p>
  </section>

<section id="ctrlshiftu">
  <title>Ersetzungscodes</title>

  <p>Sie können jedes Unicode-Zeichen ausschließlich über die Tastatur mit dem numerischen Ersetzungscode des Zeichens eingegeben werden. Jedes Zeichen wird über einen vierstelligen Code definiert. Um den Code für Ihr gewünschtes Zeichen zu suchen, suchen Sie nach dem Zeichen in der Zeichentabelle und schauen Sie in der Statuszeile nach den <gui>Zeichendetails</gui>. Der Code besteht aus vier Zeichen nach <gui>U+</gui>.</p>

  <p>Um ein Zeichen über dessen Ersetzungscode einzugeben, halten Sie die <key>Strg</key>- und <key>Umschalttaste</key> gedrückt, geben Sie <key>u</key> gefolgt von dem vierstelligen Ersetzungscode ein und lassen Sie dann die <key>Strg</key>- und <key>Umschalttaste</key> los. Falls Sie oft Zeichen verwenden, die Sie nicht leichter über andere Wege erreichen können, dann sollten Sie sich den entsprechenden Ersetzungscode merken, so dass Sie ihn schneller eingeben können.</p>

</section>

  <section id="layout">
    <title>Tastaturbelegungen</title>
    <p>Sie können Ihre Tastatur so einstellen, dass Sie sich wie eine Tastatur für eine andere Sprache verhält, ungeachtet der auf den Tasten abgebildeten Buchstaben. Sie können sogar mittels eines Symbols im oberen Panel einfach zwischen verschiedenen Tastaturbelegungen wechseln. In <link xref="keyboard-layouts"/> finden Sie weitere Informationen darüber.</p>
  </section>

<section id="im">
  <title>Eingabemethoden</title>

  <p>Eine Eingabemethode erweitert die vorigen Methoden um die Eingabe von Zeichen nicht ausschließlich mit der Tastatur, sondern auch mit anderen Eingabegeräten. Beispielsweise könnten Sie Zeichen mit Mausgesten eingeben, oder japanische Zeichen mit einer lateinischen Tastatur.</p>

  <p>Um die Eingabemethode zu ändern, klicken Sie mit der rechten Maustaste in einen Text und wählen Sie im Menü <gui>Eingabemethode</gui> die Eingabemethode, die Sie verwenden wollen. Es gibt keine vorgegebene Eingabemethode, daher sollten Sie die Dokumentation zu Eingabemethoden zu Rate ziehen, um zu sehen, wie diese angewendet werden.</p>

</section>

</page>
