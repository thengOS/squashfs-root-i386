<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="commands_mouse" xml:lang="de">
  <info>
    <link type="next" xref="commands_bookmarks"/>
    <link type="guide" xref="commands#reviewing_screen_contents"/>
    <title type="sort">3. Mouse/Pointer-Related</title>
    <title type="link">Mouse/Pointer-Related</title>
    <link type="seealso" xref="howto_flat_review"/>
    <link type="seealso" xref="howto_mouse_review"/>
    <desc>Befehle zur Beeinflussung des Mauszeigers</desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>Christian.Kirbach@googlemail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>
  <title>Befehle zur Beeinflussung des Mauszeigers</title>
  <p>
    <app>Orca</app> provides several commands which can be used to manipulate
    the mouse pointer and read the contents under it. All of the commands are
    "bound" to keystrokes with one exception: Toggling Mouse Review Mode.
    Please see <link xref="howto_key_bindings">Modifying Keybindings</link>
    for information on how to bind this command to a keystroke.
  </p>
  <note style="tip">
    <p>
      In the list that follows, you will see several references to "KP". All
      "KP" keys are located on the numeric keypad. You will also notice that
      there are different keystrokes depending upon whether you are using a
      desktop or a laptop -- or more accurately, whether you are using
      <app>Orca</app>'s Desktop keyboard layout or its Laptop keyboard layout.
      For additional information, please see the
      <link xref="howto_keyboard_layout">Keyboard Layout</link> topic.
    </p>
  </note>
  <list>
    <item>
      <p>Linksklick auf das aktuelle Objekt:</p>
      <list>
        <item><p>Desktop: <keyseq><key>/ (Nmblck)</key></keyseq></p></item>
        <item><p>Laptop: <keyseq><key>Orca-Zusatztaste</key><key>7</key></keyseq></p></item>
      </list>
    </item>
    <item>
      <p>Rechtsklick auf das aktuelle Objekt:</p>
      <list>
        <item><p>Desktop: <keyseq><key>* (Nmblck)</key></keyseq></p></item>
        <item><p>Laptop: <keyseq><key>Orca-Zusatztaste</key><key>8</key></keyseq></p></item>
      </list>
    </item>
    <item>
      <p>Den Zeiger auf das aktuelle Objekt setzen:</p>
      <list>
        <item><p>Desktop: <keyseq><key>Orca-Zusatztaste</key><key>/ (Nmblck)</key></keyseq></p></item>
        <item><p>Laptop: <keyseq><key>Orca-Zusatztaste</key><key>9</key></keyseq></p></item>
      </list>
    </item>
    <item>
      <p>Move focus into or away from the current mouse over (web only):</p>
      <list>
        <item><p>Desktop: <keyseq><key>Orca-Zusatztaste</key><key>* (Nmblck)</key></keyseq></p></item>
        <item><p>Laptop: <keyseq><key>Orca-Zusatztaste</key><key>0</key></keyseq></p></item>
      </list>
    </item>
    <item>
      <p>Enable/disable mouse review mode: (Unbound)</p>
    </item>
  </list>
</page>
