<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="preferences_gecko" xml:lang="de">
  <info>
    <link type="guide" xref="preferences#application"/>
    <link type="next" xref="preferences_table_navigation"/>
    <title type="sort">1. Gecko-Navigation</title>
    <title type="link">Gecko-Navigation</title>
    <desc>Einstellungen der <app>Orca</app>-Unterstützung für <app>Firefox</app> und <app>Thunderbird</app></desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>Christian.Kirbach@googlemail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>
  <title>Einstellungen für Gecko-Navigation</title>
  <section id="page_navigation">
    <title>Seitennavigation</title>
    <p>
      The <gui>Page Navigation</gui> group of controls make it possible for you
      to customize how <app>Orca</app> presents, and allow you to interact with,
      text and other content.
    </p>
    <section>
      <title>Control caret navigation</title>
      <p>
        This checkbox toggles <app>Orca</app>'s caret navigation on and off.
        When it is on, <app>Orca</app> takes control of the caret as you arrow
        around within a page; when it is off, Gecko's native caret navigation is
        active.
      </p>
      <p>Standardwert: aktiviert</p>
      <note style="tip">
        <title>This setting can be toggled on the fly</title>
        <p>
          To toggle this setting on the fly without saving it, use
          <keyseq><key>Orca Modifier</key><key>F12</key></keyseq>.
        </p>
       </note>
    </section>
    <section>
      <title>Automatic focus mode during caret navigation</title>
      <p>
        If this checkbox is checked, <app>Orca</app> will automatically turn on
        focus mode when you use caret navigation commands to navigate to a form
        field. For example, pressing <key>Down</key> would allow you to move
        into an entry but once you had done so, Orca would switch to focus mode
        and subsequent presses of <key>Down</key> would be controlled by the web
        browser and not by Orca. If this checkbox is not checked, <app>Orca</app>
        would continue to control what happens when you press <key>Down</key>,
        thus making it possible to arrow out of the entry and continue reading.
      </p>
      <p>Standardwert: nicht aktiviert</p>
      <note style="tip">
        <title>Manually switching between browse mode and focus mode</title>
        <p>
          In order to start or stop interacting with the focused form field,
          use <keyseq><key>Orca Modifier</key><key>A</key></keyseq> to switch
          between browse mode and focus mode.
        </p>
       </note>
    </section>
    <section>
      <title>Enable structural navigation</title>
      <p>
        This checkbox toggles <app>Orca</app>'s
        <link xref="howto_structural_navigation">Structural Navigation</link>
        on and off. Structural Navigation allows you to navigate by elements
        such as headings, links, and form fields.
      </p>
      <p>Standardwert: aktiviert</p>
      <note style="tip">
        <title>This setting can be toggled on the fly</title>
        <p>
          To toggle this setting on the fly without saving it, use
          <keyseq><key>Orca Modifier</key><key>Z</key></keyseq>.
        </p>
       </note>
    </section>
    <section>
      <title>Automatic focus mode during structural navigation</title>
      <p>
        If this checkbox is checked, <app>Orca</app> will automatically turn on
        focus mode when you use structural navigation commands to navigate to a
        form field. For example, pressing <key>E</key> to move to the next entry
        would move focus there and also turn focus mode on so that your next press
        of <key>E</key> would type an "e" into that entry. If this checkbox is not
        checked, then <app>Orca</app> will leave you in browse mode and your next
        press of <key>E</key> would move you to the next entry on the page.
      </p>
      <p>Standardwert: nicht aktiviert</p>
      <note style="tip">
        <title>Manually switching between browse mode and focus mode</title>
        <p>
          In order to start or stop interacting with the focused form field,
          use <keyseq><key>Orca Modifier</key><key>A</key></keyseq> to switch
          between browse mode and focus mode.
        </p>
       </note>
    </section>
    <section>
      <title>Automatically start speaking a page when it is first loaded</title>
      <p>
        If this checkbox is checked, <app>Orca</app> will perform a Say All on
        the newly opened web page or email.
      </p>
      <p>
        Default value: checked for Firefox; not checked for Thunderbird
      </p>
    </section>
    <section>
      <title>Enable layout mode for content</title>
      <p>
        If this checkbox is checked, <app>Orca</app>'s caret navigation will respect
        the on-screen layout of the content and present the full line, including any
        links or form fields on that line. If this checkbox is not checked, <app>Orca</app>
        will treat objects such as links and form fields as if they were on separate
        lines, both for presentation and navigation.
      </p>
      <p>Standardwert: aktiviert</p>
    </section>
  </section>
  <section id="table_options">
    <title>Table Options</title>
    <note>
      <p>
        To learn more about <app>Orca's</app> options for navigating within
        tables, please see <link xref="preferences_table_navigation">Table
	Navigation Preferences</link>.
      </p>
    </note>
  </section>
  <section id="find_options">
    <title>Suchoptionen</title>
    <p>
      The <gui>Find Options</gui> group of controls make it possible for you to
      customize how <app>Orca</app> presents the results of a search conducted
      using the application's built-in search functionality.
    </p>
    <section>
      <title>Treffer während der Suche vorlesen</title>
      <p>
         If this checkbox is checked, <app>Orca</app> will read the line which
         matches your current search query.
      </p>
      <p>Standardwert: aktiviert</p>
    </section>
    <section>
      <title>Nur veränderte Zeilen während der Suche vorlesen</title>
      <p>
          If this checkbox is checked, <app>Orca</app> will not present the
          matching line if it is the same line as the previous match. This
          option is designed to prevent "chattiness" on a line with multiple
          instances of the string for which you are searching.
      </p>
      <p>Standardwert: nicht aktiviert</p>
    </section>
    <section>
      <title>Minimale Länge der Trefferübereinstimmung</title>
      <p>
        This editable spin button is where you can specify the number of
        characters which much match before <app>Orca</app> announces the
        matching line. This option is also designed to prevent "chattiness"
        as there are many matches when you first begin typing string for
        which you are searching.
      </p>
      <p>Vorgabewert: 4</p>
    </section>
  </section>
</page>
