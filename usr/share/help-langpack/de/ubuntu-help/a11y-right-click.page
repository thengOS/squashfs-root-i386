<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task a11y" version="1.0 if/1.0" id="a11y-right-click" xml:lang="de">
  <info>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <desc>Die linke Maustaste für einen Rechtsklick gedrückt halten.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="clicking"/>
    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision version="13.10" date="2013-10-25" status="review"/>
  </info>

  <title>Einen Klick mit der rechten Maustaste simulieren</title>

  <p>Sie können rechtsklicken, indem Sie die linke Maustaste gedrückt halten. Dies ist hilfreich, wenn es Ihnen schwer fällt, die Finger einer Hand einzeln zu bewegen oder wenn Ihr Zeigegerät nur über eine Taste verfügt.</p>

  <steps>
    <item>
      <if:choose>
        <if:when test="platform:unity">
          <p>Klicken Sie auf das Symbol ganz rechts in der <gui>Menüleiste</gui> und wählen Sie <gui>Systemeinstellungen</gui>.</p>
        </if:when>
        <p>Klicken Sie auf Ihren Namen in der oberen Menüleiste und wählen Sie <gui>Systemeinstellungen …</gui>.</p>
      </if:choose>
    </item>
    <item>
      <p>Öffnen Sie <gui>Zugangshilfen</gui> und wählen Sie den Reiter <gui>Zeigen und Klicken</gui>.</p>
    </item>
    <item>
      <p>Aktivieren Sie <gui>Rechtsklick simulieren</gui>.</p>
    </item>
  </steps>

  <p>Sie können anpassen, wie lange die linke Maustaste gedrückt gehalten werden muss, bevor dies als Rechtsklick erkannt wird.  Ändern Sie im Reiter <gui>Zeigen und Klicken</gui> unter dem Titel <gui>Kontextklick</gui> die <gui>Verzögerung der Annahme</gui>.</p>

  <p>Um statt einem Rechtsklick einen simulierten Rechtsklick auszuführen, halten Sie die linke Maustaste dort gedrückt, wo Sie normalerweise einen Rechtsklick ausführen würden, dann lassen Sie die Taste los. Der Mauszeiger füllt sich blau, während Sie die linke Maustaste gedrückt halten. Wenn der gesamte Zeiger blau ist, lassen Sie die Maustaste los, um einen Rechtsklick auszuführen.</p>

  <p>Einige besondere Mauszeiger-Symbole, wie die Zeiger zur Größenanpassung, ändern Ihre Farbe nicht. Sie können trotzdem wie gewohnt einen simulierten Rechtsklick ausführen, auch wenn Sie keine optische Rückmeldung erhalten.</p>

  <p>Falls Sie die <link xref="mouse-mousekeys">Tataturmaus</link> verwenden, können Sie einen Rechtsklick ausführen, indem Sie die Taste <key>5</key> auf Ihrem Ziffernblock gedrückt halten.</p>

  <if:choose>
    <if:when test="platform:unity">
    </if:when>
    <note>
      <p>In der <gui>Aktivitäten</gui>-Übersicht können Sie die Maustaste immer lang gedrückt halten, um einen Rechtsklick auszulösen, selbst wenn diese Funktion ausgeschaltet ist. Die Taste lange gedrückt zu halten funktioniert in der Übersicht etwas anders: Sie müssen die Taste für einen Rechtsklick nicht loslassen.</p>
    </note>
  </if:choose>

</page>
