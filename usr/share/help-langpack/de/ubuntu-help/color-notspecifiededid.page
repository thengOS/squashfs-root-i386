<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="color-notspecifiededid" xml:lang="de">

  <info>
    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <desc>Standard-Bildschirmprofile haben kein Kalibrierungsdatum.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="color#problems"/>
    <link type="guide" xref="color-gettingprofiles"/>
    <link type="guide" xref="color-why-calibrate"/>
    <revision version="13.10" date="2013-10-24" status="review"/>
  </info>

  <title>Warum hat ein Standardprofil eines Bildschirms kein Ablaufdatum der Kalibrierung?</title>
  <p>Das für jeden Bildschirm verwendete Farbprofil wird auf der Basis der Display-EDID automatisch erstellt. Die Display-EDID befindet sich auf einem Speicherchip im Bildschirm selbst und vermittelt lediglich eine »Momentaufnahme« der vom Monitor darstellbaren Farben zum Zeitpunkt der Herstellung. Weitere Informationen zur Farbkorrektur sind dort nicht enthalten.</p>

  <figure>
    <desc>Da die EDID nicht aktualisiert werden kann, hat sie auch kein Ablaufdatum.</desc>
    <media type="image" mime="image/png" src="figures/color-profile-default.png"/>
  </figure>

  <note style="tip">
    <p>Ein Farbprofil vom Hersteller des Bildschirms oder die Erstellung eines eigenen Profils wird zu genauerer Darstellung der Farben führen.</p>
  </note>

</page>
