<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="disk-partitions" xml:lang="de">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="13.10" date="2013-10-23" status="review"/>
    <desc>Verstehen, was Laufwerke und Partitionen sind, und die Laufwerksverwaltung verwenden, um diese zu verwalten.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

 <title>Datenträger und Partitionen verwalten</title>

  <p>Das Wort <em>Datenträger</em> wird für Speichergeräte, z.B. Festplatten, verwendet. Es kann sich auch auf einen <em>Teil</em> des Speichers auf diesem Gerät beziehen, da Sie den Speicher in mehrere Teile aufspalten können. Der Rechner stellt diesen Speicher mittels eines Prozesses, der <em>Einhängen</em> genannt wird, über das Dateisystems für den Zugriff bereit. Eingehängte Datenträger können Festplatten, USB-Datenträger, DVD-RWs, SD-Karten und weitere Medien sein. Falls ein Laufwerk eingehängt ist, können Sie Daten daraus auslesen (und eventuell auch darauf speichern).</p>

  <p>Ein eingehängter Datenträger wird oft als <em>Partition</em> bezeichnet, obwohl diese beiden Dinge nicht zwangsläufig das Gleiche bedeuten. Eine »Partition« bezieht sich auf einen <em>physischen</em> Speicherbereich auf einer einzigen Festplatte. Sobald eine Partition eingehängt wurde, kann sie als Datenträger bezeichnet werden, da Sie auf darauf befindliche Dateien zugreifen können. Sie können sich Datenträger als beschriftete, zugreifbare »Fassaden« der funktionellen »Hinterzimmer« von Partitionen und Laufwerken vorstellen.</p>

<section id="manage">
 <title>Datenträger und Partitionen mit der Laufwerksverwaltung betrachten und verwalten</title>

  <p>Sie können die Speichergeräte Ihres Rechners mit der Laufwerksverwaltung überprüfen und bearbeiten.</p>

<steps>
 <item>
  <p>Öffnen Sie das <gui>Dash</gui> und starten Sie die Anwendung <app>Festplatten-Werkzeuge</app>.</p>
 </item>
 <item>
  <p>Im Abschnitt <gui>Speichergeräte</gui> werden Festplatten, CD/DVD-Laufwerke und andere physische Laufwerke angezeigt. Klicken Sie auf das Gerät, das Sie näher betrachten möchten.</p>
 </item>
 <item>
  <p>Auf der rechten Seite liefert der Abschnitt mit dem Titel <gui>Datenträger</gui> einen kurzen Ausschnitt der Datenträger und Partitionen auf dem gewählten Gerät. Er enthält außerdem zahlreiche Werkzeuge, um diese Datenträger zu verwalten.</p>
  <p>Seien Sie vorsichtig: Es ist mit diesen Werkzeugen möglich, alle Daten von Ihrer Festplatte zu löschen.</p>
 </item>
</steps>

  <p>Ihr Rechner verfügt sehr wahrscheinlich über mindestens eine <em>primäre</em> Partition und eine einzige <em>Swap</em>-Partition. Die Swap-Partition wird vom Betriebssystem für die Speicherverwaltung verwendet und wird kaum eingehängt. Die primäre Partition enthält Ihr Betriebssystem, Anwendungen, Einstellungen und persönliche Dateien. Diese Dateien können aus Sicherheitsgründen oder aus Gründen des Komforts auf mehrere Partitionen verteilt sein.</p>

  <p>Eine primäre Partition muss die Informationen enthalten, die Ihr Rechner zum Starten oder <em>Booten</em> benötigt. Aus diesem Grund wird sie oft Boot-Partition oder Boot-Datenträger genannt. Um zu überprüfen, ob ein Datenträger boot-fähig ist, sehen Sie sich in der Laufwerksverwaltung seine <gui>Partitions-Flags</gui> an. Externe Datenträger wie USB-Speicher und CDs können ebenfalls einen boot-fähigen Datenträger enthalten.</p>

</section>

</page>
