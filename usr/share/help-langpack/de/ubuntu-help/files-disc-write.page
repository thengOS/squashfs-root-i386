<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" id="files-disc-write" xml:lang="de">
  <info>
    <link type="guide" xref="files" group="more"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ubuntu-Dokumentationsteam</name>
    </credit>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision version="16.04" date="2016-05-07" status="review"/>

    <desc>Brennen Sie Dateien und Dokumente auf eine leere CD oder DVD mit einem CD/DVD-Brennprogramm.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Dateien auf eine CD oder DVD schreiben</title>

  <p>Sie können den <gui>CD/DVD-Ersteller</gui> verwenden, um Ihre Dateien auf einer leeren CD oder DVD abzulegen. Die Möglichkeit, eine CD oder DVD zu erstellen, erscheint in der Dateiverwaltung, sobald Sie die CD in Ihren CD/DVD-Brenner einlegen. Die Dateiverwaltung lässt Sie Dateien auf andere Rechner übertragen oder erstellt <link xref="backup-why">Datensicherungen</link>, indem sie die Dateien auf einem leeren Medium ablegt. So schreiben Sie Dateien auf eine CD oder DVD:</p>

  <steps>
    <item>
     <p>Legen Sie ein leeres Medium in Ihren CD/DVD-Brenner.</p></item>
    <item>
     <p>Im Fenster <gui>Leere CD/DVD-R</gui>, das nun erscheint, wählen Sie <gui>CD/DVD-Ersteller</gui> und klicken Sie auf <gui>OK</gui>. Ein Fenster mit dem <gui>CD/DVD-Ersteller</gui> wird geöffnet.</p>
     <p>(Sie können auch auf <gui>Leere CD/DVD-R</gui> im Abschnitt <gui>Geräte</gui> in der Seitenleiste der Dateiverwaltung klicken.)</p>
    </item>
    <item>
     <p>Geben Sie im Feld <gui>CD/DVD-Name</gui> einen Namen für das Medium ein.</p>
    </item>
    <item>
     <p>Ziehen oder kopieren Sie die gewünschten Dateien in das Fenster.</p>
    </item>
    <item>
     <p>Klicken Sie auf <gui>Auf CD/DVD schreiben</gui>.</p>
    </item>
    <item>
     <p>Nehmen Sie bei <gui>Wählen Sie die zu beschreibende CD/DVD aus</gui> das leere Medium.</p>
     <p>(Sie könnten stattdessen <gui>Abbilddatei</gui> wählen. Das würde die Dateien in ein <em>CD/DVD-Abbild</em> legen, das auf Ihrem Rechner gespeichert wird. Sie können dann das Abbild zu einem späteren Zeitpunkt auf ein leeres Medium brennen.)</p>
    </item>
    <item>
     <p>Klicken Sie auf <gui>Eigenschaften</gui>, wenn Sie die Brenngeschwindigkeit, den Ort für die temporären Dateien und andere Einstellungen anpassen wollen. Die Standardeinstellungen sollten aber passen.</p>
    </item>
    <item>
     <p>Klicken Sie auf den <gui>Brennen</gui>-Knopf, um die Aufnahme zu starten.</p>
     <p>Wenn <gui>Mehrere Kopien brennen</gui> ausgewählt ist, werden Sie zum Einlegen zusätzlicher Medien aufgefordert.</p>
    </item>
    <item>
     <p>Wenn der Brennvorgang abgeschlossen ist, wird die CD/DVD automatisch ausgeworfen. Wählen Sie <gui>Weitere Kopien erstellen</gui> oder <gui>Schließen</gui>, um den Brennvorgang zu beenden.</p>
    </item>
  </steps>

  <note>
    <p>For more advanced CD/DVD burning projects, try <app>Brasero</app>. It's
    not installed by default, but you can <link href="apt:brasero">install</link>
    it from the Ubuntu package archive.</p>

    <p>For help with using <app>Brasero</app>, read the
    <link href="help:brasero">user guide</link>.</p>
  </note>

<section id="problem">
 <title>Falls die CD/DVD nicht richtig gebrannt wurde</title>
  <p>Manchmal zeichnet der Rechner die Daten nicht korrekt auf, sodass Sie die gebrannten Dateien nicht sehen können, wenn Sie das Medium ins Laufwerk einlegen.</p>

  <p>Versuchen Sie in diesem Fall die CD/DVD noch einmal zu brennen, aber diesmal mit einer geringeren Brenngeschwindigkeit, zum Beispiel 12x statt 48x. Das Brennen ist bei niedrigeren Geschwindigkeiten zuverlässiger. Sie können die Brenngeschwindigkeit wählen, indem Sie den Knopf <gui>Eigenschaften</gui> im Fenster <gui>CD/DVD-Ersteller</gui> anklicken.</p>
</section>

</page>
