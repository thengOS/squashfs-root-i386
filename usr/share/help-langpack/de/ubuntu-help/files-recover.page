<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" id="files-recover" xml:lang="de">

  <info>
    <link type="guide" xref="files" group="more"/>
    <link type="seealso" xref="files-lost"/>
    <desc>Wenn Sie Dateien entfernen, werden sie normalerweise in den Müll verschoben, können aber aus dem Müll wiederhergestellt werden.</desc>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision version="13.10" date="2013-10-23" status="review"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Eine Datei aus dem Müll wiederherstellen</title>
    <p>Wenn Sie eine Datei mit der Dateiverwaltung entfernen, wird die Datei normalerweise in den <gui>Müll</gui> verschoben und kann wiederhergestellt werden.</p>
    <steps>
      <title>So stellen Sie eine Datei aus dem Müll wieder her:</title>
        <item><p>Öffnen Sie den <gui>Starter</gui> und klicken Sie dann auf die Verknüpfung zum <app>Papierkorb</app>, welche sich am unteren Rand des Starters befindet.</p></item>
        <item><p>Wenn Ihre entfernte Datei hier erscheint, machen Sie einen Rechtsklick darauf und wählen Sie <gui>Wiederherstellen</gui>. Die Datei wird in dem Ordner wiederhergestellt in dem sie gelöscht wurde.</p></item>
    </steps>

  <p>Wenn Sie die Datei gelöscht haben, indem sie <keyseq><key>Umschalttaste</key><key>Entf</key></keyseq> gedrückt oder die Datei auf der Befehlszeile gelöscht haben, kann sie auf diese Weise nicht mehr aus dem <gui>Müll</gui> wiederhergestellt werden, weil sie dauerhaft gelöscht wurde.</p>

  <p>Es gibt einige Wiederherstellungswerkzeuge, die manchmal Dateien wiederherstellen können, die dauerhaft gelöscht wurden. Sie sind aber normalerweise nicht einfach zu bedienen. Wenn Sie eine Datei versehentlich dauerhaft gelöscht haben, ist es wohl am besten, in einem Support-Forum um Rat zu fragen, ob die Datei wiederhergestellt werden kann.</p>

</page>
