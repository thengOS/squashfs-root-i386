<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="music-player-ipodtransfer" xml:lang="de">
  <info>
    <link type="guide" xref="media#music"/>

    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <desc>Verwenden Sie eine Medienwiedergabe, um die Musiktitel auf Ihren iPod zu kopieren und diesen anschließend sicher zu entfernen.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Musiktitel erscheinen nicht auf meinem iPod, wenn ich diese darauf kopiere</title>

<p>Wenn Sie einen iPod an Ihren Rechner anschließen, erscheint dieser in Ihrer Musikwiedergabe und in der Dateiverwaltung (die Anwendung <app>Dateien</app> im <gui>Starter</gui>). Um Musiktitel auf den iPod zu kopieren, müssen Sie die Musikwiedergabe verwenden - wenn Sie die Dateiverwaltung verwenden, wird es nicht funktionieren, denn die Titel werden so nicht an den richtigen Ort kopiert. iPods haben einen speziellen Ort, um Musiktitel zu speichern, den eine Musikwiedergabe ermitteln kann, die Dateiverwaltung aber nicht.</p>

<p>Sie müssen außerdem warten, bis das Kopieren der Titel abgeschlossen ist, bevor Sie den iPod abziehen. Vor dem Abziehen des iPods sollten Sie ihn <link xref="files-removedrive">sicher entfernt</link> haben. Dadurch wird sichergestellt, dass alle Musiktitel korrekt kopiert wurden.</p>

<p>Ein weiterer Grund, warum Titel auf Ihrem iPod nicht auftauchen, könnte sein, dass die Musikwiedergabeanwendung, die Sie verwenden, das Umwandeln der Titel von einem Audioformat ins andere nicht unterstützt. Wenn Sie einen Titel in einem Audioformat kopieren, das von Ihrem iPod nicht unterstützt wird (zum Beispiel eine Ogg-Vorbis-Datei (.oga)), wird die Musikwiedergabe versuchen, die Datei in ein Format umzuwandeln, dass der iPod kennt, wie etwa MP3. Wenn die erforderliche Umwandlungsanwendung (auch Codec oder Encoder genannt) nicht installiert ist, wird die Musikwiedergabe nicht in der Lage sein, die Umwandlung durchzuführen, und den Titel deswegen nicht kopieren. Sehen Sie in der Paketverwaltung nach einem geeigneten Codec nach.</p>

</page>
