<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" id="nautilus-views" xml:lang="de">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-views"/>

    <desc>Die Standardansicht, -anordnung und -vergrößerungsstufe der Dateiverwaltung festlegen.</desc>

    <revision pkgversion="3.8" version="0.2" date="2013-04-03" status="review"/>
    <revision version="13.10" date="2013-09-12" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <its:rules xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>
  </info>

<title>Eigenschaften in <app>Dateien</app> ansehen</title>

<p>Sie können die Standardeinstellungen für die Ansicht neuer Ordner, die Sortierung, die Anzeigegröße und die Anzeige in einer Baumstruktur anpassen. Öffnen Sie die <app>Dateiverwaltung</app> und wählen Sie <guiseq><gui style="menu">Dateien</gui><gui style="menuitem">Einstellungen</gui></guiseq> in der oberen Leiste. Sie können die Einstellungen in den Reitern <gui style="tab">Ansichten</gui>  und <gui style="tab">Anzeige</gui> entsprechend anpassen.</p>

<section id="default-view">
<title>Vorgabeansicht</title>
<terms>
  <item>
    <title><gui>Neue Ordner anzeigen mit</gui></title>
    <p>Standardmäßig werden neue Ordner in der Symbolansicht dargestellt. Wenn Sie die Listenansicht vorziehen, können Sie diese hier als neue Voreinstellung setzen. Alternativ dazu können Sie die Anzeige auch für den jeweils aktuellen Ordner über die Knöpfe <gui style="button">Objekte als Liste darstellen</gui> oder <gui style="button">Objekte als Raster aus Symbolen darstellen</gui> in der Werkzeugleiste wählen.</p>
  </item>
  <item>
    <title><gui>Objekte anordnen</gui></title>
    <p>Sie können die Standardsortierung über die Auswahlliste <gui>Objekte anordnen:</gui> im Reiter »Ansichten« der Einstellungen ändern. Mögliche Sortierkriterien sind Name, Größe, Typ, Änderungsdatum, Zugriffsdatum oder Löschdatum.</p>
    <p its:locNote="TRANSLATORS: use the translation of the tooltip for the     downwards pointing arrow button that opens the preferences menu in the main     window for 'View options'">Sie können in Listenanzeige die <link xref="files-sort">Dateisortierung</link> eines einzelnen Ordners durch klicken des <media type="image" src="figures/go-down.png">Ansichtseinstellungen</media>-Knopfes und Auswahl von <gui>Nach Name</gui>, <gui>Nach Größe</gui>, <gui>Nach Typ</gui> oder <gui>Nach Änderungsdatum</gui> oder durch klicken auf die jeweiligen Spaltenüberschriften ändern. Dies passt die Darstellung nur für den aktuellen Ordner an.</p>
  </item>
  <item>
    <title><gui>Ordner vor Dateien anzeigen</gui></title>
    <p>Die Anzeige aller Ordner vor den Dateien ist nicht mehr als Standard gesetzt. Sie können dies über diese Einstellung ändern.</p>
  </item>
  <item>
    <title><gui>Verborgene Dateien und Sicherheitskopien anzeigen</gui></title>
    <p>Standardmäßig werden <link xref="files-hidden">versteckte Dateien</link> und Ordner nicht angezeigt. Sie können dies über diese Einstellung ändern.</p>
    <p its:locNote="TRANSLATORS: use the translation of the tooltip for the     downwards pointing arrow button that opens the preferences menu in the main     window for 'View options'">Sie können die Anzeige versteckter Dateien und Ordner auch nur für das aktuelle Fenster einschalten, indem Sie <gui>Verborgene Dateien anzeigen</gui> aus dem Menü <media type="image" src="figures/go-down.png">Ansichtseinstellungen</media> in der Werkzeugleiste wählen.</p>
  </item>
</terms>
</section>

<section id="icon-view-defaults">
<title>Vorgaben für Symbolansicht</title>
<terms>
  <item>
    <title><gui>Voreingestellte Vergrößerungsstufe</gui></title>
    <p its:locNote="TRANSLATORS: use the translation of the tooltip for the     downwards pointing arrow button that opens the preferences menu in the main     window for 'View options'">Sie können den Standardwert für die Größe der Symbole und Texte in der Symbolansicht über diese Einstellung ändern. Um diese Einstellung nur für einen einzelnen Ordner zu ändern, klicken Sie den <media type="image" src="figures/go-down.png">Ansichtseinstellungen</media>-Knopf in der Werkzeugleiste und wählen Sie <gui>Vergrößern</gui>, <gui>Verkleinern</gui> oder <gui>Normale Größe</gui>. Bei häufiger Nutzung dieser Optionen empfiehlt sich die Anpassung des Standardwertes.</p>
    <p>In der Symbolansicht hängt der Umfang der in den <link xref="nautilus-display#icon-captions">Symbolunterschriften</link> angezeigten Informationen von der gewählten Vergrößerungsstufe ab.</p>
   </item>
  </terms>
</section>

<section id="list-view-defaults">
<title>Vorgaben für die Listenansicht</title>
<terms>
  <item>
    <title><gui>Voreingestellte Vergrößerungsstufe</gui></title>
    <p its:locNote="TRANSLATORS: use the translation of the tooltip for the     downwards pointing arrow button that opens the preferences menu in the main     window for 'View options'">In der Listenansicht können Sie mit dieser Option die Symbole und Texte vergrößern oder verkleinern. Für einen einzelnen Ordner können Sie diese Einstellungen über den <media type="image" src="figures/go-down.png">Ansichtseinstellungen</media>-Knopf und Auswahl von <gui>Vergrößern</gui>, <gui>Verkleinern</gui> oder <gui>Normale Größe</gui> aus dem Menü anpassen.</p>
  </item>
</terms>
</section>

</page>
