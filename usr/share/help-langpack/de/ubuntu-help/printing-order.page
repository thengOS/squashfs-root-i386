<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-order" xml:lang="de">

  <info>
    <link type="guide" xref="printing#paper"/>

    <desc>Die Reihenfolge beim Drucken abgleichen oder umkehren.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Drucken von Seiten in einer anderen Reihenfolge</title>

<section id="reverse">
 <title>Rückwärts</title>
 <p>Die erste Seite wird von den meisten Druckern zuerst und die letzte Seite zuletzt ausgegeben, wodurch die Seiten in umgekehrter Reihenfolge übereinander liegen, wenn Sie diese aus dem Drucker nehmen. Falls erforderlich, können Sie diese Druckreihenfolge umkehren.</p>
 <p>So kehren Sie die Reihenfolge um:</p>
 <steps>
  <item><p>Klicken Sie auf <guiseq><gui>Datei</gui><gui>Drucken</gui></guiseq>.</p></item>
  <item><p>Aktivieren Sie im Reiter <gui>Allgemein</gui> des Druckfensters unter <em>Kopien</em> das Ankreuzfeld <gui>Umkehren</gui>. Die letzte Seite wird zuerst gedruckt usw.</p></item>
 </steps>
</section>

<section id="collate">
 <title>Zusammentragen</title>
 <p>Wenn Sie mehr als eine Kopie eines Dokuments drucken, werden die Ausdrucke nach Seitenzahl sortiert (z.B. werden erst alle Kopien von Seite 1 gedruckt, dann alle von Seite 2 usw.). Mit <em>Zusammentragen</em> werden die Seiten stattdessen in zusammengehörigen Gruppen und der richtigen Reihenfolge ausgegeben.</p>

 <p>So lassen Sie zusammentragen:</p>
 <steps>
  <item><p>Klicken Sie auf <guiseq><gui>Datei</gui><gui>Drucken</gui></guiseq>.</p></item>
  <item><p>Wählen Sie das Ankreuzfeld <gui>Zusammentragen</gui> im Reiter <gui>Allgemein</gui> des Druckfensters unter <em>Kopien</em> aus.</p></item>
 </steps>
 <!-- Need to add this image <media type="image" src="figures/reverse-collate.png"/> -->
</section>

</page>
