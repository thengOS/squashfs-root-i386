<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="session-screenlocks" xml:lang="de">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <revision pkgversion="3.8" version="0.3" date="2013-03-09" status="candidate"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <desc>Legen Sie in den Einstellungen unter <gui>Helligkeit &amp; Sperren</gui> fest, nach welcher Zeit der Bildschirm gesperrt werden soll.</desc>
    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Der Bildschirm wird zu schnell gesperrt</title>

  <p>Wenn Sie Ihren Rechner für ein paar Minuten verlassen, wird der Bildschirm automatisch gesperrt, so dass Sie Ihr Passwort eingeben müssen, um ihn wieder zu benutzen. Dies geschieht aus Sicherheitsgründen (damit sich niemand an Ihrer Arbeit zu schaffen macht, wenn Sie den Rechner unbeaufsichtigt lassen), aber es kann lästig sein, wenn der Bildschirm zu schnell gesperrt wird.</p>

  <p>So verlängern Sie die Zeit, bis der Bildschirm automatisch gesperrt wird:</p>

  <steps>
    <item><p>Klicken Sie auf das Symbol ganz rechts in der <gui>Menüleiste</gui> und wählen Sie <gui>Systemeinstellungen</gui>.</p></item>
    <item><p>Klicken Sie auf <gui>Helligkeit &amp; Sperren</gui>.</p></item>
    <item><p>Ändern Sie den Wert in der <gui>Bildschirm sperren nach</gui>-Auswahlliste.</p></item>
  </steps>

<note style="tip">
 <p>Wenn Sie nicht möchten, dass der Bildschirm jemals gesperrt wird, schalten Sie den <gui>Sperren</gui>-Schalter aus.</p>
</note>

</page>
