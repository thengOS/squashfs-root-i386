<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="user-goodpassword" xml:lang="de">

  <info>
    <credit type="author">
      <name>GNOME-Dokumentationsprojekt</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <desc>Benutzen Sie längere, kompliziertere Passwörter.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="user-accounts#passwords"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="14.04" date="2013-10-24" status="candidate"/>
  </info>

  <title>Wählen Sie ein sicheres Passwort</title>

  <note style="important">
    <p>Wählen Sie Ihre Passwörter so, dass Sie sich einerseits gut daran erinnern können, aber dass es für andere (einschließlich Programmen) schwierig ist, sie zu erraten.</p>
  </note>

  <p>Die Wahl eines guten Passworts hilft Ihnen, Ihren Rechner sicher zu halten. Falls Ihr Passwort leicht zu erraten ist, könnte es jemand herausfinden und so Zugriff auf Ihre persönlichen Daten erlangen.</p>
  <p>Auch Rechner können dazu verwendet werden, systematisch Ihr Passwort zu erraten, so dass sogar ein für einen Menschen schwer zu erratendes Passwort durchaus mit Hilfe eines Programms leicht zu knacken ist. Hier einige Tipps für die Wahl eines guten Passworts:</p>

  <list>
    <item>
      <p>Verwenden Sie eine Mischung aus Groß- und Kleinbuchstaben, Ziffern und Symbolen in Ihren Passwörtern, damit sie schwieriger zu entziffern sind. Durch die Verwendung von Symbolen und Ziffern gibt es viel mehr Möglichkeiten für die Zusammensetzung von Passwörtern, die für eine Entzifferung geprüft werden müssen.</p>
      <note>
        <p>Eine gute Methode sich ein Passwort zu wählen und zu merken, ist, die Anfangsbuchstaben eines Satzes zu verwenden, den Sie sich gut merken können. So wird aus »Flatland: A Romance of Many Dimensions« das Passwort »F:ARoMD«, »faromd« oder »f: aromd«.</p>
      </note>
    </item>
    <item>
      <p>Gestalten Sie Ihr Passwort so lang wie möglich. Je mehr Zeichen es enthält, desto schwieriger wird es für eine Person oder einen Rechner zu erraten sein.</p>
    </item>
    <item>
      <p>Verwenden Sie keine Wörter, die in einem Standardwörterbuch irgend einer Sprache zu finden sind. Passwortknacker werden diese zuerst ausprobieren. Das am häufigsten verwendete Passwort lautet »Passwort«. Dies ist wirklich sehr schnell zu erraten!</p>
    </item>
    <item>
      <p>Verwenden Sie keine persönlichen Daten wie Autokennzeichen, Geburtsdatum oder den Namen von Familienangehörigen.</p>
    </item>
    <item>
      <p>Verwenden Sie keine Substantive.</p>
    </item>
    <item>
      <p>Wählen Sie ein Passwort, das Sie schnell eingeben können, um die Möglichkeiten eines Dritten einzuschränken, bei der Eingabe das Passwort zu erkennen und sich zu merken.</p>
      <note style="tip">
        <p>Schreiben Sie Ihre Passwörter nicht auf. Sie können gefunden werden!</p>
      </note>
    </item>
    <item>
      <p>Verwenden Sie für verschiedene Zwecke verschiedene Passwörter.</p>
    </item>
    <item>
      <p>Verwenden Sie für verschiedene Konten auch verschiedene Passwörter.</p>
      <p>Falls Sie für alle Ihre Konten das gleiche Passwort verwenden, könnte jemand, der es herausgefunden hat, dadurch unmittelbaren Zugriff auf alle Ihre Konten erlangen.</p>
      <p>Es ist schwierig, sich eine Vielzahl von Passwörtern zu merken. Obwohl es nicht so sicher wie die Verwendung jeweils verschiedener Passwörter, ist es womöglich einfacher, für weniger wichtige Zugänge (wie Websites) dasselbe Passwort zu verwenden und nur für wichtige Zugänge (Wie Online-Banking und E-Mail) jeweils eigene.</p>
    </item>
    <item>
      <p>Ändern Sie Ihre Passwörter regelmäßig.</p>
    </item>
  </list>

</page>
