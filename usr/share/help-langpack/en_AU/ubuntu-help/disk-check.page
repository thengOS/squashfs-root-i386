<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="disk-check" xml:lang="en-AU">
  <info>
    <link type="guide" xref="disk"/>


    <credit type="author">
      <name>GNOME Documentation Project</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ubuntu Documentation Team</name>
    </credit>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="16.04" date="2016-05-07" status="review"/>

    <desc>Test your hard disk for problems to make sure that it's healthy.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Check your hard disk for problems</title>

<section id="disk-status">
 <title>Checking the hard disk</title>
  <p>Hard disks have a built-in health-check tool called <app>SMART</app> (Self-Monitoring, Analysis, and Reporting Technology), which continually checks the disk for potential problems. SMART also warns you if the disk is about to fail, helping you avoid loss of important data.</p>

  <p>Although SMART runs automatically, you can also check your disk's health by running the <app>Disks</app> application:</p>

<steps>
 <title>Check your disk's health using the Disks application</title>

 <item>
<if:choose>
<if:when test="platform:unity">
  <p>Open the <app>Disks</app> application from the <gui>Dash</gui>.</p>
</if:when>
  <p>Open the <app>Disks</app> application from the <gui>Activities</gui>
  overview.</p>
</if:choose>
 </item>
 <item>
  <p>From the list to the left, select the disk whose information and status
  you want to check.</p>
 </item>
 <item>
  <p><gui>Assessment</gui> should say "Disk is OK".</p>
 </item>
 <item>
  <p>Open the menu at the top right and select <gui>SMART Data &amp;
  Self-Tests</gui> to view more drive information, or to run a self-test.</p>
 </item>

</steps>

</section>

<section id="disk-not-healthy">

 <title>What if the disk isn't healthy?</title>

  <p>Even if the <gui>SMART Status</gui> indicates that the disk <em>isn't</em> healthy, there may be no cause for alarm. However, it's better to be prepared with a <link xref="backup-why">backup</link> to prevent data loss.</p>

  <p>If the status says "Pre-fail", the disk is still reasonably healthy but signs of wear have been detected which mean it might fail in the near future. If your hard disk (or computer) is a few years old, you are likely to see this message on at least some of the health checks. You should <link xref="backup-how">backup your important files regularly</link> and check the disk status periodically to see if it gets worse.</p>

  <p>If it gets worse, you may wish to take the computer/hard disk to a professional for further diagnosis or repair.</p>

</section>

</page>
