<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" id="files-delete" xml:lang="en-AU">

  <info>
    <link type="guide" xref="files"/>
    <link type="seealso" xref="files-recover"/>
    <desc>Remove files or folders you no longer need.</desc>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision version="13.10" date="2013-09-07" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
  </info>

<title>Delete files and folders</title>

  <p>If you don't want a file or folder any more, you can delete it. When you delete an item it is moved to the <gui>Rubbish Bin</gui> folder, where it is stored until you empty the rubbish bin. You can <link xref="files-recover">restore items </link> in the <gui>Rubbish Bin</gui> folder to their original location if you decide you need them, or if they were accidentally deleted.</p>

  <steps>
    <title>To send a file to the rubbish bin:</title>
    <item><p>Select the item you want to place in the rubbish bin by clicking it once.</p></item>
    <item><p>Press <key>Delete</key> on your keyboard. Alternatively, drag the item to the <gui>Rubbish Bin</gui> in the sidebar.</p></item>
  </steps>

  <p>To delete files permanently, and free up disk space on your computer, you need to empty the rubbish bin. To empty the rubbish bin, right-click <gui>Rubbish Bin</gui> in the sidebar and select <gui>Empty Rubbish Bin</gui>.</p>

  <section id="permanent">
    <title>Permanently delete a file</title>
    <p>You can immediately delete a file permanently, without having to send it to the rubbish bin first.</p>

  <steps>
    <title>To permanently delete a file:</title>
    <item><p>Select the item you want to delete.</p></item>
    <item><p>Press and hold the <key>Shift</key> key, then press the <key>Delete</key> key on your keyboard.</p></item>
    <item><p>Because you cannot undo this, you will be asked to confirm that you want to delete the file or folder.</p></item>
  </steps>

  <note style="tip"><p>If you frequently need to delete files without using the
  trash (for example, if you often work with sensitive data), you can add a
  <gui>Delete</gui> entry to the right-click menu for files and folders.
  Click <gui>Files</gui> in the menu bar, pick <gui>Preferences</gui> and select
  the <gui>Behavior</gui> tab. Select <gui>Include a Delete command that
  bypasses Trash</gui>.</p></note>

  <note><p>Deleted files on a <link xref="files#removable">removable device </link> may not be visible on other operating systems, such Windows or Mac OS. The files are still there, and will be available when you plug the device back into your computer.</p></note>

  </section>
</page>
