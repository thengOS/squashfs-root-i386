<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="net-macaddress" xml:lang="en-AU">
  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>The unique identifier assigned to network hardware.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>What is a MAC address?</title>

  <p>A <em>MAC address</em> is the unique identifier that is assigned by the manufacturer to a piece of network hardware (like a wireless card or an Ethernet card). MAC stands for <em>Media Access Control</em>, and each identifier is intended to be unique to a particular device.</p>

  <p>A MAC address consists of six sets of two characters, each separated by a colon. <code>00:1B:44:11:3A:B7</code> is an example of a MAC address.</p>

  <p>To identify the MAC address of your own network hardware:</p>
  <steps>
    <item><p>Click the <gui>network menu</gui> on the menu bar.</p></item>
    <item><p>Select <gui>Connection Information</gui>.</p></item>
    <item><p>Your MAC address will be displayed as the <gui>Hardware Address</gui>.</p></item>
  </steps>

  <p>In practice, you may need to <link xref="net-wireless-edit-connection">modify or "spoof" a MAC address</link>. For example, some internet service providers may require that a specific MAC address be used to access their service. If the network card stops working, and you need to swap a new card in, the service won't work anymore. In such cases, you would need to spoof the MAC address.</p>

</page>
