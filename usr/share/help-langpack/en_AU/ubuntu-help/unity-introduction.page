<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="unity-introduction" xml:lang="en-AU">

  <info>
    <link type="guide" xref="index" group="unity-introduction"/>
    <link type="guide" xref="shell-overview" group="#first"/>
    
    <desc>A visual introduction to the Unity desktop.</desc>
    
    <revision version="13.10" date="2013-10-23" status="outdated"/>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit> 

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>
  
  <title>Welcome to Ubuntu</title>

<p>Ubuntu features <em>Unity</em>, a reimagined way to use your computer. Unity is designed to minimize distractions, give you more room to work, and help you get things done.</p>

<p>This guide is designed to answer your questions about using Unity and your Ubuntu desktop. 
First we will take a moment to look at some of Unity's key features, and how you can use them.</p>

<section id="unity-overview">
  <title>Getting started with Unity</title>

<media type="image" src="figures/unity-overview.png">
  <p>The Unity desktop</p>
</media>

<section id="launcher-home-button">
<title>The Launcher</title>

<media type="image" src="figures/unity-launcher.png" style="floatstart floatleft">
  <p>The Launcher</p>
</media>

<p>The <gui>Launcher</gui> appears automatically when you log in to your desktop, and gives you quick access to the applications you use most often.</p>

<list style="compact">
  <item><p><link xref="unity-launcher-intro">Learn more about the Launcher.</link></p></item>
</list>

</section>

<section id="the-dash">
  <title>The Dash</title>

<p>The <gui>Ubuntu Button</gui> sits near the top left corner of the screen and is
always the top item in the Launcher.
If you click the <gui>Ubuntu Button</gui>, Unity will present you with 
an additional feature of the desktop, the <gui>Dash</gui>.</p>

<media type="image" src="figures/unity-dash.png">
  <p>The Unity Dash</p>
</media>

<p>The <em>Dash</em> is designed to make it easier to find, open, and use applications, 
files, music, and more. For example, if you type the word "document" into the <em>Search Bar</em>, 
the Dash will show you applications that help you write and edit documents. It will also show you 
relevant folders and documents that you have been working on recently.</p>


<list style="compact">
  <item><p><link xref="unity-dash-intro">Learn more about the Dash.</link></p></item>
</list>
</section>

</section>
</page>
