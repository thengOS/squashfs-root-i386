<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-lock" xml:lang="en-CA">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="seealso" xref="session-screenlocks"/>
    <link type="seealso" xref="shell-exit#lock-screen"/>
    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-09-07" status="review"/>
    <desc>Prevent other people from using your desktop when you
    go away from your computer.</desc>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Automatically lock your screen</title>

  <p>When you leave your computer, you should
  <link xref="shell-exit#lock-screen">lock the screen</link> to prevent
  other people from using your desktop and accessing your files. You will
  still be logged in and all your applications will keep running,
  but you will have to enter your password to use your computer again.
  You can lock the screen manually, but you can also have the screen
  lock automatically.</p>

  <steps>
    <item><p>Click the icon at the very right of the <gui>menu bar</gui> and select <gui>System Settings</gui>.</p></item>
    <item><p>Select <gui>Brightness &amp; Lock</gui>.</p></item>
    <item><p>Make sure <gui>Lock</gui> is switched on, then select a timeout
    from the drop-down list below. The screen will automatically lock after
    you have been inactive for this long. You can also select <gui>Screen
    turns off</gui> to lock the screen after the screen is automatically
    turned off, controlled with the <gui>Turn screen off when inactive for</gui> drop-down
    list above.</p></item>
  </steps>

</page>
