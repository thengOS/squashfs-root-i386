<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-dual-monitors-desktop" xml:lang="en-GB">

  <info>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2013</years>
    </credit>
    <desc>Set up dual monitors on your desktop computer.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="prefs-display"/>
    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision version="13.10" date="2013-10-24" status="review"/>
  </info>

<title>Connect an extra monitor</title>

 <p>To set up a second monitor with your desktop computer, connect the monitor. If your system doesn't recognise it immediately, or you would like to adjust the settings:</p>

  <steps>
  <item><p>Click your name on the menu bar and select <gui>System Settings</gui>.</p></item>

  <item><p>Open <gui>Screen Display</gui>.</p></item>

  <item><p>Click on the image of the monitor you would like to activate or deactivate, then switch it <gui>ON/OFF</gui>.</p></item>

  <item><p>The monitor with the <link xref="unity-menubar-intro">menu bar</link> is the main monitor. To change which monitor is "main", click on the top bar and drag it over to the monitor you want to set as the "main" monitor.</p></item>

  <item><p>To change the "position" of a monitor, click on it and drag it to the desired position.</p>
           <note><p>If you would like both monitors to display the same content, check the <gui>Mirror displays</gui> box.</p></note>
  </item>

  <item><p>When you are happy with your settings, click <gui>Apply</gui> and then click <gui>Keep This Configuration</gui>.</p></item>

  <item><p>To close the <gui>Displays</gui> click on the <gui>x</gui> in the top corner.</p></item>
  </steps>

</page>
