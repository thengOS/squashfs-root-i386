<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="documents-previews" xml:lang="en-GB">

  <info>
    <credit type="author copyright">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <desc>You can only preview files stored locally.</desc>
    <link type="guide" xref="documents#question"/>
    <revision pkgversion="3.5.90" date="2012-09-05" status="review"/>
    <revision version="13.10" date="2013-10-24" status="review"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Why don't some files have previews?</title>

  <p>When you open <app>Documents</app>, a preview thumbnail is displayed for documents that are stored locally. Those stored on a remote server like <em>Google Docs</em> or <em>SkyDrive</em> show as missing (or blank) preview thumbnails.</p>
  <p>If you download a <em>Google Docs</em> or <em>SkyDrive</em> document to local storage, a thumbnail will be generated.</p>

  <note style="important">
    <p>The local copy of a document downloaded from <em>Google Docs</em> or <em>SkyDrive</em> will lose its ability to be updated online. If you want to continue to edit it online, it is better not to download it.</p>
  </note>
</page>
