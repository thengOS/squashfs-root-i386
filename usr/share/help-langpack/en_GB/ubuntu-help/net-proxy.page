<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="net-proxy" xml:lang="en-GB">
  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Baptiste Mille-Mathias</name>
      <email>baptistem@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>A proxy filters websites that you look at, usually for control or security purposes.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Define proxy settings</title>

 <section id="what">
  <title>What is a proxy?</title>

  <p>A <em>web proxy</em> filters websites that you look at, according to rules specified in its policy. They are commonly used in businesses and at public wireless hotspots to control what websites you can look at, prevent you from accessing the Internet without logging in, or to do security checks on websites.</p>
 </section>

 <section id="change">
  <title>Change proxy method</title>

  <steps>
    <item>
    <p>Click the icon at the very right of the menu bar and select <gui>System Settings</gui>.</p>
   </item>
   <item>
    <p>Open <gui>Network</gui> and choose <gui>Network Proxy</gui> from the list on the left side of the window.</p>
   </item>
   <item>
    <p>Choose which proxy method you want to use among the following methods.</p>
    <terms>
     <item>
      <title>None</title>
      <p>The applications will use a direct connection to fetch the content on the web.</p>
     </item>
     <item>
      <title>Manual</title>
      <p>For each proxied protocol, define the address of a proxy and port for the protocols. The protocols are <gui>HTTP</gui>, <gui>HTTPS</gui>, <gui>FTP</gui> and <gui>SOCKS</gui>.</p>
     </item>
     <item>
      <title>Automatic</title>
      <p>A URL points to a resource, which contains the appropriate configuration for your system.</p>
     </item>
    </terms>
   </item>
  </steps>
  <p>The proxy settings will be applied to applications that use network connection to use the chosen configuration.</p>

 </section>

</page>
