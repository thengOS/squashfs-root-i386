<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shell-windows-maximize" xml:lang="en-GB">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="seealso" xref="shell-windows-tiled"/>

    <desc>Double-click or drag a titlebar, to maximise or restore a window.</desc>

    <revision pkgversion="3.4.0" date="2012-03-14" status="candidate"/>
    <revision version="13.10" date="2013-10-23" status="review"/>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Maximise and unmaximise a window</title>

  <p>You can maximise a window to take up all of the space on your desktop and unmaximise a window to restore it to its normal size. You can also maximise windows vertically along the left and right sides of the screen, so you can look at two windows at once. See <link xref="shell-windows-tiled"/> for details.</p>

  <p>To maximise a window, grab the titlebar and drag it to the top of the screen, or double-click the titlebar. To maximise a window using the keyboard, hold down <key>Ctrl</key> and <key><link xref="windows-key">Super</link></key> and press <key>↑</key>.</p>

  <p>To restore a window to its unmaximised size, drag it away from the edges of the screen. If the window is fully maximised, you can double-click the titlebar to restore it. You can also use the keyboard shortcut <keyseq><key>Ctrl</key> <key><link xref="windows-key">Super</link></key> <key>↓</key></keyseq>.</p>

  <note style="tip">
    <p>Hold down the <key>Alt</key> key and drag anywhere in a window to move it.</p>
  </note>
</page>
