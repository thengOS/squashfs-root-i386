<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shell-workspaces-switch" xml:lang="en-GB">

  <info>

    <link type="guide" xref="shell-windows#working-with-workspaces"/>
    <link type="seealso" xref="shell-workspaces"/>

    <desc>Open the workspace switcher and double-click one of the workspaces.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="14.10" date="2014-09-14" status="review"/>
    <credit type="author">
      <name>GNOME Documentation Project</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

  </info>

<title>Switch between workspaces</title>

<note>
  <p>Please see <link xref="shell-workspaces"/> about enabling workspaces.</p>
</note>

<terms>
<item>
 <title>Using the mouse:</title>
 <p>Open the <link xref="unity-launcher-intro">Launcher</link> and click the <gui>workspace switcher</gui> button near the bottom. Double-click on any window or workspace to switch to it, or press the workspace switcher button again to return to your previous workspace.</p>
</item>

<item>
 <title>Using the keyboard:</title>

 <list>
  <item>
   <p>Press <keyseq><key>Ctrl</key><key>Alt</key><key>→</key></keyseq> to move to a workspace which is to the right of the current workspace.</p>
  </item>
  <item>
   <p>Press <keyseq><key>Ctrl</key><key>Alt</key><key>←</key></keyseq> to move to a workspace which is to the left of the current workspace.</p>
  </item>
  <item>
   <p>Press <keyseq><key>Ctrl</key><key>Alt</key><key>↓</key></keyseq> to move to a workspace which is below the current workspace.</p>
  </item>
  <item>
   <p>Press <keyseq><key>Ctrl</key><key>Alt</key><key>↑</key></keyseq> to move to a workspace which is above the current workspace.</p>
  </item>
 </list>

</item>
</terms>

<media type="image" src="figures/unity-windows.png">
</media>

</page>
