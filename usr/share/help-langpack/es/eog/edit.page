<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="edit" xml:lang="es">

  <info>
    <link type="guide" xref="index#edit"/>
    <desc>Pulse <guiseq><gui>Imagen</gui><gui>Abrir con</gui></guiseq> para editar una imagen usando un editor de imágenes externo.</desc>

    <revision pkgversion="3.2" version="0.1" date="2011-09-06" status="final"/>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="editor">
      <name>Fabiana Simões</name>
      <email>fabianapsimoes@gmail.com</email>
    </credit>
    <license>
      <p>Creative Commons Compartir Igual 3.0</p>
    </license>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011, 2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2008-2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Javier F. Serrador</mal:name>
      <mal:email>serrador@openshine.com</mal:email>
      <mal:years>2006</mal:years>
    </mal:credit>
  </info>
<title>Editar o eliminar una imagen</title>

<section id="edit"><title>Editar una imagen</title>
<p>El Visor de imágenes sólo se puede usar para ver imágenes; no puede editarlas. Para ello debe usar una aplicación de edición de imágenes, tal como GIMP. Para abrir una imagen para editarla en una aplicación diferente:</p>
<steps>
 <item>
  <p>Pulse <guiseq><gui>Imagen</gui><gui>Abrir con</gui></guiseq>, o pulse con el botón derecho sobre la imagen.</p>
 </item>
 <item>
  <p>Seleccione la aplicación que usar para editar la imagen.</p>
 </item>
 <item>
  <p>Cuando haya terminado de editar, guárdela y cierre la otra aplicación.</p>
 </item>
 <item>
  <p>El <app>Visor de imágenes</app> detectará que la imagen ha cambiado y le preguntará si quiere recargarla.</p>
 </item>
</steps>
</section>

<section id="delete">
<title>Eliminar una imagen</title>
<p>Para eliminar una imagen que ya no quiera, pulse <guiseq><gui>Editar</gui><gui>Mover a la papelera</gui></guiseq> o pulse con el botón derecho del ratón sobre ella y seleccione <gui>Mover a la papelera</gui>.</p>
</section>

</page>
