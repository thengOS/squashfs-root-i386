<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="get-involved" xml:lang="es">

  <info>
    <link type="guide" xref="more-help"/>
    <desc>Cómo y dónde informar de un error en estos temas de ayuda.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>
  <title>Participar para mejorar esta guía</title>

  <section id="bug-report">
   <title>Informar de un error o proponer una mejora</title>
   <p>Esta documentación la ha creado por una comunidad de voluntarios. Si quiere participar es bienvenido. Si encuentra un problema con estas páginas de ayuda (errores ortográficos, instrucciones incorrectas, o temas que se deberían cubrir pero no están), puede enviar un <em>informe de error</em>. Para informar de un error, vaya a <link href="https://bugzilla.gnome.org/">bugzilla.gnome.org</link>.</p>
   <p>Debe registrarse para abrir un informe de error y recibir actualizaciones por correo electrónico acerca de su estado. Si aún no tiene una cuenta, simplemente pulse en el enlace <gui>New account</gui> para crear una.</p>
   <p>Una vez que tenga una cuenta, inicie sesión, pulse en <guiseq><gui>File a Bug</gui><gui>Core</gui><gui>gnome-user-docs</gui></guiseq>. Antes de informar de un error, lea la <link href="https://bugzilla.gnome.org/page.cgi?id=bug-writing.html">guía para informar de un error</link> («bug writting guidelines»), y <link href="https://bugzilla.gnome.org/browse.cgi?product=gnome-user-docs">examine</link> por si el error se parece a algún informe que ya exista.</p>
   <p>Para informar de un error, elija el componente en el menú <gui>Component</gui>. Si va a informar de un erro en esta documentación, debe elegir el componente <gui>gnome-help</gui>. Si no está seguro de a qué componente pertenece el error, elija <gui>general</gui>.</p>
   <p>Si está solucitando ayuda acerca de un tema que cree que no está cubierto, seleccione <gui>enhancement</gui> en el menú <gui>Severity</gui>. Rellene las secciones «Summary» y «Description» y pulse <gui>Commit</gui>.</p>
   <p>Su informe obtendrá un número de ID, y su estado se actualizará a medida que se trata.Gracias por ayudar a mejorar el proyecto Ayuda de GNOME.</p>
   </section>

   <section id="contact-us">
   <title>Contactar</title>
   <p>Puede enviar un <link href="mailto:gnome-doc-list@gnome.org">correo-e</link> a la lista de correo GNOME docs para aprender como colaborar con el equipo de documentación.</p>
   </section>
</page>
