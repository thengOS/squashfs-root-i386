<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-mousekeys" xml:lang="es">

  <info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="pointing"/>

    <revision pkgversion="3.8" date="2013-03-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email its:translate="no">philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    
    <desc>Activar las teclas del ratón para controlar el ratón con el teclado numérico.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Pulsar y mover el puntero del ratón usando el teclado numérico</title>

  <p>Si tiene dificultades al usar un ratón u otro dispositivo señalador, puede controlar el puntero del ratón usando el teclado numérico de su teclado. Esta característica se llama <em>teclas de ratón</em>.</p>

  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Acceso universal</gui>.</p>
      <p>Puede acceder a la vista de <gui>Actividades</gui> pulsando sobre ella, moviendo el ratón a la esquina superior izquierda de la pantalla, usando <keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq> seguido de <key>Intro</key> o usando la tecla <key xref="keyboard-key-super">Super</key>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Acceso universal</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Use the up and down arrow keys to select <gui>Mouse Keys</gui> in the
      <gui>Pointing &amp; Clicking</gui> section then press <key>Enter</key> to
      switch <gui>Mouse Keys</gui> to <gui>On</gui>.</p>
    </item>
    <item>
      <p>Compruebe que la tecla <key>Bloq Num</key> está desactivada. Ahora podrá mover el puntero del ratón usando el teclado numérico.</p>
    </item>
  </steps>

  <p>El teclado numérico es un conjunto de botones numéricos de su teclado, normalmente dispuestos en una matriz cuadrada. Si su teclado no tiene teclado numérico (por ejemplo, el teclado de un portátil), puede que tenga que mantener pulsada la tecla Función (<key>Fn</key>) y usar otras teclas de su teclado como un teclado numérico. Si usa esta característica a menudo en un portátil, puede comprar teclados numéricos USB o bluetooth externos.</p>

  <p>Cada número del teclado numérico se corresponde con una dirección. Por ejemplo, al pulsar la tecla <key>8</key> se moverá el puntero hacia arriba y al pulsar <key>2</key> se moverá hacia abajo. Al pulsar la tecla <key>5</key> se hará una pulsación con el ratón y al pulsarla dos veces rápidamente se hará una doble pulsación.</p>

  <p>La mayoría de los teclados tiene una tecla especial que permite hacer una pulsación derecha, llamada tecla de <key xref="keyboard-key-menu">Menú</key>. Sin embargo, tenga en cuenta que esta tecla responde donde está el foco del teclado, no donde está el puntero del ratón. Consulte la <link xref="a11y-right-click"/> para obtener más información sobre cómo hacer una pulsación derecha manteniendo pulsada la tecla <key>5</key> o con el botón izquierdo del ratón.</p>

  <p>Si quiere usar el teclado numérico para teclear números cuando está activada la opción de teclas del ratón, active <key>Bloq Num</key>. El ratón no se pueden controlar con el teclado numérico mientras esté activado <key>Bloq Num</key>.</p>

  <note>
    <p>Las teclas numéricas normales, situadas en una fila en la parte superior del teclado, no controlan el puntero del ratón. Solo pueden hacerlo las teclas del teclado numérico.</p>
  </note>

</page>
