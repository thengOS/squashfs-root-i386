<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="mouse-touchpad-click" xml:lang="es">

  <info>
    <link type="guide" xref="mouse"/>

    <revision pkgversion="3.7" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-10-29" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
      <years>2013, 2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Pulsar, arrastrar o desplazarse usando toques y gestos en su «touchpad».</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Pulsar, arrastrar o desplazarse con el «touchpad»</title>

  <p>Puede pulsar, hacer dobles pulsaciones, arrastrar y desplazarse usando solo su «touchpad», sin usar botones físicos aparte.</p>

<section id="tap">
  <title>Tocar para pulsar</title>

  <p>Puede tocar el «touchpad» para pulsar en lugar de usar un botón.</p>

  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Ratón y touchpad</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Ratón y touchpad</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>En la sección <gui>Touchpad</gui>, seleccione <gui>Tocar para pulsar</gui>.</p>
      <note>
        <p>La sección <gui>Touchpad</gui> sólo aparece si su equipo tiene un «touchpad».</p>
      </note>
    </item>
  </steps>

  <list>
    <item>
      <p>Para pulsar toque el «touchpad».</p>
    </item>
    <item>
      <p>Para una doble pulsación, toque dos veces.</p>
    </item>
    <item>
      <p>Para arrastrar un elemento, toque dos veces pero no levante su dedo después del segundo toque. Arrastre el elemento a donde quiera y después levante su dedo para soltarlo.</p>
    </item>
    <item>
      <p>Si su «touchpad» es multitáctil, pulse con el botón derecho del ratón usando dos dedos a la vez. De lo contrario necesitará usar los botones hardware para pulsar con el botón derecho del ratón. Para ver un método de pulsación derecha del ratón sin usar el segundo botón del ratón consulte la <link xref="a11y-right-click"/>.</p>
    </item>
    <item>
      <p>Si su «touchpad» soporta multi-toque puede <link xref="mouse-middleclick">pulsar con el botón del medio</link> pulsando con tres dedos a la vez.</p>
    </item>
  </list>

  <note>
    <p>Asegúrese de que sus dedos están separados lo suficiente al tocar o arrastrar con varios dedos. Si sus dedos están demasiado juntos su equipo puede creer que es un solo dedo.</p>
  </note>

</section>

<section id="twofingerscroll">
  <title>Desplazamiento con dos dedos</title>

  <p>Puede hacer desplazamientos usando su «touchpad» con dos dedos.</p>

  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Ratón y touchpad</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Ratón y touchpad</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>En la sección <gui>Touchpad</gui>, marque <gui>Desplazamiento con dos dedos</gui>.</p>
    </item>
  </steps>

  <p>Cuando esto está activado, al tocar y arrastrar con un dedo funcionará de forma normal, pero si arrastra dos dedos por cualquier parte de la «touchpad», se desplazará. Mueva sus dedos entre las partes superior e inferior del «touchpad» para desplazarse arriba y abajo, o mueva sus dedos a lo ancho del «touchpad» para desplazarse hacia los lados. Procure separar los dedos un poco. Si los dedos están muy juntos, parecerán un único dedo grande para el «touchpad».</p>

  <note>
    <p>El desplazamiento con dos dedos puede no funcionar en todos los touchpads.</p>
  </note>

</section>

<section id="contentsticks">
  <title>Desplazamiento natural</title>

  <p>Puede arrastrar contenido como si desplazase una hoja física de papel usando el touchpad.</p>

  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Ratón y touchpad</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Ratón y touchpad</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>En la sección <gui>Touchpad</gui>, seleccione <gui>Desplazamiento natural</gui>.</p>
    </item>
  </steps>

  <note>
    <p>Esta característica también se conoce como <em>Desplazamiento inverso</em>.</p>
  </note>

</section>

</page>
