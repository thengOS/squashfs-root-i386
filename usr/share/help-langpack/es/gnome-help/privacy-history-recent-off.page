<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-history-recent-off" xml:lang="es">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.8" date="2013-03-11" status="final"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Dejar de registrar o limitar el registro de archivos usados recientemente.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Desactivar o limitar el registro del histórico</title>
  
  <p>Tracking recently used files and folders makes it easier to find
  items that you have been working on in the file manager and in file
  dialogs in applications. You may wish to keep your file usage history
  pvivate instead, or only track your very recent history.</p>

  <steps>
    <title>Desactivar el registro del histórico</title>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> empiece a escribir <gui>Privacidad</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Privacidad</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Seleccione <gui>Uso e histórico</gui>.</p>
    </item>
    <item>
     <p>Mueva el deslizador <gui>Usados recientemente</gui> a <gui>Apagado</gui>.</p>
     <p>Para volver a activar esta característica, <gui>Active</gui> la opción <gui>Usados recientemente</gui>.</p>
    </item>
    <item>
      <p>Use el botón <gui>Limpiar el histórico reciente</gui> para limpiar el histórico inmediatamente.</p>
    </item>
  </steps>
  
  <note><p>Esta configuración no afectará a cómo su navegador guarda información sobre las páginas web que visita.</p></note>

  <steps>
    <title>Restrict the amount of time your file history is tracked</title>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> empiece a escribir <gui>Privacidad</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Privacidad</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Seleccione <gui>Uso e histórico</gui>.</p>
    </item>
    <item>
     <p>Asegúrese de que <gui>Usados recientemente</gui> está <gui>Encendido</gui>.</p>
    </item>
    <item>
     <p>Seleccione el tiempo que <gui>Conservar el histórico</gui>. Elija entre las opciones <gui>1 día</gui>, <gui>7 días</gui>, <gui>30 días</gui>, o <gui>Para siempre</gui>.</p>
    </item>
    <item>
      <p>Use el botón <gui>Limpiar el histórico reciente</gui> para limpiar el histórico inmediatamente.</p>
    </item>
  </steps>

</page>
