<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="sharing-desktop" xml:lang="es">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.14" date="2015-01-25" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email its:translate="no">jcampbell@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Permitir que otras personas vean e interactúen con su escritorio usando VNC.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Compartir su escritorio</title>

  <p>Puede permitir que otras personas vean y controlen su escritorio desde otros equipos con una aplicación de visualización de escritorios. Configure la <app>Compartición de la pantalla</app> para permitir que otros accedan a su escritorio y configure las preferencias de seguridad.</p>

  <note style="info package">
    <p>Debe tener instalado el paquete <app>Vino</app> para que la opción de <gui>Compartición de pantalla</gui> esté visible.</p>

    <if:choose xmlns:if="http://projectmallard.org/if/1.0/">
      <if:when test="action:install">
        <p><link action="install:vino" style="button">Instalar Vino</link></p>
      </if:when>
    </if:choose>
  </note>

  <steps>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Compartición</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Compartición</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>Si la<gui>Compartición</gui> está <gui>Apagada</gui>, <gui>Actívela</gui>.</p>

      <note style="info"><p>Si puede editar el texto de debajo de <gui>Nombre del equipo</gui> puede <link xref="sharing-displayname">cambiar</link> el nombre de su equipo en la red.</p></note>
    </item>
    <item>
      <p>Seleccione <gui>Compartición de la pantalla</gui>.</p>
    </item>
    <item>
      <p>Para permitir que otros vean o interactúen con su escritorio, <gui>Active</gui> la <gui>Compartición de pantalla</gui>. Esto significa que otras personas podrán intentar conectarse con su equipo y ver lo que hay en su pantalla.</p>
    </item>
    <item>
      <p>Para permitir que otros usuarios vean o interactúen con su escritorio, <gui>Active</gui> la opción <gui>Permitir control remoto</gui>. Esto permite que otras personas puedan mover su ratón, ejecutar aplicaciones y examinar los archivos en su equipo, dependiendo de la configuración de seguridad que esté actualmente usando.</p>

      <note style="tip">
        <p>Esta opción está activada de manera predeterminada cuando la <gui>Compartición de pantalla</gui> está <gui>Activada</gui>.</p>
      </note>

    </item>
  </steps>

  <section id="security">
  <title>Seguridad</title>

  <p>Es importante que considere seriamente el significado de cada opción de seguridad antes de cambiarlas.</p>

  <terms>
    <item>
      <title>Las conexiones nuevas deben solicitar acceso</title>
      <p>Si quiere poder elegir si permite o no que alguien acceda a su escritorio, seleccione <gui>Active</gui> la opción <gui>Las conexiones nuevas deben solicitar acceso</gui>. Si desactiva esta opción, no se le preguntará si quiere permitir que alguien se conecte a su equipo.</p>
      <note style="tip">
        <p>Esta opción está activada de forma predeterminada.</p>
      </note>
    </item>
    <item>
      <title>Solicitar una contraseña</title>
      <p>Para solicitar a otras personas que usen una contraseña cuando se conecten a su escritorio, seleccione <gui>Requerir contraseña</gui>. Si no usa esta opción, cualquiera puede intentar ver su escritorio.</p>
      <note style="tip">
        <p>Esta opción está desactivada de forma predeterminada, pero debería activarla y configurar una contraseña segura.</p>
      </note>
    </item>
<!-- TODO: check whether this option exists.
    <item>
      <title>Allow access to your desktop over the Internet</title>
      <p>If your router supports UPnP Internet Gateway Device Protocol and it
      is enabled, you can allow other people who are not on your local network
      to view your desktop. To allow this, select <gui>Automatically configure
      UPnP router to open and forward ports</gui>. Alternatively, you can
      configure your router manually.</p>
      <note style="tip">
        <p>This option is disabled by default.</p>
      </note>
    </item>
-->
  </terms>
  </section>

  <section id="networks">
  <title>Redes</title>

  <p>La sección <gui>Redes</gui>lista las redes a las que está actualmente conectado. Use el interruptor <gui>Encendido | Apagado</gui> junto a cada una para elegir dónde se puede compartir su escritorio.</p>
  </section>

  <section id="notification-icon">
  <title>Dejar de compartir su escritorio</title>

  <p>Puede desconectar a alguien que esté viendo su escritorio usando el <gui>icono de la notificación</gui> en la bandeja de mensajes. Para hacerlo:</p>
    <steps>
      <item><p>Abra la bandeja de mensajes pulsando <keyseq><key>Super</key><key>M</key></keyseq> o moviendo el cursor del ratón a la parte inferior de la pantalla.</p></item>
      <item><p>Pulse en el icono de <gui>Escritorio</gui> en la <gui>Bandeja de mensajes</gui>. Esto abrirá el panel de <app>Compartición</app>.</p>
      </item>
      <item><p>Seleccione <gui>Compartición de la pantalla</gui>.</p></item>
      <item><p>Mueva el deslizador de <gui>Compartición de pantalla</gui> a <gui>Apagado</gui>.</p></item>
    </steps>

  </section>


</page>
