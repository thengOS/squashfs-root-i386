<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sound-usespeakers" xml:lang="es">

  <info>
    <link type="guide" xref="media#sound"/>
    <link type="seealso" xref="sound-usemic"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-01" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Conecte los altavoces o auriculares y seleccione el dispositivo de salida de sonido predeterminado.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Molinero</mal:name>
      <mal:email>paco@byasl.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Usar altavoces o auriculares diferentes</title>

  <p>You can use external speakers or headphones with your computer. Speakers
  usually either connect using a circular TRS (<em>tip, ring, sleeve</em>) plug
  or a USB.</p>

  <p>If your speakers or headphones have a TRS plug, plug it into the
  appropriate socket on your computer. Most computers have two sockets: one for
  microphones and one for speakers (usually accompanied by a picture of headphones).
  Speakers or headphones plugged into a TRS socket are usually used
  by default. If not, see the instructions below for selecting the default
  device.</p>

  <p>Algunos equipos soportan la salida de varios canales para el sonido envolvente. Generalmente esto usa conectores TRS múltiples, con un código de color. Si no está seguro de qué conectar en cada sitio, puede probar la salida de sonido en la configuración del sonido.</p>

  <p>Si tiene unos altavoces o unos auriculares USB, o unos auriculares analógicos enchufados en una tarjeta de sonido USB, enchúfelos en cualquier puerto USB. Los altavoces USB actúan como dispositivos de sonido separados, y puede tener que especificar qué altavoces usar de manera predeterminada.</p>

  <steps>
    <title>Seleccione el dispositivo de entrada de sonido predeterminado</title>
    <item>
      <p>Abra la vista de <gui xref="shell-introduction#activities">Actividades</gui> y empiece a escribir <gui>Sonido</gui>.</p>
    </item>
    <item>
      <p>Pulse en <gui>Sonido</gui> para abrir el panel.</p>
    </item>
    <item>
      <p>En la pestaña <gui>Salida</gui>, seleccione el dispositivo que quiere usar.</p>
    </item>
  </steps>

  <p>Use el botón <gui style="button">Probar altavoces</gui> para comprobar que los altavoces funcionan y que están conectados correctamente.</p>

</page>
