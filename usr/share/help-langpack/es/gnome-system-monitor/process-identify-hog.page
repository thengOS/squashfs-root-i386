<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="process-identify-hog" xml:lang="es">
  <info>
    <revision version="0.2" pkgversion="3.11" date="2014-01-26" status="review"/>
    <link type="guide" xref="index" group="processes-tasks"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2011</years>
    </credit>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2011, 2014</years>
    </credit>

    <desc>Ordene la lista de procesos por <gui>% CPU</gui> para ver qué aplicación está consumiendo los recursos del sistema.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2014 - 2015</mal:years>
    </mal:credit>
  </info>

  <title>¿Qué programa está haciendo que el equipo vaya lento?</title>

  <p>Un programa que esté consumiendo más memoria de la que le corresponde puede ralentizar todo el equipo. Para saber qué proceso puede estar haciendo esto:</p>

  <steps>	
    <item>
      <p>Pulse en la pestaña <gui>Procesos</gui>.</p>
    </item>
    <item>
      <p>Pulse en la cabecera de la columna <gui>% CPU</gui> para ordenar los procesos por uso de CPU.</p>
      <note>
	<p>La flecha en la cabecera de la columna muestra la dirección de la ordenación; púlsela otra vez para invertir el orden. La flecha debería apuntar hacia arriba.</p>
      </note>
   </item>
  </steps>

  <p>Los procesos de la parte superior de la lista están usando el mayor porcentaje de CPU. Una vez que haya identificado cuál puede estar usando más recursos de los que debería, puede decidir si cerrar el programa en sí o si cerrar otros programas para reducir la carga de la CPU.</p>

  <note style="tip">
    <p>Un proceso que se ha colgado o que ha fallado puede usar el 100% de la CPU. Si esto sucede, deberá <link xref="process-kill">matar</link> el proceso.</p>
  </note>

</page>
