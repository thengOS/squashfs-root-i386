<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="pref-keyboard-access" xml:lang="es">

  <info>
    <revision version="0.1" date="2013-02-25" status="candidate"/>
    <link type="guide" xref="index#preferences"/>
    <link type="guide" xref="pref"/>
    <link type="seealso" xref="pref-menubar"/>

    <credit type="author copyright">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Navegar por la <app>Terminal</app> usando las teclas del teclado.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2012 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2007-2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Francisco Javier F. Serrador</mal:name>
      <mal:email>serrador@gnome.org</mal:email>
      <mal:years>2001-2006</mal:years>
    </mal:credit>
  </info>

  <title>Accesibilidad del teclado</title>

  <note style="important">
    <p>Esta teclas sólo tienen efecto en ventanas en las que la barra de menú es visible.</p>
  </note>

  <p>Puede navegar por la <app>Terminal</app> usando una combinación de teclas llamados <em>mnemónicos</em> o una tecla especial que le lleva directamente al primer menú de la barra de menú que se llama <em>tecla de acceso rápido al menú</em>.</p>

  <section id="mnemonics">
    <title>Mnemónicos</title>

    <p>Se puede acceder al menú de la <app>Terminal</app> usando la tecla <key>Alt</key> y una letra del elemento del menú. La letra que necesita usar para acceder al menú estará subrayada cuando pulse la tecla <key>Alt</key>.</p>

    <p>Por ejemplo, vaya al menú <gui style="menu">Editar</gui> usando <keyseq><key>Alt</key><key>E</key></keyseq>. De igual manera, puede acceder al menú <gui style="menu">Archivo</gui> usando <keyseq><key>Alt</key><key>F</key></keyseq>.</p>

    <p>Para activar los mnemónicos:</p>

    <steps>
      <item>
        <p>Seleccione <guiseq><gui style="menu">Editar</gui> <gui style="menuitem">Preferencias</gui> <gui style="tab">General</gui></guiseq>.</p>
      </item>
      <item>
        <p>Marque <gui style="checkbox">Activar los mnemónicos</gui>.</p>
      </item>
    </steps>

  </section>

  <section id="menu-accelerator">
    <title>Tecla de acceso rápido al menú</title>

    <p>Esta configuración muestra el menú <gui style="menu">Archivo</gui> cuando se pulsa la tecla de acceso rápido al menú. Generalmente esta tecla es <key>F10</key> de manera predeterminada.</p>

    <steps>
      <item>
        <p>Seleccione <guiseq><gui style="menu">Editar</gui> <gui style="menuitem">Preferencias</gui> <gui style="tab">Atajos</gui></guiseq>.</p>
      </item>
      <item>
        <p>Marque <gui style="checkbox">Activar la tecla de acceso rápido al _menú</gui>.</p>
      </item>
    </steps>

  </section>

</page>
