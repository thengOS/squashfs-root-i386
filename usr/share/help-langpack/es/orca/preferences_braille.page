<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="preferences_braille" xml:lang="es">
  <info>
    <title type="sort">3. Braille</title>
    <title type="link">Braille</title>
    <desc>Configurar el soporte de <app>Orca</app> para dispositivos Braille</desc>
    <link type="guide" xref="preferences#orca"/>
    <link type="next" xref="preferences_key_echo"/>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Creative Commons Compartir Igual 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2012 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>
  <title>Preferencias Braille</title>
  <section id="braillesupport">
    <title>Activar el soporte de Braille</title>
    <p>Esta casilla de verificación conmuta entre si <app>Orca</app> puede usar un dispositivo Braille. Si BrlTTY no se está ejecutando, <app>Orca</app> se recuperará con elegancia y no se comunicará con el dispositivo Braille.</p>
    <p>Valor predeterminado: no marcado</p>
    <note style="tip">
      <p>Si configura BrlTTY más tarde, deberá reiniciar <app>Orca</app> para poder usar Braille.</p>
    </note>
  </section>
  <section id="contractedbraille">
    <title>Activar Braille contraído</title>
    <p>Orca suporta Braille contraído a través del proyecto liblouis. Ya que muchas distribuciones incluyen liblouis, probablemente tenga acceso automático de Braille contraído en <app>Orca</app>.</p>
    <p>Para activar el Braille contraído en un sistema donde se ha instalado liblouis, asegúrese de que la casilla de verificación <gui>Activar Braille contraído</gui> está marcada. Después, elija la tabla de traducción que quiera de la caja combinada <gui>Tabla de contracción</gui>.</p>
    <p>Valor predeterminado: no marcado</p>
  </section>
  <section id="rolenames">
    <title>Nombres de roles abreviados</title>
    <p>Esta casilla determina la manera en que se muestran los nombres de rol de los controles y puede usarse para ayudar a conservar el espacio real en la línea braille. Por ejemplo, si un deslizador tiene el foco, se mostrará la palabra «deslizador» si nombres de roles abreviados no está marcada; si estuviera marcada, se mostraría «desl».</p>
    <p>Valor predeterminado: no marcado</p>
  </section>
  <section id="eolindicator">
    <title>Desactivar símbolo de fin de línea</title>
    <p>Al marcar esta casilla se indica a <app>Orca</app> que no presente la cadena «$l» al final de una línea de texto.</p>
    <p>Valor predeterminado: no marcado</p>
  </section>
  <section id="verbosity">
    <title>Cantidad de información</title>
    <p>Este grupo de botones de radio determina la cantidad de información Braille que se mostrará en ciertas situaciones. Por ejemplo, si se establece a «detallada», se mostrará el atajo de teclado y la información de nombre del rol. Esta información no se muestra en el modo abreviado.</p>
    <p>Valor predeterminado: <gui>extendida</gui></p>
  </section>
  <section id="selectionandhyperlink">
    <title>Indicadores de selección e hiperenlace</title>
    <p>Los grupos de botones de radio <gui>Indicador de selección</gui> y <gui>Indicador de hiperenlace</gui> le permiten configurar el comportamiento de <app>Orca</app> cuando se muestra texto seleccionado e hiperenlaces. De forma predeterminada, cuando encuentre alguno de ellos, <app>Orca</app> «subrayará» dicho texto en su línea braille con los puntos  7 y 8. Si lo desea, puede cambiar el indicador para que sea solo punto 7, solo punto 8 o no se presente ningún indicador.</p>
    <p>Valor predeterminado: <gui>Puntos 7 y 8</gui></p>
    <note style="tip">
      <title>Indicadores de atributos del texto</title>
      <p>Opcionalmente, puede hacer que los atributos de texto se indiquen en Braille. Puede activar esta característica y elegir qué atributos le interesan en la <link xref="preferences_text_attributes">página <gui>Atributos de texto</gui></link> del diálogo de preferencias.</p>
    </note>
  </section>
</page>
