<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-calibrationdevices" xml:lang="es">

  <info>

    <link type="guide" xref="color#calibration"/>

    <desc>Permitimos muchos dispositivos de calibración.</desc>

    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>

    <revision version="13.10" date="2013-09-07" status="review"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>¿Qué instrumentos de medición del color son compatibles?</title>

  <p>GNOME depende del sistema de gestión de colores Argyll para admitir instrumentos de colorimetría. Por ende, se admiten los instrumentos de medición de pantalla siguientes:</p>

  <list>
    <item><p>Gretag-Macbeth i1 Pro (espectrómetro)</p></item>
    <item><p>Gretag-Macbeth i1 Monitor (espectrómetro)</p></item>
    <item><p>Gretag-Macbeth i1 Display 1, 2 or LT (colorímetro)</p></item>
    <item><p>X-Rite i1 Display Pro (colorímetro)</p></item>
    <item><p>X-Rite ColorMunki Design or Photo (espectrómetro)</p></item>
    <item><p>X-Rite ColorMunki Create (colorímetro)</p></item>
    <item><p>X-Rite ColorMunki Display (colorímetro)</p></item>
    <item><p>Pantone Huey (colorímtero)</p></item>
    <item><p>MonacoOPTIX (colorímtero)</p></item>
    <item><p>ColorVision Spyder 2 y 3 (colorímetro)</p></item>
    <item><p>Colorímtero HCFR (colorímetro)</p></item>
  </list>

  <note style="tip">
   <p>El  Pantone Huey es actualmente el hardware más barato y con el mejor soporte en Linux.</p>
  </note>

  <p>Gracias a Argyll hay también un cierto número de espectrómetros reflectivos de lectura de punto y tira permitidos para ayudarle a calibrar y caracterizar impresoras.</p>

  <list>
    <item><p>X-Rite DTP20 «Pulso» (espectrómetro tipo «golpe»)</p></item>
    <item><p>Muestrario digital X-Rite DTP22 (espectrómetro reflexivo de tipo punto)</p></item>
    <item><p>X-Rite DTP41 (espectrómetro reflexivo de lectura de punto y franja)</p></item>
    <item><p>X-Rite DTP41T (espectrómetro reflexivo de lectura de punto y franja)</p></item>
    <item><p>X-Rite DTP51 (espectrómetro reflexivo de lectura de punto)</p></item>
  </list>

</page>
