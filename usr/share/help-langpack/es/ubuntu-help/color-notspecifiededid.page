<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="color-notspecifiededid" xml:lang="es">

  <info>
    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <desc>Los perfiles de la pantalla predeterminada no tienen fecha de calibración.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="color#problems"/>
    <link type="guide" xref="color-gettingprofiles"/>
    <link type="guide" xref="color-why-calibrate"/>
    <revision version="13.10" date="2013-10-24" status="review"/>
  </info>

  <title>¿Por qué no tienen los perfiles de la pantalla predeterminada una caducidad de la calibración?</title>
  <p>El perfil de color predeterminado para cada monitor se genera automáticamente basándose en la información del EDID de la pantalla, la cual se almacena en un chip de memoria dentro del monitor. El EDID solamente muestra una imagen congelada de los colores disponibles que el monitor era capaz de mostrar cuando se fabricó, y no contiene mucha más información para la corrección del color.</p>

  <figure>
    <desc>Como el EDID no se puede actualizar, no tiene fecha de caducidad.</desc>
    <media type="image" mime="image/png" src="figures/color-profile-default.png"/>
  </figure>

  <note style="tip">
    <p>Obtener un perfil del fabricante del monitor o crear un perfil usted mismo permitiría una corrección del color más exacta.</p>
  </note>

</page>
