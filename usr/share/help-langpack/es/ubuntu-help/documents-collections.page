<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="documents-collections" xml:lang="es">

  <info>
    <credit type="author copyright">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <desc>Agrupar documentos relacionados en una colección.</desc>
    <link type="guide" xref="documents#print"/>
    <revision pkgversion="3.5.90" date="2012-09-05" status="review"/>
    <revision version="13.10" date="2013-10-24" status="review"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Crear colecciones de documentos</title>

  <p><app>Documentos</app> le permite agrupar documentos de diferentes tipos en un lugar denominado <em>colección</em>. Si tiene documentos que están relacionados, puede agruparlos para hacer que sea más fácil localizarlos. Por ejemplo, si realizó un viaje de negocios en el que hizo una presentación, sus transparencias, el itinerario de los vuelos (un archivo PDF), la hoja de cálculo con el presupuesto y otros documentos combinados PDF/ODF, se pueden agrupar en una colección.</p>

  <p>Para crear o añadir a una colección:</p>
  <list>
   <item><p>Pulse el botón <gui>✓</gui>.</p></item>
   <item><p>En modo de selección, compruebe los documentos a recopilar.</p></item>
   <item><p>Pulse el botón <gui>+</gui> en la barra de botones.</p></item>
   <item><p>En la lista de colecciones, haga clic sobre <gui>Añadir</gui> y escriba un nuevo nombre de colección o seleccione una colección existente. Los documentos seleccionados se añadirán a la colección.</p></item>
  </list>

  <note>
   <p>Las colecciones no se comportan como carpetas y sus jerarquías: <em>no puede meter colecciones dentro de colecciones.</em></p>
  </note>

  <p>Para eliminar una colección:</p>
  <list>
   <item><p>Pulse el botón <gui>✓</gui>.</p></item>
   <item><p>En modo de selección, compruebe la colección a ser eliminada.</p></item>
   <item><p>Haga clic en el botón de la papelera en la barra de botones. La colección se borrará, dejando los documentos originales.</p></item>
  </list>

</page>
