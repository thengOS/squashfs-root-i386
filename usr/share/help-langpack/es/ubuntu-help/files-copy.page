<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="files-copy" xml:lang="es">

  <info>
    <link type="guide" xref="files"/>
    <desc>Copiar o mover elementos a una carpeta nueva.</desc>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-15" status="review"/>
    <revision version="14.10" date="2014-09-14" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Copiar o mover archivos y carpetas</title>

 <p>Un archivo o carpeta se puede copiar o mover a una nueva ubicación arrastrándolo y soltándolo con el ratón, usando las órdenes de copiar y pegar, o usando atajos de teclado.</p>

 <p>Por ejemplo, tal vez desee copiar una presentación en una memoria extraíble para poder llevarla consigo a su trabajo. O podría hacer una copia de seguridad de un documento antes de hacerle modificaciones (y después usar la copia anterior si no le gustan sus modificaciones).</p>

 <p>Estas instrucciones se aplican a carpetas y archivos. Copie y mueva archivos y carpetas exactamente de la misma forma.</p>

<steps ui:expanded="false">
<title>Copiar y pegar archivos</title>
<item><p>Seleccione el archivo que quiere copiar pulsando una vez.</p></item>
<item><p>Haga clic derecho y elija <gui>Copiar</gui>, o pulse <keyseq><key>Ctrl</key><key>C</key></keyseq>.</p></item>
<item><p>Navegue a otra carpeta, donde quiera copiar el archivo.</p></item>
<item><p>Pulse con el botón secundario del ratón y elija <gui>Pegar</gui> para terminar de copiar el archivo, o bien, presione <keyseq><key>Ctrl</key><key>V</key></keyseq>. Ahora habrá una copia del archivo en la carpeta original y en la otra carpeta.</p></item>
</steps>

<steps ui:expanded="false">
<title>Corte y pegue archivos para moverlos</title>
<item><p>Seleccione el archivo que quiere mover, pulsando una vez sobre él.</p></item>
<item><p>Haga clic derecho y elija <gui>Cortar</gui>, o pulse <keyseq><key>Ctrl</key><key>X</key></keyseq>.</p></item>
<item><p>Navegue a otra carpeta, donde quiera copiar mover el archivo.</p></item>
<item><p>Pulse con el botón secundario y elija <gui>Pegar</gui> para terminar de mover el archivo, o bien, pulse <keyseq><key>Ctrl</key><key>V</key></keyseq>. El archivo desaparecerá de su ubicación original y se desplazará a la otra carpeta.</p></item>
</steps>

<steps ui:expanded="false">
<title>Arrastrar archivos para copiarlos o moverlos</title>
<item><p>Abra el gestor de archivos y vaya a la carpeta que contiene el archivo que quiera copiar.</p></item>
<item><p>Haga clic en <gui>Archivos</gui> en la barra superior, seleccione <gui>Nueva ventana</gui> (o presione <keyseq><key>Ctrl</key><key>N</key></keyseq>) para abrir una segunda ventana. En la nueva ventana, navegue hasta la carpeta donde desea mover o copiar el archivo.</p></item>
<item>
 <p>Pulse y arrastre el archivo desde una ventana hacia la otra. Esto <em>lo moverá</em> si el destino está en el <em>mismo</em> dispositivo, o <em>lo copiará</em> si el destino está en un dispositivo <em>diferente</em>.</p>
 <p>Por ejemplo, si arrastra un archivo desde una memoria USB a su carpeta Personal, se copiará porque está arrastrando desde un dispositivo a otro.</p>
 <p>Puede forzar el copiado del archivo manteniendo presionada la tecla <key>Ctrl</key> mientras arrastra, o forzarlo a moverse manteniendo presionada la tecla <key>Shift</key> mientras arrastra.</p>
 </item>
</steps>

<note>
 <p>No puede copiar o mover un archivo o carpeta que es de  <em>solo lectura</em>. Algunas carpetas son de solo lectura para evitar que pueda realizar cambios al contenido de las mismas. Puede cambiar elementos de solo lectura  <link xref="nautilus-Archivo-propiedades-permisos">cambiando los permisos del archivo </link>.</p>
</note>

</page>
