<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="look-resolution" xml:lang="es">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="seealso" xref="look-display-fuzzy"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision version="13.10" date="2013-10-22" status="review"/>

    <desc>Cambiar la resolución de la pantalla y su orientación (rotación).</desc>
    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Cambiar el tamaño o la rotación de la pantalla</title>

<p>Puede cambiar qué tan grandes (o detalladas), aparecen las cosas en la pantalla cambiando la <em>resolución de pantalla</em>. Puede cambiar la manera en la que se muestran las cosas (por ejemplo, si tiene una pantalla giratoria) cambiando la <em>rotación</em>.</p>

<steps>
  <item><p>Pulse en el icono situado en el extremo derecho de la barra de menús y seleccione <gui>Configuración del sistema</gui>.</p></item>
  <item><p>Abra <gui>Monitores</gui>.</p></item>
  <item><p>Si tiene varias pantallas y no se encuentran configuradas en espejo, puede establecer diferentes parámetros para cada pantalla. Seleccione una pantalla en el área de vista previa.</p></item>
  <item><p>Seleccione la resolución y rotación que quiere.</p></item>
  <item><p>Pulse <gui>Aplicar</gui>. Las nuevas configuraciones se aplicarán durante 30 segundos antes de ser revertidas automáticamente. De esa manera, si no funciona su pantalla con las nuevas configuraciones, sus configuraciones anteriores se restaurarán. Si prefiere sus nuevas configuraciones, pulse <gui>Mantener esta configuración</gui>.</p></item>
</steps>

<note style="tip">
 <p>Cuando se utiliza otra pantalla, como un proyector, se debe detectar automáticamente para que pueda cambiar su configuración en la misma forma que la pantalla normal. Si esto no sucede, simplemente pulse en <gui>Detectar  pantalla</gui>.</p>
</note>

<section id="resolution">
<title>Resolución</title>
<p>La resolución es el número de píxeles (puntos en la pantalla) que pueden mostrarse en cada dirección. Cada resolución tiene una <em>relación de aspecto</em>, la relación de la anchura con la altura. Las pantallas anchas usan una relación de aspecto de 16:9, mientras que las pantallas tradicionales usan 4:3. Si elige una resolución que no coincide con la relación de aspecto de su pantalla, la imagen será reducida con un efecto «letterbox» (con bandas negras), para evitar la distorsión.</p>
<p>Puede elegir la resolución que prefiera desde la lista desplegable <gui>Resolución</gui>. Si elige una que no es adecuada para su pantalla, ésta puede <link xref="look-display-fuzzy">aparecer borrosa o pixelada</link>.</p>
</section>

<section id="rotation">
<title>Rotación</title>
<p>En algunos portátiles puede rotar físicamente la pantalla en muchas direcciones. Es útil para poder cambiar la rotación de la pantalla. Puede elegir la rotación que quiera para su pantalla de la lista desplegable <gui>Rotación</gui>.</p>
</section>

</page>
