<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mouse-touchpad-click" xml:lang="es">
<info>
    <link type="guide" xref="mouse"/>
    <desc>Hacer clic, arrastrar o desplazarse usando toques y gestos en el touchpad.</desc>

    <revision pkgversion="3.7.1" version="0.3" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-10-22" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
</info>

<title>Hacer clic, arrastrar o desplazarse con el touchpad</title>

<p>Puede pulsar, pulsar dos veces, arrastrar y desplazarse utilizando solamente su touchpad, sin botones de hardware separados.</p>

<steps>
  <item><p>Pulse en el icono situado en el extremo derecho de la <gui>barra de menús</gui> y seleccione <gui>Configuración del sistema</gui>.</p></item>
  <item><p>Abra <gui>Ratón y touchpad</gui>.</p></item>
  <item><p>En la sección <gui>Touchpad</gui>, marque <gui>Activar pulsaciones del ratón con el touchpad</gui>.</p>
    <note>
      <p>La sección <gui>Touchpad</gui> solo aparece si su sistema tiene un panel táctil.</p>
    </note>
  </item>
</steps>

<list>
<item><p>Para pulsar, toque en el touchpad.</p></item>
<item><p>Para hacer una pulsación doble, toque dos veces.</p></item>
<item><p>Para arrastrar un elemento, toque dos veces rápidamente pero no levante su dedo después del segundo toque. Arrastre el elemento a la posición deseada, y luego levante su dedo para soltarlo.</p></item>
<item><p>Si su touchpad es multitoque, haga una pulsación derecha tocando con dos dedos a la vez. Si no, todavía necesitará de botones de hardware para hacer pulsaciones derechas. Consulte <link xref="a11y-right-click"/> para un método para hacer pulsaciones derechas sin un botón secundario del ratón.</p></item>
<item><p>Si su touchpad es multitoque, haga una <link xref="mouse-middleclick">pulsación central</link> tocando con tres dedos a la vez.</p></item>
</list>

<note><p>Al tocar o arrastrar con varios dedos, asegúrese de que sus dedos están separados lo suficiente. Si sus dedos están muy juntos, su equipo podría creer que son un solo dedo.</p></note>

<section id="twofingerscroll">

<title>Desplazamiento con dos dedos</title>

<p>Puede desplazarse mediante el touchpad usando dos dedos.</p>

<steps>
  <item><p>Pulse en el icono situado en el extremo derecho de la <gui>barra de menús</gui> y seleccione <gui>Configuración del sistema</gui>.</p></item>
  <item><p>Abra <gui>Ratón y touchpad</gui>.</p></item>
  <item><p>En la sección <gui>Touchpad</gui>, marque <gui>Desplazamiento con dos dedos</gui>.</p></item>
</steps>

<!--
<p>Select <gui>Edge scrolling</gui> under <gui>Scrolling</gui> to
scroll using the edge of your touchpad. When this is selected, dragging
your finger up and down along the right side of your touchpad will
scroll vertically. If you also select <gui>Enable horizontal
scrolling</gui>, dragging your finger left and right along the
bottom of your touchpad will scroll horizontally.</p>
-->

<p>Cuando esta opción está seleccionada, los toques y arrastres con un dedo funcionarán normalmente, pero si se arrastran dos dedos a lo largo de cualquier zona del panel táctil (touchpad), se realizará un desplazamiento vertical. Si se selecciona <gui>Activar desplazamiento horizontal</gui>, se puede hacer un desplazamiento horizontal moviendo los dedos a la derecha o a la izquierda. Recuerde espaciar sus dedos ligeramente, de no ser así el sistema puede interpretarlos como un solo dedo en su panel táctil (touchpad).</p>

<note><p>El desplazamiento con dos dedos puede no funcionar en todos los touchpads.</p></note>

</section>
<section id="contentsticks">

<title>Adherencia del contenido a los dedos</title>

<p>Puede arrastrar el contenido como si deslizar una pieza física de papel utilizando el touchpad.</p>

<steps>
  <item><p>Pulse en el icono situado en el extremo derecho de la <gui>barra de menús</gui> y seleccione <gui>Configuración del sistema</gui>.</p></item>
  <item><p>Abra <gui>Ratón y touchpad</gui>.</p></item>
  <item><p>En la sección <gui>Touchpad</gui>, seleccione <gui>Adherencia del contenido a los dedos</gui>.</p></item>
</steps>

<note><p>Esta características también se conoce como <em>Desplazamiento natural</em> o <em>Desplazamiento inverso</em>.</p>
</note>
</section>

</page>
