<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-list" xml:lang="es">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-list"/>

    <desc>Controlar qué información se visualiza en columnas en la vista de lista.</desc>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>
  </info>

<title>Preferencias de las columnas en la lista del gestor de archivos</title>

<p>Puede mostrar hasta nueve columnas de información diferentes en la vista de lista del gestor de archivos. Pulse en <gui>Archivos</gui> en la barra de menús, elija <gui>Preferencias</gui> y seleccione la pestaña <gui>Columnas de la lista</gui> para marcar o desmarcar las columnas que quiere ver.</p>

<note style="tip">
<p>Use los botones <gui>Subir</gui> y <gui>Bajar</gui> para seleccionar el orden en el que aparecerán las columnas seleccionadas.</p>
</note>
<terms>
 <item>
  <title><gui>Nombre</gui></title>
  <p>El nombre de las carpetas y archivos en la carpeta que se está visualizando.</p>
 </item>
 <item>
  <title><gui>Tamaño</gui></title>
  <p>El tamaño de una carpeta viene dado por el número de elementos que contiene dicha carpeta. El tamaño de un archivo viene dado en bytes, KB o MB.</p>
 </item>
 <item>
  <title><gui>Tipo</gui></title>
  <p>Visualizado como carpeta, o tipo de archivo como documento PDF, imagen JPEG, sonido MP3, etcétera.</p>
 </item>
 <item>
  <title><gui>Modificado</gui></title>
  <p>Shows when the file was last modified.</p>
 </item>
 <item>
  <title><gui>Propietario</gui></title>
  <p>El nombre del usuario propietario de la carpeta o archivo.</p>
 </item>
 <item>
  <title><gui>Grupo</gui></title>
  <p>El grupo al que pertenece el archivo. En equipos domésticos, cada usuario pertenece a su propio grupo. Los grupos se usan a veces en entornos empresariales, donde los usuarios pueden pertenecer a ciertos grupos en función de su departamento o proyecto.</p>
 </item>
  <item>
  <title><gui>Permisos</gui></title>
  <p>Muestra los permisos de acceso del archivo. Ej. <gui>drwxrw-r--</gui></p>
  <list>
   <item>
    <p>El primer carácter <gui>-</gui> es el tipo de archivo. <gui>-</gui> significa archivo normal y <gui>d</gui> significa directorio (carpeta).</p>
   </item>
   <item>
     <p>Los tres caracteres siguientes <gui>rwx</gui> especifican los permisos para el usuario propietario del archivo.</p>
   </item>
   <item>
     <p>Los tres siguientes <gui>rw-</gui> especifican los permisos para todos los miembros del grupo al que pertenece el archivo.</p>
   </item>
   <item>
    <p>Los tres últimos caracteres de la columna <gui>r--</gui> especifican los permisos para todos los demás usuarios del sistema.</p>
   </item>
  </list>
  <p>Cada carácter tiene el siguiente significado:</p>
  <list>
    <item><p>r : permiso de lectura.</p></item>
    <item><p>w : permiso de escritura.</p></item>
    <item><p>x : permiso de ejecución.</p></item>
    <item><p>- : sin permiso.</p></item>
    </list>
 </item>
 <item>
  <title><gui>Tipo MIME</gui></title>
  <p>Muestra el tipo MIME del elemento.</p>
 </item>
 <item>
  <title><gui>Ubicación</gui></title>
  <p>La ruta a la ubicación del archivo.</p>
 </item>
</terms>

</page>
