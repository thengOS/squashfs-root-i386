<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="net-macaddress" xml:lang="es">
  <info>
    <link type="guide" xref="net-general"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>El identificador único asignado al hardware de red.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>¿Qué es una dirección MAC?</title>

  <p>Una <em>dirección MAC</em> es un identificador único asignado por el fabricante a un dispositivo de red (como un adaptador inalámbrico o una tarjeta Ethernet). MAC significa <em>Control de acceso de medios</em> —en inglés <em>Media Access Control</em>—, y se pretende que cada identificador sea único por cada dispositivo.</p>

  <p>Una dirección MAC consiste en seis grupos de dos caracteres, cada uno de ellos separado por dos puntos. <code>00:1B:44:11:3A:B7</code> es un ejemplo de dirección MAC.</p>

  <p>Para identificar la dirección MAC de su hardware de red:</p>
  <steps>
    <item><p>Pulse en el <gui>menú de red</gui> en la barra de menús.</p></item>
    <item><p>Seleccione <gui>Información de la conexión</gui>.</p></item>
    <item><p>Su dirección MAC se mostrará como la <gui>Dirección hardware</gui>.</p></item>
  </steps>

  <p>En la práctica, puede que necesite <link xref="net-wireless-edit-connection">modificar o «enmascarar» una dirección MAC</link>. Por ejemplo, algunos proveedores de servicios de Internet pueden exigir que se use una dirección MAC específica para acceder a sus servicios. Si la tarjeta de red deja de funcionar, y necesita cambiar a una nueva tarjeta, el servicio no volverá a funcionar. En estos casos necesitará enmascarar la dirección MAC.</p>

</page>
