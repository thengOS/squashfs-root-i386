<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="net-wireless-hidden" xml:lang="es">
  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="seealso" xref="net-wireless-edit-connection#wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <credit type="author">
      <name>Equipo de documentación de Ubuntu</name>
    </credit>

    <desc>Pulse en el <gui>menú de red</gui> en la barra de menús y elija <gui>Conectar a una red inalámbrica oculta</gui>.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Conectar a una red inalámbrica oculta</title>

<p>Es posible configurar una red inalámbrica de modo que esté «oculta». Las redes ocultas no se muestran en la lista de redes que se muestran al pulsar en el menú de red en la barra de menús (o la lista de redes inalámbricas en cualquier otro equipo). Para conectarse a una red inalámbrica oculta:</p>

<steps>
 <item>
  <p>Pulse en el <gui>menú de red</gui> en la barra de menús y elija <gui>Conectar a una red inalámbrica oculta</gui>.</p>
 </item>
 <item>
  <p>En la ventana que aparece, escriba el nombre de la red, elija el tipo de seguridad inalámbrica y pulse <gui>Conectar</gui>.</p>
 </item>
</steps>

<p>Quizá deba comprobar las configuraciones de la estación base inalámbrica o enrutador para ver el nombre de la red. A veces se le llama <em>BSSID</em> (identificador de conjunto básico de servicio), y luce como esto: <gui>02:00:01:02:03:04</gui>.</p>

<p>Debería comprobar también la configuración de seguridad de la estación base inalámbrica. Busque términos como WEP y WPA.</p>

<note>
 <p>Puede que piense que ocultar su red inalámbrica mejorará la seguridad impidiendo conectarse a la gente que no la conozca. En la práctica, esto no es así; la red será ligeramente más difícil de encontrar pero aún así seguirá siendo detectable.</p>
</note>

</page>
