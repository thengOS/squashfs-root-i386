<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="question" version="1.0 if/1.0" id="power-hibernate" xml:lang="es">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspendfail"/>

    <desc>La hibernación está desactivada de manera predeterminada porque no tiene soporte adecuado.</desc>
    <revision pkgversion="3.6.0" date="2012-08-14" status="review"/>
    <revision version="16.04" date="2016-04-30" status="review"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>
    <credit type="editor">
      <name>Equipo de documentación de Ubuntu</name>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>¿Cómo hibernar mi equipo?</title>

<p>Cuando <em>hiberna</em> el equipo, se guardan todas las aplicaciones y los documentos y el equipo se apaga por completo, por lo que no utiliza ningún tipo de energía, pero sus aplicaciones y documentos seguirán estando abiertos al encender el equipo de nuevo.</p>

<p>Desafortunadamente, la hibernación<link xref="power-suspendfail">no funciona</link> en muchos casos, lo que puede provocar la pérdida de datos si espera que sus documentos y aplicaciones se abran de nuevo al volver a encender el equipo. Por lo tanto, la hibernación está desactivada de forma predeterminada.</p>

<section id="test-hibernate">
<title>Pruebe si la hibernación funciona</title>

<note style="important">
  <title>Siempre guarde su trabajo antes de hibernar</title>
  <p>Debe guardar todo su trabajo antes de hibernar el equipo, en caso de que algo salga mal y sus aplicaciones y documentos no pueden recuperarse cuando encienda de nuevo el equipo.</p>
</note>

<p>Puede usar la línea de órdenes para probar si la hibernación funciona en su equipo.</p>

<steps>
  <item>
    <if:choose>
      <if:when test="platform:unity">
        <p>Abra el <app>Terminal</app> pulsando <keyseq><key>Ctrl</key><key>Alt</key><key>t</key></keyseq> o buscando <input>terminal</input> en el <gui>Tablero</gui>.</p>
     </if:when>
     <p>Abra el <app>Terminal</app> buscando <input>terminal</input> en el <gui>Resumen de actividades</gui>.</p>
  </if:choose>
  </item>
  <item>
    <p>Escriba <cmd>sudo pm-hibernate</cmd> en la terminal y presione <key>Intro</key>.</p>
    <p>Escriba su contraseña cuando se le pida.</p>
  </item>
  <item>
    <p>Después de que se apague su equipo, enciéndalo de nuevo. ¿Se volvieron a abrir sus aplicaciones abiertas?</p>
    <p>Si la hibernación no funciona, compruebe que su partición de intercambio es al menos del mismo tamaño que su RAM disponible.</p>
  </item>
</steps>

</section>

<section id="enable-hibernate">
<title>Activar la hibernación</title>

  <p>Sí la prueba de hibernación funciona, puede continuar y utilizar la orden <cmd>sudo pm-hibernate</cmd> cuando quiera hibernar.</p>

  <p>También puede activar la opción de hibernar en los menús. Para hacerlo, use su editor de texto favorito para crear el archivo <file>/etc/polkit-1/localauthority/50-local.d/com.ubuntu.enable-hibernate.pkla</file>. Añada lo siguiente al archivo y guárdelo:</p>

<code its:translate="no">
[Re-enable hibernate by default in upower]
Identity=unix-user:*
Action=org.freedesktop.upower.hibernate
ResultActive=yes

[Re-enable hibernate by default in logind]
Identity=unix-user:*
Action=org.freedesktop.login1.hibernate;org.freedesktop.login1.handle-hibernate-key;org.freedesktop.login1;org.freedesktop.login1.hibernate-multiple-sessions;org.freedesktop.login1.hibernate-ignore-inhibit
ResultActive=yes
</code>

</section>

</page>
