<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-hotcomputer" xml:lang="es">

  <info>
    <link type="guide" xref="power#problems"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision version="13.10" date="2013-09-20" status="review"/>

    <desc>Los equipos, en general, se calientan pero pueden sobrecalentarse, lo que puede causar daños.</desc>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Mi equipo se calienta demasiado</title>

<p>Muchos equipos se calientan pasado un tiempo, y algunos pueden calentarse mucho. Esto es normal: es simplemente parte de la forma en la que se refrigeran. Sin embargo, si su equipo se calienta demasiado puede ser síntoma de que se está sobrecalentando, lo que podría causar daños.</p>

<p>Muchos portátiles se calientan razonablemente cuando se usan durante un tiempo. Normalmente no es nada de lo que preocuparse  - los equipos generan un montón de calor y los portátiles son muy compactos, por lo que necesitan eliminar su calor rápidamente y, como resultado, su carcasa se calienta. Sin embargo, algunos portátiles se calientan demasiado, y su uso puede resultar incómodo. Esto normalmente es el resultado de un sistema de refrigeración mal diseñado. A veces puede usar accesorios de refrigeración que se colocan debajo del portátil y proporcionan una refrigeración más eficiente.</p>

<p>Si tiene un equipo de escritorio que se nota caliente al tacto, puede que tenga una refrigeración insuficiente. Si esto le preocupa, puede comprar más ventiladores o comprobar que los ventiladores y rejillas de ventilación estén libres de polvo y otras obstrucciones. Es posible que desee considerar la posibilidad de tener el equipo en un lugar mejor ventilado; si está en un espacio cerrado (por ejemplo, en un armario), el sistema de refrigeración puede no ser capaz de eliminar el calor y hacer circular el aire fresco con suficiente rapidez.</p>

<p>Algunas personas se preocupan por los riesgos que pudiera tener para la salud el uso de portátiles calientes. Hay indicios de que el uso prologando de portátiles calientes en su regazo podría reducir la fertilidad (masculina), así como informes de personas que han padecido pequeñas quemaduras (en casos extremos). Si le preocupan esos posibles problemas, le recomendamos que consulte con un profesional médico. Por supuesto, siempre puede simplemente no ponerse el portátil en su regazo.</p>

<p>La mayoría de los equipos modernos se apagarán solos si se calientan demasiado, para prevenir que se dañen. Si su equipo se apaga constantemente, ésta puede ser la razón. Si su equipo se sobrecalienta, probablemente necesite llevarlo a reparación.</p>

</page>
