<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="power-othercountry" xml:lang="es">

  <info>
    <link type="guide" xref="power#problems"/>
    <desc>Su equipo funcionará, pero puede necesitar un cable de corriente diferente o un adaptador de viaje.</desc>

    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>¿Mi equipo funcionará con una fuente de energía en otro país?</title>

<p>Otros países utilizan suministros de energía de diferentes voltajes (generalmente 110V o 220-240V) y frecuencias AC (generalmente 50 Hz o 60 Hz). El equipo debería funcionar en otro país con un transformador adecuado de corriente. Puede necesitar también un adaptador de clavija de pared (enchufe).</p>

<p>Si tiene un portátil, todo lo que necesita hacer es conseguir el enchufe adecuado para su adaptador de corriente. Muchos portátiles se entregan con más de un enchufe para su adaptador, así que puede que ya tenga el adecuado. Si no es así, conectar el existente en un adaptador de viaje estándar será suficiente.</p>

<p>Si tiene un equipo de escritorio, también puede obtener un cable con un enchufe diferente, o utilizar un adaptador de viaje. En este caso, sin embargo, es posible que necesite cambiar el interruptor de voltaje en la fuente de alimentación del equipo, si es que existe. Muchos equipos no tienen un interruptor de este tipo, y trabajará sin problemas con cualquier tensión. Mire la parte posterior del equipo y busque el zócalo del cable de alimentación. En algún lugar cercano, puede haber un pequeño interruptor marcado con «110» o «230» (por ejemplo). Conmútelo si es necesario.</p>

<note style="warning">
  <p>Tenga cuidado al cambiar los cables de alimentación o al usar adaptadores de viaje. Si es posible, apague todo en primer lugar.</p>
</note>

</page>
