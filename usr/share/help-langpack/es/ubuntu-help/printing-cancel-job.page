<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-cancel-job" xml:lang="es">

  <info>
    <link type="guide" xref="printing#problems"/>

    <desc>Cancelar un trabajo de impresión pendiente y eliminarlo de la cola.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email its:translate="no">philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email its:translate="no">jwcampbell@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email its:translate="no">jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
  </info>

<title>Cancelar, pausar o liberar una tarea de impresión</title>

<p>Puede cancelar un trabajo de impresión pendiente y eliminarlo de la cola en la configuración de la impresora.</p>

<section id="cancel-print-job">

<title>Cancelar un trabajo de impresión</title>

  <p>Si accidentalmente inició la impresión de un documento, puede cancelarla de forma que no malgaste ni tinta ni papel.</p>

<steps>
  <title>Cómo cancelar un trabajo de impresión:</title>
  <item><p>Pulse en el icono situado en el extremo derecho de la <gui>barra de menús</gui> y seleccione <gui>Configuración del sistema</gui>.</p></item>
  <item><p>Pulse <gui>Impresoras</gui>.</p></item>
  <item><p>Pulse el botón <gui>Mostrar trabajos</gui> en el lado derecho del diálogo de <gui>Impresoras</gui>.</p></item>
  <item><p>Cancele la tarea de impresión pulsando en el botón «Detener» (símbolo de cuadrado).</p></item>
 </steps>

  <p>Si esto no cancela el trabajo como espera, intente mantener pulsado el botón <gui>Cancelar</gui> de su impresora.</p>

  <p>Como último recurso, especialmente si tiene un trabajo de impresión grande con un montón de páginas que no se cancela, retire el papel de la bandeja de papel de la impresora. La impresora debe darse cuenta de que no hay papel y dejará de imprimir. A continuación, puede tratar de cancelar el trabajo de impresión, o intente apagar la impresora y vuelva a encenderla.</p>

 <note style="warning">
  <p>Tenga cuidado de no dañar la impresora al extraer el papel, aunque si tiene que tirar con fuerza del papel para extraerlo, probablemente debería dejarlo donde está.</p>
 </note>
</section>

<section id="pause-release-print-job">
<title>Pausar y liberar un trabajo de impresión</title>
  <p>Si desea pausar o liberar un trabajo de impresión, solo ha de ir al cuadro de trabajos en los ajustes de impresión y pulsar el botón apropiado.</p>

 <steps>
  <item><p>Pulse en el icono situado en el extremo derecho de la <gui>barra de menús</gui> y seleccione <gui>Configuración del sistema</gui>.</p></item>
  <item><p>Pulse <gui>Impresoras</gui>.</p></item>
  <item><p>Pulse el botón <gui>Mostrar trabajos</gui> en el lado derecho del diálogo <gui>Impresoras</gui> para pausar o liberar el trabajo de impresión según sus necesidades.</p></item>
 </steps>

</section>
</page>
