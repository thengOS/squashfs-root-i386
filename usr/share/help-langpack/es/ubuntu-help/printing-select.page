<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-select" xml:lang="es">

  <info>
    <link type="guide" xref="printing#paper"/>
    <desc>Imprimir solo páginas específicas, o solo un rango de páginas.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Imprimir solo ciertas páginas</title>

<p>Para imprimir solo ciertas páginas del documento:</p>
<steps>
 <item><p>Pulse <guiseq><gui>Archivo</gui><gui>Imprimir</gui></guiseq>.</p></item>
 <item><p>En la pestaña <gui>General</gui>, en la ventana <gui>Imprimir</gui>, elija <gui>Páginas</gui> de la sección <gui>Rango</gui>.</p></item>
<item><p>Escriba los números de las páginas que quiere imprimir en el cuadro de texto, separados por comas. Utilice un guion para indicar un rango de páginas.</p></item>
</steps>

<note>
  <p>Por ejemplo, si escribe «1,3,5-7,9» en la caja de texto <gui>Páginas</gui>, se imprimirán las páginas 1,3,5,6,7.</p>
 <media type="image" src="figures/printing-select.png"/>
</note>

</page>
