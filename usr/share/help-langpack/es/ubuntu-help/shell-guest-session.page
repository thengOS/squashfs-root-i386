<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="shell-guest-session" xml:lang="es">

  <info>
    <link type="guide" xref="shell-overview#desktop" group="#last"/>
    <link type="guide" xref="user-accounts#manage"/>

    <desc>Permite a un amigo o colega compartir su equipo de una manera segura.</desc>
    <revision version="16.04" date="2016-05-08" status="review"/>
    <credit type="author">
      <name>Gunnar Hjalmarsson</name>
      <email>gunnarhj@ubuntu.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Lanzar una sesión de invitado restringida</title>

<section id="restricted">
<title>Sesión temporal con privilegios restringidos</title>

<p>De vez en cuando un amigo, familiar o colega puede que le pida prestado su equipo. La característica <app>Sesión de invitado</app> de Ubuntu le proporciona una forma adecuada, con un alto nivel de seguridad, de prestar su equipo a otra persona. Una sesión de invitado se puede lanzar desde la pantalla de inicio de sesión o desde una sesión normal. Si ya ha abierto una sesión, pulse el icono en el extremo derecho de la <gui>barra de menú</gui> y seleccione <gui>Sesión de invitado</gui>. Esto bloqueará la pantalla de su propia sesión y abrirá la sesión de invitado.</p>

<p>Un invitado no puede ver las carpetas personales de otros usuarios y por defecto cualquier dato guardado o configuración modificada será eliminada/restaurada al cerrar la sesión. Esto significa que cada sesión se inicia con un entorno limpio, no afectado por lo que invitados anteriores hayan podido hacer.</p>

</section>

<section id="customize">
<title>Personalización</title>

<p>El tutorial en línea «<link href="https://help.ubuntu.com/community/CustomizeGuestSession">Customize Guest Session</link>» (personalizar la sesión de invitado, disponible en inglés) explica cómo personalizar la apariencia y comportamiento.</p>

</section>

<section id="disable">
<title>Desactivar la característica.</title>

<p>If you prefer to not allow guest access to your computer, you can disable the <app>Guest
Session</app> feature. To do so, press <keyseq><key>Ctrl</key><key>Alt</key><key>T</key></keyseq>
to open a terminal window, and then run this command:</p>

<p><cmd its:translate="no">sudo sh -c 'printf "[Seat:*]\nallow-guest=false\n" &gt;/etc/lightdm/lightdm.conf.d/50-no-guest.conf'</cmd></p>

<p>La orden crea un pequeño archivo de configuración. Para volver a activar la <app>Sesión de invitado</app>, simplemente elimine ese archivo:</p>

<p><cmd its:translate="no">sudo rm /etc/lightdm/lightdm.conf.d/50-no-guest.conf</cmd></p>

</section>

</page>
