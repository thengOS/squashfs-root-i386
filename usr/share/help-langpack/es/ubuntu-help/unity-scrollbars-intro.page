<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="unity-scrollbars-intro" xml:lang="es">
  <info>
    <link type="guide" xref="shell-overview#desktop"/>
    <link type="seealso" xref="mouse-touchpad-click"/>

    <desc>Las barras de desplazamiento superpuestas son las bandas naranjas estrechas que aparecen en los documentos largos.</desc>
    <revision version="13.10" date="2013-10-23" status="outdated"/>

    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>¿Qué son las barras de desplazamiento superpuestas?</title>

<p>Ubuntu incluye <em>barras de desplazamiento flotantes</em>, que ocupan menos espacio en pantalla que las tradicionales, dándole enfoque al contenido. Estas barras, aunque inspiradas en los dispositivos móviles, están diseñadas para funcionar adecuadamente con un ratón.</p>

<p>Algunas aplicaciones, como por ejemplo Firefox y LibreOffice, todavía no son compatibles con las nuevas barras de desplazamiento.</p>

<section id="using">
  <title>Utilizar las barras de desplazamiento</title>

  <p>Las barras de desplazamiento superpuestas aparecen como una delgada banda naranja situada en el borde de una zona por la que es posible desplazarse. La posición de la barra de desplazamiento corresponde a la posición de la pantalla en el contenido desplazable y la longitud de la banda corresponde a la del contenido; cuando más corta sea la banda, más largo será el contenido.</p>

  <p>Mueva el puntero del ratón sobre cualquier punto del borde desplazable del contenido para que aparezca el <gui>control deslizante en miniatura</gui>.</p>

  <list>
    <title>Formas de utilizar las barras de desplazamiento:</title>
    <item><p>Arrastrar el <gui>control deslizante en miniatura</gui> hacia arriba o hacia abajo para mover la posición de la pantalla exactamente al punto que desee.</p></item>
    <item><p>Pulse en la barra de desplazamiento para mover la posición de la pantalla hacia el punto exacto que prefiera.</p></item>
  </list>
</section> 

</page>
