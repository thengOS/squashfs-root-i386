<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="video-dvd-restricted" xml:lang="es">

  <info>
    <link type="guide" xref="media#videos" group="#last"/>
    <link type="seealso" xref="video-dvd"/>
    
    <desc>La mayoría de los DVD comerciales están cifrados y no se pueden reproducir sin un software de descifrado.</desc>
    
    <revision version="16.04" date="2016-04-23" status="review"/>
    <credit type="author">
      <name>Proyecto de documentación de Ubuntu</name>
      <email>ubuntu-doc@lists.ubuntu.com</email>
    </credit>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>¿Cómo activo los códecs restrictivos para reproducir DVD?</title>

<p>Ubuntu no admite la reproducción de DVD de forma predeterminada debido a restricciones legales y técnicas. La mayoría de los DVD comerciales están cifrados y necesitan el uso de software de descifrado para poder reproducirlos.</p>

<section id="fluendo">
 <title>Usar Fluendo para reproducir DVD legalmente</title>
 <p>Puede comprar un decodificador comercial de DVD que pueda tratar la protección anticopia en <link href="apt:fluendo-dvd">Fluendo</link>. Funciona con Linux y su uso debería ser legal en todos los países.</p>
</section>

<section id="restricted">
 <title>Usar otro software de descifrado</title>

 <note style="warning"><p>En algunos países, el uso sin licencia del siguiente software de descifrado no está permitido por la ley. Verifique que tiene usted el derecho a usarlo.</p></note>

 <steps>
  <item><p>Install <link href="apt:libdvdnav4">libdvdnav4</link>, <link href="apt:libdvdread4">libdvdread4</link>,
<link href="apt:gstreamer1.0-plugins-bad">gstreamer1.0-plugins-bad</link>, <link href="apt:gstreamer1.0-plugins-ugly">gstreamer1.0-plugins-ugly</link>, and <link href="apt:libdvd-pkg">libdvd-pkg</link>.</p></item>
  <item><p>Open the Dash and launch a <app>Terminal</app>.</p></item>
  <item><p>Run the command</p>
    <p><cmd its:translate="no">sudo dpkg-reconfigure libdvd-pkg</cmd></p>
    <p>and confirm in order to install <em>libdvdcss2</em>.</p>
  </item>
</steps>
</section>
</page>
