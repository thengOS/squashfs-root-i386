<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" its:version="1.0" type="topic" style="task" id="contribute" xml:lang="fr">
<info>
  <desc>Aider à améliorer <app>Déjà Dup</app></desc>
  <link type="guide" xref="index#about"/>
</info>
<title>S'impliquer</title>

<p>Donc, vous voulez aider à rendre <app>Déjà Dup</app> encore meilleur ? Excellent ! Voici quelques suggestions.</p>

<section id="report">
<title>Signaler des problèmes</title>

<p>Vous avez découvert une faille ? Vous avez une idée concernant une nouvelle fonctionnalité ? Rapportez simplement cela <link href="https://bugs.launchpad.net/deja-dup/+filebug">en tant que bogue</link> (de préférence en anglais). Patientez et une réponse vous sera apportée.</p>

<p>Veillez noter que les rapports de bugs se font uniquement en anglais.</p>
</section>

<section id="talk">
<title>Contactez-nous</title>

<p>Nous ne mordons pas. Envoyez un courriel à la <link href="mailto:deja-dup-list@gnome.org">liste de diffusion</link>.</p>

<p>Ou <link href="https://mail.gnome.org/mailman/listinfo/deja-dup-list">souscrivez à la liste</link>. Le trafic est vraiment très faible.</p>

<p>Notez que la liste est uniquement en anglais.</p>
</section>

<section id="translate">
<title>Traduire</title>

<p>Parlez-vous anglais et une autre langue ? Bien ! Vous pouvez aider à traduire <app>Déjà Dup</app> grâce à <link href="https://translations.launchpad.net/deja-dup">cette interface web</link>. C'est à la fois simple et pratique.</p>
</section>

<section id="support">
<title>Assistance</title>

<p>Vous aimez aider les gens ? <link href="https://answers.launchpad.net/deja-dup">Répondez aux questions d'utilisateurs</link> et autres demandes de soutien.</p>

<p>Ceci est un autre excellent moyen pour les non-anglophones de s'impliquer, puisque vous pouvez répondre aux questions posées dans votre langue.</p>
</section>

<section id="code">
<title>Développement</title>

<p>Il y a toujours des bogues à corriger. Si vous voulez savoir ce qui doit être fait, demandez le nous sur la liste de diffusion tel que décrit ci-dessus.</p>
</section>

</page>
