<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="clock-world" xml:lang="fr">

  <info>
    <link type="guide" xref="clock" group="#last"/>
    <link type="seealso" href="help:gnome-clocks/index">
      <title>Clocks Help</title>
    </link>

    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhill@gnome.org</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Display times in other cities under the calendar.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Add a world clock</title>

  <p>Use <app>Clocks</app> to add times in other cities.</p>

  <note>
    <p>This requires the <app>Clocks</app> application to be installed.</p>
    <p>Most distributions come with <app>Clocks</app> installed by default.
    If yours does not, you may need to install it using your distribution
    package manager.</p>
 </note>

  <p>To add a world clock:</p>

  <steps>
    <item>
      <p>Cliquez sur l'horloge située dans la barre supérieure,</p>
    </item>
    <item>
      <p>Click the <gui>Add world clocks...</gui> link under the calendar to
      launch <app>Clocks</app>.</p>

    <note>
       <p>If you already have one or more world clocks, click on one and
       <app>Clocks</app> will launch.</p>
    </note>

    </item>
    <item>
      <p>In the <app>Clocks</app> window, click
      <gui style="button">New</gui> button or
      <keyseq><key>Ctrl</key><key>N</key></keyseq> to add a new city.</p>
    </item>
    <item>
      <p>Start typing the name of the city into the search.</p>
    </item>
    <item>
      <p>Select the correct city or the closest location to you from the
      list.</p>
    </item>
    <item>
      <p>Press <gui style="button">Add</gui> to finish adding the city.</p>
    </item>
  </steps>

  <p>Refer to the <link href="help:gnome-clocks">Clocks Help</link> for more
  of the capabilities of <app>Clocks</app>.</p>

</page>
