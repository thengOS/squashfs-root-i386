<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-brightness" xml:lang="fr">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="seealso" xref="power-whydim"/>
    <link type="seealso" xref="a11y-contrast"/>

    <revision pkgversion="3.8" version="0.4" date="2013-03-28" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>

    <credit type="author">
      <name>Le projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="author editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Change the screen brightness to make it more readable in bright
    light.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Set screen brightness</title>

  <p>Depending on your hardware, you can change the brightness of your screen to
  save power or to make the screen more readable in bright light.</p>

  <p>To change the brightness of your screen, click the
  <gui xref="shell-introduction#yourname">system menu</gui> on the right side
  of the top bar and adjust the screen brightness slider to the value you want
  to use. The change should take effect immediately.</p>

  <note style="tip">
    <p>Many laptop keyboards have special keys to adjust the brightness. These
    often have a picture that looks like the sun. Hold down the <key>Fn</key>
    key to use these keys.</p>
  </note>

  <p>You can also adjust the screen brightness by using the <gui>Power</gui> panel.</p>

  <steps>
    <title>To set the screen brightness using the Power panel:</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui>
      overview and start typing <gui>Power</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Power</gui> to open the panel.</p>
    </item>
    <item>
      <p>Adjust the <gui>Screen brightness</gui> slider to the value you want to
      use. The change should take effect immediately.</p>
    </item>
  </steps>

  <note style="tip">
    <p>If your computer features an integrated light sensor, the screen
    brightness will automatically be adjusted for you. You can disable automatic
    screen brightness in the <gui>Power</gui> panel.</p>
  </note>

  <p>If it is possible to set the brightness of your screen, you can also have
  the screen dim automatically to save power. For more information, see
  <link xref="power-whydim"/>.</p>

</page>
