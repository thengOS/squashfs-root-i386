<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="keyboard-shortcuts-set" xml:lang="fr">

  <info>
    <link type="guide" xref="keyboard"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-23" status="review"/>
    <revision pkgversion="3.9.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.17.90" date="2015-08-30" status="candidate"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Julita Inca</name>
      <email its:translate="no">yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Juanjo Marín</name>
      <email its:translate="no">juanj.marin@juntadeandalucia.es</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email its:translate="no">tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <desc>Définir ou modifier les raccourcis clavier dans les paramètres <gui>Clavier</gui>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Définition de raccourcis clavier</title>

<p>Pour modifier la ou les touches d'un raccourci clavier :</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Keyboard</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Clavier</gui> pour ouvrir le panneau.</p>
    </item>
    <item>
      <p>Cliquez sur l'onglet <gui>Raccourcis</gui>.</p>
    </item>
    <item>
      <p>Sélectionnez une catégorie dans le panneau de gauche, et le rang pour l'action désirée à droite. La définition du raccourci actuel devient <gui>Nouvel Accélérateur…</gui>.</p>
    </item>
    <item>
      <p>Effectuez la combinaison de touches désirée ou appuyez sur <key>Retour Arrière</key> pour l'effacer.</p>
    </item>
  </steps>


<section id="defined">
<title>Raccourcis courants prédéfinis</title>
  <p>Un certain nombre de raccourcis pré-configurés et qui peuvent être modifiés sont regroupés dans ces catégories :</p>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Les lanceurs</title>
  <tr>
	<td><p>Démarrer le navigateur d'aide</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Lancer la calculatrice</p></td>
	<td><p>Calculatrice</p></td>
  </tr>
  <tr>
	<td><p>Démarrer le client de messagerie</p></td>
	<td><p>Courriel</p></td>
  </tr>
  <tr>
	<td><p>Démarrer le navigateur Web</p></td>
	<td><p>WWW</p></td>
  </tr>
  <tr>
	<td><p>Dossier personnel</p></td>
	<td><p>Explorer</p></td>
  </tr>
  <tr>
	<td><p>Recherche</p></td>
	<td><p>Recherche</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Navigation</title>
  <tr>
	<td><p>Déplacer la fenêtre vers l'espace de travail 1</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre vers l'espace de travail 2</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre vers l'espace de travail 3</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre vers l'espace de travail 4</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre d'un espace de travail vers la gauche</p></td>
	<td><p><keyseq><key>Maj</key><key>Ctrl</key><key>Alt</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre d'un espace de travail vers la droite</p></td>
	<td><p><keyseq><key>Maj</key><key>Ctrl</key><key>Alt</key><key>→</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre d'un espace de travail vers le haut</p></td>
	<td><p><keyseq><key>Maj</key><key xref="keyboard-key-super">Logo</key> <key>Page haut</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre d'un espace de travail vers le bas</p></td>
	<td><p><keyseq><key>Maj</key><key>Logo</key><key>page bas</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Basculer entre les applications</p></td>
	<td><p><keyseq><key>Logo</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
  <td><p>Basculement entre fenêtres</p></td>
  <td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Basculer entre les fenêtres d'une application</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Basculer les contrôles système</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Tab</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Basculer entre les fenêtres directement</p></td>
	<td><p><keyseq><key>Alt</key><key>Échap</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Basculer entre les fenêtres d'une application directement</p></td>
	<td><p><keyseq><key>Alt</key><key>F6</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Changer les contrôles système directement</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Échap</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Masquer toutes les fenêtres normales</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Basculer vers l'espace de travail 1</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Basculer vers l'espace de travail 2</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Basculer vers l'espace de travail 3</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Basculer vers l'espace de travail 4</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Basculer vers l'espace de travail de gauche</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Basculer vers l'espace de travail de droite</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>→</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Basculer vers l'espace de travail au dessus</p></td>
	<td><p><keyseq><key>Logo</key><key>Page haut</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Basculer vers l'espace de travail en dessous</p></td>
	<td><p><keyseq><key>Logo</key><key>Page bas</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Captures d'écran</title>
  <tr>
	<td><p>Prendre une capture d'écran</p></td>
	<td><p><key>Impr. écran</key></p></td>
  </tr>
  <tr>
	<td><p>Prendre une capture d'écran d'une fenêtre</p></td>
	<td><p><keyseq><key>Alt</key><key>Impr. écran</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Prendre une capture d'une partie de l'écran </p></td>
	<td><p><keyseq><key>Maj</key><key>Impr. écran</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Copier une capture d'écran dans le presse-papiers</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Impr. écran</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Copier une capture d'écran d'une fenêtre dans le presse-papiers</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Impr. écran</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Copier une capture d'une partie de l'écran dans le presse-papiers</p></td>
	<td><p><keyseq><key>Maj</key><key>Ctrl</key><key>Impr. écran</key></keyseq></p></td>
  </tr>
  <tr>
  <td><p>Enregistrer une vidéo d'écran</p></td>
  <td><p><keyseq><key>Maj</key><key>Ctrl</key><key>Alt</key><key>R</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Son et média</title>
  <tr>
	<td><p>Couper le volume</p></td>
	<td><p>Extinction du son</p></td>
  </tr>
  <tr>
	<td><p>Baisser le volume</p></td>
	<td><p>Diminution du volume sonore</p></td>
  </tr>
  <tr>
	<td><p>Augmenter le volume</p></td>
	<td><p>Augmentation du volume sonore</p></td>
  </tr>
  <tr>
	<td><p>Lancer le lecteur multimédia</p></td>
	<td><p>Média audio</p></td>
  </tr>
  <tr>
	<td><p>Lire (ou lecture/pause)</p></td>
	<td><p>Lecture audio</p></td>
  </tr>
  <tr>
	<td><p>Mettre en pause la lecture</p></td>
	<td><p>Mise en pause de l'audio</p></td>
  </tr>
  <tr>
	<td><p>Stopper la lecture</p></td>
	<td><p>Arrêt de l'audio</p></td>
  </tr>
  <tr>
	<td><p>Morceau précédent</p></td>
	<td><p>Audio précédent</p></td>
  </tr>
  <tr>
	<td><p>Morceau suivant</p></td>
	<td><p>Audio suivant</p></td>
  </tr>
  <tr>
	<td><p>Éjecter</p></td>
	<td><p>Éjecter</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Système</title>
  <tr>
	<td><p>Afficher la fenêtre de saisie de commande</p></td>
	<td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Afficher la vue d'ensemble des activités</p></td>
	<td><p><keyseq><key>Alt</key><key>F1</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Se déconnecter</p></td>
	<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Suppr</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Verrouillage de l'écran</p></td>
	<td><p><keyseq><key>Logo</key><key>L</key></keyseq></p></td>
  </tr>
  <tr>
  <td><p>Afficher le tiroir de messagerie</p></td>
  <td><p><keyseq><key>Logo</key><key>M</key></keyseq></p></td>
  </tr>
  <tr>
  <td><p>Focaliser sur la notification active</p></td>
  <td><p><keyseq><key>logo</key><key>N</key></keyseq></p></td>
  </tr>
  <tr>
  <td><p>Afficher toutes les applications</p></td>
  <td><p><keyseq><key>Logo</key><key>A</key></keyseq></p></td>
  </tr>
  <tr>
  <td><p>Ouvrir le menu de l'application</p></td>
  <td><p><keyseq><key>logo</key><key>F10</key></keyseq></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Saisie</title>
  <tr>
  <td><p>Passer à la source d'entrées suivante</p></td>
  <td><p><keyseq><key>Logo</key><key>Barre d'espace</key></keyseq></p></td>
  </tr>

  <tr>
  <td><p>Passer à la source d'entrées précédente</p></td>
  <td><p>Désactivé</p></td>
  </tr>
  <tr>
  <td><p>Avec les touches de modification seules, basculer vers la source suivante</p></td>
  <td><p>Désactivé</p></td>
  </tr>
  <tr>
  <td><p>La touche « compose »</p></td>
  <td><p>Désactivé</p></td>
  </tr>
  <tr>
  <td><p>Touche de caractères alternatifs</p></td>
  <td><p>Désactivé</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Accès universel</title>
  <tr>
	<td><p>Activer ou désactiver le zoom</p></td>
	<td><p><keyseq><key>Alt</key><key>Logo</key><key>8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Zoom avant</p></td>
  <td><p><keyseq><key>Alt</key><key>Logo</key><key>=</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Zoom arrière</p></td>
  <td><p><keyseq><key>Alt</key><key>Logo</key><key>-</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Activer ou désactiver le lecteur d'écran</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Activer ou désactiver le clavier visuel</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Augmenter la taille du texte</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Diminuer la taille du texte</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Activer ou désactiver le contraste élevé</p></td>
	<td><p>Désactivé</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Fenêtres</title>
  <tr>
	<td><p>Activer le menu de la fenêtre</p></td>
	<td><p><keyseq><key>Alt</key><key>Barre d'espace</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Activer ou désactiver le mode plein écran</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Activer ou désactiver l'état maximisé</p></td>
	<td><p><keyseq><key>Alt</key><key>F10</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Maximiser la fenêtre</p></td>
	<td><p><keyseq><key>Logo</key><key>↑</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Restaurer la fenêtre</p></td>
  <td><p><keyseq><key>Logo</key><key>↓</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Activer ou désactiver l'état enroulé</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Fermer la fenêtre</p></td>
	<td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
  </tr>
  <tr>
  <td><p>Masquer la fenêtre</p></td>
  <td><p><keyseq><key>Logo</key><key>H</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Déplacer la fenêtre</p></td>
	<td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Redimensionner la fenêtre</p></td>
	<td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
  </tr>
  <tr>
	<td><p>Afficher la fenêtre sur tous les espaces de travail ou sur un seul</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Placer la fenêtre au-dessus des autres si elle est recouverte, sinon au-dessous</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Placer la fenêtre au-dessus des autres</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Placer la fenêtre au-dessous des autres</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Maximiser la fenêtre verticalement</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
	<td><p>Maximiser la fenêtre horizontalement</p></td>
	<td><p>Désactivé</p></td>
  </tr>
  <tr>
  <td><p>Séparer l'affichage sur la gauche</p></td>
  <td><p><keyseq><key>Logo</key><key>←</key></keyseq></p></td>
  </tr>
  <tr>
  <td><p>Séparer l'affichage sur la droite</p></td>
  <td><p><keyseq><key>Logo</key><key>→</key></keyseq></p></td>
  </tr>
</table>

</section>

<section id="custom">
<title>Raccourcis personnalisés</title>

  <p>Pour créer vos propres raccourcis clavier dans les paramètres <gui>Clavier</gui> :</p>

  <steps>
    <item>
      <p>Cliquez sur le bouton <gui style="button">+</gui>. La fenêtre <gui>Raccourcis personnalisés</gui> s'ouvre alors.</p>
    </item>
    <item>
      <p>Saisissez un <gui>Nom</gui> pour identifier le raccourci, et une <gui>Commande</gui> pour lancer une application, puis cliquez sur <gui>Ajouter</gui>. Par exemple, si vous voulez un raccourci pour ouvrir Rhythmbox, vous pouvez le nommer <input>Musique</input> et utiliser la commande <input>rhythmbox</input>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Désactivé</gui> dans le rang qui vient d'être ajouté. Quand il de vient <gui>Nouvel accélérateur...</gui>, puis effectuez la combinaison de touches désirée.</p>
    </item>
  </steps>

  <p>Le nom de la commande que vous saisissez doit être une commande système valide. Vous pouvez vérifier que la commande fonctionne en ouvrant un Terminal et en la saisissant dedans. Il est possible que la commande ouvrant une application n'aie pas exactement le même nom que l'application elle-même.</p>

  <p>Si vous voulez modifier la commande qui est associée à un raccourci clavier personnalisé, double-cliquez sur le <em>nom</em> du raccourci. La fenêtre <gui>Raccourci personnalisé</gui> apparaît et vous pouvez modifier la commande.</p>

</section>

</page>
