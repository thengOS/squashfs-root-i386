<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="mouse-problem-notmoving" xml:lang="fr">

  <info>
    <link type="guide" xref="mouse#problems"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <!-- TODO: reorganise page and tidy because it's one ugly wall of text -->
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
        <name>Phil Bull</name>
        <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>How to checki why your mouse is not working.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Le pointeur de la souris ne bouge pas</title>

<links type="section"/>

<section id="plugged-in">
 <title>Contrôle du branchement de la souris</title>
 <p>Si vous possédez une souris avec fil, contrôlez que le connecteur est proprement branché à l'ordinateur.</p>
 <p>S'il s'agit d'une souris USB (avec un connecteur rectangulaire), essayez de la brancher à un autre port USB. S'il s'agit d'une souris PS/2 (dotée d'un petit connecteur rond à six broches), vérifiez qu'elle est branchée au port souris de couleur verte, et non pas au port clavier de couleur violette. Il se peut que vous deviez redémarrer l'ordinateur si elle n'était pas branchée.</p>
</section>

<section id="broken">
 <title>Contrôle du bon fonctionnement de la souris</title>
 <p>Branchez la souris à un autre ordinateur pour voir si elle fonctionne.</p>

 <p>S'il s'agit d'une souris optique ou laser, une lumière doit être émise sous la souris lorsqu'elle est allumée. Si la lumière est absente, contrôlez que la souris est bien allumée. Si c'est le cas et qu'aucune lumière n'est visible, il est possible que la souris soit défectueuse.</p>
</section>

<section id="wireless-mice">
 <title>Contrôle de souris sans fil</title>

  <list>
    <item>
      <p>Make sure the mouse is turned on. There is often a switch on the
      bottom of the mouse to turn the mouse off completely, so you can take it
      with you without it constantly waking up.</p>
    </item>
   <item><p>Si vous utilisez une souris Bluetooth, vérifiez que la souris est bien appariée avec l'ordinateur. Consultez <link xref="bluetooth-connect-device"/>.</p></item>
  <item>
   <p>Cliquez sur un bouton et regardez si le pointeur de la souris se met à bouger. Certaines souris sans fil se mettent en veille pour économiser leur batterie et ne réagissent donc pas avant que vous cliquiez sur un de ses boutons. Consultez <link xref="mouse-wakeup"/>.</p>
  </item>
  <item>
   <p>Contrôlez que la batterie de la souris est chargée.</p>
  </item>
  <item>
   <p>Contrôlez que le récepteur est proprement branché à l'ordinateur.</p>
  </item>
  <item>
   <p>Si la souris et le récepteur ont la capacité de fonctionner sur des canaux radio différents, vérifiez qu'ils sont tous les deux réglés sur le même canal.</p>
  </item>
  <item>
   <p>Il faut parfois appuyer sur un bouton de la souris, du récepteur ou sur les deux pour établir la connexion. Le manuel de la souris devrait fournir davantage d'indications si c'est le cas.</p>
  </item>
 </list>

 <p>La plupart des souris sans fil opérant par ondes radio devraient fonctionner automatiquement lorsqu'elles sont branchées à un ordinateur. Avec des souris Bluetooth ou à infrarouge, il est parfois nécessaire de procéder à des opérations supplémentaires pour les faire fonctionner. Ces opérations dépendent du fabricant ou du modèle de la souris.</p>
</section>

</page>
