<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="music-cantplay-drm" xml:lang="fr">
  <info>
    <link type="guide" xref="media#music"/>


    <credit type="author">
      <name>Le projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <desc>La prise en charge de ce format de fichier n'est peut être pas installée ou bien les chansons sont « protégées contre la copie ».</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Impossible de lire les titres achetés dans un magasin de musique en ligne</title>

<p>Si vous avez téléchargé de la musique d'un fournisseur en ligne, il est possible que vous ne puissiez pas la lire, spécialement si vous l'aviez achetée à partir d'un ordinateur sous Windows ou Mac OS et que vous l'avez ensuite recopiée.</p>

<p>C'est sans doute parce que le fichier musical est dans un format non reconnu par votre système. Pour pouvoir écouter un titre, il vous faut disposer du support correspondant au format du fichier audio - par exemple, pour lire un fichier MP3, il faut que la prise en charge pour MP3 soit installée sur votre système. Si elle ne l'est pas, un message s'affiche pour vous en informer et vous guider sur la procédure à suivre pour résoudre le problème.</p>

<p>Si les prises en charge adéquates sont installées, mais vous ne parvenez toujours pas à écouter le titre, celui-ci est probablement <em>protégé contre la copie</em> (aussi connu sous l'appellation <em>protection par DRM</em>). Le DRM (gestion des droits numériques) est un moyen de restreindre la lecture à certaines personnes et à certains matériels autorisés. La société qui vous a vendu cette chanson est seule à pouvoir décider de ces autorisations, pas vous. Si votre fichier musical est protégé par DRM, vous ne pourrez probablement pas l'écouter - il faut en principe un logiciel spécial fourni par le vendeur pour cela, mais il n'y en a que très peu compatibles avec Linux.</p>

<p>Pour en savoir plus sur les DRM, consultez <link href="http://www.april.org/drm">le site de l'April</link>.</p>

</page>
