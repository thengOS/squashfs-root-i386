<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-list" xml:lang="fr">

  <info>
    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>

    <link type="guide" xref="nautilus-prefs" group="nautilus-list"/>

    <revision pkgversion="3.5.92" date="2012-09-19" status="review"/>
    <revision pkgversion="3.14.0" date="2014-09-23" status="review"/>
    <revision pkgversion="3.18" date="2014-09-30" status="candidate"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Contrôler les informations affichées dans les colonnes dans la vue en liste.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Préférences des colonnes dans Fichiers</title>

  <p>There are eleven columns of information that you can display in the
  <gui>Files</gui> list view. Click <gui>Files</gui> in the top bar, pick
  <gui>Preferences</gui> and choose the <gui>List Columns</gui> tab to select
  which columns will be visible.</p>

  <note style="tip">
    <p>Use the <gui>Move Up</gui> and <gui>Move Down</gui> buttons to choose
    the order in which the selected columns will appear. Click <gui>Reset to
    Default</gui> to undo any changes and return to the default columns.</p>
  </note>

  <terms>
    <item>
      <title><gui>Nom</gui></title>
      <p>Le nom des dossiers et des fichiers.</p>
      <note style="tip">
        <p>The <gui>Name</gui> column cannot be hidden.</p>
      </note>
    </item>
    <item>
      <title><gui>Taille</gui></title>
      <p>La taille d'un dossier indique le nombre d'éléments contenus dans ce dossier. La taille d'un fichier est exprimée en octets, Ko ou Mo.</p>
    </item>
    <item>
      <title><gui>Type</gui></title>
      <p>Affiché sous la dénomination « dossier » ou par type de fichier comme « document PDF », « image JPEG », « audio MP3 » ou d'autres.</p>
    </item>
    <item>
      <title><gui>Dernière modification</gui></title>
      <p>Gives the date of the last time the file was modified.</p>
    </item>
    <item>
      <title><gui>Propriétaire</gui></title>
      <p>Le nom du propriétaire du dossier ou du fichier.</p>
    </item>
    <item>
      <title><gui>Groupe</gui></title>
      <p>Indique le groupe auquel appartient le fichier. Chaque utilisateur se trouve normalement dans son propre groupe, mais il est possible d'avoir plusieurs utilisateurs dans un même groupe. Par exemple, tout un service peut avoir son propre groupe dans un même environnement professionnel.</p>
    </item>
    <item>
      <title><gui>Permissions</gui></title>
      <p>Affiche les permissions d'accès au fichier, par exemple <gui>drwxrw-r--</gui></p>
      <list>
        <item>
          <p>Le premier caractère est le type de fichier. <gui>-</gui> signifie fichier standard et <gui>d</gui> veut dire répertoire (dossier). Dans de rares cas, d'autres caractères sont aussi utilisés.</p>
        </item>
        <item>
          <p>Les trois caractères suivants <gui>rwx</gui> indiquent les permissions spécifique au propriétaire du fichier.</p>
        </item>
        <item>
          <p>Les trois suivants <gui>rw-</gui> indiquent les permissions accordées à tous les membres du groupe propriétaire du fichier.</p>
        </item>
        <item>
          <p>Les trois derniers <gui>r--</gui> de la colonne indiquent les permissions accordées à tous les autres utilisateurs du système.</p>
        </item>
      </list>
      <p>Voici la signification de chaque permission :</p>
      <list>
        <item>
          <p><gui>r</gui> : lecture, signifiant que vous pouvez ouvrir et lire le contenu du fichier ou du dossier</p>
        </item>
        <item>
          <p><gui>w</gui> : écriture, signifiant que vous pouvez y écrire et y enregistrer des modifications</p>
        </item>
        <item>
          <p><gui>x</gui> : exécutable, signifiant que vous pouvez le lancer si c'est un programme ou un script, ou accéder à ses sous-dossiers et fichiers si c'est un dossier</p>
        </item>
        <item>
          <p><gui>-</gui> : permission non définie</p>
        </item>
      </list>
    </item>
    <item>
      <title><gui>Type MIME</gui></title>
      <p>Affiche le type MIME de l'élément.</p>
    </item>
    <item>
      <title><gui>Emplacement</gui></title>
      <p>Le chemin vers l'emplacement du fichier.</p>
    </item>
    <item>
      <title><gui>Modified – Time</gui></title>
      <p>Indique la date et l'heure de dernière modification du fichier.</p>
    </item>
    <item>
      <title><gui>Dernier accès</gui></title>
      <p>Gives the date or time of the last time the file was modified.</p>
    </item>
  </terms>

</page>
