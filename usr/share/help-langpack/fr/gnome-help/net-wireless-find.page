<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="net-wireless-find" xml:lang="fr">

  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="net-wireless-hidden"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-10" status="review"/>
    <revision pkgversion="3.12" date="2014-03-10" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Le projet de documentation GNOME</name>
      <email its:translate="no">gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Liaison sans fil arrêtée ou cassée, ou tentative de connexion à un réseau masqué.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Absence de mon réseau sans fil dans la liste</title>

  <p>Il y a un certain nombre de raisons qui font que vous ne pouvez pas trouver votre réseau sans fil dans la liste des réseaux du menu système.</p>

<list>
 <item>
  <p>S'il n'y a aucun réseau dans la liste, c'est que votre périphérique sans fil est éteint, ou qu'il ne <link xref="net-wireless-troubleshooting">fonctionne pas correctement</link>. Assurez-vous qu'il soit allumé.</p>
 </item>
<!-- Does anyone have lots of wireless networks to test this? Pretty sure it's
     no longer correct.
  <item>
    <p>If there are lots of wireless networks nearby, the network you are
    looking for might not be on the first page of the list. If this is the
    case, look at the bottom of the list for an arrow pointing towards the
    right and hover your mouse over it to display the rest of the wireless
    networks.</p>
  </item>-->
 <item>
  <p>Vous vous trouvez peut-être en dehors du rayon d'émission du réseau. Essayez de vous rapprocher de l'émetteur sans fil et regardez si votre réseau apparaît dans la liste au bout d'un moment.</p>
 </item>
 <item>
  <p>Il faut un peu de temps pour que la liste des réseaux sans fil se mette à jour. Si vous venez d'allumer votre ordinateur, ou si vous vous êtes déplacé vers un autre endroit, attendez une ou deux minutes et regardez si votre réseau s'est affiché dans la liste.</p>
 </item>
 <item>
  <p>Le réseau peut être masqué. Vous devez vous <link xref="net-wireless-hidden">connecter différemment</link> si c'est un réseau masqué.</p>
 </item>
</list>

</page>
