<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="sharing-bluetooth" xml:lang="fr">

  <info>
    <link type="guide" xref="bluetooth"/>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.8.2" date="2013-05-13" status="draft"/>
    <revision pkgversion="3.10" date="2013-11-09" status="review"/>
    <revision pkgversion="3.12" date="2014-03-08" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="author">
      <name>David King</name>
      <email>davidk@gnome.org</email>
      <years>2014-2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Autoriser le téléversement de fichiers vers votre ordinateur par Bluetooth.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Contrôle du partage par Bluetooth</title>

  <p>You can enable <gui>Bluetooth</gui> sharing to receive files over
  Bluetooth in the <file>Downloads</file> folder</p>

  <steps>
    <title>Autorisation du partage des fichiers de votre dossier <gui>Téléchargements</gui>.</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Bluetooth</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Bluetooth</gui> pour ouvrir le panneau.</p>
    </item>
    <item>
      <p>Make sure that <link xref="bluetooth-turn-on-off"><gui>Bluetooth</gui>
      is switched on</link>.</p>
    </item>
    <item>
      <p>Bluetooth-enabled devices can send files to your
      <file>Downloads</file> folder only when the <gui>Bluetooth</gui> panel is
      open.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Vous pouvez <link xref="sharing-displayname">changer</link> le nom que votre ordinateur montre aux autres périphériques.</p>
  </note>

</page>
