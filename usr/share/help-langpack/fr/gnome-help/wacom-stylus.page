<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="wacom-stylus" xml:lang="fr">

  <info>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <link type="guide" xref="wacom"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Définir les fonctions des boutons et la sensibilité de pression du stylet Wacom.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Claude Paroz</mal:name>
      <mal:email>claude@2xlibre.net</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>yanngnome</mal:name>
      <mal:email>yannubuntu@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolas Delvaux</mal:name>
      <mal:email>contact@nicolas-delvaux.org</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mickael Albertus</mal:name>
      <mal:email>mickael.albertus@gmail.com</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Franke</mal:name>
      <mal:email>alexandre.franke@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Configurer le stylet</title>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
    start typing <gui>Wacom Tablet</gui>.</p>
  </item>
  <item>
    <p>Cliquez sur <gui>Tablette Wacom</gui> pour ouvrir le panneau.</p>
    <!-- TODO: document how to connet the tablet using Bluetooth/add link -->
    <note style="tip"><p>Si aucune tablette n'est détectée, il vous est demandé <gui>Veuillez connecter ou allumer votre tablette Wacom</gui>. Cliquez sur le lien <gui>Paramètres Bluetooth</gui> pour connecter une tablette sans fil.</p></note>
  </item>
  <item><p>La partie inférieure du panneau contient des informations et des paramètres spécifiques de votre stylet, avec le nom du périphérique (la catégorie du stylet) et le diagramme sur la gauche. Ces paramètres peuvent être ajustés :</p>
    <list>
      <item><p><gui>Pression ressentie sur la gomme :</gui> utilisez le curseur pour ajuster la « sensibilité » (comment la pression physique est traduit en valeurs numériques) entre <gui>Doux</gui> et <gui>Ferme</gui>.</p></item>
      <item><p><gui>Button/Scroll Wheel</gui> configuration (these change to
       reflect the stylus). Click the menu next to each label to select one of
       these functions: No Action, Left Mouse Button Click, Middle Mouse Button
       Click, Right Mouse Button Click, Scroll Up, Scroll Down, Scroll Left,
       Scroll Right, Back, or Forward.</p></item>
      <item><p><gui>Pression ressentie sur la pointe :</gui> utilisez le curseur pour ajuster la « sensibilité » entre <gui>Doux</gui> et <gui>Ferme</gui>.</p></item>
    </list>
  </item>
</steps>

<note style="info"><p>If you have more than one stylus, when the additional
 stylus gets close to the tablet, a pager will be displayed next to the stylus
 device name. Use the pager to choose which stylus to configure.</p>
</note>

</page>
