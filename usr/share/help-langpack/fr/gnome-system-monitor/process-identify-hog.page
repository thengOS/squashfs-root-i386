<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="process-identify-hog" xml:lang="fr">
  <info>
    <revision version="0.2" pkgversion="3.11" date="2014-01-26" status="review"/>
    <link type="guide" xref="index" group="processes-tasks"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
      <years>2011</years>
    </credit>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2011, 2014</years>
    </credit>

    <desc>Trier la liste des processus par <gui>% CPU</gui> pour voir quelle application utilise les ressources de l'ordinateur.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>naybnet</mal:name>
      <mal:email>naybnet@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leonor Palazzo</mal:name>
      <mal:email>leonor.palazzo@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Recherche du programme qui ralentit l'ordinateur</title>

  <p>Un programme qui utilise plus que sa part du CPU peut ralentir tout l'ordinateur. Pour chercher le processus potentiellement responsable :</p>

  <steps>	
    <item>
      <p>cliquez sur l'onglet <gui>Processus</gui>.</p>
    </item>
    <item>
      <p>cliquez sur l'en-tête de colonne <gui>% CPU</gui> pour trier les processus selon l'utilisation du CPU.</p>
      <note>
	<p>La flèche dans l'en-tête de colonne montre le sens du tri ; cliquez à nouveau pour l'inverser. La flèche devrait pointer vers le haut.</p>
      </note>
   </item>
  </steps>

  <p>Les processus en haut de la liste utilisent le plus grand pourcentage de CPU. Une fois que vous avez identifié quel processus utilise peut-être plus de ressource qu'il ne devrait, vous pouvez décider de fermer le programme lui-même ou de fermer d'autres programmes pour essayer de réduire la charge du processeur.</p>

  <note style="tip">
    <p>Un processus qui est gelé ou qui s'arrête brutalement peut utiliser 100% du CPU. Si cela arrive, il vous faut <link xref="process-kill">tuer</link> le processus.</p>
  </note>

</page>
