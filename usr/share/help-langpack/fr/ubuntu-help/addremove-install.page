<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="addremove-install" xml:lang="fr">

  <info>
    <credit type="author">
      <name>L'équipe de documentation d'Ubuntu</name>
    </credit>
    <desc>Utilisez <app>Logiciels Ubuntu</app> pour ajouter des programmes et rendre Ubuntu plus utile.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="addremove" group="#first"/>
    <link type="seealso" xref="addremove-remove"/>
    <link type="seealso" xref="addremove-install-synaptic"/>
    <link type="seealso" xref="prefs-language-install"/>
    <revision version="16.04" date="2016-03-13" status="review"/>
  </info>

  <title>Installer des logiciels supplémentaires</title>

  <p>L'équipe de développement d'Ubuntu a sélectionné un ensemble d'applications par défaut ; nous pensons qu'elles rendent Ubuntu très utile pour la plupart des tâches de tous les jours. Cependant, vous aurez certainement envie d'installer plus de logiciels afin qu'Ubuntu corresponde mieux à vos besoins.</p>

  <p>Pour installer un logiciel supplémentaire, suivez les étapes suivantes :</p>

  <steps>
    <item>
      <p>Se connecter à Internet en utilisant une <link xref="net-wireless-connect">connexion sans-fil</link> ou une <link xref="net-wired-connect">connexion câblée</link>.</p>
    </item>
    <item>
      <p>Cliquez sur l’icône de <app>Logiciels Ubuntu</app> dans le <gui>Lanceur</gui> ou recherchez <input>Logiciels</input> dans le <gui>Tableau de bord</gui>.</p>
    </item>
    <item>
      <p>Quand <app>Logiciels Ubuntu</app> s'ouvre, cherchez une application ou sélectionnez une catégorie et trouvez une application dans la liste.</p>
    </item>
    <item>
      <p>Sélectionnez l'application qui vous intéresse et cliquez sur <gui>Installer</gui>.</p>
    </item>
    <item>
      <p>Vous serez invité à entrer votre mot de passe. Ceci fait, l'installation commencera.</p>
    </item>
    <item>
      <p>L'installation est généralement rapide, mais le téléchargement peut prendre du temps selon la vitesse de votre connexion Internet.</p>
    </item>
    <item>
      <p>Un raccourci vers votre nouvelle application sera ajouté à la barre de lanceurs. Pour désactiver cette fonction, décochez <guiseq><gui>Voir</gui><gui>Nouvelles applications dans le lanceur</gui></guiseq>.</p>
    </item>
  </steps>
  
</page>
