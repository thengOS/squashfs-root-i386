<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="addremove-ppa" xml:lang="fr">

  <info>
    <credit type="author">
      <name>L'équipe de documentation d'Ubuntu</name>
    </credit>
    <desc>Ajouter des PPA pour aider à tester des pré-versions de logiciels, ou des logiciels spécialisés.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="addremove"/>
    <link type="seealso" xref="addremove-sources"/>
    <revision version="16.04" date="2016-03-13" status="review"/>
  </info>

  <title>Ajouter un dépôt personnel de logiciels (PPA)</title>

  <p>Les <em>dépôts personnels de logiciels (PPA)</em> sont les dépôts conçus pour les utilisateurs d'Ubuntu et ils sont plus faciles à installer que d'autres dépôts tiers.</p>

  <note style="warning">
    <p>Ajoutez seulement des dépôts de logiciels provenant de sources en lesquelles vous avez confiance !</p>
    <p>La sécurité et la fiabilité des dépôts de logiciels tiers ne sont pas vérifiées par les membres d'Ubuntu. Il est possible qu'un logiciel de ces dépôts puisse endommager votre ordinateur.</p>
  </note>

  <steps>
    <title>Installer un PPA</title>
    <item>
      <p>Sur la page du PPA, cherchez la section <gui>Adding this PPA to your system</gui>. Notez l'emplacement du PPA, qui devrait être en gras et ressembler à quelque chose comme <code>ppa:mozillateam/firefox-next</code>.</p>
    </item>
    <item>
      <p>Cliquez sur l’icône de <app>Logiciels Ubuntu</app> dans le <gui>Lanceur</gui> ou recherchez <input>Logiciels</input> dans le <gui>Tableau de bord</gui>.</p>
    </item>
    <item>
      <p>Lorsque <app>Logiciels Ubuntu</app> se lance, cliquez sur <guiseq><gui>Modifier</gui> <gui>Logiciels &amp; mises à jour</gui></guiseq></p>
    </item>
    <item>
      <p>Basculer vers l'onglet <gui>Autres logiciels</gui>.</p>
    </item>
    <item>
      <p>Cliquez sur <gui>Ajouter</gui> et entrez l'emplacement du <code>ppa</code>.</p>
    </item>
    <item>
      <p>Cliquer sur <gui>Ajouter une source de mise à jour</gui>. Entrez votre mot de passe dans la fenêtre d'authentification.</p>
    </item>
    <item>
      <p>Fermer la fenêtre de <app>Logiciels &amp; mises à jour</app>. <app>Logiciels Ubuntu</app> vérifiera ensuite les nouveaux logiciels disponibles dans ces sources.</p>
    </item>
  </steps>

</page>
