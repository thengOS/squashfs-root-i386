<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-canshareprofiles" xml:lang="fr">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <desc>Partager des profils de couleurs n'est jamais une bonne idée, car le matériel évolue dans le temps.</desc>
    <revision version="13.10" date="2013-09-07" status="review"/>
    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Peut-on partager son profil de couleurs ?</title>

  <p>Les profils de couleurs que vous avez générés par vous-même sont spécifiques à votre matériel et aux conditions d'éclairage que vous avez définies lors de l'étalonnage. Un écran qui a été allumé pendant quelques centaines d'heures aura un profil de couleurs très différent d'un autre similaire portant un numéro de série voisin et qui lui sera resté allumé pendant des milliers d'heures.</p>
  <p>Cela signifie que si vous partagez votre profil de couleurs avec quelqu'un, vous aurez peut-être un étalonnage <em>approchant</em>, mais il est inexact de prétendre que votre écran est étalonné.</p>
  <p>De la même manière, et à moins que chacun ait pris soin de contrôler l'éclairage (pas de soleil direct entrant par les fenêtres, murs sombres, ampoules lumière du jour, etc.) d'une pièce où il y a affichage et modification d'images, le partage d'un profil de couleurs que vous aviez créé dans les conditions spécifiques d'éclairage de votre environnement n'a pas beaucoup de sens.</p>

  <note style="warning">
    <p>Vérifiez scrupuleusement les conditions de redistribution de profils téléchargés depuis des sites marchands en ligne, ou de ceux qui ont été générés spécialement pour vous.</p>
  </note>

</page>
