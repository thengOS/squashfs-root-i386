<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-gettingprofiles" xml:lang="fr">

  <info>
    <link type="guide" xref="color#profiles"/>
    <link type="seealso" xref="color-why-calibrate"/>
    <link type="seealso" xref="color-whatisprofile"/>
    <link type="seealso" xref="color-missingvcgt"/>
    <desc>Des profils de couleurs peuvent être achetés en ligne ou générés par vous.</desc>

    <revision version="13.10" date="2013-09-07" status="review"/>
    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Où trouver des profils de couleurs ?</title>

  <p>La meilleure façon d'obtenir des profils est de les générer vous-même, même si cela nécessite un investissement initial.</p>
  <p>Beaucoup de fabricants essaient de fournir des profils de couleurs pour leurs périphériques, même si parfois ils sont cachés dans des <em>paquets de pilotes</em> que vous pouvez télécharger et extraire puis dans lesquels il vous faut les rechercher.</p>
  <p>Quelques fabricants ne fournissent pas de bon profils pour leur périphériques et dans ce cas il vaut mieux les éviter. Un bon indice est de télécharger le profil et de vérifier que la date de création n'est pas antérieure à un an par rapport à celle où vous avez acquis le matériel. Si c'est le cas, alors il s'agit de données factices qui ne vous seront d'aucune utilité.</p>

  <p>Consultez <link xref="color-why-calibrate"/> pour savoir pourquoi les profils fournis par les marchands sont parfois pires qu'inutiles.</p>

</page>
