<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-notifications" xml:lang="fr">

  <info>
    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <desc>Vous pouvez recevoir une notification si votre profil de couleur est ancien et inexact.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="color#problems"/>
    <link type="seealso" xref="color-why-calibrate"/>
    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-10-24" status="review"/>
  </info>

  <title>Puis-je être averti lorsque mon profil de couleur est inexact ?</title>
  <p>Malheureusement, il est impossible de dire si le profil d'un périphérique est exact sans procéder à un nouvel étalonnage. La recommandation pour un réétalonnage ne repose que sur la mesure du temps écoulé depuis le dernier étalonnage.</p>
  <p>Certaines entreprises appliquent des règles spécifiques de péremption de profils, car un profil de couleur inexact peut conduire à de grosses différences sur le produit fini.</p>
  <p>Si vous définissez la politique d'expiration et qu'un profil est plus vieux que cette règle, alors un triangle d'avertissement rouge s'affichera dans la boîte de dialogue <guiseq><gui>Paramètres systèmes...</gui><gui>Couleur</gui></guiseq> à côté du profil. Un message d'avertissement sera également affiché à chaque fois que vous vous connecterez à votre ordinateur.</p>
  <p>Pour définir la règle pour les périphériques d'affichage et d'impression, spécifiez l'âge maximum du profil en nombre de jours :</p>

<screen>
<output style="prompt">[rupert@gnome] </output><input>gsettings set org.gnome.settings-daemon.plugins.color recalibrate-printer-threshold 180</input>
<output style="prompt">[rupert@gnome] </output><input>gsettings set org.gnome.settings-daemon.plugins.color recalibrate-display-threshold 180</input>
</screen>

</page>
