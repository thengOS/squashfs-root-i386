<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="documents-previews" xml:lang="fr">

  <info>
    <credit type="author copyright">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <desc>Vous ne pouvez prévisualiser que les fichiers stockés localement.</desc>
    <link type="guide" xref="documents#question"/>
    <revision pkgversion="3.5.90" date="2012-09-05" status="review"/>
    <revision version="13.10" date="2013-10-24" status="review"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Pourquoi certains fichiers n'ont pas d'aperçus ?</title>

  <p>Lorsque vous ouvrez <app>Documents</app>, une miniature d'aperçu est affichée pour les documents stockés localement. Ceux stockés sur un serveur à distance comme <em>Google Docs</em> ou <em>SkyDrive</em> montrent des miniatures d'aperçu manquantes (ou blanches).</p>
  <p>Si vous téléchargez un document <em>Google Docs</em> ou <em>SkyDrive</em> vers un emplacement local, une miniature sera générée.</p>

  <note style="important">
    <p>La copie locale d'un document téléchargé depuis <em>Google Docs</em> ou <em>SkyDrive</em> perdra sa capacité à être mise à jour en ligne. Si vous souhaitez continuer à l'éditer en ligne, il est préférable de ne pas le télécharger.</p>
  </note>
</page>
