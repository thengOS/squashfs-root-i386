<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="files-browse" xml:lang="fr">


  <info>
    <link type="guide" xref="files" group="#first"/>
    <link type="seealso" xref="files-copy"/>
    <desc>Gérer et organiser les fichiers avec le gestionnaire de fichiers.</desc>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision version="14.04" date="2014-02-20" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>L'équipe de documentation d'Ubuntu</name>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Consultation de vos fichiers et dossiers</title>

<p>Servez-vous du gestionnaire de fichiers <app>Files</app> pour naviguer et organiser les fichiers de votre ordinateur. Vous pouvez également l'utiliser pour gérer les fichiers sur des supports de stockage (comme des disques durs externes), sur des <link xref="nautilus-connect">serveurs de fichiers</link> et sur des partages réseau.</p>

<p>Pour lancer le gestionnaire de fichiers, ouvrez <app>Fichiers</app> dans le <gui>Lanceur</gui>. Vous pouvez également rechercher des fichiers et des dossiers dans le <gui>Tableau de bord</gui> de la même manière que vous <link xref="unity-dash-intro#dash-home">chercheriez des applications</link>. Ils apparaîtront sous l'en-tête <gui>Fichiers et Dossiers</gui>.</p>

<section id="files-view-folder-contents">
  <title>Parcourt du contenu des dossiers</title>

<p>Dans la fenêtre du gestionnaire de fichiers, effectuez un double-clic sur un dossier pour afficher son contenu, et sur un fichier pour l'ouvrir avec l'application par défaut pour ce type de fichier. Un clic milieu sur un dossier permet d'ouvrir son contenu dans un nouvel onglet ou une nouvelle fenêtre.</p>

<p>La <em>barre chemin</em> au-dessus de la liste des dossiers et fichiers indique le dossier que vous consultez, cela comprend le dossier actuel et les dossiers parents. Cliquez sur un dossier parent de ce chemin pour vous rendre à ce dossier. Faites un clic droit sur n'importe quel dossier de ce chemin pour l'ouvrir dans un nouvel onglet, une nouvelle fenêtre, ou pour le copier, le déplacer ou afficher ses propriétés.</p>

<p>Si vous souhaitez accéder rapidement à un fichier du dossier ouvert, commencez à saisir son nom. Un champ de recherche apparaît en haut de la fenêtre et le premier fichier correspondant à votre recherche est sélectionné. Utilisez la touche flèche vers le bas ou faites défiler grâce à la molette de la souris pour vous déplacer vers le fichier correspondant à votre recherche.</p>

<p>Vous pouvez rapidement accéder à des emplacements courants à partir du <em>panneau latéral</em>. Si vous ne le voyez pas, cliquez sur le bouton <media type="image" src="figures/go-down.png">bas</media> dans la barre d'outils et choisissez <gui>Affichez le panneau latéral</gui>. Vous pouvez ajouter des signets aux dossiers que vous utilisez souvent et ils apparaîtront dans le panneau latéral. Utilisez le menu <gui>Signets</gui> pour faire cela, ou déplacez simplement un dossier dans le panneau latéral.</p>

</section>
</page>
