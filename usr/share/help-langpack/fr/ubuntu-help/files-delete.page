<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" id="files-delete" xml:lang="fr">

  <info>
    <link type="guide" xref="files"/>
    <link type="seealso" xref="files-recover"/>
    <desc>Supprimer des fichiers ou dossiers dont vous n'avez plus besoin.</desc>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision version="13.10" date="2013-09-07" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
  </info>

<title>Suppression de fichiers et de dossiers</title>

  <p>Si vous n'avez plus besoin d'un fichier ou d'un dossier, vous pouvez le supprimer. La suppression d'un élément le déplace dans le dossier Corbeille où il est stocké jusqu'à ce que vous vidiez la corbeille. Les éléments de la corbeille peuvent être <link xref="files-recover">restaurés à leur emplacement d'origine</link> si vous changez d'avis et décidez de les conserver, ou s'ils ont été supprimés par inadvertance.</p>

  <steps>
    <title>Pour mettre un fichier à la corbeille :</title>
    <item><p>Sélectionnez l'élément à supprimer en cliquant dessus une fois.</p></item>
    <item><p>Appuyez sur la touche <key>Suppr</key> de votre clavier ou faites glisser l'élément jusque dans la <gui>Corbeille</gui> du panneau latéral.</p></item>
  </steps>

  <p>Pour effacer les fichiers définitivement et libérer de l'espace disque sur votre ordinateur, vous devez vider la corbeille. Pour vider la corbeille, effectuez un clic-droit sur la <gui>Corbeille</gui> dans la barre latérale et sélectionnez <gui>Vider la corbeille</gui>.</p>

  <section id="permanent">
    <title>Suppression définitive d'un fichier</title>
    <p>Il est possible de supprimer définitivement un fichier sans passer par la corbeille.</p>

  <steps>
    <title>Pour supprimer définitivement un fichier :</title>
    <item><p>Sélectionnez l'élément à supprimer.</p></item>
    <item><p>Maintenez enfoncée la touche <key>Maj</key> et pressez la touche <key>Suppr</key> de votre clavier.</p></item>
    <item><p>Comme c'est une action irréversible, il vous est demandé une confirmation avant de supprimer définitivement le fichier ou le dossier.</p></item>
  </steps>

  <note style="tip"><p>Si vous avez fréquemment besoin de supprimer des fichiers sans utiliser la corbeille (par exemple, si vous travaillez souvent avec des données sensibles), vous pouvez ajouter une entrée <gui>Supprimer</gui> au menu accessible par clic droit pour les fichiers et les dossiers. Cliquez sur <gui>Fichiers</gui> dans la barre de menu, choisissez <gui>Préférences</gui> et sélectionnez l'onglet <gui>Comportement</gui>. Sélectionnez <gui>Inclure une commande Supprimer qui ignore la corbeille</gui>.</p></note>

  <note><p>Les fichiers supprimés d'un <link xref="files#removable">média amovible</link> peuvent être invisibles pour d'autres systèmes comme Windows ou Mac OS. Malgré tout, les fichiers y sont toujours présent et redeviennent disponibles quand vous reconnectez le média sur votre ordinateur.</p></note>

  </section>
</page>
