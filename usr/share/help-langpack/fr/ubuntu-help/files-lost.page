<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-lost" xml:lang="fr">

  <info>
    <link type="guide" xref="files" group="more"/>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision version="13.10" date="2013-09-07" status="review"/>

    <desc>Suivez ces conseils si vous ne retrouvez pas un fichier que vous aviez créé ou téléchargé.</desc>

    <credit type="author">
      <name>Projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Recherche d'un fichier perdu</title>

<p>Si vous avez créé ou téléchargé un fichier que vous ne retrouvez plus, suivez ces conseils.</p>

<list>
  <item><p>Si vous ne vous souvenez plus où vous avez enregistré le fichier, mais que vous avez une idée de son nom, faites une recherche par nom. Consultez <link xref="files-search"/> pour savoir comment procéder.</p></item>

  <item><p>Si vous venez de télécharger le fichier, votre navigateur Web l'a certainement enregistré dans un dossier usuel. Vérifiez le contenu des dossiers « Bureau » et « Téléchargements » de votre répertoire personnel.</p></item>

  <item><p>Vous pouvez avoir supprimé malencontreusement le fichier. Quand vous supprimez un fichier, il est déplacé dans la corbeille où il attend d'être définitivement supprimé manuellement. Consultez <link xref="files-recover"/> pour apprendre comment le restaurer.</p></item>

  <item><p>Vous avez pu renommer le fichier de manière à le rendre caché. Les fichiers qui commencent par un <file>.</file> ou finissent par un <file>~</file> sont cachés par le gestionnaire de fichiers. Cliquez sur le bouton <media type="image" src="figures/go-down.png">bas</media> dans la barre d'outils du gestionnaire de fichiers et choisissez <gui>Afficher les fichiers cachés </gui> pour les afficher. Voir <link xref="files-hidden"/> pour en apprendre plus.</p></item>
</list>

</page>
