<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="hardware-cardreader" xml:lang="fr">

  <info>
    <link type="guide" xref="media#photos"/>
    <link type="guide" xref="hardware#problems"/>

    <desc>Résoudre les problèmes de lecteurs de cartes de données</desc>

    <revision version="13.10" date="2013-09-12" status="review"/>

    <credit type="author">
      <name>Projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Problèmes de lecteurs de cartes de données</title>

<p>Beaucoup d'ordinateurs incluent un lecteur de cartes SD (Secure Digital), MMC (MultiMediaCard), SmartMedia, Memory Stick, CompactFlash, ou d'autres cartes de stockage. Elles seront automatiquement détectées et <link xref="disk-partitions">montées dans le système de fichier</link>. Voici quelques étapes à suivre si ce n'était pas le cas :</p>

<steps>
<item>
<p>Assurez-vous d'insérer correctement la carte. Beaucoup de cartes paraissent à l'envers lorsqu'elles sont bien insérées. Assurez-vous également que la carte est bien enfoncée dans la fente ; certaines cartes, en particulier les CompactFlash, nécessitent de forcer légèrement pour les insérer. (Faites attention à ne pas pousser trop fort ! Si vous vous heurtez à quelque chose de dur, ne forcez pas.)</p>
</item>

<item>
<p> Open <app>Files</app> by using the <gui>Dash</gui>. Does the inserted
 card appear in the <gui>Devices</gui> list in the left sidebar? Sometimes the card appears in this list but is not mounted; click it once to mount. (If the sidebar is not visible, press <key>F9</key> or click
 <guiseq><gui>View</gui><gui> Sidebar</gui><gui> Show Sidebar</gui></guiseq>.)
</p>
</item>

<item>
<p>Si votre carte ne s'affiche pas dans le panneau latéral, cliquez sur <guiseq><gui>Aller à</gui><gui>Poste de travail</gui></guiseq>. Si votre lecteur de carte est correctement configuré, il doit y apparaître soit sous forme d'un lecteur vide, soit sous forme de la carte elle-même si elle est montée (voir l'image ci-dessous).</p>
</item>

<item>
<p>Si vous voyez le lecteur de carte vide sans la carte, le problème est peut-être dû à la carte. Essayez avec une autre carte ou vérifiez la carte avec un autre lecteur si possible.</p>
</item>
</steps>

<p>S'il n'y a ni lecteur ni carte affiché dans le dossier <gui>Poste de travail</gui>, il se peut que votre lecteur de carte ne fonctionne pas avec Linux à cause d'un problème de pilote. S'il s'agit d'un lecteur de carte interne (à l'intérieur de votre ordinateur et non externe), c'est sans doute l'origine du problème. La meilleure solution est de connecter directement votre périphérique (appareil photo, téléphone cellulaire, etc.) à un port USB de votre ordinateur. Il y a aussi des lecteurs de cartes USB externes qui sont beaucoup mieux pris en charge par Linux.</p>

</page>
