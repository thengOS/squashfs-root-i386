<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="net-mobile" xml:lang="fr">
  <info>
    <link type="guide" xref="net"/>
    
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>L'équipe de documentation d'Ubuntu</name>
    </credit>

    <desc>Connexion Internet via un réseau large bande mobile (Hsdpa, 3G, Edge, GPRS, GSM...).</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Connexion via un réseau large bande mobile (Hsdpa, 3G, Edge, GPRS, GSM...).</title>

    <p><em>Réseau large bande mobile</em> fait référence à toutes les connexions Internet qui sont fournies par un dispositif extérieur comme une clef 3G ou un téléphone mobile avec une connexion de données (exemple HSDPA/3G/GPRS...). Certains ordinateurs portables récents peuvent aussi embarquer un composant permettant d'utiliser un réseau large bande mobile.</p>
    <p>La plupart des appareils mobiles à large bande devraient être reconnus automatiquement lorsque vous les connecterez à votre ordinateur. Ubuntu vous demandera de configurer l'appareil.</p>
    <steps>
        <item>
          <p>L'assistant <gui>Nouvelle connexion large bande mobile</gui> s'ouvrira automatiquement quand vous brancherez le périphérique.</p>
        </item>
        <item>
          <p>Cliquez sur <gui>Suivant</gui> et entrez les détails, y compris le pays d'où provient votre périphérique mobile à large bande, le fournisseur de réseau et le type de connexion (par exemple, <em>Forfait</em> ou <em>pre-payé</em>).</p>
        </item>
        <item>
          <p>Donnez un nom à votre connexion puis cliquez sur <gui>Appliquer</gui>.</p>
        </item>
        <item>
          <p>Votre connexion est prête à être utilisée. Pour vous connecter, cliquez sur <gui>menu réseau</gui> dans la <gui>barre de menu</gui> et sélectionnez votre nouvelle connexion.</p>
        </item>
        <item>
          <p>Pour vous déconnecter, cliquez sur le  <gui>menu réseau</gui> dans la barre de menu et cliquez sur <gui>Déconnecter</gui>.</p>
        </item>
    </steps>
    <p>Si vous n'êtes pas invité à configurer l'appareil lorsque vous le connectez, il peut encore être reconnu par Ubuntu. Dans ce cas, vous pouvez ajouter manuellement la connexion.</p>
    <steps>
    	<item>
    	  <p>Cliquer sur l'<link xref="unity-menubar-intro">indicateur réseau</link> dans la barre de menu et sélectionner <gui>Modification des connexions...</gui></p>
    	</item>
    	<item>
    	  <p>Allez dans l'onglet <gui>mobile à large bande</gui>.</p>
    	</item>
    	<item>
    	  <p>Cliquez sur <gui>Ajouter</gui></p>
    	</item>
    	<item>
    	  <p>Cela ouvrira l'assistant <gui>Nouvelle connexion mobile à large bande</gui>. Entrez les détails tels que décrit ci-dessous.</p>
    	</item>
    </steps>

 
</page>
