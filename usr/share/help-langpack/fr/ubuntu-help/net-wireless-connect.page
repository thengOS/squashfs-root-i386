<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="net-wireless-connect" xml:lang="fr">
  <info>
    <link type="guide" xref="net-wireless" group="#first"/>
    <link type="seealso" xref="net-wireless-troubleshooting"/>
    <link type="seealso" xref="net-wireless-noconnection"/>
    <link type="seealso" xref="net-wireless-disconnecting"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <credit type="author">
      <name>L'équipe de documentation d'Ubuntu</name>
    </credit>

    <desc>Aller sur internet - sans fil.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Connexion à un réseau sans fil</title>

<p>Si vous disposez d'un ordinateur avec une possibilité de réseau sans fil, vous pouvez vous connecter à un réseau sans fil situé dans votre rayon de capture pour aller naviguer sur internet, visionner des fichiers partagés sur ce réseau, etc.</p>

<steps>
  <item>
   <p>Si vous avez un matériel sans fil sur votre ordinateur, assurez-vous qu'il est bien activé.</p>
  </item>
  <item>
    <p>Cliquez sur le <gui>menu réseau</gui> dans la <gui>barre de menu</gui>, et cliquez sur le nom du réseau auquel vous voulez vous connecter.</p>
   <p>Si le nom du réseau n'est pas dans la liste, sélectionnez <gui>Plus de réseaux</gui>  pour voir si le réseau est plus bas dans la liste. Si vous ne voyez toujours pas le réseau, vous êtes peut-être hors de portée ou le réseau <link xref="net-wireless-hidden">pourrait être caché</link>.</p>
  </item>
  <item><p>Si le réseau est protégé par un mot de passe (<link xref="net-wireless-wepwpa">clé de chiffrement</link>), saisissez le mot de passe demandé et cliquez sur <gui>Connexion</gui>.</p>
  <p>Si vous ne connaissez pas la clé, regardez sur le routeur ou la base émettrice, dans le manuel d'utilisation ou demandez-la à l'administrateur du réseau.</p></item>
  <item><p>L'icône réseau va changer d'apparence au cours de la tentative de connexion.</p></item>
  <item>
   <p>Si la connexion aboutit, l'icône va se changer en un point avec plusieurs traits courbes au-dessus. Le nombre de traits indique la puissance du signal. S'il y a peu de traits, la connexion est faible et peut être aléatoire.</p>
  </item>
</steps>

<p>Si la connexion se solde d'un échec, <link xref="net-wireless-noconnection">votre mot de passe peut vous être redemandé</link> ou un message peut vous dire que la connexion a été interrompue. Il existe un grand nombre de raisons qui peuvent avoir causées cela. Vous pouvez par exemple avoir saisi un mauvais mot de passe, le signal Wi-Fi peut être trop faible ou la carte sans fil de votre ordinateur peut rencontrer un problème. Voir <link xref="net-wireless-troubleshooting"/> pour plus d'aide.</p>

<p>Un signal puissant sur un réseau sans fil ne signifie pas nécessairement une connexion internet ou des possibilités de téléchargement plus rapides. La connexion sans fil relie votre ordinateur à un <em>périphérique qui fournit une connexion internet</em> (comme un routeur ou un modem), mais ces deux connexions sont différentes et vont chacune à leur propre vitesse.</p>

</page>
