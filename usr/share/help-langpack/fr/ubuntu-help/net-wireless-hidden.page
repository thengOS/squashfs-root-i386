<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="net-wireless-hidden" xml:lang="fr">
  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="seealso" xref="net-wireless-edit-connection#wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <credit type="author">
      <name>L'équipe de documentation d'Ubuntu</name>
    </credit>

    <desc>Cliquez sur le <gui>menu réseau</gui> dans la barre de menu et sélectionnez <gui>Se connecter à un réseau sans fil invisible</gui>.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Connexion à un réseau sans fil masqué</title>

<p>Il est possible de configurer un réseau sans fil « invisible ». Les réseaux invisibles n'apparaissent pas dans la liste des réseaux qui sont affichés lorsque vous cliquez sur le menu réseau dans la barre de menu (ou la liste des réseaux sans fil sur les autres ordinateurs).  Pour vous connecter à un réseau sans fil invisible :</p>

<steps>
 <item>
  <p>Cliquez sur le <gui>menu réseau</gui> dans la barre de menu et sélectionnez <gui>Se connecter à un réseau sans fil invisible</gui>.</p>
 </item>
 <item>
  <p>Dans la boîte de dialogue qui s'affiche, saisissez le nom du réseau, choisissez le type de sécurité sans fil et cliquez sur <gui>Connecter</gui>.</p>
 </item>
</steps>

<p>Pour connaître le nom de réseau à saisir, regardez sur la station émettrice ou le routeur. Il est parfois appelé <gui>BSSID</gui> (Basic Service Set Identifier) et ressemble à ceci : <gui>02:00:01:02:03:04</gui>.</p>

<p>Il vous faut aussi vérifier les paramètres de sécurité que la station émettrice sans fil utilise. Recherchez des termes comme WEP et WPA.</p>

<note>
 <p>Vous pouvez penser que masquer votre réseau sans fil améliore sa sécurité en empêchant les personnes qui ne savent pas qu'il existe de s'y connecter. En fait, ce n'est pas le cas ; ce réseau est un peu plus difficile à trouver, mais il reste néanmoins détectable.</p>
</note>

</page>
