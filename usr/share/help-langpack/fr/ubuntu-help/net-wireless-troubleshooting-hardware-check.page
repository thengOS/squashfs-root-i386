<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="net-wireless-troubleshooting-hardware-check" xml:lang="fr">
  <info>
    <link type="next" xref="net-wireless-troubleshooting-device-drivers"/>
    <link type="guide" xref="net-wireless-troubleshooting"/>
    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Contributeurs du wiki de la documentation Ubuntu</name>
    </credit>

    <credit type="author">
      <name>Projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <desc>Même si votre adaptateur sans fil est connecté, il se peut qu'il ne soit pas bien reconnu par l'ordinateur.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Dépannage de la connexion sans fil</title>
<subtitle>Vérifier si l'adaptateur sans fil a été reconnu</subtitle>

<p>Même si votre adaptateur sans fil est connecté, il se peut qu'il ne soit pas bien reconnu comme un périphérique réseau par l'ordinateur. Cette étape est une vérification de la compatibilité de votre périphérique.</p>

<steps>
 <item>
  <p>Ouvrir un terminal, saisir <cmd>sudo lshw -C network</cmd> et appuyer sur <key>Entrée</key>. Si cela retourne un message d'erreur, vous pouvez installer le programme <app>lshw</app> sur votre ordinateur en saisissant <cmd>sudo apt-get install lshw</cmd> dans le terminal.</p>
 </item>
 <item>
  <p>Regardez les informations qui s'affichent, et recherchez la section <em>Wireless interface</em>. Si votre adaptateur sans fil est reconnu correctement, vous devriez voir s'afficher quelque chose de similaire (mais non d'identique) à ceci :</p>
   <code>*-network
       description: Wireless interface
       product: PRO/Wireless 3945ABG [Golan] Network Connection
       vendor: Intel Corporation</code>
 </item>
 <item>
  <p>S'il y a un périphérique sans fil dans la liste, continuez par l'<link xref="net-wireless-troubleshooting-device-drivers">étape sur les pilotes de périphérique</link>.</p>
  <p>S'il n'y a <em>pas</em> de périphérique sans fil dans la liste, les étapes suivantes dépendront du type de périphérique que vous utilisez. Reportez-vous à la section ci-dessous qui traite du type d'adaptateur sans fil dont votre ordinateur dispose (<link xref="#pci">PCI interne</link>, <link xref="#usb">USB</link>, <link xref="#pcmcia">PCMCIA</link>).</p>
 </item>
</steps>

<section id="pci">
 <title>Adaptateur sans fil PCI (interne)</title>
 <p>Les adaptateurs PCI internes sont les plus utilisés et sont présents dans la plupart des ordinateurs portables de ces dernières années. Pour vérifier que votre adaptateur PCI interne a été reconnu :</p>
 <steps>
  <item><p>Ouvrez un terminal, saisissez <cmd>lspci</cmd> et appuyez sur <key>Entrée</key>.</p></item>
  <item>
   <p>Regardez dans la liste des périphériques affichés et recherchez ceux qui sont appelés <code>Network controller</code> ou <code>Ethernet controller</code>. Plusieurs périphériques peuvent être qualifiés de cette façon ; celui qui correspond à votre adaptateur sans fil doit inclure des termes comme <code>wireless</code>, <code>WLAN</code>, <code>wifi</code> ou <code>802.11</code>. Voici un exemple du retour de la commande :</p>
 <code>Network controller: Intel Corporation PRO/Wireless 3945ABG [Golan] Network Connection</code>
 </item>
 <item><p>Si vous avez trouvé votre adaptateur sans fil dans la liste, allez à l'<link xref="net-wireless-troubleshooting-device-drivers">étape sur les pilotes de périphérique</link>. Si vous n'avez rien trouvé qui ressemble à votre adaptateur sans fil, consultez <link xref="#not-recognized">les instructions ci-dessous</link>.</p></item>
 </steps>
</section>

<section id="usb">
 <title>Adaptateur sans fil USB</title>
 <p>Les adaptateurs sans fil qui se branchent dans un port USB de votre ordinateur sont moins courants. Ils se branchent directement dans un port USB ou sont reliés à lui par un câble USB. Les adaptateurs 3G/mobiles à large bande ressemblent beaucoup à un adaptateur sans fil (Wifi), donc vérifiez bien que votre adaptateur USB ne soit pas en fait un adaptateur 3G. Pour vérifier si votre adaptateur sans fil USB a été reconnu :</p>
 <steps>
  <item><p>Ouvrez un terminal, saisissez <cmd>lsusb</cmd> et appuyez sur <key>Entrée</key>.</p></item>
  <item>
   <p>Regardez dans la liste des périphériques qui s'affiche et recherchez-en un qui semble se référer à un périphérique ou un réseau sans fil. Celui qui correspond à votre adaptateur devrait contenir des termes comme <code>wireless</code>, <code>WLAN</code>, <code>wifi</code> ou <code>802.11</code>. Voici un exemple du retour de cette commande :</p>
   <code>Bus 005 Device 009: ID 12d1:140b Huawei Technologies Co., Ltd. EC1260 Wireless Data Modem HSD USB Card</code>
  </item>
  <item><p>Si vous avez trouvé votre adaptateur sans fil dans la liste, allez à l'<link xref="net-wireless-troubleshooting-device-drivers">étape sur les pilotes de périphérique</link>. Si vous n'avez rien trouvé qui ressemble à votre adaptateur sans fil, consultez <link xref="#not-recognized">les instructions ci-dessous</link>.</p></item>
 </steps>
</section>

<section id="pcmcia">
 <title>Périphérique PCMCIA</title>
 <p>Les adaptateurs sans fil PCMCIA sont typiquement des cartes rectangulaires qui se branchent dans une fente sur le côté de votre ordinateur portable. Ils sont très répandus sur les vieux ordinateurs. Pour vérifier si votre adaptateur PCMCIA a été reconnu :</p>
 <steps>
  <item><p>Démarrez votre ordinateur <em>sans</em> l'adaptateur sans fil branché</p></item>
  <item>
   <p>ouvrez un terminal et saisissez ce qui suit, puis appuyez sur <key>Entrée</key> :</p>
   <code>tail -f /var/log/dmesg</code>
   <p>Cette commande affiche une liste de messages concernant le matériel composant votre ordinateur et se met à jour automatiquement dès que quelque chose change dans la composition.</p>
  </item>
  <item><p>Introduisez votre adaptateur sans fil dans le logement PCMCIA et regardez ce qui change dans la fenêtre du terminal. Les changements doivent inclure des informations concernant votre adaptateur sans fil. Recherchez-les pour les identifier.</p></item>
  <item><p>Pour arrêter ce processus dans le terminal, appuyez sur <keyseq><key>Ctrl</key><key>C</key></keyseq>. Fermez ensuite le terminal.</p></item>
  <item><p>Si vous avez trouvé des informations sur votre adaptateur sans fil, allez à l'<link xref="net-wireless-troubleshooting-device-drivers">étape pilotes de périphériques</link>. Si vous n'avez rien trouvé, consultez les <link xref="#not-recognized">instructions ci-dessous</link>.</p></item>
 </steps>
</section>

<section id="not-recognized">
 <title>Adaptateur sans fil non reconnu</title>
<p>Si votre adaptateur sans fil n'est pas reconnu, il pourrait ne pas fonctionner correctement ou les pilotes corrects pourraient ne pas être installés.</p>
<p>Pour obtenir une aide plus spécifique, consultez le site Web de votre distribution. Pensez aussi aux listes de diffusion et aux forums d'aide pour poser des questions sur votre adaptateur sans fil.</p>
</section>

</page>
