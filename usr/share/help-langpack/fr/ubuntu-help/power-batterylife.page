<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="power-batterylife" xml:lang="fr">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspend"/>
    <link type="seealso" xref="power-hibernate"/>
    <link type="seealso" xref="shell-exit#shutdown"/>
    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <desc>Conseils pour réduire la consommation d'énergie de votre ordinateur.</desc>

    <credit type="author">
      <name>Projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Économie d'énergie et accroissement de la longévité de la batterie</title>

<p>Les ordinateurs peuvent utiliser beaucoup d'énergie. En utilisant des stratégies simples d'économie d'énergie, vous pouvez réduire votre facture d'électricité et aider l'environnement. Si vous possédez un ordinateur portable, cela aidera aussi à augmenter la durée d'utilisation avec la batterie.</p>

<section id="general">

<title>Astuces générales</title>
<list>
  <item>
    <p><link xref="shell-exit#suspend">Mettez en veille votre ordinateur</link> quand vous ne l'utilisez pas. Cela réduit considérablement sa consommation d'énergie et son réveil peut être très rapide.</p>
  </item>
  <item>
    <p><link xref="shell-exit#shutdown">Éteignez</link> l'ordinateur lorsque vous ne l'utilisez pas pendant de longs moments. Certaines personnes s'inquiètent du fait qu'éteindre un ordinateur régulièrement le fait s'user plus vite, mais cela n'est pas le cas.</p>
  </item>
  <item>
    <p>Utilisez les préférences de l'<gui>Énergie</gui> dans les <app>Paramètres systèmes</app> pour modifier vos paramètres de gestion de l'énergie. Certaines options participent à l'économie d'énergie : <link xref="display-dimscreen">assombrir automatiquement</link> l'affichage au bout d'un certain temps ; <link xref="display-dimscreen">réduire la luminosité de l'affichage</link> (pour les portables) ; et <link xref="power-suspend">suspendre l'ordinateur</link> si vous ne l'utilisez plus depuis un certain temps.</p>
  </item>
  <item>
    <p>Éteignez tous les périphériques externes (comme les imprimantes et les scanners) quand vous ne les utilisez pas.</p>
  </item>
 </list>
</section>

<section id="laptop">
 <title>Portables, tablettes et autres périphériques avec batteries</title>

 <list>
   <item>
     <p><link xref="display-dimscreen">Réduisez la luminosité de l'écran</link>; une grande partie de l'énergie consommée par l'ordinateur portable sert à alimenter l'écran.</p>
     <p>La plupart des ordinateurs portables possèdent des boutons sur le clavier (ou un raccourci clavier) pour réduire la luminosité.</p>
   </item>
   <item>
     <p>Si vous ne vous servez pas de la connexion internet pendant un certain temps, éteignez le commutateur sans fil / Bluetooth de la carte réseau. Ce périphérique fonctionne avec des ondes radio, ce qui consomme pas mal d'énergie.</p>
     <p>Sur certains ordinateurs, il y a un commutateur manuel pour éteindre la carte, alors que sur d'autres, vous avez un raccourci clavier pour faire la même chose. Rallumez-la quand vous en avez besoin.</p>
   </item>
 </list>
</section>

<section id="advanced">
 <title>Astuces plus avancées</title>

 <list>
   <item>
     <p>Réduisez le nombre de tâches qui tournent en arrière-plan. Plus les ordinateurs sont sollicités, plus ils consomment d'énergie.</p>
     <p>La plupart de vos applications ouvertes ne font pas grand-chose tant que vous ne les utilisez pas. Cependant, les applications qui cherchent souvent des données sur internet, celles qui lisent de la musique, des films augmentent la consommation d'énergie.</p>
   </item>
 </list>
</section>

</page>
