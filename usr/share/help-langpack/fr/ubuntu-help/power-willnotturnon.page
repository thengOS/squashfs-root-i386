<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="power-willnotturnon" xml:lang="fr">

  <info>
    <link type="guide" xref="power#problems"/>
    <link type="guide" xref="hardware-problems-graphics" group="#last"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <desc>Des câbles mal branchés ou des problèmes de matériel sont des raisons probables.</desc>
    <credit type="author">
      <name>Projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Mise en marche de l'ordinateur impossible</title>

<p>Si votre ordinateur refuse de se mettre en marche, voici un bref aperçu des diverses raisons possibles.</p>
	
<section id="nopower">
  <title>Ordinateur non branché, batterie vide, câble mal branché</title>
  <p>Vérifiez que les cables d'alimentation de l'ordinateur sont fermement connectés et que l'alimentation est active. Vérifiez aussi que l'écran est connecté et allumé. Si vous possédez un ordinateur portable, connectez-le au chargeur (au cas où la batterie serait vide). Vous pouvez aussi vérifier que la batterie est correctement positionnée (vérifier le dessous de l'ordinateur portable) si celle-ci est amovible.</p>
</section>

<section id="hardwareproblem">
  <title>Problème de matériel</title>
  <p>Un composant de votre ordinateur est peut-être cassé ou dysfonctionne. Si c'est le cas, emmenez-le chez un réparateur. Une alimentation grillée, des composants mal enclenchés (comme une barrette mémoire/RAM) ou une carte mère défectueuse sont les raisons les plus courantes.</p>
</section>

<section id="beeps">
  <title>Bips et extinction de l'ordinateur</title>
  <p>Si votre ordinateur émet plusieurs bips quand vous l'allumez et s'éteint tout de suite après, il indique qu'il y a un problème matériel. Ces bips sont un <em>code</em> utilisé par l'ordinateur et dont l'architecture est sensée indiquer la nature du problème. Ce code varie en fonction du fabricant de l'ordinateur, consultez donc le manuel de la carte mère de votre ordinateur ou emmenez-le chez un réparateur.</p>
</section>

<section id="fans">
  <title>Ventilateurs en fonctionnement, mais pas d'affichage</title>
  <p>Vérifiez d'abord que votre moniteur est branché et allumé.</p>
  <p>Il peut aussi s'agir d'une panne matérielle. Quand vous appuyez sur le bouton de mise en marche, les ventilateurs se mettent à tourner, mais le reste ne fonctionne pas. Emmenez votre ordinateur chez un réparateur.</p>
</section>

</page>
