<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-order" xml:lang="fr">

  <info>
    <link type="guide" xref="printing#paper"/>

    <desc>Rassembler des copies et inverser l'ordre d'impression.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Inversion de l'ordre d'impression</title>

<section id="reverse">
 <title>Inversion</title>
 <p>Normalement, les imprimantes impriment en premier la première page et en dernier la dernière page de façon à ce qu'à la fin, elles se retrouvent dans l'ordre inverse et dans le bon sens quand vous les prenez. Si besoin est, vous pouvez inverser cet ordre.</p>
 <p>Pour inverser l'ordre :</p>
 <steps>
  <item><p>Cliquez sur <guiseq><gui>Fichier</gui><gui>Imprimer</gui></guiseq>.</p></item>
  <item><p>Dans l'onglet <gui>Général</gui> de la fenêtre Imprimer et dans la colonne <em>Copies</em>, cochez la case <gui>Inverser</gui>. La dernière page se retrouve imprimée en premier et ainsi de suite.</p></item>
 </steps>
</section>

<section id="collate">
 <title>Rassemblement</title>
 <p>Si vous imprimer plusieurs copies du document, les épreuves vont être groupées par défaut par leur numéro de page (toutes les copies de la première page ensemble, puis toutes les copies de la deuxième, etc.). Au lieu de cela, la fonction <em>Rassembler</em> va faire en sorte que chaque copie du document sorte avec toutes ses pages regroupées dans le bon ordre.</p>

 <p>Pour rassembler :</p>
 <steps>
  <item><p>Cliquez sur <guiseq><gui>Fichier</gui><gui>Imprimer</gui></guiseq>.</p></item>
  <item><p>Dans l'onglet <gui>Général</gui> de la fenêtre Imprimer, dans la colonne <em>Copies</em>, cochez la case <gui>Rassembler</gui>.</p></item>
 </steps>
 <!-- Need to add this image <media type="image" src="figures/reverse-collate.png"/> -->
</section>

</page>
