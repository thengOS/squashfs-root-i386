<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shell-windows-maximize" xml:lang="fr">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="seealso" xref="shell-windows-tiled"/>

    <desc>Double-cliquer ou déplacer la barre de titre pour maximiser ou restaurer une fenêtre.</desc>

    <revision pkgversion="3.4.0" date="2012-03-14" status="candidate"/>
    <revision version="13.10" date="2013-10-23" status="review"/>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Maximiser et réduire une fenêtre</title>

  <p>Vous pouvez maximiser une fenêtre pour qu'elle prenne tout l'espace disponible sur votre bureau et réduire une fenêtre pour qu'elle revienne à sa taille normale. Vous pouvez également maximiser les fenêtres verticalement le long des côtés droit et gauche de l'écran, de manière à voir deux fenêtres en même temps. Voir <link xref="shell-windows-tiled"/> pour plus de détails.</p>

  <p>Pour maximiser une fenêtre, saisissez la barre de titre et déplacez-la en haut de l'écran ou double-cliquez sur la barre de titre. Pour maximiser une fenêtre en utilisant le clavier, maintenez enfoncées les touches <key>Ctrl</key> et <key><link xref="windows-key">Super</link></key> et appuyez sur <key>↑</key>.</p>

  <p>Pour restaurer une fenêtre à sa taille normale, faites-la glisser hors de la bordure de l'écran. Si la fenêtre est complètement maximisée, vous pouvez double-cliquer sur le titre de la fenêtre pour la restaurer. Vous pouvez aussi utiliser le raccourci clavier <keyseq><key>Ctrl</key> <key><link xref="windows-key">Super</link></key> <key>↓</key></keyseq>.</p>

  <note style="tip">
    <p>Maintenez appuyée la touche <key>Alt</key> et faites glisser une fenêtre à partir de n'importe quel point de la fenêtre pour la déplacer.</p>
  </note>
</page>
