<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shell-windows-states" xml:lang="fr">

  <info>

    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="seealso" xref="unity-menubar-intro#window-management"/>

    <desc>Restaurer, redimensionner, arranger et masquer.</desc>

    <revision pkgversion="3.4.0" date="2012-09-20" status="candidate"/>
    <revision version="13.10" date="2013-10-23" status="review"/>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Opérations sur les fenêtres</title>

<p>Les fenêtres peuvent être redimensionnées ou dissimulées pour correspondre à ce que vous êtes en train de faire.</p>


<section id="min-rest-close">
<title>Minimisation, restauration et fermeture</title>

    <p>Pour minimiser ou masquer une fenêtre :</p>
    <list>
      <item>
       <p>Cliquez sur <gui>-</gui> en haut à gauche de la <gui>barre de menu</gui> de l'application. Si l'application est maximisée (elle prend tout l'écran), la barre de menu apparaîtra tout en haut de l'écran. Sinon, le bouton pour minimiser apparaîtra en haut de la fenêtre de l'application.</p>
      </item>
      <item>
       <p>Ou appuyez sur <keyseq><key>Alt</key><key>Espace</key></keyseq> pour faire apparaître le menu de la fenêtre. Ensuite, appuyez sur <key>n</key>. La fenêtre « disparaît » dand la barre de lanceurs.</p>
      </item>
    </list>

    <p>Pour restaurer la fenêtre :</p>
    <list>
      <item>
       <p>Cliquez dessus dans la <link xref="unity-launcher-intro">barre de lanceurs</link> ou retrouvez-le en appuyant sur la touche <keyseq><key>Alt</key><key>Tab</key></keyseq>.</p>
      </item>
    </list>

    <p>Pour fermer la fenêtre :</p>
    <list>
      <item>
       <p>Cliquez sur le <gui>x</gui> dans le coin supérieur gauche de la fenêtre, ou</p>
      </item>
      <item>
       <p>appuyez sur <keyseq><key>Alt</key><key>F4</key></keyseq> ou</p>
      </item>
      <item>
       <p>appuyez sur <keyseq><key>Alt</key><key>Barre d'espace</key></keyseq> pour faire apparaître le menu de la fenêtre puis sur <key>f</key>.</p>
      </item>
    </list>

</section>

<section id="resize">
<title>Redimensionner</title>

<note style="important">
 <p>Une fenêtre ne peut pas être redimensionnée si elle est <em>maximisée</em>.</p>
</note>
<p>Pour redimensionner votre fenêtre horizontalement et/ou verticalement :</p>
<list>
 <item>
 <p>Déplacez le pointeur de la souris sur un des angles de la fenêtre jusqu'à ce qu'il se transforme en « pointeur d'angle ». Cliquez, maintenez appuyé et faites glisser pour redimensionner la fenêtre dans n'importe quelle direction.</p>
 </item>
</list>
<p>Pour redimensionner uniquement dans la direction horizontale :</p>
<list>
 <item>
 <p>Déplacez le pointeur de la souris sur un des bords verticaux de la fenêtre jusqu'à ce qu'il se transforme en « pointeur de bord vertical ». Cliquez, maintenez appuyé et faites glisser pour redimensionner la fenêtre horizontalement.</p>
 </item>
</list>
<p>Pour redimensionner uniquement dans la direction verticale :</p>
<list>
 <item>
 <p>Déplacez le pointeur de la souris vers le bord supérieur ou inférieur de la fenêtre jusqu'à ce qu'il se transforme en « pointeur de bord supérieur » ou en « pointeur de bord inférieur ». Cliquez, maintenez appuyé et faites glisser pour redimensionner la fenêtre verticalement.</p>
 </item>
</list>

</section>

<section id="arrange">

<title>Positionnement des fenêtres dans votre espace de travail</title>

<p>Pour mettre deux fenêtres côte à côte :</p>
  
<list>
 <item><p>Cliquez sur la <gui>barre de titre</gui> d'une fenêtre et déplacez-la vers le côté gauche de l'écran. Lorsque le <gui>curseur</gui> touche le côté gauche, le côté gauche de l'écran est mis en évidence. Relâcher le bouton de la souris et la fenêtre est redimensionnée à la moitié de l'écran.</p></item>
 <item><p>Déplacez une autre fenêtre vers le côté droit : lorsque le côté droit de l'écran est mis en évidence, relâchez. Chacune des fenêtres remplit la moitié de l'écran.</p></item>
</list>

    <note style="tip">
      <p>En appuyant sur <key>Alt</key> et en cliquant n'importe où dans une fenêtre, vous pouvez la déplacer. Certaines personnes trouvent cela plus facile que de cliquer dans la <gui>barre de titre</gui> d'une application.</p>
    </note>
    
</section>

</page>
