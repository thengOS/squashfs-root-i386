<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="user-goodpassword" xml:lang="fr">

  <info>
    <credit type="author">
      <name>Projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <desc>Utiliser des mots de passe plus longs et plus complexes.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="user-accounts#passwords"/>
    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="14.04" date="2013-10-24" status="candidate"/>
  </info>

  <title>Choix d'un mot de passe sûr</title>

  <note style="important">
    <p>Choisissez des mots de passe dont vous pourrez vous rappeler facilement, mais que les autres (y compris les programmes informatiques) auront beaucoup de mal à deviner.</p>
  </note>

  <p>Un bon mot de passe vous aide à maintenir la sécurité de votre ordinateur. Si votre mot de passe est facile à deviner, d'autres risquent de le trouver et d'accéder à vos données personnelles.</p>
  <p>Il est même possible que des ordinateurs soient utilisés pour deviner votre mot de passe ; ce qu'un humain aurait beaucoup de mal à deviner peut s'avérer extrêmement simple pour un programme informatique. Voici quelques conseils pour choisir un bon mot de passe :</p>

  <list>
    <item>
      <p>Utilisez une combinaison de lettres minuscules ou majuscules, de nombres, de symboles et d'espaces dans le mot de passe. Cela le rend plus difficile à deviner. Il existe plusieurs choix de symboles , donc plus de mots de passe devront être vérifiés par quelqu'un qui chercherait à deviner le votre.</p>
      <note>
        <p>Une bonne méthode pour choisir un mot de passe est de prendre la première lettre de chaque mot dans une phrase dont vous pouvez vous souvenir. La phrase peut être le nom d'un film, d'un livre, d'un chanson ou d'un album. Par exemple, « Flatland : Une Romance à Plusieurs Dimensions » deviendrait F:URàPD ou furapd ou f: urapd.</p>
      </note>
    </item>
    <item>
      <p>Choisissez un mot de passe aussi long que possible. Plus il contient de caractères, plus cela demande de temps à une personne ou à un ordinateur pour le deviner.</p>
    </item>
    <item>
      <p>N'utilisez aucun mot figurant dans un dictionnaire quelle qu'en soit la langue. C'est ce que les crackers de mot de passe essaient en premier. Le mot de passe le plus communément utilisé chez les anglophones est « password ». Un mot de passe comme celui-là est très facile à deviner !</p>
    </item>
    <item>
      <p>N'utilisez pas d'informations personnelles comme une date, un numéro de plaque d'immatriculation ou le nom d'un des membres de la famille.</p>
    </item>
    <item>
      <p>N'utilisez pas de noms.</p>
    </item>
    <item>
      <p>Choisissez un mot de passe qui puisse être tapé rapidement afin de diminuer le risque qu'une personne puisse voir ce que vous tapez.</p>
      <note style="tip">
        <p>N'écrivez jamais vos mots de passe où que ce soit. Ils peuvent être trouvés !</p>
      </note>
    </item>
    <item>
      <p>Utilisez des mots de passe différents pour des choses différentes.</p>
    </item>
    <item>
      <p>Utilisez des mots de passe différents pour des comptes différents.</p>
      <p>Si vous utilisez le même mot de passe pour tous vos comptes, il suffit qu'une personne le trouve pour pouvoir accéder immédiatement à tous vos comptes.</p>
      <p>Il peut être difficile de se souvenir de plusieurs mots de passe. Bien que moins sûr que d'utiliser des mots de passe différents pour tout, il peut être pratique d'utiliser le même pour des choses qui n'ont que peu d'importance (comme des sites Web) et des différents pour des choses plus importantes (comme votre compte bancaire en ligne ou votre courrier électronique).</p>
    </item>
    <item>
      <p>Modifiez régulièrement vos mots de passe.</p>
    </item>
  </list>

</page>
