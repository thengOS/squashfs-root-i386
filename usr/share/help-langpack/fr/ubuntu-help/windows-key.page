<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="question" id="windows-key" xml:lang="fr">

  <info>
    <link type="guide" xref="keyboard"/>
    <desc>La touche Super permet d'accéder au Tableau de bord et au Lanceur.</desc>
    <revision version="14.04" date="2014-03-07" status="review"/>
    <credit type="author">
      <name>Projet de documentation GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
       <name>L'équipe de documentation d'Ubuntu</name>
       <email>ubuntu-doc@lists.ubuntu.com</email>
    </credit>
 
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Qu'est-ce que la touche « Super » ?</title>

  <p>Cette touche se trouve généralement en bas à gauche de votre clavier, juste à côté de la touche <key>Alt</key>, et porte habituellement un logo fenêtre/carrés. Elle est parfois appelée touche Windows, touche logo ou touche système.</p>

<note>
  <p>Si vous avez un clavier Apple, celui-ci ne comportera pas de touche « Windows ». Elle peut être remplacée par la touche <key>⌘</key> (commande).</p>
</note>

  <p>La touche Super a une fonction spéciale dans <em>Unity</em>. Si vous appuyez sur la touche Super, le tableau de bord est affiché. Si vous <em>maintenez enfoncée</em> la touche Super, un écran affichant des raccourcis clavier d'Unity apparaît jusqu'à ce que vous relâchiez la touche Super.</p>

<p>La touche Super peut cependant vous aider à réaliser plus que cela. Pour en savoir plus sur les utilisations de la touche <key>Super</key>, consultez la page des <link xref="shell-keyboard-shortcut">raccourcis clavier</link>.</p>

<!--
<p>To change which key is used to display the activities overview:</p>

  <steps>
    <item>
      <p>Click your name on the menu bar and select <gui>System Settings</gui>.</p>
    </item>
    <item>
      <p>Click <gui>Keyboard</gui>.</p>
    </item>
    <item>
      <p>Click the <gui>Shortcuts</gui> tab.</p>
    </item>
    <item>
      <p>Select <gui>System</gui> on the left side of the window, and
      <gui>Show the activities overview</gui> on the right.</p>
    </item>
    <item>
      <p>Click the current shortcut definition on the far right.</p>
    </item>
    <item>
      <p>Hold down the desired key combination.</p>
    </item>
  </steps>
-->


</page>
