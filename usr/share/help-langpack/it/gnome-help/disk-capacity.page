<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="disk-capacity" xml:lang="it">
  <info>
    <link type="guide" xref="disk"/>

    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <revision pkgversion="3.4.3" date="2012-06-15" status="review"/>
    <revision pkgversion="3.13.91" date="2014-09-05" status="review"/>

    <desc>Use <gui>Disk Usage Analyzer</gui> or <gui>System Monitor</gui> to
    check space and capacity.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Verificare lo spazio libero sul disco</title>

  <p>È possibile controllare la quantità libera di spazio sul disco  con <app>Analizzatore di utilizzo del disco</app> oppure con <app>Monitor di sistema</app>.</p>

<section id="disk-usage-analyzer">
<title>Verificare con «Analizzatore di utilizzo del disco»</title>

  <p>Per verificare lo spazio libero e la capacità del disco usando <app>Analizzatore di utilizzo del disco</app>:</p>

  <list>
    <item>
      <p>Open <app>Disk Usage Analyzer</app> from the <gui>Activities</gui>
      overview. The window will display a list of file locations together with
      the usage and capacity of each.</p>
    </item>
    <item>
      <p>Click one of the items in the list to view a detailed summary of the
      usage for that item. Click the menu button, and then <gui>Scan
      Folder…</gui> or <gui>Scan Remote Folder…</gui> to scan a different
      location.</p>
    </item>
  </list>
  <p>The information is displayed according to <gui>Folder</gui>,
  <gui>Size</gui>, <gui>Contents</gui> and when the data was last
  <gui>Modified</gui>. See more details in <link href="help:baobab"><app>Disk
  Usage Analyzer</app></link>.</p>

</section>

<section id="system-monitor">

<title>Verificare usando «Monitor di sistema»</title>

  <p>Per verificare lo spazio libero e la capacità del disco usando <app>Monitor di sistema</app>:</p>

<steps>
 <item>
  <p>Open the <app>System Monitor</app> application from the <gui>Activities</gui>
  overview.</p>
 </item>
 <item>
  <p>Selezionare la scheda <gui>File system</gui> per visualizzare le parizioni del sistema e l'utilizzo dello spazio del disco. Le informazioni mostrano lo spazio <gui>Totale</gui>, <gui>Libero</gui>, <gui>Disponibile</gui> e <gui>Usato</gui>.</p>
 </item>
</steps>
</section>

<section id="disk-full">

<title>Cosa fare se il disco è pieno?</title>

  <p>Se il disco è pieno è possibile:</p>

 <list>
  <item>
   <p>Eliminare i file poco importanti oppure quelli che non verranno più utilizzati.</p>
  </item>
  <item>
   <p>Effettuare <link xref="backup-why">backup</link> dei file importanti di cui non si avrà necessità nell'immediato futuro ed eliminarli dal disco.</p>
  </item>
 </list>
</section>

</page>
