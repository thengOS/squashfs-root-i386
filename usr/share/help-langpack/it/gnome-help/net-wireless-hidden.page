<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wireless-hidden" xml:lang="it">

  <info>
    <link type="guide" xref="net-wireless"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-12-05" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email its:translate="no">gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <desc>Connect to a wireless network that is not displayed in the network
    list.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>Connettersi a una rete WiFi nascosta</title>

<p>It is possible to set up a wireless network so that it is "hidden." Hidden
 networks won't show up in the list of wireless networks displayed in the
 <gui>Network</gui> settings. To connect to a hidden wireless network:</p>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#yourname">system menu</gui> from the right
    side of the top bar.</p>
  </item>
  <item>
    <p>Select
    <gui><media its:translate="no" type="image" mime="image/svg" src="figures/network-wireless-signal-excellent-symbolic.svg" width="16" height="16"/>
      Wi-Fi Not Connected</gui>. The Wi-Fi section of the menu will expand.</p>
  </item>
  <item>
    <p>Click <gui>Wi-Fi Settings</gui>.</p>
  </item>
  <item><p>Click the <gui>Connect to Hidden Network…</gui> button.</p></item>
 <item>
  <p>In the window that appears, select a previously-connected hidden network
  using the <gui>Connection</gui> drop-down list, or <gui>New</gui> for a new
  one.</p>
 </item>
 <item>
  <p>For a new connection, type the network name and choose the type of
  wireless security from the <gui>Wi-Fi security</gui> drop-down list.</p>
 </item>
 <item>
  <p>Enter the password or other security details.</p>
 </item>
 <item>
  <p>Click <gui>Connect</gui>.</p>
 </item>
</steps>

  <p>You may have to check the settings of the wireless access point or router
  to see what the network name is. If you don't have the network name (SSID),
  you can use the <em>BSSID</em> (Basic Service Set Identifier, the access
  point's MAC address), which looks something like <gui>02:00:01:02:03:04</gui>
  and can usually be found on the underside of the access point.</p>

  <p>You should also check the security settings for the wireless access point.
  Look for terms like WEP and WPA.</p>

<note>
 <p>Si potrebbe ritenere che nascondere la rete WiFi migliori la sicurezza impedendo la connessione a chi non conosce la sua esistenza. In pratica, non è così: la rete è leggermente più difficile da trovare, ma è ancora rilevabile.</p>
</note>

</page>
