<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="tip" id="power-batterylife" xml:lang="it">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspend"/>
    <link type="seealso" xref="shell-exit#suspend"/>
    <link type="seealso" xref="shell-exit#shutdown"/>
    <link type="seealso" xref="display-brightness"/>
    <link type="seealso" xref="power-whydim"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email its:translate="no">gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email its:translate="no">philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Tips to reduce the power consumption of your computer.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Usare meno energia e aumentare la durata della batteria</title>

  <p>Computers can use a lot of power. By using some simple energy-saving
  strategies, you can reduce your energy bill and help the environment.</p>

<section id="general">
  <title>Consigli generali</title>

<list>
  <item>
    <p><link xref="shell-exit#suspend">Sospendere il computer</link> quando non lo si sta usando. Questo riduce significativamente la quantità di energia consumata; inoltre il computer può essere riacceso molto velocemente.</p>
  </item>
  <item>
    <p><link xref="shell-exit#shutdown">Turn off</link> the computer when you
    will not be using it for longer periods. Some people worry that turning off
    a computer regularly may cause it to wear out faster, but this is not the
    case.</p>
  </item>
  <item>
    <p>Use the <gui>Power</gui> panel in <app>Settings</app> to change your
    power settings. There are a number of options that will help to save power:
    you can <link xref="power-whydim">automatically dim the screen</link> after
    a certain time, reduce the <link xref="display-brightness">screen
    brightness</link>, and have the computer
    <link xref="power-suspend">automatically suspend</link> if you have not used
    it for a certain period of time.</p>
  </item>
  <item>
    <p>Turn off any external devices (like printers and scanners) when you are
    not using them.</p>
  </item>
</list>

</section>

<section id="laptop">
  <title>Portatili, netbook e altri dispositivi funzionanti a batteria</title>

 <list>
   <item>
     <p>Reduce the <link xref="display-brightness">screen
     brightness</link>. Powering the screen accounts for a significant fraction
     of a laptop power consumption.</p>
     <p>La maggior parte dei portatili dispongono di pulsanti sulla tastiera (o di scorciatoie da tastiera) per ridurre la luminosità.</p>
   </item>
   <item>
     <p>If you do not need an Internet connection for a little while, turn off
     the wireless or Bluetooth cards. These devices work by broadcasting radio
     waves, which takes quite a bit of power.</p>
     <p>Alcuni computer sono dotati di uno tasto fisico per spegnerle, altrimenti è possibile usare una scorciatoia da tastiera. È possibile riaccendere la scheda all'occorrenza.</p>
   </item>
 </list>

</section>

<section id="advanced">
  <title>Consigli avanzati</title>

 <list>
   <item>
     <p>Ridurre il numero di processi in esecuzione in background perché i computer usano tanta più energia quanto più lavoro devono svolgere.</p>
     <p>Most of your running applications do very little when you are not
     actively using them. However, applications that frequently grab data from
     the internet or play music or movies can impact your power consumption.</p>
   </item>
 </list>

</section>

</page>
