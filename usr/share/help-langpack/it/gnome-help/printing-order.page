<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-order" xml:lang="it">

  <info>
    <link type="guide" xref="printing#paper"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Raggruppare e stampare in ordine inverso.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Stampare le pagine in un diverso ordine</title>

  <section id="reverse">
    <title>Inverso</title>

    <p>Le pagine di un documento vengono solitamente stampate partendo dalla prima e terminando con l'ultima: nel vassoio di stampa, il documento risulterà quindi in ordine inverso. Se necessario, è possibile cambiare questo ordine di stampa.</p>

    <steps>
      <title>Per invertire l'ordine di stampa:</title>
      <item>
        <p>Press <keyseq><key>Ctrl</key><key>P</key></keyseq> to open the Print
        dialog.</p>
      </item>
      <item>
        <p>In the <gui>General</gui> tab, under <gui>Copies</gui>, check
        <gui>Reverse</gui>. The last page will be printed first, and so on.</p>
      </item>
    </steps>

  </section>

  <section id="collate">
    <title>Raggruppato</title>

  <p>If you are printing more than one copy of the document, the print-outs
  will be grouped by page number by default (that is, all of the copies of page
  one come out, then the copies of page two, and so on). <em>Collating</em>
  will make each copy come out with its pages grouped together in the right
  order instead.</p>

  <steps>
    <title>To collate:</title>
    <item>
     <p>Press <keyseq><key>Ctrl</key><key>P</key></keyseq> to open the Print
     dialog.</p>
    </item>
    <item>
      <p>In the <gui>General</gui> tab, under <gui>Copies</gui>, check
      <gui>Collate</gui>.</p>
    </item>
  </steps>

</section>

</page>
