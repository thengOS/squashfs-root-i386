<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="session-formats" xml:lang="it">

  <info>
    <link type="guide" xref="prefs-language"/>
    <link type="seealso" xref="session-language"/>

    <revision pkgversion="3.10" version="0.4" date="2013-11-03" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Scegliere una regione per data e ora, numeri, valuta monetaria e unità di misura.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Modificare data e unità di misura</title>

  <p>All'interno del sistema è possibile controllare i formati utilizzati per visualizzare date, orari, numeri, valuta monetaria e le unità di misura in base alle impostazioni predefinite della propria regione.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Region &amp; Language</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Region &amp; Language</gui> to open the panel.</p>
    </item>
    <item>
      <p>Click <gui>Formats</gui>.</p>
    </item>
    <item>
      <p>Select the region and language that most closely matches the formats
      you would like to use. If your region and language are not listed, click
      <gui><media its:translate="no" type="image" mime="image/svg" src="figures/view-more-symbolic.svg"><span its:translate="yes">...</span></media></gui>
      at the bottom of the list to select from all available regions and
      languages.</p>
    </item>
    <item>
      <p>Click <gui style="button">Done</gui> to save.</p>
    </item>
    <item>
      <p>Respond to the prompt, <gui>Your session needs to be restarted for
      changes to take effect</gui> by clicking
      <gui style="button">Restart Now</gui>, or click
      <gui style="button">X</gui> to restart later.</p>
    </item>
  </steps>

  <p>After you have selected a region, the area to the right of the list shows
  various examples of how dates and other values are shown. Although not shown
  in the examples, your region also controls the starting day of the week in
  calendars.</p>

</page>
