<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="sharing-desktop" xml:lang="it">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.14" date="2015-01-25" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email its:translate="no">jcampbell@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Permette ad altre persone di visualizzare e interagire col proprio desktop usando VNC.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Condividere il proprio desktop</title>

  <p>You can let other people view and control your desktop from another
  computer with a desktop viewing application. Configure <gui>Screen
  Sharing</gui> to allow others to access your desktop and set the security
  preferences.</p>

  <note style="info package">
    <p>You must have the <app>Vino</app> package installed for
     <gui>Screen Sharing</gui> to be visible.</p>

    <if:choose xmlns:if="http://projectmallard.org/if/1.0/">
      <if:when test="action:install">
        <p><link action="install:vino" style="button">Install Vino</link></p>
      </if:when>
    </if:choose>
  </note>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Sharing</gui>.</p>
    </item>
    <item>
      <p>Click on <gui>Sharing</gui> to open the panel.</p>
    </item>
    <item>
      <p>If <gui>Sharing</gui> is <gui>OFF</gui>, switch it to
      <gui>ON</gui>.</p>

      <note style="info"><p>If the text below <gui>Computer Name</gui> allows
      you to edit it, you can <link xref="sharing-displayname">change</link>
      the name your computer displays on the network.</p></note>
    </item>
    <item>
      <p>Select <gui>Screen Sharing</gui>.</p>
    </item>
    <item>
      <p>To let others view your desktop, switch <gui>Screen Sharing</gui> to
      <gui>ON</gui>. This means that other people will be able to attempt to
      connect to your computer and view what's on your screen.</p>
    </item>
    <item>
      <p>To let others interact with your desktop, switch
      <gui>Allow Remote Control</gui> to <gui>ON</gui>. This may allow the
      other person to move your mouse, run applications, and browse files on
      your computer, depending on the security settings which you are currently
      using.</p>

      <note style="tip">
        <p>This option is enabled by default when <gui>Screen Sharing</gui> is
        <gui>ON</gui>.</p>
      </note>

    </item>
  </steps>

  <section id="security">
  <title>Sicurezza</title>

  <p>È importante considerare l'esatta entità di ciascuna opzione di sicurezza prima di cambiarla.</p>

  <terms>
    <item>
      <title>New connections must ask for access</title>
      <p>If you want to be able to choose whether to allow someone to access
      your desktop, enable <gui>New connections must ask for access</gui>.  If
      you disable this option, you will not be asked whether you want to allow
      someone to connect to your computer.</p>
      <note style="tip">
        <p>Questa opzione è abilitata in modo predefinito.</p>
      </note>
    </item>
    <item>
      <title>Require a Password</title>
      <p>To require other people to use a password when connecting to your
      desktop, enable <gui>Require a Password</gui>. If you do not use this
      option, anyone can attempt to view your desktop.</p>
      <note style="tip">
        <p>Questa opzione risulta disabilitata in modo predefinito, ma dovrebbe essere abilitata e configurata con una password sicura.</p>
      </note>
    </item>
<!-- TODO: check whether this option exists.
    <item>
      <title>Allow access to your desktop over the Internet</title>
      <p>If your router supports UPnP Internet Gateway Device Protocol and it
      is enabled, you can allow other people who are not on your local network
      to view your desktop. To allow this, select <gui>Automatically configure
      UPnP router to open and forward ports</gui>. Alternatively, you can
      configure your router manually.</p>
      <note style="tip">
        <p>This option is disabled by default.</p>
      </note>
    </item>
-->
  </terms>
  </section>

  <section id="networks">
  <title>Networks</title>

  <p>The <gui>Networks</gui> section lists the networks to which you are
  currently connected. Use the <gui>ON | OFF</gui> switch next to each to
  choose where your desktop can be shared.</p>
  </section>

  <section id="notification-icon">
  <title>Stop sharing your desktop</title>

  <p>You can disconnect someone who is viewing your desktop using the
  <gui>notification icon</gui> in the message Tray. To do so:</p>
    <steps>
      <item><p>Open the message tray by pressing
      <keyseq><key>Super</key><key>M</key></keyseq>, or by moving your mouse
      pointer to the very bottom of your screen.</p></item>
      <item><p>Click on the <gui>Desktop</gui> icon in the
      <gui>Message Tray</gui>. This will open the <app>Sharing</app> panel.</p>
      </item>
      <item><p>Select <gui>Screen Sharing</gui>.</p></item>
      <item><p>Toggle the <gui>Screen Sharing</gui> slider to <gui>Off.</gui>
      </p></item>
    </steps>

  </section>


</page>
