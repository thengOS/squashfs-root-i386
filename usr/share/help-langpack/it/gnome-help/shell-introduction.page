<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="ui" version="1.0 if/1.0" id="shell-introduction" xml:lang="it">

  <info>
    <link type="guide" xref="shell-overview" group="#first"/>
    <link type="guide" xref="index" group="#first"/>

    <revision pkgversion="3.6.0" date="2012-10-13" status="review"/>
    <revision pkgversion="3.10" date="2013-11-02" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>A visual introduction to your desktop, the top bar, and the
    <gui>Activities</gui> overview.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Introduzione a GNOME</title>

  <p>GNOME 3 fornisce un design per l'interfaccia utente ripensato da zero, per non intralciare, minimizzare le distrazioni e aiutare a completare le proprie attività. All'accesso è visibile solo una scrivania vuota e la barra superiore.</p>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-top-bar.png" width="600" if:test="!target:mobile">
      <p>Barra superiore di GNOME Shell</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-top-bar-classic.png" width="500" height="40" if:test="!target:mobile">
      <p>Barra superiore di GNOME Shell</p>
    </media>
  </if:when>
</if:choose>

  <p>The top bar provides access to your windows and applications, your
  calendar and appointments, and
  <link xref="status-icons">system properties</link> like sound, networking,
  and power. In the status menu in the top bar, you can change the volume or
  screen brightness, edit your <gui>Wi-Fi</gui> connection details, check your
  battery status, log out or switch users, and turn off your computer.</p>

<links type="section"/>

<section id="activities">
  <title><gui>Activities</gui> overview</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-activities.png" style="floatend floatright" if:test="!target:mobile">
      <p>Pulsante Attività</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-activities-classic.png" width="108" height="69" style="floatend floatright" if:test="!target:mobile">
      <p>Pulsante Attività</p>
    </media>
  </if:when>
</if:choose>

  <p if:test="!platform:gnome-classic">To access your windows and applications,
  click the <gui>Activities</gui> button, or just move your mouse pointer to
  the top-left hot corner. You can also press the
  <key xref="keyboard-key-super">Super</key> key on your keyboard. You can
  see your windows and applications in the overview. You can also just start
  typing to search your applications, files, folders, and the web.</p>

  <p if:test="platform:gnome-classic">To access your windows and applications,
  click the <gui xref="shell-introduction#activities">Applications</gui> menu
  at the top left of the screen and select the <gui>Activities Overview</gui>
  item. You can also press the <key xref="keyboard-key-super">Super</key> key
  to see your windows and applications in the <gui>Activities</gui> overview. 
  Just start typing to search your applications, files, and folders.</p>

  <!-- TODO: retake without the flashy bit -->
  <media type="image" src="figures/shell-dash.png" height="300" style="floatstart floatleft" if:test="!target:mobile">
    <p>La dash</p>
  </media>

  <p>A sinistra della panoramica è presente la <em>dash</em>. La dash mostra le proprie applicazioni preferite e quelle in esecuzione. Fare clic su un'icona della dash per aprire la relativa applicazione. Se l'applicazione è già in esecuzione l'icona risulta evidenziata. Facendo clic sulla sua icona verrà portata in primo piano la finestra usata più di recente. È anche possibile trascinare l'icona sulla panoramica, o su un qualsiasi spazio di lavoro sulla destra.</p>

  <p>Facendo clic-destro sull'icona viene mostrato un menù che consente di selezionare una finestra qualsiasi di una applicazione in esecuzione, oppure di aprire una nuova finestra. È anche possibile fare clic sull'icona mentre si tiene premuto il tasto <key>Ctrl</key> per aprire una nuova finestra.</p>

  <p>When you enter the overview, you will initially be in the windows
  overview. This shows you live thumbnails of all the windows on the current
  workspace.</p>

  <p>Click the grid button at the bottom of the dash to display the
  applications overview. This shows you all the applications installed on your
  computer. Click any application to run it, or drag an application to the
  overview or onto a workspace thumbnail. You can also drag an application onto
  the dash to make it a favorite. Your favorite applications stay in the dash
  even when they're not running, so you can access them quickly.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-apps-open">Learn more about starting
      applications.</link></p>
    </item>
    <item>
      <p><link xref="shell-windows">Learn more about windows and
      workspaces.</link></p>
    </item>
  </list>

</section>

<section id="appmenu">
  <title>Application menu</title>
  <if:choose>
    <if:when test="!platform:gnome-classic">
      <media type="image" src="figures/shell-appmenu-shell.png" width="250" style="floatend floatright" if:test="!target:mobile">
        <p>App Menu of <app>Terminal</app></p>
      </media>
      <p>Application menu, located beside the <gui>Activities</gui> button,
      shows the name of the active application alongside with its icon and
      provides quick access to application preferences or help. The items that
      are available in the application menu vary depending on the application.
      </p>
    </if:when>
    <if:when test="platform:gnome-classic">
      <media type="image" src="figures/shell-appmenu-classic.png" width="154" height="133" style="floatend floatright" if:test="!target:mobile">
        <p>App Menu of <app>Terminal</app></p>
      </media>
      <p>Application menu, located next to the <gui>Applications</gui> and
      <gui>Places</gui> menus, shows the name of the active application
      alongside with its icon and provides quick access to application
      preferences or help. The items that are available in the application menu
      vary depending on the application.</p>
    </if:when>
  </if:choose>

</section>

<section id="clock">
  <title>Orologio, calendario e appuntamenti</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-appts.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Clock, calendar, appointments and notifications</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-appts-classic.png" width="373" height="250" style="floatend floatright" if:test="!target:mobile">
      <p>Orologio, calendario e appuntamenti</p>
    </media>
  </if:when>
</if:choose>

  <p>Click the clock on the top bar to see the current date, a month-by-month
  calendar, a list of your upcoming appointments and new notifications. You can
  also open the calendar by pressing
  <keyseq><key>Super</key><key>M</key></keyseq>. You can access the date and
  time settings and open your full <app>Evolution</app> calendar directly from
  the menu.</p>

  <list style="compact">
    <item>
      <p><link xref="clock-calendar">Maggiori informazioni su calendario e appuntamenti.</link></p>
    </item>
    <item>
      <p><link xref="shell-notifications">Maggiori informazioni sulle notifiche e sul cassetto dei messaggi.</link></p>
    </item>
  </list>

</section>


<section id="yourname">
  <title>Tu e il tuo computer</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <media type="image" src="figures/shell-exit.png" width="250" style="floatend floatright" if:test="!target:mobile">
      <p>Menù utente</p>
    </media>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-exit-classic.png" width="250" height="200" style="floatend floatright" if:test="!target:mobile">
      <p>Menù utente</p>
    </media>
  </if:when>
</if:choose>

  <p>Click the system menu in the top-right corner to manage your system
  settings and your computer.</p>

<!-- Apparently not anymore. TODO: figure out how to update status.
  <p>You can quickly set your availability directly from the menu. This will set
  your status for your contacts to see in instant messaging applications such as
  <app>Empathy</app>.</p>-->

<!--
<p>If you set yourself to Unavailable, you won't be bothered by message popups
at the bottom of your screen. Messages will still be available in the message
tray when you move your mouse to the bottom-right corner. But only urgent
messages will be presented, such as when your battery is critically low.</p>

<list style="compact">
  <item><p><link xref="shell-session-status">Learn more about changing
  your availability.</link></p></item>
</list>
-->

  <p>When you leave your computer, you can lock your screen to prevent other
  people from using it. You can also quickly switch users without logging out
  completely to give somebody else access to the computer, or you can
  suspend or power off the computer from the menu.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-exit">Maggiori informazioni su come cambiare utente, terminare la sessione e spegnere il computer.</link></p>
    </item>
  </list>

</section>

<section id="lockscreen">
  <title>Lock Screen</title>

  <media type="image" src="figures/shell-lock.png" width="250" style="floatend floatright" if:test="!target:mobile">
    <p>Lock Screen</p>
  </media>

  <p>When you lock your screen, or it locks automatically, the lock screen is
  displayed. In addition to protecting your desktop while you're away from your
  computer, the lock screen displays the date and time. It also shows
  information about your battery and network status, and allows you to control
  media playback.</p>

  <list style="compact">
    <item>
      <p><link xref="shell-lockscreen">Learn more about the lock
      screen.</link></p>
    </item>
  </list>

</section>

<section id="window-list">
  <title>Window List</title>

<if:choose>
  <if:when test="!platform:gnome-classic">
    <p>GNOME features a different approach to switching windows than a
    permanently visible window list found in other desktop environments. This
    lets you focus on the task at hand without distractions.</p>
    <list style="compact">
      <item>
        <p><link xref="shell-windows-switching">Learn more about switching
        windows.</link></p>
      </item>
    </list>
  </if:when>
  <if:when test="platform:gnome-classic">
    <media type="image" src="figures/shell-window-list-classic.png" width="500" height="34" style="floatend floatright" if:test="!target:mobile">
      <p>Window List</p>
    </media>
    <p>The window list at the bottom of the screen provides access to all your
    open windows and applications and lets you quickly minimize and restore
    them.</p>
    <p>At the right-hand side of the window list, GNOME displays a short
    identifier for the current workspace, such as <gui>1</gui> for the first
    (top) workspace. In addition, the identifier also displays the total number
    of available workspaces. To switch to a different workspace, you can click
    the identifier and select the workspace you want to use from the menu.</p>
    <p>If an application or a system component wants to get your attention, it
    will display a blue icon at the right-hand side of the window list.
    Clicking the blue icon shows the message tray.</p>
  </if:when>
</if:choose>

</section>

</page>
