<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-windows-maximize" xml:lang="it">

  <info>
    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="seealso" xref="shell-windows-tiled"/>

    <revision pkgversion="3.4.0" date="2012-03-14" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Doppio-clic o trascinare la barra del titolo per massimizzare o ripristinare una finestra.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luca Ferretti</mal:name>
      <mal:email>lferrett@gnome.org</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flavia Weisghizzi</mal:name>
      <mal:email>flavia.weisghizzi@ubuntu.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Massimizzare e de-massimizzare una finestra</title>

  <p>È possibile massimizzare una finestra in modo che occupi tutto lo spazio dello schermo oppure de-massimizzarla per riportarla alla sua dimensione originale. La massimizzazione può essere applicata verticalmente lungo i lati destro e sinistro dello schermo, in modo da avere due finestre visibili (per maggiori informazioni, consultare <link xref="shell-windows-tiled"/>).</p>

  <p>To maximize a window, grab the titlebar and drag it to the top of the
  screen, or just double-click the titlebar. To maximize a window using the
  keyboard, hold down the <key xref="keyboard-key-super">Super</key> key and
  press <key>↑</key>, or press
  <keyseq><key>Alt</key><key>F10</key></keyseq>.</p>

  <p if:test="platform:gnome-classic">You can also maximize a window by
  clicking the maximize button in the titlebar.</p>

  <p>Per ripristinare una finestra alle sue dimensioni non massimizzate, trascinarla via dai bordi dello schermo. Se una finestra è completamente massimizzata, è possibile fare doppio-clic sulla barra del titolo per ripristinarla. È anche possibile sfruttare le stesse scorciatoie da tastiera usate per massimizzare la finestra.</p>

  <note style="tip">
    <p>Hold down the <key>Super</key> key and drag anywhere in a window to move
    it.</p>
  </note>

</page>
