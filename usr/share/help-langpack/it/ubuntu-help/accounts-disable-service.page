<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="accounts-disable-service" xml:lang="it">

	<info>
		<credit type="author">
			<name>Phil Bull</name>
			<email>philbull@gmail.com</email>
		</credit>

		<credit type="editor">
    	<name>Greg Beam</name>
    	<email>ki7mt@yahoo.com</email>
    </credit>

		<link type="guide" xref="accounts"/>
		<link type="seealso" xref="accounts-remove"/>
    <revision pkgversion="3.10.2" version="0.1" date="2014-01-08" status="review"/>
		<revision version="14.04" date="2014-01-08" status="review"/>
		<include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Alcuni account online consentono di utilizzare più servizi (come calendario e posta elettronica). È possibile scegliere quali tra questi servizi possono essere usati dalle applicazioni locali.</desc>
	</info>

  <title>Disabilitare i servizi dell'account</title>

  <p>Alcuni tipi di account online permettono l'accesso a diversi servizi da un singolo account utente. Per esempio, gli account di Google forniscono fra l'altro accesso a posta elettronica, calendario, codice, contatti. È possibile scegliere Google per la posta elettronica e Yahoo! per le conversazioni online, oppure una qualsiasi combinazione consentita dal fornitore del servizio.</p>

	<p>Per disabilitare i servizi:</p>

  <steps>
		<item>
			<p>Fare clic sull'icona a destra nella <gui>barra dei menù</gui> e selezionare <gui>Impostazioni di sistema</gui>.</p>
		</item>
		<item>
			<p>Selezionare <gui>Account Online</gui>.</p>
		</item>

		<item>
      <p>Selezionare l'account da modificare nel pannello a sinistra.</p>
    </item>
    <item>
      <p>I servizi disponibili per questo account sono elencati nel pannello a destra.</p>
    </item>
    <item>
      <p>Disabilitare i servizi da non utilizzare.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Una volta che un servizio è stato disabilitato, le applicazioni locali non possono più accedervi. Per ottenere nuovamente l'accesso, ritornare su <gui>Account Online</gui> e abilitare il servizio.</p>
  </note>

</page>
