<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="addremove-remove" xml:lang="it">

  <info>
    <credit type="author">
      <name>Ubuntu Documentation Team</name>
    </credit>
    <desc>Rimuovere software non più utilizzato.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="addremove" group="#first"/>
    <revision version="16.04" date="2016-03-13" status="review"/>
  </info>

  <title>Rimuovere un'applicazione</title>

  <p>
    <app>Ubuntu Software</app> helps you to remove software that you 
    no longer use.
  </p>
    
  <steps>
    <item>
      <p>
        Click the <app>Ubuntu Software</app> icon in the <gui>Launcher</gui>, or search
        for <input>Software</input> in the search bar of the <gui>Dash</gui>.
      </p>
    </item>
    <item>
      <p>
        When <app>Ubuntu Software</app> opens, click the <gui>Installed</gui> button at 
        the top.
      </p>
    </item>
    <item>
      <p>Trovare l'applicazione da rimuovere usando la casella di ricerca oppure scorrendo l'elenco delle applicazioni installate.</p>
    </item>
    <item>
      <p>Selezionare l'applicazione e fare clic su <gui>Rimuovi</gui>.</p>
    </item>
    <item>
      <p>Viene richiesto di inserire la password. Una volta digitata correttamente, l'applicazione viene rimossa.</p>
    </item>
  </steps>

  <note>
    <p>Alcune applicazioni necessitano dell'installazione di altro software per poter funzionare correttamente. Tentando di rimuovere un'applicazione necessaria a un'altra, verranno rimosse entrambe. Prima di procedere con tale operazione, viene comunque richiesta una conferma.</p>
  </note>

</page>
