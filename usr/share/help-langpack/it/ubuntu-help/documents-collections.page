<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="documents-collections" xml:lang="it">

  <info>
    <credit type="author copyright">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <desc>Gruppo di documenti correlati in una collezione.</desc>
    <link type="guide" xref="documents#print"/>
    <revision pkgversion="3.5.90" date="2012-09-05" status="review"/>
    <revision version="13.10" date="2013-10-24" status="review"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Creare una collezione di documenti</title>

  <p><app>Documenti</app> consente di raccogliere documenti di diverso tipo in una posizione chiamata <em>collezione</em>. Se ci sono documenti correlati, è possibile raggrupparli per renderne più facile la ricerca. Per esempio, nel caso di un viaggio di lavoro durante il quale è stata effettuata una presentazione, è possibile raggruppare, in una collezione le diapositive, l'itinerario del volo (un file PDF), il tabulato delle spese e altri documenti ibridi PDF o ODF.</p>

  <p>Per creare o  effettuare aggiunte a una collezione:</p>
  <list>
   <item><p>Fare clic sul pulsante <gui>✓</gui>.</p></item>
   <item><p>Nella modalità selezione, selezionare i documenti da mettere insieme.</p></item>
   <item><p>Fare clic sul pulsante <gui>+</gui> nella barra dei pulsanti.</p></item>
   <item><p>Nell'elenco della collezione, fare clic su <gui>Aggiungi</gui> e digitare il nome di una nuova collezione o selezionarne una esistente: i documenti selezionati saranno aggiunti alla collezione.</p></item>
  </list>

  <note>
   <p>Le collezioni non si comportano come cartelle e non ne seguono la gerarchia: <em>non è possibile inserire una collezione in un'altra.</em></p>
  </note>

  <p>Per eliminare una collezione:</p>
  <list>
   <item><p>Fare clic sul pulsante <gui>✓</gui>.</p></item>
   <item><p>Nella modalità selezione, selezionare la collezione da eliminare.</p></item>
   <item><p>Fare clic sul pulsante Cestino nella barra dei pulsanti: la collezione verrà eliminata, lasciando i documenti originali.</p></item>
  </list>

</page>
