<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="net-findip" xml:lang="it">
  <info>
    <link type="guide" xref="net-general"/>
    <link type="seealso" xref="net-what-is-ip-address"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>Conoscere il proprio indirizzo IP può essere utile per la risoluzione dei problemi di rete.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Trovare il proprio indirizzo IP</title>

  <p>Conoscere il proprio indirizzo IP può essere utile per risolvere problemi con la propria connessione a Internet. Si potrebbe essere sorpresi scoprendo che si hanno <em>due</em> indirizzi IP: uno che identifica il computer nella rete locale e un altro che lo identifica in Internet.</p>

  <steps>
    <title>Trovare il proprio indirizzo IP nella rete locale</title>
    <item><p>Fare clic sull'icona a destra nella <gui>barra dei menù</gui> e selezionare <gui>Impostazioni di sistema</gui>.</p></item>
    <item><p>Aprire <gui>Rete</gui> e selezionare <gui>Via cavo</gui> o <gui>Wireless</gui> nell'elenco sulla sinistra, a seconda della  connessione di rete da utilizzare per la ricerca dell'indirizzo IP.</p></item>
    <item><p>Il proprio indirizzo IP interno verrà visualizzato nell'elenco delle informazioni.</p></item>
  </steps>

  <steps>
  	<title>Trovare il proprio indirizzo IP esterno</title>
    <item><p>Visitare la pagina <link href="http://whatismyipaddress.com/">whatismyipaddress.com</link>.</p></item>
    <item><p>Il sito mostrerà il proprio indirizzo IP esterno.</p></item>
  </steps>

<p>In base a come il computer si connette a Internet, questi indirizzi possono essere gli stessi.</p>

</page>
