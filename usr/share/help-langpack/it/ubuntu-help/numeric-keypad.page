<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="numeric-keypad" xml:lang="it">

  <info>
    <link type="guide" xref="keyboard"/>
    <desc>Abilitare in maniera predefinita il tastierino numerico.</desc>

    <revision version="16.04" date="2016-05-08" status="review"/>

    <credit type="author">
      <name>Ubuntu Documentation Team</name>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Tastierino numerico</title>

  <p>È possibile abilitare e disabilitare manualmente il tastierino numerico sulla tastiera con il tasto <key>Bloc Num</key>. Le impostazioni del BIOS dei computer spesso contengono un'opzione che consente di decidere se il tastierino numerico debba essere abilitato all'avvio del computer, altrimenti questo è un metodo per configurare questa opzione:</p>

  <steps>
    <item>
      <p><link xref="addremove-install">Installare</link> il pacchetto <em>numlockx</em>.</p>
    </item>
    <item>
      <p>Aprire le <app><link xref="startup-applications">Applicazioni d'avvio</link></app> e aggiungere un programma d'avvio con il comando: <cmd>numlockx on</cmd></p>
    </item>
  </steps>

  <p>Se è necessario che il tastierino numerico sia attivato anche durante la schermata di accesso, è possibile creare uno speciale file di configurazione. Premere <keyseq><key>Ctrl</key><key>Alt</key><key>T</key></keyseq> per aprire un terminale e quindi eseguire questo comando (è un unico lungo comando, che potrebbe essere visualizzato su più righe sullo schermo - eseguire un copia e incolla per immetterlo correttamente):</p>

  <p><cmd its:translate="no">sudo sh -c 'printf "[Seat:*]\ngreeter-setup-script=numlockx on\n" &gt;/etc/lightdm/lightdm.conf.d/50-numlock.conf'</cmd></p>

</page>
