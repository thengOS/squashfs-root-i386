<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="printing-setup" xml:lang="it">

  <info>
    <link type="guide" xref="printing#setup" group="#first"/>

    <desc>Impostare una stampante collegata al computer.</desc>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email>stickster@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Steven Richards</name>
      <email>steven.richardspc@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Impostare una stampante locale</title>
  <p>Il sistema riconosce automaticamente molti tipi di stampanti, quando vengono collegate: per molte di esse la connessione al computer avviene mediante un cavo USB.</p>

 <note style="tip">
  <p>Non è necessario selezionare se installare una stampante locale o di rete: sono elencate in una sola finestra.</p>
 </note>

  <steps>
    <item><p>Assicurarsi che la stampante sia accesa.</p>
    </item>
    <item><p>Collegare la stampante al sistema usando il cavo appropriato: è possibile che sullo schermo venga visualizzata la ricerca dei driver da parte del sistema e che sia necessario immettere la propria password di autenticazione per installarli.</p>
    </item>
    <item><p>Quando il sistema avrà completato l'installazione, verrà visualizzato un messaggio: selezionare <gui>Stampa di prova</gui> per provare il funzionamento della stampante o <gui>Opzioni</gui> per apportare modifiche alla configurazione della stampante.</p>
    </item>
  </steps>

  <p>Se la stampante non è stata configurata automaticamente, è possibile aggiungerla manualmente.</p>

  <steps>
    <item><p>Fare clic sull'icona a destra nella <gui>barra dei menù</gui> e selezionare <gui>Impostazioni di sistema</gui>.</p></item>
    <item><p>Aprire <gui>Stampanti</gui>.</p></item>
    <item><p>Fare clic su <gui>Aggiungi</gui> e selezionare la stampante nel riquadro «Periferiche».</p></item>
    <item><p>Fare clic su <gui>Avanti</gui> e attendere durante la ricerca dei driver.</p></item>
    <item><p>È possibile personalizzare il nome, la descrizione e la posizione della stampante: al termine, fare clic su <gui>Applica</gui>.</p></item>
    <item><p>È possibile effettuare una stampa di prova o fare clic su <gui>Annulla</gui> per saltare questo passaggio.</p></item>
  </steps>

  <note>
    <p>Se sono disponibili diversi driver per la stampante, occorrerà sceglierne uno: per usare il driver consigliato, fare clic su Avanti nelle schermate della marca e modello della stampante.</p>
  </note>

  <p>Dopo aver installato la stampante, può essere necessario <link xref="printing-setup-default-printer">cambiare la stampante predefinita</link>.</p>

</page>
