<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="screen-shot-record" xml:lang="it">

  <info>

    <link type="guide" xref="tips"/>
    <link type="seealso" xref="shell-keyboard-shortcuts"/>

    <desc>Catturare una schermata del proprio schermo.</desc>
    <revision pkgversion="3.6.1" version="0.2" date="2012-11-10" status="review"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
      <years>2011</years>
    </credit>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Schermate</title>

<p>È possibile scattare una fotografia del proprio schermo (una <em>schermata</em>) per mostrare, per esempio, come svolgere alcune azioni ad altre persone. La schermate sono delle normali immagini ed è quindi possibile condividerle via email o attraverso il Web.</p>

<section id="screenshot">
 <title>Catturare una schermata</title>
 <p>Per catturare una schermata dello schermo intero:</p>
<steps>
<item><p>Aprire la <gui>Dash</gui> e avviare lo strumento <app>Schermata</app>.</p></item>

<item>
 <p>Nella finestra <app>Cattura schermata</app>, selezionare se catturare l'intero schermo, una singola finestra o un'area dello schermo. Impostare un ritardo se è necessario selezionare una finestra o impostare in altro modo lo schermo per la cattura, quindi scegliere gli effetti desiderati.</p>
</item>

<item>
  <p>Fare clic su <gui>Cattura schermata</gui>.</p>
  <p>Se è stato selezionato <gui>Seleziona l'area da catturare</gui> il puntatore assumerà la forma di una piccola croce: fare clic e trascinare sull'area da catturare.</p>
</item>

<item>
  <p>Nella finestra <gui>Salvataggio schermata</gui>, digitare un nome e scegliere una cartella, quindi fare clic su <gui>Salva</gui>.</p>
</item>
</steps>

</section>

</page>
