<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="sharing-bluetooth" xml:lang="it">
  <info>
    <link type="guide" xref="bluetooth"/>

    <desc>Opzioni di condivisione e ricezione di file via Bluetooth.</desc>

    <revision version="14.10" date="2014-09-14" status="review"/>

    <credit type="author">
      <name>Ubuntu Documentation Team</name>
      <email>ubuntu-doc@lists.ubuntu.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Controllo della condivisione tramite Bluetooth</title>

  <p>È possibile consentire l'accesso alle cartelle <file>Pubblici</file> e <file>Scaricati</file> per la condivisione di file via Bluetooth e anche restringere l'accesso ai soli <em>dispositivi affidabili</em>. Configurare <gui>Preferenze personali della condivisione di file</gui> per controllare l'accesso alle cartelle condivise sul proprio computer.</p>

  <note style="info">
    <p>Un dispositivo Bluetooth è <em>affidabile</em> se è stato <em>sincronizzato</em> o connesso al computer. Consultare <link xref="connettere-dispositivo-bluetooth"/>.</p>
  </note>

  <steps>
    <title>Condividere la propria cartella <file>Pubblici</file> tramite Bluetooth</title>
    <item>
      <p>Nella <gui>Dash</gui>, aprire <app>Condivisione di file personali</app>.</p>
    </item>
    <item>
      <p>Selezionare nell'elenco le opzioni per la condivisione e ricezione di file via Bluetooth.</p>
    </item>
  </steps>

</page>
