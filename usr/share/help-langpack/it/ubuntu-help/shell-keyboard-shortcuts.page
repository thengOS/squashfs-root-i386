<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="tip" version="1.0 ui/1.0" id="shell-keyboard-shortcuts" xml:lang="it">

  <info>
    <link type="guide" xref="tips"/>
    <link type="guide" xref="keyboard"/>
    <link type="guide" xref="shell-overview"/>
    <link type="seealso" xref="windows-key"/>
    <desc>Spostarsi all'interno dell'ambiente grafico usando la tastiera.</desc>
    <revision pkgversion="3.4.0" date="2012-03-14" status="candidate"/>
    <revision version="13.10" date="2013-10-23" status="outdated"/>
    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Utili scorciatoie da tastiera</title>

<p>Questa pagina fornisce una panoramica delle scorciatoie da tastiera che possono essere di aiuto nell'utilizzo dell'ambiente grafico e delle applicazioni. Nel caso non fosse possibile utilizzare un mouse o un sistema di puntamento, consultare <link xref="keyboard-nav"/> per informazioni su come utilizzare l'ambiente grafico solamente da tastiera.</p>

<table rules="rows" frame="top bottom" ui:expanded="true">
<title>Usare l'ambiente grafico</title>
  <tr>
    <td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
    <td><p>Chiude la finestra attuale.</p></td>
  </tr>
  <tr xml:id="alt-f2">
    <td><p><keyseq><key>Alt</key><key>F2</key></keyseq></p></td> <td><p>Attiva la finestra comandi (per eseguire rapidamente specifici comandi)</p></td>
  </tr>
  <tr xml:id="alt-tab">
    <td><p><keyseq><key>Alt</key><key>Tab</key></keyseq></p></td>
    <td><p><link xref="shell-windows-switching">Passa velocemente da una finestra all'altra.</link> Tenere premuto il tasto <key>Maiusc</key> per andare in ordine inverso.</p></td>
  </tr>
  <tr xml:id="alt-tick">
    <td><p><keyseq><key>Alt</key><key>\</key></keyseq></p></td>
    <td><p>Passa rapidamente da una finestra di una applicazione a un'altra della stessa applicazione, attivabile anche dopo aver selezionato una applicazione con <keyseq><key>Alt</key><key>Tab</key></keyseq>.</p>
    <p>Questa scorciatoia usa <key>\</key> sulle tastiere con disposizione italiana, dove il tasto <key>\</key> è da intendersi come il tasto immediatamente sopra al tasto <key>Tab</key>. Per le altre disposizioni, assieme al tasto <key>Alt</key> si dovrà premere il tasto sopra a  <key>Tab</key>.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key xref="windows-key">Super</key><key>S</key></keyseq></p></td>
    <td><p>Attiva il selettore dello spazio di lavoro presentando una panoramica di tutti gli <link xref="shell-workspaces">spazi di lavoro</link>.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key xref="windows-key">Super</key><key>W</key></keyseq></p></td>
    <td><p>Attiva la modalità «Expo» mostrando le finestre dello spazio di lavoro attuale.</p></td>
  </tr>
  <tr xml:id="ctrl-alt-updown">
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Tasti freccia</key></keyseq></p></td>
    <td><p><link xref="shell-workspaces-switch">Passa da uno spazio di lavoro all'altro.</link></p></td>
  </tr>
  <tr xml:id="ctrl-alt-shift-updown">
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Maiusc</key><key>Tasti freccia</key></keyseq></p></td>
    <td><p><link xref="shell-workspaces-movewindow">Sposta la finestra attuale in uno spazio di lavoro differente.</link></p></td>
  </tr>
  <tr xml:id="ctrl-alt-Del">
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Canc</key></keyseq></p></td>
    <td><p><link xref="shell-exit">Termina la sessione.</link></p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key xref="windows-key">Super</key><key>D</key></keyseq></p></td>
    <td><p>Nasconde tutte le finestre attive. Premere nuovamente per ripristinarle tutte.</p></td>
  </tr>
  <tr xml:id="ctrl-alt-l">
    <td><p><keyseq><key>Ctrl</key><key>Alt</key><key>L</key></keyseq></p></td>
    <td><p><link xref="shell-exit#lock-screen">Blocca lo schermo.</link></p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Scorciatoie comuni</title>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>A</key></keyseq></p></td>
    <td><p>Seleziona tutto il testo o gli elementi in un elenco.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>X</key></keyseq></p></td>
    <td><p>Taglia (rimuove) il testo selezionato o gli elementi e li mette negli appunti.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>C</key></keyseq></p></td>
    <td><p>Copia il testo selezionato o gli elementi negli appunti.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>V</key></keyseq></p></td>
    <td><p>Incolla il contenuto degli appunti.</p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Ctrl</key><key>Z</key></keyseq></p></td>
    <td><p>Annulla l'ultima azione.</p></td>
  </tr>
</table>

<table rules="rows" frame="top bottom" ui:expanded="false">
<title>Catturare immagine dallo schermo</title>
  <tr>
    <td><p><key>Stamp</key></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Cattura una schermata.</link></p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Alt</key><key>Stamp</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Cattura una schermata di una finestra.</link></p></td>
  </tr>
  <tr>
    <td><p><keyseq><key>Maiusc</key><key>Stamp</key></keyseq></p></td>
    <td><p><link xref="screen-shot-record#screenshot">Cattura una schermata di un'area dello schermo</link>. Il puntatore assumerà la forma di una piccola croce: fare clic e trascinare il puntatore per selezionare un'area.</p></td>
  </tr>
</table>

</page>
