<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="question" id="unity-scrollbars-intro" xml:lang="it">
  <info>
    <link type="guide" xref="shell-overview#desktop"/>
    <link type="seealso" xref="mouse-touchpad-click"/>

    <desc>Sono i piccoli elementi grafici arancioni presenti sul bordo nei documenti molto lunghi.</desc>
    <revision version="13.10" date="2013-10-23" status="outdated"/>

    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Cosa sono le barre di scorrimento sovrapposte?</title>

<p>Ubuntu sfrutta le <em>barre di scorrimento sovrapposte</em>, elementi grafici che utilizzano meno spazio rispetto alle barre di scorrimento tradizionali, fornendo così più spazio ai contenuti. Benché ispirate ai dispositivi portatili, dove le normali barre di scorrimento non sono necessarie, le barre di scorrimento sovrapposte di Ubuntu sono progettate per funzionare correttamente anche con un mouse.</p>

<p>Alcune applicazioni come Firefox e LibreOffice non supportano ancora le nuove barre di scorrimento.</p>

<section id="using">
  <title>Usare le barre di scorrimento</title>

  <p>Le barre di scorrimento sovrapposte sono rappresentate da una piccola striscia di colore arancione sul bordo di un elemento. La posizione della barra rispecchia la posizione nel del contenuto visualizzato. La lunghezza della striscia di colore corrisponde alla lunghezza del contenuto: più corta la striscia, più lungo il contenuto.</p>

  <p>Spostare il puntatore del mouse su un punto qualsiasi del bordo per mostrare il <gui>cursore scorrevole</gui>.</p>

  <list>
    <title>Metodi per utilizzare le barre di scorrimento:</title>
    <item><p>Trascinare il <gui>cursore scorrevole</gui> in su o giù per spostarsi alla posizione voluta del contenuto.</p></item>
    <item><p>Fare clic sulla barra di scorrimento per spostare la finestra alla posizione desiderata.</p></item>
  </list>
</section> 

</page>
