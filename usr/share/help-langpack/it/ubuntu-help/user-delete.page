<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="user-delete" xml:lang="it">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <desc>Rimuovere gli utenti che non utilizzano più il computer.</desc>
    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="13.10" date="2013-10-23" status="review"/>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Progetto documentazione di GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>
  <title>Eliminare un account utente</title>

  <p>È possibile aggiungere molteplici account al proprio computer (consultare <link xref="user-add"/>). Se qualcuno di questi account non utilizza più il computer, è possibile rimuoverlo.</p>

<steps>
  <item><p>Fare clic sull'icona a destra nella <gui>barra dei menù</gui> e selezionare <gui>Impostazioni di sistema</gui>.</p></item>
  <item><p>Aprire <gui>Account utente</gui>.</p></item>
  <item><p>Fare clic su <gui>Sblocca</gui> nell'angolo in alto a destra e digitare la propria password. Per poter eliminare degli account è necessario essere un utente con diritti di amministrazione.</p></item>
  <item><p>Selezionare l'utente da eliminare e premere il pulsante <gui>-</gui>.</p></item>
  <item><p>Ogni utente dispone della propria cartella personale con i propri file e impostazioni. È possibile scegliere se mantenere o eliminare questa cartella. Fare clic su :<gui>Elimina file</gui>  se si è sicuri non verranno più usati ed è necessario liberare spazio sul disco. I file saranno eliminati per sempre e non sarà possibile ripristinarli. Prima di compiere quest'azione potrebbe essere utile copiarli su un disco esterno o su un CD come copia di backup.</p></item>
</steps>
</page>
