<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="backup-thinkabout" xml:lang="pt">

  <info>
    <link type="guide" xref="files#backup"/>
    <desc>Uma lista de pastas onde poderá encontrar documentos, arquivos e configurações que poderá querer fazer o backup.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="13.10" date="2013-09-07" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Projeto de Documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Onde posso encontrar os arquivos que desejo fazer o backup?</title>

  <p>Listados abaixo estão os locais mais comuns com arquivos importantes e configurações que poderá querer fazer um backup.</p>

<list>
 <item>
  <p>Arquivos pessoais (documentos, músicas, fotos e vídeos)</p>
  <p xmlns:its="http://www.w3.org/2005/11/its" its:locNote="translators: xdg    dirs are localised by package xdg-user-dirs and need to be translated.  You    can find the correct translations for your language here:    http://translationproject.org/domain/xdg-user-dirs.html">Estes são geralmente armazenados na sua pasta pessoal (<file>/home/Seu_Nome</file>). Os mesmos poderiam estar em sub-pastas, como ambiente de trabalho, documentos, imagens, música e vídeos.</p>
  <p>Se o seu dispositivo de backup tem espaço suficiente (no caso de ser um disco rígido externo, por exemplo), é aconselhável fazer um backup de toda a pasta Principal. Pode descobrir quanto de espaço no disco a sua pasta ocupa usando utilizando o <app>Analisador de utilização de disco</app>.</p>
 </item>

 <item>
  <p>Ficheiros ocultos</p>
  <p>Qualquer arquivo ou pasta que tenha um nome começado por um ponto (.) está oculta por padrão. Para ver os arquivos ocultos, clique <guiseq><gui>Ver</gui><gui>Mostrar arquivos ocultos</gui></guiseq> ou pressione <keyseq><key>Ctrl</key><key>H</key></keyseq>. Poderá copiá-los para um local de backup como qualquer outro arquivo.</p>
 </item>

 <item>
  <p>Configurações pessoais (preferências do desktop, temas e configurações de software)</p>
  <p>A maioria das aplicações armazenam as suas configurações em pastas ocultas dentro de sua pasta Pessoal (veja acima para obter informações sobre os arquivos ocultos).</p>
  <p>A maioria das configurações das aplicações serão armazenadas nas pastas ocultas <file>.Config</file>, <file>.Gconf</file>, <file>.Gnome2</file> e <file>. Local </file> na sua pasta Principal.</p>
 </item>

 <item>
  <p>Configurações de todo o sistema</p>
  <p>As configurações de importantes partes do sistema não são armazenados na pasta Pessoal. Existe um número de locais em que podem ser armazenadas, mas a maioria são armazenadas na pasta <file>/etc</file>. Numa forma geral, não irá precisar fazer backup desses arquivos no seu computador de uso doméstico. Se por outro lado estiver a executar um servidor, deve fazer backup dos arquivos dos serviços que o mesmo está a executar.</p>
 </item>
</list>

</page>
