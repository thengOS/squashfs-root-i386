<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-calibrate-printer" xml:lang="pt">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-screen"/>
    <link type="seealso" xref="color-calibrate-camera"/>
    <desc>Calibrar a sua impressora é importante para que imprima cores precisas.</desc>
    <revision version="13.10" date="2013-09-20" status="review"/>
    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Como faço para calibrar a minha impressora?</title>

  <p>Há duas formas para criar perfis de uma impressora:</p>

  <list>
    <item><p>Utilizando um dispositivo espectrofotómetro como o Pantone ColorMunki</p></item>
    <item><p>Efetuado o download e a impressão de um arquivo de referência a partir de uma empresa de cores</p></item>
  </list>

  <p>Utilizando uma empresa de cores para gerar um perfil de impressora é geralmente a opção mais barata se apenas tiver um ou dois tipos de papeis diferentes. Ao fazer o download da tabela de referência a partir do site da empresa pode de seguida enviar de volta a impressão num envelope almofadado, onde a empresa de cor irá fazer um scan do papel e gerar um perfil, que lhe será enviado de volta por email um perfil ICC preciso.</p>
  <p>Utilizando um dispositivo dispendioso, como um ColorMunki, só será mais económico se estiver a criar um perfil de elevado número de conjuntos de tintas ou tipos de papel.</p>

  <note style="tip">
    <p>Se alterar o seu fornecedor de tinta, certifique-se de recalibrar a sua impressora!</p>
  </note>

</page>
