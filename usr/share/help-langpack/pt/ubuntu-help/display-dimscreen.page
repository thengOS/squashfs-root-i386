<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-dimscreen" xml:lang="pt">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="power#problems"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="seealso" xref="power-whydim"/>
    <link type="seealso" xref="display-lock"/>
    <revision pkgversion="3.7.1" version="0.3" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-09-07" status="review"/>
    <desc>Escurecer a ecrã para economizar energia ou aumentar o brilho para torná-lo mais legível sob luz forte.</desc>
    <credit type="author">
      <name>Projeto de Documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Configure o brilho do ecrã</title>

  <p>Pode mudar o brilho do seu ecrã para economizar energia ou para fazer com que o ecrã seja mais visível com luz brilhante. Pode também ter o ecrã a escurecer automaticamente quando em modo de bateria ou com que o ecrã desligue automaticamente quando não estiver em uso.</p>

  <steps>
    <title>Configure o brilho</title>
  <item><p>Clique no ícone à direita na <gui>barra de menus</gui> e selecione <gui>Definições do sistema</gui></p></item>
    <item><p>Selecione <gui>Brilho &amp; Trancar</gui>.</p></item>
    <item><p>Ajuste o controlo de <gui>Brilho</gui> para um valor confortável.</p></item>
  </steps>

  <note style="tip">
    <p>Muitos teclados dos portáteis têm teclas especiais para ajustar o brilho. Estas têm uma imagem que se parece com o sol e estão localizadas nas teclas de função no topo. Mantenha pressionada a <key>Fn</key> para usar essas teclas.</p>
  </note>

  <p>Selecione <gui> Escurecer ecrã para economizar energia</gui> para ter o brilho automaticamente baixo quando estiver com pouca bateria. A luz de trás do seu ecrã pode consumir muita energia e reduzir significativamente a sua bateria até ser recarregada.</p>

  <p>O ecrã irá desligar automaticamente se não usá-lo por um tempo. Isto afeta somente o ecrã e não desliga o seu computador. Pode ajustar quanto tempo deve ficar inativo com a opção <gui>Desligar o ecrã quando inativo por</gui>.</p>
</page>
