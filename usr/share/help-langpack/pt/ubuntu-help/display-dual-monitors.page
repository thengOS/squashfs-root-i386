<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" id="display-dual-monitors" xml:lang="pt">

  <info>
    <desc>Defina monitor duplo no seu Portátil.</desc>
    <revision pkgversion="3.8.2" date="2014-01-19" status="review"/>
    <revision version="14.04" date="2014-01-19" status="review"/>
    <credit type="author">
      <name>Equipa de documentação do Ubuntu</name>
      <email>ubuntu-doc@lists.ubuntu.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="prefs-display"/>
  </info>

<title>Ligue um monitor externo ao seu portatil</title>

	<section id="unity-steps">
		<title>Defina um monitor externo</title>
		<p>Para configurar um monitor externo com o seu computador portátil, ligue o monitor ao seu portátil. Se o sistema não o reconhecer imediatamente, ou se gostaria de ajustar as definições:</p>
		<steps>
			<item>
				<p>Clique no ícone à direita na <gui>barra de menus</gui> e selecione <gui>Definições do sistema</gui></p>
			</item>
			<item>
				<p>Abrir <gui>Ecras</gui>.</p>
			</item>
			<item>
				<p>Clique na imagem do monitor que gostaria de ativar ou desativar, em seguida, mude-o para <gui>ON/OFF</gui>.</p>
			</item>
			<item>
				<p>Por omissão, o Launcher é exibido apenas no ecrã principal. Para modificar qual o ecrã será o "principal", substitua-o no menu <gui>posição do Launcher</gui>. Poderá também arrastar o Launcher para o ecrã que você deseja definir como "principal" na visualização.</p>
				<p>Se pretender que o Launcher apareça em todos os ecrãs, modifique <gui>Posição do Launcher</gui> para <gui>Todos os ecrãs</gui>.</p>
			</item>
			<item>
				<p>Para alterar a "posição" de um monitor, clique sobre ele e arraste até à posição pretendida.</p>
				<note>
					<p>Se deseja que os dois monitores disponibilizem a mesma informação, seleccione a caixa <gui>Monitor Espelho</gui>.</p>
				</note>
			</item>
			<item>
				<p>Quando estiver satisfeito com as configurações, clique em <gui>Aplicar</gui> e em seguida clique em <gui>Manter Esta Configuração</gui>.</p>
			</item>
			<item>
				<p>Para fechar os <gui>Visores</gui> clique no <gui>x</gui> no canto superior.</p>
			</item>
		</steps>
	</section>

	<section id="sticky-edges">
		<title>Cantos pegajosos</title>
		<p>Um problema típico com ecrãs duplos é que é fácil para o ponteiro do rato "escorregar" para o outro ecrã quando você não quer que isto aconteça. O recurso <gui>Bordas aderentes</gui> do Unity ajuda com este problema, a requerer que você empurre um pouco mais forte para que o ponteiro do rato se mova de um ecrã para o outro.</p>
		<p>Pode desligar as <gui>Margens coladas</gui> se não gostar.</p>
	</section>

</page>
