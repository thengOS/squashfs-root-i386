<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="display-lock" xml:lang="pt">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="seealso" xref="session-screenlocks"/>
    <link type="seealso" xref="shell-exit#lock-screen"/>
    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-09-07" status="review"/>
    <desc>Impedir que outras pessoas utilizem sua ambiente de trabalho enquanto não estiver longe do computador.</desc>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Bloqueie automaticamente o seu ecrã</title>

  <p>Quando não está a utilizar o seu computador, deve <link xref="shell-exit#lock-screen">bloquear o ecrã</link> para prevenir que outras pessoas usem a sua área de trabalho e acessem os seus arquivos. Estará na mesma sessão e todas as suas aplicações continuam a ser executadas, mas terá que digitar sua palavra-passe para usar o computador novamente. Pode bloquear o ecrã manualmente, mas também pode definir bloqueio automático.</p>

  <steps>
    <item><p>Clique no ícone à direita na <gui>barra de menus</gui> e selecione <gui>Definições do sistema</gui></p></item>
    <item><p>Selecione <gui>Brilho &amp; Trancar</gui>.</p></item>
    <item><p>Certifique-se de que a chave <gui>Bloquear</gui> está na posição ligada, então selecione o tempo limite na lista abaixo. O ecrã será automaticamente bloqueado após você ter ficado inativo por esse período de tempo. Você também pode selecionar <gui>Desligar ecrã</gui> para bloquear o ecrã depois que ele é automaticamente desligado, controlado pela lista <gui>Desligar o ecrã quando inativo por</gui>, que está acima.</p></item>
  </steps>

</page>
