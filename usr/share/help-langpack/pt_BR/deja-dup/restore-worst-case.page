<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" its:version="1.0" type="topic" style="task" id="restore-worst-case" xml:lang="pt-BR">
<info>
  <desc>O que fazer se você não conseguir restaurar seus arquivos</desc>
  <link type="guide" xref="index#restore"/>
</info>

<title>Quando tudo dá errado</title>

<p>O <app>Déjà Dup</app> pode falhar. Pode ser que ele trave ou dê erro quando quando você tentar fazer uma restauração. Quando você realmente precisar dos seus arquivos de volta, a última coisa que você quer é lidar com um erro. Considere preencher um <link href="https://launchpad.net/deja-dup/+filebug">relatório de erros</link>, entretanto, eis algumas abordagens para solucionar um <app>Déjà Dup</app> mal comportado e obter os seus dados de volta.</p>

<note><p>Isto passará a apresentar uma linguagem técnica. Se nada disto fizer sentido para você, não tenha medo de <link xref="support">pedir ajuda</link>.</p></note>

<steps>
  <item><p>Abra uma janela do <app>Terminal</app>, pressionando <keyseq><key>Ctrl</key><key>Alt</key><key>T</key></keyseq>.</p></item>
  <item><p>Crie o diretório em que você colocará os seus arquivos restaurados. Este guia utilizará <file its:translate="no">/tmp/restore</file>:</p>
<screen its:translate="no">mkdir -p /tmp/restore</screen></item>
</steps>

<section id="duplicity">
<title>Restaurando com o duplicity</title>

<p>Assumindo que neste momento o <app>Déjà Dup</app> não esteja funcionando para você, você terá de usar a ferramenta de linha de comando <app>duplicity</app>, que é empregada pelo <app>Déjà Dup</app> por trás dos bastidores para fazer cópias de segurança e restaurar os seus arquivos.</p>

<note style="advanced"><p>Se você quiser mais informações sobre o <app>duplicity</app> do que as foram apresentadas aqui, execute <cmd>man duplicity</cmd>.</p></note>

<p>A primeira coisa que tentaremos é uma restauração simples de todos os seus dados. Assumindo que os seus arquivos estão em um drive externo montado como <file its:translate="no">/media/backup</file>, e que você optou por criptografar a cópia de segurança, tente o seguinte:</p>
<screen its:translate="no">duplicity --gio file:///media/backup /tmp/restore</screen>

<p>Se você não criptografou a cópia de segurança, acrescente <cmd its:translate="no">--no-encryption</cmd> ao comando.</p>

<section id="locations">
<title>Outros locais de cópia de segurança</title>
<p>Se você fez uma cópia de segurança em um servidor remoto ou na nuvem, a sintaxe usada com o <app>duplicity</app> será diferente do exemplo acima, em que se utilizou um disco rígido externo. Veja abaixo como se conectar ao local de sua cópia de segurança escolhido.</p>
<note><p>Lembre-se de acrescentar <cmd its:translate="no">--no-encryption</cmd> a quaisquer comandos dos exemplos, caso a cópia de segurança não seja criptografada.</p></note>
<p>Se o <app>duplicity</app> aparenta ter problemas para se conectar ao seu servidor, tente baixar você próprio todos os arquivos do <app>duplicity</app> para uma pasta local e seguir o mais simples exemplo acima.</p>
<terms>
  <item>
    <title>Amazon S3</title>
    <p>Consulte sua chave ID de acesso ao Amazon S3 e a chave de acesso secreta e troque as instâncias de <var>ID</var> e <var>SECRET</var> no exemplo abaixo com os respectivos valores.</p>
    <p>Você pode ter especificado uma pasta na qual colocar os arquivos da cópia de segurança. No exemplo abaixo, troque qualquer instância de <var>FOLDER</var> com aquele caminho.</p>
    <screen its:translate="no">
export AWS_ACCESS_KEY_ID=<var its:translate="yes">ID</var>
export AWS_SECRET_ACCESS_KEY=<var its:translate="yes">SEGREDO</var>
duplicity s3+http://deja-dup-auto-<var its:translate="yes">ID_MINÚSCULA</var>/<var its:translate="yes">PASTA</var> /tmp/restore</screen>
  </item>
  <item>
    <title>Arquivos nas nuvens do Rackspace</title>
    <p>Consulte seu nome de usuário no Rackspace e a chave API, e troque as instâncias de <var>USERNAME</var> e <var>KEY</var> no exemplo abaixo com os respectivos valores.</p>
    <p>Você deve ter especificado um contêiner para colocar os arquivos da cópia de segurança. No exemplo abaixo, troque qualquer instância de <var>CONTAINER</var> com o nome especificado.</p>
    <screen its:translate="no">
export CLOUDFILES_USERNAME=<var its:translate="yes">NOME_DE_USUÁRIO</var>
export CLOUDFILES_APIKEY=<var its:translate="yes">CHAVE</var>
duplicity cf+http://<var its:translate="yes">CONTÊINER</var> /tmp/restore</screen>
  </item>
  <item>
    <title>FTP</title>
    <p>Procure o seu endereço de servidor, porta, nome de usuário e senha e substitua as instâncias de <var>SERVIDOR</var>, <var>PORTA</var>, <var>NOME_DE_USUÁRIO</var> e <var>SENHA</var> no exemplo abaixo com os seus respectivos valores.</p>
    <p>Você pode ter especificado uma pasta na qual colocar os arquivos da cópia de segurança. No exemplo abaixo, troque qualquer instância de <var>FOLDER</var> com aquele caminho.</p>
    <p>Se você optou por não entrar com um nome de usuário, utilize abaixo <var its:translate="no">anonymous</var> como o seu <var>NOME_DE_USUÁRIO</var>.</p>
    <screen its:translate="no">
gvfs-mount ftp://<var its:translate="yes">NOME_DE_USUÁRIO</var>@<var its:translate="yes">SERVIDOR</var>:<var its:translate="yes">PORTA</var>/<var its:translate="yes">PASTA</var>
duplicity --gio ftp://<var its:translate="yes">NOME_DE_USUÁRIO</var>@<var its:translate="yes">SERVIDOR</var>:<var its:translate="yes">PORTA</var>/<var its:translate="yes">PASTA</var> /tmp/restore</screen>
  </item>
  <item>
    <title>SSH</title>
    <p>Procure o seu endereço de servidor, porta, nome de usuário e senha e substitua as instâncias de <var>SERVIDOR</var>, <var>PORTA</var>, <var>NOME_DE_USUÁRIO</var> e <var>SENHA</var> no exemplo abaixo com os seus respectivos valores.</p>
    <p>Você pode ter especificado uma pasta na qual colocar os arquivos da cópia de segurança. No exemplo abaixo, troque qualquer instância de <var>FOLDER</var> com aquele caminho.</p>
    <screen its:translate="no">
gvfs-mount ssh://<var its:translate="yes">NOME_DE_USUÁRIO</var>@<var its:translate="yes">SERVIDOR</var>:<var its:translate="yes">PORTA</var>/<var its:translate="yes">PASTA</var>
duplicity --gio ssh://<var its:translate="yes">NOME_DE_USUÁRIO</var>@<var its:translate="yes">SERVIDOR</var>:<var its:translate="yes">PORTA</var>/<var its:translate="yes">PASTA</var> /tmp/restore</screen>
  </item>
  <item>
    <title>WebDAV</title>
    <p>Procure o seu endereço de servidor, porta, nome de usuário e senha e substitua as instâncias de <var>SERVIDOR</var>, <var>PORTA</var>, <var>NOME_DE_USUÁRIO</var> e <var>SENHA</var> no exemplo abaixo com os seus respectivos valores.</p>
    <p>Você pode ter especificado uma pasta na qual colocar os arquivos da cópia de segurança. No exemplo abaixo, troque qualquer instância de <var>FOLDER</var> com aquele caminho.</p>
    <p>Se você optou por usar uma conexão segura (HTTPS) ao fazer a cópia de segurança, utilize no exemplo abaixo <var its:translate="no">davs://</var> em vez de <var its:translate="no">dav://</var>.</p>
    <screen its:translate="no">
gvfs-mount dav://<var its:translate="yes">NOME_DE_USUÁRIO</var>@<var its:translate="yes">SERVIDOR</var>:<var its:translate="yes">PORTA</var>/<var its:translate="yes">PASTA</var>
duplicity --gio dav://<var its:translate="yes">NOME_DE_USUÁRIO</var>@<var its:translate="yes">SERVIDOR</var>:<var its:translate="yes">PORTA</var>/<var its:translate="yes">PASTA</var> /tmp/restore</screen>
  </item>
  <item>
    <title>Windows Share</title>
    <p>Consulte o seu endereço do servidor, nome de usuário e senha, e troque, no exemplo abaixo, as instâncias de <var>SERVIDOR</var>, <var>NOME_DE_USUÁRIO</var>, e <var>SENHA</var> pelos respectivos valores.</p>
    <p>Você pode ter especificado uma pasta na qual colocar os arquivos da cópia de segurança. No exemplo abaixo, troque qualquer instância de <var>FOLDER</var> com aquele caminho.</p>
    <p>Se você especificou um domínio para o seu servidor Windows, adicione-o no início do <var>NOME_DE_USUÁRIO</var> com um ponto e vírgula entre eles. Por exemplo, <var>domínio;nomedeusuário</var>.</p>
    <screen its:translate="no">
gvfs-mount smb://<var its:translate="yes">NOME_DE_USUÁRIO</var>@<var its:translate="yes">SERVIDOR</var>/<var its:translate="yes">PASTA</var>
duplicity --gio smb://<var its:translate="yes">NOME_DE_USUÁRIO</var>@<var its:translate="yes">SERVIDOR</var>/<var its:translate="yes">PASTA</var> /tmp/restore</screen>
  </item>
</terms>
</section>

</section>

<section id="by-hand">
<title>Restaurando manualmente</title>

<p>Se até mesmo o <app>duplicity</app> não estiver funcionando para você, há pouca esperança. O formato do arquivo da cópia de segurança é complexo e não pode ser facilmente manipulado. Entretanto, se você estiver desesperado, vale a pena tentar.</p>

<p>Se você usou um servidor remoto ou nas nuvens para armazenar as suas cópias de segurança, primeiramente baixe todos os arquivos do <app>duplicity</app> e coloque-os em uma pasta no seu computador. Em seguida, entre nessa pasta no seu terminal.</p>

<p>O <app>Duplicity</app> armazena os seus dados em pequenos pedaços chamados volumes. Alguns volumes pertencem às cópias de segurança ‘completas’ ou recentes, e outros às cópias de segurança ‘inc’ ou incrementais. Começando com uma cópia de segurança completa de volumes no volume 1, você precisará restaurar os arquivos volume por volume.</p>

<p>Se você criptografou a sua cópia de segurança, primeiramente você deve descriptografar o volume com <cmd its:translate="no">gpg</cmd>. Digamos que você tenha <file its:translate="no">duplicity-full.20110127T131352Z.vol1.difftar.gpg</file>:</p>
<screen its:translate="no">gpg --output duplicity-full.20110127T131352Z.vol1.difftar --decrypt duplicity-full.20110127T131352Z.vol1.difftar.gpg</screen>

<p>Ou fazer tudo de uma vez (certifique-se de que você tem bastante espaço!):</p>
<screen its:translate="no">gpg --multifile --decrypt duplicity-full.20110127T131352Z.*.difftar.gpg</screen>

<p>Agora você tem tanto <file its:translate="no">.difftar</file> ou um volume <file its:translate="no">.difftar.gz</file> (dependendo de você ter de descriptografá-lo ou não). Utilize <cmd its:translate="no">tar</cmd> em qualquer um no qual você tenha de extrair os arquivos de patch individuais:</p>
<screen its:translate="no">tar xf duplicity-full.20110127T131352Z.vol1.difftar</screen>

<p>Ou novamente, para fazer tudo de uma vez:</p>
<screen its:translate="no">for t in duplicity-full.20110127T131352Z.*.difftar; do tar xf $t; done</screen>

<p>Agora os arquivos de patch estarão nas pastas <file its:translate="no">multivolume_snapshot</file> e <file its:translate="no">snapshot</file>. Cada arquivo que se estendeu em múltiplos volumes estará em <file its:translate="no">multivolume_snapshot</file>. Digamos que você fez uma cópia de segurança de <file its:translate="no">/home/jane/essay.txt</file>:</p>
<screen its:translate="no">cd multivolume_snapshot/home/jane/essay.txt
cat * &gt; essay.txt</screen>

<p>Para recuperar dados de cópias de segurança incrementais, utilize <cmd its:translate="no">rdiff</cmd> para juntar os arquivos. Veja <cmd its:translate="no">man rdiff</cmd> para a utilização.</p>

</section>

</page>
