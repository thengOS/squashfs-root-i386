<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="backup-check" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="files#backup"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>davidk@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Verifique se sua cópia de segurança foi bem sucedida.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Ferreira</mal:name>
      <mal:email>rafael.f.f1@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>Verificando sua cópia de segurança</title>

  <p>Depois de você fazer a cópia de segurança de seus arquivos, você deveria se assegurar de que ela foi bem sucedida. Se ela não funcionou corretamente, você poderia perder dados importantes, já que alguns arquivos poderiam estar faltando nessa cópia.</p>

   <p>When you use <app>Files</app> to copy or move files, the computer checks
   to make sure that all of the data transferred correctly. However, if you are
   transferring data that is very important to you, you may want to perform
   additional checks to confirm that your data has been transferred
   properly.</p>

  <p>Você pode fazer uma verificação extra dando uma olhada nos arquivos e pastas copiadas no dispositivo de destino. Conferindo que seus arquivos e pastas que você transferiu estão de fato lá na cópia de segurança, você pode ter um confiança a mais de que o processo foi bem sucedido.</p>

  <note style="tip"><p>Se você faz cópias regularmente de grandes quantidades de dados, pode achar mais fácil usar um programa dedicado à cópias de segurança, como o <app>Déjà Dup</app>. Esse tipo de programa é mais poderoso e confiável do que só copiar e colar arquivos.</p></note>

</page>
