<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="problem" id="hardware-cardreader" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="media#photos"/>
    <link type="guide" xref="hardware#problems"/>

    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Solucione problemas de leitores de cartão de memória</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Ferreira</mal:name>
      <mal:email>rafael.f.f1@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

<title>Problemas com leitor de cartão de memória</title>

<p>Muitos computadores contêm leitores de SD, MMC, SM, MS, CF e outros cartões de armazenamento de mídia. Eles devem ser detectados automaticamente e <link xref="disk-partitions">montados</link>. Seguem aqui algumas etapas de soluções, caso não sejam:</p>

<steps>
<item>
<p>Certifique-se de que o cartão está inserido corretamente. Muitos cartões fica de cabeça para baixo quando estão inseridos corretamente. Também, certifique-se de que o cartão está inserido firmemente na abertura; alguns cartões, especialmente CF, requerem uma pequena quantidade de força para serem inseridos corretamente. (Tenha cuidado para não empurrar muito forte! Se você se deparar com algo sólido, não force.)</p>
</item>

<item>
  <p>Open <app>Files</app> from the
  <gui xref="shell-introduction#activities">Activities</gui> overview. Does the inserted
  card appear in the <gui>Devices</gui> list in the left sidebar? Sometimes the
  card appears in this list but is not mounted; click it once to mount. (If the
  sidebar is not visible, press <key>F9</key> or click the
  <gui><media its:translate="no" type="image" src="figures/go-down.png"><span its:translate="yes">View options</span></media></gui>
  button in the toolbar and choose <gui>Show Sidebar</gui>.)</p>
</item>

<item>
  <p>If your card does not show up in the sidebar, press
  <keyseq><key>Ctrl</key><key>L</key></keyseq>, then type
  <input>computer:///</input> and press <key>Enter</key>. If your card reader
  is correctly configured, the reader should come up as a drive when no card is
  present, and the card itself when the card has been mounted.</p>
</item>

<item>
<p>Se você vir o leitor de cartões, mas não o cartão, o problema pode ser com o próprio cartão. Tente um cartão diferente ou verifique o cartão em um leitor diferente, se possível.</p>
</item>
</steps>

<p>If no cards or drives are shown when browsing the <gui>Computer</gui>
location, it is possible that your card reader does not work with Linux due to
driver issues. If your card reader is internal (inside the computer instead of
sitting outside) this is more likely. The best solution is to directly connect
your device (camera, cell phone, etc.) to a USB port on the computer. USB
external card readers are also available, and are far better supported by
Linux.</p>

</page>
