<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="look-background" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="prefs-display"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email its:translate="no">gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>April Gonzales</name>
      <email its:translate="no">loonycookie@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email its:translate="no">nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Andre Klapper</name>
      <email its:translate="no">ak-47@gmx.net</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email its:translate="no">tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Defina uma imagem, uma cor ou um degradê como seu plano de fundo da área de trabalho ou da tela de bloqueio.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Ferreira</mal:name>
      <mal:email>rafael.f.f1@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>Alterando o plano de fundo da área de trabalho e tela de bloqueio</title>

  <p>Você pode alterar a imagem usada como plano de fundo ou configurá-la para ser uma cor sólida.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Background</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Plano de fundo</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Selecione o <gui>Plano de fundo</gui> ou <gui>Tela de bloqueio</gui>.</p>
    </item>
    <item>
      <p>Existem três opções exibidas ao topo:</p>
      <list>
        <item>
          <p>Selecione <gui>Papéis de parede</gui> para usar uma das várias imagens de fundo profissionais que vêm junto com o GNOME. Alguns papéis de parede se alteram o dia. Estes papéis de parede têm um ícone de relógio pequeno no canto inferior direito.</p>
        </item>
        <item>
          <p>Select <gui>Pictures</gui> to use one of your own photos from your
          <file>Pictures</file> folder. Most photo management applications
          store photos there. If you would like to use an image that is not in
          your Pictures folder, either use <app>Files</app> by right-clicking
          on the image file and selecting <gui>Set as Wallpaper</gui>, or
          <app>Image Viewer</app> by opening the image file, clicking the
          menu button in the titlebar and selecting <gui>Set as
          Wallpaper</gui>.</p>
        </item>
        <item>
          <p>Selecione <gui>Cores</gui> para usar simplesmente uma cor sólida.</p>
        </item>
      </list>
    </item>
    <item>
      <p>As configurações são aplicadas imediatamente.</p>
    </item>
    <item>
      <p><link xref="shell-workspaces-switch">Alterne para um espaço de trabalho vazio</link> para ver toda a sua área de trabalho.</p>
    </item>
  </steps>

</page>
