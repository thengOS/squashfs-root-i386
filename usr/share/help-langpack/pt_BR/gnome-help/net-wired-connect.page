<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="net-wired-connect" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="net-wired" group="#first"/>

    <revision pkgversion="3.4" date="2012-02-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email its:translate="no">gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Para configurar a maioria das conexões de rede com fio, tudo que você precisa fazer é conectar um cabo de rede.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Ferreira</mal:name>
      <mal:email>rafael.f.f1@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>Conectando a rede com fio (Ethernet)</title>

  <!-- TODO: create icon manually because it is one overlayed on top of another
       in real life. -->
  <p>To set up most wired network connections, all you need to do is plug in a
  network cable. The wired network icon
  (<media its:translate="no" type="image" src="figures/network-wired-symbolic.svg"><span its:translate="yes">settings</span></media>) is displayed
  on the top bar with three dots while the connection is being established. The
  dots disappear when you are connected.</p>

  <p>Se isso não acontecer, você deveria primeiro se certificar que sua rede cabeada está conectada. Uma ponta do cabe deveria estar conectada na porta Ethernet (de rede) em seu computador e a outra ponta deveria estar conectada em um switch, roteador, tomada de rede na parede ou algo similar (dependendo da configuração de rede que você tenha). Algumas vezes, uma luz por volta da porta Ethernet vai indicar que ela esta conectada e ativa.</p>

  <note>
    <p>Você não pode conectar um computador diretamente em outro com um cabo de rede (pelo menos não sem fazer algumas configurações adicionais). Para conectar dois computadores, você deveria conectar ambos em um hub de rede, roteador ou switch.</p>
  </note>

  <p>Se você ainda não estiver conectado, sua rede pode não oferecer suporte a configuração automática (DHCP). Nesse caso, você terá que <link xref="net-manual">configurá-la manualmente</link>.</p>

</page>
