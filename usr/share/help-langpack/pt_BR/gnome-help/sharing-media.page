<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="sharing-media" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.10" version="0.2" date="2013-11-02" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Share media on your local network using UPnP.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Ferreira</mal:name>
      <mal:email>rafael.f.f1@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>Compartilhando suas músicas, fotos e vídeos</title>

  <p>You can browse, search and play the media on your computer using a
  <sys>UPnP</sys> or <sys>DLNA</sys> enabled device such as a phone, TV or game
  console. Configure <gui>Media Sharing</gui> to allow these devices to access
  the folders containing your music, photos and videos.</p>

  <note style="info package">
    <p>You must have the <app>Rygel</app> package installed for <gui>Media
    Sharing</gui> to be visible.</p>

    <if:choose xmlns:if="http://projectmallard.org/if/1.0/">
      <if:when test="action:install">
        <p><link action="install:rygel" style="button">Instalar Rygel</link></p>
      </if:when>
    </if:choose>
  </note>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Sharing</gui>.</p>
    </item>
    <item>
      <p>Clique em <gui>Compartilhamento</gui> para abrir o painel.</p>
    </item>
    <item>
      <p>Se <gui>Compartilhar</gui> estiver em <gui>OFF</gui>, alterne-o para <gui>ON</gui>.</p>

      <note style="info"><p>Se o texto embaixo de <gui>Nome do computador</gui> permite que o edite, você pode <link xref="sharing-displayname">alterar</link> o nome que o seu computador exibe na rede.</p></note>
    </item>
    <item>
      <p>Selecione <gui>Compartilhamento de multimídia</gui>.</p>
    </item>
    <item>
      <p>Switch <gui>Media Sharing</gui> to <gui>ON</gui>.</p>
    </item>
    <item>
      <p>By default, <file>Music</file>, <file>Pictures</file> and 
      <file>Videos</file> are shared. To remove one of these, click the
      <gui>X</gui> next to the folder name.</p>
    </item>
    <item>
      <p>To add another folder, click <gui style="button">+</gui> to open the
      <gui>Choose a folder</gui> window. Navigate <em>into</em> the desired
      folder and click <gui style="button">Open</gui>.</p>
    </item>
    <item>
      <p>Click <gui style="button">X</gui>. You will now be able to browse
      or play media in the folders you selected using the external device.</p>
    </item>
  </steps>

  <section id="networks">
  <title>Networks</title>

  <p>The <gui>Networks</gui> section lists the networks to which you are
  currently connected. Use the <gui>ON | OFF</gui> switch next to each to
  choose where your media can be shared.</p>

  </section>

</page>
