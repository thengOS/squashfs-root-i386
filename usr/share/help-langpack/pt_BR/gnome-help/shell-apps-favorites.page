<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" version="1.0 if/1.0" id="shell-apps-favorites" xml:lang="pt-BR">

  <info>
    <link type="seealso" xref="shell-apps-open"/>
    <link type="guide" xref="shell-overview#desktop"/>

    <revision pkgversion="3.6.0" date="2012-10-14" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email>jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Adicione (ou remova) ícones de programas frequentemente usados ao dash.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Ferreira</mal:name>
      <mal:email>rafael.f.f1@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>Fixe seus aplicativos favoritos ao dash</title>

  <p>To add an application to the <link xref="shell-introduction#activities">dash</link>
  for easy access:</p>

  <steps>
    <item>
      <p if:test="!platform:gnome-classic">Open the
      <gui xref="shell-introduction#activities">Activities</gui> overview by
      clicking <gui>Activities</gui> at the top left of the screen</p>
      <p if:test="platform:gnome-classic">Click the
      <gui xref="shell-introduction#activities">Applications</gui> menu at the
      top left of the screen and choose the <gui>Activities Overview</gui> item
      from the menu.</p></item>
    <item>
      <p>Clique no botão de grade no dash e localize o aplicativo que você deseja adicionar.</p>
    </item>
    <item>
      <p>Clique com o botão direito no ícone do aplicativo e selecione <gui>Adicionar aos favoritos</gui>.</p>
      <p>Alternativamente, você pode clicar e arrastar o ícone ao dash.</p>
    </item>
  </steps>

  <p>Para remover um ícone de aplicativo do dash, clique com botão direito no ícone do aplicativo e selecione <gui>Remover dos favoritos</gui>.</p>

  <note style="tip" if:test="platform:gnome-classic"><p>Favorite applications
    also appear in the <gui>Favorites</gui> section of the
    <gui xref="shell-introduction#activities">Applications</gui> menu.</p>
  </note>

</page>
