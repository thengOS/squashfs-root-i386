<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="ui" version="1.0 if/1.0" id="shell-workspaces" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces" group="#first"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.10.3" date="2014-01-26" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Espaços de trabalho são uma maneira de agrupar janelas na sua área de trabalho.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rodolfo Ribeiro Gomes</mal:name>
      <mal:email>rodolforg@gmail.com</mal:email>
      <mal:years>2011, 2012.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Ferreira</mal:name>
      <mal:email>rafael.f.f1@gmail.com</mal:email>
      <mal:years>2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>João Santana</mal:name>
      <mal:email>joaosantana@outlook.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

<title>O que é um espaço de trabalho e como ele vai me ajudar?</title>

    <media type="image" src="figures/shell-workspaces.png" width="162" height="434" style="floatend floatright">
        <p>Seletor de espaço de trabalho</p>
    </media>

  <p if:test="!platform:gnome-classic">Espaços de trabalho referem-se ao agrupamento de janelas na sua área de trabalho. Você pode criar múltiplos espaços de trabalho, que agem como áreas de trabalho virtuais. Eles foram feitos para reduzir o tumulto e tornar a área de trabalho mais fácil de se navegar.</p>

  <p if:test="platform:gnome-classic">Espaços de trabalho referem-se ao agrupamento de janelas na sua área de trabalho. Você pode usar múltiplos espaços de trabalho, que agem como áreas de trabalho virtuais. Eles foram feitos para reduzir o tumulto e tornar a área de trabalho mais fácil de se navegar.</p>

  <p>Os espaços de trabalho podem ser usados para organizar seu trabalho. Por exemplo, você poderia ter todas as suas janelas de comunicação, como programas de e-mail e de bate-papo, em um espaço, enquanto que o trabalho que você está fazendo em um espaço diferente. Seu gerenciador de música poderia estar em um terceiro espaço de trabalho.</p>

<p>Usando espaços de trabalho:</p>

<list>
  <item>
    <p if:test="!platform:gnome-classic">In the
    <gui xref="shell-introduction#activities">Activities</gui> overview, move your cursor
    to the right-most side of the screen.</p>
    <p if:test="platform:gnome-classic">Pressione a tecla <key xref="keyboard-key-super">Super</key> para abrir o panorama de <gui>Atividades</gui> e, então, mova seu cursor para o lado mais da direita da tela.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">A vertical panel will appear showing
    workspaces in use, plus an empty workspace. This is the
    workspace selector.</p>
    <p if:test="platform:gnome-classic">A vertical panel will appear showing
    available workspaces. This is the workspace selector.</p>
  </item>
  <item if:test="!platform:gnome-classic">
    <p>To add a workspace, drag and drop a window from an existing workspace
    onto the empty workspace in the workspace selector. This workspace now
    contains the window you have dropped,
    and a new empty workspace will appear below it.</p>
  </item>
  <item if:test="!platform:gnome-classic">
    <p>Para remover um espaço de trabalho, basta fechar todas as suas janelas ou movê-las para outros espaços. de trabalho.</p>
  </item>
</list>

<p if:test="!platform:gnome-classic">Sempre há ao menos um espaço de trabalho.</p>

</page>
