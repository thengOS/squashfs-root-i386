<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="task" id="pref-menubar" xml:lang="pt-BR">

  <info>
    <revision version="0.1" date="2013-02-21" status="candidate"/>
    <link type="guide" xref="index#preferences"/>
    <link type="guide" xref="pref"/>
    <link type="seealso" xref="pref-keyboard-access"/>
    <link type="seealso" xref="app-fullscreen"/>

    <credit type="author copyright">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>
    <credit type="copyright editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Hide and restore the menubar.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luciana Bastos de Freitas Menezes</mal:name>
      <mal:email>lubfm@gnome.org</mal:email>
      <mal:years>2007.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Pedro Vaz de Mello de Medeiros</mal:name>
      <mal:email>pedrovmm@gmail.com</mal:email>
      <mal:years>2007.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leonardo Ferreira Fontenelle</mal:name>
      <mal:email>leonardof@gnome.org</mal:email>
      <mal:years>2008.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andrius da Costa Ribas</mal:name>
      <mal:email>andriusmao@gmail.com</mal:email>
      <mal:years>2008.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2009, 2013.</mal:years>
    </mal:credit>
  </info>

  <title>Menubar visibility</title>

  <p>Você pode habilitar ou desativar a barra de menu se desejar. Isto pode ser útil se você tiver um espaço limitado de tela. Para esconder a barra de menu:</p>

  <steps>
    <item>
      <p>Select <gui style="menu">View</gui> and deselect
      <gui style="menuitem">Show Menubar</gui>.</p>
    </item>
  </steps>

  <p>To restore the menubar:</p>

  <steps>
    <item>
      <p>Right click in <app>Terminal</app> and select
      <gui style="menuitem">Show Menubar</gui>.</p>
    </item>
  </steps>

  <p>Para habilitar a barra de menus por padrão em todas as janelas do <app>Terminal</app> que você abrir:</p>

  <steps>
    <item>
      <p>Select <guiseq><gui style="menu">Edit</gui> <gui style="menuitem">
      Preferences</gui> <gui style="tab">General</gui></guiseq>.</p>
    </item>
    <item>
      <p>Selecione <gui>Mostrar barra de menu, por padrão, em novos terminais</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>You can <link xref="adv-keyboard-shortcuts">set a keyboard
    shortcut</link> to show and hide the menubar.</p>
  </note>

</page>
