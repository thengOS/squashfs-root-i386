<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY appversion "2.26">
<!ENTITY app "gnome-user-share">
]>
<article id="index" lang="pt-BR">
  <articleinfo>
	 <title>Manual de compartilhamento de arquivos pessoais</title>
	 <copyright><year>2009</year> <holder>Red Hat, Inc.</holder></copyright>

	 <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="legal.xml"/>
	 <authorgroup>
           <author role="maintainer"><firstname>Matthias</firstname> <surname>Clasen</surname></author>
	 </authorgroup>

<!-- According to GNU FDL, revision history is mandatory if you are -->
<!-- modifying/reusing someone else's document.  If not, you can omit it. -->
	 <revhistory>
		<revision><revnumber>1.0</revnumber> <date>2009</date> <revdescription>
			 <para role="author">Matthias Clasen <email>mclasen@redhat.com</email></para>
		  </revdescription></revision>
	 </revhistory>
	 <releaseinfo>Este manual descreve a versão 2.26 do gnome-user-share.</releaseinfo>
	 <legalnotice>
		<title>Feedback</title>
		<para>Para relatar um erro ou fazer uma sugestão a respeito do aplicativo do gnome-user-share ou deste manual, siga as instruções na <ulink url="help:user-guide/feedback-bugs" type="help">Página de feedback do GNOME</ulink>.</para>
	 </legalnotice>
    <abstract role="description">
      <para>gnome-user-share é um serviço de sessão que possibilita o compartilhamento de arquivos entre diversos computadores.</para>
    </abstract>
  
    <othercredit class="translator">
      <personname>
        <firstname>Marcelo Rodrigues Pires Filho</firstname>
      </personname>
      <email>marcelopires@mmsantos.com.br</email>
    </othercredit>
    <copyright>
      
        <year>2010</year>
      
      <holder>Marcelo Rodrigues Pires Filho</holder>
    </copyright>
  
    <othercredit class="translator">
      <personname>
        <firstname>Rafael Ferreira</firstname>
      </personname>
      <email>rafael.f.f1@gmail.com</email>
    </othercredit>
    <copyright>
      
        <year>2013</year>
      
      <holder>Rafael Ferreira</holder>
    </copyright>
  </articleinfo>
  <indexterm><primary>gnome-user-share</primary></indexterm>
  <indexterm><primary>compartilhamento de arquivos</primary></indexterm>
  <indexterm><primary>compartilhamento</primary></indexterm>

  <sect1 id="gnome-user-share-intro">
	 <title>Introdução</title>
	 <para>gnome-user-share é um serviço de sessão que exporta conteúdo da pasta <filename>Pública</filename> para seu diretório home, para que este conteúdo possa ser facilmente acessado de outros computadores na mesma rede local. Nos outros computadores, a pasta compartilhada aparecera com um nome parecido com 'arquivos compartilhados do <replaceable>usuário</replaceable>' na janela de rede do <application>nautilus</application>, onde <replaceable>usuário</replaceable> será substituído pelo nome do usuário.</para>
	 <para>O gnome-user-share usa um servidor WebDAV para compartilhar a pasta <filename>Public</filename>, e anuncia o compartilhamento na rede local usando mDNS.</para>
         <para>Adicionalmente, o gnome-user-share pode tornar arquivos compartilhados disponíveis via ObexFTP por Bluetooth, e receber arquivos que são enviados ao seu computador por Bluetooth via ObexPush.</para>
  </sect1>

  <sect1 id="gnome-user-share-getting-started">
    <title>Iniciando</title>

    <sect2 id="gnome-user-share-start">
       <title>Iniciando o gnome-user-share</title>

       <para>O serviço gnome-user-share é normalmente iniciado por <application>gnome-session</application> quando você entra no sistema. você pode mudar isto abrindo <menuchoice><guimenu>Preferências</guimenu><guimenu>Aplicativos de sessão</guimenu></menuchoice> no menu <guimenu>Sistema</guimenu>, e modificando a opção 'Compartilhamento de arquivos' na lista de programas iniciais.</para>

       <para>Para configurar vários aspectos do compartilhamento de arquivos, use Preferência de compartilhamento de arquivos, que pode ser encontrado em <menuchoice><guimenu>Preferências</guimenu><guimenu>Compartilhamento de arquivos pessoais</guimenu></menuchoice> no menu                                           <guimenu>Sistema</guimenu>.</para>
       <figure id="file-sharing-preferences">
         <title>Preferências de compartilhamento de arquivos</title>
         <screenshot>
           <mediaobject>
             <imageobject>
               <imagedata fileref="figures/file-sharing-preferences.png" format="PNG"/>
             </imageobject>
           </mediaobject>
         </screenshot>
       </figure>
 
     </sect2>
     <sect2 id="gnome-user-share-enabling-sharing">
       <title>Habilitando compartilhamento de arquivos na rede</title>
       <para>Abra as Preferências de compartilhamento de arquivos <menuchoice><guimenu>Preferências</guimenu><guimenu>Compatilhamento de Arquivos pessoais</guimenu></menuchoice> no menu <guimenu>Sistema</guimenu>.</para>
       <figure id="sharing-over-the-network">
         <title>Compartilhar arquivos na rede</title>
         <screenshot>
           <mediaobject>
             <imageobject>
               <imagedata fileref="figures/sharing-over-the-network.png" format="PNG"/>
             </imageobject>
           </mediaobject>
         </screenshot>
       </figure>
       <para>Para habilitar o compartilhamento de arquivos por WebDAV, use a opção <guilabel>Compartilhar arquivos públicos na rede</guilabel>. Quando o compartilhamento de arquivos é habilitado, os controles de proteção de senha permitem a definição de uma senha que necessite ser especificada antes que um usuário em outro computador tenha o acesso permitido aos arquivos compartilhados. <placeholder-1/><placeholder-2/><placeholder-3/></para>  
     </sect2>

     <sect2 id="gnome-user-share-enabling-bluetooth">
       <title>Habilitando o compartilhamento de arquivos por Bluetooth</title>
       <para>Abra as Preferências de compartilhamento de arquivos <menuchoice><guimenu>Preferências</guimenu><guimenu>Compatilhamento de Arquivos pessoais</guimenu></menuchoice> no menu <guimenu>Sistema</guimenu>.</para>
       <figure id="sharing-over-bluetooth">
         <title>Compartilhe arquivos por Bluetooth</title>
         <screenshot>
           <mediaobject>
             <imageobject>
               <imagedata fileref="figures/sharing-over-bluetooth.png" format="PNG"/>
             </imageobject>
           </mediaobject>
         </screenshot>
       </figure>
       <para>Para habilitar o compartilhamento de arquivos por Bluetooth, use marque a opção <guilabel>Compartilhar arquivos públicos pelo Bluetooth</guilabel>. Para permitir que dispositivos Bluetooth remotos deletem seus arquivos compartilhados use a opção <guilabel>Aceitar dispositivos remotos apagar arquivos</guilabel>. Para permitir que dispositivos Bluetooth remotos acessem seus arquivos compartilhados até mesmo quando eles não estão emparelhados com seu computador, marque a opção <guilabel>Exigir dispositivos remotos emparelhar com este computador</guilabel>.  <note><para>Quando você permite serviços remotos externos a acessar seus arquivos compartilhados, qualquer um com um celular com Bluetooth no alcance de seu computador pode acessar e talvez até modificar seus arquivos compartilhados.</para></note></para>
     </sect2>

     <sect2 id="gnome-user-share-bluetooth-receiving">
       <title>Recebendo arquivos por Bluetooth</title>
       <para>Abra as Preferências de compartilhamento de arquivos <menuchoice><guimenu>Preferências</guimenu><guimenu>Compatilhamento de Arquivos pessoais</guimenu></menuchoice> no menu <guimenu>Sistema</guimenu>.</para>
       <figure id="receiving-over-bluetooth">
         <title>Receba arquivos por Bluetooth</title>
         <screenshot>
           <mediaobject>
             <imageobject>
               <imagedata fileref="figures/receiving-over-bluetooth.png" format="PNG"/>
             </imageobject>
           </mediaobject>
         </screenshot>
       </figure>
       <para>Para permitir que dispositivos Bluetooth remotos enviem arquivos para seu computador, marque a opção <guilabel>Receber arquivos na pasta Downloads pelo Bluetooth</guilabel>. Os arquivos recebidos serão armazenados na pasta <filename>Downloads</filename> na sua pasta pessoal. Quando o recebimento de arquivos estiver habilitado, a seleção <guilabel>Aceitar arquivos</guilabel> permite que você determine quais dispositivos remotos são permitidos para enviar arquivos. <itemizedlist>
            <listitem>
              <para>Selecione <guilabel>Sempre</guilabel> para permitir qualquer dispositivo remoto enviar arquivos.</para>
            </listitem>
            <listitem>
              <para>Selecione <guilabel>Somente para dispositivos emparelhados</guilabel> para aceitar arquivos apenas de dispositivos emparelhados. <note><para>Dispositivos interligados são aqueles que estão conectados ao seu computador, e tiveram que entrar com um código PIN para se conectar com ele ou dele.</para></note></para>
            </listitem>
            <listitem>
              <para>Selecione <guilabel>Somente para dispositivos emparelhados e confiáveis</guilabel> para aceitar arquivos apenas de dispositivos aparelhados. <note><para>Aparelhos podem ser marcados como confiáveis na sessão <application>propriedades de bluetooth</application> dentro de <guilabel>Dispositivos conhecidos</guilabel>.</para></note></para>
            </listitem>
          </itemizedlist></para>
        <para>Use a opção <guilabel>Notificar sobre arquivos recebidos</guilabel> para selecionar se você quer ser notificado quando um arquivo é recebido por Bluetooth.</para>
     </sect2>
  </sect1>

</article>
