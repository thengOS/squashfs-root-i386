<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">
<!--
     The GNU Public License 2 in DocBook
     Markup by Eric Baudais <baudais@okstate.edu>
     Maintained by the GNOME Documentation Project
     http://developer.gnome.org/projects/gdp
     Version: 1.0.1
     Last Modified: Dec 16, 2000
-->
<article id="index" lang="pt-BR">
  <articleinfo>
    <title>Licença Pública Geral GNU</title>    
    <copyright><year>2000</year> <holder>Free Software Foundation, Inc.</holder></copyright>

      <author><surname>Free Software Foundation</surname></author>

      <publisher role="maintainer">
        <publishername>Projeto de Documentação do GNOME</publishername>
      </publisher>

      <revhistory>
        <revision><revnumber>2</revnumber> <date>1991-06</date></revision>
      </revhistory>

    <legalnotice id="legalnotice">
      <para><address>Free Software Foundation, Inc. 
	  <street>51 Franklin Street, Fifth Floor</street>, 
	  <city>Boston</city>, 
	  <state>MA</state> <postcode>02110-1301</postcode>
	  <country>USA</country>
	</address>.</para>

      <para>A qualquer pessoa é permitido copiar e distribuir cópias desse documento de licença, desde que sem qualquer alteração.</para>
    </legalnotice>

    <releaseinfo>Versão 2, junho de 1991</releaseinfo>

    <abstract role="description"><para>As licenças de muitos softwares são desenvolvidas para restringir sua liberdade de compartilhá-lo e mudá-lo. Contrária a isso, a Licença Pública Geral GNU pretende garantir sua liberdade de compartilhar e alterar software livres -- garantindo que o software será livre e gratuito para os seus usuários.</para></abstract>
  
    <othercredit class="translator">
      <personname>
        <firstname>Rafael Ferreira</firstname>
      </personname>
      <email>rafael.f.f1@gmail.com</email>
    </othercredit>
    <copyright>
      
        <year>2012</year>
      
        <year>2013</year>
      
      <holder>Rafael Ferreira</holder>
    </copyright>
  </articleinfo>

  <sect1 id="preamble" label="none">
    <title>Preâmbulo</title>
    
    <para>As licenças de muitos software são desenvolvidas para restringir sua liberdade de compartilhá-lo e mudá-lo. Contrária a isso, a Licença Pública Geral GNU pretende garantir sua liberdade de compartilhar e alterar softwares livres -- garantindo que o software será livre e gratuito para os seus usuários. Esta Licença Pública Geral aplica-se à maioria dos softwares da Free Software Foundation e a qualquer outro programa cujo autor decida aplicá-la. (Alguns outros software da FSF são cobertos pela Licença Pública Geral de Bibliotecas, no entanto.) Você pode aplicá-la também aos seus programas.</para>

    <para>Quando nos referimos a software livre, estamos nos referindo a liberdade, e não a preço. As nossas Licenças Públicas Gerais foram desenvolvidas para garantir que você tenha a liberdade de distribuir cópias de softwares livres (e cobrar por este serviço, se quiser); que você receba o código-fonte ou tenha acesso a ele, se quiser; que você possa alterar o software ou utilizar partes dele em novos programas livres; e que você saiba que pode fazer tudo isso.</para>

    <para>Para proteger seus direitos, precisamos fazer restrições que impeçam a qualquer um negar estes direitos ou solicitar que você abdique deles. Estas restrições traduzem-se em certas responsabilidades para você, se você for distribuir cópias do software ou modificá-lo.</para>

    <para>Por exemplo, se você distribuir cópias de um programa, gratuitamente ou por alguma quantia, você tem que fornecer aos receptores todos os direitos que você possui. Você tem que garantir que eles também recebam ou possam obter o código-fonte. E você tem que mostrar-lhes estes termos para que eles possam conhecer seus direitos.</para>

    <para>Nós protegemos seus direitos em dois passos: <orderedlist numeration="arabic">
	<listitem>
	  <para>com direitos autorais do software; e</para>
	</listitem>
	<listitem>
	  <para>com a oferta desta licença, que lhe dá permissão legal para copiar, distribuir e/ou modificar o software.</para>
	</listitem>
      </orderedlist></para>

    <para>Além disso, tanto para a proteção de cada autor quanto a nossa, gostaríamos de nos certificar de que todos entendam que não há qualquer garantia nestes softwares livres. Se o software é modificado por alguém mais e passado adiante, queremos que seus recebedores saibam que o que eles obtiveram não é original, de forma que qualquer problema introduzido por terceiros não interfira na reputação do autor original.</para>

    <para>Finalmente, qualquer programa livre é ameaçado constantemente por patentes de software. Queremos evitar o perigo de que redistribuidores de software livre obtenham individualmente licenças de patentes, o que teria o efeito de tornar o programa proprietário. Para prevenir isso, deixamos claro que qualquer patente tem que ser licenciada para uso livre e gratuito por qualquer pessoa, ou então que não seja licenciada.</para>

    <para>Os precisos termos e condições para cópia, distribuição e modificação se encontram abaixo.</para>

  </sect1>

  <sect1 id="terms" label="none">
    <title>TERMOS E CONDIÇÕES PARA CÓPIA, DISTRIBUIÇÃO E MODIFICAÇÃO</title>

    <sect2 id="sect0" label="0">
      <title>Seção 0</title>
      <para>Esta Licença se aplica a qualquer programa ou outro trabalho que contenha um aviso colocado pelo detentor dos direitos autorais informando que aquele pode ser distribuído sob as condições desta Licença Pública Geral. O <quote>Programa</quote> abaixo refere-se a qualquer programa ou trabalho, e <quote>trabalho baseado no Programa</quote> significa tanto o Programa em si como quaisquer trabalhos derivados, de acordo com a lei de direitos autorais: isto quer dizer um trabalho que contenha o Programa ou parte dele, tanto originalmente quanto com modificações, e/ou tradução para outro idioma. (Doravante tradução está incluído sem limites no termo <quote>modificação</quote>.) Cada licenciado é mencionado como <quote>você</quote>.</para>

      <para>Atividades além de cópia, distribuição e modificação não estão cobertas por esta Licença; elas estão fora de seu escopo.  O ato de executar o Programa não é restringido, e o resultado do Programa é coberto apenas se seu conteúdo contenha trabalhos baseados no Programa (independentemente de terem sido gerados pela execução do Programa). Se isso é verdadeiro depende do que o Programa faz.</para>
    </sect2>

    <sect2 id="sect1" label="1">
      <title>Seção 1</title>
      <para>Você pode copiar e distribuir cópias fiéis do código-fonte do Programa da mesma forma que você o recebeu, usando qualquer meio, desde que você conspícua e apropriadamente publique em cada cópia um aviso de direitos autorais e uma declaração de inexistência de garantias de forma apropriada; mantenha intactos todos os avisos que se referem a esta Licença e à ausência total de garantias; e forneça a outros recebedores do Programa uma cópia desta Licença, junto com o Programa.</para>
      
      <para>Você pode cobrar pelo ato físico de transferir uma cópia e pode, opcionalmente, oferecer garantia em troca de pagamento.</para>
    </sect2>

    <sect2 id="sect2" label="2">
      <title>Seção 2</title>
      <para>Você pode modificar sua cópia ou cópias do Programa, ou qualquer parte dele, assim gerando um trabalho baseado no Programa, e copiar e distribuir essas modificações ou trabalhos sob os termos da <link linkend="sect1">Seção 1</link> acima, desde que você também se atenda a todas estas condições: <orderedlist numeration="loweralpha">
	  <listitem>
	    <para>Você tem que fazer com que os arquivos modificados levem avisos proeminentes afirmando que você alterou os arquivos, incluindo a data de qualquer alteração.</para>
	  </listitem>
	  <listitem>
	    <para>Você tem que fazer com que quaisquer trabalhos que você distribua ou publique, e que integralmente ou em partes contenham ou sejam derivados do Programa ou de suas partes, sejam licenciados, integralmente e sem custo algum para quaisquer terceiros, sob os termos desta Licença.</para>
	  </listitem>
	  <listitem>
	    <para>Se o programa modificado normalmente lê comandos interativamente quando executados, você tem que fazer com que, quando iniciado tal uso interativo da forma mais simples, seja impresso ou mostrado um anúncio de direitos autorais um e anúncio de que não há qualquer garantia (ou então, que você fornece a garantia) e que os usuários podem redistribuir o programa sob estas condições, ainda informando os usuários como consultar uma cópia desta Licença. <note>
		<title>Exceção:</title>
		<para>se o Programa em si é interativo mas normalmente não imprime estes tipos de anúncios, seu trabalho baseado no Programa não precisa imprimir um anúncio.</para>
	      </note></para>
	  </listitem>
	</orderedlist></para>

      <para>Estas exigências aplicam-se ao trabalho modificado como um todo. Se seções identificáveis de tal trabalho não forem derivadas do Programa, e que podem ser razoavelmente consideradas trabalhos independentes e separados por si só, então esta Licença, e seus termos, não se aplicam a estas seções quando você distribuí-las como trabalhos em separado. Mas quando você distribuir as mesmas seções como parte de um todo, o qual é baseado no Programa, a distribuição como um todo deve que se enquadrar nos termos desta Licença, cujas permissões para outros licenciados se estendem ao todo, portanto para cada e toda parte, independente de quem a escreveu.</para>

      <para>Desta forma, esta seção não tem a intenção de reclamar direitos ou contestar seus direitos sobre o trabalho escrito inteiramente por você; ao invés disso, a intenção é a de exercitar o direito de controlar a distribuição de trabalhos, derivados ou coletivos, baseados no Programa.</para>

      <para>Adicionalmente, a mera agregação ao Programa de outro trabalho não baseado no Programa (ou de trabalho baseado no Programa) em um volume de armazenamento ou meio de distribuição não faz o outro trabalho parte do escopo desta Licença.</para>
    </sect2>

    <sect2 id="sect3" label="3">
      <title>Seção 3</title>

      <para>Você pode copiar e distribuir o Programa (ou um trabalho baseado nele, conforme descrito na <link linkend="sect2">Seção 2</link>) em código-objeto ou em forma executável sob os termos das <link linkend="sect1">Seções 1</link> e <link linkend="sect2">2</link> acima, desde que você também faça um dos seguintes itens: <orderedlist numeration="loweralpha">
	  <listitem>
	    <para>O acompanhe com o código-fonte completo correspondente e em forma acessível por máquinas, que deve ser distribuído sob os termos das <link linkend="sect1">Seções 1</link> e <link linkend="sect2">2</link> acima e em meio normalmente utilizado para o intercâmbio de software; ou,</para>
	  </listitem>
	  <listitem>
	    <para>O acompanhe com uma oferta expressa, válida por pelo menos três anos, de fornecer a qualquer terceiros, com um custo não superior ao custo de distribuição física do material, uma cópia do código-fonte completo e em forma acessível por máquinas, que deve ser distribuído sob os termos das Seções 1 e 2 acima e em meio normalmente utilizado para o intercâmbio de software; ou,</para>
	  </listitem>
	  <listitem>
	    <para>O acompanhe com a informação que você recebeu em relação à oferta de distribuição do código-fonte correspondente. (Esta alternativa é permitida somente em distribuição não comercial, e apenas se você recebeu o programa em forma de código-objeto ou executável, com oferta, de acordo com a Subseção b acima.)</para>
	  </listitem>
	</orderedlist></para>

      <para>O código-fonte de um trabalho corresponde à forma de trabalho preferida para se fazer modificações. Para um trabalho em forma executável, o código-fonte completo significa todo o código-fonte de todos os módulos que ele contém, mais quaisquer arquivos de definição de "interface", mais os "scripts" utilizados para se controlar a compilação e a instalação do executável. Contudo, como exceção especial, o código-fonte distribuído não precisa incluir qualquer componente normalmente distribuído (tanto em forma original quanto binária) com os maiores componentes (o compilador, o "kernel" etc.) do sistema operacional sob o qual o executável funciona, a menos que o componente em si acompanhe o executável.</para>
      
      <para>Se a distribuição do executável ou código-objeto é feita através da oferta de acesso a cópias de algum lugar, então ofertar o acesso equivalente a cópia, do mesmo lugar, do código-fonte equivale à distribuição do código-fonte, mesmo que terceiros não sejam compelidos a copiar o código-fonte com o código-objeto.</para>
    </sect2>

    <sect2 id="sect4" label="4">
      <title>Seção 4</title>
      
      <para>Você não pode copiar, modificar, sub-licenciar ou distribuir o Programa, exceto de acordo com as condições expressas nesta Licença. Qualquer outra tentativa de cópia, modificação, sub-licenciamento ou distribuição do Programa é vetado, e cancelará automaticamente os direitos que lhe foram fornecidos por esta Licença. No entanto, terceiros que receberam cópias ou direitos, fornecidos por você sob os termos desta Licença, não terão suas licenças canceladas, desde que permaneçam em total concordância com ela.</para>
    </sect2>

    <sect2 id="sect5" label="5">
      <title>Seção 5</title>

      <para>Você não é obrigado a aceitar esta Licença, já que não a assinou. No entanto, nada mais o dará permissão para modificar ou distribuir o Programa ou trabalhos derivados deste. Estas ações são proibidas por lei, caso você não aceite esta Licença. Desta forma, ao modificar ou distribuir o Programa (ou qualquer trabalho derivado do Programa), você estará indicando sua total aceitação desta Licença para fazê-los, e todos os seus termos e condições para copiar, distribuir ou modificar o Programa, ou trabalhos baseados nele.</para>
    </sect2>

    <sect2 id="sect6" label="6">
      <title>Seção 6</title>

      <para>Cada vez que você redistribuir o Programa (ou qualquer trabalho baseado nele), os recebedores adquirirão automaticamente do licenciador original uma licença para copiar, distribuir ou modificar o Programa, sujeitos a estes termos e condições. Você não poderá impor aos recebedores qualquer outra restrição ao exercício dos direitos então adquiridos. Você não é responsável em garantir a concordância de terceiros a esta Licença.</para>
    </sect2>

    <sect2 id="sect7" label="7">
      <title>Seção 7</title>

      <para>Se, em consequência de decisões judiciais ou alegações de infringimento de patentes ou quaisquer outras razões (não limitadas a assuntos relacionados a patentes), condições forem impostas a você (por ordem judicial, acordos ou outras formas) e que contradigam as condições desta Licença, elas não o livram das condições desta Licença. Se você não puder distribuir de forma a satisfazer simultaneamente suas obrigações para com esta Licença e para com as outras obrigações pertinentes, então como consequência você não poderá distribuir o Programa. Por exemplo, se uma licença de patente não permitir a redistribuição, livre de "royalties", do Programa, por todos aqueles que receberem cópias direta ou indiretamente de você, então a única forma de você satisfazer a ela e a esta Licença seria a de desistir completamente de distribuir o Programa.</para>

      <para>Se qualquer parte desta seção for considerada inválida ou não aplicável em qualquer circunstância particular, o restante da seção se aplica, e a seção como um todo se aplica em outras circunstâncias.</para>

      <para>O propósito desta seção não é o de induzi-lo a infringir quaisquer patentes ou reivindicação de direitos de propriedade outros, ou a contestar a validade de quaisquer dessas reivindicações; esta seção tem como único propósito proteger a integridade dos sistemas de distribuição de software livres, o que é implementado pela prática de licenças públicas. Várias pessoas têm contribuído generosamente e em grande escala para os software distribuídos usando este sistema, na certeza de que sua aplicação é feita de forma consistente; fica a critério do autor/doador decidir se ele ou ela está disposto a distribuir software utilizando outro sistema, e um licenciado não pode impor qualquer escolha.</para>

      <para>Esta seção destina-se a tornar bastante claro o que se acredita ser consequência do restante desta Licença.</para>
    </sect2>

    <sect2 id="sect8" label="8">
      <title>Seção 8</title>

      <para>Se a distribuição e/ou uso do Programa são restringidos em certos países por patentes ou direitos autorais, o detentor dos direitos autorais original, e que colocou o Programa sob esta Licença, pode incluir uma limitação geográfica de distribuição, excluindo aqueles países de forma a tornar a distribuição permitida apenas naqueles ou entre aqueles países então não excluídos. Nestes casos, esta Licença incorpora a limitação como se a mesma constasse escrita nesta Licença.</para>
    </sect2>

    <sect2 id="sect9" label="9">
      <title>Seção 9</title>
      
      <para>A Free Software Foundation pode publicar versões revisadas e/ou novas da Licença Pública Geral de tempos em tempos. Estas novas versões serão similares em espírito à versão atual, mas podem diferir em detalhes que resolvem novos problemas ou situações.</para>

      <para>A cada versão é dada um número distinto. Se o Programa especifica um número de versão específico desta Licença que se aplica a ele e a <quote>qualquer nova versão</quote>, você tem a opção de aceitar os termos e condições daquela versão ou de qualquer outra versão publicada pela Free Software Foundation. Se o Programa não especifica um número de versão desta Licença, você pode escolher qualquer versão já publicada pela Free Software Foundation.</para>
    </sect2>

    <sect2 id="sect10" label="10">
      <title>Seção 10</title>

      <para>Se você pretende incorporar partes do Programa em outros programas livres, cujas condições de distribuição são diferentes, escreva ao autor e solicite permissão. Para o software que a Free Software Foundation detém direitos autorais, escreva à Free Software Foundation; às vezes nós permitimos exceções a este caso. Nossa decisão será guiada pelos dois objetivos de preservar a condição de liberdade de todas as derivações do nosso software livre, e de promover o compartilhamento e reutilização de software em aspectos gerais.</para>
    </sect2>

    <sect2 id="sect11" label="11">
      <title>AUSÊNCIA DE GARANTIAS</title>
      <subtitle>Seção 11</subtitle>

      <para>UMA VEZ QUE O PROGRAMA É LICENCIADO SEM ÔNUS, NÃO HÁ QUALQUER GARANTIA PARA O PROGRAMA, NA EXTENSÃO PERMITIDA PELAS LEIS APLICÁVEIS. EXCETO QUANDO EXPRESSADO DE FORMA EXPRESSA, OS DETENTORES DOS DIREITOS AUTORAIS E/OU TERCEIROS DISPONIBILIZAM O PROGRAMA <quote>NO ESTADO</quote>, SEM QUALQUER TIPO DE GARANTIAS, EXPRESSAS OU IMPLÍCITAS, INCLUINDO, MAS NÃO LIMITADO A, AS GARANTIAS IMPLÍCITAS DE COMERCIALIZAÇÃO E AS DE ADEQUAÇÃO A QUALQUER PROPÓSITO. TODO O RISCO RELACIONADO À QUALIDADE E DESEMPENHO DO PROGRAMA É SEU. SE O PROGRAMA SE MOSTRAR DEFEITUOSO, VOCÊ ASSUME OS CUSTOS DE TODAS AS MANUTENÇÕES, REPAROS E CORREÇÕES.</para>
    </sect2>

    <sect2 id="sect12" label="12">
      <title>Seção 12</title>

      <para>EM NENHUMA OCASIÃO, A MENOS QUE EXIGIDO PELAS LEIS APLICÁVEIS OU ACORDO EXPRESSO, OS DETENTORES DOS DIREITOS AUTORAIS, OU QUALQUER OUTRA PARTE QUE POSSA MODIFICAR E/OU REDISTRIBUIR O PROGRAMA CONFORME PERMITIDO ACIMA, SERÃO RESPONSABILIZADOS POR VOCÊ POR DANOS, INCLUINDO QUALQUER DANO EM GERAL, ESPECIAL, ACIDENTAL OU CONSEQUENTE, RESULTANTES DO USO OU INCAPACIDADE DE USO DO PROGRAMA (INCLUINDO, MAS NÃO LIMITADO A, PERDA DE DADOS OU DADOS TORNADOS INCORRETOS, OU PERDAS SOFRIDAS POR VOCÊ OU POR OUTRAS PARTES, OU FALHAS DO PROGRAMA AO OPERAR COM QUALQUER OUTRO PROGRAMA), MESMO QUE TAL DETENTOR OU PARTE TENHAM SIDO AVISADOS DA POSSIBILIDADE DE TAIS DANOS.</para>
    </sect2>
  </sect1>
</article>
