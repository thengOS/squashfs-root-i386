<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="color-calibrate-camera" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="color#calibration"/>
    <link type="seealso" xref="color-calibrationtargets"/>
    <link type="seealso" xref="color-calibrate-printer"/>
    <link type="seealso" xref="color-calibrate-scanner"/>
    <link type="seealso" xref="color-calibrate-screen"/>
    <desc>Calibrar sua câmera é importante para capturar cores precisas.</desc>
    <revision version="13.10" date="2013-09-20" status="review"/>
    <credit type="author">
      <name>Richard Hughes</name>
      <email>richard@hughsie.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Como faço para calibrar minha câmera?</title>

  <p>Dispositivos de câmera são calibrados por ter uma fotografia de um alvo nas condições de iluminação desejada. Ao converter o arquivo RAW em um arquivo TIFF, ele pode ser usado para calibrar o dispositivo da câmera no painel de controle de cor.</p>
  <p>Você vai precisar cortar o arquivo TIFF para somente o alvo esteja visível. Certifique-se de que as bordas brancas ou pretas ainda estejam visíveis. A calibração não vai funcionar se a imagem estiver de cabeça para baixo ou estiver muito distorcida.</p>

  <note style="tip">
    <p>O perfil resultante é válido apenas sob a condição de iluminação que você obteve a imagem original. Isto significa que você pode precisar desse perfil várias vezes para as condições de iluminação <em>estúdio</em>, <em>luz solar</em> e <em>nublado</em>.</p>
  </note>

</page>
