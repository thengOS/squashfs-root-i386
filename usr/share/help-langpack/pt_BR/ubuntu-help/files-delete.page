<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="task" id="files-delete" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="files"/>
    <link type="seealso" xref="files-recover"/>
    <desc>Remover arquivos ou pastas que você não precisa mais.</desc>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-16" status="review"/>
    <revision version="13.10" date="2013-09-07" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
  </info>

<title>Exclua arquivos e pastas</title>

  <p>Se você não quiser mais um arquivou ou pasta, você pode excluí-lo. Quando você exclui um item, ele é movido para a pasta da <gui>Lixeira</gui>, onde ele fica armazenado até que você esvazie a lixeira. Você pode <link xref="files-recover">restaurar ítens </link> da pasta da <gui>Lixeira</gui> para a sua localização original se você resolver que precisa deles ou se eles foram acidentalmente excluídos.</p>

  <steps>
    <title>Para enviar um arquivo para a lixeira:</title>
    <item><p>Selecione o item que você quer colocar na lixeira clicando-o uma vez.</p></item>
    <item><p>Pressione <key>Delete</key> no seu teclado. Alternativamente, arraste o item para a <gui>Lixeira</gui> na barra lateral.</p></item>
  </steps>

  <p>Para excluir arquivos permanentemente e liberar espaço de disco no seu computador, você precisa esvaziar a lixeira. Para esvaziar a lixeira, clique com o botão direito do mouse em <gui>Lixeira</gui> na barra lateral e selecione <gui>Esvaziar lixeira</gui>.</p>

  <section id="permanent">
    <title>Apague um arquivo permanentemente</title>
    <p>Você pode imediatamente apagar um arquivo permanentemente, sem ter que enviá-lo para a lixeira primeiro.</p>

  <steps>
    <title>Para excluir um arquivo permanentemente:</title>
    <item><p>Selecione o item que deseja excluir.</p></item>
    <item><p>Mantenha pressionada a tecla <key>Shift</key>, então pressione a tecla <key>Delete</key> no seu teclado.</p></item>
    <item><p>Como você não pode desfazer essa ação, será solicitada a sua confirmação para a exclusão do arquivo ou pasta.</p></item>
  </steps>

  <note style="tip"><p>If you frequently need to delete files without using the
  trash (for example, if you often work with sensitive data), you can add a
  <gui>Delete</gui> entry to the right-click menu for files and folders.
  Click <gui>Files</gui> in the menu bar, pick <gui>Preferences</gui> and select
  the <gui>Behavior</gui> tab. Select <gui>Include a Delete command that
  bypasses Trash</gui>.</p></note>

  <note><p>Arquivos excluídos em um <link xref="files#removable">dispositivo removível </link> podem não ser visíveis em outros sistemas operacionais, como o Windows ou o Mac OS. Os arquivos ainda estão lá e estarão disponíveis se você inserir o dispositivo de volta em seu computador.</p></note>

  </section>
</page>
