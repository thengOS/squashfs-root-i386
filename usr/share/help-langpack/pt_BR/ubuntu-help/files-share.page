<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-share" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="files" group="more"/>
    <link type="guide" xref="sharing"/>
    <link type="seealso" xref="nautilus-connect"/>
    <desc>Transfer files to your email contacts from the file
    manager.</desc>

    <revision pkgversion="3.8.2" version="0.3" date="2013-05-11" status="review"/>
    <revision version="13.10" date="2013-10-23" status="review"/>

    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Compartilhe e transfira arquivos</title>

<p>You can share files with your contacts or transfer them to external 
devices or <link xref="nautilus-connect">network shares</link> directly 
from the file manager.</p>

<steps>
  <item><p>Abra o <link xref="files-browse">gerenciador de arquivos</link>.</p></item>
  <item><p>Localize o arquivo que você quer transferir.</p></item>
  <item><p>Clique com o botão direito no arquivo e selecione <gui>Enviar para...</gui>.</p></item>
  <item><p>A janela <gui>Enviar para...</gui> irá aparecer. Escolha onde você quer enviar o arquivo e clique em <gui>Enviar</gui>. Veja a lista de destinatários abaixo para mais informações.</p></item>
</steps>

<note style="tip">
  <p>Você pode enviar vários arquivos de uma vez. Selecione múltiplos arquivos mantendo pressionada a tecla <key>Ctrl</key>, então clique com o botão direito do mouse em quaisquer dos arquivos selecionados. Você pode comprimi-los automaticamente em um arquivo zip ou tar.</p>
</note>

<list>
  <title>Destinos</title>
  <item><p>Para enviar um arquivo por email, selecione <gui>Email</gui> e digite o endereço de email do destinatário.</p></item>
  <item><p>Para enviar o arquivo para um contato de mensageiro instantâneo, seleciona <gui>Mensageiro instantâneo</gui>, depois selecione contato da lista suspensa. Seu aplicativo de mensageiro instantâneo pode precisar ser iniciado para isso funcionar.</p></item>
  <item><p>Para gravar o arquivo em um CD ou DVD, selecione <gui>Criador de CD/DVD</gui>. Veja <link xref="files-disc-write"/> para aprender mais.</p></item>
  <item><p>Para transferir o arquivo a um dispositivo Bluetooth, selecione <gui>Bluetooth (OBEX Push)</gui> e selecione o dispositivo ao qual o arquivo será enviado. Você verá somente dispositivos com os quais já pareou. Veja <link xref="bluetooth"/> para mas informações.</p></item>
  <item><p>Para copiar um arquivo para um dispositivo externo tal como um pendrive, ou para enviá-lo para um servidor ao qual você esteja conectado, selecione <gui>Discos removíveis e compartilhamentos</gui>, então selecione o dispositivo ou servidor para o qual você deseja copiar o arquivo.</p></item>
</list>

</page>
