<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mouse-mousekeys" xml:lang="pt-BR">
<info>
    <link type="guide" xref="mouse"/>
    <link type="guide" xref="a11y#mobility" group="pointing"/>

    <revision pkgversion="3.8.0" date="2013-03-16" status="outdated"/>
    <revision version="13.10" date="2013-09-12" status="review"/>

    <desc>Habilitar as teclas do mouse para controlar o mouse com o teclado.</desc>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    
</info>

<title>Clicar e mover o ponteiro do mouse usando o teclado</title>

<p>Se você tiver dificuldades em usar um mouse ou outro dispositivo apontador, você pode controlar o ponteiro do mouse usando o teclado numérico do seu teclado. Este recurso é chamado de <em>teclas do mouse</em>.</p>

<steps>
  <item><p>Tap the <key><link xref="windows-key">Super</link></key> key to open the <gui>Dash</gui>.</p></item>
  <item><p>Digite <input>Acesso universal</input> e tecle <key>Enter</key> para abrir as configurações de Acesso universal.</p></item>
  <item><p>Tecle <key>Tab</key> uma vez para selecionar a aba <gui>Visão</gui>.</p></item>
  <item><p>Tecle <key>←</key> uma vez para alternar para a aba <gui>Apontando e clicando</gui>.</p></item>
  <item><p>Tecle <key>↓</key> uma vez para selecionar a chave <gui>Teclas do mouse</gui> e então tecle <key>Enter</key> para ativá-la.</p></item>
 <item>
  <p>Certifique-se que o <key>Num Lock</key> está desligado. Você poderá agora mover o ponteiro do mouse usando o teclado.</p>
 </item>
</steps>

 <note style="tip">
 <p>Estas instruções fornecem o caminho mais curto para habilitar teclas do mouse usando apenas o teclado. Selecione <gui>Configurações de acessibilidade</gui> para ver mais opções de acessibilidade.</p>
 </note>

<p>O teclado numérico é um conjunto de botões numéricos em seu teclado, geralmente dispostos em uma grade quadrada. Se você tiver um teclado sem o teclado numérico (como ocorre na maioria dos laptops), talvez seja necessário manter a tecla de função (<key>Fn</key>) pressionada e usar outras teclas em seu teclado como um teclado numérico. Se você utiliza este recurso com frequência em um laptop, você pode comprar teclados numéricos USB externos.</p>

<p>Cada número do teclado numérico corresponde a uma direção. Por exemplo, pressionar <key>8</key> vai mover o ponteiro para cima e pressionar <key>2</key> vai movê-lo para baixo. Pressione a tecla <key>5</key> para um clique simples, ou pressione-a rapidamente por duas vezes para um clique duplo.</p>

<p>Muitos teclados possuem uma tecla especial, geralmente próxima à barra de espaço, que simula um clique com o botão direito. Note, no entanto, que esta tecla responde a onde o foco do teclado está, e não onde o ponteiro do mouse está apontando. Veja <link xref="a11y-right-click"/> para informações de como executar o clique com o botão direito utilizando a tecla <key>5</key> ou o botão esquerdo do mouse.</p>

<p>Se você deseja usar o teclado numérico para digitar números enquanto as teclas de mouse estão habilitadas, ative o <key>Num Lock</key>. Entrentando, o mouse não será controlado pelo teclado numérico enquanto o <key>Num Lock</key> estiver ativado.</p>

<note>
    <p>As teclas normais dos números na parte superior do teclado não controlarão o ponteiro do mouse. Somente as teclas do teclado numérico poderão ser usadas.</p>
</note>

</page>
