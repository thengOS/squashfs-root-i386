<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="nautilus-file-properties-permissions" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="files#faq"/>
    <link type="seealso" xref="nautilus-file-properties-basic"/>

    <desc>Controla quem pode visualizar e editar seus arquivos e pastas.</desc>

    <revision pkgversion="3.6.0" version="0.2" date="2012-09-28" status="review"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <its:rules xmlns:its="http://www.w3.org/2005/11/its" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" xlink:type="simple" xlink:href="gnome-help.its"/>
  </info>
  <title>Configure permissões de arquivos</title>

  <p>Você pode utilizar as permissões de arquivo para controlar quem pode ver e editar seus arquivos. Para visualizar e definir as permissões de um arquivo, clique com o botão direito e selecione <gui>Propriedades</gui>, então selecione a guia <gui>Permissões</gui>.</p>

  <p>Veja <link xref="#files"/> e <link xref="#folders"/> abaixo para detalhes dos tipos de permissões que você pode definir.</p>

  <section id="files">
    <title>Arquivos</title>

    <p>Você pode definir permissões para o proprietário do arquivo, o proprietário do grupo e todos os usuários do sistema. Para seus arquivos, você é o proprietário e você pode dar a você mesmo permissão de somente leitura ou leitura e escrita. Defina um arquivo como somente leitura se você não quiser alterá-lo acidentalmente.</p>

    <p>Todo usuário no seu computador pertence a um grupo. Em computadores de casa, é comum que cada usuário tenha seu próprio grupo e permissões de grupo não são usadas com frequência. Em ambientes corporativos, grupos são usados algumas vezes para departamentos ou projetos. Assim como tem um dono, cada arquivo pertence a um grupo. Você pode definir o grupo do arquivo e controlar as permissões para todos os usuários naquele grupo. Você só pode definir o grupo do arquivo para um grupo ao qual você pertence.</p>

    <p>Você também pode definir permissões para outros usuários que não são o proprietário e aqueles no grupo do arquivo.</p>

    <p>Se o arquivo é um programa, como um script, você deve selecionar <gui>Permitir execução do arquivo como um programa</gui> para executá-lo. Mesmo com essa opção selecionada, o gerenciador de arquivos ainda pode abrir o arquivo em uma aplicação ou perguntar o que deve ser feito. Veja <link xref="nautilus-behavior#executable"/> para mais informações.</p>
  </section>

  <section id="folders">
    <title>Pastas</title>
    <p>Você pode definir permissões em pastas para o proprietário, grupo e outros usuários. Veja os detalhes das permissões de arquivos  acima para uma explicação de proprietários, grupos e outros usuários.</p>
    <p>As permissões que você pode definir para uma pasta são diferentes daquelas que você pode definir para um arquivo.</p>
    <terms>
      <item>
        <title><gui>Nenhum</gui></title>
        <p>O usuário não vai nem mesmo ser capaz de ver quais arquivos estão na pasta.</p>
      </item>
      <item>
        <title><gui>Apenas listar arquivos</gui></title>
        <p>O usuário será capaz de ver os arquivos que estão na pasta, mas não será capaz de abrir, criar ou excluir arquivos.</p>
      </item>
      <item>
        <title><gui>Acessar arquivos</gui></title>
        <p>O usuário será capaz de abrir arquivos na pasta (desde que ele tenha permissão para fazer isso no arquivo em particular), mas não será capaz de criar novos arquivos ou excluir arquivos.</p>
      </item>
      <item>
        <title><gui>Crie e exclua arquivos</gui></title>
        <p>O usuário terá acesso total aos diretórios, incluindo visualização, criação e exclusão de arquivos.</p>
      </item>
    </terms>

    <p>You can also quickly set the file permissions for all the files
    in the folder by clicking <gui>Change Permissions for Enclosed Files</gui>.
    Use the drop-down lists to adjust the permissions of contained files or
    folders, and click <gui>Change</gui>. Permissions are applied to files and
    folders in subfolders as well, to any depth.</p>
  </section>

</page>
