<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="net-wireless-disconnecting" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="net-wireless"/>
    <link type="guide" xref="net-problem"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Você pode ter um sinal baixou ou a rede pode não estar permitindo que você se conecte corretamente.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Por que minha rede sem fio fica desconectando?</title>

<p>É possível que você seja desconectado de uma rede sem fio mesmo contra a sua vontade. Seu computador normalmente tentará se reconectar à rede assim que isto acontecer (o ícone de rede na barra de menus vai piscar se ele estiver tentando se reconectar). Entretanto, isto pode ser um incômodo, especialmente se você estiver usando a Internet naquele momento.</p>

<section id="signal">
 <title>Sinal sem fio fraco</title>

 <p>Um motivo para ser desconectado de uma rede sem fio é o sinal fraco. As redes sem fio têm um alcance limitado, por isto, se você estiver muito longe da base da rede sem fio, não poderá obter um sinal forte o suficiente para manter uma conexão. Paredes e outros objetos entre você e a base também poderão enfraquecer o sinal.</p>
 
 <p>O ícone de rede na barra de menus mostra a força do seu sinal de rede sem fio. Se o sinal estiver fraco, tente ir para mais perto da estação base da rede sem fio.</p>
 
</section>

<section id="network">
 <title>A conexão de rede não está sendo estabelecida adequadamente</title>

 <p>Às vezes, quando você se conecta a uma rede sem fio, pode parecer que você se conectou com sucesso no início, mas então você é desconectado logo em seguida. Isto normalmente acontece porque seu computador teve êxito apenas parcial ao se conectar à rede. Ele conseguiu estabelecer conexão, mas foi incapaz de finalizar a conexão por algum motivo e então foi desconectado.</p>

 <p>Um possível motivo para isso é que você digitou a senha da rede sem fio errada ou porque o computador não está autorizado na rede (porque a rede necessita de um nome de usuário para efetuar o login, por exemplo).</p>

</section>

<section id="hardware">
 <title>Hardware e drivers de rede sem fio instáveis</title>

 <p>Alguns dispositivos de rede sem fio podem ser não muito estáveis. Redes sem fio são complicadas, então placas de rede sem fio e estações base ocasionalmente encontram pequenos problemas e podem derrubar conexões. Isto é um incômodo, mas acontece com muita frequência com vários dispositivos. Se você for desconectado de conexões sem fio periodicamente, este pode ser o único motivo. Se isto acontece com muita frequência, talvez você possa considerar a aquisição de outros dispositivos.</p>

</section>

<section id="busy">
 <title>Redes sem fio ocupadas</title>

 <p>Redes sem fio em lugares cheios (em universidades e cafés, por exemplo) muitas vezes têm muitos computadores tentando se conectar a elas ao mesmo tempo. Algumas vezes, estas redes ficam muito ocupadas e podem não ser capazes de lidar com todos os computadores que estão tentando se conectar, então alguns deles são desconectados.</p>

</section>

</page>
