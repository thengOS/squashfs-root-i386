<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-envelopes" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="printing#paper"/>

    <desc>Tenha certeza que você tem o envelope/etiqueta do jeito certo e escolheu o tamanho correto de papel.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Imprima em envelopes e etiquetas</title>

<p>A maioria das impressoras permitem que você imprima diretamente em um envelope ou folha de etiquetas. Isso é especialmente útil se você tem muitas cartas para mandar, por exemplo.</p>

<section id="envelope">
 <title>Imprimindo nos envelopes</title>

 <p>Há duas coisas que você precisa verificar antes de imprimir em um envelope. A primeira é se a impressora sabe o tamanho do envelope. Depois de clicar em <gui>Imprimir</gui> e a janela de impressão aparecer, vá até a guia <gui>Configurar página</gui> e em <gui>Tipo de papel</gui> escolha "Envelope" se estiver disponível. Se você não puder fazer isto, veja se você pode mudar o <gui>Tamanho do papel</gui> para tamanho de envelope (ex: "C5"). O pacote de envelopes mostra o tamanho deles. A maioria dos envelopes vêm em tamanhos padrão.</p>

 <p>Segundo, você precisa se certificar de que os envelopes são colocados do lado correto na bandeja da impressora. Para isso, verifique o manual da impressora ou experimente imprimir um envelope para ver qual lado deve ser colocado para cima.</p>

<note style="warning">
 <p>Algumas impressoras não são capazes de imprimir em envelopes, em especial algumas impressoras a laser. Veja no manual de sua impressora se ela aceita envelopes. Caso contrário, você pode danificar sua impressora ao inserir envelopes nela.</p>
</note>

</section>

<!--
INCOMPLETE: Please write this section!

<section id="labels">
 <title>Printing labels</title>

</section>
-->

</page>
