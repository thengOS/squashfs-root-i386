<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="printing-setup-default-printer" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="printing#setup"/>

    <desc>Escolha a impressora que você mais usa</desc>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email its:translate="no">jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email its:translate="no">stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Jana Svarova</name>
      <email its:translate="no">jana.svarova@gmail.com</email>
      <years>2013</years>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Defina a impressora padrão</title>
  <p>Se você tem mais de uma impressora disponível, você pode selecionar qual será a sua impressora padrão. Você talvez queira escolher a impressora que você mais usa.</p>

  <steps>
    <item>
      <p>Clique no ícone no canto direito da <gui>barra de menu</gui> e selecione <gui>Configuração do Sistema</gui>.</p>
    </item>
    <item>
      <p>Abra <gui>Impressoras</gui>.</p>
    </item>
    <item>
      <p>Clique com o botão direito na impressora padrão escolhida a partir da lista de impressoras disponíveis e clique em <gui>Definir como padrão</gui>.</p>
    </item>
  </steps>

  <note>
  <p>When choosing from the list of available printers, you can
  filter the printer search results by specifying a name or location of the
  printer (for example, <input>1st floor</input> or <input>entrance</input>).</p>
  <p>The search results filtering is available only in the dialog for addition of new printers.</p>
  </note>

  <p>Quando você imprime a partir de um aplicativo, a impressora padrão é automaticamente utilizada, a não ser que você escolha uma impressora diferente para esse trabalho de impressão específico.</p>
</page>
