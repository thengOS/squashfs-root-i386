<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="user-admin-explain" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="user-accounts#privileges"/>

    <desc>Você precisa ter privilégios de administrador para modificar partes importantes do seu sistema.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="13.10" date="2013-10-23" status="review"/>
    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Como funcionam os privilégios administrativos?</title>

  <p>Bem como os arquivos que <em>você</em> criar, o computador tem um número de arquivos que são necessários ao sistema para que funcione corretamente. Se estes <em>arquivos do sistema</em> importantes forem alterados indevidamente podem causar várias quebras, então eles estão protegidos contra alterações por predefinição. Certos aplicativos também modificam partes importantes do sistema, e por isso também são protegidos.</p>

  <p>O jeito como elas são protegidas é permitindo que apenas usuários com <em>privilégios administrativos</em> possam alterar os arquivos ou usar os aplicativos. No dia-a-dia, você não vai precisar alterar qualquer sistema de arquivos ou usar esses aplicativos, assim, por padrão você não tem privilégios de administrador.</p>

  <p>Às vezes, é necessário usar esses aplicativos, então você pode conseguir privilégios administrativos temporários para que seja capaz de fazer as alterações desejadas. Se um aplicativo precisa desses privilégios, ele lhe pedirá uma senha. Por exemplo, se quiser instalar algum aplicativo novo, o instalador de programas (ou gerenciador de pacotes) lhe pedirá sua senha de administrador para que possa adicionar o novo aplicativo ao sistema. Uma vez que ele tenha terminado, seus privilégios especiais serão retirados novamente.</p>

  <p>Os privilégios de administrador são associados com sua conta de usuário. Alguns usuários estão autorizados a ter privilégios de administrador e alguns não. Sem privilégios de administrador você não poderá instalar programas. Algumas contas de usuários (por exemplo, a conta "root") têm privilégios de administrador permanentes. Você não deve usar privilégios de administrador todo o tempo, porque você pode alterar algo você não tinha intenção de alterar (como excluir um aquivo necessário do sistema, por exemplo).</p>

  <p>Em resumo, privilégios de administrador permitem que você altere partes importantes do sistema quando necessário, mas impedem de você fazê-lo acidentalmente.</p>

<note>
 <title>O que significa "super usuário"?</title>
  <p>Um usuário com privilégios é chamado algumas vezes de <em>super usuário</em>. Simplesmente porque esse usuário tem mais privilégios que usuários normais. Você pode encontrar pessoas discutindo coisas como <cmd>su</cmd> e <cmd>sudo</cmd>; esses programas são para dar privilégios de "super usuário" (administrador) temporariamente.</p>
</note>

<section id="advantages">
 <title>Porque os privilégios de administrador são úteis?</title>
  <p>Requer que os usuários tenham privilégios de administrador antes de mudanças importantes no sistema são feitos para ajudar a impedir que o sistema seja prejudicado, intencionalmente ou não.</p>
  <p>Se você tiver privilégios de administrador o tempo todo, você poderá mudar acidentalmente um arquivo importante, ou executar um aplicativo que muda alguma coisa importante por engano. Só recebendo privilégios de administrador temporariamente, quando necessário, reduz o risco de acontecer esses erros.</p>
  <p>Apenas alguns usuários de confiança devem ser autorizados a ter privilégios de administrador. Isso impede que outros usuários possam bagunçar o computador, por exemplo desinstalando os aplicativos que você necessita, instalando aplicativos que você não quer ou até mesmo alterando arquivos importantes. Do ponto de vista da segurança, isso é muito útil.</p>
</section>

</page>
