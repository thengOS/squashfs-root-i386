<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="user-delete" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="user-accounts#manage"/>
    <desc>Remova usuários que não usam mais o seu computador.</desc>
    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="13.10" date="2013-10-23" status="review"/>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>
  <title>Exclua uma conta de usuário</title>

  <p>Você pode adicionar múltiplas contas de usuário no seu computador. Veja em <link xref="user-add"/> para saber como. Se alguém não estiver mais usando o seu computador, você poderá excluir a conta daquele usuário.</p>

<steps>
  <item><p>Clique no ícone no canto direito da <gui>barra de menu</gui> e selecione <gui>Configuração do Sistema</gui>.</p></item>
  <item><p>Abrir <gui>Contas de Usuário</gui>.</p></item>
  <item><p>Clique em <gui>Desbloquear</gui> no canto superior direito e digite sua senha para fazer modificações. Você precisa ser um usuário administrador para apagar contas de usuário.</p></item>
  <item><p>Selecione o usuário que você deseja apagar e clique no botão <gui>-</gui>.</p></item>
  <item><p>Cada usuário tem sua própria pasta pessoal para os seus arquivos e configurações. Você pode optar por manter ou excluir a pasta pessoal do usuário. Clique em <gui>Excluir arquivos</gui> se você tem certeza que não será mais usado e você precisa liberar espaço em disco. Esses arquivos são excluídos permanentemente. Eles não podem ser recuperados. Você pode querer fazer backup dos arquivos para uma unidade externa ou CD antes de excluí-los.</p></item>
</steps>
</page>
