<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="video-dvd" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="media#videos"/>

    <desc>Talvez você não tenha os codecs corretos instalados ou o DVD pode ser de uma região errada.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision version="13.10" date="2013-10-23" status="review"/>    
    <credit type="author">
      <name>Projeto de documentação do GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Por que os DVDs não são reproduzidos?</title>

  <p>Se você inserir um DVD no seu computador e não conseguir reproduzi-lo, você pode não ter os <em>codecs</em> corretos de DVD instalados, ou o DVD pode ser de uma <em>região</em> diferente.</p>

<section id="codecs">
 <title>Instalando o codec correto para assistir DVD</title>
 <p>Para reproduzir DVDs, você precisa ter os <em>codecs</em> certos instalados. Um codec é uma parte de programa que permite que o aplicativo leia um formato de vídeo ou áudio. Se você tentar reproduzir um DVD e não tiver os codecs certos instalados, o reprodutor de filme irá oferecer-se para instalá-los para você.</p>

  <p>DVDs também são <em>protegidos contra cópia</em> usando um sistema chamado CSS. Isto impede que você copie DVDs, mas também o impede de reproduzi-los, a menos que você tenha <link xref="video-dvd-restricted">programas extras</link> para lidar com a proteção de cópia.</p>
</section>

<section id="region">
 <title>Verificando a região do DVD</title>
  <p>DVDs têm um <em>código de região</em>, que lhe informa em que região do mundo você tem permissão de reproduzi-los. Se a região do reprodutores de DVD do seu computador não coincide com a região do DVD que você está tentando reproduzir, você não será capaz de reproduzi-lo. Por exemplo, se você tem um aparelho da região 1, só poderá reproduzir DVDs da América do Norte.</p>

  <p>Muitas vezes, é possível alterar a região do seu reprodutor de DVD, mas isto só pode ser feito algumas vezes antes de travá-lo em uma região permanentemente. Para alterar a região de DVD do reprodutor de DVD do seu computador, use <link href="apt:regionset">regionset</link>.</p>
</section>
	
</page>
