<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task a11y" id="a11y-stickykeys" xml:lang="ru">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard" group="a11y"/>

    <revision pkgversion="3.8.0" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-09-18" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="final"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <credit type="author">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email its:translate="no">shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Фил Булл (Phil Bull)</name>
      <email its:translate="no">philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Екатерина Герасимова (Ekaterina Gerasimova)</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <desc>Набор комбинаций клавиш нажатием их поодиночке, а не всех одновременно.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Включение «залипающих клавиш»</title>

  <p><em>«Залипающие клавиши»</em> позволяют вводить комбинации клавиш не одновременным нажатием всех клавиш, а нажимая их по одной. Например, для переключения между окнами используется комбинация клавиш <keyseq><key xref="keyboard-key-super">Super</key><key>Tab</key></keyseq>. Если «залипающие клавиши» отключены, нужно нажать обе клавиши одновременно, если включены — можно нажать сначала <key>Super</key> и затем <key>Tab</key>.</p>

  <p>«Залипающие клавиши» могут быть полезны, если вам трудно удерживать нажатыми несколько клавиш одновременно.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Universal Access</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Универсальный доступ</gui>, чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Нажмите <gui>Помощник ввода (AccessX)</gui> в разделе <gui>Ввод</gui>.</p>
    </item>
    <item>
      <p>Измените переключатель <gui>Залипающие клавиши</gui> в положение <gui>Включено</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <title>Быстрое включение и отключение «залипающих клавиш»</title>
    <p>Чтобы управлять включением и отключением залипающих клавиш с помощью клавиатуры, в пункте <gui>Управляется с клавиатуры</gui> установите флажок <gui>Включать и выключать специальные возможности с помощью клавиатуры</gui>. После этого можно будет включать или отключать залипающие клавиши нажатием клавиши <key>Shift</key> пять раз подряд.</p>
    <p>Чтобы включить или отключить залипающие клавиши, можно также нажать на <link xref="a11y-icon">значок специальных возможностей</link> в верхней панели и выбрать <gui>Залипающие клавиши</gui>. Значок специальных возможностей становится виден в том случае, если один или более параметров были включены в разделе <gui>Универсальный доступ</gui>.</p>
  </note>

  <p>Если нажать две клавиши одновременно, «залипающие клавиши» временно отключатся, чтобы вы могли ввести комбинацию клавиш «обычным» способом.</p>

  <p>Например, если залипающие клавиши включены, но вы нажали <key>Super</key> и <key>Tab</key> одновременно, залипающие клавиши не будут ждать нажатия ещё одной клавиши, если эта опция включена. Однако, они <em>будут</em> ждать, если вы нажали только одну клавишу. Это полезно, если вы можете нажимать некоторые сочетания клавиш одновременно (например, клавиши, которые находятся рядом друг с другом), но не другие.</p>

  <p>Для включения этой возможности выберите <gui>Отключать, если две клавиши нажаты вместе</gui>.</p>

  <p>Можно сделать так, чтобы компьютер подавал звуковой сигнал, если вы начинаете набор на клавиатуре при включённых «залипающих клавишах». Это полезно, если вы хотите знать, когда «залипающие клавиши» ожидают нажатия комбинации клавиш и следующее нажатие будет интерпретироваться, как часть этой комбинации. Для включения этой возможности выберите <gui>Подавать сигнал при нажатии модификатора</gui>.</p>

</page>
