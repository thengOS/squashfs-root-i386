<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="disk-benchmark" xml:lang="ru">

  <info>
    <link type="guide" xref="disk"/>

    <revision pkgversion="3.6.2" version="0.2" date="2012-11-16" status="review"/>
    <revision pkgversion="3.10" date="2013-11-03" status="candidate"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email its:translate="no">gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email its:translate="no">nruz@alumnos.inf.utfsm.cl</email>
    </credit>
   <credit type="editor">
     <name>Майкл Хилл (Michael Hill)</name>
     <email its:translate="no">mdhillca@gmail.com</email>
   </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Запустите тест производительности жёсткого диска, чтобы проверить скорость его работы.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Тестирование производительности жёсткого диска</title>

  <p>Чтобы проверить скорость жёсткого диска:</p>

  <steps>
    <item>
      <p>Open <app>Disks</app> from the
      <gui xref="shell-introduction#activities">Activities</gui> overview.</p>
    </item>
    <item>
      <p>Выберите диск из списка в панели слева.</p>
    </item>
    <item>
      <p>Click the gear button and select <gui>Benchmark…</gui> from the
      menu.</p>
    </item>
    <item>
      <p>Click <gui>Start Benchmark…</gui> and adjust the <gui>Transfer
      Rate</gui> and <gui>Access Time</gui> parameters as desired.</p>
    </item>
    <item>
      <p>Нажмите <gui>Оценка производительности</gui>, чтобы оценить скорость чтения данных с диска. Могут потребоваться <link xref="user-admin-explain">административные полномочия</link>. Введите свой пароль или пароль для запрашиваемой учётной записи администратора.</p>
      <note>
        <p>If <gui>Perform write-benchmark</gui> is checked, the benchmark
        will test how fast data can be read from and written to the disk. This
        will take longer to complete.</p>
      </note>
    </item>
  </steps>

  <p>По окончании теста его результаты появятся в графике. Замеры значений обозначены зелёными точками, соединёнными линиями. По правой оси отложено время доступа, на нижней — значения процентов времени, измеренного в результате тестирования. Синими и красными линиями отображаются скорости чтения и записи соответственно. По левой ости отложена скорость доступа к данным, на нижней - процент времени, затраченного на перемещение по диску с внешнего края к центру.</p>

  <p>Под графиком отображаются значения минимальной, максимальной и средней скорости чтения и записи, среднего времени доступа и время, прошедшего с момента последнего теста производительности.</p>

</page>
