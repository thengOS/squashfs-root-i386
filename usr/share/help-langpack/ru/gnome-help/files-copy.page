<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="files-copy" xml:lang="ru">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-15" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-20" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Копирование или перемещение объектов в другую папку.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Копирование и перемещение файлов и папок</title>

 <p>Файл или папку можно скопировать или переместить в другое место, перетаскивая их мышью, используя команды копирования и вставки, или с помощью комбинаций клавиш.</p>

 <p>Например, можно скопировать презентацию на флэш-носитель, чтобы взять её с собой на работу, или сделать резервную копию документа перед внесением в него изменений (а затем использовать старую копию, если изменения не понравились).</p>

 <p>Эти инструкции применимы как к файлам, так и к папкам. Копирование и перемещение файлов и папок выполняется одинаково.</p>

<steps ui:expanded="false">
<title>Копирование и вставка файлов</title>
<item><p>Выберите файл, который хотите скопировать, нажав на него.</p></item>
<item><p>Нажмите правой кнопкой и выберите <gui>Копировать</gui> или нажмите <keyseq><key>Ctrl</key><key>C</key></keyseq>.</p></item>
<item><p>Перейдите в папку, в которую нужно поместить копию файла.</p></item>
<item><p>Click the menu button and pick <gui>Paste</gui> to finish copying the
 file, or press <keyseq><key>Ctrl</key><key>V</key></keyseq>. There
 will now be a copy of the file in the original folder and the other
 folder.</p></item>
</steps>

<steps ui:expanded="false">
<title>Перемещение файлов командами вырезания и вставки</title>
<item><p>Выберите файл, который хотите переместить, нажав на него.</p></item>
<item><p>Нажмите правой кнопкой и выберите <gui>Вырезать</gui> или нажмите <keyseq><key>Ctrl</key><key>X</key></keyseq>.</p></item>
<item><p>Перейдите в ту папку, в которую нужно переместить файл.</p></item>
<item><p>Click the menu button in the toolbar and pick <gui>Paste</gui> to
 finish moving the file, or press <keyseq><key>Ctrl</key><key>V</key></keyseq>.
 The file will be taken out of its original folder and moved to the other
 folder.</p></item>
</steps>

<steps ui:expanded="false">
<title>Копирование или перемещение файлов перетаскиванием</title>
<item><p>Откройте менеджер файлов и перейдите в папку, содержащую файл, который нужно скопировать.</p></item>
<item><p>Нажмите <gui>Nautilus</gui> в верхней панели и выберите <gui>Создать окно</gui> (или нажмите <keyseq><key>Ctrl</key><key>N</key></keyseq>), чтобы открыть новое окно. В новом окне перейдите в папку, в которую нужно переместить или скопировать файл.</p></item>
<item>
 <p>Нажмите на файл и перетащите его из одного окна в другое. Файл будет <em>перемещён</em>, если целевая папка находится на <em>том же</em> устройстве, и <em>скопирован</em>, если целевая папка находится на <em>другом</em> устройстве.</p>
 <p>Например, при перетаскивании файла с USB-носителя в домашнюю папку он будет скопирован, так как вы перетаскиваете его с одного устройства на другое.</p>
 <p>Для принудительного копирования файла удерживайте при его перетаскивании нажатой клавишу <key>Ctrl</key>, а для принудительного перемещения — клавишу <key>Shift</key>.</p>
 </item>
</steps>

<note>
  <p>You cannot copy or move a file into a folder that is <em>read-only</em>.
  Some folders are read-only to prevent you from making changes to their
  contents. You can change things from being read-only by
  <link xref="nautilus-file-properties-permissions">changing file permissions
  </link>.</p>
</note>

</page>
