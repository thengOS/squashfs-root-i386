<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="get-involved" xml:lang="ru">

  <info>
    <link type="guide" xref="more-help"/>
    <desc>Как и куда сообщать об ошибках в этом руководстве</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>
  <title>Участвуйте в улучшении этого руководства</title>

  <section id="bug-report">
   <title>Сообщения об ошибках или запрос новых функций</title>
   <p>Эта справочная документация создана сообществом добровольцев. Вы тоже можете принять в этом участие. Если вы заметите ошибку на этих справочных страницах (опечатку, неправильную инструкцию или отсутствие тем, которые должны быть в руководстве), можете отправить <em>отчёт об ошибке</em>. Для этого зайдите на <link href="https://bugzilla.gnome.org/">bugzilla.gnome.org</link>.</p>
   <p>Чтобы сообщить об ошибке и получать по электронной почте уведомления об её состоянии, нужно зарегистрироваться. Если у вас ещё нет учётной записи, нажмите ссылку <gui>New Account</gui> для её создания.</p>
   <p>После создания учётной записи войдите в неё, и перейдите по ссылкам <guiseq><gui>File a Bug</gui><gui>Core</gui><gui>gnome-user-docs</gui></guiseq>. Перед отправкой отчёта об ошибке прочтите <link href="https://bugzilla.gnome.org/page.cgi?id=bug-writing.html">указания по составлению отчётов об ошибках</link> и <link href="https://bugzilla.gnome.org/browse.cgi?product=gnome-user-docs">посмотрите</link>, не сообщал ли уже кто-нибудь о подобной ошибке.</p>
   <p>Чтобы сообщить об ошибке, выберите компонент из меню <gui>Component</gui>. Если вы хотите сообщить об ошибке в документации, следует выбрать компонент <gui>gnome-help</gui>. Если вы не знаете, к какому компоненту относится ошибка, выберите <gui>general</gui>.</p>
   <p>Если вы сообщаете о недостающей, по вашему мнению, теме справки, выберите <gui>enhancement</gui> в меню <gui>Severity</gui>. Заполните разделы «Summary» и «Description» и нажмите <gui>Commit</gui>.</p>
   <p>Вашему отчёту будет присвоен идентификационный номер, и его статус будет обновляться по мере обработки отчёта. Спасибо за то, что помогаете сделать справку GNOME лучше!</p>
   </section>

   <section id="contact-us">
   <title>Свяжитесь с нами</title>
   <p>Вы можете отправить <link href="mailto:gnome-doc-list@gnome.org">сообщение</link> электронной почты в почтовую рассылку команды документирования GNOME, чтобы узнать больше о том, как принять участие в работе этой команды.</p>
   </section>
</page>
