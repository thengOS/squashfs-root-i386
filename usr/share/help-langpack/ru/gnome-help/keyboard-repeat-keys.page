<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="keyboard-repeat-keys" xml:lang="ru">

  <info>
    <link type="guide" xref="a11y#mobility" group="keyboard"/>
    <link type="guide" xref="keyboard"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision pkgversion="3.9.92" date="2013-10-11" status="candidate"/>
    <revision pkgversion="3.13.92" date="2013-10-11" status="candidate"/>

    <credit type="author">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
       <name>Natalia Ruz Leiva</name>
       <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="author">
       <name>Юлита Инка (Julita Inca)</name>
       <email>yrazes@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Шобха Тьяги (Shobha Tyagi)</name>
      <email>tyagishobha@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Сделайте так, чтобы клавиатура не повторяла буквы при удержании клавиши, или измените задержку и скорость повторного нажатия.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Отключение автоповтора</title>

  <p>По умолчанию при удержании клавиши нажатой буква или символ печатаются повторно до тех пор, пока вы не отпустите клавишу. Если вам трудно быстро поднимать пальцы, то можно отключить эту функцию или изменить время до начала повторения после нажатия клавиши.</p>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Keyboard</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Клавиатура</gui>, чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
     <p>Отключите <gui>Повторять удерживаемую нажатой клавишу</gui>, чтобы полностью отключить повторные нажатия клавиш.</p>
     <p>Или используйте ползунок <gui>Задержка</gui>, чтобы настроить время удержания клавиши нажатой для начала повтора, и ползунок <gui>Скорость</gui>, чтобы изменить скорость повторного нажатия.</p>
    </item>
  </steps>

</page>
