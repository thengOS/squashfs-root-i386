<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-purge" xml:lang="ru">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="guide" xref="files#more-file-tasks"/>

    <revision pkgversion="3.10" date="2013-09-29" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="candidate"/>

    <credit type="author">
      <name>Джим Кэмпбелл (Jim Campbell)</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>mdhillca@gmail.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Настройка частоты очистки корзины и удаления временных файлов.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Очистка корзины и удаление временных файлов</title>

  <p>Очистка корзины и удаление временных файлов освобождает компьютер от ненужных и нежелательных файлов и добавляет свободного места на жёстком диске. Эти операции можно проводить вручную, но также можно настроить компьютер на их автоматическое выполнение.</p>

  <p>Temporary files are files created automatically by applications in the
  background. They can increase performance by providing a copy of data that
  was downloaded or computed.</p>

  <steps>
    <title>Automatically empty your trash and clear temporary files</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Privacy</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Конфиденциальность</gui> чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Выберите <gui>Очистка корзины и удаление временных файлов</gui>.</p>
    </item>
    <item>
      <p>Set one or both of the <gui>Automatically empty Trash</gui> or
      <gui>Automatically purge Temporary Files</gui> switches to
      <gui>ON</gui>.</p>
    </item>
    <item>
      <p>Настройте частоту очистки <em>Корзины</em> и <em>Временных файлов</em>, изменив значение параметра <gui>Очищать после</gui>.</p>
    </item>
    <item>
      <p>Чтобы произвести эти действия немедленно, используйте кнопки <gui>Очистить корзину</gui> или <gui>Удалить временные файлы</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>You can delete files immediately and permanently without using the Trash.
    See <link xref="files-delete#permanent"/> for information.</p>
  </note>

</page>
