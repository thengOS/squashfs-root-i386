<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="privacy-screen-lock" xml:lang="ru">

  <info>
    <link type="guide" xref="privacy"/>
    <link type="seealso" xref="session-screenlocks"/>
    <link type="seealso" xref="shell-exit#lock-screen"/>

    <revision pkgversion="3.8" date="2013-05-21" status="candidate"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-12" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-30" status="final"/>

    <credit type="author">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit>
      <name>Джим Кэмпбелл (Jim Campbell)</name>
      <email>jwcampbell@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Предотвращение доступа других людей к вашему рабочему столу, когда вы отходите от компьютера.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Автоматическая блокировка экрана</title>
  
  <p>When you leave your computer, you should
  <link xref="shell-exit#lock-screen">lock the screen</link> to prevent
  other people from using your desktop and accessing your files. If you
  sometimes forget to lock your screen, you may wish to have your computerʼs
  screen lock automatically after a set period of time. This will help to
  secure your computer when you arenʼt using it.</p>

  <note><p>При заблокированном экране приложения и системные процессы не выключаются, но вам понадобится ввести пароль чтобы возобновить работу с ними.</p></note>
  
  <steps>
    <title>Чтобы установить промежуток времени перед автоматической блокировкой экрана:</title>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Privacy</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Конфиденциальность</gui> чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Выберите <gui>Блокировка экрана</gui>.</p>
    </item>
    <item>
      <p>Make sure <gui>Automatic Screen Lock</gui> is switched <gui>ON</gui>,
      then select a length of time from the drop-down list.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Applications can present notifications to you that are still displayed
    on your lock screen. This is convenient, for example, to see if you have
    any email without unlocking your screen. If youʼre concerned about other
    people seeing these notifications, switch <gui>Show Notifications</gui>
    off.</p>
  </note>

  <p>Если экран заблокирован, и вы хотите разблокировать его, нажмите клавишу <key>Esc</key> или потяните экран снизу вверху с помощью мыши. Затем введите свой пароль и нажмите клавишу <key>Enter</key> или нажмите кнопку <gui>Разблокировать</gui>. Вы также можете просто начать вводить свой пароль, экран блокировки будет автоматически убран.</p>

</page>
