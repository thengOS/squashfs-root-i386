<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="screen-shot-record" xml:lang="ru">

  <info>
    <link type="guide" xref="tips"/>

    <revision pkgversion="3.6.1" date="2012-11-10" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.14.0" date="2015-01-14" status="review"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
      <years>2011</years>
    </credit>
    <credit type="author copyright">
      <name>Шон МакКенс (Shaun McCance)</name>
      <email>shaunm@gnome.org</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Майкл Хилл (Michael Hill)</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Дэвид Кинг (David King)</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Создание снимка экрана или съёмка видео с экрана.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Снимки экрана и скринкасты</title>

  <p>Вы можете сделать снимок изображения свого экрана (<em>скриншот</em>) или снять видео с тем, что происходит на экране (<em>скринкаст</em>). Это полезно, когда, например, нужно кому-нибудь показать, как выполнить какое-то действие на компьютере. Скриншоты и скринкасты — это обычные файлы, которые содержат соответственно изображения или видео, их, как и обычные файлы, можно отправлять по электронной почте или выложить в общий доступ в Интернете.</p>

<section id="screenshot">
  <title>Сделать снимок экрана</title>

  <steps>
    <item>
      <p>Open <app>Screenshot</app> from the
      <gui xref="shell-introduction#activities">Activities</gui> overview.</p>
    </item>
    <item>
      <p>В окне приложения <app>Снимок экрана</app> выберите, какой именно снимок нужно сделать: всего рабочего стола, текущего окна или области экрана. Установите задержку, если для снимка необходимо дополнительное время, чтобы выбрать окно или выполнить какие-либо подготовительные действия. Затем выберите любой эффект по своему желанию.</p>
    </item>
    <item>
       <p>Нажмите кнопку <gui>Сделать снимок экрана</gui>.</p>
       <p>Если выбрано <gui>Выбрать область захвата</gui>, указатель превратится в крестик. Нажмите и перетащите крестик, чтобы выделить рамкой область экрана, снимок которой нужно сделать.</p>
    </item>
    <item>
      <p>В окне <gui>Сохранить снимок экрана</gui> введите имя файла и выберите папку, затем нажмите <gui>Сохранить</gui>.</p>
      <p>Можно также импортировать снимок экрана непосредственно в графический редактор, не сохраняя его сначала в файл. Нажмите <gui>Копировать в буфер обмена</gui>, затем вставьте изображение в другое приложение или перетащите миниатюру снимка экрана в приложение.</p>
    </item>
  </steps>

  <section id="keyboard-shortcuts">
    <title>Комбинации клавиш</title>

    <p>Чтобы быстро сделать снимок рабочего стола, окна или области на экране, используйте эти глобальные комбинации клавиш:</p>

    <list style="compact">
      <item>
        <p><key>Prt Scrn</key>, чтобы сделать снимок рабочего стола.</p>
      </item>
      <item>
        <p><keyseq><key>Alt</key><key>Prt Scrn</key></keyseq>, чтобы сделать снимок окна.</p>
      </item>
      <item>
        <p><keyseq><key>Shift</key><key>Prt Scrn</key></keyseq>, чтобы сделать снимок выделенной вами области.</p>
      </item>
    </list>

    <p>При использовании комбинаций клавиш изображение автоматически будет сохранено в папку «Изображения», имя файла с изображением начинается со слов «Снимок экрана от» и содержит дату и время снимка.</p>
    <note style="note">
      <p>Если папка <file>Изображения</file> не существует, изображения будут сохраняться в домашний каталог.</p>
    </note>
    <p>Можно также удерживать нажатой <key>Ctrl</key> при использовании одной из описанных выше комбинаций клавиш, чтобы вместо сохранения снимка экрана скопировать его в буфер обмена.</p>
  </section>

</section>

<section id="screencast">
  <title>Создание скринкаста</title>

  <p>Можно создать видеозапись того, что происходит на вашем экране:</p>

  <steps>
    <item>
      <p>Чтобы начать запись видео с экрана, нажмите <keyseq><key>Ctrl</key><key>Alt</key><key>Shift</key><key>R</key></keyseq>.</p>
      <p>A red circle is displayed in the top right corner of the screen
      when the recording is in progress.</p>
    </item>
    <item>
      <p>Чтобы остановить запись, ещё раз нажмите <keyseq><key>Ctrl</key><key>Alt</key><key>Shift</key><key>R</key></keyseq>.</p>
    </item>
    <item>
      <p>Записанные видеофайлы автоматически сохраняются в папку <file>Видео</file>, имя файла начинается со слова <file>Скринкаст</file> и содержит дату и время записи.</p>
    </item>
  </steps>

  <note style="note">
    <p>Если папка <file>Видео</file> не существует, видео будут сохраняться в домашний каталог.</p>
  </note>

</section>

</page>
