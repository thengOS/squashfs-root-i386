<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" version="1.0 if/1.0" id="sharing-personal" xml:lang="ru">

  <info>
    <link type="guide" xref="sharing"/>
    <link type="guide" xref="prefs-sharing"/>

    <revision pkgversion="3.10" version="0.1" date="2013-09-23" status="review"/>
    <revision pkgversion="3.12" date="2014-03-23" status="candidate"/>
    <revision pkgversion="3.14" date="2014-10-13" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="review"/>

    <credit type="author">
      <name>Майкл Хилл (Michael Hill)</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Открыть сторонним пользователям доступ к папке <file>Общедоступное</file>.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Александр Прокудин</mal:name>
      <mal:email>alexandre.prokoudine@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Алексей Кабанов</mal:name>
      <mal:email>ak099@mail.ru</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Станислав Соловей</mal:name>
      <mal:email>whats_up@tut.by</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юлия Дронова</mal:name>
      <mal:email>juliette.tux@gmail.com</mal:email>
      <mal:years>2013-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрий Мясоедов</mal:name>
      <mal:email>ymyasoedov@yandex.ru</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

  <title>Сделайте общими ваши личные файлы</title>

  <p>You can allow access to the <file>Public</file> folder in your
  <file>Home</file> folder from another computer on the network. Configure
  <gui>Personal File Sharing</gui> to allow others to access the contents of
  the folder.</p>

  <note style="info package">
    <p>Чтобы параметр <gui>Общий доступ к личным файлам</gui> стал видимым, в системе должен быть установлен пакет<app>gnome-user-share</app>.</p>

    <if:choose xmlns:if="http://projectmallard.org/if/1.0/">
      <if:when test="action:install">
        <p><link action="install:gnome-user-share" style="button">Установите gnome-user-share</link></p>
      </if:when>
    </if:choose>
  </note>

  <steps>
    <item>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Sharing</gui>.</p>
    </item>
    <item>
      <p>Нажмите <gui>Общий доступ</gui> чтобы открыть этот раздел настроек.</p>
    </item>
    <item>
      <p>Если переключатель <gui>Общий доступ</gui> находится в положении <gui>Выкл.</gui>, то включите его.</p>

      <note style="info"><p>If the text below <gui>Computer Name</gui> allows
      you to edit it, you can <link xref="sharing-displayname">change</link>
      the name your computer displays on the network.</p></note>
    </item>
    <item>
      <p>Выберите <gui>Общий доступ</gui>.</p>
    </item>
    <item>
      <p>Switch <gui>Personal File Sharing</gui> to <gui>ON</gui>. This means
      that other people on your current network will be able to attempt to
      connect to your computer and access files in your <file>Public</file>
      folder.</p>
      <note style="info">
        <p>A <em>URI</em> is displayed by which your <file>Public</file> folder
        can be accessed from other computers on the network.</p>
      </note>
    </item>
  </steps>

  <section id="security">
  <title>Защита</title>

  <terms>
    <item>
      <title>Требуется пароль</title>
      <p>Если вы хотите установить пароль на доступ других пользователей к папке <file>Общедоступные</file>, включите переключатель <gui>Требовать пароль</gui>. Без этого параметра любой сторонний пользователь сможет попробовать просмотреть папку <file>Общедоступные</file>.</p>
      <note style="tip">
        <p>По умолчанию эта опция отключена, но настоятельно рекомендуется включить её и выбрать надёжный пароль.</p>
      </note>
    </item>
  </terms>
  </section>

  <section id="networks">
  <title>Networks</title>

  <p>The <gui>Networks</gui> section lists the networks to which you are
  currently connected. Use the <gui>ON | OFF</gui> switch next to each to
  choose where your personal files can be shared.</p>

  </section>

</page>
