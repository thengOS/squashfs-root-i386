<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="addremove-install" xml:lang="ru">

  <info>
    <credit type="author">
      <name>Команда документации Ubuntu</name>
    </credit>
    <desc>
      Use <app>Ubuntu Software</app> to add programs and make Ubuntu more 
      useful.
    </desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <link type="guide" xref="addremove" group="#first"/>
    <link type="seealso" xref="addremove-remove"/>
    <link type="seealso" xref="addremove-install-synaptic"/>
    <link type="seealso" xref="prefs-language-install"/>
    <revision version="16.04" date="2016-03-13" status="review"/>
  </info>

  <title>Установка дополнительных программ</title>

  <p>Команда разработчиков Ubuntu выбрала устанавливаемый по умолчанию набор приложений, который, по нашему мнению, очень подходит для большинства повседневных задач. Но вы, разумеется, захотите установить дополнительные программы, чтобы сделать Ubuntu более удобной для себя.</p>

  <p>Чтобы установить дополнительное программное обеспечение, выполните следующие шаги:</p>

  <steps>
    <item>
      <p>Подключитесь к Интернету, используя <link xref="net-wireless-connect">беспроводное</link> или <link xref="net-wired-connect">проводное соединение</link>.</p>
    </item>
    <item>
      <p>
        Click the <app>Ubuntu Software</app> icon in the <gui>Launcher</gui>, or search
        for <input>Software</input> in the search bar of the <gui>Dash</gui>.
      </p>
    </item>
    <item>
      <p>
        When <app>Ubuntu Software</app> launches, search for an application, or select 
        a category and find an application from the list.
      </p>
    </item>
    <item>
      <p>Выберите интересующее приложение и щёлкните <gui>Установить</gui>.</p>
    </item>
    <item>
      <p>Система попросит ввести ваш пароль. Установка начнётся сразу после ввода.</p>
    </item>
    <item>
      <p>Обычно установка заканчивается быстро, но если интернет-соединение медленное, то может потребоваться некоторое время.</p>
    </item>
    <item>
      <p>Ярлык нового приложения будет добавлен на панель запуска. Чтобы отключить такую возможность, снимите флажок <guiseq><gui>Вид</gui><gui>Новые приложения в панели запуска</gui></guiseq>.</p>
    </item>
  </steps>
  
</page>
