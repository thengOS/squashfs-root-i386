<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="look-display-fuzzy" xml:lang="ru">

  <info>
   <link type="guide" xref="hardware-problems-graphics"/>
   <desc>Возможно, выбрано неправильное разрешение экрана.</desc>

    <revision version="13.10" date="2013-10-22" status="review"/>
    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>

    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Почему всё выглядит расплывчатым/мозаичным на моем экране?</title>

<p>Такое может случиться, если установить несоответствующее вашему экрану разрешение.</p>

<p>Чтобы это исправить, щёлкните самый правый значок на панели меню и перейдите в <gui>Параметры системы</gui>. В разделе «Оборудование» выберите <gui>Настройка экранов</gui>. Попробуйте подобрать <gui>Разрешение</gui> и установить такое, при котором экран будет выглядеть лучше.</p>

<section id="multihead">
 <title>Если подключены несколько экранов</title>

 <p>Если к вашему компьютеру подключены два экрана (например обычный монитор и проектор), они могут использовать отличающиеся разрешения. Однако видеокарта компьютера может поддерживать только одно разрешение, так что по крайней мере на одном из экранов изображение может быть нечётким.</p>

 <p>Можно установить отличающиеся разрешения на двух экранах, но тогда нельзя будет отображать одно и то же на обоих экранах одновременно. Фактически у вас будут два подключённых независимых экрана. Можно будет перемещать окна с одного экрана на другой, но показать одно окно сразу на двух экранах будет нельзя.</p>

 <p>Чтобы установить для каждого экрана своё собственное разрешение:</p>

 <steps>
  <item>
   <p>Щёлкните самый правый значок на панели меню и выберите <gui>Параметры системы</gui>. Откройте <gui>Настройка экранов</gui>.</p>
  </item>

  <item>
   <p>Снимите флажок <gui>Зеркально отразить экраны</gui>.</p>
  </item>

  <item>
   <p>Выберите по очереди каждый экран на сером поле в верхней части окна <gui>Настройка экранов</gui>. Изменяйте <gui>Разрешение</gui>, пока экран не станет выглядеть правильно.</p>
  </item>
 </steps>

</section>

</page>
