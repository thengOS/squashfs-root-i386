<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="net-wireless-troubleshooting-device-drivers" xml:lang="ru">
  <info>
    <link type="guide" xref="net-wireless-troubleshooting"/>
    <revision pkgversion="3.4.0" date="2012-03-05" status="outdated"/>
    <revision version="16.04" date="2016-03-14" status="outdated"/>

    <credit type="author">
      <name>Участники проекта вики-документации Ubuntu</name>
    </credit>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>

    <desc>Найдите лучший драйвер, так как некоторые драйверы работают не очень хорошо с определёнными беспроводными адаптерами.</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Диагностика беспроводных сетей</title>
<subtitle>Убедитесь, что установлены работающие драйверы</subtitle>

<!-- Needs links (see below) -->

<p>На этом этапе можно выяснить, возможно ли получить работающие драйверы для беспроводного адаптера. <em>Драйвер устройства</em> — это небольшая программа, сообщающая компьютеру, как должно работать аппаратное устройство. Даже если беспроводной адаптер и распознаётся компьютером, у адаптера может и не быть правильно работающих драйверов. Возможно, удастся подыскать другие, работающие драйверы для беспроводного адаптера. Попробуйте некоторые из следующих вариантов:</p>

<list>
 <item>
  <p>Выясните, есть ли ваш беспроводной адаптер в списке поддерживаемых устройств</p>
  <p>У большинства Linux-дистрибутивов есть список поддерживаемых беспроводных устройств. Иногда эти списки содержат дополнительную информацию о том, как заставить драйверы для определённых адаптеров работать правильно. Перейдите к списку вашего дистрибутива (например, <link href="https://help.ubuntu.com/community/WifiDocs/WirelessCardsSupported">Ubuntu</link>, <link href="http://linuxwireless.org/en/users/Drivers">Fedora</link> или <link href="http://en.opensuse.org/HCL:Network_(Wireless)">openSuSE</link>) и посмотрите, есть ли там ваша марка и модель беспроводного адаптера. Возможно, удастся воспользоваться полученной информацией и заставить работать ваши драйверы для беспроводного устройства.</p>
 </item>
 
 <item>
  <p>Поищите дополнительные открытые или проприетарные драйверы</p>
  <p>Несмотря на то что Ubuntu включает в себя поддержку большого количества устройств, некоторые драйверы должны быть установлены отдельно. Используйте инструмент <gui>Драйверы устройств</gui> для проверки на такие дополнительные, открытые или <link xref="hardware-driver-proprietary">проприетарные</link>, драйверы.</p>

  <steps>
    <item>
      <p>Щёлкните кнопку в самой правой части панели меню и выберите <gui>Параметры системы</gui>.</p></item>
    <item><p>В разделе «Система» выберите <gui>Программы и обновления</gui>.</p></item>
    <item><p>Перейдите на вкладку <gui>Дополнительные драйверы</gui>.</p></item>
  </steps>

 </item>
 
 <item>
  <p>Используйте драйверы Windows для вашего адаптера</p>
  <p>Обычно нельзя использовать драйвер устройства, разработанный для одной операционной системы (например, для Windows), в другой операционной системе (например, в Linux). Это происходит из-за того, что эти операционные системы по-разному взаимодействуют с устройствами. Несмотря на это, для беспроводных адаптеров можно установить режим совместимости, называемый <em>NDISwrapper</em>, который позволит использовать некоторые Windows-драйверы беспроводных устройств в Linux. Это удобно, ведь драйверы беспроводных адаптеров для Windows есть почти всегда, в то время как драйверы для Linux иногда недоступны. Узнайте больше об использовании NDISwrapper <link href="http://sourceforge.net/apps/mediawiki/ndiswrapper/index.php?title=Main_Page">здесь</link>. Заметьте, не все драйверы беспроводных устройств можно использовать в NDISwrapper.</p>
  <p>Полная информация о ndiswrapper расположена на <link href="https://help.ubuntu.com/community/WifiDocs/Driver/Ndiswrapper">этой странице</link>, включая справку по устранению неисправностей, специфических для ndiswrapper.</p>

 </item>
</list>

<!-- 
  <p>
    Your device may already be supported with a preinstalled driver. To check if a driver was
    automatically loaded, run <cmd>sudo lshw -C network</cmd>. Look in the line that
    begins with <gui>configuration:</gui> for the word <em>driver</em>. If this exists, a
    driver has already been installed and loaded.
  </p>
  <p>
    If you are not running the most recent kernel, update your system first. Drivers,
    especially wireless, are constantly being added and modified. You can do this by clicking
    on the <link xref="unity-menubar-intro">system menu</link> and selecting <gui>System
    Settings</gui>. In the System section, click <gui>Update Manager</gui>.
  </p>



<section id="net-wireless-driver-check2">
  <title>Check Driver</title>
  <p>
      If you ran <cmd>lshw -C network</cmd> and saw a driver bound to the device then let's test to make sure it's communicating with the kernel. You can also go back to the
      <link xref="net-wireless-troubleshooting-initial-check">Initial Check page</link>
      to check for any signs of connectivity.
  </p>
  <list type="numbered">
    <item>
      <p>
        Run <cmd>sudo lsmod</cmd> to see if the driver is loaded. Look for the driver name
        that was listed in the "configuration" line output of lshw.
      </p>
      <list>
        <item>
          <p>
            If you did not see the driver module in the list then use <cmd>sudo modprobe</cmd> to load it.
          </p>
        </item>
      </list>
    </item>
    <item>
      <p>
        Run <cmd>sudo iwconfig</cmd>. If you see output like in the example in the command section then the driver is at least identifying the device as a wireless device to the kernel.
      </p>
    </item>
    <item>
      <p>
          Run the command <cmd>sudo iwlist scan</cmd> to scan for a wireless access point. If
          an access point is identified, the card is probably working properly as it can
          complete a wireless interface task.
      </p>
    </item>
  </list>

  <note>
    <p>
      To find more information about all the commands used in this guide, click
      <link href="https://help.ubuntu.com/community/WifiDocs/WirelessTroubleShootingGuide/Commands">here</link>.
    </p>
  </note>

</section>

-->

<links type="series"/>
</page>
