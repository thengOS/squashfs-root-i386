<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-2sided" xml:lang="ru">

  <info>
    <link type="guide" xref="printing#paper"/>

    <desc>Печатайте на обеих сторонах бумаги или несколько страниц на листе.</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Двусторонняя печать и размещение нескольких страниц на листе</title>

<p>Чтобы напечатать на обеих сторонах каждого листа бумаги:</p>

<steps>
 <item>
  <p>Щёлкните <guiseq><gui>Файл</gui><gui>Печать</gui></guiseq>.</p>
 </item>
 <item>
  <p>Перейдите на вкладку <gui>Параметры страницы</gui> окна печати и выберите опцию из выпадающего списка <gui>Двухсторонняя</gui>. Если опция недоступна, значит двухсторонняя печать не поддерживается вашим принтером.</p>
  <p>Принтеры могут выполнять двухстороннюю печать разными способами. Желательно поэкспериментировать с вашим принтером, чтобы понять, как работает эта функция.</p>
 </item>
 <item>
  <p>Кроме того, можно печатать более одной страницы документа на <em>одной стороне</em> бумаги. Для этого воспользуйтесь опцией <gui>Страниц на сторону</gui>.</p>
 </item>
</steps>
<note>
  <p>Наличие таких режимов зависит от типа вашего принтера, а также от используемого приложения. Эта опция может быть не всегда доступна.</p>
</note>
</page>
