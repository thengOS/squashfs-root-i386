<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="sharing-bluetooth" xml:lang="ru">
  <info>
    <link type="guide" xref="bluetooth"/>

    <desc>Настройте получение файлов и совместный доступ через Bluetooth.</desc>

    <revision version="14.10" date="2014-09-14" status="review"/>

    <credit type="author">
      <name>Команда документации Ubuntu</name>
      <email>ubuntu-doc@lists.ubuntu.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>Управление совместным доступом через Bluetooth</title>

  <p>Можно разрешить совместный доступ к папкам <file>Общедоступные</file> и <file>Загрузки</file> через Bluetooth, а также ограничить такой доступ только <em>доверенными устройствами</em>. Настройте <gui>Общий доступ к личным файлам</gui> для управления доступом к общим папкам на вашем компьютере.</p>

  <note style="info">
    <p>Устройство Bluetooth считается <em>доверенным</em>, если оно <em>сопряжено</em>, или ваш компьютер подключён к нему. См. <link xref="bluetooth-connect-device"/>.</p>
  </note>

  <steps>
    <title>Предоставление совместного доступа через Bluetooth к вашей папке <file>Общедоступные</file></title>
    <item>
      <p>Откройте в <gui>главном меню</gui> <app>Общий доступ к личным файлам</app>.</p>
    </item>
    <item>
      <p>Выберите предпочтительные настройки отправки и приема файлов по Bluetooth из списка.</p>
    </item>
  </steps>

</page>
