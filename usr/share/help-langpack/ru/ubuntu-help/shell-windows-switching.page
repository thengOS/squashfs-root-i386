<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="shell-windows-switching" xml:lang="ru">

  <info>

    <link type="guide" xref="shell-windows#working-with-windows"/>
    <link type="guide" xref="shell-overview#apps"/>

    <desc>Нажмите <keyseq><key>Alt</key><key>Tab</key></keyseq>.</desc>
    <revision pkgversion="3.4.0" date="2012-02-19" status="review"/>
    <revision version="13.10" date="2013-10-23" status="outdated"/>
    <credit type="author">
      <name>Проект документирования GNOME</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Проект документации Ubuntu</name>
      <email>ubuntu-doc@lists.ubuntu.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Переключение между окнами</title>

<section id="launcher">
  <title>Из панели запуска</title>
  <steps>
    <item><p>Откройте <gui>панель запуска</gui>, переместив указатель мыши в верхний левый угол экрана.</p></item>
    <item><p>Значки работающих приложений имеют маленькую белую треугольную стрелку слева. Щёлкните на значке работающего приложения, чтобы переключиться в него.</p></item>
    <item><p>Если в запущенном приложении открыто несколько окон, слева от значка будет несколько белых стрелок. Нажмите значок приложения повторно, чтобы показать все открытые окна в уменьшенном масштабе. Щёлкните на окно, в которое хотите переключиться.</p></item>
  </steps>
</section>

<section id="keyboard">
  <title>С помощью клавиатуры</title>
<list>
  <item><p>Нажмите <keyseq><key>Alt</key><key>Tab</key></keyseq> для вызова <gui>переключателя окон</gui>.</p>
<list>
  <item><p>Отпустите <key>Alt</key>, чтобы выбрать следующее (выделенное подсветкой) окно в переключателе.</p></item>
  <item><p>Или, продолжая удерживать клавишу <key>Alt</key>, нажимайте <key>Tab</key> для циклического перемещения по списку открытых окон. Для перемещения в обратном порядке нажимайте <keyseq><key>Shift</key><key>Tab</key></keyseq>.</p>

<note style="tip">
  <p>Окна в переключателе окон сгруппированы по приложениям. Если переместиться на значок приложения с несколькими окнами, то всплывут уменьшенные изображения этих окон.</p>
</note></item>

  <item><p>Перемещаться между значками приложений в переключателе окон можно с помощью клавиш <key>→</key> или <key>←</key>, а также выбрать какой-нибудь из них щелчком мыши.</p></item>
  <item><p>Отобразить уменьшенное изображение единственного окна приложения можно с помощью клавиши <key>↓</key>.</p>

<note>
  <p>Будут показаны окна только текущего <link xref="shell-workspaces">рабочего места</link>. Чтобы показать окна всех рабочих мест, нажмите и удерживайте клавиши <key>Ctrl</key> и <key>Alt</key> и нажмите <key>Tab</key> или <keyseq><key>Shift</key><key>Tab</key></keyseq>.</p>
</note></item>
</list></item>
</list>

<list>
<item>
  <p>Нажмите <keyseq><key><link xref="windows-key">Super</link></key><key>W</key></keyseq>, чтобы увидеть уменьшенные изображения всех открытых окон.</p>
    <list>
      <item><p>Щёлкните на окно, в которое хотите переключиться.</p></item>
    </list>
</item>

</list>


</section>

</page>
