<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="unity-menubar-intro" xml:lang="ru">

  <info>
    <link type="guide" xref="index" group="unity-menubar-intro"/>
    <link type="guide" xref="shell-overview#desktop"/>

    <desc>Панель меню — это тёмная полоска наверху вашего экрана.</desc>

    <revision version="14.04" date="2014-03-05" status="review"/>

    <credit type="author">
      <name>Команда документации Ubuntu</name>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>
  <title>Управление приложениями и настройками с помощью панели меню</title>

  <p><gui>Панель меню</gui> — это тёмная полоска наверху экрана. Она содержит кнопки управления окнами, меню приложения и меню состояния.</p>

<section id="window-management">
  <title>Кнопки управления окном</title>
  <p><gui>Кнопки управления окном</gui> находятся в верхнем левом углу окна. Когда окно развёрнуто на весь экран, кнопки располагаются в верхнем левом углу экрана. Используйте эти кнопки для закрытия, сворачивания, разворачивания и восстановления размера окна.</p>
</section>

<section id="app-menus">
  <title>Меню приложения</title>
  <p><gui>Меню приложения</gui> находится справа от кнопок управления окном. Unity скрывает меню приложения и кнопки управления окном до тех пор, пока не переместить указатель мыши в верхний левый угол экрана или не нажать <keyseq><key>Alt</key><key>F10</key></keyseq>. Это освобождает дополнительное рабочее пространство для просмотра содержимого, что особенно важно на небольших экранах, таких как экраны нетбуков.</p>

  <p>При желании можно изменить стандартное место расположения меню приложений, чтобы они отображались не на панели меню, а в заголовке окна приложения, а также сделать так, чтобы меню было видимым постоянно, а не только при наведении указателя мыши.</p>

  <steps>
    <item>
      <p>Откройте <gui>системное меню</gui> с правой стороны <gui>панели меню</gui> и выберите <gui>Параметры системы</gui>.</p>
    </item>
    <item>
      <p>В разделе Персональные щёлкните <gui>Оформление</gui> и выберите вкладку <gui>Режим</gui>.</p>
    </item>
    <item>
      <p>Под надписью <gui>Показывать меню для окна</gui> выберите <gui>В заголовке окна</gui>.</p>
    </item>
    <item>
      <p>В секции <gui>Показ меню</gui> выберите <gui>Отображается всегда</gui>.</p>
    </item>
  </steps>

</section>


<section id="status-menus">
  <title>Меню состояния</title>
  <p>В Ubuntu есть несколько различных <em>меню состояния</em> (иногда называемых <gui>индикаторами</gui>), расположенных в правой части панели меню. Меню состояния — это удобное место, где можно проверять и изменять состояние вашего компьютера и приложений.</p>


  <list ui:expanded="false">
  <title>Список меню состояния и их назначение</title>

    <item>
      <p><em>Меню сети</em> <media type="image" mime="image/svg" src="figures/network-offline.svg">Значок отключенного сетевого соединения</media></p>
      <p>Подключение к <link xref="net-wired-connect">проводным</link>, <link xref="net-wireless-connect">беспроводным</link>, <link xref="net-mobile">мобильным</link> и <link xref="net-vpn-connect">VPN</link> сетям.</p>
    </item>

    <item>
      <p><em>Меню источников ввода</em> <media type="image" mime="image/svg" src="figures/indicator-keyboard-En.svg">Значок источника ввода</media></p>
      <p>Выбор раскладки клавиатуры или источника ввода, <link xref="keyboard-layouts">настройка источников ввода</link>.</p>
    </item>

    <item>
      <p><em>Меню Bluetooth</em> <media type="image" mime="image/svg" src="figures/bluetooth-active.svg">Значок Bluetooth</media></p>
      <p>Отправка или получение файлов с помощью <link xref="bluetooth">Bluetooth</link>. Это меню скрыто, если поддерживаемое устройство Bluetooth не обнаружено.</p>
    </item>

    <item>
      <p><em>Меню сообщений</em> <media type="image" mime="image/svg" src="figures/indicator-messages.svg">Значок письма</media></p>
      <p>Быстрый запуск и получение входящих уведомлений от приложений для работы с электронной почтой, социальными сетями и интернет-чатами.</p>
    </item>

    <item>
      <p><em>Меню источника питания</em> <media type="image" mime="image/svg" src="figures/battery-100.svg">Значок аккумулятора</media></p>
      <p>Проверка состояния заряда аккумулятора вашего ноутбука. Это меню скрыто, если аккумулятор не обнаружен.</p>
    </item>

    <item>
      <p><em>Звуковое меню</em> <media type="image" mime="image/svg" src="figures/audio-volume-high-panel.svg">Значок громкости</media></p>
      <p>Настройка <link xref="sound-volume">громкости</link>, изменение <link xref="media">настроек</link> звука и управление медиапроигрывателями типа <app>Rhythmbox</app>.</p>
    </item>

    <item>
      <p><em>Часы</em></p>
      <p>Отображение текущего времени и даты. Встречи, назначенные в приложении<link xref="clock-calendar">Календарь</link> также могут отображаться здесь.</p>
    </item>

    <item>
      <p><em>Системное меню</em> <media type="image" mime="image/svg" src="figures/system-devices-panel.svg">Значок в виде шестерни</media></p>
      <p>Доступ к информации о вашем компьютере, к этому справочному руководству и к <link xref="prefs">параметрам системы</link>. Позволяет переключить пользователей, заблокировать экран, завершить сеанс, перейти в ждущий режим, перезагрузить или выключить компьютер.</p>
      <note><p>Некоторые используемые в индикаторах меню значки изменяются в соответствии с состоянием приложения.</p></note>
      <p>Другие приложения, например такие, как <app>Tomboy</app> или <app>Transmission</app>, тоже могут добавлять индикаторы меню на панель.</p>
    </item>

  </list>

</section>
</page>
