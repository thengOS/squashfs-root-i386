<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" version="1.0 if/1.0" id="bluetooth-remove-connection" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="bluetooth"/>

    <revision pkgversion="3.4" date="2012-02-19" status="outdated"/>
    <revision pkgversion="3.10" date="2013-11-07" status="review"/>
    <revision pkgversion="3.12" date="2014-03-04" status="candidate"/>
    <revision pkgversion="3.13" date="2014-09-22" status="candidate"/>
    <revision pkgversion="3.18" date="2015-09-28" status="final"/>

    <credit type="author">
      <name>Jim Campbell</name>
      <email its:translate="no">jwcampbell@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Paul W. Frields</name>
      <email its:translate="no">stickster@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
      <years>2014</years>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
      <years>2015</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>从蓝牙设备列表中，删除一个设备。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>Disconnect a Bluetooth device</title>

  <p>If you do not want to be connected to a Bluetooth device anymore, you can
  remove the connection. This is useful if you no longer want to use a device
  like a mouse or headset, or if you no longer wish to transfer files to or
  from a device.</p>

  <steps>
    <item>
    <if:choose>
      <if:when test="platform:unity">
        <p>单击<gui>菜单栏</gui>上的蓝牙图标，选择<gui>蓝牙设置</gui>。</p>
      </if:when>
      <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
      start typing <gui>Bluetooth</gui>.</p>
    </if:choose>
    </item>
    <item>
    <if:choose>
      <if:when test="platform:unity">
        <p>Select <gui>Bluetooth Settings</gui> to open the panel.</p>
      </if:when>
      <p>Click on <gui>Bluetooth</gui> to open the panel.</p>
    </if:choose>
    </item>
    <item>
      <p>Select the device which you want to disconnect from the list.</p>
    </item>
    <item>
      <p>In the device dialog box, switch <gui>Connection</gui> to
      <gui>OFF</gui>, or to remove the device from the <gui>Devices</gui> list,
      click <gui>Remove Device</gui>.</p>
    </item>
  </steps>

  <p>您还可以在稍后<link xref="bluetooth-connect-device">重新连接一个蓝牙设备</link>。</p>

</page>
