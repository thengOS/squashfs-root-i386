<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="files-autorun" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="media#photos"/>
    <link type="guide" xref="media#videos"/>
    <link type="guide" xref="media#music"/>
    <link type="guide" xref="files#removable"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision pkgversion="3.9.92" date="2013-10-04" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-28" status="candidate"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email its:translate="no">gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Shobha Tyagi</name>
      <email its:translate="no">tyagishobha@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>插入 CD、DVD、数码相机、音乐播放器和其他设备和媒体时，自动运行相关应用程序。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <!-- TODO: fix bad UI strings, then update help -->
  <title>当您插入某个设备时自动打开应用程序</title>

  <p>当您插入某个设备、光盘或媒体卡时，您可以自动启动特定的应用程序。例如，当您插入数码相机时，您可能希望启动您的照片管理器。您也可以将其关闭，这样在您插入对应设备时系统将不自动进行任何操作。</p>

  <p>接入不同设备时，选择用哪一个程序打开它：</p>

<steps>
  <item>
    <p>Open the <gui xref="shell-introduction#activities">Activities</gui> overview and
    start typing <gui>Details</gui>.</p>
  </item>
  <item>
    <p>Click on <gui>Details</gui> to open the panel.</p>
  </item>
  <item>
    <p>Click <gui>Removable Media</gui>.</p>
  </item>
  <item>
    <p>找到您想要设置的设备或媒体类型，然后为它选择一个应用程序或操作。不同的设备和媒体类型，请参阅下面的描述。</p>
    <p>Instead of starting an application, you can also set it so that the
    device will be shown in the file manager, with the <gui>Open folder</gui>
    option. When that happens, you will be asked what to do, or nothing will
    happen automatically.</p>
  </item>
  <item>
    <p>If you do not see the device or media type that you want to change in
    the list (such as Blu-ray discs or E-book readers), click <gui>Other
    Media…</gui> to see a more detailed list of devices. Select the type of
    device or media from the <gui>Type</gui> drop-down and the application or
    action from the <gui>Action</gui> drop-down.</p>
  </item>
</steps>

  <note style="tip">
    <p>If you do not want any applications to be opened automatically, whatever
    you plug in, select <gui>Never prompt or start programs on media
    insertion</gui> at the bottom of the <gui>Details</gui> window.</p>
  </note>

<section id="files-types-of-devices">
  <title>设备或媒体类型</title>
<terms>
  <item>
    <title>音乐光碟</title>
    <p>Choose your favorite music application or CD audio extractor to handle
    audio CDs. If you use audio DVDs (DVD-A), select how to open them under
    <gui>Other Media…</gui>. If you open an audio disc with the file manager,
    the tracks will appear as WAV files that you can play in any audio player
    application.</p>
  </item>
  <item>
    <title>视频光碟</title>
    <p>Choose your favorite video application to handle video DVDs. Use the
    <gui>Other Media…</gui> button to set an application for Blu-ray, HD DVD,
    video CD (VCD), and super video CD (SVCD). If DVDs or other video discs
    do not work correctly when you insert them, see <link xref="video-dvd"/>.
    </p>
  </item>
  <item>
    <title>空白光盘</title>
    <p>Use the <gui>Other Media…</gui> button to select a disc-writing
    application for blank CDs, blank DVDs, blank Blu-ray discs, and blank HD
    DVDs.</p>
  </item>
  <item>
    <title>相机和照片</title>
    <p>点击<gui>照片</gui>下拉列表，选择一个照片处理器程序与之关联，当您插入一个数码相机或者插入一个相机媒体卡，如 CF 卡、SD 卡、MMC 或 MS 卡，会启动这个应用程序，您可以设为仅在文件管理器中查看。</p>
    <p>Under <gui>Other Media…</gui>, you can select an application to open
    Kodak picture CDs, such as those you might have made in a store. These are
    regular data CDs with JPEG images in a folder called
    <file>Pictures</file>.</p>
  </item>
  <item>
    <title>音乐播放器</title>
    <p>选择一个管理您的音乐媒体库的应用程序，比如您的移动音乐播放器或者文件管理器中的音乐文件。</p>
    </item>
    <item>
      <title>电子书阅读器</title>
      <p>Use the <gui>Other Media…</gui> button to choose an application to
      manage the books on your e-book reader, or manage the files yourself
      using the file manager.</p>
    </item>
    <item>
      <title>软件</title>
      <p>一些光碟或可移动媒体中，包含插入媒体时支持的自动运行程序，点击<gui>软件</gui>选项来设置插入一个带自动运行的媒体时如何操作。您可以设置程序在运行前总是询问。</p>
      <note style="warning">
        <p>不要运行不可信媒体上的程序。</p>
      </note>
   </item>
</terms>

</section>

</page>
