<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="files-rename" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="files#common-file-tasks"/>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="review"/>
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>David King</name>
      <email>amigadave@amigadave.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>更改文件或文件夹名称。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

  <title>重命名文件或文件夹</title>

  <p>As with other file managers, you can use <app>Files</app> to change the
  name of a file or folder.</p>

  <steps>
    <title>重命名文件或文件夹</title>
    <item><p>右击文件或文件夹，然后选择<gui>重命名</gui>，或选择文件，然后按 <key>F2</key> 键。</p></item>
    <item><p>Type the new name and press <key>Enter</key> or click
    <gui>Rename</gui>.</p></item>
  </steps>

  <p>您还可以从<link xref="nautilus-file-properties-basic">属性</link>窗口重命名文件。</p>

  <p>When you rename a file, only the first part of the name of the file is
  selected, not the file extension (the part after the last <file>.</file>).
  The extension normally denotes what type of file it is (for example,
  <file>file.pdf</file> is a PDF document), and you usually do not want to
  change that. If you need to change the extension as well, select the entire
  file name and change it.</p>

  <note style="tip">
    <p>If you renamed the wrong file, or named your file improperly, you can
    undo the rename. To revert the action, immediately click the menu button in
    the toolbar and select <gui>Undo Rename</gui>, or press
    <keyseq><key>Ctrl</key><key>Z</key></keyseq>, to restore the former
    name.</p>
  </note>

  <section id="valid-chars">
    <title>有效的文件名字符</title>

    <p>You can use any character except the <file>/</file> (slash) character in
    file names. Some devices, however, use a <em>file system</em> that has more
    restrictions on file names. Therefore, it is a best practice to avoid the
    following characters in your file names: <file>|</file>, <file>\</file>,
    <file>?</file>, <file>*</file>, <file>&lt;</file>, <file>"</file>,
    <file>:</file>, <file>&gt;</file>, <file>/</file>.</p>

    <note style="warning">
    <p>If you name a file with a <file>.</file> as the first character, the
    file will be <link xref="files-hidden">hidden</link> when you attempt to
    view it in the file manager.</p>
    </note>

  </section>

  <section id="common-probs">
    <title>常见问题</title>

    <terms>
      <item>
        <title>该名称已被使用</title>
        <p>You cannot have two files or folders with the same name in the same
        folder. If you try to rename a file to a name that already exists in
        the folder you are working in, the file manager will not allow it.</p>
        <p>文件和文件夹名称区分大小写。例如，<file>File.txt</file> 和 <file>file.txt</file> 是两个不同的名称。需要注意的是这种做法可能会造成您日后识别困难，因此并不推荐使用。</p>
      </item>
      <item>
        <title>文件名称过长</title>
        <p>On some file systems, file names can have no more than 255
        characters in their names.  This 255 character limit includes both the
        file name and the path to the file (for example,
        <file>/home/wanda/Documents/work/business-proposals/…</file>), so you
        should avoid long file and folder names where possible.</p>
      </item>
      <item>
        <title>重命名的选项是灰色的</title>
        <p>如果<gui>重命名</gui>变灰，您将无权重命名文件。一般，如果您没有重命名文件的正确权限，您就不应重命名文件。请参阅<link xref="nautilus-file-properties-permissions"/>。</p>
      </item>
    </terms>

  </section>

</page>
