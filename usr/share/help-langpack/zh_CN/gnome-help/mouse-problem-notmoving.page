<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="mouse-problem-notmoving" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="mouse#problems"/>

    <revision pkgversion="3.8" date="2013-03-13" status="candidate"/>
    <!-- TODO: reorganise page and tidy because it's one ugly wall of text -->
    <revision pkgversion="3.18" date="2015-09-29" status="final"/>

    <credit type="author">
        <name>Phil Bull</name>
        <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>How to checki why your mouse is not working.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>鼠标指针不移动</title>

<links type="section"/>

<section id="plugged-in">
 <title>检查鼠标是否连接好</title>
 <p>如果使用有线鼠标，检查它是否牢固地插入到计算机上。</p>
 <p>如果是 USB 鼠标(有一个长方形的接口)，试着接入其他 USB 端口上，如果是 PS/2 鼠标(有一个小的圆形接口，里面有6针)，确保插入的是绿色插孔而不是紫色的键盘接口。如果重新插入，您可能需要重启计算机。</p>
</section>

<section id="broken">
 <title>检查鼠标是否正常工作</title>
 <p>把鼠标接到其他计算机上，看看是否正常工作。</p>

 <p>如果是光电鼠标，鼠标底部应当有一个灯闪亮，如果没有光线，检查开关是否开启。如果还是没有光线，那么鼠标可能损坏了。</p>
</section>

<section id="wireless-mice">
 <title>检查无线鼠标</title>

  <list>
    <item>
      <p>Make sure the mouse is turned on. There is often a switch on the
      bottom of the mouse to turn the mouse off completely, so you can take it
      with you without it constantly waking up.</p>
    </item>
   <item><p>如果您使用蓝牙鼠标，请确保您已经将鼠标与电脑正确配对。参阅<link xref="bluetooth-connect-device"/>。</p></item>
  <item>
   <p>单击鼠标键，看鼠标指针现在是否移动。某些无线鼠标会为省电而进入休眠模式，因此可能单击鼠标键后才有响应。</p>
  </item>
  <item>
   <p>检查鼠标的电池是否有电。</p>
  </item>
  <item>
   <p>确保接收器(dongle)窂固地接入计算机上。</p>
  </item>
  <item>
   <p>如果您的鼠标和接收器可以使用其他波段，确保它们是在同一频道上。</p>
  </item>
  <item>
   <p>您可能要按一下鼠标和/或接收器上的某个键来建立连接。鼠标使用说明上应该有更详细的说明。</p>
  </item>
 </list>

 <p>绝大多数 RF(无线电)无线鼠标在接入计算机后，能够自动识别并工作。如果您有蓝牙或 IR(红外线)无线鼠标，您可能需要执行一些额外的步骤才能正常工作。这些步骤取决于您的鼠标类型。</p>
</section>

</page>
