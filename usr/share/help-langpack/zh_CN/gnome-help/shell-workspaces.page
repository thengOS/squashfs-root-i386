<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:if="http://projectmallard.org/if/1.0/" type="topic" style="ui" version="1.0 if/1.0" id="shell-workspaces" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="shell-windows#working-with-workspaces" group="#first"/>

    <revision pkgversion="3.8.0" date="2013-04-23" status="review"/>
    <revision pkgversion="3.10.3" date="2014-01-26" status="review"/>
    <revision pkgversion="3.13.92" date="2014-09-22" status="candidate"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>工作区可以用来组织您桌面上的窗口。</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>TeliuTe</mal:name>
      <mal:email>teliute@163.com</mal:email>
      <mal:years>2011, 2012</mal:years>
    </mal:credit>
  </info>

<title>什么是工作区，它有什么用处？</title>

    <media type="image" src="figures/shell-workspaces.png" width="162" height="434" style="floatend floatright">
        <p>工作区选择器</p>
    </media>

  <p if:test="!platform:gnome-classic">Workspaces refer to the grouping of
  windows on your desktop. You can create multiple workspaces, which act like
  virtual desktops. Workspaces are meant to reduce clutter and make the desktop
  easier to navigate.</p>

  <p if:test="platform:gnome-classic">Workspaces refer to the grouping of
  windows on your desktop. You can use multiple workspaces, which act like
  virtual desktops. Workspaces are meant to reduce clutter and make the desktop
  easier to navigate.</p>

  <p>您可以使用工作区来组织您的工作。例如，您可使所有的通信窗口(如电子邮件和您的聊天程序)位于一个工作区，并且使您正在进行的工作位于另一个工作区，您的音乐管理器可位于第三个工作区。</p>

<p>使用工作区</p>

<list>
  <item>
    <p if:test="!platform:gnome-classic">In the
    <gui xref="shell-introduction#activities">Activities</gui> overview, move your cursor
    to the right-most side of the screen.</p>
    <p if:test="platform:gnome-classic">Press the
    <key xref="keyboard-key-super">Super</key> key to open the
    <gui>Activities</gui> overview and then move your cursor to the right-most
    side of the screen.</p>
  </item>
  <item>
    <p if:test="!platform:gnome-classic">A vertical panel will appear showing
    workspaces in use, plus an empty workspace. This is the
    workspace selector.</p>
    <p if:test="platform:gnome-classic">A vertical panel will appear showing
    available workspaces. This is the workspace selector.</p>
  </item>
  <item if:test="!platform:gnome-classic">
    <p>To add a workspace, drag and drop a window from an existing workspace
    onto the empty workspace in the workspace selector. This workspace now
    contains the window you have dropped,
    and a new empty workspace will appear below it.</p>
  </item>
  <item if:test="!platform:gnome-classic">
    <p>To remove a workspace, simply close all of its windows or move them to
    other workspaces.</p>
  </item>
</list>

<p if:test="!platform:gnome-classic">系统会保留至少一个工作区。</p>

</page>
