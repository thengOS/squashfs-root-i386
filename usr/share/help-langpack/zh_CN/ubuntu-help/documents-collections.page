<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="documents-collections" xml:lang="zh-CN">

  <info>
    <credit type="author copyright">
      <name>Julita Inca</name>
      <email>yrazes@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
      <years>2012</years>
    </credit>
    <desc>组织相关集合中的文档。</desc>
    <link type="guide" xref="documents#print"/>
    <revision pkgversion="3.5.90" date="2012-09-05" status="review"/>
    <revision version="13.10" date="2013-10-24" status="review"/>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>文档集合</title>

  <p><app>文档</app>让您将不同类型的文档放在一起叫做<em>集合</em>。如果您有相关联的文档，可以将他们分组，让他们更容易找到。例如，您有一个商务旅行，做了一个演讲，您的幻灯片、飞行旅程( PDF 文件)，预算电子表格，和其他混合 PDF/ODF 文档，可以划分为一个集合。</p>

  <p>创建或添加到集合：</p>
  <list>
   <item><p>点击<gui>✓</gui> 按钮。</p></item>
   <item><p>在选择模式下，选中要收集的文档。</p></item>
   <item><p>点击按钮栏上的<gui> + </gui>按钮。</p></item>
   <item><p>在集合列表中，点击<gui>添加</gui>，键入新的集合名称，或选择现有集合。选定的文档将被添加到集合中。</p></item>
  </list>

  <note>
   <p>集合与文件夹不同，没有层次结构：<em>集合中不能包含集合。</em></p>
  </note>

  <p>删除集合：</p>
  <list>
   <item><p>点击<gui>✓</gui> 按钮。</p></item>
   <item><p>在选择模式下，选中要删除的集合。</p></item>
   <item><p>点击按钮栏上的回收站按钮。集合将被删除，保留原始文档。</p></item>
  </list>

</page>
