<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:ui="http://projectmallard.org/ui/1.0/" type="topic" style="task" version="1.0 ui/1.0" id="files-copy" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="files"/>
    <desc>复制或移动项目至新目录。</desc>

    <revision pkgversion="3.5.92" version="0.2" date="2012-09-15" status="review"/>
    <revision version="14.10" date="2014-09-14" status="review"/>

    <credit type="author">
      <name>Cristopher Thomas</name>
      <email>crisnoh@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>复制或移动文件和文件夹</title>

 <p>文件或文件夹可以通过鼠标拖拽将其复制或移动到新的位置，或者使用复制和粘贴命令，或者使用快捷键。</p>

 <p>例如，您可能需要把演示文稿复制一份放进 U 盘以便随身携带和修改。您也可能需要在修改文档之前先复制一份。</p>

 <p>这些做法对文件和文件夹都有效，您可以用同样的方式移动和复制文件和文件夹。</p>

<steps ui:expanded="false">
<title>复制和粘贴文件</title>
<item><p>单击以选择您想要复制的项目。</p></item>
<item><p>点击右键，选择 <gui>复制</gui>，或者按<keyseq><key> Ctrl </key><key> C </key></keyseq>键。</p></item>
<item><p>前往您想要存放复制后文件的目录。</p></item>
<item><p>点击右键，选择 <gui>粘贴</gui>，或者按<keyseq><key> Ctrl </key><key> V </key></keyseq>键。这将在源文件夹和其他文件夹上生成这个文件的一个拷贝。</p></item>
</steps>

<steps ui:expanded="false">
<title>通过剪切和粘贴操作来移动文件</title>
<item><p>单击选择您想要移动的项目。</p></item>
<item><p>点击右键，选择<gui>剪切</gui>，或者按<keyseq><key> Ctrl </key><key> X </key></keyseq>键。</p></item>
<item><p>前往您想要存放移动后项目的文件夹。</p></item>
<item><p>点击右键，选择 <gui>粘贴</gui>，或者按<keyseq><key> Ctrl </key><key> V </key></keyseq>键。该文件将从原始文件夹移动到其他文件夹。</p></item>
</steps>

<steps ui:expanded="false">
<title>拖动文件以复制或移动</title>
<item><p><link xref="files-browse">打开文件管理器</link>至包含您想要复制项目的文件夹。</p></item>
<item><p>点击顶部栏的<gui>文件</gui>，选择<gui>新窗口</gui> (或者按<keyseq><key> Ctrl </key><key> N </key></keyseq>键)打开新窗口。在新窗口中，导航到您要移动或复制的文件的文件夹。</p></item>
<item>
 <p>单击并将该项目从一个窗口拖动至另一个。默认情况下，如果目的地在同一设备上(即如果两个文件夹都在计算机的同一硬盘上)，拖动项目将<em>移动</em>该项目。</p>
 <p>例如，如果您将一个文件从 U 盘拖拽到主文件夹中，因为这是从一个设备拖拽到另一个设备，文件将会被复制过去。</p>
 <p>要强制复制文件，请在拖动的同时按住 <key>Ctrl</key> 键。</p>
 </item>
</steps>

<note>
 <p>您不能将文件复制或移动到<em>只读</em>文件夹。有些文件是只读的以防止您更改其内容。您可以通过<link xref="nautilus-file-properties-permissions">更改文件权限</link>来更改文件的只读状态。</p>
</note>

</page>
