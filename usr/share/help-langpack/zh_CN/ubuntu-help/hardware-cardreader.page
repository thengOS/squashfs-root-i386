<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="hardware-cardreader" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="media#photos"/>
    <link type="guide" xref="hardware#problems"/>

    <desc>排除读卡器故障</desc>

    <revision version="13.10" date="2013-09-12" status="review"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>读卡器问题</title>

<p>许多计算机中都有 SD (Secure Digital)、MMC (MultiMediaCard)、SmartMedia、Memory Stick、CompactFlash 读卡器和其他存储介质卡。这些卡应该都可以自动检测到并<link xref="disk-partitions">挂载</link>好。否则请看以下故障排除步骤：</p>

<steps>
<item>
<p>确保卡放置正确，许多卡正确插入后却看似倒置，同时确保卡已牢固的插入卡槽；一些卡，例如 CF 卡，正确插入需要些许用力。(请不要用力过猛！如果在此过程中遇到硬物，请不要强行用力。)</p>
</item>

<item>
<p>使用 <gui>Dash</gui> 打开 <app>文件</app> 管理器，插入的卡出现在左边栏的 <gui>设备</gui> 列表上了吗？有时候卡出现在这个列表中却不被安装，请点击一下挂载它。(如果侧边栏不可见，按下 <key>F9</key> 或者点击 <guiseq><gui>视图</gui><gui> 侧边栏 </gui><gui> 显示侧边栏</gui></guiseq> 。)</p>
</item>

<item>
<p>如果卡未出现在侧边栏中，请依次单击<guiseq><gui>转到</gui><gui>计算机</gui></guiseq>。如果读卡器已经配置妥当，则没有卡时会将读卡器显示为驱动器，卡挂载好后则会显示卡本身(请见下图)。</p>
</item>

<item>
<p>能看到读卡器却不能读取卡中内容，则卡本身可能有问题。请用另一个卡试一试；如有可能，也可在另一读卡器中检查存储卡本身是否有问题。</p>
</item>
</steps>

<p>如果<gui>计算机</gui>文件夹中没有卡或驱动程序，则可能是因为有驱动程序问题而不能将读卡器用于 Linux。如果读卡器是内置读卡器(在计算机内而不是外置)，则这种可能性更大。最有效的解决方法是直接将设备(相机、手机等设备)连接到计算机 USB 端口上。也可使用外置 USB 读卡器，Linux 对这种读卡器的支持要好得多。</p>

</page>
