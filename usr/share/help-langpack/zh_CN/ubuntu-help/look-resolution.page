<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="look-resolution" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="prefs-display"/>
    <link type="guide" xref="hardware-problems-graphics"/>
    <link type="seealso" xref="look-display-fuzzy"/>

    <revision pkgversion="3.8.0" version="0.3" date="2013-03-09" status="candidate"/>
    <revision version="13.10" date="2013-10-22" status="review"/>

    <desc>更改屏幕分辨率和屏幕方向(旋转角度)。</desc>
    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Natalia Ruz Leiva</name>
      <email>nruz@alumnos.inf.utfsm.cl </email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>更改屏幕大小/旋转角度</title>

<p>通过更改<em>屏幕分辨率</em>，可更改内容显示在屏幕上时的大小(或详细程度)。通过更改<em>旋转</em>，可更改显示内容的方向。</p>

<steps>
  <item><p>单击顶部面板最右侧的图标，选择<gui>系统设置</gui>。</p></item>
  <item><p>取消选中<gui>镜像显示</gui>。</p></item>
  <item><p>如果您有多个未工作的显示区，您可以在每个显示区进行不同的设置，在预览区域选择一个显示区。</p></item>
  <item><p>选择您需要的分辨率和屏幕方向</p></item>
  <item><p>单击<gui>应用</gui>将应用新的设置但是会有 30 秒的恢复时间。这样如果您应用了新设置之后出错无法进行操作的话，30 秒后系统将自动恢复旧的设置。如果您对新配置满意，请单击<gui>保持当前配置</gui>。</p></item>
</steps>

<note style="tip">
 <p>使用另一显示屏时(如投影仪)，系统会自动检测到该显示屏以便您更改其设置，其更改方法与普通显示屏相同。如果未自动检测到另一显示屏，只须单击<gui>检测显示器</gui>。</p>
</note>

<section id="resolution">
<title>分辨率</title>
<p>分辨率是指在每个方向上所能显示的像素数量。每个显示器都有一个高宽比。宽屏显示器一般使用 16：9 的高宽比，而普通显示器则一般使用 4：3，如果您选择了不正确的高宽比设置，屏幕将会在适当的边缘加入黑边以避免图像被拉伸变形。</p>
<p>可从下拉菜单中选择最合适的分辨率。请注意，如果所选分辨率不适用于屏幕，则屏幕<link xref="look-display-fuzzy">看起来可能会很模糊或像素化</link>。</p>
</section>

<section id="rotation">
<title>旋转角度</title>
<p>在一些笔记本电脑中，您可以多角度旋转屏幕。能够改变显示旋转是很有用的，您可以从 <gui>旋转</gui> 下拉菜单中选择您想要的显示旋转。</p>
</section>

</page>
