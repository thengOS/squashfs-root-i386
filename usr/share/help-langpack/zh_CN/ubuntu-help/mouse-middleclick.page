<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="mouse-middleclick" xml:lang="zh-CN">

<info>
 <desc>用中键执行打开应用程序、粘贴文本、打开标签页等操作。</desc>

 <link type="guide" xref="tips"/>
 <link type="guide" xref="mouse#tips"/>

 <revision pkgversion="3.8.0" version="0.3" date="2013-03-13" status="candidate"/>
    <revision version="13.10" date="2013-10-22" status="review"/>

 <credit type="author">
  <name>Tiffany Antopolski</name>
  <email>tiffany.antopolski@gmail.com</email>
 </credit>
 <credit type="author">
   <name>Shaun McCance</name>
   <email>shaunm@gnome.org</email>
 </credit>
 <credit type="editor">
   <name>Michael Hill</name>
   <email>mdhillca@gmail.com</email>
 </credit>
 <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
</info>

<title>中键单击</title>

<p>许多鼠标和某些触摸板都有中键。在有滚轮的鼠标上，通常可直接按下滚轮执行中键单击操作。如果鼠标没有中键，则可同时按左右鼠标键执行中键单击操作。</p>

<p>在支持多指点击的触摸板上，可用三个手指同时点击，以执行中键单击操作。必须在触摸板设置中<link xref="mouse-touchpad-click">启用触摸板的鼠标点击</link>，才能使上述操作奏效。</p>

<p>许多应用程序都将中键单击作为一个操作快捷键。</p>

<list>
  <item><p>例如中键单击粘贴所选文本就是一种常见的操作快捷键。(这有时称为首选内容粘贴。)选中要粘贴的文本，然后转到需要进行粘帖操作的位置，最后单击中键。会将所选文本粘贴到鼠标光标所在的位置。</p>
  <p>通过鼠标中键粘贴文本与普通的剪贴板毫无关系。选中文本并不会将其复制到剪贴板中。这种快速粘贴法只能通过鼠标中键执行。</p></item>

  <item><p>使用滚动条和滑块时，在空白区域单击一下将向对应的方向滚动一定的距离(例如向下滚动一页)。您也可以中键单击空白部分来将滚动条或滑块移动到您按下的位置。</p></item>

  <item><p>您可以使用中键单击快速打开一个应用程序的新窗口。在左边的 <gui>启动器</gui> 或者 <gui>Dash</gui> 中，中键单击应用程序的图标即可。</p></item>

  <item><p>大多网络浏览器均可用鼠标中键快速打开链接。只须用鼠标中键单击链接，就会在新标签页中将其打开。但单击 <app>Firefox</app> 网络浏览器中的链接时要小心。在 <app>Firefox</app> 中，如果用中键单击的不是链接，则会尝试以 URL 的形式加载所选文本，效果等同于通过中键单击将其粘贴到位置栏中并按 <key>回车</key>。</p></item>

  <item><p>在文件管理器中，中键单击有两种作用。如果中键单击文件夹，则会在新标签页中将其打开。这是在模仿热门网络浏览器的功能。如果中键单击文件，则会打开文件，如同执行的是双击操作。</p></item>
</list>

<p>在某些专用应用程序可用鼠标中键执行其他功能。请搜索应用程序帮助，以了解 <em>中键单击</em> 或 <em>鼠标中键</em> 的更多功能。</p>
</page>
