<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="ui" id="nautilus-behavior" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="nautilus-prefs" group="nautilus-behavior"/>

    <desc>单击即可打开文件，运行或查看可执行文本文件，以及指定回收站行为。</desc>

    <revision version="13.10" date="2013-09-15" status="review"/>
    <revision pkgversion="3.5.92" version="0.2" date="2012-09-19" status="review"/>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email>tiffany@antopolski.com</email>
    </credit>
    <credit type="author">
      <name>Shaun McCance</name>
      <email>shaunm@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Sindhu S</name>
      <email>sindhus@live.in</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>文件管理器行为首选项</title>
<p>您可以控制是单击还是双击文件、可执行文本文件的处理方式和回收站特性。点击菜单栏的 <gui>文件</gui> ，选中 <gui>参数设置</gui> 并选择 <gui>特性</gui> 标签。</p>

<section id="behavior">
<title>行为</title>
<terms>
 <item>
  <title>单击打开项目</title>
  <title>双击打开项目</title>
  <p>默认情况下，单击可选择文件，双击可打开文件。但可以决定单击文件和文件夹就将其打开。使用单击模式时，可按住 <key>Ctrl</key> 键后再单击，以选择一个或多个文件。</p>
 </item>
</terms>

</section>
<section id="executable">
<title>可执行文本文件</title>
 <p>可执行文本文件是指包含可运行 (执行) 程序的文件。它的 <link xref="nautilus-file-properties-permissions">文件权限</link> 还必须允许这个文件作为一个程序运行。最常见的可执行文本文件有 <sys>Shell</sys> 、 <sys>Python</sys> 和 <sys>Perl</sys> 脚本文件。他们的拓展名分别为 <file>.sh</file> 、 <file>.py</file> 和 <file>.pl</file> 。</p>
 
 <p>当您打开一个可执行文本文件时，您可以选择从：</p>
 
 <list>
  <item>
    <p><gui>当可执行文本文件被打开时运行它们</gui></p>
  </item>
  <item>
    <p><gui>当可执行文本文件被打开时查看它们</gui></p>
  </item>
  <item>
    <p><gui>总是询问</gui></p>
  </item>
 </list>

 <p>如果选择了 <gui>总是询问</gui> ，对话框将在您想要运行或者查看选中的文本文件时弹出询问。</p>

 <p>可执行文本文件也称作脚本文件，所有 <file>~/.local/share/nautilus/scripts</file> 中的脚本文件都将出现在 <gui style="menuitem">脚本</gui> 子菜单下的文件快捷菜单中。当一个脚本从本地文件夹执行时，所有被选中的文件都将作为该脚本的参数被粘贴。在文件中执行脚本：</p>

<steps>
  <item>
    <p>需要的文件夹导航。</p>
  </item>
  <item>
    <p>选择需要的文件。</p>
  </item>
  <item>
    <p>右击文件打开右键菜单，从 <gui style="menuitem">脚本</gui> 选择所需的脚本文件执行。</p>
  </item>
</steps>

 <note style="important">
  <p>当脚本文件从远程文件夹执行时将不会传递任何参数，就像一个显示网页或者 <sys>ftp</sys> 内容的文件夹。</p>
 </note>

</section>
<section id="trash">
<info>
<link type="seealso" xref="files-delete"/>
<title type="link">文件管理器回收站首选项</title>
</info>
<title>回收站</title>

<terms>
 <item>
  <title>清空回收站或删除文件之前询问</title>
  <p>默认情况下已选中此选项。清空回收站时会显示消息，用于确认要清空回收站或删除文件。</p>
 </item>
 <item>
  <title><gui>包含一条跳过回收站的删除命令</gui></title>
  <p>选中此项后，将会在您右击 <app>文件</app> 管理器中的文件弹出的菜单中添加 <gui>删除</gui> 选项。</p>
<note style="warning">
<p>通过<gui>删除</gui>菜单项删除项目会彻底绕过回收站。会将删除的项目从系统中彻底清除。无法恢复已删除的项目。</p>
</note>
 </item>
</terms>
</section>

</page>
