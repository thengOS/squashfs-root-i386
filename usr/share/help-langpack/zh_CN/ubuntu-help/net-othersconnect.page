<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="net-othersconnect" xml:lang="zh-CN">
  <info>
    <link type="guide" xref="net-problem"/>
    <link type="seealso" xref="net-othersedit"/>

    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <desc>您可以保存网络连接的设置信息(例如密码)，这样每个使用这台电脑的人都可以进行联网。</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>其他用户无法连接到 Internet</title>

<p>如果已设置了网络连接，但计算机上的其他用户无法连接到网络，则他们在尝试连接时可能未输入正确的设置。例如，如果您使用无线连接，他们可能未输入正确的无线安全密码。</p>

<p>您可以采用此设置，以便在您设置连接后所有人都可共享网络连接的设置。这意味着您只需要设置一次，使用这台计算机的其他所有用户将能够连接到网络，而系统不会询问他们任何问题。要如此设置，请：</p>

<steps>
 <item>
  <p>单击顶部面板中的<gui>网络菜单</gui>，然后单击<gui>编辑连接</gui>。</p>
 </item>

 <item>
  <p>找到您希望对所有人可用的连接。您可能还需要切换到 <gui>无线</gui> 标签页。选择相应的网络名称然后点击 <gui>编辑</gui>。</p>
 </item>

 <item>
  <p>选中<gui>对所有用户可用</gui>并单击<gui>保存</gui>。您必须输入管理员密码才能保存更改。仅管理员用户可执行此操作。</p>
 </item>

 <item>
  <p>计算机上的其他用户现在无需输入其他任何信息即可使用此连接。</p>
 </item>
</steps>

</page>
