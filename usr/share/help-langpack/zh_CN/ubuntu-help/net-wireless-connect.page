<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="net-wireless-connect" xml:lang="zh-CN">
  <info>
    <link type="guide" xref="net-wireless" group="#first"/>
    <link type="seealso" xref="net-wireless-troubleshooting"/>
    <link type="seealso" xref="net-wireless-noconnection"/>
    <link type="seealso" xref="net-wireless-disconnecting"/>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision version="13.10" date="2013-09-15" status="review"/>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <credit type="author">
      <name>Ubuntu 文档团队</name>
    </credit>

    <desc>以无线方式连接到 Internet。</desc>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>连接到无线网络</title>

<p>如果您的计算机启用了无线，则可在处于无线网线覆盖范围时连接到该网络，以访问 Internet、查看网络上的共享文件以及执行其他操作。</p>

<steps>
  <item>
   <p>如果计算机上有无线硬件开关，请确保打开该开关。</p>
  </item>
  <item>
    <p>单击<gui>顶部面板</gui>中的<gui>网络菜单</gui>，然后查找所需的网络。</p>
   <p>如果您的无线网络名称未出现在列表中，请单击<gui>更多网络</gui>，以查看该网络是否在列表的下方。如果仍未看到该网络，您可能超出了网络覆盖范围或该网络<link xref="net-wireless-hidden">可能已隐藏</link>。</p>
  </item>
  <item><p>如果网络受到(<link xref="net-wireless-wepwpa">密码</link>)保护，请在出现提示时输入密码并单击<gui>连接</gui>。</p>
  <p>如果不知道密钥，它可能写在无线路由器或基站设备的底部、说明手册中，或者您可向无线网络管理员咨询。</p></item>
  <item><p>计算机尝试连接到网络时，网络图标的外观将更改。</p></item>
  <item>
   <p>如果连接成功，该图标将更改为类似 wifi 标志的图标。图标上的信号格数越多，表示与网络的连接越强。如果信号格数很少，则表示连接较弱且可能不太稳定。</p>
  </item>
</steps>

<p>如果连接不成功， <link xref="net-wireless-noconnection">也许会要求您再次输入密码</link> 或者它可能会告诉您连接已断开。有很多事情可能会造成这种情况发生，例如：可能是您输错了密码、无线信号太弱或者您计算机的无线网卡出问题了。查阅 <link xref="net-wireless-troubleshooting"/> 获取更多帮助。</p>

<p>较强的无线网络连接并不一定意味着Internet 连接速度更快，或者您的下载速度更快。无线连接将您的计算机连接到<em>提供 Internet 连接</em>的设备(如路由器或调制解调器)，但设备本身所提供无线信号的强度与网速无关。</p>

</page>
