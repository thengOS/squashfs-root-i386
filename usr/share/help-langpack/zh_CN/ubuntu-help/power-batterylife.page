<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="power-batterylife" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="power"/>
    <link type="seealso" xref="power-suspend"/>
    <link type="seealso" xref="power-hibernate"/>
    <link type="seealso" xref="shell-exit#shutdown"/>
    <revision pkgversion="3.7.1" version="0.2" date="2012-11-16" status="outdated"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <desc>减少您计算机能耗的技巧。</desc>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email>mdhillca@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>降低能耗，提高电池寿命</title>

<p>计算机可使用大量能量。通过使用一些简单的节能措施，您可以节约电费并保护环境。如果您使用的是笔记本电脑，这些措施可以有效提高电池续航时间。</p>

<section id="general">

<title>常规措施</title>
<list>
  <item>
    <p>在不使用计算机时将其<link xref="shell-exit#shut-down">挂起</link>。这样可以大幅降低计算机使用的电量且可以快速唤醒计算机。</p>
  </item>
  <item>
    <p>当您不打算再使用您的计算机时，请 <link xref="shell-exit#shutdown">关闭</link> 它。有人担心定期关闭电脑也许会导致磨损快，但事实并非如此。</p>
  </item>
  <item>
    <p>在 <app>系统设置</app> 中的 <gui>电源</gui> 设置中有多个节能选项：您可以在一段时间后<link xref="display-dimscreen">自动将显示器变暗或使其进入睡眠模式</link>；<link xref="power-brighter">降低笔记本电脑显示器的亮度</link>；如果在一段时间内不使用，则将计算机<link xref="power-suspendhibernate">自动转到睡眠模式</link>。</p>
  </item>
  <item>
    <p>当您不使用外部设备(如打印机和扫描仪)时，请关闭它们。</p>
  </item>
 </list>
</section>

<section id="laptop">
 <title>笔记本、台式机和其他使用电池的设备</title>

 <list>
   <item>
     <p><link xref="power-brighter">降低屏幕的亮度</link>；为屏幕供电占笔记本电脑功耗的很大一部分。</p>
     <p>大多数笔记本电脑的键盘上都有可用于降低亮度的按钮。</p>
   </item>
   <item>
     <p>如果您短时间不需要互联网连接，请关闭无线/蓝牙装置。这些设备需要发射无线电波，这会非常耗电。</p>
     <p>有些计算机上配备了可用于关闭无线适配器和蓝牙设备的物理开关，另外一些计算机上有键盘快捷键替代物理开关供您使用。您可根据需要选择打开或关闭。</p>
   </item>
 </list>
</section>

<section id="advanced">
 <title>更多高级技巧</title>

 <list>
   <item>
     <p>减少后台运行的任务数量。计算机所执行的任务越多则需要更多电能。</p>
     <p>您所运行的大多数程序在最小化状态下能耗很小。但是，经常从网络抓取数据的应用程序、音乐和电影播放器、分布式计算软件等都可能影响耗电量。</p>
   </item>
 </list>
</section>

</page>
