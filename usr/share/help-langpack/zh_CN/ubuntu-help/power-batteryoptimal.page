<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="tip" id="power-batteryoptimal" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="power"/>
    <revision pkgversion="3.4.0" date="2012-02-20" status="review"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <desc>类似 "不要让电池充电过少" 的小贴士。</desc>

    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>更好地利用您的笔记本电池</title>

<p>笔记本电脑电池的续航能力随其使用时间的增加而降低。这里有几个有助于延长其使用寿命的技巧，尽管效果可能不明显。</p>

<list>
  <item>
    <p>不要让电池过度放电，虽然大多数电池都有内置的保护措施防止电量过低，但请在电池电量变得很低 <em>前</em> 充电。电量较低时充电效果更有效，而电量损耗不多时充电对电池是有害的。</p>
  </item>
  <item>
    <p>高温对于电池充电效率会有影响，不要让电池过热。</p>
  </item>
  <item>
    <p>即使只放着，电池也会老化。和原有电池同时购买备用电池没有太大用处 － 只在需要时购买替换用的电池。</p>
  </item>
</list>

<note>
  <p>此建议尤其适用于锂离子电池，这是最常用的电池类型。其他类型电池的情况可能有所不同。</p>
</note>

</page>
