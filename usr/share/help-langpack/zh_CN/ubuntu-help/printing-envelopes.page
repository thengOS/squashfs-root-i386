<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" style="task" id="printing-envelopes" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="printing#paper"/>

    <desc>确保将信封/标签放置正确，并已选择正确的纸张尺寸。</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="candidate"/>
    <revision version="13.10" date="2013-09-19" status="review"/>

    <credit type="author">
      <name>Phil Bull</name>
      <email>philbull@gmail.com</email>
    </credit>
    <credit type="author">
      <name>Jim Campbell</name>
      <email>jwcampbell@gmail.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>打印信封和标签</title>

<p>大多数打印机可以在信封或标签上直接打印内容。在需要邮寄大量信件等情况下，此操作非常有用。</p>

<section id="envelope">
 <title>在信封纸上打印</title>

 <p>在将文字内容打印到信封上之前需要进行两项检查。第一，让打印机了解信封的尺寸。单击<gui>打印</gui>且出现打印窗口后，转至<gui>页面设置</gui>选项并选择<gui>纸张类型</gui>，如果可以，选择“信封”。如不能选择，请将<gui>纸张大小</gui>更改为信封尺寸(例如“C5”)。信封包装上会显示尺寸；大多数信封都是标准尺寸。</p>

 <p>第二，需要确保信封正面朝上置于打印机的纸盒。有关此操作，可查看打印机的使用手册，或尝试打印一个信封，然后根据打印结果查看正确的放置方法。</p>

<note style="warning">
 <p>有些打印机设计不能打印信封，尤其是一些激光打印机。请查看使用手册以了解其是否接受打印信封，否则，放入信封将损坏打印机。</p>
</note>

</section>

<!--
INCOMPLETE: Please write this section!

<section id="labels">
 <title>Printing labels</title>

</section>
-->

</page>
