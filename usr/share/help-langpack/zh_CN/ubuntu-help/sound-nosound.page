<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="problem" id="sound-nosound" xml:lang="zh-CN">

  <info>
    <link type="guide" xref="sound-broken"/>

    <desc>检查确认未静音，电缆插入正确，并可以检测到声卡。</desc>

    <revision pkgversion="3.4.0" date="2012-02-19" status="outdated"/>
    <revision version="13.10" date="2013-10-23" status="review"/>
    <credit type="author">
      <name>GNOME 文档项目</name>
      <email>gnome-doc-list@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>听不到计算机的任何声音</title>

  <p>如果您听不到计算机上的任何声音(例如，当您试图播放音乐时)，请尝试这些故障诊断步骤能否解决您的问题。</p>

<section id="mute">
 <title>确保没有静音</title>
 <p>点击菜单栏中的 <gui>声音菜单</gui>(看上去像一个喇叭)并确保没有静音或调到很小的音量。</p>
 <p>一些笔记本电脑的键盘上有静音开关或按键，尝试按下该键以查看能否取消静音。</p>
 <p>您也应检查确认正在用来播放声音的应用程序(例如，您的音乐播放器或电影播放器)未静音。该应用程序的主窗口中可能有“静音”或“音量”按钮，如果的确有，请对其进行检查。另外，单击菜单栏中的声音按钮并选择<gui>声音设置</gui>。<gui>声音</gui>窗口出现时，进入<gui>应用程序</gui>标签页并检查确认您的应用程序在此处未静音。</p>
</section>

<section id="speakers">
 <title>检查扬声器是否已正确插入并打开。</title>
 <p>如果您的计算机有外部扬声器，则确保扬声器已打开并已调高音量。确保扬声器电缆已稳固插入计算机背面的“输出”音频插槽。此插槽一般为浅绿色。</p>

 <p>有些声卡能够切换输出(至扬声器)和输入(从麦克风)插槽。Linux 上的输出和输入插槽可能与在 Windows 或 Mac OS 上的不同。尝试依次将扬声器电缆连接到计算机上不同的音频插槽以查看插槽是否起作用。</p>

 <p>最后，检查音频电缆已稳固插入扬声器的背面。一些扬声器也不止有一个输入。</p>
</section>

<section id="device">
 <title>检查确认已选择正确的音效设备</title>
 <p>一些计算机安装了多个“声音设备”。其中一些可以进行声音输出而另一些不能，因此您应检查确认已选择正确的声音设备。选择正确的声音设备可能需要反复的实验。</p>
 <steps>
  <item>
   <p>单击<gui>顶部面板</gui>中的<gui>声音菜单</gui>并单击<gui>声音设置</gui>。</p>
  </item>
  <item>
   <p>在弹出的 <gui>声音</gui> 窗口中，在 <gui>使用以下输出播放声音</gui>列表中尝试选择不同的输出。</p>
  </item>
  <item>
   <p>对选中的设备，点击<gui>测试声音</gui>。在弹出的窗口中，点击每一个扬声器。每个按钮将只通过其对应的声道报告其位置。</p>
  </item>
  <item>
   <p>如果不起作用，则您可能需要对列出的其他设备执行相同的操作。</p>
  </item>
 </steps>
</section>

<section id="hardware-detected">
 <title>检查已检测正常声卡</title>
 <p>可能未正常检测您的声卡。如果这样，您的计算机会认为无法播放声音。无法正确检测到声卡的原因可能是因为没有安装声卡驱动程序。</p>
 <steps>
  <item>
   <p>前往 <link xref="unity-dash-intro">Dash</link> 然后打开终端。</p>
  </item>
  <item>
   <p>键入 <cmd>aplay -l</cmd> 并按<key>回车</key>。</p>
  </item>
  <item>
   <p>将显示设备列表。如果没有<gui>播放硬件设备</gui>，意味着不能检测到您的声卡。</p>
  </item>
 </steps>

 <p>如果没有检测到声卡，则您需要手动为其安装驱动程序。您要怎样做取决于您要使用的声卡。</p>

 <p>通过使用<app>终端</app>中的 <cmd>lspci</cmd> 命令，您可查看您的声卡类型。如果以 <link xref="user-admin-explain">超级用户</link> 运行 <cmd>lspci</cmd>，则您可获得更完整的结果；输入 <cmd>sudo lspci</cmd> 并键入您的密码。查看是否列示了<em>音频控制器</em>或<em>音频设备</em> — 应包含该声卡的制造商和型号。<cmd>sudo lspci -v</cmd> 将会显示一个包含更多详细信息的列表。</p>

 <p>您可以尝试通过网络搜索和安装读卡器驱动。如果仍然无法解决，则应 <link xref="report-ubuntu-bug">向 Ubuntu 报告一个问题</link>。</p>

 <p>如果找不到适合您声卡的驱动程序，则您可能需要购买一个新的声卡。您可以购买安装到计算机内部的声卡和外部的 USB 声卡。</p>
</section>

</page>
