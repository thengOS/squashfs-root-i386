<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" style="task" id="unity-dash-intro" xml:lang="zh-CN">
  <info>
    <link type="guide" xref="index" group="unity-dash-intro"/>
    <link type="guide" xref="shell-overview#desktop"/>

    <desc>Dash 是启动器顶部的按钮。</desc>

    <revision version="13.10" date="2013-10-23" status="outdated"/>

    <credit type="author">
      <name>Jeremy Bicha</name>
      <email>jbicha@ubuntu.com</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

  <title>使用 Dash 查找应用、文件、音乐以及更多内容</title>

  <media type="image" src="figures/unity-dash-sample.png" style="floatend floatright">
    <p>搜索</p>
  </media>

  <p><gui>Dash</gui> 可以用于搜索应用程序、文件、音乐和视频，以及显示您最近使用的项目。如果您最近使用过数据表或者编辑过图片，但是忘记了存在了哪里，您将会发现 Dash 的这个功能很好用。</p>

  <p>要使用 <gui>Dash</gui>，请点击 <link xref="unity-launcher-intro">启动器</link> 顶部的按钮。此按钮上有 Ubuntu 图标。要更快访问，可以直接按 <key xref="windows-key">Super</key> 键。</p>

  <p>要隐藏 <gui>Dash</gui>，再次点击顶部按钮或者按 <key>Super</key> 或 <key>Esc</key>。</p>

<section id="dash-home">
  <title>在 Dash 主界面可以搜索所有</title>

  <p>打开 Dash 后看到的第一个界面就是 Dash 主界面。不作任何输入或点击，Dash 主界面将显示最近使用的程序和文件。</p>
  <p>每种类型将只显示一行。如果有更多结果，可以点击 <gui>查看更多结果</gui> 来查看。</p>

  <p>要搜索，请直接输入。相关的搜索结果将直接显示在不同的已经安装的栏目中。</p>

  <p>点击一个结果以打开之，或者可以按 <key>回车</key> 来打开列表中的第一项。</p>

</section>

<section id="dash-lenses">
  <title>栏目</title>

  <p>栏目可以让你在 Dash 搜索结果中集中精力，并排除其他栏目的结果。</p>

  <p>你可以在 <gui>栏目条</gui> － 也就是 Dash 底部深色的条 － 中看到可用的栏目。</p>

  <p>要切换到不同的栏目，点击相应的按钮或按 <keyseq><key>Ctrl</key><key>Tab</key></keyseq>。</p>

</section>

<section id="dash-filters">
  <title>过滤器</title>

  <p>过滤器可以进一步缩窄搜索结果。</p>

  <p>点击 <gui>过滤结果</gui> 来选择过滤器。你可能需要点击过滤器头，比如 <gui>源</gui> 来查看可用的选项。</p>

</section>

<section id="dash-previews">
  <title>预览</title>

  <p>如果在搜索结果上点击右键，将会打开 <gui>预览</gui>，其含有关于结果更详细的信息。</p>

  <p>要关闭预览，点击任何空白区或按 <key>Esc</key>。</p>

</section>

</page>
