��          �   %   �      p  <   q  9   �  @   �  C   )     m  N   s     �  Z   �  ?   .  X   n  #   �     �  )   �  .   !  t   P  E   �  ?     4   K     �     �     �  $   �  #   �  $   �  [     *   y  /   �  �  �  A   a	  U   �	  P   �	  c   J
     �
  \   �
       i   !  I   �  [   �  '   1     Y  )   j  A   �  �   �  L   v  ^   �  @   "     c     o  
   �  @   �  8   �  8     g   H  2   �  H   �                                                           
                                                     	           %(command)s - %(description)s

Usage: %(command)s %(params)s A serious error occured while processing your request:
%s An internal error has occured, please contact the administrator. Credentials check failed, you may be unable to see all information. Error Following detailed information on page "%(pagename)s" is available::

%(data)s Full-text search Hello there! I'm a MoinMoin Notification Bot. Available commands:

%(internal)s
%(xmlrpc)s Here's the page "%(pagename)s" that you've requested:

%(data)s Last author: %(author)s
Last modification: %(modification)s
Current version: %(version)s Please specify the search criteria. Search text Submit this form to perform a wiki search That's the list of pages accesible to you:

%s The "help" command prints a short, helpful message about a given topic or function.

Usage: help [topic_or_function] The "ping" command returns a "pong" message as soon as it's received. This command may take a while to complete, please be patient... This command requires a client supporting Data Forms Title search Unknown command "%s"  Wiki search You are not allowed to use this bot! You must set a (long) secret string You must set a (long) secret string! You've specified a wrong parameter list. The call should look like:

%(command)s %(params)s Your request has failed. The reason is:
%s searchform - perform a wiki search using a form Project-Id-Version: moin
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2007-07-17 03:14+0200
PO-Revision-Date: 2012-03-12 22:48+0000
Last-Translator: Sascha <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:11+0000
X-Generator: Launchpad (build 18115)
 %(command)s - %(description)s

Verwendung: %(command)s %(params)s Ein schwerwiegender Fehler ist während der Bearbeitung Ihrer Anfrage aufgetreten:
%s Ein interner Fehler ist aufgetreten. Bitte kontaktieren Sie den Systemverwalter. Überprüfung der Zugangsdaten fehlgeschlagen. Unter Umständen sehen Sie nicht alle Informationen. Fehler Folgende detaillierte Informationen sind auf der Seite »%(pagename)s« abrufbar::

%(data)s Volltext-Suche Hallo! Ich bin eine MoinMoin-Ankündigungsfunktionseinheit. Verfügbare Befehle:

%(internal)s
%(xmlrpc)s Hier ist die Seite »%(pagename)s«, die Sie angefordert haben:

%(data)s Letzter Autor: %(author)s
Letzte Änderung: %(modification)s
Momentane Version: %(version)s Bitte legen Sie die Suchkriterien fest. Nach Text suchen Abschicken, um eine Wiki-Suche zu starten Das ist eine Liste von Seiten, die für Sie zugänglich sind:

%s Der »help«-Befehl zeigt eine kurze, hilfreiche Mitteilung zu einem gewünschten Stichwort oder einer Funktion an.

Verwendung: help [Stichwort_oder_Funktion] Der »Ping«-Befehl antwortet mit einem »Pong«, sobald er empfangen wurde. Zur Vollendung benötigt dieser Befehl eine Weile, bitte haben Sie einen Augenblick Geduld … Dieser Befehl benötigt eine vom Client unterstützte Dateiform. Titel-Suche Unbekannter Befehl »%s«  Wiki-Suche Es ist Ihnen nicht erlaubt, diese Funktionseinheit zu verwenden! Sie müssen eine (lange) geheime Zeichenkette festlegen. Sie müssen eine (lange) geheime Zeichenkette festlegen! Sie haben eine falsche Parameterliste angegeben. Der Aufruf sollte so aussehen:

%(command)s %(params)s Ihre Anfrage ist fehlgeschlagen. Der Grund ist:
%s searchform - Eine Wiki-Suche unter Verwendung eines Formulars ausführen 