��    I      d  a   �      0     1     G     \     r     ~     �     �     �     �     �  n   �     ]     y     �  +   �  J   �  E   +  �   q  r   �  k   n	     �	     �	  5   �	  "   &
  <   I
     �
  #   �
     �
  )   �
     �
  <      3   =      q     �     �     �      �      �     �  
        "     @     P  �   g            ;   3  -   o  	   �  (   �  u   �  h   F     �  `   �     '     5  %   Q     w  
   �  D   �  	   �  
   �  6   �  6   ,  -   c     �     �  �   �  6   E  ;   |  &   �     �  �  �     �          /     D     P     a     v  #   �     �     �  |   �      R  $   s  #   �  2   �  Q   �  L   A  y   �  |     x   �     �       7     :   L  Y   �  .   �  -        >  3   G     {  O     _   �  )   /     Y     `     i  (   �  ,   �     �     �  !        )  +   @  �   l     D     H  K   a  *   �  	   �  (   �  �     q   �       t   0     �  #   �  1   �  2        >  T   L  	   �     �  L   �  J      C   O      �   &   �   }   �   D   S!  I   �!  =   �!  +    "     *          %                          E      '   4   +       0          :   ;   1          &      I                         F      $   B   (   9      7   @   G       8   C   
      ?   2       3                   ,   )   5   H             .       =       #       A   /             "                  6              D   <   !   >         	   -               128-bit (most secure) 40-bit (less secure) <b>Authentication</b> <b>Echo</b> <b>General</b> <b>Misc</b> <b>Optional</b> <b>Security and Compression</b> Ad_vanced... All Available (Default) Allow MPPE to use stateful mode. Stateless mode is still attempted first.
config: mppe-stateful (when checked) Allow _BSD data compression Allow _Deflate data compression Allow st_ateful encryption Allow the following authentication methods: Allow/disable BSD-Compress compression.
config: nobsdcomp (when unchecked) Allow/disable Deflate compression.
config: nodeflate (when unchecked) Allow/disable Van Jacobson style TCP/IP header compression in both the transmit and the receive directions.
config: novj (when unchecked) Allow/disable authentication methods.
config: refuse-pap, refuse-chap, refuse-mschap, refuse-mschap-v2, refuse-eap Append the domain name <domain> to the local host name for authentication purposes.
config: domain <domain> Authenticate VPN CHAP Compatible with Microsoft and other PPTP VPN servers. Could not find pptp client binary. Could not find secrets (connection invalid, no vpn setting). Could not find the pppd binary. D-Bus name to use for this instance Default Don't quit when VPN connection terminates EAP Enable custom index for ppp<n> device name.
config: unit <n> Enable verbose debug logging (may expose passwords) Invalid or missing PPTP gateway. MSCHAP MSCHAPv2 Missing VPN gateway. Missing or invalid VPN password. Missing or invalid VPN username. Missing required option '%s'. NT Domain: No VPN configuration options. No VPN secrets! No cached credentials. Note: MPPE encryption is only available with MSCHAP authentication methods. To enable this checkbox, select one or more of the MSCHAP authentication methods: MSCHAP or MSCHAPv2. PAP PPTP Advanced Options PPTP server IP or name.
config: the first parameter of pptp Password passed to PPTP when prompted for it. Password: Point-to-Point Tunneling Protocol (PPTP) Require the use of MPPE, with 40/128-bit encryption or all.
config: require-mppe, require-mppe-128 or require-mppe-40 Send LCP echo-requests to find out whether peer is alive.
config: lcp-echo-failure and lcp-echo-interval Send PPP _echo packets Set the name used for authenticating the local system to the peer to <name>.
config: user <name> Show password Use TCP _header compression Use _Point-to-Point encryption (MPPE) Use custom _unit number: User name: You need to authenticate to access the Virtual Private Network '%s'. _Gateway: _Security: couldn't convert PPTP VPN gateway IP address '%s' (%d) couldn't look up PPTP VPN gateway IP address '%s' (%d) invalid boolean property '%s' (not yes or no) invalid gateway '%s' invalid integer property '%s' nm-pptp-service provides integrated PPTP VPN capability (compatible with Microsoft and other implementations) to NetworkManager. no usable addresses returned for PPTP VPN gateway '%s' no usable addresses returned for PPTP VPN gateway '%s' (%d) property '%s' invalid or not supported unhandled property '%s' type %s Project-Id-Version: network-manager-pptp master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=NetworkManager&keywords=I18N+L10N&component=VPN: pptp
POT-Creation-Date: 2016-04-14 07:51+0000
PO-Revision-Date: 2012-04-10 08:09+0000
Last-Translator: Dennis Baudys <Unknown>
Language-Team: German <gnome-de@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:03+0000
X-Generator: Launchpad (build 18115)
 128-Bit (am sichersten) 40-Bit (weniger sicher) <b>Legitimierung</b> <b>Echo</b> <b>Allgemein</b> <b>Verschiedenes</b> <b>Optional</b> <b>Sicherheit und Komprimierung</b> Er_weitert … Alle verfügbaren (Vorgabe) MPPE im Modus »stateful« erlauben. Statusloser Modus wird weiterhin zuerst versucht.
config: mppe-stateful (wenn gewählt) _BSD-Datenkomprimierung erlauben _Deflate-Datenkomprimierung erlauben St_ateful-Verschlüsselung erlauben Die folgenden Methoden zur Legitimierung erlauben: BSD-Compress Kompression erlauben/abschalten.
config: nobsdcomp (wenn abgewählt) Deflate-Kompression erlauben/abschalten.
config: nodeflate (wenn abgewählt) Van Jacobson TCP/IP Header-Kompression in Sende- und Empfangsrichtung erlauben/abschalten.
config: novj (wenn abgewählt) Authentifizierungsmethoden erlauben/abschalten.
config: refuse-pap, refuse-chap, refuse-mschap, refuse-mschap-v2, refuse-eap Hängen Sie den Domänennamen <domain> an den lokalen Rechnernamen zu Authentifizierungszwecken.
config: domain <domain> VPN legitimieren CHAP Kompatibel mit Microsofts und anderen PPTP VPN-Servern. Binärdatei für pptp-Client konnte nicht gefunden werden. Geheimnisse konnten nicht gefunden werden (Verbindung ungültig, keine VPN-Einstellungen) pppd-Binärdatei konnte nicht gefunden werden. Für diese Instanz zu verwendender D-Bus-Name Standard Nicht beenden, wenn die VPN-Verbindung beendet wird EAP Benutzerdefinierten Index für ppp<n>-Gerätenamen aktivieren.
config: unit <n> Ausführliche Protokollierung zur Fehlerdiagnose erlauben (könnte Passwörter sichtbar machen) Das VPN-Gateway fehlt oder ist ungültig. MSCHAP MSCHAPv2 Kein VPN-Gateway angegeben. Fehlendes oder ungültiges VPN-Passwort. Fehlender oder ungültiger VPN-Benutzername. Benötigte Option »%s« fehlt. NT-Domäne: Keine VPN-Konfigurationsoptionen. Keine VPN-Geheimnisse! Kein Anmeldedaten-Zwischenspeicher gefunden Achtung: MPPE-Verschlüsselung ist nur mit MSCHAP-Legitimierungsmethoden verfügbar. Um dieses Ankreuzfeld zu aktivieren, wählen Sie eine oder mehrere der folgenden Legitimierungsmethoden aus: MSCHAP oder MSCHAPv2. PAP Erweiterte PPTP-Optionen IP-Adresse oder Name des PPTP-Servers.
config: der erste Parameter von PPTP Auf Anfrage an PPTP übergebenes Passwort. Passwort: Point-to-Point Tunneling Protocol (PPTP) Den Einsatz von MPPE-Verschlüsselung auf automatische Auswahl, 128-Bit oder 40-Bit festlegen.
config: require-mppe, require-mppe-128 oder require-mppe-40 LCP Echo-Requests senden, um zu erkennen, ob der Partner reagiert.
config: lcp-echo-failure und lcp-echo-interval PPP-_Echo Pakete senden Den Namen zur Authentifizierung des lokalen Systems gegenüber dem Partner festlegen als <Name>.
config: user <Name> Passwort zeigen TCP-_Header-Komprimierung verwenden _Point-to-Point Verschlüsselung (MPPE) verwenden Benutzerdefinierte N_ummer für Einheit verwenden: Benutzername: Sie müssen sich legitimieren, um auf das Virtuelle Private Netz »%s« zuzugreifen. _Gateway: _Sicherheit IP-Adresse »%s« des PPTP-VPN-Gateways konnte nicht umgewandelt werden (%d) IP-Adresse »%s« des PPTP-VPN-Gateways konnte nicht ermittelt werden (%d) Ungültige boolesche Eigenschaft »%s« (nicht »yes« oder »no«) Ungültiges Gateway »%s« Ungültige Ganzzahl-Eigenschaft »%s« nm-pptp-service integriert PPTP-VPN-Funktionalität in NetworkManager (kompatibel zu Microsoft und anderen Implementationen). Keine nutzbaren Adressen zurückgegeben für PPTP-VPN-Gateway »%s« Keine nutzbaren Adressen zurückgegeben für PPTP-VPN-Gateway »%s« (%d) Eigenschaft »%s« ist ungültig oder wird nicht unterstützt Unbehandelte Eigenschaft »%s« des Typs %s 