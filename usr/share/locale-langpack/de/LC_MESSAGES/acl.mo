��    5      �  G   l      �     �     �     �     �     �  )   �  )   %     O  �   d  7  0  �  h  B   J
  l  �
  �   �  Z     '   �  '        *  $   H     m     �  &   �  2   �  3   �  /   2  /   b  =   �     �  %   �  2        D  $   \  &   �  +   �  '   �  ,   �  &   )  '   P  *   x  +   �     �     �     �          #     :  &   X          �     �     �     �  }  �     f     z     �     �     �  +   �  +        0  �   E  9  �  g  5  4   �  q  �  u   D  Q   �  5     4   B      w  '   �     �  0   �  /   	  8   9  7   r  5   �  5   �  E        \  "   {  3   �     �  )   �  3     @   I  2   �  ?   �  4   �  3   2   8   f   7   �   "   �   #   �      !     1!     I!     i!  /   �!     �!     �!     �!  #   �!  !   "                 	              
   5                                 -           !          1      &             *      3   +      $                    )       "              %   #         ,                         '   /   .         4          0   2   (    	%s -B pathname...
 	%s -D pathname...
 	%s -R pathname...
 	%s -b acl dacl pathname...
 	%s -d dacl pathname...
 	%s -l pathname...	[not IRIX compatible]
 	%s -r pathname...	[not IRIX compatible]
 	%s acl pathname...
       --set=acl           set the ACL of file(s), replacing the current ACL
      --set-file=file     read ACL entries to set from file
      --mask              do recalculate the effective rights mask
   -R, --recursive         recurse into subdirectories
  -L, --logical           logical walk, follow symbolic links
  -P, --physical          physical walk, do not follow symbolic links
      --restore=file      restore ACLs (inverse of `getfacl -R')
      --test              test mode (ACLs are not modified)
   -a,  --access           display the file access control list only
  -d, --default           display the default access control list only
  -c, --omit-header       do not display the comment header
  -e, --all-effective     print all effective rights
  -E, --no-effective      print no effective rights
  -s, --skip-base         skip files that only have the base entries
  -R, --recursive         recurse into subdirectories
  -L, --logical           logical walk, follow symbolic links
  -P, --physical          physical walk, do not follow symbolic links
  -t, --tabular           use tabular output format
  -n, --numeric           print numeric user/group identifiers
  -p, --absolute-names    don't strip leading '/' in pathnames
   -d, --default           display the default access control list
   -m, --modify=acl        modify the current ACL(s) of file(s)
  -M, --modify-file=file  read ACL entries to modify from file
  -x, --remove=acl        remove entries from the ACL(s) of file(s)
  -X, --remove-file=file  read ACL entries to remove from file
  -b, --remove-all        remove all extended ACL entries
  -k, --remove-default    remove the default ACL
   -n, --no-mask           don't recalculate the effective rights mask
  -d, --default           operations apply to the default ACL
   -v, --version           print version and exit
  -h, --help              this help text
 %s %s -- get file access control lists
 %s %s -- set file access control lists
 %s: %s in line %d of file %s
 %s: %s in line %d of standard input
 %s: %s: %s in line %d
 %s: %s: Cannot change mode: %s
 %s: %s: Cannot change owner/group: %s
 %s: %s: Malformed access ACL `%s': %s at entry %d
 %s: %s: Malformed default ACL `%s': %s at entry %d
 %s: %s: No filename found in line %d, aborting
 %s: %s: Only directories can have default ACLs
 %s: No filename found in line %d of standard input, aborting
 %s: Option -%c incomplete
 %s: Option -%c: %s near character %d
 %s: Removing leading '/' from absolute path names
 %s: Standard input: %s
 %s: access ACL '%s': %s at entry %d
 %s: cannot get access ACL on '%s': %s
 %s: cannot get access ACL text on '%s': %s
 %s: cannot get default ACL on '%s': %s
 %s: cannot get default ACL text on '%s': %s
 %s: cannot set access acl on "%s": %s
 %s: cannot set default acl on "%s": %s
 %s: error removing access acl on "%s": %s
 %s: error removing default acl on "%s": %s
 %s: malloc failed: %s
 %s: opendir failed: %s
 Duplicate entries Invalid entry type Missing or wrong entry Multiple entries of same type Try `%s --help' for more information.
 Usage:
 Usage: %s %s
 Usage: %s [-%s] file ...
 preserving permissions for %s setting permissions for %s Project-Id-Version: acl-2.1.2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-02-07 11:10+0000
PO-Revision-Date: 2012-02-21 16:28+0000
Last-Translator: Hendrik Knackstedt <Unknown>
Language-Team: <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:54+0000
X-Generator: Launchpad (build 18115)
 	%s -B pfadname...
 	%s -D pfadname...
 	%s -R pfadname...
 	%s -b acl dacl pfadname...
 	%s -d dacl pfadname...
 	%s -l pfadname...	[nicht IRIX-kompatibel]
 	%s -r pfadname...	[nicht IRIX-kompatibel]
 	%s acl pfadname...
       --set=acl            Ersetze die ACL von Datei(en)
      --set-file=datei     Lies die ACL-Einträge aus datei
      --mask               Effektive Rechte nicht neu berechnen
   -R, --recursive          In Unterverzeichnisse wechseln
  -L, --logical            Symbolischen Links folgen
  -P, --physical           Symbolischen Links nicht folgen
      --restore=datei      ACLs wiederherstellen (Umkehr von `getfacl -R')
      --test               Testmodus (ACLs werden nicht verändert)
   -a,  --access           nur die Liste der Datei-Zugriffsberechtigungen anzeigen
  -d, --default           nur die Standard-Liste der Datei-Zugriffsberechtigungen anzeigen
  -c, --omit-header       die Kopfzeile mit Kommentar nicht anzeigen
  -e, --all-effective     alle effektiven Rechte ausgeben
  -E, --no-effective      keine effektiven Rechte ausgeben
  -s, --skip-base         alle Dateien überspringen, die nur Basiseinträge besitzen
  -R, --recursive         rekursiv in Unterordnern arbeiten
  -L, --logical           logischer Weg, symbolischen Links folgen
  -P, --physical          physikalischer Weg, keinen symbolischen Links folgen
  -t, --tabular           tabellarisches Ausgangsformat benutzen
  -n, --numeric           numerische Benutzer-/Gruppen-Bezeichner ausgeben
  -p, --absolute-names    keine führenden »/« in den Pfadnamen ausschließen
   -d, --default            Die Vorgabe-ACL ausgeben
   -m, --modify=acl         Verändere die ACL(s) von Datei(en)
  -M, --modify-file=datei  Lies die ACL-Einträge aus datei
  -x, --remove=acl         Entferne Einträge aus ACLs von Datei(en)
  -X, --remove-file=datei  Lies die ACL-Einträge aus datei
  -b, --remove-all         Alle erweiterten ACL-Einträge entfernen
  -k, --remove-default     Vorgabe-ACL entfernen
   -n, --no-mask            Effektive Rechte nicht neu berechnen
  -d, --default            Bearbeite die Vorgabe-ACL
   -v, --version Gibt die Versionsnummer aus
-h, --help Zeigt diesen Hilfetext an
 %s %s -- Datei-Zugriffskontrollisten (ACLs) anzeigen
 %s %s -- Datei-Zugriffskontrollisten (ACLs) ändern
 %s: %s in Zeile %d der Datei %s
 %s: %s in Zeile %d der Standardeingabe
 %s: %s: %s in Zeile %d
 %s: %s: Modus konnte nicht geändert werden: %s
 %s: %s: Kann Besitzer/Gruppe nicht ändern: %s
 %s: %s: Ungültige Zugriffs-ACL `%s': %s bei Eintrag %d
 %s: %s: Ungültige Vorgabe-ACL `%s': %s bei Eintrag %d
 %s: %s: Kein Dateiname in Zeile %d gefunden; Abbruch
 %s: %s: Nur Verzeichnisse können Vorgabe-ACLs haben
 %s: Kein Dateiname in Zeile %d der Standardeingabe gefunden; Abbruch
 %s: Option -%c unvollständig
 %s: Option -%c: %s bei Zeichen %d
 %s: Entferne führende '/' von absoluten Pfadnamen
 %s: Standardeingabe: %s
 %s: Zugriffs-ACL '%s': %s bei Eintrag %d
 %s: Kann Zugriffs-ACL von '%s' nicht ermitteln: %s
 %s: Kann den Text zur Zugriffs-ACL von '%s' nicht ermitteln: %s
 %s: Kann Vorgabe-ACL von '%s' nicht ermitteln: %s
 %s: Kann den Text zur Vorgabe-ACL von '%s' nicht ermitteln: %s
 %s: Kann die Zugriffs-ACL von "%s" nicht setzen: %s
 %s: Kann die Vorgabe-ACL von "%s" nicht setzen: %s
 %s: Fehler beim Entfernen der Zugriffs-ACL von "%s": %s
 %s: Fehler beim Entfernen der Vorgabe-ACL von "%s": %s
 %s: malloc ist fehlgeschlagen: %s
 %s: opendir ist fehlgeschlagen: %s
 Doppelte Einträge Ungültiger Eintragstyp Fehlender oder falscher Eintrag Mehrere Einträge gleichen Typs Weiterführende Informationen mit `%s --help'.
 Verwendung:
 Verwendung: %s %s
 Aufruf: %s [-%s] datei ...
 Erhalten der Zugriffsrechte für %s Setzen der Zugriffsrechte für %s 