��           �  "      h-  *   i-     �-  <   �-  $   �-  
   	.     .     #.     *.     I.     ^.     ~.     �.  	   �.     �.     �.     �.     �.     /     /     =/      M/     n/     u/     �/     �/  (   �/  /   �/  ;   0  $   R0  :   w0     �0     �0  (   �0  "   	1     ,1     A1     ^1  3   |1      �1  &   �1  &   �1  /   2  /   O2     2     �2  .   �2     �2  "   �2     3     33     L3     h3     �3  "   �3     �3     �3     �3      4     4  /   24     b4     x4     �4  -   �4     �4     �4     5     5     -5     >5     ^5     v5     �5  !   �5     �5  )   �5     6     (6     ?6     R6     j6     ~6      �6  !   �6  ,   �6     7      7     @7     W7     e7  0   �7     �7     �7     �7     �7     8     8     78     Q8     j8     �8     �8     �8  &   �8     �8     9     ,9  )   B9     l9  &   {9  3   �9     �9     �9     :     $:  &   0:     W:     h:     w:     �:  9   �:  #   �:     �:     �:     ;     %;  �  1;  H  ?     ZB     jB     zB  F  �B     �C     �C     �C     �C  	   D  	   D  �  D     J  �
  -J     �T  1   �T  1   $U  a  VU  �  �V    xX  ]  �Z  �  �[  T  �_    b  �  "f    �i  +  �n  �   q  ?  �q     �t     �t     u  t  u  �   �x  �  y  �   l  g  �  �   s�    k�  �   o�  g  a�  �   ɇ  u  ��  �  2�  w  ߋ     W�  |   _�  �   ܍  �   ��  
   <�     G�     `�     {�     ��  �   ��     u�     ��     ��      ��     �     �     ��  	   �     �     #�  	   7�     A�  N   H�  ,  ��  =  Ē  f   �     i�  B  x�  �  ��     A�  P   F�     ��  �  ��  �   8�  �  �  �  ��  �  ��  �  B�    �  F   �  F   :�  L  ��  �  έ  S   ��     �     �  >  �  �  C�  "  �  ?  2�    r�  �  ��  K   <�  D   ��     ��  �   ��  	   ��     ��     ��     ��     ��     �  O  �  *   i�  
   ��     ��     ��     ��  5   ��  O  %�  B   u�  �  ��  B   ��  E   ��     �     (�     ;�     I�     X�  X   l�     ��  *   ��     �     �     *�  �   9�  �  #�     ��     ��  
   ��     �     �     (�  %   G�  $   m�  '   ��     ��     ��     ��     �     �  !   ;�     ]�     z�     ��     ��  '   ��  0   ��  .   �     B�  9   a�     ��     ��     ��  $   ��     ��     �      �     .�  &   7�  '   ^�  9   ��     ��  �   ��  .   f�  :   ��  3   ��  	   �     �  !   '�     I�  3   W�     ��  =   ��  -   ��     �  '   6�  &   ^�  *   ��  *   ��  )   ��  )   �  %   /�  %   U�      {�  1   ��  #   ��  1   ��  &   $�  5   K�     ��     ��  !   ��  !   ��  :   ��     ,�     I�      d�  1   ��  �   ��  �   M�  #   ��  '   �  $   ?�     d�  $   q�  #   ��  '   ��     ��  /   ��  .   �     I�     h�     ~�     ��     ��     ��     ��     ��     �     !�  ,   ;�  %   h�  ,   ��  %   ��     ��  @   ��     1�     :�     G�  ,   \�     ��  #   ��     ��  @   ��     �     �     3�  -   N�  ,   |�  '   ��     ��  .   ��  ,   �  &   H�  0   o�  6   ��  P   ��  (   (�     Q�  )   n�     ��     ��  ?   ��  T   �     X�     i�     �  8   ��  V   ��  &   �  '   D�     l�     ��     ��  (   ��     ��     ��     ��     �  "   '�     J�  5   W�  O   ��     ��     ��     �  	   �     �     *�     J�  
   R�  
   ]�  +   h�  9   ��  ;   ��  $   
�     /�  Y   M�     ��     ��     ��     ��     ��      �     6�     Q�     g�  H   x�     ��     ��     ��     ��  "   �  +   0�     \�     x�  4   ��  
   ��  D   ��  ?   
�  ,   J�     w�     ��  !   ��  "   ��  "   ��     
�     �  	   7�  o   A�     ��  [   ��  1    �  /   R�  )   ��  3   ��     ��  &   ��  2   !�  5   T�  ,   ��  
   ��  
   ��  1   ��  I   ��  4   I�  .   ~�  8   ��  (   ��  ,   �  ,   <�  0   i�  )   ��  	   ��     ��      ��  "   ��     �     :�     T�     a�  &   n�  =   ��     ��     ��  '   �     -�     D�  ,   a�  )   ��     ��  $   ��     ��     �     �     1�     7�  "   J�     m�     ��     ��  ,   ��  '   ��     �     �  %   5�  .   [�  -   ��  7   ��  6   ��  2   '�  1   Z�  *   ��  ,   ��  ,   ��  ;   �  #   M�     q�     y�     ��  !   ��     ��  6   ��     !�     4�  *   C�  "   n�     ��  6   ��  	   ��     ��  -   �  -   0�  !   ^�     ��  '   ��  '   ��     ��     ��  �    0   �     D   * 1   o 
   �    �    �    �    � $        +    L 
   b    m 2   � 2   �    �           ,   A 5   n    � 0   �    � .   � B   ( B   k P   � .   � E   .     t    � >   � $   �     /   4 '   d >   � %   � 0   � :   " J   ] B   � %   �    	 ?   -	 $   m	 -   �	    �	 $   �	 %   
 .   )
 $   X
 1   }
    �
    �
 2   �
        0 =   J    �    �    � $   �    � "       >    T    q #   �    �    � 1   � 6       G '   f "   � "   �    �    �     &    !   F    h 5   �    � *   �            + @   F #   � (   �    �    �            <    X     x    � /   � )   � .    *   6     a     � ,   �    � 7   � 4       J -   g    �    � 3   �    �    � ,       G >   _ 1   �    �    �    �    � )   *  5    `    o    � �  � 3       O    e    v 
   �    � �  � &   �% ;  �% &   �2 1   %3 1   W3 �  �3 �  75 S  -7 �  �9 �  :; �  �> F  �A M  7G �  �K �  0Q �   �S !  �T    �W 
   �W    �W �  �W 6  �\ �   ^ �   �d   �e 3  �h "  �i   k �   l /  �n �  p �  �q �  �s    ou �   wu B  8v �   {w    Hx    ax    sx    �x    �x �   �x    �y    �y    �y    z    $z 	   ?z    Iz 
   hz    sz    �z    �z    �z Q   �z 8   { a  Y| {   �}    7~ �  T~ �  �    �� Y   ��    � �  � �   �� a  ��   �   � �  
�   Җ B   R� F   �� E  ܘ   "� \   )�    ��    �� �  ��   �� '	  � �  :� u  � �  �� P   |� o   ͸    =�   N� 	   d� 
   n�    y�    ��    ��    Ǻ �  ֺ .   ��    �� &   Ӽ     �� #   � Z   ?� z  �� B   � k  X� C   �� :   �    C�    \�    y�    ��    �� `   �� -   � ;   F�    ��    ��    ��   ��   ��    ��    ��    
�    �    )� '   A� 2   i� 9   �� "   �� (   ��    "�    @� ,   _� %   �� 6   �� $   ��    �    -�    =� ,   Z� :   �� 2   ��    �� E   �    Y�    b� I   u� (   ��    ��    �    �    (� 2   8� A   k� J   ��    �� �   � A   �� K   �� F   F� 	   �� $   �� '   ��    �� ]   �� 1   T� G   �� A   �� &   � 6   7� :   n� =   �� D   �� 9   ,� 3   f� ;   �� ?   �� 2   � 9   I� <   �� C   �� 7   � N   <� "   �� '   �� 3   �� 3   
� 9   >� $   x�    �� &   �� 8   �� �   � �   �� 0   e� '   �� $   ��    �� $   �� A   � 1   W�    �� 0   �� 8   �� 1    �    2�    R�    i� &   ��    �� '   ��    ��    �� %   � -   ;� /   i� <   �� ,   ��    � ?   �    W�    `�    p� -   ��    �� !   ��    �� N   ��    H� 4   \� *   �� =   �� 1   �� :   ,�    g� C   �� ?   �� ;   � G   @� :   �� ]   �� ?   !� %   a� )   ��    ��    �� [   �� c   ;�    ��    ��    �� I   �� [   !� /   }� '   ��    ��    ��    � +   �    D�    b�    s�    �� )   ��    �� <   �� ^   �    {�    ��    �� 
   �� %   ��    ��    � 
   �    � +   +� E   W� R   �� L   �� -   =� `   k� 0   ��    ��    � %   � #   A� +   e�    �� !   ��    �� b   �� $   K� $   p�    ��    �� +   �� <   �� )   .� 	   X� ;   b�    �� K   �� ?   �� .   6�    e�    y� 2   �� "   �� "   ��    � "   "� 	   E� �   O�    �� b   �� 2   H� 4   {� >   �� J   ��    :� 5   Q� [   �� G   �� X   +�    �� 
   �� 8   �� U   �� F   '� 3   n� 8   �� A   �� K   � 6   i� 9   �� 4   �� 	   �    � $   +� %   P�    v�    ��    ��    �� #   �� @   ��    ,� *   F� *   q�    �� "   �� <   �� 2   � !   J� %   l�    ��    ��    ��    ��    �� -   �� !   �    :�    ?� +   ]� +   ��    ��    �� +   �� +   � 4   B� <   w� <   �� 5   �� 5   '� 4   ]� 4   �� 4   �� F   �� .   C� 	   r�    |�    �� #   �� .   �� 7   �    :�    M� 9   \� 1   �� '   �� A   �� 	   2�    <� B   L� B   �� #   ��    �� '   	� -   1�    _�    |�        l  �   U  ^         +  �      J  �  }        @  �  �  ~   �             �   e  �       @         �    �     R        3  J      k       �  7  �  /   *  �  z  #   �      �   h  �           |   o  �      �      �   �   �  �  �      �          �  �            �  �     �   |      �  �     �       �         	  $  <   �    �   �          �  #        %       G       -   �  �   �   �             �   �   �   �                      �      �   =   I   T   �   �   �               K  F          H   G      �  �      �       S   �  �   �       u       �   h       �   �  9  q   S  �   �   �       $   �       �       �       O       �  �           �   �  �  �   K       �       �  	   N    M   /  �          �          �   �      d   g  �  �        W  �   5     �  ]  �   �  r       �         �   �  �                 �  �          f           7   �  1           m   �      �  e       �  E   *   �     �  8  �  _           �   �  B  X       s   �    �   Z          �       �       2          �              ,   �   �   Y  r  [  �   �  Q   �  �  N      �  b  0       �  �               �      �  �        L  
      `  �                  ,  �         �      z   �      �       �       c  �  '       "  &  v     �       �   f              j   �       �  �           �   �       �  �  �  i       �   �       ^   �   &   ?                    R   W   �  �  }   �   F   �      �   �          �  �   �  �   �  �   �    m  �  x  5   o       �       �      u  k  E  P            �       ~      C      �      �  A   ?                   �   D   �  �      �   
  �  V  D  �       l   B     �   p          O   >  �              2   t            �   �      (       A  H  �  {    �     _             �   !   �   �           +       >       n  �  �   4  �  �  �      0            ]   �  �      C   �    �   y     6           p       s  <  �   �  ;  I  �      �  �  �  b   �    �  :   y   Z       �  4   M  �  �   �     \   �        j  �       i  \  8       '  �   v  �   �         �      �   �       �      �       �      �       
   c     �           �   t   g   �   d      �  �  �  3   U   P   L   )  x   [   6       �   �   �         �  a      �     Q  %          �       �         `   �  �  �   �   �   !  �  	      �   �    X  .  �       �   �  �   �        �  �   �   �       �      �           �  �   �  �           T          w  �  �     =  1   �  )   �   (        {       �     �      a           �       �  .   Y   n   V   �   �      :            �   �              9       q    ;           �   w      �      �   �   -  �             "   �   timed out waiting for input: auto-logout
 	-%s or -o option
 	-ilrsD or -c command or -O shopt_option		(invocation only)
 
malloc: %s:%d: assertion botched
   (wd: %s)  (core dumped)  line  $%s: cannot assign in this way %c%c: invalid option %d: invalid file descriptor: %s %s can be invoked via  %s has null exportstr %s is %s
 %s is a function
 %s is a shell builtin
 %s is a shell keyword
 %s is aliased to `%s'
 %s is hashed (%s)
 %s is not bound to any keys.
 %s out of range %s%s%s: %s (error token is "%s") %s: %s %s: %s out of range %s: %s: bad interpreter %s: %s: cannot open as FILE %s: %s: compatibility value out of range %s: %s: invalid value for trace file descriptor %s: %s: must use subscript when assigning associative array %s: %s:%d: cannot allocate %lu bytes %s: %s:%d: cannot allocate %lu bytes (%lu bytes allocated) %s: ambiguous job spec %s: ambiguous redirect %s: arguments must be process or job IDs %s: bad network path specification %s: bad substitution %s: binary operator expected %s: cannot allocate %lu bytes %s: cannot allocate %lu bytes (%lu bytes allocated) %s: cannot assign fd to variable %s: cannot assign list to array member %s: cannot assign to non-numeric index %s: cannot convert associative to indexed array %s: cannot convert indexed to associative array %s: cannot create: %s %s: cannot delete: %s %s: cannot destroy array variables in this way %s: cannot execute binary file %s: cannot execute binary file: %s %s: cannot execute: %s %s: cannot get limit: %s %s: cannot modify limit: %s %s: cannot open temp file: %s %s: cannot open: %s %s: cannot overwrite existing file %s: cannot read: %s %s: cannot unset %s: cannot unset: readonly %s %s: circular name reference %s: command not found %s: error retrieving current directory: %s: %s
 %s: expression error
 %s: file is too large %s: file not found %s: first non-whitespace character is not `"' %s: hash table empty
 %s: history expansion failed %s: host unknown %s: illegal option -- %c
 %s: inlib failed %s: integer expression expected %s: invalid action name %s: invalid argument %s: invalid array origin %s: invalid associative array key %s: invalid callback quantum %s: invalid file descriptor specification %s: invalid limit argument %s: invalid line count %s: invalid option %s: invalid option name %s: invalid service %s: invalid shell option name %s: invalid signal specification %s: invalid timeout specification %s: invalid variable name for name reference %s: is a directory %s: job %d already in background %s: job has terminated %s: line %d:  %s: missing colon separator %s: nameref variable self references not allowed %s: no completion specification %s: no job control %s: no such job %s: not a function %s: not a regular file %s: not a shell builtin %s: not an array variable %s: not an indexed array %s: not dynamically loaded %s: not found %s: numeric argument required %s: option requires an argument %s: option requires an argument -- %c
 %s: parameter null or not set %s: readonly function %s: readonly variable %s: reference variable cannot be an array %s: restricted %s: restricted: cannot redirect output %s: restricted: cannot specify `/' in command names %s: substring expression < 0 %s: unary operator expected %s: unbound variable %s: usage:  %s: variable may not be assigned value (( expression )) (core dumped)  (wd now: %s)
 . filename [arguments] /dev/(tcp|udp)/host/port not supported without networking /tmp must be a valid directory name : <no current directory> ABORT instruction Aborting... Add directories to stack.
    
    Adds a directory to the top of the directory stack, or rotates
    the stack, making the new top of the stack the current working
    directory.  With no arguments, exchanges the top two directories.
    
    Options:
      -n	Suppresses the normal change of directory when adding
    	directories to the stack, so only the stack is manipulated.
    
    Arguments:
      +N	Rotates the stack so that the Nth directory (counting
    	from the left of the list shown by `dirs', starting with
    	zero) is at the top.
    
      -N	Rotates the stack so that the Nth directory (counting
    	from the right of the list shown by `dirs', starting with
    	zero) is at the top.
    
      dir	Adds DIR to the directory stack at the top, making it the
    	new current working directory.
    
    The `dirs' builtin displays the directory stack.
    
    Exit Status:
    Returns success unless an invalid argument is supplied or the directory
    change fails. Adds a directory to the top of the directory stack, or rotates
    the stack, making the new top of the stack the current working
    directory.  With no arguments, exchanges the top two directories.
    
    Options:
      -n	Suppresses the normal change of directory when adding
    	directories to the stack, so only the stack is manipulated.
    
    Arguments:
      +N	Rotates the stack so that the Nth directory (counting
    	from the left of the list shown by `dirs', starting with
    	zero) is at the top.
    
      -N	Rotates the stack so that the Nth directory (counting
    	from the right of the list shown by `dirs', starting with
    	zero) is at the top.
    
      dir	Adds DIR to the directory stack at the top, making it the
    	new current working directory.
    
    The `dirs' builtin displays the directory stack. Alarm (profile) Alarm (virtual) Alarm clock Arithmetic for loop.
    
    Equivalent to
    	(( EXP1 ))
    	while (( EXP2 )); do
    		COMMANDS
    		(( EXP3 ))
    	done
    EXP1, EXP2, and EXP3 are arithmetic expressions.  If any expression is
    omitted, it behaves as if it evaluates to 1.
    
    Exit Status:
    Returns the status of the last command executed. BPT trace/trap Bad system call Bogus signal Broken pipe Bus error CPU limit Change the shell working directory.
    
    Change the current directory to DIR.  The default DIR is the value of the
    HOME shell variable.
    
    The variable CDPATH defines the search path for the directory containing
    DIR.  Alternative directory names in CDPATH are separated by a colon (:).
    A null directory name is the same as the current directory.  If DIR begins
    with a slash (/), then CDPATH is not used.
    
    If the directory is not found, and the shell option `cdable_vars' is set,
    the word is assumed to be  a variable name.  If that variable has a value,
    its value is used for DIR.
    
    Options:
        -L	force symbolic links to be followed: resolve symbolic links in
    	DIR after processing instances of `..'
        -P	use the physical directory structure without following symbolic
    	links: resolve symbolic links in DIR before processing instances
    	of `..'
        -e	if the -P option is supplied, and the current working directory
    	cannot be determined successfully, exit with a non-zero status
        -@  on systems that support it, present a file with extended attributes
            as a directory containing the file attributes
    
    The default is to follow symbolic links, as if `-L' were specified.
    `..' is processed by removing the immediately previous pathname component
    back to a slash or the beginning of DIR.
    
    Exit Status:
    Returns 0 if the directory is changed, and if $PWD is set successfully when
    -P is used; non-zero otherwise. Child death or stop Common shell variable names and usage.
    
    BASH_VERSION	Version information for this Bash.
    CDPATH	A colon-separated list of directories to search
    		for directories given as arguments to `cd'.
    GLOBIGNORE	A colon-separated list of patterns describing filenames to
    		be ignored by pathname expansion.
    HISTFILE	The name of the file where your command history is stored.
    HISTFILESIZE	The maximum number of lines this file can contain.
    HISTSIZE	The maximum number of history lines that a running
    		shell can access.
    HOME	The complete pathname to your login directory.
    HOSTNAME	The name of the current host.
    HOSTTYPE	The type of CPU this version of Bash is running under.
    IGNOREEOF	Controls the action of the shell on receipt of an EOF
    		character as the sole input.  If set, then the value
    		of it is the number of EOF characters that can be seen
    		in a row on an empty line before the shell will exit
    		(default 10).  When unset, EOF signifies the end of input.
    MACHTYPE	A string describing the current system Bash is running on.
    MAILCHECK	How often, in seconds, Bash checks for new mail.
    MAILPATH	A colon-separated list of filenames which Bash checks
    		for new mail.
    OSTYPE	The version of Unix this version of Bash is running on.
    PATH	A colon-separated list of directories to search when
    		looking for commands.
    PROMPT_COMMAND	A command to be executed before the printing of each
    		primary prompt.
    PS1		The primary prompt string.
    PS2		The secondary prompt string.
    PWD		The full pathname of the current directory.
    SHELLOPTS	A colon-separated list of enabled shell options.
    TERM	The name of the current terminal type.
    TIMEFORMAT	The output format for timing statistics displayed by the
    		`time' reserved word.
    auto_resume	Non-null means a command word appearing on a line by
    		itself is first looked for in the list of currently
    		stopped jobs.  If found there, that job is foregrounded.
    		A value of `exact' means that the command word must
    		exactly match a command in the list of stopped jobs.  A
    		value of `substring' means that the command word must
    		match a substring of the job.  Any other value means that
    		the command must be a prefix of a stopped job.
    histchars	Characters controlling history expansion and quick
    		substitution.  The first character is the history
    		substitution character, usually `!'.  The second is
    		the `quick substitution' character, usually `^'.  The
    		third is the `history comment' character, usually `#'.
    HISTIGNORE	A colon-separated list of patterns used to decide which
    		commands should be saved on the history list.
 Continue Copyright (C) 2012 Free Software Foundation, Inc. Copyright (C) 2013 Free Software Foundation, Inc. Create a coprocess named NAME.
    
    Execute COMMAND asynchronously, with the standard output and standard
    input of the command connected via a pipe to file descriptors assigned
    to indices 0 and 1 of an array variable NAME in the executing shell.
    The default NAME is "COPROC".
    
    Exit Status:
    Returns the exit status of COMMAND. Define local variables.
    
    Create a local variable called NAME, and give it VALUE.  OPTION can
    be any option accepted by `declare'.
    
    Local variables can only be used within a function; they are visible
    only to the function where they are defined and its children.
    
    Exit Status:
    Returns success unless an invalid option is supplied, a variable
    assignment error occurs, or the shell is not executing a function. Define or display aliases.
    
    Without arguments, `alias' prints the list of aliases in the reusable
    form `alias NAME=VALUE' on standard output.
    
    Otherwise, an alias is defined for each NAME whose VALUE is given.
    A trailing space in VALUE causes the next word to be checked for
    alias substitution when the alias is expanded.
    
    Options:
      -p	Print all defined aliases in a reusable format
    
    Exit Status:
    alias returns true unless a NAME is supplied for which no alias has been
    defined. Define shell function.
    
    Create a shell function named NAME.  When invoked as a simple command,
    NAME runs COMMANDs in the calling shell's context.  When NAME is invoked,
    the arguments are passed to the function as $1...$n, and the function's
    name is in $FUNCNAME.
    
    Exit Status:
    Returns success unless NAME is readonly. Display directory stack.
    
    Display the list of currently remembered directories.  Directories
    find their way onto the list with the `pushd' command; you can get
    back up through the list with the `popd' command.
    
    Options:
      -c	clear the directory stack by deleting all of the elements
      -l	do not print tilde-prefixed versions of directories relative
    	to your home directory
      -p	print the directory stack with one entry per line
      -v	print the directory stack with one entry per line prefixed
    	with its position in the stack
    
    Arguments:
      +N	Displays the Nth entry counting from the left of the list shown by
    	dirs when invoked without options, starting with zero.
    
      -N	Displays the Nth entry counting from the right of the list shown by
    	dirs when invoked without options, starting with zero.
    
    Exit Status:
    Returns success unless an invalid option is supplied or an error occurs. Display information about builtin commands.
    
    Displays brief summaries of builtin commands.  If PATTERN is
    specified, gives detailed help on all commands matching PATTERN,
    otherwise the list of help topics is printed.
    
    Options:
      -d	output short description for each topic
      -m	display usage in pseudo-manpage format
      -s	output only a short usage synopsis for each topic matching
    	PATTERN
    
    Arguments:
      PATTERN	Pattern specifiying a help topic
    
    Exit Status:
    Returns success unless PATTERN is not found or an invalid option is given. Display information about command type.
    
    For each NAME, indicate how it would be interpreted if used as a
    command name.
    
    Options:
      -a	display all locations containing an executable named NAME;
    	includes aliases, builtins, and functions, if and only if
    	the `-p' option is not also used
      -f	suppress shell function lookup
      -P	force a PATH search for each NAME, even if it is an alias,
    	builtin, or function, and returns the name of the disk file
    	that would be executed
      -p	returns either the name of the disk file that would be executed,
    	or nothing if `type -t NAME' would not return `file'.
      -t	output a single word which is one of `alias', `keyword',
    	`function', `builtin', `file' or `', if NAME is an alias, shell
    	reserved word, shell function, shell builtin, disk file, or not
    	found, respectively
    
    Arguments:
      NAME	Command name to be interpreted.
    
    Exit Status:
    Returns success if all of the NAMEs are found; fails if any are not found. Display or execute commands from the history list.
    
    fc is used to list or edit and re-execute commands from the history list.
    FIRST and LAST can be numbers specifying the range, or FIRST can be a
    string, which means the most recent command beginning with that
    string.
    
    Options:
      -e ENAME	select which editor to use.  Default is FCEDIT, then EDITOR,
    		then vi
      -l 	list lines instead of editing
      -n	omit line numbers when listing
      -r	reverse the order of the lines (newest listed first)
    
    With the `fc -s [pat=rep ...] [command]' format, COMMAND is
    re-executed after the substitution OLD=NEW is performed.
    
    A useful alias to use with this is r='fc -s', so that typing `r cc'
    runs the last command beginning with `cc' and typing `r' re-executes
    the last command.
    
    Exit Status:
    Returns success or status of executed command; non-zero if an error occurs. Display or manipulate the history list.
    
    Display the history list with line numbers, prefixing each modified
    entry with a `*'.  An argument of N lists only the last N entries.
    
    Options:
      -c	clear the history list by deleting all of the entries
      -d offset	delete the history entry at offset OFFSET.
    
      -a	append history lines from this session to the history file
      -n	read all history lines not already read from the history file
      -r	read the history file and append the contents to the history
    	list
      -w	write the current history to the history file
    	and append them to the history list
    
      -p	perform history expansion on each ARG and display the result
    	without storing it in the history list
      -s	append the ARGs to the history list as a single entry
    
    If FILENAME is given, it is used as the history file.  Otherwise,
    if $HISTFILE has a value, that is used, else ~/.bash_history.
    
    If the $HISTTIMEFORMAT variable is set and not null, its value is used
    as a format string for strftime(3) to print the time stamp associated
    with each displayed history entry.  No time stamps are printed otherwise.
    
    Exit Status:
    Returns success unless an invalid option is given or an error occurs. Display or set file mode mask.
    
    Sets the user file-creation mask to MODE.  If MODE is omitted, prints
    the current value of the mask.
    
    If MODE begins with a digit, it is interpreted as an octal number;
    otherwise it is a symbolic mode string like that accepted by chmod(1).
    
    Options:
      -p	if MODE is omitted, output in a form that may be reused as input
      -S	makes the output symbolic; otherwise an octal number is output
    
    Exit Status:
    Returns success unless MODE is invalid or an invalid option is given. Display process times.
    
    Prints the accumulated user and system times for the shell and all of its
    child processes.
    
    Exit Status:
    Always succeeds. Display the list of currently remembered directories.  Directories
    find their way onto the list with the `pushd' command; you can get
    back up through the list with the `popd' command.
    
    Options:
      -c	clear the directory stack by deleting all of the elements
      -l	do not print tilde-prefixed versions of directories relative
    	to your home directory
      -p	print the directory stack with one entry per line
      -v	print the directory stack with one entry per line prefixed
    	with its position in the stack
    
    Arguments:
      +N	Displays the Nth entry counting from the left of the list shown by
    	dirs when invoked without options, starting with zero.
    
      -N	Displays the Nth entry counting from the right of the list shown by
	dirs when invoked without options, starting with zero. Done Done(%d) EMT instruction Enable and disable shell builtins.
    
    Enables and disables builtin shell commands.  Disabling allows you to
    execute a disk command which has the same name as a shell builtin
    without using a full pathname.
    
    Options:
      -a	print a list of builtins showing whether or not each is enabled
      -n	disable each NAME or display a list of disabled builtins
      -p	print the list of builtins in a reusable format
      -s	print only the names of Posix `special' builtins
    
    Options controlling dynamic loading:
      -f	Load builtin NAME from shared object FILENAME
      -d	Remove a builtin loaded with -f
    
    Without options, each NAME is enabled.
    
    To use the `test' found in $PATH instead of the shell builtin
    version, type `enable -n test'.
    
    Exit Status:
    Returns success unless NAME is not a shell builtin or an error occurs. Evaluate arithmetic expression.
    
    The EXPRESSION is evaluated according to the rules for arithmetic
    evaluation.  Equivalent to "let EXPRESSION".
    
    Exit Status:
    Returns 1 if EXPRESSION evaluates to 0; returns 0 otherwise. Evaluate arithmetic expressions.
    
    Evaluate each ARG as an arithmetic expression.  Evaluation is done in
    fixed-width integers with no check for overflow, though division by 0
    is trapped and flagged as an error.  The following list of operators is
    grouped into levels of equal-precedence operators.  The levels are listed
    in order of decreasing precedence.
    
    	id++, id--	variable post-increment, post-decrement
    	++id, --id	variable pre-increment, pre-decrement
    	-, +		unary minus, plus
    	!, ~		logical and bitwise negation
    	**		exponentiation
    	*, /, %		multiplication, division, remainder
    	+, -		addition, subtraction
    	<<, >>		left and right bitwise shifts
    	<=, >=, <, >	comparison
    	==, !=		equality, inequality
    	&		bitwise AND
    	^		bitwise XOR
    	|		bitwise OR
    	&&		logical AND
    	||		logical OR
    	expr ? expr : expr
    			conditional operator
    	=, *=, /=, %=,
    	+=, -=, <<=, >>=,
    	&=, ^=, |=	assignment
    
    Shell variables are allowed as operands.  The name of the variable
    is replaced by its value (coerced to a fixed-width integer) within
    an expression.  The variable need not have its integer attribute
    turned on to be used in an expression.
    
    Operators are evaluated in order of precedence.  Sub-expressions in
    parentheses are evaluated first and may override the precedence
    rules above.
    
    Exit Status:
    If the last ARG evaluates to 0, let returns 1; let returns 0 otherwise. Evaluate conditional expression.
    
    This is a synonym for the "test" builtin, but the last argument must
    be a literal `]', to match the opening `['. Execute a simple command or display information about commands.
    
    Runs COMMAND with ARGS suppressing  shell function lookup, or display
    information about the specified COMMANDs.  Can be used to invoke commands
    on disk when a function with the same name exists.
    
    Options:
      -p	use a default value for PATH that is guaranteed to find all of
    	the standard utilities
      -v	print a description of COMMAND similar to the `type' builtin
      -V	print a more verbose description of each COMMAND
    
    Exit Status:
    Returns exit status of COMMAND, or failure if COMMAND is not found. Execute arguments as a shell command.
    
    Combine ARGs into a single string, use the result as input to the shell,
    and execute the resulting commands.
    
    Exit Status:
    Returns exit status of command or success if command is null. Execute commands as long as a test does not succeed.
    
    Expand and execute COMMANDS as long as the final command in the
    `until' COMMANDS has an exit status which is not zero.
    
    Exit Status:
    Returns the status of the last command executed. Execute commands as long as a test succeeds.
    
    Expand and execute COMMANDS as long as the final command in the
    `while' COMMANDS has an exit status of zero.
    
    Exit Status:
    Returns the status of the last command executed. Execute commands based on conditional.
    
    The `if COMMANDS' list is executed.  If its exit status is zero, then the
    `then COMMANDS' list is executed.  Otherwise, each `elif COMMANDS' list is
    executed in turn, and if its exit status is zero, the corresponding
    `then COMMANDS' list is executed and the if command completes.  Otherwise,
    the `else COMMANDS' list is executed, if present.  The exit status of the
    entire construct is the exit status of the last command executed, or zero
    if no condition tested true.
    
    Exit Status:
    Returns the status of the last command executed. Execute commands based on pattern matching.
    
    Selectively execute COMMANDS based upon WORD matching PATTERN.  The
    `|' is used to separate multiple patterns.
    
    Exit Status:
    Returns the status of the last command executed. Execute commands for each member in a list.
    
    The `for' loop executes a sequence of commands for each member in a
    list of items.  If `in WORDS ...;' is not present, then `in "$@"' is
    assumed.  For each element in WORDS, NAME is set to that element, and
    the COMMANDS are executed.
    
    Exit Status:
    Returns the status of the last command executed. Execute commands from a file in the current shell.
    
    Read and execute commands from FILENAME in the current shell.  The
    entries in $PATH are used to find the directory containing FILENAME.
    If any ARGUMENTS are supplied, they become the positional parameters
    when FILENAME is executed.
    
    Exit Status:
    Returns the status of the last command executed in FILENAME; fails if
    FILENAME cannot be read. Execute shell builtins.
    
    Execute SHELL-BUILTIN with arguments ARGs without performing command
    lookup.  This is useful when you wish to reimplement a shell builtin
    as a shell function, but need to execute the builtin within the function.
    
    Exit Status:
    Returns the exit status of SHELL-BUILTIN, or false if SHELL-BUILTIN is
    not a shell builtin.. Exit %d Exit a login shell.
    
    Exits a login shell with exit status N.  Returns an error if not executed
    in a login shell. Exit for, while, or until loops.
    
    Exit a FOR, WHILE or UNTIL loop.  If N is specified, break N enclosing
    loops.
    
    Exit Status:
    The exit status is 0 unless N is not greater than or equal to 1. Exit the shell.
    
    Exits the shell with a status of N.  If N is omitted, the exit status
    is that of the last command executed. File limit Floating point exception GNU bash, version %s (%s)
 GNU bash, version %s-(%s)
 GNU long options:
 Group commands as a unit.
    
    Run a set of commands in a group.  This is one way to redirect an
    entire set of commands.
    
    Exit Status:
    Returns the status of the last command executed. HFT input data pending HFT monitor mode granted HFT monitor mode retracted HFT sound sequence has completed HOME not set Hangup I have no name! I/O ready Illegal instruction Information request Interrupt Killed License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
 Move job to the foreground.
    
    Place the job identified by JOB_SPEC in the foreground, making it the
    current job.  If JOB_SPEC is not present, the shell's notion of the
    current job is used.
    
    Exit Status:
    Status of command placed in foreground, or failure if an error occurs. Move jobs to the background.
    
    Place the jobs identified by each JOB_SPEC in the background, as if they
    had been started with `&'.  If JOB_SPEC is not present, the shell's notion
    of the current job is used.
    
    Exit Status:
    Returns success unless job control is not enabled or an error occurs. Null command.
    
    No effect; the command does nothing.
    
    Exit Status:
    Always succeeds. OLDPWD not set Parse option arguments.
    
    Getopts is used by shell procedures to parse positional parameters
    as options.
    
    OPTSTRING contains the option letters to be recognized; if a letter
    is followed by a colon, the option is expected to have an argument,
    which should be separated from it by white space.
    
    Each time it is invoked, getopts will place the next option in the
    shell variable $name, initializing name if it does not exist, and
    the index of the next argument to be processed into the shell
    variable OPTIND.  OPTIND is initialized to 1 each time the shell or
    a shell script is invoked.  When an option requires an argument,
    getopts places that argument into the shell variable OPTARG.
    
    getopts reports errors in one of two ways.  If the first character
    of OPTSTRING is a colon, getopts uses silent error reporting.  In
    this mode, no error messages are printed.  If an invalid option is
    seen, getopts places the option character found into OPTARG.  If a
    required argument is not found, getopts places a ':' into NAME and
    sets OPTARG to the option character found.  If getopts is not in
    silent mode, and an invalid option is seen, getopts places '?' into
    NAME and unsets OPTARG.  If a required argument is not found, a '?'
    is placed in NAME, OPTARG is unset, and a diagnostic message is
    printed.
    
    If the shell variable OPTERR has the value 0, getopts disables the
    printing of error messages, even if the first character of
    OPTSTRING is not a colon.  OPTERR has the value 1 by default.
    
    Getopts normally parses the positional parameters ($0 - $9), but if
    more arguments are given, they are parsed instead.
    
    Exit Status:
    Returns success if an option is found; fails if the end of options is
    encountered or an error occurs. Print the name of the current working directory.
    
    Options:
      -L	print the value of $PWD if it names the current working
    	directory
      -P	print the physical directory, without any symbolic links
    
    By default, `pwd' behaves as if `-L' were specified.
    
    Exit Status:
    Returns 0 unless an invalid option is given or the current directory
    cannot be read. Quit Read lines from a file into an array variable.
    
    A synonym for `mapfile'. Record lock Remove directories from stack.
    
    Removes entries from the directory stack.  With no arguments, removes
    the top directory from the stack, and changes to the new top directory.
    
    Options:
      -n	Suppresses the normal change of directory when removing
    	directories from the stack, so only the stack is manipulated.
    
    Arguments:
      +N	Removes the Nth entry counting from the left of the list
    	shown by `dirs', starting with zero.  For example: `popd +0'
    	removes the first directory, `popd +1' the second.
    
      -N	Removes the Nth entry counting from the right of the list
    	shown by `dirs', starting with zero.  For example: `popd -0'
    	removes the last directory, `popd -1' the next to last.
    
    The `dirs' builtin displays the directory stack.
    
    Exit Status:
    Returns success unless an invalid argument is supplied or the directory
    change fails. Remove each NAME from the list of defined aliases.
    
    Options:
      -a	remove all alias definitions.
    
    Return success unless a NAME is not an existing alias. Remove jobs from current shell.
    
    Removes each JOBSPEC argument from the table of active jobs.  Without
    any JOBSPECs, the shell uses its notion of the current job.
    
    Options:
      -a	remove all jobs if JOBSPEC is not supplied
      -h	mark each JOBSPEC so that SIGHUP is not sent to the job if the
    	shell receives a SIGHUP
      -r	remove only running jobs
    
    Exit Status:
    Returns success unless an invalid option or JOBSPEC is given. Removes entries from the directory stack.  With no arguments, removes
    the top directory from the stack, and changes to the new top directory.
    
    Options:
      -n	Suppresses the normal change of directory when removing
    	directories from the stack, so only the stack is manipulated.
    
    Arguments:
      +N	Removes the Nth entry counting from the left of the list
    	shown by `dirs', starting with zero.  For example: `popd +0'
    	removes the first directory, `popd +1' the second.
    
      -N	Removes the Nth entry counting from the right of the list
    	shown by `dirs', starting with zero.  For example: `popd -0'
    	removes the last directory, `popd -1' the next to last.
    
    The `dirs' builtin displays the directory stack. Replace the shell with the given command.
    
    Execute COMMAND, replacing this shell with the specified program.
    ARGUMENTS become the arguments to COMMAND.  If COMMAND is not specified,
    any redirections take effect in the current shell.
    
    Options:
      -a name	pass NAME as the zeroth argument to COMMAND
      -c		execute COMMAND with an empty environment
      -l		place a dash in the zeroth argument to COMMAND
    
    If the command cannot be executed, a non-interactive shell exits, unless
    the shell option `execfail' is set.
    
    Exit Status:
    Returns success unless COMMAND is not found or a redirection error occurs. Report time consumed by pipeline's execution.
    
    Execute PIPELINE and print a summary of the real time, user CPU time,
    and system CPU time spent executing PIPELINE when it terminates.
    
    Options:
      -p	print the timing summary in the portable Posix format
    
    The value of the TIMEFORMAT variable is used as the output format.
    
    Exit Status:
    The return status is the return status of PIPELINE. Resume for, while, or until loops.
    
    Resumes the next iteration of the enclosing FOR, WHILE or UNTIL loop.
    If N is specified, resumes the Nth enclosing loop.
    
    Exit Status:
    The exit status is 0 unless N is not greater than or equal to 1. Return a successful result.
    
    Exit Status:
    Always succeeds. Return an unsuccessful result.
    
    Exit Status:
    Always fails. Return from a shell function.
    
    Causes a function or sourced script to exit with the return value
    specified by N.  If N is omitted, the return status is that of the
    last command executed within the function or script.
    
    Exit Status:
    Returns N, or failure if the shell is not executing a function or script. Return the context of the current subroutine call.
    
    Without EXPR, returns "$line $filename".  With EXPR, returns
    "$line $subroutine $filename"; this extra information can be used to
    provide a stack trace.
    
    The value of EXPR indicates how many call frames to go back before the
    current one; the top frame is frame 0.
    
    Exit Status:
    Returns 0 unless the shell is not executing a shell function or EXPR
    is invalid. Returns the context of the current subroutine call.
    
    Without EXPR, returns  Running Segmentation fault Select words from a list and execute commands.
    
    The WORDS are expanded, generating a list of words.  The
    set of expanded words is printed on the standard error, each
    preceded by a number.  If `in WORDS' is not present, `in "$@"'
    is assumed.  The PS3 prompt is then displayed and a line read
    from the standard input.  If the line consists of the number
    corresponding to one of the displayed words, then NAME is set
    to that word.  If the line is empty, WORDS and the prompt are
    redisplayed.  If EOF is read, the command completes.  Any other
    value read causes NAME to be set to null.  The line read is saved
    in the variable REPLY.  COMMANDS are executed after each selection
    until a break command is executed.
    
    Exit Status:
    Returns the status of the last command executed. Send a signal to a job.
    
    Send the processes identified by PID or JOBSPEC the signal named by
    SIGSPEC or SIGNUM.  If neither SIGSPEC nor SIGNUM is present, then
    SIGTERM is assumed.
    
    Options:
      -s sig	SIG is a signal name
      -n sig	SIG is a signal number
      -l	list the signal names; if arguments follow `-l' they are
    	assumed to be signal numbers for which names should be listed
    
    Kill is a shell builtin for two reasons: it allows job IDs to be used
    instead of process IDs, and allows processes to be killed if the limit
    on processes that you can create is reached.
    
    Exit Status:
    Returns success unless an invalid option is given or an error occurs. Set Readline key bindings and variables.
    
    Bind a key sequence to a Readline function or a macro, or set a
    Readline variable.  The non-option argument syntax is equivalent to
    that found in ~/.inputrc, but must be passed as a single argument:
    e.g., bind '"\C-x\C-r": re-read-init-file'.
    
    Options:
      -m  keymap         Use KEYMAP as the keymap for the duration of this
                         command.  Acceptable keymap names are emacs,
                         emacs-standard, emacs-meta, emacs-ctlx, vi, vi-move,
                         vi-command, and vi-insert.
      -l                 List names of functions.
      -P                 List function names and bindings.
      -p                 List functions and bindings in a form that can be
                         reused as input.
      -S                 List key sequences that invoke macros and their values
      -s                 List key sequences that invoke macros and their values
                         in a form that can be reused as input.
      -V                 List variable names and values
      -v                 List variable names and values in a form that can
                         be reused as input.
      -q  function-name  Query about which keys invoke the named function.
      -u  function-name  Unbind all keys which are bound to the named function.
      -r  keyseq         Remove the binding for KEYSEQ.
      -f  filename       Read key bindings from FILENAME.
      -x  keyseq:shell-command	Cause SHELL-COMMAND to be executed when
    				KEYSEQ is entered.
      -X		     List key sequences bound with -x and associated commands
                         in a form that can be reused as input.
    
    Exit Status:
    bind returns 0 unless an unrecognized option is given or an error occurs. Set and unset shell options.
    
    Change the setting of each shell option OPTNAME.  Without any option
    arguments, list all shell options with an indication of whether or not each
    is set.
    
    Options:
      -o	restrict OPTNAMEs to those defined for use with `set -o'
      -p	print each shell option with an indication of its status
      -q	suppress output
      -s	enable (set) each OPTNAME
      -u	disable (unset) each OPTNAME
    
    Exit Status:
    Returns success if OPTNAME is enabled; fails if an invalid option is
    given or OPTNAME is disabled. Set export attribute for shell variables.
    
    Marks each NAME for automatic export to the environment of subsequently
    executed commands.  If VALUE is supplied, assign VALUE before exporting.
    
    Options:
      -f	refer to shell functions
      -n	remove the export property from each NAME
      -p	display a list of all exported variables and functions
    
    An argument of `--' disables further option processing.
    
    Exit Status:
    Returns success unless an invalid option is given or NAME is invalid. Set variable values and attributes.
    
    Declare variables and give them attributes.  If no NAMEs are given,
    display the attributes and values of all variables.
    
    Options:
      -f	restrict action or display to function names and definitions
      -F	restrict display to function names only (plus line number and
    	source file when debugging)
      -g	create global variables when used in a shell function; otherwise
    	ignored
      -p	display the attributes and value of each NAME
    
    Options which set attributes:
      -a	to make NAMEs indexed arrays (if supported)
      -A	to make NAMEs associative arrays (if supported)
      -i	to make NAMEs have the `integer' attribute
      -l	to convert NAMEs to lower case on assignment
      -n	make NAME a reference to the variable named by its value
      -r	to make NAMEs readonly
      -t	to make NAMEs have the `trace' attribute
      -u	to convert NAMEs to upper case on assignment
      -x	to make NAMEs export
    
    Using `+' instead of `-' turns off the given attribute.
    
    Variables with the integer attribute have arithmetic evaluation (see
    the `let' command) performed when the variable is assigned a value.
    
    When used in a function, `declare' makes NAMEs local, as with the `local'
    command.  The `-g' option suppresses this behavior.
    
    Exit Status:
    Returns success unless an invalid option is supplied or a variable
    assignment error occurs. Set variable values and attributes.
    
    Obsolete.  See `help declare'. Shell commands matching keyword ` Shell commands matching keywords ` Shell options:
 Shift positional parameters.
    
    Rename the positional parameters $N+1,$N+2 ... to $1,$2 ...  If N is
    not given, it is assumed to be 1.
    
    Exit Status:
    Returns success unless N is negative or greater than $#. Signal %d Stopped Stopped (signal) Stopped (tty input) Stopped (tty output) Stopped(%s) Suspend shell execution.
    
    Suspend the execution of this shell until it receives a SIGCONT signal.
    Unless forced, login shells cannot be suspended.
    
    Options:
      -f	force the suspend, even if the shell is a login shell
    
    Exit Status:
    Returns success unless job control is not enabled or an error occurs. TIMEFORMAT: `%c': invalid format character Terminated The mail in %s has been read
 There are running jobs.
 There are stopped jobs.
 There is NO WARRANTY, to the extent permitted by law. These shell commands are defined internally.  Type `help' to see this list.
Type `help name' to find out more about the function `name'.
Use `info bash' to find out more about the shell in general.
Use `man -k' or `info' to find out more about commands not in this list.

A star (*) next to a name means that the command is disabled.

 This is free software; you are free to change and redistribute it. Trap signals and other events.
    
    Defines and activates handlers to be run when the shell receives signals
    or other conditions.
    
    ARG is a command to be read and executed when the shell receives the
    signal(s) SIGNAL_SPEC.  If ARG is absent (and a single SIGNAL_SPEC
    is supplied) or `-', each specified signal is reset to its original
    value.  If ARG is the null string each SIGNAL_SPEC is ignored by the
    shell and by the commands it invokes.
    
    If a SIGNAL_SPEC is EXIT (0) ARG is executed on exit from the shell.  If
    a SIGNAL_SPEC is DEBUG, ARG is executed before every simple command.  If
    a SIGNAL_SPEC is RETURN, ARG is executed each time a shell function or a
    script run by the . or source builtins finishes executing.  A SIGNAL_SPEC
    of ERR means to execute ARG each time a command's failure would cause the
    shell to exit when the -e option is enabled.
    
    If no arguments are supplied, trap prints the list of commands associated
    with each signal.
    
    Options:
      -l	print a list of signal names and their corresponding numbers
      -p	display the trap commands associated with each SIGNAL_SPEC
    
    Each SIGNAL_SPEC is either a signal name in <signal.h> or a signal number.
    Signal names are case insensitive and the SIG prefix is optional.  A
    signal may be sent to the shell with "kill -signal $$".
    
    Exit Status:
    Returns success unless a SIGSPEC is invalid or an invalid option is given. Type `%s -c "help set"' for more information about shell options.
 Type `%s -c help' for more information about shell builtin commands.
 Unknown Signal # Unknown Signal #%d Unknown error Unknown status Urgent IO condition Usage:	%s [GNU long option] [option] ...
	%s [GNU long option] [option] script-file ...
 Use "%s" to leave the shell.
 Use the `bashbug' command to report bugs.
 User signal 1 User signal 2 Window changed Write arguments to the standard output.
    
    Display the ARGs on the standard output followed by a newline.
    
    Options:
      -n	do not append a newline
    
    Exit Status:
    Returns success unless a write error occurs. Write arguments to the standard output.
    
    Display the ARGs, separated by a single space character and followed by a
    newline, on the standard output.
    
    Options:
      -n	do not append a newline
      -e	enable interpretation of the following backslash escapes
      -E	explicitly suppress interpretation of backslash escapes
    
    `echo' interprets the following backslash-escaped characters:
      \a	alert (bell)
      \b	backspace
      \c	suppress further output
      \e	escape character
      \E	escape character
      \f	form feed
      \n	new line
      \r	carriage return
      \t	horizontal tab
      \v	vertical tab
      \\	backslash
      \0nnn	the character whose ASCII code is NNN (octal).  NNN can be
    	0 to 3 octal digits
      \xHH	the eight-bit character whose value is HH (hexadecimal).  HH
    	can be one or two hex digits
    
    Exit Status:
    Returns success unless a write error occurs. You have mail in $_ You have new mail in $_ [ arg... ] [[ expression ]] `%c': bad command `%c': invalid format character `%c': invalid symbolic mode character `%c': invalid symbolic mode operator `%c': invalid time format specification `%s': cannot unbind `%s': invalid alias name `%s': invalid keymap name `%s': is a special builtin `%s': missing format character `%s': not a pid or valid job spec `%s': not a valid identifier `%s': unknown function name `)' expected `)' expected, found %s `:' expected for conditional expression add_process: pid %5ld (%s) marked as still alive add_process: process %5ld (%s) in the_pipeline alias [-p] [name[=value] ... ] all_local_variables: no function context at current scope argument argument expected array variable support required attempted assignment to non-variable bad array subscript bad command type bad connector bad jump bad substitution: no closing "`" in %s bad substitution: no closing `%s' in %s bash_execute_unix_command: cannot find keymap for command bg [job_spec ...] bind [-lpsvPSVX] [-m keymap] [-f filename] [-q name] [-u name] [-r keyseq] [-x keyseq:shell-command] [keyseq:readline-function or readline-command] brace expansion: cannot allocate memory for %s brace expansion: failed to allocate memory for %d elements brace expansion: failed to allocate memory for `%s' break [n] bug: bad expassign token builtin [shell-builtin [arg ...]] caller [expr] can only `return' from a function or sourced script can only be used in a function cannot allocate new file descriptor for bash input from fd %d cannot create temp file for here-document: %s cannot duplicate fd %d to fd %d cannot duplicate named pipe %s as fd %d cannot find %s in shared object %s: %s cannot make child for command substitution cannot make child for process substitution cannot make pipe for command substitution cannot make pipe for process substitution cannot open named pipe %s for reading cannot open named pipe %s for writing cannot open shared object %s: %s cannot redirect standard input from /dev/null: %s cannot reset nodelay mode for fd %d cannot set and unset shell options simultaneously cannot set terminal process group (%d) cannot simultaneously unset a function and a variable cannot suspend cannot suspend a login shell cannot use `-f' to make functions cannot use more than one of -anrw case WORD in [PATTERN [| PATTERN]...) COMMANDS ;;]... esac cd [-L|[-P [-e]] [-@]] [dir] child setpgid (%ld to %ld) command [-pVv] command [arg ...] command_substitute: cannot duplicate pipe as fd 1 compgen [-abcdefgjksuv] [-o option]  [-A action] [-G globpat] [-W wordlist]  [-F function] [-C command] [-X filterpat] [-P prefix] [-S suffix] [word] complete [-abcdefgjksuv] [-pr] [-DE] [-o option] [-A action] [-G globpat] [-W wordlist]  [-F function] [-C command] [-X filterpat] [-P prefix] [-S suffix] [name ...] completion: function `%s' not found compopt [-o|+o option] [-DE] [name ...] conditional binary operator expected continue [n] coproc [NAME] command [redirections] could not find /tmp, please create! cprintf: `%c': invalid format character current declare [-aAfFgilnrtux] [-p] [name[=value] ...] deleting stopped job %d with process group %ld describe_pid: %ld: no such pid directory stack empty directory stack index dirs [-clpv] [+N] [-N] disown [-h] [-ar] [jobspec ...] division by 0 dynamic loading not available echo [-n] [arg ...] echo [-neE] [arg ...] empty array variable name enable [-a] [-dnps] [-f filename] [name ...] error getting terminal attributes: %s error importing function definition for `%s' error setting terminal attributes: %s eval [arg ...] exec [-cl] [-a name] [command [arguments ...]] [redirection ...] exit [n] expected `)' exponent less than 0 export [-fn] [name[=value] ...] or export -p expression expected expression recursion level exceeded false fc [-e ename] [-lnr] [first] [last] or fc -s [pat=rep] [command] fg [job_spec] file descriptor out of range filename argument required for (( exp1; exp2; exp3 )); do COMMANDS; done for NAME [in WORDS ... ] ; do COMMANDS; done forked pid %d appears in running job %d format parsing problem: %s free: called with already freed block argument free: called with unallocated block argument free: start and end chunk sizes differ free: underflow detected; mh_nbytes out of range function name { COMMANDS ; } or name () { COMMANDS ; } future versions of the shell will force evaluation as an arithmetic substitution getcwd: cannot access parent directories getopts optstring name [arg] hash [-lr] [-p pathname] [-dt] [name ...] hashing disabled help [-dms] [pattern ...] here-document at line %d delimited by end-of-file (wanted `%s') history [-c] [-d offset] [n] or history -anrw [filename] or history -ps arg [arg...] history position history specification hits	command
 identifier expected after pre-increment or pre-decrement if COMMANDS; then COMMANDS; [ elif COMMANDS; then COMMANDS; ]... [ else COMMANDS; ] fi initialize_job_control: getpgrp failed initialize_job_control: line discipline initialize_job_control: setpgid invalid arithmetic base invalid base invalid character %d in exportstr for %s invalid hex number invalid number invalid octal number invalid signal number job %d started without job control job_spec [&] jobs [-lnprs] [jobspec ...] or jobs -x command [args] kill [-s sigspec | -n signum | -sigspec] pid | jobspec ... or kill -l [sigspec] last command: %s
 let arg [arg ...] limit line %d:  line editing not enabled local [option] name[=value] ... logout
 logout [n] loop count make_here_document: bad instruction type %d make_local_variable: no function context at current scope make_redirection: redirection instruction `%d' out of range malloc: block on free list clobbered malloc: failed assertion: %s
 mapfile [-n count] [-O origin] [-s count] [-t] [-u fd] [-C callback] [-c quantum] [array] migrate process to another CPU missing `)' missing `]' missing hex digit for \x missing unicode digit for \%c network operations not supported no `=' in exportstr for %s no closing `%c' in %s no command found no help topics match `%s'.  Try `help help' or `man -k %s' or `info %s'. no job control no job control in this shell no match: %s no other directory no other options allowed with `-x' not currently executing completion function not login shell: use `exit' octal number only meaningful in a `for', `while', or `until' loop pipe error pop_scope: head of shell_variables not a temporary environment scope pop_var_context: head of shell_variables not a function context pop_var_context: no global_variables context popd [-n] [+N | -N] power failure imminent print_command: bad connector `%d' printf [-v var] format [arguments] progcomp_insert: %s: NULL COMPSPEC programming error pushd [-n] [+N | -N | dir] pwd [-LP] read [-ers] [-a array] [-d delim] [-i text] [-n nchars] [-N nchars] [-p prompt] [-t timeout] [-u fd] [name ...] read error: %d: %s readarray [-n count] [-O origin] [-s count] [-t] [-u fd] [-C callback] [-c quantum] [array] readonly [-aAf] [name[=value] ...] or readonly -p realloc: called with unallocated block argument realloc: start and end chunk sizes differ realloc: underflow detected; mh_nbytes out of range recursion stack underflow redirection error: cannot duplicate fd register_alloc: %p already in table as allocated?
 register_alloc: alloc table is full with FIND_ALLOC?
 register_free: %p already in table as free?
 restricted return [n] run_pending_traps: bad value in trap_list[%d]: %p run_pending_traps: signal handler is SIG_DFL, resending %d (%s) to myself save_bash_input: buffer already exists for new fd %d select NAME [in WORDS ... ;] do COMMANDS; done set [-abefhkmnptuvxBCHP] [-o option-name] [--] [arg ...] setlocale: %s: cannot change locale (%s) setlocale: %s: cannot change locale (%s): %s setlocale: LC_ALL: cannot change locale (%s) setlocale: LC_ALL: cannot change locale (%s): %s shell level (%d) too high, resetting to 1 shift [n] shift count shopt [-pqsu] [-o] [optname ...] sigprocmask: %d: invalid operation source filename [arguments] start_pipeline: pgrp pipe suspend [-f] syntax error syntax error in conditional expression syntax error in conditional expression: unexpected token `%s' syntax error in expression syntax error near `%s' syntax error near unexpected token `%s' syntax error: `((%s))' syntax error: `;' unexpected syntax error: arithmetic expression required syntax error: invalid arithmetic operator syntax error: operand expected syntax error: unexpected end of file system crash imminent test [expr] time [-p] pipeline times too many arguments trap [-lp] [[arg] signal_spec ...] trap_handler: bad signal %d true type [-afptP] name [name ...] typeset [-aAfFgilrtux] [-p] name[=value] ... ulimit [-SHabcdefilmnpqrstuvxT] [limit] umask [-p] [-S] [mode] unalias [-a] name [name ...] unexpected EOF while looking for `]]' unexpected EOF while looking for matching `%c' unexpected EOF while looking for matching `)' unexpected argument `%s' to conditional binary operator unexpected argument `%s' to conditional unary operator unexpected argument to conditional binary operator unexpected argument to conditional unary operator unexpected token %d in conditional command unexpected token `%c' in conditional command unexpected token `%s' in conditional command unexpected token `%s', conditional binary operator expected unexpected token `%s', expected `)' unknown unknown command error unset [-f] [-v] [-n] [name ...] until COMMANDS; do COMMANDS; done value too great for base variables - Names and meanings of some shell variables wait [-n] [id ...] wait [pid ...] wait: pid %ld is not a child of this shell wait_for: No record of process %ld wait_for_job: job %d is stopped waitchld: turning on WNOHANG to avoid indefinite block warning:  warning: %s: %s warning: -C option may not work as you expect warning: -F option may not work as you expect while COMMANDS; do COMMANDS; done write error: %s xtrace fd (%d) != fileno xtrace fp (%d) xtrace_set: %d: invalid file descriptor xtrace_set: NULL file pointer { COMMANDS ; } Project-Id-Version: bash 4.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-02-11 11:19-0500
PO-Revision-Date: 2015-09-08 18:44+0000
Last-Translator: schuko24 <gerdsaenger@t-online.de>
Language-Team: German <translation-team-de@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 15:59+0000
X-Generator: Launchpad (build 18115)
 Zu lange keine Eingabe: Automatisch abgemeldet.
 	-%s oder Option »-o«
 	-ilrsD oder -c Befehl	oder -O shopt_option            (Nur Aufruf)
 
malloc: %s:%d: Speicherzusicherung verpfuscht
   (wd: %s)  (Speicherabzug geschrieben)  Zeile  $%s: Kann so nicht zuweisen. %c%c: Ungültige Option %d: Ungültiger Datei-Deskriptor: %s %s kann aufgerufen werden durch  %s hat Null-exportstr %s ist %s
 %s ist eine Funktion.
 %s ist eine von der Shell mitgelieferte Funktion.
 %s Ist ein reserviertes Schlüsselwort der Shell.
 %s ist ein Alias von »%s«.
 %s gehasht ergibt (%s)
 %s ist keiner Taste zugeordnet.
 %s ist außerhalb des Gültigkeitsbereiches. %s%s%s: %s (Fehlerverursachendes Zeichen ist »%s«). %s: %s %s: %s ist außerhalb des Gültigkeitsbereiches. %s: %s: Defekter Interpreter %s: %s: Kann nicht als Datei geöffnet werden. %s: %s: Kompatibilitätswert außerhalb des Gültigkeitsbereiches. %s: %s: Ungültiger Wert für den Rückverfolgungsdateibezeichner. %s: %s: Ein Feldbezeicher wird zum Zuweisen eines assoziativen Arrays benötigt. %s: %s:%d: Konnte nicht %lu Bytes reservieren. %s: %s:%d: Konnte nicht %lu Bytes reservieren (%lu Bytes reserviert). %s: Mehrdeutige Job-Bezeichnung. %s: Mehrdeutige Umlenkung. %s: Die Argumente müssen Prozess- oder Jobbezeichnungen sein. %s: Fehlerhafte Netzwerkspfadangabe. %s: Falsche Variablenersetzung. %s: Zweistelliger (binärer) Operator erwartet. %s: Konnte nicht %lu Bytes reservieren. %s: Konnte nicht %lu Bytes reservieren (%lu Bytes reserviert). %s: Kann fd keiner Variable zuweisen. %s: Kann einem Feldelement keine Liste zuweisen. %s: Kann nicht auf einen nicht-numerischen Index zuweisen. %s: Konvertieren von assoziativen in indizierte Arrays ist nicht möglich. %s: Kann das indizierte nicht in ein assoziatives Array umwandeln. %s: Kann die Datei %s nicht erzeugen. %s: Kann nicht löschen: %s %s: Feldvariablen können nicht auf diese Art gelöscht werden. %s: Kann die Datei nicht ausführen. %s: Kann die Binärdatei nicht ausführen: %s %s: Kann nicht ausführen: %s %s: Kann die nicht Grenze setzen: %s %s: Kann die Grenze nicht ändern: %s %s: Kann die tempräre Datei nicht öffnen: %s %s: Kann die Datei nicht öffnen: %s %s: Kann existierende Datei nicht überschreiben. %s: Nicht lesbar: %s %s: »unset« nicht möglich. %s: »unset« nicht möglich: schreibgeschützt %s %s: Namensreferenzschleife %s: Befehl nicht gefunden %s: Kann das aktuelle Verzeichnis nicht wiederfinden: %s: %s
 %s: Fehler im Ausdruck.
 %s: Die Datei ist zu groß. %s: Datei nicht gefunden. %s: Das erste Zeichen ist nicht `\'. %s: Die Hashtabelle ist leer.
 %s: Kommandoersetzung gescheitert. %s: Unbekannter Host. %s: Ungültige Option -- %c
 %s: inlib gescheitert. %s: Ganzzahliger Ausdruck erwartet. %s: Ungültige Methode. %s: Ungültiges Argument. %s:  Ungültiger Zeilenindex für den Feldbeginn. %s: Ungültiger Schlüssel für das assoziative Array. %s: Fehlerhafte Rückgabemenge %s: Ungültige Datei-Deskriptor Angabe. %s: Ungültiges Grenzwertargument. %s: Ungültige Zeilenanzahlangabe. %s: Ungültige Option %s: Ungültiger Optionsname. %s: unbekannter Dienst. %s: Ungültiger Name der Shell-Option. %s: Ungültige Signalbezeichnung. %s: Ungültige Wartezeitangebe. %s: ungültiger Variablenname für die Namensreferenz %s: ist ein Verzeichnis. %s: Prozess %d läuft schon im Hintergrund %s: Programm ist beendet. %s: Zeile %d:  %s: Fehlender Doppelpunkt. %s: nameref Selbstreferenzierung einer Variablen ist unzulässig %s: Keine Komplettierung angegeben. %s: Keine Job-Steuerung in dieser Shell. %s: Kein solcher Job. %s: Ist keine Funktion. %s: Ist keine normale Datei. %s: Ist kein Shell-Kommando. %s: Ist keine Feldvariable. %s: Ist kein indiziertes Array. %s: Ist nicht dynamisch geladen. %s: Nicht gefunden. %s: Ein numerischer Parameter ist erforderlich. %s: Die Option benötigt einen Paremeter. %s: Diese Option erfordert ein Argument -- %c
 %s: Parameter ist Null oder nicht gesetzt. %s: Schreibgeschützte Funktion. %s: Schreibgeschützte Variable. %s: Die Referenzvariable darf kein Feld sein %s: gesperrt %s: Gesperrt: Die Ausgabe darf nicht umgeleitet werden. %s: Verboten:  »/« ist in Befehlsnamen unzulässig %s: Teilstring-Ausdruck < 0. %s: Einstelliger (unärer) Operator erwartet. %s ist nicht gesetzt. %s: Gebrauch:  %s: Der Variable könnte kein Wert zugewiesen sein. (( Ausdruck )) (Speicherabzug geschrieben)  (gegenwärtiges Arbeitsverzeichnis ist: %s)
 . Dateiname [Argumente] /dev/(tcp|udp)/host/port wird ohne Netzwerk nicht unterstützt »/tmp« muss ein gültiger Verzeichnisname sein. : <kein aktuelles Verzeichnis> Abbruchkommando Abbruch … Fügt ein Verzeichnis dem Stapel hinzu.

    Legt ein Verzeichnisnamen auf den Verzeichnisstapel oder rotiert
    diesen so,daß das Arbeitsverzeichnis auf der Spitze des Stapels
    liegt. Ohne angegebene Argumente werden die obersten zwei
    Verzeichnisse auf dem Stapel getauscht.

    Optionen:
    -n	unterdrückt das Wechseln in das Verzeichnis beim Hinzufügen
        zum Stapel, so daß nur der Stapel verändert wird.

    Argumente:
    +N	Rotiert den Stapel so, daß das N'te Verzeichnis
    (angezeigt von `dirs',gezählt von links) sich an der Spitze des
    Stapels befindet.

    -N	Rotiert den Stapel so, daß das N'te Verzeichnis (angezeigt von
        -`dirs',gezählt von rechts) sich an der Spitze des Stapels
        -befindet.
    

    DIR	Legt DIR auf die Spitze des Verzeichnisstapels und wechselt dorthin.

    Der Verzeichnisstapel kann mit dem Kommando `dirs' angezeigt
    werden.

    Rückgabewert: 
    Gibt Erfolg zurück, außer wenn ein ungültiges Argument angegeben
    wurde oder der Verzeichniswechsel nicht erfolgreich war. Legt ein Verzeichniseintrag auf den Verzeichnisstapel ab oder rotiert
den Stapel so, dass das aktuelle Verzeichnis oben liegt. Ohne Argumente
werden die beiden oberen Einträge vertauscht.

    Optionen: 
       -n	Vermeidet das Wechseln des Verzeichnisses, so dass
	nur der Verzeichnisstapel geändert wird.

    Argumente:
      +N	Rotiert den Verzeichnisstapel, dass das N-te Verzeichnis
	von links, das von »dirs« angezeigt wird, nach oben kommt. Die Zählung
	beginnt dabei mit Null.

      -N	Rotiert den Verzeichnisstapel, dass das N-te Verzeichnis
	von rechts, das von »dirs« angezeigt wird, nach oben kommt. Die 
	Zählung beginnt dabei mit Null.

      dir	Legt DIR auf den Verzeichnisstapel und wechselt in dieses
      Verzeichnis.
    
    Das Kommando »dirs« zeigt den Verzeichnisstapel an. Alarm (Profil) Alarm (virtuell) Wecker Arithmetische »for«-Schleife.
    
    Entspricht:
    	(( AUSDRUCK1 ))
    	while (( AUSDRUCK2 )); do
    		KOMMANDOS
    		(( AUSDRUCK3 ))
    	done

    AUSDRUCK1, AUSDRUCK2 und AUSDRUCK3 sind arithmetische Ausdrücke. Wenn ein
    Ausdruck nicht angegeben ist, verhält er sich so, als wäre sein Ergebnis 1.
    
    Rückgabewert:
    Gibt den Status des zuletzt ausgeführten Kommandos zurück. Verfolgen/anhalten abfangen (Trace/breakpoint trap) Falscher Systemaufruf Falsches Signal. Unterbrochene Pipe Bus-Fehler Rechenzeitgrenze Wechselt das Arbeitsverzeichnis.
    
    Wechselt in das angegebene Abeitsverzeichnis.  Ohne Angabe eines
    Verzeichnisses wird in das Heimatverzeichnis gewechselt.
    
    Die Variable CDPATH gibt eine Liste von Orten an, in denen nach
    dem angegebeben Verzeichnisnamen gesucht wird.  Die Pfadnamen
    werden durch Doppelpunkte »:« getrennt. Ein leerer Pfadname
    bezeichnet das aktuelle Verzeichnis. Wenn ein vollständiger
    Pfadname angegeben ist, wird der CDPATH nicht durchsucht.
    
    Wenn kein entsprechendes Verzeichnis gefunden wurde und die Shell
    Option `cdable_vars' gesetzt ist, dann wird angenommen, dass der
    Verzeichnisname einen Variablennamen enthält.  Wenn diese ein Wert
    besitzt, wird dieser als Verzeichnisname verwendet.
    
    Optionen:
        -L	Erzwingt das symbolischen Verweisen gefolgt wird.
                Symbolische Links im aktuellen Verzeichnis werden nach
                dem übergeordneten Verzeichnis aufgelöst.
        -P	Symbolische Verweise werden ignoriert. Symbolische
                Links im aktuellen Verzeichnis werden vor dem
                übergeordneten Verzeichnis aufgelöst.
        -e	Wenn mit der »-P« das aktuelle Arbeitsverzeichns nicht
                ermittelt werden kann, wird ein Rückgabwert ungleich 0
                geliefert.
        -@      Wenn es das System Unterstützt wird eine Datei mit  
                erweiterten Attributen als ein Verzeichnis angezeigt,
                welches die erweiterten Attribute enthält.
    
    Standardmäßig wird symbolischen Verweisen gefolgt (Option -L).
    Das übergeordnete Verzeichnis wird ermittelt, indem der
    Dateiname am letzten Schrägstrich gekürzt wird oder es wird der
    Anfang von DIR verwendet.
    
    Rückgabewert: 
    Der Rückgabewert ist 0, wenn das Verzeichnis gewechselt wurde,
    sonst ungleich 0.
    Mit den Optionen »-P -e« wird ein Rückgabewert ungleich 0 auch
    dann gesetzt, wenn das neue aktuelle Verzeichnis nicht ermittelt
    werden konnte. Kindprozess abgebrochen oder gestoppt. BASH_VERSION	Versionsnummer der Bash.
    CDPATH	Eine durch Doppelpunkte getrennte Liste von
                Verzeichnissen, die durchsucht werden, wenn das
                Argument von `cd' nicht im aktuellen Verzeichnis
                gefunden wird.
    GLOBIGNORE  Eine durch Doppelpunkte getrennte Liste von
                Dateinamenmustern, die für die Dateinamensergänzung
                ignoriert werden.
    HISTFILE	Datei, die den Kommandozeilenspeicher enthält.
    HISTFILESIZE	Maximale Zeilenanzahl, dieser Datei.
    HISTSIZE	Maximale Anzahl von Zeilen, auf die der
                Historymechanismus der Shell zurückgreifen kann.
    HOME	Heimatverzeichnis des aktuellen Benutzers.
    HOSTNAME    Der aktuelle Rechnername.
    HOSTTYPE	CPU-Typ des aktuellen Rechners.
    IGNOREEOF	Legt die Reaktion der Shell auf ein EOF-Zeichen fest.
                Wenn die Variable eine ganze Zahl enthält, wird diese
                Anzahl EOF Zeichen (Ctrl-D) abgewartet, bis die Shell
                verlassen wird.  Der Vorgabewert ist 10. Ist IGNOREEOF
                nicht gesetzt, signalisiert EOF das Ende der Eingabe.
    MACHTYPE    Eine Zeichenkette die das aktuell laufende System beschreibt.
    MAILCHECK	Zeit in s, nach der nach E-Mail gesehen wird.
    MAILPATH	Eine durch Doppelpunkt getrennte Liste von Dateinamen,
                die nach E-Mail durchsucht werden.
    OSTYPE	Unix Version, auf der die Bash gegenwärtig läuft.
    PATH	Durch Doppelpunkt getrennte Liste von Verzeichnissen,
                die nach Kommandos durchsucht werden.
    PROMPT_COMMAND	Kommando, das vor der Anzeige einer primären
                        Eingabeaufforderung (PS1) ausgeführt wird.
    PS1                 Zeichenkette, die die primäre
                        Eingabeaufforderung enthält.
    PS2                 Zeichenkette, die die sekundäre
                        Eingabeaufforderung enthält.
    PWD                 Der vollständige aktuelle Verzeichnisname.
    SHELLOPTS           Durch Doppelpunkt getrennte Liste der aktiven
                        Shell Optionen.
    TERM	Name des aktuellen Terminaltyps.
    auto_resume Ein Wert ungleich Null bewirkt, daß ein einzelnes
                Kommando auf einer Zeile zunächst in der Liste
                gegenwärtig gestoppter Jobs gesucht und dieser in den
                Vordergrund geholt wird. `exact' bewirkt, daß das
                Kommando genau dem Kommando in der Liste der
                gestoppten Jobs entsprechen muß. Wenn die Variable den
                Wert `substring' enthält, muß das Kommando einem
                Substring der Jobbezeichnung entsprechen. Bei einem
                anderen Wert müssen die ersten Zeichen übereinstimmen.
    histchars         Zeichen, die die Befehlswiederholung und die
                      Schnellersetzung steuern. An erster Stelle steht
                      das Befehlswiederholungszeichen (normalerweise
                      `!'); an zweiter das `Schnell-Ersetzen-Zeichen'
                      (normalerweise `^'). Das dritte Zeichen ist das
                      `Kommentarzeichen' (normalerweise `#').
    HISTIGNORE        Eine durch Doppelpunkt getrennte Liste von
                      Mustern, welche die in der
                      Befehlswiederholungsliste zu speichernden
                      Kommandos angibt.
 Prozeßbearbeitung wieder aufgenommen. Copyright (C) 2012 Free Software Foundation, Inc. Copyright (C) 2013 Free Software Foundation, Inc. Erzeugt einen Unterprozess mit dem Namen NAME.
    
    Führt KOMMANDO asynchron aus, mit der Standardeingabe und -ausgabe des
    Kommandos, welches über eine Weiterleitung (Pipe) mit den Datei-
    Deskriptoren verbunden ist, die den Indices 0 und 1 der Feldvariable NAME
    in der ausführenden Shell zugewiesen sind. Der Standardname ist »COPROC«.
    
    Rückgabewert:
    Gibt den Rückgabewert von KOMMANDO zurück. Definiert lokale Variablen.
    
    Erzeugt eine Lokale Variable NAME und weist ihr den Wert VALUE zu.
    OPTION kann eine beliebige von `declare' akzeptierte Option sein.

    Lokale Variablen können nur innerhalb einer Funktion benutzt
    werden. Sie sind nur in der sie erzeugenden Funktion und ihren
    Kindern sichtbar.
    
    Rückgabewert: 
    Liefert 0 außer bei Angabe einer ungültigen Option, einer
    fehlerhaften Variablenzuweisung oder dem Aufruf außerhalb einer
    Funktion. Definiert Aliase oder zeigt sie an.
    
    Ohne Argumente wird die Liste der Aliase (Synonyme) in der Form
    »alias NAME=WERT« auf der Standardausgabe ausgegeben.

    Sonst wird ein Alias für jeden angegebenen NAMEn definiert, für den ein
    WERT angegeben wurde.
    Ein führendes Leerzeichen in WERT bewirkt, dass das nächste Wort auf
    eine Alias-Ersetzung überprüft wird, wenn der Alias erweitert wird.
    
    Optionen:
      -p	Gibt alle definierten Aliase in einem wiederverwendbaren Format an.
    
    Rückgabewert:
    Meldet Erfolg, außer wenn NAME nicht existiert. Definiert Shell-Funktionen.
    
    Definiert eine Shell-Funktion mit dem Namen NAME. Wenn sie als einfaches 
    Kommando aufgerufen wird, führt NAME die KOMMANDOS im Kontext der
    aufrufenden Shell aus. Wenn NAME aufgerufen wird, werden die Argumente
    der Funktion als $1...$n überreicht und der Name der Funktion in $FUNCNAME
    gespeichert.
   
    Rückgabewert:
    Gibt »Erfolg« zurück, außer NAME ist schreibgeschützt. Zeigt den Verzeichnisstapel an.

    Zeigt die Liste der gegenwärtig gespeicherten Verzeichnisse.
    Diese werden mit dem `pushd' Kommando eingetragen und mit dem
    `popd' Kommando ausgelesen.

    Optionen:
      -c        Löscht den Verzeichnisstapel.
      -l        Keine Abkürzung für das Heimatverzeichnis durch die
                Tilde (~).
      -p        Ausgabe von einem Eintrag pro Zeile.
      -v        Ausgabe von einem Eintrag pro Zeile mit Angabe der
                Position im Stapel<

    Argumente:
      +N        Gibt das N'te Element von links der Liste aus, die
                ohne Argumente ausgegeben wird.  Die Zählung beginnt
                bei 0.
      -N        Gibt das N'te Element von rechts der Liste aus, die
                ohne Argumente ausgegeben wird.  Die Zählung beginnt
                bei 0.

    Rückgabewert:
    Gibt Erfolg zurück, außer bei einer ungültigen Option oder wenn
    ein Fehler auftritt. Zeigt Informationen über eingebaute Kommandos an.
    
    Zeigt eine kurze Zusammenfassung über eingebaute Kommandos an.
    Wenn MUSTER angegeben ist, wird die detaillierte Hilfe zu allen
    Kommandos angezeigt, die MUSTER enthalten, ansonsten wird eine
    Liste der Hilfe-Themen ausgegeben.
    
    Optionen:
      -d	Zeigt eine kurze Beschreibung zu jedem Thema an.
      -m	Zeigt die Benutzung im Format von Hilfeseiten (manpage).
      -s	Zeigt nur eine kurze Nutzungsübersicht für jedes Thema,
        	welches MUSTER enthält.
    
    Argumente:
      MUSTER	Das MUSTER spezifiziert das Hilfe-Thema.
    
    Rückgabewert:
    Gibt »Erfolg« zurück, außer MUSTER wurde nicht gefunden oder eine
    ungültige Option wurde angegeben. Zeigt Informationen über den Kommando-Typ.
    
    Für jeden NAMEn wird angezeigt, wie er interpretiert würde, wenn er als
    Kommando verwendet wird.
    
    Optionen:
      -a	Zeigt alle Orte, die eine ausführbare Anwendung NAME enthalten;
        	beinhaltet Aliase, eingebaute Shell-Kommandos und Funktionen,
        	wenn und nur wenn die Option »-p« nicht auch genutzt wird.
      -f	Unterdrückt das Nachschlagen von Shell-Funktionen.
      -P	Erzwingt eine Suche nach jedem NAMEn in $PATH, auch wenn es ein 
        	Alias, eingebautes Kommando oder Funktion ist und gibt den 
        	Namen der Datenträgerdatei zurück, die ausgeführt würde.
      -p	Gibt entweder den Namen der Datenträgerdatei zurück, die ausgeführt
        	würde, oder nichts, wenn »type -t NAME« nicht »file« zurück gibt.
      -t	Gibt ein einzelnes Wort aus »alias«, »keyword«,
        	»function«, »builtin«, »file« oder »« zurück, wenn NAME ein
        	Alias, ein reserviertes Shell-Wort, eine Shell-Funktion, ein
        	eingebautes Shell-Kommando oder eine Datenträgerdatei ist, oder
        	nicht gefunden wurde.
    
    Argumente:
      NAME	Der Name des zu interpretierenden Kommandos.
    
    Rückgabewert:
    Gibt »Erfolg« zurück, wenn alle NAMEn gefunden werden, scheitert, wenn
    einige nicht gefunden werden. Zeigt Kommandos aus dem Verlauf an oder führt sie aus.
    
    Mit »fc« können Kommandos aus dem Verlauf angezeigt, bearbeitet und
    erneut ausgeführt werden. ANFANG und ENDE sind Ziffern, die den
    Bereich angeben. ANFANG kann auch eine Zeichenkette sein, die das
    jüngste Kommando mit diesem Anfangswert bezeichnet.
    
    Optionen:
      -e ENAME	Bestimmt den zu nutzenden Editor. Standard ist FCEDIT,
    		danach EDITOR, dann vi.
      -l 	Zeigt die Kommandos an anstatt zu bearbeiten.
      -n	Ausgabe ohne Zeilennummern
      -r	Ausgabe in umgekehrter Reihenfolge (neueste zuerst)
    
    Mit dem Format »fc -s [Muster=Ersetzung ...] [Kommando]« wird KOMMANDO
    erneut ausgeführt nachdem die Ersetzung ALT=NEU durchgeführt wurde.
    
    Ein sinnvoller Alias dafür ist »r='fc -s'«, sodass die Eingabe »r cc«
    das letzte Kommando, das mit »cc« beginnt, ausführt und »r« das letzte
    Kommando erneut ausführt.
    
    Rückgabewert:
    Gibt »Erfolg« oder den Status des ausgeführten Kommandos zurück oder
    »gescheitert« wenn ein Fehler auftritt. Zeigt den Verlauf an oder manipuliert ihn.
    
    Zeigt den Verlauf mit Zeilennummern an. Jede modifizierte Zeile
    erhält ein führendes »*«. Das Argument »N« gibt nur die letzten
    N Einträge aus.
    
    Optionen:
      -c	Leert den Verlauf durch Löschen aller Einträge.
      -d Offset	Löscht den Verlaufseintrag mit der Nummer Offset.
    
      -a	Ergänzt die Verlaufsdatei mit dem Verlauf dieser Sitzung.
      -n	Liest alle noch nicht eingelesenen Verlaufseinträge aus
        	der Verlaufsdatei.
      -r	Liest die Verlaufsdatei und hängt deren Inhalt an den Verlauf an.
      -w	Schreibt den aktuellen Verlauf in die Verlaufsdatei
        	und hängt ihn an die Verlaufsliste an.
    
      -p	Führt eine Verlaufserweiterung für jedes Argument aus und zeigt
        	das Ergebnis an.
      -s	Hängt die Argumente als einzelnen Eintrag an die Verlaufsliste an.
    
    Wenn DATEINAME angegeben ist, wird dieser als Verlaufsdatei verwendet.
    Andererseits wird $HISTFILE (wenn definiert), sonst ~/.bash_history
    verwendet.
    
    Wenn die Variable $HISTTIMEFORMAT gesetzt ist und nicht Null ist, wird
    deren Wert als Formatierung für strftime(3) zur Ausgabe des Zeitstempels
    und des entsprechenden Verlaufseintrag verwendet. Sonst werden keinerlei
    Zeitstempel ausgegeben.
    
    Rückgabewert:
    Gibt »Erfolg« zurück, außer eine ungültige Option wird angegeben oder ein
    Fehler tritt auf. Zeigt die Maske für den Dateimodus an, oder ändert diese.
    
    Ändert die Anwendermaske zur Dateierzeugung auf MODUS. Wenn MODUS nicht
    angegeben ist, wird der aktuelle Wert der Maske ausgegeben.
    
    Wenn MODUS mit einer Ziffer beginnt, wird er als Oktalzahl interpretiert;
    ansonsten ist er eine symbolische Zeichenkette, wie sie auch von chmod(1)
    akzeptiert wird.
    
    Optionen:
      -p	Wenn MODUS nicht angegeben ist, erfolgt die Ausgabe in einem
        	wiederverwendbaren Format
      -S	Ausgabe erfolgt symbolisch, ansonsten als Oktalzahl
    
    Rückgabewert:
    Gibt »Erfolg« zurück, außer MODUS ist ungültig, oder eine ungültige Option
    wurde angegeben. Zeigt den Zeitverbrauch an.
    
    Gibt den kumulierten Nutzer- und Systemzeitverbrauch der Shell und
    aller von ihr gestarteten Prozesse aus.
    
    Rückgabewert:
    Immer 0. Zeigt die Liste der gegenwärtig gespeicherten Verzeichnisse an.  Durch
    das Befehl »pushd« werden die Verzeichnisse auf den Stapel gelegt
    und können durch den Befehl »popd« wieder vom Stapel entfernt
    werden.

    Optionen:
	-c	Verzeichnisstapel durch Löschen aller Einträge bereinigen.
	-l	Das Heimatverzeichnis wird nicht mit vorangestellter Tilde
	ausgegeben
	-p	Den Verzeichnisstapel zeilenweise ausgeben.
	-v	Den Verzeichnisstapel zeilenweise mit vorangestellter
	Positionsnummer ausgeben.

    Argumente:
	+N	Zeigt den N'ten Eintrag von links an, der von »dirs« ausgegeben
	wird, wenn es ohne Optionen aufgerufen wird, beginnend mit Null.
	-N	Zeigt den N'ten Eintrag von rechts an, der von »dirs« ausgegeben
	wird, wenn es ohne Optionen aufgerufen wird, beginnend mit Null. Fertig Fertig(%d) EMT abfangen (EMT trap) Eingebaute Shell-Kommandos aktivieren und deaktivieren.
    
    Aktiviert und deaktiviert eingebaute Shell-Kommandos. Die Deaktivierung
    erlaubt Ihnen, eigene Kommandos mit demselben Namen wie die eingebauten
    Kommandos zu nutzen, ohne den kompletten Pfad angeben zu müssen.
    
    Optionen:
      -a	Gibt eine Liste der eingebauten Kommandos aus inklusive der
    		Information, ob sie aktiv sind oder nicht.

      -n	deaktiviert jedes angegebene Kommando oder gibt eine
                Liste der deaktivierten eingebauten Kommandos aus.
      -p	Gibt eine Liste der eingebauten Kommandos in einem
    		wiederverwendbaren Format aus.
      -s	Gibt nur die Namen der »speziellen« in POSIX eingebauten
    		Kommandos aus.
    
    Optionen zum Beeinflussen des dynamischen Ladens:
      -f	Lade eingebautes Kommando aus der angegebenen Datei.
      -d	Entfernt ein mit »-f« geladenes Kommando.
    
    Ohne Optionen wird jedes angegebe Kommando aktiviert.
    
    Um das unter $PATH liegende Kommando »test« anstelle der eingebauten
    Version zu nutzen, geben Sie »enable -n test« ein.
    
    Rückgabewert:
    Gibt »Erfolg« zurück, außer NAME ist kein eingebautes Kommando 
    oder ein Fehler tritt auf. Wertet arithmetische Ausdrücke aus.

       Der Ausdruck wird nach den Regeln für arithmetische
       Berechnungen ausgewertet.  Diese Schreibweise entspricht "let
       Ausdruck".

       Rückgabewert: 
       Gibt »1« zurück, wenn die Auswertung des letzten Arguments Null
       ergibt, sonst »0«. Wertet arithmetische Ausdrücke aus.
    
    Wertet jedes Argument als arithmetischen Ausdruck aus. Die Auswertung wird
    mit Integer-Werten fester Breite und ohne Überlaufüberprüfung durchgeführt.
    Division durch Null wird abgefangen und als Fehler markiert. Die folgende
    Liste mit Operatoren ist nach Gleichrangigkeit gruppiert und nach
    absteigendem Vorrang sortiert.
    
    	id++, id--	Variable nach der Rückgabe erhöhen, bzw. Verringern
    	++id, --id	Variable vor der Rückgabe erhöhen, bzw. Verringern
    	-, +		unäres Minus, Plus
    	!, ~		Logische und Bitweise Negation
    	**		Potenzierung
    	*, /, %		Multiplikation, Division, Rest
    	+, -		Addition, Subtraktion
    	<<, >>		Bitweise nach links bzw. rechts verschieben
    	<=, >=, <, >	Vergleich
    	==, !=		Gleichheit, Ungleichheit
    	&		Bitweises UND (Konjunktion)
    	^		Bitweises XOR (Kontravalenz)
    	|		Bitweises ODER (Disjunktion)
    	&&		Logisches UND (Konjunktion)
    	||		Logisches ODER (Disjunktion)
    	Ausdruck ? Ausdruck : Ausdruck
    			Bedingter Operator
    	=, *=, /=, %=,
    	+=, -=, <<=, >>=,
    	&=, ^=, |=	Zuweisung
    
    Shell-Variablen sind als Operanden zulässig. Der Variablenname wird
    innerhalb eines Ausdrucks durch seinen Wert (Integer-Wert fester Breite)
    ersetzt. Das Integer-Attribut der Variable muss nicht aktiv sein, damit
    die Variable in einem Ausdruck verwendet werden kann.
    
    Operatoren werden entsprechend ihrer Vorrangsreihenfolge ausgewertet.
    Unterausdrücke in Klammern werden zuerst ausgewertet und können die oben
    genannten Vorrangsregeln überschreiben.
    
    Rückgabewert:
    Gibt »1« zurück, wenn die Auswertung des letzten Arguments Null ergibt,
    sonst »0«. Wertet einen bedingten Ausdruck aus.
    
    Dieses Kommando entspricht dem Kommando »test«. Jedoch muss das
    letzte Argument ein »]« sein, welches die öffnende Klammer »[«
    schließt. Führt ein einfaches Kommando aus oder zeigt Informationen über Kommandos an.

    Führt das Kommando mit den angegebeneb Argumenten aus, ohne
    Shell-Funktion nachzuschlagen oder zeigt Informationen über die
    Kommandos an. Dadurch können auch dann Kommandos ausgeführt
    werden, wenn eine Shell-Funktion gleichen Namens existiert.
    
    Optionen:
      -p	Standardwert für PATH verwenden. Dies garantiert, dass alle
    		Standard-Dienstprogramme gefunden werden.
      -v	Beschreibung des Kommandos ausgeben.
    		Ähnlich dem eingebauten Kommando »type«.
      -V	Eine ausführlichere Beschreibung jedes Kommandos ausgeben.
    
    Rückgabewert:
    Gibt den Rückgabewert des Kommandos zurück, oder eine Fehlermeldung, wenn
    das Kommando nicht gefunden wird. Führt die Argumente als Shell-Kommando aus.
    
    Fügt die Argumente zu einer Zeichenkette zusammen und verwendet
    das Ergebnis als Eingebe in eine Shell, welche die enthaltenen
    Kommandos ausführt.
    
    Rückgabewert:
    Der Status des Kommandos oder »Erfolg« wenn das Kommando leer war. Führt Kommandos aus, solange ein Test nicht wahr ist.
    
    Erweitert und führt KOMMANDOS aus, solange das abschließende Kommando in
    »until KOMMANDOS« einen Rückgabewert ungleich Null hat.
    
    Rückgabewert:
    Gibt den Status des zuletzt ausgeführten Kommandos zurück. Führt Kommandos aus, solange ein Test wahr ist.
    
    Erweitert und führt KOMMANDOS aus, solange das abschließende Kommando in
    »while KOMMANDOS« den Rückgabewert Null hat.
    
    Rückgabewert:
    Gibt den Status des zuletzt ausgeführten Kommandos zurück. Führt Kommandos auf Basis von Bedingungen aus.
    
    Die Liste »if KOMMANDOS« wird ausgeführt. Wenn deren Rückgabewert Null ist,
    wird die Liste »then KOMMANDOS« ausgeführt. Ansonsten wird eins nach dem
    anderen jede Liste »elif KOMMANDOS« ausgeführt und wenn deren Rückgabewert
    Null ist, die entsprechende Liste »then KOMMANDOS« und das Kommando »if«
    beendet. Ansonsten wird die Liste »else KOMMANDOS«, wenn angegeben,
    ausgeführt. Der Rückgabewert des gesamten Konstrukts ist der Rückgabewert
    des zuletzt ausgeführten Kommandos oder Null, wenn keine Bedingung wahr ist.
    
    Rückgabewert:
    Gibt den Status des zuletzt ausgeführten Kommandos zurück. Führt Kommandos auf Basis von Musterübereinstimmung aus.
    
    Führt selektive KOMMANDOS basierend auf der Übereinstimmung von WORT
    und MUSTER aus. Zur Trennung verschiedener MUSTER wird »|« verwendet.
    
    Rückgabewert:
    Gibt den Status des zuletzt ausgeführten Kommandos zurück. Führt Kommandos für jedes Element der Liste aus.
    
    Die »for«-Schleife führt eine Kommandosequenz für jedes Element einer Liste
    aus. Wenn »in Wortliste …;« nicht angegeben ist, wird »in $@« angenommen.
    Für jedes Element in WORTLISTE wird NAME auf dieses Element gesetzt und die
    KOMMANDOS werden ausgeführt. 
    
    Rückgabewert:
    Gibt den Status des zuletzt ausgeführten Kommandos zurück. Führt Kommandos aus einer Datei in der aktuellen Shell aus.
    
    Liest Kommandos aus DATEINAME und führt diese in der aktuellen Shell aus.
    Die Einträge in $PATH werden bei der Bestimmung des Verzeichnisses von
    DATEINAME verwendet. Wenn ARGUMENTE angegeben werden, sind diese die
    Positionsparameter wenn DATEINAME ausgeführt wird.
    
    Rückgabewert:
    Gibt den Status des zuletzt in DATEINAME ausgeführten Kommandos zurück;
    scheitert, wenn DATEINAME nicht gelesen werden kann. Führt ein in der Shell definiertes Kommando aus.
    
    Führt ein in der Shell definiertes Kommando aus. Dies ist dann
    nützlich, wenn es mit gleichem Namen als Funktion reimplementiert
    werden soll, aber die Funktionalität des eingebauten Kommandos
    innerhalb der neuen Funktion benötigt wird.
    
    Rückgabewert:
    Der Rückgabewert des aufgerufenen Kommandos oder »falsch«, wenn
    dieses nicht existiert. Exit %d Beendet eine Login-Shell.
    
    Beendet eine Login-Shell mit dem Rückgabewert »n«.  Wenn logout
    nicht von einer Login-Shell aus ausgeführt wurde, wird ein Fehler
    zurückgegeben. Beendet »for«-, »while«- oder »until«-Schleifen.
    
    Break beendet eine »for«-, »while«- oder »until«-Schleife.  Wenn »n«
    angegeben ist, werden entsprechend viele geschachtelte Schleifen
    beendet.
    
    Rückgabewert:
    Der Rückgabewert ist 0, außer »n« ist nicht größer oder gleich 1. Beendet die aktuelle Shell.

    Beendet die die aktuelle Shell mit dem Rückgabewert »N«. Wenn »N« nicht
    angegeben ist, wird der Rückgabewert des letzten ausgeführten Kommandos
    übernommen. Grenze für Dateigröße Fließkommafehler GNU bash, Version %s (%s)
 GNU bash, Version %s-(%s)
 Lange GNU-Optionen:
 Gruppiert Kommandos als Einheit.
    
    Führt einen Satz von Kommandos in einer Gruppe aus. Dies ist ein Weg,
    einen ganzen Satz von Kommandos umzuleiten.
    
    Rückgabewert:
    Gibt den Status des zuletzt ausgeführten Kommandos zurück. HFT-Eingabedaten ausstehend. HFT-Monitormodus erlaubt. HFT-Monitormodus abgeschaltet. HFT-Tonfolge beendet. HOME ist nicht zugewiesen. Aufgelegt Ich habe keinen Benutzernamen! E/A fertig Ungültige Anweisung. Informationsanforderung Unterbrochen (Interrupt) Abgewürgt (Killed) Lizenz GPLv3+: GNU GPL Version 3 oder jünger <http://gnu.org/licenses/gpl.html>
 Bringt einen Job in den Vordergrund.
    
    Bringt den mit JOBBEZEICHNUNG bezeichneten Prozess als aktuellen Job in
    den Vordergrund. Wenn JOBBEZEICHNUNG nicht angegeben ist, wird der zuletzt
    angehaltene Job verwendet.
    
    Rückgabewert:
    Status des in den Vordergrund geholten Jobs oder Fehler. Bringt einen Job in den Hintergrund.
    
    Bringt den mit JOBBEZEICHNUNG bezeichneten Job in den Hintergrund, als
    ob er mit »&« gestartet wurde. Wenn JOBBEZEICHNUNG nicht angegeben ist,
    wird der aktuelle Job verwendet.
    
    Rückgabewert:
    Immer Erfolg, außer wenn die Jobsteuerung nicht verfügbar ist
    oder ein Fehler auftritt. Leeranweisung.

    Leeranweisung; das Kommando hat keine Wirkung.

    Rückgabewert:
    Das Kommando ist immer »wahr«. OLDPWD ist nicht zugewiesen. Analysieren von Optionsargumenten.
    
    Getopts wird von Shell-Prozeduren verwendet, um die
    Kommandozeilenoptionen auszuwerten.
    
    "Optionen" enthält die auszuwertenden Buchstaben.  Ein Doppelpunkt
    nach dem Buchstaben zeigt an, dass ein Argument erwartet wird,
    welches durch ein Leerzeichen von der Option getrennt ist.
    
    Bei jedem Aufruf von »getopts« wird die nächste Option der
    $Variable zugewiesen. Diese wird angelegt, falls sie noch
    nicht existiert. Weiterhin wird der Indes Index des nächsten zu
    verarbeitenden Arguments der Shell-Variablen OPTIND
    zugewiesen. OPTIND wird bei jedem Aufruf einer Shell oder eines
    Shell-Skripts mit 1 initialisiert.  Wenn eine Option ein Argument
    benötigt, wird dieses OPTARG zugewiesen.
    
    Für Fehlermeldungen gibt es zwei Varianten.  Wenn das erste
    Zeichen des Optionsstrings ein Doppelpunkt ist, wird der stille
    Fehlermodus von »getopts« verwendet. In diesem Modus wird keine
    Fehlermeldung ausgegeben. Wenn eine ungültige Option erkannt wird,
    wird das gefundene Optionenzeichen OPTARG zugewiesen. Wenn ein
    benötigtes Argument fehlt, wird ein »:« der Variable zugewiesen
    und OPTARG auf das gefundene Optionenzeichen gesetzt.  Im anderen
    Fehlermodus wird ein »?« der Variable zugewiesen, OPTARG geleert
    und eine Fehlermeldung ausgegeben.
    
    Wenn die Shell-Variable OPTERR den Wert »0« hat, werden durch getopts 
    keine Fehlermeldungen ausgegeben, auch wenn das erste Zeichen 
    von OPTSTRING kein Doppelpunkt ist. OPTERR hat den Vorgabewert »1«.
    
    Getopts analysiert normalerweise die von der Position abhängigen
    Parameter ($0 - $9). Aber wenn mehr Argumente angegeben sind,
    werden stattdessen diese analysiert.
    
    Rückgabewert:
    Gibt »Erfolg« zurück wenn eine Option gefunden wird und
    »gescheitert«, wenn das Ende der Optionen erreicht oder ein Fehler
    aufgetreten ist. Gibt den Namen des aktuellen Arbeitsverzeichnisses aus.
    
    Optionen:
      -L	Gibt den Inhalt der Variable $PWD aus.
      -P	Gibt den physischen Verzeichnispfad aus, ohne
                symbolische Verweise.
    
    Standardmäßig wird immer die Option »-L« gesetzt.
    
    Rückgabewert:
    Ist 0 außer wenn eine ungültige Option angegeben oder das aktuelle
    Verzeichnis nicht lesbar ist. Beenden Liest Zeilen einer Datei in eine Feldvariable.
    
    Ist ein Synonym für »mapfile«. Datei blockiert. Entfernt Einträge vom Verzeichnisstapel.

    Entfernt Einträge vom Verzeichnisstapel. Ohne Argumente wird die
    Spitze des Stapels entfernt und in das Verzeichnis gewechselt, das
    dann an der Spitze steht.

    Optionen:
      -n	Entfernt nur den Verzeichniseintrag und wechselt nicht
    			das Verzeichnis.
          
    Argumente:
      +N	Entfernt den N-ten Eintrag von links, gezählt von Null, aus der
    			von »dirs« anzeigten Liste. Beispielsweise entfernen »popd +0«
    			den ersten und »popd +1« den zweiten Verzeichniseintrag.

      -N	Entfernt den N-ten Eintrag von rechts, gezählt von Null,
    			aus der von »dirs« angezeigten Liste. Beispielsweise entfernen
    			»popd -0« den letzten und »popd -1« den vorletzten
    			Verzeichniseintrag.

    Mit »dirs« kann der Verzeichnisstapel angezeigt werden.

    Rückgabewert:
    Gibt 0 zurück, außer wenn ein ungültiges Argument angegeben
    wurde oder der Verzeichniswechsel nicht erfolgreich war. Entferne jeden angegebenen Namen von der Aliasliste.
    
    Optionen:
      -a	Entferne alle Alias-Definitionen.
    
    Gibt immer 0 zurück wenn der Alias existierte. Entfernt Aufträge aus der aktuellen Shell.
    
    Entfernt jede JOBBEZEICHNUNG aus der Tabelle der aktiven Aufträge. Ohne
    Angabe einer JOBBEZEICHNUNG nutzt die Shell ihre Auffassung vom aktuellen
    Auftrag.
    
    Optionen:
      -a	Entfernt alle Aufträge, wenn JOBBEZEICHNUNG nicht angegeben ist.
      -h	Markiert jede JOBBEZEICHNUNG, sodass kein SIGHUP an den Auftrag
        	gesendet wird, wenn die Shell ein SIGHUP empfängt.
      -r	Entfernt nur laufende Aufträge.
    
    Rückgabewert:
    Gibt »Erfolg« zurück, außer eine ungültige Option oder JOBBEZEICHNUNG
    wird angegeben. Entfernt Einträge vom Stapel.  Ohne Argumente wird der oberste Eintrag
    gelöscht und anschließend in das das neue oben liegende Verzeichnis
    gewechselt.
    
    Optionen:
      -n	Vermeidet das Wechseln des Verzeichnisses, so dass
	nur der Verzeichnisstapel geändert wird.
    
    Argumente:
      +N	Entfernt den N-ten Eintrag von links, der von »dirs«
	angezeigt wird. Dabei beginnt die Zählung von Null. So
	entfernt z.B. »popd +0« den ersten und »popd +1« den zweiten
	Eintrag.
    
      -N	Entfernt den N-ten Eintrag von rechts, der von »dirs«
	angezeigt wird. Dabei beginnt die Zählung von Null. So
	entfernt z.B. »popd -0« den letzten und »popd +1« den vorletzten
	Eintrag.
    
    Das Kommando »dirs« zeigt den Verzeichnisstapel an. Ersetzt die Shell durch das angegebene Kommando.
    
    Führt das angebebene Kommando einschließlich dessen Optionen aus
    und ersetzt durch dieses die Shell. Wenn kein Kommando angegeben
    ist, wirken alle Weiterleitungen für die aktuellen Shell.
    
    Optionen:
      -a Name	Setzt den Namen als nulltes Argument für das Kommando.
      -c	Führt das Kommando in einer leeren Umgebung aus.
      -l	Setzt einen Strich als nulltes Argument für das Kommando.
    
    Wenn das Kommando nicht ausgeführt werden kann, wird eine nicht
    interaktive Shell beendet, außer die Shell-Option »execfail« ist
    gesetzt.
    
    Rückgabewert:
    Gibt »Erfolg« zurück, außer das Kommando wurde nicht gefunden oder
    ein Weiterleitungsfehler trat auf. Gibt die durch PIPELINE benötigte Zeit aus.
    
    Führt PIPELINE aus und gibt eine Zusammenfassung der realen Zeit,
    der Anwender-CPU-Zeit und der System-CPU-Zeit nach der Beendigung
    von PIPELINE aus. 
  
    Optionen:
      -p	Gibt die Zeitzusammenfassung im allgemeinen POSIX-Format aus.
    
    Der Wert der Variable TIMEFORMAT wird für das Ausgabeformat verwendet.
    
    Rückgabewert:
    Gibt den Rückgabewert von PIPELINE zurück. Springt zum Schleifenanfang von »for«-, »while«-, oder »until«-Schleifen.
    
    Springt zum Schleifenanfang der aktuellen »for«-, »while«- oder »until«-
    Schleife. Wenn »n« angegeben ist, wird zum Beginn der »n«-ten
    übergeordneten Schleife gesprungen.
    
    Rückgabewert:
    Der Rückgabewert ist 0, außer wenn »n« nicht größer oder gleich 1 ist. Gibt »wahr« zurück.
    
    Rückgabewert:
    Immer »wahr«. Gibt »falsch« zurück.
    
    Rückgabewert:
    Immer »falsch«. Eine Shellfunktion beenden.
    
    Beendet die Funktion oder das Skript und gibt dessen durch N angegebenen
    Rückgabewert aus.  Wird N nicht festgelegt, wird der Rückgabewert
    des zuletzt ausgeführen Befehls oder Skriptes ausgegeben.
    
    Rückgabewert:
    Gibt N aus, bei einem Fehler wird dieser ausgegeben. Gibt den Kontext des aktuellen Subroutinen-Aufrufs zurück.
    
    Gibt ohne AUSDRUCK »$line $filename« zurück. Mit AUSDRUCK wird
    »$line $subroutine $filename« zurückgegeben; diese Informationen können für
    eine Stapelzurückverfolgung genutzt werden.
    
    Der Wert von AUSDRUCK zeigt an, wie viele Aufrufe vor dem aktuellen Aufruf
    liegen; der oberste Aufruf ist 0.
    
    Rückgabewert:
    Gibt 0 zurück, außer wenn die Shell keine Shell-Funktion aufruft oder
    AUSDRUCK ungültig ist. Gibt den Kontext des aktuellen Subroutinen-Aufrufs zurück.
    
    Ohne AUSDRUCK, liefert  Läuft Adressierungsfehler Wählt Wörter aus einer Liste und führt Kommandos aus.
    
    Die WORTLISTE wird expandiert und erzeugt eine Liste von Wörtern. Der Satz
    der expandierten Wörter wird mit je einer führenden Nummer auf die
    Standardfehlerausgabe ausgegeben. Wenn »in WORTLISTE« nicht angegeben ist,
    wird »in §@« angenommen. Es wird dann die Eingabeaufforderung PS3 angezeigt
    und eine Zeile von der Standardeingabe gelesen. Wenn diese Eingabe aus der
    Nummer der angezeigten Wörter besteht, wird NAME auf dieses Wort gesetzt.
    Ist die Eingabe leer, wird die WORTLISTE und die Eingabeaufforderung erneut
    angezeigt. Wird EOF gelesen, wird die Ausführung des Kommandos beendet. Die
    Eingabe eines anderen Wertes setzt NAME auf Null. Die eingelesene Zeile
    wird in der Variable REPLY gespeichert. Die KOMMANDOS werden nach jeder
    Auswahl ausgeführt, bis das Kommando »break« ausgeführt wird.
    
    Rückgabewert:
    Gibt den Status des zuletzt ausgeführten Kommandos zurück. Sendet ein Signal an einen Auftrag.
    
    Sendet an die durch PID oder AUFTRAGSSPEZ bezeichneten Prozesse das Signal
    SIGNALNAME oder SIGNALNUMMER. Wenn weder SIGNALNAME noch SIGNALNUMMER
    angegeben sind, wird SIGNALNAME angenommen.
    
    Optionen:
      -s sig	SIG ist ein Signalname
      -n sig	SIG ist eine Signalnummer
      -l	Gibt die Signalnamen aus. Wenn auf »-l« Argumente folgen, werden
        	diese als Signalnummern interpretiert, für die die Signalnamen
        	ausgegeben werden sollen.
    
    Kill ist eine eingebautes Shell-Kommando für zwei Fälle: Es erlaubt die 
    Nutzung von Auftrags-IDs anstelle von Prozess-IDs und erlaubt das Abwürgen von
    Prozessen, wenn die maximale Anzahl der erzeugbaren Prozesse erreicht ist.
    
    Rückgabewert:
    Gibt »Erfolg« zurück, außer eine ungültige Option wird angegeben oder ein
    Fehler tritt auf. Konfiguriert Readline Tastenzuordnungen und Variablen.
    
    Weist eine Tastensequenz einer Readline Funktion oder einem Makro
    zu oder setzt eine Readline Variable.  Der Argument syntax ist zu
    den Einträgen in ~/.inputrc äquivalent, aber sie müssen als
    einzelnes Argument übergeben werden.  Z.B: bind '"\C-x\C-r":
    re-read-init-file'.
    
    Optionen:
      -m  Keymap         Benutzt KEYMAP as Tastaturbelegung für die Laufzeit
                         dieses Kommandos.  Gültige Keymap Namen sind: emacs,
                         emacs-standard, emacs-meta, emacs-ctlx, vi, vi-move,
                         vi-command und vi-insert.
      -l                 Listet Funktionsnamen auf.
      -P                 Listet Funktionsnamen und Tastenzuordnungen auf.
      -p                 Listet Funktionsnamen und Tastenzuordnungen so auf,
                         dass sie direkt als Eingabe verwendet werden können.
      -S                 Listet Tastenfolgen und deren Werte auf, die Makros 
                         aufrufen.
      -s                 Listet Tastenfolgen und deren Werte auf, die Makros 
                         aufrufen, dass sie als Eingabe wiederverwendet werden
                         können.
      -V                 Listet Variablennamen und Werte auf.
      -v                 Listet Variablennamen und Werte so auf, dass sie als
                         Eingabe verwendet werden können.
      -q  Funktionsname  Sucht die Tastenfolgen, welche die angegebene
                         Funktion aufrufen.
      -u  Funktionsname  Entfernt alle der Funktion zugeordneten Tastenfolgen.
      -r  Tastenfolge    Entfernt die Zuweisungen der angegebeben Tastenfolge.
      -f  Dateiname      Liest die Tastenzuordnungen aus der angegebenen Datei.
      -x  Tastenfolge:Shellkommando	Weist der Tastenfolge das Shellkommando
    					zu.
      -X                                Listet mit -x erzeugte
                                        Tastenfolgen und deren Werte
                                        auf, die Makros aufrufen, dass
                                        sie als Eingabe wiederverwendet werden
                                        können.
    
    Rückgabewert: 
    Bind gibt 0 zurück, wenn keine unerkannte Option angegeben wurde
    oder ein Fehler eintrat. Setzt oder löscht Shell Optionen.

    Ändert die in `Optionsname' genannten Shell Optionen.  Ohne
    Argumente wird eine Liste der Shell Optionen un deren Stati
    ausgegeben.

    Optionen:
      -o        Beschränkt die Optionsmanen auf die, welche mit 
                `set -o' definiert werden müssen.
      -p        Gibt alle Shelloptionen und deren Stati aus.
      -q        Unterdrückt Ausgaben.
      -s        Setzt jede Option in `Optionsname.'
      -u        Deaktiviert jede Option in `Optionsname'.

    Rückgabewert:
    Gibt Erfolg zurück, wenn eine Option gesetzt worden ist.  Wenn
    eine ungültige Option angegeben wurde oder eine Option deaktiviert
    worden ist, wird Fehler zurückgegeben. Setzt das Attribut »export« für Shell-Variablen.
    
    Markiert jeden NAMEn für den automatischen Export in der Umgebung der
    nachfolgend ausgeführten Kommandos. Wenn WERT angegeben ist, wird WERT
    vor dem Export zugewiesen.
    
    Optionen:
      -f	Bezieht sich auf Shell-Funktionen
      -n	Entfernt die Eigenschaft »export« von jedem NAMEn.
      -p	Zeigt eine Liste aller exportierten Variablen und Funktionen.
    
    Das Argument »--« deaktiviert die weitere Verarbeitung.
    
    Rückgabewert:
    Gibt »Erfolg« zurück, außer eine ungültige Option wurde angegeben,
    oder NAME ist ungültig. Setzt Variablenwerte und deren Attribute.
    
    Deklariert Variablen und weist ihnen Attribute zu. Wenn keine
    Namen angegeben sind, werden die Attribute und Werte aller
    Variablen ausgegeben.
    
    Optionen:
      -f	Zeigt nur Funktionsnamen und Definitionen an.
      -F	Zeigt nur Funktionsnamen an (inklusive Zeilennummer
    		und Quelldatei beim debuggen).
      -g	Deklariert innerhalb ener Shellfunktion globale
                Variablen; wird sonst ignoriert.
      -p	Zeigt die Attribute und Werte jeder angegebenen
                Variable an.
    
    Attribute setzen:
      -a	Deklariert ein indiziertes Feld (wenn unterstützt).
      -A	Deklariert ein assoziatives Feld (wenn unterstützt).
      -i	Deklariert eine Integer Variable.
      -l	Konvertiert die Variabennmamen in Kleinbuchstaben.
      -r	Deklariert nur lesbare Variablen.
      -t	Weist das »trace« Attibut zu.
      -u	Konvertiert die Variablennamen in Großbuchstaben.
      -x	Exportiert die Variablen über die aktuelle Shell
                Umgebung hinaus.
    
    Das Voranstellen von »+« anstelle von »-« schaltet die gegebenen
    Attribute ab.
    
    Für Integer Variablen werden bei der Zuweisung arithmetische
    Berechnungen durchgeführt (siehe `help let').
    
    Innerhalb einer Funktion werden lokale Variablen erzeugt. Die
    Option »-g« unterdrückt dieses Verhalten.
    
    Rückgabewert:
    Gibt »Erfolg« zurück, außer eine ungültige Option wurde angegeben,
    oder ein Fehler trat auf. Setzt Variablen-Werte und -Eigenschaften.

    Veraltet. Siehe »help declare«. Shell-Kommandos auf die das Schlüsselwort zutrifft ` Shell-Kommandos auf die die Schlüsselwörter zutreffen ` Shell-Optionen:
 Verschiebt Positionsparameter.
    
    Benennt die Positionsparameter »$N+1«, »$N+2« … in »$1«, »$2« … um.
    Wenn N nicht angegeben ist, wird es als 1 angenommen.
    
    Rückgabewert:
    Gibt »Erfolg« zurück, außer N ist negativ oder größer als »$#«. Signal %d Angehalten Angehalten (Signal) Angehalten (Terminaleingabe) Angehalten (Terminalausgabe) Angehalten(%s) Shell-Ausführung unterbrechen
    
    Unterbricht die Ausführung dieser Shell, bis sie das Signal »SIGCONT«
    empfängt. Login-Shells können nicht unterbrochen werden, außer es wird
    erzwungen.
    
    Optionen:
      -f	Erzwingt die Unterbrechung auch, wenn die Shell eine Login-Shell
        	ist.
    
    Rückgabewert:
    Gibt «Erfolg« zurück, außer Job-Steuerung ist nicht aktiviert oder ein
    Fehler trat auf. TIMEFORMAT: »%c«: Ungültiges Formatzeichen. Abgebrochen (Terminated) Die Post in %s wurde bereits gelesen.
 Es gibt noch laufende Prozesse.
 Es gibt noch angehaltene Prozesse.
 Für den größtmöglichen gesetzlich zulässigen Umfang wird jede Haftung ausgeschlossen. Diese Shell-Befehle sind intern definiert.  Mit »help« kann eine Liste
angesehen werden.  Durch »help Name« wird eine Beschreibung der
Funktion »Name« angezeigt.  Die Dokumentation ist mit »info bash«
einsehbar.  Detaillierte Beschreibungen der Shell-Befehle sind mit
»man -k« oder »info« abrufbar.

Ein Stern (*) neben dem Namen kennzeichnet deaktivierte Befehle.

 Dies ist freie Software.  Sie darf verändert und verteilt werden. Fängt Signale und andere Ereignisse ab.
    
    Definiert und aktiviert Prüfroutinen, die ausgeführt werden, wenn die Shell
    Signale oder andere Bedingungen empfängt.
    
    ARGUMENT ist ein Kommando, dass gelesen und ausgeführt wird, wenn die Shell 
    die Signale SIGNALBEZEICHNUNG empfängt. Wenn ein ARGUMENT oder »-« fehlt
    (und eine einzelne SIGNALBEZEICHNUNG angegeben ist), wird jedes angegebene
    Signal auf den ursprünglichen Wert zurückgesetzt. Wenn ARGUMENT eine leere
    Zeichenkette ist, wird jede SIGNALBEZEICHNUNG von der Shell und das
    aufrufende Kommando ignoriert.
    
    Wenn eine SIGNALBEZEICHNUNG »EXIT (0)« ist, wird ARGUMENT beim Beenden der
    Shell ausgeführt. Wenn eine SIGNALBEZEICHNUNG »DEBUG« ist, wird ARGUMENT
    vor jedem einfachen Kommando ausgeführt. Wenn eine SIGNALBEZEICHNUNG
    »RETURN« ist, wird ARGUMENT jedes mal ausgeführt, wenn eine Shell-Funktion
    oder durch ».« oder »source« ausgeführtes Skript beendet wurde. Wenn eine
    SIGNALBEZEICHNUNG »ERR« ist, wird ARGUMENT jedes mal ausgeführt, wenn bei
    aktivierter Option »-e« ein Kommandoabbruch die Shell beenden würde.
    
    Wenn keine Argumente angegeben sind, wird eine Liste von Kommandos mit den 
    zugehörigen Signalen ausgegeben.
    
    Optionen:
      -l	Gibt eine Liste der Signalnamen und der zugehörigen Nummern aus.
      -p	Zeigt die »trap«-Kommandos, die mit jeder SIGNALBEZEICHNUNG
        	verbunden sind. 
    
    Jede SIGNALBEZEICHNUNG ist entweder ein Signalname in <signal.h> oder eine
    Signalnummer. Bei den Signalnamen ist Groß- und Kleinschreibung egal und
    das Präfix »SIG« ist optional. Ein Signal kann mit »kill -signal $$« an die
    Shell gesendet werden.
    
    Rückgabewert:
    Gibt »Erfolg« zurück, außer SIGNALBEZEICHNUNG ist ungültig oder eine
    ungültige Option wurde angegeben. »%s -c "help set"« für mehr Informationen über Shell-Optionen.
 »%s -c help« für mehr Information über Shell-Befehle.
 Unbekannte Signalnummer. Unbekannte Signalnummer: %d. Unbekannter Fehler. Unbekannter Status Dringende IO-Bedingung Benutzung:	%s [Lange GNU-Option] [Option] ...
		%s [Lange GNU-Option] [Option] Skript-Datei ...
 »%s« verwenden, um die Shell zu verlassen.
 Mit dem Befehl »bashbug« können Fehler gemeldet werden.
 Nutzersignal 1 Nutzersignal 2 Fenster geändert. Schreibt Argumente auf die Standardausgabe.
    
    Zeigt die Argumente auf der Standardausgabe an, gefolgt von einem Zeilenumbruch.
    
    Optionen:
      -n	Keinen Zeilenumbruch anhängen
    
    Rückgabewert:
    Gibt Erfolg zurück, solange kein Schreibfehler auftritt. Ausgabe der Argumente auf die Standardausgabe.
    
    Zeigt die Argumente auf der Standardausgabe gefolgt von einem
    Zeilenumbruch an.
    
    Optionen:
      -n	Keinen Zeilenumbruch anfügen
      -e	Interpretation der folgenden Escape-Sequenzen zulassen
      -E	Keine Interpretation der Escape-Sequenzen.
    
    »echo« interpretiert die folgenden Escape-Sequenzen:
      \a	Alarm (Glocke)
      \b	Rücktaste (Backspace)
      \c	weitere Ausgabe unterdrücken
      \e	Escape-Zeichen
      \E        Escape Zeichen
      \f	Seitenvorschub
      \n	Zeilenvorschub
      \r	Wagenrücklauf
      \t	Horizontaler Tabulator
      \v	Vertikaler Tabulator
      \\        umgekehrter Schrägstrich (Backslash)
      \0nnn	Zeichen mit dem ASCII-Code »NNN« (oktal). »NNN« kann null
    		bis drei oktale Ziffern haben.
      \xHH	Acht-Bit-Zeichen mit dem Wert »HH« (hexadezimal). »HH«
    		kann ein oder zwei hexadezimale Ziffern haben.
    
    Rückgabewert:
    Gibt »Erfolg« zurück, außer ein Ausgabefehler tritt auf. Sie haben Post in $_. Sie haben neue Post in $_. [ Argument... ] [[ Ausdruck ]] »%c«: Falscher Befehl `%c': Ungültiges Formatierungszeichen. »%c«: Ungültiges Zeichen im symbolischen Modus. »%c«: Ungültiger Operator für den symbolischen Modus. `%c': Ungültige Zeitformatangabe. `%s': Bindung kann nicht gelöst werden. `%s': Ungültiger Alias-Name. `%s': Ungültiger KEYMAP-Name. `%s' ist eine spezielle eingebaute Funktion. `%s': Fehlendes Formatierungszeichen. `%s': Ist keine gültige Prozess- oder Jobbezeichnung. `%s': Ist kein gültiger Bezeichner. %s: Unbekannter Funktionsname. »)« erwartet. »)« erwartet, %s gefunden. »:« für einen bedingten Ausdruck erwaret. add_process: Pid %5ld (%s) markiert als immer noch laufend add_process: verarbeitet %5ld (%s) in the_pipeline alias [-p] [Name[=Wert] ... ] all_local_variables: Kein Funktionszusammenhang im aktuellen Bereich. Argument Argument erwartet. Die Unterstützung von Feldvariablen ist in dieser Shell nicht vorhanden. Versuchte Zuweisung zu keiner Variablen. Falscher Feldbezeichner. Falscher Befehlstyp Falscher Verbinder Falscher Sprung Falsche Ersetzung: Keine schließende »`« in %s. Falsche Ersetzung: Keine schließende »%s« in »%s« enthalten. bash_execute_unix_command: Tastenzuordnung für den Befehl nicht gefunden. bg [Jobbezeichnung ...] bind [-lpsvPSVX] [-m Tastaturtabelle] [-f Dateiname] [-q Name] [-u
Name] [-r Tastenfolge] [-x Tastenfolge:Shell Kommando] [Tastenfolge:readline Funktion oder Kommando] Klammererweiterung: Speicher für %s kann nicht zugewiesen werden Klammererweiterung: Speicher für %d Elemente zuzuweisen ist fehlgeschlagen Klammererweiterung: Speicher für »%s« zuzuweisen ist fehlgeschlagen break [n] Fehler: Falscher Zuweisungsoperator. builtin [Shell-Kommando [Argument ...]] caller [Ausdruck] »Return« ist nur aus einer Funktion oder einem mit »source« ausgefühten Skript möglich. kann nur innerhalb einer Funktion benutzt werden. Kann keinen neuen Datei-Deskriptor für die Eingabe von fd %d zuweisen. Temporäre Datei für here-document kann nicht erzeugt werden: %s Kann fd %d nicht auf fd %d verdoppeln. Kann die benannte Pipe %s nicht auf fd %d duplizieren. Kann %s nicht in der dynamischen Bibiliothek finden %s: %s Kann keinen Unterprozess für die Kommandoersetzung erzeugen. Der Kindprozess für die Prozessersetzung kann nicht erzeugt werden. Es kann keine Pipe für Kommandoersetzung erzeugt werden. Kann keine Pipe für die Prozessersetzung erzeugen. Die benannte Pipe %s kann nicht zum Lesen geöffnet werden. Die benannte Pipe %s kann nicht zum Schreiben geöffnet werden. Kann die dynamische Bibiliothek nicht laden %s: %s Kann nicht die Standardeingabe von /dev/null umleiten: %s Konnte den No-Delay-Modus für fd %d nicht wiederherstellen. Kann Shell-Optionen nicht gleichzeitig aktivieren und deaktivieren. Kann die Prozessgruppe des Terminals nicht setzen (%d). Gleichzeitiges »unset« einer Funktion und einer Variable ist nicht möglich. Kann die Shell nicht unterbrechen. Kann die Loginshell nicht unterbrechen. Mit »-f« können keine Funktionen erzeugt werden. Es darf nur eine Option aus -anrw angegeben werden. case Wort in [Muster [| Muster]...) Kommandos ;;]... esac cd [-L|[-P [-e]] [-@]] [Verzeichnis] child setpgid (%ld bis %ld) command [-pVv] Kommando [Argument ...] Kommandoersetzung: Kann Pipe nicht als fd 1 duplizieren. compgen [-abcdefgjksuv] [-o Option]  [-A Aktion] [-G Suchmuster] [-W Wortliste]  [-F Funktion] [-C Kommando] [-X Filtermuster] [-P Prefix] [-S Suffix] [Wort] complete [-abcdefgjksuv] [-pr] [-DE] [-o Option] [-A Aktion] [-G Suchmuster] [-W Wortliste]  [-F Funktion] [-C Kommando] [-X Filtermuster] [-P Prefix] [-S Suffix] [Name ...] Beendigung: Funktion »%s« wurde nicht gefunden compopt [-o|+o Option] [-DE] [Name ...] Bedingter binärer Operator erwartet continue [n] coproc [Name] Kommando [Umleitungen] Verzeichnis »/tmp« konnte nicht gefunden werden, bitte anlegen. cprintf: »%c«: ungültiges Formatierungszeichen gegenwärtig declare [-aAfFgilrntux] [-p] Variable[=Wert] ... Lösche den gestoppten Prozess %d der Prozessgruppe %ld. describe_pid: %ld: Prozessnummer existiert nicht. Der Verzeichnisstapel ist leer. Verzeichnisstapelindex dirs [-clpv] [+N] [-N] disown [-h] [-ar] [Jobbezeichnung ...] Division durch 0. Dynamisches Laden ist nicht verfügbar. echo [-n] [Argument ...] echo [-neE] [Argument ...] Fehlender Name für die Feldvariable. enable [-a] [-dnps] [-f Dateiname] [Name ...] Fehler beim Ermitteln der Terminalattribute: %s Fehler beim Importieren der Funktionsdefinition für »%s«. Fehler beim Setzen der Terminalattribute: %s eval [Argument ...] exec [-cl] [-a Name] [Kommando [Argumente ...]] [Umleitung ...] exit [n] »)« erwartet. Der Exponent ist kleiner als 0. export [-fn] [Name[=Wert] ...] oder export -p Ausdruck erwartet. Zu viele Rekursionen in Ausdruck. false fc [-e Editor] [-lnr] [Anfang] [Ende] oder fc -s [Muster=Ersetzung] [Kommando] fg [Jobbezeichnung] Dateideskriptor außerhalb des zugelassenen Bereichs Ein Dateiname wird als Argument benötigt. for (( Ausdruck1; Ausdruck2; Ausdruck3 )); do Kommandos; done for Name [in Wortliste ... ] ; do Kommandos; done Die abgespaltene PID %d erscheint im laufenden Prozess %d. Formatanalyseproblem: %s free:  Wurde für bereits freigegebenen Speicherbereich aufgerufen. free: Wurde für nicht zugeordneten Speicherbereich aufgerufen. free: Anfangs- und Endsegmentgrößen sind unterschiedlich. free: Underflow erkannt; mh_nbytes außerhalb des Gültigkeitsbereichs. function Name { Kommandos ; } oder Name () { Kommandos ; } Zukünftige Versionen dieser Shell werden das Auswerten arithmetischer Ersetzungen erzwingen. getwd: Kann auf das übergeordnete Verzeichnis nicht zugreifen. getopts Optionen Variable [Argumente] hash [-lr] [-p Pfadname] [-dt] [Name ...] Hashing deaktiviert. help [-dms] [Muster ...] Das in der Zeile %d beginnende Here-Dokument geht bis zum Dateiende (erwartet wird »%s«). history [-c] [-d Offset] [n] oder history -anrw [Dateiname] oder history -ps Argument [Argument...] Kommandostapelposition. Verlaufsangaben Treffer	Befehl
 Nach einem Präinkrement oder Prädekrement wird ein Bezeichner erwartet. if Kommandos; then Kommandos; [ elif Kommandos; then Kommandos; ]... [ else Kommandos; ] fi initialize_jobs: getpgrp war nicht erfolgreich. initialize_job_control: line discipline initialize_job_control: setpgid Ungültige Basis. Ungültige Basis Ungültiges Zeichen %d in exportstr für %s Ungültige hexadezimale Zahl. Ungültige Zahl. Ungültige Oktalzahl. Ungültige Signalnummer. Job %d wurde ohne Jobsteuerung gestartet. Jobbezeichnung [&] jobs [-lnprs] [Jobbezeichnung ...] or jobs -x Kommando [Arg] kill [-s Signalname | -n Signalnummer | -Signalname] [pid | job] ... oder kill -l [Signalname] Letzter Befehl: %s
 let Argument [Argument ...] Grenze Zeile %d:  Zeileneditierung ist nicht aktiviert. local [Option] Name[=Wert] ... Abgemeldet
 logout [n] Schleifen-Zähler make_here_document: Falscher Befehlstyp %d. make_local_variable: Kein Funktionszusammenhang im aktuellen Bereich. make_redirection: Weiterleitungsangabe »%d« außerhalb des zulässigen Bereichs. Malloc:  Ein als frei gekennzeichneter Speicherbereich wurde überschrieben. malloc: Speicherzusicherung gescheitert: %s.
 mapfile [-n Anzahl] [-O Quelle] [-s Anzahl] [-t] [-u fd] [-C Callback] [-c Menge] [Feldvariable] Verlege den Prozess auf einen anderen Prozessor. Fehlende »)« Fehlende »]« Fehlende hexadezimale Ziffer nach \x. Fehlendes Unicode Zeichen für \%c. Der Netzwerkbetrieb ist nicht unterstützt. Kein »=« in exportstr für %s fehlende schließende `%c' in %s. Kein Befehl gefunden Auf »%s« trifft kein Hilfethema zu. Probieren Sie »help help«, »man -k %s« oder »info %s«. Keine Job-Steuerung in dieser Shell. Keine Job-Steuerung in dieser Shell. Keine Entsprechung: %s kein anderes Verzeichnis Keine weiteren Optionen mit »-x« erlaubt. Gegenwärtig wird keine Komplettierungsfunktion ausgeführt. Keine Login-Shell: Mit »exit« abmelden! Oktalzahl nur in einer `for', `while' oder `until' Schleife sinnvoll. Pipe-Fehler pop_scope: Kopf von shell_variables ist kein vorübergehendes Umgebungsfeld pop_var_context: Kopf der shell_variables kein Funktionskontext pop_var_context: Kein global_variables-Kontext popd [-n] [+N | -N] Spannungsausfall steht bevor. print_command: Falsches Verbindungszeichen »%d«. printf [-v var] Format [Argumente] progcomp_insert: %s: NULL COMPSPEC Programmierfehler pushd [-n] [+N | -N | Verzeichnis] pwd [-LP] read [-ers] [-a Feld] [-d Begrenzer] [-i Text] [-n Zeichenanzahl] [-N Zeichenanzahl] [-p Prompt] [-t Zeitlimit] [-u fd] [Name ...] Lesefehler: %d: %s readarray [-n Anzahl] [-O Quelle] [-s Anzahl] [-t] [-u fd] [-C Callback] [-c Menge] [Feldvariable] readonly [-aAf] [Name[=Wert] ...] oder readonly -p realloc: Mit nicht zugewiesenen Argument aufgerufen. realloc: Anfangs- und Endsegmentgrößen sind unterschiedlich. realloc: Underflow erkannt; mh_nbytes außerhalb des Gültigkeitsbereichs. Rekursionsstapel leer. Weiterleitungsfehler: Fd kann nicht dupliziert werden register_alloc: %p ist bereits in der Speicherzuordnungstabelle als belegt gekennzeichnet?
 register_alloc: Speicherzuordnungstabelle ist mit FIND_ALLOC gefüllt?
 register_free: %p ist bereits in der Speicherzuordnungstabelle als frei gekennzeichnet?
 gesperrt return [n] run_pending_traps: Ungültiger Wert in trap_list[%d]: %p run_pending_traps: Signalsteuerung ist SIG_DFL, %d (%s) wird erneut dorthin gesendet. save_bash_input: Es existiert bereits ein Puffer für den neuen fd %d. select Name [in Wortliste ... ;] do Kommandos; done set [-abefhkmnptuvxBCHP] [-o Option] [--] [Argument ...] setlocale: %s: Kann die Standorteinstellungen nicht ändern (%s). setlocale: %s: Die Standorteinstellung (%s) kann nicht geändert werden: %s setlocale: LC_ALL: Kann die Locale nicht ändern (%s). setlocale: LC_ALL: Kann die Locale nicht ändern (%s): %s Shell-Stufe (%d) zu hoch, wird auf 1 zurückgesetzt. shift [n] Verschiebezähler shopt [-pqsu] [-o] [Optionsname ...] sigprocmask: %d: Ungültige Operation source Dateiname [Argumente] start_pipeline: pgrp pipe suspend [-f] Syntaxfehler Syntaxfehler im bedingten Ausdruck. Syntaxfehler im bedingten Ausdruck: Unerwartetes Zeichen »%s«. Syntaxfehler im Ausdruck. Syntaxfehler beim unerwarteten Wort »%s« Syntaxfehler beim unerwarteten Wort »%s« Syntax-Fehler: »((%s))«. Syntax Fehler: unerwartetes »;«. Syntaxfehler: Es wird ein arithmetischer Ausdruck benötigt. Syntaxfehler: Ungültiger arithmetischer Operator. Syntax Fehler: Operator erwartet. Syntaxfehler: Unerwartetes Dateiende. Systemausfall steht bevor. test [Ausdruck] time [-p] Pipeline times Zu viele Argumente. trap [-lp] [[Argument] Signalbezeichnung ...] trap_handler: Falsches Signal %d. true type [-afptP] Name [Name ...] typeset [-aAfFgilrtux] [-p] Name[=Wert] ... ulimit [-SHabcdefilmnpqrstuvxT] [Grenzwert] umask [-p] [-S] [Modus] unalias [-a] Name [Name ...] Dateiende beim Suchen nach »]]« erreicht. Dateiende beim Suchen nach »%c« erreicht. Dateiende beim Suchen nach passender »)« erreicht. Unerwartetes Argument »%s« für bedingten unären Operator Unerwartetes Argument »%s« für bedingten unären Operator Unerwartetes Argument für bedingten unären Operator Unerwartetes Argument für bedingten unären Operator Unerwartetes Sendezeichen »%d« in bedingtem Befehl Unerwartetes Sendezeichen »%c« in bedingtem Befehl Unerwartetes Sendezeichen »%s« in bedingtem Befehl Unerwartetes Sendezeichen »%s«, bedingter binärer Operator erwartet Unerwartetes Zeichen: »%s« anstatt von »)« Unbekannt Unbekannter Befehl unset [-f] [-v] [-n] [NAME ...] until Kommandos; do Kommandos; done Der Wert ist für die aktuelle Basis zu groß. variables - Namen und Bedeutung einiger Shell-Variablen wait [-n] [id ...] wait [pid ...] wait: Prozess %ld wurde nicht von dieser Shell gestartet. wait_for: Keine Aufzeichnung von Prozessdaten %ld wait_for_job: Prozess %d wurde gestoppt waitchld: schaltet WNOHANG an, um unbestimmten Block zu vermeiden Warnung:  Warnung: %s: %s Warnung: Die Option »-C« könnte unerwartete Ergebnisse liefern. Warnung: Die Option »-F« könnte unerwartete Ergebnisse liefern. while Kommandos; do Kommandos; done Schreibfehler: %s. xtrace fd (%d) != fileno xtrace fp (%d) xtrace_set: %d: Ungültige Dateibeschreibung. xtrace_set: NULL-Dateizeiger { Kommandos ; } 