��          �      ,      �     �  /   �     �     �  +   �      '     H  $   e  (   �  &   �     �     �  *   
  '   5  %   ]  +   �  �  �  "   p  0   �     �     �  5   �  '   -  "   U  %   x  5   �  -   �            &   0  5   W  -   �  /   �               	                
                                               error loading dictionary it does not contain enough DIFFERENT characters it is WAY too short it is all whitespace it is based on a (reversed) dictionary word it is based on a dictionary word it is based on your username it is based upon your password entry it is derivable from your password entry it is derived from your password entry it is too short it is too simplistic/systematic it looks like a National Insurance number. it's derivable from your password entry it's derived from your password entry you are not registered in the password file Project-Id-Version: new
Report-Msgid-Bugs-To: cracklib-devel@lists.sourceforge.net
POT-Creation-Date: 2014-10-05 10:58-0500
PO-Revision-Date: 2013-02-01 16:08+0000
Last-Translator: Hendrik Knackstedt <Unknown>
Language-Team: Novell Language <language@novell.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:09+0000
X-Generator: Launchpad (build 18115)
Language: 
 Fehler beim Laden des Wörterbuchs Es enthält nicht genug unterschiedliche Zeichen Es ist VIEL zu kurz Es besteht nur aus Leerzeichen Es basiert auf einem (umgekehrten) Wörterbucheintrag Es basiert auf einem Wörterbucheintrag Es basiert auf Ihrem Benutzernamen Es basiert auf Ihrem Passwort-Eintrag Es kann von Ihrem Passwort-Eintrag abgeleitet werden. Es wird von Ihrem Passwort-Eintrag abgeleitet Es ist zu kurz Es ist zu einfach/systematisch Es schaut nach Versicherungsnummer aus Es kann von Ihrem Passwort-Eintrag abgeleitet werden. Es wird von Ihrem Passwort-Eintrag abgeleitet Sie sind nicht in der passwd-Datei eingetragen. 