��          �   %   �      P      Q     r     �  #   �  =   �  @   �     5     B     O     \     l  '   �     �  #   �  1   �  2        A  F   P  $   �  "   �  [   �  :   ;  X   v  S   �  '   #  �  K  )   �     %     ;  ,   L  D   y  H   �     	      	     1	     E	     S	  4   n	     �	  *   �	  8   �	  8   
     N
  k   ^
  .   �
  !   �
  X     1   t  ^   �  G     (   M                                                                                                    	               
           - Edit Web credentials settings Add account… All applications Another instance is already running Are you sure that you wish to remove this Ubuntu Web Account? Control whether this application integrates with Online Accounts Edit Options Grant access Legal notice Online Accounts Online Accounts preferences Online account credentials and settings Options Output version information and exit Please authorize Ubuntu to access your %s account Please authorize Ubuntu to access your %s account: Remove Account Run '%s --help' to see a full list of available command line options.
 Select to configure a new %s account Show accounts that integrate with: The Web Account which manages the integration of %s with your applications will be removed. The following applications integrate with your %s account: There are currently no account providers available which integrate with this application There are currently no applications installed which integrate with your %s account. Your online %s account is not affected. Project-Id-Version: gnome-control-center-signon
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-01 12:56+0000
PO-Revision-Date: 2013-03-18 23:40+0000
Last-Translator: Hendrik Knackstedt <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:14+0000
X-Generator: Launchpad (build 18115)
  - Web-Zugangsdateneinstellungen anpassen Konto hinzufügen … Alle Anwendungen Eine andere Instanz wird bereits ausgeführt Sind Sie sicher, dass Sie dieses Ubuntu-Web-Konto löschen möchten? Legen Sie fest, ob sich diese Anwendung mit Online-Konten verbinden soll Einstellungen bearbeiten Zugriff erlauben Rechtlicher Hinweis Online-Konten Online-Konto-Einstellungen Zugangsdaten und Einstellungen für das Online-Konto Einstellungen Versionsinformationen anzeigen und beenden Bitte genehmigen Sie Ubuntu den Zugriff auf Ihr %s-Konto Bitte erlauben Sie Ubuntu, auf Ihr %s-Konto zuzugreifen: Konto entfernen Führen Sie »%s --help« aus, um eine vollständige Liste verfügbarer Befehlszeilenoptionen zu erhalten.
 Auswählen, um ein neues %s-Konto einzurichten Darin verwendete Konten anzeigen: Das Web-Konto, das die Verwendung von %s in Ihren Anwendungen verwaltet, wird gelöscht. Die folgenden Anwendungen verwenden Ihr %s-Konto: Im Moment sind keine Anbieter für Konten verfügbar, die in dieser Anwendung verwendet werden Zurzeit sind keine Anwendungen installiert, die Ihr %s-Konto verwenden. Ihr %s-Online-Konto ist nicht betroffen. 