��    A      $  Y   ,      �     �  �   �     f     �     �     �     �     �     �     �  7   �     6     Q     V     m     |     �     �     �  )   �       ?     4   S     �  %   �  &   �     �  -   	     9	     A	  "   X	  6   {	  >   �	     �	     
  &   '
  M   N
  +   �
  6   �
  #   �
     #  .   3  '   b     �     �     �     �     �       G     ,   _     �     �  "   �     �     �     �                  (   3     \     v  1   �  �  �     U  	  n     x     �     �     �     �  !   �     
       B   !     d     }  (   �     �     �  *   �            9   ;     u  R   �  9   �  8     4   K  '   �     �  =   �     �       $     B   D  R   �     �  ,   �  "     a   B  7   �  ;   �  '        @  3   S  )   �  %   �     �  %   �       8   )     b  ]   �  6   �          .  (   3     \     {     �  #   �     �     �  *   �  %        5  D   L     +   %   $   /   1      &                   =   <      @      ?   *   ;              (   
      A       >             #      0       ,                                   .         "                                        '                   9   -   )   	   :   4   !           5   8   7   2                     3          6    
<action> is one of:
 
<name> is the device to create under %s
<device> is the encrypted device
<key slot> is the LUKS key slot number to modify
<key file> optional key file for the new key for luksAddKey action
 %s is not a LUKS partition
 %s: requires %s as arguments <device> <device> <key slot> <device> <name>  <device> [<new key file>] <name> <name> <device> Align payload at <n> sector boundaries - for luksFormat Argument <action> missing. BITS Can't open device: %s
 Command failed Command successful.
 Create a readonly mapping Display brief usage Do not ask for confirmation Failed to obtain device mapper directory. Help options: How many sectors of the encrypted data to skip at the beginning How often the input of the passphrase can be retried Key %d not active. Can't wipe.
 Key size must be a multiple of 8 bits PBKDF2 iteration time for LUKS (in ms) Print package version Read the key from a file (can be /dev/random) SECTORS Show this help message Shows more detailed error messages The cipher used to encrypt the disk (see /proc/crypto) The hash used to create the encryption key from the passphrase The size of the device The size of the encryption key The start offset in the backend device This is the last keyslot. Device will become unusable after purging this key. This will overwrite data on %s irrevocably. Timeout for interactive passphrase prompt (in seconds) Unable to obtain sector size for %s Unknown action. Verifies the passphrase by asking for it twice [OPTION...] <action> <action-specific>] add key to LUKS device create device dump LUKS partition information formats a LUKS device key %d active, purge first.
 key %d is disabled.
 key material section %d includes too few stripes. Header manipulation?
 memory allocation error in action_luksFormat modify active device msecs open LUKS device as mapping <name> print UUID of LUKS device remove LUKS mapping remove device resize active device secs show device status tests <device> for LUKS partition header unknown hash spec in phdr unknown version %d
 wipes key with number <key slot> from LUKS device Project-Id-Version: cryptsetup
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2007-01-02 16:49+0100
PO-Revision-Date: 2012-02-28 08:20+0000
Last-Translator: greenscandic <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:25+0000
X-Generator: Launchpad (build 18115)
 
<Aktion> ist eine von:
 
<Name> ist das Gerät, das unter %s erzeugt wird
<Gerät> ist das verschlüsselte Gerät
<Schlüsselfach> ist die Nummer des zu verändernden LUKS-Schlüsselfachs
<Schlüsseldatei> optionale Schlüsseldatei für für den neuen Schlüssel der »luksAddKey«-Aktion
 %s ist keine LUKS-Partition
 %s: Benötigt %s als Argumente <Gerät> <Gerät> <Schlüsselfach> <Gerät> <Name>  <Gerät> [<neue Schlüsseldatei>] <Name> <Name> <Gerät> Nutzdaten an Grenzen von <n> Sektoren ausrichten - für luksFormat Argument <Aktion> fehlt. BITS Gerät kann nicht geöffnet werden: %s.
 Befehl fehlgeschlagen Befehl erfolgreich.
 Eine schreibgeschützte Zuordnung erzeugen Kurze Aufrufsyntax anzeigen Nicht auf Bestätigung warten Gerätezuordnungsverzeichnis kann nicht ermittelt werden. Hilfe-Optionen: Wieviele Sektoren der verschlüsselten Daten am Anfang übersprungen werden sollen Wie oft die Eingabe des Passsatzes wiederholt werden kann Schlüssel %d nicht aktiv. Kann nicht gelöscht werden.
 Schlüsselgröße muss ein Vielfaches von 8 Bit sein PBKDF2-Iterationszeit für LUKS (in ms) Paketversion ausgeben Schlüssel aus einer Datei lesen (kann auch /dev/random sein) SEKTOREN Diesen Hilfetext anzeigen Zeigt detailliertere Fehlermeldungen Der Algorithmus zum Verschlüsseln der Platte (siehe /proc/crypto) Das Hashverfahren, um den Verschlüsselungsschlüssel aus dem Passsatz zu erzeugen Die Größe des Geräts Die Größe des Verschlüsselungsschlüssels Der Startversatz im Backend-Gerät Dies ist das letzte Schlüsselfach. Wenn Sie das auch noch löschen, wird das Gerät unbrauchbar. Hiermit überschreiben Sie Daten auf %s unwiderruflich. Frist für interaktive Eingabe des Passsatzes (in Sekunden) Sektorgröße für %s nicht ermittelbar Unbekannte Aktion. Verifiziert den Passsatz durch doppeltes Nachfragen [OPTION …] <Aktion> <aktionsabhängig>] Schlüssel zu LUKS-Gerät hinzufügen Gerät erstellen LUKS-Partitionsinformationen ausgeben Formatiert ein LUKS-Gerät Schlüssel %d aktiv, wird zuerst vollständig entfernt.
 Schlüssel %d ist deaktiviert.
 Schlüsselmaterialabschnitt %d enthält zu wenige Inhalte. Soll der Header geändert werden?
 Fehler bei der Speicherallokation in action_luksFormat aktives Gerät ändern msek LUKS-Gerät als Zuordnung <Name> öffnen UUID des LUKS-Geräts ausgeben LUKS-Zuordnung entfernen Gerät entfernen Größe des aktiven Geräts ändern sek Gerätestatus anzeigen <Gerät> auf LUKS-Partitions-Header testen Unbekannte Hash-Spezifikation in phdr Unbekannte Version %d
 Schlüssel mit der Nummer <Schlüsselfach> vom LUKS-Gerät entfernen 