��          4      L       `   �   a      �   �     �   �     Y                    This task sets up a basic user environment, providing a reasonably small selection of services and tools usable on the command line. standard system utilities Project-Id-Version: tasksel-tasks
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-11 16:19-0500
PO-Revision-Date: 2015-12-22 01:10+0000
Last-Translator: Holger Wansing <Unknown>
Language-Team: Debian German <debian-l10n-german@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:37+0000
X-Generator: Launchpad (build 18115)
Language: 
 Dieser Task richtet eine grundlegende Benutzerumgebung ein; er stellt eine kleine, aber vernünftige Auswahl von Diensten und Werkzeugen für die Kommandozeile bereit. Standard-Systemwerkzeuge 