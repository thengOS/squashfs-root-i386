��          �      <      �  $   �  +   �  F     4   I  L   ~  F   �  4     "   G     j     |     �     �     �     �     �       0   #  �  T  ,     2   ?  K   r  .   �  a   �  V   O  A   �  +   �          #     8     T     l     �  $   �     �  3   �     
                                   	                                                             CommentA system restart is required CommentAdditional drivers can be installed CommentAn application has crashed on your system (now or in the past) CommentControl the notifications for system helpers CommentExtra packages can be installed to enhance application functionality CommentExtra packages can be installed to enhance system localization CommentSoftware upgrade notifications are available CommentSystem Notification Helper NameApport Crash NameDriver Enhancement NameLocalization Enhancement NameNotification Helper NameOther Notifications NameReboot Required NameRestricted Install NameUpgrade Hook X-KDE-KeywordsNotify,Alerts,Notification,popups Project-Id-Version: kubuntu-notification-helper
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-04-01 17:07+0000
PO-Revision-Date: 2014-03-02 19:49+0000
Last-Translator: Christoph Klotz <klotzchristoph@gmail.com>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:48+0000
X-Generator: Launchpad (build 18115)
 Es ist ein Neustart des Systems erforderlich Es können zusätzliche Treiber installiert werden Eine Anwendung auf Ihrem System ist abgestürzt (jetzt oder bereits vorher) Benachrichtigungen für Systemhelfer festlegen Zusätzliche Pakete können installiert werden, um den Funktionsumfang der Anwendung zu erweitern Es können weitere Pakete installiert werden um die System-Lokalisierung zu verbessern Benachrichtigungen über Softwareaktualisierungen sind verfügbar Hilfsprogramm für Systembenachrichtigungen Apport-Absturz Treiber-Verbesserung Lokalisierungs-Verbesserung Benachrichtigungshelfer Andere Benachrichtigungen Neustart erforderlich Installation eingeschränkter Pakete Software-Aktualisierungen Benachrichtigen,Warnungen,Benachrichtigung,Hinweise 