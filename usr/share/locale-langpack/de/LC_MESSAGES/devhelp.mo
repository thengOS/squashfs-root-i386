��    l      |  �   �      0	  I   1	  J   {	  F   �	  A   
     O
     T
     Y
     ^
     c
     h
     m
     r
     v
  $   z
     �
     �
  
   �
     �
     �
     �
     �
          
  K     �   f     (     8     N     k  
   �     �     �  !   �  -   �     �       6     "   H     k     q     y     �     �     �  ,   �     �     �            #   #     G     M  4   i     �     �     �     �     �     �     �  2        9  #   N     r     �  '   �  "   �  '   �  "     #   &     J  "   i  '   �     �     �     �     �     �  5     0   B  1   s  /   �  5   �       "   )     L     e     �     �     �     �     �     �     �     �               (     5     B     I  
   O     Z     l  /   ~  Z   �     	  �    T     T   k  Q   �  M        `     e     j     o     t     y     ~     �     �  '   �     �     �     �     �     �  ,   �          5     =  W   N  �   �     �     �  $   �     �       	        &  0   9  =   j  !   �     �  F   �  1     	   O     Y     ]     f     ~     �  3   �     �     �            ,   &     S     Y  ;   x     �     �  !   �     �     �  (        0  2   @      s  B   �     �     �  ,   	  +   6  *   b  )   �     �     �     �  )        ;     Y     _     o  0   �  U   �  7      8   F   8      Y   �      !  $   ,!     Q!  '   j!  &   �!  %   �!  $   �!     "     
"     "     '"     /"     C"     \"     m"     ~"     �"     �"     �"     �"     �"  2   �"  Z   #  �  g#     _   V      ^          N   W   /          #   5   !   M   a   ?   .   e   7       b               `   1   I       4   j      K       "      <   3                      2   H               (   R             h   
       [       c   8   \   g             	      f         -   '   6       E   ]   S              T       &          k       :       =             C   i       J           %   0      B       F           U   *   D       +   G      X       d   A   Q   L       ,               P         Z          9          $      ;                Y   l   >   )      @   O    "name" and "link" elements are required inside '%s' on line %d, column %d "name" and "link" elements are required inside <sub> on line %d, column %d "title", "name" and "link" elements are required at line %d, column %d "type" element is required inside <keyword> on line %d, column %d 100% 125% 150% 175% 200% 300% 400% 50% 75% A developers' help browser for GNOME Back Book Book Shelf Book: Books disabled Cannot uncompress book '%s': %s Developer's Help program Devhelp Devhelp Website Devhelp integrates with other applications such as Glade, Anjuta, or Geany. Devhelp is an API documentation browser. It provides an easy way to navigate through libraries, search by function, struct, or macro. It provides a tabbed interface and allows to print results. Devhelp support Devhelp — Assistant Display the version and exit Documentation Browser Empty Page Enabled Enum Error opening the requested link. Expected '%s', got '%s' at line %d, column %d Font for fixed width text Font for text Font for text with fixed width, such as code examples. Font for text with variable width. Fonts Forward Function Group by language Height of assistant window Height of main window Invalid namespace '%s' at line %d, column %d KEYWORD Keyword Language: %s Language: Undefined List of books disabled by the user. Macro Main window maximized state Makes F2 bring up Devhelp for the word at the cursor New _Tab New _Window Opens a new Devhelp window Page Preferences Quit any running Devhelp S_maller Text Search and display any hit in the assistant window Search for a keyword Selected tab: "content" or "search" Show API Documentation Struct The X position of the assistant window. The X position of the main window. The Y position of the assistant window. The Y position of the main window. The height of the assistant window. The height of the main window. The width of the assistant window. The width of the index and search pane. The width of the main window. Title Type Use system fonts Use the system default fonts. Whether books should be grouped by language in the UI Whether the assistant window should be maximized Whether the assistant window should be maximized. Whether the main window should start maximized. Which of the tabs is selected: "content" or "search". Width of the assistant window Width of the index and search pane Width of the main window X position of assistant window X position of main window Y position of assistant window Y position of main window _About _About Devhelp _Close _Find _Fixed width: _Group by language _Larger Text _Normal Size _Preferences _Print _Quit _Side pane _Use system fonts _Variable width:  documentation;information;manual;developer;api; https://wiki.gnome.org/Apps/Devhelp?action=AttachFile&#x26;do=view&#x26;target=devhelp.png translator-credits Project-Id-Version: devhelp master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=devhelp&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-02-16 12:08+0000
PO-Revision-Date: 2015-11-10 05:01+0000
Last-Translator: Paul Seyfert <Unknown>
Language-Team: Deutsch <gnome-de@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:09+0000
X-Generator: Launchpad (build 18115)
Language: de_DE
 »name« sowie »link«-Elemente innerhalb »%s« erforderlich - Zeile %d, Spalte %d »name« sowie »link«-Elemente innerhalb <sub> erforderlich in Zeile %d, Spalte %d »title«-, »name«- sowie »link«-Elemente erforderlich in Zeile %d, Spalte %d Elemente »type« ist innerhalb <keyword> erforderlich in Zeile %d, Spalte %d 100% 125% 150% 175% 200% 300% 400% 50% 75% Ein GNOME-Hilfe-Browser für Entwickler Zurück Buch Büchersammlung Buch: Bücher deaktiviert Buch »%s« konnte nicht entpackt werden: %s Hilfeprogramm für Entwickler Devhelp Devhelp-Webseite Devhelp ist automatisch in anderen Anwendungen wie Glade, Anjuta oder Geany integriert. Devhelp ist ein API-Dokumentations-Browser. Auf einfache Art und Weise können Bibliotheken angezeigt und nach Funktion, Struct (Datenstruktur) oder Makro durchsucht werden. Die Oberfläche verwendet Reiter und Suchergebnisse können ausgedruckt werden. Devhelp-Unterstützung Devhelp — Assistent Programmversion anzeigen und beenden Dokumentation durchsuchen Leere Seite Aktiviert Enum (Aufzählung) Fehler beim Öffnen des angeforderten Verweises. »%s« wurde erwartet, »%s« erhalten in Zeile %d, Spalte %d Schrift für dicktengleichen Text Textschrift Schrift für Text gleicher Schriftzeichenbreite wie z.B. Programmcode. Schrift für Text variabler Schriftzeichenbreite. Schriften Vor Funktion Nach Sprache gruppieren Höhe des Zusatzfensters Höhe des Hauptfensters Ungültiger Namensraum »%s« - Zeile %d, Spalte %d SCHLÜSSELWORT Schlüsselwort Sprache: %s Sprache: Nicht definiert Bücherliste wurde vom Benutzer deaktiviert. Macro Hauptfenstermaximierungsstatus F2 schlägt das Wort unter der Eingabemarke in Devhelp nach Neuer _Reiter Neues _Fenster Öffnet ein neues Devhelp-Fenster Seite Einstellungen Alle laufenden Devhelp-Instanzen beenden _Kleinerer Text Suchen und jeden Treffer im Zusatzfenster anzeigen Nach einem Schlüsselwort suchen Ausgewählter Reiter: »content« (Inhalt) oder »search« (Suche) API-Dokumentation anzeigen Struct (Datenstruktur) Die horizontale Position des Zusatzfensters. Die horizontale Position des Hauptfensters. Die vertikale Position des Zusatzfensters. Die vertikale Position des Hauptfensters. Die Höhe des Zusatzfensters. Die Höhe des Hauptfensters. Die Breite des Zusatzfensters. Die Breite des Index- und Suchabschnitts. Die Breite des Hauptfensters. Titel Type (Datentyp) Systemschrift verwenden Die vom System vorgegebenen Schriften verwenden. Legt fest, ob Bücher in der Benutzeroberfläche nach Sprache gruppiert werden sollen Legt fest, ob das Assistent-Fenster maximiert sein soll Legt fest, ob das Assistent-Fenster maximiert sein soll. Legt fest, ob das Hauptfenster beim Start maximiert ist. Welcher der Reiter aktuell ausgewählt ist, »content« (Inhalt) oder »search« (Suche). Breite des Zusatzfensters Breite des Index- und Suchabschnitts Breite des Hauptfensters Horizontale Position des Zusatzfensters Horizontale Position des Hauptfensters Vertikale Position des Zusatzfensters Vertikale Position des Hauptfensters _Info _Info zu Devhelp S_chließen _Suchen _Dicktengleichheit: Nach Sprache _gruppieren _Größerer Text _Normale Größe _Einstellungen _Drucken _Beenden _Seitenleiste S_ystemschrift verwenden _Variable Breite:  Dokumentation;Information;Handbuch;Entwickler;API; https://wiki.gnome.org/Apps/Devhelp?action=AttachFile&#x26;do=view&#x26;target=devhelp.png Christian Meyer
Christian Neumair
Christian Kirbach
Mario Blättermann

Launchpad Contributions:
  Andre Klapper https://launchpad.net/~a9016009
  Christian Kirbach https://launchpad.net/~christian-kirbach-e
  Daniel Winzen https://launchpad.net/~q-d
  Frank Arnold https://launchpad.net/~frank-scirocco-5v-turbo
  Hendrik Richter https://launchpad.net/~hendi
  Keruskerfuerst https://launchpad.net/~arminmohring
  Mario Blättermann https://launchpad.net/~mario.blaettermann
  Mario Blättermann https://launchpad.net/~mariobl
  Paul Seyfert https://launchpad.net/~pseyfert
  Wolfgang Stöggl https://launchpad.net/~c72578
  phiker https://launchpad.net/~k-philipp 