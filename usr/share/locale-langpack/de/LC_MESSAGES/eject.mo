��    O      �  k         �  $   �  &   �  )         /  #   P  -   t  *   �  +   �  =   �  )   7  ;   a  <   �  @   �      	  :   <	     w	     �	      �	     �	  $   �	     

     '
  (   F
     o
     �
     �
     �
     �
     �
  '      &   (  "   O  $   r     �  $   �     �      �  #     )   ;  ,   e  0   �  "   �     �  /         0     P     n      �  !   �      �  #   �          (  ?   @  4   �  -   �  4   �  4     $   M  &   r  ,   �     �  "   �       ;     *   V     �     �  %   �     �  &   �  �      �  �   �  5   r     �     �     �  v  �  2   ^  /   �  :   �  #   �        $   >  )   c  5   �  G   �  2     D   >  X   �  k   �  !   H  C   j  %   �     �  (   �       1   =  !   o     �  2   �     �       &        =     W     l  2   �  0   �  *   �  )     *   ?  "   j     �  '   �  !   �  3   �  6   *  :   a  ,   �  &   �  ?   �  8   0   1   i      �   )   �   *   �   )   !  #   -!     Q!      m!  X   �!  6   �!  0   "  2   O"  <   �"  -   �"  /   �"  *   #     H#  &   c#     �#  F   �#  ,   �#  &   $     B$  "   U$      x$  0   �$  =  �$    )  �   *  6   �*     +  $   ,+     Q+     
                 1      M                                        !       H   5       0       %          /   ?             '   	   $   K   *          N   F   4                 I          +      7   2          >   9   D   E   J   L   O   B   "                          A       ,   @   )      :           #              &   <   .       C   ;       (       8   =   6   -       3                 G    %s: %s doesn't exist, skipping call
 %s: %s is encrypted on real device %s
 %s: CD-ROM auto-eject command failed: %s
 %s: CD-ROM eject command failed
 %s: CD-ROM eject command succeeded
 %s: CD-ROM load from slot command failed: %s
 %s: CD-ROM select disc command failed: %s
 %s: CD-ROM select speed command failed: %s
 %s: CD-ROM select speed command not supported by this kernel
 %s: CD-ROM tray close command failed: %s
 %s: CD-ROM tray close command not supported by this kernel
 %s: CD-ROM tray toggle command not supported by this kernel
 %s: Could not restore original CD-ROM speed (was %d, is now %d)
 %s: FindDevice called too often
 %s: IDE/ATAPI CD-ROM changer not supported by this kernel
 %s: SCSI eject failed
 %s: SCSI eject succeeded
 %s: `%s' can be mounted at `%s'
 %s: `%s' is a link to `%s'
 %s: `%s' is a multipartition device
 %s: `%s' is mounted at `%s'
 %s: `%s' is not a mount point
 %s: `%s' is not a multipartition device
 %s: `%s' is not mounted
 %s: closing tray
 %s: could not allocate memory
 %s: default device: `%s'
 %s: device is `%s'
 %s: device name is `%s'
 %s: disabling auto-eject mode for `%s'
 %s: enabling auto-eject mode for `%s'
 %s: error while allocating string
 %s: error while finding CD-ROM name
 %s: error while reading speed
 %s: exiting due to -n/--noop option
 %s: expanded name is `%s'
 %s: floppy eject command failed
 %s: floppy eject command succeeded
 %s: invalid argument to --auto/-a option
 %s: invalid argument to --cdspeed/-x option
 %s: invalid argument to --changerslot/-c option
 %s: invalid argument to -i option
 %s: listing CD-ROM speed
 %s: maximum symbolic link depth exceeded: `%s'
 %s: restored original speed %d
 %s: saving original speed %d
 %s: selecting CD-ROM disc #%d
 %s: setting CD-ROM speed to %dX
 %s: setting CD-ROM speed to auto
 %s: tape offline command failed
 %s: tape offline command succeeded
 %s: toggling tray
 %s: too many arguments
 %s: tried to use `%s' as device name but it is no block device
 %s: trying to eject `%s' using CD-ROM eject command
 %s: trying to eject `%s' using SCSI commands
 %s: trying to eject `%s' using floppy eject command
 %s: trying to eject `%s' using tape offline command
 %s: unable to eject, last error: %s
 %s: unable to exec umount of `%s': %s
 %s: unable to find or open device for: `%s'
 %s: unable to fork: %s
 %s: unable to open /etc/fstab: %s
 %s: unable to open `%s'
 %s: unable to read the speed from /proc/sys/dev/cdrom/info
 %s: unmount of `%s' did not exit normally
 %s: unmount of `%s' failed
 %s: unmounting `%s'
 %s: unmounting device `%s' from `%s'
 %s: using default device `%s'
 %s: using device name `%s' for ioctls
 Eject version %s by Jeff Tranter (tranter@pobox.com)
Usage:
  eject -h				-- display command usage and exit
  eject -V				-- display program version and exit
  eject [-vnrsfqpm] [<name>]		-- eject device
  eject [-vn] -d			-- display default device
  eject [-vn] -a on|off|1|0 [<name>]	-- turn auto-eject feature on or off
  eject [-vn] -c <slot> [<name>]	-- switch discs on a CD-ROM changer
  eject [-vn] -t [<name>]		-- close tray
  eject [-vn] -T [<name>]		-- toggle tray
  eject [-vn] -i on|off|1|0 [<name>]	-- toggle manual eject protection on/off
  eject [-vn] -x <speed> [<name>]	-- set CD-ROM max speed
  eject [-vn] -X [<name>]		-- list CD-ROM available speeds
Options:
  -v	-- enable verbose output
  -n	-- don't eject, just show device found
  -r	-- eject CD-ROM
  -s	-- eject SCSI device
  -f	-- eject floppy
  -q	-- eject tape
  -p	-- use /proc/mounts instead of /etc/mtab
  -m	-- do not unmount device even if it is mounted
 Long options:
  -h --help   -v --verbose      -d --default
  -a --auto   -c --changerslot  -t --trayclose  -x --cdspeed
  -r --cdrom  -s --scsi         -f --floppy     -X --listspeed     -q --tape
  -n --noop   -V --version
  -p --proc   -m --no-unmount   -T --traytoggle
 Parameter <name> can be a device file or a mount point.
If omitted, name defaults to `%s'.
By default tries -r, -s, -f, and -q in order until success.
 eject version %s by Jeff Tranter (tranter@pobox.com)
 unable to open %s: %s
 usage: volname [<device-file>]
 volname Project-Id-Version: eject 2.1.3-1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-02-20 00:35+0100
PO-Revision-Date: 2013-01-25 12:25+0000
Last-Translator: Hendrik Knackstedt <Unknown>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:13+0000
X-Generator: Launchpad (build 18115)
 %s: %s existiert nicht, Aufruf wird übersprungen
 %s: %s ist verschlüsselt auf echtem Gerät %s
 %s: Automatisches Auswerfen der CD-ROM fehlgeschlagen: %s
 %s: CD-eject war nicht erfolgreich
 %s: CD-eject war erfolgreich
 %s: CD-ROM laden fehlgeschlagen: %s
 %s: CD-ROM auswählen fehlgeschlagen: %s
 %s: CD-ROM-Geschwindigkeit setzen fehlgeschlagen: %s
 %s: CD-ROM-Geschwindigkeit setzen von diesem Kernel nicht unterstützt
 %s: CD-ROM-Laufwerk schließen fehlgeschlagen: %s
 %s: CD-ROM-Laufwerk schließen von diesem Kernel nicht unterstützt
 %s: Automatisches Schließen/Öffnen des Laufwerks von diesem Kernel nicht unterstützt
 %s: Die ursprüngliche CD-ROM-Geschwindigkeit konnte nicht wiederhergestellt werden (war %d, ist jetzt %d)
 %s: FindDevice zu oft aufgerufen
 %s: IDE/ATAPI CD-ROM-Wechsler von diesem Kernel nicht unterstützt
 %s: SCSI-eject war nicht erfolgreich
 %s: SCSI-eject war erfolgreich
 %s: `%s' kann nach`%s' gemountet werden
 %s: `%s' ist ein Link auf `%s'
 %s: `%s' ist ein Gerät mit mehreren Partitionen
 %s: `%s' ist nach `%s' gemountet
 %s: `%s' ist kein Mount-Punkt
 %s: `%s' ist kein Gerät mit mehreren Partitionen
 %s: `%s' ist nicht gemountet
 %s: schließe jetzt
 %s: Konnte keinen Speicher allozieren
 %s: Standardgerät: `%s'
 %s: Gerät ist `%s'
 %s: Gerätename ist `%s'
 %s: deaktiviere Automatisches Auswerfen für `%s'
 %s: aktiviere Automatisches Auswerfen für `%s'
 %s: Fehler beim Zuweisen der Zeichenkette
 %s: Fehler beim Suchen des CD-ROM-Namens
 %s: Fehler beim Lesen der Geschwindigkeit
 %s: beende wegen -n/--noop Option
 %s: erweiterter Name ist `%s'
 %s: Floppy-eject war nicht erfolgreich
 %s: Floppy-eject war erfolgreich
 %s: ungültiges Argument für die --auto/-a Option
 %s: ungültiges Argument für die --cdspeed/-x Option
 %s: ungültiges Argument für die --changerslot/-c Option
 %s: ungültiges Argument für die -i Option
 %s: CD-ROM-Geschwindigkeiten anzeigen
 %s: maximale Tiefe für symbolische Links überschritten: `%s'
 %s: Ursprungsgeschwindigkeit %d wurde wiederhergestellt
 %s: Ursprungsgeschwindigkeit %d wird gespeichert
 %s: wähle CD #%d
 %s: setze CD-ROM-Geschwindigkeit auf %dX
 %s: setze CD-ROM-Geschwindigkeit auf auto
 %s: `Band offline' war nicht erfolgreich
 %s: `Band offline' war erfolgreich
 %s: öffne/schließe jetzt
 %s: zu viele Optionen angegeben
 %s: versuchte `%s' als Gerätenamen zu benutzen, aber es ist kein blockbasiertes Gerät
 %s: Versuche `%s' mit dem CD-eject-Befehl auszuwerfen
 %s: versuche `%s' mit SCSI-Befehlen auszuwerfen
 %s: versuche `%s' mit Floppy-Befehlen auszuwerfen
 %s: versuche `%s' mit dem `Band offline'-Befehl auszuwerfen
 %s: Kann nicht auswerfen! Letzter Fehler: %s
 %s: kann umount für `%s' nicht ausführen: %s
 %s: kann Gerät `%s' nicht finden/öffnen
 %s: kann nicht forken: %s
 %s: kann /etc/fstab nicht öffnen: %s
 %s: kann `%s' nicht öffnen
 %s: kann die Geschwindigkeit nicht aus /proc/sys/dev/cdrom/info lesen
 %s: Unmounten von `%s' nicht normal beendet
 %s: Unmounten von `%s' fehlgeschlagen
 %s: Unmounte `%s'
 %s: Unmounte Gerät `%s' von `%s'
 %s: benutze Standardgerät `%s'
 %s: Gerätename »%s« wird für ioctls benutzt
 Eject Version %s von Jeff Tranter (tranter@pobox.com)
Usage:
  eject -h				-- gibt die Hilfe aus und beendet das Programm
  eject -V				-- gibt Versioninformation aus und beendet das Programm
  eject [-vnrsfqpm] [<name>]		-- Laufwerk öffnen
  eject [-vn] -d			-- zeige Standardlaufwerk an
  eject [-vn] -a on|off|1|0 [<name>]	-- auto-eject an-/ausschalten
  eject [-vn] -c <slot> [<name>]	-- wechselt CD im CD-Wechsler
  eject [-vn] -t [<name>]		-- Laufwerk schließen
  eject [-vn] -T [<name>]		-- Laufwerk öffnen oder schließen
  eject [-vn] -i on|off|1|0 [<name>]	-- Schutz vor manuellem Öffnen an-/ausschalten
  eject [-vn] -x <speed> [<name>]	-- maximale CD-ROM-Geschwindigkeit setzen
  eject [-vn] -X [<name>]		-- verfügbare CD-ROM-Geschwindigkeiten anzeigen
Optionen:
  -v	-- zeige Details an
  -n	-- Laufwerk nicht öffnen, nur gefundenes Gerät anzeigen
  -r	-- CD-ROM auswerfen
  -s	-- Disk im SCSI-Gerät auswerfen
  -f	-- Floppy auswerfen
  -q	-- Band auswerfen
  -p	-- benutze /proc/mounts statt /etc/mtab
  -m	-- Gerät nicht unmounten, selbst wenn es gemounted ist
 Lange Optionen:
  -h --help   -v --verbose      -d --default
  -a --auto   -c --changerslot  -t --trayclose  -x --cdspeed
  -r --cdrom  -s --scsi         -f --floppy     -X --listspeed     -q --tape
  -n --noop   -V --version
  -p --proc   -m --no-unmount   -T --traytoggle
 Parameter <Name> kann eine Gerätedatei oder ein Mount-Punkt sein.
Wenn ausgelassen wird `%s' gewählt.
Versucht standardmäßig -r, -s, -f und -q in dieser Reihenfolge bis es funktioniert.
 eject Version %s von Jeff Tranter (tranter@pobox.com)
 kann %s nicht öffnen: %s
 Benutzung: volname [<Gerätedatei>]
 Volname 