��          �      L      �     �     �     �     �     �       3        M     [     v  7   �  ,   �     �          '  #   ?  
   c  	   n  �  x  %   F     l     n     �     �     �  B   �     �  (         1  I   R  ;   �     �     �       '   .     V     h                                                 	                                     
              %d New Message %d New Messages : Compose New Message Contacts Evolution Indicator Inbox Only create notifications for new mail in an Inbox. Pla_y a sound Play a sound for new mail. Show a notification bubble. Show new message count in the message indicator applet. Shows new mail count in a message indicator. When New Mail Arrives When new mail arri_ves in _Display a notification _Indicate new messages in the panel any Folder any Inbox Project-Id-Version: evolution-indicator
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-06-24 15:14+0000
PO-Revision-Date: 2013-04-11 09:03+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 18:39+0000
X-Generator: Launchpad (build 18115)
 %d neue Nachricht %d neue Nachrichten : Neue Nachricht erstellen Kontakte Evolution-Benachrichtigungen Eingang Nur für neue Nachrichten im Eingang Benachrichtigungen erstellen. Klang abspielen Einen Klang bei neuen E-Mails abspielen. Benachrichtigungsblase anzeigen. Anzahl neuer Nachrichten im Nachrichten-Benachrichtigungsapplet anzeigen. Zeigt die Anzahl neuer E-Mails im Benachrichtigungsbereich. Wenn neue E-Mails eingehen Wenn neue E-Mails an_kommen Eine Benachrichtigung anzeigen Im Panel auf neue Nachrichten hinweisen beliebiger Ordner jeder Eingang 