��          �      \      �  /   �               1  !   H  6   j  !   �     �     �     �     �          !  7   A     y  Y   |     �     �     �  �  �  +   �     �  !   �     �  %     6   8  %   o     �     �     �     �     �      �  1        B  X   G     �     �  	   �                                          
                     	                                    Allow '%s' to access your location information? Demo geoclue agent Demo geolocation application Display version number Enable submission of network data Exit after T seconds of inactivity. Default: 0 (never) Exit after T seconds. Default: 30 Find your current location GeoClue Geoclue Demo agent Geolocation Geolocation service in use
 Geolocation service not in use
 Nickname to submit network data under (2-32 characters) No Request accuracy level A. Country = 1, City = 4, Neighborhood = 5, Street = 6, Exact = 8. Where am I? Yes geolocation; Project-Id-Version: geoclue-2.0
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-01-12 09:11+0000
PO-Revision-Date: 2016-02-26 18:21+0000
Last-Translator: schuko24 <gerdsaenger@t-online.de>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:33+0000
X-Generator: Launchpad (build 18115)
 Darf '%s' auf Ihre Standortdaten zugreifen? Demo Geoclue Agent Demo Standortbestimmungsanwendung Versionsnummer anzeigen Meldung der Netzwerkdaten einschalten Nach T Sekunden Inaktivität beenden. Vorgabe: 0 (nie) Nach T Sekunden beeenden. Vorgabe: 30 aktuellen Standort finden GeoClue Geoclue Demo Dienst Standort verwendeter Ortungsdienst
 nicht verwendeter Ortungsdienst
 Name für das zu meldende Netzwerk (2-32 Zeichen) Nein Genauigkeitsgrad A anfordern. Land = 1, Stadt = 4, Umgebung = 5, Straße = 6, Genau = 8. Wo bin ich? Ja Standort; 