��    	     d  �  �       �+  ~   �+  X    ,  H   y,     �,     �,     �,  &   �,  &   -  %   D-  $   j-  =   �-  '   �-     �-     .     .  �   +.  :   �.     �.     �.     �.     /     /  
   %/     0/     9/     F/  	   M/     W/  $   p/     �/     �/     �/     �/     �/  &   0  <   90     v0     �0     �0      �0  	   �0  2   �0  2   1     A1      I1  #   j1     �1     �1     �1  O   �1     2     12     =2     E2     N2     U2     g2     {2     �2     �2     �2     �2  "   �2     3  D   3  �   Z3  7   4  C   T4  3   �4     �4     �4     �4  f   5  (   �5      �5     �5  6   �5      6     <6     O6     f6     u6     |6     �6     �6     �6     �6     �6  	   �6     �6     �6     7     7     7     $7     47     :7     C7     c7     }7  #   �7  #   �7  +   �7     8     8     8     %8  
   28     =8     S8     e8     �8     �8     �8     �8     �8     �8     �8     �8      9     9  #   79     [9     y9     �9     �9     �9     �9     �9     �9  	   �9      :     :     1:     A:     E:     Y:     l:     t:     �:     �:     �:     �:     �:     �:     �:  	   ;     ;     2;     I;     b;     t;  
   �;      �;  !   �;     �;     �;  
   <     !<  :   5<  $   p<     �<     �<     �<     �<  $   �<     �<     �<     =     !=     7=  +   L=  "   x=     �=  1   �=     �=     >     &>     D>  �   [>     =?     \?      {?      �?     �?     �?     �?     �?  
   @     @     -@     J@     Z@  8   r@     �@     �@     �@     �@     �@     A     A     *A     0A     =A  	   IA  5   SA  %   �A  %   �A      �A     �A     B  $    B     EB     TB     bB     oB     }B  (   �B     �B     �B     �B     �B  �   C     �C  4   �C     �C     �C     �C     �C     D     D     1D  %   OD     uD     �D     �D     �D     �D     �D     E  $   'E     LE     PE     XE  �   pE  �   XF  �   EG     6H     FH     TH  )   jH     �H  (   �H     �H     �H     �H     �H  	   I  	   $I     .I     II     aI     gI     }I     �I     �I  /   �I  "   �I     J  ,   J     LJ     YJ      nJ     �J     �J     �J     �J     �J  
   �J  	   �J     �J     �J     �J     �J     K  �   $K  M   �K     �K     L     M     (M     @M     VM     kM     �M  �   �M     'N     AN     ^N     vN     �N     �N  v   �N     O     .O     3O     LO     dO     lO  
   tO     O     �O     �O  "   �O     �O  !   �O      �O  -   P     =P  �   ZP  "   ,Q     OQ     lQ  �   �Q     R     R  $   $R  
   IR  )   TR     ~R     �R     �R     �R     �R     �R     �R     �R     �R     �R     �R     S     S  H   +S  (   tS     �S     �S     �S     �S     �S     �S     �S     �S     �S     T  	   T     "T     )T  	   7T  
   AT     LT     XT     rT     yT     T  �   �T     U  7   U     QU     bU     qU     �U  !   �U     �U     �U     �U     �U     V     V     /V     MV     ]V  4   lV  $   �V     �V  "   �V     �V     W     W     W     ?W  
   FW     QW  ,   kW  +   �W     �W     �W     �W     X     X     $X     AX     JX     ZX     kX  (   �X     �X     �X     �X     �X     Y     Y     4Y     KY     [Y     xY     �Y     �Y  !   �Y  '   �Y  *   Z  -   -Z     [Z     zZ     �Z     �Z     �Z     �Z     �Z     �Z     �Z      [  -   [  9   @[  @   z[  r   �[     .\  A   E\  �   �\     =]     W]     d]     �]     �]     �]     �]  <   �]     �]  "   ^     $^     (^     =^  *   W^     �^     �^     �^     �^     �^     
_  !   _  N   1_     �_     �_  
   `     `     3`     G`     ``     y`     �`     �`     �`     �`     �`     	a     !a  `   )a  %   �a  ,   �a     �a     �a     �a  ,   b  	   >b     Hb  /   Xb  	   �b     �b     �b  -   �b  1   �b  "   &c  (   Ic     rc  �   �c  �   Ld  }   e  4   �e     �e     �e     �e     �e     f     f     !f  5   :f     pf     yf     f     �f  -   �f     �f     �f     �f     �f     �f     g     g     'g  ,   /g     \g  ~  og  �   �h  �   �i  Q   j     cj     �j  	   �j  &   �j  8   �j  8   �j  +   5k  e   ak  =   �k     l     "l     Al  �   Yl  S   �l     9m     Am     Jm  !   Vm  (   xm     �m     �m     �m     �m     �m     n  '   +n     Sn     fn  "   �n  .   �n     �n     �n  M   o     Yo     so     �o     �o  	   �o  ;   �o  .   �o     )p  ,   0p  "   ]p     �p     �p     �p  �   �p     Fq  	   dq     nq     uq  	   |q     �q  !   �q      �q     �q     r  I   r  =   br  I   �r  	   �r  g   �r  -  \s  z   �t  z   u  t   �u  2   �u  '   (v  )   Pv  �   zv  N   )w  -   xw  (   �w  z   �w  0   Jx      {x  '   �x  $   �x     �x     �x     y  	   y  %   %y     Ky     _y  
   ly     wy     ~y     �y     �y  	   �y     �y  
   �y  	   �y  *   �y  #   z     =z  T   Sz  H   �z  T   �z     F{     `{     i{  	   q{     {{     �{     �{     �{     �{  
   �{      |     |     (|     6|     G|     b|  .   {|  (   �|  <   �|  /   }  (   @}     i}  	   �}     �}     �}     �}     �}     �}     �}     ~     ~     -~     :~     M~     h~      w~     �~     �~  !   �~     �~     �~     �~  (        8  "   E     h  "   {     �  %   �     �  ,   �  +   �  '   ?�     g�  
   }�  
   ��  K   ��     ߀     ��  
   �     �     �  5   ,�  !   b�     ��  *   ��     ́     �  L   	�  *   V�  1   ��  >   ��  2   �  #   %�  3   I�  "   }�    ��  2   ��  /   �  H   �  E   ]�  (   ��     ̅     �     ��     �  '   !�  !   I�     k�     |�  E   ��     �     ��  *   ��  (   )�  "   R�     u�     |�     ��     ��     ��     ��  Q   ��  %   �  &   9�  %   `�  (   ��     ��  +   ��     �     �     �     +�  #   7�  @   [�     ��     ��      ��     ׉  �   ��     ��  5   Ҋ     �  	   �     �     5�     Q�     j�  *   ��  2   ��     �  *   ��     !�  $   ?�  *   d�  )   ��     ��  3   Ќ     �     �     �  �   *�  �   �  �   �     �     %�     1�  9   F�     ��  8   ��     א     �     �  '   �     6�     ?�     R�     r�     ��     ��     ��  .   ב  *   �  A   1�  3   s�  )   ��  E   ђ     �      -�  )   N�     x�     ��      ��     ��      ��     ٓ     �     �  	   ��     �     �  2   �  �   M�  �   �  %   ��  $  ��     ږ     ��     �     0�  *   H�  *   s�  �   ��  (   j�  '   ��     ��     ٘     ��     �  �   �     ə     ޙ     �  (   ��  
   %�     0�     9�     F�     \�     o�  8   v�  	   ��  -   ��  (   �  D   �  $   U�    z�  '   ��  4   ��  &   �  �   �     ��       0   ʝ     ��  I   
�  	   T�     ^�     o�     ~�     ��  !   ��     Ş  	   ͞     מ     �     ��     �     �  �   <�  Q   ş     �     #�     1�     O�  !   _�     ��  	   ��     ��     ��     ɠ     �  
   �     ��     �     �     ,�     ;�  	   U�     _�     m�  �   ��     �  G   %�  !   m�     ��     ��  &   Ţ  3   �      �  "   <�  +   _�     ��     ��     ��     ȣ     �     ��  G   �  7   \�  $   ��  #   ��     ݤ  	   �     �  )   �  
   -�     8�     H�  2   h�  9   ��  1   ե     �     "�     :�     O�  '   g�  
   ��     ��  	   ��  #   ��  4   ֦  %   �  !   1�     S�      g�  &   ��  $   ��  %   ԧ     ��     �     5�  %   R�     x�  /   ��  E   Ĩ  2   
�  7   =�     u�     ��  (   ��  
   ˩  $   ֩     ��  *   �     2�  
   B�     M�  4   ^�  <   ��  C   Ъ  �   �      ��  a   ֫  �   8�  "   �     1�  &   L�     s�     ��  	   ��     ��  @   ��     ��  "   �     1�  1   5�  $   g�  E   ��  (   Ү  )   ��  4   %�  *   Z�  4   ��     ��  2   ǯ  N   ��     I�  �   b�     3�  )   @�  #   j�  '   ��  )   ��  *   �  !   �  0   -�  -   ^�  0   ��  3   ��  "   �     �  �   !�  $   ų  D   �     /�     G�     a�  ?   z�     ��     Ǵ  >   ޴  
   �     (�     G�  $   ^�  (   ��     ��  *   ǵ     �     �  9  1�  �   k�  J   (�  #   s�  "   ��     ��     ƹ     ι     �  ?   �  [   1�     ��     ��     ��     ��  .   Һ     �     �     *�     2�     6�     J�     `�     {�  ?   ��     û         �  �        i  -  �   q       �   �          [  �  �     [       )   �   �  7   �   (   �  �      Q   �   �      �  �           �  +   `   n      �  �      �  �     k   �  m   0      l       �  �  �  �   �          �  <   �  4             �   �   �     �  s  �          p          /               �  p  n   �       j  +      �   �     �  a   C   $   H  �   �           �   x      T      �   �          �   �   =   �  b  �          e  �   �  �  �                  i   �          |           }   V           w  �    �  �               �  r  �   �  �   �  W      �   �  w   |  l  �   �  �  1        d       �   #       �  g     �   �               �  �  �  g            �   �      9       �   �   �   �      t         8   0   �                 X      �   �             �       �               �           )  �  ^   �   "    3   �  �  �   �          %  �  A   M  �  ~  �  e   B  ;   �  �   �  �  4               	      �        �  o   *   �    �           �                    _      m  �     �  ~   b       L  �          �  �  �   �      R            T   �   �   �   \      %   �  �   N       �  A  �      �  @      ]   v     (  Z      O  �   h  �   J   O      �  �      �     �   �      �   U   �   =  r   E  �      {  �     &  .     D   {   z   �   �       >  �   �  G   �     �   �   �      �     E   �  ^  _   �          7  H   �  f       �   �         G  c  u      �   �   �   Z       	  	       ,           f   �   �      Q  �   �      ?   �   q  #  �  C            �       $  3  �   �  ]      �   �   �              �   y   �   '     �  �  �   �           
  �              B          x           �   `                 �  "   �  �  �  �  F    F   �  9  �              *  �      @       �    8  ?  �   M   �  \      �        �     s   �  �  <  D      P  �       V  d    �       &       �  S    z  �      �      �       �     �      �   t   W              o  P   �  �   �   ;      }    �  �   �       K  �   J  �   u   �       �   I  �  �         �       :   ,  -           �      L   �              �  5  �       R   �   /  �   �  y  �      �  �          Y  1   �       Y   �      �       �   �  �       �  �   �   k  S   �  �    �   �  �               �       �     K       �  �  �  �   �       !      �   �  �   �       �  �   v      6   >       �      h           
   �  �  �  �  �   5               U                    �   6  2     N  a  .         �   �    �  '       I   2        X  �       �   �                 !      �   :      �       �  j       c    

A good replacement for %s
is placing values for the user.name and
user.email settings into your personal
~/.gitconfig file.
 
* Untracked file clipped here by %s.
* To see the entire file, use an external editor.
 
This is due to a known issue with the
Tcl binary distributed by Cygwin. %s ... %*i of %*i %s (%3i%%) %s Repository %s of %s '%s' is not an acceptable branch name. '%s' is not an acceptable remote name. (Blue denotes repository-local tools) * Binary file (not showing content). * Untracked file is %d bytes.
* Showing only first %d bytes.
 A branch is required for 'Merged Into'. Abort Merge... Abort completed.  Ready. Abort failed. Abort merge?

Aborting the current merge will cause *ALL* uncommitted changes to be lost.

Continue with aborting the current merge? Aborted checkout of '%s' (file level merging is required). Aborting About %s Add Add New Remote Add New Tool Command Add Remote Add Tool Add globally Add... Adding %s Adding resolution for %s Always (Do not perform merge checks) Amend Last Commit Amended Commit Message: Amended Initial Commit Message: Amended Merge Commit Message: Annotation complete. Annotation process is already running. Any unstaged changes will be permanently lost by the revert. Apply/Reverse Hunk Apply/Reverse Line Arbitrary Location: Are you sure you want to run %s? Arguments Ask the user for additional arguments (sets $ARGS) Ask the user to select a revision (sets $REVISION) Author: Blame Copy Only On Changed Files Blame History Context Radius (days) Blame Parent Commit Branch Branch '%s' already exists. Branch '%s' already exists.

It cannot fast-forward to %s.
A merge is required. Branch '%s' does not exist. Branch Name Branch: Branches Browse Browse %s's Files Browse Branch Files Browse Branch Files... Browse Current Branch's Files Busy Calling commit-msg hook... Calling pre-commit hook... Calling prepare-commit-msg hook... Cancel Cannot abort while amending.

You must finish amending this commit.
 Cannot amend while merging.

You are currently in the middle of a merge that has not been fully completed.  You cannot amend the prior commit unless you first abort the current merge activity.
 Cannot determine HEAD.  See console output for details. Cannot fetch branches and objects.  See console output for details. Cannot fetch tags.  See console output for details. Cannot find HEAD commit: Cannot find git in PATH. Cannot find parent commit: Cannot merge while amending.

You must finish amending this commit before starting any type of merge.
 Cannot move to top of working directory: Cannot parse Git version string: Cannot resolve %s as a commit. Cannot resolve deletion or link conflicts using a tool Cannot use bare repository: Cannot write icon: Cannot write shortcut: Case-Sensitive Change Change Font Checked out '%s'. Checkout Checkout After Creation Checkout Branch Checkout... Choose %s Clone Clone Existing Repository Clone Type: Clone failed. Clone... Cloning from %s Close Command: Commit %s appears to be corrupt Commit Message Text Width Commit Message: Commit declined by commit-msg hook. Commit declined by pre-commit hook. Commit declined by prepare-commit-msg hook. Commit failed. Commit: Commit@@noun Commit@@verb Committer: Committing changes... Compress Database Compressing the object database Conflict file does not exist Continue Copied Or Moved Here By: Copy Copy All Copy Commit Copy To Clipboard Copying objects Could not add tool:
%s Could not start ssh-keygen:

%s Could not start the merge tool:

%s Couldn't find git gui in PATH Couldn't find gitk in PATH Counting objects Create Create Branch Create Desktop Icon Create New Branch Create New Repository Create... Created commit %s: %s Creating working directory Current Branch: Cut Database Statistics Decrease Font Size Default Default File Contents Encoding Delete Delete Branch Delete Branch Remotely Delete Branch... Delete Local Branch Delete Only If Delete Only If Merged Into Delete... Deleting branches from %s Destination Repository Detach From Local Branch Diff/Console Font Directory %s already exists. Directory: Disk space used by loose objects Disk space used by packed objects Displaying only %s of %s files. Do Full Copy Detection Do Nothing Do Nothing Else Now Do not know how to initialize repository at location '%s'. Don't show the command output window Done Edit Email Address Encoding Error loading commit data for amend: Error loading diff: Error loading file: Error retrieving versions:
%s Error: Command Failed Explore Working Copy Failed to add remote '%s' of location '%s'. Failed to completely save options: Failed to configure origin Failed to configure simplified git-pull for '%s'. Failed to create repository %s: Failed to delete branches:
%s Failed to open repository %s: Failed to rename '%s'. Failed to set current branch.

This working directory is only partially switched.  We successfully updated your files, but failed to update an internal Git file.

This should not have occurred.  %s will now close and give up. Failed to stage selected hunk. Failed to stage selected line. Failed to unstage selected hunk. Failed to unstage selected line. Failed to update '%s'. Fast Forward Only Fetch Immediately Fetch Tracking Branch Fetch from Fetching %s from %s Fetching new changes from %s Fetching the %s File %s already exists. File %s seems to have unresolved conflicts, still stage? File Browser File Viewer File level merge required. File type changed, not staged File type changed, staged File: Find Text... Find: Font Example Font Family Font Size Force overwrite existing branch (may discard changes) Force resolution to the base version? Force resolution to the other branch? Force resolution to this branch? Found a public key in: %s From Repository Full Copy (Slower, Redundant Backup) Further Action Garbage files Generate Key Generating... Generation failed. Generation succeeded, but no keys found. Git Gui Git Repository Git Repository (subproject) Git directory not found: Git version cannot be determined.

%s claims it is version '%s'.

%s requires at least Git 1.5.0 or later.

Assume '%s' is version 1.5.0?
 Global (All Repositories) Hardlinks are unavailable.  Falling back to copying. Help In File: Include tags Increase Font Size Index Error Initial Commit Message: Initial file checkout failed. Initialize Remote Repository and Push Initializing... Invalid GIT_COMMITTER_IDENT: Invalid date from Git: %s Invalid font specified in %s: Invalid global encoding '%s' Invalid repo encoding '%s' Invalid revision: %s Invalid spell checking configuration KiB LOCAL:
 LOCAL: deleted
REMOTE:
 Last scanned state does not match repository state.

Another Git program has modified this repository since the last scan.  A rescan must be performed before a merge can be performed.

The rescan will be automatically started now.
 Last scanned state does not match repository state.

Another Git program has modified this repository since the last scan.  A rescan must be performed before another commit can be created.

The rescan will be automatically started now.
 Last scanned state does not match repository state.

Another Git program has modified this repository since the last scan.  A rescan must be performed before the current branch can be changed.

The rescan will be automatically started now.
 Linking objects Loading %s... Loading annotation... Loading copy/move tracking annotations... Loading diff of %s... Loading original location annotations... Local Branch Local Branches Local Merge... Location %s already exists. Location: Main Font Match Tracking Branch Name Match Tracking Branches Merge Merge Commit Message: Merge Into %s Merge Verbosity Merge completed successfully. Merge failed.  Conflict resolution is required. Merge strategy '%s' not supported. Merge tool failed. Merge tool is already running, terminate it? Merged Into: Merging %s and %s... Minimum Letters To Blame Copy On Mirroring to %s Missing Modified, not staged Name: New Branch Name Template New Commit New Name: New... Next No No Suggestions No changes to commit. No changes to commit.

No files were modified by this commit and it was not a merge commit.

A rescan will be automatically started now.
 No changes to commit.

You must stage at least 1 file before you can commit.
 No default branch obtained. No differences detected.

%s has no changes.

The modification date of this file was updated by another application, but the content within the file was not changed.

A rescan will be automatically started to find other files which may have the same state. No keys found. No repository selected. No revision selected. No working directory Not a GUI merge tool: '%s' Not a Git repository: %s Note that the diff shows only conflicting changes.

%s will be overwritten.

This operation can be undone only by restarting the merge. Nothing to clone from %s. Number of Diff Context Lines Number of loose objects Number of packed objects Number of packs OK One or more of the merge tests failed because you have not fetched the necessary commits.  Try fetching from %s first. Online Documentation Open Open Existing Repository Open Recent Repository: Open... Options Options... Original File: Originally By: Other Packed objects waiting for pruning Paste Please select a branch to rename. Please select a tracking branch. Please select one or more branches to delete. Please supply a branch name. Please supply a commit message.

A good commit message has the following format:

- First line: Describe in one sentence what you did.
- Second line: Blank
- Remaining lines: Describe why this change is good.
 Please supply a name for the tool. Please supply a remote name. Portions staged for commit Possible environment issues exist.

The following environment variables are probably
going to be ignored by any Git subprocess run
by %s:

 Preferences Prev Prune Tracking Branches During Fetch Prune from Pruning tracking branches deleted from %s Push Push Branches Push to Push... Pushing %s %s to %s Pushing changes to %s Quit REMOTE:
 REMOTE: deleted
LOCAL:
 Reading %s... Ready to commit. Ready. Recent Repositories Recovering deleted branches is difficult.

Delete the selected branches? Recovering lost commits may not be easy. Redo Refresh Refreshing file status... Remote Remote Details Remote: Remove Remove Remote Remove Tool Remove Tool Commands Remove... Rename Rename Branch Rename... Repository Repository: Requires merge resolution Rescan Reset Reset '%s'? Reset changes?

Resetting the changes will cause *ALL* uncommitted changes to be lost.

Continue with resetting the current changes? Reset... Resetting '%s' to '%s' will lose the following commits: Restore Defaults Revert Changes Revert To Base Revert changes in file %s? Revert changes in these %i files? Reverting %s Reverting dictionary to %s. Reverting selected files Revision Revision Expression: Revision To Merge Revision expression is empty. Run Command: %s Run Merge Tool Run only if a diff is selected ($FILENAME not empty) Running %s requires a selected file. Running merge tool... Running thorough copy detection... Running: %s Save Scanning %s... Scanning for modified files ... Select Select All Setting up the %s (at %s) Shared (Fastest, Not Recommended, No Backup) Shared only available for local repository. Show Diffstat After Merge Show History Context Show Less Context Show More Context Show SSH Key Show a dialog before running Sign Off Source Branches Source Location: Spell Checker Failed Spell checker silently failed on startup Spell checking is unavailable Spelling Dictionary: Stage Changed Stage Changed Files To Commit Stage Hunk For Commit Stage Line For Commit Stage Lines For Commit Stage To Commit Staged Changes (Will Commit) Staged for commit Staged for commit, missing Staged for removal Staged for removal, still present Staging area (index) is already locked. Standard (Fast, Semi-Redundant, Hardlinks) Standard only available for local repository. Start git gui In The Submodule Starting Revision Starting gitk... please wait... Starting... Staying on branch '%s'. Success Summarize Merge Commits System (%s) Tag Target Directory: The 'master' branch has not been initialized. The following branches are not completely merged into %s: The following branches are not completely merged into %s:

 - %s There is nothing to amend.

You are about to create the initial commit.  There is no commit before this to amend.
 This Detached Checkout This is example text.
If you like this text, it can be your font. This repository currently has approximately %i loose objects.

To maintain optimal performance it is strongly recommended that you compress the database.

Compress the database now? Tool '%s' already exists. Tool Details Tool completed successfully: %s Tool failed: %s Tool: %s Tools Tracking Branch Tracking branch %s is not a branch in the remote repository. Transfer Options Trust File Modification Timestamps URL Unable to cleanup %s Unable to copy object: %s Unable to copy objects/info/alternates: %s Unable to display %s Unable to display parent Unable to hardlink object: %s Unable to obtain your identity: Unable to unlock the index. Undo Unexpected EOF from spell checker Unknown file state %s detected.

File %s cannot be committed by this program.
 Unlock Index Unmerged files cannot be committed.

File %s has merge conflicts.  You must resolve them and stage the file before committing.
 Unmodified Unrecognized spell checker Unstage From Commit Unstage Hunk From Commit Unstage Line From Commit Unstage Lines From Commit Unstaged Changes Unstaging %s from commit Unsupported merge tool '%s' Unsupported spell checker Untracked, not staged Update Existing Branch: Updated Updating the Git index failed.  A rescan will be automatically started to resynchronize git-gui. Updating working directory to '%s'... Use '/' separators to create a submenu tree: Use Local Version Use Merge Tool Use Remote Version Use thin pack (for slow network connections) User Name Verify Database Verifying the object database with fsck-objects Visualize Visualize %s's History Visualize All Branch History Visualize All Branch History In The Submodule Visualize Current Branch History In The Submodule Visualize Current Branch's History Visualize These Changes In The Submodule Working... please wait... You are in the middle of a change.

File %s is modified.

You should complete the current commit before starting a merge.  Doing so will help you abort a failed merge, should the need arise.
 You are in the middle of a conflicted merge.

File %s has merge conflicts.

You must resolve them, stage the file, and commit to complete the current merge.  Only then can you begin another merge.
 You are no longer on a local branch.

If you wanted to be on a branch, create one now starting from 'This Detached Checkout'. You must correct the above errors before committing. Your OpenSSH Public Key Your key is in: %s [Up To Parent] buckets commit-tree failed: error fatal: Cannot resolve %s fatal: cannot stat path %s: No such file or directory fetch %s files files checked out files reset git-gui - a graphical user interface for Git. git-gui: fatal error lines annotated objects pt. push %s remote prune %s update-ref failed: warning warning: Tcl does not support encoding '%s'. write-tree failed: Project-Id-Version: git-gui
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-01-26 15:47-0800
PO-Revision-Date: 2015-07-25 14:23+0000
Last-Translator: Tobias Bannert <tobannert@gmail.com>
Language-Team: German
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:28+0000
X-Generator: Launchpad (build 18115)
 

Um den Namen »%s« zu ändern, sollten Sie die 
gewünschten Werte für die Einstellung user.name und 
user.email in Ihre Datei ~/.gitconfig einfügen.
 
* Datei nicht unter Versionskontrolle, hier abgeschnitten durch %s.
* Zum Ansehen der vollständigen Datei externen Editor benutzen.
 
Dies ist ein bekanntes Problem der Tcl-Version, die
in Cygwin mitgeliefert wird. %s ... %*i von %*i %s (%3i%%) Projektarchiv %s %s von %s »%s« ist kein zulässiger Zweigname. »%s« ist kein zulässiger Name eines externen Archivs. (Werkzeuge für lokales Archiv werden in Blau angezeigt) * Binärdatei (Inhalt wird nicht angezeigt) * Datei nicht unter Versionskontrolle, Dateigröße %d Bytes.
* Nur erste %d Bytes werden angezeigt.
 Für »Zusammenführen mit« muss ein Zweig angegeben werden. Zusammenführen abbrechen... Abbruch durchgeführt. Bereit. Abbruch fehlgeschlagen. Zusammenführen abbrechen?

Wenn Sie abbrechen, gehen alle noch nicht eingetragenen Änderungen verloren.

Zusammenführen jetzt abbrechen? Auf Zweig »%s« umstellen abgebrochen (Zusammenführen der Dateien ist notwendig). Abbruch Über %s Hinzufügen Neues externes Archiv hinzufügen Neues Kommando für Werkzeug hinzufügen Externes Archiv hinzufügen Werkzeug hinzufügen Global hinzufügen Hinzufügen … »%s« hinzufügen... Auflösung hinzugefügt für %s Immer (Keine Zusammenführungsprüfung) Letzte nachbessern Nachgebesserte Beschreibung: Nachgebesserte erste Beschreibung: Nachgebesserte Zusammenführungs-Beschreibung: Annotierung vollständig. Annotierung läuft bereits. Alle nicht bereitgestellten Änderungen werden beim Verwerfen verloren gehen. Kontext anwenden/umkehren Zeile anwenden/umkehren Adresse: Wollen Sie %s wirklich starten? Argumente Benutzer nach zusätzlichen Argumenten fragen (setzt $ARGS) Benutzer nach Version fragen (setzt $REVISION) Autor: Kopie-Annotieren nur bei geänderten Dateien Anzahl Tage für Historien-Kontext Elternversion annotieren Zweig Zweig »%s« existiert bereits. Zweig »%s« existiert bereits.

Zweig kann nicht mit »%s« schnellzusammengeführt werden. Reguläres Zusammenführen ist notwendig. Zweig »%s« existiert nicht. Zweigname Zweig: Zweige Blättern Zweig »%s« durchblättern Dateien des Zweigs durchblättern Dateien im Zweig durchsuchen … Aktuellen Zweig durchblättern Verarbeitung läuft Aufrufen der Versionsbeschreibungs-Kontrolle (»commit-message hook«)... Aufrufen der Vor-Eintragen-Kontrolle (»pre-commit hook«)... Aufrufen der Eintragen-Vorbereiten-Kontrolle (»prepare-commit hook«)... Abbrechen Abbruch der Nachbesserung ist nicht möglich.

Sie müssen die Nachbesserung der Version abschließen.
 Nachbesserung währen Zusammenführung nicht möglich.

Sie haben das Zusammenführen von Versionen angefangen, aber noch nicht beendet. Sie können keine vorige Übertragung nachbessern, solange eine unfertige Zusammenführung existiert. Dazu müssen Sie die Zusammenführung beenden oder abbrechen.
 Die Zweigspitze (HEAD) konnte nicht gefunden werden.  Kontrollieren Sie die Ausgaben auf der Konsole für weitere Angaben. Zweige und Objekte konnten nicht angefordert werden.  Kontrollieren Sie die Ausgaben auf der Konsole für weitere Angaben. Markierungen konnten nicht angefordert werden.  Kontrollieren Sie die Ausgaben auf der Konsole für weitere Angaben. Zweigspitze (»HEAD«) kann nicht gefunden werden: Git kann im PATH nicht gefunden werden. Elternversion kann nicht gefunden werden: Zusammenführen kann nicht gleichzeitig mit Nachbessern durchgeführt werden.

Sie müssen zuerst die Nachbesserungs-Version abschließen, bevor Sie zusammenführen können.
 Es konnte nicht in das oberste Verzeichnis der Arbeitskopie gewechselt werden: Git Versionsangabe kann nicht erkannt werden: »%s« wurde nicht als Version gefunden. Konflikte durch gelöschte Dateien oder symbolische Links können nicht durch das Zusamenführungswerkzeug gelöst werden. Bloßes Projektarchiv kann nicht benutzt werden: Fehler beim Erstellen des Icons: Fehler beim Schreiben der Verknüpfung: Groß-/Kleinschreibung unterscheiden Ändern Schriftart ändern Umgestellt auf »%s«. Umstellen Arbeitskopie umstellen nach Erstellen Auf Zweig umstellen Umstellen... %s wählen Klonen Projektarchiv klonen Art des Klonens: Klonen fehlgeschlagen. Klonen... Kopieren von »%s« Schließen Kommando: Version »%s« scheint beschädigt zu sein Textbreite der Versionsbeschreibung Versionsbeschreibung: Eintragen abgelehnt durch Versionsbeschreibungs-Kontrolle (»commit-message hook«). Eintragen abgelehnt durch Vor-Eintragen-Kontrolle (»pre-commit hook«). Eintragen abgelehnt durch Eintragen-Vorbereiten-Kontrolle (»prepare-commit hook«). Eintragen fehlgeschlagen. Version: Version Eintragen Eintragender: Änderungen eintragen... Datenbank komprimieren Objektdatenbank komprimieren Konflikt-Datei existiert nicht Fortsetzen Kopiert oder verschoben durch: Kopieren Alle kopieren Version kopieren In Zwischenablage kopieren Objektdatenbank kopieren Werkzeug konnte nicht hinzugefügt werden:

%s Konnte »ssh-keygen« nicht starten:

%s Zusammenführungswerkzeug konnte nicht gestartet werden:

%s »Git gui« kann im PATH nicht gefunden werden. Gitk kann im PATH nicht gefunden werden. Objekte werden gezählt Erstellen Zweig erstellen Desktop-Icon erstellen Neuen Zweig erstellen Neues Projektarchiv Erstellen … Version %s übertragen: %s Arbeitskopie erstellen Aktueller Zweig: Ausschneiden Datenbankstatistik Schriftgröße verkleinern Voreinstellung Voreingestellte Zeichenkodierung Löschen Zweig löschen Zweig in externem Archiv löschen Zweig löschen … Lokalen Zweig löschen Nur löschen, wenn Nur löschen, wenn zusammengeführt nach Löschen … Zweige auf »%s« werden gelöscht Ziel-Projektarchiv Verbindung zu lokalem Zweig lösen Vergleich-Schriftart Verzeichnis »%s« existiert bereits. Verzeichnis: Festplattenplatz von unverknüpften Objekten Festplattenplatz von komprimierten Objekten Nur %s von %s Dateien werden angezeigt. Volle Kopie-Erkennung Nichts tun Nichts tun Initialisieren eines externen Archivs an Adresse »%s« ist nicht möglich. Kein Ausgabefenster zeigen Fertig Bearbeiten E-Mail-Adresse Zeichenkodierung Fehler beim Laden der Versionsdaten für Nachbessern: Fehler beim Laden des Vergleichs: Fehler beim Laden der Datei: Fehler beim Abrufen der Dateiversionen:
%s Fehler: Kommando fehlgeschlagen Arbeitskopie im Dateimanager Fehler beim Hinzufügen des externen Archivs »%s« aus Herkunftsort »%s«. Optionen konnten nicht gespeichert werden: Der Ursprungsort konnte nicht eingerichtet werden Fehler beim Einrichten der vereinfachten git-pull für »%s«. Projektarchiv »%s« konnte nicht erstellt werden: Fehler beim Löschen der Zweige:
%s Projektarchiv »%s« konnte nicht geöffnet werden. Fehler beim Umbenennen von »%s«. Lokaler Zweig kann nicht gesetzt werden.

Diese Arbeitskopie ist nur teilweise umgestellt. Die Dateien sind korrekt aktualisiert, aber einige interne Git-Dateien konnten nicht geändert werden.

Dies ist ein interner Programmfehler von %s. Programm wird jetzt abgebrochen. Fehler beim Bereitstellen des gewählten Kontexts. Fehler beim Bereitstellen der gewählten Zeile. Fehler beim Herausnehmen des gewählten Kontexts aus der Bereitstellung. Fehler beim Herausnehmen der gewählten Zeile aus der Bereitstellung. Aktualisieren von »%s« fehlgeschlagen. Nur Schnellzusammenführung Gleich anfordern Übernahmezweig anfordern Anfordern von Änderungen »%s« von »%s« anfordern Neue Änderungen von »%s« holen »%s« anfordern Datei »%s« existiert bereits. Datei »%s« hat nicht aufgelöste Konflikte. Trotzdem bereitstellen? Datei-Browser Datei-Browser Zusammenführen der Dateien ist notwendig. Dateityp geändert, nicht bereitgestellt Dateityp geändert, bereitgestellt Datei: Text suchen... Suchen: Schriftbeispiel Schriftfamilie Schriftgröße Überschreiben von existierenden Zweigen erzwingen (könnte Änderungen löschen) Konflikt durch Basisversion ersetzen? Konflikt durch anderen Zweig ersetzen? Konflikt durch diesen Zweig ersetzen? Öffentlicher Schlüssel gefunden in: %s In Projektarchiv Alles kopieren (langsamer, volle Redundanz) Weitere Aktion jetzt Dateien im Mülleimer Schlüssel erzeugen Erzeugen... Schlüsselerzeugung fehlgeschlagen. Schlüsselerzeugung erfolgreich, aber keine Schlüssel gefunden. Git Gui Git Projektarchiv Git-Projektarchiv (Unterprojekt) Git-Verzeichnis nicht gefunden: Die Version von Git kann nicht bestimmt werden.

»%s« behauptet, es sei Version »%s«.

%s benötigt mindestens Git 1.5.0 oder höher.

Soll angenommen werden, »%s« sei Version 1.5.0?
 Global (Alle Projektarchive) Hardlinks nicht verfügbar. Stattdessen wird kopiert. Hilfe In Datei: Mit Markierungen übertragen Schriftgröße vergrößern Fehler in Bereitstellung Erste Versionsbeschreibung: Erstellen der Arbeitskopie fehlgeschlagen. Externes Archiv initialisieren und dahin versenden Initialisieren... Ungültiger Wert von GIT_COMMITTER_INDENT: Ungültiges Datum von Git: %s Ungültige Zeichensatz-Angabe in %s: Ungültige globale Zeichenkodierung »%s« Ungültige Archiv-Zeichenkodierung »%s« Ungültige Version: %s Unbenutzbare Konfiguration der Rechtschreibprüfung KB LOKAL:
 LOKAL: gelöscht
ANDERES:
 Der letzte geladene Status stimmt nicht mehr mit dem Projektarchiv überein.

Ein anderes Git-Programm hat das Projektarchiv seit dem letzten Laden geändert.  Vor einem Zusammenführen muss neu geladen werden.

Es wird gleich neu geladen.
 Der letzte geladene Status stimmt nicht mehr mit dem Projektarchiv überein.

Ein anderes Git-Programm hat das Projektarchiv seit dem letzten Laden geändert.  Vor dem Eintragen einer neuen Version muss neu geladen werden.

Es wird gleich neu geladen.
 Der letzte geladene Status stimmt nicht mehr mit dem Projektarchiv überein.

Ein anderes Git-Programm hat das Projektarchiv seit dem letzten Laden geändert.  Vor dem Wechseln des lokalen Zweigs muss neu geladen werden.

Es wird gleich neu geladen.
 Objekte verlinken %s laden... Annotierung laden... Annotierungen für Kopieren/Verschieben werden geladen... Vergleich von »%s« laden... Annotierungen für ursprünglichen Ort werden geladen... Lokaler Zweig Lokale Zweige Lokales Zusammenführen... Projektarchiv »%s« existiert bereits. Adresse: Programmschriftart Passend zu Übernahmezweig-Name Passend zu Übernahmezweig Zusammenführen Zusammenführungs-Beschreibung: Zusammenführen in »%s« Ausführlichkeit der Zusammenführen-Meldungen Zusammenführen erfolgreich abgeschlossen. Zusammenführen fehlgeschlagen. Konfliktauflösung ist notwendig. Zusammenführungsmethode »%s« nicht unterstützt. Zusammenführungswerkzeug fehlgeschlagen. Zusammenführungswerkzeug läuft bereits. Soll es abgebrochen werden? Zusammengeführt mit: Zusammenführen von %s und %s... Mindestzahl Zeichen für Kopie-Annotieren Spiegeln nach %s Fehlend Verändert, nicht bereitgestellt Name: Namensvorschlag für neue Zweige Neue Version Neuer Name: Neu... Nächster Nein Keine Vorschläge Keine Änderungen, die eingetragen werden können. Keine Änderungen einzutragen.

Es gibt keine geänderte Datei bei dieser Version und es wurde auch nichts zusammengeführt.

Das Arbeitsverzeichnis wird daher jetzt neu geladen.
 Keine Änderungen vorhanden, die eingetragen werden könnten.

Sie müssen mindestens eine Datei bereitstellen, bevor Sie eintragen können.
 Kein voreingestellter Zweig gefunden. Keine Änderungen feststellbar.

»%s« enthält keine Änderungen. Zwar wurde das Änderungsdatum dieser Datei von einem anderen Programm modifiziert, aber der Inhalt der Datei ist unverändert.

Das Arbeitsverzeichnis wird jetzt neu geladen, um diese Änderung bei allen Dateien zu prüfen. Keine Schlüssel gefunden. Kein Projektarchiv ausgewählt. Keine Version ausgewählt. Kein Arbeitsverzeichnis Kein GUI Zusammenführungswerkzeug: »%s« Kein Git-Projektarchiv in »%s« gefunden. Hinweis: Der Vergleich zeigt nur konfliktverursachende Änderungen an.

»%s« wird überschrieben.

Diese Operation kann nur rückgängig gemacht werden, wenn die
Zusammenführung erneut gestartet wird. Von »%s« konnte nichts geklont werden. Anzahl der Kontextzeilen beim Vergleich Anzahl unverknüpfter Objekte Anzahl komprimierter Objekte Anzahl Komprimierungseinheiten Ok Ein oder mehrere Zusammenführungen sind fehlgeschlagen, da Sie nicht die notwendigen Versionen vorher angefordert haben.  Sie sollten versuchen, zuerst von »%s« anzufordern. Online-Dokumentation Öffnen Projektarchiv öffnen Zuletzt benutztes Projektarchiv öffnen: Öffnen... Optionen Optionen … Ursprüngliche Datei: Ursprünglich von: Andere Komprimierte Objekte, die zum Aufräumen vorgesehen sind Einfügen Bitte wählen Sie einen Zweig zum umbenennen. Bitte wählen Sie einen Übernahmezweig. Bitte wählen Sie mindestens einen Zweig, der gelöscht werden soll. Bitte geben Sie einen Zweignamen an. Bitte geben Sie eine Versionsbeschreibung ein.

Eine gute Versionsbeschreibung enthält folgende Abschnitte:

- Erste Zeile: Eine Zusammenfassung, was man gemacht hat.

- Zweite Zeile: Leerzeile

- Rest: Eine ausführliche Beschreibung, warum diese Änderung hilfreich ist.
 Bitte geben Sie einen Werkzeugnamen an. Bitte geben Sie einen Namen des externen Archivs an. Teilweise bereitgestellt zum Eintragen Möglicherweise gibt es Probleme mit manchen Umgebungsvariablen.

Die folgenden Umgebungsvariablen können vermutlich nicht 
von %s an Git weitergegeben werden:

 Einstellungen Voriger Übernahmezweige aufräumen während Anforderung Aufräumen von Übernahmezweige aufräumen und entfernen, die in »%s« gelöscht wurden Versenden Zweige versenden Versenden nach Versenden... %s %s nach %s versenden Änderungen nach »%s« versenden Beenden ANDERES:
 ANDERES: gelöscht
LOKAL:
 %s lesen... Bereit zum Eintragen. Bereit. Zuletzt benutzte Projektarchive Das Wiederherstellen von gelöschten Zweigen ist nur mit größerem Aufwand möglich.

Sollen die ausgewählten Zweige gelöscht werden? Verworfene Versionen können nur mit größerem Aufwand wiederhergestellt werden. Wiederholen Aktualisieren Dateistatus aktualisieren … Externe Archive Einzelheiten des externen Archivs Externes Archiv: Entfernen Externes Archiv entfernen Werkzeug entfernen Werkzeugkommandos entfernen Entfernen … Umbenennen Zweig umbenennen Umbenennen … Projektarchiv Projektarchiv: Konfliktauflösung nötig Neu laden Zurücksetzen »%s« zurücksetzen? Änderungen zurücksetzen?

Wenn Sie zurücksetzen, gehen alle noch nicht eingetragenen Änderungen verloren.

Änderungen jetzt zurücksetzen? Zurücksetzen … Zurücksetzen von »%s« nach »%s« wird folgende Versionen verwerfen: Voreinstellungen wiederherstellen Änderungen verwerfen Ursprüngliche Version benutzen Änderungen in Datei »%s« verwerfen? Änderungen in den gewählten %i Dateien verwerfen? Änderungen in %s verwerfen Wörterbuch auf %s zurückgesetzt. Änderungen in gewählten Dateien verwerfen Version Version Regexp-Ausdruck: Zusammenzuführende Version Versions-Ausdruck ist leer. Kommando aufrufen: %s Zusammenführungswerkzeug Nur starten, wenn ein Vergleich gewählt ist ($FILENAME ist nicht leer) Um »%s« zu starten, muss eine Datei ausgewählt sein. Zusammenführungswerkzeug starten... Intensive Kopie-Erkennung läuft... Starten: %s Speichern »%s« laden... Nach geänderten Dateien wird gesucht … Auswählen Alle auswählen Einrichten von »%s« an »%s« Verknüpft (schnell, nicht empfohlen, kein Backup) Verknüpft ist nur für lokale Projektarchive verfügbar. Vergleichsstatistik nach Zusammenführen anzeigen Historien-Kontext anzeigen Weniger Zeilen anzeigen Mehr Zeilen anzeigen SSH-Schlüssel anzeigen Bestätigungsfrage vor Starten anzeigen Abzeichnen Lokale Zweige Herkunft: Rechtschreibprüfung fehlgeschlagen Rechtschreibprüfungsprogramm mit Fehler abgebrochen Rechtschreibprüfung nicht verfügbar Wörterbuch Rechtschreibprüfung: Alles bereitstellen Geänderte Dateien bereitstellen Kontext zur Bereitstellung hinzufügen Zeile zur Bereitstellung hinzufügen Zeilen zur Bereitstellung hinzufügen Zum Eintragen bereitstellen Bereitstellung (zum Eintragen) Bereitgestellt zum Eintragen Bereitgestellt zum Eintragen, fehlend Bereitgestellt zum Löschen Bereitgestellt zum Löschen, trotzdem vorhanden Bereitstellung (»index«) ist zur Bearbeitung gesperrt (»locked«). Standard (schnell, teilweise redundant, Hardlinks) Standard ist nur für lokale Projektarchive verfügbar. Git gui im Untermodul starten Anfangsversion Gitk wird gestartet … bitte warten … Starten... Es wird auf Zweig »%s« verblieben. Erfolgreich Zusammenführungs-Versionen zusammenfassen Systemweit (%s) Markierung Zielverzeichnis: Der »master«-Zweig wurde noch nicht initialisiert. Folgende Zweige sind noch nicht mit »%s« zusammengeführt: Folgende Zweige sind noch nicht mit »%s« zusammengeführt:

 - %s Keine Version zur Nachbesserung vorhanden.

Sie sind dabei, die erste Version zu übertragen. Es gibt keine existierende Version, die Sie nachbessern könnten.
 Abgetrennte Arbeitskopie-Version Dies ist ein Beispieltext.
Wenn Ihnen dieser Text gefällt, sollten Sie diese Schriftart wählen. Dieses Projektarchiv enthält ungefähr %i nicht verknüpfte Objekte.

Für eine optimale Performance wird empfohlen, die Datenbank des Projektarchivs zu komprimieren.

Soll die Datenbank jetzt komprimiert werden? Werkzeug »%s« existiert bereits. Einzelheiten des Werkzeugs Werkzeug erfolgreich abgeschlossen: %s Werkzeug fehlgeschlagen: %s Werkzeug: %s Werkzeuge Übernahmezweig Übernahmezweig »%s« ist kein Zweig im externen Projektarchiv. Netzwerk-Einstellungen Auf Dateiänderungsdatum verlassen URL Verzeichnis »%s« kann nicht aufgeräumt werden. Objekt kann nicht kopiert werden: %s Kopien von Objekten/Info/Alternates konnten nicht erstellt werden: %s Datei »%s« kann nicht angezeigt werden Elternversion kann nicht angezeigt werden Für Objekt konnte kein Hardlink erstellt werden: %s Benutzername konnte nicht bestimmt werden: Bereitstellung kann nicht wieder freigegeben werden. Rückgängig Unerwartetes EOF vom Rechtschreibprüfungsprogramm Unbekannter Dateizustand »%s«.

Datei »%s« kann nicht eingetragen werden.
 Bereitstellung freigeben Nicht zusammengeführte Dateien können nicht eingetragen werden.

Die Datei »%s« hat noch nicht aufgelöste Zusammenführungs-Konflikte. Sie müssen diese Konflikte auflösen, bevor Sie eintragen können.
 Unverändert Unbekanntes Rechtschreibprüfungsprogramm Aus der Bereitstellung herausnehmen Kontext aus Bereitstellung herausnehmen Zeile aus der Bereitstellung herausnehmen Zeilen aus der Bereitstellung herausnehmen Nicht bereitgestellte Änderungen Datei »%s« aus der Bereitstellung herausnehmen Unbekanntes Zusammenführungswerkzeug: »%s« Rechtschreibprüfungsprogramm nicht unterstützt Nicht unter Versionskontrolle, nicht bereitgestellt Existierenden Zweig aktualisieren: Aktualisiert Das Aktualisieren der Git-Bereitstellung ist fehlgeschlagen. Eine allgemeine Git-Aktualisierung wird jetzt gestartet, um git-gui wieder mit git zu synchronisieren. Arbeitskopie umstellen auf »%s«... Benutzen Sie einen Schrägstrich »/«, um Untermenüs zu erstellen: Lokale Version benutzen Zusammenführungswerkzeug Externe Version benutzen Kompaktes Datenformat benutzen (für langsame Netzverbindungen) Benutzername Datenbank überprüfen Die Objektdatenbank durch »fsck-objects« überprüfen lassen Darstellen Historie von »%s« darstellen Alle Zweige darstellen Alle Zweige im Untermodul darstellen Aktuellen Zweig im Untermodul darstellen Aktuellen Zweig darstellen Diese Änderungen im Untermodul darstellen Verarbeitung. Bitte warten... Es liegen Änderungen vor.

Die Datei »%s« wurde geändert.  Sie sollten zuerst die bereitgestellte Version abschließen, bevor Sie eine Zusammenführung beginnen.  Mit dieser Reihenfolge können Sie mögliche Konflikte beim Zusammenführen wesentlich einfacher beheben oder abbrechen.
 Zusammenführung mit Konflikten.

Die Datei »%s« enthält Konflikte beim Zusammenführen. Sie müssen diese Konflikte per Hand auflösen. Anschließend müssen Sie die Datei wieder bereitstellen und eintragen, um die Zusammenführung abzuschließen. Erst danach kann eine neue Zusammenführung begonnen werden.
 Die Arbeitskopie ist nicht auf einem lokalen Zweig.

Wenn Sie auf einem Zweig arbeiten möchten, erstellen Sie bitte jetzt einen Zweig mit der Auswahl »Abgetrennte Arbeitskopie-Version«. Sie müssen die obigen Fehler zuerst beheben, bevor Sie eintragen können. Ihr OpenSSH öffenlicher Schlüssel Ihr Schlüssel ist abgelegt in: %s [Nach oben] Buckets commit-tree fehlgeschlagen: Fehler Fehler: »%s« kann nicht als Zweig oder Version erkannt werden Fehler: Verzeichnis »%s« kann nicht gelesen werden: Datei oder Verzeichnis nicht gefunden »%s« anfordern Dateien Dateien aktualisiert Dateien zurückgesetzt git-gui - eine grafische Oberfläche für Git. git-gui: Programmfehler Zeilen annotiert Objekte pt. »%s« versenden... Aufräumen von »%s« update-ref fehlgeschlagen: Warnung Warning: Tcl/Tk unterstützt die Zeichencodierung »%s« nicht. write-tree fehlgeschlagen: 