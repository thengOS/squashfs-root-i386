��            )         �     �  	   �     �  	   �     �     �     �     �  �     �   �     �     �     �  	   �     �     �  E   �     �          !     '     ,  !   L     n     �     �     �  	   �     �     �  4  �               ,  	   4     >     K     j     �  �   �  �   �	     y
     ~
     �
     �
     �
     �
  f   �
     '  "   /     R     W     \  -   |     �     �     �  %   �  
   �       O  $                                                                                  	      
                                                About All Fonts Back Copyright Description FONT-FILE OUTPUT-FILE Font Viewer GNOME Font Viewer GNOME Font Viewer also supports installing new font files downloaded in the .ttf and other formats. Fonts may be installed only for your use or made available to all users on the computer. GNOME Font Viewer shows you the fonts installed on your computer for your use as thumbnails. Selecting any thumbnails shows the full view of how the font would look under various sizes. Info Install Install Failed Installed Name Quit Run '%s --help' to see a full list of available command line options. SIZE Show the application's version Style TEXT Text to thumbnail (default: Aa) This font could not be displayed. Thumbnail size (default: 128) Type Version View fonts on your system [FILE...] fonts;fontface; translator-credits Project-Id-Version: gnome-font-viewer master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-font-viewer&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 22:21+0000
PO-Revision-Date: 2015-12-04 14:45+0000
Last-Translator: drei <Unknown>
Language-Team: Deutsch <gnome-de@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:15+0000
X-Generator: Launchpad (build 18115)
Language: de
X-Poedit-Bookmarks: -1,-1,-1,-1,-1,-1,27,-1,-1,-1
 Info Alle Schriftarten Zurück Copyright Beschreibung SCHRIFTART-DATEI AUSGABE-DATEI Schriftartenbetrachter GNOME Schriftartenbetrachter GNOME Schriftartenbetrachter unterstützt auch die Installation neuer Schriftartendateien als .ttf oder auch in anderen Formaten. Schriftarten können zu Ihrer eigenen Verwendung oder für alle Benutzer eines Rechners installiert werden. GNOME Schriftartenbetrachter zeigt Ihnen eine Vorschau der Schriftarten, die auf Ihrem Rechner installiert sind. Die Auswahl eines Vorschaubildes zeigt die vollständige Ansicht, wie die Schriftart unter verschiedenen Größen aussieht. Info Installieren Installation fehlgeschlagen Installiert Name Beenden »%s --help« ausführen, um eine vollständige Liste der zur Verfügung stehenden Befehle anzuzeigen. GRÖSSE Die Version der Anwendung anzeigen Stil TEXT Text der Vorschau (Vorgabe: Aa) Diese Schriftart kann nicht angezeigt werden. Vorschaugröße (Vorgabe: 128) Typ Version Schriftarten Ihres Systems betrachten [Datei…] Schriftart;Schriftartschnitt; Christian Kirbach <Christian.Kirbach@googlemail.com>, 2009, 2011, 2012.
Tobias Endrigkeit <tobiasendrigkeit@googlemail.com>, 2012.
Benjamin Steinwender <b@stbe.at>, 2014.
Bernd Homuth <dev@hmt.im>, 2014.

Launchpad Contributions:
  Benjamin Steinwender https://launchpad.net/~b-u
  Christian Kirbach https://launchpad.net/~christian-kirbach-e
  Daniel Winzen https://launchpad.net/~q-d
  Mario Blättermann https://launchpad.net/~mario.blaettermann
  Ronald Müller https://launchpad.net/~ronald-ikl-trainer
  Tobias Bannert https://launchpad.net/~toba
  drei https://launchpad.net/~dreinull 