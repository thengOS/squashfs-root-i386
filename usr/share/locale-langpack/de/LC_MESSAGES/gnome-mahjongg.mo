��    @        Y         �  *   �  �  �     H  6   M  .   �  �   �     9  �   H     �  ;   
	     F	     N	  
   W	     b	     h	     �	     �	     �	     �	     �	     �	     �	  !   �	     
     
     0
     7
     <
     P
     a
     n
  O   �
     �
     �
     �
  	   �
                 	   (  	   2  	   <     F     S     Y     b     p     x     �     �     �     �  #   �     �               9  !   T     v     �     �     �     �       Y   $  �  ~     N  S   T  )   �  �   �     �  �   �     �  O   �     &     .     7     C  =   K     �     �     �     �     �     �     �  *   �  !     #   @     d     l  #   q     �     �     �  �   �     S     Y     l     x     �     �     �  	   �     �     �     �     �     �     �     �     	            "   3     V     \     q     x          �     �     �     �     �  �  �  %   q        /   4                       ?   $   :                      ,                 2         #   ;              <   6   1         %   -   9   @           	      (         +                      8       3      )          5      "                                     *   &   >      .           
       !   0   7       '   =       A matching game played with Mahjongg tiles A solitaire version of the classic Eastern tile game. Tiles are stacked on the board at the start of the game. The goal is to remove all the tiles in as little time as possible. Select two matching tiles and they will disappear from the board, but you can only select a tile if there is an empty space to its left or right at the same level. Be careful: tiles that look alike may actually be slightly different. Date Disassemble a pile of tiles by removing matching pairs Do you want to start a new game with this map? Each puzzle has at least one solution.  You can undo your moves and try and find the solution, restart this game, or start a new one. GNOME Mahjongg GNOME Mahjongg features a variety of starting layouts, some easy and some difficult. If you get stuck, you can ask for a hint, but this adds a large time penalty. Height of the window in pixels If you continue playing the next game will use the new map. Layout: Mahjongg Main game: Maps: Match tiles and clear the board Moves Left: New Game OK Pause the game Paused Preferences Print release version and exit Receive a hint for your next move Redo your last move There are no more moves. Tiles: Time Undo your last move Unpause the game Use _new map Width of the window in pixels You can also try to reshuffle the game, but this does not guarantee a solution. _About _Background color: _Close _Contents _Continue playing _Help _Layout: _Mahjongg _New Game _New game _Preferences _Quit _Restart _Restart Game _Scores _Shuffle _Theme: _Undo game;strategy;puzzle;board; mahjongg map nameCloud mahjongg map nameConfounding Cross mahjongg map nameDifficult mahjongg map nameEasy mahjongg map nameFour Bridges mahjongg map nameOverpass mahjongg map namePyramid's Walls mahjongg map nameRed Dragon mahjongg map nameThe Ziggurat mahjongg map nameTic-Tac-Toe translator-credits true if the window is maximized Project-Id-Version: gnome-mahjongg master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-mahjongg&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 22:17+0000
PO-Revision-Date: 2015-12-04 14:56+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Deutsch <gnome-de@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:21+0000
X-Generator: Launchpad (build 18115)
Language: de_DE
 Ein Übereinstimmungsspiel mit Mahjongg-Kacheln.

Mahjongg ist ein Teil der GNOME-Spiele. Eine Solospielvariante des klassischen Kachelspiels aus Asien. Die Kacheln sind zu Beginn des Spiels auf dem Brett in Stapeln ausgelegt. Ziel des Spiels ist es, in möglichst kurzer Zeit alle Kacheln zu entfernen. Sobald zwei gleiche Kacheln ausgewählt werden, verschwinden sie vom Spielfeld. Aber vorsicht, eine Kachel kann nur gewählt werden, wenn unmittelbar daneben ein freies Feld ist. Erschwerend kommt hinzu, dass einige Kacheln sich sehr ähnlich sehen. Datum Einen Haufen Steine durch Entfernen der zusammengehörenden Paare auseinandernehmen Ein neues Spiel mit dieser Karte starten? Jedes Rätsel hat mindestens eine Lösung. Sie können Ihre Züge zurücknehmen und durch Ausprobieren die Lösung zu finden (wofür Sie eine Zeitstrafe in Kauf nehmen müssen), dieses Spiel neu starten oder ein neues Spiel beginnen. GNOME Mahjongg Gnome Mahjongg bietet eine Vielzahl an verschiedenen Ausgangspositionen. Einige davon sehr leicht, andere eher schwierig. Kommen Sie einmal nicht weiter, können Sie sich helfen lassen. Dafür gibt es dann allerdings eine größere Zeitstrafe. Fensterhöhe in Pixeln Falls Sie dieses Spiel fortsetzen, verwendet das nächste Spiel die neue Karte. Layout: Mahjongg Hauptspiel: Karten: Finden Sie übereinstimmende Kacheln und leeren Sie das Brett Verbleibende Züge: Neues Spiel OK Das Spiel anhalten Pausiert Einstellungen Version ausgeben und beenden Einen Tipp bekommen für den nächsten Zug Den letzten Zug erneut ausführen Es gibt keine gültigen Züge mehr. Kachel: Zeit Den letzten Zug rückgängig machen Weiterspielen Neue Karte _verwenden Fensterbreite in Pixeln Sie können versuchen das Spiel neu zu mischen – eine Garantie dafür, dass es sich dann leichter lösen lässt, gibt es nicht. _Info _Hintergrundfarbe: S_chließen _Inhalt Spiel _fortsetzen _Hilfe _Layout: _Mahjongg _Neues Spiel _Neues Spiel _Einstellungen _Beenden Neu sta_rten Spiel neusta_rten Er_gebnisse _Mischen _Thema: Zug _rückgängig machen Spiel;Strategie;Puzzle;Brettspiel; Wolke Durchbrochenes Kreuz Schwer Leicht Vier Brücken Überführung Pyramidenwände Roter Drache Der Ziggurat Tic-Tac-Toe Karl Eichwalder <ke@suse.de>
Christian Meyer <chrisime@gnome.org>
Christian Neumair <christian-neumair@web.de>
Carsten Schaar <nhadcasc@fs-maphy.uni-hannover.de>
Matthias Warkus <mawarkus@gnome.org>
Frank Arnold <frank@scirocco-5v-turbo.de>
Manuel Borchers <m.borchers@gnome-de.org>
Hendrik Richter <hrichter@gnome.org>
Benedikt Wicklein <benedikt.wicklein@googlemail.com>
Philipp Kerling <k.philipp@gmail.com>
Björn Deiseroth <service@dual-creators.de>
Andre Klapper <ak-47@gmx.net>
Mario Blättermann <mario.blaettermann@gmail.com>
Christian Kirbach <Christian.Kirbach@googlemail.com>
Tobias Endrigkeit <tobiasendrigkeit@googlemail.com>

Launchpad Contributions:
  Christian Kirbach https://launchpad.net/~christian-kirbach-e
  Hendrik Lübke https://launchpad.net/~henni123468
  Mario Blättermann https://launchpad.net/~mario.blaettermann
  Tobias Bannert https://launchpad.net/~toba
  drei https://launchpad.net/~dreinull Wahr, falls das Fenster maximiert ist 