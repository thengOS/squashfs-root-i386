��    L      |  e   �      p  $   q     �     �  	   �     �  
   �     �  #   �  #        :     @     G      L     m  !        �    �     �  <   �     	  
    	     +	     8	     D	     J	  "   S	     v	     �	     �	     �	     �	     �	     �	     �	  ;   
  S   A
  2   �
  [   �
  /   $     T     Z  
   f     q     }  $   �     �  #   �     �     �     �          -  �   K                    "  	   )     3     ;     A  	   H     R     V     ]  
   i     t     z     �     �     �     �     �     �     �  C  �  $   8     ]     |     �     �     �     �      �  ,   �  
   '     2     D  %   J     p  *   �     �  E  �     �  J        a     }     �     �     �     �  6   �  5   �     4     =     O  #   `  '   �     �     �  F   �  r     ^     Y   �  5   8     n     w     �     �     �  7   �     �  /        A     F     ]     {     �  �   �     �     �  
   �     �     �     �     �     �     �     �     �     �                     $     0     A     V     ^  �  }  %   |     C   D                !   *   <   E          J      	       G   :   L              +      &      ,                    #   )   3   $               "         @   /             2   H       A      1              '      I   F   .   >                    0   K   ?   6            (               7   9                     B       ;   =       4   5       
             %   -   8    %u × %u, %u mine %u × %u, %u mines <b>%d</b> mine <b>%d</b> mines A_ppearance Big board Big game Board size Change _Difficulty Clear explosive mines off the board Clear hidden mines from a minefield Close Custom Date Do you want to start a new game? Enable animations Enable automatic placing of flags GNOME Mines GNOME Mines is a puzzle game where you search for hidden mines. Flag the spaces with mines as quickly as possible to make the board a safer place. You win the game when you've flagged every mine on the board. Be careful not to trigger one, or the game is over! Height of the window in pixels If you start a new game, your current progress will be lost. Keep Current Game Main game: Medium board Medium game Mines New Game Number of columns in a custom game Number of rows in a custom game Paused Percent _mines Play _Again Print release version and exit Resizing and SVG support: Score: Select Theme Set to false to disable theme-defined transition animations Set to true to automatically flag squares as mined when enough squares are revealed Set to true to be able to mark squares as unknown. Set to true to enable warning icons when too many flags are placed next to a numbered tile. Size of the board (0-2 = small-large, 3=custom) Size: Small board Small game St_art Over Start New Game The number of mines in a custom game The theme to use The title of the tile theme to use. Time Use _animations Use the unknown flag Warning about too many flags Width of the window in pixels You can select the size of the field you want to play on at the start of the game. If you get stuck, you can ask for a hint: there's a time penalty, but that's better than hitting a mine! _About _Best Times _Cancel _Close _Contents _Height _Help _Mines _New Game _OK _Pause _Play Again _Play Game _Quit _Resume _Scores _Show Warnings _Use Question Flags _Width minesweeper; translator-credits true if the window is maximized Project-Id-Version: gnome-mines master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-mines&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-02-11 17:13+0000
PO-Revision-Date: 2016-02-12 02:20+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Deutsch <gnome-de@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 19:21+0000
X-Generator: Launchpad (build 18115)
Language: de_DE
 %u × %u, %u Mine %u × %u, %u Minen <b>%d</b> Mine <b>%d</b> Minen _Erscheinungsbild Großes Spielfeld Großes Spiel Spielfeldgröße _Schwierigkeit verändern Explosive Minen vom Feld räumen Versteckte Minen aus einem Minenfeld räumen Schließen Benutzerdefiniert Datum Möchten Sie ein neues Spiel starten? Animationen einschalten Automatische Fahnen-Platzierung aktivieren Minen »Minen« ist ein Denkspiel, bei dem man nach versteckten Minen sucht. Markieren Sie die Felder mit Minen so schnell wie möglich, um das Spielfeld zu einem sicheren Ort zu machen. Sie gewinnen das Spiel, wenn Sie jede Mine auf dem Feld markiert haben. Geben Sie Acht, keine der Minen auszulösen, sonst ist das Spiel vorbei! Fensterhöhe in Pixeln Wenn Sie ein neues Spiel starten, geht Ihr aktueller Fortschritt verloren. Das aktuelle Spiel behalten Hauptspiel: Mittelgroßes Spielfeld Mittelgroßes Spiel Minen Neues Spiel Anzahl der Spalten des benutzerdefinierten Spielfeldes Anzahl der Reihen des benutzerdefinierten Spielfeldes Pausiert _Anteil der Minen Nochmal _spielen Versionsnummer ausgeben und beenden Größenänderungs-/SVG-Unterstützung: Punkte: Thema auswählen Legt fest, ob themenbasierte Übergangsanimationen abgeschaltet werden Legt fest, ob Kästchen automatisch als vermint markiert werden sollen, wenn genügend Kästchen aufgedeckt wurden Falls dieser Schlüssel wahr ist, ist es Ihnen gestattet, Quadrate als unbekannt zu markieren. Legt fest, ob Sie gewarnt werden, wenn zu viele Fahnen neben einer Zahl platziert wurden. Brettgröße (0-2: klein-groß, 3: Benutzerdefiniert) Größe: Kleines Spielfeld Kleines Spiel _Von neuem beginnen Ein neues Spiel starten Die Anzahl der Minen in einem benutzerdefinierten Spiel Zu verwendendes Thema Der Titel des zu verwendenden Spielsteinthemas. Zeit _Animationen verwenden Die Unbekannt-Fahne verwenden Warnen bei zu vielen Fahnen Fensterbreite in Pixeln Sie können die Spielfeldgröße zu Beginn festlegen. Falls Sie nicht weiterkommen, können Sie sich einen Hinweis anzeigen lassen: Es gibt eine Zeitstrafe dafür, aber das ist immer noch besser, als auf eine Mine zu treffen! _Info _Bestzeiten _Abbrechen _Schließen Inha_lt _Höhe _Hilfe _Minen _Neues Spiel _OK _Pause _Weiterers Spiel Spiel _starten _Beenden _Fortsetzen Er_gebnisse _Zeige Warnungen _Fragen-Fahne zeigen Brei_te Minesweeper;Mine;Spiel;Puzzle; Karl Eichwalder <ke@suse.de>
Christian Meyer <chrisime@gnome.org>
Christian Neumair <christian-neumair@web.de>
Carsten Schaar <nhadcasc@fs-maphy.uni-hannover.de>
Matthias Warkus <mawarkus@gnome.org>
Frank Arnold <frank@scirocco-5v-turbo.de>
Manuel Borchers <m.borchers@gnome-de.org>
Hendrik Richter <hrichter@gnome.org>
Benedikt Wicklein <benedikt.wicklein@googlemail.com>
Philipp Kerling <k.philipp@gmail.com>
Björn Deiseroth <service@dual-creators.de>
Andre Klapper <ak-47@gmx.net>
Mario Blättermann <mario.blaettermann@gmail.com>
Christian Kirbach <Christian.Kirbach@googlemail.com>
Tobias Endrigkeit <tobiasendrigkeit@googlemail.com>
Jonatan Zeidler <jonatan_zeidler@gmx.de>

Launchpad Contributions:
  Benjamin Steinwender https://launchpad.net/~b-u
  Christian Kirbach https://launchpad.net/~christian-kirbach-e
  Hendrik Lübke https://launchpad.net/~henni123468
  Mario Blättermann https://launchpad.net/~mario.blaettermann
  Wolfgang Stöggl https://launchpad.net/~c72578
  drei https://launchpad.net/~dreinull Wahr, falls das Fenster maximiert ist 