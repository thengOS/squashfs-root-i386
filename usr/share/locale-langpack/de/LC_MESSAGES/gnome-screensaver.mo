��    7      �  I   �      �     �     �     �     �  )   
     4  (   @     i          �  "   �  E   �       %   +     Q     Z     b  )   �     �  *   �     �  /   �     #  	   6  8   @  "   y     �     �     �     �     �     �       D   9  `   ~     �     �  )   	     A	  *   S	  #   ~	  	   �	     �	  D   �	  D   
     R
  !   q
  ,   �
  B   �
  
          '         >  +   _  >  �     �     �  &     *   +     V     t  ,   �     �  #   �     �  :     X   F     �  0   �  	   �  	   �  $   �  ;        X  (   p     �  G   �     �  	     3     *   I      t     �     �      �     �  '   �  6   '  J   ^  d   �        "   /  /   R     �  7   �  /   �     �       ;   &  M   b  !   �  *   �  7   �  S   5  
   �     �  7   �  ,   �  9            -              0                	      1              4      %       '      *      6                 !   &          #         7   
   "       (      5   /   +              )   2                           ,   $         .            3                         (current) Unix password: Authentication failed. Can't set PAM_TTY=%s Cannot get username Causes the screensaver to exit gracefully Checking… Command to invoke from the logout button Don't become a daemon Enable debugging code Enter new Unix password: Error while changing NIS password. If the screensaver is active then deactivate it (un-blank the screen) Incorrect password. Launch screensaver and locker program Log _Out MESSAGE Message to show in the dialog No longer permitted to access the system. No password supplied Not permitted to gain access at this time. Not used Password has been already used. Choose another. Password unchanged Password: Query the length of time the screensaver has been active Query the state of the screensaver Retype new Unix password: S_witch User… Screensaver Show debugging output Show the logout button Show the switch user button Sorry, passwords do not match Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Time has expired. Turn the screensaver on (blank the screen) Unable to establish service %s: %s
 Username: Version of this application You are required to change your password immediately (password aged) You are required to change your password immediately (root enforced) You have the Caps Lock key on. You must choose a longer password You must wait longer to change your password Your account has expired; please contact your system administrator _Password: _Unlock failed to register with the message bus not connected to the message bus screensaver already running in this session Project-Id-Version: gnome-screensaver master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-01-04 14:27+0000
PO-Revision-Date: 2016-01-05 00:35+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: German <gnome-de@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:43+0000
X-Generator: Launchpad (build 18115)
 (Aktuelles) UNIX-Passwort: Legitimierung ist gescheitert. PAM_TTY=%s konnte nicht gesetzt werden Benutzername konnte nicht ermittelt werden Den Bildschirmschoner beenden Überprüfung läuft … Befehl, der vom Abmeldeknopf aufgerufen wird Kein Hintergrundprozess Fehlerdiagnosefunktionen aktivieren Neues UNIX-Passwort: Beim Ändern des NIS-Passworts ist ein Fehler aufgetreten. Deaktivieren des Bildschirmschoners, falls er aktiv ist (Bildschirmabdunkelung aufheben) Falsches Passwort. Bildschirmschoner und Bildschirmsperrung starten A_bmelden NACHRICHT Die im Dialog anzuzeigende Nachricht Es ist nicht länger gestattet, auf das System zuzugreifen. Kein Passwort angegeben Ein Zugriff ist derzeit nicht gestattet. Nicht verwendet Dieses Passwort wurde bereits verwendet. Bitte wählen Sie ein anderes. Passwort ist unverändert Passwort: Die Länge der Bildschirmschoneraktivität abfragen Den Status des Bildschirmschoners abfragen Neues UNIX-Passwort wiederholen: Benutzer _wechseln … Bildschirmschoner Fehlerdiagnose-Ausgaben anzeigen Den Knopf zum Abmelden anzeigen Knopf für den Benutzerwechsel anzeigen Entschuldigung, die Passwörter stimmen nicht überein Den Bildschirm über den laufenden Bildschirmschonerprozess sofort sperren Der Bildschirmschoner war für %d Sekunde aktiv.
 Der Bildschirmschoner war für %d Sekunden aktiv.
 Der Bildschirmschoner ist aktiv
 Der Bildschirmschoner ist inaktiv
 Der Bildschirmschoner ist derzeit nicht aktiv.
 Zeitüberschreitung. Den Bildschirmschoner aktivieren (Bildschirm abdunkeln) Dienst %s konnte nicht eingerichtet werden: %s
 Benutzername: Version dieser Anwendung Sie müssen Ihr Passwort sofort ändern (Passwort veraltet) Sie müssen Ihr Passwort sofort ändern (durch Systemadministrator erzwungen) Die Feststelltaste ist aktiviert. Sie müssen ein längeres Passwort wählen Sie müssen länger warten, um Ihr Passwort zu ändern. Ihr Benutzerkonto ist abgelaufen. Bitte kontaktieren Sie Ihren Systemadministrator. _Passwort: Sperrung auf_heben Registrierung am Benachrichtigungssystem fehlgeschlagen Keine Verbindung zum Benachrichtigungssystem Der Bildschirmschoner ist in dieser Sitzung bereits aktiv 