��    F      L  a   |                        
   #  )   .  {   X  �   �     m     }     �     �     �     �     �  ?   �  �   7     	     ,	     G	     N	     Z	  �   g	     "
     ?
     V
     o
     �
  %   �
  &   �
     �
  v   �
  +   v     �  &   �     �  0   �     "  1   )  A   [  3   �  2   �            /   -     ]  3   r  d   �          )     0     6     >     K     c     i     o     u     }     �     �     �     �     �     �  
   �  	   �     �     �     �  �       �     �     �           +  �   L  �   �     �     �     �  !   �          !  
   8  @   C    �     �     �     �     �     �  �   �  *   �               ;     U  +   o  /   �     �     �  1   g     �  (   �     �  !   �       E     D   \  -   �  2   �  !        $  0   C      t  @   �  r   �     I     a     h  
   q     |  !   �     �     �     �     �     �     �     �     �                    (  
   5     @  !  N  %   p            2   D       (         '             -   3           >       &   ;      #       )   6   A      $   4   =      E              B   5   	       ?          C         /                        +                   9   7   ,      F      @          1   .   
   %           8                 *   "       :                0                    !             <    Clear Create Puzzle Custom Puzzle Difficulty Difficulty level of sudokus to be printed Displays a big red X in a square if it cannot possibly be filled by any number and duplicate numbers are highlighted in red Each game is assigned a difficulty similar to those given by newspapers and websites, so your game will be as easy or as difficult as you want it to be. Easy Difficulty Error printing file: GNOME Sudoku Go back to the current game Hard Difficulty Height of the window in pixels High_lighter Highlight row, column and square that contain the selected cell If you like to play on paper, you can print games out. You can choose how many games you want to print per page and what difficulty of games you want to print: as a result, GNOME Sudoku can act a renewable Sudoku book for you. Medium Difficulty Number of Sudokus to print Paused Play _Again Play _Anyway Play the popular Japanese logic game. GNOME Sudoku is a must-install for Sudoku lovers, with a simple, unobtrusive interface that makes playing Sudoku fun for players of any skill level. Please enter a valid puzzle. Print Multiple Puzzles Print _Current Puzzle… Print _Multiple Puzzles… Redo your last action Reset the board to its original state Reset the board to its original state? Select Difficulty Set the difficulty level of the sudokus you want to print. Possible values are - "easy", "medium", "hard", "very_hard" Set the number of sudokus you want to print Show release version Show the possible values for each cell Start a new puzzle Start playing the custom puzzle you have created Sudoku Test your logic skills in this number grid puzzle The popular Japanese logic puzzle

Puzzles generated by QQwing %s The puzzle you have entered has multiple solutions. The puzzle you have entered is not a valid Sudoku. Undo your last action Unknown Difficulty Valid Sudoku puzzles have exactly one solution. Very Hard Difficulty Warn about unfillable squares and duplicate numbers Well done, you completed the puzzle in %d minute! Well done, you completed the puzzle in %d minutes! Width of the window in pixels _About _Back _Cancel _Clear Board _Create your own puzzle _Easy _Hard _Help _Medium _New Puzzle _Number of puzzles _Pause _Print _Quit _Resume _Start Playing _Very Hard _Warnings magic;square; translator-credits true if the window is maximized Project-Id-Version: gnome-sudoku
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-03 22:29+0000
PO-Revision-Date: 2016-06-14 09:05+0000
Last-Translator: Tim Düsterhus <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:48+0000
X-Generator: Launchpad (build 18115)
 Löschen Puzzle erstellen Benutzerdefiniertes Puzzle Schwierigkeitsgrad Zu druckende Schwierigkeitstufen Anzeige eines großen roten »X« in einem Kasten, in den keine Zahl eingetragen werden kann. Doppelte Zahlen werden in roter Farbe hervorgehoben. Jedem Spiel ist ein eigener Schwierigkeitsgrad zugeordnet, so wie man es aus Zeitungen oder von Webseiten kennt. Das Spiel wird also so leicht oder so schwierig sein, wie Sie es möchten. Leichter Schwierigkeitsgrad Fehler beim Drucken der Datei: GNOME Sudoku Zurück zum laufenden Spiel gehen Schwere Schwierigkeitsstufe Fensterhöhe in Pixeln Textmarker Zeile, Spalte und Kasten mit der ausgewählten Zelle hervorheben Falls Sie lieber mit Stift und Papier spielen, können Sie die Spiele ausdrucken. Sie können entscheiden, wieviele Spiele Sie auf eine Seite drucken möchten und welche Schwierigkeit die Spiele haben sollen: so kann GNOME Sudoku Ihr erneuerbares Sudoku-Rätsel-Heft sein. Mittelschwer Anzahl zu druckender Sudokus Pausiert Nochmal _spielen _Trotzdem Spielen Spielen Sie das beliebte japanische Logikspiel. GNOME Sudoku ist eine Pflichtinstallation für Sudoku-Liebhaber. Sie verfügt über über eine einfache, unaufdringliche Bedienung, die Spielern unterschiedlicher Stärke viel Freude bereitet. Bitte geben Sie ein gültiges Rätsel ein. Mehrere Rätsel drucken Drucke_aktuelles Rätsel… Drucke_mehrere Rätsel… Letzte Aktion wiederholen Zahlenfeld in Ausgangszustand zurücksetzen Spielfläche auf Originalzustand zurücksetzen? Schwierigkeitsstufe wählen Stellen Sie die druckenden Schwierigkeitsstufen ein. Mögliche Werte sind - »leicht«, »mittel«, »schwer«, »sehr_schwer« Geben Sie die Anzahl der zu druckenden Sudokus an Version anzeigen Mögliche Werte für jede Zelle anzeigen Ein neues Rätsel starten Selbst erstelltes Rätsel starten Sudoku Testen Sie Ihre Logik-Fähigkeiten bei diesem Zahlen-Kreuzworträtsel Das beliebte japanische Logikrätsel

Rätsel erstellt von QQwing %s Das eingegebene Puzzle hat mehrere Lösungen. Das eingegebene Rätsel ist kein gültiges Sudoku. Letzte Aktion rückgängig machen Unbekannter Schwierigkeitsgrad Korrekte Sudokus haben immer genau eine Lösung. Sehr schwere Schwierigkeitsstufe Warnung anzeigen für unausfüllbare Kästen und doppelte Zahlen Gut gemacht, Sie haben das Rätsel in %d Minute gelöst! Gut gemacht, Sie haben das Rätsel in %d Minuten gelöst! Fensterbreite in Pixeln _Über _Zurück _Abbrechen _Zahlenfeld löschen _Erstellen Sie Ihr eigenes Puzzel _Leicht _Schwer _Hilfe _Mittel _Neues Rätsel _Anzahl der Rätsel _Pause _Drucken _Beenden _Fortsetzen _Starte Spiel _Sehr schwer _Warnungen magic;square; Launchpad Contributions:
  Alexander Eifler https://launchpad.net/~eifx
  Christian Heitjan https://launchpad.net/~heitjan
  Christoph Klotz https://launchpad.net/~klotzchristoph
  Hendrik Knackstedt https://launchpad.net/~hennekn
  Maciej Krüger https://launchpad.net/~mkg20001
  Marc https://launchpad.net/~m-welpen123
  Tim Düsterhus https://launchpad.net/~timwolla
  Torsten Franz https://launchpad.net/~torsten.franz
  bug https://launchpad.net/~clma
  floe-de https://launchpad.net/~floe-de
  schuko24 https://launchpad.net/~gerd-saenger Wahr, falls das Fenster maximiert ist 