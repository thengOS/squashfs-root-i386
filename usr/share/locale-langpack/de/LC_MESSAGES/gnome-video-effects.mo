��    E      D  a   l      �     �  @   	  /   J  &   z  )   �  -   �     �     �          &     =     I      P     q  -   v  =   �  +   �       
   &     1  B   6     y     �  )   �     �  
   �     �  !   �      �  	   	     "	     /	  -   7	     e	     k	     r	  
   �	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	  
   �	  #   
     ,
     2
     9
  !   A
  
   c
  )   n
  $   �
  (   �
  *   �
  -     $   ?  +   d  4   �  (   �     �     �                    (  (  .     W  Q   s  6   �  (   �  -   %  E   S  
   �     �     �  -   �     �       &        3  -   ;  I   i  *   �     �  
   �       <     (   D     m  2   v  
   �  
   �     �     �  %   �  	   	            )   '     Q     Y  3   b     �     �  
   �  &   �     �  
   �     �  
   	               (  -   6     d     j     r     {     �  1   �  (   �  2     -   7  ,   e  %   �  -   �  9   �           8     ?     _     g  
   l     w        7                     B                 6   1      =   >       E               8       $              ;   %   -       ?   *             D       )   0       '              &      !      <   	   :   +   3                 A      /              C   
   #                          2                (   @   4       5             "       9   ,         .        A triangle Kaleidoscope Add a loopback alpha blending effector with rotating and scaling Add age to video input using scratches and dust Add more saturation to the video input Add some hallucination to the video input Add the ripple mark effect on the video input Bulge Bulges the center of the video Cartoon Cartoonify video input Che Guevara Chrome Detect radioactivity and show it Dice Dices the video input into many small squares Display video input like good old low resolution computer way Dissolves moving objects in the video input Distort the video input Distortion Edge Extracts edges in the video input through using the Sobel operator Fake heat camera toning Flip Flip the image, as if looking at a mirror Heat Historical Hulk Invert and slightly shade to blue Invert colors of the video input Invertion Kaleidoscope Kung-Fu Makes a square out of the center of the video Mauve Mirror Mirrors the video Noir/Blanc Optical Illusion Pinch Pinches the center of the video Quark Radioactive Ripple Saturation Sepia Sepia toning Shagadelic Show what was happening in the past Sobel Square Stretch Stretches the center of the video Time delay Traditional black-white optical animation Transform motions into Kung-Fu style Transform video input into a mauve color Transform video input into a metallic look Transform video input into a waveform monitor Transform video input into grayscale Transform video input into realtime goo'ing Transform video input into typical Che Guevara style Transform yourself into the amazing Hulk Twirl Twirl the center of the video Vertigo Warp Waveform X-Ray Project-Id-Version: gnome-video-effects master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-video-effects&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-10-29 03:54+0000
PO-Revision-Date: 2015-11-10 03:41+0000
Last-Translator: greenscandic <Unknown>
Language-Team: Deutsch <gnome-de@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:02+0000
X-Generator: Launchpad (build 18115)
Language: de
X-Poedit-Language: German
 Ein dreieckiges Kaleidoskop Einen Überblendungseffekt des Alphakanals mit Drehung und Skalierung hinzufügen Video durch Kratzer und Staub älter erscheinen lassen Die Sättigung im Eingangsvideo erhöhen Dem Eingangsvideo Halluzinationen hinzufügen Den Effekt von auf eine Wasserfläche fallender Regentropfen anwenden Aufblähen Bläht die Mitte des Videos auf Cartoon Eingangsvideo wie ein Cartoon aussehen lassen Che Guevara Chrom Radioaktivität erkennen und aufzeigen Würfel Eingangsvideo in viele kleine Felder würfeln Eingangsvideo wie auf alten Computern mit niedriger Auflösung darstellen Bewegte Objekte im Eingangsvideo auflösen Eingangsvideo verzerren Verzerrung Ecken Im Eingangsvideo Kanten mit Hilfe des Sobel-Operators finden Imitiert das Bild einer Wärmebildkamera Spiegeln Das Bild spiegeln, wie beim Blick in einen Spiegel Wärmebild Historisch Hulk Negativbild mit blauer Tönung Farben des Eingangsvideos invertieren Umkehrung Kaleidoskop Kung-Fu Verzerrt die Mitte des Videos quadratisch Violett Spiegeln Spiegelt das Video an einer senkrechten Mittellinie Schwarz-weiß Optische Täuschung Schrumpfen Lässt die Mitte des Videos schrumpfen Quark Radioaktiv Wassertropfen Sättigung Sepia Sepia-Tönung Psychedelisch Zeigen, was in der Vergangenheit passiert ist Sobel Quadrat Strecken Streckt die Mitte des Videos Zeitverzögerung Traditionelle optische Animation in schwarz-weiß Bewegungen in den Kung-Fu-Stil umwandeln Das Eingangsvideo in eine violette Farbe umwandeln Eingangsvideo ein metallisches Aussehen geben Erscheinungsbild eines Oszilloskops anwenden Eingangsvideo in Graustufen umwandeln Video in eine Echtzeitschleife transformieren Eingangsvideo in den typischen Che Guevara-Stil umwandeln Sich in Hulk verwandeln Wirbel Verwirbelt die Mitte des Videos Vertigo Warp Wellenform Röntgen 