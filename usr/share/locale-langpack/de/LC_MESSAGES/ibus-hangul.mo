��          �   %   �      0  
   1     <     M     d     m     �     �     �     �     �     �  
   �     �  C        L     ]     q  �   �          ,     =     Y     _  �  u  
        &     9  	   O     Y     q  #   �     �     �     �     �     �     �  P        \     r     �  �   �  '   �     �  #   �  
   �     �                                                       	              
                                                          <b>Etc</b> <b>Hanja key</b> <b>Keyboard Layout</b> Advanced Configure hangul engine Enable/Disable Hangul mode Enable/Disable Hanja mode Hangul Hangul _keyboard: Hangul mode Hanja Hanja lock IBus Hangul Preferences IBus daemon is not running.
Hangul engine settings cannot be saved. IBusHangul Setup Korean Input Method Korean input method Press any key which you want to use as hanja key. The key you pressed is displayed below.
If you want to use it, click "Ok" or click "Cancel" Select Hangul toggle key Select Hanja key Set IBus Hangul Preferences Setup Start in _hangul mode Project-Id-Version: ibus-hangul
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-03 22:49+0000
PO-Revision-Date: 2015-09-25 17:42+0000
Last-Translator: schuko24 <gerdsaenger@t-online.de>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:38+0000
X-Generator: Launchpad (build 18115)
 <b>Etc</b> <b>Hanja-Taste</b> <b>Tastenbelegung</b> Erweitert Hangul-Modul einrichten Hangul-Modus Ein-/Ausschalten Hanja-Modus aktivieren/deaktivieren Hangul Hangul _Tastatur: Hangul-Modus Hanja Hanja-Sperre Einstellungen für IBus-Hangul IBus Daemon läuft nicht.
Hangul Einstellungen können nicht gespeichert werden. IBusHangul einrichten Koreanische Eingabemethode Koreanische Eingabemethode Drücken Sie eine beliebige Taste, die Sie als Hanja-Taste verwenden möchten. Die Taste, die sie drücken, wird unten angezeigt.
Wenn Sie diese verwenden möchten, klicken Sie auf »Ok«, ansonsten klicken Sie auf »Abbrechen« Umschalttaste für Hangul-Modus wählen Hanja-Taste auswählen IBus-Hangul-Einstellungen festlegen Einrichten Im Hangul-Modus beginnen 