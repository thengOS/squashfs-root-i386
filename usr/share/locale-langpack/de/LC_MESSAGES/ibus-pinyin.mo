��    K      t  e   �      `     a     v     �     �     �     �     �     �  	           d   /  1   �     �     �     �     �     �               !     *     A  
   V     a     o  &   �  .   �  (   �     	     	     .	     6	     ;	     Q	     V	     b	     r	     �	     �	     �	     �	     �	  
   �	     �	     �	     �	     �	  	   

     
     
     /
     J
     O
     ^
     r
     �
     �
     �
     �
     �
     �
       
        *     3     ?     R     [     _     d     h     z     �     �  �  �     `     v     �     �  !   �     �     �       	         )  c   J  1   �     �     �     �     �  (        >  
   M     X     d     �     �     �  %   �  *   �  <     ,   L  #   y     �     �     �     �     �     �       '        C  	   V     `     h     m  
   �     �     �     �     �     �     �     �     �               0  &   F     m     �     �     �     �  (   �  '   �          $     -  $   :     _     h     l     q     u     �     �     �             ,   A       #      $      )          "   E   @      :   	                  6      &       4                       2   9      !   <   1   5   ;         7              C   F       K   ?   
          8      H   *         '      G                 -   =   %       >   D          I   +             /   0          B         .          3           (           J            <b>Bopomofo mode</b> <b>Correct pinyin</b> <b>Dictionary option</b> <b>Initial state</b> <b>Input Custom</b> <b>Other</b> <b>Pinyin mode</b> <b>Selection mode</b> <b>UI</b> <big><b>IBus Pinyin %s</b></big> <small>
<b>Authors:</b>
Peng Huang
BYVoid
Peng Wu

<b>Contributors:</b>
koterpilla, Zerng07
</small> <small>Copyright (c) 2009-2010 Peng Huang</small> ABC About Auto commit Bopomofo (debug) Bopomofo input method (debug) Bopomofo mode Chinese Chinese: Commit first candidate Commit original text Dictionary Double pinyin Edit custom phrases Enable Auxiliary Select Keys F1 .. F10 Enable Auxiliary Select Keys Numbers on Keypad Enable Guidekey for Candidates Selection Enable correct pinyin Enable fuzzy syllable English Eten Feature of Enter key: Full Full pinyin Full/Half width Full/Half width punctuation Fuzzy syllable General GinYieh Half Half/full width: Horizontal IBM Incomplete Bopomofo Incomplete pinyin Keyboard Mapping: Language: MSPY Number of candidates: Orientation of candidates: PYJJ Pinyin (debug) Pinyin input method Pinyin input method (debug) Pinyin input method for IBus Pinyin mode Preferences Punctuations: Selection Keys: Show raw input of Double Pinyin Simplfied/Traditional Chinese Simplified Standard Traditional Use custom phrases Vertical XHE ZGPY ZRM [,] [.] flip page [-] [=] flip page [Shift] select candidate http://ibus.googlecode.com Project-Id-Version: ibus-pinyin
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-09-20 13:06+0000
PO-Revision-Date: 2013-03-02 10:12+0000
Last-Translator: Hendrik Knackstedt <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:36+0000
X-Generator: Launchpad (build 18115)
 <b>Bopomofo-Modus</b> <b>Korrektes Pinyin</b> <b>Wörterbucheinstellungen</b> <b>Anfangszustand</b> <b>Benutzerdefinierte Eingabe</b> <b>Sonstiges</b> <b>Pinyin-Modus</b> <b>Auswahlmodus</b> <b>UI</b> <big><b>IBus-Pinyin %s</b></big> <small>
<b>Autoren:</b>
Peng Huang
BYVoid
Peng Wu

<b>Mitwirkende:</b>
koterpilla, Zerng07
</small> <small>Copyright (c) 2009-2010 Peng Huang</small> ABC Über Auto-Bestätigen Bopomofo (Fehlerdiagnose) Bopomofo-Eingabemethode (Fehlerdiagnose) Bopomofo-Modus Chinesisch Chinesisch: Ersten Kandidaten einreichen Ursprünglichen Text einreichen Wörterbuch Doppeltes Pinyin Benutzerdefinierte Phrasen bearbeiten Auswahlhilfstasten (F1 .. F10) einschalten Auswahlhilfstasten (Zahlen auf dem Ziffernblock) einschalten Leittaste für Kandidatenauswahl einschalten Korrekte Pinyin-Eingabe einschalten Undeutliche Silben einschalten Englisch Eten Funktion der Eingabetaste: Vollständig Vollständiges Pinyin Volle/Halbe Breite Zeichensetzung mit voller/halber Breite Undeutliche Silben Allgemein GinYieh Halb Halbe/Volle Breite: Horizontal IBM Unvollständiges Bopmofo Unvollständiges Pinyin Tastenbelegung: Sprache: MSPY Anzahl der Kandidaten: Orientierung der Kandidaten: PYJJ Pinyin (Fehlerdiagnose) Pinyin-Eingabemethode Pinyin-Eingabemethode (Fehlerdiagnose) Pinyin-Eingabemethode für IBus Pinyin-Modus Einstellungen Zeichensetzung: Auswahltasten: Roheingabe des doppelten Pinyin anzeigen Vereinfachtes/Traditionelles Chinesisch Vereinfacht Standard Traditionell Benutzerdefinierte Phrasen verwenden Vertikal XHE ZGPY ZRM [,] [.] Seite umblättern [-] [=] Seite umblättern [Shift] Kandidat auswählen http://ibus.googlecode.com 