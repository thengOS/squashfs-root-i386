��    m      �  �   �      @	     A	     W	     f	  �   z	     T
     Z
     q
     �
     �
     �
     �
  F   �
               #     1  �  8     �  *   �                         :     Z     b     g  
   l  1   w     �  %   �  �  �  �   �  W   ;  �   �  
   I     T     `     m     �  	   �     �     �     �     �  
   �     �     �     �     �       >        Y     b     h     |  %   �     �     �     �     �          !     3     J     ^     r     �     �     �  Q   �           7     S     s     �  .   �  )   �  �        �     �     �  '   �  6     (   B  6   k  )   �     �     �     �     �       �   3  h   �  q   >     �  %   �     �       �     Q   �  C   M  B   �  G   �  F        c    g    �  �  �     Y      p         �   �      r!     �!     �!     �!     �!     �!     �!  j   	"  
   t"     "     �"     �"    �"     �$  2   �$     �$     �$     �$  &   %  +   9%     e%     n%     {%  
   �%  9   �%     �%  )   �%  �  &  	  �(  �   �)  �   ]*     O+     \+     i+  (   z+     �+     �+     �+     �+     �+     �+     �+  	   ,     ,     ,     &,  -   <,  l   j,     �,     �,     �,     
-     #-     C-     c-     v-     �-     �-     �-     �-     �-     
.     %.     9.  #   W.  !   {.  X   �.  &   �.  -   /  1   K/  -   }/  1   �/  0   �/  &   0  �   50  %   �0     �0  !   1  )   51  ;   _1  (   �1  ;   �1  )    2     *2     :2  (   O2  )   x2  -   �2  �   �2  {   }3  q   �3     k4      �4      �4     �4  !  �4  F   �5  W   96  Y   �6  Y   �6  [   E7     �7  y  �7     9     \   2               )   =   h   k   l   g       i   '          A   +   f   ^   !   
   d   `                (      #      Z          .   B              e   /       b   m          V       c   >   	       "      ;   P   J              O       X   0   R           %   &                   j       M                 6          F   I   U                            Y   N       _   1   4      G      <      E   8   7      3   a   ?       Q   D       [   -      ]                  W   ,   H       :   T   9   S   5               $           @       C      *       K   L    <b>Candidate list</b> <b>Details</b> <b>Inital state</b> <small>
<b>Authors:</b>
Yuwei YU (‘acevery’)
Peng Huang
BYVoid
Peng Wu

<b>Contributors:</b>
koterpilla
Zerng07
Caius ‘kaio’ Chance
Mike FABIAN
Bernard Nauwelaerts
Xiaojun Ma
mozbugbox
Seán de Búrca
</small> About All Chinese characters Auto commit mode Auto commit mode: Auto select: Auto wildcard: Behavior of space key: Cannot determine the engine name. Please use the --engine-name option. Chinese Chinese mode Chinese mode: Commit Committing with the commit keys or with the mouse
always commits to the application. This option is about
“automatic” commits which may happen when
one just continues typing input without committing
manually. From time to time, “automatic” commits will
happen then.
“Direct” means such “automatic” commits go directly
into the application, “Normal” means they get committed
to preëdit. Compose: Configure ibus-table “%(engine-name)s” Details Direct Direct input Direct input letter width: Direct input punctuation width: English Full Half Horizontal IBUS_ENGINE_NAME environment variable is not set. IBus Table %s Preferences IBus Table engine %s is not available If set to “Yes”, this does the following 4 things:
1) When typing “Return”, commit the 
   candidate + line-feed
2) When tying Tab, commit the candidate
3) When committing using a commit key, commit
   the candidate + " "
4) If typing the next character matches no candidates,
   commit the first candidate of the previous match.
   (Mostly needed for non-Chinese input methods like
   the Russian “translit”) If this is set to “single char”, only single
character candidates will be shown. If it is
set to “Phrase” candidates consisting of
several characters may be shown. If yes, a multi wildcard will be automatically
appended to the end of the input string. If you choose the space key to do “Next page”,
 you can only commit using the selection keys
(i.e. the labels in front of the candidates in the
lookup table) or using the mouse. Input mode Input mode: Letter width Multi wildcard character: Multiple character match Next Page No Normal Onechar mode Orientation: Page size: Phrase Pinyin Pinyin mode Punctuation width Restore all defaults Restore defaults as specified in the database for this engine. Settings Setup Show candidate list Simplified Chinese Simplified Chinese before traditional Simplified Chinese first Single Char Single character match Single wildchard character: Switch Chinese mode Switch Input mode Switch autocommit mode Switch letter width Switch onechar mode Switch pinyin mode Switch punctuation width Switch to Chinese input Switch to English input Switch to direct commit mode (automatic commits go directly into the application) Switch to direct input Switch to fullwidth letters Switch to fullwidth punctuation Switch to halfwidth letters Switch to halfwidth punctuation Switch to matching multiple characters at once Switch to matching only single characters Switch to normal commit mode (automatic commits go into the preedit instead of into the application. This enables automatic definitions of new shortcuts) Switch to pinyin mode Switch to table input Switch to table mode Switch to “All Chinese characters”. Switch to “Simplified Chinese before traditional”. Switch to “Simplified Chinese only”. Switch to “Traditional Chinese before simplified”. Switch to “Traditional Chinese only”. Table Table input Table input letter width: Table input method for IBus Table input punctuation width: The maximum number of candidates in
one page of the lookup table. You can switch
pages in the lookup table using the page up/down
keys or the arrow up/down keys. The wildcard to match any single character.
Type RETURN or ENTER to confirm after changing the wildcard. The wildcard used to match any number of characters.
Type RETURN or ENTER to confirm after changing the wildcard. Traditional Chinese Traditional Chinese before simplified Traditional Chinese first Vertical Whether candidate lists should be shown or hidden.
For Chinese input methods one usually wants the
candidate lists to be shown. But for some non-Chinese
input methods like the Russian “translit”, hiding the
candidate lists is better. Whether the lookup table showing the candidates
should be vertical or horizontal. Whether to use fullwidth or halfwidth
letters in direct input mode. Whether to use fullwidth or halfwidth
letters in table input mode. Whether to use fullwidth or halfwidth
punctuation in direct input mode. Whether to use fullwidth or halfwidth
punctuation in table input mode. Yes “Direct input” is almost the same as if the
input method were off, i.e. not used at all, most
characters just get passed to the application.
But some conversion between fullwidth and
halfwidth may still happen in direct input mode.
“Table input” means the input method is on. “Simplified Chinese” shows only characters 
used in simplified Chinese. “Traditional Chinese”
shows only characters used in traditional Chinese.
“Simplified Chinese before traditional” shows all
characters but puts the simplified characters higher
up in the candidate list. “Traditional Chinese before
simplified” puts the traditional characters higher up
in the candidate list. “All characters” just shows all
matches without any particular filtering on traditional
versus simplified Chinese. Project-Id-Version: ibus-table 1.4.99
Report-Msgid-Bugs-To: http://code.google.com/p/ibus/issues/entry
POT-Creation-Date: 2015-07-22 17:16+0000
PO-Revision-Date: 2015-07-23 00:01+0000
Last-Translator: Mike FABIAN <Unknown>
Language-Team: German <trans-de@lists.fedoraproject.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:38+0000
X-Generator: Launchpad (build 18115)
Language: de
 <b>Kandidatenliste</b> <b>Details</b> <b>Zustand beim Start</b> <small>
<b>Autoren:</b>
Yuwei YU (‘acevery’)
Peng Huang
BYVoid
Peng Wu

<b>Mitwirkende:</b>
koterpilla
Zerng07
Caius ‘kaio’ Chance
Mike FABIAN
Bernard Nauwelaerts
Xiaojun Ma
mozbugbox
Seán de Búrca
</small> Info über IBus Table Alle chinesischen Zeichen Autocommitmodus Autocommitmodus Automatische Auswahl Automatischer Platzhalter Verhalten der Leertaste Der Name für diesen Input-Engine kann nicht gefunden werden. Bitte benutzen Sie die --engine-name Option. Chinesisch Chinesischer Modus Chinesischer Modus: Commit Wenn man manuell mittels Commit-Tasten oder mit der
Maus committed, geht der Text immer direkt in die
Applikation. In dieser Option geht es um
“automatische” Commits, die ab und zu vorkommen wenn
man einfach immer weiter tippt ohne manuell zu
committen.
“Direkt” bedeutet, daß solche “automatischen” Commits
direkt in die Applikation committed werden, “Normal”
bedeutet, daß sie zunächst nur ins “Preedit” committed
werden, wo sie weiter editiert werden können und später
manuell committed. Komposition Konfiguration von ibus-table “%(engine-name)s” Details Direkt Direkteingabemodus Buchstabenbreite im Direkteingabemodus Zeichensetzungsbreite im Direkteingabemodus Englisch Volle Breite Halbe Breite Horizontal Die Umgebungsvariable IBUS_ENGINE_NAME ist nicht gesetzt. IBus Table %s Einstellungen IBus Table Engine %s ist nicht verfügbar Wenn diese Option auf “Ja” gesetzt ist, werden die
folgenden 4 Dinge gemacht, bei “Nein” nicht:
1) Beim tippen der “Return”-Taste wird der Kandidat
   und ein Zeilenumbruch committed.
2) Beim tippen der “Tab”-wird der Kandidat committed.
3) Beim Committen mittels einer “Commit-Taste” (meist
   Leertaste) wird der Kandidat gefolgt von “ ” committed.
4) Wenn das Tippen eines weiteren Zeichens dazu führt,
   daß es keine Kandidaten mehr gibt, wird der erste
   Kandidat der Kandidatenliste vor dem Tippen diese
   Zeichens committed (Hauptsächlich nützlich für
   nicht-chinesische Eingabemethoden, zum Beispiel die
   Russische Eingabemethode “translit”) Wenn dies auf “Einzelzeichenmodus” gesetzt wird,
werden nur Kandidaten gezeigt, die nur aus einem
einzigen Zeichen bestehen. Wenn dies auf
“Satz- bzw. Wortmodus” gesetzt wird, können auch
längere Kandidaten angezeigt werden, ganze Wörter
oder gar Sätze. Wenn diese Option auf “ja” gesetzt ist wird
automatich ein Platzhalter für beliebig viele
Zeichen ans Ende des Eingabetextes angehängt. Wenn man die Leertaste benutzt um zur nächsten Seite
in der Kandidatenliste zu blättern, kann man nur noch
mit den Auswahltasten (d.h. die Tasten, die in der
Kandidatenliste vor den Kandidaten angezeigt werden)
oder mit der Maus committen. Eingabemodus Eingabemodus Buchstabenbreite Platzhalter für beliebig viele Zeichen: Mehrere Zeichen auf einmal Nächste Seite Nein Normal Einzelzeichenmodus Ausrichtung: Seitengröße: Satzmodus Pinyin Pinyin Modus Zeichensetzungsbreite Alles auf Standardeinstellungen zurücksetzen Auf Standardeinstellungen zurücksetzen, so
wie sie in der Datenbank für diesen Inputengine
definiert sind. Einstellungen Einstellungen Kandidatenliste zeigen Vereinfachtes Chinesisch Vereinfachtes Chinesisch zuerst Vereinfachtes Chinesisch zuerst Einzelzeichenmodus Nur einzelne Zeichen Platzhalter für ein Zeichen: Chinesischen Modus ändern Eingabemodus ändern Autocommitmodus ändern Buchstabenbreite ändern Einzelzeichenmodus ändern Pinyinmodus ändern Zeichensetzungsbreite ändern Wechseln zur Eingabe von Chinesisch Wechseln zur Eingabe von Englisch Wechseln zum direkten Commitmodus (automatische Commits gehen direkt in
die Applikation) Wechseln zum “Direkteingabe Modus” Wechseln zu “Buchstaben in voller Breite” Wechseln zu “Zeichensetzung in voller Breite” Wechseln zu “Buchstaben in halber Breite” Wechseln zu “Zeichensetzung in halber Breite” Zur Eingabe mehrerer Zeichen auf einmal wechseln Zur Eingabe einzelner Zeichen wechseln Wechseln zum normallen Commitmodus (automatische Commits gehen in das Preedit
anstatt in die Applikation. Das ermöglicht automatische Definitionen neuer
Kürzel) Zum “Pinyin Modus” Modus wechseln Tabelleneingabemodus Zum “Tabellen Modus” wechseln Zu »Alle chinesischen Zeichen« wechseln Zu »Vereinfachtes Chinesisch vor traditionellem« wechseln Zu »Vereinfachtes Chinesisch« wechseln Zu »Traditionelles Chinesisch vor vereinfachtem« wechseln Zu »Traditionelles Chinesisch« wechseln Tabelleneingabe Tabelleneingabemodus Buchstabenbreite im Tabelleneingabemodus Tabellenbasierte Eingabemethode für IBus Zeichensetzungsbreite im Tabelleneingabemodus Die maximale Anzahl an Kandidaten in einer
Seite der Kandidatenliste. Man kann in der
Kandidatenliste mit den Seite hoch/runter oder
den Pfeil hoch/runter Tasten blättern. Das Platzhalterzeichen für ein beliebiges einzelnes
Zeichen. Nach Ändern dieser Option mit RETURN oder ENTER bestätigen. Das Platzhalterzeichen für beliebig viele Zeichen.
Nach Ändern dieser Option mit RETURN oder ENTER bestätigen. Traditionelles Chinesisch Traditionelles Chinesisch zuerst Traditionelles Chinesisch zuerst Vertikal Ob Kandidatenlisten gezeigt werden sollen oder nicht.
Für chinesische Inputmethoden will man Kandidatenlisten
normalerweise sehen. Aber für einige nicht-chinesische
Inputmethode, wie zum Beispiel die russische Inputmethode
“translit” ist es besser die Kandidatenlisten zu
verstecken. Ob die Kandidatenliste horizontal oder
vertikal angezeigt werden soll. Ob Buchstaben in voller oder halber
Breite im Direkteingabemodus benutzt werden
sollen. Ob Buchstaben in voller oder halber
Breite im Tabelleneingabemodus benutzt werden
sollen. Ob Zeichensetzung in voller oder halber
Breite im Direkteingabemodus benutzt
werden soll. Ob Zeichensetzung in voller oder halber
Breite im Tabelleneingabemodus benutzt
werden soll. Ja “Direkteingabemodus” ist beinahe dasselbe als ob
die Eingabemethode überhaupt nicht verwendet würde,
die meisten Zeichen werden einfach an die Applikation
durchgereicht. Aber auch im Direkteingabemodus
werden Zeichen in voller Breite eventuell in Zeichen
in halber Breite konvertiert oder umgekehrt.
“Tabelleneingabemodus” bedeutet, daß die Eingabemethode
aktiv ist. “Vereinfachtes Chinesisch” zeigt nur Zeichen, die in
vereinfachtem Chinesisch benutzt werden. “Traditionelles
Chinesisch” zeigt nur Zeichen, die in traditionellem
Chinesisch benutzt werden. “Vereinfachtes Chinesisch zuerst”
zeigt die vereinfachten Zeichen höher oben in der
Kandidatenliste. “Traditionelles Chinesisch zuerst” zeigt
die traditionellen Zeichen höher in der Kandidatenliste.
“Alle chinesischen Zeichen” zeigt alle Kandidaten ohne
daß die Reihenfolge irgendwie gefiltert wird. 