��    (      \  5   �      p     q     �     �     �     �  %   �  $     @   )     j          �     �     �     �     �     �               $     7  /   I     y     �     �     �     �     �     �     �       !     #   7  "   [  �  ~  R   T     �     �  
   �  
   �  �  �     n
     ~
     �
  "   �
     �
  3   �
  @      a   a     �     �      �           6  '   U     }     �     �      �     �     �  B   	     L     \     t     �     �     �     �     �     �  '     %   A  -   g  @  �  z   �     Q     V     h     y                	   !                                       #                      
          "                                   %                   $       &      (         '          (replace text) <b>Input/Output</b> <b>Options</b> Allow type with more _freedom Allow type with more freedom Auto _restore keys with invalid words Auto restore keys with invalid words Auto send PreEdit string to Application when mouse move or click Capture _mouse event Capture mouse event Choose file to export Choose file to import Choose input method Choose output charset Delete _all Enable Macro Enable _macro Enable _spell check Enable spell check IBus-Unikey Setup If enable, you can decrease mistake when typing Input method: Macro table definition Options Options for Unikey Output charset:  Process W at word begin Process _W at word begin Replace with Run Setup... Run setup utility for IBus-Unikey Use oà, _uý (instead of òa, úy) Use oà, uý (instead of òa, úy) Vietnamese Input Method Engine for IBus using Unikey Engine
Usage:
  - Choose input method, output charset, options in language bar.
  - There are 4 input methods: Telex, Vni, STelex (simple telex) and STelex2 (which same as STelex, the difference is it use w as ư).
  - And 7 output charsets: Unicode (UTF-8), TCVN3, VNI Win, VIQR, CString, NCR Decimal and NCR Hex.
  - Use <Shift>+<Space> or <Shift>+<Shift> to restore keystrokes.
  - Use <Control> to commit a word. When typing a word not in Vietnamese,
it will auto restore keystroke into original Word _Edit macro _Export... _Import... Project-Id-Version: ibus-unikey
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-06-11 17:18+0700
PO-Revision-Date: 2012-02-22 11:32+0000
Last-Translator: Hendrik Knackstedt <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:44+0000
X-Generator: Launchpad (build 18115)
 (Text ersetzen) <b>Eingabe/Ausgabe</b> <b>Einstellungen</b> Tippen mit mehr _Freiheit erlauben Typ mit mehr Freiheit erlauben Schlüssel mit ungültigen Worten _wiederherstellen Schlüssel mit ungültigen Wörtern automatisch wiederherstellen Automatisch PreEdit-Zeichenkette an die Anwendung senden, wenn die Maus bewegt oder geklickt wird _Mausereignis einfangen Mausbewegung aufzeichnen Datei zum Exportieren auswählen Datei zum Importieren auswählen Wählen Sie die Eingabemethode Wählen Sie den Zeichensatz der Ausgabe _Alles löschen Makro aktivieren _Makro aktivieren _Rechtschreibprüfung aktivieren Rechtschreibprüfung aktivieren IBus-Unikey-Einrichtung Wenn Sie diese Option aktivieren, können Sie Tippfehler vermeiden Eingabemethode: Makrotabellendefinition Einstellungen Einstellungen für Unikey Zeichensatz der Ausgabe:  W am Wortanfang verarbeiten _W am Wortanfang verarbeiten Ersetzen durch Einrichtung ausführen … Einrichtung für IBus-Unikey ausführen oà, _uý benutzen (anstatt òa, úy) Benutzen Sie oà, uý (anstelle von òa, úy) Vietnamesische Eingabemethode für IBus mittels Unikey-Engine
Benutzung:
  - Wählen Sie die Eingabemethode, Zeichensatz der Ausgabe und die Optionen in der Sprachleiste.
  - Es gibt die 4 Eingabemethoden Telex, Vni, STelex (einfaches telex) und STelex2 (wie STelex, es benutzt aber w als ư).
  - Es gibt die 7 Zeichensätze für die Ausgabe: Unicode (UTF-8), TCVN3, VNI Win, VIQR, CString, NCR Decimal und NCR Hex.
  - Drücken Sie <Umschalt>+<Leertaste> oder <Umschalt>+<Umschalt>, um Tastenanschläge wiederherzustellen.
  - Benutzen Sie <Strg>, um ein Wort zu übergeben. Wenn ein Wort eingegeben wird,
das nicht vietnamesisch ist, wird automatisch
die ursprüngliche Eingabe wiederhergestellt. Wort Makro _bearbeiten _Exportieren … _Importieren … 