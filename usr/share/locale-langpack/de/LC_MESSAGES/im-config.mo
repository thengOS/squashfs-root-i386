��    G      T  a   �        �     t   �       �   2  n   �  "  D  e   g
  X   �
  O   &     v    �    �     �  J   �  �     Q   �  B   �  K   9  ;   �     �     �  x   �  �  v  	  �  �     9   �  �   �  ?   f  {  �  D   "  X   g  F  �            -   +  :   Y  R   �  N   �    6  {   T  �   �  L   l  }   �  <   7  s   t  $   �  0      (   >   %   g   (   �   %   �   &   �   )   !  ,   -!  '   Z!  &   �!  3   �!  %   �!     "      "     0"  6   5"     l"     s"  �  �"  F   R$  >   �$  :   �$     %     1%  �  D%  �   �&  �   �'     
(  �   )(  q   )  �  x)  g   �+  g   a,  c   �,     --    C-  f  b.      �0  g   �0  �   R1  _   2  K   l2  U   �2  D   3  !   S3      u3  �   �3  �   4  "  �5  �   �6  ;   �7  �   �7  E   o8  �  �8  K   u;  ^   �;  N   <     o=     w=  6   �=  ;   �=  X   >  S   ^>  S  �>  �   A  �   �A  ]   9B  z   �B  A   C  x   TC  ,   �C  6   �C  2   1D  -   dD  7   �D  *   �D  ,   �D  -   "E  /   PE  ,   �E  )   �E  8   �E  +   F     <F      IF     jF  E   oF     �F     �F  �  �F  O   �H  F   I  A   ^I  %   �I     �I     .   )   A             6             #             :          =   &   @   D   ;   	      /      3       !       1          +       2                                          F   E   B      (      ,      %          7   >   $         C              G          0         "                         ?   
      '      5       *   <       -      8   4   9          
The $IM_CONFIG_XINPUTRC_TYPE is modified by im-config.

Restart the X session to activate the new $IM_CONFIG_XINPUTRC_TYPE.
$IM_CONFIG_RTFM $IM_CONFIG_ID
(c) Osamu Aoki <osamu@debian.org>, GPL-2+
See im-config(8), /usr/share/doc/im-config/README.Debian.gz. $IM_CONFIG_MSG
$IM_CONFIG_MSGA $IM_CONFIG_MSG
$IM_CONFIG_MSGA
  Available input methods:$IM_CONFIG_AVAIL
Unless you really need them all, please make sure to install only one input method tool. $IM_CONFIG_MSG
Automatic configuration selects: $IM_CONFIG_AUTOMATIC
$IM_CONFIG_AUTOMATIC_LONG
$IM_CONFIG_RTFM $IM_CONFIG_MSG
In order to enter non-ASCII native characters, you must install one set of input method tools:
 * ibus and its assocoated packages (recommended)
   * multilingual support
   * GUI configuration
 * fcitx and its assocoated packages
   * multilingual support with focus on Chinese
   * GUI configuration
 * uim and its assocoated packages
   * multilingual support
   * manual configuration with the Scheme code
   * text terminal support even under non-X environments
 * any set of packages which depend on im-config
$IM_CONFIG_MSGA $IM_CONFIG_MSG
Manual configuration selects: $IM_CONFIG_ACTIVE
$IM_CONFIG_ACTIVE_LONG
$IM_CONFIG_RTFM $IM_CONFIG_RTFM
See im-config(8) and /usr/share/doc/im-config/README.Debian.gz for more. *** This is merely a simulated run and no changes are made. ***

$IM_CONFIG_MSG Bogus Configuration Chinese input method (gcin)
 * Required for all: gcin
 * Language specific input conversion support:
  * Traditional Chinese: gcin-chewing
  * Japanese: gcin-anthy
 * Application platform support:
  * GNOME/GTK+: gcin-gtk3-immodule
  * KDE/Qt: gcin-qt4-immodule Current configuration for the input method:
 * Active configuration: $IM_CONFIG_ACTIVE (normally missing)
 * Normal automatic choice: $IM_CONFIG_AUTOBASE (normally ibus or fcitx or uim)
 * Override rule: $IM_CONFIG_PREFERRED_RULE
 * Current override choice: $IM_CONFIG_PREFERRED ($IM_CONFIG_LC_CTYPE)
 * Current automatic choice: $IM_CONFIG_AUTOMATIC
 * Number of valid choices: $IM_CONFIG_NUMBER (normally 1)
The override rule is defined in /etc/default/im-config.
The configuration set by im-config is activated by re-starting X. Custom Configuration Custom configuration is created by the user or administrator using editor. Do you explicitly select the ${IM_CONFIG_XINPUTRC_TYPE}?

 * Select NO, if you do not wish to update it. (recommended)
 * Select YES, if you wish to update it. E: $IM_CONFIG_NAME is bogus configuration for $IM_CONFIG_XINPUTRC. Doing nothing. E: Configuration for $IM_CONFIG_NAME not found at $IM_CONFIG_CODE. E: Configuration in $IM_CONFIG_XINPUTRC is manually managed. Doing nothing. E: Script for $IM_CONFIG_NAME not found at $IM_CONFIG_CODE. E: X server must be available. E: zenity must be installed. Explicit selection is not required to enable the automatic configuration if the active one is default/auto/cjkv/missing. Flexible Input Method Framework (fcitx)
 * Required for all: fcitx
 * Language specific input conversion support:
   * Simplified Chinese: fcitx-pinyin or fcitx-sunpinyin or fcitx-googlepinyin
   * Generic keyboard translation table: fcitx-table* packages
 * Application platform support:
   * GNOME/GTK+: fcitx-frontend-gtk2 and fcitx-frontend-gtk3 (both)
   * KDE/Qt4: fcitx-frontend-qt4 HIME Input Method Editor (hime)
 * Required for all: hime
 * Language specific input conversion support:
  * Traditional Chinese: hime-chewing
  * Japanese: hime-anthy
 * Application platform support:
  * GNOME/GTK+: hime-gtk3-immodule
  * KDE/Qt: hime-qt4-immodule Hangul (Korean) input method
 * XIM: nabi
 * GNOME/GTK+: imhangul-gtk2 and imhangul-gtk3
 * KDE/Qt4: qimhangul-qt4
 * GUI companion: imhangul-status-applet I: Script for $IM_CONFIG_NAME started at $IM_CONFIG_CODE. If a daemon program for the previous configuration is re-started by the X session manager, you may need to kill it manually with kill(1). Input Method Configuration (im-config, ver. $IM_CONFIG_VERSION) Intelligent Input Bus (IBus)
 * Required for all: ibus
 * Language specific input conversion support:
   * Japanese: ibus-mozc (best) or ibus-anthy or ibus-skk
   * Korean: ibus-hangul
   * Simplified Chinese: ibus-pinyin or ibus-sunpinyin or ibus-googlepinyin
   * Traditional Chinese: ibus-chewing
   * Thai: ibus-table-thai
   * Vietnamese: ibus-unikey or ibus-table-viqr
   * X Keyboard emulation: ibus-xkbc
   * Generic keyboard translation table: ibus-m17n or ibus-table* packages
 * Application platform support:
   * GNOME/GTK+: ibus-gtk and ibus-gtk3 (both)
   * KDE/Qt: ibus-qt4
   * Clutter: ibus-clutter
   * EMACS: ibus-el Keeping the $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC as missing. Keeping the $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC unchanged as $IM_CONFIG_ACTIVE. Mallit input method framework
 * Required for everything: maliit-framework
 * Keyboards part of (maliit-plugins):
   * reference keyboard: maliit-keyboard
   * QML keyboard: nemo-keyboard
 * Application platform support:
   * GTK2: maliit-inputcontext-gtk2
   * GTK3: maliit-inputcontext-gtk3
   * Qt4: maliit-inputcontext-qt4 Missing Missing configuration file. Non existing configuration name is specified. Removing the $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC. Select $IM_CONFIG_XINPUTRC_TYPE. The user configuration supersedes the system one. Setting the $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC to $IM_CONFIG_ACTIVE. Smart Common Input Method (SCIM)
 * Required for all: scim
 * Language specific input conversion support:
   * Japanese: scim-mozc (best) or scim-anthy or scim-skk
   * Korean: scim-hangul
   * Simplified Chinese: scim-pinyin or scim-sunpinyin
   * Traditional Chinese: scim-chewing
   * Thai: scim-thai
   * Vietnamese: scim-unikey
   * Generic keyboard translation table: scim-m17 or scim-table* packages
 * Application platform support:
   * GNOME/GTK+: scim-gtk-immodule
   * KDE/Qt4: scim-qt-immodule
   * Clutter: scim-clutter-immodule Thai input method with thai-libthai
 * GNOME/GTK+: gtk-im-libthai and gtk3-im-libthai
 * No XIM nor KDE/Qt4 support (FIXME) The $IM_CONFIG_XINPUTRC_TYPE has been manually modified.
Remove the $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC manually to use im-config.
$IM_CONFIG_RTFM This activates the bare XIM with the X Keyboard Extension for all softwares. This does not set any IM from im-config.
This is the automatic configuration choice if no required IM packages are installed. X input method for Chinese with Sunpinyin
 * XIM: xsunpinyin X input method for Japanese with kinput2
 * XIM: one of kinput2-* packages
 * kanji conversion server: canna or wnn activate Chinese input method (gcin) activate Flexible Input Method Framework (fcitx) activate HIME Input Method Editor (hime) activate Hangul (Korean) input method activate IM with @-mark for most locales activate Intelligent Input Bus (IBus) activate Mallit input method framework activate Smart Common Input Method (SCIM) activate Thai input method with thai-libthai activate XIM for Chinese with Sunpinyin activate XIM for Japanese with kinput2 activate the bare XIM with the X Keyboard Extension activate universal input method (uim) description do not set any IM from im-config name remove IM $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC select system configuration universal input method (uim)
 * Required for all: uim
 * Language specific input conversion support:
   * Japanese: uim-mozc (best) or uim-anthy or uim-skk
   * Korean: uim-byeoru
   * Simplified Chinese: uim-pinyin
   * Traditional Chinese: uim-chewing
   * Vietnamese: uim-viqr
   * General-purpose M17n: uim-m17nlib
 * Application platform support:
   * XIM: uim-xim
   * GNOME/GTK+: uim-gtk2.0 and uim-gtk3 (both)
   * KDE/Qt4: uim-qt
   * EMACS: uim-el use $IM_CONFIG_DEFAULT_MODE mode (bogus content in $IM_CONFIG_DEFAULT) use $IM_CONFIG_DEFAULT_MODE mode (missing $IM_CONFIG_DEFAULT ) use $IM_CONFIG_DEFAULT_MODE mode set by $IM_CONFIG_DEFAULT use auto mode only under CJKV user configuration Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-05-26 23:27+0000
PO-Revision-Date: 2016-05-27 10:14+0000
Last-Translator: Hendrik Knackstedt <Unknown>
Language-Team: Ubuntu German Translators
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:56+0000
X-Generator: Launchpad (build 18115)
Language: de
 
$IM_CONFIG_XINPUTRC_TYPE wird von im-config bearbeitet.

Starten Sie die X-Sitzung neu, um die neue $IM_CONFIG_XINPUTRC_TYPE zu aktivieren.
$IM_CONFIG_RTFM $IM_CONFIG_ID
(c) Osamu Aoki <osamu@debian.org>, GPL-2+
Weitere Informationen unter im-config(8), /usr/share/doc/im-config/README.Debian. $IM_CONFIG_MSG
$IM_CONFIG_MSGA $IM_CONFIG_MSG
$IM_CONFIG_MSGA
  Verfügbare Eingabemethoden:$IM_CONFIG_AVAIL
Wenn Sie nicht wirklich alle Eingabemethoden benötigen, stellen Sie sicher, dass Sie nur das benötigte Eingabemethodenwerkzeug installieren. $IM_CONFIG_MSG
Automatische Konfigurationsauswahl: $IM_CONFIG_AUTOMATIC
$IM_CONFIG_AUTOMATIC_LONG
$IM_CONFIG_RTFM $IM_CONFIG_MSG
Um nicht-ASCII-Zeichen einzugeben, müssen Sie einen Satz an Werkzeugen für eine Eingabemethode installieren:
 * ibus und die zugehörigen Pakete (empfohlen)
   * Mehrsprachenunterstützung
   * Konfiguration über grafische Oberfläche
 * fcitx und die zugehörigen Pakete
   * Mehrsprachenunterstützung mit Fokus auf Chinesisch
   * Konfiguration über grafische Oberfläche
 * uim und die zugehörigen Pakete
   * Mehrsprachenunterstützung
   * Manuelle Konfiguration mit Schemen-Code
   * Text-Terminal-Unterstützung selbst in nicht-X-Umgebungen
 * irgendeine Paketsammlung, die von im-config abhängt
$IM_CONFIG_MSGA $IM_CONFIG_MSG
Manuelle Konfigurationsauswahl: $IM_CONFIG_ACTIVE
$IM_CONFIG_ACTIVE_LONG
$IM_CONFIG_RTFM $IM_CONFIG_RTFM
Weitere Informationen unter im-config(8) und /usr/share/doc/im-config/README.Debian.gz. *** Dies ist nur eine Simulation und es werden keine Änderungen durchgeführt. ***

$IM_CONFIG_MSG Falsche Konfiguration Chinese Eingabemethode (gcin)
 * Benötigt für alle: gcin
 * Sprachspezifische Eingabeumwandlungsunterstützung:
  * Traditionelles Chinesisch: gcin-chewing
  * Japanisch: gcin-anthy
 * Anwendungsplattformunterstützung:
  * GNOME/GTK+: gcin-gtk3-immodule
  * KDE/Qt: gcin-qt4-immodule Derzeitige Konfiguration der Eingabemethode:
 * Aktive Konfiguration: $IM_CONFIG_ACTIVE (fehlt üblicherweise)
 * Normale automatische Auswahl: $IM_CONFIG_AUTOBASE (üblicherweise ibus, fcitx oder uim)
 * Überschreibungsregel: $IM_CONFIG_PREFERRED_RULE
 * Derzeitige Überschreibungsauswahl: $IM_CONFIG_PREFERRED ($IM_CONFIG_LC_CTYPE)
 * Derzeitige automatische Auswahl: $IM_CONFIG_AUTOMATIC
 * Anzahl gültiger Auswahlen: $IM_CONFIG_NUMBER (üblicherweise 1)
Die  Überschreibungsregel ist festgelegt in /etc/default/im-config.
Die Konfigurationseinstellung mittels im-config wird durch Neustart von X aktiviert. Benutzerdefinierte Konfiguration Eine benutzerdefinierte Konfiguration wird vom Benutzer oder Systemverwalter mit einem Editor erstellt. Wählen Sie ${IM_CONFIG_XINPUTRC_TYPE} explizit aus?

 * Wählen Sie NEIN, falls Sie es nicht aktualisieren möchten. (empfohlen)
 * Wählen Sie JA, falls Sie es aktualisieren möchten. E: $IM_CONFIG_NAME ist eine Scheinkonfiguration für $IM_CONFIG_XINPUTRC. Es wird nichts getan. E: Konfiguration für $IM_CONFIG_NAME unter $IM_CONFIG_CODE nicht gefunden. E: Konfiguration in $IM_CONFIG_XINPUTRC wird manuell verwaltet. Es wird nichts getan. E: Skript für $IM_CONFIG_NAME unter $IM_CONFIG_CODE nicht gefunden. E: X-Server muss verfügbar sein. E: Zenity muss installiert sein. Explizite Auswahl ist nicht erforderlich, um die automatische Konfiguration zu aktivieren, falls default/auto/cjkv/missing aktiviert ist. Flexibles Eingabemethodenrahmenwerk (fcitx)
 * Immer erforderlich: fcitx
 * Sprachspezifische Unterstützung der Eingabemethode:
   * Vereinfachtes Chinesisch: fcitx-pinyin oder fcitx-sunpinyin oder fcitx-googlepinyin
   * Generische Tastaturübersetzungstabelle: fcitx-table*-Pakete
 * Anwendungsplattformunterstützung:
   * GNOME/GTK+: fcitx-frontend-gtk2 und fcitx-frontend-gtk3 (beide)
   * KDE/Qt4: fcitx-frontend-qt4 HIME Eingabemethodeneditor (hime)
 * Benötigt für alle: hime
 * Sprachspezifische Eingabeumwandlungsunterstützung:
  * Traditionelles Chinesisch: hime-chewing
  * Japanisch: hime-anthy
 * Anwendungsplattformunterstützung:
  * GNOME/GTK+: hime-gtk3-immodule
  * KDE/Qt: hime-qt4-immodule Hangul-Eingabemethode (Koreanisch)
 * XIM: nabi
 * GNOME/GTK+: imhangul-gtk2 und imhangul-gtk3
 * KDE/Qt4: qimhangul-qt4
 * GUI companion: imhangul-status-applet I: Script für $IM_CONFIG_NAME beginnt bei $IM_CONFIG_CODE. Falls ein Dienstprogramm (Daemon) für die vorherige Konfiguration von der X-Sitzungsverwaltung neu gestartet wird, müssen Sie es mit kill(1) evtl. manuell beenden. Konfiguration der Eingabemethode (im-config, ver. $IM_CONFIG_VERSION) Intelligenter Eingabebus (IBus)
 * Immer erforderlich: ibus
 * Sprachspezifische Unterstützung der Eingabemethode:
   * Japanisch: ibus-mozc (empfohlen) oder ibus-anthy oder ibus-skk
   * Koreanisch: ibus-hangul
   * Vereinfachtes Chinesisch: ibus-pinyin oder ibus-sunpinyin oder ibus-googlepinyin
   * Traditionelles Chinesisch: ibus-chewing
   * Thailändisch: ibus-table-thai
   * Vietnamesisch: ibus-unikey oder ibus-table-viqr
   * X-Tastaturemulation: ibus-xkbc
   * Generische Tastaturübersetzungstabelle: ibus-m17n oder ibus-table*-Pakete
 * Anwendungsplattformunterstützung:
   * GNOME/GTK+: ibus-gtk und ibus-gtk3 (beide)
   * KDE/Qt: ibus-qt4
   * Clutter: ibus-clutter
   * EMACS: ibus-el $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC wird als »fehlend« behalten. $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC wird unverändert als $IM_CONFIG_ACTIVE behalten. Mallit Eingabemethoden Rahmenwerk
 * Erforderlich für alles: maliit-framework
 * Tastatur Teil von (maliit-plugins):
   * Referenz Tastatur: maliit-keyboard
   * QML Tastatur: nemo-keyboard
 * Anwendungsplattformunterstützung:
   * GTK2: maliit-inputcontext-gtk2
   * GTK3: maliit-inputcontext-gtk3
   * Qt4: maliit-inputcontext-qt4 Fehlend Konfigurationsdatei fehlt. Kein existierender Konfigurationsname wurde angegeben. $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC wird entfernt. Wählen Sie $IM_CONFIG_XINPUTRC_TYPE. Die Benutzerkonfiguration ersetzt die des Systems. $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC wird als $IM_CONFIG_ACTIVE festgelegt. Intelligente gemeinsame Eingabemethode (SCIM)
 * Erforderlich für alle: scim
 * Sprachspezifische Eingabeumwandlungsunterstützung:
   * Japanisch: scim-mozc (best) or scim-anthy or scim-skk
   * Koreanisch: scim-hangul
   * Vereinfachtes Chinesisch: scim-pinyin or scim-sunpinyin
   * Traditionelles Chinesisch: scim-chewing
   * Thai: scim-thai
   * Vietnamese: scim-unikey
   * Allgemeine Tastaturübersetzungstabelle: scim-m17 or scim-table* packages
 * Anwendungsplattformunterstützung:
   * GNOME/GTK+: scim-gtk-immodule
   * KDE/Qt4: scim-qt-immodule
   * Clutter: scim-clutter-immodule Thai-Eingabemethode mit thai-libthai
 * GNOME/GTK+: gtk-im-libthai und gtk3-im-libthai
 * Keine Unterstützung für XIM oder KDE/Qt4 (FIXME) $IM_CONFIG_XINPUTRC_TYPE wurde manuell bearbeitet.
Entfernen Sie den $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC manuell, um im-config zu verwenden.
$IM_CONFIG_RTFM Hierdurch wird die bloße XIM mit der X-Tastaturerweiterung für jede Software eingeschaltet. Dadurch wird keine IM aus im-config festgelegt.
Dies ist die automatische Auswahl, falls keine IM-Pakete installiert sind. X-Eingabemethode für Chinesisch mit Sunpinyin
 * XIM: xsunpinyin X-Eingabemethode für Japanisch mit kinput2
 * XIM: eins der kinput2-*-Pakete
 * kanji conversion server: canna oder wnn Chinesische Eingabemethode (gcin) aktivieren Flexibles Eingabemethodenrahmenwerk (fcitx) aktivieren HIME-Eingabemethodenbearbeitung (hime) einschalten Hangul-Eingabemethode (Koreanisch) aktivieren Aktivieren Sie IM mit @-Zeichen in den meisten Sprachen Intelligenten Eingabebus (IBus) aktivieren Mallit Eingabemethoden Rahmenwerk aktivieren Smart Common-Eingabemethode (SCIM) aktivieren Thai-Eingabemethode mit thai-libthai aktivieren XIM für Chinesisch mit Sunpinyin aktivieren XIM für Japanisch mit kinput2 aktivieren Die bloße XIM mit der X-Tastaturerweiterung einschalten Universelle Eingabemethode (uim) aktivieren Beschreibung Keine IM aus im-config festlegen Name Eingabemethode $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC entfernen Auswahl Systemkonfiguration Universelle Eingabemethode (uim)
 * Immer erforderlich: uim
 * Sprachspezifische Unterstützung der Eingabemethode:
   * Japanisch: uim-mozc (empfohlen) oder uim-anthy oder uim-skk
   * Koreanisch: uim-byeoru
   * Vereinfachtes Chinesisch: uim-pinyin
   * Traditionelles Chinesisch: uim-chewing
   * Vietnamesisch: uim-viqr
   * Allgemein M17n: uim-m17nlib
 * Anwendungsplattformunterstützung:
   * XIM: uim-xim
   * GNOME/GTK+: uim-gtk2.0 und uim-gtk3 (beide)
   * KDE/Qt4: uim-qt
   * EMACS: uim-el $IM_CONFIG_DEFAULT_MODE-Modus verwenden (falscher Inhalt in $IM_CONFIG_DEFAULT) $IM_CONFIG_DEFAULT_MODE-Modus verwenden (fehlende $IM_CONFIG_DEFAULT ) $IM_CONFIG_DEFAULT_MODE-Modus festgelegt durch $IM_CONFIG_DEFAULT Automatikmodus nur mit CJKV verwenden Benutzerkonfiguration 