��          �      \      �  ^   �  (   0     Y     ^  &   e  `   �  L   �     :  :   C  #   ~  g   �  4   
  S   ?     �     �     �     �  8   �  2   '  �  Z  f      (   �     �     �  -   �  h   �  d   T     �  Q   �  /   	  s   D	  >   �	  e   �	  (   ]
     �
     �
     �
  E   �
  >                               	                                                             
          Common name for the supplied codes. This may be the same as --name (only applies to ISO 3166). Copyright © 2007-2014 Tobias Quathamer
 FILE LOCALE Name for the supplied codes (default). Official name for the supplied codes. This may be the same as --name (only applies to ISO 3166). Run "isoquery --help" to see a full list of available command line options.
 STANDARD Separate entries with a NULL character instead of newline. Show program version and copyright. The ISO standard to use. Possible values: 639, 639-3, 639-5, 3166, 3166-2, 4217, 15924 (default: 3166). Translation to LANGUAGE Copyright © YEAR YOUR-NAME
 Use another XML file with ISO data (default: /usr/share/xml/iso-codes/iso_3166.xml) Use this locale for output. [ISO codes] isoquery %(version)s
 isoquery: %(error_message)s
 isoquery: ISO standard "%(standard)s" is not supported.
 isoquery: Internal error. Please report this bug.
 Project-Id-Version: isoquery
Report-Msgid-Bugs-To: toddy@debian.org
POT-Creation-Date: 2014-05-08 10:15+0200
PO-Revision-Date: 2016-01-28 05:55+0000
Last-Translator: Dr. Tobias Quathamer <toddy@debian.org>
Language-Team: German <debian-l10n-german@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:32+0000
X-Generator: Launchpad (build 18115)
Language: de
 Üblicher Name der übergebenen Codes. Dies kann das Gleiche sein wie --name (gilt nur für ISO 3166). Copyright © 2007-2014 Tobias Quathamer
 DATEI LOCALE Name der übergebenen Codes (Voreinstellung). Offizieller Name der übergebenen Codes. Dies kann das Gleiche sein wie --name (gilt nur für ISO 3166). Rufen Sie »isoquery --help« auf, um eine Liste aller Befehlszeilenoptionen angezeigt zu bekommen.
 STANDARD Einträge durch ein NULL-Zeichen anstatt des Zeilenumbruches voneinander trennen. Anzeige der Programmversion und des Copyrights. Der gewünschte ISO-Standard. Mögliche Werte: 639, 639-3, 639-5, 3166, 3166-2, 4217, 15924 (Voreinstellung: 3166). Deutsche Übersetzung Copyright © 2007-2014 Tobias Quathamer
 Eine andere XML-Datei mit ISO-Daten verwenden (Voreinstellung: /usr/share/xml/iso-codes/iso_3166.xml) Diese Locale für die Ausgabe verwenden. [ISO-Codes] isoquery %(version)s
 isoquery: %(error_message)s
 isoquery: Der ISO-Standard »%(standard)s« wird nicht unterstützt.
 isoquery: Interner Fehler. Bitte berichten Sie diesen Fehler.
 