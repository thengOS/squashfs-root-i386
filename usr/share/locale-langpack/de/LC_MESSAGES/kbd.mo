��    /     �	  �        p  V   q      �     �  6     5   ;  )   q  H  �  
   �  2   �     "     0  �   <          ,  E   F     �     �     �     �     �  P        i  *   �  0   �     �  &   �  !      9   A   T   {   #   �   !   �   2   !     I!  $   e!     �!     �!     �!     �!  &   �!     "  $   6"  #   ["  /   "  %   �"  ,   �"  $   #      '#  !   H#  &   j#  �  �#  H   C%     �%     �%  .   �%      �%     &     !&     :&     X&     x&     �&  g   �&  g   '     t'     �'     �'  !   �'  $   �'  <   (  	   C(     M(     e(     {(     �(     �(     �(  9   �(     )     )     4)     J)     d)     ~)     �)     �)     �)     �)     �)     *     &*     >*  #   ]*  7   �*  8   �*     �*     +     +     2+     K+     d+  .   }+     �+  G   �+  1   ,  <   8,  8   u,  !   �,     �,     �,     -  4   -  0   S-  +   �-      �-     �-     �-     .     $.     9.  5   P.     �.  K   �.  '   �.  &   /     :/     Y/     p/     �/     �/      �/  -   �/     0  (   '0     P0  !   \0  6   ~0  /   �0  )   �0  !   1     11     L1     j1     �1     �1  !   �1  =   �1     2     &2     =2  !   T2     v2  /   �2     �2  %   �2     3  $   3     A3     [3     v3     �3  .   �3     �3  3   �3     4  -   :4  ;   h4  (   �4  ,   �4  '   �4  %   "5  ,   H5  |   u5  (   �5     6  &   66  /   ]6     �6     �6     �6  &   �6  G   �6     @7     ^7  "   |7     �7  /  �7  �   �9  �   �:  �  �;  	  H>  .   R?  �  �?  :   [D     �D     �D  =   �D  :   �D  9   :E  B   tE  f   �E  ,   F     KF  !   kF     �F     �F  "   �F     �F     G     !G     9G     QG  #   iG  '   �G      �G     �G     �G  !   H     )H     =H     UH  (   gH     �H  
   �H     �H     �H     �H     �H     �H     �H  !   I      -I  "   NI  *   qI     �I  (   �I  -   �I  "   J  (   1J  /   ZJ     �J     �J     �J     �J  &   �J     K     K     5K     EK  4   bK     �K      �K      �K  h  �K  +   GN  #   sN  "   �N  4   �N     �N  ,   �N  0   +O  4   \O  "   �O     �O     �O     �O     �O  >   �O  ,   P     =P  �   EP  *   �P  (   Q  K   0Q  $   |Q  O   �Q     �Q    R     $S  $   7S     \S     cS     sS     {S     �S  !   �S     �S  $   �S     T  .   +T     ZT     oT     �T  
   �T  &   �T     �T     �T  *   �T     U  �   6U  G  �U     W  7   W  �  QW  Z   1Y  $   �Y     �Y  8   �Y  0   Z  )   8Z  a  bZ     �\  >   �\     ]  
   %]  �   0]  (   ^     B^  <   `^  %   �^  #   �^  %   �^     _  #   +_  U   O_      �_  3   �_  3   �_     .`  '   G`  ,   o`  G   �`  W   �`  -   <a  *   ja  D   �a  $   �a  4   �a  %   4b  +   Zb     �b     �b  1   �b     �b  +   	c  *   5c  D   `c  /   �c  :   �c  /   d  -   @d  ,   nd     �d    �d  h   �f      1g  $   Rg  H   wg  6   �g      �g     h      4h  0   Uh     �h     �h  �   �h  �   @i     �i  !   �i     �i  $   
j  (   /j  G   Xj     �j  !   �j     �j     �j     k     k     =k  G   [k     �k     �k     �k  %   �k  -   l     Il      gl  "   �l  #   �l  $   �l  -   �l     "m     5m  %   Qm  8   wm  B   �m  D   �m     8n     Pn  !   on     �n     �n  "   �n  :   �n     'o  I   Bo  2   �o  T   �o  ^   p  2   sp     �p  0   �p  -   �p  /   #q  4   Sq  '   �q  &   �q  #   �q     �q     r     .r     Ar  <   [r     �r  V   �r  ,   	s  +   6s  '   bs     �s     �s     �s     �s     t  )   t     It  (   at  	   �t     �t  *   �t  %   �t     �t  &   u  (   @u  8   iu     �u     �u     �u  @   �u  9   ;v     uv     �v     �v  '   �v  %   �v  ?   w  )   [w  (   �w     �w  -   �w  %   �w  %   x     =x     Kx  <   hx  )   �x  4   �x  "   y  8   'y  G   `y  +   �y  2   �y  (   z  !   0z  4   Rz  �   �z  ,   {  %   9{  C   _{  I   �{     �{     |      !|  1   B|  f   t|  "   �|  .   �|  9   -}  *   g}  z  �}    �  �   $�  �  �  K  
�  :   V�  v  ��  Z   �     c�     q�  J   ��  H   ڌ  J   #�  P   n�  v   ��  8   6�  +   o�  0   ��  -   ̎  +   ��  /   &�     V�     o�     ��     ��     ��  6   ԏ  ?   �  9   K�     ��  $   ��  #   Ȑ      �     �     ,�  A   B�     ��  
   ��     ��     ��  	   ő     ϑ     ב      ��  0   �  .   I�  0   x�  7   ��  .   �  ;   �  R   L�  D   ��  D   �  I   )�  #   s�     ��  &   ��     ה  /   �     �  "   �     A�  !   T�  <   v�     ��  ,   ��  .   �  �  �  ?   �  2   /�  9   b�  ;   ��     ؙ  =   �  9   (�  <   b�  $   ��     Ě     Ț     ښ  	   ޚ  L   �  4   5�     j�  �   v�  7   :�  8   r�  V   ��  +   �  _   .�  "   ��  2  ��     �  /   �     2�     9�     H�     P�     `�  '   |�     ��  +   ğ  5   �  D   &�     k�     ��     ��     ��  7   ͠     �     �  /   2�     b�  �   ~�  �  �     ��  6   ٣         �   X   v   �              �   �   .         �   �   �      /  �       �   �   &  �       m   t               0   u           <   �             �   +   l       =          �   ^   �   �   o   �   %  �       �                         �   �   �   E     y   D   }   .   �       �   $       �   �   9   �         �   b   _   �         �       B           p   r               N   �   Y     ,      g       �         #   �       �   h   �      �   6             �   2   �   M   �   �       #                           �           �   4   *  �      �   a   �      �   *          �   �   k       e   �       |   �   �   �   �   �   f       %      i       �   �           Q   �       �   �       G   �   �      x   �                 �   	               �   �       �   $  �       (  \      �                               �   �             �   �   �      �   �   �                            !   �   C       L       �   -   [   �      �         �       z       3   �     ]   
              �     �     �   �   �   H   �   �   �   )  w   5      /     �   �   �   ;   '  �   q   �       �       �   P   �   �      �   A   +  -      �   	     "  �       �   :   ,       �   S       �           �           ~   �   U   �   �      )   �   s   @           �   �   j         �   >       T   !  J   F       R   7   �       I   
  �   �   {               n   �   `   K               "   �       O   �   c   �   �       �         ?     (         1   �           W          �             �     d   &   �   V       �         �   8   '       Z    			    interpret character action codes to be from the
			    specified character set
 

Escaped scancodes e0 xx (hex)
 
Changed %d %s and %d %s.
 
Press any keys - Ctrl-D will terminate this program

 
Recognized modifier names and their column numbers:
 
The following synonyms are recognized:

 
usage: dumpkeys [options...]

valid options are:

	-h --help	    display this help text
	-i --short-info	    display information about keyboard driver
	-l --long-info	    display above and symbols known to loadkeys
	-n --numeric	    display keytable in hexadecimal notation
	-f --full-table	    don't use short-hand notations, one row per keycode
	-1 --separate-lines one line per (modifier,keycode) pair
	   --funcs-only	    display only the function key strings
	   --keys-only	    display only key bindings
	   --compose-only   display only compose key combinations
	-c --charset=     FAILED # not alt_is_meta: on keymap %d key %d is bound to %-15s for %s
 %s from %s
 %s version %s

Usage: %s [options]

Valid options are:

	-h --help            display this help text
	-V --version         display program version
	-n --next-available  display number of next unallocated VT
 %s: %s: Warning: line too long
 %s: 0: illegal VT number
 %s: Bad Unicode range corresponding to font position range 0x%x-0x%x
 %s: Bad call of readpsffont
 %s: Bad end of range (0x%lx)
 %s: Bad end of range (0x%x)
 %s: Bad input line: %s
 %s: Bad magic number on %s
 %s: Corresponding to a range of font positions, there should be a Unicode range
 %s: Error reading input font %s: Glyph number (0x%lx) past end of font
 %s: Glyph number (0x%x) larger than font length
 %s: Illegal vt number %s: Input file: bad input length (%d)
 %s: Input file: trailing garbage
 %s: Options --unicode and --ascii are mutually exclusive
 %s: Unicode range U+%x-U+%x not of the same length as font position range 0x%x-0x%x
 %s: Unsupported psf file mode (%d)
 %s: Unsupported psf version (%d)
 %s: VT 1 is the console and cannot be deallocated
 %s: Warning: line too long
 %s: addfunc called with bad func %d
 %s: addfunc: func_buf overflow
 %s: background will look funny
 %s: bad utf8
 %s: bug in do_loadtable
 %s: cannot deallocate or clear keymap
 %s: cannot find any keymaps?
 %s: could not deallocate console %d
 %s: could not deallocate keymap %d
 %s: could not return to original keyboard mode
 %s: could not switch to Unicode mode
 %s: deallocating all unused consoles failed
 %s: error reading keyboard mode: %m
 %s: error setting keyboard mode
 %s: font position 32 is nonblank
 %s: input font does not have an index
 %s: locks virtual consoles, saving your current session.
Usage: %s [options]
       Where [options] are any of:
-c or --current: lock only this virtual console, allowing user to
       switch to other virtual consoles.
-a or --all: lock all virtual consoles by preventing other users
       from switching virtual consoles.
-v or --version: Print the version number of vlock and exit.
-h or --help: Print this help message and exit.
 %s: not loading empty unimap
(if you insist: use option -f to override)
 %s: out of memory
 %s: out of memory?
 %s: plain map not allocated? very strange ...
 %s: psf file with unknown magic
 %s: short ucs2 unicode table
 %s: short unicode table
 %s: short utf8 unicode table
 %s: trailing junk (%s) ignored
 %s: unknown option
 %s: unknown utf8 error
 %s: warning: loading Unicode keymap on non-Unicode console
    (perhaps you want to do `kbd_mode -u'?)
 %s: warning: loading non-Unicode keymap on Unicode console
    (perhaps you want to do `kbd_mode -a'?)
 %s: wiped it
 %s: zero input character size?
 %s: zero input font length?
 '%s' is not a function key symbol (No change in compose definitions.)
 0 is an error; for 1-88 (0x01-0x58) scancode equals keycode
 ?UNKNOWN? Activation interrupted? Appended Unicode map
 Bad character height %d
 Bad character width %d
 Bad input file size
 Bad input line: %s
 Cannot check whether vt %d is free; use `%s -f' to force. Cannot find %s
 Cannot find %s font
 Cannot find a free vt Cannot find default font
 Cannot open %s read/write Cannot open /dev/port Cannot open font file %s
 Cannot read console map
 Cannot stat map file Cannot write font file Cannot write font file header Character count: %d
 Couldn't activate vt %d Couldn't deallocate console %d Couldn't find owner of current tty! Couldn't get a file descriptor referring to the console Couldn't get a file descriptor referring to the console
 Couldn't open %s
 Couldn't read VTNO:  Current default flags:   Current flags:           Current leds:            Error opening /dev/kbd.
 Error parsing symbolic map from `%s', line %d
 Error reading %s
 Error reading current flags setting. Maybe you are not on the console?
 Error reading current led setting from /dev/kbd.
 Error reading current led setting. Maybe stdin is not a VT?
 Error reading current setting. Maybe stdin is not a VT?
 Error reading map from file `%s'
 Error resetting ledmode
 Error writing map to file
 Error writing screendump
 Error: %s: Insufficient number of fields in line %u. Error: %s: Invalid value in field %u in line %u. Error: %s: Line %u has ended unexpectedly.
 Error: %s: Line %u is too long.
 Error: Not enough arguments.
 Error: Unrecognized action: %s
 Font height    : %d
 Font width     : %d
 Found nothing to save
 Hmm - a font from restorefont? Using the first half.
 Invalid number of lines
 It seems this kernel is older than 1.1.92
No Unicode mapping table loaded.
 KDGKBENT error at index %d in table %d
 KDGKBENT error at index 0 in table %d
 KDGKBSENT failed at index %d:  KIOCGLED unavailable?
 KIOCSLED unavailable?
 Keymap %d: Permission denied
 Loaded %d compose %s.
 Loading %d-char %dx%d (%d) font
 Loading %d-char %dx%d (%d) font from file %s
 Loading %d-char %dx%d font
 Loading %d-char %dx%d font from file %s
 Loading %s
 Loading Unicode mapping table...
 Loading binary direct-to-font screen map from file %s
 Loading binary unicode screen map from file %s
 Loading symbolic screen map from file %s
 Loading unicode map from file %s
 Meta key gives Esc prefix
 Meta key sets high order bit
 New default flags:     New flags:             New leds:              No final newline in combine file
 Old #scanlines: %d  New #scanlines: %d  Character height: %d
 Old default flags:     Old flags:             Old leds:              Old mode: %dx%d  New mode: %dx%d
 Only root can use the -u flag. Plain scancodes xx (hex) versus keycodes (dec)
 Please try again later.


 Read %d-char %dx%d font from file %s
 Reading font file %s
 Saved %d-char %dx%d font file on %s
 Saved screen map in `%s'
 Saved unicode map on `%s'
 Searching in %s
 Showing %d-char font

 Strange ... screen is both %dx%d and %dx%d ??
 Strange mode for Meta key?
 Symbols recognized by %s:
(numeric value, symbol)

 The %s is now locked by %s.
 The entire console display cannot be locked.
 The entire console display is now completely locked by %s.
 The keyboard is in Unicode (UTF-8) mode
 The keyboard is in mediumraw (keycode) mode
 The keyboard is in raw (scancode) mode
 The keyboard is in some unknown mode
 The keyboard is in the default (ASCII) mode
 This file contains 3 fonts: 8x8, 8x14 and 8x16. Please indicate
using an option -8 or -14 or -16 which one you want loaded.
 This tty (%s) is not a virtual console.
 Too many files to combine
 Try `%s --help' for more information.
 Typematic Rate set to %.1f cps (delay = %d ms)
 Unable to find command. Unable to open %s Unable to set new session Usage:
	%s [-C console] [-o map.orig]
 Usage:
	%s [-i infont] [-o outfont] [-it intable] [-ot outtable] [-nt]
 Usage:
	%s [-s] [-C console]
 Usage:
	%s infont [outtable]
 Usage:
	%s infont intable outfont
 Usage:
	%s infont outfont
 Usage:
	setleds [-v] [-L] [-D] [-F] [[+|-][ num | caps | scroll %s]]
Thus,
	setleds +caps -num
will set CapsLock, clear NumLock and leave ScrollLock unchanged.
The settings before and after the change (if any) are reported
when the -v option is given or when no change is requested.
Normally, setleds influences the vt flag settings
(and these are usually reflected in the leds).
With -L, setleds only sets the leds, and leaves the flags alone.
With -D, setleds sets both the flags and the default flags, so
that a subsequent reset will not change the flags.
 Usage:
	setmetamode [ metabit | meta | bit | escprefix | esc | prefix ]
Each vt has his own copy of this bit. Use
	setmetamode [arg] < /dev/ttyn
to change the settings of another vt.
The setting before and after the change are reported.
 Usage: %1$s [-C DEVICE] getmode [text|graphics]
   or: %1$s [-C DEVICE] gkbmode [raw|xlate|mediumraw|unicode]
   or: %1$s [-C DEVICE] gkbmeta [metabit|escprefix]
   or: %1$s [-C DEVICE] gkbled  [scrolllock|numlock|capslock]
 Usage: %s [OPTIONS] -- command

This utility help you to start a program on a new virtual terminal (VT).

Options:
  -c, --console=NUM   use the given VT number;
  -e, --exec          execute the command, without forking;
  -f, --force         force opening a VT without checking;
  -l, --login         make the command a login shell;
  -u, --user          figure out the owner of the current VT;
  -s, --switch        switch to the new VT;
  -w, --wait          wait for command to complete;
  -v, --verbose       print a message for each action;
  -V, --version       print program version and exit;
  -h, --help          output a brief help message.

 Usage: %s vga|FILE|-

If you use the FILE parameter, FILE should be exactly 3 lines of
comma-separated decimal values for RED, GREEN, and BLUE.

To seed a valid FILE:
   cat /sys/module/vt/parameters/default_{red,grn,blu} > FILE

and then edit the values in FILE.

 Usage: kbdrate [-V] [-s] [-r rate] [-d delay]
 Usage: setfont [write-options] [-<N>] [newfont..] [-m consolemap] [-u unicodemap]
  write-options (take place before file loading):
    -o  <filename>  Write current font to <filename>
    -O  <filename>  Write current font and unicode map to <filename>
    -om <filename>  Write current consolemap to <filename>
    -ou <filename>  Write current unicodemap to <filename>
If no newfont and no -[o|O|om|ou|m|u] option is given,
a default font is loaded:
    setfont         Load font "default[.gz]"
    setfont -<N>    Load font "default8x<N>[.gz]"
The -<N> option selects a font from a codepage that contains three fonts:
    setfont -{8|14|16} codepage.cp[.gz]   Load 8x<N> font from codepage.cp
Explicitly (with -m or -u) or implicitly (in the fontfile) given mappings
will be loaded and, in the case of consolemaps, activated.
    -h<N>      (no space) Override font height.
    -m <fn>    Load console screen map.
    -u <fn>    Load font unicode map.
    -m none    Suppress loading and activation of a screen map.
    -u none    Suppress loading of a unicode map.
    -v         Be verbose.
    -C <cons>  Indicate console device to be used.
    -V         Print version and exit.
Files are loaded from the current directory or %s/*/.
 Use Alt-function keys to switch to other virtual consoles. Using VT %s Warning: path too long: %s/%s
 When loading several fonts, all must be psf fonts - %s isn't
 When loading several fonts, all must have the same height
 When loading several fonts, all must have the same width
 You asked for font size %d, but only 8, 14, 16 are possible here.
 [ if you are trying this under X, it might not work
since the X server is also reading /dev/console ]
 adding map %d violates explicit keymaps line addkey called with bad index %d addkey called with bad keycode %d addkey called with bad table %d addmap called with bad index %d appendunicode: illegal unicode %u
 assuming iso-8859-1 %s
 assuming iso-8859-15 %s
 assuming iso-8859-2 %s
 assuming iso-8859-3 %s
 assuming iso-8859-4 %s
 bug: getfont called with count<256
 bug: getfont using GIO_FONT needs buf.
 cannot change translation table
 cannot open file %s
 cannot open include file %s caught signal %d, cleaning up...
 code outside bounds compose table overflow
 couldn't read %s
 couldn't read %s, and cannot ioctl dump
 deallocate keymap %d
 definition definitions dumpkeys version %s entries entry error executing  %s
 error reading scancode even number of arguments expected expected filename between quotes failed to bind key %d to value %d
 failed to bind string '%s' to function %s
 failed to clear string %s
 failed to get keycode for scancode 0x%x
 failed to restore original translation table
 failed to restore original unimap
 failed to set scancode %x to keycode %d
 for 1-%d (0x01-0x%02x) scancode equals keycode
 impossible error in do_constant impossible: not meta?
 includes are nested too deeply kb mode was %s
 kbd_mode: error reading keyboard mode
 key key bindings not changed
 keycode %3d %s
 keycode %d, table %d = %d%s
 keycode range supported by kernel:           1 - %d
 keys killkey called with bad index %d killkey called with bad table %d loadkeys version %s

Usage: loadkeys [option...] [mapfile...]

Valid options are:

  -a --ascii         force conversion to ASCII
  -b --bkeymap       output a binary keymap to stdout
  -c --clearcompose  clear kernel compose table
  -C --console=file
                     the console device to be used
  -d --default       load "%s"
  -h --help          display this help text
  -m --mktable       output a "defkeymap.c" to stdout
  -q --quiet         suppress all normal output
  -s --clearstrings  clear kernel string table
  -u --unicode       force conversion to Unicode
  -v --verbose       report the changes
 loadkeys: don't know how to compose for %s
 mapscrn: cannot open map file _%s_
 max nr of compose definitions: %d
 max number of actions bindable to a key:         %d
 new state:     nr of compose definitions in actual use: %d
 number of function keys supported by kernel: %d
 number of keymaps in actual use:                 %d
 of which %d dynamically allocated
 off old state:     on  press press any key (program terminates 10s after last keypress)...
 ranges of action codes supported by kernel:
 release resizecons:
call is:  resizecons COLSxROWS  or:  resizecons COLS ROWS
or: resizecons -lines ROWS, with ROWS one of 25, 28, 30, 34, 36, 40, 44, 50, 60
 resizecons: cannot find videomode file %s
 resizecons: cannot get I/O permissions.
 resizecons: don't forget to change TERM (maybe to con%dx%d or linux-%dx%d)
 resizecons: the command `%s' failed
 setfont: cannot both restore from character ROM and from file. Font unchanged.
 setfont: too many input files
 showkey version %s

usage: showkey [options...]

valid options are:

	-h --help	display this help text
	-a --ascii	display the decimal/octal/hex values of the keys
	-s --scancodes	display only the raw scan-codes
	-k --keycodes	display only the interpreted keycodes (default)
 stdin is not a tty strange... ct changed from %d to %d
 string string too long strings switching to %s
 syntax error in map file
 too many (%d) entries on one line too many compose definitions
 too many key definitions on one line unicode keysym out of range: %s unknown charset %s - ignoring charset request
 unknown keysym '%s'
 unrecognized argument: _%s_

 unrecognized user usage: %s
 usage: %s [-v] [-o map.orig] map-file
 usage: chvt N
 usage: getkeycodes
 usage: kbd_mode [-a|-u|-k|-s] [-C device]
 usage: screendump [n]
 usage: setkeycode scancode keycode ...
 (where scancode is either xx or e0xx, given in hexadecimal,
  and keycode is given in decimal)
 usage: showconsolefont -V|--version
       showconsolefont [-C tty] [-v] [-i]
(probably after loading a font with `setfont font')

Valid options are:
 -C tty   Device to read the font from. Default: current tty.
 -v       Be more verbose.
 -i       Don't print out the font table, just show
          ROWSxCOLSxCOUNT and exit.
 usage: totextmode
 vt %d is in use; command aborted; use `%s -f' to force. Project-Id-Version: kbd 1.15.3-rc1
Report-Msgid-Bugs-To: Alexey Gladkov <gladkov.alexey@gmail.com>
POT-Creation-Date: 2012-11-16 00:52+0400
PO-Revision-Date: 2013-03-01 09:05+0000
Last-Translator: Hendrik Knackstedt <Unknown>
Language-Team: German <translation-team-de@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:33+0000
X-Generator: Launchpad (build 18115)
Language: de
 			    interpretiere Zeichencodes, als kämen sie aus dem
			    angegebenen Zeichensatz.
 

Scancodes mit Escape e0 xx (hex):
 
%d %s und %d %s geändert.
 
Drücken Sie eine Taste - Strg+D beendet das Programm

 
Erkannte Wandlernamen und ihre Spaltennummern:
 
Die folgenden Synonyme werden erkannt:

 
Verwendung: dumpkeys [Optionen...]

Gültige Optionen sind:

	-h --help	    gibt diese Hilfe aus
	-i --short-info	    zeigt Informationen über den Tastaturtreiber
	-l --long-info	    short-info und die von loadkeys erkannten Symbole
	-n --numeric	    Tastaturtabelle in hexadezimaler Notierung
	-f --full-table	    ohne Abkürzungen, eine Zeile pro Tastencode
	-1 --separate-lines eine Zeile für jedes (Wandler, Tastencode) Paar
	   --funcs-only	    zeigt nur die Funktionstastenbelegungen
	   --keys-only	    zeigt nur die Tastenbelegungen
	   --compose-only   zeigt nur die Tastenfolgen an
	-c --charset=     FEHLGESCHLAGEN # keine alt_is_meta: in der Keymap %d ist Taste %d gebunden an %-15s für %s
 %s von %s
 %s Version %s

Verwendung: %s [Optionen...]

Gültige Optionen sind:

	-h --help            gibt diese Hilfe aus
	-V --version         zeigt die Programmversion an
	-n --next-available  zeigt die Nummer des nächsten unbenutzten VT
 %s: %s: Warnung: Die Zeile ist zu lang.
 %s: 0: Ungültige VT-Nummer.
 %s: Ungültiger Unicode-Bereich zum Font-Bereich 0x%x-0x%x.
 %s: Falscher Aufruf von readpsffont.
 %s: Falsches Bereichsende (0x%lx).
 %s: Ungültiges Bereichsende (0x%x).
 %s: Falsche Eingabezeile: %s
 %s: Falsche magische Nummer in %s.
 %s: Zu einem Bereich von Font-Positionen sollte auch ein Unicode-Bereich existieren.
 %s: Fehler beim Lesen des Fonts. %s: Glyphennummer (0x%lx) nach dem Ende des Fonts.
 %s: Glyphennummer (0x%x) größer als Font-Länge.
 %s: Ungültige VT-Nummer %s: Eingabedatei: Falsche Länge (%d).
 %s: Eingabedatei: Angehänger Zeichenmüll.
 %s: Die Optionen --unicode und --ascii schließen sich gegenseitig aus
 %s: Der Unicode-Bereich U+%x-U+%x ist nicht gleich lang wie der Font-Bereich 0x%x-0x%x
 %s: Nicht unterstützer PSF-Dateimodus (%d).
 %s: Nicht unterstützte PSF-Version (%d).
 %s: VT 1 ist die Konsole und kann deshalb nicht freigegeben werden.
 %s: Warnung: Die Zeile ist zu lang.
 %s: addfunc mit ungültiger Funktion %d aufgerufen.
 %s: addfunc: Überlauf von func_buf.
 %s: Der Hintergrund wird seltsam aussehen.
 %s: Falsches UTF-8.
 %s: Fehler in do_loadtable.
 %s: Kann die Keymap nicht freigeben oder leeren.
 %s: Kann keine Keymap finden.
 %s: Konnte die Konsole %d nicht freigeben.
 %s: Konnte die Keymap %d nicht freigeben.
 %s: Konnte den ursprünglichen Tastaturmodus nicht wiederherstellen
 %s: Konnte nicht in den Unicode-Modus wechseln
 %s: Das Freigeben aller unbenutzten Konsolen schlug fehl.
 %s: Fehler beim Auslesen des Tastaturmodus: %m
 %s: Fehler beim Einstellen des Tastaturmodus
 %s: Stelle 32 im Font ist kein Leerzeichen.
 %s: Der Font hat keinen Index.
 %s: sperrt virtuelle Konsolen, speichert Ihre aktuelle Sitzung.
Aufruf: %s [Optionen]
       Wobei [Optionen] Folgendes sein kann:
-c oder --current: nur diese virtuelle Konsole sperren, was dem
       Benutzer erlaubt zu einer anderen virtuellen Konsole zu wechseln.
-a oder --all: Alle virtuellen Konsolen sperren, wodurch verhindert
       wird, dass der Benutzer die virtuelle Konsole wechseln kann.
-v oder --version: Die Versionsnummer von vlock ausgeben und beenden.
-h oder --help: Diese Hilfe anzeigen und beenden.
 %s: Eine leere Unicode-Tabelle wird nicht geladen.
Benutzen Sie die Option -f zum Erzwingen der Aktion.
 %s: Arbeitsspeicher erschöpft.
 %s: Virtueller Speicher erschöpft.
 %s: Eine einfache Tastaturtabelle wurde nicht zugeteilt. Eigenartig ...
 %s: PSF-Datei mit unbekannter magischer Zeichenkette.
 %s: Kurze UCS2-Unicode-Tabelle.
 %s: Kurze Unicode-Tabelle.
 %s: Kurze UTF8-Unicode-Tabelle.
 %s: Überflüssiger Müll (%s) wurde ignoriert.
 %s: Unbekannte Option
 %s: Unbekannter UTF-8-Fehler.
 %s: Warnung: lade Unicode-Tastaturbelegung auf Nicht-Unicode-Konsole
    (vielleicht möchten Sie zuvor "kbd_mode -u" ausführen?)
 %s: Warnung: lade Nicht-Unicode-Tastaturbelegung auf Unicode-Konsole
    (vielleicht möchten Sie zuvor "kbd_mode -a" ausführen?)
 %s: löschte es.
 %s: Ist die Zeichengröße Null?
 %s: Leerer Font?
 '%s' ist kein Funktionstastensymbol. (Compose-Definitionen nicht geändert.)
 0 ist ein Fehler; für 1-88 (0x01-0x58) ist Scancode gleich Tastencode
 ?UNBEKANNT? Aktivierungsvorgang unterbrochen? Hänge Unicode-Tabelle an.
 Falsche Zeichenhöhe %d.
 Falsche Zeichenbreite %d.
 Falsche Eingabedateigröße.
 Ungültige Eingabezeile: %s.
 Kann die Verfügbarkeit des VT %d nicht überprüfen; "%s -f" erzwingt. Kann %s nicht finden.
 Kann den Font %s nicht finden.
 Kann kein freies VT finden Kann den Standard-Font nicht finden.
 Kann %s nicht zum Schreiben und Lesen öffnen Kann /dev/port nicht öffnen. Kann Font-Datei %s nicht lesen.
 Kann Konsolentabelle nicht lesen.
 Kann Zuordnungsdatei nicht öffnen. Kann die Font-Datei nicht schreiben. Kann den Kopf der Font-Datei nicht schreiben. Zeichenanzahl: %d
 Kann VT %d nicht aktivieren Konnte die Konsole %d nicht freigeben Konnte Eigentümer des aktuellen Terminals nicht finden! Konnte keinen Dateideskriptor finden, der auf die Konsole verweist Konnte keinen Dateideskriptor finden, der auf die Konsole verweist.
 Kann %s nicht öffnen.
 Konnte VT-Nummer nicht lesen:  Aktuelle Standardeinstellungen:   Aktive Einstellungen:           Aktive LEDs:            Fehler beim Öffnen von /dev/kbd.
 Fehler beim Verarbeiten der Symboltabelle `%s', Zeile %d.
 Fehler beim Lesen von %s.
 Fehler beim Lesen der Flags. Befinden Sie sich wirklich auf der Konsole?
 Fehler beim Lesen der LED-Zustände von /dev/kbd.
 Fehler beim Lesen der LED-Zustände.
Ist die Standardeingabe wirklich ein Terminal?
 Die aktuellen Einstellungen sind nicht lesbar.
Ist die Standardeingabe wirklich ein Terminal?
 Fehler beim Lesen der Tabelle aus der Datei `%s'.
 Fehler beim Setzen der LEDs.
 Fehler beim Schreiben der Tabelle in die Datei.
 Fehler beim Schreiben des Bildschirminhalts.
 Fehler: %s: Nicht genügend Felder in Zeile %u. Fehler: %s: Ungültiger Wert im Feld %u in Zeile %u. Fehler: %s: Zeile %u endet unerwartet.
 Fehler: %s: Die Zeile %u ist zu lang.
 Fehler: Nicht genügend Argumente.
 Fehler: Unbekannte Aktion: %s
 Font-Höhe    : %d
 Font-Breite  : %d
 Nichts zum Speichern da.
 Hmm - ein Font von restorefont? Verwende die erste Hälfte.
 Ungültige Zeilenanzahl.
 Der Kernel scheint älter als 1.1.92 zu sein.
Es wurde keine Unicode-Tabelle geladen.
 KDGKBENT-Fehler an Stelle %d in Tabelle %d.
 KDGKBENT-Fehler an Stelle 0 in Tabelle %d.
 KDGKBSENT fehlgeschlagen an Stelle %d:  Ist KIOCGLED nicht verfügbar?
 Ist KIOCSLED nicht verfügbar?
 Keymap %d: Zugriff verweigert.
 %d Compose-%s geladen.
 Lade %d-Zeichen %dx%d (%d).
 Lade %d-Zeichen %dx%d (%d) aus Datei %s.
 Lade %d-Zeichen %dx%d.
 Lade %d-Zeichen %dx%d aus der Datei %s.
 Lade %s.
 Lade Unicodetabelle...
 Lade eine binären Font aus der Datei %s.
 Lade binäre Unicode-Tabelle aus %s.
 Lade Symboltabelle aus %s.
 Lade Unicodetabelle aus der Datei %s.
 Meta-Taste erzeugt ein Umschaltpräfix.
 Meta-Taste setzt das Bit mit den höchsten Stellenwert.
 Neue Standardeinstellungen:     Neue Einstellungen:             Neue LEDs:              Keine abschließender Zeilenumbruch in zusammengefügter Datei.
 Zeilenzahl alt: %d  Zeilenzahl neu: %d  Zeichenhöhe: %d
 Alte Standardeinstellungen:     Alte Einstellungen:             Alte LEDs:              Alter Modus: %dx%d  Neuer Modus: %dx%d
 Nur root darf die Option -u benutzen. Einfache Scancodes xx (hexadezimal) und Tastencodes (dezimal):
 Bitte versuchen Sie es später erneut.


 Lese %d-Zeichen %dx%d aus der Datei %s.
 Lese Font-Datei %s.
 Speichere %d-Zeichen %dx%d in Font-Datei %s.
 Speichere Bildschirmtabelle in `%s'.
 Unicode-Tabelle in `%s' gespeichert.
 Suche in %s.
 Zeige Font mit %d Zeichen.

 Seltsam ... der Bildschirm ist sowohl %dx%d als auch %dx%d?
 Merkwürdiger Modus für die Meta-Taste?
 Symbole erkannt von %s:
(Numerischer Wert, Symbol)

 Die %s ist nun durch %s gesperrt.
 Die gesamte Konsolenanzeige kann nicht gesperrt werden.
 Die komplette Konsolenanzeige wird nun vollständig gesperrt durch %s.
 Die Tastatur ist im Unicode-Modus (UTF-8).
 Die Tastatur ist im Tastencode-Modus (mediumraw).
 Die Tastatur ist im Urmodus (scancode).
 Der Tastaturmodus ist unbekannt.
 Die Tastatur ist im voreingestellten Modus (ASCII).
 Diese Datei enthält 3 Fonts: 8x8, 8x14 und 8x16. Wählen Sie bitte
mit einer Option aus -8, -14 oder -16 den zu ladenden Font aus.
 Diese TTY (%s) ist keine virtuelle Konsole.
 Zu viele Dateien zum Zusammenfassen.
 Rufen Sie »%s --help« auf, um weitere Informationen zu erhalten.
 Die Wiederholrate ist auf %.1f Zeichen/s gesetzt (Verzögerung = %d ms).
 Konnte Kommando nicht finden. Kann %s nicht öffnen Kann keine neue Sitzung erzeugen Verwendung:
%s [-C Konsole] [-o Originaltabelle]
 Verwendung:
	%s [-i Eingabe-Font] [-o Ausgabe-Font]
		[-it Eingabetabelle] [-ot Ausgabetabelle] [-nt]
 Verwendung:
	%s [-s] [-C Konsole]
 Verwendung:
	%s Eingabe-Font [Ausgabetabelle]
 Verwendung:
	%s Eingabe-Font Eingabetabelle Ausgabe-Font
 Verwendung:
	%s Eingabe-Font Ausgabe-Font
 Verwendung:
	setleds [-v] [-L] [-D] [-F] [[+|-][ num | caps | scroll %s]]
Beispiel:
	setleds +caps -num
setzt CapsLock, löscht NumLock und ändert ScrollLock nicht.

Die Einstellungen vor und nach der Änderung (falls erfolgt) werden
ausgegeben wenn die -v Option angegeben oder keine Änderung gewünscht
wird. Normalerweise beeinflusst setleds die VT-Einstellungen (die
gewöhnlich mit den LEDs auf der Tastatur angezeigt werden). Mit -L
setzt setleds nur die LEDs und verändert die Einstellungen nicht.
Mit -D setzt setleds die Flags und die Standardeinstellungen, sodass
ein nachfolgendes Reset die Einstellungen nicht ändert.
 Verwendung:
	setmetamode [ metabit | meta | bit | escprefix | esc | prefix ]
Jedes VT besitzt eine eigene Kopie dieses Bits. Mit
	setmetamode [arg] < /dev/ttyn
kann dieses Bit für ein anderes VT geändert werden.
Die Einstellungen vor und nach der Änderung werden ausgegeben.
 Verwendung: %1$s [-C GERÄT] getmode [text|graphics]
      oder: %1$s [-C GERÄT] gkbmode [raw|xlate|mediumraw|unicode]
      oder: %1$s [-C GERÄT] gkbmeta [metabit|escprefix]
      oder: %1$s [-C GERÄT] gkbled  [scrolllock|numlock|capslock]
 Aufruf: %s [OPTIONEN] -- Befehl

Dieses Werkzeug hilft Ihnen, ein Programm auf einem neuen virtuellen Terminal (VT) zu starten.

Optionen:
  -c, --console=NUM   Die angegebene VT-Nummer verwenden;
  -e, --exec          Den Befehl ausführen ohne einen neuen Prozess zu erzeugen;
  -f, --force         Öffnen eines VT ohne Prüfung erzwingen;
  -l, --login         Den Befehl zu einer Login-Shell machen;
  -u, --user          Den Besitzer des aktuellen VT bestimmen;
  -s, --switch        Zum neuen VT wechseln;
  -w, --wait          Auf Abschluss des Befehls warten;
  -v, --verbose       Eine Nachricht für jede Aktion ausgeben;
  -V, --version       Programmversion ausgeben und beenden;
  -h, --help          Eine kurze Hilfenachricht ausgeben.

 Verwendung: %s vga|DATEI|-

Wenn Sie eine DATEI angeben, sollte sie aus genau drei Zeilen mit
Komma-getrennten Werten für ROT, GRÜN und BLAU bestehen.

Um eine Vorlage für eine solche Datei zu erhalten:
   cat /sys/module/vt/parameters/default_{red,grn,blu} > DATEI

Dann können Sie DATEI nach Ihren Vorstellungen bearbeiten.

 Verwendung: kbdrate [-V] [-s] [-r Wert] [-d Verzögerung]
 Verwendung: setfont [Schreib-Optionen] [-<N>] [neuer_font..] [-m Konsolen-Map] [-u Unicode-Map]
  Schreib-Optionen (werden vor dem Laden von Dateien aktiviert):
    -o  <Dateiname>	Schreibe den aktuellen Font nach <Dateiname>.
    -O  <Dateiname>	Schreibe zusätzlich die Unicode-Map nach <Dateiname>.
    -om <Dateiname>	Schreibe die aktuelle Konsolen-Map nach <Dateiname>.
    -ou <Dateiname>	Schreibe die aktuelle Unicode-Map nach <Dateiname>.
Wenn weder ein Font noch die -[o|O|om|ou|m|u] angegeben wurde,
wird der Standardfont verwendet:
    setfont             Lade Font "default[.gz]"
    setfont -<N>        Lade Font "default8x<N>[.gz]"
Die Option -<N> wählt einen Font aus einer Codepage die drei Fonts enthält:
    setfont -{8|14|16} codepage.cp[.gz]   Lade 8x<N> Font aus codepage.cp
Explizit (mit -m or -u) oder implizit (in der Fontdatei) gegebene Zuordnungen
werden geladen und, im Fall von Konsolen-Maps, aktiviert.
    -h<N>      (ohne Leerzeichen) Überschreibe Fonthöhe.
    -m <fn>   Lade Konsolen-Map.
    -u <fn>    Lade Font-Unicode-Map.
    -m none   Unterdrücke das Laden und Aktivieren der Screen-Map.
    -u none    Unterdrücke das Laden der Unicode-Map.
    -v             Geschwätziger Modus.
    -C <Kons> Gibt das zu benutzende Gerät an.
    -V             Ausgabe der Versionsnummer und Beenden.
Dateien werden im aktuellen Verzeichnis oder in %s/*/ gesucht.
 Verwenden Sie die Alt-Funktionstasten, um zu einer anderen virtuellen Konsole zu wechseln. Benutze VT %s Warnung: Pfad zu lang: %s/%s
 Gleichzeitig geladene Fonts müssen im PSF Format sein - %s ist es nicht.
 Wenn mehrere Fonts geladen werden, müssen sie die gleiche Höhe haben.
 Wenn mehrere Fonts geladen werden, müssen alle die gleiche Breite haben.
 Die Zeichengröße %d wurde verlangt, es sind aber nur 8, 14 und 16 verfügbar.
 [ Wenn Sie das unter X probieren, muss es nicht funktionieren, 
  da der X Server ebenfalls von /dev/console liest. ]
 Hinzufügen von Map %d verletzt explizite keymaps-Zeile. addkey mit ungültigem Index %d aufgerufen. addkey mit ungültigem Tastencode %d aufgerufen. addkey mit ungültiger Tabelle %d aufgerufen. addmap mit ungültigem Index %d aufgerufen. appendunicode: Ungültiges Unicode-Zeichen %u.
 Verwende ISO-8859-1 %s.
 Verwende ISO-8859-15 %s.
 Verwende ISO-8859-2 %s.
 Verwende ISO-8859-3 %s.
 Verwende ISO-8859-4 %s.
 Bug: getfont wurde mit einer Anzahl < 256 aufgerufen.
 Bug: Bei Verwendung von GIO_FONT braucht getfont einen Puffer.
 Die Übersetzungstabelle konnte nicht gewechselt werden.
 Kann Datei %s nicht öffnen.
 Kann Include-Datei %s nicht öffnen. Signal %d erhalten, räume auf ...
 Scancode außerhalb der Grenzen. Überlauf der Compose-Tabelle
 Kann %s nicht lesen.
 Kann %s nicht lesen und ioctl auf den Bildschirminhalt anwenden.
 Keymap %d freigeben
 Definition Definitionen dumpkeys Version %s Einträge Eintrag Fehler beim Ausführen von %s.
 Fehler beim Lesen des Scancodes. Eine gerade Anzahl von Argumenten wird erwartet. Dateiname zwischen Anführungszeichen erwartet Konnte der Taste %d nicht den Wert %d zuordnen.
 Konnte den String '%s' nicht der Funktion %s zuordnen.
 Das Leeren des Strings %s ist fehlgeschlagen.
 Kann den Tastencode für den Scancode 0x%x nicht erhalten.
 Das Wiederherstellen der ursprünglichen Übersetzungstabelle ist fehlgeschlagen.
 Das Wiederherstellen der ursprünglichen Unimap ist fehlgeschlagen.
 Der Scancode %x konnte nicht für den Tastencode %d gesetzt werden.
 0 ist ein Fehler; für 1-%d (0x01-0x%02x) ist Scancode gleich Tastencode
 Unmöglicher Fehler in do_constant. unmöglich: Keine Meta?
 Include-Dateien zu tief verschachtelt. KB-Modus war %s
 kbd_mode: Fehler beim Lesen des Tastaturmodus.
 Taste Tastenbelegungen nicht geändert.
 Tastencode %3d %s
 Tastencode %d, Tabelle %d = %d%s
 Vom Kernel unterstützter Tastencodebereich:         1 - %d
 Tasten killkey mit ungültigem Index %d aufgerufen. killkey mit ungültiger Tabelle %d aufgerufen. loadkeys Version %s

Aufruf: loadkeys [Option …] [Zuordnungsdatei …]

Gültige Optionen sind:

  -a --ascii         Umwandlung in ASCII erzwingen
  -b --bkeymap       Eine binäre Zuordnungstabelle an Standardausgabe übergeben
  -c --clearcompose  Kernel-Zusammensetzungstabelle leeren
  -C --console=Datei
                     Das zu verwendende Konsolengerät
  -d --default       »%s« laden
  -h --help          Diese Hilfe anzeigen
  -m --mktable       »defkeymap.c« an die Standardausgabe übergeben
  -q --quiet         Jede normale Ausgabe unterdrücken
  -s --clearstrings  Kernel-Zeichenkettentabelle leeren
  -u --unicode       Umwandlung in Unicode erzwingen
  -v --verbose       Über Änderungen berichten
 loadkeys: Weiß nicht, wie Compose für %s funktionieren soll.
 mapscrn: Kann Zuordnungsdatei _%s_ nicht öffnen.
 Maximale Anzahl zusammengesetzter Tastendefinitionen: %d
 Maximale Anzahl von an eine Taste bindbaren Aktionen:   %d
 Neuer Status:     Benutzte Anzahl von zusammengesetzten Tastendefinitionen: %d
 Anzahl der vom Kernel unterstützten Funktionstasten: %d
 Anzahl tatsächlich benutzter Keymaps:                   %d
 Davon sind %d dynamisch zugewiesen.
 aus Alter Status:     an  gedrückt Drücken Sie eine Taste (Programmende 10 s nach dem letzten Tastendruck)...
 Bereich der vom Kernel unterstützten Aktionscodes:
 losgelassen resizecons:
Verwendung:  resizecons SPALTENxZEILEN
oder:        resizecons SPALTENxZEILEN
oder:        resizecons -lines ZEILEN,
                 mit ZEILEN in 25, 28, 30, 34, 36, 40, 44, 50, 60
 resizecons: Kann die Videomodus-Datei %s nicht finden.
 resizecons: Kann keine Ein-/Ausgabe-Erlaubnis erhalten.
 resizecons: Vergessen Sie nicht, TERM zu ändern (z.B. zu con%dx%d oder linux-%dx%d).
 resizecons: Das Kommando `%s' schlug fehl.
 setfont: Kann den Font nicht zugleich von ROM und Datei wiederherstellen. Vorgang abgebrochen.
 setfont: Zu viele Eingabedateien.
 showkey Version %s

Verwendung: showkey [Optionen...]

Gültige Optionen sind:

	-h --help	zeigt diesen Hilfetext an
	-a --ascii	zeigt die dezimalen/oktalen/hexadezimalen Tastencodes
	-s --scancodes	zeigt die originalen Tastaturcodes an
	-k --keycodes	zeigt nur die übersetzten Tastaturcodes an (Vorgabe)
 Standardeingabe ist keine TTY Seltsam... ct hat sich von %d auf %d geändert
 String String zu lang Strings Wechsle zu %s.
 Syntaxfehler in Map-Datei.
 Zu viele (%d) Einträge in einer Zeile. Zu viele Compose-Definitionen.
 Zu viele Tastendefinitionen in einer Zeile. Unicode-Tastensymbol nicht im zulässigen Bereich: %s Unbekannter Zeichensatz %s - Zeichensatzanforderung wird ignoriert.
 Unbekanntes Tastensymbol '%s'.
 Unbekanntes Argument: _%s_

 Unbekannter Benutzer Verwendung: %s
 Verwendung: %s [-v] [-o Originaltabelle] Tabellendatei
 Verwendung: chvt N
 Verwendung: getkeycodes
 Verwendung: kbd_mode [-a|-u|-k|-s] [-C Gerät]
 Verwendung: screendump [n]
 Verwendung: setkeycode Scancode Tastencode ...
 (Wobei Scancode entweder xx oder e0xx hexadezimal
  und der Tastencode dezimal angegeben wird.)
 Verwendung: showconsolefont -V|--version
            showconsolefont [-C tty] [-v] [-i]
(möglicherweise nach dem Laden eines Fonts mit "setfont font")

Gültige Optionen sind:

 -C tty   Gerät, von dem der Font gelesen werden soll. Standard ist
          das aktive Terminal.
 -v       Ausführlichere Meldungen ausgeben.
 -i       Die Font-Tabelle nicht ausgeben, stattdessen
          ZEILENxSPALTENxANZAHL anzeigen und beenden.
 Verwendung: totextmode
 VT %d wird bereits benutzt, Abbruch; "%s -f" erzwingt. 