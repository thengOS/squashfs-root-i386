��          |      �             !  $   (  �   M  &   6     ]     c     o  �   r     G      K     l  �  ~     .  ,   4  �   a  ,   M     z     �     �  �   �     �  %   �     �     
                                            	          Always Connecting to system bus failed: %s
 Diagnostic information from your Linux kernel has been sent to <a href="http://oops.kernel.org">oops.kernel.org</a> for the Linux kernel developers to work on. 
Thank you for contributing to improve the quality of the Linux kernel.
 Kernel bug diagnostic information sent Never Never again No There is diagnostic information available for this failure. Do you want to submit this information to the <a href="http://oops.kernel.org/">www.oops.kernel.org</a> website for use by the Linux kernel developers?
 Yes Your system had a kernel failure kerneloops client Project-Id-Version: kerneloops-0.10.2
Report-Msgid-Bugs-To: willy@debian.org
POT-Creation-Date: 2008-01-01 06:36-0800
PO-Revision-Date: 2016-03-11 03:12+0000
Last-Translator: Chris Leick <Unknown>
Language-Team: German <debian-l10n-german@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:32+0000
X-Generator: Launchpad (build 18115)
 Immer Verbinden zum System-Bus fehlgeschlagen: %s
 Diagnose-Information von Ihrem Linux-Kernel wurde an <a href="http://oops.kernel.org">oops.kernel.org</a> gesandt, damit die Kernel-Entwickler daran arbeiten können.
Vielen Dank, dass Sie zur Verbesserung des Linux-Kernels beitragen.
 Gesendete Kernel-Fehler-Diagnose-Information Niemals Niemals wieder Nein Es sind dort Diagnose-Informationen für diesen Ausfall verfügbar. Möchten Sie diese Information zu der <a href="http://oops.kernel.org/">www.oops.kernel.org</a>-Website für die Benutzung durch die Linux-Kernel-Entwickler senden?
 Ja Ihr System hatte einen Kernel-Ausfall kerneloops-Client 