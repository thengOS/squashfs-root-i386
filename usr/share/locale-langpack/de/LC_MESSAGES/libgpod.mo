��    �      �  �   �      �  *   �      �     �  	     *     0   8     i     q     �     �     �  /   �     �  ,   �  A        \      z  %   �  "   �  $   �  H   	      R  J   s  .   �  "   �       '   -     U  $   r  $   �  (   �  O   �  I   5  #        �  !   �     �  #   �  !   !  *   C  +   n  #   �  *   �     �               8     V  5   u  .   �     �  	   �       #        5  5   N  /   �     �     �  U   �  I   1  	   {     �     �     �     �     �     �     �  
   �     �     �       -   &     T     d     t     �     �     �     �     �  
   �     �     �     �     �          "     8  #   T  4   x  3   �  +   �  9     9   G     �     �  =   �  D   �  F   !  "   h     �     �     �  .   �  D   �  E   )     o     x  .   �     �     �     �     �     �               *     =     P     `     o     ~     �     �     �     �     �     �  *   �          #     4     E     V  b   e  X   �  .   !       P      q      �   9   �      �   :   �   �   !  N   "  ?   a"  1  �"     �#     �#     �#     $     $  8   $  (   S$  C   |$  �   �$  Q   J%     �%     �%  
   �%     �%     �%  	   �%  
   �%     �%     �%  .   �%  (   #&  Y   L&  N   �&  ,   �&  T   "'  =   w'  5   �'  5   �'  H   !(  \   j(  R   �(  ^   )  D   y)  D   �)     *  0   "*  �  S*  ,   �+  4   ,     <,     O,  =   [,  O   �,     �,     �,     -     -     -  K   #-  '   o-  C   �-  K   �-  "   '.  1   J.  /   |.  &   �.  .   �.  V   /      Y/  f   z/  :   �/  (   0  (   E0  5   n0  %   �0  ,   �0  0   �0  -   (1  ^   V1  Y   �1  (   2  $   82  /   ]2  +   �2      �2  -   �2  1   3  5   :3  +   p3  1   �3  %   �3  $   �3  $   4  0   >4  1   o4  J   �4  ?   �4  "   ,5  
   O5     Z5  (   h5  #   �5  =   �5  3   �5  	   '6  #   16  �   U6  f   �6     A7     V7     k7     �7     �7     �7     �7     �7  
   �7     �7  #   �7  $   �7  K   !8     m8     �8     �8     �8     �8     �8     �8     �8  
   �8     �8     �8     9     9     29     M9     h9  %   �9  F   �9  8   �9  .   .:  a   ]:  a   �:     !;     5;  `   L;  c   �;  Y   <  *   k<     �<     �<  
   �<  P   �<  R   =  J   e=     �=     �=  P   �=     >     3>     L>     e>     ~>     �>     �>     �>     �>     �>     �>     ?     ?     &?     7?     F?     U?     c?     t?  3   �?     �?     �?     �?     �?     @  �   @  ~   �@  +   A      KA     lA  	   �A  J   �A     �A  J   �A    EB  [   SC  M   �C  t  �C     rE     �E     �E     �E     �E  k   �E  ,   1F  M   ^F  �   �F  a   [G     �G     �G  
   �G     �G     �G  	   �G  
   �G     	H     H  L   H  -   dH  �   �H  z   I  S   �I  X   �I  T   DJ  =   �J  E   �J  T   K  }   rK  g   �K  x   XL  L   �L  M   M  #   lM  9   �M     *       H   +       �       u      6      �   Q           1       �       �   w   �   -      �   !   l   �       t   ^       �                      �   9   i          b   D   �   �   V   �      [   �   ,       s   Z   ~   C          �   �   G       �   P                  �       L   e   �       �   &   R       �   �       d       |   S   �      j   �   �   U       5   �   �   $       M      ;      �       �   K   �       W      %   �      �   E           a   �       `   �   =      '               �       �   �   �   3          T   �       �   x       #       ?   �      "   �       (   �   Y   q   )       �   \                     �   .   �       /   	           B   z       f   }           4   {       �          
   k       X   8   g             �   o   >       :      �          �   �       m   �       O   v   n      �   ]      �   �   �   A   y   p       0       _          �   @   2   c   �           �   r   <          h       J      I   7   N      �   F    %s: Unexpected length %d for genius_cuid!
 '%s' could not be accessed (%s). <No members>
 <Unnamed> Artwork support not compiled into libgpod. Cannot remove Photo Library playlist. Aborting.
 Classic Classic (Black) Classic (Silver) Color Color U2 Control directory not found: '%s' (or similar). Could not access file '%s'. Could not access file '%s'. Photo not added. Could not find corresponding track (dbid: %s) for artwork entry.
 Could not find on iPod: '%s'
 Could not open '%s' for writing. Couldn't find an iPod database on %s. Couldn't read xml sysinfo from %s
 Couldn't write SysInfoExtended to %s Destination file '%s' does not appear to be on the iPod mounted at '%s'. Device directory does not exist. Encountered unknown MHOD type (%d) while parsing the iTunesDB. Ignoring.

 Error adding photo (%s) to photo database: %s
 Error compressing iTunesCDB file!
 Error creating '%s' (mkdir)
 Error initialising iPod, unknown error
 Error initialising iPod: %s
 Error opening '%s' for reading (%s). Error opening '%s' for writing (%s). Error reading iPod photo database (%s).
 Error reading iPod photo database (%s).
Will attempt to create a new database.
 Error reading iPod photo database, will attempt to create a new database
 Error reading iPod photo database.
 Error removing '%s' (%s). Error renaming '%s' to '%s' (%s). Error when closing '%s' (%s). Error while reading from '%s' (%s). Error while writing to '%s' (%s). Error writing list of albums (mhsd type 4) Error writing list of artists (mhsd type 8) Error writing list of tracks (hths) Error writing list of tracks (mhsd type 1) Error writing mhsd type 10 Error writing mhsd type 6 Error writing mhsd type 9 Error writing mhsd5 playlists Error writing playlists (hphs) Error writing special podcast playlists (mhsd type 3) Error writing standard playlists (mhsd type 2) Error: '%s' is not a directory
 Grayscale Grayscale U2 Header is too small for iTunesCDB!
 Illegal filename: '%s'.
 Illegal seek to offset %ld (length %ld) in file '%s'. Insufficient number of command line arguments.
 Invalid Itdb_Track ID '%d' not found.
 Length of smart playlist rule field (%d) not as expected. Trying to continue anyhow.
 Library compiled without gdk-pixbuf support. Picture support is disabled. Master-PL Mini (1st Gen.) Mini (2nd Gen.) Mini (Blue) Mini (Gold) Mini (Green) Mini (Pink) Mini (Silver) Mobile (1) Mobile Phones Mountpoint not set. Mountpoint not set.
 Music directory not found: '%s' (or similar). Nano (1st Gen.) Nano (2nd Gen.) Nano (Black) Nano (Blue) Nano (Green) Nano (Orange) Nano (Pink) Nano (Purple) Nano (Red) Nano (Silver) Nano (White) Nano (Yellow) Nano Video (3rd Gen.) Nano Video (4th Gen.) Nano touch (6th Gen.) Nano with camera (5th Gen.) No 'F..' directories found in '%s'. Not a OTG playlist file: '%s' (missing mhpo header). Not a Play Counts file: '%s' (missing mhdp header). Not a iTunesDB: '%s' (missing mhdb header). Number of MHODs in mhip at %ld inconsistent in file '%s'. Number of MHODs in mhyp at %ld inconsistent in file '%s'. OTG Playlist OTG Playlist %d OTG playlist file '%s': reference to non-existent track (%d). OTG playlist file ('%s'): entry length smaller than expected (%d<4). OTG playlist file ('%s'): header length smaller than expected (%d<20). Path not found: '%s' (or similar). Path not found: '%s'. Photo Photo Library Photos directory not found: '%s' (or similar). Play Counts file ('%s'): entry length smaller than expected (%d<12). Play Counts file ('%s'): header length smaller than expected (%d<96). Playlist Podcasts Problem creating iPod directory or file: '%s'. Regular (1st Gen.) Regular (2nd Gen.) Regular (3rd Gen.) Regular (4th Gen.) Shuffle Shuffle (1st Gen.) Shuffle (2nd Gen.) Shuffle (3rd Gen.) Shuffle (4th Gen.) Shuffle (Black) Shuffle (Blue) Shuffle (Gold) Shuffle (Green) Shuffle (Orange) Shuffle (Pink) Shuffle (Purple) Shuffle (Red) Shuffle (Silver) Shuffle (Stainless) Specified album '%s' not found. Aborting.
 Touch Touch (2nd Gen.) Touch (3rd Gen.) Touch (4th Gen.) Touch (Silver) Unable to retrieve thumbnail (appears to be on iPod, but no image info available): filename: '%s'
 Unexpected error in itdb_photodb_add_photo_internal() while adding photo, please report. Unexpected image type in mhni: %d, offset: %d
 Unexpected mhod string type: %d
 Unexpected mhsd index: %d
 Unknown Unknown action (0x%x) in smart playlist will be ignored.
 Unknown command '%s'
 Unknown smart rule action at %ld: %x. Trying to continue.
 Usage to add photos:
  %s add <mountpoint> <albumname> [<filename(s)>]
  <albumname> should be set to 'NULL' to add photos to the master photo album
  (Photo Library) only. If you don't specify any filenames an empty album will
  be created.
 Usage to dump all photos to <output_dir>:
  %s dump <mountpoint> <output_dir>
 Usage to list all photos IDs to stdout:
  %s list <mountpoint>
 Usage to remove photo IDs from Photo Library:
  %s remove <mountpoint> <albumname> [<ID(s)>]
  <albumname> should be set to 'NULL' to remove photos from the iPod
  altogether. If you don't specify any IDs, the photoalbum will be removed
  instead.
  WARNING: IDs may change when writing the PhotoDB file.
 Video (1st Gen.) Video (2nd Gen.) Video (Black) Video (White) Video U2 Warning: could not find photo with ID <%d>. Skipping...
 Wrong number of command line arguments.
 You need to specify the iPod model used before photos can be added. Your iPod does not seem to support photos. Maybe you need to specify the correct iPod model number? It is currently set to 'x%s' (%s/%s). header length of '%s' smaller than expected (%d < %d) at offset %ld in file '%s'. iPad iPhone iPhone (1) iPhone (Black) iPhone (White) iPhone 3G iPhone 3GS iPhone 4 iPod iTunes directory not found: '%s' (or similar). iTunesCDB '%s' could not be decompressed iTunesDB '%s' corrupt: Could not find playlists (no mhsd type 2 or type 3 sections found) iTunesDB '%s' corrupt: Could not find tracklist (no mhsd type 1 section found) iTunesDB '%s' corrupt: mhsd expected at %ld. iTunesDB ('%s'): header length of mhsd hunk smaller than expected (%d<32). Aborting. iTunesDB corrupt: hunk length 0 for hunk at %ld in file '%s'. iTunesDB corrupt: no MHOD at offset %ld in file '%s'. iTunesDB corrupt: no SLst at offset %ld in file '%s'. iTunesDB corrupt: no section '%s' found in section '%s' starting at %ld. iTunesDB corrupt: number of mhip sections inconsistent in mhyp starting at %ld in file '%s'. iTunesDB corrupt: number of tracks (mhit hunks) inconsistent. Trying to continue.
 iTunesDB possibly corrupt: number of playlists (mhyp hunks) inconsistent. Trying to continue.
 iTunesStats file ('%s'): entry length smaller than expected (%d<18). iTunesStats file ('%s'): entry length smaller than expected (%d<32). usage: %s <device> [timezone]
 usage: %s <device|uuid|bus device> <mountpoint>
 Project-Id-Version: de
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-04-13 08:40+0000
PO-Revision-Date: 2016-04-12 23:34+0000
Last-Translator: Kai-Ove Pietsch <Unknown>
Language-Team: deutsch <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:52+0000
X-Generator: Launchpad (build 18115)
Language: 
 %s: Unerwartete Länge %d für genius_cuid!
 Auf Datei '%s' konnte nicht zugegriffen werden (%s). <Keine Einträge>
 <Unbenannt> Bildunterstützung ist nicht in libgpod einkompiliert worden. Es ist nicht möglich, die Fotoarchiv-Wiedergabeliste zu entfernen. Breche ab.
 Classic Classic (schwarz) Classic (silber) Farbe Farbe U2 Das Kontrollverzeichnis konnte nicht gefunden werden: '%s' (oder ähnlich). Konnte nicht auf Datei  '%s' zugreifen. Konnte nicht auf die Datei '%s' zugreifen. Foto nicht hinzugefügt. Konnte zugehöriges Musikstück (dbid: %s) für Bild-Eintrag nicht finden.
 Nicht auf dem iPod gefunden: '%s'
 '%s' konnte nicht zum Schreiben geöffnet werden. Die iPod-Datenbank auf %s wurde nicht gefunden. Konnte xml sysinfo nicht von %s lesen
 Konnte SysInfoExtended nicht nach %s schreiben Zieldatei '%s' scheint nicht auf dem iPod zu sein, welcher unter '%s' eingehängt ist. Das Verzeichnis existiert nicht. Unbekannter MHOD-Typ (%d) während der Analyse der iTunesDB gefunden. Es wird versucht fortzufahren.

 Fehler beim Hinzufügen des Fotos (%s) zum Fotoarchiv: %s
 Fehler beim Packen der iTunesCDB-Datei!
 Fehler beim Erstellen von '%s' (mkdir).
 Konnte iPod nicht initialisieren: Unbekannter Fehler
 Konnte iPod nicht initialisieren: %s
 Fehler beim Öffnen von '%s' zum Lesen (%s). Fehler beim Öffnen von '%s' zum Schreiben (%s). Fehler beim Lesen des iPod Fotoarchivs (%s).
 Fehler beim Lesen des iPod Fotoarchivs (%s).
Es wird versucht, ein neues Archiv zu erstellen.
 Fehler beim Lesen des iPod Fotoarchivs. Es wird versucht, ein neues Archiv zu erstellen.
 Fehler beim Lesen des iPod Fotoarchivs.
 Fehler beim Entfernen von '%s' (%s). Fehler beim Umbenennen von '%s' nach '%s' (%s). Fehler beim Schließen der Datei '%s' (%s). Fehler beim Lesen aus '%s' (%s). Fehler beim Schreiben in die Datei '%s' (%s). Fehler beim Schreiben der Albenliste (mhsd-Typ 4) Fehler beim Schreiben der Künstlerliste (mhsd-Typ 8) Fehler beim Schreiben der Titelliste (hths) Fehler beim Schreiben der Titelliste (mhsd-Typ 1) Fehler beim Schreiben von mshd-Typ 10 Fehler beim Schreiben von mshd-Typ 6 Fehler beim Schreiben von mshd-Typ 9 Fehler beim Schreiben von mhsd5-Wiedergabelisten Fehler beim Schreiben der Wiedergabelisten (hphs) Fehler beim Schreiben der speziellen Podcast-Wiedergabelisten (mhsd-Typ 3) Fehler beim Schreiben der Standardwiedergabelisten (mhsd-Typ 2) Fehler: '%s' ist kein Verzeichnis
 Graustufen Graustufen U2 Header ist zu klein für die iTunesCDB!
 Nicht zulässiger Dateiname: '%s'.
 Unzulässige Suche bei Offset %ld (Länge %ld) in Datei '%s'. Unzureichende Anzahl von Kommandozeilenargumenten.
 Ungültig Itdb_Track ID '%d' nicht gefunden.
 Die Länge einer Regel (%d) einer intelligenten Wiedergabeliste weicht von der erwarteten Länge ab. Es wird versucht fortzufahren.
 Die Bibliothek wurde ohne gdk-pixbuf-Unterstützung kompiliert. Fotos werden daher nicht unterstützt. Haupt-Wiedegabeliste Mini (1. Generation) Mini (2. Generation) Mini (blau) Mini (gold) Mini (grün) Mini (pink) Mini (silber) Mobile (1) Mobiltelefone Einhängepunkt wurde nicht gesetzt. Einhängepunkt wurde nicht gesetzt.
 Das 'Musik'-Verzeichnis konnte nicht gefunden werden: '%s' (oder ähnlich). Nano (1. Generation) Nano (2. Generation) Nano (schwarz) Nano (blau) Nano (grün) Nano (orange) Nano (pink) Nano (lila) Nano (rot) Nano (silber) Nano (weiß) Nano (gelb) Nano Video (3. Generation) Nano Video (4. Generation) Nano touch (6. Generation) Nano mit Kamera (5. Generation) Keine Verzeichnisse gefunden in '%s'. Keine OTG (On-The-Go) Wiedergabelistendatei: '%s' (mhpo-Header fehlt). Keine Wiedergabezähler Datei: '%s' (mhdp-Header fehlt). Keine iTunesDB Datei: '%s' (kein mhdb-Header). Anzahl der MHODs-Abschnitte in der mhip-Sektion an Offset %ld ist in der Datei '%s' inkonsistent. Anzahl der MHOD-Abschnitte in der mhyp-Sektion bei Offset %ld in der Datei '%s' ist inkonsistent. OTG-Wiedergabeliste OTG-Wiedergabeliste %d OTG (On-The-Go) Wiedergabelistendatei '%s' bezieht sich auf ein nicht existierendes Stück (%d). OTG (On-The-Go) Wiedergabelistendatei ('%s'): Länge der Einträge ist kleiner als erwartet (%d<4). OTG (On-The-Go) Wiedergabelistendatei ('%s'): Headerlänger kleiner als erwartet (%d<20). Pfad nicht gefunden: '%s' (oder ähnlich). Pfad nicht gefunden: '%s'. Foto Fotoarchiv Der Pfad zum Fotoverzeichnis konnte nicht gefunden werden: '%s' (oder ähnlich). Wiedergabezähler Datei ('%s'): Länge der Einträge kleiner als erwartet (%d<12). Wiedergabezähler Datei ('%s'): Headerlänge kleiner als erwartet (%d<96). Wiedergabeliste Podcasts Problem beim Erstellen eines Verzeichnisses oder einer Datei auf dem iPod: '%s'. Standard (1. Generation) Standard (2. Generation) Standard (3. Generation) Standard (4. Generation) Shuffle Shuffle (1. Generation) Shuffle (2. Generation) Shuffle (3. Generation) Shuffle (4. Generation) Shuffle (Schwarz) Shuffle (blau) Shuffle (Gold) Shuffle (grün) Shuffle (orange) Shuffle (pink) Shuffle (lila) Shuffle (rot) Shuffle (silber) Shuffle (Rostfrei) Gewünschtes Album '%s' nicht gefunden. Breche ab.
 Touch Touch (2. Generation) Touch (3. Generation) Touch (4. Generation) Touch (Silber) Vorschaubild konnte nicht geladen werden (scheint auf dem iPod vorhanden zu sein, aber die Bilddaten sind nicht verfügbar): Dateiname: '%s'
 Unerwarteter Fehler in itdb_photodb_add_photo_internal() beim Hinzufügen eines Fotos. Bitte teilen Sie uns diesen Fehler mit. Unerwarter Bildtyp in mhni: %d, Offset: %d
 Unerwarteter mhod Stringtyp: %d
 Unerwarteter mhsd-Index: %d
 Unbekannt Unbekannte Aktion (0x%x) in intelligenter Wiedergabeliste wird ignoriert.
 Unbekanntes Kommando: '%s'
 Unbekannte Aktion bei %ld: %x. Es wird trotzdem versucht, weiterzumachen.
 Syntax, um Fotos hinzuzufügen:
  %s add <Mountpoint> <Name des Albums> [<Dateiname(n)>]
  <Name des Albums> sollte als 'NULL' angegeben, um Fotos zum nur Haupalbum
  (Fotoarchiv) hinzuzufügen. Wenn keine Dateinamen angegeben werden, wird
  ein leeres Album erstellt.
 Syntax, um alle Fotos nach <Verzeichnis> zu kopieren:
  %s dump <mountpoint> <Verzeichnis>
 Syntax, um eine Liste der IDs aller Fotos auszugeben:
  %s list <mountpoint>
 Syntax, um Fotos aus dem Fotoarchiv zu entfernen:
  %s remove <Mountpoint> <Name des Albums> [<ID(s)>]
  <Name des Albums> sollte als 'Null' angegeben werden, um Fotos vollständig
  vom iPod zu entfernen. Wenn keine IDs angegeben werden, wird stattdessen
  das bezeichnete Fotoalbum entfernt.
  Vorsicht: IDs können sich verändern, wenn die Datenbank geschrieben wird.
 Video (1. Generation) Video (2. Generation) Video (schwarz) Video (weiß) Video U2 Warnung: Das Foto mit der ID <%d> konnte nicht gefunden werden. Ich mache mit dem nächsten Foto weiter...
 Falsche Anzahl an Kommandozeilenargumenten.
 Sie müssen das iPod-Modell angeben, bevor Fotos hinzugefügt werden können. Ihr iPod scheint Fotos nicht zu unterstützen. Eventuell müssen Sie auch nur das korrekte iPod-Model angeben. Zur Zeit haben Sie folgendes Modell eingestellt: 'x%s' (%s/%s). Header-Länge des Abschnitts '%s' ist kleiner als erwartet (%d < %d) an Offset %ld in Datei '%s'. iPad iPhone iPhone (1) iPhone (Schwarz) iPhone (Weiß) iPhone 3G iPhone 3GS iPhone 4 iPod Das 'iTunes'-Verzeichnis konnte nicht gefunden werden: '%s' (oder ähnlich). iTunesCDB »%s« konnte nicht entpackt werden Die iTunesDB '%s' ist beschädigt: Die Wiedergabelisten konnten nicht gefunden werden (keine mhsd-Abschnitte des Typs 2 oder 3 gefunden). Die iTunesDB '%s' ist beschädigt: Die Stückliste konnte nicht gefunden werden (kein mhsd-Abschnitt des Typs 1 gefunden). Die iTunesDB '%s' ist beschädigt: bei Offset %ld wird ein mhsd-Abschnitt erwartet. iTunesDB ('%s'): Headerlänge des mhsd-Hunks is kleiner als erwartet (%d<32). Breche ab. iTunesDB beschädigt: Hunklänge ist Null für den Hunk an Offset %ld in Datei '%s'. iTunesDB beschädigt: Kein MHOD bei Offset %ld in Datei '%s'. iTunesDB beschädigt: Kein SLst-Eintrag bei Offset %ld in Datei '%s'. iTunesDB beschädigt: Kein '%s'-Eintrag in Sektion '%s' (startend bei %ld) gefunden. Die iTunesDB ist beschädigt: Die Anzahl der mhip-Sektionen im mhyp-Abschnit startend bei %ld in Datei '%s' ist inkonsistent. Die iTunesDB ist beschädigt: Anzahl der Stücke (mhit-Hunks) ist inkonsistent. Versuche fortzufahren.
 iTunesDB möglicherweise beschädigt: Anzahl der Wiedergabelisten (mhyp-Hunks) ist inkonsistent. Versuche fortzufahren.
 iTunesStats Datei ('%s'): Länge der Einträge kleiner als erwartet (%d<18). iTunesStats-Datei (»%s«): Länge des Eintrags kürzer als erwartet (%d<32). Verwendung: %s <Gerät> [Zeitzone]
 Verwendung: %s <Gerät|Uuid|Bus-Gerät> <Einhängepunkt>
 