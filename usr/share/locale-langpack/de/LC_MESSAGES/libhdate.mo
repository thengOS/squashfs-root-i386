��    �      ,  3  <        E   	  �   O  ,     W   9  :   �  
   �     �     �     �     �     �          	               "     %     +  
   4     ?     L     R     c     k  	   t     ~     �     �     �     �     �     �     �     �     �     �     �     �     �     �  
                            %     4     =  �  I  %   �     �                )     6     <     @     H     L     Q     U     Z     c  
   k     v     ~     �     �     �  
   �  
   �     �     �     �     �     �     �     �     �  8   �     /     7     C  	   J     T     X     _     f     l     z     �     �     �     �     �     �     �     �     �     �     �  	   �  
   �     �     �                    !     0     @     D     M  	   Q     [     c     j  
   r     }     �     �     �     �     �     �     �  	   �     �     �     �     �     �     �                       &      /   �   5      �      �      �      �      �      !  
   
!     !     !     !!     &!     4!  �   @!  ?  >"     ~#  
   �#     �#     �#     �#  	   �#     �#     �#     �#     �#     �#  
   �#     �#     �#  	   $     $     $     .$     =$     I$  
   W$     b$     s$     �$     �$     �$     �$     �$  -   �$     �$     �$  (   %     -%     =%     I%     U%  "   ^%  �  �%  3   A,     u,     },  #   �,     �,     �,     �,     �,  )   
-     4-     ;-     M-     S-  
   X-     c-  	   j-     t-     }-     �-     �-     �-     �-     �-     �-  3   �-  4   �-  
   0.  >   ;.  4   z.  4   �.     �.  0   �.     /     (/  �  5/  X   �0  �   71  >   2  e   U2  N   �2  
   
3     3     *3     /3     63     >3     B3     H3     W3     [3     b3     e3     k3  
   t3     3     �3     �3     �3  	   �3  
   �3     �3     �3  	   �3  
   �3  	   �3     �3     �3     4     4     4     4     4     #4     (4     64     F4     R4     V4     ^4     a4     i4     4     �4  w  �4  /   <     C<     V<     h<     q<     �<     �<     �<     �<     �<     �<     �<  	   �<     �<     �<     �<     �<     �<     �<     �<  
   �<  
   �<     
=     =     =     "=     (=     .=     5=     C=  9   G=     �=     �=     �=  
   �=     �=     �=     �=     �=     �=     �=     �=     �=     �=     �=     �=     �=     >     	>     >     >     >  
   '>     2>     >>     K>     S>     Y>     h>     n>     �>     �>     �>     �>  	   �>     �>     �>     �>  
   �>     �>     �>     �>     �>      ?     ?      ?     &?  	   -?     7?     :?     B?     Q?  	   X?     b?     i?     w?     �?     �?     �?  �   �?     [@  
   ^@     i@     u@     }@     �@  
   �@     �@     �@     �@     �@  
   �@    �@  T  �A     &C  
   -C     8C     AC     QC     YC     bC  	   iC     sC  	   zC     �C     �C     �C     �C     �C     �C     �C     �C     �C     �C  
    D     D     D     5D     DD     PD     fD     jD  F   oD     �D  	   �D  )   �D     �D     E     E     "E  *   +E    VE  >   ^M     �M     �M  (   �M  <   �M     N     *N  "   :N  A   ]N     �N     �N     �N     �N     �N     �N  	   �N     �N     �N     �N     O     O     !O     -O     ?O  =   MO  2   �O     �O  F   �O  A   P  A   SP     �P  F   �P     �P     �P         7   @   �   ?          >       �       �   �   	   A   �   �   �   [   #           �       �   �               �   6   L   &   �           9               }   (   �   �           n   )   �   =   `   �   r       �   b          �          �   1       _   �   �   o       �       �   g   �   �       �       �   �   y   Y   B   {          �   X   "   �   �   G   �   �   �   �   5   �   �           �   Q   �   U         V   ,   �   �   �       �   %   �       �               m          C      a   q       �   �   �   �   N       �   �   �   �               ]       �   3              \       $               v       s   J      c          �       �   O          �   �   Z   t   H   �   �   D   
   .   �   �               �                  S                    �       p   '   �   -   P           �       �   u   �       �   k   �   �       �   �   0   j   w               K   2   |   �   T   �      �   �   ;   z             W   /      �                     �          *      �   �       �   �   �   ^   E   8   �   �   �   4         d   �   �              �      <   �   �   :   �   I       i   !   ~   �          h       �   �   x   +   l   e       �       R   F   f       M   �       �    ALERT: -m (--menu) option specified, but no menu items in config file ALERT: The information displayed is for today's Hebrew date.
              Because it is now after sunset, that means the data is
              for the Gregorian day beginning at midnight. ALERT: guessing... will use co-ordinates for ALERT: options --parasha, --shabbat, --footnote are not supported in 'three-month' mode ALERT: time zone not entered, using system local time zone Achrei Mot Achrei Mot-Kedoshim Adar Adar I Adar II Apr April Asara B'Tevet Aug August Av Balak Bamidbar Bechukotai Beha'alotcha Behar Behar-Bechukotai Beijing Bereshit Beshalach Bo Buenos Aires Chanukah Chayei Sara Cheshvan Chukat Chukat-Balak Dec December Devarim Eikev Elul Emor Erev Shavuot Erev Yom Kippur Family Day Feb February Fri Friday Gregorian date Ha'Azinu Hebrew Date Hebrew calendar
OPTIONS:
   -1 --one-month     over-ride config file setting if you had set
                      option --three-month as a default there
   -3 --three-month   displays previous/next months
                      side by side. requires 127 columns
   -b --bidi          prints hebrew in reverse (visual)
      --visual
      --no-bidi       over-ride config file setting if you had set
      --no-visual     option -bidi as a default there
   -c --colorize      displays in calming, muted tones
      --no-color      over-ride a --colorize setting in your config file
   -d --diaspora      use diaspora reading and holidays.
   -f --footnote      print descriptive notes of holidays
      --no-footnote   over-ride a footnote setting in your config file
   -h --html          output in html format to stdout
   -H --hebrew        print hebrew information in Hebrew
   -i                 use external css file "./hcal.css".
   -I --israel        override a diaspora default
      --no-reverse    do not highlight today's date
   -m --menu          prompt user-defined menu from config file
   -p --parasha       print week's parasha on each calendar row
   -q --quiet-alerts  suppress warning messages
   -s --shabbat       print Shabbat times and parshiot
      --candles       modify default minhag of 20 minutes. (17<n<91)
      --havdalah      modify default minhag of 3 stars. (19<n<91 minutes)
   -z --timezone nn   timezone, +/-UTC
   -l --latitude yy   latitude yy degrees. Negative values are South
   -L --longitude xx  longitude xx degrees. Negative values are West

All options can be made default in the config file, or menu-ized for
easy selection. Hmmm, ... hate to do this, really ... Hol hamoed Pesach Hol hamoed Sukkot Honolulu Hoshana raba Iyyar Jan January Jul July Jun June Kedoshim Ki Tavo Ki Teitzei Ki Tisa Kislev Korach L (Longitude) L (Longitue) Lag B'Omer Lech-Lecha London Los Angeles Mar March Masei Matot Matot-Masei May Memorial day for fallen whose place of burial is unknown Metzora Mexico City Miketz Mishpatim Mon Monday Moscow Nasso New York City Nisan Nitzavim Nitzavim-Vayeilech Noach Nov November Oct October Parashat Paris Pekudei Pesach Pesach II Pesach VII Pesach VIII Pinchas Purim Rabin memorial day Re'eh Rosh Hashana I Rosh Hashana II Sat Saturday Sep September Sh'lach Sh'vat Shavuot Shavuot II Shemot Shmini Shmini Atzeret Shoftim Shushan Purim Simchat Torah Sivan Sukkot Sukkot II Sun Sunday Ta'anit Esther Tammuz Tashkent Tazria Tazria-Metzora Tel-Aviv Terumah Tetzaveh Tevet This seems to be to be your first time using this version.
Please read the new documentation in the man page and config
file. Attempting to create a config file ... Thu Thursday Tish'a B'Av Tishrei Toldot Tu B'Av Tu B'Shvat Tue Tuesday Tzav Tzom Gedaliah Tzom Tammuz Usage: hcal [options] [coordinates timezone] ] [[month] year]
       coordinates: -l [NS]yy[.xxx] -L [EW]xx[.xxx]
                    -l [NS]yy[:mm[:ss]] -L [EW]xx[:mm[:ss]]
       timezone:    -z nn[( .nn | :mm )]
Try 'hcal --help' for more information Usage: hdate [options] [coordinates timezone] [[[day] month] year]
       hdate [options] [coordinates timezone] [julian_day]

       coordinates: -l [NS]yy[.yyy] -L [EW]xx[.xxx]
                    -l [NS]yy[:mm[:ss]] -L [EW]xx[:mm[:ss]]
       timezone:    -z nn[( .nn | :mm )]
Try 'hdate --help' for more information Vaera Vaetchanan Vayakhel Vayakhel-Pekudei Vayechi Vayeilech Vayera Vayeshev Vayetzei Vayigash Vayikra Vayishlach Vezot Habracha Wed Wednesday Yitro Yitzhak Rabin memorial day Yom HaAtzma'ut Yom HaShoah Yom HaZikaron Yom Kippur Yom Yerushalayim Zeev Zhabotinsky day Zhabotinsky day candle-lighting config file created day degrees enter your selection, or <return> to continue error eve of  failure attempting to create config file failure closing first_light first_stars havdalah hcal - Hebrew calendar
version 1.6 hdate - display Hebrew date information
OPTIONS:
   -b --bidi          prints hebrew in reverse (visual)
      --visual
   -c                 print Shabbat start/end times.
      --candles       modify default minhag of 20 minutes. (17<n<91)
      --havdalah      modify default minhag of 3 stars. (19<n<91 minutes)
   -d --diaspora      use diaspora reading and holidays.
   -h --holidays      print holidays.
   -H                 print only it is a holiday.
   -i --ical          use iCal formated output.
   -j --julian        print Julian day number.
   -m --menu          prompt user-defined menu from config file
   -o --omer          print Sefirat Haomer.
   -q --quiet-alerts  suppress warning messages
   -r --parasha       print weekly reading on saturday.
   -R                 print only if there is a weekly reading on Shabbat.
   -s --sun           print sunrise/sunset times.
   -S --short-format  print using short format.
   -t                 print day times: first light, talit, sunrise,
                            midday, sunset, first stars, three stars.
   -T --table         tabular output, suitable for spreadsheets

   -z --timezone nn   timezone, +/-UTC
   -l --latitude yy   latitude yy degrees. Negative values are South
   -L --longitude xx  longitude xx degrees. Negative values are West

   --hebrew           forces Hebrew to print in Hebrew characters
   --yom              force Hebrew prefix to Hebrew day of week
   --leshabbat        insert parasha between day of week and day
   --leseder          insert parasha between day of week and day
   --not-sunset-aware don't display next day if after sunset

All options can be made default in the config file, or menu-ized for
easy selection. hdate - display Hebrew date information
version 1.6 holiday in the Omer is incompatible with a longitude of is non-numeric or out of bounds is not a valid option l (latitude) memory allocation failure menu selection received was out of bounds midday missing parameter month none omer count option parameter parashat sun_hour sunrise sunset talit three_stars time zone value of today is day too many arguments; after options max is dd mm yyyy too many parameters received. expected  [[mm] [yyyy] translator using co-ordinates for the equator, at the center of time zone valid latitude parameter missing for given longitude valid longitude parameter missing for given latitude year your custom menu options (from your config file) z (time zone) z (timezone) Project-Id-Version: libhdate
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2012-01-08 19:48-0500
PO-Revision-Date: 2013-07-04 18:20+0000
Last-Translator: Carsten Gerlach <carsten-gerlach@gmx.de>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:52+0000
X-Generator: Launchpad (build 18115)
 WARNUNG: Option -m (--menu) angegeben, aber kein Menüeintrag in der Konfigurationsdatei WARNUNG: Die angezeigten Informationen beziehen sich auf das heutige hebräische Daten.
              Da es gerade nach Sonnenuntergang ist, beziehen sich die Daten ab
              Mitternacht auf den gregorianischen Tag. WARNUNG: Es wird geraten … Koordinaten werden verwendet für ACHTUNG: Die Optionen --parasha, --shabbat und --footnote werden im 3-Monatsmodus nicht unterstützt. WARNUNG: Zeitzone wurde nicht angegeben, die lokale Systemzeit wird verwendet. Achare Mot Achare Mot-Kedoschim Adar Adar I Adar II Apr April Assara B'Tevet Aug August Aw Balak Bemidbar Bechukotaj Beha'alotcha Behar Behar-Bechukotaj Peking Bereschit Beschalach Bo Buenos Aires Chanukkah Chaje Sara Cheschwan Chukkat Chukkat-Balak Dez Dezember Dewarim Ekew Elul Emor Erev Schawuot Erev Jom Kippur Familientag Feb Februar Fr Freitag Gregorianisches Datum Ha'Asinu Hebräisches Datum Hebräischer Kalender
OPTIONEN:
   -1 --one-month     Überschreibt die Einstellung in der Konfigurationsdatei
                      wenn die Option --three-month als Standard gesetzt ist
   -3 --three-month   Zeigt vorherigen/nächsten Monat nebeneinander,
                      benötigt 127 Spalten.
   -b --bidi          Schreibt Hebräisch in umgekehrter Reihenfolge (visuell)
      --visual
      --no-bidi       Überschreibt die Einstellung in der Konfigurationsdatei
      --no-visual     wenn die Option -bidi als Standard gesetzt ist
   -c --colorize      Farbige Ausgabe
      --no-color      Überschreibt --colorize in der Konfigurationsdatei
   -d --diaspora      Nutzt Diaspora-Einstellungen und -Feiertage
   -f --footnote      Gibt Hinweise zu Feiertagen aus
      --no-footnote   Überschreibt --footnote in der Konfigurationsdatei
   -h --html          HTML-Ausgabe auf der Standardausgabe
   -H --hebrew        Gibt hebräische Informationen in Hebräisch aus
   -i                 Nutzt externe CSS-Datei "./hcal.css".
   -I --israel        Überschreibt die Einstellung --diaspora
      --no-reverse    Hebt aktuelles Datum nicht hervor
   -m --menu          Fragt Benutzer definiertes Menü aus der 
                      Konfigurationsdatei ab
   -p --parasha       Gibt Parascha der Woche in jeder Kalenderspalte aus
   -q --quiet-alerts  Unterdrückt Warnungen
   -s --shabbat       Gibt Sabbatzeiten und Parascha aus
      --candles       Ändert Standard-Minhag von 20 Minuten (17<n<91)
      --havdalah      Ändert Standard-Minhag von 3 Minuten (19<n<91)
   -z --timezone nn   Zeitzone/-UTC
   -l --latitude yy   Geographische Breite in Grad. Negative Werte sind Süden
   -L --longitude xx  Geographische Länge in Grad. Negative Werte sind Westen

Alle Optionen können in der Konfigurationsdatei eingestellt werden 
oder für eine einfache Auswahl in Menüform gebracht werden. Hmmm, … ich tue dies wirklich nicht gerne … Hol hamoed Pessach Hol hamoed Sukkot Honolulu Hoschana Rabba Ijjar Jan Januar Jul Juli Jun Juni Kedoschim Ki Tawo Ki Teze Ki Tissa Kislew Korach L (Längengrad) L (Längengrad) Lag B'Omer Lech-Lecha London Los Angeles Mär März Masse Mattot Mattot-Massee Mai Gedenktag für Gefallene, deren Grabstätte unbekannt ist Mezora Mexiko-Stadt Mikez Mischpatim Mo Montag Moskau Nasso New York City Nisan Nizavim Nizawim-Wajelech Noach Nov November Okt Oktober Parashat Paris Pkudei Pessach Pessach II Pessach VII Pessach VIII Pinchas Purim Rabi-Gedenktag Re'eh Rosch ha-Schana I Rosch ha-Schana II Sa Samstag Sep September Schelach Lecha Schevat Schawuot Shawuot II Schemot Schemini Schmini Azeret Schoftim Schoschan Purim Simchath Thorah Siwan Sukkot Sukkot II So Sonntag Ta'anit Esther Tammus Taschkent Tasria Tasria-Mezora Tel-Aviv Truma Tezave Tewet Dies scheint das erste Mal zu sein, dass Sie diese Version verwenden.
Bitte lesen Sie die neue Dokumentation in der Handbuchseite und config-Datei.
Es wird versucht eine config-Datei zu erstellen … Do Donnerstag Tish'a B'Av Tischri Toldot Tu B'Av Tu B'ShWaT Di Dienstag Zaw Zom Gedaliah Zom Tammus Aufruf: hcal [Optionen] [Koordinaten Zeitzone] ] [[Monat] Jahr]
       Koordinaten: -l [NS]yy[.xxx] -L [EW]xx[.xxx]
                    -l [NS]yy[:mm[:ss]] -L [EW]xx[:mm[:ss]]
       Zeitzone:    -z nn[( .nn | :mm )]
Probieren Sie »hcal --help« für weitere Informationen Aufruf: hdate [Optionen] [Koordinaten Zeitzone] [[[Tag] Monat] Jahr]
       hdate [Optionen] [Koordinaten Zeitzone] [julian_Tag]

       Koordinaten: -l [NS]yy[.yyy] -L [EW]xx[.xxx]
                    -l [NS]yy[:mm[:ss]] -L [EW]xx[:mm[:ss]]
       Zeitzone:    -z nn[( .nn | :mm )]
Probieren Sie »hdate --help« für weitere Informationen Wa'era Waetchanan Vajachel Wajakhel-Pekude Vajechi Wajelech Wajera Vajeschev Wajeze Vajigasch Vajikra Vajischlach Wesot Habracha Mi Mittwoch Jitro »Rabbi Jitzchak«-Gedenktag Yom HaAtzma'ut Jom HaSchoah Jom HaSikaron Jom Kippur Jom Jeruschalajim »Zeev Jabotinsky«-Tag Jabotinsky-Tag Kerzenlicht config-Datei erstellt Tag Grad Geben Sie Ihre Auswahl ein oder drücken Sie <Eingabe> um fortzufahren Fehler Auge von  config-Datei konnte nicht erstellt werden Fehler beim Schließen erstes_Licht erste_Sterne havdalah hcal – Hebräischer Kalender
Version 1.6 hdate - Gibt Hebräische Datumsinformationen aus
OPTIONEN:
   -b --bidi          Schreibt Hebräisch in umgekehrter Reihenfolge (visuell)
      --visual
   -c                 Gibt Anfangs- und Endzeiten des Sabbat aus
      --candles       Ändert Standard-Minhag von 20 Minuten (17<n<91)
      --havdalah      Ändert Standard-Minhag von 3 Minuten (19<n<91)
   -d --diaspora      Nutzt Diaspora-Einstellungen und -Feiertage
   -h --holidays      Gibt Feiertage aus
   -H                 Gibt nur die Information aus, ob es ein Feiertag ist
   -i --ical          Ausgabe im iCal-Format
   -j --julian        Gibt Julianische Tagesnummer aus
   -m --menu          Fragt Benutzer definiertes Menü aus der 
                      Konfigurationsdatei ab
   -o --omer          Gibt Sefirat Ha'omer aus
   -q --quiet-alerts  Unterdrückt Warnungen
   -r --parasha       Gibt die wöchentliche Lesung am Samstag aus
   -R                 Ausgabe nur, wenn eine wöchentliche Lesung am Sabbat ist
   -s --sun           Gibt Sonnenaufgangs- und Sonnenuntergangszeiten aus 
   -S --short-format  Ausgabe im kurzen Format
   -t                 Gibt die Tageszeiten aus: Tagesanbruch, Tallit, Sonnenaufgang,
                      Mittag, Sonnenuntergang, Erste Sterne, Drei Sterne
   -T --table         Ausgabe im Tabellenformat, geeignet für Kalkulationstabellen

   -z --timezone nn   Zeitzone, +/-UTC
   -l --latitude yy   Geographische Breite in Grad. Negative Werte sind Süden
   -L --longitude xx  Geographische Länge in Grad. Negative Werte sind Westen

   --hebrew           Gibt hebräische Informationen in Hebräisch aus
   --yom              Gibt hebräischen Präfix für hebräischen Wochentag aus
   --leshabbat        Fügt Parascha zwischen Tag der Woche und Tag ein
   --leseder          Fügt Parascha zwischen Tag der Woche und Tag ein
   --not-sunset-aware Keine Anzeige des nächsten Tages nach Sonnenuntergang

Alle Optionen können in der Konfigurationsdatei eingestellt werden 
oder für eine einfache Auswahl in Menüform gebracht werden. hdate – Hebräische Datumsinformationen anzeigen
Version 1.6 Feiertag im Omer ist kompatibel mit einem Längengrad von ist nicht numerisch oder außerhalb des zulässigen Bereichs Keine gültige Option l (Breitengrad) Fehler beim Anfordern von Speicher Die empfangene Menüauswahl war außerhalb des gültigen Bereichs Mittag Fehlender Parameter Monat Nichts omer-Zählung Option Parameter parashat Sonnen_stunde Sonnenaufgang Sonnenuntergang talit drei_Sterne Zeitzonenwert von Heute ist Tag Zu viele Argumente; Nach Optionen ist das Maxiumum dd mm yyyy Zu viele Parameter empfangen. [[mm][yyyy] erwartet Übersetzer Koordinaten für den Equator werden verwendet, im Zentrum der Zeitzone gültiger Breitengradparameter fehlt für angegebenen Längengrad gültiger Längengradparameter fehlt für angegebenen Breitengrad Jahr Ihre benutzerdefinierten Menüoptionen (aus Ihrer Konfigurationsdatei) z (Zeitzone) z (Zeitzone) 