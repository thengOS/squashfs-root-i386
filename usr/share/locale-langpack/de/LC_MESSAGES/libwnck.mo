��    �      T  7  �      h     i     n  	   {     �     �     �     �     �  "   �     �     �     �     �          '     :     P     b     q     �  N   �  g   �  G   C  :   �  E   �  A     *   N  *   y  )   �  (   �  7   �  s   /  '   �  ?   �  <        H  ,   Y  $   �     �     �     �  /   �  8   !  \   Z  b   �  d     ]     d   �  f   B  b   �  m     `   z     �  "   �  !     /   8     h     {     �     �  
   �  >   �  G   �  5   3     i     �  `   �  G   �  	   A     K     Y      w  #   �  #   �     �  5   �  !   1   $   S   '   x   '   �   9   �   $   !  )   '!  5   Q!      �!     �!     �!  	   �!     �!     �!  <   "  <   D"  :   �"     �"     �"     �"     #      #     6#     ;#  )   B#  ,   l#  	   �#     �#     �#     �#  #   �#     $  %   $  (   @$  (   i$  +   �$     �$  )   �$     �$  �   %     �%     �%     &     &     )&  *   :&  -   e&  -   �&  0   �&     �&     '  (   '  *   E'     p'  
   �'     �'     �'  &   �'     �'  !   �'     (     #(     7(     G(  "   S(     v(      �(     �(     �(     �(     �(     �(  L   )  8   T)     �)     �)     �)     �)     �)     �)  E   �)     8*     E*     T*  :   b*     �*     �*     �*     �*  <   �*  .   +     H+  	   L+     V+     X+     u+  
   |+     �+     �+     �+     �+     �+     �+     �+     �+     �+      ,     ,     ,     ,     *,  
   0,  
   ;,  
   F,     Q,     Z,     p,  	   �,     �,     �,     �,  	   �,     �,     �,     �,     �,     �,     
-     -     -     -      -     &-  
   --     8-     F-     T-     c-     i-     p-     }-     �-     �-     �-  
   �-     �-     �-  
   �-     �-     �-     �-     .     .     !.  �  0.     �/     �/     �/     �/     �/     �/     �/     0  )   0     D0     _0     s0     �0     �0     �0     �0     �0     �0     1     $1  s   +1  r   �1  [   2  D   n2  X   �2  Y   3  *   f3  *   �3  '   �3  ,   �3  @   4  o   R4  )   �4  R   �4  Q   ?5     �5  ,   �5  4   �5     6  #   %6     I6  9   \6  D   �6  j   �6  m   F7  s   �7  l   (8  }   �8  �   9  �   �9  �   :  p   �:     %;  *   E;  "   p;  0   �;     �;     �;     �;     �;     �;  F   <  S   S<  ?   �<  !   �<     	=  p   =  Z   �=     �=     �=  "   
>  2   ->  $   `>     �>  )   �>  <   �>  (   	?  8   2?  *   k?  "   �?  =   �?  &   �?  .   @  9   M@     �@     �@     �@     �@     �@     �@  T   A  T   `A  R   �A  &   B  &   /B  (   VB  %   B  (   �B     �B     �B  '   �B  *   C  	   -C     7C     KC     cC  +   �C     �C  8   �C  ;    D  8   <D  >   uD     �D  2   �D     �D  �   
E     F     F     .F     DF     UF  A   gF  D   �F  A   �F  G   0G     xG     �G  $   �G  6   �G     
H     H  #   'H     KH  5   OH  *   �H  5   �H     �H     �H     I     &I  #   8I     \I  "   uI     �I     �I     �I     �I     �I  s   �I  ^   pJ  -   �J     �J     K     K     (K     7K  c   GK     �K     �K     �K  D   �K     )L     FL     eL     L  >   �L  )   �L     �L  	   �L     �L  (   �L     #M     /M     @M     MM     lM     }M     �M     �M     �M     �M     �M  
   �M     �M     �M     N     N     #N     ,N     ?N  
   RN     ]N     sN  	   �N     �N     �N  
   �N  	   �N     �N     �N     �N     O     O     %O  
   .O     9O     IO  	   UO  
   _O  '   jO     �O     �O     �O     �O  
   �O     �O     �O     P     P     &P     FP     WP     sP     �P  	   �P  	   �P  	   �P     �P     �P     �P     �       �   v   |   Q   �   �   	   �   t       �   �           �          �               S   �       �   O           �   �          �       ,   �   �   �      �          <       (           %   `   i   2   �   a   >   g      �           �       /                �       �   �   k   V   A       �   �       �   �   n   "   5                 [      K   H       �   9   �   Z   m      y       q       �   �   �   @   I               f          R           L   �       �   +   $   N   �   =   �   �   �   c   o          &   �       z       �   }   �   U   F   #       )   �   �   �       �               �      �   h           �   7   .       G       u       �       �           �           �       �   �       �          -       Y       �   �   �   �   4   �   �      l   T   �   b   ?   p   �   �          �   r   �   �   �      �   �   '   �   �   �      {   ]      :              
   �       M   �   �       �   �   �   8          �   s   �   �       W   �   �   3   X   �   �   \   !   *       B       P   �                   j   �   �   w                     �   J   �          �   6   �   �   �       �          _   ^   D   ~   0   e   ;   �      �   1   �   E           �   C      d   �       �   x   �    "%s" %1$s%2$s%3$s %d ("%s") %d: %s
 %lu (%s) %lu: %s
 ,  <name unset> <no EWMH-compliant window manager> <no viewport> <unset> Action not allowed
 Activate the window Activate the workspace Active Window: %s
 Active Workspace: %s
 Alias of --window Always On _Top Bottom Neighbor: %s
 CLASS Cannot change the workspace layout on the screen: the layout is already owned
 Cannot interact with application having its group leader with XID %lu: the application cannot be found
 Cannot interact with class group "%s": the class group cannot be found
 Cannot interact with screen %d: the screen does not exist
 Cannot interact with window with XID %lu: the window cannot be found
 Cannot interact with workspace %d: the workspace cannot be found
 Change the X coordinate of the window to X Change the Y coordinate of the window to Y Change the height of the window to HEIGHT Change the name of the workspace to NAME Change the number of workspaces of the screen to NUMBER Change the type of the window to TYPE (valid values: normal, desktop, dock, dialog, toolbar, menu, utility, splash) Change the width of the window to WIDTH Change the workspace layout of the screen to use NUMBER columns Change the workspace layout of the screen to use NUMBER rows Class Group: %s
 Class resource of the class group to examine Click this to switch to workspace %s Click to start dragging "%s" Click to switch to "%s" Close the window Conflicting options are present: --%s and --%s
 Conflicting options are present: --%s or --%s, and --%s
 Conflicting options are present: a window should be interacted with, but --%s has been used
 Conflicting options are present: an application should be interacted with, but --%s has been used
 Conflicting options are present: class group "%s" should be interacted with, but --%s has been used
 Conflicting options are present: screen %d should be interacted with, but --%s has been used
 Conflicting options are present: windows of an application should be listed, but --%s has been used
 Conflicting options are present: windows of class group "%s" should be listed, but --%s has been used
 Conflicting options are present: windows of workspace %d should be listed, but --%s has been used
 Conflicting options are present: windows or workspaces of screen %d should be listed, but --%s has been used
 Conflicting options are present: workspace %d should be interacted with, but --%s has been used
 Current workspace: "%s" Error while parsing arguments: %s
 Geometry (width, height): %d, %d
 Geometry (x, y, width, height): %d, %d, %d, %d
 Group Leader: %lu
 Group Name: %s
 HEIGHT Icon Name: %s
 Icons: %s
 Invalid argument "%d" for --%s: the argument must be positive
 Invalid argument "%d" for --%s: the argument must be strictly positive
 Invalid argument "%s" for --%s, valid values are: %s
 Invalid value "%s" for --%s Left Neighbor: %s
 List windows of the application/class group/workspace/screen (output format: "XID: Window Name") List workspaces of the screen (output format: "Number: Workspace Name") Ma_ximize Ma_ximize All Make the window always on top Make the window appear in pagers Make the window appear in tasklists Make the window below other windows Make the window fullscreen Make the window have a fixed position in the viewport Make the window not always on top Make the window not appear in pagers Make the window not appear in tasklists Make the window not below other windows Make the window not have a fixed position in the viewport Make the window quit fullscreen mode Make the window visible on all workspaces Make the window visible on the current workspace only Maximize horizontally the window Maximize the window Maximize vertically the window Mi_nimize Mi_nimize All Minimize the window Move the viewport of the current workspace to X coordinate X Move the viewport of the current workspace to Y coordinate Y Move the window to workspace NUMBER (first workspace is 0) Move to Another _Workspace Move to Workspace R_ight Move to Workspace _Down Move to Workspace _Left Move to Workspace _Up NAME NUMBER NUMBER of the screen to examine or modify NUMBER of the workspace to examine or modify Name: %s
 No Windows Open Number of Windows: %d
 Number of Workspaces: %d
 On Screen: %d (Window Manager: %s)
 On Workspace: %s
 Options to list windows or workspaces Options to modify properties of a screen Options to modify properties of a window Options to modify properties of a workspace PID: %s
 Position in Layout (row, column): %d, %d
 Possible Actions: %s
 Print or modify the properties of a screen/workspace/window, or interact with it, following the EWMH specification.
For information about this specification, see:
	http://freedesktop.org/wiki/Specifications/wm-spec Resource Class: %s
 Right Neighbor: %s
 Screen Number: %d
 Session ID: %s
 Shade the window Show options to list windows or workspaces Show options to modify properties of a screen Show options to modify properties of a window Show options to modify properties of a workspace Show the desktop Showing the desktop: %s
 Start moving the window via the keyboard Start resizing the window via the keyboard Startup ID: %s
 State: %s
 Stop showing the desktop TYPE Tool to switch between visible windows Tool to switch between windows Tool to switch between workspaces Top Neighbor: %s
 Transient for: %lu
 Un_minimize All Unma_ximize Unmaximize horizontally the window Unmaximize the window Unmaximize vertically the window Unmi_nimize Unminimize the window Unshade the window Untitled application Untitled window Viewport cannot be moved: the current workspace does not contain a viewport
 Viewport cannot be moved: there is no current workspace
 Viewport position (x, y): %s
 WIDTH Window List Window Manager: %s
 Window Selector Window Type: %s
 Window cannot be moved to workspace %d: the workspace does not exist
 Workspace %d Workspace %s%d Workspace 1_0 Workspace Layout (rows, columns, orientation): %d, %d, %s
 Workspace Name: %s
 Workspace Number: %d
 Workspace Switcher X X window ID of the group leader of an application to examine X window ID of the window to examine or modify XID XID: %lu
 Y _Always on Visible Workspace _Close _Close All _Move _Only on This Workspace _Resize _Unmaximize All above all workspaces below change fullscreen mode change workspace close desktop dialog window dock or panel false fullscreen make above make below maximize maximize horizontally maximize vertically maximized maximized horizontally maximized vertically minimize minimized move needs attention no action possible normal normal window pin pinned resize set shade shaded skip pager skip tasklist splash screen startupIDnone stick sticky tearoff menu tearoff toolbar true unmake above unmake below unmaximize unmaximize horizontally unmaximize vertically unminimize unpin unshade unstick utility window windownone workspacenone Project-Id-Version: libwnck
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-03 23:36+0000
PO-Revision-Date: 2016-05-11 08:30+0000
Last-Translator: Tobias Bannert <tobannert@gmail.com>
Language-Team: German <gnome-de@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:08+0000
X-Generator: Launchpad (build 18115)
 »%s« %1$s%2$s%3$s %d (»%s«) %d: %s
 %lu (%s) %lu: %s
 ,  <Name nicht eingestellt> <keine EWMH-kompatible Fensterverwaltung> <kein Darstellungsbereich> <nicht eingestellt> Aktion nicht erlaubt
 Fenster aktivieren Arbeitsfläche aktivieren Aktives Fenster: %s
 Aktive Arbeitsfläche: %s
 Alias von --window Immer im V_ordergrund Unterer Nachbar: %s
 KLASSE Arbeitsflächenanordnung auf dem Bildschirm konnte nicht geändert werden: Die Gestaltung ist bereits in Benutzung
 Kann nicht Anwendung mit dem Gruppenleiter mit der XID %lu bearbeiten: Die Anwendung konnte nicht gefunden werden
 Kann nicht Klassengruppe »%s« bearbeiten: Die Klassengruppe konnte nicht gefunden werden
 Kann nicht Bildschirm %d bearbeiten: Der Bildschirm existiert nicht
 Kann nicht Fenster mit der XID %lu bearbeiten: Das Fenster konnte nicht gefunden werden
 Kann nicht Arbeitsfläche %d bearbeiten: Die Arbeitsfläche konnte nicht gefunden werden
 Ändert die X-Koordinate des Fensters zu X Ändert die Y-Koordinate des Fensters zu Y Ändert die Höhe des Fensters zu HÖHE Ändert den Namen der Arbeitsfläche zu NAME Ändert die Anzahl der Arbeitsflächen des Bildschirms zu NUMMER Ändert den Fenstertypen zu TYP (erlaubte Werte: normal, desktop, dock, dialog, toolbar, menu, utility, splash) Ändert die Breite des Fensters zu BREITE Ändert die Arbeitsflächenanordnung des Bildschirms um NUMMER Spalten zu benutzen Ändert die Arbeitsflächenanordnung des Bildschirms um NUMMER Reihen zu benutzen Klassengruppe: %s
 Klassenressource der zu bearbeitenden Klasse Hier anklicken, um zur Arbeitsfläche %s zu wechseln Anklicken, um »%s« zu ziehen Anklicken, um zu »%s« zu wechseln Fenster schließen Es bestehen sich widersprechende Optionen: --%s und --%s
 Es bestehen sich widersprechende Optionen: --%s oder --%s, und --%s
 Es bestehen sich widersprechende Optionen: Ein Fenster soll bearbeitet werden, aber es wurde --%s benutzt
 Es bestehen sich widersprechende Optionen: Eine Anwendung soll bearbeitet werden, aber es wurde --%s benutzt
 Es bestehen sich widersprechende Optionen: Klassengruppe »%s« soll bearbeitet werden, aber es wurde --%s benutzt
 Es bestehen sich widersprechende Optionen: Bildschirm %d soll bearbeitet werden, aber es wurde --%s benutzt
 Es bestehen sich widersprechende Optionen: Die Fenster einer Anwendung sollen aufgelistet werden, aber es wurde --%s benutzt
 Es bestehen sich widersprechende Optionen: Die Fenster der Klassengruppe »%s« sollen aufgelistet werden, aber es wurde --%s benutzt
 Es bestehen sich widersprechende Optionen: Die Fenster der Arbeitsfläche %d sollen aufgelistet werden, aber es wurde --%s benutzt
 Es bestehen sich widersprechende Optionen: Die Fenster oder Arbeitsflächen des Bildschirms %d sollen aufgelistet werden, aber es wurde --%s benutzt
 Es bestehen sich widersprechende Optionen: Arbeitsfläche %d soll bearbeitet werden, aber es wurde --%s benutzt
 Aktuelle Arbeitsfläche: »%s« Fehler beim Verarbeiten der Argumente: %s
 Geometrie (Breite, Höhe): %d, %d
 Geometrie (x, y, Breite, Höhe): %d, %d, %d, %d
 Gruppenleiter: %lu
 Gruppenname: %s
 HÖHE Symbolname: %s
 Symbole: %s
 Ungültiges Argument »%d« für --%s: Das Argument muss positiv sein
 Ungültiges Argument »%d« für --%s: Das Argument muss vollständig positiv sein
 Ungültiges Argument »%s« für --%s, gültige Werte sind: %s
 Ungültiger Wert »%s« für --%s Linker Nachbar: %s
 Fenster der/s Anwendung/Klassengruppe/Arbeitsfläche/Bildschirms auflisten (Ausgabeformat: »XID: Fenstername«) Arbeitsflächen des Bildschirms auflisten (Ausgabeformat: »Nummer: Arbeitsflächenname«) Ver_größern Alle ver_größern Fenster immer im Vordergrund haben Das Fenster in Arbeitsflächenumschaltern anzeigen Fenster in der Fensterliste anzeigen Fenster im Hintergrund haben Vollbildmodus für das Fenster aktivieren Dem Fenster eine feste Position im Darstellungsbereich geben Fenster nicht immer im Vordergrund haben Das Fenster nicht in Arbeitsflächenumschaltern anzeigen Fenster nicht in der Fensterliste anzeigen Fenster nicht im Hintergrund haben Dem Fenster keine feste Position im Darstellungsbereich geben Vollbildmodus für das Fenster beenden Das Fenster auf allen Arbeitsflächen anzeigen Das Fenster nur auf der aktuellen Arbeitsfläche anzeigen Fenster horizontal vergrößern Fenster maximieren Fenster senkrecht vergrößern Ver_kleinern Alle ver_kleinern Fenster minimieren Den Darstellungsbereich der momentanen Arbeitsfläche zur X-Koordinate X verschieben Den Darstellungsbereich der momentanen Arbeitsfläche zur Y-Koordinate Y verschieben Das Fenster auf die Arbeitsfläche NUMMER verschieben (erste Arbeitsfläche ist 0) Auf a_ndere Arbeitsfläche verschieben Auf Arbeitsfläche _rechts verschieben Auf Arbeitsfläche dar_unter verschieben Auf Arbeitsfläche _links verschieben Auf Arbeitsfläche darü_ber verschieben NAME NUMMER NUMMER des zu bearbeitenden Bildschirms NUMMER der zu bearbeitenden Arbeitsfläche Name: %s
 Keine Fenster offen Anzahl der Fenster: %d
 Anzahl der Arbeitsflächen: %d
 Auf Bildschirm: %d (Fensterverwaltung: %s)
 Auf Arbeitsfläche: %s
 Optionen zum Auflisten von Fenstern oder Arbeitsflächen Optionen zum Verändern der Eigenschaften eines Bildschirms Optionen zum Verändern der Eigenschaften eines Fensters Optionen zum Verändern der Eigenschaften einer Arbeitsfläche PID: %s
 Position in der Anordnung (Reihe, Spalte): %d, %d
 Verfügbare Aktionen: %s
 Drucken oder verändern der Eigenschaften eines Bildschirms/Arbeitsfläche/Fenster, oder hiermit arbeiten, gemäß der EWMH-Spezifikation.
Für weitere Informationen zu dieser Spezifikation siehe:
	http://freedesktop.org/wiki/Specifications/wm-spec Klassenressource: %s
 Rechter Nachbar: %s
 Bildschirmnummer: %d
 Sitzungs-ID: %s
 Fenster einrollen Optionen zum Auflisten von Fenstern oder Arbeitsflächen anzeigen Optionen zum Verändern der Eigenschaften eines Bildschirms anzeigen Optionen zum Verändern der Eigenschaften eines Fensters anzeigen Optionen zum Verändern der Eigenschaften einer Arbeitsfläche anzeigen Schreibtisch anzeigen Anzeigen des Schreibtischs: %s
 Das Fenster per Tastatur verschieben Die Größe des Fensters über die Tastatur verändern Startup-ID: %s
 Zustand: %s
 Anzeigen des Schreibtischs anhalten TYP Werkzeug, um zwischen sichtbaren Fenstern zu wechseln Werkzeug, um zwischen Fenstern zu wechseln Werkzeug, um zwischen den Arbeitsflächen zu wechseln Oberer Nachbar: %s
 Transient für: %lu
 Alle wieder_herstellen _Wiederherstellen Fenster horizontal wiederherstellen Fenster wiederherstellen Fenster senkrecht wiederherstellen _Wiederherstellen Fenster wiederherstellen Fenster ausrollen Namenlose Anwendung Namenloses Fenster Darstellungsbereich kann nicht verschoben werden: Die momentane Arbeitsfläche enthält keinen Darstellungsbereich
 Darstellungsbereich kann nicht verschoben werden: Es existiert keine momentane Arbeitsfläche
 Position des Darstellungsbereichs (x, y): %s
 BREITE Fensterliste Fensterverwaltung: %s
 Fensterwähler Fenstertyp: %s
 Fenster kann nicht auf Arbeitsfläche %d verschoben werden: Die Arbeitsfläche ist nicht vorhanden
 Arbeitsfläche %d Arbeitsfläche %s%d Arbeitsfläche 1_0 Arbeitsflächenanordnung (Zeilen, Spalten, Ausrichtung): %d, %d, %s
 Name der Arbeitsfläche: %s
 Nummer der Arbeitsfläche: %d
 Arbeitsflächenumschalter X X-Fenster-ID des Gruppenleiters der zu bearbeitenden Anwendung X-Window-ID des zu bearbeitenden Fensters XID XID: %lu
 Y Immer auf der _sichtbaren Arbeitsfläche S_chließen Alle sch_ließen _Verschieben Nur auf _dieser Arbeitsfläche Größe _ändern Alle wieder_herstellen vorne alle Arbeitsflächen hinten Vollbild-Modus ändern Arbeitsfläche wechseln schließen Schreibtisch Dialogfenster Dock oder Leiste falsch Vollbild in den Vordergrund in den Hintergrund maximieren horizontal maximieren vertikal maximieren maximiert horizontal maximiert vertikal maximiert minimieren minimiert verschieben benötigt Aufmerksamkeit keine Aktionen verfügbar normal normales Fenster anheften angeheftet Größe ändern eingestellt einrollen eingerollt Arbeitsflächenumschalter überspringen Fensterliste überspringen Begrüßungsbildschirm keine anheften angeheftet Abriss-Menü Abriss-Werkzeugleiste wahr in den Vordergrund rückgängig in den Hintergrund rückgängig wiederherstellen horizontal wiederherstellen vertikal wiederherstellen wiederherstellen loslösen ausrollen loslösen Werkzeugfenster keine keine 