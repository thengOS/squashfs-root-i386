��          �   %   �      P  &   Q     x     �     �  (   �  (   �                0  %   H     n     ~     �     �     �     �     �     �     �  &   
     1  R   8  #   �     �  K   �  �    1   �     �     �       .   0  3   _     �  )   �     �  2   �          '     -      4     U     u     �     �  !   �  )   �     	  j   	  -   v	  +   �	  Z   �	                                                           
              	                                                    Adding LTSP network to Network Manager Configuring DNSmasq Configuring LTSP Creating the guest users Enabling LTSP network in Network Manager Extracting thin client kernel and initrd Failed Installing the required packages LTSP-Live configuration LTSP-Live should now be ready to use! Network devices None Ready Restarting Network Manager Restarting openbsd-inetd Start LTSP-Live Starting DNSmasq Starting NBD server Starting OpenSSH server Starts an LTSP server from the live CD Status The selected network interface is already in use.
Are you sure you want to use it? Unable to configure Network Manager Unable to parse config Welcome to LTSP Live.
Please choose a network interface below and click OK. Project-Id-Version: ltsp
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-07-18 13:50-0400
PO-Revision-Date: 2015-07-20 15:23+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:19+0000
X-Generator: Launchpad (build 18115)
Language: de
 LTSP-Netzwerk wird zu NetworkManager hinzugefügt DNSmasq wird eingerichtet LTSP wird eingerichtet Gastbenutzer wird erstellt LTSP-Netzwerk wird in NetworkManager aktiviert »Thin Client«-Kernel und initrd werden extrahiert Fehlgeschlagen Die benötigten Pakete werden installiert LTSP-Live-Konfiguration LTSP-Live sollte jetzt zur Verwendung bereit sein! Netzwerkgeräte Keine Bereit NetworkManager wird neugestartet Openbsd-inetd wird neugestartet LTSP-Live starten DNSmasq wird gestartet NBD-Server wird gestartet Der openSSH-Server wird gestartet Einen LTSP-Server von der Live-CD starten Status Die ausgewählte Netzwerkkarte wird bereits verwendet.
Sind Sie sicher, dass Sie diese verwenden möchten? NetworkManager kann nicht eingerichtet werden Konfiguration kann nicht verarbeitet werden Willkommen bei LTSP-Live.
Bitte wählen Sie eine Netzwerkkarte aus und klicken Sie auf OK. 