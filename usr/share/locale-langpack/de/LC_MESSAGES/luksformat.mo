��          t      �              +   1  #   ]  6   �     �     �  0   �  /   $  )   T  x   ~  �  �  +   �  <   �  -     9   K  &   �  $   �  4   �  3     8   :  �   s                                 
      	                     Could not create LUKS device %s Could not format device with file system %s Creating encrypted device on %s...
 Error: could not generate temporary mapped device name Error: device mounted: %s
 Error: invalid file system: %s
 Please enter your passphrase again to verify it
 The passphrases you entered were not identical
 This program needs to be started as root
 luksformat - Create and format an encrypted LUKS device
Usage: luksformat [-t <file system>] <device> [ mkfs options ]

 Project-Id-Version: cryptsetup 2:1.3.0-1
Report-Msgid-Bugs-To: pkg-cryptsetup-devel@lists.alioth.debian.org
POT-Creation-Date: 2014-03-03 20:26+0100
PO-Revision-Date: 2015-12-04 11:21+0000
Last-Translator: mejo <jonas@freesources.org>
Language-Team: German
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:25+0000
X-Generator: Launchpad (build 18115)
Language: de
 Erstellen der LUKS-Partition %s schlug fehl Formatieren der Partition mit dem Dateisystem %s schlug fehl Erstelle verschlüsselte Partition auf %s...
 Fehler: Erstellen einer temporären Partition schlug fehl Fehler: Partition ist eingebunden: %s
 Fehler: Ungültiges Dateisystem: %s
 Bitte zum verifizieren das Passwort erneut eingeben
 Die eingegebenen Passwörter waren nicht identisch
 Dieses Programm muss als Benutzer root gestartet werden
 luksformat - LUKS-verschlüsselte Partition erstellen und formatieren
Verwendung: luksformat [-t <Dateisystem>] <Partition> [ mkfs Optionen ]

 