��    M      �  g   �      �     �     �     �     �     �     �     �  %   �     %     -      F     g     s     �     �     �     �     �     �     �                -     N     S     X     j     |     �     �  =   �     �     �     �     �     	     	     	     &	     <	  	   A	     K	  !   Q	  -   s	     �	     �	     �	  -   �	  (   "
  
   K
     V
     ^
     j
     �
  1   �
     �
     �
     �
  	   �
  T   �
  8   8     q  >   u  %   �     �     �     �     �       8        ?     X     p  $   �     �  (   �  �  �     s           �     �     �     �     �  /        3     ;  @   Z     �     �     �  "   �  "   �          -  	   M     W     m     {  (   �     �     �     �     �  
   �     �       G   %     m     r     w     �  	   �     �     �  !   �     �     �     �  /   �  J   ,  (   w  .   �  (   �  0   �  6   )     `     q     y  '   �     �  ,   �     �     �     �  
   
  f     3   |     �  M   �  .        0     8  	   D     N     _  5   d  &   �  $   �  '   �  -        <  6   ?     !   (                     	   ?                 ;   I      
                F                 *   A       ,                         '   <   K      1   8                  =   E          2       6   /   5   L      D   3   0   J              .                                   7      )   >   $   H   @       B   %       #   C   &   G      "   +   4      -   :   9            M       ARCHITECTURE Accept certificate Admin password for %s:  Aliases: Architecture: %s Auto update: %s Available commands: Client certificate stored at server:  Columns Copy aliases from source Could not create server cert dir Creating %s Creating the container DESCRIPTION Device %s added to %s Device %s removed from %s Enables debug mode. Enables verbose mode. Environment: Ephemeral container FINGERPRINT Fingerprint: %s Force the container to shutdown. IPV4 IPV6 Invalid source %s Invalid target %s Log: Make image public Missing summary. More than one file to download, but target is not a directory NAME NO Name: %s No fingerprint specified. Options: PID Properties: Remote admin password SIZE SNAPSHOTS STATE Server certificate NACKed by user Server doesn't trust us after adding our cert Set the file's gid on push Set the file's perms on push Set the file's uid on push Show all commands (not just interesting ones) Show the container's last 100 log lines? Snapshots: Source: Starting %s Stopping container failed! TYPE Time to wait for the container before killing it. UPLOAD DATE URL Uploaded: %s Usage: %s Whether or not to restore the container's running state from snapshot (if available) Whether or not to snapshot the container's running state YES bad number of things scanned from image, container or snapshot can't copy to the same container name default disabled enabled got bad version no not all the profiles from the source exist on the target remote %s already exists remote %s doesn't exist remote %s exists as <%s> wrong number of subcommand arguments yes you must specify a source container name Project-Id-Version: LXD
Report-Msgid-Bugs-To: lxc-devel@lists.linuxcontainers.org
POT-Creation-Date: 2016-05-11 18:51-0400
PO-Revision-Date: 2016-06-01 03:27+0000
Last-Translator: Felix Engelmann <Unknown>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:34+0000
X-Generator: Launchpad (build 18115)
Language: 
 ARCHITEKTUR Akzeptiere Zertifikat Administrator Passwort für %s:  Aliase: Architektur: %s Automatische Aktualisierung: %s Verfügbare Befehle: Gespeichertes Nutzerzertifikat auf dem Server:  Spalten Kopiere Aliasse von der Quelle Kann Verzeichnis für Zertifikate auf dem Server nicht erstellen %s wird erstellt Container wird erstellt BESCHREIBUNG Gerät %s wurde zu %s hinzugefügt Gerät %s wurde zu %s hinzugefügt Aktiviert Debug Modus Aktiviert ausführliche Ausgabe Umgebung: Flüchtiger Container FINGERABDRUCK Fingerabdruck: %s Herunterfahren des Containers erzwingen. IPV4 IPV6 Ungültige Quelle %s Ungültiges Ziel %s Protokoll: Veröffentliche Abbild Fehlende Zusammenfassung. Mehr als eine Datei herunterzuladen, aber das Ziel ist kein Verzeichnis NAME NEIN Name: %s Kein Fingerabdruck angegeben. Optionen: PID Eigenschaften: Entferntes Administrator Passwort GRÖßE SCHNAPPSCHÜSSE STATUS Server Zertifikat vom Benutzer nicht akzeptiert Der Server vertraut uns nicht nachdem er unser Zertifikat hinzugefügt hat Setzt die gid der Datei beim Übertragen Setzt die Dateiberechtigungen beim Übertragen Setzt die uid der Datei beim Übertragen Zeigt alle Befehle (nicht nur die interessanten) Zeige die letzten 100 Zeilen Protokoll des Containers? Schnappschüsse: Quelle: %s wird gestartet Anhalten des Containers fehlgeschlagen! TYP Wartezeit bevor der Container gestoppt wird. HOCHLADEDATUM ADRESSE Hochgeladen: %s Aufruf: %s Laufenden Zustand des Containers aus dem Sicherungspunkt (falls vorhanden) wiederherstellen oder nicht Zustand des laufenden Containers sichern oder nicht JA Falsche Anzahl an Objekten im Abbild, Container oder Sicherungspunkt gelesen. kann nicht zum selben Container Namen kopieren Vorgabe deaktiviert aktiviert Versionskonflikt Nein nicht alle Profile der Quelle sind am Ziel vorhanden. entfernte Instanz %s existiert bereits entfernte Instanz %s existiert nicht entfernte Instanz %s existiert als <%s> falsche Anzahl an Parametern für Unterbefehl Ja der Name des Ursprung Containers muss angegeben werden 