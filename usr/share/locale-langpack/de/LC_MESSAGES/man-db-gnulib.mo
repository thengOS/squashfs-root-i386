��    7      �  I   �      �     �     �  .   �  .   �  %   #     I     a  ,   }  ,   �  ,   �  '     -   ,      Z  (   {  (   �     �     �  "     4   0  3   e     �     �     �     �       $        C     U  s   p     �     �     �     	  #   "	     F	     a	     u	     z	     �	  6   �	     �	     �	     �	     
     
     $
  -   +
     Y
     t
  $   �
     �
     �
     �
  *   �
  �       �     �  /   �  2   )  )   \     �      �  *   �  1   �  *     +   G  +   s  $   �  ,   �  ,   �          >  )   ^  8   �  5   �     �          +     J     c  .   ~     �     �  k   �     R     i     n  '   �  )   �  #   �  8   �     4     8     ?  W   Y     �  %   �  %   �  %        <     [  K   c  "   �     �  +   �          ,     E  ?   ^               "   /       2                                  &   6           -   1          $          )   	              %      #      
   3      '         ,   (                  0      +         !          5   .   7      4          *                                or:   [OPTION...] %.*s: ARGP_HELP_FMT parameter must be positive %.*s: ARGP_HELP_FMT parameter requires a value %.*s: Unknown ARGP_HELP_FMT parameter %s: Too many arguments
 %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 (PROGRAM ERROR) No version known!? (PROGRAM ERROR) Option should have been recognized!? ARGP_HELP_FMT: %s value is less than or equal to %s Garbage in ARGP_HELP_FMT: %s Invalid back reference Invalid character class name Invalid collation character Invalid content of \{\} Invalid preceding regular expression Invalid range end Invalid regular expression Mandatory or optional arguments to long options are also mandatory or optional for any corresponding short options. Memory exhausted NAME No match No previous regular expression Premature end of regular expression Regular expression too big Report bugs to %s.
 SECS Success Trailing backslash Try '%s --help' or '%s --usage' for more information.
 Unknown system error Unmatched ( or \( Unmatched ) or \) Unmatched [ or [^ Unmatched \{ Usage: failed to return to initial working directory give a short usage message give this help list hang for SECS seconds (default 3600) memory exhausted print program version set the program name unable to record current working directory Project-Id-Version: GNU gnulib-3.0.0.6062.a6b16
Report-Msgid-Bugs-To: bug-gnulib@gnu.org
POT-Creation-Date: 2015-11-06 15:42+0000
PO-Revision-Date: 2015-12-03 18:33+0000
Last-Translator: Arun Persaud <Unknown>
Language-Team: German <translation-team-de@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:11+0000
X-Generator: Launchpad (build 18115)
Language: de
   oder:   [OPTIONEN] %.*s: ARGP_HELP_FMT Parameter muss positiv sein %.*s: ARGP_HELP_FMT Parameter benötigt einen Wert %.*s: Unbekannter ARGP_HELP_FMT Parameter %s: zu viele Argumente
 %s: ungültige Option -- »%c«
 %s: Option »%c%s« erlaubt kein Argument
 %s: Option »%s« ist mehrdeutig; Möglichkeiten: %s: Option »--%s« erlaubt kein Argument
 %s: Option »--%s« erfordert ein Argument
 %s: Option »-W %s« erlaubt kein Argument
 %s: Option »-W %s« ist mehrdeutig
 %s: Option »-W %s« erfordert ein Argument
 %s: Option erfordert ein Argument -- »%c«
 %s: unbekannte Option »%c%s«
 %s: unbekannte Option »--%s«
 (PROGRAMM FEHLER) Keine Version bekannt!? (PROGRAMM FEHLER) Option hätte erkannt werden müssen!? ARGP_HELP_FMT: Der Wert %s ist kleiner oder gleich %s Müll in ARGP_HELP_FMT: %s Ungültige Rückreferenz Ungültiger Zeichenklassenname Ungültige Zeichenklasse Ungültiger Inhalt in \{\} ungültiger vorhergehender regulärer Ausdruck Ungültiges Bereichsende ungültiger regulärer Ausdruck Erforderliche oder optionale Argumente für lange Optionen sind auch für kurze erforderlich bzw. optional. Speicher ausgeschöpft NAME Keine Übereinstimmung Kein vorhergehender regulärer Ausdruck Vorzeitiges Ende des regulären Ausdrucks Der reguläre Ausdruck ist zu groß Melden Sie Fehler (auf Englisch, mit LC_ALL=C) an <%s>.
 SEK Erfolg abschließender Backslash Rufen Sie »%s --help« oder »%s --usage« auf, um weitere Informationen zu erhalten.
 Unbekannter Systemfehler Keine Übereinstimmung für ( oder \( Keine Übereinstimmung für ) oder \) Keine Übereinstimmung für [ oder [^ Keine Übereinstimmung für \{ Aufruf: es konnte nicht ins ursprüngliche Arbeitsverzeichnis zurückgekehrt werden zeigt eine Kurzfassung des Aufrufs zeigt diese Hilfeliste warte für SEK Sekunden (Standardwert 3600) Speicher ausgeschöpft zeige Programmversion an den Programmnamen setzen aktuelles Arbeitsverzeichnisses kann nicht aufgezeichnet werden 