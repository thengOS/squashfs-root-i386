��    G      T  a   �        /     `   A  �   �  &   �  %   �  #   �  %   �  
        (     1  	   F     P     Y     h     y  C   �     �     �  
   �  
   �     �  
   �  *   		     4	  	   G	  	   Q	     [	     d	     x	     �	     �	     �	     �	     �	     �	  �   �	  �   }
  �   	     �     �     �     �     �     �     �       f      `   �  d   �     M     S  
   X     c     p     �     �     �     �  	   �     �     �     �                    %     3     C     O     [  �  h  >     f   A    �  &   �  9   �  8     %   Q  
   w  
   �     �     �     �     �     �     �  K        N     j     w     �     �     �  -   �     �               -     6     H     _     x     �     �     �     �  �   �  �   �  �   Y  (        >     \     |     �     �     �     �  y   �  s   ]  r   �     D     J     X     u  "   �      �  &   �     �  5     	   C     M     [     n     }     �     �     �     �     �     �     �     2   6   5       >         ,      
          E       &         '   B       1       $   "   3                     A   0   .   %      /          #   !          ;      G         @                                             D             8       =   4   ?   (                     7          C      :          )   *   F      	   +   9               -       <    * You should restart nabi to apply above option <span size="large">An Easy Hangul XIM</span>
version %s

Copyright (C) 2003-2011 Choe Hwanjin
%s <span size="x-large" weight="bold">Can't load tray icons</span>

There are some errors on loading tray icons.
Nabi will use default builtin icons and the theme will be changed to default value.
Please change the theme settings. <span weight="bold">Connected</span>:  <span weight="bold">Encoding</span>:  <span weight="bold">Locale</span>:  <span weight="bold">XIM name</span>:  About Nabi Advanced Automatic reordering BackSpace Choseong Commit by word English keyboard Hangul Hangul input method: Nabi - You can input hangul using this program Hangul keyboard Hanja Hanja Font Hanja Lock Hanja Options Hanja keys Ignore fontset information from the client Input mode scope:  Jongseong Jungseong Keyboard Keypress Statistics Nabi Preferences Nabi keypress statistics Nabi: %s Nabi: error message Off keys Orientation Preedit string font:  Press any key which you want to use as candidate key. The key you pressed is displayed below.
If you want to use it, click "Ok", or click "Cancel" Press any key which you want to use as off key. The key you pressed is displayed below.
If you want to use it, click "Ok" or click "Cancel" Press any key which you want to use as trigger key. The key you pressed is displayed below.
If you want to use it, click "Ok" or click "Cancel" Select candidate key Select hanja font Select off key Select trigger key Shift Space Start in hangul mode The orientation of the tray. This key is already registered for <span weight="bold">candidate key</span>
Please  select another key This key is already registered for <span weight="bold">off key</span>
Please  select another key This key is already registered for <span weight="bold">trigger key</span>
Please  select another key Total Tray Tray icons Trigger keys Use dynamic event flow Use simplified chinese Use system keymap Use tray icon XIM Server is not running XIM name: _Hanja Lock _Hide palette _Reset _Show palette hangul(hanja) hanja hanja(hangul) per application per context per desktop per toplevel Project-Id-Version: de
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-01-26 17:31+0900
PO-Revision-Date: 2015-02-18 14:56+0000
Last-Translator: Hendrik Lübke <henni123468@gmail.com>
Language-Team: <ami-list@lists.kldp.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:12+0000
X-Generator: Launchpad (build 18115)
Language: 
 * Sie müssen Nabi neu starten, um diese Option zu aktivieren. <span size="large">Ein einfaches Hangul XIM</span>
Version %s

Copyright (C) 2003-2011 Choe Hwanjin
%s <span size="x-large" weight="bold">Kann ein Icon nicht laden</span>

Es gab Fehler beim Laden der Icons für die Schaltfläche. Nabi wird deshalb die eingebauten Icons verwenden und das verwendete Schema wird auf die Vorgabe zurückgesetzt.
Bitte ändern Sie das Schema. <span weight="bold">Verbunden</span>:  <span weight="bold">Encoding (Eingabe-Kodierung)</span>:  <span weight="bold">Locale (Ländereinstellung)</span>:  <span weight="bold">XIM Name</span>:  Über Nabi Feinheiten Automatische Neuanordnung Zurücktaste 초성/Choseong Bestätigung nach jedem Wort englische Tastatur 한글/Hangul Hangul Eingabehilfe: Nabi - Damit können Sie koreanische Zeichen eingeben. Hangul/koreanische Tastatur 한자/Hanja 한자/Hanja Zeichensatz Hanja-Sperre 한자/Hanja Optionen 한자/Hanja Tasten Fontset-Informationen des Clienten ignorieren Eingabemodusbereich:  종성/Jongseong 중성/Jungseong Tastatur Tastaturstatistik Einstellungen von Nabi Tastenstatistik von Nabi Nabi: Zeichensatz: %s Nabi: Fehlermeldung Ausschalttaste. Ausrichtung Vorbearbeitungsschrift:  Drücken Sie irgendeine Taste, welche Sie als Kandidatauswahltaste verwenden möchten. Die gedrückte Taste wird unten dargestellt.
Falls Sie sie benutzen wollen, drücken Sie "Ok" oder "Abbrechen" Drücken Sie irgendeine Taste, welche Sie zum Umschalten verwenden möchten. Die gedrückte Taste wird unten dargestellt.
Falls Sie sie benutzen wollen, drücken Sie "Ok" oder "Abbrechen" Drücken Sie irgendeine Taste, welche Sie zum Umschalten verwenden möchten. Die gedrückte Taste wird unten dargestellt.
Falls Sie sie benutzen wollen, drücken Sie "Ok" oder "Abbrechen" Bitte "Kandidatauswahl" Taste auswählen Wahl 한자/Hanja Zeichensatz Bitte Ausschalttaste auswählen Bitte Umschalttaste auswählen Grosstelltaste Grösse Im Hangul-Modus starten Ausrichtung. der Ablage. Diese Taste wurde schon als <span weight="bold">Kandidatauswahltaste</span> gewählt.
Bitte eine andere Taste auswählen. Diese Taste wurde schon als <span weight="bold">Ausschalttaste</span> gewählt.
Bitte eine andere Taste auswählen. Diese Taste wurde schon als <span weight="bold">Umschalttaste</span> gewählt.
Bitte eine andere Taste auswählen. Total Schaltfläche Icons für die Schaltfläche Umschalttasten Dynamischen Ereignisfluss benutzen Vereinfaches Chinesisch benutzen Tastaturbelegung des Systems verwenden Systembereichsymbol verwenden XIM Server (Dienst für Eingabe-Methode) läuft nicht XIM Name: _Hanja-Sperre Hinweis ausblenden Zu_rücksetzen Zeige Hinweis Hangul(Hanja) 한자/Hanja Hanja (Hangul) pro Anwendung Pro Kontext Pro Desktop Pro höchste Stufe 