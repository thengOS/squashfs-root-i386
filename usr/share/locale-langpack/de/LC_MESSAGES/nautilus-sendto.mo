��    	      d      �       �      �   )   �   2        F  ,   T  #   �  "   �  0   �  �  �     �  :   �  9   �        I   4  *   ~  0   �  9   �                                          	    Archive Could not parse command-line options: %s
 Expects URIs or filenames to be passed as options
 Files to send No mail client installed, not sending files
 Output version information and exit Run from build directory (ignored) Use XID as parent to the send dialogue (ignored) Project-Id-Version: nautilus-sendto master
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-03 23:48+0000
PO-Revision-Date: 2015-12-04 22:00+0000
Last-Translator: Christian Kirbach <Unknown>
Language-Team: Deutsch <gnome-de@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:34+0000
X-Generator: Launchpad (build 18115)
Language: de_DE
 Archiv Befehlszeilenoptionen konnten nicht eingelesen werden: %s
 URLs oder Dateinamen sollten als Optionen gesetzt werden
 Zu sendende Dateien Es ist kein E-Mail-Programm installiert. Dateien werden nicht verschickt
 Versionsinformationen ausgeben und beenden Aus dem Erstellungsordner ausführen (ignoriert) XID als Eltern für den Sendedialog verwenden (ignoriert) 