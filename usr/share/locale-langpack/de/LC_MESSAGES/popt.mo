��             +         �     �     �     �  "   �               '     +     0     9  (   >     g     m     t     �     �     �     �     �     �     �               7  /   H     x     �  )   �     �     �     �  �       �     �  '   �  "   �               &     .     3     <  -   C     q     w     ~     �     �     �     �     �  *   �  #   	     -      K     l  /        �     �  ,   �     	     3	     F	                                                                 	                 
                                                              ARG DOUBLE Display brief usage message Display option defaults in message FLOAT Help options: INT LONG LONGLONG NONE Options implemented via popt alias/exec: SHORT STRING Show this help message Terminate options Usage: VAL [OPTION...] aliases nested too deeply config file failed sanity test error in parameter quoting invalid numeric value memory allocation failed missing argument mutually exclusive logical operations requested number too large or too small opt->arg should not be NULL option type (%u) not implemented in popt
 unknown errno unknown error unknown option Project-Id-Version: popt 1.14
Report-Msgid-Bugs-To: <popt-devel@rpm5.org>
POT-Creation-Date: 2010-02-17 13:35-0500
PO-Revision-Date: 2012-03-08 12:03+0000
Last-Translator: Simeon <Unknown>
Language-Team: German <translation-team-de@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:04+0000
X-Generator: Launchpad (build 18115)
 ARGUMENT DOUBLE Zeigt eine kurze Verwendungsinformation Zeigt die Standardeinstellungen an FLOAT Hilfe-Optionen: INTEGER LONG LONGLONG NICHTS Optionen über popt alias/exec implementiert: SHORT STRING Zeigt diese Hilfe an Optionen beenden Verwendung: WERT [OPTION...] Aliase zu tief verschachtelt Konfigurationsdatei ist nicht wohl-geformt Fehler beim Quotieren der Parameter Ungültiger nummerischer Wert Speicherzuordnung fehlgeschlagen Fehlendes Argument Gegenseitig ausschließende logische Operatoren Nummer zu groß oder zu klein opt->arg sollte nicht NULL sein Optionstyp (%u) ist in popt nicht vorhanden
 Unbekannte Fehler-Nummer Unbekannter Fehler Unbekannte Option 