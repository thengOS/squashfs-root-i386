��          �   %   �      p  '  q  A   �  @   �  D     =   a  :   �  l   �     G     a  ,   {     �  %   �  ,   �  -         G  &   h     �     �  t   �  �   D  y   �  [   h	  �   �	  �   M
  �   �
  �   y  0     �  =  '  �  F     K   N  U   �  3   �  E   $  p   j     �     �  *     !   @  )   b  *   �  +   �  $   �  (        1     Q  �   q  �   �  m   �  [   &  �   �  �     �   �  �   p  5                            
                                                                    	                              		[CDEF:vname=rpn-expression]
		[VDEF:vdefname=rpn-expression]
		[PRINT:vdefname:format]
		[GPRINT:vdefname:format]
		[COMMENT:text]
		[SHIFT:vname:offset]
		[TEXTALIGN:{left|right|justified|center}]
		[TICK:vname#rrggbb[aa][:[fraction][:legend]]]
		[HRULE:value#rrggbb[aa][:legend]]
		[VRULE:value#rrggbb[aa][:legend]]
		[LINE[width]:vname[#rrggbb[aa][:[legend][:STACK]]]]
		[AREA:vname[#rrggbb[aa][:[legend][:STACK]]]]
		[PRINT:vname:CF:format] (deprecated)
		[GPRINT:vname:CF:format] (deprecated)
		[STACK:vname[#rrggbb[aa][:legend]]] (deprecated)
  * cd - changes the current directory

	rrdtool cd new directory
  * ls - lists all *.rrd files in current directory

	rrdtool ls
  * mkdir - creates a new directory

	rrdtool mkdir newdirectoryname
  * pwd - returns the current working directory

	rrdtool pwd
  * quit - closing a session in remote mode

	rrdtool quit
  * resize - alter the length of one of the RRAs in an RRD

	rrdtool resize filename rranum GROW|SHRINK rows
 %s: illegal option -- %c
 %s: invalid option -- %c
 %s: option `%c%s' doesn't allow an argument
 %s: option `%s' is ambiguous
 %s: option `%s' requires an argument
 %s: option `--%s' doesn't allow an argument
 %s: option `-W %s' doesn't allow an argument
 %s: option `-W %s' is ambiguous
 %s: option requires an argument -- %c
 %s: unrecognized option `%c%s'
 %s: unrecognized option `--%s'
 * graph - generate a graph from one or several RRD

	rrdtool graph filename [-s|--start seconds] [-e|--end seconds]
 * graphv - generate a graph from one or several RRD
           with meta data printed before the graph

	rrdtool graphv filename [-s|--start seconds] [-e|--end seconds]
 * info - returns the configuration and status of the RRD

	rrdtool info [--daemon|-d <addr> [--noflush|-F]] filename.rrd
 * last - show last update time for RRD

	rrdtool last filename.rrd
		[--daemon|-d address]
 * restore - restore an RRD file from its XML form

	rrdtool restore [--range-check|-r] [--force-overwrite|-f] filename.xml filename.rrd
 RRDtool %s  Copyright by Tobias Oetiker <tobi@oetiker.ch>
               Compiled %s %s

Usage: rrdtool [options] command command_options
 RRDtool is distributed under the Terms of the GNU General
Public License Version 2. (www.gnu.org/copyleft/gpl.html)

For more information read the RRD manpages
 Valid commands: create, update, updatev, graph, graphv,  dump, restore,
		last, lastupdate, first, info, fetch, tune
		resize, xport, flushcached
 Valid remote commands: quit, ls, cd, mkdir, pwd
 Project-Id-Version: rrdtool
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-04-06 16:14+0000
PO-Revision-Date: 2015-10-14 18:13+0000
Last-Translator: schuko24 <gerdsaenger@t-online.de>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:05+0000
X-Generator: Launchpad (build 18115)
 		[CDEF:vname=RPN-Ausdruck]
		[VDEF:vdefname=RPN-Ausdruck]
		[PRINT:vdefname:Format]
		[GPRINT:vdefname:Format]
		[COMMENT:Text]
		[SHIFT:vname:Versatz]
		[TEXTALIGN:{left|right|justified|center}]
		[TICK:vname#rrggbb[aa][:[Unterteilung][:Legende]]]
		[HRULE:Wert#rrggbb[aa][:Legende]]
		[VRULE:Wert#rrggbb[aa][:Legende]]
		[LINE[Breite]:vname[#rrggbb[aa][:[Legende][:STACK]]]]
		[AREA:vname[#rrggbb[aa][:[Legende][:STACK]]]]
		[PRINT:vname:CF:Format] (veraltet)
		[GPRINT:vname:CF:Format] (veraltet)
		[STACK:vname[#rrggbb[aa][:Legende]]] (veraltet)
  * cd - Aktuelles Verzeichnis wechseln

	rrdtool cd Neues Verzeichnis
  * ls - Alle *.rrd-Dateien im aktuellen Verzeichnis auflisten

	rrdtool ls
  * mkdir - Erstellt ein neues Verzeichnis

	rrdtool mkdir NameDesNeuenVerzeichnisses
  * pwd - Gibt den aktuellen Pfad aus

	rrdtool pwd
  * quit - Eine Sitzung im Fernsteuerungsmodus beenden

	rrdtool quit
  * resize - Die Länge eines RRAs in einem RRD ändern

	rrdtool resize Dateiname RRA-Nummer GROW|SHRINK Zeilen
 %s: ungültige Option -- %c
 %s: ungültige Option -- %c
 %s: Option »%c%s« erlaubt kein Argument
 %s: Option »%s« ist mehrdeutig
 %s: Option »%s« erfordert ein Argument
 %s: Option »--%s« erlaubt kein Argument
 %s: Option »-W %s« erlaubt kein Argument
 %s: Option »-W %s« ist mehrdeutig
 %s: Option benötigt ein Argument -- %c
 %s: unbekannte Option »%c%s«
 %s: unbekannte Option »--%s«
 * graph - Einen Graphen aus einem oder mehreren RRDs erzeugen

	rrdtool graph Dateiname [-s|--start Sekunden] [-e|--end Sekunden]
 * graphv - Einen Graphen aus einem oder mehreren RRDs erzeugen
           mit Metadaten, die vor dem Graphen angezeigt werden

	rrdtool graphv Dateiname [-s|--start Sekunden] [-e|--end Sekunden]
 * info - zeigt RRD-Konfiguration und -Status

	rrdtool info [--daemon|-d <addr> [--noflush|-F]] filename.rrd
 * last - show last update time for RRD

	rrdtool last filename.rrd
		[--daemon|-d address]
 * restore - Eine RRD-Datei aus dem XML-Format wiederherstellen

	rrdtool restore [--range-check|-r] [--force-overwrite|-f] Dateiname.xml Dateiname.rrd
 RRDtool %s  Urheberrecht liegt bei Tobias Oetiker <tobi@oetiker.ch>
               Kompiliert %s %s

Nutzung: rrdtool [options] Befehl Befehls_Optionen
 RRDtool wurde unter den Bedingungen der GNU General
Public License Version 2 veröffentlicht. (www.gnu.org/copyleft/gpl.html)

Weitere Informationen erhalten Sie in den RRD-Handbuchseiten
 Gültige Befehle: create, update, updatev, graph, graphv,  dump, restore,
		last, lastupdate, first, info, fetch, tune
		resize, xport, flushcached
 Erlaubte entfernte Befehle: quit, ls, cd, mkdir, pwd
 