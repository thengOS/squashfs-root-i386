��    \      �     �      �  �   �  ,   �  5   	  N   7	  7   �	  \   �	  _   
  `   {
  u   �
  i   R  b   �  V     Y   v  ~   �  �   O  D   �  %   $     J     a  5   {  B   �     �  e     w   w     �          #     ?  $   W     |     �     �     �     �     �     �     �  #   	     -     H     P     Y     l     ~     �     �  H   �     �       !   1     S     h  (   }     �     �  #   �     �       $   5     Z     y  #   �  B   �  2   �     -      A     b     �     �  *   �  *   �          (     8  #   F  #   j  &   �     �     �  ,   �          )  -   >     l     {     �     �     �     �     �     �  �    $  �  ;     @   [  ^   �  I   �  `   E  a   �  d     �   m  �   �  i   �  `   �  d   _  �   �  �   O  P   �  &   =      d      �   E   �   U   �      9!  �   U!  w   "     �"     �"     �"     �"  ,   #     3#     K#     k#     x#     �#     �#  #   �#     �#  "   �#      $     %$     1$     :$     T$     o$     �$     �$  N   �$  $   %     -%  7   I%  !   �%  "   �%  I   �%  '   &  !   8&  "   Z&     }&  .   �&  5   �&  2   '  (   5'  *   ^'  V   �'  '   �'     (  &   (     F(  $   f(     �(  ?   �(  9   �(  )   )     G)     X)  "   f)  "   �)  )   �)     �)  #   �)  4   *  "   D*     g*  9   |*     �*     �*     �*     �*     +     !+     <+  )   W+           C          	   5             !   <      J   +                 R   #   
   3   L   S           E   M       *   B                     F      :   X      2   I                 H   .                 >                  ?       %          $   Q   9               T   4               1       [                -             V       @              G   =       ,   U   \   O   7   N      )   &           P   (   ;   A   Z       W   '   /   0       K   D      8   Y   "   6          
If no -e, --expression, -f, or --file option is given, then the first
non-option argument is taken as the sed script to interpret.  All
remaining arguments are names of input files; if no input files are
specified, then the standard input is read.

       --help     display this help and exit
       --version  output version information and exit
   --follow-symlinks
                 follow symlinks when processing in place
   --posix
                 disable all GNU extensions.
   -R, --regexp-perl
                 use Perl 5's regular expressions syntax in the script.
   -b, --binary
                 open files in binary mode (CR+LFs are not processed specially)
   -e script, --expression=script
                 add the script to the commands to be executed
   -f script-file, --file=script-file
                 add the contents of script-file to the commands to be executed
   -i[SUFFIX], --in-place[=SUFFIX]
                 edit files in place (makes backup if SUFFIX supplied)
   -l N, --line-length=N
                 specify the desired line-wrap length for the `l' command
   -n, --quiet, --silent
                 suppress automatic printing of pattern space
   -r, --regexp-extended
                 use extended regular expressions in the script.
   -s, --separate
                 consider files as separate rather than as a single continuous
                 long stream.
   -u, --unbuffered
                 load minimal amounts of data from the input files and flush
                 the output buffers more often
   -z, --null-data
                 separate lines by NUL characters
 %s: -e expression #%lu, char %lu: %s
 %s: can't read %s: %s
 %s: file %s line %lu: %s
 %s: warning: failed to get security context of %s: %s %s: warning: failed to set default file creation context to %s: %s : doesn't want any addresses E-mail bug reports to: <%s>.
Be sure to include the word ``%s'' somewhere in the ``Subject:'' field.
 GNU sed home page: <http://www.gnu.org/software/sed/>.
General help using GNU software: <http://www.gnu.org/gethelp/>.
 Invalid back reference Invalid character class name Invalid collation character Invalid content of \{\} Invalid preceding regular expression Invalid range end Invalid regular expression Jay Fenlason Ken Pizzini Memory exhausted No match No previous regular expression Paolo Bonzini Premature end of regular expression Regular expression too big Success Tom Lord Trailing backslash Unmatched ( or \( Unmatched ) or \) Unmatched [ or [^ Unmatched \{ Usage: %s [OPTION]... {script-only-if-no-other-script} [input-file]...

 `e' command not supported `}' doesn't want any addresses can't find label for jump to `%s' cannot remove %s: %s cannot rename %s: %s cannot specify modifiers on empty regexp cannot stat %s: %s command only uses one address comments don't accept any addresses couldn't attach to %s: %s couldn't edit %s: is a terminal couldn't edit %s: not a regular file couldn't follow symlink %s: %s couldn't open file %s: %s couldn't open temporary file %s: %s couldn't write %d item to %s: %s couldn't write %d items to %s: %s delimiter character is not a single-byte character error in subprocess expected \ after `a', `c' or `i' expected newer version of sed extra characters after command incomplete command invalid reference \%d on `s' command's RHS invalid usage of +N or ~N as first address invalid usage of line address 0 missing command multiple `!'s multiple `g' options to `s' command multiple `p' options to `s' command multiple number options to `s' command no input files no previous regular expression number option to `s' command may not be zero option `e' not supported read error on %s: %s strings for `y' command are different lengths unexpected `,' unexpected `}' unknown command: `%c' unknown option to `s' unmatched `{' unterminated `s' command unterminated `y' command unterminated address regex Project-Id-Version: sed 4.2.0
Report-Msgid-Bugs-To: bug-gnu-utils@gnu.org
POT-Creation-Date: 2012-12-22 14:36+0100
PO-Revision-Date: 2016-02-12 13:39+0000
Last-Translator: Walter Koch <Unknown>
Language-Team: German <translation-team-de@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:16+0000
X-Generator: Launchpad (build 18115)
Language: de
 
Falls kein -e, --expression, -f oder --file als Option angegeben ist, 
wird das erste Argument, das keine Option ist als sed-Skript verwendet.
Alle restlichen Argumente werden als Eingabedateinamen behandelt.
Falls keine Eingabedateien angegeben sind, wird von der Standardeingabe gelesen.

       --help     nur diese Hilfe anzeigen und dann beenden
       --version  nur die Versionsinfo ausgeben und dann beenden
   --follow-symlinks
                 symbolischen Verknüpfungen bei Verwendung von -i folgen
   --posix
                 schaltet alle GNU-Funktions-Erweiterungen ab.
   -R, --regexp-perl
                 Verwende die Perl-5-Syntax für reg. Ausdrücke im Skript.
   -b, --binary
                 öffnet Dateien binär (CR+LFs werden nicht besonders behandelt)
   -e skript, --expression=skript
                 hängt `skript' an die auszuführenden Befehle an
   -f skript-Datei, --file=skript-Datei
                 hängt den Inhalt von `Skript-Datei' an die
                 auszuführenden Befehle an
   -i[SUFFIX], --in-place[=SUFFIX]
                 Dateien an Ihrem Speicherort bearbeiten (erzeugt eine Sicherung, falls ein SUFFIX angegeben wird)
   -l N, --line-length=N
                 gibt die gewünschte Zeilenumbruchlänge für den `l'-Befehl an
   -n, --quiet, --silent
                 verhindert die automatische Ausgabe des Arbeitspuffers
   -r, --regexp-extended
                 verwendet die erweiterten reg. Ausdrücke für das Skript.
   -s, --separate
                 die Dateien werden getrennt und nicht als einzige
                 zusammenhängende Quelle betrachtet.
   -u, --unbuffered
                 lade nur kleinste Datenmengen aus den Eingabedateien
                 und schreibe die Ausgabepuffer häufiger zurück.
   -z, --null-data
                 Zeilen durch NUL-Zeichen voneinander trennen
 %s: -e Ausdruck #%lu, Zeichen %lu: %s
 %s: kann %s nicht lesen: %s
 %s: Datei %s Zeile %lu: %s
 %s: Warnung: Fehler beim Erlangen des Sicherheitskontextes von %s: %s %s: Warnung: Festlegen des Standarddateierstellungskontexts auf %s fehlgeschlagen: %s `:' erwartet keine Adressen Fehlerberichte bitte per E-Mail (auf englisch) an: <%s>.
Verwenden Sie dabei den Begriff `%s' irgendwo in der `Betreff:'-Zeile.
Sinn- oder Schreibfehler in den deutschen Texten bitte an <de@li.org>.
 GNU-sed-Homepage: <http://www.gnu.org/software/sed/>.
Allgemeine Hilfe zu GNU-Software: <http://www.gnu.org/gethelp/>.
 Ungültiger Rückverweis Ungültiger Zeichenklassenname Ungültiges Vergleichszeichen Ungültiger Inhalt in \{\} Vorheriger regulärer Ausdruck ist ungültig Ungültiges Bereichende Ungültiger regulärer Ausdruck Jay Fenlason Ken Pizzini Speicher erschöpft Keine Übereinstimmung Kein vorheriger regulärer Ausdruck Paolo Bonzini Regulärer Ausdruck endet zu früh Regulärer Ausdruck ist zu groß Erfolgreich Tom Lord Abschließender Backslash Nicht paarweises ( bzw. \( Nicht paarweises ) bzw. \) Nicht paarweises [ bzw. [^ Nicht paarweises \{ Aufruf: %s [OPTION]... {Skript-falls-kein-anderes-Skript} [Eingabe-Datei]...

 `e'-Kommando wird nicht unterstützt `}' erwartet keine Adressen Kann die Zielmarke für den Sprung zu `%s' nicht finden %s kann nicht entfernt werden: %s %s kann nicht umbenannt werden: %s Für leere reguläre Ausdrücke können keine `modifier' angegeben werden Statusermittlung von %s schlug fehl: %s Befehl verwendet nur eine Adresse Kommentare erlauben keine Adressen Kann nicht in %s einklinken: %s Kann %s nicht bearbeiten: Das ist ein Terminal Kann %s nicht bearbeiten: Das ist keine normale Datei Symbolischem Link %s kann nicht gefolgt werden: %s Datei %s kann nicht geöffnet werden: %s Kann temporäre Datei %s nicht öffnen: %s Kann %d Element nicht auf %s schreiben: %s Kann %d Elemente nicht auf %s schreiben: %s Trennzeichen ist kein Einzelbytezeichen Fehler im Unterprozess Nach `a', `c' oder `i' wird \ erwartet Neuere Version von sed erwartet Zusätzliche Zeichen nach dem Befehl Unvollständiger Befehl Ungültiger Verweis \%d im rechten Teil (`RHS') des `s'-Befehls +N oder ~N können nicht als erste Adresse benutzt werden Ungültige Verwendung der Zeilenadresse 0 Fehlender Befehl Mehrfache `!' Mehrere 'g'-Optionen am `s'-Befehl Mehrere 'p'-Optionen am `s'-Befehl Mehrere numerische Optionen am `s'-Befehl Keine Eingabedateien Kein vorheriger regulärer Ausdruck Numerische Option am `s'-Befehl kann nicht Null sein Option `e' wird nicht unterstützt Lesefehler in %s: %s Unterschiedliche Länge der Zeichenketten des `y'-Befehls Unerwartetes `,' Unerwartetes `}' Unbekannter Befehl: `%c' Unbekannte Option für `s' Nicht paarweises `{' Nicht beendeter `s'-Befehl Nicht beendeter `y'-Befehl Nicht beendeter regulärer Adressausdruck 