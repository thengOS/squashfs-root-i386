��    �      4  �   L
      �     �     �     �     �     	  �        �     �     �     �            	   0     :  	   Q     [     `     s          �     �     �  	   �     �     �     �     �          "     2     ;  _   L  _   �  [        h     �      �  *   �     �     �            :   (     c     i  !   x     �  4   �     �     �     �               '     .     3     ;     C     K  	   P  
   Z     e     i  1   v     �  4   �     �       
        &     8  5   >     t     �     �  )   �  *   �     �                     '     B     \     d     p     }     �  /   �  (   �  E   �  #   7     [  
   `     k     �     �     �     �     �     �     �     �  
     #        <     Q     f  F   r     �     �     �     �     �       :     8   T  M   �  O   �     +  <   B  :     N   �  c  	     m  h   �     �       (   +      T     u  -   �      �  %   �  %   �  $   %  !   J  5   l  &   �     �  
   �     �     �  	               	        $     +     1     B     J     P  
   V     a     t     �  
   �     �     �     �     �  �  �     �!     �!     �!     �!  	   �!  9  �!     
#     #     #     #     #  $   .#     S#  !   c#     �#  
   �#     �#     �#     �#     �#  .   �#     $  	   '$     1$  $   =$     b$  
   j$  2   u$     �$     �$     �$  c   �$  c   <%  _   �%  '    &  
   (&  ,   3&  5   `&  #   �&  %   �&     �&     �&  T   '     Y'     e'  !   |'     �'  J   �'     �'     �'     (     .(     E(  	   Y(     c(     k(     r(     w(     (     �(     �(     �(     �(  @   �(     )  9   )     Q)     m)     )     �)     �)  J   �)     �)      *     ,*  *   8*  F   c*  
   �*     �*     �*     �*     �*     +     +     *+     =+     Q+     e+  6   z+  0   �+  k   �+  /   N,  	   ~,     �,     �,  #   �,     �,     �,      -     1-     :-     B-     T-     b-  *   v-     �-     �-     �-  k   �-  -   N.     |.     �.     �.  "   �.     �.  2   �.  /   	/  z   9/  L   �/     0  A   0  @   [0  M   �0  	  �0     �3  p   4  6   �4  ,   �4  3   �4  *   5  )   H5  N   r5  "   �5  0   �5  0   6  .   F6  +   u6  ?   �6  %   �6     7     $7  
   27     =7     I7     Q7     ^7  	   q7     {7     �7     �7  
   �7     �7     �7  
   �7     �7     �7  
   �7     8     8     -8  B   H8  b  �8     �   �       @       t   �   3                 u   U   7      w   �           �   I   z       �   ~   ;   _   >          �       P   Q   H       �              �   �   e         N       #   E   }   K   F   W   �   `       X              v   �   �   \   *   =   x   �           ?         �                  �   5   j   <   �       r          B         T   g               0   :               9   �   2   "       ]          A       1   +      �       �           �   �   [   q       k           o   ,   /          -          C   c   n   f   Z              i   y   �   8          	   
      D   M          S   4              h   G       '   �   �   �       s   )              J   &   ^      �   O   �   L           %         b   !   V   |      m   $       a       6   �   �   �   Y                       �   �   p   .   l   �       R   d      (   �       {   �    %d dpi %d dpi (default) %d dpi (draft) %d dpi (high resolution) 4×6 A really easy way to scan both documents and photos. You can crop out the bad parts of a photo and rotate it if it is the wrong way round. You can print your scans, export them to pdf, or save them in a range of image formats. A_4 A_5 A_6 About About Simple Scan Additional software needed All Files All Pages From _Feeder Automatic Back Brightness of scan Brightness: Change _Scanner Combine sides Combine sides (reverse) Contrast of scan Contrast: Crop Crop the selected page Darker Device to scan from Directory to save files to Discard Changes Document Document Scanner Drivers for this are available on the <a href="http://samsung.com/support">Samsung website</a>. Drivers for this are available on the <a href="http://support.brother.com">Brother website</a>. Drivers for this are available on the <a href="http://support.epson.com">Epson website</a>. Drivers installed successfully! Email... Error communicating with scanner Failed to install drivers (error code %d). Failed to install drivers. Failed to save file Failed to scan File format: Fix PDF files generated with older versions of Simple Scan Front Front and Back Height of paper in tenths of a mm Help If you don't save, changes will be permanently lost. Image Files Install drivers Installing drivers... JPEG (compressed) Keep unchanged Le_gal Less Lighter Maximum Minimum More Move Left Move Right New New Document No scanners available.  Please connect a scanner. No scanners detected Once installed you will need to restart Simple Scan. PDF (multi-page document) PNG (lossless) Page Size: Page side to scan Photo Please check your scanner is connected and powered on Preferences Print debugging messages Print... Quality value to use for JPEG compression Quality value to use for JPEG compression. Quality: Quit Quit without Saving Reorder Pages Resolution for photo scans Resolution for text scans Reverse Rotate Left Rotate Right Rotate _Left Rotate _Right Rotate the page to the left (counter-clockwise) Rotate the page to the right (clockwise) Run '%s --help' to see a full list of available command line options. SANE device to acquire images from. Save Save As... Save current document? Save document before quitting? Save document to a file Saving document... Saving page %d out of %d Sc_an Scan Scan Documents Scan S_ource: Scan Side: Scan a single page from the scanner Scanned Document.pdf Show release version Simple Scan Simple Scan uses the SANE framework to support most existing scanners. Simple document scanning tool Single _Page Start a new document Stop Stop the current scan Text The brightness adjustment from -100 to 100 (0 being none). The contrast adjustment from -100 to 100 (0 being none). The directory to save files to. Defaults to the documents directory if unset. The height of the paper in tenths of a mm (or 0 for automatic paper detection). The page side to scan. The resolution in dots-per-inch to use when scanning photos. The resolution in dots-per-inch to use when scanning text. The width of the paper in tenths of a mm (or 0 for automatic paper detection). This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. Type of document being scanned Type of document being scanned. This setting decides on the scan resolution, colors and post-processing. Unable to connect to scanner Unable to open help file Unable to open image preview application Unable to save image for preview Unable to start scan Username and password required to access '%s' Width of paper in tenths of a mm You appear to have a Brother scanner. You appear to have a Samsung scanner. You appear to have an Epson scanner. You appear to have an HP scanner. You need to install driver software for your scanner. You need to install the %s package(s). [DEVICE...] - Scanning utility _Authorize _Cancel _Close _Contents _Crop _Custom _Document _Email _Help _Install Drivers _Letter _None _Page _Password: _Photo Resolution: _Rotate Crop _Save _Stop Scan _Text Resolution: _Username for resource: scan;scanner;flatbed;adf; translator-credits Project-Id-Version: simple-scan
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-03-20 21:13+0000
PO-Revision-Date: 2015-11-06 19:07+0000
Last-Translator: Torsten Franz <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:48+0000
X-Generator: Launchpad (build 18115)
 %d dpi %d dpi (Vorgabe) %d dpi (Entwurf) %d dpi (Hohe Auflösung) 4×6 Zoll Eine ganz einfache Methode, um sowohl Dokumente als auch Fotos zu scannen. Unbrauchbare Teile eines Fotos können abgeschnitten werden, es kann gedreht werden, falls es verkehrt herum liegt. Die Scans können gedruckt, als PDF-Dokument exportiert oder in einer Anzahl verschiedener Bildformate gespeichert werden. A_4 A_5 A_6 Über Über Simple Scan Zusätzliche Software wird benötigt Alle Dateitypen Alle Seiten aus dem _Papiereinzug Automatisch Rückseite Helligkeit des Scans Helligkeit: _Scanner wechseln Seiten zusammenfügen Seiten zusammenfügen (umgekehrte Reihenfolge) Kontrast des Scans Kontrast: Zuschneiden Die gewählte Seite zurechtschneiden Dunkler Scangerät Verzeichnis, in dem die Dateien gespeichert werden Änderungen verwerfen Dokument Dokument-Scanner Treiber hierfür sind verfügbar auf der <a href="http://samsung.com/support">Samsung Webseite</a>. Treiber hierfür sind verfügbar auf der <a href="http://support.brother.com">Brother Webseite</a>. Treiber hierfür sind auf der <a href="http://support.epson.com">Epson Webseite</a> verfügbar. Treiber wurden erfolgreich installiert! E-Mail … Fehler bei der Kommunikation mit dem Scanner Treiberinstallation fehlgeschlagen (Fehlernummer %d). Treiberinstallation fehlgeschlagen. Datei konnte nicht gespeichert werden Scannen fehlgeschlagen Dateiformat: Repariere PDF-Dateien, die mit einer älteren Version von Simpe Scan erstellt wurden Vorderseite Vorder- und Rückseite Papierhöhe in Zehntelmillimetern Hilfe Wenn Sie nicht speichern, werden die Änderungen unwiderruflich verworfen. Bilddateien Treiber installieren Treiber werden installiert… JPEG (Verlustbehaftet) Unverändert lassen US _Legal Weniger Heller Hoch Niedrig Mehr Nach links schieben Nach rechts schieben Neu Neues Dokument Keine Scanner verfügbar. Bitte schließen Sie einen Scanner an. Kein Scanner gefunden Nach der Installation müssen Simple Scan erneut starten. PDF (Mehrseitiges Dokument) PNG (Verlustfrei) Seitengröße: Zu scannende Seite des Blattes Foto Bitte überprüfen Sie, ob Ihr Scanner angeschlossen und eingeschaltet ist Einstellungen Fehlerdiagnosemeldungen anzeigen Drucken … Qualitätswert für die JPEG-Komprimierung Qualitätswert, der für die JPEG-Komprimierung verwendet werden soll. Qualität: Beenden Beenden ohne zu speichern Seiten umsortieren Auflösung von Foto-Scans Auflösung von Text-Scans Vertauschen Links herum drehen Rechts herum drehen _Links herum drehen _Rechts herum drehen Die Seite links herum drehen (Gegen den Uhrzeigersinn) Die Seite rechts herum drehen (Im Uhrzeigersinn) Rufen Sie »%s --help« auf, um eine vollständige Liste der verfügbaren Befehlszeileoptionen zu erhalten. SANE-Gerät, von dem Bilder angefordert werden. Speichern Speichern unter … Aktuelles Dokument speichern? Dokument vor dem Beenden speichern? Als Datei speichern Dokument wird gespeichert … Seite %d von %d wird gespeichert Sc_annen Scannen Dokumente scannen Scan-_Quelle: Zu scannende Seite: Eine einzelne Seite vom Scanner einscannen Gescanntes Dokument.pdf Versionsinformation anzeigen Simple Scan Simple Scan verwendet das SANE-Programmiergerüst für die Unterstützung der meisten verfügbaren Scanner. Einfaches Werkzeug zum Scannen von Dokumenten _Einzelne Seite Neues Dokument erstellen Anhalten Aktuellen Scanvorgang unterbrechen Text Helligkeitseinstellung von -100 bis 100 (0 keine). Kontrasteinstellung von -100 bis 100 (0 keine). Das Verzeichnis, in dem die Dateien gespeichert werden. Vorgabe ist der »Dokumente«-Ordner, wenn nichts eingestellt ist. Papierhöhe in Zehntelmillimetern (oder 0 für automatische Papiererkennung) Die zu scannende Seite. Die für Fotos zu verwendende Auflösung in DPI (Punkte pro Zoll) Die für Text zu verwendende Auflösung in DPI (Punkte pro Zoll) Seitenbreite in Zehntelmillimetern (oder 0 für automatische Papiererkennung) Dieses Programm ist freie Software, Sie können es weitergeben
und/oder verändern, solange Sie sich an die Vorgaben der GNU
General Public License halten, wie von der Free Software
Foundation festgelegt; entweder in Version 3 der Lizenz oder
(nach Ihrem Ermessen) in jeder folgenden Version.

Das Programm wurde mit dem Ziel veröffentlicht, dass Sie es
nützlich finden, jedoch OHNE JEDWEDE GARANTIE, sogar
ohne eine implizite Garantie der VERKAUFBARKEIT oder der
NUTZBARKEIT FÜR EINEN BESTIMMTEN ZWECK. Für mehr
Informationen lesen Sie bitte die GNU General Public License
(GNU GPL).

Mit dem Programm sollten Sie außerdem eine Kopie der GNU
General Public License erhalten haben. Falls dem nicht so
ist, finden Sie diese im Internet unter
<http://www.gnu.org/licenses/>. Typ des zu scannenden Dokuments Typ des zu scannenden Dokuments. Diese Einstellung entscheidet über die Auflösung, Farben und Nachbearbeitung. Verbindung zum Scanner konnte nicht hergestellt werden Die Hilfedatei konnte nicht geöffnet werden Bildvorschauanwendung konnte nicht gestartet werden Vorschaubild kann nicht gespeichert werden Scanvorgang konnte nicht gestartet werden Für den Zugriff auf »%s« werden ein Benutzername und ein Passwort benötigt Seitenbreite in Zehntelmillimetern Scheinbar ist ein Brother Scanner angeschlossen. Scheinbar ist ein Samsung Scanner angeschlossen. Scheinbar ist ein Epson Scanner angeschlossen. Scheinbar ist ein HP Scanner angeschlossen. Sie müssen die Treibersoftware für Ihren Scanner installieren Sie müssen %s Paket(e) installieren. [GERÄT …] - Scan-Werkzeug _Legitimieren Abbre_chen S_chließen I_nhalt _Zuschneiden _Benutzerdefiniert _Dokument _E-Mail _Hilfe _Treiber installieren US _Letter _Nicht zuschneiden _Seite _Passwort: _Fotoauflösung: Zuschneideauswahl _drehen _Speichern _Scanvorgang anhalten _Textauflösung: _Benutzername für Quelle: scan;einlesen;scanner;Flachbrett;flatbed;AVW;Vorlagenwechsler;adf; Launchpad Contributions:
  .daniel. https://launchpad.net/~faessje
  Alexander Wilms https://launchpad.net/~alexander-wilms
  Alvaro Aleman https://launchpad.net/~alvaroaleman
  Charon https://launchpad.net/~markus-lobedann
  Daniel Schury https://launchpad.net/~surst
  Daniel Winzen https://launchpad.net/~q-d
  Dennis Baudys https://launchpad.net/~thecondordb
  Dennisgamer https://launchpad.net/~dennis-ertelt
  Dominik Grafenhofer https://launchpad.net/~dpjg
  EgLe https://launchpad.net/~egle1
  Ettore Atalan https://launchpad.net/~atalanttore
  Felix https://launchpad.net/~apoapo
  Franz E. https://launchpad.net/~franzellendorff
  Funky Future https://launchpad.net/~funky-future
  Ghenrik https://launchpad.net/~ghenrik-deactivatedaccount
  HOMBRESINIESTRO https://launchpad.net/~hombre
  Hendrik Brandt https://launchpad.net/~heb
  Hendrik Knackstedt https://launchpad.net/~hennekn
  Jan https://launchpad.net/~jancborchardt-deactivatedaccount
  John Doe https://launchpad.net/~jodo-deactivatedaccount
  Julian Gehring https://launchpad.net/~julian-gehring
  Jörg BUCHMANN https://launchpad.net/~jorg-buchmann
  Lars Vopicka https://launchpad.net/~lars+vopicka-deactivatedaccount
  Marcel Schmücker https://launchpad.net/~versus666-deactivatedaccount
  Martin Lettner https://launchpad.net/~m.lettner
  Mathias Dietrich https://launchpad.net/~theghost
  Moritz Baumann https://launchpad.net/~mo42
  Phillip Sz https://launchpad.net/~phillip-sz
  Raphael J. Schmid https://launchpad.net/~raphael-j-schmid
  Scherbruch https://launchpad.net/~scherbruch
  Stefan Buchholz https://launchpad.net/~stef-buchholz
  Steve G. https://launchpad.net/~sgo.ger
  Thomas Heidrich https://launchpad.net/~gnuheidix
  Tobias Bannert https://launchpad.net/~toba
  Torsten Franz https://launchpad.net/~torsten.franz
  UweS https://launchpad.net/~uwes
  Webschiff https://launchpad.net/~webschiff
  William Glover https://launchpad.net/~williamglover
  schuko24 https://launchpad.net/~gerd-saenger
  simon danner https://launchpad.net/~simondanner
  staedtler-przyborski https://launchpad.net/~staedtler-przyborski
  tlue https://launchpad.net/~tlueber 