��          �      l      �  i   �  E   K  Q   �     �     �     �               +  
   7     B     Z     l     ~     �     �     �     �     �     �  �    l   �  E   7  Q   }     �     �     �     �          !  
   -      8     Y     n     �     �     �     �     �     	     "                       
   	                                                                        <a href="https://plus.google.com/communities/108331279007926658904" class="caption">Google+ Community</a> <a href="https://twitter.com/ubuntu_mate" class="caption">Twitter</a> <a href="https://www.facebook.com/UbuntuMATEedition" class="caption">Facebook</a> About Announcements Banshee Media Player Chromium Customization options Development Discussion GDebi Package Installer GIMP Image Editor Included software Language support LibreOffice Calc LibreOffice Impress LibreOffice Writer Shotwell Photo Manager Synaptic Package Manager Welcome to Ubuntu MATE Project-Id-Version: ubiquity-slideshow-ubuntu
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-03-24 23:23+0200
PO-Revision-Date: 2016-06-22 11:59+0000
Last-Translator: Ettore Atalan <atalanttore@googlemail.com>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:35+0000
X-Generator: Launchpad (build 18115)
 <a href="https://plus.google.com/communities/108331279007926658904" class="caption">Google+ Gemeinschaft</a> <a href="https://twitter.com/ubuntu_mate" class="caption">Twitter</a> <a href="https://www.facebook.com/UbuntuMATEedition" class="caption">Facebook</a> Über Bekanntmachungen Banshee-Medienwiedergabe Chromium Anpassungsmöglichkeiten Entwicklung Diskussion GDebi-Paketinstallationsprogramm GIMP-Bildbearbeitung Enthaltene Anwendungen Sprachunterstützung LibreOffice-Tabellenkalkulation LibreOffice-Präsentation LibreOffice-Textverarbeitung Shotwell-Fotoverwaltung Synaptic-Paketverwaltung Willkommen bei Ubuntu MATE 