��    U      �  q   l      0  "   1     T  8   t  <   �  5   �  >      $   _  2   �  B   �  2   �  >   -	  G   l	  =   �	  ?   �	  :   2
  0   m
  %   �
     �
     �
  $   �
  Z        r     �  )   �     �  w   �     n     �  *   �     �  #   �  !     "   3      V     w     �  )   �  &   �  E   �  H   @  $   �  '   �  .   �  -     !   3     U  "   k  <   �  B   �  C     *   R  C   }  *   �  C   �  *   0      [  ,   |  D   �     �  F     /   N  -   ~     �  ;   �     �     
  !   "  $   D     i     �     �      �     �     �               8     U  .   s  -   �     �  9   �     *     F  �  f  6   ?  "   v  R   �  U   �  I   B  o   �  J   �  E   G  `   �  O   �  @   >  p     J   �  Y   ;  O   �  L   �  -   2  ,   `  !   �  5   �  p   �  (   V  '     0   �  0   �  �   	     �     �  /   �  %     -   >  4   l  5   �  3   �  0         <   @   X   I   �   J   �   M   .!  1   |!  6   �!  5   �!  C   "  2   _"  -   �"  1   �"  <   �"  c   /#  v   �#  3   
$  r   >$  /   �$  r   �$  /   T%  %   �%  ,   �%  ]   �%     5&  S   O&  @   �&  >   �&     #'  \   <'     �'     �'  2   �'  *   �'     )(  !   E(  #   g(  +   �(  $   �(     �(     �(  %   )  $   +)  %   P)  /   v)  &   �)  <   �)  N   
*  *   Y*  ,   �*             >   P          D   %            K      1      8   O      ,       U   /   M   $      )   -       L   J   
              +   Q             '               *               R   	         !   :   9   5       0   4   #       ?          =       2          <               6   3           E       B      N         H   &       A      T   C   (              G       .          I   "   S      F              7   ;      @    !
Ensure that it has been loaded.
 %s: ASSERT: Invalid option: %d
 %s: Could not allocate memory for subdomain mount point
 %s: Could not allocate memory for subdomainbase mount point
 %s: Errors found during regex postprocess. Aborting.
 %s: Errors found in combining rules postprocessing. Aborting.
 %s: Errors found in file. Aborting.
 %s: Failed to compile regex '%s' [original: '%s']
 %s: Failed to compile regex '%s' [original: '%s'] - malloc failed
 %s: Illegal open {, nesting groupings not allowed
 %s: Internal buffer overflow detected, %d characters exceeded
 %s: Regex grouping error: Invalid close }, no matching open { detected
 %s: Regex grouping error: Invalid number of items between {}
 %s: Regex grouping error: Unclosed grouping, expecting close }
 %s: Sorry. You need root privileges to run this program.

 %s: Subdomain '%s' defined, but no parent '%s'.
 %s: Two SubDomains defined for '%s'.
 %s: Unable to add "%s".   %s: Unable to find  %s: Unable to parse input line '%s'
 %s: Unable to query modules - '%s'
Either modules are disabled or your kernel is too old.
 %s: Unable to remove "%s".   %s: Unable to replace "%s".   %s: Unable to write entire profile entry
 %s: Unable to write to stdout
 %s: Warning! You've set this program setuid root.
Anybody who can run this program can update your AppArmor profiles.

 %s: error near                %s: error reason: '%s'
 (ip_mode) Found unexpected character: '%s' Addition succeeded for "%s".
 AppArmor parser error, line %d: %s
 Assert: 'hat rule' returned NULL. Assert: `addresses' returned NULL. Assert: `netrule' returned NULL. Assert: `rule' returned NULL. Bad write position
 Couldn't copy profile Bad memory address
 Couldn't merge entries. Out of Memory
 Default allow subdomains are no longer supported, sorry. (domain: %s) Default allow subdomains are no longer supported, sorry. (domain: %s^%s) ERROR in profile %s, failed to load
 Error couldn't allocate temporary file
 Error: #include %s%c not found. line %d in %s
 Error: Can't add directory %s to search path
 Error: Could not allocate memory
 Error: Out of Memory
 Error: bad include. line %d in %s
 Error: could not allocate buffer for include. line %d in %s
 Error: exceeded %d levels of includes.  NOT processing %s include
 Exec qualifier 'i' invalid, conflicting qualifier already specified Exec qualifier 'i' must be followed by 'x' Exec qualifier 'p' invalid, conflicting qualifier already specified Exec qualifier 'p' must be followed by 'x' Exec qualifier 'u' invalid, conflicting qualifier already specified Exec qualifier 'u' must be followed by 'x' Found unexpected character: '%s' Internal: unexpected mode character in input Invalid mode, 'x' must be preceded by exec qualifier 'i', 'u' or 'p' Memory allocation error. Negative subdomain entries are no longer supported, sorry. (entry: %s) Network entries can only have one FROM address. Network entries can only have one TO address. Out of memory
 PANIC bad increment buffer %p pos %p ext %p size %d res %p
 Permission denied
 Profile already exists
 Profile does not match signature
 Profile doesn't conform to protocol
 Profile doesn't exist
 Profile version not supported
 Removal succeeded for "%s".
 Replacement succeeded for "%s".
 Unable to open %s - %s
 Unknown error
 Warning (line %d):  `%s' is not a valid ip address. `%s' is not a valid netmask. `/%d' is not a valid netmask. md5 signature given without execute privilege. missing an end of line character? (entry: %s) ports must be between %d and %d profile %s: has merged rule %s with multiple x modifiers
 unable to create work area
 unable to serialize profile %s
 Project-Id-Version: apparmor-parser
Report-Msgid-Bugs-To: <apparmor@lists.ubuntu.com>
POT-Creation-Date: 2005-03-31 13:39-0800
PO-Revision-Date: 2016-04-13 10:49+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Novell Language <language@novell.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:13+0000
X-Generator: Launchpad (build 18115)
Language: de
 !
Stellen Sie sicher, dass das Element geladen wurde.
 %s: ASSERT: Ungültige Option: %d
 %s: Es konnte kein Speicher für den Subdomänen-Einhängepunkt reserviert werden
 %s: Dem Einhängepunkt der Unterdomänenbasis konnte kein Speicher zugeordnet werden
 %s: Während der regex-Nachverarbeitung wurden Fehler gefunden. Abbruch.
 %s: Beim Kombinieren von Regeln in der Nachverarbeitung sind Fehler aufgetreten. Der Vorgang wird abgebrochen.
 %s: In der Datei wurde ein Fehler gefunden. Der Vorgang wird abgebrochen.
 %s: Fehler beim Kompilieren von regex »%s« [ursprünglich: »%s«]
 %s: Fehler beim Kompilieren von regex »%s« [ursprünglich: »%s«] - malloc nicht erfolgreich
 %s: Öffnen mit { ungültig, verschachtelte Gruppierungen sind nicht zulässig
 %s: Interner Pufferüberlauf erkannt, %d Zeichen überschritten
 %s: Regex-Gruppierungsfehler: Ungültiges schließendes Zeichen }, kein passendes öffnendes Zeichen { gefunden
 %s: Regex-Gruppierungsfehler: Ungültige Anzahl an Einträgen zwischen {}
 %s: Regex-Gruppierungsfehler: Nicht geschlossene Gruppierung, abschließendes } erwartet
 »%s«: Sie benötigen Systemverwalterrechte zum Ausführen dieses Programms.

 %s: Subdomäne »%s« festgelegt, aber kein übergeordnetes Element »%s«.
 %s: Zwei Subdomänen für »%s« festgelegt.
 %s: Hinzufügen von »%s« nicht möglich.   %s: Konnte nicht gefunden werden  %s: Eingabezeile »%s« kann nicht analysiert werden
 %s: Abfrage der Module nicht möglich - »%s«
Die Module sind entweder deaktiviert oder der Kernel ist zu alt.
 %s: »%s« kann nicht entfernt werden.   %s: »%s« kann nicht ersetzt werden.   %s: Profileintrag kann nicht geschrieben werden
 %s: Schreiben in Standardausgabe nicht möglich
 %s: Achtung! Sie haben für dieses Programm »setuid root« festgelegt.
Alle Personen, die dieses Programm ausführen, können Ihre AppArmor-Profile aktualisieren.

 %s: Fehler bei                %s: Fehlerursache: »%s«
 (ip_mode) Unerwartetes Zeichen gefunden: »%s« Hinzufügen für »%s« erfolgreich.
 AppArmor-Analysefehler, Zeile »%d«: »%s«
 Assert: Für »hat rule« wurde NULL zurückgegeben. Assert: Für »addresses« wurde NULL zurückgegeben. Assert: Für »netrule« wurde NULL zurückgegeben. Assert: Für »rule« wurde NULL zurückgegeben. Ungültige Schreibposition
 Ungültige Speicheradresse - Profil konnte nicht kopiert werden
 Einträge konnten nicht zusammengeführt werden. Kein Speicher vorhanden
 Subdomänen werden nicht länger unterstützt, tut uns Leid. (Domäne: %s) Subdomänen werden nicht länger unterstützt, tut uns Leid. (Domäne: %s^%s) FEHLER in Profil %s, konnte nicht geladen werden
 Fehler: Temporäre Datei konnte nicht angelegt werden
 Fehler: #include %s%c nicht gefunden. Zeile %d in %s
 Fehler: Verzeichnis %s kann nicht zum Suchpfad hinzugefügt werden
 Fehler: Es konnte kein Speicher reserviert werden
 Fehler: Kein freier Speicher mehr verfügbar
 Fehler: Fehlerhafte Inkludierung. Zeile %d in %s
 Fehler: Speicher konnte nicht belegt werden. Zeile %d in %s
 Fehler: %d Stufen von Inkludierungen überschritten. %s Inkludierung wird/werden NICHT verarbeitet
 Ausführungskennzeichner »i« ist ungültig, ein Kennzeichner, mit dem ein Konflikt besteht, wurde bereits angegeben. Auf den Exec-Qualifizierer »i« muss »x« folgen. Exec-Qualifizierer »p« ist ungültig, ein mit diesem in Konflikt stehender Qualifizierer wurde bereits angegeben Nach Exec-Qualifizierer »p« muss »x« folgen Exec-Qualifizierer »u« ist ungültig, ein mit diesem in Konflikt stehender Qualifizierer wurde bereits angegeben Nach Exec-Qualifizierer »u« muss »x« folgen Unerwartetes Zeichen gefunden: »%s« Intern: Unerwartetes Moduszeichen in Eingabe Ungültiger Modus, vor »x« muss einer der Exec-Qualifizierer »i«, »u« oder »p« stehen Speicherzuordnungsfehler. Negative Subdomänen werden nicht länger unterstützt, tut uns Leid. (Eintrag: %s) Netzwerkeinträge können nur über eine FROM-Adresse verfügen. Netzwerkeinträge können nur über eine TO-Adresse verfügen. Kein Speicher vorhanden
 PANIC - ungültiger Inkrement-Puffer %p Position %p Erweiterung %p Größe %d Auflösung %p
 Zugriff verweigert
 Profil ist bereits vorhanden
 Das Profil stimmt nicht mit der Signatur überein
 Das Profil entspricht nicht dem Protokoll
 Profil ist nicht vorhanden
 Profilversion nicht unterstützt
 Entfernen für »%s« erfolgreich.
 Ersetzungsvorgang für »%s« erfolgreich.
 %s kann nicht geöffnet werden - %s
 Unbekannter Fehler
 Achtung (Zeile %d):  »%s« ist keine gültige IP-Adresse. »%s« ist keine gültige Netzmaske. »/%d« ist keine gültige Netzmaske. Md5-Signatur ohne Ausführungsrecht übergeben. Fehlt ein Zeilenumbruch? (Eintrag: %s) Zwischen %d und %d müssen sich Anschlüsse (Ports) befinden Profil %s: verfügt über zusammengeführte Regel %s mit mehreren x-Modifiern
 Arbeitsbereich kann nicht erstellt werden
 Serialisierung von Profil %s nicht möglich
 