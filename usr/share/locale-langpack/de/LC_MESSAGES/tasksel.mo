��          4      L       `   j  a      �  �  �  q  �                         Usage:
tasksel install <task>...
tasksel remove <task>...
tasksel [options]
	-t, --test          test mode; don't really do anything
	    --new-install   automatically install some tasks
	    --list-tasks    list tasks that would be displayed and exit
	    --task-packages list available packages in a task
	    --task-desc     returns the description of a task
 apt-get failed Project-Id-Version: tasksel
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-01-02 02:46+0000
PO-Revision-Date: 2015-12-22 01:19+0000
Last-Translator: Jens Seidel <jensseidel@users.sf.net>
Language-Team: Debian German <debian-l10n-german@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:37+0000
X-Generator: Launchpad (build 18115)
Language: 
 Verwendung:
tasksel install <Task>...
tasksel remove <Task>...
tasksel [Optionen]
	-t, --test          Test-Modus. Nichts ändern
	    --new-install   Installiert einige Tasks automatisch
	    --list-tasks    Zeigt Tasks an und verlässt das Programm
	    --task-packages Zeigt vorhandene Pakete in einem Task
	    --task-desc     Gibt die Beschreibung eines Tasks aus
 Aptitude fehlgeschlagen 