��          4      L       `   �   a   (   �   �    �   �  B   ;                    URL of a service that can be contacted and returns an XML file containing location information about the requestor's IP address. URL used to get information about the IP Project-Id-Version: ubuntu-geoip
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-04 02:07+0000
PO-Revision-Date: 2013-12-18 14:39+0000
Last-Translator: Phillip Sz <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:58+0000
X-Generator: Launchpad (build 18115)
 URL eines Dienstes, der kontaktiert werden kann und eine XML-Datei mit Informationen über den Standort der IP-Adresse des Antragstellers zurückgibt. URL, die verwendet wird, um Informationen über die IP zu beziehen 