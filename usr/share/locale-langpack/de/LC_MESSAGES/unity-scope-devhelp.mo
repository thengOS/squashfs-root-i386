��          \      �       �      �      �      �   ;   �      !  �   5       �  ,     �  #   �       W        i  �   ~  7   z                                       Devhelp Search Devhelp Show Sorry, there are no Devhelp results that match your search. Technical Documents This is an Ubuntu search plugin that enables information from Devhelp to be searched and displayed in the Dash underneath the Code header. If you do not wish to search this content source, you can disable this search plugin. devhelp;dev;help;doc; Project-Id-Version: unity-scope-devhelp
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-28 09:22+0000
PO-Revision-Date: 2014-04-29 14:41+0000
Last-Translator: Phillip Sz <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:23+0000
X-Generator: Launchpad (build 18115)
 Entwicklerdokumentation Entwicklerdokumentation durchsuchen Anzeigen Entschuldigung, es gibt zu Ihrer Suche in der Entwicklerdokumentation keine Ergebnisse. Technische Dokumente Dies ist eine Ubuntu-Such-Erweiterung, die es möglich macht Informationen der Entwicklerdokumentation zu durchsuchen und im Dash unter Quelltext anzuzeigen. Wenn Sie diese Quelle nicht durchsuchen möchten, können Sie diese Erweiterung deaktivieren. Hilfe;Dokumentation;Entwicklerdokumentation;Entwickler; 