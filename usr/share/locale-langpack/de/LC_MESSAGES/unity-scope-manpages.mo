��    	      d      �       �      �      �      �      �   4        H  �   \     D  �  R     �     �       )     _   I     �    �     �        	                                      Manpages Open Search Manpages Search for Manpages Sorry, there are no Manpages that match your search. Technical Documents This is an Ubuntu search plugin that enables information from local manpages to be searched and displayed in the Dash underneath the Code header. If you do not wish to search this content source, you can disable this search plugin. manpages;man; Project-Id-Version: unity-scope-manpages
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-24 16:55+0000
PO-Revision-Date: 2014-04-29 14:43+0000
Last-Translator: Phillip Sz <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:23+0000
X-Generator: Launchpad (build 18115)
 Hilfeseiten Öffnen Hilfeseiten durchsuchen Nach Einträgen in den Hilfeseiten suchen Leider konnten keine Einträge in den Hilfeseiten gefunden werden, die Ihrer Suche entsprechen. Technische Dokumente Dies ist eine Ubuntu-Such-Erweiterung, welche es ermöglicht Informationen in den lokalen Hilfeseiten zu suchen und in der Dash unterhalb der Informations-Kopfzeile anzuzeigen. Falls Sie diese Quelle nicht durchsuchen wollen, können Sie diese Erweiterung deaktivieren. manpages;man;Hilfeseiten;Hilfe; 