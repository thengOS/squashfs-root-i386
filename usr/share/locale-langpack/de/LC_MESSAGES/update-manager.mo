��    s      �  �   L      �	  1  �	     �
       &        >  ,   F     s     �     �  �   �  �   M            	        (  F   0  *   w  :   �     �     �            ,   :     g     s  &   �  &   �  !   �     �     �       O   4  .   �     �     �     �     �     �  �     A   �     �  K     "   c      �     �     �     �     �          "  %   B  G   h     �  2  �  O   �     B     S     _     k  #   �  "   �     �  8   �  '     )   C     m     �     �     �  2   �  0   �     "     B     V  (   l  �   �  I   B  ;   �  "   �  �   �  ,   �  Q   �  �         �  A     ,   H     u     �     �     �     �  Y   �  O   ,  4   |     �  A   �  
   �       �     V   �  D   �  @   3  P   t  A   �  L     "   T     w  	   �     �     �     �     �  
   �     �  �  �  t  �      D"     `"  )   h"     �"  .   �"     �"  
   �"     �"  �   #  �   �#  #   �$  	   �$     �$     �$  Q   �$  5   A%  N   w%  #   �%     �%  &    &  O   '&  5   w&     �&  !   �&  *   �&  /   '  (   7'     `'  (   n'  .   �'  o   �'  5   6(     l(  /   y(     �(     �(  '   �(  �   )  Q   �)     A*  `   ]*  *   �*  5   �*  1   +     Q+  "   p+     �+     �+  %   �+  D   �+  V   1,     �,  �  �,  ^   .     ~.     �.     �.     �.  %   �.  6   �.      1/  B   R/  0   �/  6   �/  (   �/     &0     @0  )   Q0  G   {0  I   �0  -   1     ;1     P1  >   h1  �   �1  K   y2  _   �2  &   %3  �   L3  0   4  f   E4  L  �4  J   �5  Z   D6  J   �6      �6     7     '7     :7  $   O7  {   t7  k   �7  S   \8     �8  g   �8     )9     ;9  �   I9  �    :  P   �:  >   ;  _   A;  i   �;  [   <  5   g<     �<     �<     �<  ,   �<     �<     
=      =     2=        +                  $   W   7          k   '                @   g   K      9   R           3      6       D   C               <   .   f      L   l       	   Q   X       e          s      Z   ?   N   P   &   F          4           a   B   ]   [               (                 "   p   ,   c   b   1   U   j   5       /   A   :          #   G      E      d       
   _          S      h   -      n              J       m   \   M       `               2       )       >   T       0      i   *   %   o   ;   V   8             r   !   ^       =       I   q                            H   Y      O    
A normal upgrade can not be calculated, please run:
  sudo apt-get dist-upgrade


This can be caused by:
 * A previous upgrade which didn't complete
 * Problems with some of the installed software
 * Unofficial software packages not provided by Ubuntu
 * Normal changes of a pre-release version of Ubuntu %(size).0f kB %(size).0f kB %.1f MB %i obsolete entries in the status file %s base %s needs to be marked as manually installed. %s will be downloaded. .deb package A file on disk An unresolvable problem occurred while calculating the upgrade.

Please report this bug against the 'update-manager' package and include the following error message:
 An unresolvable problem occurred while initializing the package information.

Please report this bug against the 'update-manager' package and include the following error message:
 Building Updates List Cancel Changelog Changes Changes for %s versions:
Installed version: %s
Available version: %s

 Check if a new Ubuntu release is available Check if upgrading to the latest devel release is possible Checking for updates… Connecting... Copy Link to Clipboard Could not calculate the upgrade Could not initialize the package information Description Details of updates Directory that contains the data files Do not check for updates when starting Do not focus on map when starting Download Downloading changelog Downloading list of changes... Failed to download the list of changes. 
Please check your Internet connection. However, %s %s is now available (you have %s). Install Install All Available Updates Install Now Install missing package. Installing updates… It is impossible to install or remove any software. Please use the package manager "Synaptic" or run "sudo apt-get install -f" in a terminal to fix this issue at first. It’s safer to connect the computer to AC power before updating. No longer downloadable: No network connection detected, you can not download changelog information. No software updates are available. Not all updates can be installed Not enough free disk space Obsolete dpkg status entries Obsolete entries in dpkg status Open Link in Browser Other updates Package %s should be installed. Please wait, this can take some time. Remove lilo since grub is also installed.(See bug #314004 for details.) Restart _Later Run a partial upgrade, to install as many updates as possible.

    This can be caused by:
     * A previous upgrade which didn't complete
     * Problems with some of the installed software
     * Unofficial software packages not provided by Ubuntu
     * Normal changes of a pre-release version of Ubuntu Run with --show-unsupported, --show-supported or --show-all to see more details Security updates Select _All Settings… Show all packages in a list Show all packages with their status Show and install available updates Show debug messages Show description of the package instead of the changelog Show supported packages on this machine Show unsupported packages on this machine Show version and exit Software Updater Software Updates Software index is broken Software updates are no longer provided for %s %s. Some software couldn’t be checked for updates. Support status summary of '%s': Supported until %s: Technical description Test upgrade with a sandbox aufs overlay The changelog does not contain any relevant changes.

Please use http://launchpad.net/ubuntu/+source/%s/%s/+changelog
until the changes become available or try again later. The computer also needs to restart to finish installing previous updates. The computer needs to restart to finish installing updates. The computer will need to restart. The list of changes is not available yet.

Please use http://launchpad.net/ubuntu/+source/%s/%s/+changelog
until the changes become available or try again later. The software on this computer is up to date. The update has already been downloaded. The updates have already been downloaded. The upgrade needs a total of %s free space on disk '%s'. Please free at least an additional %s of disk space on '%s'. Empty your trash and remove temporary packages of former installations using 'sudo apt-get clean'. There are no updates to install. This update does not come from a source that supports changelogs. To stay secure, you should upgrade to %s %s. Unimplemented method: %s Unknown download size. Unsupported Unsupported:  Update is complete Updated software has been issued since %s %s was released. Do you want to install it now? Updated software is available for this computer. Do you want to install it now? Updated software is available from a previous check. Updates Upgrade using the latest proposed version of the release upgrader Upgrade… Version %s: 
 When upgrading, if kdelibs4-dev is installed, kdelibs5-dev needs to be installed. See bugs.launchpad.net, bug #279621 for details. You are connected via roaming and may be charged for the data consumed by this update. You have %(num)s packages (%(percent).1f%%) supported until %(time)s You have %(num)s packages (%(percent).1f%%) that are unsupported You have %(num)s packages (%(percent).1f%%) that can not/no-longer be downloaded You may not be able to check for updates or download new updates. You may want to wait until you’re not using a mobile broadband connection. You stopped the check for updates. _Check Again _Continue _Deselect All _Partial Upgrade _Remind Me Later _Restart Now… _Try Again updates Project-Id-Version: update-manager
Report-Msgid-Bugs-To: sebastian.heinlein@web.de
POT-Creation-Date: 2016-04-12 03:58+0000
PO-Revision-Date: 2016-06-04 14:32+0000
Last-Translator: Torsten Franz <Unknown>
Language-Team: German GNOME Translations <gnome-de@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:19+0000
X-Generator: Launchpad (build 18115)
Language: 
 
Eine normale Aktualisierung konnte nicht berechnet werden. Bitte starten Sie:
  sudo apt-get dist-upgrade


Das kann folgende Ursachen haben:
 * Eine vorherige Aktualisierung, die nicht beendet wurde
 * Probleme mit installierten Anwendungen
 * Inoffizielle Anwendungspakete, die nicht von Ubuntu bereitgestellt wurden
 * Normale Änderungen einer Vorabversion von Ubuntu %(size).0f kB %(size).0f kB %.1f MB %i unnötige Einträge in der Statusdatei %s-Kern %s muss als manuell installiert markiert sein. %s werden heruntergeladen .deb-Paket Datei auf der Festplatte Ein unlösbares Problem ist beim Berechnen der Aktualisierungen aufgetreten.

Bitte berichten Sie diesen Fehler für das Paket »update-manager« und geben Sie die folgende Fehlermeldung an:
 Ein unlösbares Problem ist während der Initialisierung der Paketinformation aufgetreten.

Bitte melden Sie einen Fehler im Paket »update-manager« und fügen Sie die folgende Fehlermeldung an den Fehlerbericht an:
 Aktualisierungsliste wird aufgebaut Abbrechen Änderungsprotokoll Änderungen Änderungen für %s Versionen:
Installierte Version: %s
Verfügbare Version: %s

 Auf Verfügbarkeit einer neuen Ubuntu-Version prüfen Möglichkeit einer Aktualisierung auf die aktuelle Entwicklungsversion prüfen Aktualisierungen werden gesucht … Verbindungsaufbau … Verweis in die Zwischenablage kopieren Es konnte nicht ermittelt werden, welche Systemaktualisierungen verfügbar sind Paketinformationen konnten nicht initialisiert werden Beschreibung Einzelheiten der Aktualisierungen Verzeichnis, das die Datendateien enthält Beim Starten nicht auf Aktualisierungen prüfen Beim Starten die Karte nicht fokussieren Herunterladen Änderungsprotokoll wird heruntergeladen Liste der Änderungen wird heruntergeladen … Die Liste der Änderungen konnte nicht heruntergeladen werden. 
Bitte überprüfen Sie Ihre Internetverbindung. Allerdings ist %s %s jetzt verfügbar (Sie haben %s). Installieren Alle verfügbaren Aktualisierungen installieren Jetzt installieren Fehlendes Paket installieren. Aktualisierungen werden installiert … Das Installieren und Entfernen von Anwendungen ist zurzeit nicht möglich. Bitte verwenden Sie die Paketverwaltung »Synaptic« oder führen Sie den Befehl »sudo apt-get install -f« in einem Terminal aus, um dieses Problem zu beheben. Es ist sicherer, den Rechner vor der Aktualisierung ans Stromnetz anzuschließen. Nicht mehr herunterzuladen: Es wurde keine Netzwerkverbindung erkannt, Sie können keine Änderungsprotokolle herunterladen. Es sind keine Aktualisierungen verfügbar. Nicht alle Aktualisierungen können installiert weden Nicht genug freier Festplattenspeicher verfügbar Unnötige dpkg-Statuseinträge Unnötige Einträge im dpkg-Status Verweis im Browser öffnen Andere Aktualisierungen Das Paket %s sollte installiert sein. Bitte warten Sie, dieser Vorgang kann etwas Zeit in Anspruch nehmen. LILO entfernen, da auch GRUB installiert ist (weitere Einzelheiten siehe Bug #314004). Neustart _verschieben Starten Sie eine teilweise Systemaktualisierung, um so viele Aktualisierungen wie möglich zu installieren.

Das kann folgende Ursachen haben:
    * Eine vorherige Aktualisierung, die nicht beendet wurde
    * Probleme mit installierten Anwendungen
    * Inoffizielle Anwendungspakete, die nicht von Ubuntu bereitgestellt wurden
    * Normale Änderungen einer Vorabversion von Ubuntu Für weitere Informationen mit --show-unsupported, --show-supported oder --show-all ausführen Sicherheitsaktualisierungen _Alles auswählen Einstellungen … Alle Pakete auflisten Alle Pakete mit ihrem Status anzeigen Verfügbare Aktualisierungen anzeigen und installieren Fehlerdiagnosemeldungen anzeigen Statt der Beschreibung des Pakets das Änderungsprotokoll anzeigen Unterstützte Pakete auf diesem Rechner anzeigen Nicht unterstützte Pakete auf diesem Rechner anzeigen Version anzeigen und Programm schließen Aktualisierungsverwaltung Aktualisierungen Das Anwendungsverzeichnis ist beschädigt Für %s %s werden keine Anwendungsaktualisierungen mehr bereitgestellt. Einige Anwendungen konnten nicht auf Aktualisierungen überprüft werden. Zusammenfassung der Unterstützung für '%s': Unterstützt bis %s: Technische Beschreibung Testen der Systemaktualisierung mit einem Sandbox-aufs-Overlay Das Änderungsprotokoll enthält keine relevanten Änderungen.

Bitte besuchen Sie http://launchpad.net/ubuntu/+source/%s/%s/+changelog
bis die Änderungen verfügbar sind oder versuchen Sie es später erneut. Der Rechner muss zum Abschließen der Aktualisierungen neugestartet werden. Der Rechner muss neu gestartet werden, um die Installation der Aktualisierungen abzuschließen. Der Rechner muss neu gestartet werden. Die Änderungsprotokolle sind noch nicht verfügbar.

Bitte nutzen Sie http://launchpad.net/ubuntu/+source/%s/%s/+changelog
bis die Änderungen verfügbar werden oder versuchen Sie es später erneut. Die Anwendungen auf diesem Rechner sind aktuell. Die Aktualisierung wurde bereits heruntergeladen. Die Aktualisierungen wurden bereits heruntergeladen. Die Systemaktualisierung benötigt %s an freiem Speicherplatz auf der Festplatte »%s«. Bitte stellen Sie mindestens %s an zusätzlichem Speicherplatz auf der Festplatte »%s« zur Verfügung. Leeren Sie beispielsweise den Müll und löschen Sie temporäre Pakete aus früheren Installationen mit dem Befehl »sudo apt-get clean«. Es sind keine Aktualisierungen vorhanden, die installiert werden könnten. Diese Aktualisierung stammt nicht aus einer Quelle, die Änderungsprotokolle unterstützt. Um kein Sicherheitsrisiko einzugehen, sollten Sie auf %s %s aktualisieren. Nicht implementierte Methode: %s Unbekannte Download-Größe Nicht unterstützt Nicht unterstützt:  Die Aktualisierung ist abgeschlossen Aktualisierte Anwendungen wurden seit der Veröffentlichung von %s %s herausgegeben. Möchten Sie diese jetzt installieren? Aktualisierte Anwendungen stehen für diesen Rechner zur Verfügung. Möchten Sie diese jetzt installieren? Aktualisierte Anwendungen sind aus einer früheren Aktualisierungssuche verfügbar. Aktualisierungen Systemaktualisierung unter Verwendung der letzten vorgeschlagenen Version der Aktualisierungsverwaltung Aktualisieren … Version %s: 
 Falls bei der Systemaktualisierung kdelibs4-dev installiert ist, so muss kdelibs5-dev installiert werden. Sehen Sie auf der Webseite bugs.launchpad.net nach dem Fehler #279621, um weitere Informationen zu erhalten. Sie sind per Roaming verbunden und es werden Ihnen möglicherweise Kosten für die Datenübertragung dieser Aktualisierung in Rechnung gestellt. Sie haben %(num)s Pakete (%(percent).1f%%), die bis %(time)s unterstützt werden Sie haben %(num)s nicht unterstützte Pakete (%(percent).1f%%) Sie haben %(num)s Pakete (%(percent).1f%%), die nicht/nicht mehr heruntergeladen werden können Sie können möglicherweise nicht nach neuen Aktualisierungen suchen oder Aktualisierungen herunterladen. Eventuell ist es besser zu warten, bis Sie keine mobile Breitbandverbindung mehr verwenden. Sie haben die Suche nach Aktualisierungen angehalten. Erneut _prüfen _Fortsetzen Auswahl au_fheben Teilweise Systemaktualisierung _durchführen _Später erinnern Jetzt neusta_rten … Erneut _versuchen Aktualisierungen 