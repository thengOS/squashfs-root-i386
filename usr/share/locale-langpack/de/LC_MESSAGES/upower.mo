��    
      l      �       �   #   �   #        9      R     s  &   �     �      �  "   �  �    .   �  '   �      �  4     /   I  5   y     �  6   �  +   �                               
          	    Dump all parameters for all objects Enumerate objects paths for devices Exit after a small delay Exit after the engine has loaded Get the wakeup data Monitor activity from the power daemon Monitor with detail Show extra debugging information Show information about object path Project-Id-Version: upower
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-06-15 11:24+0000
PO-Revision-Date: 2012-03-08 12:02+0000
Last-Translator: Dennis Baudys <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:49+0000
X-Generator: Launchpad (build 18115)
 Alle Informationen über alle Objekte anzeigen Objektpfade für die Geräte aufzählen Nach kurzer Verzögerung beenden Abbrechen, nachdem die Programmroutine geladen wurde Weckrufe von Geräten (remote wakeups) anzeigen Aktivität des Energieverwaltungsdienstes überwachen Detailliert überwachen Zusätzliche Informationen zur Fehlerdiagnose anzeigen Informationen über den Objektpfad anzeigen 