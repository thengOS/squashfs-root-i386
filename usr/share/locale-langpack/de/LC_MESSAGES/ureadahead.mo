��          �   %   �      P     Q     k     �     �      �     �     �          $     ?     Z     y     �  1  �     �     �     �  "     4   :     o      v     �  6   �  (   �  1     �  D      �  +     &   0  %   W  ,   }  %   �  
   �  #   �  &   �  &   &	  "   M	  #   p	     �	  �  �	     9     J  "   V  "   y  I   �     �  '   �        6   6  9   m  <   �                                             	                       
                                                         %s: illegal argument: %s
 Error mapping into memory Error retrieving chunk extents Error retrieving file stat Error retrieving page cache info Error unmapping from memory Error while reading Error while tracing Failed to set CPU priority Failed to set I/O priority File vanished or error reading Ignored far too long path Ignored relative path PATH should be the location of a mounted filesystem for which files should be read.  If not given, the root filesystem is assumed.

If PATH is not given, and no readahead information exists for the root filesystem (or it is old), tracing is performed instead to generate the information for the next boot. Pack data error Pack too old Read required files in advance Unable to determine pack file name Unable to obtain rotationalness for device %u:%u: %s [PATH] detach and run in the background dump the current pack file how to sort the pack file when dumping [default: path] ignore existing pack and force retracing maximum time to trace [default: until terminated] Project-Id-Version: ureadahead
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-11-29 15:05+0000
PO-Revision-Date: 2011-04-15 11:55+0000
Last-Translator: Hendrik Knackstedt <Unknown>
Language-Team: German <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:47+0000
X-Generator: Launchpad (build 18115)
 %s: unzulässiges  Argument: %s
 Fehler bei der Übertragung in den Speicher Fehler beim Abfragen des chunk-Umfangs Fehler beim Empfangen des Dateistatus Fehler bei der Abfrage der Cache-Seiten-Info Fehler beim Entladen aus dem Speicher Lesefehler Fehler während der Rückverfolgung Ändern der CPU-Priorität gescheitert Ändern der I/O-Priorität gescheitert Datei verschwunden oder Lesefehler Viel zu langer Pfad wurde ignoriert Relativer Pfad wurde ignoriert PFAD sollte der Ort eines eingehängten Dateisystems sein, für das Dateien gelesen werden sollen. Das Basis-Dateisystem wird verwendet, wenn nichts angegeben ist.
Wenn PFAD nicht gegeben ist und keine readahead-Information für das Basis-Dateisystem existiert (oder diese alt ist), wird stattdessen Rückverfolgung verwendet, um Informationen für den nächsten Startvorgang zu erstellen. Pack-Datenfehler Pack zu alt Benötigte Dateien im Voraus lesen Konnte Paket-Dateiname nicht lesen Es ist nicht möglich, die Informationen des Gerätes %u:%u: %s abzurufen [PFAD] Abtrennen und im Hintergrund ausführen Die aktuelle Pack-Datei ausgeben Sortierung der ausgegebenen Pack-Datei [Vorgabe: Pfad] vorhandenes Pack ignorieren und Rückverfolgung erzwingen maximale Zeit zur Rückverfolgung [Vorgabe: bis abgebrochen] 