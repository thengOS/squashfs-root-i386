��    S     �"  �  LE      p\  	   q\  #   {\     �\  �   �\     <]  $   W]     |]  (   �]     �]     �]     �]     �]     ^     0^     8^     O^     a^  �   z^  
   �^  �  _     �`     �`     a      a  ^   >a     �a  B   �a  I   �a  '   Ab  %   ib  X   �b  L   �b  2   5c  >   hc  ?   �c     �c  H   �c  :   <d  )   wd     �d     �d  1   �d  9   e  9   Ae  &   {e  >   �e  *   �e  =   f  $   Jf     of  g   �f  %   �f  6   g  1   Lg  !   ~g     �g     �g  #   �g  9   �g  -   ,h  -   Zh  -   �h  '   �h  "   �h  L   i  >   Ni  ?   �i  ;   �i  G   	j  B   Qj  -   �j  9   �j  :   �j  8   7k  G   pk  -   �k  '   �k  x   l  x   �l  :    m  #   ;m  D   _m  @   �m  9   �m  C   n  @   cn     �n  4   �n  4   �n  =   -o  :   ko  6   �o  ;   �o  %   p  5   ?p  =   up  .   �p  _   �p  %   Bq  l   hq  .   �q  T   r  1   Yr  o   �r  '   �r  M   #s  D   qs  Q   �s  *   t     3t  4   Qt  :   �t  <   �t     �t     u     u     u  
   "u     -u     :u     Cu     \u     uu     �u     �u     �u  )   �u     �u     �u     �u     v     "v     8v     Ov  
   \v  
   gv  	   rv  &   |v     �v     �v     �v     �v     �v  /   �v      #w     Dw     \w     ow     �w     �w  4   �w     �w     �w     x  9   x     Nx     Vx     tx  #   �x  %   �x     �x     �x  D   y     Uy     sy     �y     �y     �y  &   �y     �y  -   z     Gz     [z  .   mz  >   �z     �z  "   �z     {     &{     A{  $   ]{     �{     �{     �{     �{     �{  (   |      0|  %   Q|     w|     �|     �|     �|     �|     �|     }     -}     J}     j}     �}     �}     �}  
   �}     �}     �}  	   �}     �}     �}     �}     �}  $   ~  6   @~  !   w~  1   �~  !   �~  1   �~  !     9   A  %   {  6   �  '   �  %    �     &�  W   ;�     ��     ��     ��     ̀     �     ��     �     �     -�  *   H�     s�  &   ��  l  ��  Y   '�  3   ��     ��     ��     Ɔ     Ն     �  
   ��  <   �  ,   @�     m�     y�     }�     ��     ��     ��  	   ��     ��  +   ��  -   �     �     )�     7�     <�     O�     V�     _�     d�     v�     �     ��     ��     ��     ��     Ԉ  '   �  o   �  8   ��  (   ĉ  $   �     �  %   *�  .   P�  -   �     ��     Ɋ  "   ϊ     �     �     �     1�     E�     M�     l�  O   ��     ܋  E   ��     =�     Y�  $   l�     ��  	   ��     ��  	   ͌  !   ׌  +   ��     %�  
   -�     8�     F�     Y�     r�  
   ��     ��     ��     ��     ��     ͍  5   ԍ  !   
�     ,�  &   8�     _�  #   z�     ��      ��     ӎ     ܎     �  .   �  ,   4�     a�     ��  .   ��     ��  5   ��     �     �     �     �     �     .�     G�     b�     ��     ��  '   ��     ̐     Ґ  
   ؐ     �     ��     �  !   �     @�     Z�     y�  '   ��     ��  +   ב     �     �     8�     I�  +   [�     ��     ��  4   ��     ݒ      ��  "   �     >�     G�     T�     m�     ��      ��     ��     ��     ��     ғ     ֓     �     
�     �  6   �  
   R�  
   ]�     h�     ��     ��  %   ��     ��     Ĕ     є     �     ��     �     �  
   3�  F   >�     ��  1   ��     ʕ  
   �     �  /   ��  1   -�     _�  .   {�  ,   ��     ז     ��     �     ,�     A�  @   S�     ��  '   ��  ?   җ     �     $�  D   >�  )   ��     ��     ��     ǘ     �  	   �     ��  0   �  5   E�     {�  2   ��     ��     Ι  	   ԙ  
   ޙ     �     ��     �     �  
   +�     6�     K�     ^�     v�     ��     ��     ��     ܚ     �     �  E   �  *   V�  "   ��     ��     ��     ՛     �     �     �     ��     �     �     7�     E�     J�  A   S�     ��     ��     ��     ��     ֜  	   ߜ     �  %   ��     �     -�  !   I�     k�  |   w�  T   ��     I�     ^�  7   f�     ��  3   ��  <   �  C   �     b�     u�     ��     ��     ��     ʟ     �     ��     ��     �     �  	   $�  
   .�     9�     J�  
   [�     f�     ��     ��     ��  !   ��  9   à     ��     �     -�     =�     N�  .   _�  +   ��     ��     ʡ  
   �     ��     ��     �     �     5�     A�  #   S�  7   w�     ��     ��     Ƣ     ֢  ,   ۢ  
   �  9   �  	   M�     W�      e�     ��     ��  #   ��  1   ԣ     �     �  .   �     I�     ^�  "   q�     ��     ��     ��     ��     ��     Ť     Τ  	   ֤     �     �     ��  
   �  
   �     �  
   �     *�     2�     :�     F�     M�     f�     {�     ��     ��  5   ϥ     �     �  :   �     V�  E   l�     ��     Ħ     ڦ     �     �  B   �     I�     _�  +   o�     ��     ��      ��     ȧ     Ч     ݧ  	   �     �  	   �     ��      �     �  
    �     +�  
   :�     E�  
   Q�  	   \�  	   f�     p�     ��     ��     ��  >   ��  7   ��  A   -�  �   o�  _   �  ?   p�     ��  %   ͪ  =   �  '   1�  9   Y�     ��  <   ��  -   �     �  6   �  '   N�     v�  )   z�  '   ��  $   ̬  (   �  &   �     A�     Z�     p�  =   ��     ƭ  -   �  '   �     9�     S�     j�  
   ��  !   ��     ��     ��     ̮     �     �  2   ��  2   *�  ;   ]�  )   ��  >   ï     �     �     .�     A�     M�     \�  
   p�     {�     ��     ��     ��     ��     ˰     ۰  	   �     ��  p   �     v�  +   ��  2   ��  %   �     �  (   5�     ^�  
   u�  *   ��  %   ��     Ѳ  $   ײ  7   ��  
   4�  	   ?�  ,   I�  /   v�     ��     ��  L   ʳ     �  )   5�  !   _�  !   ��     ��     ´     ϴ     �  '   �     *�     ?�  2   S�     ��     ��     ��     ˵     ߵ     �     ��  ,   	�     6�     J�     [�     j�     ��     ��  &   ��  &   ۶     �     �     &�  '   8�     `�  2   d�  &   ��     ��     ҷ     �     �     ��     �  "   �     <�     Q�     l�     }�     ��     ��     ø  %   ٸ     ��     �     !�     -�     <�     H�  %   c�     ��      ��     ��     ��     Ź     ݹ     �     	�  $   "�     G�  +   b�     ��  #   ��     ɺ     �     ��     �     0�     K�     a�     i�     y�     ��     ��     ��  '   ��     �     ��      �  %   :�     `�     e�  	   ~�     ��  &   ��  &   ��  -   �  &   �  $   ;�     `�     |�     ��     ��     ��  	   ƽ     н     ݽ  2   �     %�     =�     P�     U�     h�  	   q�  5   {�     ��     ��  &   ľ     �     ��  -   �     =�  ,   Y�     ��     ��     ��     Ŀ     ؿ     �  (   
�     3�     G�     a�     n�  "   ��     ��     ��     ��  #   ��     �     /�      O�     p�  !   ��     ��     ��     ��     ��     �     '�  !   E�     g�     ��  "   ��     ��     ��  #   ��  ,   �  ,   I�     v�     ��     ��     ��     ��     ��     ��  
   �     �     1�     6�  1   B�     t�     ��  !   ��     ��     ��     ��     �     ,�      A�     b�     p�     ~�     ��  
   ��  (   ��  (   ��     ��     �  5   +�     a�  
   s�     ~�  #   ��  *   ��     ��     ��     �     $�     ;�     V�     j�     ��     ��     ��  <   ��     �     /�  
   F�     Q�     ]�     u�     ��     ��     ��  !   ��     ��     �     3�     J�  1   \�     ��  2   ��  >   ��  "   �  !   >�     `�     r�  )   ��  %   ��  (   ��     ��     �     �     �  '   (�     P�     n�     ��  (   ��     ��     ��     ��     ��     ��     ��     ��     ��     �     4�      S�     t�     }�     ��     ��     ��     ��     ��  T  �  %   W�     }�  1   ��     ��     ��     ��     ��  %   ��     ��  
   ��     �     %�     .�     D�     `�     e�  C   ��  +   ��     ��     �     �     '�      0�     Q�     c�     i�     n�     ~�     ��     ��     ��     ��  #   ��     ��  3   	�  6   =�     t�     ��     ��  
   ��     ��     ��     ��     ��     ��      �     3�  (   J�     s�     ��     ��     ��     ��     ��  "   ��     ��     �  
   �     �     -�     9�     Q�     m�      ��     ��     ��     ��  ,   ��     �     "�     )�     .�     B�     [�     i�     x�     ��     ��     ��     ��     ��     ��     ��  $   �     -�     B�  	   I�     S�     n�     ��     ��     ��  8   ��  '   �     ;�     R�  !   p�  $   ��     ��  $   ��     ��  "   �     3�     M�     `�     q�     ��     ��     ��  	   ��     ��     ��  %   ��  "   �  $   (�      M�     n�     ��     ��     ��  %   ��     ��     �     5�     =�  *   ]�     ��     ��     ��     ��  �  ��     ��     ��  
   ��  :   ��  <   �  ;   Y�     ��     ��     ��     ��     ��     ��  6   
�  )   A�  *   k�  F   ��  F   ��  :   $�     _�     e�     r�     ��     ��     ��     ��     ��     ��     ��  �  ��  	   ��  #   ��     	�  �   "�     ��  (   ��     �  (   1�     Z�     k�     ��  "   ��     ��     ��     ��     �  #   �  �   <�     ��  *  ��     $�     B�     V�  '   p�  `   ��     ��  b   �  T   x�  )   ��  $   ��  ]   �  N   z�  3   ��  S   ��  E   Q�     ��  Z   ��  M   ��  ,   M�  !   z�     ��  @   ��  J   ��  H   >�     ��  K   ��  -   ��  A   �  0   ]�     ��  ~   ��  &   (�  7   O�  2   ��  #   ��     ��     ��  $   �  =   4�  1   r�  1   ��  1   ��  +   �  %   4�  f   Z�  L   ��  ?   �  <   N�  T   ��  K   ��  ?   ,�  N   l�  >   ��  F   ��  J   A�  2   ��  -   ��  �   ��  �   y�  B   �  )   E�  S   o�  G   ��  H   �  R   T�  B   ��  -   ��  3   �  :   L�  @   ��  G   ��  F   �  A   W�  -   ��  8   ��  E    �  2   F�  b   y�  (   ��  �   �  5   ��  `   ��  9   2�  �   l�  %   ��  \   �  S   r�  `   ��  )   '�     Q�  :   q�  B   ��  :   ��  
   *�     5�     D�     M�     R�     ^�     n�     w�     ��     ��     ��     ��     ��  /   ��     �     !�     :�     I�     e�     {�     ��  
   ��  
   ��  	   ��  #   ��     ��  
   ��  (   
     3     G  >   ]  $   �     �     �     �         / <   N    �    �    � ?   �         '   2 6   Z <   � )   � %   � r    7   � 3   �    � 9       M <   l %   � >   � %       4 6   L [   �    � '   �          6 $   W 7   | +   �    � 2   � +   # +   O 9   { 2   � =   �    &    A     [ 5   | 1   � +   � .   	 0   ?	 ,   p	 )   �	    �	    �	    �	 	   �	    
    
 	   
     
    .
    7
 *   R
 0   }
 L   �
 !   �
 4    "   R 3   u !   � B   � .    E   = /   � -   �    � R   �    K    T    o    �    �    �    �    � $    =   ) +   g (   � �  � y   z 8   �    -    1    @    O    n 
   u Q   � )   �    �     *       2    6    = 	   N    X 4   i /   �    �    �    �    �     
           $    6    ?    Q     Z    {    � &   � 7   � }    ?   � 3   � *       - 2   E -   x 7   � "   �     A       Q    g        �    � 0   � ,   � \    %   l Q   � )   �     (   (    Q    p !   y 	   � .   � 4   �    	            +    >    W 
   j    u    �    �    �    � 8   � $   �    # =   /    m &   �    � 4   � 
   �         9   7 =   q     �    � 7   �     D       a    t    }    �    � )   �    �    � 
          0   .     _     e  
   k     v     �     �  C   �  &   �  &   $! "   K! A   n! 2   �! =   �! 7   !" %   Y"    "    �" E   �"    �"    
# G   #    _# $   ~# ,   �# 	   �#    �#    �# )   $    +$ 5   8$    n$    �$    �$    �$ "   �$ $   �$ 	   �$ 	   �$ 7   % 
   :% 
   E%    P%    o%    v% 7   |%    �%    �%    �%    �%    �%    &    &    0& L   <&    �& 4   �&    �& 
   �&    �& I   ' 3   V' $   �' A   �' =   �'    /(    N(    n(    �(    �( \   �(    ) 2   &) >   Y)    �) (   �) `   �) /   =*    m*    u*    �*    �* 	   �* !   �* ;   �* C   ,+    p+ =   �+    �+    �+ 	   �+ 
   �+    �+    ,    ,    , 
   5,    @,    U, &   h, /   �,    �,    �, !   �,    -    3-    S- S   i- -   �- $   �- %   .    6.    V.    j.    x.    �.    �.    �. &   �.    �.    �.    �. Y   �.    J/    Q/     U/    v/ 
   �/ 
   �/    �/ 5   �/    �/    0 ,   0    G0 �   ^0 h   1    z1    �1 <   �1    �1 8   �1 W   +2 =   �2    �2    �2     �2    3    3    .3    M3    a3    f3    w3    }3    �3 
   �3    �3    �3 
   �3 7   �3    4    4    4 2   "4 C   U4    �4    �4    �4    �4    �4 Z   �4 9   Z5    �5     �5 
   �5    �5    �5    �5 (   6    ;6    G6    Z6 K   w6    �6    �6    �6    �6 =   �6    +7 ;   K7 
   �7    �7 1   �7    �7    �7 7   8 A   ?8    �8    �8 8   �8 !   �8    �8 &   9    /9    M9    Q9    U9    ]9    e9    n9 	   v9    �9    �9    �9 
   �9    �9    �9 
   �9    �9    �9    �9    �9 '   �9    #: #   ;: )   _: )   �: 9   �:    �:    �: R   ;    W; o   q;    �;    �;    < 	   &<    0< F   L< )   �<    �< 7   �<    =    = $   =    <=    D= 	   Q= 	   [=    e= 	   k=    u=    y=    �= 
   �=    �= 
   �=    �= 
   �= 	   �= 	   �=    �=    >    >    #> >   @> J   > J   �> �   ? x   �? Q   0@ $   �@ &   �@ 9   �@ ,   A E   5A    {A ;   �A @   �A    B S   B +   dB    �B :   �B 8   �B 5   C 9   >C 7   xC '   �C '   �C &    D S   'D ,   {D 7   �D K   �D -   ,E +   ZE ,   �E    �E &   �E 	   �E    �E    F    (F    <F 9   HF 8   �F E   �F /   G N   1G    �G    �G    �G    �G    �G 0   �G    H    .H    :H    TH    iH    zH    �H    �H 	   �H    �H u   �H    ?I 1   ^I C   �I %   �I '   �I 6   "J    YJ    sJ 6   �J ,   �J    �J ;   �J <   /K 
   lK 	   wK /   �K 3   �K    �K    �K c   L +   tL 5   �L *   �L (   M %   *M    PM    aM     xM .   �M #   �M !   �M 3   N     BN    cN    �N    �N    �N 	   �N    �N @   �N *   !O    LO    fO &   O    �O    �O 9   �O 9   P    OP    kP    �P N   �P    �P @   �P 0   :Q    kQ    �Q    �Q    �Q (   �Q    �Q :   �Q &   #R ,   JR 5   wR 7   �R +   �R 0   S 7   BS 8   zS    �S "   �S    �S    
T    'T ,   @T 9   mT '   �T 0   �T     U 	   U    U    *U +   CU    oU (   �U    �U :   �U    V &   )V "   PV    sV    �V &   �V    �V    �V 	   W    W    'W    @W    [W 	   uW 5   W 2   �W .   �W -   X 5   EX    {X "   �X 
   �X    �X )   �X 1   �X 0   #Y )   TY *   ~Y 8   �Y &   �Y :   	Z    DZ    VZ    [Z    dZ    wZ 7   �Z    �Z    �Z 	   �Z    [    [    "[ L   .[    {[    �[ 9   �[    �[    �[ &   \ &   )\ 2   P\    �\ *   �\ )   �\    �\ $   ] %   ;] 5   a]    �] "   �]    �] .   �] )   ^    I^ %   X^ +   ~^ 6   �^ +   �^ -   _ 3   ;_ "   o_ 3   �_ 0   �_ "   �_ )   ` (   D` )   m` ,   �` /   �` *   �` 2   a :   Ra 1   �a "   �a 2   �a <   b E   Rb    �b #   �b &   �b     �b    c    *c )   Bc 	   lc    vc    �c    �c N   �c &   �c    d 7   6d $   nd ,   �d 5   �d    �d    e -   4e    be    }e    �e    �e    �e 9   �e 9   �e -   /f /   ]f L   �f    �f 
   �f    g ,   g 1   ?g    qg "   �g    �g    �g    �g    	h '   !h #   Ih    mh    �h L   �h "   �h !   i    ;i    Ni    ei "   �i    �i $   �i    �i    �i /   j #   Ej    ij    �j M   �j    �j B   
k C   Mk +   �k !   �k    �k    �k =   l *   Ql J   |l 
   �l    �l    �l    �l ;   m 0   Am #   rm    �m 6   �m    �m    �m    �m    �m    �m    �m !   n /   'n )   Wn "   �n %   �n    �n &   �n    �n    o    3o    No    jo �  �o $   q    Cq D   Iq    �q    �q    �q    �q 8   �q    �q    �q    �q    r    'r #   ?r    cr ,   hr a   �r B   �r 2   :s    ms    ss 	   �s %   �s    �s    �s    �s    �s    �s    t "   $t    Gt    Nt    Rt    rt 2   �t 4   �t    �t )   u    8u    Qu    _u &   pu    �u !   �u    �u "   �u    �u 2   v    Fv "   ]v #   �v    �v    �v    �v $   �v    �v    �v 
   w    w    w !   7w $   Yw &   ~w ,   �w    �w     �w    x ;   -x    ix    ox    vx    ~x =   �x    �x    �x ,   y    5y    Sy    ky .   qy /   �y    �y $   �y 4   �y *   1z    \z    cz '   oz .   �z 7   �z 6   �z 4   5{ G   j{ *   �{ &   �{ "   | )   '| 4   Q| %   �| )   �| $   �| *   �| &   &}    M}    `} #   q}    �}    �}    �}    �} *   �}    ~ 5   ~ 6   ;~ 0   r~ &   �~ .   �~ &   �~ %     3   F 3   z *   �     � 	   � (   � 1   -�    _�    v� )   �� -   �� k  �    Y�    o�    �� =   �� ?   ф >   �    P�    m� "   r�    �� (   ��    ۅ A   �� 4   8� 4   m� S   �� S   �� >   J� 	   ��    ��    ��     ɇ ,   � 0   �    H�    K�    N�    U�      i       e       h  A   �  i  �  �       �      �  �  [  A  j  .      $       �  �      '   V  ?           �       �    !  ,   �            �  >  �      �  �  �   $  �     W      �  I  �      =        �       %    M  d   k  �  #  f    �   �       2  k        F  ;  �  �      8  �  �  �  �  ]                 @  �                 �   u    .  �      �   /  �  6  �        �      �      �  q          �   �       �  �  �   �  m   M         �  �      I  ^  )  �      �  v   �   \          �  �  U  F      �  .      �     M    �  �   4  �   �  r  �   �  �          `     �  �  T                �      �      (         D  S   �        9  �  �              �   �  0  J        �           �   �  D  z  7  �  l            �      k                   �  �      �              �  y  G  �  &  f  �   1      T   �   7  �  9          w       �    j   n    z  �          �       �  y   �   :  J  .   �  �     �       c           �  �  >  �  F            �   �  �  {  v  �   �      �   �      Q  �       L      �  �    �  /  �   �      h   �   L  X  3            �        &          s  �  �  {   �   �  �      @        )  &   =   �  �  �  �         �    V   �   d          v          #      4  M   �    �  �  �  �      �  	  �  k  �  9     6      u  X    �        �  |  h  �      4  �      <         �  w  
  �      o  �   i  t  �      �  )   x  �  �  �   �  ;      �  ?      6  A  V  �   �         �     '  �  #       �  a  �              �        :   �      <      �             ~      �   n  W      �   2       O  �      �  \  0  �   �      
  t  ?  �   P  !       �          �    9  �             �      (  c    r  �     J    Y  �        s  _     �  �  �   /  a   o          �      �  ;   o  �   �     5  �  C   �         �  D  Q  �       J   �  8      �      �  �  	               j  �  �   H      P     �        �  :      (            �   /  q  �          E   �  �    �  �  �      O  �   R  s  �                        b  �  U  �  G           N  �  U   �          �      �   �   t   =          K  `        �  �    �        :  Y  �   �  �      }  �        �    �  c  �  �  �       �  	   <  e  $  5  K       �  �  �              �  4   +         Z            [  +  �  �   T      �  �     %    �  �  �  �   g    �       �   �          8      R  �       �    �  B      p       �  �  �  y  %      �  *                  �  0  �  ]  `        H  [        �  z      �      l            �  �  �  %      Q  R  L  N      �   �  �  �  �  �  �  �          h      �   B    6   �  B             �  m  �  �      �  �  �                         �       0  A  "  b  "  '      �      �  �  g         �  �           �               '          �      �      �  �      �  <   Z  �  �  ~  �   �      M      �      �  ,          T  $  �  �      �  �      
   ^   �  !  �  �  7  l      �      �      �  3   �  �                R  �  4      �  �  �  \  �  �   o   I         "  �   �  p  =      �  "  H   �   �   �    �  p  C  2  �          �  �  �  �   j  (  �  �          �  �      <  7  =      �      �   �  U  �          �  �          8   �  a  y  w  �        X    �      -  �  �  �  �  b          z     �        Q       �      �  1      �  �   ]  {      3     �      r    �  �  n   l     A  *  �   v  f        �  �  7   �   5      �  �      �  �  *       �               2  �  �  �      ;      G          W     �   �  a      �  9          �  �           �       +      �  �   F   m      �  >       �   _        �  	  @      !  �  x  S  �   5   �  �       d      �  �       �  )  �       |  I   �  Y       �  -  �          �  e  �  �  �        q   �              �      �   �  �  �   �      .  �      �            @  G  �   �  �  �          �  �  �   �  x      �  1   �  5  c  �  w  ]  �  X   D       '  �  �   �  S            ,  �                  �   �       _   2          3              ,  +  C      �  ;  :  �   �    E          �  L   N   n  6  i  \       -  �   �   �  �            E  B      (     �  �   �  3  �       L  q  }  �      �       �      ^      �          �         P  �  P  �  
  �   �  |   -   r   *  �    C      �   �               J  �     �  �  *  K  �  �  �       �   �   Q  g            Z   "   �  �  1        �   R             %   �           �  O                      p    �  -        �    �  �   `  �  �   �       ~      W  G      >  0                   �   �  �  �  ~          �   �  �  �  &  1    �  t  �         �  ?  �   �  |      H          �          O          m          Z  �  �  �     �        u   &      S  N      �      N  �  �  �                     Y  #              g  �  �  �  )  F  �  �      �  u  S        �  #  �   �       �  �  �      D  �  �     K  �                  s   �  �   �  �          x   8      @  O   C  ,  >  }           �  �   �  _      	      �  �  �      �        B                           �  �  ?  �  }  �  
      �  �   d      �   �   V  �  b   e  f           [      �     +  /   {  �  �  I      !      �  $  �  P   �  E  �      �   �  �  E  �   �  �  �      K      �    �   H  ^  �      	UTC: %s
 	tv.tv_sec = %ld, tv.tv_usec = %ld
 	tz.tz_minuteswest = %d
 
%6d regular files
%6d directories
%6d character device files
%6d block device files
%6d links
%6d symbolic links
------
%6d files
 
%6ld inodes used (%ld%%)
 
%s: offset = %ju, size = %zu bytes. 
*** %s: directory ***

 
******** %s: Not a text file ********

 
***Back***

 
...Skipping  
Available columns (for -o):
 
Do you really want to quit?  
Help (expert commands):
 
Help:
 
Known <ldisc> names:
 
Login incorrect
 
Message Queue msqid=%d
 
Most commands optionally preceded by integer argument k.  Defaults in brackets.
Star (*) indicates argument becomes new default.
 
Options:
 
Options:
 -N, --inodes=NUM    specify desired number of inodes
 -V, --vname=NAME    specify volume name
 -F, --fname=NAME    specify file system name
 -v, --verbose       explain what is being done
 -c                  this option is silently ignored
 -l                  this option is silently ignored
 -V, --version       output version information and exit
                     -V as version must be only option
 -h, --help          display this help and exit

 
Pattern not found
 
Script done on %s 
Semaphore Array semid=%d
 
Shared memory Segment shmid=%d
 
Usage:
 %1$s -V
 %1$s --report [devices]
 %1$s [-v|-q] commands devices

Available commands:
 
Usage:
 %s [options]
                confirm or deny the write by entering 'yes' or 'no'                since this might destroy data on the disk, you must either         (%s partition table detected).          (compiled without libblkid).       --bytes                   print SIZE in bytes rather than in human readable format
      --extract[=<dir>]    test uncompression, optionally extract into <dir>
      <device>       path to the device to be used
      <size>         number of blocks to be used on the device
      fs-options     parameters for the real filesystem builder
   Overflow
   W          Write partition table to disk (you must enter uppercase W);   b          Toggle bootable flag of the current partition   d          Delete the current partition   h          Print this screen   hole at %lu (%zu)
   n          Create new partition from free space   q          Quit program without writing partition table   s          Fix partitions order (only when in disarray)   t          Change the partition type   u          Dump disk layout to sfdisk compatible script file   uncompressing block at %lu to %lu (%lu)
   x          Display/hide extra information about a partition  %-25s get size in 512-byte sectors
  %1$s [options] <disk>
  %1$s [options] <disk>      change partition table
 %1$s [options] -l [<disk>] list partition table(s)
  %s <disk device> <partition number>
  %s <disk device> <partition number> <start> <length>
  %s [options] -- [fs-options] [<filesystem> ...]
  %s [options] /dev/name [blocks]
  %s [options] <device>
  %s [options] <file>
  %s [options] <iso9660_image_file>
  %s [options] [-t <type>] [fs-options] <device> [<size>]
  -1                      use Minix version 1
  -2, -v                  use Minix version 2
  -3                      use Minix version 3
  -?         display this help and exit
  -A         check all filesystems
  -B, --protect-boot            don't erase bootbits when create a new label
  -C [<fd>]  display progress bar; file descriptor is for GUIs
  -C, --cylinders <number>      specify the number of cylinders
  -H, --heads <number>          specify the number of heads
  -L, --color[=<when>]          colorize output (auto, always or never)
  -L, --color[=<when>]     colorize output (auto, always or never)
  -M         do not check mounted filesystems
  -N         do not execute, just show what would be done
  -P         check filesystems in parallel, including root
  -R         skip root filesystem; useful only with '-A'
  -S, --sectors <number>        specify the number of sectors per track
  -T         do not show the title on startup
  -V         explain what is being done
  -V, --verbose      explain what is being done;
                      specifying -V more than once will cause a dry-run
  -V, --version      display version information and exit;
                      -V as --version must be the only option
  -a                       for compatibility only, ignored
  -a, --auto       automatic repair
  -b, --blocksize <size>   use this blocksize, defaults to page size
  -b, --sector-size <size>      physical and logical sector size
  -c, --check             check the device for bad blocks
  -c, --compatibility[=<mode>]  mode is 'dos' or 'nondos' (default)
  -d, --divisor=<number>  divide the amount of bytes by <number>
  -f, --force      force check
  -f, --from <N>    start at the track N (default 0)
  -h, --help         display this help text and exit
  -i, --inodes <num>      number of inodes for the filesystem
  -l         lock the device to guarantee exclusive access
  -l, --badblocks <file>  list of bad blocks from file
  -l, --list                    display partitions end exit
  -l, --list       list all filenames
  -n, --namelength <num>  maximum length of filenames
  -n, --no-verify   disable the verification after the format
  -o, --output <list>           output columns
  -r [<fd>]  report statistics for each device checked;
            file descriptor is for GUIs
  -r, --repair     interactive repair
  -r, --repair <N>  try to repair tracks failed during
                     the verification (max N retries)
  -s         serialize the checking operations
  -s, --getsz                   display device size in 512-byte sectors [DEPRECATED]
  -s, --super      output super-block information
  -t <type>  specify filesystem types to be checked;
            <type> is allowed to be a comma-separated list
  -t, --to <N>      stop at the track N
  -t, --type <type>             recognize specified partition table type only
  -t, --type=<type>  filesystem type; when unspecified, ext2 is used
  -u, --units[=<unit>]          display units: 'cylinders' or 'sectors' (default)
  -v, --verbose            be more verbose
  -v, --verbose    be verbose
  -x, --sectors           show sector count and size
  -y                       for compatibility only, ignored
  -z, --zero               start with zeroed partition table
  Remove  [Y]es/[N]o:   badsect  ecc  removable "%s" line %d %15s: %s %6.2f%% (%+ld bytes)	%s
 %6ld zones used (%ld%%)
 %c: unknown command %ld blocks
 %ld inodes
 %s %d %s %s: status is %x, should never happen. %s (%c-%c):  %s (%c-%c, default %c):  %s (%ju-%ju):  %s (%ju-%ju, default %ju):  %s (%s, default %c):  %s (%s, default %ju):  %s (mounted) %s (n/y)?  %s (y/n)?  %s cache: %s does not have interrupt functions.  %s failed.
 %s from %s
 %s is clean, no check.
 %s is mounted
 %s is mounted.	  %s is mounted; will not make a filesystem here! %s is not a block special device %s is not a serial line %s is not mounted
 %s requires an argument %s status is %d %s succeeded.
 %s takes no non-option arguments.  You supplied %d.
 %s unknown column: %s %s using IRQ %d
 %s using polling
 %s-sided, %d tracks, %d sec/track. Total capacity %d kB.
 %s: OK
 %s: Unrecognized architecture %s: assuming RTC uses UTC ...
 %s: bad directory: '.' isn't first
 %s: bad directory: '..' isn't second
 %s: bad directory: size < 32 %s: can't exec %s: %m %s: cannot add inotify watch (limit of inotify watches was reached). %s: cannot add inotify watch. %s: cannot read inotify events %s: dup problem: %m %s: exceeded limit of symlinks %s: execute failed %s: failed to initialize sysfs handler %s: failed to parse fstab %s: failed to read partition start from sysfs %s: get size failed %s: input overrun %s: insecure permissions %04o, %04o suggested. %s: last_page 0x%08llx is larger than actual size of swapspace %s: lseek failed %s: might not be an ISO filesystem %s: not a block device %s: not enough good blocks %s: not open for read/write %s: parse error at line %d -- ignore %s: read swap header failed %s: read: %m %s: reinitializing the swap. %s: seek failed in write_block %s: seek failed in write_tables %s: skipping - it appears to have holes. %s: skipping nonexistent device
 %s: skipping unknown filesystem type
 %s: swapoff failed %s: swapon failed %s: too many bad blocks %s: unable to clear boot sector %s: unable to write inode map %s: unable to write inodes %s: unable to write super-block %s: unable to write zone map %s: write failed in write_block %s: write signature failed (EOF) (Next file:  (Next file: %s) (waiting)  , busy , error , on-line , out of paper , ready -- line already flushed ------ Message Queues --------
 ------ Message Queues PIDs --------
 ------ Message Queues Send/Recv/Change Times --------
 ------ Semaphore Arrays --------
 ------ Semaphore Arrays Creators/Owners --------
 ------ Semaphore Limits --------
 ------ Semaphore Operation/Change Times --------
 ------ Semaphore Status --------
 ------ Shared Memory Attach/Detach/Change Times --------
 ------ Shared Memory Limits --------
 ------ Shared Memory Segment Creators/Owners --------
 ------ Shared Memory Segments --------
 ------ Shared Memory Status --------
 -------      ------- ----------------------------
FILE SYSTEM HAS BEEN CHANGED
----------------------------
 --More-- --waiting-- (pass %d)
 ...Skipping back to file  ...Skipping to file  ...got clock tick
 ...skipping
 ...skipping backward
 ...skipping forward
 ...synchronization failed
 /dev/%s: cannot open as standard input: %m /dev/%s: not a character device : !command not allowed in rflag mode.
 <space>                 Display next k lines of text [current screen size]
z                       Display next k lines of text [current screen size]*
<return>                Display next k lines of text [1]*
d or ctrl-D             Scroll k lines [current scroll size, initially 11]*
q or Q or <interrupt>   Exit from more
s                       Skip forward k lines of text [1]
f                       Skip forward k screenfuls of text [1]
b or ctrl-B             Skip backwards k screenfuls of text [1]
'                       Go to place where previous search started
=                       Display current line number
/<regular expression>   Search for kth occurrence of regular expression [1]
n                       Search for kth occurrence of last r.e [1]
!<cmd> or :!<cmd>       Execute <cmd> in a subshell
v                       Start up /usr/bin/vi at current line
ctrl-L                  Redraw screen
:n                      Go to kth next file [1]
:p                      Go to kth previous file [1]
:f                      Display current file name and line number
.                       Repeat previous command
 A hybrid GPT was detected. You have to sync the hybrid MBR manually (expert command 'M'). AIEEE: block "compressed" to > 2*blocklength (%ld)
 AIX AIX bootable AST SmartSleep Alignment offset: %lu bytes Amoeba Amoeba BBT Are you sure you want to write the partition table to disk?  Assuming hardware clock is kept in %s time.
 Attributes: BBT BLKGETSIZE ioctl failed on %s BSD BSD/OS BSDI fs BSDI swap BeOS fs Block %d in file `%s' is marked not in use. Block has been used before. Now in file `%s'. BlockSize: %d
 Blocks: %llu
 Boot Boot Wizard hidden BootIt Bootable CP/M CP/M / CTOS / ... CPU MHz: CPU family: CRC: %x
 Calling settimeofday:
 Cannot open %s Change the partition type Changed type of partition %zu. Changed type of partition '%s' to '%s'. Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.
 Changing display/entry units to cylinders (DEPRECATED!). Changing display/entry units to sectors. Changing finger information for %s.
 Changing shell for %s.
 Check and repair a Linux filesystem.
 Check and repair a compressed ROM filesystem.
 Check the consistency of a Minix filesystem.
 Checking all file systems.
 Clear Clock not changed - testing only.
 Command      Meaning Command (m for help):  Compaq diagnostics Core(s) per socket: Correct Could not delete partition %zu Could not delete partition %zu. Could not open file with the clock adjustment parameters in it (%s) for writing Could not toggle the flag. Could not update file with the clock adjustment parameters (%s) in it Couldn't initialize PAM: %s Create a new label Create new partition from free space Current system time: %ld = %s
 Cylinders DIALUP AT %s BY %s DOS (MBR) DOS Compatibility flag is not set DOS Compatibility flag is set (DEPRECATED!) DOS R/O DOS access DOS secondary DRDOS/sec (FAT-12) DRDOS/sec (FAT-16 < 32M) DRDOS/sec (FAT-16) Darwin UFS Darwin boot Delete Delete the current partition Dell Utility Device Device does not contain a recognized partition table. Device is open in read-only mode. Device: %s
 Did not write partition table to disk. Directory data: %zd bytes
 Disk %s: %s, %ju bytes, %ju sectors Disk identifier: %s Disk layout successfully dumped. Disk: %s DiskSecure Multi-Boot Disklabel type: %s Display or manipulate a disk partition table.
 Do a low-level formatting of a floppy disk.
 Do you really want to continue Double Down Arrow   Move cursor to the next partition Dump Dump partition table to sfdisk compatible script file EFI (FAT-12/16/32) EZ-Drive Empty End Enter script file name Enter script file name:  Everything: %zd kilobytes
 Expert command (m for help):  Extended Extra sectors per cylinder FAILED LOGIN SESSION FROM %s FOR %s, %s FAT12 FAT16 FAT16 <32M FATAL: %s is not a terminal FATAL: bad tty FSname: <%-6s>
 Failed to allocate script handler Failed to apply script %s Failed to parse script file %s Failed to parse size. Failed to read disk layout into script. Failed to set personality to %s Failed to transform disk layout into script Failed to write disklabel. Failed to write script %s Filesystem UUID: Filesystem label: Filesystem on %s is dirty, needs checking.
 Filesystem state=%d
 Filesystem: Finger information *NOT* changed.  Try again later.
 Finger information changed.
 Finger information not changed.
 Finished with %s (exit status %d)
 First %s First sector Firstdatazone=%jd (%jd)
 Fix partitions order Flags Forcing filesystem check on %s.
 Formatting ...  FreeBSD GNU HURD or SysV GPT Generated random UUID: %s
 Generated time UUID: %s
 Generic Geometry Geometry: %d heads, %llu sectors/track, %llu cylinders Golden Bow HFS / HFS+ Hardware clock is on %s time
 Heads Help Hex code (type L to list all codes):  Hidden FAT12 Hidden FAT16 Hidden FAT16 <32M Hidden HPFS/NTFS Hidden W95 FAT16 (LBA) Hidden W95 FAT32 Hidden W95 FAT32 (LBA) Home Phone Hw clock time : %4d/%.2d/%.2d %.2d:%.2d:%.2d = %ld seconds since 1969
 Hypervisor vendor: I/O size (minimum/optimal): %lu bytes / %lu bytes IBM Thinkpad hibernation Id  Name

 Including: %s
 Inode %d marked unused, but used for file '%s'
 Inode %lu (mode = %07o), i_nlinks=%d, counted=%d. Inode %lu mode not cleared. Inode %lu not used, marked used in the bitmap. Inode %lu used, marked unused in the bitmap. Inode end: %d, Data end: %d
 Inodes: %ld (in %llu blocks)
 Inodes: %ld (in 1 block)
 Input line too long. Interleave factor Internal error: trying to write bad block
Write request ignored
 Invalid operation %d
 Invalid user name "%s" in %s:%d. Abort. Invalid values in hardware clock: %4d/%.2d/%.2d %.2d:%.2d:%.2d
 Is /proc mounted? Issuing date command: %s
 It lets you create, delete, and modify partitions on a block device. Kernel is assuming an epoch value of %lu
 LANstep LOGIN ON %s BY %s LOGIN ON %s BY %s FROM %s LPGETIRQ error Label: %s Label: %s, identifier: %s Last calibration done at %ld seconds after 1969
 Last drift adjustment done at %ld seconds after 1969
 Last login: %.*s  Left Arrow   Move cursor to the previous menu item Line too long Linux Linux LVM Linux RAID Linux extended Linux native Linux plaintext Linux raid autodetect Linux swap Linux swap / Solaris Linux/PA-RISC boot Locking disk by %s ...  Logging in with home = "/".
 Login incorrect

 Make a Linux filesystem.
 Make an SCO bfs filesystem.
 Mark in use Maximum size is %ju bytes. Maxsize=%zu
 May be followed by M for MiB, G for GiB, T for TiB, or S for sectors. Message from %s@%s (as %s) on %s at %s ... Message from %s@%s on %s at %s ... Message queue id: %d
 Minimum size is %ju bytes. Minix / old Linux Misc Model: Mountpoint: NEC DOS NTFS volume set NULL user name in %s:%d. Abort. NUMA node(s): Name NeXTSTEP Needed adjustment is less than one second, so not setting clock.
 NetBSD New New UUID (in 8-4-4-4-12 format) New beginning of data New name New shell No next file No previous command to substitute for No previous file No remembered search string No usable clock interface found.
 Non-FS data Not adjusting drift factor because last calibration time is zero,
so history is bad and calibration startover is necessary.
 Not adjusting drift factor because the Hardware Clock previously contained garbage.
 Not enough arguments Not set Not setting system clock because running in test mode.
 Not superuser. Not updating adjtime file because of testing mode.
 Note that partition table entries are not in disk order now. Note: All of the commands can be entered with either upper or lower Novell Netware 286 Novell Netware 386 Number of alternate cylinders Number of cylinders Number of heads Number of physical cylinders Number of sectors OPUS OS/2 Boot Manager Office Office Phone Old Minix OnTrack DM OnTrack DM6 Aux1 OnTrack DM6 Aux3 OnTrackDM6 Only 1k blocks/zones supported OpenBSD PC/IX PPC PReP Boot Partition %zu does not exist yet! Partition %zu does not start on physical sector boundary. Partition %zu has been deleted. Partition UUID: Partition name: Partition number Partition size:  Partition table entries are not in disk order. Partition type (type L to list all types):  Partition type: PartitionMagic recovery Password:  Pattern not found Plan 9 Please, specify size. Press a key to continue. Priam Edisk Print help screen Probably you need root privileges.
 Problem reading track/head %u/%u, expected %d, read %d
 QNX4.x QNX4.x 2nd part QNX4.x 3rd part Quit Quit program without writing partition table RE error:  RO    RA   SSZ   BSZ   StartSec            Size   Device
 ROM image ROM image map ROM image write failed (%zd %zd) ROOT LOGIN ON %s ROOT LOGIN ON %s FROM %s Read error: bad block in file '%s'
 Read error: unable to seek to block in file '%s'
 Read:  Remove block Right Arrow  Move cursor to the next menu item Rotation speed (rpm) Ruffian BCD clock
 SCHED_%s min/max priority	: %d/%d
 SCHED_%s not supported?
 SFS SGI SGI bsd SGI efs SGI lvol SGI raw SGI rlvol SGI secrepl SGI sysv SGI trkrepl SGI volhdr SGI volume SGI xfs SGI xfslog SGI xlv SGI xvm Save & Exit Script Script done, file is %s
 Script started on %s Script started, file is %s
 Script successfully applied. Script successfully saved. Sector size (logical/physical): %lu bytes / %lu bytes Sectors Sectors/track See the specific fsck.* commands for available fs-options. Select (default %c):  Select a type to create a new label or press 'L' to load script file. Select label type Select partition type Semaphore id: %d
 Set Set i_nlinks to count Setting Hardware Clock to %.2d:%.2d:%.2d = %ld seconds since 1969
 Shared memory id: %d
 Shell changed.
 Show the length of an ISO-9660 filesystem.
 Single Size Size: %s, %ju bytes, %ju sectors Solaris Solaris boot Sort SpeedStor Start Stepping: Sun SunOS alt sectors SunOS cachefs SunOS home SunOS reserved SunOS root SunOS stand SunOS swap SunOS usr SunOS var Super block: %zd bytes
 Switching on %s.
 Syrinx TIOCSCTTY failed: %m Tell the kernel about the existence of a specified partition.
 Tell the kernel to forget about a specified partition.
 The current in-memory partition table will be dumped to the file. The date command issued by %s returned something other than an integer where the converted time value was expected.
The command was:
  %s
The response was:
 %s
 The device properties (sector size and geometry) should be used with one specified device only. The directory '%s' contains a bad inode number for file '%.*s'. The file `%s' has mode %05o
 The partition table has been altered. The script file will be applied to in-memory partition table. The type of partition %zu is unchanged. This is cfdisk, a curses-based disk partitioning program. Thread(s) per core: Time read from Hardware Clock: %4d/%.2d/%.2d %02d:%02d:%02d
 Toggle bootable flag of the current partition Type Type "yes" or "no", or press ESC to leave this dialog. Type of partition %zu is unchanged: %s. UTC Unable to allocate buffer for inode count Unable to allocate buffer for inode map Unable to allocate buffer for inodes Unable to allocate buffer for zone count Unable to allocate buffer for zone map Unable to read inode map Unable to read inodes Unable to read zone map Unable to run 'date' program in /bin/sh shell. popen() failed Unable to set system clock.
 Unable to set the epoch value in the kernel.
 Unable to set up swap-space: unreadable Unable to write inode map Unable to write inodes Unable to write zone map Unassigned Units: %s of %d * %ld = %ld bytes Unknown Unknown command: %s Unknown user context Unlocking %s.
 Unmark Unpartitioned space %s: %s, %ju bytes, %ju sectors Up Arrow     Move cursor to the previous partition Usage: %s [options] [mask | cpu-list] [pid|cmd [args...]]

 Usage: %s [options] device [block-count]
 Use lsblk(8) or partx(8) to see more details about the device. Using UTC time.
 Using default response %c. Using local time.
 VMware VMFS VMware VMKCORE Value out of range. Vendor ID: Venix 80286 Verifying ...  Virtualization type: Virtualization: Volume: <%-6s>
 W95 Ext'd (LBA) W95 FAT16 (LBA) W95 FAT32 W95 FAT32 (LBA) WARNING: device numbers truncated to %u bits.  This almost certainly means
that some device files will be wrong. Waiting for clock tick...
 Waiting in loop for time from %s to change
 Warning... %s for device %s exited with signal %d. Warning: Firstzone != Norm_firstzone
 Warning: inode count too big.
 Weird values in do_check: probably bugs
 Welcome to fdisk (%s). Whole disk Would have written the following to %s:
%s Would you like to edit %s now [y/n]?  Write Write error: bad block in file '%s'
 Write partition table to disk (this might destroy data) XENIX root XENIX usr You are using shadow groups on this system.
 You are using shadow passwords on this system.
 You have mail.
 You have new mail.
 You're editing nested '%s' partition table, primary partition table is '%s'. Zone %lu: in use, counted=%d
 Zone %lu: marked in use, no file uses it. Zone %lu: not in use, counted=%d
 Zone nr < FIRSTZONE in file `%s'. Zone nr >= ZONES in file `%s'. Zonesize=%d
 [Not a file] line %d [Press 'h' for instructions.] [Press space to continue, 'q' to quit.] [Use q or Q to quit] add a new partition alarm %ld, sys_time %ld, rtc_time %ld, seconds %u
 allocated queues = %d
 allocated semaphores = %d
 already removed id already removed key att_time=%-26.24s
 attached bad arguments bad data in track/head %u/%u
Continuing ...  bad filename length bad inode offset bad inode size bad magic number in super-block bad response length bad root offset (%lu) bad s_imap_blocks field in super-block bad s_zmap_blocks field in super-block bad speed: %s bad timeout value: %s bad v2 inode size badblock number input error on line %d
 big block size smaller than physical sector size of %s blocks argument too large, max is %llu bogus mode: %s (%o) booted from MILO
 bytes bytes/sector can't fork
 cannot access file %s cannot check %s: fsck.%s not found cannot close file %s cannot create directory %s cannot daemonize cannot determine size of %s cannot find the device for %s cannot fork cannot get size of %s cannot get terminal attributes for %s cannot open %s cannot open %s: %s cannot read cannot read %s cannot seek cannot set line discipline cannot set terminal attributes for %s cannot stat %s case letters (except for Write). cgid change change a partition type change disk GUID change display/entry units change interleave factor change number of alternate cylinders change number of cylinders change number of extra sectors per cylinder change number of heads change number of physical cylinders change number of sectors/track change partition UUID change partition name change rotation speed (rpm) change the disk identifier change_time=%-26.24s
 changed check aborted.
 chown failed: %s clockport adjusted to 0x%x
 close failed connect could not determine current format type could not get device size could not read directory %s couldn't compute selinux context couldn't find matching filesystem: %s cpid cramfs endianness is %s
 crc error create SGI info create a new empty DOS partition table create a new empty GPT partition table create a new empty SGI (IRIX) partition table create a new empty Sun partition table create an IRIX (SGI) partition table create message queue failed create semaphore failed create share memory failed ctime = %-26.24s
 cuid cylinders cylinderskew data block too large date string %s equates to %ld seconds since 1969.
 decompression error: %s delete a partition dest det_time=%-26.24s
 detached different directory inode has zero offset and non-zero size: %s divisor '%s' done
 dump disk layout to sfdisk script file edit bootfile entry edit drive data empty long option after -l or --long argument enter protective/hybrid MBR error %d (%m) while executing fsck.%s for %s error closing %s error writing . entry error writing .. entry error writing inode error writing root inode error writing superblock error: %s: probing initialization failed error: uname failed excessively long line arg exec failed
 expected a number, but got '%s' extra functionality (experts only) failed failed to add partition failed to allocate iterator failed to allocate libfdisk context failed to allocate output line failed to allocate output table failed to create a new disklabel failed to execute %s failed to get pid %d's attributes failed to get pid %d's policy failed to parse pid failed to parse priority failed to read partitions failed to read symlink: %s failed to read timing file %s failed to read typescript file %s failed to remove partition failed to set pid %d's policy failed to setup description for %s failed to write disklabel fifo has non-zero size: %s file extends past end of filesystem file inode has zero offset and non-zero size file inode has zero size and non-zero offset file length too short filename length is zero filesystem too big.  Exiting. fix partitions order flush buffers fork failed fork() failed, try again later
 from %.*s
 fsname name too long full funky TOY!
 get 32-bit sector count (deprecated, use --getsz) get alignment offset in bytes get blocksize get discard zeroes support status get filesystem readahead get logical block (sector) size get max sectors per request get minimum I/O size get optimal I/O size get physical block (sector) size get read-only get readahead get size in bytes gid headswitch ignoring given class data for idle class ignoring given class data for none class illegal day value: use 1-%d illegal month value: use 1-12 incomplete write to "%s" (written %zd, expected %zd)
 install bootstrap interleave internal error internal error, contact the author. internal error: unsupported dialog type %d invalid argument - from invalid argument - repair invalid argument - to invalid argument of -r invalid argument of -r: %d invalid block-count invalid blocksize argument invalid cylinders argument invalid divisor argument invalid edition number argument invalid endianness given; must be 'big', 'little', or 'host' invalid file data offset invalid heads argument invalid id invalid key invalid length argument invalid length value specified invalid number of inodes invalid offset value specified invalid option invalid partition number argument invalid sector size argument invalid sectors argument invalid start argument ioctl error on %s ioctl failed: unable to determine device size: %s ioctl(%s) was successful.
 ioctl() to %s to turn off update interrupts failed ioctl() to %s to turn on update interrupts failed unexpectedly ioctl(RTC_EPOCH_READ) to %s failed ioctl(RTC_EPOCH_SET) to %s failed ioprio_get failed ioprio_set failed kernel not configured for message queues
 kernel not configured for semaphores
 kernel not configured for shared memory
 key last-changed last-op lchown failed: %s link BSD partition to non-BSD partition list free unpartitioned space list known partition types little load disk layout from sfdisk script file local locked login:  lpid lrpid lspid max number of arrays = %d
 max ops per semop call = %d
 max queues system wide = %d
 max semaphores per array = %d
 max semaphores system wide = %d
 messages missing optstring argument mkdir failed: %s mknod failed: %s mode=%#o	access_perms=%#o
 mode=%#o, access_perms=%#o
 mount table full mount: %s does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
 move beginning of data in a partition msqid must be root to scan for matching filesystems: %s n
 namelen=%zd

 nattch ncount need terminal for interactive repairs no no label,  no length argument specified no uuid
 non-block (%ld) bytes non-size (%ld vs %ld) bytes none not a block device or file: %s not enough space allocated for ROM image (%lld allocated, %zu used) not enough space, need at least %llu blocks not found DOS label driver nsems old cramfs format on %.*s
 operation %d, incoming num = %d
 otime = %-26.24s
 owner para parse error: %s past first line permission denied for id permission denied for key perms pid print information about a partition print the partition table print the raw data of the disklabel from the device print the raw data of the first sector from the device print this menu quit without saving changes rcv_time=%-26.24s
 read count read error on %s read romfs failed recv reread partition table resource(s) deleted
 response from date command = %s
 return from BSD to DOS return from protective/hybrid MBR to GPT return to main menu root inode is not directory root inode isn't a directory rpm same saved sector count: %d, sector size: %d
 sectors/cylinder sectors/track seek error seek error on %s seek failed seek failed in bad_zone seek failed in check_blocks seek failed in write_block seek failed in write_super_block seek on %s failed select bootable partition select sgi swap partition select() to %s to wait for clock tick failed semid semnum send send_time=%-26.24s
 set filesystem readahead set read-only set read-write set readahead settimeofday() failed setuid() failed shmid show complete disklabel size error in symlink: %s socket socket has non-zero size: %s special file has non-zero offset: %s speed %d unsupported status succeeded superblock magic not found superblock size (%d) too small symbolic link has zero offset symbolic link has zero size symlink failed: %s the -l option can be used with one device only -- ignore timings file %s: %lu: unexpected format toggle a bootable flag toggle the GUID specific bits toggle the dos compatibility flag toggle the legacy BIOS bootable flag toggle the mountable flag toggle the no block IO protocol flag toggle the read-only flag toggle the required partition flag too many alternate speeds too many arguments too many devices too many inodes - max is 512 total track-to-track seek tracks/cylinder trackskew trouble reading terminfo uid unable to alloc buffer for superblock unable to alloc new libblkid probe unable to create new selinux context unable to erase bootbits sectors unable to matchpathcon() unable to read super block unable to resolve '%s' unable to rewind swap-device unable to test CRC: old cramfs format unable to write super-block unexpected end of file on %s unknown unknown compatibility mode '%s' unknown shell after -s or --shell argument unshare failed unsupported color mode unsupported disklabel: %s unsupported filesystem features usage: %s [-h] [-v] [-b blksize] [-e edition] [-N endian] [-i file] [-n name] dirname outfile
 -h         print this help
 -v         be verbose
 -E         make all warnings errors (non-zero exit status)
 -b blksize use this blocksize, must equal page size
 -e edition set edition number (part of fsid)
 -N endian  set cramfs endianness (big|little|host), default host
 -i file    insert a file image into the filesystem (requires >= 2.4.0)
 -n name    set name of cramfs filesystem
 -p         pad by %d bytes for boot code
 -s         sort directory entries (old option, ignored)
 -z         make explicit holes (requires >= 2.3.39)
 dirname    root of the filesystem to be compressed
 outfile    output file
 used arrays = %d
 used headers = %d
 used-bytes user defined end track exceeds the medium specific maximum user defined start track exceeds the medium specific maximum user defined start track exceeds the user defined end track utime failed: %s value verify the partition table volume name too long wait: no more child process?!? waitpid failed warning: file sizes truncated to %luMB (minus 1 byte). warning: filenames truncated to %u bytes. warning: files were skipped due to errors. warning: gids truncated to %u bits.  (This may be a security concern.) warning: uids truncated to %u bits.  (This may be a security concern.) we have read epoch %ld from %s with RTC_EPOCH_READ ioctl.
 write write failed write failed: %s write table to disk write table to disk and exit write to stdout failed y
 yes zcount zero file count Project-Id-Version: util-linux-ng 2.13.1-rc1
Report-Msgid-Bugs-To: Karel Zak <kzak@redhat.com>
POT-Creation-Date: 2015-11-02 11:43+0100
PO-Revision-Date: 2016-02-19 20:36+0000
Last-Translator: Wendrock <wendrock@googlemail.com>
Language-Team: Ubuntu German Translators
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 16:05+0000
X-Generator: Launchpad (build 18115)
Language: de
 	UTC: %s
 	tv.tv_sec = %ld, tv.tv_usec = %ld
 	tz.tz_minuteswest = %d
 
%6d reguläre Dateien
%6d Verzeichnisse
%6d zeichenorientierte Geräte
%6d blockorientierte Geräte
%6d Verknüpfungen
%6d symbolische Verknüpfungen
------
%6d Dateien
 
%6ld INodes benutzten (%ld%%)
 
%s: Versatz = %ju, Größe = %zu Bytes. 
*** %s: Verzeichnis ***

 
******** %s: Keine Textdatei ********

 
***Zurück***

 
… wird übersprungen  
Mögliche Spalten (für -o):
 
Möchten Sie wirklich abbrechen?  
Hilfe (Experten-Befehle):
 
Hilfe:
 
Bekannte <ldisc>-Namen:
 
Anmeldung falsch
 
Nachrichtenwarteschlange msqid=%d
 
Den meisten Befehlen kann optional ein Ganzzahlargument k vorausgehen; die
Voreinstellung in Klammern. Sternchen (*) bedeutet, dass das Argument die neue
Voreinstellung wird.
 
Optionen:
 
Optionen:
 -N, --inodes=NUM    legt die gewünschte Anzahl der Inodes fest
 -V, --vname=NAME    legt den Namen des Datenträgers fest
 -F, --fname=NAME    legt den Namen des Dateisystems fest
 -v, --verbose       zeigt, was durchgeführt wird
 -c                  diese Option wird ohne Ausgabe ignoriert
 -l                  diese Option wird ohne Ausgabe ignoriert
 -V, --version       zeigt die Versionsinformationen an und beendet
                     -V als Version muss die einzige Option sein
 -h, --help          zeigt die Hilfe an und beendet

 
Muster wurde nicht gefunden
 
Skript beendet: %s 
Semaphorenfeld semid=%d
 
Gemeinsamer Speicher Segment shmid=%d
 
Aufruf:
 %1$s -V
 %1$s --report [Geräte]
 %1$s -[-v|-q] Befehle Geräte

Verfügbare Befehle:
 
Benutzung:
 %s [Optionen]
                bestätigen (Eingabe von »yes«) oder das Schreiben abbrechen (Eingabe von »no«)                da dies Daten auf dem Gerät zerstören könnte, sollten Sie entweder         (%s-Partitionstabelle gefunden).          (ohne libblkid kompiliert).       --bytes                   Ausgabe der GRÖSSE in Bytes statt im menschenlesbaren Format
      --extract[=<dir>]    entpacken testen, optional wird nach <dir> entpackt
      <device>       Pfad zum zu benutzenden Gerät
      <Größe>        Anzahl der Blöcke, die auf den Gerät benutzt werden sollen
      FS-Optionen     Parameter für den realen Dateisystem-Ersteller
   Überlauf
   W  Partitionstabelle auf das Gerät schreiben (das W ist als Großbuchstabe einzugeben);   b          (De)Aktivieren der Bootfähig-Markierung der aktuellen Partition   d          Die aktuelle Partition löschen   h          Diese Hilfe anzeigen   Loch bei %lu (%zu)
   n          Aus dem freien Bereich eine neue Partition erzeugen   q          das Programm beenden, ohne die Partitionstabelle zu schreiben   s          korrigiert die Partitionsreihenfolge (nur falls ungeordnet)   t Partitionstyp ändern   u  Abspeichern der Laufwerksstruktur in einer zu sfdisk kompatiblen Datei   Block bei %lu wird nach %lu entpackt (%lu)
   x Anzeigen oder verbergen der Zusatzinformationen der Partition  %-25s Die Größe in 512-Byte-Sektoren abrufen
  %1$s [Optionen] <Medium>
  %1$s [Optionen] <Medium>     ändern der Partitionstabelle
 %1$s [Optionen] -l [<Medium>] auflisten der Partitionstabelle(n)
  %s <Datenträger> <Partitionsnummer>
  %s <Datenträger> <Partitionsnummer> <Start> <Länge>
  %s [options] -- [fs-options] [<Dateisystem> ...]
  %s [Optionen] /dev/name [Blöcke]
  %s [Optionen] <Gerät>
  %s [Optionen] <Datei>
  %s [Optionen] <iso9660_image_file>
  %s [Optionen] [-t <Typ>] [FS-Optionen] <Gerät> [<Größe>]
  -1                      benutze Minix-Version 1
  -2, -v                  benutze Minix-Version 2
  -3,                     benutze Minix-Version 3
  -?         zeigt die Hilfe an und beendet
  -A         Dateisystem überprüfen
  -B, --protect-boot            die Boot-Bits werden beim Erstellen eines neuen Labels nicht gelöscht
  -C [<fd>]  zeigt einen Fortschrittsbalken; Dateibezeichnung (fd) für GUIs
  -C, --cylinders <Nummer>      gibt die Anzahl der Zylinder an
  -H, --heads <Nummer>         gibt die Anzahl der Köpfe an
  -L, --color[=<when>]          einfärben der Ausgabe (automatisch, immer oder nie)
  -L, --color[=<when>]     färbt die Ausgabe (automatisch, immer oder nie)
  -M         eingehängte Dateisysteme werden nicht überprüft
  -N         Es wird nicht ausgeführt, es wird nur angezeigt was getan würde
  -P         überprüft Dateisysteme parallel, inklusive root
  -R         Überspringe das Root-Dateisystem; nur nützlich mit '-A'
  -S, --sectors <number>        legt die Anzahl der Sektoren pro Spur fest
  -T         Zeigt den Titel beim Starten nicht an
  -V         zeigt an, was durchgeführt wird
  -V, --verbose      zeigt an, was durchgeführt wird;
                      wird -V mehr als einmal verwendet, wird ein Testlauf gestartet
  -V, --version      zeigt die Versionsinformationen an und beendet;
                      -V als --version muss die einzige Option sein
  -a                       nur zur Kompatibilität, wird ignoriert
  -a, --auto       automatische Reparatur
  -b, --blocksize <size>   verwende diese Blockgröße, keine Anpassung der Größe
  -b, --sector-size <Größe>      physische und logische Sektorgröße
  -c, --check             überprüfe das Gerät auf fehlerhafte Blöcke
  -c, --compatibility[=<mode>]  Modus ist »dos« oder »nondos« (Voreinstellung)
  -d, --divisor=<number>  Teilt die Summe der Bytes durch <number>
  -f, --force      erzwingt die Überprüfung
  -f, --from <N>    Beginne bei Spur N (Standard 0)
  -h, --help         zeigt diesen Hilfetext an und beendet
  -i, --inodes <num>      Anzahl der Inodes für das Dateisystem
  -l         sperrt das Gerät für einen garantiert exklusiven Zugriff
  -l, --badblocks <Datei>  zeigt die fehlerhaften Blöcke der Datei an
  -l, --list                    zeigt die Partitionen und beendet
  -l, --list       listet alle Dateinamen auf
  -n, --namelength <num>  maximale Länge der Dateinamen
  -n, --no-verify   abschalten der Verifizierung nach dem Formatieren
  -o, --output <list>           Ausgabe in Spalten
  -r [<fd>]  liefert eine Statistik für jedes überprüfte Gerät;Dateibezeichnung (fd) für GUIs
  -r, --repair     interaktive Reparatur
  -r, --repair <N>  Versuche fehlerhafte Spuren
                      während der Überprüfung zu
                      reparieren (max N Versuche)
  -s         serialisiert die Überprüfungs-Aktionen
  -s, --getsz                   zeigt die Größe des Mediums in 512-Byte Sektoren an [VERALTET]
  -s, --super      gibt die Super-Block Informationen aus
  -t <type>  legt den Typ des zu überprüfenden Dateisystemes fest;
            <type> eine mit Komma getrennte Liste ist erlaubt
  -t, --to <N>      Stoppe bei Spur N
  -t, --type <type>             identifiziert nur den angegebenen Typ der Partitionstabellen
  -t, --type=<Typ>  Typ des Dateisystems; wenn nicht angegeben, wird ext2 verwendet
  -u, --units[=<unit>]          Anzeige der Einheiten: »Zylinder« oder »Sektoren« (Standard)
  -v, --verbose            ausführlicher
  -v, --verbose    ausführlich
  -x, --sectors           zeigt Sektorennummer und Größe
  -y                       nur zur Kompatibilität, wird ignoriert
  -z, --zero  mit einer leeren Partitions-Tabelle beginnen
  Entfernen  [J]a/[N]ein:   badsect  ecc  entfernbar »%s« Zeile %d %15s: %s %6.2f%% (%+ld Bytes)	%s
 %6ld zones benutzten (%ld%%)
 %c: unbekannter Befehl %ld Blöcke
 %ld inodes
 %s %d %s %s: Status ist %x, sollte niemals passieren. %s (%c-%c):  %s (%c-%c, Vorgabe %c):  %s (%ju-%ju):  %s (%ju-%ju, Vorgabe %ju):  %s (%s, Vorgabe %c):  %s (%s, Vorgabe %ju):  %s (eingehängt) %s (J/N)?  %s (J/N)?  %s Cache: %s hat keine Interrupt-Funktionen.  %s ist fehlgeschlagen.
 %s von %s
 %s ist in Ordnung, keine Überprüfung.
 %s ist eingehängt
 %s ist eingehängt.	  %s ist eingehängt; daher wird hier kein Dateisystem angelegt! %s ist kein blockorientiertes Gerät %s ist keine serielle Leitung %s ist nicht eingehängt
 %s benötigt einen Parameter Der Status von %s ist %d %s wurde erfolgreich beendet.
 %s nimmt keine Nicht-Options-Argumente an. Sie gaben %d an.
 %s unbekannte Spalte: %s %s benutzt IRQ %d
 %s benutzt Polling
 %sseitig, %d Spuren, %d Sektoren/Spur, Gesamtkapazität: %dkB.
 %s: OK
 %s: nicht erkannte Architektur %s: RTC wird als UTC interpretiert …
 %s: ungültiges Verzeichnis: ».« kommt nicht zuerst
 %s: ungültiges Verzeichnis: »..« kommt nicht als zweites
 %s: ungültiges Verzeichnis: Größe < 32 %s: Fehler beim Ausführen von %s: %m %s: inotify-Überwachung kann nicht hinzugefügt werden, die Begrenzung der inotify-Überwachungen wurde erreicht. %s: inotify-Überwachung kann nicht hinzugefügt werden %s: inotify-Ereignisse können nicht gelesen werden %s: dup‐Problem: %m %s: Anzahl der symbolischen Verknüpfungen überschritten %s: Ausführung fehlgeschlagen %s: Initialisierung des sysfs Steuerprogramms fehlgeschlagen %s: Auslesen von fstab fehlgeschlagen %s: Lesen des Anfangs der Partition durch sysfs fehlgeschlagen %s: Größenermittlung fehlgeschlagen %s: Eingabe‐Überlauf %s: unsichere Zugriffsrechte %04o, %04o wird empfohlen %s: last_page 0x%08llx ist größer als die tatsächliches Größe des Auslagerungsbereichs %s: lseek fehlgeschlagen %s: ist vielleicht kein ISO Dateisystem %s: Kein Block-Gerät %s: nicht genügend gute Blöcke %s: nicht offen für Lesen/Schreiben %s: Auswertungsfehler bei Zeilennummer %d -- ignorieren %s: Swap-Header konnte nicht gelesen werden %s: gelesen: %m %s: Auslagerungsbereich wird erneut initialisiert. %s: »seek« fehlgeschlagen im Schreibblock %s: »seek« fehlgeschlagen im Schreibblock %s: wird übersprungen –  scheint Löcher zu enthalten. %s: nicht existierendes Gerät wird übersprungen
 %s: Überspringen des unbekannten Types für das Dateisystem
 %s: swapoff fehlgeschlagen %s: swapon fehlgeschlagen %s: Zu viele fehlerhafte Blöcke %s: es ist nicht möglich den Boot-Sektor zu löschen %s: »inode map« konnte nicht geschrieben werden %s: Inodes konnten nicht geschrieben werden %s: Superblock konnte nicht geschrieben werden %s: »zone map« konnte nicht geschrieben werden %s: Schreiben fehlgeschlagen im Schreibblock %s: Schreiben der Signatur fehlgeschlagen (Dateiende) (Nächste Datei:  (Nächste Datei: %s) (warten)  , belegt , Fehler , on-line , kein Papier , bereit -- Zeile schon geschrieben ------ Nachrichtenwarteschlangen --------
 ------ Nachrichtenwarteschlangen: PIDs --------
 ------ Nachrichtenwarteschlangen: Versand/Empfang/Änderung-Zeiten --------
 ------ Semaphorenfelder --------
 ------ Semaphorenfelder: Erzeuger/Besitzer --------
 ------ Semaphorengrenzen --------
 ------ Semaphoroperation/Änderungszeiten --------
 ------ Semaphorenstatus --------
 ------ Gemeinsamer Speicher: Attach/Detach/Change-Zeiten --------
 ------ Gemeinsamer Speicher: Grenzen --------
 ------ Gemeinsamer Speicher: Erzeuger/Besitzer der Segmente --------
 ------ Gemeinsamer Speicher: Segmente --------
 ------ Gemeinsamer Speicher: Status --------
 --------     --------- --------------------------
DATEISYSTEM WURDE GEÄNDERT
--------------------------
 --Mehr-- --warten-- (Durchgang %d)
 … Sprung zurück zu Datei  … Sprung zu Datei  … Uhrtick wurde empfangen
 … wird übersprungen
 … Sprung rückwärts
 … Sprung vorwärts
 … Synchronisierung fehlgeschlagen
 /dev/%s konnte nicht als Standardeingabe geöffnet werden: %m /dev/%s ist kein zeichenorientiertes Gerät : !befehl im rflag-Modus nicht erlaubt.
 <Leertaste>             die nächsten k Zeilen Text zeigen [Bildschirmgröße]
z                       die nächsten k Zeilen Text zeigen [Bildschirmgröße]*
<Eingabe>               die nächsten k Zeilen Text zeigen [1]*
d oder Strg-D           k Zeilen rollen [momentane Rollgröße, anfangs 11]*
q oder Q oder <Untbr>   more beenden
s                       k Zeilen Text vorwärts springen [1]
f                       k Bildschirme Text vorwärts springen [1]
b oder Strg-B           k Bildschirme Text rückwärts springen [1]
'                       zum Anfang der letzten Suche gehen
=                       momentane Zeilennummer zeigen
/<regulärer Ausdruck>   nach k-tem Auftreten des regulären Ausdrucks suchen [1]
n                       nach k-tem Auftreten des letzten reg. Ausdr. suchen [1]
!<bef> oder :!<bef>     <bef> in einer Subshell ausführen
v                       /usr/bin/vi an momentaner Zeile starten
Strg-L                  Schirm neu zeichnen
:n                      zur k-ten nächsten Datei gehen [1]
:p                      zur k-ten vorigen Datei gehen [1]
:f                      momentanen Dateinamen und Zeilennummer zeigen
.                       letzten Befehl wiederholen
 Eine hybride GPT wurde entdeckt. Sie müssen den hybriden MBR manuell synchronisieren (Befehl für erfahrene Nutzer 'M'). AIEEE: Block auf > 2×Blocklänge (%ld) »komprimiert«
 AIX AIX bootfähig AST SmartSleep Ausrichtungsversatz: %lu Bytes Amoeba Amoeba BBT Sind Sie sicher, dass Sie die Partitionstabelle auf das Medium schreiben wollen?  Die Hardwareuhr läuft vermutlich in %s.
 Werte: BBT »BLKGETSIZE ioctl« fehlgeschlagen auf %s BSD BSD/OS BSDi Dateisystem BSDI Swap BeOS Dateisystem Block %d in Datei »%s« ist als unbenutzt markiert. Block wurde zuvor benutzt, nun in Datei »%s«. Blockgröße: %d
 Blöcke: %llu
 Boot Boot-Assistent versteckt BootIt Bootfähig CP/M CP/M / CTOS / ... CPU MHz: Prozessorfamilie: CRC: %x
 settimeofday() wird aufgerufen:
 %s kann nicht geöffnet werden Partitions-Typ ändern Partitiontyp von  %zu wurde geändert. Der Typ der Partition »%s« wurde zu »%s« geändert. Änderungen werden im Speicher behalten, bis Sie sich entscheiden sie zu schreiben.
Nutzen sie den Schreibbefehl vorsichtig.
 Anzeige/Eintrag der Einheiten der Zylinder ändern (VERALTET!). Anzeige/Eintrag der Einheiten der Sektoren ändern. Finger-Information für %s wird geändert
 Shell für %s ändern.
 Ein Linuxdateisystem überprüfen und reparieren.
 ROM-Dateisystem überprüfen und reparieren.
 Überprüfung der Konsistenz eines Minix Dateisystems.
 Überprüfung aller Dateisysteme.
 Zurücksetzen Die Uhrzeit der Hardwareuhr wurde nicht verstellt – Testmodus.
 Befehl      Bedeutung Befehl (m für Hilfe):  Compaq Diagnostik Kern(e) pro Socket: Korrigieren Die Partition %zu konnte nicht gelöscht werden. Partition %zu konnte nicht gelöscht werden. Datei mit den Parametern zur Uhreinstellung (%s) konnte nicht zum Schreiben geöffnet werden Konnte den Schalter nicht verändern. Datei mit den Parametern zur Uhreinstellung (%s) konnte nicht aktualisiert werden PAM konnte nicht initialisiert werden: %s Neue Bezeichnung erzeugen Neue Partition im freien Bereich anlegen Aktuelle Systemzeit: %ld = %s
 Zylinder VERBINDUNGSAUFBAU BEI %s DURCH %s DOS (MBR) DOS Kompatibilitätsschalter ist nicht gesetzt DOS Kompatibilitätsschalter ist gesetzt (VERALTET!) DOS R/O DOS-Zugriff DOS sekundär DRDOS/sec (FAT-12) DRDOS/sec (FAT-16 < 32M) DRDOS/sec (FAT-16) Darwin UFS Darwin Boot Löschen Die aktuelle Partition löschen Dell Dienstprogramm Gerät Gerät verfügt über keine erkennbare Partitionstabelle Das Gerät ist nur lesbar geöffnet. Gerät: %s
 Die Partitionstabelle wurde nicht auf das Medium geschrieben. Verzeichnisdaten: %zd Bytes
 Medium %s: %s, %ju Bytes, %ju Sektoren Medienkennung: %s Das Layout des Mediums wurde erfolgreich abgebildet. Platte: %s DiskSecure Multi-Boot Typ der Medienbezeichnung: %s Anzeigen oder ändern der Partitionstabelle des Mediums.
 Führe eine »low-level« Formatierung einer Diskette durch.
 Möchten Sie wirklich fortfahren Doppelt Pfeil-runter den Cursor zur nächsten Partition bewegen Abspeichern Abspeichern der Partitionstabelle in eine zu sfdisk kompatible Datei EFI (FAT-12/16/32) EZ-Drive Leer Ende Dateiname des Skriptes eingeben Eingabe des Dateinamens für das Script:  Alles: %zd Kilobytes
 Expertenbefehl (m für Hilfe):  Erweiterte Extra-Sektoren pro Zylinder ANMELDESITZUNG VON %s FÜR %s, %s FEHLGESCHLAGEN FAT12 FAT16 FAT16 <32M FATAL: %s ist kein Terminal FATAL: ungültiges TTY DSName: <%-6s>
 Das Zuweisen des Steuerprogramms für das Skript ist fehlgeschlagen Ausführung des Skripts %s gescheitert Fehler beim Parsen der Skriptdatei  %s Fehler bei der Größenbestimmung. Das Layout des Mediums konnte nicht in das Script geladen werden. Persönlichkeit konnte nicht auf %s gesetzt werden Umwandlung der Laufwerksstruktur in das Skript fehlgeschlagen Das Schreiben der Medienbezeichnung ist fehlgeschlagen. Fehler beim Schreiben des Skriptes %s Dateisystem-UUID Bezeichnung des Dateisystems: Dateisystem auf %s ist gestört (dirty), Überprüfung erforderlich.
 Dateisystemstatus=%d
 Dateisystem: Finger-Information *NICHT* geändert. Versuchen Sie es später erneut.
 Finger-Information geändert.
 Finger-Information nicht verändert
 Abgeschlossen mit %s (Beendigungsstatus %d)
 Erster %s Erster Sektor Firstdatazone=%jd (%jd)
 Partitions-Reihenfolge in Ordnung bringen Markierungen Überprüfung des Dateisystem auf %s wird erzwungen.
 Formatierung läuft …  FreeBSD GNU HURD oder SysV GPT Zufällige UUID wurde erzeugt: %s
 Zeitbasierte UUID wurde erzeugt: %s
 generisch Geometrie Geometrie: %d Köpfe, %llu Sektoren/Spur, %llu Zylinder Golden Bow HFS / HFS+ Hardwareuhr geht nach %s Zeit
 Köpfe Hilfe Hexadezimalcode (Eingabe von L listet alle Typen auf):  Verst. FAT12 Verst. FAT16 Verst. FAT16 <32M Verst. HPFS/NTFS Verst. W95 FAT16 (LBA) Verst. W95 FAT32 Verst. W95 FAT32 (LBA) Haustelefon Zeit der Hardwareuhr: %4d/%.2d/%.2d %.2d:%.2d:%.2d = %ld Sekunden seit 1969
 Hypervisor-Anbieter: I/O Größe (minimal/optimal): %lu Bytes / %lu Bytes IBM Thinkpad Ruhezustand Id  Name

 Einschließlich: %s
 Inode %d ist als unbenutzt gekennzeichnet, aber Datei »%s« benutzt sie
 Inode %lu (Modus = %07o), i_nlinks=%d, gezählt=%d. Modus des Inode %lu nicht bereinigt. Inode %lu wird nicht benutzt, in der bitmap als benutzt markiert. Inode %lu wird benutzt, in der bitmap als unbenutzt markiert. Inode-Ende: %d, Datenende: %d
 Inodes: %ld (in %llu Blöcken)
 Inodes: %ld (im 1 Block)
 Eingabezeile zu lang. Interleave-Faktor Interner Fehler: Versuch, einen beschädigten Block zu schreiben
Schreibanweisung ignoriert
 Ungültige Operation %d
 Ungültiger Benutzername »%s« in %s:%d. Abbruch. Ungültige Werte in Hardwareuhr: %4d/%.2d/%.2d %.2d:%.2d:%.2d
 Ist »/proc« eingehängt? Der »date«-Befehl wird aufgerufen: %s
 Sie können damit auf einem blockorientierten Gerät Partitionen anlegen, löschen oder ändern. Kernel geht von einem Epochenwert von %lu aus.
 LANstep ANMELDUNG AN %s DURCH %s ANMELDUNG AN %s DURCH %s VON %s „LPGETIRQ“ Fehler Label: %s Bezeichnung:%s, Identifizierer:%s Letzte Kalibrierung vorgenommen bei %ld Sekunden nach 1969
 Letzte Abweichungskorrektur vorgenommen bei %ld Sekunden nach 1969
 Letzte Anmeldung: %.*s  Cursortaste »links« ruft den vorangegangenen Menüpunkt auf Zeile ist zu lang Linux Linux LVM Linux RAID Linux erweitert Linux native Linux Klartext Linux raid autodetect Linux Swap Linux Swap / Solaris Linux/PA-RISC Boot Das Medium wird durch %s gesperrt …  Wird angemeldet mit Heimatverzeichnis = »/«.
 Anmeldung falsch

 Erzeuge ein Linux Dateisystem.
 Erzeuge ein SCO bfs Dateisystem.
 Inode als benutzt kennzeichnen Maximale Größe ist %ju Bytes. Maximale Größe=%zu
 Könnte gefolgt werden von M für MiB, G für GiB, T für TiB oder S für Sektoren. Nachricht von %s@%s (als %s) auf %s um %s … Nachricht von %s@%s auf %s um %s … Nachrichtenwarteschlangenkennung: %d
 Kleinste Größe ist %ju Bytes. Minix / altes Linux Verschiedenes Modell: Einhängepunkt: NEC DOS NTFS Datenträgersatz LEERER Benutzername in %s:%d. Abbruch. NUMA-Knoten: Name NeXTSTEP Da die Anpassung weniger als eine Sekunde betragen hätte, wird sie nicht durchgeführt.
 NetBSD Neu Neue UUID (im 8-4-4-4-12 Format) Neuer Datenanfang Neuer Name Neue Shell Keine nächste Datei Kein vorheriger Befehl, der eingefügt werden könnte Kein vorhergehende Datei Kein gemerkter Suchtext Keine brauchbare Uhrschnittstelle gefunden.
 Keine Dateisystemdaten Der Abweichungsfaktor wird nicht verändert, da die Zeit der letzten Kalibrierung
Null ist, also die Aufzeichnungen beschädigt sind und die Kalibrierung von neuem
starten muss.
 Der Abweichungsfaktor wird nicht verändert, da die Hardwareuhr vorher
keinen sinnvollen Wert enthielt.
 Nicht genug Argumente Nicht festgelegt Keine Änderung an der Systemuhr vorgenommen – Testmodus.
 Kein Administrator. Die adjtime-Datei wird nicht aktualisiert – Testmodus
 Achtung: Die Einträge der Partitions-Tabelle sind nicht in der Reihenfolge der Medien. Hinweis: Alle Befehle können mit Klein- oder Großbuchstaben Novell Netware 286 Novell Netware 386 Anzahl der alternativen Zylinder Anzahl der Zylinder Anzahl der Köpfe Anzahl der physischen Zylinder Anzahl der Sektoren OPUS OS/2-Bootmanager Büro Bürotelefon Altes Minix OnTrack DM OnTrack DM6 Aux1 OnTrack DM6 Aux3 OnTrackDM6 Es werden nur Blöcke/Zonen von 1k Größe unterstützt OpenBSD PC/IX PPC PReP Boot Die Partition %zu exisitiert bis jetzt noch nicht! Die Partition %zu beginnt nicht im Bereich der physischen Sektoren. Partition %zu wurde gelöscht. Partitions-UUID : Partitionsname: Partitionsnummer Partitionsgröße:  Die Einträge der Partitionstabelle stimmen nicht mit der Reihenfolge der Medien überein. Partitionstyp (Eingabe von »L« listet alle Typen auf):  Partitionstyp: PartitionMagic Wiederherstellung Passwort:  Muster wurde nicht gefunden Plan 9 Bitte geben Sie die Größe an. Drücken Sie eine Taste um fortzufahren. Priam Edisk Die Hilfe anzeigen Sie benötigen root-Rechte.
 Beim Lesen der Spur / des Kopfes %u/%u wurde %d erwartet, aber %d gelesen.
 QNX4.x QNX4.x 2. Teil QNX4.x 3. Teil Ende Das Programm beenden, ohne die Partitionstabelle zu speichern Fehler in regulärem Ausdruck:  NurL  RA   SGr   BGr   Startsek            Größe  Gerät
 ROM-Abbild ROM-Abbilddatei Schreiben des ROM-Images fehlgeschlagen (%zd %zd) ROOT-ANMELDUNG AUF %s ROOT-ANMELDUNG AUF %s VON %s Fehler beim Lesen: beschädigter Block in Datei »%s«
 Lesefehler: Springen zu Block in Datei »%s« ist nicht möglich
 Lesen:  Block entfernen Cursortaste »rechts« ruft den nächsten Menüpunkt auf Umdrehungsgeschwindigkeit (U/min) Bösartige BCD-Uhr
 SCHED_%s min./max. Priorität	: %d/%d
 SCHED_%s nicht unterstützt?
 SFS SGI SGI bsd SGI efs SGI lvol SGI raw SGI rlvol SGI secrepl SGI sysv SGI trkrepl SGI volhdr SGI-Datenträger SGI xfs SGI xfslog SGI xlv SGI xvm Speichern & Beenden Skript Skript wurde beendet, die Datei ist %s
 Skript gestartet auf %s Skript gestartet, die Datei ist %s
 Das Skript wurde erfolgreich ausgeführt. Das Skript wurde erfolgreich gespeichert. Sektorengröße (logisch/physisch): %lu Bytes / %lu Bytes Sektoren Sektoren/Spur Sehen Sie sich die speziellen fsck.* Befehle für die verfügbaren fs-Optionen an. Auswählen (Vorgabe %c):  Wählen Sie einen Typ, um eine neue Bezeichnung zu erzeugen oder drücken Sie 'L' um eine Skriptdatei zu laden. Bezeichnungsart auswählen Partitionstyp auswählen Semaphor-ID: %d
 Festlegen i_nlinks auf Zähler setzen Hardwareuhr wird auf %.2d:%.2d:%.2d gestellt = %ld Sekunden seit 1969
 ID des gemeinsam genutzten Speichers: %d
 Shell geändert.
 Die Länge eines ISO-9660 Dateisystems wird angezeigt.
 Einfach Größe Größe: %s, %ju Bytes, %ju Sektoren Solaris Solaris Boot Sortieren SpeedStor Start Stepping: Sun SunOS alt Sektoren SunOS cachefs SunOS home SunOS reserviert SunOS Root SunOS stand SunOS Swap SunOS usr SunOS var Superblock: %zd Bytes
 Auf %s wird geschaltet.
 Syrinx TIOCSCTTY fehlgeschlagen: %m Informiert den Kernel ob eine angegebene Partition existiert.
 Teilt dem Kernel mit, dass er die entsprechende Partition verwerfen soll.
 Die aktuell im Speicher gehaltene Partition wird in die Datei geschrieben. Der »date«-Befehl, der von %s aufgerufen wurde, lieferte keine ganze Zahl, wo die umgewandelte Zeit erwartet wurde.
Der Befehl war:
  %s
Die Ausgabe war:
  %s
 Die Eigenschaften des Gerätes (Sektorgröße und Geometrie) sollten nur mit einem festgelegten Gerät verwendet werden. Das Verzeichnis »%s« enthält eine ungültige Inode-Nummer für Datei »%.*s«. Die Datei »%s« hat den Modus %05o
 Die Partitionstabelle wurde geändert. Das Skript wird auf die Partition im Speicher angewendet. Partitiontyp von  %zu wurde nicht geändert. Das ist cfdisk, eine mit Cursortasten bedienbares Partitionsprogramm. Thread(s) pro Kern: Zeit gelesen aus Hardwareuhr: %4d/%.2d/%.2d %02d:%02d:%02d
 (De)Aktivieren der Bootfähig-Markierung der aktuellen Partition Typ Geben Sie "yes" oder "no" ein oder drücken sie ESC, um diesen Dialog zu verlassen. Der Partitionstyp %zu ist unverändert: %s. UTC Puffer für »inode count« konnte nicht zugewiesen werden Puffer für »inode_map« konnte nicht zugewiesen werden Puffer für »inodes« konnte nicht zugewiesen werden Puffer für »zone count« konnte nicht zugewiesen werden Puffer für »zone map« konnte nicht zugewiesen werden »inode map« kann nicht gelesen werden »inodes« können nicht gelesen werden »zone map« kann nicht gelesen werden »date« konnte nicht in der /bin/sh-Shell gestartet werden. popen() fehlgeschlagen Die Systemuhr konnte nicht gestellt werden.
 Der Epochenwert im Kernel kann nicht geändert werden.
 Es ist nicht möglich, einen Auslagerungsbereich einzurichten: nicht lesbar »inode map« konnte nicht geschrieben werden »inodes« konnten nicht geschrieben werden »zone map« konnte nicht geschrieben werden Nicht zugeordnet Einheiten: %s von %d * %ld = %ld Bytes Unbekannt Unbekannter Befehl: %s unbekannter Benutzerkontext Entsperren von %s.
 Mark. entf. Unpartitionierter Speicher %s: %s, %ju Byte, %ju Sektoren Pfeil-hoch   den Cursor zur vorherigen Partition bewegen Aufruf: %s [Optionen] [Maske | CPU-Liste] [pid|cmd [Argumente …]]

 Benutzung: %s [Optionen] Gerät [Block-Anzahl]
 Benutze lsblk(8) oder partx(8), um  mehr Informationen zum Gerät zu erhalten. UTC wird verwendet.
 Nutzung der Vorgabeantwort %c. Lokale Zeit wird verwendet.
 VMware VMFS VMware VMKCORE Der Wert ist außerhalb des gültigen Bereiches. Anbieterkennung: Venix 80286 Überprüfung läuft …  Virtualisierungstyp: Virtualisierung: Datenträger: <%-6s>
 W95 Erw. (LBA) W95 FAT16 (LBA) W95 FAT32 W95 FAT32 (LBA) WARNUNG: Anzahl der Geräte wurde auf %u Bits verkürzt. Das bedeutet meistens
das einige Gerätedateien falsch sind. Auf Uhrtick wird gewartet …
 Warten in Schleife auf Änderung der Zeit aus %s
 Warnung … %s für das Gerät %s wurde mit der Meldung %d beendet. Warnung: Firstzone != Norm_firstzone
 Warnung: »inode count« ist zu groß.
 Merkwürdige Werte in do_check: wahrscheinlich Fehler
 Willkomen bei fdisk (%s). Gesamte Festplatte Folgendes wäre in die Datei %s geschrieben worden:
%s Würden Sie jetzt gern %s bearbeiten [j/n]?  Schreib. Fehler beim Schreiben: beschädigter Block in Datei »%s«
 Die Partitionstabelle schreiben (dies kann Daten zerstören) XENIX root XENIX usr Sie benutzen Shadow-Gruppen auf diesem System.
 Sie benutzen Shadow-Passwörter auf diesem System.
 Sie haben E-Mail.
 Sie haben neue E-Mail.
 Sie bearbeiten eine verschachtelte '%s' Partitionstabelle, die primäre Partitionstabelle ist '%s'. Zone %lu: wird gerade genutzt, gezählt=%d
 Zone %lu: als benutzt markiert, keine Datei nutzt es. Zone %lu: wird nicht genutzt, gezählt=%d
 Zonennummer < FIRSTZONE in Datei »%s«. Zonennummer >= ZONES in Datei »%s«. Zonengröße=%d
 [Keine Datei] Zeile %d [Drücken Sie »h« für Hilfe.] [Leertaste zum Fortfahren, »q« zum Beenden.] [Benutzen Sie q oder Q zum Beenden] Hinzufügen einer neuen Partition Alarm %ld, sys_time %ld, rtc_time %ld, Sekunden %u
 zugewiesene Warteschlangen = %d
 zugewiesene Semaphoren = %d
 ID schon entfernt Schlüssel schon entfernt att_time = %-26.24s
 verbunden ungültiges Argument fehlerhafte Daten in der Spur / des Kopfes %u/%u
Setze fort …  Die Länge des Dateinamens ist unzulässig ungültige Inode-Position ungültige Inode-Größe Ungültige magische Zahl im Superblock falsche Antwortlänge Ungültige Root-Position (%lu) Unzulässiger Wert im Feld »s_imap_block« im Superblock Unzulässiger Wert im Feld »s_zmap_block« im Superblock falsche Geschwindigkeit: %s ungültiger Auszeit-Wert: %s ungültige V2-Inode-Größe Fehler beim Einlesen des Wertes für die Anzahl schlechter Böcke in Zeile %d
 groß die Blockgröße ist geringer als die physische Sektorgröße %s „blocks“-Argument zu groß, Maximum ist %llu unsinniger Modus: %s (%o) von MILO gebootet
 Bytes Bytes/Sektor neuer Prozess kann nicht erzeugt werden
 Kein Zugriff auf %s Überprüfen von %s fehlgeschlagen: fsck.%s nicht gefunden Datei %s kann nicht geschlossen werden Das Verzeichnis %s kann nicht erzeugt werden das Programm kann nicht zum Hintergrundprozess werden es ist nicht möglich, die Größe von %s festzustellen Gerät für %s konnte nicht gefunden werden Es kann kein neuer Prozess erzeugt werden (fork) es ist nicht möglich, die Größe von %s festzustellen Terminalattribute für %s konnten nicht ermittelt werden %s kann nicht geöffnet werden %s kann nicht geöffnet werden: %s Lesen nicht möglich %s kann nicht gelesen werden Positionieren unmöglich Die Line-Disciplin kann nicht gesetzt werden Terminalattribute für %s konnten nicht festgelegt werden %s kann nicht mit stat abgefragt werden Groß- bzw. Kleinbuchstaben (außer für Write). cgid Änderung Ändern des Partitionstyps GUID des Mediums ändern Einheiten der Anzeige/des Eintrages ändern ändert den Überlappungsfaktor Anzahl der alternativen Zylinder ändern Anzahl der Zylinder ändern die Anzahl der zusätzlichen Sektoren pro Zylinder ändern Anzahl der Köpfe ändern Anzahl der physischen Zylinder ändern Anzahl der Sektoren/Spuren ändern UUID der Partition ändern Partitionsname ändern Rotationsgeschwindigkeit (rpm) ändern Ändert die Medienkennung Änderungszeit = %-26.24s
 geändert Überprüfung abgebrochen.
 chown fehlgeschlagen: %s Uhrport auf 0x%x gestellt
 Schließen fehlgeschlagen verbinden Konnte den aktuellen Formatierungs-Typ nicht erkennen Größe des Geräts konnte nicht ausgelesen werden Das Verzeichnis %s konnte nicht gelesen werden SELinux-Kontext konnte nicht berechnet werden Passendes Dateisystem: %s kann nicht gefunden werden. cpid »crams«-Byte-Reihenfolge ist %s
 CRC-Fehler SGI-Information erzeugen neue leere DOS-Partitionstabelle erzeugen Erzeugen einer neuen leeren GPT-Partitionstabelle neue leere SGI-Partitionstabelle (IRIX) erzeugen neue leere Sun-Partitionstabelle erzeugen neue IRIX-Partitionstabelle (SGI) erzeugen Erzeugen der Nachrichtenwarteschlange ist fehlgeschlagen Semaphor-Erstellung ist fehlgeschlagen Erstellen gemeinsam genutzten Speichers ist fehlgeschlagen ctime = %-26.24s
 cuid Zylinder Zylinderabweichung Datenblock ist zu groß Die Zeichenkette %s entspricht %ld Sekunden seit 1969.
 Fehler beim Entpacken: %s Löschen einer Partition zerstört det_time = %-26.24s
 getrennt verschieden Verzeichnis-I-Node hat eine Position Null und eine Größe ungleich Null: %s Divisor »%s« Fertig
 Schreiben des Layouts des Mediums in die »sfdisk«-Datei Bootdatei Eintrag ändern Daten des Laufwerks ändern leere lange Option nach -l oder --long den geschützten/hybriden MBR eingeben Fehler %d (%m) beim Ausführen von fsck.%s für %s Fehler beim Schließen von %s Fehler beim Schreiben des „.“-Eintrags Fehler beim Schreiben des »..«-Eintrags Fehler beim Schreiben der Inode Fehler beim Schreiben der Root-Inode Fehler beim Schreiben des Superblocks Fehler: %s: Initialisierung der Suche ist gescheitert Fehler: uname fehlgeschlagen übermäßig langes Zeilenargument »exec« fehlgeschlagen
 Eine Zahl wurde erwartet, aber »%s« erhalten extra Funktionalität (nur für Experten) fehlgeschlagen Partition hinzufügen ist gescheitert der Iterator konnte nicht zugewiesen werden Das Zuweisen des libfdisk Kontextes ist fehlgeschlagen Ausgabezeile konnte nicht zugewiesen werden Ausgabetabelle konnte nicht zugewiesen werden Bezeichnung des Mediums konnte nicht erzeugt werden %s konnte nicht ausgeführt werden Attribute für PID %d können nicht erhalten werden Regeln für PID %d können nicht erhalten werden PID konnte nicht eingelesen werden Priorität konnte nicht eingelesen werden Partitionen konnten nicht gelesen werden Lesen der Verknüpfung fehlgeschlagen: %s Lesen der Timing-Datei %s ist fehlgeschlagen Lesen der Mitschnittdatei %s ist fehlgeschlagen Entfernen der Partition ist fehlgeschlagen Festlegen der Regel für PID %d ist fehlgeschlagen Das Einrichten der Beschreibung für %s ist fehlgeschlagen Medienbezeichnung konnte nicht geschrieben werden FIFO hat Größe ungleich Null: %s Die Datei überschreitet das Ende des Dateisystems Datei-Inode hat keine Position und eine Länge ungleich Null Datei-Inode hat eine Größe von Null und eine Position ungleich Null Dateilänge ist zu kurz die Länge des Dateinamens ist Null Das Dateisystem ist zu groß. Beenden. Partitionsreihenfolge reparieren Puffer leeren »fork« fehlgeschlagen fork() fehlgeschlagen, später versuchen
 von %.*s
 Dateisystemname ist zu lang voll „funky TOY“!
 Anzahl der 32-Bit-Sektoren ermitteln (veraltet, stattdessen --getsz verwenden) Ausrichtungs-Versatz in Bytes bekommen Blockgröße ermitteln Unterstützungs-Status für verwerfende Nullen bekommen Readahead für Dateisystem ermitteln logische Block- bzw. Sektorgröße ermitteln Maximale Anzahl an Sektoren pro Anforderung ermitteln minimale E/A-Größe ermitteln optimale E/A-Größe ermitteln physische Block- bzw. Sektorgröße ermitteln Nur-Lesen-Status ermitteln Readahead ermitteln Größe in Byte ermitteln gid Kopfwechsel Angegebene Klassendaten für idle-Klasse werden ignoriert Angegebene Klassendaten für none-Klasse werden ignoriert falscher Wert für den Tag: benutzen Sie 1-%d falscher Wert für den Monat: benutzen Sie 1-12 unvollständiger Schreibvorgang nach »%s« (%zd geschrieben, %zd erwartet)
 Booteinrichtung installieren Interleave interner Fehler Interner Fehler, kontaktieren Sie den Autor. interner Fehler: nichtunterstützer Dialog Typ %d ungültiges Argument – von ungültiges Argument – repariere ungültiges Argument – an ungültiges Argument von -r ungültiges Argument von -r: %d ungültige Block-Anzahl ungültige Angabe für die Blockgröße ungültige Angabe für die Zylinder ungültiges Teilungs-Argument ungültige Berabeitungsnummer ungültige Byte-Reihenfolge; es muss »groß«, »klein« oder »Host« sein ungültige Position der Dateidaten ungültige Angabe für die Köpfe ungültige Kennung ungültiger Schlüssel ungültiges Längen-Argument ungültiger Längenwert festgelegt ungültige Anzahl von Inodes ungültiger Positionswert festgelegt ungültige Option ungültige Partitionsnummer ungültige Angabe für die Größe der Sektoren ungültige Angabe für die Sektoren ungültiges Start-Argument »ioctl«-Fehler auf %s ioctl fehlgeschlagen: die Größe des Gerätes kann nicht bestimmt werden: %s »ioctl(%s)« war erfolgreich.
 ioctl() auf %s, um Update-Interrupts abzuschalten, fehlgeschlagen. ioctl() auf %s, um Update-Interrupts einzuschalten, fehlgeschlagen. ioctl(RTC_EPOCH_READ) auf %s fehlgeschlagen ioctl(RTC_EPOCH_SET) auf %s fehlt ioprio_get fehlgeschlagen ioprio_set fehlgeschlagen Kernel ist nicht für Nachrichtenwarteschlangen konfiguriert
 Kernel nicht für Semaphoren konfiguriert
 Der Kernel ist nicht konfiguriert, gemeinsamen Speicher zu unterstützen.
 Schlüssel last-changed last-op lchown ist fehlgeschlagen: %s verknüpft eine BSD Partition mit einer nicht-BSD Partition freien unpartitionierten Speicherplatz auflisten Auflisten bekannter Partitionstypen klein Laden des Layouts des Mediums aus der »sfdisk«-Datei lokale Zeit gesperrt Anmeldung:  lpid lrpid lspid maximale Anzahl von Feldern = %d
 maximale Operationen pro Semaphorenaufruf = %d
 maximale systemweite Warteschlangen = %d
 maximale Semaphoren pro Feld = %d
 maximale systemweite Semaphoren = %d
 Nachrichten fehlendes Optionszeichenkettenargument mkdir ist fehlgeschlagen: %s mknod fehlgeschlagen: %s mode=%#o	access_perms=%#o
 mode=%#o, access_perms=%#o
 Einhängetabelle ist voll mount: %s enthält keine SELinux-Labels.
       Sie haben gerade auf einem SELinux-Rechner ein Dateisystem eingehängt,
       das Labels unterstützt, aber keine enthält. Es ist wahrscheinlich, dass
       Anwendungen mit entsprechenden Einschränkungen AVC-Meldungen erzeugen und
       den Zugriff auf dieses Dateisystem verweigern. Weitere Details hierzu
       finden Sie in restorecon(8) und mount(8).
 den Beginn der Partition verschieben msqid Sie müssen Root sein um nach passenden Dateisystemen: %s zu suchen. N
 namelen=%zd

 nattch ncount für interaktive Reparaturen wird ein Terminal gebraucht nein keine Bezeichnung,  kein Längenwert festgelegt keine UUID
 Nicht-Block (%ld) Bytes Nicht-Größe (%ld statt %ld) Bytes kein kein blockorientiertes Gerät oder Datei: %s es wurde nicht genügend Speicher für das ROM Abbild zugewiesen (%lld zugewiesen, %zu benötigt) nicht genügend Platz, es werden wenigstens %llu Blöcke benötigt Es wurde kein Treiber für DOS Bezeichner gefunden nsems altes »cramfs«-Format auf %.*s
 Vorgang %d, eintreffende Anzahl = %d
 otime = %-26.24s
 Besitzer neben Auswertungsfehler: %s über erste Zeile Keine Berechtigung für Kennung Keine Berechtigung für Schlüssel Rechte Pid Partitionsinformationen drucken Anzeige der Partitionstabelle Anzeige der Rohdaten des Mediumnamens des Gerätes Anzeige der Rohdaten des ersten Sektors des Gerätes Anzeige dieses Menüs Beenden ohne die Änderungen zu speichern Empfangszeit = %-26.24s
 Lesezählwert Lesefehler in %s Das Lesen von »romfs« fehlgeschlagen Empfang Partitionstabelle erneut einlesen Ressource(n) gelöscht
 Ausgabe des »date«-Befehls = %s
 Zurück von BSD zu DOS vom gesicherten/hybriden MBR zur GPT zurückkehren Zurück ins Hauptmenü Wurzel-I-Node ist kein Verzeichnis »root inode« ist kein Verzeichnis U/min gleich gespeichert Sektoranzahl: %d, Sektorgröße: %d
 Sektoren/Zylinder Sektoren/Spur Suchfehler Suchfehler in %s »seek« fehlgeschlagen. Suchen in bad_zone fehlgeschlagen Suche fehlgeschlagen in check_blocks »seek« fehlgeschlagen in write_block »seek« fehlgeschlagen in write_super_block positionieren auf %s misslungen bootfähige Partition auswählen wähle sgi Swap Partition select() auf %s, um auf Zeittick zu warten, fehlgeschlagen. SemID Semnum Versand Sendezeit = %-26.24s
 Readahead (vorausschauendes Lesen) für Dateisystem festlegen auf Nur-Lesen festlegen auf Lesen-Schreiben festlegen Readahead (vorausschauendes Lesen) festlegen settimeofday() fehlgeschlagen setuid() fehlgeschlagen shmid vollständige Bezeichnung des Mediums anzeigen Größenfehler in symbolischer Verknüpfung: %s Socket Socket hat Größe ungleich Null: %s die Spezialdatei hat eine Position ungleich Null: %s Geschwindigkeit %d wird nicht unterstützt Status erfolgreich Superblock-Kennung wurde nicht gefunden Die Größe des Superblocks (%d) ist zu gering die symbolische Verknüpfung hat eine Position von Null die symbolische Verknüpfung hat eine Größe von Null Anlegen der symbolischen Verknüpfung scheiterte: %s Die Option -l kann nur auf einem Gerät angewendet werden -- ignorieren Timings-Datei %s: %lu: unerwartetes Format das «bootfähig«-Kennzeichen ändern Schalte die GUID spezifischen Bits Schalte den DOS-Kompatibilitäts-Schalter den veralteten »BIOS bootfähig«- Schalter wählen den »einhängbar«- Schalter wählen Schalte die »no block IO protocol flag« den »nur-lesbar«- Schalter wählen den benötigten Partitionsschalter wählen zu viele alternative Geschwindigkeiten zu viele Argumente zu viele Geräte zu viele Inodes – Maximum ist 512 gesamt Spur‐zu‐Spur‐Wechsel Spuren/Zylinder Spurabweichung Probleme beim Lesen der terminfo-Datenbank uid Puffer für Superblock konnte nicht zugewiesen werden kein Speicherplatz für neue libblkid-Probe verfügbar neuer SELinux-Kontext kann nicht angelegt werden Bootsektor kann nicht gelöscht werden matchpathcon() kann nicht durchgeführt werden Superblock konnte nicht gelesen werden »%s« konnte nicht aufgelöst werden Auslagerungsgerät kann nicht zurückgespult werden CRC kann nicht getestet werden: altes Cramfs-Format Superblock konnte nicht geschrieben werden Unerwartetes Dateiende in »%s« unbekannt unbekannter Kombatibilitätsmodus »%s« Unbekannte Shell als Argument von -s oder --shell unshare fehlgeschlagen nicht unterstützter Farbmodus nicht unterstützte Mediumbezeichnung: %s nicht unterstützte Dateisystem-Eigenschaften Aufruf: %s [-h] [-v] [-b Blockgröße] [-e Ausgabe] [-N Byte-Reihenfolge] [-i Datei] [-n Name] Verzeichnisname für Ausgabedatei
 -h             diese Hilfe ausgeben
 -v             ausführliche Meldungen
 -E             alle Warnungen zu Fehlern machen (Beendigungsstatus ungleich Null)
 -b Blockgröße  Diese Blockgröße verwenden, muss gleich der Seitengröße sein
 -e Ausgabe     Ausgabenummer festlegen (Teil der fsid)
 -i Datei       ein Dateiabbild in das Dateisystem einfügen
                (benötigt Kernel >= 2.4.0)
 -n Name        Name des Cramfs-Dateisystems festlegen
 -p             Mit %d Bytes für Boot-Code auffüllen
 -s             Verzeichniseinträge sortieren (alte Option, ignoriert)
 -z             Explizite Löcher erzeugen (benötigt Kernel >= 2.3.39)
 dirname        Wurzel des zu komprimierenden Dateisystems
 outfile        Ausgabedatei
 benutzte Felder = %d
 benutzte Köpfe = %d
 Benutzt-Bytes das gewählte Endtrack überschreitet das Maximum des Mediums das gewählte Starttrack überschreitet das Maximum des Mediums das gewählte Starttrack überschreitet das gewählte Endtrack utime ist fehlgeschlagen: %s Wert Überprüfen der Partitionstabelle Datenträgername ist zu lang Warten: keine weiteren Kinderprozesse?!? waitpid ist fehlgeschlagen Warnung: Dateigrößen wurden auf %luMB (minus 1 Byte) verkürzt. Warnung: Die Dateinamen wurden auf %u Bytes gekürzt Warnung: Dateien wurden wegen Fehlern übersprungen. Warnung: gids wurden auf %u Bits verkürzt. (Das kann ein Sicherheitsproblem sein.) Warnung: uids wurden auf %u Bits verkürzt. (Das kann ein Sicherheitsproblem sein.) Epoche %ld aus %s mit dem RTC_EPOCH_READ-Ioctl wurde gelesen.
 schreiben Schreiben fehlgeschlagen Schreiben fehlgeschlagen: %s Tabelle auf das Medium schreiben Tabelle auf das Medium schreiben und beenden Schreiben auf die Standardausgabe fehlgeschlagen J
 ja zcount Dateizähler ist Null 