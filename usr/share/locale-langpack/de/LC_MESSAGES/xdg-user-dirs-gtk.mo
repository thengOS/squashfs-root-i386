��          |      �             !     5  -   E  '   s  3   �  ,   �     �  �        �     �     �  �       �     �  4   �  8     J   :  6   �  '   �  �   �     �     �     �                            
      	                         Current folder name New folder name Note that existing content will not be moved. There was an error updating the folders Update common folders names to match current locale Update standard folders to current language? User folders update You have logged in in a new language. You can automatically update the names of some standard folders in your home folder to match this language. The update would change the following folders: _Don't ask me this again _Keep Old Names _Update Names Project-Id-Version: xdg-user-dirs-gtk HEAD
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-04 03:00+0000
PO-Revision-Date: 2008-10-14 00:38+0000
Last-Translator: Andre Klapper <a9016009@gmx.de>
Language-Team: German <gnome-de@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:21+0000
X-Generator: Launchpad (build 18115)
 Aktueller Ordnername Neuer Ordnername Hinweis: Bestehende Inhalte werden nicht verschoben. Beim Aktualisieren der Ordner ist ein Fehler aufgetreten Namen der Standardordner auf die momentan verwendete Sprache aktualisieren Standardordner auf die aktuelle Sprache aktualisieren? Aktualisierung der Ordner des Benutzers Sie haben sich unter einer neuen Sprache angemeldet. Sie können automatisch alle Standardordner in Ihrem persönlichen Verzeichnis an diese Sprache anpassen lassen. Dies würde die folgenden Ordner betreffen: _Nicht erneut nachfragen Alte Namen _beibehalten Namen a_ktualisieren 