��    �     �  �  /      �>  ?   �>     !?  4   A?     v?  3   �?     �?  &   �?      @  '   @     =@     P@     e@  x   �@  H   �@  m   GA  =   �A  {   �A  K   oB  p   �B  -   ,C  #   ZC  2   ~C     �C  '   �C  2   �C     *D     HD     ]D     yD  $   �D     �D     �D  0   �D  0   �D     /E     CE     ]E     iE     {E  
   �E  
   �E     �E     �E  "   �E     �E     �E     
F     F  ,   +F     XF     jF  5   yF     �F  #   �F  M   �F  N   -G     |G  <   �G  S   �G  B   -H     pH      �H     �H  %   �H     �H     �H     I  *   I     II     cI  <   }I  *   �I     �I     �I     J     /J     ?J     TJ  %   mJ      �J     �J     �J  (   �J     	K  %   !K  &   GK     nK      �K     �K     �K     �K     �K     �K     L     L  &   5L  $   \L     �L     �L     �L     �L     �L     �L  !   
M     ,M  	   DM  
   NM     YM  +   pM  :   �M  /   �M  (   N  '   0N  $   XN     }N  3   �N  &   �N     �N  9   	O  9   CO  4   }O  &   �O     �O  )   �O     "P  ,   AP  K   nP  >   �P  s   �P     mQ  2   �Q  '   �Q     �Q     �Q     R     /R     ;R     IR     [R     sR  #   �R  $   �R     �R     �R     �R     S     )S     ;S     WS  "   fS     �S     �S     �S     �S     �S     �S  $   �S  $   T     <T  2   DT  #   wT  2   �T  #   �T     �T  	   U  -   U  =   CU  7   �U  5   �U     �U  O   �U  /   CV      sV     �V  "   �V     �V  3   �V  7   W  5   OW     �W  3   �W     �W      �W  1   	X  4   ;X  %   pX  :   �X  )   �X  (   �X  %   $Y     JY     hY     �Y     �Y  A   �Y  	   �Y     �Y  )   �Y  3   Z  ,   SZ  2   �Z  3   �Z      �Z  )   [     2[     E[  	   V[  /   `[  /   �[  9   �[  @   �[     ;\     L\     i\  "   p\  4   �\  5   �\     �\     ]  7   )]     a]  1   q]  <   �]  5   �]  -   ^  >   D^  7   �^  $   �^  $   �^  >   _  M   D_  /   �_  a   �_     $`  '   C`  "   k`  :   �`  M   �`  W   a     oa  $   ua     �a     �a     �a  A   �a     /b  "   Ob     rb  5   �b     �b  7   �b  B   	c  R   Lc  !   �c  1   �c     �c  9   d  A   Md  $   �d  :   �d  $   �d  W   e  /   le  0   �e  %   �e  )   �e  $   f  .   Bf     qf     �f  %   �f     �f     �f     �f  %   �f  %   g     Dg     Yg     bg     ng     �g     �g     �g     �g  2   �g  +   h     Jh  0   gh  )   �h  5   �h  E   �h  W   >i  '   �i  =   �i  +   �i  /   (j  4   Xj     �j  /   �j  A   �j  E   k  3   \k  4   �k  +   �k  #   �k  6   l  :   Ll  *   �l     �l  #   �l  >   �l  <   .m  6   km     �m  V   �m  ?   n  5   Ln  8   �n  3   �n  J   �n     :o  *   @o  $   ko  >   �o  +   �o     �o     p     p     3p     Op     np     ~p  S   �p  +   �p     q  	   $q  8   .q  #   gq  6   �q  =   �q      r     r  /   #r  &   Sr  I   zr     �r     �r  '   �r     s     "s     /s     Ds     Us     rs     �s  k   �s     t     1t  :   Ot  '   �t     �t  M   �t  C   u  F   cu     �u  f   �u  >   1v  @   pv  S   �v  B   w  1   Hw  '   zw     �w  
   �w     �w     �w     �w     �w     x     x  #   6x     Zx  '   ix  +   �x      �x     �x     �x     y  &   &y  2   My  2   �y  8   �y  (   �y  *   z     @z  &   ]z     �z     �z  "   �z  %   �z  "   �z      !{  8   B{  P   {{  *   �{  2   �{  /   *|     Z|     b|     z|     �|     �|     �|     �|  P   }  ^   \}     �}     �}  7   �}  ,   ~  ?   G~  1   �~  5   �~     �~  '     "   -     P  0   o  +   �  1   �  4   �  1   3�  B   e�  ?   ��     �     ��     �     2�      L�  %   m�  (   ��  P   ��     �     "�     6�     E�  C   ]�  M   ��  U   �  M   E�  U   ��  I   �  I   3�     }�     ��  B   ��     ܄     �     ��     �     7�     S�  /   [�     ��  !   ��     ȅ     ޅ  X   ��     R�  ;   h�  4   ��  X   ن  :   2�  3   m�     ��  ]   ��  9   �     G�  "   _�  !   ��  0   ��  2   Ո     �     $�     2�     I�  0   W�  3   ��     ��     Љ  (   �     �  )   '�  D   Q�  m   ��  m   �     r�  .   ��  
   ��     ��  #   ҋ     ��  	   ��  #   �  	   *�  
   4�  !   ?�     a�  =   {�  ;   ��  )   ��     �     ;�     X�  6   x�  :   ��     �     �     ��  (   �  7   /�  N   g�  #   ��     ڎ  +   �     �     :�     A�     P�     e�      v�  9   ��     я     �     ��      �  5   8�     n�  0   ~�  (   ��  -   ؐ  >   �  ,   E�  +   r�  .   ��  .   ͑  0   ��     -�  )   >�  &   h�     ��  '   ��  '   Ӓ  3   ��  *   /�     Z�     o�  *   ��  [   ��  &   �  )   :�  &   d�  @   ��  ?   ̔  5   �  "   B�  K   e�  
   ��     ��  	   ȕ     ҕ     �     ��  &   ��  L   &�     s�  I   ��  )   ܖ  Z   �  %   a�  3   ��  (   ��  
   �     �  A   ��  H   7�  6   ��  6   ��  8   �  6   '�  6   ^�     ��     ��  #   ��     ݙ     ��  '   ��  4   &�  +   [�  3   ��  B   ��     ��  +   �     :�     U�     p�     ��     ��     ��     Λ     �     ��     �     �     (�     D�     `�     |�     ��     ��     Ü     Ȝ  !   Μ  ,   �  S   �     q�     ��     ��  �   ��  [   ?�  #   ��     ��  .   ؞  .   �     6�     S�     g�  /   ��  8   ��  ;   �  0   %�     V�  $   o�     ��  
   ��  &   ��  #   �  /   �  (   6�  `   _�  ,   ��      ��     �     �     #�     6�     I�     _�     u�  /   ��     ��     Ţ  <   ˢ  C   �  .   L�  ,   {�  /   ��  !   أ  .   ��     )�  4   A�  0   v�     ��  5   ��  ?   ��  *   6�  !   a�  1   ��  1   ��  =   �  -   %�  7   S�  (   ��     ��  ,   Ԧ  )   �  <   +�     h�     ��  &   ��  &   ��     �     ��  '   �  *   B�     m�  <   ��  <   ��  7   ��     4�     O�  &   h�  %   ��  ,   ��  G   �     *�     B�  4   a�  6   ��     ͪ     ��     �  5   "�     X�  <   q�  C   ��     �  '   �  *   :�  /   e�  P   ��  K   �  K   2�  (   ~�  <   ��  |   �  >   a�  ?   ��  0   �     �  <   �  ,   U�     ��  "   ��  G   ï     �     �     >�     ]�     e�  0   q�     ��  +   ��  =   �  #   '�  J   K�  "   ��  I   ��     �      �     :�  �  U�  i   $�     ��  G   ��     ��  D   �     S�  ,   k�     ��  1   ��     �  !   ��  "   !�  �   D�  h   �  �   X�  [   ��  �   R�  q   �  �   x�  9   �  !   Y�  3   {�  "   ��  /   Һ  C   �  %   F�     l�  *   ��     ��  2   ͻ      �     	�  6   �  :   L�     ��      ��     ��     ̼     �  
   ��  
   �  !   �  "   1�  .   T�     ��     ��     ��     ��  2   ˽     ��     �  C   )�     m�     z�  ^   ��  ^   ��  ,   X�  D   ��  �   ʿ  W   M�     ��  :   ��     ��  /   �     <�     T�     p�  '   ��     ��     ��  @   ��  .   &�     U�  '   e�  )   ��     ��     ��     ��  *   ��  +   )�  &   U�     |�  2   ��  (   ��  +   ��  9   �  '   Q�  +   y�     ��  '   ��     ��     ��     ��     �  .   ,�  ;   [�  ;   ��     ��  %   ��  $   �  *   +�      V�     w�  /   ��  *   ��  	   ��  
   ��     �  ;   �  O   U�  D   ��  *   ��  -   �  ,   C�     p�  9   ��  -   ��     ��  C   	�  B   M�  ?   ��  +   ��     ��  0   �  %   M�  I   s�  l   ��  W   *�  �   ��  $   "�  F   G�  ;   ��     ��     ��     �      �     /�     @�  !   U�     w�  *   ��  5   ��     ��      �  $   �     =�     [�  $   o�     ��  )   ��     ��     ��      ��     �     �      *�  %   K�  I   q�     ��  >   ��  '   �  B   *�  +   m�     ��  	   ��  <   ��  W   ��  >   W�  9   ��     ��  V   ��  3   +�  1   _�  $   ��  '   ��     ��  C   ��  @   8�  ?   y�  #   ��  Q   ��  &   /�  2   V�  J   ��  <   ��  )   �  9   ;�  .   u�  6   ��  6   ��  $   �  "   7�     Z�     ]�  U   `�     ��  #   ��  5   ��  ?   �  8   _�  >   ��  ?   ��  (   �  1   @�     r�  !   ��  	   ��  L   ��  >    �  D   ?�  Q   ��     ��     ��  
   �  (   �  ]   C�  Z   ��     ��     �  T   (�     }�  I   ��  T   ��  v   /�  [   ��  T   �  M   W�  6   ��  /   ��  Z   �  i   g�  @   ��  n   �  8   ��  0   ��  +   ��  K   �  `   c�  b   ��     '�  6   -�  .   d�  0   ��  #   ��  k   ��     T�  $   t�     ��  A   ��  1   ��  H   "�  S   k�  i   ��  /   )�  C   Y�  :   ��  |   ��  e   U�  5   ��  O   ��  8   A�  v   z�  ?   ��  @   1�  E   r�  @   ��  B   ��  7   <�     t�     ��  5   ��     ��     ��     �  -   "�  -   P�     ~�  
   ��     ��  ,   ��  $   ��  &   ��     $�  $   B�  D   g�  >   ��  (   ��  D   �  >   Y�  C   ��  d   ��  r   A�  =   ��  M   ��  >   @�  A   �  G   ��  #   	�  I   -�  Y   w�  Y   ��  D   +�  N   p�  F   ��  0   �  N   7�  F   ��  F   ��  (   �  6   =�  W   t�  R   ��  G   �  ,   g�  ~   ��  V   �  F   j�  C   ��  N   ��  r   D�     ��  (   ��      ��  5   �  ;   C�  $   �     ��  #   ��  *   ��     ��     �  !   &�  i   H�  F   ��     ��     �  O   �  /   b�  Q   ��  S   ��     8�     J�  /   a�  /   ��  r   ��     4�     J�  5   `�     ��     ��     ��     ��  %   ��      �  )   6�  �   `�      ��  .   �  >   1�  9   p�     ��  i   ��  O   0�  Q   ��  3   ��  v   �  J   }�  Q   ��  p   �  Q   ��  :   ��  .   �     G�     M�     [�     l�     ��     ��     ��  #   ��  /   ��     (  >   ?  5   ~  &   �     �     �      3   8 ?   l L   � O   � 6   I >   �    � >   �     *   2 .   ] 3   � +   �     � F    `   T 3   � @   � H   *    s $    *   � )   � ,   � 2   & 1   Y s   � �   � "   �    � N   � :    B   C F   � :   �    	 ,   	 %   L	 "   r	 6   �	 A   �	 >   
 =   M
 @   �
 `   �
 S   -    � !   �    �    � 5   � &   0 5   W R   �    �    �        " \   ? g   � |    g   � |   � m   f m   �    B    S J   `    � "   � ,   � %    ,   -    Z A   b ;   � :   �         6 �   W    � M   � :   > �   y \    I   b    � k   � H   *    s %   � ,   � F   � D   +    p    �    �    � C   � =       E    X (   v     � *   � K   � �   7 �   �    j N   ~    �    � -   � 	   +    5 ,   E    r    �    �    � [   � ^   - <   �     �    � 2   
 L   = O   �    � 
   �    � +    F   : `   � +   � !    ;   0 )   l    �    �    �    � 4   � [    )   s    � *   � -   � I        M  =   `  :   �  G   �  e   !! 5   �! ,   �! 1   �! 3   " ;   P"    �" :   �" G   �" )   *# 5   T# 2   �# =   �# $   �#     $ (   >$ 9   g$ k   �$ 2   % 9   @% =   z% S   �% S   & I   `& ,   �& a   �&    9'    P'    _' )   m'    �'    �' 9   �' t   �' 2   i( _   �( >   �( y   ;) 7   �) <   �) 2   **    ]*    r* M   x* V   �* =   + =   [+ P   �+ T   �+ ;   ?,    {, -   �, +   �,     �,    �, E   - P   H- >   �- M   �- \   &.    �. (   �. +   �.    �.    /    /    </    [/    m/    �/    �/    �/ #   �/ )   �/ (   0 ,   @0 2   m0 1   �0    �0    �0    �0 !   �0 )   1 g   81    �1 #   �1    �1 �   �1 h   �2 )   �2    #3 A   C3 A   �3    �3    �3    �3 <   4 I   S4 I   �4 ;   �4    #5 &   ?5     f5    �5 '   �5 %   �5 9   �5 )   6 �   B6 E   �6 9   7    K7    `7    l7    7    �7    �7    �7 5   �7    8    8 M   "8 L   p8 =   �8 7   �8 I   39 1   }9 ?   �9     �9 ?   : >   P: "   �: C   �: L   �: 5   C; +   y; K   �; I   �; N   ;< <   �< O   �< 5   = +   M= 9   y= >   �= Z   �= (   M> $   v> ?   �> 2   �> #   ? +   2? 7   ^? @   �? !   �? V   �? W   P@ H   �@ )   �@ "   A 5   >A 4   tA ?   �A m   �A /   WB 7   �B C   �B J   C .   NC +   }C 0   �C I   �C #   $D O   HD ]   �D .   �D 7   %E K   ]E <   �E i   �E d   PF `   �F .   G E   EG �   �G e   H L   wH 4   �H    �H I   I *   LI 1   wI ;   �I `   �I    FJ $   aJ 0   �J    �J    �J A   �J (   K ?   @K T   �K 5   �K ^   L 5   jL ^   �L )   �L 5   )M '   _M    �  �      �      Y  W          �   #  h          R   G   �   "   a      �  �  �           N  �      �  �  �   k               ]  �   �          �    t     �        �   -   �      �   �  ^  '      �  �  K  ~   �      L      �  �   �   I   e    @        7  P  ^   �  �      �  9             �      \  �   �  h  �   �   Z   �   �      g             �  ]   �   �   s  �       �  C  w  d      l  �  8  �      q  �   �   �      i  �    <  �  �  9  �          �   �  :  �  Z  �   8  n  �        �  �  �  <   H    =  �  �       �      �   F  f   �  �  �  )     �  �  �  4  �   �      �   �   �   B   )      �      �  >  E  ~  �   �              Y     ]  �  F   1  c  �      �   �      u   �             Q           �   {  �        	   �    �   8   U  (                      �               w  �    �   �  �   �  �   V  >   n     �  �      �   �  �  +   d           :       �   �       2      G  �   �      �                  �  �  @   �  M  9  `      �  i  v          s          1  j  W      �   �  �   ~  �  �  �  x  I  �      �  m     �  b   r    N               �   �      �   [  _      t   �  .      }             z           �  �  �      �       �  �               �  }      �   �  {   �   �   �  �                  �  �   
   �  �      �      �  4  �     "      �   2  L   �  f  r      �    �  #   �  E     �      s      �        �        �  �      |  �   '  Y      �       �      �      �  N              0   P  l  V   �      �  �   �      o  �  &  �       $  r  �                      �  d      J   �  �   3  �   �       |   �                   [   �          G  �   �   /       �       �   0  q       �  �  7  �      H       �   S      u  �              �      �     �  �  a       T   m      �   �      �    k      p  �    L  �  �   J  �  3      �      ,  �   �  v  �  �  @  �          �   o      �  �  �  ;  �     1   �     *          �     c            �      _    �   ?  �  �  �  �          	  �          �   �   �  �         �   �  :  *  (   �  �   #  %  �   �   x   Q  �   �  �   �          �          "              �  �    n       2       �  \  !       �      *  �    �       �   y  p                  D  �   `       �   �  �          z  X      �   �   �   �     R  �      Z      ,   �      =  
      A   �   '   �      $   �  T  -         W  V      ;                      %   u  �  e  p   �             y   �   T  �  P       �  �      �  �    z      �   <  �   �  %  �   $  q  �  x  7   +  �           (  �  t  �  �   �  �      �    �          �  l           �   ?   `      �          �      6  �  �      �   �  �  �  �     �   k  �  b     R        �    �   !  �           X   b  �   �  
  �  A  O  O   y  J  O                            �      U        >  _   &   �           6   �    �  �                �     �   �  �   j  m   D              �       .               M  \     �  �         �    |  a  C  Q      �   �     �  E  �   �  �  g      �           K  C   �  ,  c           �  �       +      F  -  �  �   �  ^  �  �      I             �       5  �         �      �   �  6  �  �  �   ;       �         w   �      j   �   �  �  �   �  �  U   f      �   �    �   �  .       0  �      {  /      i   �  !  }  �  M       �      �  �      h       B      H      )     	    K       �   �  �  �   D  [              4   �  �  �  S      �   �  =      �  �  �  �   �    �   �   �   &  �  �          o   �  X  �    A  5      �      �       B      �  /  5               S  �      ?  �            �       �   v   3           g  �      �  e   �   	here may be unidentified media objects not yet fully restored
 

--------- fstab ------------
 
        may be additional unidentified media files
 
        media file %u 
    may be additional unidentified media objects

 
    media object %u:

 
    media objects not yet identified
 
examine this dump?
 
interactively restore from this dump?
 
media stream %u:
 
restore this dump?
 
session interrupt in progress
 
there are additional unidentified media objects containing media files not yet tried for directory hierarchy restoral:
 
there are additional unidentified media objects not yet fully restored
 
there are unidentified media objects containing media files not yet tried for directory hierarchy restoral:
 
there are unidentified media objects not yet fully restored
 
there may be additional unidentified media objects containing media files not yet tried for directory hierarchy restoral:
 
there may be additional unidentified media objects not yet fully restored
 
there may be unidentified media objects containing media files not yet tried for directory hierarchy restoral:
         contains no selected non-directories
         contains session inventory
         first extent contained: ino %llu off %lld
         is stream terminator
         media files not yet identified
         next extent to restore: ino %llu off %lld
         non-directories done
         now reading
         rollback mark %lld
         size: %lld bytes
         used for directory restoral
     id:      id: %s
     index within object of first media file: %u
     index within stream of first media file: %u
     label is blank
     label not identified
     label:      now in drive
     now in drive %u
  (current)  (default)  (timeout in %u sec)  (timeout in %u sec)
  - type %s for status and control
  resumed %2u. label:  %d second intervals
 %d seconds
 %llu directories and %llu entries processed
 %s (offset %lld)
 %s above root
 %s already exists: rm -rf prior to initating restore
 %s created
 %s does not identify a file system
 %s extended attribute name for %s ino %llu too long: %u, max is %u: skipping
 %s extended attribute value for %s ino %llu too long: %u, max is %u: skipping
 %s failed with exit status: %d
 %s format corrupt or wrong endianness (0x%x, expected 0x%x)
 %s format differs from previous restore due to page size change (was %lu, now %lu)
 %s format version differs from previous restore (%u, expected %u)
 %s is not a directory
 %s must be mounted to be dumped
 %s not found
 %s quota information written to '%s'
 %s%s%s%s: drive %d:  %s:   stream %d %s %s (%s)
 %s: %s Summary:
 %s: -%c argument does not require a value
 %s: -%c argument invalid
 %s: -%c argument missing
 %s: -%c subsystem subargument %s requires a verbosity value
 %s: unknown file type: mode 0x%x ino %llu
 %s: usage: %s  (allow files to be excluded) (check tape record checksums) (contents only) (cumulative restore) (display dump inventory) (don't dump extended file attributes) (don't overwrite existing files) (don't overwrite if changed) (don't prompt) (don't restore extended file attributes) (don't timeout dialogs) (dump DMF dualstate files as offline) (force interrupted session completion) (force usage of minimal rmt) (generate tape record checksums) (help) (inhibit inventory update) (interactive) (overwrite tape) (pin down I/O buffers) (pre-erase media) (restore DMAPI event settings) (restore owner/group even if not root) (restore root dir owner/permissions) (resume) (show subsystem in messages) (show verbosity in messages) (skip unchanged directories) (timeout in %d sec) (timestamp messages) (unload media when change needed) (use small tree window) - (stdin) - (stdout) -%c allowed only once
 -%c and -%c option cannot be used together
 -%c and -%c valid only when initiating cumulative restore
 -%c and -%c valid only when initiating restore
 -%c argument %s (%s) is not a directory
 -%c argument %s is an invalid pathname
 -%c argument %s too long: max is %d
 -%c argument (%s) invalid
 -%c argument (subtree) must be a relative pathname
 -%c argument is not a valid file size
 -%c argument missing
 -%c argument must be a positive number (MB): ignoring %u
 -%c argument must be a positive number (Mb): ignoring %u
 -%c argument must be between %u and %u: ignoring %u
 -%c argument must be between 0 and %d
 -%c argument must be relative
 -%c argument not a valid dump session id
 -%c argument not a valid uuid
 -%c cannot reset flag from previous restore
 -%c option invalid: there is no interrupted restore to force completion of
 -%c option invalid: there is no interrupted restore to resume
 -%c option required to resume or -%c option required to force completion of previously interrupted restore session
 -%c unavailable: no /dev/tty
 -%c valid only when initiating cumulative restore
 -%c valid only when initiating restore
 <I/O buffer ring length> <alt. workspace dir> ... <base dump session id> <blocksize> <destination> <destination> ... <dump media file size>  <excluded subtree> ... <file> (restore only if newer than) <file> (use file mtime for dump time <level> <maximum file size> <maximum thread stack size> <media change alert program>  <media label> ... <minimum thread stack size> <options file> <seconds between progress reports> <session id> <session label> <source (mntpnt|device)> <source> ... <subtree> ... <use QIC tape settings> <verbosity {silent, verbose, trace}> Can't open %s for mount information
 Dev	%s
 Error in reading inventory record (lseek failed):  Error in reading inventory record : Error in writing inventory record (lseek failed):  Error in writing inventory record : FSF tape command failed
 FSid	%s

 Failed to allocate extended attribute buffer
 Failed to get data from media file: possible file corruption
 HSM could not add new %s attribute #%d for %s ino %llu
 HSM could not filter %s attribute %s for %s ino %llu
 I/O I/O metrics: %u by %s%s %sring; %lld/%lld (%.0lf%%) records streamed; %.0lfB/s
 INV : Unknown version %d - Expected version %d
 INV: Cant get inv-name for uuid
 INV: Check %d failed.
 INV: Check %d out of %d succeeded
 INV: Error in fstab
 INV: No recorded filesystems in inventory's fstab.
 INV: Only one of mnt=,dev= and fsid=value can be used.
 INV: Only one of mobjid= and mobjlabel= can be used.
 INV: Open failed on %s
 INV: cant create more than %d streams. Max'd out..
 INV: checking fs "%s"
 INV: couldn't get fstab headers
 INV: failed to find a different sh_time to split
 INV: idx_put_sesstime failed in inv_stream_close() 
 INV: inv_put_session: unknown cookie
 INV: inv_put_session: unknown packed inventory version %d
 INV: invalid sub-option %s for -I option
 INV: open failed on file system id "%s"
 INV: open failed on mount point "%s"
 INV: put_fstab_entry failed.
 INV: put_starttime failed.
 KB MB Minimal rmt cannot be used without specifying blocksize. Use -%c
 Mount	%s
 Overwrite command line option
 RMTCLOSE( %d ) returns %d: errno=%d (%s)
 RMTIOCTL( %d, %d, 0x%x ) returns %d: errno=%d (%s)
 RMTOPEN( %s, %d ) returns %d: errno=%d (%s)
 RMTREAD( %d, 0x%x, %u ) returns %d: errno=%d (%s)
 RMTWRITE( %d, 0x%x, %u ) returns %d: errno=%d (%s)
 SGI_FS_BULKSTAT failed: %s (%d)
 This tape was erased earlier by xfsdump.
 UNIX domain socket Unmark and quit
 WARNING:  WARNING: could not stat dirent %s ino %llu: %s
 WARNING: unable to open directory ino %llu: %s
 WARNING: unable to process dirent %s: ino %llu too large
 WARNING: unable to read dirents (%d) for directory ino %llu: %s
 XENIX named pipe abnormal dialog termination
 abort
 advancing tape to next media file
 all map windows in use. Check virtual memory limits
 all media files examined, none selected for restoral
 all of the above all subsystems selected

 assuming media is corrupt or contains non-xfsdump data
 at end of data
 attempt to access/open device %s failed: %d (%s)
 attempt to access/open remote tape drive %s failed: %d (%s)
 attempt to disown child when parent has no children!
 attempt to disown child which has no parent!
 attempt to get status of remote tape drive %s failed: %d (%s)
 attempt to get status of tape drive %s failed: %d (%s)
 attempt to link %s to %s failed: %s
 attempt to read %u bytes failed: %s
 attempt to reserve %lld bytes for %s using %s failed: %s (%d)
 attempt to seek %s to %lld failed: %s: not restoring extent off %lld sz %lld
 attempt to set DMI attributes of %s failed: %s
 attempt to set extended attributes (xflags 0x%x, extsize = 0x%x, projid = 0x%x) of %s failed: %s
 attempt to stat %s failed: %s
 attempt to truncate %s failed: %d (%s)
 attempt to truncate %s failed: %s
 attempt to write %u bytes to %s at offset %lld failed: %s
 attempt to write %u bytes to %s at offset %lld failed: only %d bytes written
 attr_multi indicates error while retrieving %s attribute [%s] for %s ino %llu: %s (%d)
 audio bad directory entry header checksum
 bad extattr header checksum
 bad extent header checksum
 bad file header checksum
 bad media file header at BOT indicates foreign or corrupted tape
 beginning inventory media file
 beginning media stream terminator
 block special file both /var/lib/xfsdump and /var/xfsdump exist - fatal
 can't lseek ino %llu
 can't read ino %llu at offset %d (act=%d req=%d) rt=%d
 cannot calculate incremental dump: online inventory not available
 cannot create %s thread for stream %u: too many child threads (max allowed is %d)
 cannot determine tape block size
 cannot determine tape block size after two tries
 cannot dump to %s file type %x
 cannot find earlier dump to base level %d increment upon
 cannot interleave dump streams: must supply a blank media object
 cannot open option file %s: %s (%d)
 cannot restore %s (ino %llu gen %u): pathname contains %s
 cannot restore from %s file type %x
 cannot select dump session %d as base for incremental dump: level must be less than %d
 cannot specify source files and stdin together
 cannot specify source files and stdout together
 cannot stat -%c argument %s (%s): %s
 cannot stat destination directory %s: %s
 cannot stat option file %s: %s (%d)
 change interval of or disable progress reports change media dialog change verbosity changing progress report interval to  char special file chdir %s failed: %s
 chmod %s failed: %s
 chown (uid=%d, gid=%d) %s failed: %s
 chown (uid=%u, gid=%u) %s failed: %s
 confirm media change continue continuing
 corrupt directory entry header
 corrupt extattr header
 corrupt extent header
 corrupt file header
 could not create %s: %s
 could not create directory attributes file %s: %s
 could not create name registry file %s: %s
 could not erase %s: %s (%d)
 could not find directory attributes file %s: %s
 could not find name registry file %s: %s
 could not find specified base dump (%s) in inventory
 could not forward space %d tape blocks: rval == %d, errno == %d (%s)
 could not forward space one tape block beyond read error: rval == %d, errno == %d (%s)
 could not fstat stdin (fd %d): %s (%d)
 could not get list of %s attributes for %s ino %llu: %s (%d)
 could not map persistent state file %s: %s
 could not map persistent state file hdr %s: %s
 could not map persistent state file inv %s: %s (%d)
 could not open %s: %s
 could not open ino %llu to read extent map: %s
 could not open regular file ino %llu mode 0x%08x: %s: not dumped
 could not open/create directory extended attributes file %s: %s (%d)
 could not open/create persistent state file %s: %s
 could not read extended attributes file %s: %s (%d)
 could not read extent map for ino %llu: %s
 could not read from drive: %s (%d)
 could not remap persistent state file inv %s: %d (%s)
 could not retrieve %s attributes for %s ino %llu: %s (%d)
 could not rewind %s in prep for erase: %s
 could not rewind %s: %s
 could not save first mark: %d (%s)
 could not seek to end of extended attributes file %s: %s (%d)
 could not seek to into extended attributes file %s: %s (%d)
 could not set access and modification times of %s: %s
 could not stat %s
 could not stat dirent %s ino %llu: %s: using null generation count in directory entry
 could not write at end of extended attributes file %s: %s (%d)
 could not write extended attributes file %s: %s (%d)
 creating dump session media file %u (media %u, file %u)
 cumulative restores must begin with a level 0 dump
 cumulative restores must begin with an initial (not resumed) level 0 dump
 debug destination directory %s invalid pathname
 destination directory not specified
 destination directory pathname too long: max is %d characters
 dioinfo %s failed: %s: discarding ino %llu
 dioinfo failed ino %llu
 dir directory post-processing
 disabling progress reports
 display media inventory status display metrics display needed media objects do not specify destination directory if contents only restore invoked (-%c option)
 don't know how to dump ino %llu: mode %08x
 drive %u drive %u  drive does not support media erase: ignoring -%c option
 dump complete: %ld seconds elapsed
 dump contains %llu inodes, restore can only handle %u
 dump contains too many dirents, restore can only handle %llu
 dump date: %s
 dump description: 
 dump interrupted prior to ino %llu offset %lld
 dump interrupted: %ld seconds elapsed
 dump interrupted: %ld seconds elapsed: may resume later using -%c option
 dump label dialog dump selection dialog dump size (non-dir files) : %llu bytes
 dump skipped
 dumping core dumping directories
 dumping ino map
 dumping non-directory files
 dumping session inventory
 effective user ID must be root
 either try using a smaller block size with the -b option, or increase max_sg_segs for the scsi tape driver
 enable progress reports enabling progress reports at  encountered 0 ino (%s) in directory ino %llu: NOT dumping
 encountered EOD : assuming blank media
 encountered EOD : end of data
 encountered EOD but expecting a media stream terminator: assuming full media
 encountered EOM while writing inventory media file size %lld bytes
 encountered EOM while writing media stream terminator size %lld bytes
 encountered EOM: media is full
 encountered corrupt or foreign data but expecting a media stream terminator: assuming corrupted media
 encountered corrupt or foreign data: assuming corrupted media
 encountered corrupt or foreign data: repositioning to overwrite
 encountered corrupt or foreign data: unable to overwrite: assuming corrupted media
 encountered end of media while attempting to begin new media file
 encountered end of media while ending media file
 encountered media error attempting BSF
 end
 end dialog end of media end of media file end of recorded data ending inventory media file
 ending media file
 ending media stream terminator
 ending stream: %ld seconds elapsed
 erasing media
 error removing temp DMF attr on %s: %s
 estimated dump size per stream: %llu bytes
 estimated dump size: %llu bytes
 examining media file %u
 examining new media
 existing version is newer failed to allocate inomap context: %s
 failed to get bulkstat information for inode %llu
 failed to get bulkstat information for root inode
 failed to get valid bulkstat information for inode %llu
 failed to map in node (node handle: %u)
 failed to save %s information, continuing
 file mark missing from tape
 file mark missing from tape (hit EOD)
 file system id: %s
 flush of dirattr failed: %s
 found dump matching specified id:
 found dump matching specified label:
 fssetdm_by_handle of %s failed %s
 get_headers() - malloc(seshdrs)
 getbmapx %d ino %lld mode 0x%08x offset %lld failed: %s
 getbmapx %d ino %lld mode 0x%08x offset %lld ix %d returns negative entry count
 given option file %s is not ordinary file
 giving up attempt to determining tape record size
 giving up waiting for drive to indicate online
 hangup
 hide log message levels hide log message subsystems hide log message timestamps hiding log message levels
 hiding log message subsystems
 hiding log message timestamps
 hit EOD at stream %u object %u, yet inventory indicates last object index is %u
 hit next dump at stream %u object %u file %u, yet inventory indicates last object index is %u
 hostname length is zero
 hostname: %s
 hsm detected an extent map error in ino %lld, skipping
 ignoring old-style extattr header checksums
 incorporating on-media session inventory into online inventory
 initiating session interrupt (timeout in %d sec)
 ino %llu gen %u not referenced: placing in orphanage
 ino %llu offset %lld
 ino %llu salvaging file, placing in %s
 ino %llu: unknown file type: %08x
 ino map construction complete
 ino map phase 1: constructing initial dump list
 ino map phase 2: pruning unneeded subtrees
 ino map phase 2: skipping (no pruning necessary)
 ino map phase 3: identifying stream starting points
 ino map phase 3: skipping (only one dump stream)
 inomap inconsistency ino %llu: %s but is now non-dir: NOT dumping
 inomap inconsistency ino %llu: hsm detected error: NOT dumping
 interactively restore
 interrupt request accepted
 interrupt this session invalid subtree specified inventory media file put failed
 inventory media file size %lld bytes
 inventory session media file put failed
 inventory session uuid (%s) does not match the media header's session uuid (%s)
 invidx (%d)	%d - %d
 keyboard interrupt
 keyboard quit
 level %d dump of %s:%s
 level %d incremental dump of %s:%s based on level %d dump begun %s
 level %u incremental non-subtree dump will be based on subtree level %u dump
 level %u incremental non-subtree dump will be based on subtree level %u resumed dump
 level %u incremental subtree dump will be based on non-subtree level %u dump
 level %u incremental subtree dump will be based on non-subtree level %u resumed dump
 level %u non-subtree dump will be based on subtree level %u resumed dump
 level %u subtree dump will be based on non-subtree level %u resumed dump
 level changed
 level: %s%s
 likely problem is that the block size, %d, is too large for Linux
 link list needed media objects locking check failed ino %llu
 lseek of dirattr failed: %s
 lseek of namreg failed: %s
 makedir malloc of stream context failed (%d bytes): %s
 may be an EFS dump at BOT
 may not specify both -%c and -%c
 media change aborted
 media change acknowledged
 media change decline will be treated as a request to stop using drive: can resume later
 media change declined media change declined: media stream terminator not written
 media change declined: session inventory not dumped
 media change timeout will be treated as a request to stop using drive: can resume later
 media change timeout: media stream terminator not written
 media change timeout: session inventory not dumped
 media changed media contains non-xfsdump data or a corrupt xfsdump media file header at beginning of media
 media contains valid xfsdump but does not support append
 media error or no media media file %u (media %u, file %u)
 media file header checksum error
 media file header magic number mismatch: %s, %s
 media file header version (%d) invalid: advancing
 media file size %lld bytes
 media id: %s
 media inventory status media label:  media labels given for only %d out of %d drives
 media may contain data. Overwrite option specified
 media object empty
 media object not useful
 media stream terminator size %lld bytes
 mkdir %s failed: %s
 more -%c arguments than number of drives
 most recent base dump (level %d begun %s) was interrupted: aborting
 most recent base for incremental dump was interrupted (level %u): must resume or redump at or below level %d
 most recent level %d dump was interrupted, but not resuming that dump since resume (-R) option not specified
 mount point: %s
 must rm -rf %s prior to noncumulative restore
 named pipe needed media objects nh 0x%x np 0x%x hash link not null
 nitty nitty + 1 no additional media objects needed
 no change no change
 no destination file(s) specified
 no media label specified
 no media strategy available for selected dump destination(s)
 no media strategy available for selected restore source(s)
 no more data can be written to this tape
 no session label specified
 no source file(s) specified
 node %x %s %llu %u parent NULL
 node %x %s %llu %u parent mismatch: nodepar %x par %x
 node allocation failed when placing ino %llu in orphanage
 non-root nondir noref debug not allowed to pin down I/O buffer ring
 not enough physical memory to pin down I/O buffer ring
 not enough virtual memory for node abstraction: remaining-vsmz=%llu need=%llu
 on-media session inventory corrupt
 open failed %s: %s
 open of %s failed: %s: discarding ino %llu
 open_by_handle of %s failed:%s
 orphan other controls overwrites inhibited overwriting: %s
 parent of %s is not a directory
 partial_reg: Out of records. Extend attrs applied early.
 path_to_handle of %s failed:%s
 pinned  please change media in drive
 please change media in drive %u
 please change media: type %s to confirm media change
 please confirm
 please enter a value between 1 and %d inclusive  please enter label for this dump session please enter seconds between progress reports please enter seconds between progress reports, or 0 to disable please select one of the following controls
 please select one of the following metrics
 please select one of the following operations
 please select one of the following subsystems
 positioned at media file %u: dump %u, stream %u
 preparing drive
 pruned %llu files: maximum size exceeded
 pruned %llu files: skip attribute set
 read of dirattr failed: %s
 read of namreg failed: %s (nread = %d)
 read of option file %s failed: %s (%d)
 read_record encountered EOD : assuming blank media
 read_record encountered EOD : end of data
 reading directories
 reading non-directory files
 received signal %d (%s): cleanup and exit
 recommended media file size of %llu Mb less than estimated file header size %llu Mb for %s
 record %lld corrupt: bad magic number
 record %lld corrupt: bad record checksum
 record %lld corrupt: dump id mismatch
 record %lld corrupt: incorrect record offset in header (0x%llx)
 record %lld corrupt: incorrect record padding offset in header
 record %lld corrupt: incorrect record size in header
 record %lld corrupt: null dump id
 record %lld corrupt: record offset in header not a multiple of record size
 rename dir rename from rename to repositioning to overwrite
 restore restore
 restore complete: %ld seconds elapsed
 restore interrupted: %ld seconds elapsed: may resume later using -%c option
 restoring non-directory files
 resume (-R) option inappropriate: no interrupted level %d dump to resume
 resuming level %d dump of %s:%s begun %s
 resuming level %d incremental dump of %s:%s begun %s (incremental base level %d begun %s)
 resuming restore previously begun %s
 resynchronization achieved at ino %llu offset %lld
 resynchronized at record %lld offset %u
 rewinding
 rmdir rmtioctl: IRIX mtget structure of wrong size - got %d, wanted %d
 rmtioctl: Linux mtget structure of wrong size - got %d, wanted %d or %d
 rmtioctl: remote host type not supported for MTIOCGET
 rmtioctl: remote host type not supported for MTIOCTOP
 rmtopen: failed to detect remote host type reading "%s"
 rmtopen: failed to detect remote host type using "%s"
 rmtopen: remote host type, "%s", is unknown to librmt
 root saving %s information for: %s
 searching media for directory dump
 searching media for dump
 secure seeking past media file directory dump
 seeking past portion of media file already restored
 select a drive to acknowledge media change
 selected dump not based on previously applied dump
 selected resumed dump not a resumption of previously applied dump
 session id: %s
 session interrupt in progress: please wait
 session interrupt timeout
 session inventory display
 session inventory unknown
 session label entered: " session label left blank
 session label:  session label: "%s"
 session time: %s set dir extattr set dirattr show log message levels show log message subsystems show log message timestamps showing log message levels
 showing log message subsystems
 showing log message timestamps
 silent skip slave source file system not specified
 specified destination %s is not a directory
 specified minimum stack size is larger than maximum: min is 0x%llx,  max is 0x%llx
 stat failed %s: %s
 stat of %s failed: %s
 status and control dialog status at %02d:%02d:%02d: %llu/%llu directories reconstructed, %.1f%%%% complete, %llu directory entries processed, %ld seconds elapsed
 status at %02d:%02d:%02d: %llu/%llu files restored, %.1f%%%% complete, %ld seconds elapsed
 stream %u: ino %llu offset %lld to  stream terminator found
 stripping setgid bit on %s since chown failed
 stripping setuid bit on %s since chown failed
 subtree argument %s invalid
 subtree not present subtree selection dialog syssgi( SGI_FS_BULKSTAT ) on fsroot failed: %s
 table of contents display complete: %ld seconds elapsed
 table of contents display interrupted: %ld seconds elapsed
 tape drive %s is not ready (0x%x): retrying ...
 tape is write protected
 tape media error on write operation
 tape record checksum error
 terminate
 the following commands are available:
 the following dump has been found

 the following dump has been found on drive %u

 the following media objects are needed:
 the following media objects contain media files not yet tried for directory hierarchy restoral:
 this dump selected for interactive restoral
 this dump selected for restoral
 timeout
 tm (%d)	%d
 tmp dir rename dst tmp dir rename src tmp nondir rename dst tmp nondir rename src too many -%c arguments
 too many -%c arguments: "-%c %s" already given
 too old trace tree_extattr_recurse: Could not convert node to path for %s
 unable %s ino %llu gen %u: relative pathname too long (partial %s)
 unable to allocate memory for I/O buffer ring
 unable to autogrow node segment %u: %s (%d)
 unable to backspace tape: assuming media error
 unable to backspace/rewind media
 unable to bind %s ino %llu %s: %s: discarding
 unable to chown %s: %s
 unable to construct a file system handle for %s: %s
 unable to create %s ino %llu %s: %s: discarding
 unable to create %s: %s
 unable to create symlink ino %llu %s: %s: discarding
 unable to create symlink ino %llu %s: src too long: discarding
 unable to determine current directory: %s
 unable to determine hostname: %s
 unable to determine uuid of fs containing %s: %s
 unable to determine uuid of fs mounted at %s: %s
 unable to dump directory %llu entry %s (%llu): name too long
 unable to dump directory: ino %llu too large
 unable to find base dump in inventory to validate dump
 unable to get session inventory to dump
 unable to get status of %s: %s
 unable to get status of -%c argument %s: %s
 unable to locate next mark in media file
 unable to lower stack size soft limit from 0x%llx to 0x%llx
 unable to map %s hdr: %s
 unable to map %s: %s
 unable to map node hdr of size %d: %s
 unable to mmap hash array into %s: %s
 unable to open %s: %s
 unable to open directory %s
 unable to open directory: ino %llu: %s
 unable to open inventory to validate dump
 unable to overwrite
 unable to raise stack size hard limit from 0x%llx to 0x%llx
 unable to raise stack size soft limit from 0x%llx to 0x%llx
 unable to read dirents (%d) for directory ino %llu: %s
 unable to recreate %s: %s
 unable to remove %s: %s
 unable to rename dir %s to dir %s: %s
 unable to rename nondir %s to %s: %s
 unable to rename nondir %s to nondir %s: %s
 unable to resync media file: some portion of dump will NOT be restored
 unable to rmdir %s: %s
 unable to rmdir %s: not empty
 unable to set %s extended attribute for %s: %s (%d)
 unable to set access and modification times of %s: %s
 unable to set block size to %d
 unable to set mode of %s: %s
 unable to stat %s: %s
 unable to strip setuid/setgid on %s, unlinking file.
 unable to unlink %s: %s
 unable to unlink current file prior to linking %s to %s: %s
 unable to unlink current file prior to restore: %s: %s: discarding
 unable to unlink nondir %s: %s
 unable to use %s as a hard link source
 unable to write file mark at eod: %s (%d)
 unexpected EIO error attempting to read record
 unexpected error attempting to read record %lld: read returns %d, errno %d (%s)
 unexpected error attempting to read record: read returns %d, errno %d (%s)
 unexpected error attempting to read record: read returns %d, errno %s (%s)
 unexpected error from do_begin_read: %d
 unexpected tape error: errno %d nread %d blksz %d recsz %d 
 unexpected tape error: errno %d nread %d blksz %d recsz %d isvar %d wasatbot %d eod %d fmk %d eot %d onl %d wprot %d ew %d 
 unexpectedly encountered EOD at BOT: assuming corrupted media
 unexpectedly encountered a file mark: assuming corrupted media
 unknown inode type: ino=%llu, mode=0x%04x 0%06o
 unlink unrecognized drive strategy ID (media says %d, expected %d)
 unrecognized media file header version (%d)
 update of dirattr failed: %s
 use 'xfs_quota' to restore quotas
 use of QIC drives via variable blocksize device nodes is not supported
 using %s strategy
 using online session inventory
 valid record %lld but no mark
 verbose volume: %s
 waiting for synchronized session inventory dump
 will rewind and try again
 win_map(): try to select a different win_t
 win_map(): unable to map a node segment of size %d at %d: %s
 write of dirattr buffer failed: %s
 write of dirattr buffer failed: expected to write %ld, actually wrote %ld
 write of namreg buffer failed: %s
 write of namreg buffer failed: expected to write %ld, actually wrote %ld
 write to %s failed: %d (%s)
 writing file mark at EOD
 writing stream terminator
 Project-Id-Version: xfsdump 3.0.4
Report-Msgid-Bugs-To: Nathan Scott nathans@debian.org
POT-Creation-Date: 2016-01-08 05:09+0000
PO-Revision-Date: 2015-11-11 13:08+0000
Last-Translator: Hendrik Knackstedt <Unknown>
Language-Team: German <debian-l10n-german@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:22+0000
X-Generator: Launchpad (build 18115)
Language: de
 	es könnte hier nicht identifizierte Medienobjekte geben, die nicht vollständig wiederhergestellt sind
 

--------- fstab ------------
 
        könnten zusätzliche nicht identifizierte Mediendateien sein
 
        Mediendatei %u 
    könnten zusätzliche nicht identifizierte Medienobjekte sein

 
    Medienobjekt %u:

 
    Medienobjekte noch nicht identifiziert
 
diese Ausgabe untersuchen?
 
interaktiv aus dieser Ausgabe wiederherstellen?
 
Medien-Datenstrom %u:
 
diese Ausgabe wiederherstellen?
 
Sitzung wird gerade unterbrochen
 
es gibt zusätzliche nicht identifizierte Medienobjekte, die Mediendateien enthalten, bei denen noch nicht versucht wurde, die Verzeichnishierarchie wiederherzustellen:
 
es gibt zusätzliche nicht identifizierte Medienobjekte, die nicht vollständig wiederhergestellt sind
 
es gibt nicht identifizierte Medienobjekte, die Mediendateien enthalten, bei denen noch nicht versucht wurde, die Verzeichnishierarchie wiederherzustellen:
 
es gibt nicht identifizierte Medienobjekte, die nicht vollständig wiederhergestellt sind
 
es könnte zusätzliche nicht identifizierte Medienobjekte geben, die Mediendateien enthalten, bei denen noch nicht versucht wurde, die Verzeichnishierarchie wiederherzustellen:
 
es könnte zusätzliche nicht identifizierte Medienobjekte geben, die nicht vollständig wiederhergestellt sind
 
es könnte nicht identifizierte Medienobjekte geben, die Mediendateien enthalten, bei denen noch nicht versucht wurde, die Verzeichnishierarchie wiederherzustellen:
         enthält keine ausgewählten Nicht-Verzeichnisse
         enthält Sitzungsbestand
         erster Bereich enthielt: Ino %llu aus %lld
         ist Datenstrom-Terminator
         Mediendateien noch nicht identifiziert
         nächster Bereich zur Wiederherstellung: Ino %llu aus %lld
         Nicht-Verzeichnisse erledigt
         wird nun gelesen
         Wiederherstellungsmarkierung %lld
         Größe: %lld Byte
         benutzt für Verzeichniswiederherstellung
     ID:      ID: %s
     Index innerhalb Objekt der ersten Mediendatei: %u
     Index innerhalb Datenstrom der ersten Mediendatei: %u
     Kennung ist leer
     Kennung nicht identifiziert
     Kennung:      nun in Laufwerk
     nun in Laufwerk %u
  (Aktuell)  (Vorgabe)  (Zeitüberschreitung in %u Sek.)  (Zeitüberschreitung in %u Sek.)
  - geben Sie für Status und Steuerung %s ein
  wieder aufgenommen %2u. Eitkett:  %d Sekundenintervallen
 %d Sekunden
 %llu Verzeichnisse und %llu Einträge verarbeitet
 %s (Versatz %lld)
 %s oberhalb der Wurzel
 %s exisitiert bereits: »rm -rf« vor Beginn der Wiederherstellung
 %s erstellt
 %s bezeichnet kein Dateisystem
 %s erweiterter Attributsname für %s-Ino %llu zu lang: %u, Maximum ist %u: Wird übersprungen
 %s erweiterter Attributswert für %s Ino %llu zu lang: %u, Maximum ist %u: Wird übersprungen
 %s fehlgeschlagen mit Beendigungsstatus: %d
 %s-Format beschädigt oder falsche Endianness (0x%x, erwartet 0x%x)
 %s-Format unterscheidet sich aufgrund einer Seitengrößenänderung von dem der vorherigen Wiederherstellung (war %lu, jetzt %lu)
 %s-Formatversion unterscheidet sich von vorheriger Wiederherstellung (%u, erwartet %u)
 %s ist kein Verzeichnis
 %s muss eingehängt sein, um ausgegeben werden zu können
 %s nicht gefunden
 %s Quota-Informationen nach »%s« geschrieben
 %s%s%s%s: Laufwerk %d:  %s:   Stream %d %s %s (%s)
 %s: %s Zusammenfassung:
 %s: -%c-Argument benötigt keinen Wert
 %s: -%c-Argument ungültig
 %s: -%c-Argument fehlt
 %s: -%c-Untersystem-Unterargument %s benötigt einen Detailgrad
 %s: Unbekannter Dateityp: Modus 0x%x Ino %llu
 %s: Aufruf: %s  (erlaubt das Ausschließen von Dateien) (Prüfsummen der Banddatensätze prüfen) (nur Inhalte) (kumulatives Wiederherstellen) (Ausgabebestand anzeigen) (keine Ausgabe erweiterter Dateiattribute) (existierende Dateien nicht überschreiben) (nicht überschreiben, wenn geändert) (nicht auffordern) (erweiterte Dateiattribute nicht wiederherstellen) (keine Zeitüberschreitung für Dialoge) (Ausgabe von DMF-Dualstate-Dateien offline) (Vervollständigung der unterbrochenen Sitzung erzwingen) (Benutzung von minimalen Rmt erzwingen) (Prüfsumme der Banddatensätze generieren) (Hilfe) (Aktualisieren des Bestands verhindern) (interaktiv) (Band überschreiben) (I/O-Puffer festsetzen) (vorher gelöschtes Medium) (DMAPI-Ereigniseinstellungen wiederherstellen) (Besitzer/Gruppe auch dann herstellen, wenn nicht »root«) (Besitzer/Rechte des Wurzelverzeichnisses wiederherstellen) (fortsetzen) (Untersystem in Nachrichten anzeigen) (Detailgrad in Nachrichten anzeigen) (überspringe unveränderte Verzeichnisse) (Zeitüberschreitung in %d Sek.) (Zeitstempel-Nachrichten) (Medium entladen, falls Änderung erforderlich) (kleines Fenster mit Baumansicht benutzen) - (STDIN) - (STDOUT) -%c nur einmal erlaubt
 Optionen -%c und -%c können nicht zusammen benutzt werden
 -%c und -%c sind nur gültig, wenn kumulative Wiederherstellung gestartet wird
 -%c und -%c sind nur gültig, wenn Wiederherstellung gestartet wird
 -%c-Argument %s (%s) ist kein Verzeichnis
 -%c-Argument %s ist ein ungültiger Pfadname
 -%c-Argument %s ist zu lang: Maximum ist %d
 -%c-Argument (%s) ungültig
 -%c-Argument (Teilbaum) muss ein relativer Pfadname sein
 -%c-Argument ist keine gültige Dateigröße
 -%c-Argument fehlt
 -%c-Argument muss  eine positive Zahl (MB) sein: %u wird ignoriert
 -%c-Argument muss eine positive Zahl (MB) sein: %u wird ignoriert
 -%c-Argument muss zwischen %u und %u liegen: %u wird ignoriert
 -%c-Argument muss zwischen 0 und %d liegen
 -%c-Argument muss relativ sein
 -%c-Argument ist keine gültige Ausgabedatei-ID
 -%c-Argument ist keine gültige UUID
 -%c kann Markierung von vorheriger Wiederherstellung nicht zurücksetzen
 Option -%c ungültig: Es gibt keine unterbrochene Wiederherstellung, um die Komplettierung zu erzwingen von
 Option -%c ungültig: Es gibt keine unterbrochene Wiederherstellung zur Wiederaufnahme
 Option -%c zum Fortfahren erforderlich oder Option -%c benötigt, um die Vervollständigung von vorher unterbrochenen Wiederherstellungssitzungen zu erzwingen
 -%c nicht verfügbar: Kein /dev/tty
 -%c ist nur gültig, wenn kumulative Wiederherstellung gestartet wird
 -%c ist nur gültig, wenn Wiederherstellung gestartet wird
 <Ringlänge des I/O-Puffers> <alt. Arbeitsplatz Verz.> ... <Basis-Ausgabesitzungs-ID> <Blockgröße> <Bestimmungsort> <Bestimmungsort> ... <Ausgabe der Mediendateigröße>  <ausgeschlossener Teilbaum> ... <Datei> (nur wiederherstellen, wenn neuer) <Datei> (»mtime« der Datei als Ausgabezeit benutzen <Stufe> <maximale Dateigröße> <maximale Größe des Thread-Stacks> <Medienwechsel-Warnprogramm>  <Medienkennung> ... <minimale Größe des Thread-Stacks> <Optionsdatei> <Sekunden zwischen Fortschrittsberichten> <Sitzungs-ID> <Sitzungskennung> <Quelle (Einhängepunkt|Gerät)> <Quelle> ... <Teilbaum> ... <QIC-Bandeinstellungen benutzen> <Detailgrad {silent, verbose, trace}> %s kann zur Ermittlung der Einhängeinformationen nicht geöffnet werden
 Dev	%s
 Fehler im gelesenen Bestandsdatensatz (lseek fehlgeschlagen):  Fehler im gelesenen Bestandsdatensatz : Fehler im geschriebenen Bestandsdatensatz (lseek fehlgeschlagen):  Fehler im geschriebenen Bestandsdatensatz : FSF-Bandbefehl fehlgeschlagen
 FSid	%s

 Reservieren des erweiterten Attribut-Puffers fehlgeschlagen
 Daten aus der Mediendatei abzufragen fehlgeschlagen: Datei möglicherweise beschädigt
 HSM konnte %s-Attribut #%d für %s-Ino %llu nicht hinzufügen
 HSM konnte %s-Attribut %s für %s-Ino %llu nicht filtern
 E/A I/O-Metrik: %u mit %s%s %sring; %lld/%lld (%.0lf%%) Datensätze übertragen; %.0lfB/s
 INV : Unbekannte Version %d - erwartete Version %d
 INV: inv-name für UUID kann nicht geholt werden
 INV: Prüfen von %d fehlgeschlagen.
 INV: Prüfen von %d von %d erfolgreich
 INV: Fehler in fstab
 INV: Keine aufgezeichneten Dateisysteme in der Fstab des Bestands.
 INV: Nur eines von mnt=,dev= und fsid=Wert kann benutzt werden.
 INV: Nur eines von mobjid= und mobjlabel= kann benutzt werden.
 INV: Öffnen auf %s fehlgeschlagen
 INV: Es können nicht mehr als %d Datenströme erstellt werden. Übergröße ...
 INV: Dateisystem »%s« wird geprüft
 INV: Fstab-Kopfzeilen konnten nicht geholt werden
 INV: Eine unterschiedliche sh_time zum Aufteilen zu finden fehlgeschlagen
 INV: idx_put_sesstime in inv_stream_close() fehlgeschlagen 
 INV: inv_put_session: Unbekanntes Cookie
 INV: inv_put_session: Unbekannte Paketbestandsversion %d
 INV: Ungültige Unteroption %s für Option -I
 INV: Öffnen auf Dateisystem-ID »%s« fehlgeschlagen
 INV: Öffnen auf Einhängepunkt »%s« fehlgeschlagen
 INV: put_fstab_entry fehlgeschlagen
 INV: put_starttime fehlgeschlagen
 KB MB Minimal-Rmt kann nicht ohne Angabe der Blockgröße benutzt werden. Benutzen Sie -%c
 Einhängen	%s
 Befehlszeilenoption überschreiben
 RMTCLOSE( %d ) gibt %d zurück: Fehlernummer=%d (%s)
 RMTIOCTL( %d, %d, 0x%x ) gibt %d zurück: Fehlernummer=%d (%s)
 RMTOPEN( %s, %d ) gibt %d zurück: Fehlernummer=%d (%s)
 RMTREAD( %d, 0x%x, %u ) gibt %d zurück: Fehlernummer=%d (%s)
 RMTWRITE( %d, 0x%x, %u ) gibt %d zurück: Fehlernummer=%d (%s)
 SGI_FS_BULKSTAT fehlgeschlagen: %s (%d)
 Dieses Band wurde früher mit Xfsdump gelöscht.
 Unix-Domänen-Socket Markierung entfernen und beenden
 WARNUNG:  WARNUNG: »stat« kann nicht für Dirent %s Ino %llu ausgeführt werden: %s
 WARNUNG: Verzeichnis-Ino %llu kann nicht geöffnet werden: %s
 WARNUNG: Dirent %s kann nicht verarbeitet werden: Ino %llu zu groß
 WARNUNG: Dirents (%d) für Verzeichnis-Ino %llu können nicht gelesen werden: %s
 XENIX benannte Weiterleitung Ungewöhnliches Dialogende
 Abbrechen
 Band auf nächste Mediendatei vorspulen
 alle Abbildungsfenster im Gebrauch. Prüfen Sie die Beschränkungen des virtuellen Speichers
 alle Mediendateien wurden untersucht, keines wurde für die Wiederherstellung ausgewählt
 alle obigen alle Untersysteme ausgewählt

 es wird vermutet, dass das Medium beschädigt ist oder keine Xfsdump-Daten enthält
 am Ende der Daten
 Versuch, (auf) Gerät %s zuzugreifen/zu öffnen, fehlgeschlagen: %d (%s)
 Versuch (auf) fernes Bandlaufwerk %s zuzugreifen/zu öffnen fehlgeschlagen: %d (%s)
 Versuch von »disown« für untergeordnetes Element, wenn übergeordnetes Element keine untergeordneten Elemente hat!
 Versuch von »disown« für untergeordnetes Element, das kein übergeordnetes Element hat!
 Versuch, den Status des fernen Bandlaufwerks %s abzufragen, fehlgeschlagen: %d (%s)
 Versuch, den Status des Bandlaufwerks %s abzufragen, fehlgeschlagen: %d (%s)
 Versuch, %s mit %s zu verknüpfen, fehlgeschlagen: %s
 Versuch, %u Bytes zu lesen, fehlgeschlagen: %s
 Versuch, %lld Byte für %s unter Benutzung von %s zu reservieren, fehlgeschlagen: %s (%d)
 Versuch, %s zu %lld zu suchen, fehlgeschlagen: %s: Bereich von %lld sz %lld wird nicht wiederhergestellt
 Versuch, die DMI-Attribute von %s zu setzen, fehlgeschlagen: %s
 Versuch erweiterte Attribute (xflags 0x%x, extsize = 0x%x, projid = 0x%x) von %s zu setzen fehlgeschlagen: %s
 Versuch, %s mit »stat« abzufragen, fehlgeschlagen: %s
 Versuch, %s zu kürzen, fehlgeschlagen: %d (%s)
 Versuch, %s zu kürzen, fehlgeschlagen: %s
 Versuch, %u Byte nach %s bei Versatz %lld zu schreiben, fehlgeschlagen: %s
 Versuch, %u Byte nach %s bei Versatz %lld zu schreiben, fehlgeschlagen: Nur %d Byte geschrieben
 »attr_multi« zeigt Fehler während des Abfragens von %s-Attribut [%s] für %s Ino %llu: %s (%d)
 Audio falsche Prüfsumme der Verzeichniseintrags-Kopfzeilen
 falsche Prüfsumme der »extattr«-Kopfzeilen
 falsche Prüfsumme der Bereichs-Dateikopfzeilen
 falsche Dateikopfzeilen-Prüfsumme
 falsche Mediendatei-Kopfzeilen bei BOT (Massenübertragung) deuten auf fremdes oder beschädigtes Band hin
 Anfang der Medienbestandsdatei
 Datenstrom-Terminator wird begonnen
 spezielle Blockdatei sowohl /var/lib/xfsdump als auch /var/xfsdump existieren - fatal
 »lseek ino %llu« kann nicht ausgeführt werden
 Ino %llu bei Versatz %d kann nicht gelesen werden (act=%d req=%d) rt=%d
 inkrementelle Ausgabe kann nicht berechnet werden: Online-Bestand nicht verfügbar
 %s-Thread für Datenstrom %u kann nicht angelegt werden: Zu viele Kind-Threads (maximal sind %d erlaubt)
 Band-Blockgröße kann nicht festgelegt werden
 Band-Blockgröße kann nach zwei Versuchen nicht festgelegt werden
 Ausgabe in Datei %s vom Typ %x kann nicht erstellt werden
 es können keine früheren Ausgaben gefunden werden, die als Basis für die inkrementelle Ausgabe Stufe %d dienen könnten.
 Auszugsdatenströme können nicht überlappen: Es muss ein leeres Medienobjekt bereitgestellt werden
 Optionsdatei %s kann nicht geöffnet werden: %s (%d)
 %s (Ino %llu Gen %u) kann nicht wiederhergestellt werden: Pfadname enthält %s
 aus Datei %s Typ %x kann nicht wiederhergestellt werden
 Ausgabessitzung %d kann nicht als Basis für inkrementelle Ausgabe ausgewählt werden: Stufe muss kleiner als %d sein
 Quelldateien und STDIN können nicht zusammen angegeben werden
 Quelldateien und STDOUT können nicht zusammen angegeben werden
 »stat« kann nicht für -%c-Argument %s (%s) ausgeführt werden: %s
 Zielverzeichnis %s kann nicht mit »stat« abgefragt werden: %s
 Optionsdatei %s kann nicht mit »stat« abgefragt werden: %s (%d)
 Intervall ändern oder Fortschrittsberichte ausschalten Medienwechseldialog Detailgrad ändern Intervall der Fortschrittsberichte wird geändert in  spezielle Zeichendatei chdir %s fehlgeschlagen: %s
 chmod %s fehlgeschlagen: %s
 chown (uid=%d, gid=%d) %s fehlgeschlagen: %s
 chown (uid=%u, gid=%u) %s fehlgeschlagen: %s
 Medienwechsel bestätigen fortfahren weiter
 beschädigte Verzeichniseintrags-Kopfzeilen
 beschädigte »extattr«-Kopfzeilen
 Beschädigte Bereichs-Dateikopfzeilen
 Beschädigte Dateikopfzeilen
 %s konnte nicht erstellt werden: %s
 Datei mit Verzeichnisattributen %s konnte nicht erstellt werden: %s
 Namensregistrierungsdatei %s konnte nicht erstellt werden: %s
 %s kann nicht gelöscht werden: %s (%d)
 Datei mit Verzeichnisattributen %s konnte nicht gefunden werden: %s
 Namensregistrierungsdatei %s konnte nicht gefunden werden: %s
 angegebene Basisausgabe (%s) kann nicht im Bestand gefunden werden
 Bereich für %d Bandblöcke konnte nicht weitergeleitet werden: rval == %d, Fehlernummer == %d (%s)
 Bereich konnte nicht einen Bandblock hinter Lesefehler weitergeleitet werden: rval == %d, Fehlernummer == %d (%s)
 fstat stdin (fd %d) kann nicht durchgeführt werden: %s (%d)
 Liste von %s-Attributen für %s-Ino %llu konnte nicht geholt werden: %s (%d)
 beständige Statusdatei %s konnte nicht abgebildet werden: %s
 beständiger Statusdatei-Hdr %s kann nicht abgebildet werden: %s
 beständige Statusdatei-Inv %s konnte nicht abgebildet werden: %s (%d)
 %s kann nicht geöffnet werden: %s
 Ino %llu kann nicht zum Lesen der Bereichsabbildung geöffnet werden: %s
 regulärer Datei-Ino %llu Modus 0x%08x kann nicht geöffnet werden: %s: Nicht ausgegeben
 erweiterte Verzeichnis-Attributsdatei %s konnte nicht geöffnet/erstellt werden: %s (%d)
 beständige Statusdatei %s kann nicht geöffnet/erstellt werden: %s
 erweiterte Verzeichnis-Attributsdatei %s konnte nicht gelesen werden: %s (%d)
 Bereichsabbildung für %llu kann nicht zum Lesen geöffnet werden: %s
 vom Laufwerk kann nicht gelesen werden: %s (%d)
 beständige Statusdatei-Inv %s konnte nicht erneut abgebildet werden: %d (%s)
 %s-Attribute für %s Ino %llu konnten nicht abgefragt werden: %s (%d)
 %s kann als Vorbereitung zum Löschen nicht zurückgespult werden: %s
 %s kann nicht zurückgespult werden: %s
 erste Markierung kann nicht gesichert werden: %d (%s)
 es konnte nicht bis zum Ende der erweiterten Attributsdatei %s gesucht werden: %s (%d)
 in erweiterter Verzeichnis-Attributsdatei %s konnte nicht gesucht werden: %s (%d)
 Zugriffs- und Änderungszeiten von %s konnten nicht gesetzt werden: %s
 %s kann nicht mit »stat« abgefragt werden
 »stat dirent %s ino %llu« konnte nicht ausgeführt werden: %s: »null generation count« wird im Verzeichniseintrag benutzt
 es konnte nicht am Ende der erweiterten Attributsdatei %s geschrieben werden: %s (%d)
 erweiterte Attributsdatei %s konnte nicht geschrieben werden: %s (%d)
 Ausgabesitzungs-Mediendatei %u wird erstellt (Medium %u, Datei %u)
 kumulative Wiederherstellungen müssen mit einer Ausgabe der Stufe 0 beginnen
 kumulative Wiederherstellungen müssen mit einer anfänglichen (nicht fortgesetzten) Ausgabe der Stufe 0 beginnen
 Fehlersuche Zielverzeichnis %s ungültiger Pfadname
 Zielverzeichnis nicht angegeben
 Zielverzeichnis-Pfadname zu lang: Maximal %d Zeichen
 »dioinfo %s« fehlgeschlagen: %s: Ino %llu wird verworfen
 »dioinfo« fehlgeschlagen Ino %llu
 Verz. Vorverarbeitung des Verzeichnisses
 Fortschrittsberichte werden ausgeschaltet
 Medienbestandsstatus anzeigen Maße anzeigen Benötigte Medienobjekte anzeigen geben Sie das Zielverzeicnis nicht an, wenn Inhalte nur wiederhergestellt aufgerufen werden (Option -%c)
 es ist nicht bekannt, wie Ino %llu ausgegeben werden soll: Modus %08x
 Laufwerk %u Laufwerk %u  Laufwerk unterstützt nicht das Löschen von Medien: Option -%c wird ignoriert
 Ausgabe vollständig: %ld Sekunden verstrichen
 Ausgabe enthält %llu Inodes, die Wiederherstellung kann aber nur %u verarbeiten
 Abfrage enthält zu viele dirents, die Wiederherstellung kann nur %llu verarbeiten
 Ausgabedatum: %s
 Ausgabebeschreibung: 
 Ausgabe unterbrochen vor Ino %llu Versatz %lld
 Ausgabe unterbrochen: %ld Sekunden verstrichen
 Ausgabe unterbrochen: %ld Sekunden verstrichen: Könnte später unter Benutzung der Option -%c fortgesetzt werden
 Ausgabesitzungsdialog Ausgabe-Auswahldialog Ausgabegröße (Nicht-Verzeichnisdateien): %llu Byte
 Ausgabe übersprungen
 Ausgabe des Kerns Ausgabe der Verzeichnisse
 Ausgabe der Ino-Map
 Ausgabe der Nicht-Verzeichnisdateien
 Sitzungsbestand wird ausgegeben
 effektive Benutzer-ID muss »root« sein
 versuchen Sie entweder eine kleinere Blockgröße mit der Option -b oder erhöhen Sie »max_sg_segs« für den SCSI-Bandtreiber
 Fortschrittsberichte einschalten Fortschrittsberichte werden eingeschaltet bei  0-Ino (%s) in Verzeichnis-Ino %llu vorgefunden: KEINE Ausgabe
 EOD (Datenende) vorgefunden: Leeres Medium wird vermutet
 EOD vorgefunden: Datenende
 EOD (Datenende) wurde vorgefunden, aber ein Medien-Datenstromterminator erwartet: Medium vermutlich voll
 beim Schreiben der Bestandsmedien-Dateigröße %lld Byte wurde EOM vorgefunden
 beim Schreiben der Datenstrom-Terminator-Größe %lld Byte wurde EOM vorgefunden
 EOD (Datenende) wurde vorgefunden: Medium ist voll
 beschädigte oder falsche Daten vorgefunden, aber Mediendatenstrom-Terminator erwartet: Medium vermutlich beschädigt
 beschädigte oder fremde Daten vorgefunden: Medium vermutlich beschädigt
 beschädigte oder falsche Daten vorgefunden: Zum Überschreiben neu positioniert
 beschädigte oder falsche Daten vorgefunden: Können nicht überschrieben werden: Medium vermutlich beschädigt
 Ende des Mediums vorgefunden, während versucht wird neue Mediendatei anzufangen
 Ende des Mediums vorgefunden, während Mediendatei endete
 Medienfehler beim Versuch von BSF vorgefunden
 Ende
 Schlussdialog Ende des Mediums Ende der Mediendatei Ende der aufgezeichneten Daten Ende der Medienbestandsdatei
 Ende der Mediendatei
 Datenstrom-Terminator wird beendet
 Ende des Datenstroms: %ld Sekunden verstrichen
 Medium wird gelöscht
 Fehler beim Entfernen des temporären »DMF attr« auf %s: %s
 geschätzte Ausgabegröße pro Datenstrom: %llu Byte
 geschätzte Ausgabegröße: %llu Byte
 Mediendatei %u wird untersucht
 Neues Medium wird untersucht
 existierende Version ist neuer Reservieren des Inomap-Kontexts fehlgeschlagen: %s
 Bulkstat-Informationen für Inode %llu abfragen fehlgeschlagen
 Erhalt von »bulkstat«-Informationen über den Wurzel-Inode fehlgeschlagen
 gültige Bulkstat-Informationen für Inode %llu können nicht abgefragt werden
 Abbilden in Knoten fehlgeschlagen (Knoten-Handle: %u)
 sichern von %s-Informationen fehlgeschlagen, wird fortgesetzt
 Dateimarkierung vom Band fehlt
 Dateimarkierung vom Band fehlt (drücken Sie EOD [Datenende])
 Datensystem-ID: %s
 Leeren von »dirattr« fehlgeschlagen: %s
 zur angegebenen ID passende Ausgabe gefunden:
 zur angegebenen Kennung passende Ausgabe gefunden:
 fssetdm_by_handle von %s fehlgeschlagen %s
 get_headers() - malloc(seshdrs)
 »getbmapx« %d Ino %lld Modus 0x%08x Versatz %lld fehlgeschlagen: %s
 »getbmapx« %d Ino %lld Modus 0x%08x Versatz %lld ix %d gibt negative Anzahl Einträge zurück
 angegebene Optionsdatei %s ist keine normale Datei
 Versuch, die Band-Datensatzgröße festzulegen, wird aufgegeben
 Warten auf Laufwerk, um festzustellen ob es online ist, wird aufgegeben
 blockieren
 Protokollnachrichtenstufen verbergen Protokollnachrichtenuntersysteme verbergen Protokollnachrichtenzeitstempel verbergen Protokollnachrichtenstufen werden verborgen
 Protokollnachrichtenuntersysteme werden verborgen
 Protokollnachrichtenzeitstempel werden verborgen
 drücken Sie EOD (Datenende) bei Datenstrom %u, Objekt %u, Bestand zeigt schon an, dass letzter Objektindex %u ist
 starten Sie auf die nächste Ausgabe bei Datenstrom %u, Objekt %u, Datei %u, Bestand zeigt schon an, dass letzter Objektindex %u ist
 Länge des Rechnernamens ist null
 Rechnername: %s
 HSM entdeckte einen Bereichs-Abbildungsfehler in Ino %lld, wird übersprungen
 extattr-Header-Prüfsummen im alten Stil werden ignoriert
 Sitzungsbestand auf dem Medium wird in Online-Bestand aufgenommen
 Sitzungsunterbrechung wird initiiert (Zeitüberschreitung in %d Sek.)
 Ino %llu Gen %u ohne Bezug: Wird bei Verwaisten platziert
 Ino %llu Versatz %lld
 Ino %llu rettet Datei, wird in %s platziert
 Ino %llu: Unbekannter Dateityp: %08x
 Ino-Map-Konstruktion vollständig
 Ino-Map Phase 1: Anfängliche Ausgabeliste aufstellen
 Ino-Map Phase 2: Nicht benötigte Unterbäume werden verkleinert
 Ino-Map Phase 2: Wird übersprungen (kein Verkleinern nötig)
 Ino-Map Phase 3: Datenstrom-Startpunkte werden identifiziert
 Ino-Map Phase 3: Wird übersprungen (nur ein Ausgabedatenstrom)
 »inomap«-Unvollständigkeit Ino %llu: %s ist nun aber kein Verzeichnis: Wird NICHT ausgegeben
 »inomap«-Unvollständigkeit Ino %llu: HSM entdeckt Fehler: Wird NICHT ausgegeben
 interaktiv wiederherstellen
 Unterbrechungsanfrage akzeptiert
 diese Sitzung unterbrechen ungültiger Teilbaum angegeben Bereitstellen der Medienbestandsdatei fehlgeschlagen
 Bestandsmedien-Dateigröße %lld Byte
 Bereitstellen der Sitzungsmediendatei fehlgeschlagen
 Bestandssitzungs-UUID (%s) passt nicht zu der Medienkopfzeilen-Sitzungs-UUID (%s)
 invidx (%d)	%d - %d
 Tastatur-Unterbrechung
 Tastatur-Beendigung
 Stufe-%d-Ausgabe von %s: %s
 Stufe %d der inkrementellen Ausgabe von %s: %s basiert auf Stufe %d der Ausgabe begonnen %s
 Stufe %u der inkrementellen Nicht-Teilbaumausgabe wird auf einer Ausgabe der Teilbaumstufe %u basieren
 Stufe %u der inkrementellen Nicht-Teilbaumausgabe wird auf einer wieder aufgenommenen Ausgabe der Teilbaumstufe %u basieren
 Stufe %u der inkrementellen Teilbaumausgabe wird auf einer Ausgabe der Nicht-Teilbaumstufe %u basieren
 Stufe %u der inkrementellen Teilbaumausgabe wird auf einer wieder aufgenommenen Ausgabe der Nicht-Teilbaumstufe %u basieren
 Stufe %u der Nicht-Teilbaumausgabe wird auf einer wieder aufgenommenen Ausgabe der Teilbaumstufe %u basieren
 Stufe %u der Teilbaumausgabe wird auf einer wieder aufgenommenen Ausgabe der Nicht-Teilbaumstufe %u basieren
 Stufe geändert
 Stufe: %s%s
 wahrscheinliches Problem ist die Blockgröße, %d ist für Linux zu groß
 verknüpfen Benötigte Medienobjekte auflisten Prüfung der Sperre fehlgeschlagen Ino %llu
 lseek von dirattr fehlgeschlagen: %s
 »lseek« von »namreg« fehlgeschlagen: %s
 makedir »Malloc« des Datenstrom-Kontextes fehlgeschlagen (%d Byte): %s
 bei BOT (Massenübertragung) könnte eine EFS-Ausgabe sein
 es können nicht sowohl -%c als auch -%c angegeben werden
 Medienwechsel abgebrochen
 Bestätigung des Medienwechsels
 Ablehnen des Medienwechsels wird als Stopp-Anfrage für die Benutzung des Laufwerks betrachtet: Kann später fortgesetzt werden
 Medienwechsel abgelehnt Medienwechsel abgelehnt: Datenstrom-Terminator für Medium nicht geschrieben
 Medienwechsel abgelehnt: Sitzungsbestand nicht ausgegeben
 Zeitüberschreitung des Medienwechsels wird als Stopp-Anfrage für die Benutzung des Laufwerks betrachtet: Kann später fortgesetzt werden
 Zeitüberschreitung beim Medienwechsel: Datenstrom-Terminator für Medium nicht geschrieben
 Zeitüberschreitung beim Medienwechsel: Sitzungsbestand nicht ausgegeben
 Medium gewechselt Medium enthält Nicht-Xfsdump-Daten oder beschädigte Xfsdump-Mediendatei-Kopfzeilen am Anfang des Mediums
 Medium enthält gültiges »xfsdump«, unterstützt aber kein Anhängen
 Medienfehler oder kein Medium Mediendatei %u (Medium %u, Datei %u)
 Prüfsummenfehler von Mediendateikopfzeilen
 Magische Zahl von Mediendateikopfzeilen stimmt nicht überein: %s, %s
 Mediendatei-Kopfzeilenversion (%d) ungültig: Es wird fortgefahren.
 Mediendateigröße %lld Byte
 Medien-ID: %s
 Medien-Bestandsstatus Medienkennung:  es wurden nur für %d von %d Laufwerken Medien-Kennungen angegeben
 Medium könnte Daten enthalten. Überschreiboption angegeben
 Medienobjekt leer
 Medienobjekt nicht nützlich
 Datenstrom-Terminator-Größe %lld Byte
 »mkdir %s« fehlgeschlagen: %s
 mehr -%c-Argumente als Anzahl der Geräte
 aktuellste Basisausgabe (Stufe %d begonnen %s) wurde unterbrochen: Abbruch
 aktuellste Basis für inkrementelle Ausgabe wurde unterbrochen (Stufe %u): Muss auf oder über Stufe %d wieder aufgenommen oder erneut ausgegeben werden
 Ausgabe der aktuellsten Stufe %d wurde unterbrochen, aber nicht wieder aufgenommen, so dass Ausgabe seit der Option »resume« (-R) nicht angegeben ist
 Einhängepunkt: %s
 vor nicht-kumulativer Wiederherstellung muss »rm -rf %s« ausgeführt werden
 benannte Weiterleitung benötigte Medienobjekte nh 0x%x np 0x%x Hash-Verknüpfung nicht null
 verrückt total verrückt keine zusätzlichen Mediendateien benötigt
 keine Änderung keine Änderung
 keine Zieldatei(en) angegeben
 Kein Medien-Kennung angegeben
 für die/den ausgewählte(n) Ausgabebestimmungsort(e) ist keine Medienstrategie verfügbar
 für die/den ausgewählte(n) Wiederherstellungsquelle(n) ist keine Medienstrategie verfügbar
 auf dieses Band können keine Daten mehr geschrieben werden
 Keine Sitzungskennung angegeben
 keine Quelldatei(en) angegeben
 Knoten %x %s %llu %u übergeordnetes Element NULL
 Knoten %x %s %llu %u übergeordnetes Element passt nicht: Nodepar %x par %x
 Knotenreservierung beim Platzieren von Ino %llu bei Verwaisten 
fehlgeschlagen
 Kein Root-Benutzer kein Verz. »noref«-Fehlersuche I/O-Puffer-Ring festzusetzen nicht erlaubt
 physischer Speicher reicht nicht aus, um I/O-Puffer-Ring festzusetzen
 Nicht genug virtueller Speicher für Knoten-Abstraktion: Verbleibender vsmz=%llu Benötigt=%llu
 Sitzungsbestand auf dem Medium beschädigt
 %s zu öffnen fehlgeschlagen: %s
 Öffnen von %s fehlgeschlagen: %s: Ino %llu wird verworfen
 open_by_handle von %s fehlgeschlagen: %s
 Waise andere Steuerungen Überschreiben verhindert wird überschrieben: %s
 übergeordnetes Element von %s ist kein Verzeichnis
 partial_reg: Keine verbleibenden Datensätze. Erweiterte »attrs« frühzeitig angewendet.
 path_to_handle von %s fehlgeschlagen: %s
 aufgesteckt  bitte wechseln Sie das Medium im Laufwerk
 Bitte wechseln Sie das Medium in Laufwerk %u
 bitte Medium wechseln: Geben Sie %s ein, um Medienwechsel zu bestätigen
 bitte bestätigen
 Bitte geben Sie einen Wert zwischen 1 und %d (inklusive) ein  bitte geben Sie eine Kennung für diese Ausgabesitzung ein bitte geben Sie die Zeit in Sekunden zwischen Fortschrittsberichten ein bitte geben Sie die Zeit in Sekunden zwischen Fortschrittsberichten ein oder 0, um sie auszuschalten. bitte wählen Sie eine der folgenden Steuerungen aus
 bitte wählen Sie eines der folgenden Maße
 bitte wählen Sie eine der folgenden Operationen
 bitte wählen Sie eines der folgenden Untersysteme
 positioniert auf Mediendatei %u: Ausgabe %u, Datenstrom %u
 Laufwerk wird vorbereitet
 %llu Dateien verkleinert: Maximale Größe überschritten
 %llu Dateien verkleinert: Attributszusammenstellung wird übersprungen
 Lesen von »dirattr« fehlgeschlagen: %s
 Lesen von »namreg« fehlgeschlagen: %s (nread = %d)
 Lesen der Optionsdatei %s fehlgeschlagen: %s (%d)
 read_record fand EOD (Datenende) vor: Medium vermutlich leer
 read_record fand EOD vor: Datenende
 Verzeichnisse werden gelesen
 Nicht-Verzeichnisdateien werden gelesen
 Signal %d erhalten (%s): Es wird aufgeräumt und beendet
 empfohlene Mediendateigröße von %llu MB ist kleiner als geschätzte Datei-Header-Größe %llu MB für %s
 Datensatz %lld beschädigt: Falsche Magische Zahl
 Datensatz %lld beschädigt: Falsche Datensatz-Prüfsumme
 Datensatz %lld beschädigt: Ausgabe-ID stimmt nicht überein
 Datensatz %lld beschädigt: Datensatz-Versatz in Kopfzeilen (0x%llx) nicht korrekt
 Datensatz %lld beschädigt: Datensatz-Füllungsversatz in Kopfzeilen nicht korrekt
 Datensatz %lld beschädigt: Datensatzgröße in Kopfzeilen nicht korrekt
 Datensatz %lld beschädigt: Ausgabe-ID Null
 Datensatz %lld beschädigt: Datensatz-Versatz in Kopfzeilen kein Vielfaches der Datensatzgröße
 Verzeichnis umbenennen umbenennen von umbenennen in wird zum Überschreiben neu positioniert
 Wiederherstellen Wiederherstellen
 Wiederherstellung vollständig: %ld Sekunden verstrichen
 Wiederherstellung unterbrochen: %ld Sekunden verstrichen: Könnte später unter Benutzung der Option -%c fortfahren
 Nicht-Verzeichnisdateien werden wiederhergestellt
 Option »resume« (-R) ungeeignet: Keine unterbrochene Stufe %d der Ausgabe zur Wiederaufnahme
 Stufe-%d-Ausgabe von %s wird wieder aufgenommen: %s begann %s
 Stufe %d der inkrementellen Ausgabe von %s wird wieder aufgenommen: %s begann %s (inkrementelle Basisstufe %d begann %s)
 vorher begonnene Wiederherstellung %s wird fortgesetzt
 erneute Synchronisation bei Ino %llu, Versatz %lld erreicht
 neu synchronisiert bei Datensatz %lld, Versatz %u
 wird zurückgespult
 rmdir rmtioctl: IRIX-mtget-Struktur mit falscher Größe - %d erhalten, %d gesucht
 rmtioctl: Linux-mtget-Struktur mit falscher Größe - %d erhalten,
%d oder %d gesucht
 rmtioctl: Ferner Rechnertyp nicht für MTIOCGET unterstützt
 rmtioctl: Ferner Rechnertyp nicht für MTIOCTOP unterstützt
 rmtopen: Ermitteln des fernen Rechnertyps durch Lesen von »%s« fehlgeschlagen
 rmtopen: Ermitteln des fernen Rechnertyps unter Benutzung von »%s« fehlgeschlagen
 rmtopen: ferner Rechnertyp »%s« ist »librmt« unbekannt
 Root %s-Informationen werden gespeichert für: %s
 Medium zur Verzeichnisausgabe wird gesucht
 Medium zur Ausgabe wird gesucht
 sicher es wird nach früherer Ausgabe des Mediendateiverzeichnisses gesucht
 es wird nach früherem Teil der bereits wiederhergestellten Mediendatei gesucht
 wählen Sie ein Laufwerk, um den Medienwechsel zu bestätigen
 ausgewählte Ausgabe basiert nicht auf einer vorher zugrundegelegten Ausgabe
 ausgewählte fortgesetzte Ausgabe ist keine Fortsetzung der vorher zugrundegelegten Ausgabe
 Sitzungs-ID: %s
 Sitzung wird unterbrochen: Bitte warten
 Sitzungsunterbrechungs-Zeitüberschreitung
 Sitzungsbestandsanzeige
 Sitzungsbestand unbekannt
 eingegebene Sitzungskennung: " Sitzungskennung leer gelassen
 Sitzungskennung:  Sitzungskennung: »%s«
 Sitzungszeit: %s Verzeichnis-»extattr« setzen »dirattr« setzen Protokollnachrichtenstufen anzeigen Protokollnachrichtenuntersysteme anzeigen Protokollnachrichtenzeitstempel anzeigen Protokollnachrichtenstufen werden angezeigt
 Protokollnachrichtenuntersysteme werden angezeigt
 Protokollnachrichtenzeitstempel werden angezeigt
 still Überspringen Slave Quelldateisystem nicht angegeben
 angegebenes Ziel %s ist kein Verzeichnis
 Angegebene minimale Stack-Größe ist größer als das Maximun: Minimum ist 0x%llx, Maximum ist 0x%llx
 stat gescheitert %s: %s
 »stat« von %s fehlgeschlagen: %s
 Status- und Steuerungsdialog Status bei %02d:%02d:%02d: %llu/%llu Verzeichnisse rekonstruiert, %.1f%%%% komplett, %llu Verzeichniseinträge verarbeitet, %ld Sekunden verstrichen
 Status bei %02d:%02d:%02d: %llu/%llu Dateien rekonstruiert, %.1f%%%% komplett, %ld Sekunden verstrichen
 Datenstrom %u: Ino %llu Versatz %lld bis  Datenstrom-Terminator gefunden
 Setgid-Bit von %s wird entfernt, da »chown« fehlgeschlagen ist
 Setuid-Bit von %s wird entfernt, da »chown« fehlgeschlagen ist
 Teilbaumargument %s ungültig
 Teilbaum nicht vorhanden Teilbaum-Auswahldialog syssgi( SGI_FS_BULKSTAT ) auf »fsroot« fehlgeschlagen: %s
 Anzeige des Inhaltsverzeichnisses vollständig: %ld Sekunden verstrichen
 Anzeige des Inhaltsverzeichnisses unterbrochen: %ld Sekunden verstrichen
 Bandlaufwerk %s ist nicht bereit (0x%x): neuer Versuch ...
 Band ist schreibgeschützt
 Bandmedienfehler bei Schreiboperation
 Banddatensatz-Prüfsummenfehler
 Beenden
 die folgenden Befehle sind verfügbar:
 die folgende Ausgabe wurde gefunden

 die folgende Ausgabe wurde auf dem Laufwerk %u gefunden

 die folgenden Medienobjekte sind nötig:
 die folgenden Medienobjekte enthalten Mediendateien, bei denen noch nicht versucht wurde, die Verzeichnishierarchie wiederherzustellen:
 diese Ausgabe ist für die interaktive Wiederherstellung ausgewählt
 diese Ausgabe ist für die Wiederherstellung ausgewählt
 Zeitüberschreitung
 tm (%d)	%d
 tmp dir rename dst tmp dir rename src tmp nondir rename dst tmp nondir rename src zu viele -%c-Argumente
 zu viele -%c-Argumente: »-%c %s« bereits angegeben
 Zu alt Nachverfolgung tree_extattr_recurse: Konvertieren von Knoten zu Pfad für %s nicht möglich
 %s Ino %llu Gen %u nicht möglich: Relativer Pfadname zu lang (partiell %s)
 Speicher für I/O-Puffer-Ring konnte nicht reserviert werden
 Autogrow des Knotensegments %u nicht möglich: %s (%d)
 Band kann nicht zurückgesetzt werden: Es wird ein Medienfehler vermutet
 Medium kann nicht zurückgesetzt/-gespult werden
 %s Ino %llu %s kann nicht verbunden werden: %s: Wird verworfen
 »chown %s« nicht möglich: %s
 es kann kein Dateisystem-Handle für %s konstruiert werden: %s
 %s Ino %llu %s kann nicht erstellt werden: %s: Wird verworfen
 %s kann nicht erstellt werden: %s
 Symlink Ino %llu %s kann nicht erstellt werden: %s: Wird verworfen
 Symlink Ino %llu %s kann nicht erstellt werden: Src zu lang: Wird verworfen
 aktuelles Verzeichnis kann nicht bestimmt werden: %s
 Rechnername kann nicht bestimmt werden: %s
 UUID des Dateisystems, das %s enthält, kann nicht festgestellt werden: %s
 UUID des bei %s eingehängten Dateisystems kann nicht bestimmt werden:%s
 Verzeichnis %llu Eintrag %s (%llu) kann nicht ausgegeben werden: Name zu lang
 Verzeichnis kann nicht ausgegeben werden: Ino %llu zu groß
 Basisausgabe kann nicht im Bestand gefunden werden, um Ausgabe zu überprüfen
 Sitzungsbestand kann nicht zur Ausgabe geholt werden
 Status von %s kann nicht geholt werden: %s
 Status des -%c-Arguments %s kann nicht geholt werden: %s
 nächste Markierung in Mediendatei kann nicht gefunden werden
 weiche Beschränkung der Stack-Größe kann nicht von 0x%llx auf 0x%llx verringert werden
 %s-hdr kann nicht abgebildet werden: %s
 %s kann nicht abgebildet werden: %s
 Knoten-»hdr« der Größe %d kann nicht abgebildet werden: %s
 »mmap« für Hash-Array in %s nicht möglich: %s
 %s kann nicht geöffnet werden: %s
 Verzeichnis %s kann nicht geöffnet werden
 Verzeichnis kann nicht ausgegeben werden: Ino %llu: %s
 Bestand kann nicht geöffnet werden, um Ausgabe zu überprüfen
 kann nicht überschrieben werden
 harte Beschränkung der Stack-Größe kann nicht von 0x%llx auf 0x%llx erhöht werden
 weiche Beschränkung der Stack-Größe kann nicht von 0x%llx auf 0x%llx erhöht werden
 Dirents (%d) für Verzeichnis-Ino %llu können nicht gelesen werden: %s
 %s kann nicht wieder erstellt werden: %s
 %s kann nicht entfernt werden: %s
 Verzeichnis %s kann nicht in %s umbenannt werden: %s
 »nondir« %s kann nicht in %s umbenannt werden: %s
 »nondir %s« kann nicht zu »nondir %s« umbenannt werden: %s
 Mediendatei kann nicht erneut synchronisiert werden: Einige Teile der Ausgabe werden NICHT wiederhergestellt
 »rmdir %s« kann nicht ausgeführt werden: %s
 »rmdir %s« kann nicht ausgeführt werden: Nicht leer
 erweitertes Attribut %s für %s kann nicht gesetzt werden: %s (%d)
 Zugriffs- und Veränderungszeiten von %s können nicht gesetzt werden: %s
 Blockgröße kann nicht auf %d gesetzt werden
 Modus von %s kann nicht gesetzt werden: %s
 Abfragen von %s mit »stat« nicht möglich: %s
 Setuid/Setgid auf %s konnte nicht entfernt werden, Datei wird gelöscht.
 %s kann nicht gelöscht werden: %s
 aktuelle Datei kann nicht gelöscht werden bevor %s mit %s verknüpft wird: %s
 aktuelle Datei kann nicht vor der Wiederherstellung gelöscht werden: %s: %s: Wird verworfen
 »nondir %s« kann nicht gelöscht werden: %s
 %s kann nicht als Quelle für Hard-Link benutzt werden
 Dateimarkierung bei EOD (Datenende) kann nicht geschrieben werden: %s (%d)
 unerwarteter EIO-Fehler beim Versuch den Datensatz zu lesen
 unerwarteter Fehler beim Versuch den Datensatz %lld zu lesen: Lesen gab %d zurück, Fehlernummer %d (%s)
 unerwarteter Fehler beim Versuch den Datensatz zu lesen: Lesen gab %d zurück, Fehlernummer %d (%s)
 unerwarteter Fehler beim Versuch Datensatz zu lesen: Lesen gab %d zurück, Fehlernummer %s (%s)
 unerwarteter Fehler von »do_begin_read«: %d
 unerwarteter Bandfehler: Fehlernummer %d nread %d blksz %d recsz %d 
 unerwarteter Bandfehler: Fehlernummer %d nread %d blksz %d recsz %d isvar %d wasatbot %d eod %d fmk %d eot %d onl %d wprot %d ew %d 
 unerwartetes EOD (Datenende) bei BOT (Massenübertragung) vorgefunden:
Medium vermutlich beschädigt
 eine unerwartete Dateimarkierung vorgefunden: Medium vermutlich beschädigt
 unbekannter Inode-Typ: Ino=%llu, Modus=0x%04x 0%06o
 löschen Laufwerks-Strategie-ID nicht erkannt (laut Medium %d, erwartet wurde %d)
 Dateikopfzeilenversion (%d) nicht erkannt
 Aktualisieren von »dirattr« fehlgeschlagen: %s
 benutzen Sie zum Wiederherstellen von Quotas »xfs_quota«
 Benutzung von QIC-Laufwerken über Geräteknoten variabler Blockgröße wird nicht unterstützt
 Strategie %s wird benutzt
 Online-Sitzungsbestand wird benutzt
 gültiger Datensatz %lld, aber keine Markierung
 detailliert Datenträger: %s
 es wird auf die synchronisierte Sitzungsbestandsausgabe gewartet
 wird zurückgespult und erneut versucht
 win_map(): Es wird versucht ein anderes »win_t« auszuwählen
 win_map(): Ein Knotensegment der Größe %d bei %d kann nicht abgebildet werden: %s
 Schreiben des »dirattr«-Puffers fehlgeschlagen: %s
 Schreiben des »dirattr«-Puffers fehlgeschlagen: Erwartet %ld zu schreiben, tatsächlich %ld
 Schreiben des »namereg«-Puffers fehlgeschlagen: %s
 Schreiben des »namereg«-Puffers fehlgeschlagen: Erwartet %ld zu schreiben, tatsächlich %ld
 Schreiben auf %s fehlgeschlagen: %d (%s)
 Dateimarkierung bei EOD (Datenende) wird geschrieben
 Datenstrom-Terminator wird geschrieben
 