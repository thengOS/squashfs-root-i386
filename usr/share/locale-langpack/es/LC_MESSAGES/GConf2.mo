��    �     T  �  �      �(     �(     �(  "   �(  C   �(     6)  *   E)  �   p)  k   �)  g   b*  7   �*  6   +     9+  2   P+  F   �+  6   �+     ,  !   ,  9   7,  '   q,  1   �,     �,     �,     �,     -  c   -     �-  7   �-  '   �-  ,   �-  /   .  #   M.  (   q.  F   �.  '   �.  .   	/  )   8/  ,   b/     �/     �/     �/     �/     �/     �/     0     70     G0  *   a0  :   �0  -   �0  $   �0  #   1  >   >1  %   }1  1   �1  &   �1  !   �1  !   2  2   @2  9   s2  4   �2     �2     �2     3  0   33  8   d3     �3  /   �3  '   �3  G   4  !   P4  1   r4  ;   �4     �4     �4  *   �4  0   '5     X5  )   k5  7   �5  7   �5  )   6  -   /6  /   ]6  2   �6  >   �6  >   �6  1   >7  3   p7  1   �7  3   �7  1   
8     <8  ?   W8  )   �8  E   �8     9      9  Y   <9  &   �9  &   �9  1   �9  2   :      I:  -   j:  :   �:  $   �:     �:     ;  (   .;     W;  $   r;  !   �;      �;  #   �;     �;     <  ,   5<  R   b<  &   �<     �<     �<     =  "   2=     U=  !   r=  )   �=  $   �=  !   �=      >     &>     >>  $   W>  1   |>     �>     �>     �>     �>     ?  g   ?     �?  
   �?     �?  A   �?  !   �?     @     0@     G@  #   \@     �@  .   �@     �@     �@     �@     A     A  +   A  -   GA  ,   uA      �A     �A  J   �A  -   ,B  !   ZB  �   |B  1   7C     iC     �C     �C     �C  "   �C  2   D      6D     WD  %   lD  .   �D  /   �D  '   �D  /   E  !   IE  0   kE     �E     �E     �E     �E  |   F  �   �F  }   7G  }   �G  y   3H  2   �H  =   �H     I     7I  \   PI  #   �I     �I  :   �I     *J  (   DJ  #   mJ  !   �J  $   �J  K   �J     $K     CK  6   _K     �K  )   �K  &   �K  $   �K     $L  $   >L  N   cL  2   �L     �L  0   M  +   4M  $   `M  .   �M  >   �M  E   �M  I   9N     �N  *   �N  0   �N  9   �N     *O  <   DO  !   �O  "   �O  .   �O  )   �O  "   P     BP     TP     tP  +   �P     �P  $   �P  "   �P  "   Q  &   ?Q  %   fQ  "   �Q     �Q     �Q  &   �Q  "   R  *   /R  "   ZR  ,   }R  !   �R  -   �R  %   �R      S  '   >S     fS     �S  4   �S     �S  @   �S     -T     9T     TT  *   fT  W   �T     �T     U  3   U     EU  !   cU  ?   �U     �U  _   �U     CV     VV  *   eV  5   �V  +   �V  +   �V     W  6   :W  )   qW  3   �W  /   �W  "   �W  '   "X  *   JX  &   uX  C   �X  )   �X  2   
Y  4   =Y  )   rY     �Y  .   �Y  B   �Y  :   (Z  :   cZ  +   �Z  4   �Z  (   �Z  '   ([      P[  *   q[  !   �[  5   �[  !   �[     \     4\     G\     g\  +   �\  Q   �\  _   �\  T   ^]  0   �]  '   �]     ^     &^     @^  &   S^     z^     �^     �^  i   �^     5_  !   N_      p_     �_  !   �_  2   �_  7   �_  
   $`  C   /`  C   s`     �`     �`  )   �`  D   �`  (   Da  0   ma  ,   �a     �a  8   �a  5   b  �   Hb  �   9c  1   d  /   ?d     od  %   �d  .   �d  (   �d  &   
e  R   1e  0   �e  ^   �e  Q   f  H   ff  G   �f  (   �f  3    g     Tg  -   lg  F   �g  (   �g     
h  8   h     Jh     hh  L   xh  O   �h     i  &   4i  (   [i     �i     �i  /   �i  O   �i     *j     :j     Nj     hj     ~j     �j     �j     �j  <   �j     k      k  K   :k  B   �k  %   �k  5   �k  8   %l     ^l  G   |l  w   �l  w   <m  s   �m  n   (n     �n     �n     �n  /   �n  ,   �n     &o  p   =o  w   �o  E   &p  S   lp  &   �p  &   �p  -   q  .   <q     kq  	   yq  (   �q     �q  o   �q  K   :r  w  �r  )   �v     (w     6w  5   Kw  0   �w     �w  %   �w     �w  "   �w     x  �   0x     �x  P    y  1   Qy  B   �y  ,   �y  <   �y  *   0z  (   [z  +   �z  .   �z  5   �z  F   {  F   \{     �{  ;   �{  8   �{  (   6|  :   _|  :   �|  A   �|  6   }  1   N}  B   �}  G   �}  J   ~  .   V~  ;   �~  9   �~  9   �~  :   5  %   p  -   �  "   �  -   �  1   �  8   G�  ;   ��  G   ��      �  8   %�  Q   ^�     ��  ?   ́  3   �  @   @�  "   ��  S   ��     ��     �     .�  A   3�  '   u�  �  ��     ~�  
   ��  )   ��  I   ��     �  8   �  �   S�  f   ؆  b   ?�  >   ��  ?   �      !�  C   B�  X   ��  E   ߈     %�  +   B�  E   n�  0   ��  >   �     $�     :�     U�  "   t�  �   ��     �  G   (�  =   p�  E   ��  4   �  .   )�  1   X�  I   ��  =   Ԍ  F   �  ,   Y�  .   ��  *   ��  #   ��  -   �     2�     C�  )   T�  (   ~�     ��  0   ��  6   �  N   %�  =   t�  0   ��  .   �  C   �  0   V�  D   ��  3   ̐  &    �  &   '�  ;   N�  M   ��  @   ؑ  *   �  &   D�  '   k�  D   ��  A   ؒ     �  =   6�  4   t�  Z   ��  3   �  C   8�  L   |�     ɔ     ܔ  2   �  5   �     R�  4   l�  @   ��  <   �  3   �  9   S�  >   ��  =   ̖  F   
�  J   Q�  >   ��  =   ۗ  ;   �  :   U�  A   ��  !   Ҙ  M   ��  9   B�  X   |�  $   ՙ  "   ��  i   �  0   ��  (   ��  9   �  9   �  '   U�  9   }�  ;   ��  /   �  ,   #�  )   P�  9   z�  *   ��  0   ߜ  *   �  *   ;�  ,   f�  '   ��  "   ��  5   ޝ  l   �  0   ��  &   ��  '   ٞ     �  '   �      C�  (   d�  8   ��  /   Ɵ  /   ��  -   &�     T�  !   r�  (   ��  F   ��     �     �     7�     U�  ,   s�  �   ��  '   3�  
   [�     f�  U   r�  3   Ȣ  1   ��  .   .�  /   ]�  B   ��  .   У  K   ��  ,   K�  0   x�  0   ��     ڤ     �  :   �  :   $�  5   _�  &   ��  &   ��  u   �  6   Y�  -   ��  �   ��  ;   ��  #   ۧ  %   ��     %�     D�  0   b�  ?   ��  ,   Ө      �  6   �  A   T�  O   ��  1   �  >   �  +   W�  9   ��     ��  %   ڪ  &    �  %   '�  �   M�  �   ګ  �   ��  �   1�  �   ȭ  9   s�  K   ��     ��     �  {   1�  0   ��  (   ޯ  B   �     J�  6   h�  3   ��  '   Ӱ  (   ��  S   $�  *   x�     ��  @   ��  -    �  +   .�  -   Z�  =   ��     Ʋ  *   �  u   �  L   ��  (   ӳ  <   ��  ;   9�  -   u�  5   ��  T   ٴ  R   .�  U   ��     ׵  /   �  C   �  D   `�  !   ��  S   Ƕ  *   �  -   F�  3   t�  ;   ��  *   �     �  .   !�  !   P�  4   r�      ��  .   ȸ  *   ��  &   "�  5   I�  ,   �  8   ��  (   �     �  C   (�  9   l�  3   ��  C   ں  <   �  :   [�  <   ��  3   ӻ  '   �  8   /�  (   h�     ��  ?   ��     �  J   �     Z�  !   k�     ��  :   ��  j   �     N�     l�  ;   �  1   ��  -   ��  B   �     ^�  m   }�     �     �  4   �  :   T�  0   ��  0   ��  !   ��  F   �  :   Z�  G   ��  =   ��  -   �  5   I�  2   �  2   ��  Y   ��  0   ?�  A   p�  E   ��  ?   ��  '   8�  2   `�  J   ��  H   ��  @   '�  3   h�  O   ��  A   ��  -   .�  )   \�  8   ��  8   ��  D   ��  8   =�  4   v�     ��  (   ��  +   ��  ;   �  q   Z�  y   ��  e   F�  @   ��  ;   ��  ,   )�  *   V�     ��  ,   ��  /   ��  *   ��  -   &�  �   T�     ��  +   ��      '�     H�  4   O�  ?   ��  H   ��     �  B   �  B   \�     ��     ��  2   ��  J   ��  -   @�  >   n�  6   ��     ��  G   ��  F   =�  !  ��  )  ��  1   ��  1   �  +   4�  9   `�  @   ��  E   ��  <   !�  f   ^�  =   ��  _   �  P   c�  c   ��  ^   �  ;   w�  0   ��     ��  3   �  c   6�  A   ��     ��  F   ��  $   *�     O�  [   d�  Y   ��      �  &   ;�  1   b�     ��  $   ��  =   ��  Y   �     h�     ��      ��  !   ��  "   ��     �     �     =�  8   X�      ��  -   ��  Q   ��  W   2�  /   ��  >   ��  A   ��  #   ;�  \   _�  �   ��  �   F�  �   ��  x   T�     ��     ��  "   ��  ;   �  6   G�     ~�  z   ��  �   �  R   ��  _   �  1   f�  1   ��  .   ��  /   ��     )�  	   =�  1   G�  ,   y�     ��  Y   &�  W  ��  ;   ��     �     &�  D   ?�  <   ��  )   ��  7   ��     #�  =   2�     p�  �   �  $   H�  Z   m�  >   ��  K   �  D   S�  S   ��  G   ��  E   4�  K   z�  6   ��  I   ��  L   G�  I   ��  "   ��  W   �  V   Y�  H   ��  Y   ��  S   S�  H   ��  A   ��  >   2�  R   q�  K   ��  c   �  2   t�  H   ��  L   ��  L   =�  N   ��  7   ��  6   �  &   H�  /   o�  2   ��  J   ��  D   �  K   b�  $   ��  8   ��  V   �      c�  U   ��  A   ��  T   �  '   q�  k   ��     �     �     ;�  X   C�  0   ��     S   �   �                 �  �          �   �   6   �   j          $      �   R  <              �  �       �  �   �   n    J  �  �      )  �   U              �  `                      �      �  w              �       t      �  �  c          �   �  %   �   6            �                �  0   �          d  }   �   #  @  a     �  +   �  v   /  /   �  �  f  �  �   |  �  h  7   �       �      �       �      �   �      �              ;   �  1   �     %  q       �   �  ]   �  �  R   �   a   �  �   �   .   �  7  O  t   y      �   8             P  y   X  �  >   �  �   �         K      1  �  [   �   V  Z   �  E           �  +         `      u   i          H       �  �   8  l      (  u  m  �     _   z   	  L  G       "   T   �         P       X       r   �   �   �  :            =   �   �       �  C      �  �   �     �   �            �  G  �   i   �  m   D  �       '   B  �   �  �  �  2   _  �  �   �   �     �   �  H  �    �       Y   )   T  �          5  $    �  �   �     E  �   h     !   �      �      �  �      �  j  �      �   M   �   ?      �   �     �   }      �      �     w   5     &       �          �  ,          �   9   �       �       �   �       Q   �   �       �                    �          F           �       '  �   �      �  �  C   �   9  �           �  �   �      	   �  q      �  s  �  �      @      K   �   c       �  �   �      �          ,   {   �  W       .          g  �  g       �       \       �    �   ;  x   b   2  �  e  �  4   �   �          �             n   �  �   x    �   W  *  �       Z     �   U  ?      !  �  4      �  o   k         �  e   I   �       <               �  �   [      A    �   F   �   ]     �         #       |   �   �          �   z  B      �   �       �  �       �   
  p   &  �   0  Q  b  �   �       \      �                �   �     �   �   d           v  N  o  :   *   �           �  �   �       S  l   �     �   �  >  -  �                  M    �         ^              �  �  �   ~   V       �                   p  �   J           �   �   O           k     �  ^   A   �           �  {              �  -   
   ~  D       �  �   (   �   �   �   �  L          f       �       �   �   �  "    N   �           Y      �  I      �  �           �   �      =  r          �  �   �   �           �   �   �           �  �   s   �  �       3   �   3  �   �            
%s
 "%s": %s %s option must be used by itself.
 '\%o' is not an ASCII character and thus isn't allowed in key names (no value set) - Tool to manipulate a GConf configuration --ignore-schema-defaults is only relevant with --get, --all-entries, --dump, --recursive-list, --get-list-size or --get-list-element
 --recursive-list should not be used with --get, --set, --unset, --all-entries, --all-dirs, or --search-key
 --set_schema should not be used with --get, --set, --unset, --all-entries, --all-dirs, or --search-key
 <%s> provided but current element does not have type %s <%s> provided but parent <entry> does not have a value <li> has wrong type %s A node has unknown "type" attribute `%s', ignoring A toplevel node in XML file `%s' is <%s> rather than <entry>, ignoring Adding client to server's list failed, CORBA error: %s Adding source `%s'
 Attached schema `%s' to key `%s'
 Attribute "%s" is invalid on <%s> element in this context Backend `%s' failed to return a vtable
 Backend `%s' missing required vtable member `%s'
 Bad XML node: %s Bad address `%s' Bad address `%s': %s Bad key or directory name Bypass server, and access the configuration database directly. Requires that gconfd is not running. CORBA error: %s Can't add notifications to a local configuration source Can't get and set/unset simultaneously
 Can't get type and set/unset simultaneously
 Can't have a period '.' right after a slash '/' Can't have two slashes '/' in a row Can't overwrite existing read-only value Can't read from or write to the XML root directory in the address "%s" Can't set and get/unset simultaneously
 Can't toggle and get/set/unset simultaneously
 Can't use --all-dirs with --get or --set
 Can't use --all-entries with --get or --set
 Can't write to file `%s': %s Cannot find directory %s
 Cannot set schema as value
 Car Type: %s
 Cdr Type: %s
 Change GConf mandatory values Change GConf system values Client options: Config file '%s' is empty Configuration server couldn't be contacted Contacting LDAP server: host '%s', port '%d', base DN '%s' Corrupt data in configuration source database Could not connect to session bus: %s Could not connect to system bus: %s Could not create file '%s', probably because it already exists Could not flush file '%s' to disk: %s Could not flush saved state file '%s' to disk: %s Could not lock temporary file '%s': %s Could not make directory "%s": %s Could not make directory `%s': %s Could not move aside old saved state file '%s': %s Could not open lock directory for %s to remove locks: %s
 Could not open saved state file '%s' for writing: %s Could not remove "%s": %s
 Could not remove file %s: %s
 Could not stat `%s': %s Could not write saved state file '%s' fd: %d: %s Couldn't find the XML root directory in the address `%s' Couldn't get value Couldn't interpret CORBA value for list element Couldn't locate backend module for `%s' Couldn't make sense of CORBA value received in set request for key `%s' Couldn't open path file `%s': %s
 Couldn't resolve address for configuration source Created Evolution/LDAP source using configuration file '%s' D-BUS error: %s DESCRIPTION Daemon failed to acquire gconf service:
%s Daemon failed to connect to the D-BUS daemon:
%s Default Value: %s
 Didn't find car and cdr for XML pair node Didn't understand XML node <%s> inside an XML list node Didn't understand XML node <%s> inside an XML pair node Didn't understand `%s' (expected integer) Didn't understand `%s' (expected real number) Didn't understand `%s' (expected true or false) Didn't understand `%s' (extra trailing characters) Didn't understand `%s' (extra unescaped ')' found inside pair) Didn't understand `%s' (extra unescaped ']' found inside list) Didn't understand `%s' (list must end with a ']') Didn't understand `%s' (list must start with a '[') Didn't understand `%s' (pair must end with a ')') Didn't understand `%s' (pair must start with a '(') Didn't understand `%s' (wrong number of elements) Directory operation on key Directory/file permissions for XML source at root %s are: %o/%o Document `%s' has no top level <%s> node
 Document `%s' has the wrong type of root node (<%s>, should be <%s>)
 Document `%s' is empty?
 Don't understand type `%s'
 Dump to standard output an XML description of all entries under a directory, recursively. Duplicate entry `%s' in `%s', ignoring Element <%s> is not allowed below <%s> Element <%s> is not allowed inside a <%s> element Element <%s> is not allowed inside current element Encoded value is not valid UTF-8 Entry with no name in XML file `%s', ignoring Error associating schema name '%s' with key name '%s': %s
 Error checking existence of `%s': %s Error compiling regex: %s
 Error finding metainfo: %s Error getting default value for `%s': %s Error getting metainfo: %s Error getting new value for "%s": %s Error getting schema at '%s': %s
 Error getting value for `%s': %s Error initializing module `%s': %s
 Error listing dirs in `%s': %s Error listing dirs: %s
 Error loading some configuration sources: %s Error obtaining new value for `%s' after change notification from backend `%s': %s Error obtaining new value for `%s': %s Error opening module `%s': %s
 Error querying LDAP server: %s Error reading "%s": %s
 Error reading saved state file: %s Error releasing lockfile: %s Error removing directory "%s": %s Error removing schema name from '%s': %s
 Error saving GConf tree to '%s': %s
 Error setting schema for `%s': %s Error setting value for `%s': %s Error setting value: %s Error setting value: %s
 Error syncing configuration data: %s Error syncing the XML backend directory cache: %s Error syncing: %s Error syncing: %s
 Error unsetting "%s": %s Error unsetting `%s': %s Error unsetting `%s': %s
 Error while parsing options: %s.
Run '%s --help' to see a full list of available command line options.
 Error writing file "%s": %s Error: %s
 Exiting Expected (%s,%s) pair, got a pair with one or both values missing Expected `%s' got `%s' for key %s Expected bool, got %s Expected float, got %s Expected int, got %s Expected list of %s, got list of %s Expected list, got %s Expected pair of type (%s,%s) got type (%s,%s) Expected pair, got %s Expected schema, got %s Expected string, got %s FILENAME Failed Failed reading default value for schema: %s Failed to access configuration source(s): %s
 Failed to activate configuration server: %s
 Failed to clean up file '%s': %s Failed to close file `%s': %s Failed to close gconfd logfile; data may not have been properly saved (%s) Failed to close new saved state file '%s': %s Failed to contact LDAP server: %s Failed to contact configuration server; the most common cause is a missing or misconfigured D-Bus session bus daemon. See http://projects.gnome.org/gconf/ for information. (Details - %s) Failed to convert IOR '%s' to an object reference Failed to convert object to IOR Failed to create file `%s': %s Failed to create or open '%s' Failed to delete "%s": %s Failed to delete old file `%s': %s Failed to flush client add to saved state file: %s Failed to get IOR for client: %s Failed to get a lock Failed to get all entries in `%s': %s Failed to get bus name for daemon, exiting: %s Failed to get configuration file path from '%s' Failed to get connection to session: %s Failed to get object reference for ConfigServer Failed to get value for `%s': %s
 Failed to give up lock on XML directory "%s": %s Failed to init GConf: %s
 Failed to link '%s' to '%s': %s Failed to load file "%s": %s Failed to load source "%s": %s Failed to lock '%s': probably another process has the lock, or your operating system has NFS file locking misconfigured (%s) Failed to log addition of listener %s (%s); will not be able to restore this listener on gconfd restart, resulting in unreliable notification of configuration changes. Failed to log addition of listener to gconfd logfile; won't be able to re-add the listener if gconfd exits or shuts down (%s) Failed to log removal of listener to gconfd logfile; might erroneously re-add the listener if gconfd exits or shuts down (%s) Failed to log removal of listener to logfile (most likely harmless, may result in a notification weirdly reappearing): %s Failed to move new saved state file into place: %s Failed to move temporary file "%s" to final location "%s": %s Failed to open "%s": %s
 Failed to open `%s': %s
 Failed to open gconfd logfile; won't be able to restore listeners after gconfd shutdown (%s) Failed to open saved state file: %s Failed to parse XML file "%s" Failed to register server object with the D-BUS bus daemon Failed to remove '%s': %s Failed to remove lock directory `%s': %s Failed to remove lock file `%s': %s Failed to rename `%s' to `%s': %s Failed to restore `%s' from `%s': %s Failed to restore original saved state file that had been moved to '%s': %s Failed to set mode on `%s': %s Failed to shut down backend Failed to spawn the configuration server (gconfd): %s
 Failed to stat `%s': %s Failed to sync XML cache contents to disk Failed to sync one or more sources: %s Failed to unset breakage key %s: %s
 Failed to write "%s": %s
 Failed to write XML data to `%s': %s Failed to write byte to pipe file descriptor %d so client program may hang: %s Failed to write client add to saved state file: %s Failed to write file `%s': %s Failed to write some configuration data to disk
 Failure during recursive unset of "%s": %s
 Failure listing entries in `%s': %s
 Failure shutting down configuration server: %s Fatal error: failed to get object reference for ConfigDatabase GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL is set, not installing schemas
 GCONF_DISABLE_MAKEFILE_SCHEMA_UNINSTALL is set, not uninstalling schemas
 GConf Error: %s
 GConf server is not in use, shutting down. GConf warning: failure listing pairs in `%s': %s GConf won't work without dynamic module support (gmodule) GSettings Data Conversion Get a specific element from a list key, numerically indexed. Get the long doc string for a key Get the name of the default source Get the name of the schema applied to this key Get the number of elements in a list key. Get the short doc string for a key GetIOR failed: %s Got %d entries using filter: %s Got a malformed message. Ignore schema defaults when reading values. Ignoring XML node `%s': %s Ignoring XML node with name `%s': %s Ignoring bad car from XML pair: %s Ignoring bad cdr from XML pair: %s Ignoring schema name `%s', invalid: %s Incorrect type for list element in %s Initializing Markup backend module Initializing XML backend module Installation options: Installed schema `%s' for locale `%s'
 Integer `%s' is too large or small Invalid UTF-8 in gettext domain for schema Invalid UTF-8 in locale for schema Invalid UTF-8 in long description for schema Invalid UTF-8 in owner for schema Invalid UTF-8 in short description for schema Invalid UTF-8 in string value in '%s' Invalid cdr_type "%s" on <%s> Invalid first-element type "%s" on <%s> Invalid list_type "%s" on <%s> Invalid ltype "%s" on <%s> Invalid type (list, pair, or unknown) in a list node Key %s is not a list.
 Key `%s' listed as schema for key `%s' actually stores type `%s' Key is NULL Key operation on directory Key type options: Key/directory may not end with a slash '/' Launch the configuration server (gconfd). (Normally happens automatically when needed.) Line %d character %d: %s List Type: %s
 List contains a badly-typed node (%s, should be %s) List index is out of bounds.
 List index must be non-negative.
 List type must be a primitive type: string, int, float or bool
 Listener ID %lu doesn't exist Load from the specified file an XML description of values and set them relative to a directory. Load/Save options: Long Desc: %s
 Migrates user settings from GConf to dconf Missing both car and cdr values from pair in XML file Missing car from pair of values in XML file Missing cdr from pair of values in XML file Must begin with a slash '/' Must set the GCONF_CONFIG_SOURCE environment variable
 Must specify a PCRE regex to search for.
 Must specify a key from which to get list element.
 Must specify a key or keys on the command line
 Must specify a key or keys to get
 Must specify a key or keys to get type
 Must specify a key pattern to search for.
 Must specify a key to lookup size of.
 Must specify a schema name followed by the key name to apply it to
 Must specify a type when setting a value
 Must specify alternating keys/values as arguments
 Must specify key (schema name) as the only argument
 Must specify keys to unapply schema from
 Must specify list index.
 Must specify one or more directories to dump.
 Must specify one or more directories to get key/value pairs from.
 Must specify one or more directories to get subdirs from.
 Must specify one or more directories to recursively list.
 Must specify one or more keys as arguments
 Must specify one or more keys to recursively unset.
 Must specify one or more keys to unset.
 Must specify some directories to break
 Must specify some keys to break
 Must specify some schema files to install
 No "%s" attribute on element <%s> No "filter" attribute specified on <template> in '%s' No "type" attribute for <%s> node No "value" attribute for node No '/' in key "%s" No <template> specified in '%s' No D-BUS daemon running
 No LDAP server or base DN specified in '%s' No configuration files found. Trying to use the default configuration source `%s' No configuration source addresses successfully resolved. Can't load or store configuration data No configuration sources in the source path. Configuration won't be saved; edit %s%s No database available to save your configuration No doc string stored in schema at '%s'
 No schema known for `%s'
 No schema stored at '%s'
 No such file `%s'
 No text is allowed inside element <%s> No value found for key %s
 No value set for `%s'
 No value to set for key: `%s'
 No writable configuration sources successfully resolved. May be unable to save some configuration changes Not a boolean value: %s
 Not running within active session Notification on %s doesn't exist OWNER Object Activation Framework error Operation not allowed without configuration server Outermost element in menu file must be <gconf> not <%s> Owner: %s
 Pair car type must be a primitive type: string, int, float or bool
 Pair cdr type must be a primitive type: string, int, float or bool
 Parse error Permission denied Print all key/value pairs in a directory. Print all subdirectories and entries under a directory, recursively. Print all subdirectories in a directory. Print the data type of a key to standard output. Print the value of a key to standard output. Print version Privileges are required to change GConf mandatory values Privileges are required to change GConf system values Properly installs schema files on the command line into the database. Specify a custom configuration source in the GCONF_CONFIG_SOURCE environment variable, or set set the variable to an empty string to use the default configuration source. Properly uninstalls schema files on the command line from the database. GCONF_CONFIG_SOURCE environment variable should be set to a non-default configuration source or set to the empty string to use the default. Quoted string doesn't begin with a quotation mark Quoted string doesn't end with a quotation mark Read error on file `%s': %s
 Received invalid value in set request Received list from gconfd with a bad list type Received request to drop all cached data Received request to sync synchronously Recursively unset all keys at or below the key/directory names on the command line Remove any schema name applied to the given keys Remove directory operation is no longer supported, just remove all the values in the directory Resolved address "%s" to a partially writable configuration source at position %d Resolved address "%s" to a read-only configuration source at position %d Resolved address "%s" to a writable configuration source at position %d Return 0 if gconfd is running, 2 if not. Return 0 if the directory exists, 2 if it does not. Returning exception: %s Root node of '%s' must be <evoldap>, not <%s> Run '%s --help' to see a full list of available command line options.
 SIGHUP received, reloading all databases SOURCE Schema `%s' specified for `%s' stores a non-schema value Schema contains invalid UTF-8 Schema options: Schema specifies type list but doesn't specify the type of the list elements Schema specifies type pair but doesn't specify the type of the car/cdr elements Search for a key, recursively. Searching for entries using filter: %s Server couldn't resolve the address `%s' Server options: Server ping error: %s Set a key to a value and sync. Use with --type. Set a schema and sync. Use with --short-desc, --long-desc, --owner, and --type. Short Desc: %s
 Show client options Show installation options Show key type options Show load/save options Show schema options Show server options Show test options Shut down gconfd. DON'T USE THIS OPTION WITHOUT GOOD REASON. Shutdown error: %s
 Shutdown request received Some client removed itself from the GConf server when it hadn't been added. Specify a configuration source to use rather than the default path Specify a schema file to be installed Specify a several-line description to go in a schema. Specify a short half-line description to go in a schema. Specify the owner of a schema Specify the schema name followed by the key to apply the schema name to Specify the type of the car pair value being set, or the type of the value a schema describes. Unique abbreviations OK. Specify the type of the cdr pair value being set, or the type of the value a schema describes. Unique abbreviations OK. Specify the type of the list value being set, or the type of the value a schema describes. Unique abbreviations OK. Specify the type of the value being set, or the type of the value a schema describes. Unique abbreviations OK. Success Test options: Text contains invalid UTF-8 The '/' name can only be a directory, not a key The GConf daemon is currently shutting down. Toggles a boolean key. Torture-test an application by setting and unsetting a bunch of keys inside the directories on the command line. Torture-test an application by setting and unsetting a bunch of values of different types for keys on the command line. Trying to break your application by setting bad values for key:
  %s
 Trying to break your application by setting bad values for keys in directory:
  %s
 Two <car> elements given for same pair Two <cdr> elements given for same pair Two <default> elements below a <local_schema> Two <longdesc> elements below a <local_schema> Type mismatch Type: %s
 Unable to open saved state file '%s': %s Unable to parse XML file '%s' Unable to remove directory `%s' from the XML backend cache, because it has not been successfully synced to disk Unable to restore a listener on address '%s', couldn't resolve the database Unable to store a value at key '%s', as the configuration server has no writable databases. There are some common causes of this problem: 1) your configuration path file %s/path doesn't contain any databases or wasn't found 2) somehow we mistakenly created two gconfd processes 3) your operating system is misconfigured so NFS file locking doesn't work in your home directory or 4) your NFS client machine crashed and didn't properly notify the server on reboot that file locks should be dropped. If you have two gconfd processes (or had two at the time the second was launched), logging out, killing all copies of gconfd, and logging back in may help. If you have stale locks, remove ~/.gconf*/*lock. Perhaps the problem is that you attempted to use GConf from two machines at once, and ORBit still has its default configuration that prevents remote CORBA connections - put "ORBIIOPIPv4=1" in /etc/orbitrc. As always, check the user.* syslog for details on problems gconfd encountered. There can only be one gconfd per home directory, and it must own a lockfile in ~/.gconfd and also lockfiles in individual storage locations such as ~/.gconf Uninstalled schema `%s' from locale `%s'
 Unknown error Unknown error %s: %s Unknown value "%s" for "%s" attribute on element <%s> Unload a set of values described in an XML file. Unloading XML backend module. Unloading text markup backend module. Unset Unset the keys on the command line Usage: %s <dir>
 Usage: %s <dir>
  Merges a markup backend filesystem hierarchy like:
    dir/%%gconf.xml
        subdir1/%%gconf.xml
        subdir2/%%gconf.xml
  to:
    dir/%%gconf-tree.xml
 Value at '%s' is not a schema
 Value for `%s' set in a read-only source at the front of your configuration path Value type is only relevant when setting a value
 WARNING: <locale> node has no `name="locale"' attribute, ignoring
 WARNING: Failed to parse boolean value `%s'
 WARNING: Failed to parse default value `%s' for schema (%s)
 WARNING: Failed to parse float value `%s'
 WARNING: Failed to parse int value `%s'
 WARNING: Failed to parse string value `%s'
 WARNING: Invalid node <%s> in a <locale> node
 WARNING: You cannot set a default value for a schema
 WARNING: car_type can only be int, float, string or bool and not `%s'
 WARNING: cdr_type can only be int, float, string or bool and not `%s'
 WARNING: empty <applyto> node WARNING: failed to associate schema `%s' with key `%s': %s
 WARNING: failed to install schema `%s', locale `%s': %s
 WARNING: failed to parse type name `%s'
 WARNING: failed to uninstall schema `%s', locale `%s': %s
 WARNING: gconftool internal error, unknown GConfValueType
 WARNING: invalid or missing car_type or cdr_type for schema (%s)
 WARNING: invalid or missing list_type for schema (%s)
 WARNING: invalid or missing type for schema (%s)
 WARNING: key specified (%s) for schema under a <value> - ignoring
 WARNING: list_type can only be int, float, string or bool and not `%s'
 WARNING: multiple <locale> nodes for locale `%s', ignoring all past first
 WARNING: must have a child node under <value>
 WARNING: must specify both a <car> and a <cdr> in a <pair>
 WARNING: no <car_type> specified for schema of type pair
 WARNING: no <cdr_type> specified for schema of type pair
 WARNING: no <list_type> specified for schema of type list
 WARNING: no key specified for schema
 WARNING: node <%s> below <%s> not understood
 WARNING: node <%s> not understood
 WARNING: node <%s> not understood below <%s>
 WARNING: node <%s> not understood below <schema>
 We didn't have the lock on file `%s', but we should have When setting a list you must specify a primitive list-type
 When setting a pair you must specify a primitive car-type and cdr-type
 XML filename `%s' is a directory You must have at least one <locale> entry in a <schema>
 You must specify a configuration source with --config-source when using --direct
 [FILE...]|[KEY...]|[DIR...] `%c' is an invalid character in a configuration storage address `%c' is an invalid character in key/directory names couldn't contact ORB to resolve existing gconfd object reference couldn't create directory `%s': %s gconfd compiled with debugging; trying to load gconf.path from the source directory int|bool|float|string int|bool|float|string|list|pair none parsing XML file: lists and pairs may not be placed inside a pair starting (version %s), pid %u user '%s' Project-Id-Version: gconf.master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gconf&component=general
POT-Creation-Date: 2015-12-03 21:36+0000
PO-Revision-Date: 2013-01-04 18:56+0000
Last-Translator: Paco Molinero <paco@byasl.com>
Language-Team: Español <gnome-es-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:26+0000
X-Generator: Launchpad (build 18115)
 
%s
 «%s»: %s La opción %s debe usarse por sí misma.
 «\%o» no es un carácter ASCII, no está permitido en nombres de claves (no hay valor asignado) - Herramienta para manipular una configuración de GConf --ignore-schema-defaults es solo relevante con --get, --all-entries, --dump, --recursive-list, --get-list-size o --get-list-element
 No debería usar --recursive-list con --get, --set, --unset, --all-entries, --all-dirs o --search-key
 No debería usar --set_schema con --get, --set, --unset, --all-entries, --all-dirs o --search-key
 <%s> proporcionado pero el elemento actual no tiene el tipo %s <%s> proporcionado pero la <entry> antecesora no tiene un valor <li> tiene el tipo no válido %s Un nodo tiene un atributo «type» desconocido «%s», ignorándolo Un nodo de alto nivel en el archivo XML «%s» es <%s> en lugar de <entry>, ignorándolo Falló al añadir el cliente a la lista del servidor, error CORBA: %s Añadiendo la fuente «%s»
 Esquema «%s» adjuntado a la clave «%s»
 El atributo «%s» no es válido en el elemento <%s> en este contexto El backend «%s» falló al devolver una vtable
 Al backend «%s» le falta un miembro vtable requerido «%s»
 Nodo XML erróneo: %s Dirección errónea «%s» Dirección errónea «%s»: %s Clave o nombre de carpeta erróneo Acceder a la base de datos de configuraciones directamente, saltándose el servidor. Requiere que no se esté ejecutando gconfd. Error CORBA: %s No se puede añadir notificaciones a una fuente de configuración local No puede obtener y establecer/desestablecer simultáneamente
 No puede obtener el tipo y establecer/desestablecer simultáneamente
 No puede existir un punto «.» tras una barra «/» No pueden existir dos barras «/» en una fila No se puede sobrescribir un valor de solo lectura No se puede leer ni escribir la carpeta raíz XML en la dirección «%s» No puede establecer y obtener/desestablecer simultáneamente
 No puede conmutar y obtener/establecer/desestablecer simultáneamente
 No puede usar --all-dirs  con --get o --set
 No puede usar --all-entries con --get o --set
 No se puede escribir al archivo «%s»: %s No se pudo encontrar la carpeta %s
 No se puede establecer un esquema como valor
 Tipo de car: %s
 Tipo de cdr: %s
 Cambiar los valores obligatorios de GConf Cambiar los valores de GConf del sistema Opciones del cliente: El archivo de configuración «%s» está vacío No se pudo contactar con el servidor de configuración Contactando con el servidor LDAP: equipo «%s», puerto «%d», ND base «%s» Corrupción de datos en la base de datos de la configuración No se pudo conectar con el bus de la sesión: %s No se pudo conectar con el bus del sistema: %s No se puede crear el archivo «%s», probablemente porque ya existe No se pudo volcar el archivo «%s» al disco: %s No se pudo volcar el estado guardado del archivo «%s» al disco: %s No se puede bloquear el archivo temporal «%s»: %s No se pudo crear la carpeta «%s»: %s No se pudo crear la carpeta «%s»: %s No se puede apartar el antiguo archivo de estado «%s»: %s No se pudo abrir la carpeta de bloqueos para %s para quitar los bloqueos: %s
 No se puede abrir el archivo de estado «%s» para escritura: %s No se pudo eliminar al archivo «%s»: %s
 No se pudo eliminar al archivo %s: %s
 No se puede obtener datos de «%s»: %s No puede escribir en el archivo de estado guardado «%s» fd: %d: %s No se pudo encontrar la carpeta raíz XML en la dirección «%s» No se pudo obtener el valor No se puede interpretar el valor CORBA para el elemento lista No pude encontrar el módulo del backend para «%s» El el valor CORBA recibido al realizar la petición para la clave «%s» es incomprensible No se pudo abrir la carpeta del archivo «%s»: %s
 No se puede resolver la dirección para la fuente de configuración Se creó un origen Evolution/LDAP usando el archivo de configuración «%s» Error de D-Bus: %s DESCRIPCIÓN Falló el demonio al obtener el servicio gconf:
%s Falló el demonio al conectar al demonio de D-Bus:
%s Valor predeterminado: %s
 No se encontró car y cdr en el par del nodo par XML No se entiende el nodo XML <%s> dentro de una lista de nodos XML El nodo XML <%s> es incomprensible dentro de un nodo XML par No se puede procesar «%s» (se esperaba un entero) No se puede procesar «%s» (se esperaba un número real) No se puede procesar «%s» (se esperaba «true» o «false») No se puede procesar «%s» (tiene caracteres extra al final) No se puede procesar «%s» (hay un símbolo ')' extra dentro del par) No se puede procesar «%s» (hay un símbolo ']' extra dentro de la lista) No se puede procesar «%s» (las lista debe finalizar con ']') No se puede procesar «%s» (las lista debe comenzar con '[') No se puede procesar «%s» (el par debe finalizar con ')') No se puede procesar «%s» (el par debe comenzar con '(') No se puede procesar «%s» (el número de elementos es erróneo) Operación de carpeta sobre clave Los permisos del archivo/carpeta para la fuente XML en la raíz %s son: %o/%o El documento «%s» no tiene nodo <%s> de nivel superior
 El documento «%s» tiene un tipo erróneo para el nodo raíz (<%s>, debería ser <%s>)
 ¿Está vacío el documento «%s»?
 No es comprensible el tipo «%s»
 Volcar a la salida estándar una descripción XML de todas las entradas bajo una carpeta, recursivamente. Entrada duplicada «%s» en «%s», ignorándola El elemento <%s> no se permite bajo <%s> El elemento <%s> no se permite dentro de un elemento <%s> El elemento <%s> no se permite dentro del elemento actual El valor codificado no es UTF-8 válido Entrada sin nombre en el archivo XML «%s», ignorándola Error al asociar el esquema «%s» con la clave «%s»: %s
 Falló al comprobar la existencia de «%s»: %s Error al compilar la expresión regular: %s
 Falló al buscar la meta-información: %s Falló al obtener el valor predeterminado para «%s»: %s Falló al obtener la meta-información: %s Falló al obtener un nuevo valor para «%s»: %s Error al obtener el esquema de «%s»: %s
 Falló al obtener el valor para «%s»: %s Falló al inicializar el módulo «%s»: %s
 Falló al listar carpetas en «%s»: %s Falló al listar las carpetas: %s
 Error al cargar algunas fuentes de configuración: %s Error al obtener un valor nuevo para «%s» después de cambiar la notificación desde el backend «%s»: %s Falló al obtener un nuevo valor para «%s»: %s Falló al abrir el módulo «%s»: %s
 Error al consultar el servidor LDAP: %s Error al leer «%s»: %s
 Falló al leer el archivo de estado: %s Falló al liberar el bloqueo: %s Falló al eliminar la carpeta «%s»: %s Falló al eliminar el nombre de esquema para «%s»: %s
 Error al guardar el árbol Gconf en «%s»: %s
 Falló al establecer el esquema para «%s»: %s Falló al establecer el valor para «%s»: %s Error al establecer valor: %s Error al establecer el valor: %s
 Error en la sincronización de datos: %s Falló al sincronizar el backend de la caché de la carpeta en XML: %s Error al sincronizar: %s Error al sincronizar: %s
 Falló al eliminar «%s»: %s Falló al eliminar «%s»: %s Error al eliminar la asignación «%s»: %s
 Error durante el análisis de opciones: %s.
Ejecute «%s --help» para ver una lista completa de las opciones de 
línea de comandos disponibles.
 Error al escribir el archivo «%s»: %s Error: %s
 Finalizando Se esperaba el par (%s,%s), sin embargo se obtuvo un par sin uno o dos de los valores Esperaba «%s» y se obtuvo «%s» para la clave %s Se esperaba un booleano, sin embargo se obtuvo %s Se esperaba un float, sin embargo se obtuvo %s Se esperaba un entero, sin embargo se obtuvo %s Se esperaba una lista de %s, sin embargo se obtuvo una lista de %s Se esperaba un lista, sin embargo se obtuvo %s Se esperaba un par del tipo (%s,%s), sin embargo se obtuvo del tipo (%s,%s) Se esperaba un par, sin embargo se obtuvo %s Se esperaba un esquema, sin embargo se obtuvo %s Se esperaba una cadena, sin embargo se obtuvo %s ARCHIVO Falló Falló al leer el valor predeterminado para el esquema: %s Falló al acceder a la(s) fuente(s) de configuración: %s
 Falló al activar la configuración del servidor: %s
 Falló al quitar el archivo «%s»: %s Falló al cerrar el archivo «%s»: %s Falló al cerrar el archivo de registro de gconfd. Es probable que los datos no se hayan guardado apropiadamente (%s) Falló al cerrar el nuevo archivo de estado «%s»: %s No se pudo contactar con el servidor LDAP: %s Falló al contactar con el servidor de configuraciones; la causa más común es un demonio de sesión D-Bus mal configurado o faltante. Para obtener más información consulte http://projects.gnome.org/gconf/. (Detalles: %s) Falló al convertir IOR «%s» a una referencia a un objeto Falló al convertir el objeto a IOR Falló al crear el archivo «%s»: %s Falló al crear o abrir «%s» Falló al eliminar «%s»: %s Falló al eliminar el archivo antiguo «%s»: %s Falló al volcar los datos del cliente al archivo de estado: %s Falló al obtener el IOR para el cliente: %s Falló al obtener un bloqueo Falló al establecer todos los registros en «%s»: %s Falló al obtener el nombre del bus para el demonio, saliendo: %s Ha ocurrido un error al obtener la ruta del archivo de configuración de «%s» Falló al obtener la conexión con la sesión: %s Falló al obtener la referencia de un objeto para ConfigServer Falló al obtener un valor para «%s»: %s
 Falló al liberar el bloqueo en la carpeta XML «%s»: %s Falló al iniciar GConf: %s
 Falló al enlazar «%s» a «%s»: %s Falló al cargar el archivo «%s»: %s Falló al cargar la fuente «%s»: %s Falló al bloquear «%s»: probablemente otro proceso tiene el bloqueo, o el sistema operativo tiene el bloqueo por NFS mal configurado (%s) Falló al registrar la adición del control %s (%s); no será posible restaurar este control al reiniciar gconfd, por consiguiente no será confiable la notificación de los cambios de configuración. Falló al anotar la adición de un servicio al archivo de registro de gconfd. No será posible restaurar el servicio si se cierra gconfd (%s) Error al anotar la sustracción de un servicio al archivo de registro de gconfd. Puede que restaure erróneamente el servicio si se cierra gconfd (%s) Falló al registrar la eliminación de un control en el archivo de registro (aunque lo más probable es que no ocurra nada, puede que surjan extrañas notificaciones): %s Falló al mover el nuevo archivo de estado a su sitio: %s Falló al mover el archivo temporal «%s» a la ubicación final «%s»: %s Falló al abrir «%s»: %s
 Falló al abrir «%s»: %s
 Falló al abrir el archivo de registro de gconfd. No será posible restaurar los servicios después de terminar gconfd (%s) No puede abrir el archivo de estado guardado: %s Falló al analizar el archivo XML «%s» Falló al registrar el objeto del servidor con el demonio de D-Bus Falló al eliminar «%s»: %s Falló el eliminar el bloqueo de la carpeta «%s»: %s Falló al eliminar el archivo de bloqueo «%s»: %s Falló al renombrar «%s» a «%s»: %s Falló al recuperar «%s» de «%s»: %s Falló al reponer el archivo de estado original que había sido movido a «%s»: %s Falló al establecer el modo en «%s»: %s Falló al apagar el backend Falló al reiniciar el servidor de configuraciones (gconfd): %s
 Falló al pedir la información de «%s»: %s Falló al sincronizar la caché XML a disco Falló al sincronizar uno o más recursos: %s Error al eliminar la asignación a la clave de rotura %s: %s
 Falló al escribir «%s»: %s
 Falló al escribir datos XML en «%s»: %s Falló al escribir el byte al descriptor de archivo del pipe %d así que el programa cliente puede que se cuelgue: %s Falló al escribir la adición del cliente al archivo de estado guardado: %s Falló al escribir el archivo «%s»: %s Falló al escribir algunos datos de configuración al disco
 Error al eliminar la asignación recursivamente «%s»: %s
 Falló al listar los registros en «%s»: %s
 Fallo al finalizar el servidor de configuraciones: %s Error fatal: ha ocurrido un error al obtener la referencia del objeto ConfigDatabase GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL está definido, no se instalan los esquemas
 GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL está definido, no se desinstalan los esquemas
 Falló en GConf: %s
 El servidor GConf no está en uso, cerrándolo. Aviso GConf: ha ocurrido un error al listar los pares en «%s»: %s GConf no funcionará sin el soporte de módulos dinámicos (gmodule) Conversión de datos de GSettings Obtener un elemento específico desde una lista de claves, indexada numéricamente. Obtener la cadena doc larga para una clave Obtener el nombre de la fuente predeterminada Obtener el nombre del esquema aplicado a esta clave Obtener el número de los elementos en una lista de claves. Obtener la cadena doc corta para una clave Falló GetIOR: %s Se obtuvieron %d entradas usando el filtro: %s Se obtuvo un mensaje mal formado. Ignorar predeterminados del esquema al leer valores. Ignorando el nodo XML «%s»: %s Ignorando el nodo XML con el nombre «%s»: %s Ignorando car erróneo para el par XML: %s Ignorando cdr erróneo del par XML: %s Ignorando el nombre de esquema «%s», no válido: %s Tipo incorrecto para el elemento lista en %s Inicializando el backend del módulo de marcado de texto Inicializando el módulo del backend XML Opciones de instalación: Instalado el esquema «%s» para la configuración regional «%s»
 El entero «%s» es demasiado grande o demasiado pequeño UTF-8 incorrecto en dominio gettext para el esquema UTF-8 no es válido para el esquema para la configuración regional UTF-8 no es válido para el esquema en la descripción larga UTF-8 no es válido para el esquema pera en el propietario UTF-8 no es válido para el esquema en la descripción corta UTF-8 no válido en el valor de la cadena en «%s» Tipo cdr_type «%s» no válido en <%s> El tipo «%s» del primer elemento no es válido en <%s> Tipo list_type «%s» no válido en <%s> Ltype no válido «%s» en <%s> Tipo no válido (lista, pareja, o desconocido) en un nodo lista La clave %s no es una lista.
 La clave «%s» listada como esquema por la clave «%s» es de tipo «%s» La clave es NULL Operación de clave sobre carpeta Opciones de tipo de clave: Las claves/carpetas no pueden terminar con una barra «/» Ejecuta el servidor de configuración (gconfd). (Normalmente ocurre automáticamente cuando es necesario.) La línea %d carácter %d: %s Tipo de lista: %s
 La lista contiene un nodo mal escrito (%s, debería ser %s) El índice de la lista está fuera de márgenes.
 El índice de la lista no debe ser negativo.
 El tipo de la lista debe ser primitivo: string, int, float o bool
 El ID %lu de control no existe Cargar desde el archivo especificado una descripción XML de valores y establecerlos relativos a una carpeta. Opciones de carga/guardado: Descripción larga: %s
 Migra la configuración del usuario de GConf a dconf Faltan ambos valores, car y cdr, del par en el archivo XML Falta car en el par de valores en el archivo XML Falta cdr en el par de valores en el archivo XML Debe comenzar con una barra «/» Debe establecer un valor a la variable de entorno GCONF_CONFIG_SOURCE
 Debe especificar una expresión regular PCRE para buscar.
 Debe especificar una clave desde la que obtener la lista de elementos.
 Debe especificar una clave o claves en la línea de comandos
 Debe especificar una o más claves a obtener
 Debe especificar una o más claves a obtener el tipo
 Debe especificar un patrón de clave para buscar.
 Debe especificar una clave para buscar el tamaño
 Debe especificar un nombre de esquema seguido por el nombre de la clave para aplicarlo a
 Debe especificar un tipo cuando asigna un valor
 Debe especificar alternativamente claves/valores como argumentos
 Debe especificar una clave (nombre de esquema) como único argumento
 Debe especificar las claves para las que desaplicar un esquema
 Debe especificar un índice de listas.
 Debe especificar una o más carpetas para volcar.
 Debe especificar una o más carpetas donde obtener los pares clave/valor.
 Debe especificar una o más carpetas de los cuales obtener subcarpetas.
 Debe especificar una o más carpetas que listar recursivamente.
 Debe especificar una o más claves como argumentos
 Debe especificar una o mas claves para eliminar la asignación recursivamente.
 Debe especificar una o más claves para eliminar la asignación.
 Debe especificar algunas carpetas que romper
 Debe especificar algunas claves a romper
 Debe especificar algunos archivos de esquema a instalar
 No está definido el atributo «%s» en el elemento <%s> No está especificado el atributo «filter» en <template> en «%s» No está definido el atributo «type» para el nodo <%s> No está definido el atributo «value» para el nodo No hay '/' en la clave «%s» No hay <template> especificado en «%s» El demonio de D-Bus no está en ejecución
 No hay un servidor LDAP o un DN base especificado en «%s» No se encontraron los archivos de configuración. Intentado usar la fuente de configuraciónpredeterminada «%s» No se ha resuelto ninguna dirección de origen satisfactoriamente. No se pueden cargar ni guardar datos de configuración No hay fuentes de configuración en la ruta de fuentes. La configuración no se guardará; edite %s%s No hay bases de datos disponibles para guardar su configuración No hay ninguna cadena doc guardada en el esquema en «%s»
 No hay ningún esquema conocido para «%s»
 No hay ningún esquema guardado en «%s»
 No existe el archivo «%s»
 No se permite texto dentro del elemento <%s> No se encontró ningún valor para la clave %s
 No se ha establecido un valor para «%s»
 No hay valor asignados para la clave: «%s»
 No se resolvió satisfactoriamente ninguna fuente en modo de escritura. Puede que no sea posible guardar los cambios de la configuración No es un valor booleano: %s
 No se está ejecutando en la sesión activa La notificación en %s no existe DUEÑO Falló en el sistema de activación de objetos (OAF) La operación no es permitida sin un servidor de configuración El elemento más externo en el archivo de menú debe ser <gconf> no <%s> Dueño: %s
 El tipo del par car debe ser primitivo: string, int, float o bool
 El tipo del par cdr debe ser primitivo: string, int, float o bool
 Error al analizar Permiso denegado Imprime todos los pares clave/valor en un carpeta. Imprime todos las subcarpetas y entradas bajo una carpeta, recursivamente. Imprime todos las subcarpetas de una carpeta. Imprime el tipo de datos de una clave por la salida estándar. Imprime el valor de una clave por la salida estándar. Versión impresa Se necesitan privilegios para cambiar los valores obligatorios de GConf Se necesitan privilegios para cambiar los valores de GConf del sistema Instala apropiadamente los archivos de esquema desde la línea de comandos en la base de datos. La variable de entorno GCONF_CONFIG_SOURCE debe estar fijada a una fuente de configuración que no sea la predeterminada o debe contener una cadena en blanco para usar la fuente predeterminada. Desinstala apropiadamente los archivos de esquema desde la línea de comandos en la base de datos. La variable de entorno GCONF_CONFIG_SOURCE debe estar establecida a una fuente de configuración que no sea la predeterminada o debe contener una cadena en blanco para usar la fuente predeterminada. La cadena entrecomillada no comienza con comillas La cadena entrecomillada no finaliza con comillas Falló de lectura en el archivo «%s»: %s
 Se ha recibido un valor erróneo al realizar la petición Se ha recibido una lista de gconfd con un tipo de lista erróneo Se ha recibido una petición para eliminar todos los datos del caché Se ha recibido una petición para sincronizar síncronamente Eliminar las asignaciones de todas las claves en o bajo la carpeta de nombres en la línea de comandos Eliminar cualquier nombre de esquema aplicado a la clave dada La operación de eliminar carpeta ya no está admitida, elimine todos los valores en la carpeta La dirección «%s» resuelve una fuente de escritura parcial en la posición %d Se resolvió la dirección «%s» a una fuente de configuración de solo lectura en la posición %d Se resolvió la dirección «%s» a una fuente de configuración escribible en la posición %d Devuelve 0 si gconfd está ejecutándose, 2 si no lo está. Devuelve 0 si la carpeta existe, 2 si no existe. Devolviendo la excepción: %s El nodo raíz de «%s» debe ser <evoldap>, no <%s> Ejecute '%s --help' para ver una lista completa de las opciones de línea de comandos disponibles.
 Se recibió la señal SIGHUP, recargando todas las bases de datos FUENTE Esquema «%s» especificado por «%s» guarda un valor no esquemático El esquema contiene UTF-8 no válido Opciones de esquema: El esquema especifica un tipo de lista pero no especifica el tipo de los elementos listados El esquema especifica el tipo «par» pero no especifica el tipo de los elementos car/cdr Buscar una clave recursivamente. Buscando entradas usando el filtro: %s El servidor no pudo resolver la dirección «%s» Opciones del servidor: Falló al hacer ping al servidor: %s Asigna una clave a un valor y lo sincroniza. Usar con --type. Asigna un esquema y lo sincroniza. Usar con --short-desc, --long-desc, --owner, y --type. Descripción corta: %s
 Mostrar opciones del cliente Mostrar opciones de instalación Mostrar opciones de tipo de clave Mostrar opciones de carga/guardado Mostrar opciones de esquema Mostrar opciones del servidor Mostrar opciones de prueba Cierra gconfd. NO USE ESTA OPCIÓN SIN UNA BUENA RAZÓN. Error al cerrar el programa: %s
 Se ha recibido una petición de finalización Se ha eliminado algún cliente del servidor GConf cuando no había sido añadido. Especifique una fuente de configuración para usar en lugar de la fuente predeterminada Especifique un archivo de esquema para instalar Especifique un descripción de varias líneas para un esquema. Especifique una descripción corta de media línea en un esquema. Especifique al dueño de un esquema Especificar el nombre del esquema seguido por una clave para aplicarlo al nombre del esquema Especifique el tipo de la pareja de valores car asignados, o el tipo del valor descrito por un esquema. Se admiten abreviaciones únicas. Especifique el tipo de la pareja de valores cdr asignados, o el tipo del valor descrito por un esquema. Únicas abreviaciones admitidas. Especifique el tipo de la lista de valores asignados, o el tipo del valor descrito por un esquema. Se admiten abreviaciones únicas. Especifique el tipo de un valor asignado, o el tipo del valor descrito por un esquema. Se admiten abreviaciones únicas. Éxito Opciones de prueba: El texto contiene UTF-8 no válido El nombre «/»  solo puede ser un directorio, no una clave El demonio de GConf se está apagando en este momento. Conmuta una clave booleana. Realiza un test exhaustivo de una aplicación mediante la asignación y la desasignación de claves dentro de una carpeta. Realiza un test exhaustivo de una aplicación mediante la asignación y la desasignación de valores de distintos tipos para claves de la línea de comandos. Tratando de romper su aplicación asignando valores erróneos para la clave:
  %s
 Tratando de romper su aplicación asignando valores erróneos a las claves en la carpeta:
  %s
 Se han dado dos elementos <car> para el mismo par Se han dado dos elementos <cdr> para el mismo par Dos elementos <default> bajo un <local_schema> Des elementos <longdesc> bajo un <local_schema> Tipos incompatibles Tipo: %s
 No se puede abrir el archivo de estado «%s»: %s No es posible analizar el archivo XML «%s» No se puede eliminar la carpeta «%s» de la caché del backend XML, porque no se ha sincronizado con la información del disco No se puede reponer un servicio en la dirección «%s», no se encuentra la base de datos No se puede guardar un valor en la clave «%s», ya que el servidor de configuraciones no tiene ninguna base de datos en modo de escritura. Hay dos causas comunes para este problema: 1) su ruta de configuración %s/path no contiene ninguna base de datos o no ha sido encontrada; o 2) de alguna manera se han creado erróneamente dos procesos gconfd; o 3) el sistema está mal configurado y el bloqueo de NFS no funciona en su carpeta raíz; o 4) la máquina NFS cliente dejó de funcionar y no notificó correctamente al servidor al reiniciarse que se deberían eliminar los bloqueos de archivo. Si tiene dos procesos gconfd (o tenía dos en el momento en el que el segundo fue lanzado), cierre, termine todas las copias de gconfd, y vuelva a ingresar, esto puede solucionar el problema. Si tiene bloqueos antiguos, elimine ~/.gconf*/*lock. Quizá el problema está en que intento usar GConf desde dos máquinas al mismo tiempo, y ORBit todavía tiene la configuración predeterminada que previene las conexiones remotas de CORBA - defina "ORBIIOPIPv4=1" en /etc/orbitrc. Como siempre, compruebe las bitácoras user.* para obtener más detalles sobre los problemas encontrados por gconfd. Solo puede haber un gconfd por carpeta raíz, y debe tener un archivo de bloqueo en ~/.gconfd y también archivos de bloqueo en los sitios de almacenamiento locales como ~/.gconf Desinstalado el esquema «%s» del entorno regional «%s»
 Error desconocido Error %s desconocido: %s Valor desconocido «%s» para el atributo «%s» en el elemento <%s> Descartar un conjunto de valores descrito en un archivo XML. Descargando el módulo de backend de XML. Descargando el backend del módulo de marcado de texto. Sin establecer Elimina la asignación de las claves en la línea de comandos Uso: %s <dir>
 Uso: %s <dir>
  Combina el marcado del backend de una jerarquía de sistema de archivo como:
    dir/%%gconf.xml
        subdir1/%%gconf.xml
        subdir2/%%gconf.xml
  en:
    dir/%%gconf-tree.xml
 El valor en «%s» no es un esquema
 El valor de «%s» es una fuente solo de lectura en la parte superior de su configuración El tipo del valor solo es relevante cuando se asigna el valor
 ADVERTENCIA: nodo <locale> no tiene atributo `name="locale"', ignorándolo
 ADVERTENCIA: ha ocurrido un error al analizar valor booleano «%s»
 ADVERTENCIA: Error al analizar el valor predeterminado «%s» para el esquema (%s)
 ADVERTENCIA: ha ocurrido un error al analizar el valor flotante «%s»
 ADVERTENCIA: ha ocurrido un error al analizar el valor entero «%s»
 ADVERTENCIA: ha ocurrido un error al analizar el valor de la cadena «%s»
 ADVERTENCIA: Nodo <%s> no válido en un nodo <locale>
 ADVERTENCIA: no puede establecer un valor predeterminado para un esquema
 AVISO: el «car_type» solo puede ser int, float, string o bool y no «%s»
 AVISO: «cdr_type» solo puede ser int, float, string o bool y no «%s»
 ADVERTENCIA: nodo <applyto> vacío ADVERTENCIA: ha ocurrido un error al asociar el esquema «%s» con la clave «%s»: %s
 ADVERTENCIA: falló al instalar el esquema «%s», configuración regional «%s»: %s
 ADVERTENCIA: ha ocurrido un error al analizar el nombre del tipo «%s»
 ADVERTENCIA: falló al desinstalar el esquema «%s», configuración regional «%s»: %s
 ADVERTENCIA: ha ocurrido un error interno en gconftool, GConfValueType desconocido
 ADVERTENCIA: falta el tipo car o cdr o es erróneo para el esquema (%s)
 ADVERTENCIA: 'list_type' perdido o erróneo para el esquema (%s)
 ADVERTENCIA: falta el tipo o es erróneo para el esquema (%s)
 ADVERTENCIA: la clave especificada (%s) para el esquema bajo un <value>- ignorada
 AVISO: «list_type» sólo puede ser int, float, string o bool y no «%s»
 ADVERTENCIA: múltiples nodos <locale> para el locale «%s», ignorando todos después del primero
 ADVERTENCIA: debe tener un nodo hijo bajo <valor>
 ADVERTENCIA: debe especificar tanto un <car> como in <cdr> en un <pair>
 ADVERTENCIA: no hay <car_type> especificado para el esquema de tipo «par»
 ADVERTENCIA: no hay <cdr_type> especificado para el esquema de tipo «par»
 ADVERTENCIA: no hay <list_type> especificada para el esquema de tipo de lista
 ADVERTENCIA: no hay clave especificada para el esquema
 ADVERTENCIA: es incomprensible el nodo <%s> bajo <%s>
 ADVERTENCIA: nodo <%s> no comprendido
 ADVERTENCIA: nodo <%s> sin sentido debajo <%s>
 ADVERTENCIA: nodo <%s> no comprendido en <schema>
 No se tiene un bloqueo en el archivo «%s», sin embargo se debía tenerlo Cuando asigne una lista debe especificar un tipo de lista primitivo
 Cuando asigne un par debe especificar un tipo car y un tipo cdr primitivos
 El archivo XML «%s» es una carpeta Debe tener al menos un registro <locale> en un <schema>
 Debe especificar una fuente de configuración con --config-source cuando use --direct
 [ARCHIVO...]|[CLAVE...]|[DIR...] `%c' es un carácter no válido en una dirección de configuración de almacenamiento `%c' no es un carácter válido en los nombres de claves/carpetas no se puede contactar con ORB para resolver la referencia al objeto gconfd existente no se puede crear la carpeta «%s»: %s gconfd ha sido compilado con opciones de depuración: tratando de cargar gconf.path desde la carpeta fuente int|bool|float|string int|bool|float|string|list|pair ninguna analizando el archivo XML: en un par no es posible que no se incluyan ni listas ni pares comenzando (versión  %s), pid %u usuario «%s» 