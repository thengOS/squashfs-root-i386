��    H      \  a   �            !     7     L     b     n     }     �     �     �     �  n   �     M     i     �  +   �  J   �  E     �   a  r   �  k   ^	     �	     �	  5   �	  "   
  <   9
     v
     �
  )   �
     �
  <   �
  3   	      =     ^     e     n      �      �     �  
   �     �            �   3     �     �  ;   �  -   ;  	   i  (   s  u   �  h        {  `   �     �       %        C  
   \  D   g  	   �  
   �  6   �  6   �  -   /     ]     r  �   �  6     ;   H  &   �     �  �  �     �     �     �  
                   .     >     ]  &   j  |   �  "     &   1  $   X  3   }  Q   �  R     w   V  }   �  {   L     �     �  8   �  0     ^   F  2   �     �  )   �       S     F   i  /   �     �     �     �  /     +   ?  "   k     �  *   �     �  $   �  �        �     �  C   �  )   4     ^  (   k  x   �  p        ~  g   �     �  "     "   6  &   Y     �  B   �  
   �     �  F   �  F   *  <   q  #   �  "   �  �   �  K   y   O   �   +   !  /   A!     )          $                           D      &   3   *       /          9   :   0          %      H                         E      #   A   '   8      6   ?   F       7   B   
      >   1       2                  +   (   4   G             -       <       "       @   .             !                  5              C   ;       =         	   ,               128-bit (most secure) 40-bit (less secure) <b>Authentication</b> <b>Echo</b> <b>General</b> <b>Misc</b> <b>Optional</b> <b>Security and Compression</b> Ad_vanced... All Available (Default) Allow MPPE to use stateful mode. Stateless mode is still attempted first.
config: mppe-stateful (when checked) Allow _BSD data compression Allow _Deflate data compression Allow st_ateful encryption Allow the following authentication methods: Allow/disable BSD-Compress compression.
config: nobsdcomp (when unchecked) Allow/disable Deflate compression.
config: nodeflate (when unchecked) Allow/disable Van Jacobson style TCP/IP header compression in both the transmit and the receive directions.
config: novj (when unchecked) Allow/disable authentication methods.
config: refuse-pap, refuse-chap, refuse-mschap, refuse-mschap-v2, refuse-eap Append the domain name <domain> to the local host name for authentication purposes.
config: domain <domain> Authenticate VPN CHAP Compatible with Microsoft and other PPTP VPN servers. Could not find pptp client binary. Could not find secrets (connection invalid, no vpn setting). Could not find the pppd binary. Default Don't quit when VPN connection terminates EAP Enable custom index for ppp<n> device name.
config: unit <n> Enable verbose debug logging (may expose passwords) Invalid or missing PPTP gateway. MSCHAP MSCHAPv2 Missing VPN gateway. Missing or invalid VPN password. Missing or invalid VPN username. Missing required option '%s'. NT Domain: No VPN configuration options. No VPN secrets! No cached credentials. Note: MPPE encryption is only available with MSCHAP authentication methods. To enable this checkbox, select one or more of the MSCHAP authentication methods: MSCHAP or MSCHAPv2. PAP PPTP Advanced Options PPTP server IP or name.
config: the first parameter of pptp Password passed to PPTP when prompted for it. Password: Point-to-Point Tunneling Protocol (PPTP) Require the use of MPPE, with 40/128-bit encryption or all.
config: require-mppe, require-mppe-128 or require-mppe-40 Send LCP echo-requests to find out whether peer is alive.
config: lcp-echo-failure and lcp-echo-interval Send PPP _echo packets Set the name used for authenticating the local system to the peer to <name>.
config: user <name> Show password Use TCP _header compression Use _Point-to-Point encryption (MPPE) Use custom _unit number: User name: You need to authenticate to access the Virtual Private Network '%s'. _Gateway: _Security: couldn't convert PPTP VPN gateway IP address '%s' (%d) couldn't look up PPTP VPN gateway IP address '%s' (%d) invalid boolean property '%s' (not yes or no) invalid gateway '%s' invalid integer property '%s' nm-pptp-service provides integrated PPTP VPN capability (compatible with Microsoft and other implementations) to NetworkManager. no usable addresses returned for PPTP VPN gateway '%s' no usable addresses returned for PPTP VPN gateway '%s' (%d) property '%s' invalid or not supported unhandled property '%s' type %s Project-Id-Version: NetworkManager-pptp
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=NetworkManager&keywords=I18N+L10N&component=VPN: pptp
POT-Creation-Date: 2016-04-14 07:51+0000
PO-Revision-Date: 2012-04-14 16:01+0000
Last-Translator: nat6091 <Unknown>
Language-Team: Español <gnome-es-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:03+0000
X-Generator: Launchpad (build 18115)
 128-bit (más seguro) 40-bit (menos seguro) <b>Autenticación</b> <b>Eco</b> <b>General</b> <b>Varios</b> <b>Opcional</b> <b>Seguridad y compresión</b> A_vanzado... Todas las disponibles (predeterminado) Permitir a MPPE usar el modo con estado. El modo sin estado se sigue intentando primero.
opción: mppe-stateful (desmarcada) Permitir compresión de datos _BSD Permitir compresión de datos _Deflate Permitir cifrado de est_ado completo Permitir los siguientes métodos de autenticación: Permitir/desactivar compresión «BSD-Compress».
opción: nobsdcomp (desmarcada) Permitir/desactivar la compresión por desinflado.
opción: nodeflate (desmarcada) Permitir/desactivar compresión de cabeceras TCP/IP estilo Van Jacobson al enviar y recibir.
opción: novj (desmarcada) Permitir/desactivar métodos de autenticación.
opción: refuse-pap, refuse-chap, refuse-mschap, refuse-mschap-v2, refuse-eap Añadir el nombre del dominio <dominio> al nombre local del equipo por motivos de autenticación.
opción: domain <dominio> Autenticar VPN CHAP Compatible con servidores PPTP VPN de Microsoft y otros. No se pudo encontrar el binario del cliente pptp No se pudieron encontrar los secretos (conexión no válida, no existe la configuración VPN). No se pudo encontrar el binario del cliente pptpd. Predeterminado No salir cuando la conexión VPN finaliza EAP Activar índice personalizado para ppp<n> nombre del dispositivo.
opción: unit <n> Activar registro de depuración detallado (puede exponer contraseñas) Falta o no es válida la puerta de enlace PPTP. MSCHAP MSCHAPv2 Falta la puerta de enlace VPN. Falta o no es válida la contraseña de la VPN. Falta o no es válido el usuario de la VPN. Falta la opción requerida «%s». Dominio de red: No existen opciones de configuración VPN. No existen secretos VPN No existen credenciales almacenadas. Nota: el cifrado MPPE solo está disponible con los métodos de autenticación MSCHAP. Para activar esta casilla de selección seleccione uno o más métodos de autenticación MSCHAP: MSCHAP o MSCHAPv2. PAP Opciones avanzadas de PPTP Nombre o IP del servidor PPTP.
opción: the first parameter of pptp Contraseña pasada a PPTP al solicitarla. Contraseña: Protocolo de túnel punto a punto (PPTP) Requerir el uso de MPPE, con cifrado de 40/128 bits para todo.
opción: require-mppe, require-mppe-128 o require-mppe-40 Enviar solicitudes de eco LCP para averiguar si el par está vivo.
opción: lcp-echo-failure y lcp-echo-interval Mandar paquetes _eco PPP Establecer el nombre usado para autenticar el equipo local en el par a <nombre>.
opción: user <nombre> Mostrar contraseña Usar compresión de _cabeceras TCP Usar cifrado pun_to a punto (MPPE) Usar número de _unidad personalizado: Usuario: Necesita autenticarse para acceder a la red privada virtual «%s» _Pasarela: _Seguridad: no se pudo convertir la IP de la puerta de enlace VPN PPTP «%s» (%d) no se pudo encontrar la IP de la puerta de enlace VPN PPTP «%s» (%d) propiedad lógica «%s» no válida (no es «sí» o «no») puerta de enlace «%s» no válida. propiedad entera «%s» no válida nm-pptp-service proporciona capacidades VPN PPTP integradas con NetworkManager (compatible con Microsoft y otras implementaciones). la puerta de enlace VPN PPTP devolvió una dirección no utilizable «%s» la puerta de enlace VPN PPTP devolvió una dirección no utilizable «%s» (%d) propiedad «%s» no válida o no permitida no se puede manejar la propiedad «%s» tipo %s 