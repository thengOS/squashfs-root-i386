��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  I   �  /   �  3     .   K  #   z     �     �  (   �  I        L               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
POT-Creation-Date: 2013-01-17 09:48-0500
PO-Revision-Date: 2011-09-09 21:51+0000
Last-Translator: Jorge González <Unknown>
Language-Team: Spanish (http://www.transifex.com/projects/p/freedesktop/language/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:01+0000
X-Generator: Launchpad (build 18115)
Language: es
 Para cambiar la configuración de la pantalla de acceso debe autenticarse Para cambiar datos de usuario debe autenticarse Para cambiar sus datos de usuario debe autenticarse Cambie la configuración de pantalla de acceso Cambie sus propios datos de usuario Activar código de depuración Gestione cuentas de usuario Mostrar información de versión y salir Proporciona interfaces DBus para consultar y manipular
cuentas de usuario Reemplazar la instancia actual 