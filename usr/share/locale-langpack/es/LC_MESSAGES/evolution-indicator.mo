��          �      L      �     �     �     �     �     �       3        M     [     v  7   �  ,   �     �          '  #   ?  
   c  	   n  �  x  #   L     p     r  	   �     �     �  F   �       0     %   N  W   t  A   �          +     J  (   e     �     �                                                 	                                     
              %d New Message %d New Messages : Compose New Message Contacts Evolution Indicator Inbox Only create notifications for new mail in an Inbox. Pla_y a sound Play a sound for new mail. Show a notification bubble. Show new message count in the message indicator applet. Shows new mail count in a message indicator. When New Mail Arrives When new mail arri_ves in _Display a notification _Indicate new messages in the panel any Folder any Inbox Project-Id-Version: evolution-indicator
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-06-24 15:14+0000
PO-Revision-Date: 2013-04-14 08:06+0000
Last-Translator: Paco Molinero <paco@byasl.com>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 18:39+0000
X-Generator: Launchpad (build 18115)
 %d mensaje nuevo %d mensajes nuevos : Redactar un mensaje nuevo Contactos Indicador de Evolution Bandeja de entrada Solo crear notificaciones para correo nuevo en una bandeja de entrada. Repr_oducir un sonido Reproducir un sonido al recibir un correo nuevo. Mostrar una burbuja de notificación. Mostrar la cantidad de mensajes nuevos en la miniaplicación del indicador de mensajes. Muestra un contador de correo nuevo, en un indicador de mensajes. Cunado llega un correo nuevo Cuando llegue correo nue_vo en _Mostrar una notificación _Indicar los mensajes nuevos en el panel cualquier carpeta cualquier bandeja de entrada 