��    "      ,  /   <      �     �     
       
        #  .   :     i  	   |     �     �     �  !   �     �     �     �     �          .  ,   E     r     �     �     �     �  $   �     �     �       �   '     �     �     �     �  �  �     �     �     �  
   �     �  @   �  %     
   B     M     g  (   v  )   �     �     �     �  %   �      	  &   ?	  :   f	     �	     �	  '   �	     �	     �	  *   
     /
     B
     ]
  �   x
             	   0     :                                                                                 	             
         !                                             "          Add input method Addon Advance Appearance Available Input Method Cannot load currently used user interface info Clear font setting Configure Current Input Method Default Default keyboard layout Didn't install related component. Empty Global Config Input Method Input Method Configuration Input Method Default Input method settings: Keyboard layout to use when no input window: Keyboard layout: Language No configuration option for %s. Only Show Current Language Other Please press the new key combination Search Addon Search Input Method Show Advance Option The first input method will be inactive state. Usually you need to put <b>Keyboard</b> or <b>Keyboard - <i>layout name</i></b> in the first place. Unknown Unspecified _Cancel _OK Project-Id-Version: fcitx-configtool
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2013-10-18 03:07-0400
PO-Revision-Date: 2015-06-21 07:08+0000
Last-Translator: Paco Molinero <paco@byasl.com>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:00+0000
X-Generator: Launchpad (build 18115)
 Añadir método de entrada Complemento Avanzado Apariencia Método de entrada disponible No se puede cargar la información de interfaz de usuario en uso Limpiar configuración de tipografía Configurar Método de entrada actual Predeterminado Distribución del teclado predeterminada No se instaló el componente relacionado. Vacío Configuracion global Método de entrada Configuración del método de entrada Metodo de entrada predeterminado Configuración del método de entrada: Metodo de entrada a usar cuando no hay ventana de entrada: Diseño de teclado: Idioma No hay opcion de configuración para %s Mostrar solo idioma actual Otra Introduzca la nueva combinación de teclas Buscar complemento Buscar métodos de entrada Mostrar opciones avanzadas El primer método de entrada quedará inactivo. Normalmente debe poner <b>Teclado</b> o <b>Teclado - <i>nombre de la distribución</i></b> en primer lugar. Desconocido Sin especificar _Cancelar _Aceptar 