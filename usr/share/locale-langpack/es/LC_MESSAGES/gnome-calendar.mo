��    x      �  �   �      (
     )
     I
     O
     U
     k
     q
     z
     |
     
     �
     �
     �
     �
     �
  *   �
     �
               !     <     M     _     n     �     �     �  	   �     �     �     �     �  $   �  )        :     >     K     \     s     x     �     �  �   �     Q     _     l  �   x     W     ^     {     �     �     �     �     �     �     �  	   �                          $     '     7      <  &   ]     �     �     �     �     �     �     �     �     �            '   &     N     U     [     a     x  >   �     �     �     �  )   �       �   "     �     �     
          5     A     a     f     m  
   s     ~     �     �     �     �     �     �          /     J     d     ~     �     �     �     �               1  E  D  '   �  
   �  
   �     �     �     �     �     �                 '     0     C     V  E   h     �     �  
   �     �     �          3     J     `     x     �     �     �     �     �     �  4   �  9        X     ]     m     �     �     �     �     �  �   �     �     �     �    �     �     �               '  
   :     E     I     a     t     �     �  	   �     �     �     �     �     �  '   �  (      0   I     z     �     �     �     �     �     �     �  $   �       >   (     g     o     w     {     �  >   �     �     �       ,        L  �   T     .      A      a       y      �   )   �      �   
   �      �   
   �      �      
!     !     !     +!  
   3!     >!     F!     T!     `!     m!     }!     �!     �!     �!     �!     �!     �!  !  �!                   C   .       p   a   #   i   E      "   2   9   8                 &   +       *   n                       t      N   P      V   K   
          ]   G   m                      A   o   5   \   U              -   l   w   s              7      x       ?           g   Z           /   1   >   (      r         M       W              '   _   ^      e       [           B   Y      @           %       k      X   :   L   Q   F   b   )   c   J   v       O       H   $       <       f   0   ;       6   `   !   S               	   ,   j   R           =   q      3      T      D       d                 4   u   I   h    %s (this calendar is read-only) %s AM %s PM - Calendar management 00:00 00:00 PM : AM Access, and manage calendar Account Add Add Calendar Add Eve_nt… Add Event… Add new events to this calendar by default All day Another event deleted Calendar Calendar <b>%s</b> removed Calendar Address Calendar Settings Calendar files Calendar for GNOME Calendar management Calendar name Calendar;Event;Reminder; Calendars Cancel Click to set up Color Connect Copyright © %d The Calendar authors Copyright © %d–%d The Calendar authors Day Delete event Display calendar Display version number Done Edit Calendar Edit Details… Ends Enter the address of the calendar that you want to add. If the calendar belongs to one of your online accounts, you can add it through the <a href="GOA">online account settings</a>. Event deleted From File… From Web… GNOME Calendar is a simple and beautiful calendar application designed to perfectly fit the GNOME desktop. By reusing the components which the GNOME desktop is build on, Calendar nicely integrates with the GNOME ecosystem. Google List of the disabled sources Location Manage your calendars Microsoft Exchange Midnight Month New Event from %s to %s New Event on %s New Local Calendar… No events No results found Noon Notes Off On Online Accounts Open Open calendar on the passed date Open calendar showing the passed event Open online account settings Other event Other %d events Other events Overview PM Password Remove Calendar Save Search for events Select a calendar file Settings Sources disabled last time Calendar ran Starts Title Today Try a different search Type of the active view Type of the active window view, default value is: monthly view Undo Unnamed Calendar Unnamed event Use the entry above to search for events. User We aim to find the perfect balance between nicely crafted features and user-centred usability. No excess, no lacks. You'll feel comfortable using Calendar, like you've been using it for ages! Window maximized Window maximized state Window position Window position (x and y). Window size Window size (width and height). Year _About _Quit _Search… _Synchronize event date format%B %d ownCloud shortcut windowClose window shortcut windowGeneral shortcut windowGo back shortcut windowGo forward shortcut windowMonth view shortcut windowNavigation shortcut windowNew event shortcut windowNext view shortcut windowPrevious view shortcut windowSearch shortcut windowShortcuts shortcut windowShow help shortcut windowShow today shortcut windowView shortcut windowYear view translator-credits Project-Id-Version: gnome-calendar master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-calendar&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-05-18 08:25+0000
PO-Revision-Date: 2016-04-09 07:36+0000
Last-Translator: Ricardo Pérez López <ricpelo@gmail.com>
Language-Team: Spanish <gnome-es-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 19:34+0000
X-Generator: Launchpad (build 18115)
Language: es
 %s (este calendario es de solo lectura) %s a. m. %s p. m. - Gestión del calendario 00:00 00:00 p. m. : a. m. Acceder y gestionar calendarios Cuenta Añadir Añadir calendario Añadir eve_nto… Añadir evento… Añadir los eventos nuevos a este calendario de manera predeterminada Todo el día Otro evento eliminado Calendario Calendario <b>%s</b> eliminado Dirección del calendario Configuración del calendario Archivos de calendario Calendario para GNOME Gestión del calendario Nombre del calendario Calendario;Evento;Recordatorio; Calendarios Cancelar Pulsar para configurar Color Conectar Derechos de autor © %d de los autores de Calendario Derechos de autor © %d–%d de los autores de Calendario Día Eliminar evento Mostrar calendario Mostrar el número de versión Hecho Editar calendario Editar detalles… Termina Escriba la dirección del calendario que quiera añadir. Si el calendario pertenece a una de sus cuentas en línea, puede añadirlo mediante la <a href="GOA">configuración de cuentas en línea</a>. Evento eliminado A partir de un archivo… Desde la web… Calendario de GNOME es una aplicación de calendario bonita y sencilla diseñada para ajustarse perfectamente al escritorio GNOME. Al reutilizar componentes con los que se ha construido el escritorio GNOME, Calendario se integra muy bien con el entorno de GNOME. Google Lista de recursos desactivados Ubicación Gestionar sus calendarios Microsoft Exchange Medianoche Mes Evento nuevo de %s a %s Evento nuevo en %s Calendario local nuevo… No hay eventos No se encontraron resultados. Mediodía Notas Desactivado Activado Cuentas en línea Abrir Abrir el calendario en una fecha pasada Abrir el calendario en una evento pasada Abrir la configuración de las cuentas en línea Otro evento Otros %d eventos Otros eventos Vista general p. m. Contraseña Eliminar calendario Guardar Buscar eventos Seleccionar un archivo de calendario Configuración Fuentes desactivadas la última vez que se ejecutó Calendario Empieza Título Hoy Pruebe una búsqueda diferente Tipo de vista activa Tipo de vista activa; el tipo predeterminado es: vista mensual Deshacer Calendario sin nombre Evento sin nombre Use la entrada anterior para buscar eventos. Usuario Se ha tratado de buscar el equilibrio entre características bien elaboradas y la usabilidad centrada en el usuario. No hay excesos ni carencias. Se sentirá cómodo usando Calendario, como si llevara años usándolo. Ventana maximizada Estado maximizado de la ventana Posición de la ventana Posición de la ventana (X e Y). Tamaño de la ventana Tamaño de la ventana (anchura y altura). Año _Acerca de _Salir _Buscar… _Sincronizar %d de %B ownCloud Cerrar ventana General Retroceder Avanzar Vista mensual Navegación Evento nuevo Vista siguiente Vista anterior Buscar Atajos Mostrar ayuda Mostrar el día de hoy Vista Vista anual Daniel Mustieles <daniel.mustieles@gmail.com>, 2012 - 2015

Launchpad Contributions:
  Adolfo Jayme https://launchpad.net/~fitojb
  Daniel Mustieles https://launchpad.net/~daniel-mustieles
  Jesus Delgado https://launchpad.net/~talpio
  Ricardo Pérez López https://launchpad.net/~ricardo 