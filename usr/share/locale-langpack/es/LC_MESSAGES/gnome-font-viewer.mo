��            )         �     �  	   �     �  	   �     �     �     �     �  �     �   �     �     �     �  	   �     �     �  E   �     �          !     '     ,  !   L     n     �     �     �  	   �     �     �  +  �  	             -  	   5     ?  (   L     u     �  �   �  �   �	     ~
     �
     �
  	   �
     �
     �
  d   �
     '  %   /     U     \  )   b  %   �  -   �     �     �  "   �              6                                                                                  	      
                                                About All Fonts Back Copyright Description FONT-FILE OUTPUT-FILE Font Viewer GNOME Font Viewer GNOME Font Viewer also supports installing new font files downloaded in the .ttf and other formats. Fonts may be installed only for your use or made available to all users on the computer. GNOME Font Viewer shows you the fonts installed on your computer for your use as thumbnails. Selecting any thumbnails shows the full view of how the font would look under various sizes. Info Install Install Failed Installed Name Quit Run '%s --help' to see a full list of available command line options. SIZE Show the application's version Style TEXT Text to thumbnail (default: Aa) This font could not be displayed. Thumbnail size (default: 128) Type Version View fonts on your system [FILE...] fonts;fontface; translator-credits Project-Id-Version: gnome-utils.master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-font-viewer&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 22:21+0000
PO-Revision-Date: 2015-12-04 14:45+0000
Last-Translator: Daniel Mustieles <daniel.mustieles@gmail.com>
Language-Team: Español; Castellano <gnome-es-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:15+0000
X-Generator: Launchpad (build 18115)
Language: 
 Acerca de Todas las tipografías Atrás Copyright Descripción ARCHIVO-DE-TIPOGRAFÍA ARCHIVO-DE-SALIDA Visor de tipografías Visor de tipografías de GNOME El visor de tipografías de GNOME también soporta instalar nuevas tipografías descargadas como archivos .ttf u otros formatos. Las tipografías se puede instalar para su uso personal o para que estén disponibles para todos los usuarios del sistema. El visor de tipografías de GNOME muestra las tipografías instaladas en el equipo como miniaturas. Al seleccionar cualquiera de ellas se muestra una vista completa de cómo queda la tipografía con varios tamaños. Información Instalar Falló la instalación Instalado Nombre Salir Ejecute «%s --help» para ver una lista completa de las opciones de línea de comandos disponibles. TAMAÑO Mostrar la versión de la aplicación Estilo TEXTO Texto a miniaturizar (predeterminado: Aa) No se puede mostrar esta tipografía. Tamaño de la miniatura (predeterminado: 128) Tipo Versión Ver las tipografías de su sistema [ARCHIVO…] tipografía;letra;tipo; Daniel Mustieles <daniel.mustieles@gmail.com>, 2012

Launchpad Contributions:
  Adolfo Jayme https://launchpad.net/~fitojb
  Daniel Mustieles https://launchpad.net/~daniel-mustieles
  Monkey https://launchpad.net/~monkey-libre
  Paco Molinero https://launchpad.net/~franciscomol 