��    @        Y         �  *   �  �  �     H  6   M  .   �  �   �     9  �   H     �  ;   
	     F	     N	  
   W	     b	     h	     �	     �	     �	     �	     �	     �	     �	  !   �	     
     
     0
     7
     <
     P
     a
     n
  O   �
     �
     �
     �
  	   �
                 	   (  	   2  	   <     F     S     Y     b     p     x     �     �     �     �  #   �     �               9  !   T     v     �     �     �     �  A    =   E  �  �     '  =   -  .   k  �   �     /  �   >      �  C        Y     g     p     �  -   �     �     �     �     �     �     �  (   	  .   2     a  !        �     �     �     �     �  !   �  H     
   ]     h     y     �     �     �     �  	   �     �     �     �     �  
   �     �       	        #  	   *     4     T     Y  
   g     r     y     �     �     �  
   �     �  B  �  %           /   4                       ?   $   :                      ,                 2         #   ;              <   6   1         %   -   9   @           	      (         +                      8       3      )          5      "                                     *   &   >      .           
       !   0   7       '   =       A matching game played with Mahjongg tiles A solitaire version of the classic Eastern tile game. Tiles are stacked on the board at the start of the game. The goal is to remove all the tiles in as little time as possible. Select two matching tiles and they will disappear from the board, but you can only select a tile if there is an empty space to its left or right at the same level. Be careful: tiles that look alike may actually be slightly different. Date Disassemble a pile of tiles by removing matching pairs Do you want to start a new game with this map? Each puzzle has at least one solution.  You can undo your moves and try and find the solution, restart this game, or start a new one. GNOME Mahjongg GNOME Mahjongg features a variety of starting layouts, some easy and some difficult. If you get stuck, you can ask for a hint, but this adds a large time penalty. Height of the window in pixels If you continue playing the next game will use the new map. Layout: Mahjongg Main game: Maps: Match tiles and clear the board Moves Left: New Game OK Pause the game Paused Preferences Print release version and exit Receive a hint for your next move Redo your last move There are no more moves. Tiles: Time Undo your last move Unpause the game Use _new map Width of the window in pixels You can also try to reshuffle the game, but this does not guarantee a solution. _About _Background color: _Close _Contents _Continue playing _Help _Layout: _Mahjongg _New Game _New game _Preferences _Quit _Restart _Restart Game _Scores _Shuffle _Theme: _Undo game;strategy;puzzle;board; mahjongg map nameCloud mahjongg map nameConfounding Cross mahjongg map nameDifficult mahjongg map nameEasy mahjongg map nameFour Bridges mahjongg map nameOverpass mahjongg map namePyramid's Walls mahjongg map nameRed Dragon mahjongg map nameThe Ziggurat mahjongg map nameTic-Tac-Toe translator-credits true if the window is maximized Project-Id-Version: gnome-games.master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-mahjongg&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 22:17+0000
PO-Revision-Date: 2015-12-04 15:54+0000
Last-Translator: Daniel Mustieles <daniel.mustieles@gmail.com>
Language-Team: Español; Castellano <gnome-es-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:21+0000
X-Generator: Launchpad (build 18115)
X-Project-Style: gnome
Language: es
 Un juego de comparaciones que se juega con fichas de Mahjongg Una versión del solitario Eastern. Las fichas se apilan en el tablero al principio del juego. El objetivo es quitar todas las fichas en el menor tiempo posible. Seleccione dos fichas iguales y desaparecerán del tablero, pero sólo puede seleccionar una ficha si tiene un espacio vacío a su izquierda o a su derecha en el mismo nivel. Tenga cuidado: hay fichas que parecen iguales pero que son ligeramente diferentes. Fecha Desmonte un montón de fichas eliminando parejas coincidentes ¿Quiere empezar un juego nuevo con este mapa? Cada puzle tiene, al menos, una solución. Puede deshacer sus movimientos e intentar encontrar la solución, reiniciar el juego o empezar uno nuevo. GNOME Mahjongg GNOME Mahjongg tiene varias distribuciones iniciales, algunas fáciles y algunas difíciles. Si tiene problemas, puede pedir una pista, pero esto añade una penalización de tiempo. Altura de la ventana en píxeles Si elige continuar jugando el siguiente juego usará el mapa nuevo. Disposición: Mahjongg Juego principal: Mapas: Emparejar fichas iguales y limpiar el tablero Movimientos restantes: Partida nueva Aceptar Pausar el juego Detenido Preferencias Mostrar el número de versión y salir Obtener una pista para el siguiente movimiento Rehacer el último movimiento No hay más movimientos válidos. Fichas: Tiempo Deshacer el último movimiento Reanudar el juego Usar mapa _nuevo Anchura de la ventana en píxeles También puede mezclar las piezas, pero esto no garantiza una solución. _Acerca de Color de _fondo: _Cerrar Índ_ice _Continuar jugando Ay_uda _Disposición: _Mahjongg Juego _nuevo Partida _nueva _Preferencias _Salir _Reiniciar _Reiniciar partida _Puntuación _Revolver _Tema: _Deshacer juego;estrategia;puzle;tablero; Nube Cruce confuso Dificultad Fácil Cuatro puentes Paso a desnivel Muro de la pirámide Dragón rojo El Zigurat Tic-Tac-Toe Daniel Mustieles <daniel.mustieles@gmail.com>, 2008-2014
Jorge González <jorgegonz@svn.gnome.org>, 2008
Lucas Vieites <lucas@asixinformatica.com>, 2002-2008
Germán Poo Caamaño <gpoo@ubiobio.cl>
Pablo Saratxaga <pablo@mandrakesoft.com>

Launchpad Contributions:
  Daniel Mustieles https://launchpad.net/~daniel-mustieles cierto si la ventana está maximizada 