��    I      d  a   �      0     1  4  N     �  R   �  g   �  Z   W	  '   �	     �	     �	     �	     
  	   '
  1   1
  	   c
  	   m
  (   w
  )   �
     �
     �
     �
  #      '   $  "   L     o     �     �     �     �     �     �     �      �          	             !   /  '   Q  %   y  	   �     �  '   �     �  P   �     1     H     X     e  
   t  ;     3   �  Q   �     A     P  &   g     �      �  #   �      �  *   	      4      U  /   v     �  =   �  	         
               #  (   0     Y  �  i  !   "  i  D     �  `   �  s   )  d   �  '        *     @  $   [     �     �  5   �     �     �  -   �  .     (   L     u     �  7   �  7   �  %   	     /  !   I     k     t     �     �     �     �  '   �     �     �  
   �     
       =   4  /   r     �     �  /   �  	   �  \   �     I     _     u     �  
   �  =   �  6   �  R        o  ,   �  8   �     �  *   �  (   (      Q  5   r  4   �     �  4   �     .  P   K  
   �     �     �     �     �  4   �              )      +   I   E   A       #   D   =      "          ;          C   *           0      /              H            9   4      	      
              >   <   8       $           -                   B   ,          3       ?              :   &          5   2   7                @          6                           G   1      '   %   F   .   !       (     - the GNOME session manager %s [OPTION...] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can't recover.
Please log out and try again. A problem has occurred and the system can't recover. All extensions have been disabled as a precaution. A problem has occurred and the system can't recover. Please contact a system administrator A session named ‘%s’ already exists AUTOSTART_DIR Add Startup Program Additional startup _programs: Allow logout Browse… Choose what applications to start when you log in Co_mmand: Comm_ent: Could not connect to the session manager Could not create ICE listening socket: %s Could not display help document Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don't prompt for user confirmation Edit Startup Program Enable debugging code Enabled Failed to execute %s
 GNOME GNOME dummy GNOME on Wayland Icon Ignoring any existing inhibitors Log out No description No name Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Select Command Session %d Session names are not allowed to contain ‘/’ characters Session names are not allowed to start with ‘.’ Session names are not allowed to start with ‘.’ or contain ‘/’ characters Session to use Show extension warning Show the fail whale dialog for testing Startup Applications Startup Applications Preferences The startup command cannot be empty The startup command is not valid This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME This session logs you into GNOME, using Wayland Version of this application _Automatically remember running applications when logging out _Continue _Log Out _Log out _Name: _New Session _Remember Currently Running Applications _Remove Session Project-Id-Version: gnome-session.master
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-05-18 08:40+0000
PO-Revision-Date: 2015-04-02 08:24+0000
Last-Translator: Paco Molinero <paco@byasl.com>
Language-Team: Español; Castellano <gnome-es-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:39+0000
X-Generator: Launchpad (build 18115)
Language: 
  - El gestor de sesiones de GNOME %s [OPCIÓN...] ORDEN

Ejecutar ORDEN mientras se inhiben algunas funciones de sesión.

  -h, --help        Muestra esta ayuda
  --version         Muestra la versión del programa
  --app-id ID       Identificador de la aplicación a usar
                    durante la inhibición (opcional)
  --reason RAZÓN    Razón para la inhibición (opcional)
  --inhibit ARG     Elementos a inhibir, lista separada por comas de:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    No lanzar la ORDEN y en su lugar esperar para siempre

Si no se indica la opción --inhibit, se asume idle.
 %s necesita un argumento
 Ocurrió un problema y el sistema no puede recuperarse.
Cierre la sesión e inténtelo de nuevo. Ha ocurrido un problema y el sistema no se puede recuperar. Se desactivaron todas las extensiones como precaución. Ha ocurrido un problema y el sistema no puede recuperarse. Contacte con el administrador del sistema Ya existe una sesión con nombre «%s» CARPETA_DE_AUTOINICIO Añadir programa al inicio _Programas adicionales para iniciar: Permitir cerrar sesión Examinar… Elija qué aplicaciones iniciar al iniciar la sesión _Orden: Com_entario: No se pudo conectar con el gestor de sesiones No se pudo crear el zócalo de escucha ICE: %s No se pudo mostrar el documento de ayuda Personalizada Sesión personalizada Desactivar la comprobación de aceleración de hardware No cargar las aplicaciones especificadas por el usuario No requerir confirmación del usuario Editar programa al inicio Activar el código de depuración Activado Falló al ejecutar %s
 GNOME GNOME dummy GNOME en Wayland Icono Ignorando cualquier inhibidor existente Cerrar la sesión Sin descripción Sin nombre No responde Lo sentimos, algo salió mal. Sobreescribir los directorios de inicio automático estándar Seleccione una sesión personalizada a ejecutar Apagar Programa Se llamó al programa con opciones en conflicto Reiniciar Rechazando la conexión de un nuevo cliente porque actualmente se está cerrando la sesión
 Aplicación recordada Reno_mbrar la sesión NOMBRE_DE_LA_SESIÓN Seleccionar orden Sesión %d Los nombres de la sesión no pueden contener caracteres «/» Los nombres de la sesión no pueden comenzar por «.» Los nombres de la sesión no pueden comenzar por «.» o contener caracteres «/» Sesión que usar Mostrar mensaje de advertencia de extensión Muestra el diálogo de la ballena de fallos para pruebas Aplicaciones al inicio Preferencias de las aplicaciones al inicio La orden de inicio no puede estar vacía La orden de inicio no es válida Esta opción permite seleccionar una sesión guardada Este programa está bloqueando el cierre de sesión. Inicia una sesión en GNOME Esta opción inicia una sesión de GNOME con Wayland Versión de esta aplicación Recordar _automáticamente las aplicaciones en ejecución al salir de la sesión _Continuar Ce_rrar la sesión _Cerrar sesión _Nombre: Sesión _nueva _Recordar las aplicaciones ejecutándose actualmente Elimina_r la sesión 