��    E      D  a   l      �     �  @   	  /   J  &   z  )   �  -   �     �     �          &     =     I      P     q  -   v  =   �  +   �       
   &     1  B   6     y     �  )   �     �  
   �     �  !   �      �  	   	     "	     /	  -   7	     e	     k	     r	  
   �	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	  
   �	  #   
     ,
     2
     9
  !   A
  
   c
  )   n
  $   �
  (   �
  *   �
  -     $   ?  +   d  4   �  (   �     �     �                    (    .     M  ?   ^  =   �  /   �  3     8   @     y          �  (   �     �     �  #   �       9     P   M  :   �  !   �     �  	     B     &   T     {  4   �     �  
   �     �     �  ,   �  
        "     0  &   8     _     e     l     ~     �     �     �     �     �     �     �     �     �       *        :     G     P     X     t  0   �  )   �  2   �  <     @   Q  7   �  O   �  D     #   _     �  (   �     �     �     �     �        7                     B                 6   1      =   >       E               8       $              ;   %   -       ?   *             D       )   0       '              &      !      <   	   :   +   3                 A      /              C   
   #                          2                (   @   4       5             "       9   ,         .        A triangle Kaleidoscope Add a loopback alpha blending effector with rotating and scaling Add age to video input using scratches and dust Add more saturation to the video input Add some hallucination to the video input Add the ripple mark effect on the video input Bulge Bulges the center of the video Cartoon Cartoonify video input Che Guevara Chrome Detect radioactivity and show it Dice Dices the video input into many small squares Display video input like good old low resolution computer way Dissolves moving objects in the video input Distort the video input Distortion Edge Extracts edges in the video input through using the Sobel operator Fake heat camera toning Flip Flip the image, as if looking at a mirror Heat Historical Hulk Invert and slightly shade to blue Invert colors of the video input Invertion Kaleidoscope Kung-Fu Makes a square out of the center of the video Mauve Mirror Mirrors the video Noir/Blanc Optical Illusion Pinch Pinches the center of the video Quark Radioactive Ripple Saturation Sepia Sepia toning Shagadelic Show what was happening in the past Sobel Square Stretch Stretches the center of the video Time delay Traditional black-white optical animation Transform motions into Kung-Fu style Transform video input into a mauve color Transform video input into a metallic look Transform video input into a waveform monitor Transform video input into grayscale Transform video input into realtime goo'ing Transform video input into typical Che Guevara style Transform yourself into the amazing Hulk Twirl Twirl the center of the video Vertigo Warp Waveform X-Ray Project-Id-Version: gnome-video-effects.master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-video-effects&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-10-29 03:54+0000
PO-Revision-Date: 2015-11-10 03:41+0000
Last-Translator: Daniel Mustieles <daniel.mustieles@gmail.com>
Language-Team: Español <gnome-es-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:02+0000
X-Generator: Launchpad (build 18115)
 Un caleidoscopio Añadir efecto de bucle de mezcla alfa con rotación y escalado Añadir años a la entrada de vídeo usando arañazos y polvo Añadir más saturación a la entrada de vídeo Añadir algo de alucinación a la entrada de vídeo Añadir un efecto de ondulación en la salida del vídeo Bulto Abulta el centro del vídeo Dibujos animados Mostrar la entrada como dibujos animados Che Guevara Cromo Detectar radioactividad y mostrarla Pedazos Trocea la entrada de vídeo en muchos cuadrados pequeños Mostrar la entrada de vídeo de la forma de la resolución de monitores antiguos Disolver los objetos en movimiento en la entrada de vídeo Distorsionar la entrada de vídeo Distorsión Contornos Extrae los bordes de la entrada de vídeo usando el operador Sobel Tonalidad de calor de la cámara falsa Voltear Voltear la imagen, como si se reflejase en un espejo Calor Histórico Hulk Invertir y sombrear en azul Invertir los colores de la entrada de vídeo Inversión Caleidoscopio Kung-Fu Crea un cuadrado del centro del vídeo Malva Espejo Refleja el vídeo Blanco y negro Ilusión óptica Aspiración Aspira el centro del vídeo Quark Radioactivo Rizado Saturación Sepia Tonalidad sepia Psicodélico Mostrar lo que estaba pasando en el pasado Bordes Sobel Cuadrado Estirar Estira el centro del vídeo Retardo de tiempo Animación óptica en blanco y negro tradicional Transformar movimientos en estilo Kung-fu Transformar la entrada de vídeo en un color malva Transformar la entrada de vídeo en una apariencia metálica Transformar la entrada de vídeo en un monitor de formas de onda Transformar la entrada de vídeo a una escala de grises Transformar la entrada de vídeo con una distorsión «goo'ing» en tiempo real Transformar la entrada de vídeo en un estilo típico de Che Guevara Transformarse en el Increíble Hulk Remolino Crea un remolino en el centro del vídeo Vértigo Distorsión Forma de onda Rayos X 