��    �      �  �   �      �  *   �      �     �  	     *     0   8     i     q     �     �     �  /   �     �  ,   �  A        \      z  %   �  "   �  $   �  H   	      R  J   s  .   �  "   �       '   -     U  $   r  $   �  (   �  O   �  I   5  #        �  !   �     �  #   �  !   !  *   C  +   n  #   �  *   �     �               8     V  5   u  .   �     �  	   �       #        5  5   N  /   �     �     �  U   �  I   1  	   {     �     �     �     �     �     �     �  
   �     �     �       -   &     T     d     t     �     �     �     �     �  
   �     �     �     �     �          "     8  #   T  4   x  3   �  +   �  9     9   G     �     �  =   �  D   �  F   !  "   h     �     �     �  .   �  D   �  E   )     o     x  .   �     �     �     �     �     �               *     =     P     `     o     ~     �     �     �     �     �     �  *   �          #     4     E     V  b   e  X   �  .   !       P      q      �   9   �      �   :   �   �   !  N   "  ?   a"  1  �"     �#     �#     �#     $     $  8   $  (   S$  C   |$  �   �$  Q   J%     �%     �%  
   �%     �%     �%  	   �%  
   �%     �%     �%  .   �%  (   #&  Y   L&  N   �&  ,   �&  T   "'  =   w'  5   �'  5   �'  H   !(  \   j(  R   �(  ^   )  D   y)  D   �)     *  0   "*  �  S*  -   �+  /   ,     6,     E,  ,   R,  T   ,     �,     �,     �,     �,     -  >   -  (   M-  E   v-  N   �-  )   .  ,   5.  3   b.  '   �.  -   �.  L   �.  (   9/  T   b/  >   �/  +   �/     "0  -   A0     o0  +   �0  -   �0  7   �0  d    1  ^   �1  2   �1     2  (   42     ]2  (   z2  *   �2  4   �2  6   3  (   :3  2   c3  !   �3     �3      �3  /   �3  0   '4  M   X4  E   �4  "   �4     5      5  ,   45  &   a5  G   �5  $   �5  	   �5  #   �5  w   #6  N   �6      �6     7     "7     97  
   E7     P7     ]7     i7  
   v7     �7      �7  !   �7  5   �7     8     %8     <8     I8     U8     b8     q8     }8     �8     �8     �8     �8     �8     �8     �8     9  1   09  B   b9  E   �9  ;   �9  J   ':  J   r:     �:     �:  T   �:  c   K;  b   �;  '   <     :<     V<     [<  9   o<  Y   �<  Z   =     ^=     u=  :   ~=     �=     �=     �=     >     >     %>     ?>     X>     q>     �>     �>     �>     �>     �>     �>     �>     �>     ?     ?  ?   $?     d?     j?     {?     �?     �?  �   �?  h   7@  :   �@  $   �@      A     A  U   *A     �A  ?   �A  '  �A  n   C  _   qC  Y  �C     +E     CE     [E     iE     xE  C   �E  "   �E  I   �E  �   2F  i   �F     1G     6G  
   =G     HG     WG  	   gG  
   qG     |G     �G  8   �G  )   �G  �   �G  q   uH  2   �H  a   I  B   |I  J   �I  K   
J  a   VJ  p   �J  d   )K  �   �K  Y   L  Z   mL  %   �L  >   �L     *       H   +       �       u      6      �   Q           1       �       �   w   �   -      �   !   l   �       t   ^       �                      �   9   i          b   D   �   �   V   �      [   �   ,       s   Z   ~   C          �   �   G       �   P                  �       L   e   �       �   &   R       �   �       d       |   S   �      j   �   �   U       5   �   �   $       M      ;      �       �   K   �       W      %   �      �   E           a   �       `   �   =      '               �       �   �   �   3          T   �       �   x       #       ?   �      "   �       (   �   Y   q   )       �   \                     �   .   �       /   	           B   z       f   }           4   {       �          
   k       X   8   g             �   o   >       :      �          �   �       m   �       O   v   n      �   ]      �   �   �   A   y   p       0       _          �   @   2   c   �           �   r   <          h       J      I   7   N      �   F    %s: Unexpected length %d for genius_cuid!
 '%s' could not be accessed (%s). <No members>
 <Unnamed> Artwork support not compiled into libgpod. Cannot remove Photo Library playlist. Aborting.
 Classic Classic (Black) Classic (Silver) Color Color U2 Control directory not found: '%s' (or similar). Could not access file '%s'. Could not access file '%s'. Photo not added. Could not find corresponding track (dbid: %s) for artwork entry.
 Could not find on iPod: '%s'
 Could not open '%s' for writing. Couldn't find an iPod database on %s. Couldn't read xml sysinfo from %s
 Couldn't write SysInfoExtended to %s Destination file '%s' does not appear to be on the iPod mounted at '%s'. Device directory does not exist. Encountered unknown MHOD type (%d) while parsing the iTunesDB. Ignoring.

 Error adding photo (%s) to photo database: %s
 Error compressing iTunesCDB file!
 Error creating '%s' (mkdir)
 Error initialising iPod, unknown error
 Error initialising iPod: %s
 Error opening '%s' for reading (%s). Error opening '%s' for writing (%s). Error reading iPod photo database (%s).
 Error reading iPod photo database (%s).
Will attempt to create a new database.
 Error reading iPod photo database, will attempt to create a new database
 Error reading iPod photo database.
 Error removing '%s' (%s). Error renaming '%s' to '%s' (%s). Error when closing '%s' (%s). Error while reading from '%s' (%s). Error while writing to '%s' (%s). Error writing list of albums (mhsd type 4) Error writing list of artists (mhsd type 8) Error writing list of tracks (hths) Error writing list of tracks (mhsd type 1) Error writing mhsd type 10 Error writing mhsd type 6 Error writing mhsd type 9 Error writing mhsd5 playlists Error writing playlists (hphs) Error writing special podcast playlists (mhsd type 3) Error writing standard playlists (mhsd type 2) Error: '%s' is not a directory
 Grayscale Grayscale U2 Header is too small for iTunesCDB!
 Illegal filename: '%s'.
 Illegal seek to offset %ld (length %ld) in file '%s'. Insufficient number of command line arguments.
 Invalid Itdb_Track ID '%d' not found.
 Length of smart playlist rule field (%d) not as expected. Trying to continue anyhow.
 Library compiled without gdk-pixbuf support. Picture support is disabled. Master-PL Mini (1st Gen.) Mini (2nd Gen.) Mini (Blue) Mini (Gold) Mini (Green) Mini (Pink) Mini (Silver) Mobile (1) Mobile Phones Mountpoint not set. Mountpoint not set.
 Music directory not found: '%s' (or similar). Nano (1st Gen.) Nano (2nd Gen.) Nano (Black) Nano (Blue) Nano (Green) Nano (Orange) Nano (Pink) Nano (Purple) Nano (Red) Nano (Silver) Nano (White) Nano (Yellow) Nano Video (3rd Gen.) Nano Video (4th Gen.) Nano touch (6th Gen.) Nano with camera (5th Gen.) No 'F..' directories found in '%s'. Not a OTG playlist file: '%s' (missing mhpo header). Not a Play Counts file: '%s' (missing mhdp header). Not a iTunesDB: '%s' (missing mhdb header). Number of MHODs in mhip at %ld inconsistent in file '%s'. Number of MHODs in mhyp at %ld inconsistent in file '%s'. OTG Playlist OTG Playlist %d OTG playlist file '%s': reference to non-existent track (%d). OTG playlist file ('%s'): entry length smaller than expected (%d<4). OTG playlist file ('%s'): header length smaller than expected (%d<20). Path not found: '%s' (or similar). Path not found: '%s'. Photo Photo Library Photos directory not found: '%s' (or similar). Play Counts file ('%s'): entry length smaller than expected (%d<12). Play Counts file ('%s'): header length smaller than expected (%d<96). Playlist Podcasts Problem creating iPod directory or file: '%s'. Regular (1st Gen.) Regular (2nd Gen.) Regular (3rd Gen.) Regular (4th Gen.) Shuffle Shuffle (1st Gen.) Shuffle (2nd Gen.) Shuffle (3rd Gen.) Shuffle (4th Gen.) Shuffle (Black) Shuffle (Blue) Shuffle (Gold) Shuffle (Green) Shuffle (Orange) Shuffle (Pink) Shuffle (Purple) Shuffle (Red) Shuffle (Silver) Shuffle (Stainless) Specified album '%s' not found. Aborting.
 Touch Touch (2nd Gen.) Touch (3rd Gen.) Touch (4th Gen.) Touch (Silver) Unable to retrieve thumbnail (appears to be on iPod, but no image info available): filename: '%s'
 Unexpected error in itdb_photodb_add_photo_internal() while adding photo, please report. Unexpected image type in mhni: %d, offset: %d
 Unexpected mhod string type: %d
 Unexpected mhsd index: %d
 Unknown Unknown action (0x%x) in smart playlist will be ignored.
 Unknown command '%s'
 Unknown smart rule action at %ld: %x. Trying to continue.
 Usage to add photos:
  %s add <mountpoint> <albumname> [<filename(s)>]
  <albumname> should be set to 'NULL' to add photos to the master photo album
  (Photo Library) only. If you don't specify any filenames an empty album will
  be created.
 Usage to dump all photos to <output_dir>:
  %s dump <mountpoint> <output_dir>
 Usage to list all photos IDs to stdout:
  %s list <mountpoint>
 Usage to remove photo IDs from Photo Library:
  %s remove <mountpoint> <albumname> [<ID(s)>]
  <albumname> should be set to 'NULL' to remove photos from the iPod
  altogether. If you don't specify any IDs, the photoalbum will be removed
  instead.
  WARNING: IDs may change when writing the PhotoDB file.
 Video (1st Gen.) Video (2nd Gen.) Video (Black) Video (White) Video U2 Warning: could not find photo with ID <%d>. Skipping...
 Wrong number of command line arguments.
 You need to specify the iPod model used before photos can be added. Your iPod does not seem to support photos. Maybe you need to specify the correct iPod model number? It is currently set to 'x%s' (%s/%s). header length of '%s' smaller than expected (%d < %d) at offset %ld in file '%s'. iPad iPhone iPhone (1) iPhone (Black) iPhone (White) iPhone 3G iPhone 3GS iPhone 4 iPod iTunes directory not found: '%s' (or similar). iTunesCDB '%s' could not be decompressed iTunesDB '%s' corrupt: Could not find playlists (no mhsd type 2 or type 3 sections found) iTunesDB '%s' corrupt: Could not find tracklist (no mhsd type 1 section found) iTunesDB '%s' corrupt: mhsd expected at %ld. iTunesDB ('%s'): header length of mhsd hunk smaller than expected (%d<32). Aborting. iTunesDB corrupt: hunk length 0 for hunk at %ld in file '%s'. iTunesDB corrupt: no MHOD at offset %ld in file '%s'. iTunesDB corrupt: no SLst at offset %ld in file '%s'. iTunesDB corrupt: no section '%s' found in section '%s' starting at %ld. iTunesDB corrupt: number of mhip sections inconsistent in mhyp starting at %ld in file '%s'. iTunesDB corrupt: number of tracks (mhit hunks) inconsistent. Trying to continue.
 iTunesDB possibly corrupt: number of playlists (mhyp hunks) inconsistent. Trying to continue.
 iTunesStats file ('%s'): entry length smaller than expected (%d<18). iTunesStats file ('%s'): entry length smaller than expected (%d<32). usage: %s <device> [timezone]
 usage: %s <device|uuid|bus device> <mountpoint>
 Project-Id-Version: libgpod
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-04-13 08:40+0000
PO-Revision-Date: 2016-04-12 23:34+0000
Last-Translator: Alejandro Lamas Daviña <Unknown>
Language-Team: spanish <es_ES>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:52+0000
X-Generator: Launchpad (build 18115)
 %s: longitud inesperada %d para genius_cuid!
 No se ha podido acceder al archivo «%s» (%s). <No miembros>
 <Sin nombre> libgpod compilado sin soporte de carátulas. No se puede borrar la lista de reproducción de la biblioteca de fotos. Cancelando.
 Clásico Clásico (Negro) Clásico (Plata) Color Color U2 Directorio «iPod Control» no encontrado: «%s» (o similar). No se ha podido acceder al archivo «%sx No se ha podido acceder al archivo «%s». No se ha añadido la foto. No se ha podido encontrar la pista correspondiente (dbid: %s) a la carátula.
 Archivo no encontrado en el iPod: «%s»
 No se ha podido abrir «%s» para escritura. No se puede encontrar una base de datos iPod en %s. No se ha podido leer sysinfo xml de %s
 No se ha podido escribir SysInfoExtended a %s El destino del archivo «%s» parece no existir en el iPod montado en «%s» No existe el directorio del dispositivo. Se ha encontrado un tipo MHOD desconocido (%d) al analizar la iTunesDB. Ignorando.

 Error al añadir la foto (%s) a la base de datos de fotos: %s
 Error al comprimir el archivo de iTunesCDB
 Error al crear «%s» (mkdir)
 Error al inicializar iPod, error desconocido
 Error al inicializar iPod: %s
 Error al abrir «%s» para su lectura (%s). Error al abrir «%s» para su escritura (%s). Error al leer la base de datos de fotos del iPod (%s).
 Error al leer la base de datos de fotos del iPod (%s).
Se intentará crear una nueva base de datos.
 Error al leer la base de datos de fotos del iPod, se intentará crear una nueva base de datos
 Error al leer la base de datos de fotos del iPod.
 Error al borrar «%s» (%s). Error al renombrar «%s» a «%s» (%s). Error al cerrar «%s» (%s). Error durante la lectura de «%s» (%s). Error durante la escritura de «%s» (%s). Error al escribir la lista de álbumes (mhsd tipo 4) Error al escribir las listas de artistas (mhsd tipo 8) Error al escribir lista de pistas (hths) Error al escribir la lista de pistas (mhsd tipo 1) Error al escribir el tipo mhsd 10 Error al escribir mhsd tipo 6 Error al escribir el tipo mhsd 9 Error al escribir listas de reproducción mhsd5 Error al escribir listas de reproducción (hphs) Error al escribir la lista de reproducción especial de podcast (mhsd tipo 3) Error al escribir las listas de reproducción estándar (mhsd tipo 2) Error: «%s» no es un directorio
 Escala de grises Escala de grises U2 La cabecera es muy pequeña para iTunesCDB!
 Nombre de archivo no válido: «%s».
 Acceso ilegal al desplazamiento %ld (tamaño %ld) en el archivo «%s». Número de argumentos insuficiente.
 Inválido Itdb_Track ID «%dx no encontrada.
 La longitud del campo de reglas (%d) de la lista de reproducción inteligente no es la esperada. Intentando continuar.
 Biblioteca compilada sin soporte gdk-pixbuf. Soporte de imágenes desactivado. Lista de reproducción principal Mini (1ª generación) Mini (2ª generación) Mini (Azul) Mini (Oro) Mini (Verde) Mini (Rosa) Mini (Plata) Móvil (1) Teléfonos móviles Punto de montaje no configurado. Punto de montaje no configurado.
 Directorio «Musicx no encontrado: «%s» (o similar) Nano (1ª generación) Nano (2ª generación) Nano (Negro) Nano (Azul) Nano (Verde) Nano (Naranja) Nano (Rosa) Nano (Púrpura) Nano (Rojo) Nano (Plata) Nano (Blanco) Nano (Amarillo) Nano Video (3ª generación) Nano Video (4ª generación) Nano touch (6ª Gen.) Nano con cámara (5ª Gen.) No se ha encontrado ningún directorio en «%s». No es una lista de reproducción: «%s» (falta la cabecera mhpo). No es un archivo cuenta reproducciones: «%s» (falta cabecera mhdp). No es un archivo iTunesDB: «%s» (falta la cabecera mhdb). El número de MHODs en mhip en %ld no es consistente en el archivo «%s». El número de MHODs en mhyp en %ld no es consistente en el archivo «%s». Lista de reproducción OTG Lista de reproducción OTG %d La lista de reproducción OTG «%s»: hace referencia a una pista no existente (%d). Archivo («%s») de la lista de reproducción OTG: tamaño de la entrada menor del esperado (%d<4). Archivo de lista de reproducción OTG («%s»): tamaño de la cabecera menor del esperado (%d<20). Ruta no encontrada: «%s» (o similar). Ruta no encontrada: «%s». Foto Biblioteca de fotos Directorio «de fotosx no encontrado: «%s» (o similar). Archivo cuenta reproducciones («%s»): tamaño de la entrada menor del esperado (%d<12). Archivo cuenta reproducciones («%s»): tamaño de la cabecera menor del esperado (%d<96). Lista de reproducción Podcasts Problema al crear el directorio iPod o el archivo: «%s». Normal (1ª generación) Normal (2ª generación) Normal (3ª generación) Normal (4ª generación) Shuffle Shuffle (1ª generación) Suffle (2ª generación) Suffle (3ª generación) Shuffle (4ª Gen.) Shuffle (Negro) Shuffle (Azul) Shuffle (Oro) Shuffle (Verde) Shuffle (Naranja) Shuffle (Rosa) Shuffle (Púrpura) Shuffle (Rojo) Shuffle (Plata) Shuffle (Acero) No se ha encontrado el álbum especificado «%s». Cancelando.
 Touch Touch (2ª Gen.) Touch (3ª Gen.) Touch (4ª Gen.) Touch (Plata) Imposible cargar la imagen de muestra (parece estar en el iPod, pero no hay información disponible de la imagen): nombre de fichero: '%s'
 Error inesperado en  itdb_photodb_add_photo_internal() mientras se añadía la foto. Informe, por favor. Tipo de imagen inesperado en mhni: %d, desplazamiento: %d
 Tipo de cadena mhod no esperado: %d
 Índice mhsd no esperado: %d
 Desconocido Se ignorará la acción desconocida (0x%x) en la lista de reproducción inteligente.
 Orden desconocida «%s»
 Regla de acción desconocida en %ld: %x. Intentando continuar.
 Uso para añadir fotos:
  %s add <punto_de_montaje> <nombre_de_álbum> [<nombre_de_archivo(s)>]
  Para añadir fotos al álbum maestro de fotos, debe ponerse <nombre_de_álbum> como 'NULL' 
  (Sólo para la biblioteca de fotos). Si no se especifica ningún nombre, se creará
 un álbum vacío.
 Uso para volcar todas las fotos a <directorio_de_salida>:
  %s dump <punto_de_montaje> <directorio_de_salida>
 Uso para listar todas las IDs de las fotos a la salida estándar:
  %s list <punto_de_montaje>
 Uso para borrar IDs de fotos de la biblioteca de fotos:
  %s remove <punto_de_montaje> <nombre_de_álbum> [<ID(s)>]
  Para borrar las fotos también del iPod, debe ponerse <nombre_de_álbum> como 'NULL'.
  En caso de no especificar ningún ID, se borrará el álbum de fotos
  AVISO: los IDs pueden cambiar cuando se escribe el archivo PhotoDB.
 Video (1ª generación) Video (2ª generación) Video (Negro) Video (Blanco) Video U2 Aviso: no se ha podido encontrar la foto con ID <%d>. Ignorando...
 Número de argumentos incorrecto.
 Es necesario especificar el modelo del iPod antes de poder añadir fotos. Parece que el iPod no soporta fotos. Quizás sea necesario especificar el modelo de iPod correcto. Actualmente está configurado como 'x%s' (%s/%s). Tamaño de la cabecera de «%s» menor del esperado (%d<%d). en el desplazamiento %ld del archivo «%s». iPad iPhone iPhone (1) iPhone (Negro) iPhone (Blanco) iPhone 3G iPhone 3GS iPhone 4 iPod Directorio «iTunes» no encontrado: «%s» (o similar). iTunesCDB «%s» no se puede descomprimir iTunesDB «%s» corrputa: No se han encontrado las listas de reproducción (no se han encontrado las secciones mhsd de tipo 2 o tipo 3) iTunesDB «%s» corrputa: No se ha encontrado la lista de pistas (no se ha encontrado la sección mhsd de tipo 1) iTunesDB «%s» corrupta: se esperaba mhsd en %ld. iTunesDB («%s»): tamaño de la cabecera de la zona mhsd menor del esperado (%d<32). Cancelando. iTunesDB corrupta: tamaño 0 para la zona %ld en el archivo «%sx. iTunesDB corrupta: no hay MHOD en el desplazamiento %ld del archivo «%sx. iTunesDB corrupta: no hay SLst en el desplazamiento %ld del archivo «%s». iTunesDB corrupta: no se ha encontrado la sección «%s» en la sección «%s» empezando en %ld. iTunesDB corrupta: el número de secciones mhip no es consistente en mhyp empezando en %ld en el archivo «%s». iTunesDB esté corrupta: el número de pistas (zonas mhit) no es consistente. Intentando continuar.
 Es posible que iTunesDB esté corrupta: el número de listas de reproducción (zonas mhyp) no es consistente. Intentando continuar.
 Archivo cuenta reproducciones («%s»): tamaño de la entrada menor del esperado (%d<18). Archivo iTunesStats («%s»): longitud de la entrada más pequeña de lo esperado (%d<32). uso: %s <dispositivo> [zona horaria]
 uso: %s <dispositivo|uuid|dispositivo bus> <punto de montaje>
 