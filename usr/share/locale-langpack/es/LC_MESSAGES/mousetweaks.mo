��            )         �  "   �     �  &   �          #     A     Y     f     k      ~     �     �  
   �     �  
   �     �       %        D     T     n     �  e   �     �          5  	   D  !   N  /   p     �  �  �  .   �     �  7   �  /     1   >  +   p     �  	   �      �  )   �     	  (   	  
   F	     Q	     p	  -   |	     �	  .   �	     �	  #   �	  #   !
     E
  �   Y
  4   �
  >        N  
   ]  5   h  >   �     �                                                   
                     	                                                                - GNOME mouse accessibility daemon Button Style Button style of the click-type window. Click-type window geometry Click-type window orientation Click-type window style Double Click Drag Enable dwell click Enable simulated secondary click Failed to Display Help Hide the click-type window Horizontal Hover Click Icons only Ignore small pointer movements Orientation Orientation of the click-type window. Secondary Click Set the active dwell mode Shut down mousetweaks Single Click Size and position of the click-type window. The format is a standard X Window System geometry string. Start mousetweaks as a daemon Start mousetweaks in login mode Text and Icons Text only Time to wait before a dwell click Time to wait before a simulated secondary click Vertical Project-Id-Version: mousetweaks.master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=mousetweaks&component=general
POT-Creation-Date: 2015-12-03 23:45+0000
PO-Revision-Date: 2012-02-22 07:34+0000
Last-Translator: Paco Molinero <paco@byasl.com>
Language-Team: Español <gnome-es-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:24+0000
X-Generator: Launchpad (build 18115)
 - Demonio de accesibilidad del ratón de GNOME Estilo de botón Estilo del botón de la ventana del tipo de pulsación. Geometría de la ventana del tipo de pulsación Orientación de la ventana del tipo de pulsación Estilo de la ventana del tipo de pulsación Pulsación doble Arrastrar Activar la pulsación al posarse Activar la pulsación secundaria simulada Falló al mostrar la ayuda Ocultar la ventana de tipo de pulsación Horizontal Pulsación al pasar por encima Solo iconos Ignorar los movimientos pequeños del puntero Orientación Orientación de la ventana del tipo de botón. Pulsación secundaria Establecer el modo de posado activo Apagar los ajustes finos del ratón Pulsación sencilla Tamaño y posición de la ventana del tipo de pulsación. El formato es una cadena de geometría estándar del sistema X Window. Iniciar los ajustes finos del ratón como un demonio Iniciar los ajustes finos del ratón en modo inicio de sesión Iconos y texto Solo texto Tiempo que esperar antes de una pulsación al posarse Tiempo que esperar antes de una pulsación secundaria simulada Vertical 