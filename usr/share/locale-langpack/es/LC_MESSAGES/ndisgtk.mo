��          �   %   �      p     q     �  +   �  @   �  5        M  ,   _     �     �     �  $   �  	   �  2   �  $   2     W     Z     l  #   �  !   �     �  %   �               8     <     E     Y  �  h     �  "     7   1  G   i  <   �     �  ?   �  !   =     _     }  )   �     �  7   �  8    	     9	     <	  *   Z	  !   �	  *   �	     �	  -   �	  2   
  $   J
     o
  	   s
     }
     �
                    
                                                                                          	             <b>%s</b>
Hardware present: %s <b>%s</b>
Invalid Driver! <b>Currently Installed Windows Drivers:</b> <span size="larger" weight="bold">Select <i>inf</i> file:</span> Are you sure you want to remove the <b>%s</b> driver? Configure Network Could not find a network configuration tool. Driver is already installed. Error while installing. Install Driver Is the ndiswrapper module installed? Location: Module could not be loaded. Error was:

<i>%s</i>
 Ndiswrapper driver installation tool No No file selected. Not a valid driver .inf file. Please drag an '.inf' file instead. Root or sudo privileges required! Select inf File Unable to see if hardware is present. Windows Wireless Drivers Wireless Network Drivers Yes _Install _Install New Driver _Remove Driver Project-Id-Version: ndisgtk
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2008-10-02 17:50+0200
PO-Revision-Date: 2010-10-09 04:07+0000
Last-Translator: Paco Molinero <paco@byasl.com>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:44+0000
X-Generator: Launchpad (build 18115)
 <b>%s</b>
Hardware presente: %s <b>%s</b>
¡Controlador inválido! <b>Controladores de Windows actualmente instalados:</b> <span size="larger" weight="bold">Seleccione archivo <i>inf</i>:</span> ¿Está seguro que quiere eliminar el controlador <b>%s</b>? Configurar red No se puede encontrar una herramienta de configuración de red. El controlador ya está instalado Error durante la instalación Instalar controlador ¿Está el módulo ndiswrapper instalado? Ubicación: El módulo no se pudo cargar. El error fue:

<i>%s</i>
 Herramienta de instalación de controladores Ndiswrapper No No se seleccionó un archivo. No es un archivo controlador .inf válido. Coloque sólo un archivo «.inf» ¡Se necesitan privilegios de root o sudo! Seleccione archivo inf Incapaz de ver si el hardware está presente. Controladores para redes inalámbricas de Windows. Controladores de redes inalámbricas Sí _Instalar _Instalar  controlador nuevo _Eliminar controlador 