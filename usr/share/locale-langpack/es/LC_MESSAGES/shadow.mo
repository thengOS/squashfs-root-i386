��    $     <  �  \"      �-     �-     �-     �-  #   �-  &   .  _   3.  :   �.  -   �.     �.  #   /  j   8/  N   �/  K   �/  p   >0  A   �0  s   �0  B   e1  6   �1  ~   �1  A   ^2  H   �2  u   �2  H   _3  9   �3  L   �3  8   /4  N   h4  I   �4  S   5  2   U5  I   �5  F   �5  G   6  y   a6  K   �6  E   '7  ?   m7  =   �7  J   �7  7   68  N   n8  H   �8  B   9  M   I9  A   �9  P   �9  C   *:  K   n:  �   �:  x   S;  r   �;  8   ?<  N   x<  s   �<  ;   ;=  :   w=  E   �=  s   �=  4   l>  y   �>  C   ?  @   _?  s   �?  @   @  H   U@  >   �@  ?   �@  G   A  P   eA  <   �A  y   �A  A   mB  I   �B  x   �B  ~   rC  �   �C  ~   }D  =   �D  w   :E  v   �E  J   )F  G   tF  F   �F  5   G  q   9G  F   �G  L   �G  N   ?H  A   �H  +   �H  3   �H  t   0I  E   �I  <   �I  I   (J  E   rJ  8   �J  7   �J  s   )K  @   �K  ?   �K  E   L  4   dL  N   �L  Q   �L  C   :M  =   ~M  ;   �M  I   �M  E   BN  L   �N  �   �N  I   �O  ~   �O     |P  #   �P     �P     �P  $   �P     �P     	Q  
   Q     !Q  b   *Q  
   �Q     �Q     �Q     �Q     �Q  -   �Q  %   �Q     R  .   7R  6   fR     �R     �R  %   �R  -   �R  %    S  '   FS  #   nS  ,   �S  ,   �S     �S  '   T     /T     OT  9   kT  2   �T  2   �T  9   U  9   EU  !   U  "   �U  #   �U  '   �U  #   V  '   4V  %   \V     �V  )   �V     �V  "   �V     W     $W  0   <W  %   mW     �W  #   �W  '   �W  (   �W     "X  "   BX     eX  b   }X  4   �X  *   Y  (   @Y  +   iY  '   �Y  "   �Y     �Y     �Y     Z  8   8Z  O   qZ  O   �Z  8   [     J[     a[  %   p[     �[     �[  "   �[  q   �[     P\     e\     �\     �\  -   �\     �\  "   ]  .   .]  <   ]]     �]     �]  2   �]      �]  *   ^  +   H^     t^  $   �^     �^     �^  %   �^     
_  "   _  %   A_  1   g_  %   �_     �_  D   �_     #`  (   <`     e`  $   �`      �`  "   �`  ,   �`  A   a  A   Xa  #   �a  (   �a  ,   �a     b  5   -b  5   cb     �b     �b     �b  '   �b  (   c     4c  J   Ic     �c  F   �c  7   �c     0d     Od  $   nd     �d     �d  C   �d      e     7e     Ue     oe     �e     �e     �e      �e     �e     f  "   -f     Pf     of     �f     �f     �f      �f     �f      g  #   =g  !   ag  5   �g  '   �g     �g     �g  "   h  !   ;h  &   ]h  ,   �h     �h     �h     �h       i  )   !i  @   Ki     �i     �i  =   �i  0   �i     j  ,   ;j  %   hj     �j     �j     �j     �j  )   �j     k      /k  0   Pk  +   �k  %   �k  ,   �k  %    l     &l     >l  /   ^l  +   �l  +   �l      �l  ,   m  (   4m  )   ]m     �m     �m     �m  �   �m  �   cn  !   �n     o     5o  #   So     wo  #   �o  *   �o  &   �o      p  !   ,p  >   Np  b   �p  A   �p  0   2q     cq     }q     �q  %   �q  $   �q     �q  	   �q     r     r  $   1r      Vr  !   wr     �r  "   �r  +   �r  (   s     ,s  /   Ls     |s     �s     �s     �s  #   �s  (   �s  !   %t     Gt     gt  &   �t      �t  #   �t  %   �t  #   u  *   7u  %   bu  &   �u     �u  "   �u     �u     v     )v     Gv     av     tv     �v  x   �v  �   w  3   �w      �w     �w  B   x  	   Nx  G   Xx  
   �x     �x  7   �x     �x     y     %y  !   By     dy     |y     �y  9   �y     �y     �y  D   z     Hz  6   ]z  &   �z     �z  6   �z  G   {     O{     ^{     m{  $   p{     �{     �{     �{     �{  @   �{  8   ,|     e|     t|     z|     �|  "   �|     �|     �|  
   �|  2   }     :}     R}     r}     ~}  (   �}      �}     �}     �}      ~  +   -~  '   Y~  "   �~     �~     �~     �~  
   �~     �~  /     "   A     d  #   {     �  $   �  $   �  H   �  &   Q�  '   x�  0   ��  &   р  0   ��  '   )�  
   Q�     \�     l�     |�     ��  �  ��  '   r�  1   ��  2   ̃     ��     �  
   4�      ?�     `�  g   o�     ׄ     �  '   �     *�     I�     a�     |�  '   ��      ��  #   ��     �     �     $�     ;�     Q�  1   c�  1   ��  ?   ǆ     �  -   $�  #   R�     v�     ��     ��     ��     ͇     �     
�     $�     C�  "   `�  &   ��     ��     Ĉ     ވ     ��     �     .�  L   L�     ��     ��     ǉ     ��     ��     �  "   5�     X�     o�     ��     ��  !   ��     Ǌ     �  	   �  #   ��  &   �     A�     Z�  %   f�     ��  &   ��     ϋ     �     ��  +   	�     5�     R�  	   c�     m�  
   y�  K   ��  ,   Ќ  )   ��     '�  '   @�    h�     �     ��     ��  ,   ��  ,   ď  �   �  O   ��  I   ��     ?�  )   [�  p   ��  }   ��  L   t�  r   ��  ?   4�  �   t�     ��  =   �  �   ��  H   Q�  K   ��  t   �  K   [�  B   ��  x   �  @   c�  q   ��  {   �  y   ��  8   �  H   E�  y   ��  |   �     ��  y   �  Y   �  ?   ٛ  <   �  M   V�  :   ��  r   ߜ  L   R�  G   ��  �   �  J   p�  q   ��  J   -�  {   x�  �   ��  l   ~�  �   �  L   x�  |   š  �   B�  H   Ӣ  @   �  w   ]�  }   գ  A   S�  �   ��  H   X�  O   ��  �   �  K   s�  Q   ��  =   �  P   O�  M   ��  �   �  =   ~�     ��  H   <�  w   ��  z   ��  �   x�  �   �  �   ȫ  ?   b�  u   ��  �   �  N   ��  I   �  J   ;�  E   ��  O   ̮  H   �  R   e�  v   ��  C   /�  0   s�  +   ��  u   а  u   F�  @   ��  L   ��  I   J�  <   ��  9   Ѳ  z   �  H   ��  E   ϳ  s   �  *   ��  {   ��  �   0�  H   ��  v   ��  M   v�  P   Ķ  t   �  O   ��    ڷ  O   �  �   :�     ι      ��     �      �  '   .�     V�     i�     x�     ��  x   ��     �     �     &�     +�     3�  >   F�  .   ��  "   ��  7   ׻  :   �     J�     d�  +   ~�  *   ��  (   ռ  ;   ��  ,   :�  0   g�  :   ��     ӽ  6   �     '�  -   F�  C   t�  C   ��  C   ��  O   @�  O   ��  )   �  +   
�  1   6�  6   h�  *   ��  0   ��  0   ��  .   ,�  1   [�  >   ��  .   ��  +   ��     '�  ;   D�  0   ��     ��  -   ��  9   ��  /   9�  %   i�  4   ��  *   ��  j   ��  D   Z�  ;   ��  @   ��  K   �  K   h�  :   ��     ��     �     +�  <   I�  S   ��  S   ��  <   .�     k�     ��  +   ��     ��     ��  0   ��  y   �     ��     ��     ��  $   ��  =   �     N�  "   g�  2   ��  E   ��     �      #�  <   D�  (   ��  4   ��  <   ��  '   �  G   D�     ��     ��  1   ��     ��  G   �  0   V�  =   ��  0   ��  .   ��  ]   %�     ��  ,   ��     ��  '   ��  &   �  0   <�  8   m�  [   ��  [   �  @   ^�  :   ��  ?   ��  '   �  K   B�  K   ��  *   ��  0   �  4   6�  9   k�  5   ��     ��  M   ��      F�  F   g�  7   ��     ��     �  $   $�  $   I�  %   n�  I   ��  &   ��  "   �  !   (�     J�     g�  -   ��  '   ��  5   ��  )   �     :�  *   Y�  -   ��     ��  /   ��  (   �  -   *�  )   X�  +   ��  1   ��  5   ��  #   �  J   :�  )   ��  !   ��  &   ��  *   ��  #   #�  +   G�  1   s�  '   ��     ��     ��  &    �  >   '�  Q   f�     ��     ��  W   ��  -   6�  !   d�  @   ��  +   ��  -   ��     !�     2�     K�  :   f�     ��      ��  C   ��  7   �  0   U�  5   ��  0   ��  +   ��  ?   �  ,   Y�  ,   ��  ,   ��  $   ��  9   �  8   ?�  1   x�     ��     ��     ��  �   ��  �   ��  )   Z�  $   ��      ��  &   ��      ��  &   �  1   9�  .   k�      ��  '   ��  M   ��  e   1�  X   ��  <   ��  "   -�     P�     f�  1   i�  ,   ��     ��  
   ��  %   ��     �  0   0�  7   a�  ,   ��  N   ��  .   �  A   D�  6   ��  0   ��  :   ��     )�     A�  &   Z�  +   ��  D   ��  7   ��  5   *�  3   `�  !   ��  -   ��  '   ��  *   �  -   7�  =   e�  D   ��  ?   ��  >   (�  $   g�  2   ��  %   ��  %   ��  &   �  ,   2�     _�  +   �  '   ��  �   ��  �   e�  B   
�  0   M�     ~�  B   ��     ��  l   ��     Y�      k�  Z   ��     ��     �  #   �  *   @�     k�     ��  $   ��  <   ��     �     �  U   /�  #   ��  <   ��  *   ��  #   �  <   5�  _   r�     ��     ��     ��  %   ��     #�  2   /�  +   b�     ��  I   ��  D   ��     <�     R�  $   W�     |�  '   ��     ��     ��     ��  A    �  *   B�  &   m�     ��      ��  *   ��  ;   ��     3�     D�  $   Z�  3   �  ,   ��  ,   ��  "   �  $   0�     U�     j�  *   �  T   ��  *   ��     *�  ,   A�  &   n�  ,   ��  .   ��  _   ��  0   Q�  0   ��  8   ��  .   ��  8   �  ,   T�     ��     ��     ��     ��     ��    ��  (   ��  2   �  :   B�     }�     ��     ��  !   ��     ��  u   ��     p�     ��  ,   ��     ��     ��     ��     �  '   5�     ]�  !   {�     ��     ��  '   ��  ,   ��  $   �  @   9�  @   z�  P   ��  (   �  ;   5�  3   q�  #   ��  $   ��     ��      	  7   *  4   b  '   �  B   �  >    3   A 7   u 6   � *   � 0    =   @ ?   ~ 1   � e   � "   V *   y '   � "   � -   � 7    5   U ,   � $   �    �    � #    "   &    I    O ?   [ =   � $   �    � %   
 )   0 .   Z    �    �    � ;   � ,       1    D    T    f j   w H   � /   + '   [ -   �        �     _  <       	  C  o      =  �  ~       �   �   �  �     u   �          �    �   �  �  w      O  �  �   �   �   L  g      !          �  �  �     �  �   �  �       �           �   	  �      W  &  �   �   �  �   ]  I   �      �         H   r       �        �           �   �     �              k       �          �  r      A     �  Y   �  �      b  �  u  �  
   d         �      ?                �           �     /  �   �    "   �   �  F  -  �  p  l  �  \      �  �    �  �           5           �  m       �   �   �  �  �  �         �   k  �  �  �  N   ,  X   �               q  �   �           $       �   �       �  S              �        �  D   Z   �   2   `   �   �   p       (    7   0  �   .        �      U        ;  {                J      �   �       �  $  ;   �  e      G      �   I  Q   �   5   �       s          �  K  P  @   �   3           z  �   �       �   �       y   �   L     �  ^       >       �   �           ,   :   �  $  �      �   0   �  �      �   �   �  9  �           �   �  K       �   O           {     �  �  s          V  �   A  c   �      �   �  %   "             a   )   e   �                 �     �   P   x      �   �  h  �    Y      �        �  �      �  o    z   �          �  �   �      d   �   c  �       V   �   �  �  �  !     ]   v       _   �  �  2  �                   �       |  |   #   �     �         4  &   +     �  T   f            	   �   �         7  [        �           �   �            G   t      l   �                  i      D  ?          �       �  �  "  �   M    �    �  @  �   �  T  �   N             W           )  �   �   �        �  �       �  �  �   }           t   �          �   Q  1         S   �      w   �  -   �           �   h   �   �    E      F   �  �   �   �         �   �       �      �   �      �  8  #  �   �  ^                   =     �     }     �   �  C   !  .      �   �    �  �   �   #      �           �      �   �        �          �      y                  �   '  �  b   �  �  �           i          �         ~  +  B  �              E   '   �       [     n      �  �   R  6   x   f   �          �          j               �          B      �  �  �              U           �  
  �  a  �  M   �          %      �  �   �       v  g       �  �   :      �  9      `    �   �   �     �   �   >  �  Z    �   �  �   m  �  8             �  (   *  �  q   *     �  4       j  �   �  �  �       �      �  1  <  �              �         6      /       �  �   3          
     n   J           �  R           \  �          �     �     �           X  �   H      �   �    	%s: %s
 
 
%s login:  
Login timed out after %u seconds.
 
System closed for routine maintenance 
Type control-d to proceed with normal startup,
(or give root password for system maintenance): 
Warning: weak password (enter it again to use it anyway). 
[Disconnect bypassed -- root login allowed.]        %s [-p] -r host
        %s [-p] [-h host] [-f name]
   -A, --administrators ADMIN,...
                                set the list of administrators for GROUP
   -D, --defaults                print or change default useradd configuration
   -E, --expiredate EXPIRE_DATE  set account expiration date to EXPIRE_DATE
   -G, --groups GROUPS           list of supplementary groups of the new
                                account
   -G, --groups GROUPS           new list of supplementary GROUPS
   -I, --inactive INACTIVE       set password inactive after expiration
                                to INACTIVE
   -K, --key KEY=VALUE           override /etc/login.defs defaults
   -L, --lock                    lock the user account
   -M, --maxdays MAX_DAYS        set maximim number of days before password
                                change to MAX_DAYS
   -M, --members USER,...        set the list of members of GROUP
   -M, --no-create-home          do not create the user's home directory
   -N, --no-user-group           do not create a group with the same name as
                                the user
   -R, --restrict                restrict access to GROUP to its members
   -R, --root CHROOT_DIR         directory to chroot into
   -S, --status                  report password status on the named account
   -U, --unlock                  unlock the user account
   -U, --user-group              create a group with the same name as the user
   -W, --warndays WARN_DAYS      set expiration warning days to WARN_DAYS
   -Z, --selinux-user SEUSER     use a specific SEUSER for the SELinux user mapping
   -a, --add USER                add USER to GROUP
   -a, --add username            add username to the members of the group
   -a, --all                     display faillog records for all users
   -a, --all                     report password status on all accounts
   -b, --base-dir BASE_DIR       base directory for the home directory of the
                                new account
   -b, --before DAYS             print only lastlog records older than DAYS
   -c, --check                   check the user's password expiration
   -c, --comment COMMENT         GECOS field of the new account
   -c, --comment COMMENT         new value of the GECOS field
   -d, --delete                  delete the password for the named account
   -d, --delete USER             remove USER from GROUP
   -d, --delete username         remove username from the members of the group
   -d, --home HOME_DIR           new home directory for the user account
   -d, --home-dir HOME_DIR       home directory of the new account
   -d, --lastday LAST_DAY        set date of last password change to LAST_DAY
   -e, --encrypted               supplied passwords are encrypted
   -e, --expire                  force expire the password for the named account
   -e, --expiredate EXPIRE_DATE  expiration date of the new account
   -e, --expiredate EXPIRE_DATE  set account expiration date to EXPIRE_DATE
   -f, --force                   exit successfully if the group already exists,
                                and cancel -g if the GID is already used
   -f, --force                   force password change if the user's password
                                is expired
   -f, --force                   force removal of files,
                                even if not owned by user
   -f, --full-name FULL_NAME     change user's full name
   -f, --inactive INACTIVE       password inactivity period of the new account
   -f, --inactive INACTIVE       set password inactive after expiration
                                to INACTIVE
   -g, --gid GID                 change the group ID to GID
   -g, --gid GID                 use GID for the new group
   -g, --gid GROUP               force use GROUP as new primary group
   -g, --gid GROUP               name or ID of the primary group of the new
                                account
   -g, --group                   edit group database
   -g, --group groupname         change groupname instead of the user's group
                                (root only)
   -h, --help                    display this help message and exit
   -h, --home-phone HOME_PHONE   change user's home phone number
   -i, --inactive INACTIVE       set password inactive after expiration
                                to INACTIVE
   -k, --keep-tokens             change password only if expired
   -k, --skel SKEL_DIR           use this alternative skeleton directory
   -l, --list                    list the members of the group
   -l, --list                    show account aging information
   -l, --lock                    lock the password of the named account
   -l, --lock-secs SEC           after failed login lock account for SEC seconds
   -l, --login NEW_LOGIN         new value of the login name
   -l, --no-log-init             do not add the user to the lastlog and
                                faillog databases
   -m, --create-home             create the user's home directory
   -m, --maximum MAX             set maximum failed login counters to MAX
   -m, --md5                     encrypt the clear text password using
                                the MD5 algorithm
   -m, --mindays MIN_DAYS        set minimum number of days before password
                                change to MIN_DAYS
   -m, --move-home               move contents of the home directory to the
                                new location (use only with -d)
   -n, --mindays MIN_DAYS        set minimum number of days before password
                                change to MIN_DAYS
   -n, --new-name NEW_GROUP      change the name to NEW_GROUP
   -o, --non-unique              allow to create groups with duplicate
                                (non-unique) GID
   -o, --non-unique              allow to create users with duplicate
                                (non-unique) UID
   -o, --non-unique              allow to use a duplicate (non-unique) GID
   -o, --non-unique              allow using duplicate (non-unique) UID
   -o, --other OTHER_INFO        change user's other GECOS information
   -p, --passwd                  edit passwd database
   -p, --password PASSWORD       change the password to this (encrypted)
                                PASSWORD
   -p, --password PASSWORD       encrypted password of the new account
   -p, --password PASSWORD       use encrypted password for the new password
   -p, --password PASSWORD       use this encrypted password for the new group
   -p, --purge                   purge all members from the group
   -q, --quiet                   quiet mode
   -q, --quiet                   report errors only
   -r, --read-only               display errors and warnings
                                but do not change files
   -r, --remove                  remove home directory and mail spool
   -r, --remove-password         remove the GROUP's password
   -r, --repository REPOSITORY   change password in REPOSITORY repository
   -r, --reset                   reset the counters of login failures
   -r, --system                  create a system account
   -r, --system                  create system accounts
   -s, --sha-rounds              number of SHA rounds for the SHA*
                                crypt algorithms
   -s, --shadow                  edit shadow or gshadow database
   -s, --shell SHELL             login shell of the new account
   -s, --shell SHELL             new login shell for the user account
   -s, --sort                    sort entries by UID
   -t, --time DAYS               display faillog records more recent than DAYS
   -t, --time DAYS               print only lastlog records more recent than DAYS
   -u, --help                    display this help message and exit
   -u, --uid UID                 new UID for the user account
   -u, --uid UID                 user ID of the new account
   -u, --unlock                  unlock the password of the named account
   -u, --user                    which user's tcb shadow file to edit
   -u, --user LOGIN              print lastlog record of the specified LOGIN
   -u, --user LOGIN/RANGE        display faillog record or maintains failure
                                counters and limits (if used with -r, -m,
                                or -l) only for the specified LOGIN(s)
   -w, --warndays WARN_DAYS      set expiration warning days to WARN_DAYS
   -x, --maxdays MAX_DAYS        set maximum number of days before password
                                change to MAX_DAYS
   Choose a new password.   Contact the system administrator.  ...killed.
  ...terminated.
  ...waiting for child to terminate.
  [%lds lock]  [%lus left]  from %.*s  groups= %d failure since last login.
Last was %s on %s.
 %d failures since last login.
Last was %s on %s.
 %s login:  %s's Password:  %s:  %s: %s
 %s: %s
(Ignored)
 %s: %s flag is only allowed with the %s flag
 %s: %s home directory (%s) not found
 %s: %s is an invalid shell
 %s: %s is neither a directory, nor a symlink.
 %s: %s is not authorized to change the password of %s
 %s: %s is the NIS master
 %s: %s is unchanged
 %s: %s not owned by %s, not removing
 %s: %s was created, but could not be removed
 %s: '%s' contains illegal characters
 %s: '%s' contains non-ASCII characters
 %s: '%s' is not a valid group name
 %s: '%s' is the NIS master for this client.
 %s: (line %d, user %s) password not changed
 %s: -K requires KEY=VALUE
 %s: -Z requires SELinux enabled kernel
 %s: -s and -r are incompatible
 %s: Authentication failure
 %s: Can't allocate memory, tcb entry for %s not removed.
 %s: Can't get unique GID (no more available GIDs)
 %s: Can't get unique UID (no more available UIDs)
 %s: Can't get unique system GID (no more available GIDs)
 %s: Can't get unique system UID (no more available UIDs)
 %s: Cannot change mode of %s: %s
 %s: Cannot change owner of %s: %s
 %s: Cannot change owners of %s: %s
 %s: Cannot create backup file (%s): %s
 %s: Cannot create directory %s: %s
 %s: Cannot create symbolic link %s: %s
 %s: Cannot determine your user name.
 %s: Cannot drop privileges: %s
 %s: Cannot drop the controlling terminal
 %s: Cannot fork user shell
 %s: Cannot get the size of %s: %s
 %s: Cannot lstat %s: %s
 %s: Cannot open %s: %s
 %s: Cannot possibly work without effective root
 %s: Cannot read symbolic link %s: %s
 %s: Cannot remove %s: %s
 %s: Cannot remove directory %s: %s
 %s: Cannot remove tcb files for %s: %s
 %s: Cannot remove the content of %s: %s
 %s: Cannot rename %s to %s: %s
 %s: Cannot setup cleanup service.
 %s: Cannot stat %s: %s
 %s: Emergency: %s's tcb shadow is not a regular file with st_nlink=1.
The account is left locked.
 %s: Failed to change ownership of the home directory %s: Failed to create tcb directory for %s
 %s: Failed to get the entry for UID %lu
 %s: Failed to reset fail count for UID %lu
 %s: Failed to set locktime for UID %lu
 %s: Failed to set max for UID %lu
 %s: Failed to write %s: %s
 %s: GID '%lu' already exists
 %s: GID '%lu' does not exist
 %s: Invalid configuration: GID_MIN (%lu), GID_MAX (%lu)
 %s: Invalid configuration: SYS_GID_MIN (%lu), GID_MIN (%lu), SYS_GID_MAX (%lu)
 %s: Invalid configuration: SYS_UID_MIN (%lu), UID_MIN (%lu), SYS_UID_MAX (%lu)
 %s: Invalid configuration: UID_MIN (%lu), UID_MAX (%lu)
 %s: Invalid entry: %s
 %s: Not a tty
 %s: Out of memory. Cannot update %s.
 %s: PAM: %s
 %s: Permission denied.
 %s: Suspiciously long symlink: %s
 %s: The previous home directory (%s) was not a directory. It is not removed and no home directories are created.
 %s: Try again later
 %s: UID %lu is not unique
 %s: UID '%lu' already exists
 %s: Unknown user or range: %s
 %s: Warning, user %s has no tcb shadow file.
 %s: Warning: %s does not exist
 %s: Warning: %s is not executable
 %s: You are not authorized to su at that time
 %s: You may not view or modify password information for %s.
 %s: can't create group
 %s: can't create user
 %s: can't restore %s: %s (your changes are in %s)
 %s: can't work with tcb enabled
 %s: cannot access chroot directory %s: %s
 %s: cannot change user '%s' on NIS client.
 %s: cannot create directory %s
 %s: cannot create new defaults file
 %s: cannot delete %s
 %s: cannot lock %s.
 %s: cannot lock %s; try again later.
 %s: cannot open %s
 %s: cannot open new defaults file
 %s: cannot remove entry '%s' from %s
 %s: cannot remove the primary group of user '%s'
 %s: cannot rename directory %s to %s
 %s: cannot sort entries in %s
 %s: cannot update the entry of user %s (not in the passwd database)
 %s: directory %s exists
 %s: do not include "l" with other flags
 %s: error changing fields
 %s: error detected, changes ignored
 %s: error removing directory %s
 %s: failed to allocate memory: %s
 %s: failed to change the mode of %s to 0600
 %s: failed to copy the faillog entry of user %lu to user %lu: %s
 %s: failed to copy the lastlog entry of user %lu to user %lu: %s
 %s: failed to drop privileges (%s)
 %s: failed to find tcb directory for %s
 %s: failed to prepare the new %s entry '%s'
 %s: failed to remove %s
 %s: failed to reset the faillog entry of UID %lu: %s
 %s: failed to reset the lastlog entry of UID %lu: %s
 %s: failed to unlock %s
 %s: failure forking: %s %s: failure forking: %s
 %s: failure while closing read-only %s
 %s: failure while writing changes to %s
 %s: fields too long
 %s: group %s exists - if you want to add this user to that group, use -g.
 %s: group %s is a NIS group
 %s: group %s is the primary group of another user and is not removed.
 %s: group %s not removed because it has other members.
 %s: group '%s' already exists
 %s: group '%s' does not exist
 %s: group '%s' does not exist in %s
 %s: group '%s' is a NIS group
 %s: group '%s' is a NIS group.
 %s: group '%s' is a shadow group, but does not exist in /etc/group
 %s: invalid base directory '%s'
 %s: invalid chroot path '%s'
 %s: invalid comment '%s'
 %s: invalid date '%s'
 %s: invalid field '%s'
 %s: invalid group ID '%s'
 %s: invalid group name '%s'
 %s: invalid home directory '%s'
 %s: invalid home phone: '%s'
 %s: invalid name: '%s'
 %s: invalid numeric argument '%s'
 %s: invalid room number: '%s'
 %s: invalid shell '%s'
 %s: invalid user ID '%s'
 %s: invalid user name '%s'
 %s: invalid work phone: '%s'
 %s: line %d: can't create group
 %s: line %d: can't create user
 %s: line %d: can't update entry
 %s: line %d: can't update password
 %s: line %d: chown %s failed: %s
 %s: line %d: failed to prepare the new %s entry '%s'
 %s: line %d: group '%s' does not exist
 %s: line %d: invalid line
 %s: line %d: line too long
 %s: line %d: missing new password
 %s: line %d: mkdir %s failed: %s
 %s: line %d: user '%s' does not exist
 %s: line %d: user '%s' does not exist in %s
 %s: line too long in %s: %s... %s: mkdir: %s: %s
 %s: multiple --root options
 %s: must be run from a terminal
 %s: name with non-ASCII characters: '%s'
 %s: no alternative shadow file allowed when USE_TCB is enabled.
 %s: no changes
 %s: no options
 %s: not removing directory %s (would remove home of user %s)
 %s: nscd did not terminate normally (signal %d)
 %s: nscd exited with status %d
 %s: only root can use the -g/--group option
 %s: option '%s' requires an argument
 %s: options %s and %s conflict
 %s: out of memory
 %s: pam_start: error %d
 %s: password changed.
 %s: password expiry information changed.
 %s: rename: %s: %s
 %s: repository %s not supported
 %s: room number with non-ASCII characters: '%s'
 %s: shadow group passwords required for -A
 %s: shadow passwords required for -e
 %s: shadow passwords required for -e and -f
 %s: shadow passwords required for -f
 %s: signal malfunction
 %s: signal masking malfunction
 %s: the %s configuration in %s will be ignored
 %s: the -L, -p, and -U flags are exclusive
 %s: the -c, -e, and -m flags are exclusive
 %s: the files have been updated
 %s: the shadow password file is not present
 %s: too many groups specified (max %d).
 %s: unable to chroot to directory %s: %s
 %s: unexpected argument: %s
 %s: unknown user %s
 %s: unlink: %s: %s
 %s: unlocking the password would result in a passwordless account.
You should set a password with usermod -p to unlock the password of this account.
 %s: unlocking the user's password would result in a passwordless account.
You should set a password with usermod -p to unlock this user's password.
 %s: unsupported crypt method: %s
 %s: user %s is a NIS user
 %s: user '%s' already exists
 %s: user '%s' already exists in %s
 %s: user '%s' does not exist
 %s: user '%s' does not exist in %s
 %s: user '%s' is already a member of '%s'
 %s: user '%s' is not a member of '%s'
 %s: warning: %s not owned by %s
 %s: warning: can't remove %s: %s
 %s: warning: failed to completely remove old home directory %s %s: warning: the home directory already exists.
Not copying any file from skel directory into it.
 %s: warning: the user name %s to %s SELinux user mapping failed.
 %s: your groupname does not match your username
 (Enter your own password) **Never logged in** :  Access to su to that account DENIED.
 Account Expiration Date (YYYY-MM-DD) Account expires						:  Actions:
 Adding user %s to group %s
 Bad password: %s.   Can't change root directory to '%s'
 Cannot add SELinux user mapping
 Cannot begin SELinux transaction
 Cannot change ID to root.
 Cannot commit SELinux transaction
 Cannot create SELinux login mapping for %s
 Cannot create SELinux management handle
 Cannot create SELinux user key
 Cannot establish SELinux management connection
 Cannot execute %s Cannot execute %s
 Cannot find user (%s)
 Cannot init SELinux management
 Cannot modify SELinux user mapping
 Cannot open audit interface - aborting.
 Cannot read SELinux policy store
 Cannot verify the SELinux user
 Changing password for %s
 Changing the aging information for %s
 Changing the login shell for %s
 Changing the password for group %s
 Changing the user information for %s
 Could not add login mapping for %s
 Could not allocate space for config info.
 Could not delete login mapping for %s Could not modify login mapping for %s
 Could not query seuser for %s
 Could not set SELinux user for %s
 Could not set name for %s
 Could not set sename for %s
 Could not set serange for %s
 Couldn't get file context Couldn't lock file Couldn't make backup Creating mailbox file Enter the new password (minimum of %d characters)
Please use a combination of upper and lower case letters and numbers.
 Enter the new password (minimum of %d, maximum of %d characters)
Please use a combination of upper and lower case letters and numbers.
 Enter the new value, or press ENTER for the default Entering System Maintenance Mode Environment overflow
 Except for the -A and -M options, the options cannot be combined.
 Full Name Group 'mail' not found. Creating the user mailbox file with 0600 mode.
 Home Phone Incorrect password for %s.
 Invalid ENCRYPT_METHOD value: '%s'.
Defaulting to DES.
 Invalid login time Invalid password.
 Invalid root directory '%s'
 Last Password Change (YYYY-MM-DD) Last login: %.19s on %s Last login: %s on %s Last password change					:  Login       Failures Maximum Latest                   On
 Login Shell Login incorrect Login mapping for %s is not defined, OK if default mapping was used
 Maximum Password Age Maximum number of days between password change		: %ld
 Maximum number of tries exceeded (%u)
 Minimum Password Age Minimum number of days between password change		: %ld
 Multiple entries named '%s' in %s. Please fix this with pwck or grpck.
 New Password:  New password:  No No directory, logging in with HOME=/ No mail. No passwd entry for user '%s'
 No password entry for 'root' No password file No utmp entry.  You must exec "login" from the lowest level "sh" Number of days of warning before password expires	: %ld
 Old password:  Other Password Expiration Warning Password Inactive Password authentication bypassed.
 Password expires					:  Password inactive					:  Password:  Please enter your OWN password as authentication.
 Re-enter new password:  Removing user %s from group %s
 Room Number SELinux policy not managed
 Session terminated, terminating shell... Setting mailbox file permissions TIOCSCTTY failed TIOCSCTTY failed on %s The options cannot be combined.
 The password for %s cannot be changed yet.
 The password for %s cannot be changed.
 The password for %s is unchanged.
 They don't match; try again They don't match; try again.
 Too many logins.
 Try again. Unable to cd to '%s'
 Unable to change owner or mode of tty stdin: %s Unable to determine your tty name. Usage: %s [-p] [name]
 Usage: %s [option] GROUP

Options:
 Usage: %s [options]

Options:
 Usage: %s [options] GROUP

Options:
 Usage: %s [options] LOGIN

Options:
 Usage: %s [options] LOGIN
       %s -D
       %s -D [options]

Options:
 Usage: %s [options] [LOGIN]

Options:
 Usage: %s [options] [action]

Options:
 Usage: %s [options] [group [gshadow]]

Options:
 Usage: %s [options] [group]

Options:
 Usage: %s [options] [passwd [shadow]]

Options:
 Usage: %s [options] [passwd]

Options:
 Usage: id
 Usage: id [-a]
 Usage: logoutd
 Usage: newgrp [-] [group]
 Usage: sg group [[-c] command]
 Usage: su [options] [LOGIN]

Options:
  -c, --command COMMAND         pass COMMAND to the invoked shell
  -h, --help                    display this help message and exit
  -, -l, --login                make the shell a login shell
  -m, -p,
  --preserve-environment        do not reset environment variables, and
                                keep the same shell
  -s, --shell SHELL             use SHELL instead of the default in passwd

 Username                Port     Latest Username         Port     From             Latest Warning: login re-enabled after temporary lockout. Warning: too many groups
 Warning: unknown group %s
 Work Phone You are not authorized to su %s
 You have mail. You have modified %s.
You may need to modify %s for consistency.
Please use the command '%s' to do so.
 You have new mail. You may not change $%s
 You may not change the shell for '%s'.
 You must change your password. Your login has expired. Your password has expired. Your password is inactive. Your password will expire in %ld days.
 Your password will expire today. Your password will expire tomorrow. [libsemanage]: %s
 a palindrome add group '%s' in %s?  add user '%s' in %s?  case changes only configuration error - cannot parse %s value: '%d' configuration error - cannot parse %s value: '%s' configuration error - unknown item '%s' (notify administrator)
 create tcb directory for %s? crypt method not supported by libcrypt? (%s)
 delete administrative member '%s'?  delete line '%s'?  delete member '%s'?  duplicate group entry duplicate password entry duplicate shadow group entry duplicate shadow password entry failed to allocate memory failed to change mailbox owner failed to create backup file failed to create scratch directory failed to create tcb directory for %s
 failed to drop privileges failed to gain privileges failed to open scratch file failed to rename mailbox failed to stat edited file failed to unlink scratch file group %s has an entry in %s, but its password field in %s is not set to 'x'
 group %s: no user %s
 invalid group ID '%lu'
 invalid group file entry invalid group name '%s'
 invalid password file entry invalid shadow group file entry invalid shadow password file entry invalid user ID '%lu'
 invalid user name '%s'
 login time exceeded

 login:  login: PAM Failure, aborting: %s
 login: abort requested by PAM
 never no change no matching group file entry in %s
 no matching password file entry in %s
 no tcb directory for %s
 passwd: %s
 passwd: pam_start() failed, error %d
 passwd: password unchanged
 passwd: password updated successfully
 password must be changed rotated setfscreatecon () failed shadow group %s: no administrative user %s
 shadow group %s: no user %s
 too many groups
 too short too similar too simple user %s has an entry in %s, but its password field in %s is not set to 'x'
 user %s: last password change in the future
 user '%s': directory '%s' does not exist
 user '%s': no group %lu
 user '%s': program '%s' does not exist
 Project-Id-Version: shadow 4.1.4.2
Report-Msgid-Bugs-To: pkg-shadow-devel@lists.alioth.debian.org
POT-Creation-Date: 2012-05-20 19:52+0200
PO-Revision-Date: 2016-03-29 23:14+0000
Last-Translator: Francisco Javier Cuadrado <Unknown>
Language-Team: Debian l10n Spanish <debian-l10n-spanish@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:08+0000
X-Generator: Launchpad (build 18115)
Language: es
 	%s: %s
 
 
%s nombre:  
El acceso caducó después de %u segundos.
 
Sistema cerrado por mantenimiento rutinario 
Pulse control-d para continuar con un arranque normal del sistema,
(o introduzca la contraseña del administrador («root») para entrar en el modo de mantenimiento del sistema): 
Aviso: contraseña débil (introdúzcala de nuevo para usarla de todos modos). 
[Desconexión evitada -- acceso del administrador («root») permitido.]        %s [-p] -r máquina
        %s [-p] [-h máquina] [-f nombre]
   -A, --administrators ADMIN,...
                                establece la lista de administradores de GRUPO
   -D, --defaults                imprime o cambia la configuración
                                predeterminada de useradd
   -E, --expiredate FECHA_CAD    establece la fecha de caducidad a FECHA_CAD
   -G, --groups GRUPOS           lista de grupos suplementarios de la nueva
                                cuenta
   -G, --groups GRUPOS           lista de grupos suplementarios
   -I, --inactive INACTIVA       deshabilita la cuenta después de INACTIVA
                                días de la fecha de caducidad
   -K, --key CLAVE=VALOR         sobrescribe los valores predeterminados de
                                «/etc/login.defs»
   -L, --lock                    bloquea la cuenta de usuario
   -M, --maxdays DÍAS_MAX        establece el número máximo de días antes de
                                cambiar la contraseña a DÍAS_MAX
   -M, --members USUARIO,...     establece la lista de miembros de GRUPO
   -M, --no-create-home          no crea el directorio personal del usuario
   -N, --no-user-group           no crea un grupo con el mismo nombre que el
                                usuario
   -R, --restrict                restringe el acceso a GRUPO a sus miembros
   -R, --root CHROOT_DIR         directorio en el que hacer chroot
   -S, --status                  informa del estado de la contraseña la cuenta
                                indicada
   -U, --unlock                  desbloquea la cuenta de usuario
   -U, --user-group              crea un grupo con el mismo nombre que el
                                usuario
   -W, --warndays DÍAS_AVISO     establece los días de aviso de expiración a
                                DÍAS_AVISO
   -Z, --selinux-user USUARIO_SE  utiliza el usuario indicado para el usuario
                                 de SELinux
   -a, --add USUARIO             añade USUARIO al GRUPO
   -a, --add USUARIO             añade USUARIO a los miembros del grupo
   -a, --all                     muestra los registros de faillog para
                                todos los usuarios
   -a, --all                     informa del estado de las contraseñas de
                                todas las cuentas
   -b, --base-dir DIR_BASE       directorio base para el directorio personal
                                de la nueva cuenta
   -b, --before DÍAS             muestra los registros de «lastlog» anteriores
                                a DÍAS
   -c, --check                   comprueba la terminación de la contraseña del usuarioo
   -c, --comment COMENTARIO      campo GECOS de la nueva cuenta
   -c, --comment COMENTARIO      nuevo valor del campo GECOS
   -d, --delete                  borra la contraseña para la cuenta indicada
   -d, --delete USUARIO          elimina USUARIO del GRUPO
   -d, --delete USUARIO          elimina USUARIO de la lista de miembros del
                                grupo
   -d, --home DIR_PERSONAL       nuevo directorio personal del nuevo usuario
   -d, --home-dir DIR_PERSONAL   directorio personal de la nueva cuenta
   -d, --lastday ÚLTIMO_DÍA      establece el día del último cambio de la
                                contraseña a ÚLTIMO_DÍA
   -e, --encrypted               se cifran las contraseñas proporcionadas
   -e, --expire                  fuerza a que la contraseña de la cuenta
                                caduque
   -e, --expiredate FECHA_CADUCIDAD  fecha de caducidad de la nueva cuenta
   -e, --expiredate FECHA_EXPIR  establece la fecha de caducidad de la
                                cuenta a FECHA_EXPIR
   -f, --force                   termina si el grupo ya existe, y cancela -g
                                si el GID ya se está en uso
   -f, -force forzar a cambiar la clave de acceso del usuario si
                                ha expirado
   -f, --force                   forzar la eliminación de los ficheros,
                                incluso si no pertenecen al usuario
   -f, --full-name NOMBRE_COMPLETO     cambia el nombre completo del usuario
   -f, --inactive INACTIVO       periodo de inactividad de la contraseña
                                de la nueva cuenta
   -f, --inactive INACTIVO       establece el tiempo de inactividad después
                                de que caduque la cuenta a INACTIVO
   -g, --gid GID                 cambia el identificador del grupo a GID
   -g, --gid GID                 utiliza GID para el nuevo grupo
   -g, --gid GRUPO               fuerza el uso de GRUPO para la nueva cuenta
                                de usuario
   -g, --gid GRUPO               nombre o identificador del grupo primario de
                                la nueva cuenta
   -g, --group                   edita la base de datos del grupo
   -g, --group GRUPO             cambia el nombre del grupo en lugar del grupo
                                del usuario (sólo lo puede hacer el
                                administrador)
   -h, --help                    muestra este mensaje de ayuda y termina
   -h, --home-phone TELÉFONO_DE_CASA   cambia el teléfono de casa del usuario
   -i, --inactive INACTIVO       establece la contraseña inactiva después de
                                caducar a INACTIVO
   -k, --keep-tokens             cambia la contraseña sólo si ha caducado
   -k, --skel DIR_SKEL           utiliza este directorio «skeleton» alternativo
   -l, --list                    lista los miembros del grupo
   -l, --list                    muestra la información de la edad de la cuenta
   -l, --lock                    bloquea la contraseña de la cuenta indicada
   -l, --lock-secs SEG           después de fallar al acceder la cuenta se
                                bloqueará después de SEG segundos
   -l, --login NOMBRE            nuevo nombre para el usuario
   -l, --no-log-init             no añade el usuario a las bases de datos de
                                lastlog y faillog
   -m, --create-home             crea el directorio personal del usuario
   -m, --maximum MAX             establece el máximo número de accesos fallidos
                                a MAX
   -m, --md5                     cifra la contraseña en claro utilizando
                                el algoritmo MD5
   -m, --mindays DÍAS_MIN        establece el número mínimo de días antes de
                                cambiar la contraseña a DÍAS_MIN
   -m, --move-home               mueve los contenidos del directorio
                                personal al directorio nuevo (usar sólo
                                junto con -d)
   -n, --mindays DÍAS_MIN        establece el número mínimo de días antes
                                de que se cambie la contraseña a DÍAS_MIN
   -n, --new-name GRUPO_NUEVO    cambia el nombre a GRUPO_NUEVO
   -o, --non-unique              permite crear grupos con GID (no únicos)
                                duplicados
   -o, --non-unique              permite crear usuarios con identificadores
                                (UID) duplicados (no únicos)
   -o, --non-unique              permite utilizar un GID duplicado (no único)
   -o, --non-unique              permite usar UID duplicados (no únicos)
   -o, --other OTRA_INFO        cambia otra información GECOS del usuario
   -p, --passwd                  edita la base de datos de «passwd»
   -p, --password CONTRASEÑA     cambia la contraseña a CONTRASEÑA (cifrada)
   -p, --password CONTRASEÑA     contraseña cifrada de la nueva cuenta
   -p, --password CONTRASEÑA     usar la contraseña cifrada para la nueva cuenta
   -p, --password CONTRASEÑA     utiliza esta contraseña cifrada para el nuevo
                                grupo
   -p, --purge                   purga todos los miembros del grupo
   -q, --quiet                   modo silencioso
   -q, --quiet informa de errores solamente
   -r, --read-only               muestra errores y avisos
                                pero no cambia los archivos
   -r, --remove                  elimina el directorio personal y el buzón de
                                correo
   -r, --remove-password         elimina la contraseña de GRUPO
   -r, --repository REP          cambia la contraseña en el repositorio REP
   -r, --reset                   reinicia el contador de accesos fallidos
   -r, --system                  crea una cuenta del sistema
   -r, --system                  crea cuentas del sistema
   -s, --sha-rounds              número de rondas SHA para los algoritmos
                                de cifrado SHA*
   -s, --shadow                  edita la base de datos shadow o gshadow
   -s, --shell CONSOLA           consola de acceso de la nueva cuenta
   -s, --shell CONSOLA           nueva consola de acceso para la cuenta del
                                usuario
   -s, -- sort ordena las entradas por UID
   -t, --time DÍAS               muestra los registros de faillog más recientes
                                que DÍAS
   -t, --time DÍAS               muestra sólo los registros de «lastlog» más
                                recientes que DÍAS
   -u, --help                    muestra este mensaje de ayuda y termina
   -u, --uid UID                 fuerza el uso del UID para la nueva cuenta
                                de usuario
   -u, --uid UID                 identificador del usuario de la nueva cuenta
   -u, --unlock                  desbloquea la contraseña de la cuenta indicada
   -u, --user                    indica que fichero tcb de shadow del usuario
                                editar
   -u, --user USUARIO            muestra el registro de «lastlog» del USUARIO
   -u, --user USUARIO/RANGO      muestra los registros de faillog o mantiene
                                los contadores y límites de fallos (si se
                                utiliza con -r, -m o -l) sólo para el USUARIO
                                indicado
   -w, --warndays DÍAS_AVISO     establece el aviso de caducidad a DÍAS_AVISO
   -x, --maxdays DÍAS_MAX        establece el número máximo de días antes de
                                cambiar la contraseña a DÍAS_MAX
   Elija una contraseña nueva.   Contacte con el administrador.  ... finalizado.
  ... parada.
  ... esperando a que el hijo finalice.
  [bloqueados %lds]  [faltan %lus]  desde %.*s  grupos= %d fallo desde el último acceso.
El último fue %s en %s.
 %d fallos desde el último acceso.
El último fue %s en %s.
 %s nombre:  Contraseña de %s:  %s:  %s: %s
 %s: %s
(Ignorado)
 %s: la opción %s sólo está permitida junto a la opción %s
 %s: %s directorio personal (%s) no encontrado
 %s: %s es una consola incorrecta.
 %s: %s no es ni un directorio ni un enlace simbólico.
 %s: %s no está autorizado a cambiar la contraseña de %s
 %s: %s es el NIS maestro
 %s: no se ha cambiado %s
 %s: %s no pertenece a %s, no se eliminará
 %s: se creó %s, pero no se pudo eliminar
 %s: «%s» contiene caracteres ilegales
 %s: «%s» contiene caracteres ilegales (que no son ASCII)
 %s: «%s» no es un nombre de grupo válido
 %s: «%s» es el NIS maestro para este cliente.
 %s: (línea %d, usuario %s) la contraseña no ha cambiado
 %s: -K requiere CLAVE=VALOR
 %s: -Z necesita que el núcleo tenga activado SELinux
 %s: -s y -r son incompatibles
 %s: se produjo un fallo en la autenticación
 %s: no se pudo reservar memoria, la entrada tcb %s no se eliminó.
 %s: no se pudo obtener un GID único (no hay más GID disponibles)
 %s: no se pudo obtener un UID único (no hay más UID disponibles)
 %s: no se pudo obtener un GID único del sistema (no hay más GID disponibles)
 %s: no se pudo obtener un UID único del sistema (no hay más UID disponibles)
 %s: no se pudo cambiar el modo de %s: %s
 %s: no se pudo cambiar el dueño de %s: %s
 %s: no se pudieron cambiar los dueños de %s: %s
 %s: no se puede crear el archivo de respaldo (%s): %s
 %s: no se pudo crear el directorio %s: %s
 %s no se pudo crear el enlace simbólico %s: %s
 %s: no se pudo determinar su nombre de usuario.
 %s: no se pudo desprender los privilegios: %s
 %s: no se puede eliminar el control del terminal
 %s: no se pudo realizar el «fork» de la consola del usuario
 %s: no se pudo conseguir el tamaño de %s: %s
 %s: no se pudo realizar «lstat» a %s: %s
 %s: no se pudo abrir %s: %s
 %s: posiblemente no se puede trabajar sin el administrador
 %s: no se pudo leer el enlace simbólico %s: %s
 %s: no se pudo eliminar %s: %s
 %s: no se pudo eliminar el directorio %s: %s
 %s: no se pudieron eliminar los ficheros tcb para %s: %s
 %s: no se pudo eliminar el contenido de %s: %s
 %s: no se pudo renombrar %s a %s: %s
 %s: no se puece configurar el servicio de limpieza.
 %s: no se pudo realizar «stat» a %s: %s
 %s: emergencia: el tcb oculto de %s no es un fichero normal con st_nlink=1.
La cuenta se queda bloqueada.
 %s: se produjo un fallo al cambiar el dueño del directorio personal %s: se produjo un fallo al crear el directorio tcb para %s
 %s: Se produjo un fallo al conseguir la entrada para el UID %lu
 %s: se produjo un fallo al reiniciar el contador de fallos para el UID %lu
 %s: se produjo un fallo al establecer el tiempo de bloqueo para el UID %lu
 %s: se produjo un fallo al establecer max para el UID %lu
 %s: Fallo al escribir %s: %s
 %s: el GID «%lu» ya existe
 %s: el GID «%lu» no existe
 %s: configuración incorrecta: GID_MIN (%lu), GID_MAX (%lu)
 %s: configuración incorrecta: SYS_GID_MIN (%lu), GID_MIN (%lu), SYS_GID_MAX (%lu)
 %s: configuración incorrecta: SYS_UID_MIN (%lu), UID_MIN (%lu), SYS_UID_MAX (%lu)
 %s: configuración incorrecta: UID_MIN (%lu), UID_MAX (%lu)
 %s: entrada incorrecta: %s
 %s: No es una tty
 %s: sin memoria. No se pudo actualizar %s.
 %s: PAM: %s
 %s: permiso denegado.
 %s: enlace simbólico sospechosamente largo: %s
 %s: el directorio personal anterior (%s) no era un directorio. No se eliminará y no se crearán directorios personales.
 %s: inténtelo más tarde
 %s: el UID %lu no es único
 %s: el UID «%lu» ya existe
 %s: usuario o rango desconocido: %s
 %s: aviso, el usuario %s no tiene un fichero «tcb shadow».
 %s: aviso: %s no existe
 %s: aviso: %s no es un ejecutable
 %s: no está autorizado a usar su en este momento
 %s: no debe ver o cambiar la información de la contraseña para %s.
 %s: no se puede crear el grupo
 %s: no se pudo crear el usuario
 %s: no se puede restaurar %s: %s (sus cambios están en %s)
 %s: no puede funcionar con tcb activado
 %s: no se puede acceder al directorio chroot %s: %s
 %s: no se pudo cambiar el usuario «%s» en el cliente NIS.
 %s: no se puede crear el directorio %s
 %s: no se puede crear un nuevo archivo de preferencias predeterminadas
 %s: no se pudo borrar %s
 %s: no se pudo bloquear %s.
 %s: no se pudo bloquear %s, inténtelo de nuevo.
 %s: no se pudo abrir %s
 %s: no se puede abrir un nuevo archivo de preferencias predeterminadas
 %s: no se pudo eliminar la entrada «%s» de %s
 %s: no se pudo eliminar el grupo primario del usuario «%s»
 %s: no se puede renombrar el directorio %s a %s
 %s: no se pudieron ordenar las entradas en %s
 %s: no se pudo actualizar la entrada del usuario %s (no está en la base de datos de passwd)
 %s: el directorio %s existe
 %s: no incluya «l» junto a otras opciones
 %s: error cambiando los campos
 %s: error detectado, cambios ignorados
 %s: error eliminando el directorio %s
 %s: se produjo un fallo al reservar memoria: %s
 %s: se produjo un fallo al cambiar el modo de %s a 0600
 %s: se produjo un fallo al copiar la entrada de faillog del usuario %lu al usuario %lu: %s
 %s: se produjo un fallo al copiar la entrada de lastlog del usuario %lu al usuario %lu: %s
 %s: se produjo un fallo al desprenderse de los privilegios (%s)
 %s: se produjo un fallo al buscar el directorio tcb de %s
 %s: se produjo un fallo al preparar la nueva %s entrada «%s»
 %s: se produjo un fallo al eliminar %s
 %s: se produjo un fallo al reiniciar la entrada de faillog del UID %lu: %s
 %s: se produjo un fallo al reiniciar la entrada de lastlog del UID %lu: %s
 %s: se produjo un fallo al desbloquear %s
 %s: se produjo un fallo en la llamada a fork: %s %s: se produjo un fallo al realizar el «fork»: %s
 %s: se produjo un fallo al cerrar el %s de sólo lectura
 %s: se produjo un fallo al escribir los cambios a %s
 %s: campos demasiado largos
 %s: el grupo %s existe - si quiere añadir este usuario a ese grupo, use -g.
 %s: el grupo %s es un grupo NIS
 %s: grupo %s es el grupo primario de otro usuario y no se eliminará.
 %s: grupo %s no eliminado porque tiene otros miembros.
 %s: el grupo «%s» ya existe
 %s: el grupo «%s» no existe
 %s: el grupo «%s» no existe en %s
 %s: el grupo «%s» es un grupo NIS
 %s: el grupo «%s» es un grupo NIS.
 %s: el grupo «%s» es un grupo oculto, pero no existe en «/etc/group»
 %s: directorio base «%s» incorrecto
 %s: ruta no válida chroot «%s»
 %s: comentario «%s» incorrecto
 %s: fecha «%s» incorrecta
 %s: campo «%s» incorrecto
 %s: identificador de grupo «%s» incorrecto
 %s: nombre del grupo «%s» incorrecto
 %s: directorio personal («home») «%s» incorrecto
 %s: teléfono de casa incorrecto: «%s»
 %s: nombre incorrecto: «%s»
 %s: argumento numérico incorrecto «%s»
 %s: nombre de habitación incorrecto: «%s»
 %s: consola «%s» incorrecta
 %s: identificador de usuario «%s» incorrecto
 %s: nombre de usuario «%s» incorrecto
 %s: teléfono del trabajo incorrecto: «%s»
 %s: línea %d: no se pudo crear el grupo
 %s: línea %d: no se pudo crear el usuario
 %s: línea %d: no se puede actualizar la entrada
 %s: línea %d: no se puede actualizar la contraseña
 %s: línea %d: chown %s falló: %s
 %s: línea %d: se produjo un fallo al preparar la nueva %s entrada «%s»
 %s: línea %d: el grupo «%s» no existe
 %s: línea %d: línea incorrecta
 %s: línea %d: línea demasiado larga
 %s: línea %d: falta la nueva contraseña
 %s: línea %d: mkdir %s falló: %s
 %s: línea %d: el usuario «%s» no existe
 %s: línea %d: el usuario «%s» no existe en %s
 %s: línea demasiado larga en %s: %s... %s: mkdir: %s: %s
 %s: múltiples opciones --root
 %s: debe ejecutarse desde un terminal
 %s: el nombre contiene caracteres ilegales (no ASCII): «%s»
 %s: no se permite un fichero oculto alternativo cuando USE_TCB está habilitado.
 %s: sin cambios
 %s: no hay opciones
 %s: no se elimina el directorio %s (eliminaría el directorio personal del usuario %s)
 %s: nscd no terminó normalmente (señal %d)
 %s: nscd salió con el estado %d
 %s: sólo el administrador puede utilizar la opción -g/--group
 %s: la opción «%s» precisa un argumento
 %s: las opciones %s y %s están en conflicto
 %s: sin memoria
 %s: pam_start: error %d
 %s: contraseña cambiada.
 %s: información de caducidad de la contraseña cambiada.
 %s: renombrar: %s: %s
 %s: repositorio %s no soportado
 %s: la habitación contiene caracteres ilegales (no ASCII): «%s»
 %s: se requieren contraseñas de grupo ocultas para -A
 %s: se requieren contraseñas de shadow para -e
 %s: se necesitan contraseñas de shadow para -e y -f
 %s: se requieren contraseñas de shadow para -f
 %s: funcionamiento incorrecto de la señal
 %s: funcionamiento incorrecto del enmascaramiento de la señal
 %s: se ignorará la configuración %s en %s
 %s: las opciones -L, -p y -U son exclusivas
 %s: las opciones -c, -e y -m son exclusivas
 %s: los archivos se han actualizado
 %s: el archivo de contraseñas ocultas no está presente
 %s: demasiados grupos especificados (el máximo es %d).
 %s: no se puede hacer chroot al directorio %s:%s
 %s: argumento inesperado: %s
 %s: usuario desconocido %s
 %s: unlink: %s: %s
 %s: desbloquear la contraseña dejaría una cuenta sin contraseña.
Debería establecer una contraseña mediante «usermod -p» para desbloquear la contraseña de esta cuenta.
 %s: desbloquear la contraseña dejaría una cuenta sin contraseña.
Debería establecer una contraseña mediante «usermod -p» para desbloquear la contraseña de esta cuenta.
 %s: método de cifrado no compatible: %s
 %s: el usuario %s es un usuario NIS
 %s: el usuario «%s» ya existe
 %s: el usuario «%s» ya existe en %s
 %s: el usuario «%s» no existe
 %s: el usuario «%s» no existe en %s
 %s: el usuario «%s» ya es un miembro de «%s»
 %s: el usuario «%s» no es miembro de «%s»
 %s: aviso: %s no pertenece a %s
 %s: aviso: no se puede eliminar %s: %s
 %s: aviso: no se pudo eliminar completamente el directorio personal previo %s %s: aviso: el directorio personal ya existe.
No se copia ningún archivo del directorio skel en él.
 %s: aviso: no se pudo relacionar el nombre del usuario %s con el usuario %s de SELinux.
 %s: su nombre de grupo no coincide con su nombre de usuario
 (Introduzca su propia contraseña) **Nunca ha accedido** :  El acceso a «su» en esa cuenta se ha DENEGADO.
 Fecha de caducidad de la cuenta (AAAA-MM-DD) La cuenta caduca						:  Acciones:
 Añadiendo al usuario %s al grupo %s
 Contraseña incorrecta: %s.   No se pudo cambiar el directorio raíz a «%s»
 No se puede añadir la asignación de usuarios SELinux
 Imposible iniciar las transacciones SELinux
 No se puede cambiar el identificador al usuario del administrador («root»).
 No se puede confirmar la transacción SELinux
 No se pueden crear mapas de inicio de sesión de SELinux para %s
 No se puede crear el manejador de gestión de SELinux
 No es posible crear la clave de usuario SELinux
 No se puede establecer la conexión del gestor de SELinux
 No se puede ejecutar %s No se puede ejecutar %s
 No se puede encontrar al usuario (%s)
 No se puede iniciar la gestión de SELinux
 Es imposible la modificación de la asignación de usuarios SELinux
 No se pudo abrir la interfaz de auditoría, abortando.
 Imposible leer el almacén de directivas  de SELinux
 No se puede verificar quién es el usuario SELinux
 Cambiando la contraseña para %s
 Cambiando la información de la edad para %s
 Cambiando la consola de acceso para %s
 Cambiando la contraseña para el grupo %s
 Cambiando la información de usuario para %s
 No se pudo añadir la asignación de inicio de sesión de %s
 No se pudo reservar espacio para la información de configuración.
 No se pudo eliminar la asignación de inicio de sesión para %s No se pudo modificar asignación de inicio de sesión para %s
 No se pudo consultar seuser para %s
 No se puede configurar el usuario SELinux para %s
 Imposible establecer el nombre de %s
 No se pudo establecer sename para %s
 No se pudo establecer serange para %s
 No se pudo conseguir el contexto del fichero No se puede bloquear el archivo No se puede realizar una copia de seguridad Creando el fichero del buzón de correo Introduzca la nueva contraseña (con un mínimo de %d caracteres)
Por favor, use una combinación de letras mayúsculas, minúsculas y números.
 Introduzca la nueva contraseña (con un mínimo de %d caracteres y un máximo de %d)
Por favor, use una combinación de letras mayúsculas, minúsculas y números.
 Introduzca el nuevo valor, o presione INTRO para el predeterminado Entrando en el modo de mantenimiento del sistema Desbordamiento de entorno
 Excepto las opciones -A y -M, las opciones no se pueden combinar.
 Nombre completo No se encontró el grupo «mail». Se creará el fichero del buzón de correo del usuario con el modo 0600.
 Teléfono de casa Contraseña incorrecta para %s.
 El valor de ENCRYPT_METHOD no es válido: «%s».
Se usará DES, el valor predeterminado.
 Tiempo de acceso incorrecto Contraseña incorrecta.
 Directorio raíz «%s» incorrecto
 Último cambio de contraseña (AAAA-MM-DD) Último acceso: %.19s en %s Último acceso: %s en %s Último cambio de contraseña					:  Usuario     Fallos Máximo Último                   Activo
 Consola de acceso Identificación incorrecta No está definida la asignación para %s, pero se usó la asignación predeterminada
 Duración máxima de la contraseña Número de días máximo entre cambio de contraseña		: %ld
 Número de intentos máximo excedido (%u)
 Duración mínima de la contraseña Número de días mínimo entre cambio de contraseña		: %ld
 Hay varias entradas con el nombre «%s» en %s. Por favor, corríjalo utilizando pwck o grpck.
 Nueva contraseña:  Nueva contraseña:  No Sin directorio, accediendo con HOME=/ Sin correo. No existe clave de entrada para el usuario «%s»
 No hay entrada de contraseña para «root» No hay archivo de contraseñas Sin entrada utmp. Debe ejecutar «login» desde el nivel «sh» más bajo Número de días de aviso antes de que caduque la contraseña	: %ld
 Contraseña antigua:  Otro Aviso de caducidad de la contraseña Contraseña inactiva Autenticación de contraseña evitada.
 La contraseña caduca					:  Contraseña inactiva					:  Contraseña:  Por favor, introduzca su PROPIA contraseña como autenticación.
 Vuelva a introducir la nueva contraseña:  Eliminando al usuario %s del grupo %s
 Número de habitación Política SELinux no gestionada
 Sesión finalizada, parando la consola ... Estableciendo los permisos del fichero del buzón de correo TIOCSCTTY falló TIOCSTTY falló en %s Las opciones no se pueden combinar.
 Todavía no se puede cambiar la contraseña de %s.
 No se puede cambiar la contraseña para %s.
 La contraseña para %s no se ha modificado.
 No concuerdan, inténtelo de nuevo No concuerdan, inténtelo de nuevo.
 Demasiados accesos.
 Inténtelo de nuevo. Incapaz de cambiar el directorio a «%s»
 No se pudo cambiar el dueño o el modo de la tty de la entrada estándar (stdin): %s No se pudo determinar el nombre de su tty. Uso: %s [-p] [nombre]
 Modo de uso: %s [opciones] GRUPO

Opciones:
 Modo de uso: %s [opciones]

Opciones:
 Modo de uso: %s [opciones] GRUPO

Opciones:
 Modo de uso: %s [opciones] USUARIO

Opciones:
 Modo de uso: %s [opciones] USUARIO
             %s -D
             %s -D [opciones]

Opciones:
 Modo de uso: %s [opciones] [USUARIO]

Opciones:
 Modo de uso: %s [opciones] [acción]

Opciones:
 Modo de uso: %s [opciones] [grupo [gshadow]]

Opciones:
 Modo de uso: %s [opciones] [group]

Opciones:
 Modo de uso: %s [opciones] [passwd [shadow]]

Opciones:
 Uso: %s [opciones] [contraseña]

Opciones:
 Uso: id
 Uso: id [-a]
 Modo de uso: logout
 Uso: newgrp [-] [grupo]
 Uso: sg grupo [[-c] orden]
 Modo de uso: su [opciones] [USUARIO]

Opciones:
  -c, --command ORDEN           pasa la ORDEN a la consola invocada
  -h, --help                    muestra este mensaje de ayuda y termina
  -, -l, --login                hace que la consola sea una de acceso
  -m, -p,
  --preserve-environment        no reinicia las variables de entorno, y
                                mantiene la misma consola
  -s, --shell CONSOLA           usa CONSOLA en lugar de la predeterminada en
                                passwd

 Nombre                  Puerto   Último Nombre           Puerto   De               Último Aviso: acceso restablecido después de un cierre temporal. Aviso: demasiados grupos
 Aviso: grupo %s desconocido
 Teléfono del trabajo No está autorizado a usar su %s
 Tiene correo. Ha modificado %s.
Puede que necesite modificar %s por consistencia.
Por favor, utilice la orden «%s» para hacerlo.
 Tiene correo nuevo. No debe cambiar $%s
 No debería cambiar la consola para «%s».
 Debe cambiar su contraseña. Su acceso ha caducado. Su contraseña ha caducado. Su contraseña está inactiva. Su contraseña caducará en %ld días.
 Su contraseña caducará hoy. Su contraseña caducará mañana. [libsemanage]: %s
 un palíndromo ¿desea añadir el grupo «%s» en %s?  ¿desea añadir el usuario «%s» a «%s»?  solo cambios mayúsculas/minúsculas error de configuración, no se pudo procesar el valor %s: «%d» error de configuración, no se pudo procesar el valor %s: «%s» error de configuración, elemento «%s» desconocido (informe al administrador)
 ¿desea crear el directorio tcb para %s? ¿libcrypt no permite utilizar el método de cifrado? (%s)
 ¿desea eliminar el miembro administrativo «%s»?  ¿desea eliminar la línea «%s»?  ¿desea eliminar el miembro «%s»?  entrada de grupo duplicada entrada de contraseña duplicada entrada del archivo de grupos oculto (shadow) duplicada entrada del fichero shadow de contraseñas duplicada se produjo un fallo al reservar memoria se produjo un fallo al cambiar el propietario del buzón de correo se produjo un fallo al crear la copia de seguridad del fichero se produjo un fallo al crear el directorio temporal se produjo un fallo al crear el directorio tcb para %s
 se produjo un fallo al desprenderse de los privilegios se produjo un fallo al obtener privilegios se produjo un fallo al abrir el fichero temporal se produjo un fallo al cambiar el nombre del buzón de correo se produjo un fallo al realizar el «stat» del fichero editado se produjo un fallo al borrar el fichero temporal el grupo %s tiene una entrada en %s, pero su campo de contraseña en %s no está establecido a «x»
 grupo %s: no existe el usuario %s
 identificador de grupo «%lu» incorrecto
 entrada del archivo de grupos inválida nombre de grupo «%s» incorrecto
 entrada del archivo de contraseñas inválida entrada del archivo de grupos oculto (shadow) inválida entrada del fichero shadow de contraseñas incorrecta identificador «%lu» de usuario incorrecto
 nombre de usuario «%s» incorrecto
 tiempo de acceso excedido

 nombre:  login: fallo de PAM, abortando: %s
 login: petición abortada por PAM
 nunca sin cambios no hay entradas coincidentes en el archivo de grupos en «%s»
 ninguna entrada del archivo de contraseñas concuerda con %s
 no existe el directorio tcb para %s
 passwd: %s
 passwd: pam_start() falló, error %d
 passwd: no se ha cambiado la contraseña
 passwd: contraseña actualizada correctamente
 se debe cambiar la contraseña rotada setfscreatecon () falló grupo de shadow %s: no existe el usuario administrativo %s
 grupo de shadow %s: no existe el usuario %s
 demasiados grupos
 demasiado corta demasiado similar demasiado simple el usuario %s tiene una entrada en %s, pero su campo de la contraseña en %s no está establecido a «x»
 usuario %s: el último cambio de la contraseña se produjo en el futuro
 usuario «%s»: el directorio «%s» no existe
 usuario «%s»: no existe el grupo %lu
 usuario «%s»: el programa «%s» no existe
 