��          4      L       `   �   a   (   �   �    �   �  5   L                    URL of a service that can be contacted and returns an XML file containing location information about the requestor's IP address. URL used to get information about the IP Project-Id-Version: ubuntu-geoip
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-04 02:07+0000
PO-Revision-Date: 2013-12-15 06:59+0000
Last-Translator: Paco Molinero <paco@byasl.com>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:58+0000
X-Generator: Launchpad (build 18115)
 URL del servicio al que se puede contactar y que devuelve un archivo XML que contiene la información de la ubicación de la dirección IP del peticionario. URL usado para conseguir información acerca de la IP 