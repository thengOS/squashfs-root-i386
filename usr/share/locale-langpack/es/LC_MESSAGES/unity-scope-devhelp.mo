��          \      �       �      �      �      �   ;   �      !  �   5       �  ,     �     �     �  H   �     C  �   X  !   0                                       Devhelp Search Devhelp Show Sorry, there are no Devhelp results that match your search. Technical Documents This is an Ubuntu search plugin that enables information from Devhelp to be searched and displayed in the Dash underneath the Code header. If you do not wish to search this content source, you can disable this search plugin. devhelp;dev;help;doc; Project-Id-Version: unity-scope-devhelp
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-28 09:22+0000
PO-Revision-Date: 2014-05-03 07:20+0000
Last-Translator: Paco Molinero <paco@byasl.com>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:23+0000
X-Generator: Launchpad (build 18115)
 Devhelp Buscar con Devhelp Mostrar Lo sentimos, no hay resultados de Devhelp que coincidan con su búsqueda Documentos técnicos Este es un complemento de búsqueda de Ubuntu que permite buscar y mostrar información de Devhelp en el Tablero debajo del encabezado Code. Si no quiere buscar desde esta fuente, puede desactivar este complemento . devhelp;dev;desarrollo;ayuda;doc; 