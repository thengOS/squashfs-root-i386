��          �      \      �  $   �  )   �        6   9     p  /   �  (   �     �  +   �     $     B  $   ^  -   �  +   �  7   �       &   -  /   T     �  �  �  6   y  1   �     �  H         I  7   [  /   �  -   �  N   �      @  :   a  9   �  D   �  -   	  T   I	     �	  4   �	  D   �	     8
                                  	                      
                                           Attempt to set invalid NRC map '%c'. Attempt to set invalid wide NRC map '%c'. Could not open console.
 Could not parse the geometry spec passed to --geometry Duplicate (%s/%s)! Error (%s) converting data for child, dropping. Error compiling regular expression "%s". Error creating signal pipe. Error reading PTY size, using defaults: %s. Error reading from child: %s. Error setting PTY size: %s. Got unexpected (key?) sequence `%s'. No handler for control sequence `%s' defined. Unable to convert characters from %s to %s. Unable to send data to child, invalid charset convertor Unknown pixel mode %d.
 Unrecognized identified coding system. _vte_conv_open() failed setting word characters can not run %s Project-Id-Version: vte.HEAD
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=vte&component=general
POT-Creation-Date: 2007-10-06 10:12+0000
PO-Revision-Date: 2016-02-08 00:36+0000
Last-Translator: Jorge González <jorgegonz@svn.gnome.org>
Language-Team: Español <gnome-es-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:07+0000
X-Generator: Launchpad (build 18115)
 Se ha intentado definir un mapa NRC no válido «%c». Se ha intentado definir un mapa NRC total «%c». No se pudo abrir la consola.
 No se pudo analizar la especificación de geometría pasada a --geometry Duplicado (%s/%s) Error (%s) al convertir datos desde el hijo, omitiendo. Error al compilar la expresión regular «%s». Ha ocurrido un error al crear la señal pipe. Ha ocurrido un error al leer el tamaño PTY, utilizando el predeterminado: %s. Error al leer desde el hijo: %s. Ha ocurrido un error al establecer el tamaño del PTY: %s. Se obtuvo una secuencia inesperada (¿de teclas?) «%s». No existe un manejador para la secuencia de control «%s» definida. No se pueden convertir caracteres de %s a %s. No se pueden enviar los datos al hijo, conversor de códigos de caracteres inválido Modo de píxel %d desconocido.
 Sistema de codificación identificado no reconocido. Error en _vte_conv_open() al establecer los caracteres de la palabra no se puede ejecutar %s 