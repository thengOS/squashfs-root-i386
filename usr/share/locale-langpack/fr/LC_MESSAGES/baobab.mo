��    9      �  O   �      �     �               $     7  '   H  ;   p  �   �     V  !   c     �  +   �     �     �     �     �     �  %        .  4   I     ~     �     �      �     �     �     
          &     2     A  "   J      m     �     �     �     �      �     �     �     	     	  (   	     A	     M	     Z	  	   c	     m	     t	     |	     �	     �	     �	     �	     �	     �	  $  �	  &        )     :     U     e  ;   r  7   �  �   �     �  3   �     �  >        E  
   L     W  .   `      �  9   �  +   �  @        W  #   w     �  4   �     �     
     '     /     H     Z     r  /   {  -   �     �     �            %   %  !   K     m     y     �  )   �     �     �     �     �  
          (        B     H     P     c  !   l  �  �     .                3            ,                              6                7   9          	             -       %             +   !                      $         (       &           #   '          8   /   
   4          "       1      )            2       *   0       5            "%s" is not a valid folder %d day %d days %d item %d items %d month %d months %d year %d years A graphical tool to analyze disk usage. A list of URIs for partitions to be excluded from scanning. A simple application which can scan either specific folders (local or remote) or volumes and give a graphical representation including each directory size or percentage. Active Chart Apparent sizes are shown instead. Baobab Check folder sizes and available disk space Close Computer Contents Could not analyze disk usage. Could not analyze volume. Could not detect occupied disk sizes. Could not scan folder "%s" Could not scan some of the folders contained in "%s" Devices and locations Disk Usage Analyzer Excluded partitions URIs Failed to move file to the trash Failed to open file Failed to show help Folder Go to _parent folder Home folder Mo_ve to Trash Modified Print version information and exit Recursively analyze mount points Rings Chart Scan Folder… Select Folder Size The GdkWindowState of the window The initial size of the window Today Treemap Chart Unknown Which type of chart should be displayed. Window size Window state Zoom _in Zoom _out _About _Cancel _Copy Path to Clipboard _Help _Open _Open Folder _Quit storage;space;cleanup; translator-credits Project-Id-Version: gnome-utils HEAD
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=baobab&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 14:31+0000
PO-Revision-Date: 2015-10-24 05:55+0000
Last-Translator: Jean-Marc <Unknown>
Language-Team: GNOME French Team <gnomefr@traduc.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 17:41+0000
X-Generator: Launchpad (build 18115)
Language: 
 « %s » n'est pas un dossier valide %d jour %d jours %d élément %d éléments %d mois %d mois %d an %d ans Un outil graphique pour analyser l'utilisation des disques. Une liste d'URI des partitions à exclure de l'analyse. Une application simple qui peut analyser aussi bien des dossiers particuliers (locaux ou distants) que des volumes et donner une représentation graphique qui indique la taille et le pourcentage des répertoires. Graphique actif Les tailles apparentes sont affichées à la place. Baobab Vérifier la taille des dossiers et l'espace disque disponible Fermer Ordinateur Sommaire Impossible d'analyser l'utilisation du disque. Impossible d'analyser le volume. Impossible de détecter les tailles des espaces occupés. Impossible d'analyser le dossier « %s » Impossible d'analyser certains dossiers contenus dans « %s » Périphériques et emplacements Analyseur d'utilisation des disques URI des partitions exclues Impossible de déplacer le fichier vers la corbeille Impossible d'ouvrir le fichier Impossible d'afficher l'aide Dossier Aller au dossier _parent Dossier personnel _Mettre à la corbeille Modifié Afficher les informations de version et quitter Analyser récursivement les points de montage Diagramme en anneaux Analyser le dossier… Sélectionner un dossier Taille L'état GdkWindowState de la fenêtre La taille initiale de la fenêtre Aujourd'hui Carte arborescente Inconnue Indique le type de graphique à afficher. Taille de la fenêtre État de la fenêtre Zoom a_vant Zoom a_rrière _À propos _Annuler _Copier le chemin dans le presse-papiers Aid_e _Ouvrir _Ouvrir le dossier _Quitter stockage;espace disque;nettoyage; Christophe Merlet <redfox@redfoxcenter.org>
Christophe Fergeau <teuf@users.sourceforge.net>
Baptiste Mille-Mathias <bmm80@free.fr>
Cyprien Le Pannérer <cyplp@free.fr>
Jonathan Ernst <jonathan@ernstfamily.ch>
Robert-André Mauchin <zebob.m@pengzone.org>
Stéphane Raimbault <stephane.raimbault@gmail.com>
Alain Lojewski <allomervan@gmail.com>

Launchpad Contributions:
  Alain Lojewski https://launchpad.net/~allomervan
  Anne https://launchpad.net/~anneonyme017
  Jean-Marc https://launchpad.net/~m-balthazar
  Moez Bouhlel https://launchpad.net/~lejenome
  Sam Friedmann https://launchpad.net/~sam-friedmann
  Sylvie Gallet https://launchpad.net/~sylvie-gallet
  bruno https://launchpad.net/~annoa.b
  gisele perreault https://launchpad.net/~gisele-perreault 