��    l      |  �   �      0	  I   1	  J   {	  F   �	  A   
     O
     T
     Y
     ^
     c
     h
     m
     r
     v
  $   z
     �
     �
  
   �
     �
     �
     �
     �
          
  K     �   f     (     8     N     k  
   �     �     �  !   �  -   �     �       6     "   H     k     q     y     �     �     �  ,   �     �     �            #   #     G     M  4   i     �     �     �     �     �     �     �  2        9  #   N     r     �  '   �  "   �  '   �  "     #   &     J  "   i  '   �     �     �     �     �     �  5     0   B  1   s  /   �  5   �       "   )     L     e     �     �     �     �     �     �     �     �               (     5     B     I  
   O     Z     l  /   ~  Z   �     	       n     i   �  m   �  V   d     �     �     �     �     �     �     �     �     �  1   �     *     6     <     J     S  5   g  '   �     �     �  Q   �  �   3           ;     Q     n  	   �     �     �  ,   �  F   �  $        :  M   P  *   �     �     �     �     �  (   �  $     A   D     �     �     �     �  0   �     �  )   �  [        y     �  #   �     �     �      �     �  C        J  9   `  !   �     �  ,   �  (   �  ,     (   F  )   o  %   �  )   �  1   �  %         A      G   /   L   <   |   I   �   ?   !  @   C!  <   �!  Z   �!  %   "  -   B"  !   p"  (   �"  $   �"  (   �"  $   	#  
   .#     9#     O#     W#     c#     t#     �#     �#     �#  	   �#     �#     �#  0   �#     $  2   '$  Z   Z$  2  �$     _   V      ^          N   W   /          #   5   !   M   a   ?   .   e   7       b               `   1   I       4   j      K       "      <   3                      2   H               (   R             h   
       [       c   8   \   g             	      f         -   '   6       E   ]   S              T       &          k       :       =             C   i       J           %   0      B       F           U   *   D       +   G      X       d   A   Q   L       ,               P         Z          9          $      ;                Y   l   >   )      @   O    "name" and "link" elements are required inside '%s' on line %d, column %d "name" and "link" elements are required inside <sub> on line %d, column %d "title", "name" and "link" elements are required at line %d, column %d "type" element is required inside <keyword> on line %d, column %d 100% 125% 150% 175% 200% 300% 400% 50% 75% A developers' help browser for GNOME Back Book Book Shelf Book: Books disabled Cannot uncompress book '%s': %s Developer's Help program Devhelp Devhelp Website Devhelp integrates with other applications such as Glade, Anjuta, or Geany. Devhelp is an API documentation browser. It provides an easy way to navigate through libraries, search by function, struct, or macro. It provides a tabbed interface and allows to print results. Devhelp support Devhelp — Assistant Display the version and exit Documentation Browser Empty Page Enabled Enum Error opening the requested link. Expected '%s', got '%s' at line %d, column %d Font for fixed width text Font for text Font for text with fixed width, such as code examples. Font for text with variable width. Fonts Forward Function Group by language Height of assistant window Height of main window Invalid namespace '%s' at line %d, column %d KEYWORD Keyword Language: %s Language: Undefined List of books disabled by the user. Macro Main window maximized state Makes F2 bring up Devhelp for the word at the cursor New _Tab New _Window Opens a new Devhelp window Page Preferences Quit any running Devhelp S_maller Text Search and display any hit in the assistant window Search for a keyword Selected tab: "content" or "search" Show API Documentation Struct The X position of the assistant window. The X position of the main window. The Y position of the assistant window. The Y position of the main window. The height of the assistant window. The height of the main window. The width of the assistant window. The width of the index and search pane. The width of the main window. Title Type Use system fonts Use the system default fonts. Whether books should be grouped by language in the UI Whether the assistant window should be maximized Whether the assistant window should be maximized. Whether the main window should start maximized. Which of the tabs is selected: "content" or "search". Width of the assistant window Width of the index and search pane Width of the main window X position of assistant window X position of main window Y position of assistant window Y position of main window _About _About Devhelp _Close _Find _Fixed width: _Group by language _Larger Text _Normal Size _Preferences _Print _Quit _Side pane _Use system fonts _Variable width:  documentation;information;manual;developer;api; https://wiki.gnome.org/Apps/Devhelp?action=AttachFile&#x26;do=view&#x26;target=devhelp.png translator-credits Project-Id-Version: devhelp master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=devhelp&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-02-16 12:08+0000
PO-Revision-Date: 2015-11-10 05:03+0000
Last-Translator: Alain Lojewski <Unknown>
Language-Team: GNOME French Team <gnomefr@traduc.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:09+0000
X-Generator: Launchpad (build 18115)
Language: 
 Les éléments « name » et « link » doivent être présents dans « %s » à la ligne %d, colonne %d Les éléments « name » et « link » doivent être présents dans <sub> à la ligne %d, colonne %d Les éléments « title », « name » et « link » doivent être présents à la ligne %d, colonne %d L'élément « type » doit être présent dans <keyword> à la ligne %d, colonne %d 100 % 125 % 150 % 175 % 200 % 300 % 400 % 50 % 75 % Un navigateur d'aide pour les développeurs GNOME Précédent Livre Bibliothèque Livre : Livres désactivés Impossible de décompresser le livre « %s » : %s Programme d'aide pour les développeurs Devhelp Site Web de Devhelp Devhelp s'intègre avec d'autres applications, telles que Glade, Anjuta ou Geany. Devhelp est un navigateur pour la documentation des API. Il permet de naviguer facilement à travers les bibliothèques, chercher par fonction, structure ou macro. Il fournit une interface à onglets et permet d'imprimer les résultats. Prise en charge de Devhelp Devhelp — Assistant Affiche la version et quitte Navigateur de documentation Page vide Activé Enum Erreur lors de l'ouverture du lien demandé. « %s » était attendu, reçu « %s » à la ligne %d, colonne %d Police de caractères à chasse fixe Police de caractères Police pour les caractères à largeur fixe, comme pour les exemples de code. Police de caractères à largeur variable. Polices Suivant Fonction Grouper par langage La hauteur de la fenêtre de l'assistant La hauteur de la fenêtre principale Espace de nommage « %s » incorrect à la ligne %d, colonne %d MOTCLÉ Mot-clé Langage : %s Langage : non défini Liste des livres désactivés par l'utilisateur. Macro État maximisé de la fenêtre principale Un appui sur la touche F2 permet de lancer Devhelp pour le mot à l'emplacement du curseur. Nouvel _onglet Nouvelle _fenêtre Ouvre une nouvelle fenêtre Devhelp Page Préférences Quitte tous les Devhelp en cours Ré_duire le texte Recherche et affiche tout résultat dans la fenêtre de l'assistant Recherche un mot-clé Onglet sélectionné : « content » ou « search » Afficher la documentation des API Struct La position X de la fenêtre de l'assistant. La position X de la fenêtre principale. La position Y de la fenêtre de l'assistant. La position Y de la fenêtre principale. La hauteur de la fenêtre de l'assistant. La hauteur de la fenêtre principale. La largeur de la fenêtre de l'assistant. La largeur de l'index et du panneau de recherche. La largeur de la fenêtre principale. Titre Type Utiliser les polices de caractères du système Utiliser les polices de caractères par défaut du système. Indique si les livres doivent être groupés par langage dans l'interface Indique si la fenêtre de l'assitant doit démarrer maximisée. Indique si la fenêtre de l'assistant doit démarrer maximisée. Indique si la fenêtre principale doit démarrer maximisée. L'onglet qui est sélectionné : « content » (contenu) ou « search » (recherche). Largeur de la fenêtre de l'assistant Largeur de l'index et du panneau de recherche Largeur de la fenêtre principale Position X de la fenêtre de l'assistant Position X de la fenêtre principale Position Y de la fenêtre de l'assistant Position Y de la fenêtre principale À _propos À _propos de Devhelp F_ermer Re_chercher Largeur _fixe : _Grouper par langage A_grandir le texte Taille _normale _Préférences _Imprimer _Quitter Panneau _latéral _Utiliser les polices de caractères du système Largeur _variable :  documentation;information;manuel;développeur;api; https://wiki.gnome.org/Apps/Devhelp?action=AttachFile&#x26;do=view&#x26;target=devhelp.png Christophe Fergeau <christophe.fergeau@laposte.net>
Christophe Merlet <redfox@redfoxcenter.org>
Laurent Richard <laurent.richard@lilit.be>
Sebastien Bacher <seb128@debian.org>
Thierry Moisan <thierryn@videotron.ca>
Robert-André Mauchin <zebob.m@pengzone.org>
Jonathan Ernst <jonathan@ernstfamily.ch>
Damien Durand <splinux@fedoraproject.org>
Stéphane Raimbault <stephane.raimbault@gmail.com>

Launchpad Contributions:
  Alain Lojewski https://launchpad.net/~allomervan
  BobMauchin https://launchpad.net/~zebob.m
  Claude Paroz https://launchpad.net/~paroz
  Damien Durand https://launchpad.net/~splinux
  Gilles Accad https://launchpad.net/~dagg
  NSV https://launchpad.net/~nsv
  Stéphane Raimbault https://launchpad.net/~sra
  Thierry Moisan https://launchpad.net/~thierryn
  bruno https://launchpad.net/~annoa.b 