��    "      ,  /   <      �     �     
       
        #  .   :     i  	   |     �     �     �  !   �     �     �     �     �          .  ,   E     r     �     �     �     �  $   �     �     �       �   '     �     �     �     �  �  �     x     �     �  	   �     �  X   �  2   7  
   j     u     �  !   �  .   �     �     �     		  &   	  +   C	  $   o	  N   �	     �	     �	  '   
  %   +
     Q
  7   W
  '   �
     �
     �
  �   �
     �     �     �     �                                                                                 	             
         !                                             "          Add input method Addon Advance Appearance Available Input Method Cannot load currently used user interface info Clear font setting Configure Current Input Method Default Default keyboard layout Didn't install related component. Empty Global Config Input Method Input Method Configuration Input Method Default Input method settings: Keyboard layout to use when no input window: Keyboard layout: Language No configuration option for %s. Only Show Current Language Other Please press the new key combination Search Addon Search Input Method Show Advance Option The first input method will be inactive state. Usually you need to put <b>Keyboard</b> or <b>Keyboard - <i>layout name</i></b> in the first place. Unknown Unspecified _Cancel _OK Project-Id-Version: fcitx-configtool
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2013-10-18 03:07-0400
PO-Revision-Date: 2015-12-16 19:06+0000
Last-Translator: Ubuntu-QC-1 <Unknown>
Language-Team: French <fr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:00+0000
X-Generator: Launchpad (build 18115)
 Ajouter une méthode de saisie Module complémentaire Avancé Apparence Méthode de saisie disponible Impossible de charger les informations de l'interface utilisateur utilisée actuellement Effacer la configuration de polices de caractères Configurer Méthode de saisie actuelle Par défaut Agencement par défaut du clavier Le composant associé n'a pas été installé. Vide Configuration globale Méthode de saisie Configuration de la méthode de saisie Valeur par défaut de la méthode de saisie Paramètres de méthode de saisie : Agencement du clavier à utiliser lorsqu'il n'y a aucune fenêtre de saisie : Agencement du clavier : Langue Aucune option de configuration pour %s. Afficher seulement la langue actuelle Autre Veuillez appuyer sur la nouvelle combinaison de touches Rechercher des modules complémentaires Chercher une méthode de saisie Afficher les options avancées La première méthode de saisie sera à l'état inactif. Habituellement, vous devez saisir <b>Clavier</b> ou <b>Clavier - <i>nom de l'agencement</i></b> en premier. Inconnue Indéterminé _Annuler _OK 