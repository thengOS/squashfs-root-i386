��            )   �      �     �  	   �     �  	   �     �     �     �     �  �   �  �   �     g     l     t  	   �     �     �     �     �     �     �     �  !   �          &     +     3     M     ]  	  p  	   z  !   �     �     �     �     �  $   �  *       9  �   ?	     &
  	   3
     =
  
   V
     a
     e
     m
  #   t
     �
     �
  )   �
  7   �
  )        0     5  4   =     r  �  �                                                                                   	      
                                                 About All Fonts Back Copyright Description FONT-FILE OUTPUT-FILE Font Viewer GNOME Font Viewer GNOME Font Viewer also supports installing new font files downloaded in the .ttf and other formats. Fonts may be installed only for your use or made available to all users on the computer. GNOME Font Viewer shows you the fonts installed on your computer for your use as thumbnails. Selecting any thumbnails shows the full view of how the font would look under various sizes. Info Install Install Failed Installed Name Quit SIZE Show the application's version Style TEXT Text to thumbnail (default: Aa) This font could not be displayed. Thumbnail size (default: 128) Type Version View fonts on your system fonts;fontface; translator-credits Project-Id-Version: gnome-font-viewer master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-font-viewer&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-08-22 10:41+0000
PO-Revision-Date: 2016-08-23 03:30+0000
Last-Translator: Jean-Marc <Unknown>
Language-Team: français <gnomefr@traduc.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-10-09 11:50+0000
X-Generator: Launchpad (build 18227)
Language: fr
 À propos Toutes les polices de caractères Retour Droits d’auteur Description FICHIER-POLICE FICHIER-SORTIE Visionneur de polices de caractères Visionneur de polices de caractères GNOME Le visionneur de police de caractères GNOME prend aussi en charge l'installation de nouveaux fichiers de police téléchargés aux formats .ttf et autres. Les polices peuvent être installées pour vous seulement, ou pour tous les utilisateurs de l'ordinateur. Le visionneur de police GNOME affiche les polices de caractères installées sur votre ordinateur sous la forme de miniatures. En choisissant l'une de celles-ci, vous obtenez une vue complète de la police à différentes tailles. Informations Installer Échec de l'installation Installée Nom Quitter TAILLE Affiche la version de l'application Style TEXTE Texte à miniaturiser (par défaut : Aa) Cette police de caractères n'a pas pu être affichée. Taille des vignettes (par défaut : 128) Type Version Affiche les polices de caractères de votre système polices;fontes; Bruno Brouard <annoa.b@gmail.com>
Bruno Cauet <brunocauet@gmail.com>
Guillaume Bernard <translate@filorin.fr>

Launchpad Contributions:
  Anne017 https://launchpad.net/~anneonyme017
  Brun Cauet https://launchpad.net/~brunocauet
  Guillaume Bernard https://launchpad.net/~translate-g
  Jean-Marc https://launchpad.net/~m-balthazar
  Philip Millan https://launchpad.net/~flaipe1
  Pierre Slamich https://launchpad.net/~pierre-slamich
  Thomas.M https://launchpad.net/~maucourt-t
  Ubuntu-QC-1 https://launchpad.net/~ubuntu-qc-1
  bruno https://launchpad.net/~annoa.b
  electroluth https://launchpad.net/~jerome-barbaza
  londumas https://launchpad.net/~helion331990 