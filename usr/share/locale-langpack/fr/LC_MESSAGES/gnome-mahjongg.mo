��    @        Y         �  *   �  �  �     H  6   M  .   �  �   �     9  �   H     �  ;   
	     F	     N	  
   W	     b	     h	     �	     �	     �	     �	     �	     �	     �	  !   �	     
     
     0
     7
     <
     P
     a
     n
  O   �
     �
     �
     �
  	   �
                 	   (  	   2  	   <     F     S     Y     b     p     x     �     �     �     �  #   �     �               9  !   T     v     �     �     �     �      2        P     Q  G   V  C   �  �   �     �  �   �      {  e   �                    +  2   ;     n     �     �     �     �     �     �  )   �  !     1   1  	   c     m     t     �  !   �      �  j   �  
   V     a     }  	   �     �     �     �  	   �     �     �     �     �     �          #  
   +  
   6     A      J     k     q  	   �     �     �     �     �     �     �     �  �  �  "   �        /   4                       ?   $   :                      ,                 2         #   ;              <   6   1         %   -   9   @           	      (         +                      8       3      )          5      "                                     *   &   >      .           
       !   0   7       '   =       A matching game played with Mahjongg tiles A solitaire version of the classic Eastern tile game. Tiles are stacked on the board at the start of the game. The goal is to remove all the tiles in as little time as possible. Select two matching tiles and they will disappear from the board, but you can only select a tile if there is an empty space to its left or right at the same level. Be careful: tiles that look alike may actually be slightly different. Date Disassemble a pile of tiles by removing matching pairs Do you want to start a new game with this map? Each puzzle has at least one solution.  You can undo your moves and try and find the solution, restart this game, or start a new one. GNOME Mahjongg GNOME Mahjongg features a variety of starting layouts, some easy and some difficult. If you get stuck, you can ask for a hint, but this adds a large time penalty. Height of the window in pixels If you continue playing the next game will use the new map. Layout: Mahjongg Main game: Maps: Match tiles and clear the board Moves Left: New Game OK Pause the game Paused Preferences Print release version and exit Receive a hint for your next move Redo your last move There are no more moves. Tiles: Time Undo your last move Unpause the game Use _new map Width of the window in pixels You can also try to reshuffle the game, but this does not guarantee a solution. _About _Background color: _Close _Contents _Continue playing _Help _Layout: _Mahjongg _New Game _New game _Preferences _Quit _Restart _Restart Game _Scores _Shuffle _Theme: _Undo game;strategy;puzzle;board; mahjongg map nameCloud mahjongg map nameConfounding Cross mahjongg map nameDifficult mahjongg map nameEasy mahjongg map nameFour Bridges mahjongg map nameOverpass mahjongg map namePyramid's Walls mahjongg map nameRed Dragon mahjongg map nameThe Ziggurat mahjongg map nameTic-Tac-Toe translator-credits true if the window is maximized Project-Id-Version: gnome-mahjongg master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-mahjongg&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 22:17+0000
PO-Revision-Date: 2015-12-04 15:59+0000
Last-Translator: Claude Paroz <claude@2xlibre.net>
Language-Team: GNOME French Team <gnomefr@traduc.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:21+0000
X-Generator: Launchpad (build 18115)
Language: fr
 Un jeu de correspondances avec des tuiles Mahjongg Une version solitaire du classique jeu de tuiles oriental. Les tuiles sont empilées sur le plateau au début de la partie. Le but est d'enlever toutes les tuiles en prenant le moins de temps possible. Lorsque deux tuiles qui se correspondent sont sélectionnées, elles disparaissent du plateau. Cependant, une tuile ne peut être sélectionnée que s'il y a un espace vide au même niveau à sa gauche ou à sa droite. Attention : des tuiles qui se ressemblent peuvent être en fait légèrement différentes. Date Désassembler une pile de tuiles en enlevant les paires correspondantes Voulez-vous commencer une nouvelle partie avec cette disposition ? Chaque énigme possède au moins une solution. Vous pouvez annuler vos mouvements et essayer de trouver la solution, redémarrer la partie ou en démarrer une nouvelle. GNOME Mahjongg GNOME Mahjongg comprend une variété de dispositions de départ, certaines faciles, d'autres plus difficiles. Si vous êtes bloqué, vous pouvez demander un indice mais vous recevez alors une pénalité de temps importante. Hauteur de la fenêtre en pixels Si vous choisissez de poursuivre cette partie, la prochaine partie utilisera la nouvelle disposition. Disposition : Mahjongg Jeu principal : Dispositions : Faites correspondre des tuiles et videz le plateau Mouvements restants : Nouvelle partie Valider Suspend la partie Suspendu Préférences Afficher la version et quitter Afficher une astuce pour le prochain coup Rétablir le dernier déplacement Désolé, il n'y a plus de déplacement possible. Tuiles : Durée Annuler le dernier déplacement Reprendre la partie Utiliser la _nouvelle disposition Largeur de la fenêtre en pixels Vous pouvez aussi essayer de remélanger les tuiles, mais cela ne garantit pas l'existence d'une solution. À _propos Co_uleur d'arrière-plan : _Fermer _Sommaire _Poursuivre la partie Aid_e _Disposition : _Mahjongg _Nouvelle partie _Nouvelle partie _Préférences _Quitter _Recommencer _Recommencer la partie _Scores _Mélanger _Thème : Ann_uler jeu;stratégie;énigmes;plateau; Nuage Croix de confusion Difficile Facile Quatre ponts Viaduc Les murs de la pyramide Dragon rouge La Ziggurat Tic-Tac-Toe Christophe Merlet <redfox@redfoxcenter.org>
Guy Daniel Clotilde <guy.clotilde@wanadoo.fr>
Sébastien Bacher <seb128@debian.org>
Vincent Carriere <carriere_vincent@yahoo.fr>
Christophe Bliard <christophe.bliard@netcourrier.com>
Xavier Claessens <x_claessens@skynet.be>
Jonathan Ernst <jonathan@ernstfamily.ch>
Claude Paroz <claude@2xlibre.net>
Stéphane Raimbault <stephane.raimbault@gmail.com>
Didier Vidal <didier-devel@melix.net>
Pierre Lemaire <pierre.lemaire@kamick.org>

Launchpad Contributions:
  Anne https://launchpad.net/~anneonyme017
  Claude Paroz https://launchpad.net/~paroz
  Jean-Marc https://launchpad.net/~m-balthazar
  bruno https://launchpad.net/~annoa.b
  naybnet https://launchpad.net/~naybnet vrai si la fenêtre est maximisée 