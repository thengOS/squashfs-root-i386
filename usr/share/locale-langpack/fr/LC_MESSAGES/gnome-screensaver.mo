��    7      �  I   �      �     �     �     �     �  )   
     4  (   @     i          �  "   �  E   �       %   +     Q     Z     b  )   �     �  *   �     �  /   �     #  	   6  8   @  "   y     �     �     �     �     �     �       D   9  `   ~     �     �  )   	     A	  *   S	  #   ~	  	   �	     �	  D   �	  D   
     R
  !   q
  ,   �
  B   �
  
          '         >  +   _  H  �     �     �  !     )   3  8   ]     �  5   �  )   �       )   -  3   W  G   �     �  >   �     +     <  .   D  4   s     �  B   �       @        S     m  H   }  *   �  ,   �          8     N  "   n  .   �  0   �  d   �  v   V  "   �  $   �  6        L  2   \  *   �     �     �  K   �  Y   8  "   �  ,   �  C   �  J   &     q     �  1   �     �  P   �         -              0                	      1              4      %       '      *      6                 !   &          #         7   
   "       (      5   /   +              )   2                           ,   $         .            3                         (current) Unix password: Authentication failed. Can't set PAM_TTY=%s Cannot get username Causes the screensaver to exit gracefully Checking… Command to invoke from the logout button Don't become a daemon Enable debugging code Enter new Unix password: Error while changing NIS password. If the screensaver is active then deactivate it (un-blank the screen) Incorrect password. Launch screensaver and locker program Log _Out MESSAGE Message to show in the dialog No longer permitted to access the system. No password supplied Not permitted to gain access at this time. Not used Password has been already used. Choose another. Password unchanged Password: Query the length of time the screensaver has been active Query the state of the screensaver Retype new Unix password: S_witch User… Screensaver Show debugging output Show the logout button Show the switch user button Sorry, passwords do not match Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Time has expired. Turn the screensaver on (blank the screen) Unable to establish service %s: %s
 Username: Version of this application You are required to change your password immediately (password aged) You are required to change your password immediately (root enforced) You have the Caps Lock key on. You must choose a longer password You must wait longer to change your password Your account has expired; please contact your system administrator _Password: _Unlock failed to register with the message bus not connected to the message bus screensaver already running in this session Project-Id-Version: gnome-screensaver master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-01-04 14:27+0000
PO-Revision-Date: 2016-01-05 00:35+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: GNOME French Team <gnomefr@traduc.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 17:43+0000
X-Generator: Launchpad (build 18115)
 Mot de passe Unix (actuel) : L'authentification a échoué. Impossible de définir PAM_TTY=%s Impossible d'obtenir un nom d'utilisateur Demande à l'économiseur d'écran de quitter proprement Vérification en cours… Commande à invoquer depuis le bouton de déconnexion Ne pas devenir un « daemon » (démon) Activer le code de débogage Saisissez un nouveau mot de passe Unix : Erreur lors de la modification du mot de passe NIS. Désactive l'économiseur d'écran s'il est actif (réaffiche l'écran) Mot de passe non valide. Lancer l'économiseur d'écran et le programme de verrouillage Se _déconnecter MESSAGE Message à afficher dans la boîte de dialogue Vous n'êtes plus autorisé à accéder au système. Aucun mot de passe fourni Vous n'êtes pas autorisé à accéder au système à cette heure. Non utilisé Le mot de passe a déjà été utilisé. Choisissez en un autre. Mot de passe non modifié Mot de passe : Indique la durée pendant laquelle l'économiseur d'écran a été actif Indique l'état de l'économiseur d'écran Ressaisissez un nouveau mot de passe Unix : Changer d'_utilisateur… Économiseur d'écran Afficher la sortie de débogage Afficher le bouton de déconnexion Afficher le bouton de changement d'utilisateur Désolé, les mots de passe ne correspondent pas Demande à l'économiseur d'écran en cours de fonctionnement de verrouiller l'écran immédiatement L'économiseur d'écran a été actif pendant %d seconde.
 L'économiseur d'écran a été actif pendant %d secondes.
 L'économiseur d'écran est actif
 L'économiseur d'écran est inactif
 L'économiseur d'écran n'est actuellement pas actif.
 Délai expiré. Démarre l'économiseur d'écran (efface l'écran) Impossible d'établir le service %s : %s
 Nom d'utilisateur : Version de cette application Vous devez modifier votre mot de passe immédiatement (mot de passe ancien) Vous devez modifier votre mot de passe immédiatement (à la demande de l'administrateur) La touche Verr. Maj. est activée. Vous devez choisir un mot de passe plus long Vous devez attendre plus longtemps pour modifier votre mot de passe Votre compte a expiré ; veuillez contacter votre administrateur système _Mot de passe : _Déverrouiller l'enregistrement sur le bus de message a échoué non connecté au bus de message l'économiseur d'écran est déjà en cours de fonctionnement pour cette session 