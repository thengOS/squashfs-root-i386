��    -      �  =   �      �     �     �          <     \     n  I   �  &   �  :   �     4     9  !   L     n     �  $   �     �     �     �  +        F     Y      y  5   �  4   �  1     )   7  6   a     �  ,   �  I   �  =     0   ]  1   �  2   �  3   �  M   '	  N   u	  E   �	  %   

  '   0
  !   X
     z
  (   �
  8   �
  �  �
  &   �  0   �  .   �  *        8      Q  X   r  ;   �  9        A     I  '   i     �     �  .   �  (   �  %     %   C  3   i     �  0   �  &   �  @     F   O  I   �  )   �  ?   
     J  0   ]  M   �  >   �  ?     I   [  I   �  G   �  N   7  W   �  G   �  3   &  6   Z  $   �     �  2   �  ?              *              (          )         +   %   ,                                  !   "                       $      -                    &   
                               #      '             	              %s: %s: error closing: %s
 %s: %s: error opening file: %s
 %s: %s: error parsing file: %s
 %s: %s: error writing to stdout %s: missing files %s:%d:%d: Parse error: %s A GVariant dictionary entry expects a JSON object with exactly one member Error parsing commandline options: %s
 Expecting a JSON object, but the root node is of type `%s' FILE Format JSON files. GVariant class '%c' not supported Indentation spaces Invalid GVariant signature Invalid array index definition '%*s' Invalid first character '%c' Invalid set definition '%*s' Invalid slice definition '%*s' Invalid string value converting to GVariant JSON data is empty JSON data must be UTF-8 encoded Malformed slice expression '%*s' Missing closing symbol ')' in the GVariant tuple type Missing elements in JSON array to conform to a tuple Missing member name or wildcard after . character No node available at the current position Only one root node is allowed in a JSONPath expression Prettify output Root node followed by invalid character '%c' The current node is of type '%s', but an array or an object was expected. The current node is of type '%s', but an object was expected. The current position does not hold a string type The current position holds a '%s' and not a value The current position holds a '%s' and not an array The current position holds a '%s' and not an object The index '%d' is greater than the size of the array at the current position. The index '%d' is greater than the size of the object at the current position. The member '%s' is not defined in the object at the current position. Try "%s --help" for more information. Unexpected extra elements in JSON array Unexpected type '%s' in JSON node Validate JSON files. json-glib-format formats JSON resources. json-glib-validate validates JSON data at the given URI. Project-Id-Version: json-glib
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-03-01 22:13+0000
PO-Revision-Date: 2015-01-31 08:26+0000
Last-Translator: Jean-Marc <Unknown>
Language-Team: French <fr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:32+0000
X-Generator: Launchpad (build 18115)
 %s : %s : erreur de fermeture : %s
 %s : %s : erreur d'ouverture du fichier : %s
 %s : %s : erreur d'analyse du fichier : %s
 %s : %s : erreur d'écriture vers stdout %s : fichiers manquants %s:%d:%d: Erreur d'analyse : %s Une entrée dans le dictionnaire GVariant attend un objet JSON avec exactement un membre Erreur d'analyse des options de la ligne de commande : %s
 Objet JSON attendu, mais le nœud racine est du type '%s' FICHIER Mise en forme de fichiers JSON. Classe GVariant '%c' non pris en charge Espaces d'indentation Signature GVariant non valide Définition d'indice de tableau invalide '%*s' Premier caractère « %c » non valide Définition d'ensemble invalide '%*s' Définition de tranche invalide '%*s' Valeur de chaîne à convertir en GVariant invalide La donnée JSON est vide Les données JSON doivent être codées en UTF-8 Expression de tranche malformée '%*s' Symbole de fermeture manquant ')' dans le type de tuple GVariant Des éléments son manquant dans le tableau JSON pour un tuple correct Nom de membre ou caractère joker manquant après un caractère « . » Aucun nœud disponible à cette position. Un seul nœud racine est autorisé dans une expression JSONPath Formatage indenté Nœud racine suivi d'un caractère invalide '%c' Le nœud courant est de type '%s' mais un tableau ou un objet était attendu. Le nœud actuel est de type '%s' mais un objet était attendu. La position actuelle ne contient pas une chaîne de caractères La position actuelle contient un élément de type '%s' et non une valeur La position actuelle contient un élément de type '%s' et non un tableau La position actuelle contient un élément de type '%s' et non un objet L'indice '%d' est plus grand que la taille du tableau à la position actuelle. L'indice '%d' est plus grand que la taille de l'objet indiqué à la position actuelle. Le membre '%s'  n'est pas défini dans l'objet à la position actuelle. Essayez « %s --help » pour plus d'informations. Eléments superflus et innatendus dans le tableau JSON Type '%s' attendu dans le nœud JSON Validation de fichiers JSON. json-glib-format met en forme des ressources JSON. json-glib-validate valide des données JSON à l'URI indiquée. 