��    (      \  5   �      p  *   q  
   �  )   �     �     �  z   �     l     {  ,   �     �     �  3   �     %     6  7   O  (   �  a   �          1     P     m     �  -   �     �  (   �       &        :  -   O  '   }     �     �     �     �     �     �  	     
        )    B  A   S
     �
  5   �
     �
     �
  �        �     �  5   �  4     )   A  E   k     �  #   �  D   �  8   .  �   g  !   �  !        8     V     t  9   �     �  7   �       2   .     a  9   |  6   �     �     �          '  ,   :     g     �     �  $   �                 
      	           '                    $         %                             !                                             #       "             &            (       Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd HEAD fr
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&component=general
POT-Creation-Date: 2016-01-28 14:10+0000
PO-Revision-Date: 2014-11-17 12:33+0000
Last-Translator: Ubuntu-QC-1 <Unknown>
Language-Team: GNOME French Team <gnomefr@traduc.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 18:11+0000
X-Generator: Launchpad (build 18115)
 Groupe par défaut, attribué lors de la création d'une fenêtre Indicateur : Gère et maintient des groupes distincts par fenêtre Agencement de clavier Agencement de clavier Agencement de clavier « %s »
Copyright &#169; X.Org Foundation et contributeurs XKeyboardConfig
Les détails de licence se trouvent dans les méta-données du paquet. Modèle de clavier Options de clavier Charger les agencements et options rarement utilisés Charger les objets de configuration supplémentaires Prévisualiser les agencements de clavier Enregistre et restaure les indicateurs avec les groupes d'agencements Groupes secondaires Afficher les drapeaux dans l'applet Affiche des drapeaux dans l'applet pour indiquer l'agencement actuel Affiche les noms d'agencement au lieu des noms de groupe Affiche les noms d'agencement au lieu des noms de groupe (uniquement pour les versions de XFree prenant en charge les agencements multiples) L'aperçu du clavier, décalage X L'aperçu du clavier, décalage Y L'aperçu du clavier, hauteur L'aperçu du clavier, largeur La couleur d'arrière-plan La couleur d'arrière-plan pour l'indicateur d'agencement La police de caractères La police de caractères pour l'indicateur d'agencement La taille de police La taille de police pour l'indicateur d'agencement La couleur de premier plan La couleur de premier plan pour l'indicateur d'agencement Une erreur s'est produite en chargeant une image : %s Inconnu Erreur d'initialisation XKB agencement de clavier modèle de clavier agencement « %s » agencements « %s » modèle « %s », %s et %s aucun agencement aucune option option « %s » options « %s » 