��    �      �  �   �      �  *   �      �     �  	     *     0   8     i     q     �     �     �  /   �     �  ,   �  A        \      z  %   �  "   �  $   �  H   	      R  J   s  .   �  "   �       '   -     U  $   r  $   �  (   �  O   �  I   5  #        �  !   �     �  #   �  !   !  *   C  +   n  #   �  *   �     �               8     V  5   u  .   �     �  	   �       #        5  5   N  /   �     �     �  U   �  I   1  	   {     �     �     �     �     �     �     �  
   �     �     �       -   &     T     d     t     �     �     �     �     �  
   �     �     �     �     �          "     8  #   T  4   x  3   �  +   �  9     9   G     �     �  =   �  D   �  F   !  "   h     �     �     �  .   �  D   �  E   )     o     x  .   �     �     �     �     �     �               *     =     P     `     o     ~     �     �     �     �     �     �  *   �          #     4     E     V  b   e  X   �  .   !       P      q      �   9   �      �   :   �   �   !  N   "  ?   a"  1  �"     �#     �#     �#     $     $  8   $  (   S$  C   |$  �   �$  Q   J%     �%     �%  
   �%     �%     �%  	   �%  
   �%     �%     �%  .   �%  (   #&  Y   L&  N   �&  ,   �&  T   "'  =   w'  5   �'  5   �'  H   !(  \   j(  R   �(  ^   )  D   y)  D   �)     *  0   "*  �  S*  -   �+  )   ,     C,  
   R,  /   ],  E   �,     �,     �,     �,     �,  
   -  C   -  ,   R-  @   -  :   �-  ,   �-  7   (.  ?   `.  5   �.  4   �.  d   /  +   p/  G   �/  R   �/  1   70  -   i0  5   �0  (   �0  4   �0  6   +1  B   b1  u   �1  p   2  =   �2  1   �2  5   �2  *   23  8   ]3  1   �3  B   �3  D   4  :   P4  D   �4     �4  )   �4     5  ?   95  8   y5  K   �5  L   �5  .   K6     z6     �6  1   �6  '   �6  I   �6  :   A7  
   |7  &   �7  o   �7  c   8     �8     �8     �8     �8  	   �8     �8     �8     �8  
   �8      9     9     39  C   R9     �9     �9     �9     �9     �9     �9     �9     �9     :     :     #:     0:     =:     P:     j:     �:  6   �:  P   �:  E   &;  B   l;  Q   �;  Q   <     S<     h<  M   �<  S   �<  O   "=  0   r=  !   �=     �=     �=  D   �=  ^   %>  ^   �>     �>     �>  J   �>     H?     ]?     r?     �?     �?     �?     �?     �?     �?     �?     @     @     '@     6@     G@     V@     g@     w@     �@  '   �@     �@     �@     �@     �@     A  {   A  t   �A  5   B  %   7B     ]B     yB  P   �B     �B  \   �B    MC  }   \D  [   �D  �  6E     �F     �F     �F     �F  	   G  `   G  8   yG  N   �G  �   H  l   �H     I     !I  
   (I     3I     AI  	   PI  
   ZI     eI     nI  <   sI  4   �I  x   �I  r   ^J  B   �J  f   K  A   {K  K   �K  K   	L  h   UL  m   �L  f   ,M  m   �M  ^   N  ^   `N  6   �N  F   �N     *       H   +       �       u      6      �   Q           1       �       �   w   �   -      �   !   l   �       t   ^       �                      �   9   i          b   D   �   �   V   �      [   �   ,       s   Z   ~   C          �   �   G       �   P                  �       L   e   �       �   &   R       �   �       d       |   S   �      j   �   �   U       5   �   �   $       M      ;      �       �   K   �       W      %   �      �   E           a   �       `   �   =      '               �       �   �   �   3          T   �       �   x       #       ?   �      "   �       (   �   Y   q   )       �   \                     �   .   �       /   	           B   z       f   }           4   {       �          
   k       X   8   g             �   o   >       :      �          �   �       m   �       O   v   n      �   ]      �   �   �   A   y   p       0       _          �   @   2   c   �           �   r   <          h       J      I   7   N      �   F    %s: Unexpected length %d for genius_cuid!
 '%s' could not be accessed (%s). <No members>
 <Unnamed> Artwork support not compiled into libgpod. Cannot remove Photo Library playlist. Aborting.
 Classic Classic (Black) Classic (Silver) Color Color U2 Control directory not found: '%s' (or similar). Could not access file '%s'. Could not access file '%s'. Photo not added. Could not find corresponding track (dbid: %s) for artwork entry.
 Could not find on iPod: '%s'
 Could not open '%s' for writing. Couldn't find an iPod database on %s. Couldn't read xml sysinfo from %s
 Couldn't write SysInfoExtended to %s Destination file '%s' does not appear to be on the iPod mounted at '%s'. Device directory does not exist. Encountered unknown MHOD type (%d) while parsing the iTunesDB. Ignoring.

 Error adding photo (%s) to photo database: %s
 Error compressing iTunesCDB file!
 Error creating '%s' (mkdir)
 Error initialising iPod, unknown error
 Error initialising iPod: %s
 Error opening '%s' for reading (%s). Error opening '%s' for writing (%s). Error reading iPod photo database (%s).
 Error reading iPod photo database (%s).
Will attempt to create a new database.
 Error reading iPod photo database, will attempt to create a new database
 Error reading iPod photo database.
 Error removing '%s' (%s). Error renaming '%s' to '%s' (%s). Error when closing '%s' (%s). Error while reading from '%s' (%s). Error while writing to '%s' (%s). Error writing list of albums (mhsd type 4) Error writing list of artists (mhsd type 8) Error writing list of tracks (hths) Error writing list of tracks (mhsd type 1) Error writing mhsd type 10 Error writing mhsd type 6 Error writing mhsd type 9 Error writing mhsd5 playlists Error writing playlists (hphs) Error writing special podcast playlists (mhsd type 3) Error writing standard playlists (mhsd type 2) Error: '%s' is not a directory
 Grayscale Grayscale U2 Header is too small for iTunesCDB!
 Illegal filename: '%s'.
 Illegal seek to offset %ld (length %ld) in file '%s'. Insufficient number of command line arguments.
 Invalid Itdb_Track ID '%d' not found.
 Length of smart playlist rule field (%d) not as expected. Trying to continue anyhow.
 Library compiled without gdk-pixbuf support. Picture support is disabled. Master-PL Mini (1st Gen.) Mini (2nd Gen.) Mini (Blue) Mini (Gold) Mini (Green) Mini (Pink) Mini (Silver) Mobile (1) Mobile Phones Mountpoint not set. Mountpoint not set.
 Music directory not found: '%s' (or similar). Nano (1st Gen.) Nano (2nd Gen.) Nano (Black) Nano (Blue) Nano (Green) Nano (Orange) Nano (Pink) Nano (Purple) Nano (Red) Nano (Silver) Nano (White) Nano (Yellow) Nano Video (3rd Gen.) Nano Video (4th Gen.) Nano touch (6th Gen.) Nano with camera (5th Gen.) No 'F..' directories found in '%s'. Not a OTG playlist file: '%s' (missing mhpo header). Not a Play Counts file: '%s' (missing mhdp header). Not a iTunesDB: '%s' (missing mhdb header). Number of MHODs in mhip at %ld inconsistent in file '%s'. Number of MHODs in mhyp at %ld inconsistent in file '%s'. OTG Playlist OTG Playlist %d OTG playlist file '%s': reference to non-existent track (%d). OTG playlist file ('%s'): entry length smaller than expected (%d<4). OTG playlist file ('%s'): header length smaller than expected (%d<20). Path not found: '%s' (or similar). Path not found: '%s'. Photo Photo Library Photos directory not found: '%s' (or similar). Play Counts file ('%s'): entry length smaller than expected (%d<12). Play Counts file ('%s'): header length smaller than expected (%d<96). Playlist Podcasts Problem creating iPod directory or file: '%s'. Regular (1st Gen.) Regular (2nd Gen.) Regular (3rd Gen.) Regular (4th Gen.) Shuffle Shuffle (1st Gen.) Shuffle (2nd Gen.) Shuffle (3rd Gen.) Shuffle (4th Gen.) Shuffle (Black) Shuffle (Blue) Shuffle (Gold) Shuffle (Green) Shuffle (Orange) Shuffle (Pink) Shuffle (Purple) Shuffle (Red) Shuffle (Silver) Shuffle (Stainless) Specified album '%s' not found. Aborting.
 Touch Touch (2nd Gen.) Touch (3rd Gen.) Touch (4th Gen.) Touch (Silver) Unable to retrieve thumbnail (appears to be on iPod, but no image info available): filename: '%s'
 Unexpected error in itdb_photodb_add_photo_internal() while adding photo, please report. Unexpected image type in mhni: %d, offset: %d
 Unexpected mhod string type: %d
 Unexpected mhsd index: %d
 Unknown Unknown action (0x%x) in smart playlist will be ignored.
 Unknown command '%s'
 Unknown smart rule action at %ld: %x. Trying to continue.
 Usage to add photos:
  %s add <mountpoint> <albumname> [<filename(s)>]
  <albumname> should be set to 'NULL' to add photos to the master photo album
  (Photo Library) only. If you don't specify any filenames an empty album will
  be created.
 Usage to dump all photos to <output_dir>:
  %s dump <mountpoint> <output_dir>
 Usage to list all photos IDs to stdout:
  %s list <mountpoint>
 Usage to remove photo IDs from Photo Library:
  %s remove <mountpoint> <albumname> [<ID(s)>]
  <albumname> should be set to 'NULL' to remove photos from the iPod
  altogether. If you don't specify any IDs, the photoalbum will be removed
  instead.
  WARNING: IDs may change when writing the PhotoDB file.
 Video (1st Gen.) Video (2nd Gen.) Video (Black) Video (White) Video U2 Warning: could not find photo with ID <%d>. Skipping...
 Wrong number of command line arguments.
 You need to specify the iPod model used before photos can be added. Your iPod does not seem to support photos. Maybe you need to specify the correct iPod model number? It is currently set to 'x%s' (%s/%s). header length of '%s' smaller than expected (%d < %d) at offset %ld in file '%s'. iPad iPhone iPhone (1) iPhone (Black) iPhone (White) iPhone 3G iPhone 3GS iPhone 4 iPod iTunes directory not found: '%s' (or similar). iTunesCDB '%s' could not be decompressed iTunesDB '%s' corrupt: Could not find playlists (no mhsd type 2 or type 3 sections found) iTunesDB '%s' corrupt: Could not find tracklist (no mhsd type 1 section found) iTunesDB '%s' corrupt: mhsd expected at %ld. iTunesDB ('%s'): header length of mhsd hunk smaller than expected (%d<32). Aborting. iTunesDB corrupt: hunk length 0 for hunk at %ld in file '%s'. iTunesDB corrupt: no MHOD at offset %ld in file '%s'. iTunesDB corrupt: no SLst at offset %ld in file '%s'. iTunesDB corrupt: no section '%s' found in section '%s' starting at %ld. iTunesDB corrupt: number of mhip sections inconsistent in mhyp starting at %ld in file '%s'. iTunesDB corrupt: number of tracks (mhit hunks) inconsistent. Trying to continue.
 iTunesDB possibly corrupt: number of playlists (mhyp hunks) inconsistent. Trying to continue.
 iTunesStats file ('%s'): entry length smaller than expected (%d<18). iTunesStats file ('%s'): entry length smaller than expected (%d<32). usage: %s <device> [timezone]
 usage: %s <device|uuid|bus device> <mountpoint>
 Project-Id-Version: libgpod-0.8.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-04-13 08:40+0000
PO-Revision-Date: 2013-08-25 12:56+0000
Last-Translator: Pierre Slamich <pierre.slamich@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:52+0000
X-Generator: Launchpad (build 18115)
 %s: Longueur inattendue %d pour genius_cuid!
 Impossible d'accéder à « %s » (%s). <sans membre>
 <sans_nom> Pas de prise en charge des images dans libgpod. Impossible de supprimer la liste de la bibliothèque Photo. Abandon.
 Classic Classic (noir) Classic (argent) Couleur U2 couleur Répertoire de contrôle introuvable :  « %s » (ou similaire). Impossible d'accéder au fichier « %s ». Impossible d'accéder au fichier « %s ». Photo non ajoutée. Impossible de trouver la piste (dbid : %s) pour l'image.
 Impossible de trouver « %s » sur l'iPod
 Impossible d'ouvrir le fichier « %s » en écriture. Impossible de trouver une base de données iPod sur « %s ». Impossible de lire les « sysinfo xml » depuis %s
 Impossible d'écrire « SysInfoExtended » dans %s Le fichier de destination « %s » ne semble pas être présent dans l'iPod monté sur « %s ». Le répertoire « Device » n'existe pas. Type MHOD inconnu (%d) lors du parcours de iTunesDB. Erreur ignorée.

 Erreur à l'ajout de la photo (%s) dans la base de données des photos iPod : %s
 Erreur de décompression du fichier iTunesCDB !
 Erreur à la création de « %s » (mkdir)
 Erreur d'initialisation de l'iPod : erreur inconnue
 Erreur d'initialisation de l'iPod : %s
 Échec de l'ouverture de « %s » en lecture (%s). Échec de l'ouverture de « %s » en écriture (%s). Erreur à la lecture de la base de données des photos iPod (%s).
 Erreur à la lecture de la base de données des photos iPod (%s).
Tentative de créer une nouvelle base de données.
 Erreur à la lecture de la base de données des photos iPod, Tentative de créer une nouvelle base de données.
 Erreur à la lecture de la base de données des photos iPod.
 Erreur de suppression du fichier « %s » (%s). Impossible de renommer « %s » en « %s » (%s). Erreur à la fermeture de « %s » (%s). Erreur lors de la lecture à partir de « %s » (%s). Erreur lors de l'écriture dans « %s » (%s). Erreur lors de l'écriture de la liste des albums (mhsd de type 4) Erreur lors de l'écriture de la liste des artistes (mhsd de type 8) Erreur lors de l'écriture de la liste des morceaux (hths) Erreur lors de l'écriture de la liste des morceaux (mhsd de type 1) Erreur d'écriture mhsd type 10 Erreur lors de l'écriture mhsd de type 6 Erreur d'écriture mhsd type 9 Erreur lors de l'écriture des listes de lecture mhsd de type 6 Erreur lors de l'écriture des listes de lectures (hphs) Erreur lors de l'écriture des listes de lectures podcasts (mhsd de type 3) Erreur lors de l'écriture des listes de lectures standards (mhsd de type 2) Erreur : « %s » n'est pas un répertoire
 Niveaux de gris U2 niveaux de gris En-tête trop petit pour le fichier iTunesCDB !
 Nom de fichier illégal : « %s ».
 Erreur de flux à l'offset %ld (longueur %ld) dans le fichier « %s ». Nombre d'arguments insuffisant pour la ligne de commande.
 Non valide Itdb_Track ID « %d » introuvable.
 Longueur du champ de la règle (%d) de la liste intelligente inattendue. Tentative pour continuer quand même.
 Bibliothèque compilée sans la prise en charge gdk-pixbuf. La gestion des photos est désactivée. Liste principale Mini (1ère gén.) Mini (2ème gén.) Mini (bleu) Mini (or) Mini (vert) Mini (rose) Mini (argent) Mobile (1) Téléphones mobiles Point de montage non défini. Point de montage non défini.
 Répertoire « Music » introuvable : « %s » (ou similaire). Nano (1ère gén.) Nano (2ème gén.) Nano (noir) Nano (bleu) Nano (vert) Nano (orange) Nano (rose) Nano (violet) Nano (rouge) Nano (argent) Nano (blanc) Nano (jaune) Nano (3ème gén.) Nano vidéo (4ème gén.) Nano touch (6ème gén.) Nano caméra (5ème gén.) Répertoires « F.. » introuvables dans « %s ». Le fichier « %s » n'est pas un fichier de liste OTG (entête mhpo manquant). « %s » n'est pas un fichier de compteur (entête mhdp manquante). « %s » n'est pas un fichier iTunesDB (entête mhdb manquante). Nombre de MHODs dans mhip à l'offset %ld incohérent dans le fichier « %s ». Nombre de MHODs dans mhyp à l'offset %ld incohérent dans le fichier « %s ». Liste de lecture OTG Liste de lecture OTG %d Fichier de liste OTG « %s » : référence à un morceau inexistant (%d). Fichier de liste OTG ('%s') : longueur de données plus petite que prévue (%d<4). Fichier de liste OTG (« %s ») : entête plus petit que prévu (%d < 20). Chemin introuvable : « %s » (ou similaire). Chemin introuvable : « %s ». Photo Bibliothèque Photos Répertoire « Photos » introuvable : « %s » (ou similaire). Fichier de compteur (« %s ») : longueur des données plus petite que prévue (%d < 12). Fichier de compteur (« %s ») : longueur de l'entête plus petite que prévue (%d < 96). Liste de lecture Podcasts Problème à la création du répertoire ou du fichier iPod : « %s ». Normal (1ère gén.) Normal (2ème gén.) Normal (3ème gén.) Normal (4ème gén.) Shuffle Shuffle (1ère gén.) Shuffle (2ème gén.) Shuffle (3ème gén.) Shuffle (4ème gén.) Shuffle (noir) Shuffle (bleu) Shuffle (or) Shuffle (vert) Shuffle (orange) Shuffle (rose) Shuffle (violet) Shuffle (rouge) Shuffle (argent) Shuffle (inox) Album « %s » non trouvé. Abandon.
 Touch Touch (2ème gén.) Touch (3ème gén.) Touch (4ème gén.) Touch (argent) Impossible de récupérer l'imagette (apparemment sur l'iPod, mais aucune info d'image disponible : fichier : « %s »
 Erreur inattendue dans « itdb_photodb_add_photo_internal() » pendant l'ajout d'une photo, pensez à le signaler. Type d'image inattendu dans mhni : %d, offset : %d
 Type de chaîne mhod inattendu : %d
 Index mhsd inattendu : %d
 Inconnu L'action indéfinie (0x%x) dans la liste de lecture intelligente sera ignorée.
 Commande inconnue « %s »
 Règle de la liste intelligente inattendue %ld : %x. Tentative pour continuer quand même.
 Utilisation pour ajouter des photos :
    %s add <point_de_montage> <album> [<fichier(s)>]
    Le nom de l'album doit être « NULL » pour ajouter des photos
    dans l'album photo principal.
    Si vous ne précisez pas de nom de fichier un album vide sera créé.
 Utilisation pour copier toutes les photos dans <répertoire_de_copie> :
  %s dump <point_de_montage> <répertoire_de_copie>
 Utilisation pour afficher tous les identifiants des photos :
  %s list <point_de_montage>
 Utilisation pour supprimer des photos dans la librairie des photos :
    %s remove <point_de_montage> <album> [<ID(s)>]
    Le nom de l'album doit être « NULL » pour supprimer complètement
    les photos dans l'iPod.
    Si vous ne précisez pas d'identifiants, seul l'album sera supprimé.
    Attention : les identifiants peuvent changer lors de l'écriture
    du fichier « PhotoDB ».
 Vidéo (1ère gén.) Vidéo (2ème gén.) Vidéo (noir) Vidéo (blanc) U2 Vidéo Attention : impossible de trouver la photo avec l'identifiant <%d>. On passe à la suivante…
 Nombre d'arguments incorrect pour la ligne de commande.
 Vous devez préciser le modèle de l'iPod avant de pouvoir ajouter des photos. Votre iPod ne semble pas prendre en charge les photos. Peut-être devriez-vous spécifier le modèle correct d'iPod ? Il est actuellement configuré à « x%s » (%s/%s). longueur de l'entête « %s » plus petite que prévue (%d <  %d) à l'offset %ld du fichier « %s ». iPad iPhone iPhone (1) iPhone (noir) iPhone (blanc) iPhone 3G iPhone 3GS iPhone 4 iPod Répertoire iTunes introuvable : « %s » (ou similaire). iTunesCDB « %s » ne peut pas être décompressé Corruption de iTunesDB (« %s ») :  impossible de trouver les listes de lecture (pas de section mhsd de type 1 ou 3) Corruption de iTunesDB (« %s ») : impossible de trouver la liste des morceaux (pas de section mhsd de type 1) Corruption d'iTunesDB « %s » : mhsd attendu à l'offset %ld. iTunesDB (« %s ») : longueur de l'entête mhsd plus petite que prévue (%d < 32). Interruption. Corruption d'iTunesDB : longueur 0 à %ld du fichier « %s ». Corruption d'iTunesDB : pas de MHOD à l'offset %ld du fichier « %s ». Corruption d'iTunesDB : pas de SLst à l'offset %ld du fichier « %s ». Corruption d'iTunesDB : pas de section « %s » dans la section « %s » débutant à l'offset %ld. Corruption d'iTunesDB : nombre de sections mhip incohérent dans mhyp à l'offset %ld du fichier « %s ». Corruption d'iTunesDB : nombre de morceaux (mhit) incohérent. Tentative pour continuer quand même.
 Corruption possible d'iTunesDB : nombre de listes (mhyp) incohérent. Tentative pour continuer quand même.
 Fichier iTunesStats (« %s ») : longueur des données plus petite que prévue (%d < 18). Fichier iTunesStats (« %s ») : longueur des données plus petite que prévue (%d < 32). utilisation : %s <périphérique> [<fuseau_horaire>]
 utilisation : %s <dispositif|uuid|peripherique bus> <pointdemontage>
 