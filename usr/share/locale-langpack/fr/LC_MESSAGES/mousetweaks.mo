��            )         �  "   �     �  &   �          #     A     Y     f     k      ~     �     �  
   �     �  
   �     �       %        D     T     n     �  e   �     �          5  	   D  !   N  /   p     �  �  �  +   �     �  /   �  *     *   2  $   ]     �     �     �  &   �      �  +   �  
   )	     4	     C	  (   R	     {	  +   �	     �	  "   �	     �	     �	  }   
  (   �
  &   �
     �
  
   �
  !   �
  '        9                                                   
                     	                                                                - GNOME mouse accessibility daemon Button Style Button style of the click-type window. Click-type window geometry Click-type window orientation Click-type window style Double Click Drag Enable dwell click Enable simulated secondary click Failed to Display Help Hide the click-type window Horizontal Hover Click Icons only Ignore small pointer movements Orientation Orientation of the click-type window. Secondary Click Set the active dwell mode Shut down mousetweaks Single Click Size and position of the click-type window. The format is a standard X Window System geometry string. Start mousetweaks as a daemon Start mousetweaks in login mode Text and Icons Text only Time to wait before a dwell click Time to wait before a simulated secondary click Vertical Project-Id-Version: mousetweaks.HEAD
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=mousetweaks&component=general
POT-Creation-Date: 2015-12-03 23:45+0000
PO-Revision-Date: 2012-03-03 00:37+0000
Last-Translator: YannUbuntu <yannubuntu@gmail.com>
Language-Team: GNOME French Team <gnomefr@traduc.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:24+0000
X-Generator: Launchpad (build 18115)
 - Service d'accessibilité de souris GNOME Style de bouton Style de bouton de la fenêtre de type de clic. Géométrie de la fenêtre de type de clic Orientation de la fenêtre de type de clic Style de la fenêtre de type de clic Double-clic Glisser Activer les clics par maintien Activer les clics secondaires simulés L'affichage de l'aide a échoué Cacher la fenêtre de choix du type de clic Horizontal Clic de survol Icônes seules Ignore les petits mouvements du pointeur Orientation Orientation de la fenêtre de type de clic. Clic secondaire Définit le mode de maintien actif Quitte Mousetweaks Clic simple Dimension et position de la fenêtre de type de clic. Le format est une chaîne de géométrie standard du système X Window. Démarre Mousetweaks en tant que service Démarre Mousetweaks en mode connexion Texte et icônes Texte seul Délai avant un clic par maintien Délai avant un clic secondaire simulé Vertical 