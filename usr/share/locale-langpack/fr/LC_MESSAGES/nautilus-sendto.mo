��    	      d      �       �      �   )   �   2        F  ,   T  #   �  "   �  0   �  #  �       @   %  ;   f     �  <   �  .   �  <   "  ?   _                                          	    Archive Could not parse command-line options: %s
 Expects URIs or filenames to be passed as options
 Files to send No mail client installed, not sending files
 Output version information and exit Run from build directory (ignored) Use XID as parent to the send dialogue (ignored) Project-Id-Version: nautilus-sendto HEAD
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=nautilus-sendto&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 23:48+0000
PO-Revision-Date: 2015-12-04 22:01+0000
Last-Translator: Alexandre Franke <alexandre.franke@gmail.com>
Language-Team: GNOME French Team <gnomefr@traduc.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:34+0000
X-Generator: Launchpad (build 18115)
Language: 
 Archive Impossible d'analyser les options de la ligne de commande : %s
 Les options attendues sont des URI ou des noms de fichiers
 Fichiers à envoyer Aucun client de courriel installé, pas d'envoi de fichiers
 Retourne les informations de version et quitte Exécuter à partir du répertoire de construction (ignoré) Utiliser le XID comme parent pour la fenêtre d'envoi (ignoré) 