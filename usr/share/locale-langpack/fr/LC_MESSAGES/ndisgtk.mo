��          �   %   �      p     q     �  +   �  @   �  5        M  ,   _     �     �     �  $   �  	   �  2   �  $   2     W     Z     l  #   �  !   �     �  %   �               8     <     E     Y  �  h  "   �       2   +  O   ^  <   �     �  7        9     X     w  1   �     �  ?   �  +   	     8	     <	  +   Y	  5   �	  ,   �	     �	  0   
     7
     P
     m
  
   q
     |
     �
                    
                                                                                          	             <b>%s</b>
Hardware present: %s <b>%s</b>
Invalid Driver! <b>Currently Installed Windows Drivers:</b> <span size="larger" weight="bold">Select <i>inf</i> file:</span> Are you sure you want to remove the <b>%s</b> driver? Configure Network Could not find a network configuration tool. Driver is already installed. Error while installing. Install Driver Is the ndiswrapper module installed? Location: Module could not be loaded. Error was:

<i>%s</i>
 Ndiswrapper driver installation tool No No file selected. Not a valid driver .inf file. Please drag an '.inf' file instead. Root or sudo privileges required! Select inf File Unable to see if hardware is present. Windows Wireless Drivers Wireless Network Drivers Yes _Install _Install New Driver _Remove Driver Project-Id-Version: fr
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2008-10-02 17:50+0200
PO-Revision-Date: 2010-02-04 07:32+0000
Last-Translator: Cyril GRAGEON <cyril_g@users.sourceforge.net>
Language-Team: French
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:44+0000
X-Generator: Launchpad (build 18115)
 <b>%s</b>
Matériel présent : %s <b>%s</b>
Pilote invalide ! <b>Pilotes Windows actuellement installés : </b> <span size="larger" weight="bold">Sélectionner un fichier <i>inf</i> :</span> Êtes-vous certain de vouloir retirer le pilote <b>%s</b> ? Configurer le réseau Aucun outil de configuration réseau n'a été trouvé. Le pilote est déja installé. Erreur pendant l'installation. Installer un pilote Est-ce que le module ndiswrapper est installé ? Emplacement : Le module n'a pas pu être chargé. L'erreur est :

<i>%s</i>
 Outil d'installation de pilotes Ndiswrapper Non Aucun fichier sélectionné. N'est pas un fichier .inf de pilote valide. Veuillez glisser un fichier « .inf » à la place. Les prévilèges root ou sudo sont requis ! Sélectionner un fichier .inf Impossible de voir si le matériel est présent. Pilotes sans fil Windows Pilotes de réseaux sans fil Oui _Installer _Installer un nouveau pilote _Retirer un pilote 