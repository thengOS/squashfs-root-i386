��          �      ,      �     �     �     �     �  �   �  �   �  ~        �  /   �     �     �     �  ?        C     Q  
   c  �  n     l     t  $   �     �  �   �  �   ~  �        �  @   �     �     	     %	  K   E	     �	     �	     �	        	                                                                  
    %s (%s) <small><b>Action:</b></small> <small><b>Vendor:</b></small> <small><b>_Details</b></small> An application is attempting to perform an action that requires privileges. Authentication as one of the users below is required to perform this action. An application is attempting to perform an action that requires privileges. Authentication as the super user is required to perform this action. An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Authenticate Authentication dialog was dismissed by the user Click to edit %s Click to open %s Select user... Your authentication attempt was unsuccessful. Please try again. _Authenticate _Password for %s: _Password: Project-Id-Version: policykit-gnome HEAD
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=policykit-gnome&component=general
POT-Creation-Date: 2015-12-04 00:05+0000
PO-Revision-Date: 2012-03-03 00:37+0000
Last-Translator: Messer Kevin <messer.kevin@gmail.com>
Language-Team: GNOME French Team <gnomefr@traduc.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:41+0000
X-Generator: Launchpad (build 18115)
 %s (%s) <small><b>Action :</b></small> <small><b>Fournisseur :</b></small> <small><b>_Détails</b></small> Une application tente d'effectuer une action qui nécessite des privilèges. Pour effectuer cette action, un utilisateur de la liste ci-dessous doit s'authentifier. Une application tente d'effectuer une action qui nécessite des privilèges. Pour effectuer cette action, l'utilisateur principal doit s'authentifier. Une application tente d'effectuer une action qui nécessite des privilèges. Pour effectuer cette action, il est nécessaire de s'authentifier. S'authentifier La demande d'authentification a été rejetée par l'utilisateur Cliquez pour modifier %s Cliquez pour ouvrir %s Sélectionner un utilisateur... Votre tentative d'authentification a échoué. Veuillez essayer de nouveau. S'_authentifier Mot de _passe pour %s : Mot de _passe : 