��    �      4  �   L
      �     �     �     �     �     	  �        �     �     �     �            	   0     :  	   Q     [     `     s          �     �     �  	   �     �     �     �     �          "     2     ;  _   L  _   �  [        h     �      �  *   �     �     �            :   (     c     i  !   x     �  4   �     �     �     �               '     .     3     ;     C     K  	   P  
   Z     e     i  1   v     �  4   �     �       
        &     8  5   >     t     �     �  )   �  *   �     �                     '     B     \     d     p     }     �  /   �  (   �  E   �  #   7     [  
   `     k     �     �     �     �     �     �     �     �  
     #        <     Q     f  F   r     �     �     �     �     �       :     8   T  M   �  O   �     +  <   B  :     N   �  c  	     m  h   �     �       (   +      T     u  -   �      �  %   �  %   �  $   %  !   J  5   l  &   �     �  
   �     �     �  	               	        $     +     1     B     J     P  
   V     a     t     �  
   �     �     �     �     �  �  �     �!     �!     �!     �!     �!    �!     �"     �"     �"  	   �"     �"  $   #     8#     J#     h#     t#     z#     �#  +   �#     �#     �#     
$     ($     5$     >$     ^$     j$  2   |$     �$     �$     �$  a   �$  a   P%  \   �%  "   &     2&  ?   >&  6   ~&  #   �&  #   �&     �&     '  O   -'     }'     �'  $   �'     �'  L   �'     (     (     +(     G(     Y(     m(     y(  
   (     �(     �(     �(     �(     �(     �(     �(  K   �(  :   6)  9   q)     �)     �)     �)      �)     *  [   *     m*  "   {*     �*  5   �*  6   �*     +     #+     ++     D+  (   [+  (   �+     �+     �+     �+     �+     ,  7   ,,  3   d,  f   �,  >   �,     >-     J-  !   ^-  +   �-  '   �-     �-  #   �-     .  
   ".     -.     F.     b.  C   x.     �.  $   �.  "   �.  �   /  #   �/     �/     �/     �/  "   �/     0  =    0  8   ^0  �   �0  Y   1  !   x1  R   �1  Q   �1  Y   ?2  }  �2     5  v   35  =   �5  %   �5  (   6  7   76  (   o6  J   �6  %   �6  /   	7  /   97  -   i7  *   �7  <   �7  (   �7  $   (8  
   M8     X8     a8  	   i8  	   s8     }8  	   �8  	   �8     �8     �8     �8     �8     �8     �8     �8     9     !9     .9     I9     g9  +   w9  �  �9     �   �       @       t   �   3                 u   U   7      w   �           �   I   z       �   ~   ;   _   >          �       P   Q   H       �              �   �   e         N       #   E   }   K   F   W   �   `       X              v   �   �   \   *   =   x   �           ?         �                  �   5   j   <   �       r          B         T   g               0   :               9   �   2   "       ]          A       1   +      �       �           �   �   [   q       k           o   ,   /          -          C   c   n   f   Z              i   y   �   8          	   
      D   M          S   4              h   G       '   �   �   �       s   )              J   &   ^      �   O   �   L           %         b   !   V   |      m   $       a       6   �   �   �   Y                       �   �   p   .   l   �       R   d      (   �       {   �    %d dpi %d dpi (default) %d dpi (draft) %d dpi (high resolution) 4×6 A really easy way to scan both documents and photos. You can crop out the bad parts of a photo and rotate it if it is the wrong way round. You can print your scans, export them to pdf, or save them in a range of image formats. A_4 A_5 A_6 About About Simple Scan Additional software needed All Files All Pages From _Feeder Automatic Back Brightness of scan Brightness: Change _Scanner Combine sides Combine sides (reverse) Contrast of scan Contrast: Crop Crop the selected page Darker Device to scan from Directory to save files to Discard Changes Document Document Scanner Drivers for this are available on the <a href="http://samsung.com/support">Samsung website</a>. Drivers for this are available on the <a href="http://support.brother.com">Brother website</a>. Drivers for this are available on the <a href="http://support.epson.com">Epson website</a>. Drivers installed successfully! Email... Error communicating with scanner Failed to install drivers (error code %d). Failed to install drivers. Failed to save file Failed to scan File format: Fix PDF files generated with older versions of Simple Scan Front Front and Back Height of paper in tenths of a mm Help If you don't save, changes will be permanently lost. Image Files Install drivers Installing drivers... JPEG (compressed) Keep unchanged Le_gal Less Lighter Maximum Minimum More Move Left Move Right New New Document No scanners available.  Please connect a scanner. No scanners detected Once installed you will need to restart Simple Scan. PDF (multi-page document) PNG (lossless) Page Size: Page side to scan Photo Please check your scanner is connected and powered on Preferences Print debugging messages Print... Quality value to use for JPEG compression Quality value to use for JPEG compression. Quality: Quit Quit without Saving Reorder Pages Resolution for photo scans Resolution for text scans Reverse Rotate Left Rotate Right Rotate _Left Rotate _Right Rotate the page to the left (counter-clockwise) Rotate the page to the right (clockwise) Run '%s --help' to see a full list of available command line options. SANE device to acquire images from. Save Save As... Save current document? Save document before quitting? Save document to a file Saving document... Saving page %d out of %d Sc_an Scan Scan Documents Scan S_ource: Scan Side: Scan a single page from the scanner Scanned Document.pdf Show release version Simple Scan Simple Scan uses the SANE framework to support most existing scanners. Simple document scanning tool Single _Page Start a new document Stop Stop the current scan Text The brightness adjustment from -100 to 100 (0 being none). The contrast adjustment from -100 to 100 (0 being none). The directory to save files to. Defaults to the documents directory if unset. The height of the paper in tenths of a mm (or 0 for automatic paper detection). The page side to scan. The resolution in dots-per-inch to use when scanning photos. The resolution in dots-per-inch to use when scanning text. The width of the paper in tenths of a mm (or 0 for automatic paper detection). This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. Type of document being scanned Type of document being scanned. This setting decides on the scan resolution, colors and post-processing. Unable to connect to scanner Unable to open help file Unable to open image preview application Unable to save image for preview Unable to start scan Username and password required to access '%s' Width of paper in tenths of a mm You appear to have a Brother scanner. You appear to have a Samsung scanner. You appear to have an Epson scanner. You appear to have an HP scanner. You need to install driver software for your scanner. You need to install the %s package(s). [DEVICE...] - Scanning utility _Authorize _Cancel _Close _Contents _Crop _Custom _Document _Email _Help _Install Drivers _Letter _None _Page _Password: _Photo Resolution: _Rotate Crop _Save _Stop Scan _Text Resolution: _Username for resource: scan;scanner;flatbed;adf; translator-credits Project-Id-Version: simple-scan
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-03-20 21:13+0000
PO-Revision-Date: 2015-12-16 19:01+0000
Last-Translator: Jean-Marc <Unknown>
Language-Team: French <fr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:48+0000
X-Generator: Launchpad (build 18115)
 %d ppp %d ppp (par défaut) %d ppp (brouillon) %d ppp (haute résolution) 4 × 6 Une façon très simple de numériser à la fois documents et photos. Vous pouvez recadrer une photo et la faire pivoter si elle est dans le mauvais sens. Vous pouvez imprimer vos numérisations, les exporter au format pdf ou les enregistrer dans d'autres formats d'image. A_4 A_5 A_6 À propos À propos de Simple Scan Logiciel supplémentaire nécessaire Tous les fichiers Toutes les pages du _chargeur Automatique Verso Luminosité de la numérisation Luminosité : Changer de _périphérique de numérisation Associer les côtés Associer les côtés (inverser) Contraste de la numérisation Contraste : Recadrer Recadrer la page sélectionnée Plus foncé Numériser depuis Dossier dans lequel les fichiers sont enregistrés Abandonner les modifications Document Numériseur de documents Les pilotes sont disponibles sur le <a href="http://samsung.com/support">site Web de Samsung</a>. Les pilotes sont disponibles sur le <a href="http://support.brother.com">site Web de Brother</a>. Les pilotes sont disponibles sur le <a href="http://support.epson.com">site web d'Epson</a>. Pilotes installés avec succès ! Courriel... Erreur de communication avec le périphérique de numérisation Impossible d'installer les pilotes (code d'erreur %d). Impossible d'installer les pilotes. Impossible d'enregistrer le fichier Échec de la numérisation Format du fichier : Corriger les fichiers PDF générés avec les anciennes versions de Simple Scan Recto Recto-verso Hauteur du papier en dixièmes de mm Aide Si vous n'enregistrez pas, les modifications seront définitivement perdues. Fichiers image Installer les pilotes Installation des pilotes... JPEG (compressé) Maintenir inchangé _Legal (US) Moins Plus clair Maximum Minimum Plus Déplacer vers la gauche Déplacer vers la droite Nouveau Nouveau document Aucun périphérique de numérisation disponible. Veuillez en connecter un. Aucun périphérique de numérisation n'a été détecté. Une fois installés, vous devrez redémarrer Simple Scan. PDF (document multipage) PNG (sans perte) Taille de la page : Face(s) de la page à numériser Photo Veuillez vérifier que votre périphérique de numérisation est connecté et sous tension. Préférences Afficher les messages de débogage Imprimer... Taux de qualité à utiliser pour la compression JPEG Taux de qualité à utiliser pour la compression JPEG. Qualité : Quitter Quitter sans enregistrer Réorganiser les pages Résolution des numérisations de photos Résolution des numérisations de textes Inverser Faire pivoter vers la gauche Faire pivoter vers la droite Faire pivoter vers la _gauche Faire pivoter vers la _droite Faire pivoter la page vers la gauche (sens antihoraire) Faire pivoter la page vers la droite (sens horaire) Exécutez « %s --help » pour voir la liste complète des options disponibles en ligne de commande. Périphérique SANE à utiliser pour l'acquisition des images. Enregistrer Enregistrer sous... Enregistrer le document actuel ? Enregistrer le document avant de quitter ? Enregistrer le document dans un fichier Enregistrement du document... Enregistrement de la page %d sur %d Nu_mériser Numériser Numériser des documents S_ource de numérisation : Face à numériser : Numériser une seule page depuis le périphérique de numérisation Document numérisé.pdf Afficher les informations de version Outil de numérisation Simple Scan Simple Scan utilise la structure logicielle de SANE pour prendre en charge la plupart des périphériques de numérisation existants. Numériser simplement vos documents _Page unique Créer un nouveau document Arrêter Arrêter la numérisation en cours Texte L'ajustement de la luminosité de -100 à 100 (0 pour aucun). L'ajustement du contraste de -100 à 100 (0 pour aucun). Le dossier dans lequel les fichiers sont enregistrés. S'il n'est pas défini, le dossier « Documents » sera utilisé par défaut. La hauteur du papier en dixièmes de mm (ou 0 pour une détection automatique du papier). La face de la page à numériser. La résolution en points par pouce à utiliser lors de la numérisation de photos. La résolution en points par pouce à utiliser lors de la numérisation de texte. La largeur du papier en dixièmes de mm (ou 0 pour une détection automatique du papier). Ce programme est libre, vous pouvez le redistribuer et/ou le modifier
selon les termes de la licence publique générale (GPL) GNU publiée
par la Free Software Foundation, dans sa version 3 ou supérieure
(selon votre choix).

Ce programme est distribué dans l'espoir d'être utile, mais
SANS AUCUNE GARANTIE, y compris les garanties de 
COMMERCIALISATION ou d'ADAPTATION À UN BUT SPÉCIFIQUE.
Consultez la licence publique générale GNU pour plus de détails.

Vous devez avoir reçu une copie de la licence publique générale GNU
en même temps que ce programme ; si ce n'est pas le cas, consultez
<http://www.gnu.org/licenses/>. Type de document numérisé Type de document numérisé. Ce réglage définit la résolution de numérisation, les couleurs et le post-traitement. Impossible de se connecter au périphérique de numérisation Impossible d'ouvrir le fichier d'aide Impossible d'ouvrir l'aperçu de l'image Impossible d'enregistrer l'image pour la prévisualiser Impossible de démarrer la numérisation Nom d'utilisateur et mot de passe nécessaires pour accéder à « %s » Largeur du papier en dixièmes de mm. Il semblerait que vous ayez un scanner Brother. Il semblerait que vous ayez un scanner Samsung. Il semblerait que vous ayez un scanner Epson. Il semblerait que vous ayez un scanner HP. Vous devez installer le logiciel du pilote de votre scanner. Vous devez installer le(s) paquet(s) %s. [DEVICE...] - Outil de numérisation _Autoriser _Annuler _Fermer _Contenus Re_cadrer _Personnalisé _Document _Courriel _Aide _Installer les pilotes _Letter (US) Aucu_n _Page _Mot de passe : Résolution de la _photo : Faire pivote_r la sélection _Enregistrer _Arrêter la numérisation Résolution pour le _texte : _Utilisateur : numériser;scanner;numériseur;à plat;adf; Launchpad Contributions:
  65GYgzf https://launchpad.net/~65gygzf
  Alexandre Franke https://launchpad.net/~afranke
  Alexandre Pliarchopoulos https://launchpad.net/~al-pliar
  Anne https://launchpad.net/~anneonyme017
  Antoine Pernot https://launchpad.net/~antoinepernot
  Bruno Patri https://launchpad.net/~bruno666-deactivatedaccount
  Christophe Herot https://launchpad.net/~tictoc-free
  Darknautilus https://launchpad.net/~darknautilus
  David Bosman https://launchpad.net/~david-bosman
  Emmanuel Sunyer https://launchpad.net/~esunyer
  Fabien Lusseau https://launchpad.net/~fabien-beosfrance
  Florent Thévenet https://launchpad.net/~florent-thevenet
  François Tissandier https://launchpad.net/~baloo
  Grégoire Seux https://launchpad.net/~kamaradclimber
  Guillaume Lanquepin-Chesnais https://launchpad.net/~guyomel
  Jean-Marc https://launchpad.net/~m-balthazar
  Jörg BUCHMANN https://launchpad.net/~jorg-buchmann
  Kaïs Bejaoui https://launchpad.net/~kais
  Manuel Berrocal https://launchpad.net/~manu-berrocal
  Nicolas Delvaux https://launchpad.net/~malizor
  Olivier Febwin https://launchpad.net/~febcrash
  Ozouli.SaLeH https://launchpad.net/~ouzouli
  Pascal Maugendre https://launchpad.net/~pmaugendre
  Paul Forget https://launchpad.net/~paul92
  Pierre Slamich https://launchpad.net/~pierre-slamich
  RemiJ https://launchpad.net/~remij
  Robert Ancell https://launchpad.net/~robert-ancell
  Simon THOBY https://launchpad.net/~simonthoby-deactivatedaccount
  Stanislas Michalak https://launchpad.net/~stanislas-michalak
  Sylvie Gallet https://launchpad.net/~sylvie-gallet
  Tankypon https://launchpad.net/~tankypon
  Xavier Verne https://launchpad.net/~xavier-verne
  YannUbuntu https://launchpad.net/~yannubuntu
  beudbeud https://launchpad.net/~beudbeud
  blackbg https://launchpad.net/~corentin-cadiou
  loic08 https://launchpad.net/~loic08
  londumas https://launchpad.net/~helion331990
  torglut https://launchpad.net/~torglut 