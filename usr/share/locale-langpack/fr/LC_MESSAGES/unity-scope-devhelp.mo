��          \      �       �      �      �      �   ;   �      !  �   5       �  ,     �     �     �  F   �     A  �   V     0                                       Devhelp Search Devhelp Show Sorry, there are no Devhelp results that match your search. Technical Documents This is an Ubuntu search plugin that enables information from Devhelp to be searched and displayed in the Dash underneath the Code header. If you do not wish to search this content source, you can disable this search plugin. devhelp;dev;help;doc; Project-Id-Version: unity-scope-devhelp
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-28 09:22+0000
PO-Revision-Date: 2014-04-10 11:02+0000
Last-Translator: Olivier Febwin <febwin@free.fr>
Language-Team: French <fr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:23+0000
X-Generator: Launchpad (build 18115)
 Devhelp Recherche Devhelp Afficher Désolé, aucun résultat de Devhelp ne correspond à votre recherche. Documents Techniques Il s'agit d'un plugin de recherche qui permet la recherche d'informations de Devhelp et de les afficher dans le tableau de bord. Si vous ne souhaitez pas utiliser cette source de recherche, vous pouvez la désactiver. devhelp;dev;aide;doc; 