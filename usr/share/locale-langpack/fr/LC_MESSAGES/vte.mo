��          �      \      �  $   �  )   �        6   9     p  /   �  (   �     �  +   �     $     B  $   ^  -   �  +   �  7   �       &   -  /   T     �    �  :   �  @   �        K   ?     �  D   �  E   �  /   )  W   Y  0   �  ;   �  8   	  G   W	  1   �	  Y   �	     +
  *   G
  I   r
     �
                                  	                      
                                           Attempt to set invalid NRC map '%c'. Attempt to set invalid wide NRC map '%c'. Could not open console.
 Could not parse the geometry spec passed to --geometry Duplicate (%s/%s)! Error (%s) converting data for child, dropping. Error compiling regular expression "%s". Error creating signal pipe. Error reading PTY size, using defaults: %s. Error reading from child: %s. Error setting PTY size: %s. Got unexpected (key?) sequence `%s'. No handler for control sequence `%s' defined. Unable to convert characters from %s to %s. Unable to send data to child, invalid charset convertor Unknown pixel mode %d.
 Unrecognized identified coding system. _vte_conv_open() failed setting word characters can not run %s Project-Id-Version: vte 0.14.1
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=vte&component=general
POT-Creation-Date: 2007-10-06 10:12+0000
PO-Revision-Date: 2016-02-08 00:36+0000
Last-Translator: Stéphane Raimbault <stephane.raimbault@gmail.com>
Language-Team: GNOME French Team <gnomefr@traduc.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:07+0000
X-Generator: Launchpad (build 18115)
X-Poedit-Language: French
 Tentative de définir une carte NRC « %c » non valide. Tentative de définir une carte NRC large « %c » non valide. Impossible d'ouvrir la console.
 Impossible d'analyser les arguments de géométrie indiqués par --geometry Doublon (%s/%s) ! Erreur (%s) lors de la conversion de données pour le fils, abandon. Erreur lors de la compilation de l'expression régulière « %s ». Erreur lors de la création du tube de signaux. Erreur lors de la lecture de la taille PTY, utilisation de la valeur par défaut : %s. Erreur lors de la lecture du fils : « %s ». Erreur lors du paramétrage de la taille PTY : « %s ». A obtenu une séquence (touche ?) inattendue « %s ». Aucune manipulation définie pour la séquence de contrôle « %s ». Impossible de convertir les caractères %s en %s. Impossible d'envoyer les données au fils, convertisseur de jeux de caractères incorrect Mode de pixels %d inconnu.
 Système de codage identifié non reconnu. _vte_conv_open() a échoué lors de la définition des caractères du mot Impossible d'exécuter %s 