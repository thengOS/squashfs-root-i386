��          |      �             !     5  -   E  '   s  3   �  ,   �     �  �        �     �     �  �       �     �  5   �  <     f   S  @   �  %   �  �   !     �          -                            
      	                         Current folder name New folder name Note that existing content will not be moved. There was an error updating the folders Update common folders names to match current locale Update standard folders to current language? User folders update You have logged in in a new language. You can automatically update the names of some standard folders in your home folder to match this language. The update would change the following folders: _Don't ask me this again _Keep Old Names _Update Names Project-Id-Version: xdg-user-dirs-gtk HEAD
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-04 03:00+0000
PO-Revision-Date: 2008-02-20 11:13+0000
Last-Translator: Claude Paroz <claude@2xlibre.net>
Language-Team: GNOME French team <gnomefr@traduc.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:21+0000
X-Generator: Launchpad (build 18115)
 Nom de dossier actuel Nouveau nom de dossier Sachez que le contenu existant ne sera pas déplacé. Une erreur est survenue lors de la mise à jour des dossiers Mettre à jour les noms des dossiers courants pour correspondre à la langue (« locale ») actuelle Mettre à jour les dossiers standards vers la langue actuelle ? Mise à jour des dossiers utilisateur Vous vous êtes connecté avec une nouvelle langue. Vous pouvez mettre à jour les dossiers standards de votre dossier personnel dans cette nouvelle langue. La mise à jour modifierait les dossiers suivants : _Ne plus me poser la question _Conserver les anciens noms _Mettre à jour les noms 