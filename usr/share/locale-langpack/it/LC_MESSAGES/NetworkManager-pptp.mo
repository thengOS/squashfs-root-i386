��    I      d  a   �      0     1     G     \     r     ~     �     �     �     �     �  n   �     ]     y     �  +   �  J   �  E   +  �   q  r   �  k   n	     �	     �	  5   �	  "   &
  <   I
     �
  #   �
     �
  )   �
     �
  <      3   =      q     �     �     �      �      �     �  
        "     @     P  �   g            ;   3  -   o  	   �  (   �  u   �  h   F     �  `   �     '     5  %   Q     w  
   �  D   �  	   �  
   �  6   �  6   ,  -   c     �     �  �   �  6   E  ;   |  &   �     �  �  �     �          *  
   @     K     [     h     y     �  !   �  �   �  $   U  (   z  "   �  /   �  X   �  S   O  �   �  {   *  g   �          !  ;   &  /   b  P   �  $   �  &        /  *   ;     f  M   j  E   �  #   �     "     )     2  #   H  &   l  "   �     �  &   �     �     �  �        �     �  =     (   D  	   m  (   w  x   �  j        �  c   �       -     )   A     k     �  P   �  	   �     �  F   �  C   D  4   �     �  #   �  �   �  S   ~   X   �   -   +!  (   Y!     *          %                          E      '   4   +       0          :   ;   1          &      I                         F      $   B   (   9      7   @   G       8   C   
      ?   2       3                   ,   )   5   H             .       =       #       A   /             "                  6              D   <   !   >         	   -               128-bit (most secure) 40-bit (less secure) <b>Authentication</b> <b>Echo</b> <b>General</b> <b>Misc</b> <b>Optional</b> <b>Security and Compression</b> Ad_vanced... All Available (Default) Allow MPPE to use stateful mode. Stateless mode is still attempted first.
config: mppe-stateful (when checked) Allow _BSD data compression Allow _Deflate data compression Allow st_ateful encryption Allow the following authentication methods: Allow/disable BSD-Compress compression.
config: nobsdcomp (when unchecked) Allow/disable Deflate compression.
config: nodeflate (when unchecked) Allow/disable Van Jacobson style TCP/IP header compression in both the transmit and the receive directions.
config: novj (when unchecked) Allow/disable authentication methods.
config: refuse-pap, refuse-chap, refuse-mschap, refuse-mschap-v2, refuse-eap Append the domain name <domain> to the local host name for authentication purposes.
config: domain <domain> Authenticate VPN CHAP Compatible with Microsoft and other PPTP VPN servers. Could not find pptp client binary. Could not find secrets (connection invalid, no vpn setting). Could not find the pppd binary. D-Bus name to use for this instance Default Don't quit when VPN connection terminates EAP Enable custom index for ppp<n> device name.
config: unit <n> Enable verbose debug logging (may expose passwords) Invalid or missing PPTP gateway. MSCHAP MSCHAPv2 Missing VPN gateway. Missing or invalid VPN password. Missing or invalid VPN username. Missing required option '%s'. NT Domain: No VPN configuration options. No VPN secrets! No cached credentials. Note: MPPE encryption is only available with MSCHAP authentication methods. To enable this checkbox, select one or more of the MSCHAP authentication methods: MSCHAP or MSCHAPv2. PAP PPTP Advanced Options PPTP server IP or name.
config: the first parameter of pptp Password passed to PPTP when prompted for it. Password: Point-to-Point Tunneling Protocol (PPTP) Require the use of MPPE, with 40/128-bit encryption or all.
config: require-mppe, require-mppe-128 or require-mppe-40 Send LCP echo-requests to find out whether peer is alive.
config: lcp-echo-failure and lcp-echo-interval Send PPP _echo packets Set the name used for authenticating the local system to the peer to <name>.
config: user <name> Show password Use TCP _header compression Use _Point-to-Point encryption (MPPE) Use custom _unit number: User name: You need to authenticate to access the Virtual Private Network '%s'. _Gateway: _Security: couldn't convert PPTP VPN gateway IP address '%s' (%d) couldn't look up PPTP VPN gateway IP address '%s' (%d) invalid boolean property '%s' (not yes or no) invalid gateway '%s' invalid integer property '%s' nm-pptp-service provides integrated PPTP VPN capability (compatible with Microsoft and other implementations) to NetworkManager. no usable addresses returned for PPTP VPN gateway '%s' no usable addresses returned for PPTP VPN gateway '%s' (%d) property '%s' invalid or not supported unhandled property '%s' type %s Project-Id-Version: network-manager-pptp 0.8.x
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=NetworkManager&component=VPN: pptp
POT-Creation-Date: 2016-04-14 07:51+0000
PO-Revision-Date: 2012-04-14 15:09+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:03+0000
X-Generator: Launchpad (build 18115)
 128-bit (più sicuro) 40-bit (meno sicuro) <b>Autenticazione</b> <b>Eco</b> <b>Generale</b> <b>Varie</b> <b>Opzionali</b> <b>Sicurezza e compressione</b> A_vanzate... Tutti i disponibili (predefinito) Consente MPPE di utilizzare la modalità stateful. La modalità sateless viene provata per prima.
config: mppe-stateful (quando selezionata) Consentire la compressione dati _BSD Consentire la compressione dati _Deflate Consentire la cifratura con st_ato Consentire i seguenti metodi di autenticazione: Abilita/Disabilita compressione BSD-Compress.
config: nobsdcomp (quando non selezionata) Abilita/Disabilita compressione Deflate.
config: nodeflate (quando non selezionata) Abilita/Disabilita compressione header TCP/IP in stile Van Jacobson in ricezione e trasmissione.
config: novj (quando non selezionata) Abilita/Disabilita i metodi di autenticazione.
config: refuse-pap, refuse-chap, refuse-mschap, refuse-mschap-v2, refuse-eap Aggiunge il nome del dominio <dominio> al nome host locale per autenticazione.
config: domain <dominio> Autenticazione VPN CHAP Compatibile con i server Microsoft e altri server VPN PPTP. Impossibile trovare il binario del client pptp. Impossibile trovare segreti (connessione non valida o nessuna impostazione VPN). Impossibile trovare il binario pppd. Nome D-Bus da usare per questa istanza Predefinito Non esce quando la connessione VPN termina EAP Abilita indice personalizzato per il nome del device ppp<n>.
config: unit <n> Abilita output prolisso per il debug (potrebbe visualizzare password) Gateway PPTP non valido o mancante. MSCHAP MSCHAPv2 Gateway VPN mancante. Password VPN mancante o non valida. Nome utente VPN mancante o non valido. Opzioni richiesta «%s» mancante. Dominio NT: Nessuna opzione di configurazione VPN. Nessun segreto VPN. Nessuna credenziale salvata. Nota: la cifratura MPPE è disponibile sono con i metodi di autenticazione MSCHAP. Per abilitare questa casella di spunta, selezionare uno o più tra i metodi di autenticazione MSCHAP, come MSCHAP or MSCHAPv2. PAP Opzioni avanzate PPTP IP del server PPTP o nome.
config: il primo parametro di pptp Password passata a PPTP quando richiesta Password: Point-to-Point Tunneling Protocol (PPTP) Richiede l'uso di MPPE con cifratura a 40/128-bit o tutte.
config: require-mppe, require-mppe-128 oppure require-mppe-40 Invia richieste echo LCP per verificare se il nodo è attivo.
config: lcp-echo-failure e lcp-echo-interval Inviare pacchetti _eco PPP Imposta il nome usato per l'autenticazione del sistema locale al nodo a <nome>.
config: user <nome> Mostra password Usare la compressione delle _intestazioni TCP Usare la cifratura _Point-to-Point (MPPE) Usare _unità personalizzata: Nome utente: È necessario autenticarsi per accedere alla VPN (rete privata virtuale) «%s». _Gateway: _Sicurezza: impossibile convertire l'indirizzo IP «%s» del gateway VPN PPTP (%d) impossibile trovare l'indirizzo IP «%s» del gateway VPN PPTP (%d) proprietà booleana «%s» non valida (non yes o no) gateway «%s» non valido proprietà intera «%s» non valida nm-pptp-service fornisce funzionalità VPN PPTP  integrate (compatibile con Microsoft e altre implementazioni) per NetworkManager. non è stato restituito alcun indirizzo utilizzabile per il gateway VPN PPTP «%s» non è stato restituito alcun indirizzo utilizzabile per il gateway VPN PPTP «%s» (%d) proprietà «%s» non valida o non supportata proprietà «%s» di tipo %s non gestita 