��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  T   �  5   �  <   1  3   n     �     �     �  *   �  \         }               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
POT-Creation-Date: 2013-01-17 09:48-0500
PO-Revision-Date: 2014-01-18 04:22+0000
Last-Translator: Luca Ferretti <elle.uca@gmail.com>
Language-Team: Italian (http://www.transifex.com/projects/p/freedesktop/language/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:01+0000
X-Generator: Launchpad (build 18115)
Language: it
 È necessario autenticarsi per cambiare la configurazione della schermata di accesso È necessario autenticarsi per cambiare i dati utente È necessario autenticarsi per cambiare i propri dati utente Cambia la configurazione della schermata di accesso Cambia i propri dati utente Abilita il codice di debug Gestisci gli account utente Stampa le informazioni di versione ed esce Fornisce interfacce D-Bus per interrogare e manipolare
le informazioni degli account utente. Sostituisce l'istanza esistente 