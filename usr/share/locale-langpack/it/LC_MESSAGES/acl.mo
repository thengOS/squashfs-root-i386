��    5      �  G   l      �     �     �     �     �     �  )   �  )   %     O  �   d  7  0  �  h  B   J
  l  �
  �   �  Z     '   �  '        *  $   H     m     �  &   �  2   �  3   �  /   2  /   b  =   �     �  %   �  2        D  $   \  &   �  +   �  '   �  ,   �  &   )  '   P  *   x  +   �     �     �     �          #     :  &   X          �     �     �     �  �  �     �     �     �      �     �  2     2   @     s  �   �  ^  `    �  3   �  `    �   h  e   �  >   a  >   �      �  )         *  +   C  5   o  9   �  :   �  =     8   X  M   �     �  +   �  >   '     f  )   ~  4   �  :   �  5      ;   N   5   �   6   �   4   �   5   ,!     b!     !     �!     �!     �!     �!  /   �!     +"     1"     ="  !   U"      w"                 	              
   5                                 -           !          1      &             *      3   +      $                    )       "              %   #         ,                         '   /   .         4          0   2   (    	%s -B pathname...
 	%s -D pathname...
 	%s -R pathname...
 	%s -b acl dacl pathname...
 	%s -d dacl pathname...
 	%s -l pathname...	[not IRIX compatible]
 	%s -r pathname...	[not IRIX compatible]
 	%s acl pathname...
       --set=acl           set the ACL of file(s), replacing the current ACL
      --set-file=file     read ACL entries to set from file
      --mask              do recalculate the effective rights mask
   -R, --recursive         recurse into subdirectories
  -L, --logical           logical walk, follow symbolic links
  -P, --physical          physical walk, do not follow symbolic links
      --restore=file      restore ACLs (inverse of `getfacl -R')
      --test              test mode (ACLs are not modified)
   -a,  --access           display the file access control list only
  -d, --default           display the default access control list only
  -c, --omit-header       do not display the comment header
  -e, --all-effective     print all effective rights
  -E, --no-effective      print no effective rights
  -s, --skip-base         skip files that only have the base entries
  -R, --recursive         recurse into subdirectories
  -L, --logical           logical walk, follow symbolic links
  -P, --physical          physical walk, do not follow symbolic links
  -t, --tabular           use tabular output format
  -n, --numeric           print numeric user/group identifiers
  -p, --absolute-names    don't strip leading '/' in pathnames
   -d, --default           display the default access control list
   -m, --modify=acl        modify the current ACL(s) of file(s)
  -M, --modify-file=file  read ACL entries to modify from file
  -x, --remove=acl        remove entries from the ACL(s) of file(s)
  -X, --remove-file=file  read ACL entries to remove from file
  -b, --remove-all        remove all extended ACL entries
  -k, --remove-default    remove the default ACL
   -n, --no-mask           don't recalculate the effective rights mask
  -d, --default           operations apply to the default ACL
   -v, --version           print version and exit
  -h, --help              this help text
 %s %s -- get file access control lists
 %s %s -- set file access control lists
 %s: %s in line %d of file %s
 %s: %s in line %d of standard input
 %s: %s: %s in line %d
 %s: %s: Cannot change mode: %s
 %s: %s: Cannot change owner/group: %s
 %s: %s: Malformed access ACL `%s': %s at entry %d
 %s: %s: Malformed default ACL `%s': %s at entry %d
 %s: %s: No filename found in line %d, aborting
 %s: %s: Only directories can have default ACLs
 %s: No filename found in line %d of standard input, aborting
 %s: Option -%c incomplete
 %s: Option -%c: %s near character %d
 %s: Removing leading '/' from absolute path names
 %s: Standard input: %s
 %s: access ACL '%s': %s at entry %d
 %s: cannot get access ACL on '%s': %s
 %s: cannot get access ACL text on '%s': %s
 %s: cannot get default ACL on '%s': %s
 %s: cannot get default ACL text on '%s': %s
 %s: cannot set access acl on "%s": %s
 %s: cannot set default acl on "%s": %s
 %s: error removing access acl on "%s": %s
 %s: error removing default acl on "%s": %s
 %s: malloc failed: %s
 %s: opendir failed: %s
 Duplicate entries Invalid entry type Missing or wrong entry Multiple entries of same type Try `%s --help' for more information.
 Usage:
 Usage: %s %s
 Usage: %s [-%s] file ...
 preserving permissions for %s setting permissions for %s Project-Id-Version: acl
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-07 11:10+0000
PO-Revision-Date: 2010-04-18 18:17+0000
Last-Translator: Luca Ferretti <elle.uca@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:54+0000
X-Generator: Launchpad (build 18115)
 	%s -B NOMEPERCORSO...
 	%s -D NOMEPERCORSO...
 	%s -R NOMEPERCORSO...
 	%s -b ACL DACL NOMEPERCORSO...
 	%s -d DACL NOMEPERCORSO...
 	%s -l NOMEPERCORSO...	[non compatibile con IRIX]
 	%s -R NOMEPERCORSO...	[non compatibile con IRIX]
 	%s ACL NOMEPERCORSO...
       --set=acl           imposta l'ACL dei file, sostituendo l'ACL corrente
      --set-file=FILE     legge le voci ACL da impostare da FILE
      --mask              ricalcola l'effettiva maschera di permessi
   -R, --recursive         opera ricorsivamente nelle sottodirectory
  -L, --logical           prosegue seguendo i collegamenti simbolici
  -P, --physical          prosegue senza seguire i collegamenti simbolici
      --restore=file      ripristina le ACL (opposto a `getfacl -R')
      --test              modalità test (le ACL non sono modificate)
   -a,  --access           mostra solo l'elenco di controllo accesso del file
  -d, --default           mostra solo l'elenco di controllo accesso predefinito
  -c, --omit-header       non mostra l'intestazione di commento
  -e, --all-effective     stampa tutti i diritti effettivi
  -E, --no-effective      non stampa alcun diritto effettivo
  -s, --skip-base         ignora i file che hanno sono le voci base
  -R, --recursive         ricorsivo nelle sottodirectory
  -L, --logical           segue i collegamenti simbolici
  -P, --physical          non segue i collegamenti simbolici
  -t, --tabular           usa formato di output con tabulazioni
  -n, --numeric           stampa identificatori numerici per utente/gruppo
  -p, --absolute-names    non rimuove i / iniziali nei percorsi
   -d, --default           mostra l'ACL predefinita
   -m, --modify=ACL        modifica l'ACL dei file
  -M, --modify-file=FILE  legge le voci ACL da modificare da FILE
  -x, --remove=ACL        rimuove le voci dall'ACL dei file
  -X, --remove-file=FILE  legge le voci ACL da rimuovere da FILE
  -b, --remove-all        rimuove tutte le voci ACL estese
  -k, --remove-default    rimuove l'ACL predefinita
   -n, --no-mask           non ricalcola l'effettiva maschera di permessi
  -d, --default           le operazioni si applicano all'ACL predefinita
   -v, --version           stampa la versione ed esce
  -h, --help              questo testo di aiuto
 %s %s -- ottiene le ACL (lista di controllo accesso) dei file
 %s %s -- imposta la ACL (lista di controllo accesso) del file
 %s: %s alla riga %d del file %s
 %s: %s alla riga %d dello standard input
 %s: %s: %s alla riga %d
 %s: %s: impossibile cambiare modalità: %s
 %s: %s: impossibile cambiare proprietario/gruppo: %s
 %s: %s: ACL di accesso "%s" mal formata: %s alla voce %d
 %s: %s: ACL predefinita "%s" mal formata: %s alla voce %d
 %s: %s: nessun nome di file trovato alla riga %d, interrotto
 %s: %s: solo le directory possono avere ACL predefinite
 %s: nessun nome di file trovato alla riga %d o su standard input, interrotto
 %s: opzione -%c incompleta
 %s: option -%c: %s accanto al carattere %d
 %s: rimozione di '/' dall'inizio di nomi di percorso assoluti
 %s: standard input: %s
 %s: ACL di accesso "%s": %s alla voce %d
 %s: impossibile ottenere ACL di accesso su "%s": %s
 %s: impossibile ottenere testo ACL di accesso su "%s": %s
 %s: impossibile ottenere ACL predefinita su "%s": %s
 %s: impossibile ottenere testo ACL predefinita su "%s": %s
 %s: impossibile impostare acl di accesso su "%s": %s
 %s: impossibile impostare acl predefinita su "%s": %s
 %s: errore nel rimuovere acl di accesso su "%s": %s
 %s: errore nel rimuovere acl predefinita su "%s": %s
 %s: malloc non riuscito: %s
 %s: opendir non riuscito: %s
 Voci duplicate Tipo di voce non valido Voce mancante o errata Voci multiple dello stesso tipo Provare "%s --help" per maggiori informazioni.
 Uso:
 Uso: %s %s
 Uso: %s [-%s] FILE ...
 preservazione dei permessi per %s impostazioni dei permessi per %s 