��    3     �	  �  L      �  9   �  J   �  '   6  (   ^  $   �  .   �  5   �  ?     6   Q  :   �  :   �     �  )        >  '   \  5   �  -   �  9   �  8   "  )   [  &   �  "   �  "   �     �            .   (  "   W     z  .   �  1   �  #   �       I     /   _  &   �     �     �  	   �     �  &   �        ,   3   *   `   -   �      �   $   �   !   �       !     9!  +   S!     !     �!  )   �!  C   �!  >   �!  "   9"     \"  0   d"  1   �"  2   �"  $   �"     #     8#     O#     n#     v#     |#  !   �#  !   �#  !   �#  !   �#  '   $  
   3$  
   >$  
   I$  
   T$  
   _$  
   j$  
   u$  	   �$  =   �$     �$  4   �$     	%     &%     ,%     ;%     I%  !   a%  %   �%  �   �%     +&     8&     S&     Z&     v&     |&  	   �&     �&  	   �&     �&     �&     �&     �&     '     4'     N'     e'     �'  
   �'     �'     �'     �'     �'     �'  !   �'     (     )(     /(     1(     G(  $   K(     p(     r(     �(     �(     �(     �(     �(     �(     �(     �(  %   �(  "   )     @)     N)     W)     n)     �)  +   �)  &   �)  +   �)     *     *  )    *  6   J*  "   �*     �*  	   �*  
   �*     �*  	   �*     �*     +      !+     B+  -   ^+  &   �+     �+     �+     �+     �+  	   ,  
   ,     ,  
   3,     >,     X,  (   d,     �,     �,     �,     �,  ,   �,     �,  O   -  #   V-  $   z-     �-  5   �-  -   �-  6   #.     Z.     q.  &   �.  6   �.     �.  '   /  )   )/  /   S/  1   �/     �/  )   �/  +   �/  1   *0  4   \0  )   �0     �0     �0  !   �0     1  )   "1     L1     e1  	  m1     �:  �   �:     r;     �;     �;     �;     �;     �;  !   �;  %   �;  %   <  !   7<  =   Y<  #   �<  !   �<     �<     �<     �<  +   =     A=  -   O=  2   }=     �=     �=     �=  !   �=     >     0>     E>     S>  D   i>  $   �>  $   �>     �>     ?     ,?     I?     d?  4   v?  %   �?     �?     �?  /   �?     -@     6@     D@     Y@     f@     u@     �@     �@     �@     �@     �@     �@     �@     A     .A     @A  	   PA     ZA     bA     wA     �A  
   �A      �A     �A     �A     �A      B  +   B     EB     dB  #   uB     �B     �B  ,   �B     �B     C     C      =C  ?   ^C  J   �C     �C     �C     D     !D      &D     GD     eD     yD     �D     �D     �D     �D     �D  +   �D  !   #E     EE     ]E  �  zE  N    G  J   oG  3   �G  #   �G  "   H  *   5H  9   `H  >   �H  9   �H  6   I  6   JI     �I  0   �I     �I  +   �I  B   J  +   UJ  A   �J  B   �J  +   K  &   2K  "   YK  "   |K     �K     �K     �K  3   �K  '   L     =L  +   FL  ?   rL  *   �L     �L  n   �L  <   YM  ,   �M  $   �M     �M     �M     �M  +   N     7N  ,   SN  *   �N  -   �N  %   �N  6   �N  5   6O     lO     �O  8   �O     �O     �O  1   �O  W   #P  V   {P  )   �P     �P  E   Q  A   JQ  O   �Q  %   �Q  "   R      %R  +   FR     rR     zR     �R  %   �R  !   �R      �R     �R  3   S  	   GS  	   QS  	   [S  	   eS  	   oS  	   yS  	   �S     �S  M   �S  	   �S  B   �S  $   1T     VT     bT     nT     |T  *   �T  %   �T  �   �T     {U     �U  
   �U  %   �U     �U     �U  	   �U     �U  
   �U     	V     )V  (   FV  #   oV      �V  !   �V     �V     �V     W     W     +W  #   ;W     _W     eW      }W  '   �W     �W  	   �W     �W     �W     
X  *   X     9X  #   ;X  %   _X     �X     �X     �X     �X     �X     �X     �X  /   �X  &   Y  
   9Y     DY  $   QY     vY     �Y  3   �Y  1   �Y  7   Z     IZ     KZ  7   ]Z  H   �Z  ,   �Z     [     [     %[  1   7[     i[  $   �[  %   �[  '   �[      �[  A   \  :   V\     �\  5   �\  5   �\     ]     ]     #]  0   3]     d]  &   q]     �]  +   �]     �]     �]     �]     �]  2   ^      6^  [   W^  3   �^  ,   �^  *   _  >   ?_  8   ~_  C   �_     �_     `  .   0`  N   _`  ,   �`  :   �`  ?   a  L   Va  J   �a  2   �a  C   !b  =   eb  L   �b  H   �b  C   9c     }c     �c  %   �c  &   �c  6   �c     3d     Od  �	  Ud     �m  �   n      �n  *   
o     5o     9o  	   @o     Jo  .   Oo  -   ~o  -   �o  )   �o  M   p  9   Rp  "   �p     �p     �p     �p  :   �p     )q  8   8q  0   qq     �q  !   �q  1   �q  .   r  $   @r  %   er     �r      �r  X   �r  .   #s  4   Rs  *   �s  1   �s  %   �s  *   
t     5t  I   Qt  ,   �t     �t     �t  ,   �t     &u     3u     @u     \u     ju  #   �u  (   �u  %   �u  !   �u     v     #v     4v     9v  %   Yv     v     �v  	   �v     �v     �v     �v     �v     �v  +   w     /w  $   Ew     jw     {w  9   �w     �w     �w  0   x  '   8x     `x  5   ux  ,   �x     �x  &   �x  &   y  N   /y  X   ~y     �y  $   �y     z     z  !   "z  "   Dz     gz     |z     �z     �z     �z     �z     �z  8   {  *   H{      s{     �{        J      '   �   m   �       �   �         )  7           H      �   �       �   �   �               s       *       2              �   �      �   �     �   	   �   �       �   p   �   S   �   a       �      F                       =       �   b       o   �   &  5   #  �   Z       y   }   �   �       "   B   �         �       ,   �   g   '      �   �   �   %     �               !  1          �   �       �       �   �   ,  w   �             4   �   �   u       �   �   �   �         G   �   �   !       9   f   �             2     �   $  &   .   �   /       �   l   )       �   ?          �       �         �   \   v         >   �       �   n             |              X   %   �   �           �   x       �   �   3  "  �   {           -   �      e   �   W       I   U       �   
  c   �   �           �     �   �   �       �             R   z   D        �   �   �   A              �   �       �   i   �           �   �   �   $   �   �   /          *  �   �   C   V   �   �              �          �       �   �       �   �   �   �   �       �   (     K   L   M   N      P   Q   3   �   +                              �     �   �      (      �             <   j          �   
   :   T   [   8   �   �       �          1     ;       .                    �        q   r       @   �      �       �   ^      �   O   �     0  �           �   +      �   d       k   �              �   h          #           �     6           Y     �   	  �   0   _   �   E           ~       �   ]       �   t   �   `         �   �   -  �    
Some of these may not be available on selected hardware
 === PAUSE ===                                                             PAUSE command ignored (no hw support)
          please, try the plug plugin %s
      -d,--disconnect     disconnect
      -e,--exclusive      exclusive connection
      -i,--input          list input (readable) ports
      -l,--list           list current connections of each port
      -o,--output         list output (writable) ports
      -r,--real #         convert real-time-stamp on queue
      -t,--tick #         convert tick-time-stamp on queue
      -x, --removeall
      sender, receiver = client:port pair
    aconnect -i|-o [-options]
    aconnect [-options] sender receiver
   -d,--dest addr : write to given addr (client:port)
   -i, --info : print certain received events
   -p,--port # : specify TCP port (digit or service name)
   -s,--source addr : read from given addr (client:port)
   -v, --verbose : print verbose messages
   Clemens Ladisch <clemens@ladisch.de>   Copyright (C) 1999 Takashi Iwai
   Jaroslav Kysela <perex@perex.cz>   Subdevice #%i: %s
   Subdevices: %i/%i
   Tim Janik   client mode: aseqnet [-options] server_host
   server mode: aseqnet [-options]
  !clip    * Connection/disconnection between two ports
  * List connected ports (no subscription action)
  * Remove all exported connections
  [%s %s, %s]  can't play WAVE-files with sample %d bits in %d bytes wide (%d channels)  can't play WAVE-files with sample %d bits wide %s is not a mono stream (%d channels)
 %s!!! (at least %.3f ms long)
 %s: %s
 (default) (unplugged) **** List of %s Hardware Devices ****
 + -        Change volume -f cd (16 bit little endian, 44100, stereo)
 -f cdr (16 bit big endian, 44100, stereo)
 -f dat (16 bit little endian, 48000, stereo)
 0-9        Set volume to 0%-90% ; '        Toggle left/right capture < >        Toggle left/right mute Aborted by signal %s...
 Access type not available Access type not available for playback: %s
 All Authors: B          Balance left and right volumes Broken configuration for playback: no configurations available: %s
 Broken configuration for this PCM: no configurations available Buffer size range from %lu to %lu
 CAPTURE Can't recovery from suspend, prepare failed: %s
 Can't recovery from underrun, prepare failed: %s
 Can't use period equal to buffer size (%lu == %lu) Cannot create process ID file %s: %s Cannot open WAV file %s
 Cannot open file "%s". Cannot open mixer device '%s'. Capture Card: Center Channel %2d: Control event : %5d
 Channel %2d: Note Off event: %5d
 Channel %2d: Note On event : %5d
 Channel %2d: Pitchbender   : %5d
 Channel %d doesn't match with hw_parmas Channel 10 Channel 11 Channel 12 Channel 13 Channel 14 Channel 15 Channel 16 Channel 9 Channel numbers don't match between hw_params and channel map Channels %i Channels count (%i) not available for playbacks: %s
 Channels count non available Chip: Connected From Connecting To Connection failed (%s)
 Connection is already subscribed
 Copyright (C) 1999-2000 Takashi Iwai
 Debugging options:
  -g, --no-color          toggle using of colors
  -a, --abstraction=NAME  mixer abstraction level: none/basic Device name: Disconnection failed (%s)
 Done.
 End        Set volume to 0% Error Esc     Exit Esc: Exit F1 ? H  Help F1:  Help F2 /    System information F2:  System information F3      Show playback controls F4      Show capture controls F5      Show all controls F6 S    Select sound card F6:  Select sound card Failed. Restarting stream.  Front Front Left Front Right HW Params of device "%s":
 Help Invalid WAV file %s
 Invalid number of periods %d
 Invalid parameter for -s option.
 Invalid test type %s
 Item: L L       Redraw screen LFE Left    Move to the previous control M M          Toggle mute Max peak (%li samples): 0x%08x  Mono No enough memory
 No subscription is found
 Not a WAV file: %s
 O Off On Page Up/Dn Change volume in big steps Period size range from %lu to %lu
 Periods = %u
 Playback Playback device is %s
 Playback open error: %d,%s
 Playing Playing Creative Labs Channel file '%s'...
 Press F6 to select another sound card. Q W E      Increase left/both/right volumes R Rate %d Hz,  Rate %iHz not available for playback: %s
 Rate doesn't match (requested %iHz, get %iHz, err %d)
 Rate set to %iHz (requested %iHz)
 Rear Rear Left Rear Right Recognized sample formats are: Recording Requested buffer time %u us
 Requested period time %u us
 Right   Move to the next control Sample format non available Sample format not available for playback: %s
 Sample rate doesn't match (%d) for %s
 Select File Setting of hwparams failed: %s
 Setting of swparams failed: %s
 Side Side Left Side Right Sine wave rate is %.4fHz
 Sound Card Space      Toggle capture Sparc Audio Sparc Audio doesn't support %s format... Status(DRAINING):
 Status(R/W):
 Status:
 Stereo Stream parameters are %iHz, %s, %i channels
 Suspended. Trying resume.  Suspicious buffer position (%li total): avail = %li, delay = %li, buffer = %li
 Tab     Toggle view mode (F3/F4/F5) The available format shortcuts are:
 The sound device was unplugged. This sound device does not have any capture controls. This sound device does not have any controls. This sound device does not have any playback controls. Time per period = %lf
 Transfer failed: %s
 Try `%s --help' for more information.
 Unable to determine current swparams for playback: %s
 Unable to install hw params: Unable to parse channel map string: %s
 Unable to set avail min for playback: %s
 Unable to set buffer size %lu for playback: %s
 Unable to set buffer time %u us for playback: %s
 Unable to set channel map: %s
 Unable to set hw params for playback: %s
 Unable to set nperiods %u for playback: %s
 Unable to set period time %u us for playback: %s
 Unable to set start threshold mode for playback: %s
 Unable to set sw params for playback: %s
 Undefined channel %d
 Unknown option '%c'
 Unsupported WAV format %d for %s
 Unsupported bit size %d.
 Unsupported sample format bits %d for %s
 Up/Down    Change volume Usage:
 Usage: %s [OPTION]... [FILE]...

-h, --help              help
    --version           print current version
-l, --list-devices      list all soundcards and digital audio devices
-L, --list-pcms         list device names
-D, --device=NAME       select PCM by name
-q, --quiet             quiet mode
-t, --file-type TYPE    file type (voc, wav, raw or au)
-c, --channels=#        channels
-f, --format=FORMAT     sample format (case insensitive)
-r, --rate=#            sample rate
-d, --duration=#        interrupt after # seconds
-M, --mmap              mmap stream
-N, --nonblock          nonblocking mode
-F, --period-time=#     distance between interrupts is # microseconds
-B, --buffer-time=#     buffer duration is # microseconds
    --period-size=#     distance between interrupts is # frames
    --buffer-size=#     buffer duration is # frames
-A, --avail-min=#       min available space for wakeup is # microseconds
-R, --start-delay=#     delay for automatic PCM start is # microseconds 
                        (relative to buffer size if <= 0)
-T, --stop-delay=#      delay for automatic PCM stop is # microseconds from xrun
-v, --verbose           show PCM structure and setup (accumulative)
-V, --vumeter=TYPE      enable VU meter (TYPE: mono or stereo)
-I, --separate-channels one file for each channel
-i, --interactive       allow interactive operation from stdin
-m, --chmap=ch1,ch2,..  Give the channel map to override or follow
    --disable-resample  disable automatic rate resample
    --disable-channels  disable automatic channel conversions
    --disable-format    disable automatic format conversions
    --disable-softvol   disable software volume control (softvol)
    --test-position     test ring buffer position
    --test-coef=#       test coefficient for ring buffer position (default 8)
                        expression for validation is: coef * (buffer_size / 2)
    --test-nowait       do not wait for ring buffer - eats whole CPU
    --max-file-time=#   start another output file when the old file has recorded
                        for this many seconds
    --process-id-file   write the process ID here
    --use-strftime      apply the strftime facility to the output file name
    --dump-hw-params    dump hw_params of the device
    --fatal-errors      treat all errors as fatal
 Usage: alsamixer [options] Useful options:
  -h, --help              this help
  -c, --card=NUMBER       sound card number or id
  -D, --device=NAME       mixer device name
  -V, --view=MODE         starting view mode: playback/capture/all Using 16 octaves of pink noise
 Using max buffer size %lu
 VOC View: WAV file(s)
 WAVE Warning: format is changed to %s
 Warning: format is changed to MU_LAW
 Warning: format is changed to S16_BE
 Warning: format is changed to U8
 Warning: rate is not accurate (requested = %iHz, got = %iHz)
 Warning: unable to get channel map
 Wave doesn't support %s format... Woofer Write error: %d,%s
 You need to specify %d files Z X C      Decrease left/both/right volumes accepted[%d]
 aconnect - ALSA sequencer connection manager
 aseqnet - network client/server on ALSA sequencer
 audio open error: %s bad speed value %i buffer to small, could not use
 can't allocate buffer for silence can't get address %s
 can't get client id
 can't malloc
 can't open sequencer
 can't play WAVE-file format 0x%04x which is not PCM or FLOAT encoded can't play WAVE-files with %d tracks can't play loops; %s isn't seekable
 can't play packed .voc files can't set client info
 cannot enumerate sound cards cannot load mixer controls cannot open mixer capture stream format change? attempting recover...
 card %i: %s [%s], device %i: %s [%s]
 client %d: '%s' [type=%s]
 closing files..
 command should be named either arecord or aplay dB gain: disconnected
 enter device name... fatal %s: %s info error: %s invalid card index: %s
 invalid destination address %s
 invalid sender address %s
 invalid source address %s
 kernel malloc error mute no soundcards found... nonblock setting error: %s not enough memory ok.. connected
 options:
 overrun pause push error: %s pause release error: %s raw data read error read error (called from line %i) read error: %s read/write error, state = %s readv error: %s sequencer opened: %d:%d
 service '%s' is not found in /etc/services
 snd_pcm_mmap_begin problem: %s status error: %s stdin O_NONBLOCK flag setup failed
 suspend: prepare error: %s too many connections!
 try `alsamixer --help' for more information
 unable to install sw params: underrun unknown abstraction level: %s
 unknown blocktype %d. terminate. unknown length of 'fmt ' chunk (read %u, should be %u at least) unknown length of extensible 'fmt ' chunk (read %u, should be %u at least) unknown option: %c
 unrecognized file format %s usage:
 user value %i for channels is invalid voc_pcm_flush - silence error voc_pcm_flush error was set buffer_size = %lu
 was set period_size = %lu
 write error write error: %s writev error: %s wrong extended format '%s' wrong format tag in extensible 'fmt ' chunk xrun(DRAINING): prepare error: %s xrun: prepare error: %s xrun_recovery failed: %d,%s
 Project-Id-Version: alsa-utils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-10-27 17:34+0100
PO-Revision-Date: 2014-09-13 11:55+0000
Last-Translator: Valter Mura <valtermura@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 15:56+0000
X-Generator: Launchpad (build 18115)
 
Alcuni di questi potrebbero non essere disponibili sull'hardware selezionato
 === PAUSE ===                                                             Comando PAUSE ignorato (nessun supporto hardware)
          provare il plugin plug %s
      -d,--disconnesso disconnesso
      -e,--exclusive connessione esclusiva
      -i, --input elenca le porte in ingresso (leggibili)
      -l, --list Elenca le connessioni correnti per ogni porta
      -o, --output elenca le porte in uscita (scrivibili)
      -r, --real # converte il real-time-stamp in coda
      -t, --tick # converte il tick-time-stamp in coda
      -x, --removeall
      mittente, ricevitore = client:coppia porta
    aconnect -i|-o [-opzioni]
    aconnect [-opzioni] mittente ricevitore
   -d,--dest indirizzo: scrive in un dato indirizzo (client:porta)
   -i, --info: mostra degli eventi ricevuti
   -p,--port # : specificare la porta TCP (cifra o nome servizio)
   -s,--source indirizzo: legge da un dato indirizzo (client:port)
   -v, --verbose: stampa messaggi espliciti
   Clemens Ladisch <clemens@ladisch.de>   Copyright (C) 1999 Takashi Iwai
   Jaroslav Kysela <perex@perex.cz>   Sottoperiferica #%i: %s
   Sottoperiferiche: %i/%i
   Tim Janik   modalità client: aseqnet [-opzioni] server_host
   modalità server: aseqnet [-opzioni]
  !clip    * Connession/disconnessione tra due porte
  * Elenca le porte connesse (nessuna azione di sottoscrizione)
  * Rimuovi tutte le connessioni esportate
  [%s %s, %s]  Non possono essere riprodotti file WAVE con campionamento a %d bit in byte di larghezza pari a %d (%d canali)  Impossibile riprodurre file WAVE con campionamento a %d bit %s non è uno stream monofonico (%d canali)
 %s!!! (almeno %.3f ms di lunghezza)
 %s: %s
 (predefinita) (scollegato) **** Lista di %s dispositivi hardware ****
 + -        Regola il volume -f cd (16 bit little endian, 44100, stereo)
 -f cdr (16 bit big endian, 44100, stereo)
 -f dat (16 bit little endian, 48000, stereo)
 0-9        Imposta il volume a 0%-90% ; '        Attiva/Disattiva la cattura destra/sinistra < >        Attiva/Disattiva il muto a sinistra/destra Interrotto dal segnale %s...
 Tipo di accesso non disponibile Tipo di accesso non disponibile per la riproduzione: %s
 Tutto Autori: B          Bilancia i volumi di sinistra e destra Configurazione danneggiata per la riproduzione: nessuna configurazione disponibile: %s
 La configurazione di questo PCM è danneggiata: non ci sono configurazioni disponibili La dimensione del buffer va da %lu a %lu
 CATTURA Impossibile ripristinare dalla sospensione, preparazione fallita: %s
 Impossibile ripristinare dall'underrun, preparazione fallita: %s
 Impossibile utilizzare un periodo uguale alla larghezza del buffer (%lu == %lu) Impossibile creare il file PID %s: %s Impossibile aprire il file WAV %s
 Impossibile aprire il file "%s". Impossibile aprire il device "%s" del mixer Cattura Scheda: Centrale Canale %2d: Evento di controllo: %5d
 Canale %2d: Nota evento Off: %5d
 Canale %2d: Nota evento On: %5d
 Canale %2d: Pitchbender : %5d
 Il canale %d non trova corrispondenza con hw_params Canale 10 Canale 11 Canale 12 Canale 13 Canale 14 Canale 15 Canale 16 Canale 9 I numeri dei canali non corrispondono tra hw_params e la mappatura del canale Canali %i Conteggio dei canali (%i) non disponibile per la riproduzione: %s
 Conteggio dei canali non disponibile Processore: Connesso da Connessione a Connessione fallita (%s)
 La connessione è già stata sottoscritta
 Copyright (C) 1999-2000 Takashi Iwai
 Opzioni di debug:
  -g, --no-color          Attiva/Disattiva l'uso dei colori
  -a, --abstraction=NAME  Livello di astrazione del mixer: none/basic Nome device: Disconnessione fallita (%s)
 Eseguito.
 Fine        Imposta il volume allo 0% Errore Esc Esce Esc: Esce F1 ? H  Aiuto F1:  Aiuto F2 /    Informazioni di sistema F2:  Informazioni di sistema F3      Mostra controlli di riproduzione F4      Mostra controlli di cattura F5      Mostra tutti i controlli F6 S    Seleziona la scheda audio F6:  Selezione scheda sonora Fallito. Riavviare il flusso.  Fronte Frontale sinistro Frontale destro Parametri HW del dispositivo "%s":
 Aiuto File WAV %s non valido
 Numero non valido di periodi %d
 Parametro non valido per l'opzione -s.
 Tipo di test non valido %s
 Elemento: S L       Aggiorna lo schermo LFE Sinistra    Sposta al controllo precedente M M          Attiva/Disattiva il muto Picco massimo (%li campioni): 0x%08x  Mono Memoria insufficiente
 Nessuna sottoscrizione trovata
 Non è un file WAV: %s
 O Off On Pag Su/Giù Regola il volume con passi maggiori La durata del periodo va da %lu a %lu
 Fasi = %u
 Riproduzione La periferica di riproduzione è %s
 Errore di riproduzione: %d,%s
 Riproduzione in corso File Creative Labs Channel in esecuzione «%s»...
 Premere F6 per selezionare un'altra scheda audio. Q W E      Aumenta i volumi di sinistra/entrambi/destra D Frequenza %d Hz,  Frequenza %iHz non disponibile per la riproduzione: %s
 La frequenza non corrisponde (richiesti %iHz, ottenuti %iHz, errore %d)
 Frequenza impostata a %iHz (richiesti %iHz)
 Retro Posteriore sinistro Posteriore destro I formati riconosciuti per il campionamento sono: Registrazione in corso Buffer richiesto in %u microsecondi
 Richiesto periodo di %u microsecondi
 Destra   Sposta al controllo successivo Formato campione non disponibile Formato di campionamento non disponibile per la riproduzione: %s
 La frequenza di campionamento non corrisponde (%d) per %s
 Seleziona file L'impostazione dei parametri hardware è fallita: %s
 L'impostazione dei parametri software è fallita: %s
 Lato Laterale sinistro Laterale destro La frequenza dell'onda sinusoidale è di %.4fHz
 Scheda audio Space      Attiva/Disattiva la cattura Audio Sparc L'Audio Sparc non supporta il formato %s... Stato (SCARICAMENTO):
 Stato(R/W):
 Stato:
 Stereo I parametri dello stream sono %iHz, %s, %i canali
 Sospeso. Tentare di riprendere.  Posizione del buffer sospetta (%li totale): disponibile = %li, ritardo = %li, buffer = %li
 Tab     Cambia modalità visualizzazione (F3/F4/F5) Le scorciatoie di formato disponibili sono:
 Il dispositivo sonoro è stato scollegato. Questo dispositivo audio non dispone dei controlli di cattura. Questo dispositivo audio non dispone di alcun controllo. Questo dispositivo audio non dispone dei controlli di riproduzione. Durata per periodo = %lf
 Trasferimento fallito: %s
 Usare `%s --help' per ulteriori informazioni.
 Impossibile determinare i parametri software correnti per la riproduzione: %s
 Impossibile installare i parametri hardware: Impossibile analizzare la stringa di mappa del canale: %s
 Impossibile impostare il limite minimo per la riproduzione: %s
 Impossibile impostare la grandezza del buffer a %lu per la riproduzione: %s
 Impossibile impostare il buffer a %u microsecondi per la riproduzione: %s
 Impossibile impostare la mappatura del canale: %s
 Impossibile impostare i parametri hardware per la riproduzione: %s
 Impossibile impostare n-periodi a %u per la riproduzione: %s
 Impossibile impostare il periodo di %u microsecondi per la riproduzione: %s
 Impossibile impostare la modalità di threshold per la riproduzione: %s
 Impossibile impostare i parametri software per la riproduzione: %s
 Canale %d non definito
 Opzione sconosciuta '%c'
 Formato WAV %d per %s non supportato
 Dimensione del bit %d non supportata.
 Campione del formato dei bit %d non supportato per %s
 Su/Giù    Regola il volume Uso:
 Uso: %s [OPZIONE]... [FILE]...

-h, --help guida
    --version mostra la versione corrente
-l, --list-devices elenca tutte le schede audio e i dispositivi audio digitali
-L, --list-pcms elenca i nomi dei dispositivi
-D, --device=NOME seleziona PCM in base al nome
-q, --quiet modalità silenziosa
-t, --file-type TYPE tipo di file (voc, wav, raw o au)
-c, --channels=# canali
-f, --format=FORMAT formato campionamento (non distingue fra maiuscole e minuscole)
-r, --rate=# frequenza di campionamento
-d, --duration=# interrompe dopo # secondi
-M, --mmap flusso mmap
-N, --nonblock modalità di non-blocco
-F, --period-time=# la distanza tra interrupt è di # microsecondi
-B, --buffer-time=# la durata del buffer è di # microsecondi
    --period-size=# la distanza tra interrupt è di # frames
    --buffer-size=# la durata del buffer è di # frames
-A, --avail-min=# min lo spazio disponibile per la ripresa è di # microsecondi
-R, --start-delay=# ritardo per l'avvio automatico PCM è di # microsecondi
                        (rispetto alla dimensione del buffer se<= 0)
-T, --stop-delay=# il ritardo per lo stop automatico PCM è di # microsecondi da xrun
-v, --verbose mostra la struttura e le impostazioni PCM (complessivo)
-V, --vumeter=TYPE attiva il misuratore VU (TIPO: mono o stereo)
-I, --separate-channels un file per ogni canale
-i, --interactive consente il funzionamento interattivo da stdin
-m, --chmap=ch1,ch2,.. Fornisce la mappa del canale da ignorare o seguire
    --disable-resample disattiva la frequenza di ricampionamento automatico
    --disable-channels disattiva la conversione automatica dei canali
    --disable-format disattiva la conversione automatica dei formati
    --disable-softvol disattiva il controllo software del volume (softvol)
    --test-position test posizione del buffer circolare
    --test-coef=# coefficiente di prova per la posizione del buffer circolare (default 8)
                        l'espressione di convalida è: coef * (buffer_size / 2)
    --test-nowait non attende il buffer circolare - carica la CPU
    --max-file-time=# inizia un altro file di output quando il vecchio file è stato registrato
                        per il numero di secondi indicato
    --process-id-file inserire qui l'ID del processo
    --use-strftime applica la funzione strftime al nome del file di output
    --dump-hw-params scarica hw_params dal dispositivo
    --fatal-errors tratta tutti gli errori fatali
 Uso: alsamixer [OPZIONI] Opzioni utili:
  -h, --help              Stampa questo aiuto
  -c, --card=NUMBER       Numero o ID della scheda audio
  -D, --device=NAME       Nome del device mixer
  -V, --view=MODE         Vista di avvio: playback/capture/all In uso 16 ottave di rumore rosa
 Usare la dimensione massima di buffer %lu
 VOC Vista: File WAV
 WAVE Attenzione: il formato viene modificato in %s
 Attenzione: il formato è cambiato in MU_LAW
 Attenzione: il formato è cambiato in S16_BE
 Attenzione: il formato è cambiato in U8
 Attenzione: il campionamento non è accurato (richiesto=%iHz, ottenuto=%iHz)
 Attenzione: impossibile ottenere la mappatura del canale
 Wave non supporta il formato %s... Woofer Errore di scrittura: %d,%s
 Bisogna specificare %d file Z X C      Diminuisce i volumi di sinistra/entrambi/destra accettato[%d]
 aconnect - gestore delle connessioni del sequencer ALSA
 aseqnet - rete client/server del sequencer ALSA
 errore aprendo l'audio: %s valore della velocità errato: %i buffer troppo piccolo, potrebbe non essere usato
 impossibile allocare il buffer per il silenzio impossibile ottenere l'indirizzo %s
 impossibile ottenere l'id del client
 impossibile allocare memoria
 impossibile aprire il sequencer
 impossibile riprodurre il file WAVE nel formato 0x%04x, non è codificato in PCM o FLOAT impossibile riprodurre file WAVE con %d tracce Impossibile riprodurre cicli; %s non è ricercabile
 impossibile eseguire i file compressi .voc impossibile impostare le informazioni sul client
 Impossibile enumerare le schede audio impossibile caricare i controlli del mixer impossibile aprire il mixer Il formato dello stream di cattura è cambiato? Tentativo di recupero...
 scheda %i: %s [%s], dispositivo %i: %s [%s]
 client %d: «%s» [type=%s]
 chiudendo i file..
 il comando dovrebbe essere o arecord o aplay Guadagno dB: disconnesso
 Inserire il nome del device Fatale %s: %s Informazioni sull'errore: %s indice della scheda non valido: %s
 indirizzo di destinazione non valido %s
 indirizzo del mittente non valido %s
 indirizzo sorgente %s non valido
 kernel errore di malloc muto nessuna scheda audio trovata... errore non bloccante nei parametri:%s memoria insufficiente ok.. connesso
 opzioni:
 overrun Errore pause push: %s Errore pause release: %s dati grezzi errore di lettura errore di lettura (chiamato dalla linea %i) errore di lettura: %s errori lettura/scrittura, stato = %s errore readv: %s sequencer aperto: %d:%d
 il servizio «%s» non è stato trovato in /etc/services
 problema snd_pcm_mmap_begin: %s condizione di errore: %s impostazione flag stdin O_NONBLOCK non riuscita
 sospensione: errore di preparazione: %s troppe connessioni!
 provare "alsamixer --help" per maggiori informazioni
 Impossibile installare i parametri software: underrun livello di astrazione sconosciuto: %s
 tipo si blocco sconosciuto %d. termino lunghezza sconosciuta del blocco "fmt" (letti %u, dovrebbero essere almeno %u) lunghezza del blocco estensibile "fmt" sconosciuta (letto %u, dovrebbe essere almeno %u) opzione sconosciuta: %c
 formato del file %s non riconosciuto uso:
 utente valore %i per i canali non valido voc_pcm_flush - errore di silenzio errore voc_pcm_flush buffer_size impostato a %lu
 period_size impostato a %lu
 errore di scrittura errore di scrittura: %s errore writev: %s formato esteso errato "%s" tag di formato nel blocco estensibile "fmt" non corretto xrun(DRAINING): errore di preparazione: %s xrun: errore di preparazione: %s xrun_recovery fallito: %d,%s
 