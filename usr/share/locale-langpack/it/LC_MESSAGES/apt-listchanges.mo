��    !      $  /   ,      �  $   �       /   *  g   Z     �     �  *   �       M        j     �  (   �     �     �     �     �     �        *   ,  �   W  ;   �       0   2  <   c  r   �  2        F     b  "   x     �     �  #   �  �  �  )   �	  (   �	  4   
  �   P
     �
     �
  <   �
     -  f   I     �     �  -   �     �               .     =  ,   U  1   �  �   �  B   >     �  >   �  :   �  �     1   �     �     �  !        &  %   F  +   l     !                                                                   	                                
                                                       %s: Version %s has already been seen %s: will be newly installed --since=<version> expects a only path to a .deb <big><b>Changelogs</b></big>

The following changes are found in the packages you are about to install: Aborting Changes for %s Confirmation failed, don't save seen state Continue Installation? Could not run apt-changelog (%s), unable to retrieve changelog for package %s Do you want to continue? [Y/n]  Done Ignoring `%s' (seems to be a directory!) Informational notes List the changes Mailing %s: %s News for %s Reading changelogs Reading changelogs. Please wait. The %s frontend is deprecated, using pager The gtk frontend needs a working python-gtk2 and python-glade2.
Those imports can not be found. Falling back to pager.
The error is: %s
 The mail frontend needs a installed 'sendmail', using pager Unknown frontend: %s
 Unknown option %s for --which.  Allowed are: %s. Usage: apt-listchanges [options] {--apt | filename.deb ...}
 Wrong or missing VERSION from apt pipeline
(is Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version set to 2?)
 You can abort the installation if you select 'no'. apt-listchanges: Changelogs apt-listchanges: News apt-listchanges: changelogs for %s apt-listchanges: news for %s database %s failed to load.
 didn't find any valid .deb archives Project-Id-Version: apt-listchanges 2.82 italian translation
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-02-17 14:04+0000
PO-Revision-Date: 2013-01-28 11:18+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <debian-l10n-italian@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 15:57+0000
X-Generator: Launchpad (build 18115)
 %s: la versione %s è già stata mostrata %s: verrà installato per la prima volta --since=VERSIONE richiede un solo percorso a un .deb <big><b>Registri delle modifiche</b></big>
 
Nei pacchetti che stanno per essere installati sono presenti i seguenti cambiamenti: Annullamento Modifiche per %s Fallita la conferma, lo stato «visto» non è stato salvato Continuare l'installazione? Impossibile eseguire apt-changelog (%s): scaricamento delle modifiche per il pacchetto %s non riuscito Continuare [S/n]  Fatto Ignorata «%s» (sembra essere una directory) Note informative Elenco dei cambiamenti Invio di %s: %s Notizie per %s Lettura delle modifiche Lettura registri delle modifiche. Attendere. L'interfaccia %s è deprecata; verrà usata pager L'interfaccia gtk ha bisogno di python-gtk2 e python-glade2.
Non è possibile trovare questi moduli; verrà usata pager.
L'errore è: %s
 L'interfaccia mail ha bisogno di un "sendmail"; verrà usata pager Frontend sconosciuto: %s
 Opzione %s non valida con --which. Quelle consentite sono: %s. Uso: apt-listchanges [opzioni] {--apt | nomefile.deb ...}
 Il valore VERSION dalla pipe apt è sbagliato o inesistente
(Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version è impostato a 2?)
 Per annullare l'installazione selezionare «no». apt-listchanges: changelog apt-listchanges: notizie apt-listchanges: modifiche per %s apt-listchanges: notizie per %s impossibile caricare il database %s.
 impossibile trovare un archivio .deb valido 