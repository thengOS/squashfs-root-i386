��            )         �     �  �  �  �  �  !      !   B  /   d  =   �  2   �  $        *  %   C  .   i  +   �  *   �  .   �     	     9	     U	     s	     �	  &   �	     �	     �	  @   �	  3   /
  R  c
     �     �     �       �        �  T  �  �  #  $   �  &   �  =   !  N   _  =   �  )   �       ,   4  ,   a  1   �  1   �  5   �  !   (  !   J  "   l  "   �     �  +   �     �       E     :   _  e  �            !  %   >  #   d                                                                                     	                                      
                    %s %s
   -n, --name=name         get the named extended attribute value
  -d, --dump              get all extended attribute values
  -e, --encoding=...      encode values (as 'text', 'hex' or 'base64')
      --match=pattern     only get attributes with names matching pattern
      --only-values       print the bare values only
  -h, --no-dereference    do not dereference symbolic links
      --absolute-names    don't strip leading '/' in pathnames
  -R, --recursive         recurse into subdirectories
  -L, --logical           logical walk, follow symbolic links
  -P  --physical          physical walk, do not follow symbolic links
      --version           print version and exit
      --help              this help text
   -n, --name=name         set the value of the named extended attribute
  -x, --remove=name       remove the named extended attribute
  -v, --value=value       use value as the attribute value
  -h, --no-dereference    do not dereference symbolic links
      --restore=file      restore extended attributes
      --version           print version and exit
      --help              this help text
 %s %s -- get extended attributes
 %s %s -- set extended attributes
 %s: %s: No filename found in line %d, aborting
 %s: No filename found in line %d of standard input, aborting
 %s: Removing leading '/' from absolute path names
 %s: invalid regular expression "%s"
 -V only allowed with -s
 A filename to operate on is required
 At least one of -s, -g, -r, or -l is required
 Attribute "%s" had a %d byte value for %s:
 Attribute "%s" has a %d byte value for %s
 Attribute "%s" set to a %d byte value for %s:
 Could not get "%s" for %s
 Could not list "%s" for %s
 Could not remove "%s" for %s
 Could not set "%s" for %s
 No such attribute Only one of -s, -g, -r, or -l allowed
 Unrecognized option: %c
 Usage: %s %s
 Usage: %s %s
       %s %s
Try `%s --help' for more information.
 Usage: %s %s
Try `%s --help' for more information.
 Usage: %s [-LRSq] -s attrname [-V attrvalue] pathname  # set value
       %s [-LRSq] -g attrname pathname                 # get value
       %s [-LRSq] -r attrname pathname                 # remove attr
       %s [-LRq]  -l pathname                          # list attrs 
      -s reads a value from stdin and -g writes a value to stdout
 getting attribute %s of %s listing attributes of %s setting attribute %s for %s setting attributes for %s Project-Id-Version: attr
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-03 13:57+0000
PO-Revision-Date: 2007-10-12 20:07+0000
Last-Translator: Luca Ferretti <elle.uca@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 15:59+0000
X-Generator: Launchpad (build 18115)
        %s %s
   -n, --name=NOME         legge il valore dell'attributo esteso fornito
  -d, --dump              legge tutti i valori degli attributi estesi
  -e, --encoding=...      codifica i valori (come "text", "hex" o "base64")
      --match=MODELLO     legge solo gli attributi i cui nomi corrispondono
                          al modello
      --only-values       stampa solo i semplici valori
  -h, --no-dereference    non dereferzia i collegamenti simbolici
      --absolute-names    non toglie i '/' iniziali nei nomi di percorso
  -R, --recursive         esegue ricorsivamente nelle sottodirectory
  -L, --logical           cammino logico, segue i collegamenti simbolici
  -P  --physical          cammino fisico, non segue i collegamenti simbolici
      --version           stampa la versione ed esce
      --help              mostra questo testo d'aiuto
   -n, --name=NOME         imposta il valore dell'attributo esteso indicato
  -x, --remove=NOME       rimove l'attributo esteso indicato
  -v, --value=VALORE      usa VALORE come valore dell'attributo
  -h, --no-dereference    non dereferenzia i collegamenti simbolici
      --restore=FILE      ripristina gli attributi estesi
      --version           stampa la versione ed esce
      --help              mostra questo testo d'aiuto
 %s %s -- legge gli attributi estesi
 %s %s -- imposta gli attributi estesi
 %s: %s: nessun nome di file trovato alla riga %d, interrotto
 %s: nessun nome di file trovato alla riga %d dello standard input, interrotto
 %s: rimozione dei '/' iniziali dai nomi di percorso assoluti
 %s: espressione regolare "%s" non valida
 -V è consentita solo con -s
 È richiesto un nome di file su cui operare
 È richiesta almeno una tra -s, -g, -r o -l
 L'attributo "%s" aveva un valore %d byte per %s:
 L'attributo "%s" aveva un valore %d byte per %s:
 Attributo "%s" impostato a un valore %d byte per %s:
 Impossibile ottenere "%s" per %s
 Impossibile elencare "%s" per %s
 Impossibile rimuovere "%s" per %s
 Impossibile impostare "%s" per %s
 Nessun attributo È consentita solo una tra -s, -g, -r o -l
 Opzione non riconosciuta: %c
 Uso: %s %s
 Uso: %s %s
     %s %s
Provare "%s --help" per maggiori informazioni.
 Uso: %s %s
Provare "%s --help" per maggiori informazioni.
 Uso: %s [-LRSq] -s NOMEATTR [-V VALOREATTR] NOMEPERCORSO  # imposta valore
     %s [-LRSq] -g NOMEATTR NOMEPERCORSO                  # legge valore
     %s [-LRSq] -r NOMEATTR NOMEPERCORSO                  # rimuove attr
     %s [-LRq]  -l NOMEPERCORSO                           # elenca attr
    -s legge un valore da stdin e -g scrive un valore su stdout
 recupero dell'attributo %s di %s elenco degli attributi di %s impostazione dell'attributo %s per %s impostazione degli attributi per %s 