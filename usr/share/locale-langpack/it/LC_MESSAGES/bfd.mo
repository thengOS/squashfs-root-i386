��    F      L  a   |         &        (     A     Z      s     �  #   �  %   �  $   �     #     =  P   U  F   �  &   �  o     (   �  1   �  4   �     	  *   2	  *   ]	  -   �	  ,   �	  7   �	  6   
  >   R
  I   �
  0   �
  &     /   3  	   c  #   m     �  .   �  	   �  #   �  Y   
     d     y     �     �     �  +   �     �               %     .  
   E  "   P  /   s     �     �     �  :   �  6   $  8   [  7   �     �  7   �     $      7     X     v     �  0   �     �  -   �  6     �  K  *   �     #     @     ]  '   x  $   �  #   �  $   �  )        8     S  Z   s  U   �  -   $  �   R  0   �  9     ?   K  5   �  3   �  5   �  1   +  0   ]  E   �  I   �  E     S   d  -   �  /   �  B        Y  ,   g     �  3   �     �  1   �  q   (     �     �     �     �       6        H     d     z     �     �     �  ?   �  7        =     Z  +   y  8   �  4   �  A     Z   U  9   �  P   �     ;      Z  1   {     �     �  7   �     
  5   )  D   _     +   
              =       @   F                 ?   ,      D              5      /      	      B   3   (       4   %       1   -                 0   <         &                               C       >      "   9                       !      6          ;   2   8      .   )       :       '   A                7       $                          *   #   E    
      End+1 symbol: %-7ld   Type:  %s 
      End+1 symbol: %ld 
      First symbol: %ld 
      Local symbol: %ld 
      struct; End+1 symbol: %ld 
      union; End+1 symbol: %ld  [floats passed in float registers]  [floats passed in integer registers]  [interworking flag not initialised]  [interworking supported]  [position independent] %B(%A+0x%lx): Only ADD or SUB instructions are allowed for ALU group relocations %B(%A+0x%lx): R_ARM_TLS_LE32 relocation not permitted in shared object %B: Bad relocation record imported: %d %B: Cannot handle compressed Alpha binaries.
   Use compiler flags, or objZ, to generate uncompressed binaries. %B: Invalid relocation type imported: %d %B: Unknown section type in a.out.adobe file: %x
 %B: Unrecognized storage class %d for %s symbol `%s' %B: bad string table size %lu %B: symbol `%s' has unrecognized smclas %d %B: unknown/unsupported relocation type %d %B: unsupported relocation: ALPHA_R_GPRELHIGH %B: unsupported relocation: ALPHA_R_GPRELLOW %B: warning: duplicate line number information for `%s' %s: TOC reloc at 0x%x to symbol `%s' with no TOC entry %s: can not represent section `%s' in a.out object file format %s: can not represent section for symbol `%s' in a.out object file format %s: relocatable link from %s to %s not supported %s: unsupported relocation type 0x%02x %s: warning: illegal symbol index %ld in relocs *unknown* Archive object file in wrong format BFD %s assertion fail %s:%d BFD %s internal error, aborting at %s line %d
 Bad value Dwarf Error: Bad abbrev number: %u. Dwarf Error: found address size '%u', this reader can not handle sizes greater than '%u'. Error reading %s: %s File format is ambiguous File in wrong format File too big File truncated GP relative relocation when _gp not defined Invalid bfd target Invalid operation Memory exhausted No error No more archived files No symbols Reading archive file mod timestamp Symbol needs debug section which does not exist Unknown basic type %d Unrecognized reloc Unrecognized reloc type 0x%x Warning: %B does not support interworking, whereas %B does Warning: %B supports interworking, whereas %B does not Warning: type of symbol `%s' changed from %d to %d in %B Warning: writing archive was slow: rewriting timestamp
 Writing updated armap timestamp cannot handle R_MEM_INDIRECT reloc when using %s output ignoring reloc %s
 not mapping: data=%lx mapped=%d
 not mapping: env var not set
 private flags = %x: reopening %B: %s
 uncertain calling convention for non-COFF symbol using multiple gp values warning: %B: local symbol `%s' has no section warning: unable to update contents of %s section in %s Project-Id-Version: binutils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-02-10 09:42+1030
PO-Revision-Date: 2010-04-16 13:15+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-10-09 10:33+0000
X-Generator: Launchpad (build 18227)
 
      Simbolo finale+1: %-7ld   Tipo:  %s 
      Simbolo finale+1: %ld 
      Simbolo iniziale: %ld 
      Simbolo locale: %ld 
      struttura; Simbolo finale+1: %ld 
      unione; Simbolo finale+1: %ld  [float forniti nei registri float]  [float forniti nei registri interi]  [flag di interworking non inizializzato]  [interworking supportato]  [indipendente dalla posizione] %B(%A+0x%lx): Solo le istruzioni ADD o SUB sono permesse per i gruppi di riallocazione ALU %B(%A+0x%lx): la riallocazione R_ARM_TLS_LE32 non è permessa in un oggetto condiviso %B: Importato record di rilocazione errato %d %B: Non posso gestire binari compressi di tipo Alpha.
   Usare le opzioni del compilatore, oppure objZ, per generare dei binari non compressi %s: Tipo di rilocazione importato non valido: %d %B: Tipo di sezione sconosciuto nel file a.out.adobe: %x
 %B: classe di memorizzazione sconosciuta  %d for %s symbol `%s' %B: dimensione errata della tabella della stringa %lu %B: il simbolo `%s' ha un irriconoscibile smclas %d %B: tipo di rilocazione %d sconosciuto/non supportato %B: rilocazione non supportata: ALPHA_R_GPRELHIGH %B: rilocazione non supportata: ALPHA_R_GPRELLOW %B: attenzione: informazione sul numero della riga duplicata per `%s' %s: riallocazione TOC al 0x%x per il simbolo `%s' con nessun ingresso TOC %s: impossibile rappresentare una sezione `%s' nel file oggetto a.out %s: impossibile rappresentare la sezione per il simbolo '%s' nel file oggetto a.out %s: link rilocabile da %s a %s non supportato %s: tipo di riallocazione non supportato 0x%02x %s: attenzione: simbolo di indice illegale %ld nelle riallocazioni *sconosciuto* File oggetto dell'archivio in formato errato BFD %s asserzione fallita %s:%d BFD %s errore interno, interruzione in %s linea %d
 Valore errato Errore Dwarf: Numero di abbrievazione errato: %u. Errore Dwarf: trovata dimensione indiirizzo '%u'. Questo lettore non può gestire dimensioni più grandi di '%u'. Errore nella lettura di %s: %s Il formato del file è ambiguo Formato del file errato File troppo grande File troncato Riallocazione relativa a GP quando _gp non è definito Destinazione bfd non valida Operazione non valida Memoria esaurita Nessun errore Nessun'altro file archiviato Nessun simbolo Lettura del timestamp dell'ultima modifica del file di archivio Il simbolo richiede una sezione di debug che non esiste Tipo semplice sconosciuto %d Riallocazione non riconosciuta Tipo di riallocazione 0x%x non riconosciuto Attenzione: %B non supporta l'interworking, mentre %B si Attenzione: %B supporta l'interworking, mentre %B no Attenzione: il tipo del simbolo `%s' è cambiato da %d a %d in %B Attenzione: la scrittura dell'archivio è stata lenta: riscrittura del timestamp in corso
 Scrittura del timestamp aggiornato dei mapping dei moduli impossibile gestire la rilocazione R_MEM_INDIRECT quando si utilizza l'output %s ignorando la riallocazione %s
 non mappato: data=%lx mapped=%d
 non mappato: variabile di ambiente non impostata
 flag privati = %x: riaprendo %B: %s
 convenzione incerta di chiamata per il simbolo non-COFF utilizzando valori gp multipli attenzione: %B: il simbolo locale `%s' non ha sezioni attenzione impossibile aggiornare i contenuti della sezione %s in %s 