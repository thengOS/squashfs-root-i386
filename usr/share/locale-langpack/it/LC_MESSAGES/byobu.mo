��    -      �  =   �      �     �  "   �  @        _     v     |  
   �  4   �  .   �  9   �  +   -     Y     `     v     �     �     �  	   �     �     �     �            @   '     h     t  �   y          $     )     1  	   6     @     H     \     t     �  &   �     �     �     �  $     /   ,     \  �  e  !   
  9   #
  C   ]
  $   �
     �
     �
     �
  6   �
  5     ?   S  7   �     �     �     �       !   (  	   J  	   T     ^     u     �     �     �  J   �          '  �   -     �     �  	   �     �                    3     Q  $   g  *   �     �     �     �     �  "     	   3           
                                                     %                     )          *   "       !          -      ,      &                $         	   '      (       #             +                       Byobu Configuration Menu  file exists, but is not a symlink <Tab>/<Alt-Tab> between elements | <Enter> selects | <Esc> exits Add to default windows Apply Archive Byobu Help Byobu currently does not launch at login (toggle on) Byobu currently launches at login (toggle off) Byobu will be launched automatically next time you login. Byobu will not be used next time you login. Cancel Change Byobu's colors Change escape sequence Change escape sequence: Change keybinding set Choose Command:  Create new window(s): Create new windows ERROR: Invalid selection Error: Escape key: ctrl- Extract the archive in your home directory on the target system. File exists Help If you are using the default set of keybindings, press\n<F5> to activate these changes.\n\nOtherwise, exit this screen session and start a new one. Manage default windows Menu Message Okay Presets:  Profile Remove file? [y/N]  Run "byobu" to activate Select a color:  Select a screen profile:  Select window(s) to create by default: Title:  Toggle status notifications Toggle status notifications: Which profile would you like to use? Which set of keybindings would you like to use? Windows: Project-Id-Version: byobu
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2009-11-23 20:48-0600
PO-Revision-Date: 2012-03-03 09:41+0000
Last-Translator: Claudio Arseni <claudio.arseni@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:40+0000
X-Generator: Launchpad (build 18115)
Language: it
  Menù di configurazione di Byobu  il file esiste già, ma non è un collegamento simbolico <Tab>/<Alt-Tab> Muove tra elementi | <Invio> Seleziona | <Esc> Esce Aggiungere alle finestre predefinite Applica Archivio Aiuto di Byobu Byobu attualmente non si avvia all'accesso (abilitare) Byobu attualmente si avvia all'accesso (disabilitare) Il programma sarà avviato automaticamente al prossimo accesso. Il programma non verrà utilizzato al prossimo accesso. Annulla Cambia i colori di Byobu Modificare la sequenza d'uscita Cambiare sequenza d'uscita: Modificare associazioni dei tasti Scegliere Comando:  Creare nuove finestre: Creare nuove finestre Errore: selezione non valida Errore: Comando d'uscita: Ctrl- Estrarre l'archivio nella directory personale sul sistema di destinazione. Il file esiste già Aiuto Se viene usata l'associazione dei tasti predefinita, premere\n<F5> per rendere attive queste modifiche.\nAltrimenti uscire da questa sessione e iniziarne una nuova. Gestire finestre predefinite Menù Messaggio OK Preselezioni:  Profilo Rimuovere il file? [s/N]  Eseguire "byobu" per attivare Seleziona un colore:  Selezionare un profilo di "screen":  Selezionare una o più finestre da creare: Titolo:  Abilitare notifiche di stato Abilitare notifiche di stato: Quale profilo usare? Quale associazione di tasti usare? Finestre: 