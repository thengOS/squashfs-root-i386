��    A      $  Y   ,      �     �  �   �     f     �     �     �     �     �     �     �  7   �     6     Q     V     m     |     �     �     �  )   �       ?     4   S     �  %   �  &   �     �  -   	     9	     A	  "   X	  6   {	  >   �	     �	     
  &   '
  M   N
  +   �
  6   �
  #   �
     #  .   3  '   b     �     �     �     �     �       G     ,   _     �     �  "   �     �     �     �                  (   3     \     v  1   �  �  �     f  �   }     ]     |     �     �     �     �     �     �  @        F     c  !   g     �     �  "   �     �        4        I  3   [  A   �  -   �  ;   �  .   ;      j  4   �     �     �  1   �  <     ?   W     �  '   �  '   �  d      7   e  ?   �  5   �       ,   '  )   T  !   ~     �  7   �     �  $   �     $  _   C  5   �     �     �  ,   �     %     C     ]     o     �     �  >   �     �     	  ?   "     +   %   $   /   1      &                   =   <      @      ?   *   ;              (   
      A       >             #      0       ,                                   .         "                                        '                   9   -   )   	   :   4   !           5   8   7   2                     3          6    
<action> is one of:
 
<name> is the device to create under %s
<device> is the encrypted device
<key slot> is the LUKS key slot number to modify
<key file> optional key file for the new key for luksAddKey action
 %s is not a LUKS partition
 %s: requires %s as arguments <device> <device> <key slot> <device> <name>  <device> [<new key file>] <name> <name> <device> Align payload at <n> sector boundaries - for luksFormat Argument <action> missing. BITS Can't open device: %s
 Command failed Command successful.
 Create a readonly mapping Display brief usage Do not ask for confirmation Failed to obtain device mapper directory. Help options: How many sectors of the encrypted data to skip at the beginning How often the input of the passphrase can be retried Key %d not active. Can't wipe.
 Key size must be a multiple of 8 bits PBKDF2 iteration time for LUKS (in ms) Print package version Read the key from a file (can be /dev/random) SECTORS Show this help message Shows more detailed error messages The cipher used to encrypt the disk (see /proc/crypto) The hash used to create the encryption key from the passphrase The size of the device The size of the encryption key The start offset in the backend device This is the last keyslot. Device will become unusable after purging this key. This will overwrite data on %s irrevocably. Timeout for interactive passphrase prompt (in seconds) Unable to obtain sector size for %s Unknown action. Verifies the passphrase by asking for it twice [OPTION...] <action> <action-specific>] add key to LUKS device create device dump LUKS partition information formats a LUKS device key %d active, purge first.
 key %d is disabled.
 key material section %d includes too few stripes. Header manipulation?
 memory allocation error in action_luksFormat modify active device msecs open LUKS device as mapping <name> print UUID of LUKS device remove LUKS mapping remove device resize active device secs show device status tests <device> for LUKS partition header unknown hash spec in phdr unknown version %d
 wipes key with number <key slot> from LUKS device Project-Id-Version: cryptsetup
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2007-01-02 16:49+0100
PO-Revision-Date: 2010-09-10 10:21+0000
Last-Translator: Sergio Zanchetta <primes2h@ubuntu.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:25+0000
X-Generator: Launchpad (build 18115)
 
<azione> è una tra:
 
<nome> è il device da creare in %s
<device> è il device cifrato
<slot di chiave> è il numero dello slot di chiave LUKS da modificare
<file chiave> è il file chiave opzionale per la nuova chiave per l'azione luksAddKey
 %s non è una partizione LUKS
 %s: richiede %s come argomenti <device> <device> <slot di chiave> <device> <nome>  <device> [<nuovo file chiave>] <nome> <nome> <device> Allinea il payload agli estremi del settore <n> - per luksFormat Argomento <azione> mancante. BIT Impossibile aprire il device: %s
 Comando non riuscito Comando eseguito con successo.
 Crea una mappatura in sola lettura Mostra il modo d'uso sintetico Non chiede conferma Recupero della directory device mapper non riuscito. Opzioni di aiuto: Quanti settori dei dati cifrati saltare dall'inizio Quante volte può essere ritentato l'inserimento della passphrase Chiave %d non attiva. Impossibile ripulirla.
 La dimensione della chiave deve essere un multiplo di 8 bit Tempo di iterazione di PBKDF2 per LUKS (in ms) Stampa la versione del pacchetto Legge la chiave da un file (può essere /dev/random) SETTORI Mostra questo messaggio d'aiuto Mostra i messaggi di errore con maggior dettaglio Il cifrario usato per cifrare il disco (vedere /proc/crypto) L'hash usato per creare la chiave di cifratura dalla passphrase La dimensione del device La dimensione della chiave di cifratura L'offset iniziale del device di backend Questo è l'ultimo slot di chiave. Il device sarà inutilizzabile dopo aver eliminato questa chiave. Ciò sovrascriverà i dati in %s in modo irreversibile. Timeout per il prompt interattivo della passphrase (in secondi) Impossibile ottenere la dimensione del settore per %s Azione sconosciuta. Verifica la passphrase chiedendola due volte [OPZIONE...] <azione> <azione-specifica>] aggiunge la chiave al device LUKS crea il device esegue il dump delle informazioni sulla partizione LUKS formatta un device LUKS chiave %d attiva, eliminarla prima.
 la chiave %d è disabilitata.
 la sezione materiale della chiave %d contiene troppe poche strisce. Manipolazione dell'header?
 errore di allocazione di memoria in action_luksFormat modifica il device attivo msec apre il device LUKS come mappatura in <nome> stampa l'UUID del device LUKS rimuove la mappatura LUKS rimuove il device ridimensiona il device attivo sec mostra lo stato del device esegue il test del <device> per l'header della partizione LUKS hash spec in phdr sconosciuta versione %d sconosciuta
 ripulisce la chiave con numero <slot di chiave> dal device LUKS 