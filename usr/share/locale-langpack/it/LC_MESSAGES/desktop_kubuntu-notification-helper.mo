��          �      <      �  $   �  +   �  F     4   I  L   ~  F   �  4     "   G     j     |     �     �     �     �     �       0   #  �  T  #     +   &  =   R  7   �  `   �  [   )  4   �  $   �     �     �  "        +     @     P     b     y     �     
                                   	                                                             CommentA system restart is required CommentAdditional drivers can be installed CommentAn application has crashed on your system (now or in the past) CommentControl the notifications for system helpers CommentExtra packages can be installed to enhance application functionality CommentExtra packages can be installed to enhance system localization CommentSoftware upgrade notifications are available CommentSystem Notification Helper NameApport Crash NameDriver Enhancement NameLocalization Enhancement NameNotification Helper NameOther Notifications NameReboot Required NameRestricted Install NameUpgrade Hook X-KDE-KeywordsNotify,Alerts,Notification,popups Project-Id-Version: kubuntu-notification-helper
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-04-01 17:07+0000
PO-Revision-Date: 2014-04-01 17:34+0000
Last-Translator: Gio <gio.scino@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:48+0000
X-Generator: Launchpad (build 18115)
 È richiesto il riavvio del sistema Possono essere installati driver aggiuntivi Un'applicazione si è bloccata nel sistema (ora o in passato) Controllo delle notifiche per gli assistenti di sistema Possono essere installati pacchetti aggiuntivi per migliorare le funzionalità dell'applicazione Possono essere installati pacchetti aggiuntivi per migliorare la localizzazione del sistema Sono disponibili notifiche di aggiornamento software Assistente alle notifiche di sistema Crash di Apport Miglioramento dei driver Miglioramento della localizzazione Assistente notifiche Altre notifiche Richiesto riavvio Installazione limitata Connessione di aggiornamento Notifica,Avvisi,Notifiche,popup 