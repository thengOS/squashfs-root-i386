��    	     d  �  �       �+     �+     �+  
   �+     �+  *   �+      ,  %   3,  %   Y,  U   ,     �,  #   �,  *   -  A   7-  H  y-  $   �.  3   �.     /  &   ;/  ~   b/  E   �/  0   '0  .   X0     �0     �0     �0     �0     �0     �0     �0     1  #   1  0   @1  '   q1     �1  *   �1  *   �1  3   
2  -   >2     l2  "   |2     �2  "   �2     �2  ,   �2     *3     @3  &   W3     ~3     �3     �3  #   �3  .   �3     4  	    4     *4  ,   ;4     h4     }4  -   �4  .   �4  (   �4     5     5     *5     B5     U5     f5     }5     �5     �5     �5     �5     �5     6     6     36     K6     h6     |6     �6     �6     �6     �6     �6     �6     	7      "7     C7     ^7     m7     �7  *   �7     �7  '   �7     �7  +   8     E8     ^8  s   r8  +   �8     9  '   &9  :   N9  )   �9     �9     �9     �9     :     :  -   ;:  +   i:  1   �:     �:     �:  "   �:     !;  -   >;  9   l;     �;     �;     �;     �;     <     %<  '   9<  N   a<     �<  )   �<  !   �<  0   =     G=     Y=     m=  	   u=     =     �=     �=     �=     �=  5   �=  "   >     %>  D   ,>  $   q>     �>     �>  =   �>  :   �>     2?     J?     R?     Z?     l?     �?     �?      �?     �?     �?  :   @  *   S@  ;   ~@  *   �@  +   �@     A     A      A     2A     >A     \A     sA  $   |A     �A  '   �A  )   �A      B  .   .B  3   ]B  2   �B     �B  C   �B  L   !C  .   nC  +   �C  &   �C     �C     �C     D     /D     OD     XD  �   dD  5   IE  O   E     �E     �E     F     !F  $   6F     [F  [   dF  +   �F     �F      G  .   -G      \G     }G  #   �G     �G  (   �G     �G  9   H  (   SH  (   |H  1   �H  ,   �H  "   I  %   'I  #   MI  #   qI  (   �I     �I  +   �I     
J     J  %   +J     QJ  1   iJ     �J     �J     �J     �J  ,   �J  /   K  "   3K     VK     kK     oK     �K     �K  '   �K  2   �K  '   L  2   /L     bL     yL     �L     �L     �L     �L     �L     �L     M  #   
M     .M  /   IM     yM     �M  "   �M     �M  =   �M     "N     3N  
   RN     ]N  >   |N  1   �N     �N     O     "O     5O     OO     cO  $   |O     �O  %   �O     �O     �O     �O  !   �O      P     >P  "   EP     hP     oP  "   {P  %   �P  &   �P     �P     �P  D   
Q     OQ  	   [Q  4   eQ     �Q  :   �Q     �Q     �Q  '   �Q     'R  (   9R     bR     pR     �R     �R  
   �R     �R     �R  "   �R     �R  "   �R  #   "S  #   FS  +   jS  V   �S     �S     �S  %   �S     "T  	   4T     >T     VT  D   tT  :   �T  �   �T  0   �U  &   �U  v   �U  Y   TV  +   �V  2   �V  �   W     �W     �W     �W     �W     �W     �W     	X     X  /   *X     ZX     zX     �X  !   �X     �X     �X  %   �X     Y     5Y     CY  3   ]Y  H   �Y  $   �Y  2   �Y  '   2Z  (   ZZ  $   �Z  *   �Z  >   �Z  %   [  O   8[     �[     �[     �[     �[  ;   �[  5   /\     e\     ~\     �\     �\     �\     �\     �\     �\     ]     %]     <]     N]     U]     b]  #   o]  &   �]     �]  +   �]     �]  	   ^     ^      ^  
   0^  	   ;^     E^     Y^     _^  !   f^     �^     �^  !   �^     �^  #   �^  )   _  "   7_  "   Z_      }_     �_     �_     �_     �_  0   �_     	`  
   `      `  )   5`     _`     |`     �`     �`     �`  !   �`     �`     �`     �`     �`     a  6   a      Sa     ta     }a     �a  4   �a     �a     �a     �a     b  %   b     7b     :b  	   >b     Hb     [b     gb  #   b     �b     �b     �b     �b     c     c     2c     ;c     Mc  
   Tc     _c     mc  %   �c     �c     �c     �c  (   �c     d  !   &d  (   Hd  /   qd  "   �d     �d     �d     �d  (   e     =e     Te     pe  %   �e  %   �e     �e      �e     f     1f     Jf  0   hf     �f     �f  -   �f     �f      g     4g  "   Mg  #   pg     �g  %   �g  *   �g     �g     h  "   5h     Xh     th     �h  *   �h  %   �h  )   �h  .   !i     Pi  "   ii     �i  %   �i  '   �i     �i     j     'j     ?j     ^j     yj     �j     �j      �j     �j     k     k     !k     %k     *k  �  7k  "   m     6m     Jm     Vm  0   km     �m  '   �m  &   �m  ]   n     in  -   �n  3   �n  O   �n  �  4o  *   �p  =   �p  &   %q  9   Lq  �   �q  K   r  0   gr  <   �r     �r     �r     s     s  #   *s     Ns     is     ps  "   �s  3   �s  0   �s  *   t  8   =t  2   vt  >   �t  1   �t     u  (   *u  (   Su  %   |u      �u  (   �u     �u     v  >   v     ]v     yv     �v  +   �v  A   �v     w  	   w     #w  /   9w     iw     ~w  ?   �w  @   �w  (   x  	   ?x     Ix     Yx     tx     �x     �x  )   �x  )   �x     y     *y     =y     Ry     fy     sy     �y  !   �y     �y      �y     �y     �y      z     z  )   1z     [z     vz  -   �z     �z     �z     �z     {  6   {  '   P{  +   x{  #   �{  :   �{  "   |     &|  �   ;|  3   �|     }  -   *}  \   X}  2   �}  /   �}  #   ~  .   <~  "   k~  &   �~  9   �~  6   �~  ;   &  "   b     �  &   �     �  8   �  F   "�     i�  &   ��     ��     ǀ     �     �  .   !�  h   P�      ��  1   ځ  +   �  J   8�     ��     ��  
   ��     ��  
   ��     ˂  #   ݂     �     �  ?   �  3   Z�     ��  _   ��  .   ��     $�      3�  P   T�  N   ��     �     �     �     !�  "   9�  /   \�  #   ��  *   ��  !   ۅ  #   ��  T   !�  6   v�  N   ��  9   ��  >   6�     u�     }�     ��     ��  "   ��     χ     �  &   �     �  ?   5�  6   u�  .   ��  6   ۈ  L   �  C   _�      ��  K   ĉ  c   �  ?   t�  =   ��  2   �     %�  $   *�  "   O�  %   r�     ��     ��  "  ��  >   Ԍ  [   �  
   o�     z�  "   ��     ��  *   ʍ     ��  b   ��  ,   `�  "   ��  #   ��  7   Ԏ  &   �     3�  .   M�  )   |�  3   ��  (   ڏ  H   �  2   L�  +   �  -   ��  (   ِ      �  &   #�     J�  /   c�  5   ��  +   ɑ  1   ��     '�  #   /�  %   S�  "   y�  9   ��     ֒     �     �     �  C    �  ?   d�  &   ��     ˓     �     �     �  !   �  1   <�  ?   n�  0   ��  >   ߔ     �  -   <�     j�  .   ��     ��     Õ     ̕     ٕ  	   �  '   �     �  5   :�     p�     ��  -   ��  +   Ж  \   ��     Y�  %   j�     ��  $   ��  K   ɗ  4   �  "   J�  #   m�  $   ��     ��     Ԙ      �  )   �     =�  -   E�     s�  *   {�     ��  -   ��  )   ܙ     �  -   �     <�     D�  '   U�      }�      ��     ��     Ț  L   �  
   0�  
   ;�  A   F�     ��  E   ��     �     �  +   ��      �  !   :�     \�     q�  
   ��     ��  	   ��     ��  "   ��  .   ՜  -   �  .   2�  1   a�  2   ��  F   Ɲ  a   �     o�     v�  ,   �     ��     ��     Ǟ  "   ��  C   �  =   G�  �   ��  8   #�  "   \�  }   �  \   ��  9   Z�  =   ��  �   ҡ      k�     ��  
   ��     ��  "   ��     ܢ  #   �  )   �  .   ?�     n�     ��     ��  &   ��     ԣ     �  3   �  &   ;�     b�     r�  ?   ��  Y   Τ  4   (�  M   ]�  5   ��  0   �  0   �  2   C�  S   v�  6   ʦ  U   �  "   W�  !   z�  '   ��  $   ħ  P   �  D   :�  &   �  	   ��     ��     ¨     �     ��     �  0   6�  +   g�     ��     ��     ͩ     թ     �  6   �  0   ;�     l�  9   ��     ��  	   Ǫ     Ѫ     �  
   ��  	   
�  !   �     6�     =�  "   E�     h�     ��  2   ��     ̫  0   �  :   �  3   X�  5   ��  #   ¬  	   �     �     �     �  /   6�     f�  
   r�     }�  6   ��  2   ˭     ��     �     �      �  $   9�     ^�     w�     ��     ��     ɮ  >   ܮ  "   �     >�     G�     O�  H   [�     ��  &   ��  &   �     �  ,   �     8�     ;�     ?�     G�     ]�     j�  +   ��  )   ��  $   ٰ     ��  "   �     .�     @�     ^�     g�     ��     ��     ��     ��  3   ̱  )    �     *�     >�  *   F�     q�     ��  3   ��  -   ղ  #   �     '�     E�     Y�     s�     ��     ��  )   ɳ  9   �  )   -�     W�  4   l�  +   ��  "   ʹ  (   �  9   �     S�     m�  4   ��     ��  #   յ     ��  +   �  #   :�     ^�  +   u�  =   ��  '   ߶     �  %   �     E�  "   a�  .   ��  .   ��  0   �  4   �  9   H�  )   ��  &   ��  &   Ӹ  %   ��  4    �     U�  )   q�     ��  2   ��  %   ��  %   �     9�     Y�  $   w�     ��     ��     Ӻ     ֺ     ں     ߺ         �      �    m             �  A       G     W   ,  
                      y   &  �      �  1   �   _   �   O  j  �       8  �     �  G  �         '  X   �  �  �   .       �   D      &               �  B         �  �  	  [  �   �   �   l       �  �    �       �  \   +         �       u   �       F            �  �       �   �    z       Z      �  s   s  )  o   l  =   �              �   `       �   P  �   v     �  a  ]       |  k  �     �  �   9   �   �   J   �      >           f  }   �  �   �     �   �           �   U       �   �   .  T  �   �   j   �         �  �  �       �      �   �     U   a   �  I   �       �      :      �               �   �  -  (  �  �   <      I        �  V  �  Q  �      �   o  �   k   g             �       O   +          3  z    �  ^                     �              �      �       !     T     L    S   9  Y       C  6   n      b   �   �   �   �  �  �  �   �  t   �   �      h   �  �      }  c          x   2  A  �   �   �      @      �   g   ?          �   �   t  {  �             "                q      �      �         �       �          �       m  N           *  �   �      �   {       �          �               F       �  �  R  �   �   \  �  �   �       K  c   �  E           ;  �   ;   p            �   Q   4          �  h  �          �   �                      �     �           �   �   �  �  ?   �   �  w  �  �   �  _  �  �          $        �  �  0   �        H       �  �      X  �   �   �   V       �   y         1  W      �   '       �               �   �   �  N  �     ~   �   �      	  �       f   �  �  �   �      �      �  �   �  �  r   �  /   �  K   5    �              �      n   �   �       �      �   �   �   8           q   C       7   u  �      �  �   �  �  d      ~        P   	       �   (   D  �   J  �  �  %   �          �   �   �       !    ^  �   �   /  �      e  �  �   i  -   �  �       �   �  
   �   R       r    �                     �         Z     �  �  #   �   �          �       �      ]  B          �       S      L   �      �   �          M       �      �  �       �   |           �  �          w   2   �   4   Y  H  )                   E  �      �  �      #      [   e   �  �   �   �  �  �                  v  �   %  �   i       �  "   �   *   �  <   $   �   x      �  �  �  @  d   M      ,      �      �    �           �  �   �  �   �   �  �  0  �  >  �  �      :   7         p   �   �   �       �         �  6             `  �   �  3   b  =  �   �   �        �         5           �   	%Q (@i #%i, mod time %IM)
 	<@f metadata>
 	Using %s
 	Using %s, %s
 
	while trying to add journal to device %s 
	while trying to create journal 
	while trying to create journal file 
	while trying to open journal on %s
 

%s: UNEXPECTED INCONSISTENCY; RUN fsck MANUALLY.
	(i.e., without -a or -p options)
 
  Inode table at  
%s: %s: error reading bitmaps: %s
 
%s: ***** FILE SYSTEM WAS MODIFIED *****
 
%s: ********** WARNING: Filesystem still has errors **********

 
Emergency help:
 -p                   Automatic repair (no questions)
 -n                   Make no changes to the filesystem
 -y                   Assume "yes" to all questions
 -c                   Check for bad blocks and add them to the badblock list
 -f                   Force checking even if filesystem is marked clean
 
Filesystem too small for a journal
 
If the @b is really bad, the @f can not be fixed.
 
Interrupt caught, cleaning up
 
Journal size too big for filesystem.
 
The bad @b @i has probably been corrupted.  You probably
should stop now and run e2fsck -c to scan for bad blocks
in the @f.
 
The device apparently does not exist; did you specify it correctly?
 
The filesystem already has sparse superblocks.
 
Warning, had trouble writing out superblocks.   %s superblock at    Block bitmap at    Free blocks:    Free inodes:   (check after next mount)  (check in %ld mounts)  (y/n)  Group descriptor at   contains a file system with errors  has been mounted %u times without being checked  has gone %u days without being checked  was not cleanly unmounted %d-byte blocks too big for system (max %d) %s %s: status is %x, should never happen.
 %s @o @i %i (uid=%Iu, gid=%Ig, mode=%Im, size=%Is)
 %s is entire device, not just one partition!
 %s is mounted;  %s is not a block special device.
 %s is not a journal device.
 %s: %s filename nblocks blocksize
 %s: ***** REBOOT LINUX *****
 %s: Error %d while executing fsck.%s for %s
 %s: e2fsck canceled.
 %s: journal too short
 %s: no valid journal superblock found
 %s: recovering journal
 %s: too many arguments
 %s: too many devices
 %s: wait: No more child process?!?
 %s: won't do journal recovery while read-only
 %s? no

 %s? yes

 %u block groups
 %u blocks per group, %u fragments per group
 %u inodes per group
 %u inodes scanned.
 '.' @d @e in @d @i %i is not NULL terminated
 '..' @d @e in @d @i %i is not NULL terminated
 '..' in %Q (%i) is %P (%j), @s %q (%d).
 (NONE) (no prompt) , Group descriptors at  , Inode bitmap at  , check forced.
 --waiting-- (pass %d)
 -O may only be specified once -o may only be specified once /@l is not a @d (ino=%i)
 /@l not found.   <Reserved inode 10> <Reserved inode 9> <The NULL inode> <The bad blocks inode> <The boot loader inode> <The group descriptor inode> <The journal inode> <The undelete directory inode> <n> <y> = is incompatible with - and +
 @A @b @B (%N): %m
 @A @b buffer for relocating %s
 @A @i @B (%N): %m
 @A icount structure: %m
 @A new @d @b for @i %i (%s): %m
 @D @i %i has zero dtime.   @E @L to '.'   @E @L to @d %P (%Di).
 @E @L to the @r.
 @E has a non-unique filename.
Rename to %s @E has filetype set.
 @E has illegal characters in its name.
 @E is duplicate '..' @e.
 @E points to @i (%Di) located in a bad @b.
 @I @i %i in @o @i list.
 @I @o @i %i in @S.
 @S @b_size = %b, fragsize = %c.
This version of e2fsck does not support fragment sizes different
from the @b size.
 @S @bs_per_group = %b, should have been %c
 @b @B differences:  @b @B for @g %g is not in @g.  (@b %b)
 @f contains large files, but lacks LARGE_FILE flag in @S.
 @f did not have a UUID; generating one.

 @g %g's @b @B (%b) is bad.   @g %g's @b @B at %b @C.
 @g %g's @i @B (%b) is bad.   @g %g's @i @B at %b @C.
 @g %g's @i table at %b @C.
 @h %i has a tree depth (%N) which is too big
 @h %i has an unsupported hash version (%N)
 @h %i uses an incompatible htree root node flag.
 @i %i (%Q) is an @I @b @v.
 @i %i (%Q) is an @I FIFO.
 @i %i (%Q) is an @I character @v.
 @i %i (%Q) is an @I socket.
 @i %i has INDEX_FL flag set but is not a @d.
 @i %i has INDEX_FL flag set on @f without htree support.
 @i %i has illegal @b(s).   @i %i has imagic flag set.   @i %i is a @z @d.   @i %i is too big.   @i %i, i_size is %Is, @s %N.   @i @B differences:  @i @B for @g %g is not in @g.  (@b %b)
 @i table for @g %g is not in @g.  (@b %b)
WARNING: SEVERE DATA LOSS POSSIBLE.
 @j is not regular file.   @j version not supported by this e2fsck.
 @p @h %d (%q): bad @b number %b.
 @r has dtime set (probably due to old mke2fs).   @r is not a @d.   @r not allocated.   ABORTED ALLOCATED Abort Aborting....
 Adding journal to device %s:  Aerror allocating Allocate BLKFLSBUF ioctl not supported!  Can't flush buffers.
 Backing up @j @i @b information.

 Backup Bad @b @i has an indirect @b (%b) that conflicts with
@f metadata.   Bad block %u out of range; ignored.
 Bbitmap Begin pass %d (max = %lu)
 Block %b in the primary @g descriptors is on the bad @b list
 Block %d in primary superblock/group descriptor area bad.
 Block size=%u (log=%u)
 CLEARED CREATED Can not continue. Can't find external @j
 Cannot continue, aborting.

 Cannot proceed without a @r.
 Cconflicts with some other fs @b Checking all file systems.
 Checking blocks %lu to %lu
 Checking for bad blocks (non-destructive read-write test)
 Checking for bad blocks (read-only test):  Checking for bad blocks in non-destructive read-write mode
 Checking for bad blocks in read-only mode
 Checking for bad blocks in read-write mode
 Clear Clear @j Clear HTree index Clear inode Clone multiply-claimed blocks Connect to /lost+found Continue Corruption found in @S.  (%s = %N).
 Could not expand /@l: %m
 Could this be a zero-length partition?
 Couldn't allocate block buffer (size=%d)
 Couldn't allocate header buffer
 Couldn't allocate memory for filesystem types
 Couldn't allocate memory to parse journal options!
 Couldn't allocate path variable in chattr_dir_proc Couldn't clone file: %m
 Couldn't determine device size; you must specify
the size manually
 Couldn't determine device size; you must specify
the size of the filesystem
 Couldn't find journal superblock magic numbers Couldn't find valid filesystem superblock.
 Couldn't parse date/time specifier: %s Create Creating journal (%d blocks):  Creating journal inode:  Creating journal on device %s:  Ddeleted Delete file Device size reported to be zero.  Invalid partition specified, or
	partition table wasn't reread after running fdisk, due to
	a modified partition being busy and in use.  You may need to reboot
	to re-read your partition table.
 Directories count wrong for @g #%g (%i, counted=%j).
 Disk write-protected; use the -n option to do a read-only
check of the device.
 Do you really want to continue Duplicate @E found.   Duplicate or bad @b in use!
 E@e '%Dn' in %p (%i) ERROR: Couldn't open /dev/null (%s)
 EXPANDED Either all or none of the filesystem types passed to -t must be prefixed
with 'no' or '!'.
 Empty directory block %u (#%d) in inode %u
 Error creating /@l @d (%s): %m
 Error creating root @d (%s): %m
 Error determining size of the physical @v: %m
 Error iterating over @d @bs: %m
 Error moving @j: %m

 Error reading @d @b %b (@i %i): %m
 Error reading @i %i: %m
 Error reading block %lu (%s) while %s.   Error reading block %lu (%s).   Error storing @i count information (@i=%i, count=%N): %m
 Error validating file descriptor %d: %s
 Error while adjusting @i count on @i %i
 Error while iterating over @bs in @i %i (%s): %m
 Error while iterating over @bs in @i %i: %m
 Error while scanning @is (%i): %m
 Error while scanning inodes (%i): %m
 Error while trying to find /@l: %m
 Error writing @d @b %b (@i %i): %m
 Error writing block %lu (%s) while %s.   Error writing block %lu (%s).   Error: ext2fs library version out of date!
 Expand Extending the inode table External @j does not support this @f
 External @j has bad @S
 External @j has multiple @f users (unsupported).
 FILE DELETED FIXED Ffor @i %i (%Q) is Filesystem label=%s
 Filesystem larger than apparent device size. Filesystem's UUID not found on journal device.
 Finished with %s (exit status %d)
 First data block=%u
 Fix Flags of %s set as  Force rewrite Fragment size=%u (log=%u)
 Free @bs count wrong (%b, counted=%c).
 Free @bs count wrong for @g #%g (%b, counted=%c).
 Free @is count wrong (%i, counted=%j).
 Free @is count wrong for @g #%g (%i, counted=%j).
 From block %lu to %lu
 Get a newer version of e2fsck! Group %lu: (Blocks  Group descriptors look bad... HTREE INDEX CLEARED IGNORED INODE CLEARED Ignore error Iillegal Illegal number for blocks per group Illegal number of blocks!
 Internal error: couldn't find dir_info for %i.
 Invalid EA version.
 Invalid UUID format
 Invalid filesystem option set: %s
 Invalid mount option set: %s
 Journal dev blocksize (%d) smaller than minimum blocksize %d
 Journal removed
 Journal superblock not found!
 Lis a link MULTIPLY-CLAIMED BLOCKS CLONED Maximum of one test_pattern may be specified in read-only mode Memory used: %d, elapsed time: %6.3f/%6.3f/%6.3f
 Missing '.' in @d @i %i.
 Missing '..' in @d @i %i.
 Moving inode table Must use '-v', =, - or +
 No room in @l @d.   Optimizing directories:  Out of memory erasing sectors %d-%d
 Pass 1 Pass 1: Checking @is, @bs, and sizes
 Pass 2 Pass 2: Checking @d structure
 Pass 3 Pass 3: Checking @d connectivity
 Pass 3A: Optimizing directories
 Pass 4 Pass 4: Checking reference counts
 Pass 5 Peak memory Please run 'e2fsck -f %s' first.

 Please run e2fsck on the filesystem.
 Possibly non-existent or swap device?
 Primary Proceed anyway? (y,n)  Programming error?  @b #%b claimed for no reason in process_bad_@b.
 RECONNECTED RELOCATED Random test_pattern is not allowed in read-only mode Reading and comparing:  Recovery flag not set in backup @S, so running @j anyway.
 Recreate Relocate Relocating @g %g's %s from %b to %c...
 Relocating blocks Restarting e2fsck from the beginning...
 Run @j anyway Running command: %s
 SALVAGED SPLIT SUPPRESSED Salvage Scanning inode table Setting current mount count to %d
 Setting filetype for @E to %N.
 Setting maximal mount count to %d
 Setting reserved blocks gid to %lu
 Setting reserved blocks uid to %lu
 Setting time filesystem last checked to %s
 Special (@v/socket/fifo/symlink) file (@i %i) has immutable
or append-only flag set.   Split Ssuper@b Superblock backups stored on blocks:  Suppress messages TRUNCATED Testing with pattern 0x Testing with random pattern:  The -c and the -l/-L options may not be both used at the same time.
 The -t option is not supported on this version of e2fsck.
 The @f size (according to the @S) is %b @bs
The physical size of the @v is %c @bs
Either the @S or the partition table is likely to be corrupt!
 The Hurd does not support the filetype feature.
 The filesystem already has a journal.
 The filesystem revision is apparently too high for this version of e2fsck.
(Or the filesystem superblock is corrupt)

 The needs_recovery flag is set.  Please run e2fsck before clearing
the has_journal flag.
 The primary @S (%b) is on the bad @b list.
 This doesn't bode well, but we'll try to go on...
 This filesystem will be automatically checked every %d mounts or
%g days, whichever comes first.  Use tune2fs -c or -i to override.
 Too many illegal @bs in @i %i.
 Truncate UNLINKED Unable to resolve '%s' Unhandled error code (0x%x)!
 Unknown pass?!? Unlink Updating inode references Usage: %s [-F] [-I inode_buffer_blocks] device
 Usage: %s [-RVadlv] [files...]
 Usage: %s [-r] [-t]
 Usage: %s disk
 Usage: e2label device [newlabel]
 Usage: mklost+found
 Version of %s set as %lu
 WARNING: bad format on line %d of %s
 WARNING: couldn't open %s: %s
 WILL RECREATE Warning!  %s is mounted.
 Warning... %s for device %s exited with signal %d.
 Warning: %d-byte blocks too big for system (max %d), forced to continue
 Warning: Group %g's @S (%b) is bad.
 Warning: blocksize %d not usable on most systems.
 Warning: could not erase sector %d: %s
 Warning: could not read @b %b of %s: %m
 Warning: could not read block 0: %s
 Warning: could not write @b %b for %s: %m
 Warning: illegal block %u found in bad block inode.  Cleared.
 Warning: label too long, truncating.
 Warning: skipping journal recovery because doing a read-only filesystem check.
 Weird value (%ld) in do_read
 While reading flags on %s While reading version on %s Writing inode tables:  Writing superblocks and filesystem accounting information:  You must have %s access to the filesystem or be root
 Zeroing journal device:  aborted aextended attribute bad gid/group name - %s bad inode map bad interval - %s bad mounts count - %s bad reserved block ratio - %s bad reserved blocks count - %s bad uid/user name - %s bad version - %s
 bblock block bitmap block device blocks per group count out of range blocks per group must be multiple of 8 blocks to be moved can't allocate memory for test_pattern - %s cancelled!
 ccompress character device check aborted.
 ddirectory directory directory inode map done
 done

 done                            
 during ext2fs_sync_device during seek during test data write, block %lu e2label: cannot open %s
 e2label: cannot seek to superblock
 e2label: cannot seek to superblock again
 e2label: error reading superblock
 e2label: error writing superblock
 e2label: not an ext2 filesystem
 eentry elapsed time: %6.3f
 empty dir map empty dirblocks ext2fs_new_@i: %m while trying to create /@l @d
 ffilesystem filesystem fsck: %s: not found
 fsck: cannot check %s: fsck.%s not found
 getting next inode from scan ggroup hHTREE @d @i iinode imagic inode map in malloc for bad_blocks_filename in-use block map in-use inode map inode bitmap inode in bad block map inode table internal error: couldn't lookup EA inode record for %u it's not safe to run badblocks!
 jjournal journal llost+found mke2fs forced anyway.  Hope /etc/mtab is incorrect.
 mmultiply-claimed multiply claimed block map multiply claimed inode map nN need terminal for interactive repairs no no
 oorphaned opening inode scan pproblem in reading directory block reading indirect blocks of inode %u reading inode and block bitmaps reading journal superblock
 regular file regular file inode map reserved blocks returned from clone_file_block rroot @i size of inode=%d
 socket sshould be symbolic link time: %5.2f/%5.2f/%5.2f
 unable to set superblock flags on %s
 unknown file type with mode 0%o unknown os - %s vdevice while adding filesystem to journal on %s while allocating buffers while allocating zeroizing buffer while beginning bad block list iteration while calling ext2fs_block_iterate for inode %d while checking ext3 journal for %s while clearing journal inode while creating /lost+found while creating root dir while determining whether %s is mounted. while doing inode scan while expanding /lost+found while getting next inode while getting stat information for %s while initializing journal superblock while looking up /lost+found while marking bad blocks as used while opening %s for flushing while opening inode scan while printing bad block list while processing list of bad blocks from program while reading bitmaps while reading flags on %s while reading in list of bad blocks from file while reading journal inode while reading journal superblock while reading root inode while reading the bad blocks inode while recovering ext3 journal of %s while resetting context while retrying to read bitmaps for %s while sanity checking the bad blocks inode while setting bad block inode while setting flags on %s while setting root inode ownership while setting version on %s while starting inode scan while trying popen '%s' while trying to allocate filesystem tables while trying to determine device size while trying to determine filesystem size while trying to determine hardware sector size while trying to flush %s while trying to initialize program while trying to open %s while trying to open external journal while trying to open journal device %s
 while trying to re-open %s while trying to resize %s while trying to stat %s while updating bad block inode while writing block bitmap while writing inode bitmap while writing inode table while writing journal inode while writing journal superblock while writing superblock will not make a %s here!
 yY yes yes
 zzero-length Project-Id-Version: e2fsprogs 1.36-b2
Report-Msgid-Bugs-To: tytso@alum.mit.edu
POT-Creation-Date: 2015-05-17 20:31-0400
PO-Revision-Date: 2014-05-12 09:48+0000
Last-Translator: Gianfranco Frisani <gfrisani@libero.it>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 16:06+0000
X-Generator: Launchpad (build 18115)
 	%Q (@i #%i, ultima modifica %IM)
 	<metadata del @f>
 	Usando %s
 	Utilizzando %s, %s
 
	cercando di agigungere il journal al device %s 
	cercando di creare il journal 
	cercando di creare il file di journal 
	tentando di aprire il journal in %s
 

%s: INCONSISTENZA INASPETTATA: ESEGUIRE fsck MANUALMENTE.
	(es., senza le opzioni -a o -p)
 
  Tavola degli inode a  
%s: %s: errore leggendo le mappe di bit: %s
 
%s: ***** IL FILESYSTEM E' STATO MODIFICATO *****
 
%s: ********** ATTENZIONE: Il filesystem contiene ancora errori ************

 
Aiuto di emergenza:
 -p                   Riparazione automatica (senza domande)
 -n                   Non effettuare cambiamenti nel filesystem
 -y                   Risposta affermativa a tutte le domande
 -c                   Cerca blocchi non validi, ed aggiungili alla lista dei blocchi non validi
 -f                   Forza il controllo anche se il filesystem è segnato come pulito
 
Filesystem troppo piccolo per un journal
 
Se il @b è molto corrotto, il @f non può essere riparato.
 
Rilevato interrupt, pulizia in corso
 
Dimensione del journal troppo grande per il filesystem.
 
L'@i del @b non valido è stato probabilmente corrotto. E' consigliabile
fermarsi ora ed eseguire e2fsck -c per cercare blocchi non validi
nel @f.
 
Sembra che il dispositivo non esista; è stato specificato correttamente?
 
Il filesystem ha già dei superblocchi sparsi.
 
Attenzione, problemi durante la scrittura dei superblocchi.   superblocco %s a    Mappa dei bit di blocco a    Blocchi liberi:    Inode liberi:   (controllo dopo il prossimo mount)  (controllo tra %ld mount)  (s/n)  Descrittori di gruppo a   contiene un filesystem con errori  è stato montato %u volte senza essere controllato  non è stato controllato negli ultimi %u giorni  non è stato smontato in maniera corretta blocchi di %d byte troppo grandi per il sistema (max %d) %s %s: lo stato è %x, non dovrebbe avvenire mai.
 %s @o @i %i (uid=%Iu, gid=%Ig, modalità=%Im, dimensione=%Is)
 %s è un device intero, non solo una partizione.
 %s è montato;  %s non è un device speciale a blocchi.
 %s non è un dispositivo di journaling.
 %s: %s NOMEFILE N_BLOCCHI DIM_BLOCCO
 %s: ***** RIAVVIARE LINUX *****
 %s: Errore %d eseguendo fsck. %s per %s
 %s: e2fsck cancellato.
 %s: journal troppo corto
 %s: non è stato trovato alcun superblocco del journal valido
 %s: ripristino del journal
 %s: troppi parametri
 %s: troppi device
 %s: aspetta: Non ci sono processi figli?!?
 %s: impossibile ripritinare il journal in modalità sola lettura
 %s? no

 %s? sì

 %u gruppi di blocchi
 %u blocchi per gruppo, %u frammenti per gruppo
 %u inode per gruppo
 %u inode scansionati.
 L'@e della @d '.' nell'@i %i della @d non è terminato da NULL
 L'@e della @d '..' nell'@i %i della @d non è terminato da NULL
 '..' in %Q (%i) è %P (%j), @s %q (%d).
 (NESSUNO) (nessun prompt) , Descrittori di gruppo a  , mappa dei bit inode a  , controllo forzato.
 --attesa-- (passo %d)
 -O può essere specificata solo una volta -o può essere specificata solo una volta /@l non è una @d (ino=%i)
 /@l non trovata.   <Inode riservato 10> <Inode riservato 9> <Inode NULL> <Inode dei bad block> <Inode del boot loader> <Inode del descrittore di gruppo> <Inode del journal> <Inode della directory undelete> <n> <s> = è incompatibile con + e -
 @A @b @B (%N): %m
 @A il buffer @b per la rilocazione di %s
 @A la @B dell'@i (%N): %m
 @A la struttura icount: %m
 @A un nuovo @b della @d per l'@i %i (%s): %m
 L'@i @D  %i ha dtime zero.   L'@E @L a '.'   L'@E @L alla @d %P (%Di).
 L'@E @L all'@r.
 @E ha un nome file non univoco.
Cambiare il nome in %s E' impostato il tipo di file per l'@E.
 L'@E ha caratteri non validi nel suo nome.
 L'@E è un duplicato dell'@e '..'.
 L'@E punta all'@i (%Di), posizionato in un @b non valido.
 @i @I %i nella lista degli @i @o.
 @i @o @I %i nel @S.
 Dimensione del @b del @S = %b, dimensione frammento = %c.
Questa versione di e2fsck non supporta dimensioni di frammento
differenti dalla dimensione del @b.
 @S @b(i)_per_gruppo = %b, avrebbe dovuto essere %c
 Differenze nella @B dei @b:  @B del @b per il @g %g non è in @g. (@b %b)
 Il @f contiene files di grandi dimensioni, ma nel @S non è specificato il flag LARGE_FILE.
 il @f non aveva un UUID; generazione di un UUID.

 La @B dei @b(i) del @g %g (%b) non è valida.   La @B dei @b(i) del @g %g a %b @C.
 La @B degli @i del @g %g (%b) non è valida.   La @B degli @i del @g %g a %b @C.
 La tavola degli @i del @g %g a %b @C.
 L'@h %i ha un livello di profondità (%N) troppo elevato
 L'@h %i ha una versione dell'hash non supportata (%N)
 L'@h %i usa un flag di nodo htree di root non compatibile.
 L'@i %i (%Q) è un @v a @b(i) @I.
 L'@i %i (%Q) è una FIFO @I.
 L'@i %i (%Q) è un @v a caratteri @I.
 L'@i %i (%Q) è un socket @I.
 L'@i %i ha il flag INDEX_FL impostato, ma non è un @d.
 L'@i %i ga il flag INDEX_FL impostato nel @f senza il supporto htree.
 L'@i %i ha @b(i) illegali.   L'@i %i ha il flag imagic impostato.   L'@i %i è una @d a @z.   L'@i %i è troppo grande.   @i %i, i_size è %Is, @s %N.   Differenze nella @B degli @i:  @B dell'@i per il @g %g non è in @g. (@b %b)
 La tavola degli @i per il @g %g non è in @g. (@b %b)
ATTENZIONE: SONO POSSIBILI GRAVI PERDITE DI DATI.
 Il @j non è un file regolare.   Versione del @j non supportata da questo e2fsck.
 @p un @h %d (%q): @b non valido numero %b.
 L'@r ha il dtime impostato (probabilmente a causa di un vecchio mke2fs).   L'@r non è una @d.   @r non allocato.   INTERROTTO ALLOCATO Interrompi Annullamento....
 Aggiunta del journal al device %s:  Aerrore allocando Alloca Ioctl BLKFLSBUF non supportato! Impossibile svuotare i buffer.
 Esecuzione del backup dell'informazione @j @i @b.

 Backup L'@i del @b non valido ha un @b indiretto (%b) che entra in
conflitto con il metadata del @f.   Bad block %u fuori dall'intervallo; ignorato.
 Bmappa dei bit Inizio del passo %d (max = %lu)
 Il blocco %b nei descrittori primari del @g è nella lista dei @b(i) non validi
 Blocco %d non valido nel superblocco primario/area del descrittore di gruppo.
 Dimensione blocco=%u (log=%u)
 PULITO CREATO Impossibile continuare. Impossibile trovare il @j esterno
 Impossibile continuare, operazione annullata.

 Impossibile procedere senza un @r.
 Centra in conflitto con altri @b(i) del fs Controllo di tutti i filesystem.
 Controllo dei blocchi da %lu a %lu
 Ricerca dei blocchi non validi (test in moalità lettura-scrittura non distruttiva)
 Ricerca dei blocchi non validi (test a sola lettura):  Ricerca dei blocchi non validi in modalità lettura-scrittura non distruttiva
 Ricerca dei blocchi non validi in modalità sola lettura
 Ricerca dei blocchi non validi in modalità lettura-scrittura
 Pulisci Azzerare @j Pulisci indice HTree Pulisci inode Clona blocchi richiesti più volte Connetti a /lost+found Continua Trovata corruzione nel @S. (%s = %N).
 Impossibile espandere /@l: %m
 E' possibile che questa sia una partizione di dimensione zero?
 Impossibile allocare il buffer blocco (dimensione=%d)
 Impossibile allocare il buffer d'intestazione
 Impossibile allocare memoria per i tipi di filesystem
 Impossibile allocare memoria per fare il parsing delle opzioni del journal!
 Impossibile allocare la variabile di percorso nella chattr_dir_proc Impossibile clonare il file: %m
 Impossibile determinare la dimensione del device:
specificarla manualmente
 Impossibile determinare la dimensione del device: bisogna
specificare la dimensione del filesystem
 Impossibile trovare i magic numbers del superblocco del journal Impossibile trovare un valido superblocco per il filesystem.
 Impossibile comprendere il formato di data/ora: %s Crea Creazione del journal (%d blocchi):  Creazione dell'inode del journal:  Creando il journal per il device %s:  Dcancellato Elimina file Sembra che la dimensione del device sia zero. Specificata partizione non
	valida o la tabella delle partizioni non è stata riletta dopo l'esecuzione
	di fdisk, poiché una partizione modificata era occupata. Potrebbe essere
	necessario riavviare per rileggere la tabella delle partizioni.
 Numero delle directory errato per il @g #%g (%i, contati=%j).
 Disco protetto da scrittura: usare l'opzione -n per controllare
in modalità sola lettura.
 Continuare Trovato @E duplicato.   @b duplicato o non valido in uso!
 E@e '%Dn' in %p (%i) ERRORE: Impossibile aprire /dev/null (%s)
 ESPANSO Tutti o nessun tipo di filesystem passati con l'opzione -t devono essere preceduti
da 'no' o '!'.
 Blocco directory vuoto %u (#%d) in inode %u
 Errore creando la @d /@l (%s): %m
 Errore creando la @d root (%s): %m
 Errore nel determinare la dimensione del @v fisico: %m
 Errore scorrendo i @b(i) delle @d: %m
 Errore spostando @j: %m

 Errore leggendo il @b della @d %b (@i %i): %m
 Errore durante la lettura dell'@i %i: %m
 Errore nel leggere il blocco %lu (%s) durante %s.   Errore nel leggere il blocco %lu (%s).   Errore salvando le informazioni sul numero di @i (@i=%i, numero=%N): %m
 Errore convalidando il descrittore di file %d: %s
 Errore aggiustando il numero @i nell'@i %i
 Errore scorrendo i @b(i) nell'@i %i (%s): %m
 Errore scorrendo i @b(i) nell'@i %i: %m
 Errore analizzando @is (%i): %m
 Errore analizzando gli inode (%i): %m
 Errore cercando /@l: %m
 Errore scrivendo il @b della @d %b (@i %i): %m
 Errore nello scrivere il blocco %lu (%s) mentre %s.   Errore nello scrivere il blocco %lu (%s).   Errore: versione obsoleta della libreria ext2fs!
 Espandi Estensione della tavola degli inode Il @j esterno non supporta questo @f
 Il @j esterno ha un @S non valido
 Il @j esterno ha diversi utenti del @f (non supportato).
 FILE ELIMINATO CORRETTO Fper l'@i %i (%Q) è Etichetta del filesystem=%s
 Il filesystem è più grande della dimensione apparente del device. UUID del filesystem non trovato sul dispositivo di journaling.
 Terminato con %s (stato di uscita %d)
 Primo blocco dati=%u
 Correggi flag di %s impostati come  Forza la riscrittura Dimensione frammento=%u (log=%u)
 Numero dei @b(i) liberi errato (%b, contati=%c).
 Numero dei @b(i) liberi errato per il @g #%g (%b, contati=%c).
 Numero degli @i liberi errato (%i, contati=%j).
 Numero degli @i liberi errato per il @g #%g (%i, contati=%j).
 Dal blocco %lu al blocco %lu
 Utilizzare una versione più nuova di e2fsck. Gruppo %lu: (Blocchi  I descrittori di gruppo sembrano non validi... INDICE HTREE PULITO IGNORATO INODE PULITO Ignora l'errore Iillegale Numero di blocchi per gruppo non valido Numero di blocchi non lecito!
 Errore interno: impossibile trovare dir_info per %i.
 Versione EA non valida.
 Formato UUID non valido
 Set di opzioni del filesystem non valido: %s
 Insieme di opzioni di mount non valido: %s
 Dimensione dei blocchi del device di journaling (%d) minore della dim minima dei blocchi %d
 Journal rimosso
 Superblocco del journal non trovato!
 Lè un collegamento CLONATI BLOCCHI RICHIESTI PIÙ VOLTE E' possibile specificare un solo modello di prova in modalità sola lettura Memoria usata: %d, tempo rimasto: %6.3f/%6.3f/%6.3f
 '.' mancante nell'@i %i della @d.
 '..' mancante nell'@i %i della @d.
 Spostamento della tavola degli inode Si deve usare '-v', =, - o +
 Non c'è spazio nella @d @l   Ottimizzazione delle directory:  Fine memoria cancellando i settori %d-%d
 Passo 1 Passo 1: Controllo di @i, @b(i) e dimensioni
 Passo 2 Passo 2: Analisi della struttura delle @d
 Passo 3 Passo 3: Controllo della connettività di @d
 Passo 3A: Ottimizzazione delle directory
 Passo 4 Pass 4: Controllo del numero dei riferimenti
 Passo 5 Memoria di picco Eseguire innanzitutto "e2fsck -f %s".

 Eseguire e2fsck sul filesystem.
 Device non esistente o di swap?
 Primario Procedere comunque? (s,n)  Errore di programmazione? @b #%b reclamato senza ragione in process_bad_@b.
 RICONNESSO RIALLOCATO Modello di prova casuale non consentito in modalità sola lettura Lettura e confronto:  Flag di recupero non impostato nel @S di backup, eseguo @j comunque.
 Ricrea Rialloca Rilocazione del %s del @g %g da %b a %c...
 Riallocazione dei blocchi Riavvio di e2fsck dall'inizio...
 Eseguire @j comunque Esecuzione del comando: %s
 RECUPERATO DIVISO SOPPRESSO Recupera Scansione della tavola degli inode Impostazione del numero attuale di mount a %d
 Impostazione del tipo di file per l'@E a %N.
 Impostazione del numero massimo di mount a %d
 Impostazione del gid dei blocchi riservati a %lu
 Impostazione dell'uid dei blocchi riservati a %lu
 Impostazione di data ed ora dell'ultimo controllo del filesystem a %s
 Un file (@i %i) speciale (@v/socket/fifo/symlink) ha il flag
immutable o append-only impostato.   Dividi Ssuper@b Backup del superblocco salvati nei blocchi:  Sopprimi messaggi TRONCATO Controllo con modello 0x Controllo con un modello casuale:  Le opzioni -c e -I/-L non possono essere usate contemporaneamente.
 L'opzione -t non è supportata da questa versione di e2fsck.
 La dimensione del @f (secondo il @S) è %b @b(i)
La dimensione fisica del @v è %c @b(i)
È probabile che il @S o la tavola delle partizioni siano corrotti!
 Il kernel Hurd non supporta la caratteristica filetype.
 Il filesystem ha già un journal.
 La revisione del filesystem sembra troppo alta per questa versione di e2fsck.
(O il superblocco del filesystem è corrotto)

 Il flag needs_recovery è impostato. Eseguire e2fsck prima di azzerare
il flag has_journal.
 Il @S primario (%b) è nella listi dei @b(i) non validi.
 Questo non è un buon segno, ma si tenterà di continuare...
 Questo filesystem verrà automaticamente controllato ogni %d montaggi, o
%g giorni, a seconda di quale venga prima. Usare tune2fs -c o -i per cambiare.
 Troppi @b(i) illegali in @i %i.
 Tronca SCOLLEGATO Impossibile risolvere '%s' Codice errore non gestito (0x%x)!
 Passo sconosciuto?!? Effettua l'unlink -- Scollega FIXME Aggiornamento dei riferimenti degli inode Uso: %s [-F] [-I BLOCCHI_BUFFER_INODE] DEVICE
 Uso: %s [RVadlv] [file ...]
 Uso: %s [-r] [-t]
 Uso: %s DISCO
 Uso: e2label device [nuova_etichetta]
 Uso: mklost+found
 Versione di %s impostata a %lu
 ATTENZIONE: formato non valido alla linea %d di %s
 ATTENZIONE: impossibile aprire %s: %s
 VERRÀ RICREATO Attenzione! %s è montato.
 Attenzione... %s per il device %s è uscito con il segnale %d.
 Attenzione: blocchi di %d bytes troppo grandi per il sistema (max %d), continuo comunque
 Attenzione: Il @S (%b) del gruppo %g non è valido.
 Attenzione: la dimensione di blocco %d non è utilizzabile su molti sistemi.
 Attenzione: impossibile cancellare il settore %d: %s
 Attenzione: impossibile leggere @b %b di %s: %m
 Attenzione: impossibile leggere il blocco 0: %s
 Attenzione: impossibile scrivere @b %b per %s: %m
 Attenzione: blocco non valido %u trovato in blocco inode danneggiato.  Cancellato.
 Attenzione: troncamento dell'etichetta, troppo lunga.
 Attenzione: essendo un controllo a sola lettura, il journal non verrà ripristinato.
 Valore strano (%ld) nella do_read
 Durante la lettura dei flag di %s Durante la lettura della versione di %s Scrittura delle tavole degli inode:  Scrittura delle informazioni dei superblocchi e dell'accounting del filesystem:  Serve accesso di tipo %s al filesystem, o è necessario essere root
 Azzeramento del device di journaling:  annullato aattributo esteso gid/nome gruppo non valido - %s mappa degli inode errata intervallo non valido - %s numero di mount non validi - %s percentuale di blocchi riservati non valida - %s numero di blocchi riservati non valido - %s uid/nome utente non valido - %s versione non valida - %s
 bblocco mappa dei bit del blocco Dispositivo a blocchi conteggio dei blocchi per gruppo fuori dall'intervallo i blocchi per gruppo devono essere multipli di 8 blocchi da spostare impossibile allocare memoria per il modello di prova - %s cancellato!
 ccomprimi Dispositivo a caratteri controllo annullato.
 ddirectory Directory mappa degli inode delle directory fatto
 fatto

 fatto                            
 durante la ext2fs_sync_device durante la ricerca durante la scrittura dei dati del test, blocco %lu e2label: impossibile aprire %s
 e2label: impossibile raggiungere il superblocco
 e2label: impossibile spostarsi nuovamente sul superblocco
 e2label: errore durante la lettura del superblocco
 e2label: errore durante la scrittura del superblocco
 e2label: non è un filesystem ext2
 eelemento tempo rimanente: %6.3f
 mappa directory vuota blocchi directory vuoti ext2fs_new_@i: %m cercando di creare la @d /@l
 ffilesystem filesystem fsck: %s: non trovato
 fsck: impossibile controllare %s: fsck.%s non trovato
 Acquisizione dell'inode successivo dalla scansione ggruppo h@i della @d HTREE iinode mappa degli inode imagic nella malloc per bad_blocks_filename mappa dei blocchi in uso mappa degli inode in uso mappa dei bit dell'inode inode nella mappa dei bad block tavola degli inode Errore interno: impossibile cercare nel record inode EA per %u non è sicuro eseguire badblocks!
 jjournal journal llost+found mke2fs è stato forzato comunque. Si spera che /etc/mtab sia sbagliato.
 mrichiesta più volte mappa dei blocchi richiesta più volte mappa degli inode richiesta più volte nN serve il terminale per il riparo interattivo no no
 oorfano Avvio scansione inode pproblema in Lettura blocco directory lettura dei blocchi indiretti dell'inode %u lettura delle mappe di bit inode e blocco lettura del superblocco del journal
 File normale mappa degli inode dei file normali blocchi riservati ritornato da clone_file_block r@i root dimensione di un inode=%d
 Socket sdovrebbe essere Collegamento simbolico durata: %5.2f/%5.2f/%5.2f
 impossibile impostarei i flag del superblocco a %s
 Tipo di file sconosciuto in modalità 0%o so sconosciuto - %s vdevice aggiungendo un filesystem al journal in %s allocando i buffer allocando i buffer zeroizing iniziando a scorrere la lista dei blocchi difettosi chiamando ext2fs_block_iterate per l'inode %d controllando il journal ext3 per %s azzerando l'inode del journal creando /lost+found creando la directory root determinando se %s è montato. durante la scansione dell'inode espandendo /lost+found durante il recupero dell'inode successivo durante il recupero dello informazioni statistiche per %s inizializzando il superblocco del journal cercando /lost+found contrassegnando i blocchi non validi come utilizzati durante l'apertura di %s per lo svuotamento durante l'apertura dell'inode scan stampando la lista dei blocchi difettosi analizzando una lista di blocchi non validi dal programma leggendo le mappe dei bit leggendo i flag di %s durante la lettura nell'elenco dei bad block da file leggendo l'inode del journal leggendo il superblocco del journal creando l'inode root durante la lettura dell'inode dei bad block ripristinando il journal ext3 di %s resettando il contesto riprovando a leggere le mappe di bit per %s durante il controllo sull'integrità dell'inode dei bad block impostando l'inode del blocco difettoso impostando i flag di %s impostando i permessi dell'inode root impostando la versione a %s iniziando la scansione degli inode durante il tentativo di eseguire popen su '%s' tentando di allocare le tabelle del filesystem tentando di determinare la dimensione del device tentando di determinare la dimensione del filesystem provando a determinare la dimensione del settore hardware durante il tentativo di svuotamento di %s tentando di inizializzare il programma durante il tentativo di apertura di %s cercando di aprire il journal esterno durante l'apertura del dispositivo di journaling %s
 durante la riapertura di %s durante il tentativo di ridimensionare %s tentando di fare lo stat di %s durante l'aggiornamento dell'inode di un bad block scrivendo la mappa dei bit del blocco scrivendo la mappa dei bit dell'inode scrivendo la tavola degli inode scrivendo l'inode del journal scrivendo il superblocco del journal scrivendo il superblocco qui non verrà creato un %s.
 sS sì sì
 zlunghezza-zero 