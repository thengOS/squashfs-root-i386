��          D      l       �      �   !   �   &   �   R  �   �  >     �  #   	  &   -  G  T                          Access Your Private Data Record your encryption passphrase Setup Your Encrypted Private Directory To encrypt your home directory or "Private" folder, a strong passphrase has been automatically generated. Usually your directory is unlocked with your user password, but if you ever need to manually recover this directory, you will need this passphrase. Please print or write it down and store it in a safe location. If you click "Run this action now", enter your login password at the "Passphrase" prompt and you can display your randomly generated passphrase. Otherwise, you will need to run "ecryptfs-unwrap-passphrase" from the command line to retrieve and record your generated passphrase. Project-Id-Version: ecryptfs-utils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-07-13 17:14+0000
PO-Revision-Date: 2013-01-28 16:27+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-10-09 11:32+0000
X-Generator: Launchpad (build 18227)
 Accesso ai dati privati Registra la passphrase di cifratura Impostazione directory privata cifrata Per cifrare la propria directory personale o la cartella «Private», è stata generata automaticamente una passphrase robusta. Solitamente, la propria directory viene sbloccata attraverso la password utente, ma se fosse necessario ripristinare tale directory, è necessario disporre della passphrase: è quindi utile stamparla o scriverla e conservarla in un luogo sicuro. Per visualizzare la passphrase generata, fare clic su «Esegui ora» e inserire la propria password di accesso. In alternativa, è necessario eseguire da un terminale il comando «ecryptfs-unwrap-passphrase». 