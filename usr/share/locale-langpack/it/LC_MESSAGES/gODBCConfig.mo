��    F      L  a   |                      	                     '  ,   4     a  &   s     �     �  $   �     �                    '     3  
   :     E     T     \  	   m     w  
   |     �     �     �     �     �  �   �    �     �
     �
     �
     �
     �
             )   7     a  	   g  
   q  �   |  �  -  E  �  �  %  �   �  �  �  ]     
   t       
   �  �   �  +   /     [  �   d     �          $     C     `  ,   y     �     �     �  D  �  �    �  �     N     P     `     i     v     ~     �  ;   �     �  :   �     .  !   N  .   p  &   �     �     �     �     �     �     �     
          $     <     I  
   N     Y  !   n     �     �     �  �   �     �     �     �     �  !   �     �  $      "   >   ;   a      �      �      �   �   �      �!  {  �#  �  +%  �   '  �  �'  a   �)     )*     6*     C*  �   W*  7   H+  
   �+  �   �+     A,     `,  '   {,      �,     �,  /   �,     -     '-     0-  y  =-  �  �.                  @          :   <   7       4                  ?          %       ;   9           >         *            
   )   .   0      1          E   /       "   F      2                             D          A   -       5                       +          '   $       #         C   6              (   =          ,          B   &   3          	   !   8      About Add Application Browse Config Configure... Could not construct a property list for (%s) Could not load %s Could not write property list for (%s) Could not write to %s Could not write to (%s) Couldn't create pixmap from file: %s Couldn't find pixmap file: %s Credits DSN Database System Description Driver Driver Lib Driver Manager Drivers Enter a DSN name FileUsage Name ODBCConfig ODBCConfig - Credits ODBCConfig - Database Systems ODBCConfig - Drivers ODBCConfig - odbc.ini ODBCConfig - odbcinst.ini Open DataBase Connectivity (ODBC) was developed to be an Open and portable standard for accessing data. unixODBC implements this standard for Linux/UNIX. Perhaps the most common type of Database System today is an SQL Server

SQL Servers with Heavy Functionality
  ADABAS-D
  Empress
  Sybase - www.sybase.com
  Oracle - www.oracle.com

SQL Servers with Lite Functionality
  MiniSQL
  MySQL
  Solid

The Database System may be running on the local machine or on a remote machine. It may also store its information in a variety of ways. This does not matter to an ODBC application because the Driver Manager and the Driver provides a consistent interface to the Database System. Remove Select File Select a DSN to Remove Select a DSN to configure Select a driver to Use Select a driver to configure Select a driver to remove Select the DRIVER to use or Add a new one Setup Setup Lib System DSN System data sources are shared among all users of this machine.These data sources may also be used by system services. Only the administrator can configure system data sources. The Application communicates with the Driver Manager using the standard ODBC calls.

The application does not care; where the data is stored, how it is stored, or even how the system is configured to access the data.

The Application only needs to know the data source name (DSN)

The Application is not hard wired to a particular database system. This allows the user to select a different database system using the ODBCConfig Tool. The Driver Manager carries out a number of functions, such as:
1. Resolve data source names via odbcinst lib)
2. Loads any required drivers
3. Calls the drivers exposed functions to communicate with the database. Some functionality, such as listing all Data Source, is only present in the Driver Manager or via odbcinst lib). The ODBC Drivers contain code specific to a Database System and provides a set of callable functions to the Driver Manager.
Drivers may implement some database functionality when it is required by ODBC and is not present in the Database System.
Drivers may also translate data types.

ODBC Drivers can be obtained from the Internet or directly from the Database vendor.

Check http://www.unixodbc.org for drivers These drivers facilitate communication between the Driver Manager and the data server. Many ODBC drivers  for Linux can be downloaded from the Internet while others are obtained from your database vendor. This is the main configuration file for ODBC.
It contains Data Source configuration.

It is used by the Driver Manager to determine, from a given Data Source Name, such things as the name of the Driver.

It is a simple text file but is configured using the ODBCConfig tool.
The User data sources are typically stored in ~/.odbc.ini while the System data sources are stored in /etc/odbc.ini
 This is the program you are using now. This program allows the user to easily configure ODBC. Trace File Tracing Tracing On Tracing allows you to create logs of the calls to ODBC drivers. Great for support people, or to aid you in debugging applications.
You must be 'root' to set Unable to find a Driver line for this entry User DSN User data source configuration is stored in your home directory. This allows you configure data access without having to be system administrator gODBCConfig - Add DSN gODBCConfig - Appication gODBCConfig - Configure Driver gODBCConfig - Driver Manager gODBCConfig - New Driver gODBCConfig - ODBC Data Source Administrator http://www.unixodbc.org odbc.ini odbcinst.ini odbcinst.ini contains a list of all installed ODBC Drivers. Each entry also contains some information about the driver such as the file name(s) of the driver.

An entry should be made when an ODBC driver is installed and removed when the driver is uninstalled. This can be done using ODBCConfig or the odbcinst command tool. unixODBC consists of the following components

- Driver Manager
- GUI Data Manager
- GUI Config
- Several Drivers and Driver Config libs
- Driver Code Template
- Driver Config Code Template
- ODBCINST lib
- odbcinst (command line tool for install scripts)
- INI lib
- LOG lib
- LST lib
- TRE lib
- SQI lib
- isql (command line tool for SQL)

All code is released under GPL and the LGPL license.
 Project-Id-Version: unixodbc
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2003-12-02 14:45+0000
PO-Revision-Date: 2013-12-25 18:40+0000
Last-Translator: Gianfranco Frisani <gfrisani@libero.it>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:46+0000
X-Generator: Launchpad (build 18115)
   Informazioni su Aggiungi Applicazione Esplora Configurazione Configura... Non è possibile creare un elenco delle proprietà per (%s) Non è possibile caricare %s Non è possibile scrivere un elenco di proprietà per (%s) Non è possibile scrivere in %s Non è possibile scrivere in (%s) Non è possibile creare un pixmap dal file: %s Impossibile trovare il file pixmap: %s Crediti DSN Sistema dei database Descrizione Driver Libreria Driver Gestore dei Driver Driver Digitare un nome di DSN Uso del file Nome ODBCConfig ODBCConfig - Crediti ODBCConfig - Sistemi dei Database ODBCConfig - Driver ODBCConfig - odbc.ini ODBCConfig - odbcinst.ini Open DataBase Connectivity (ODBC) è stato sviluppato per essere uno standard aperto e portabile per l'accesso ai dati; unixODBC implementa questo standard per Linux/UNIX. Forse il tipo più comune di sistema dei database oggi è SQL Server
↵
Server SQL con funzionalità complete
  ADABAS-D
  Empress
  Sybase - www.sybase.com
  Oracle - www.oracle.com

Server SQL con funzionalità essenziali
  MiniSQL
  MySQL
  Solid

Il sistema dei database può essere eseguito sulla macchina locale o da remoto; è in grado di memorizzare le informazioni in un molti modi. Ciò non fa differenza in un'applicazione ODBC perché il Gestore del driver e i Driver forniscono una consistente interfaccia al sistema dei database. Rimuovi Seleziona File Selezionare un DSN da rimuovere Selezionare un DSN da configurare Selezionare un driver da usare Selezionare un driver da configurare Selezionare un driver da rimuovere Selezionare il DRIVER da utilizzare o aggiungerne uno nuovo Impostazione Impostazione Libreria DSN di sistema Le sorgenti dati di sistema sono condivise tra tutti gli utenti di questa macchina; queste sorgenti possono anche essere usate dai servizi di sistema. Soltanto l'amministratore può configurare le sorgenti dati di sistema. L'applicazione comunica con il Gestore dei Driver usando le chiamate ODBC standard.

All'applicazione non interessa dove e come i dati sono memorizzati o anche come il sistema è configurato per accedere ai dati.

All'applicazione serve soltanto sapere il nome della sorgente dei dati (Data Source Name, DSN) 

L'Applicazione non è collegata indissolubilmente a un particolare sistema dei database. Questo permette all'utente di selezionare un sistema dei database diverso usando l'applicazione ODBCConfig Tool. Il Gestore dei Driver produce un certo numero di funzioni, quali:
1. Risolve i nomi della sorgente dei dati tramite odbcinst lib)
2. Carica qualunque driver richiesto.
3. Evoca le funzioni esposte dei driver per comunicare con il database. Alcune funzionalità, come l'elenco di tutta la sorgente dati, sono disponibili solo nel Gestore dei Driver o tramite la libreria odbcinst. I driver ODBC contengono il codice specifico a un Sistema dei Database e fornisce un insieme di funzioni evocabili da parte del gestore dei driver. 
I driver possono implementare alcune funzionalità del database quando sono richieste da ODBC e non sono presenti nel database di sistema.
I driver possono anche tradurre tipi di dati.
  
I driver ODBC possono essere ottenuti da Internet o direttamente dal fornitore del database.

Consultare http://www.unixodbc.org per la ricerca di driver. Questi driver facilitano la comunicazione tra il Gestore dei Driver e il server dei dati. Molti driver ODBC per Linux possono essere scaricati da internet e altri possono essere ottenuti dal fornitore del database. Questo è il file principale di configurazione per ODBC.
Contiene la configurazione della Sorgente dei Dati.

È usato dal Gestore del driver per determinare, a partire da un dato Nome della Sorgente dei Dati, elementi come il nome del Driver.

È un file di testo semplice ma viene configurato con lo strumento ODBCConfig.
Tipicamente, le sorgenti dei dati utente sono memorizzate in ~/.odbc.ini mentre le sorgenti dei dati di sistema sono memorizzate in  /etc/odbc.ini.
 Questo è il programma attualmente in uso: consente all'utente di configurare ODBC con facilità. Traccia file Tracciamento Tracciamento attivo Il tracciamento consente di creare i registri delle chiamate ai driver ODBC, caratteristica molto importante per il supporto al prodotto o per aiutare nel debug delle applicazioni.
Per abilitarlo sono necessari privilegi di amministrazione. Impossibile trovare una riga del Driver per questa voce DSN Utente La configurazione della sorgente dati dell'utente è memorizzata nella directory home. Questo consente di configurare l'accesso ai dati senza privilegi di amministratore di sistema. gODBCConfig - Aggiunta del DSN gODBCConfig - Applicazione gODBCConfig - Configurazione del Driver gODBCConfig - Gestore dei Driver gODBCConfig - Nuovi Driver gODBCConfig - ODBC Amministratore sorgente dati http://www.unixodbc.org odbc.ini odbcinst.ini Odbcinst.ini contiene un elenco di tutti i driver ODBC installati. Ogni voce inoltre contiene alcune informazioni sul driver come il nome o i nomi del file del driver.

Quando un driver ODBC viene installato, deve essere creata una voce; quando il driver viene disinstallato, è necessario rimuovere la voce. Ciò può essere effettuato usando ODBCConfig o il comando odbcinst. unixODBC consiste dei seguenti componenti

- Gestore dei Driver
- Interfaccia grafica del Gestore dei Dati
- Configuratore dell'interfaccia grafica (GUI Config)
- Diverse librerie dei Driver e del Configuratore dei Driver
- Driver Code Template
- Driver Config Code Template
- ODBCINST lib
- odbcinst (strumento a riga di comando per installare script)
- INI lib
- LOG lib
- LST lib
- TRE lib
- SQI lib
- isql (strumento a riga di comando per SQL)

Tutto il codice è rilasciato con licenze GPL e LGPL.
 