��            )   �      �     �  	   �     �  	   �     �     �     �     �  �   �  �   �     g     l     t  	   �     �     �     �     �     �     �     �  !   �          &     +     3     M     ]    p     �     �     �  	   �     �     �     �  !   �  �     �   �     �	     �	     �	  
   �	     �	     
  
   	
  $   
     9
     ?
  %   E
  *   k
  '   �
     �
     �
  +   �
  <   �
  �  5                                                                                   	      
                                                 About All Fonts Back Copyright Description FONT-FILE OUTPUT-FILE Font Viewer GNOME Font Viewer GNOME Font Viewer also supports installing new font files downloaded in the .ttf and other formats. Fonts may be installed only for your use or made available to all users on the computer. GNOME Font Viewer shows you the fonts installed on your computer for your use as thumbnails. Selecting any thumbnails shows the full view of how the font would look under various sizes. Info Install Install Failed Installed Name Quit SIZE Show the application's version Style TEXT Text to thumbnail (default: Aa) This font could not be displayed. Thumbnail size (default: 128) Type Version View fonts on your system fonts;fontface; translator-credits Project-Id-Version: gnome-font-viewer
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-font-viewer&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-08-22 10:41+0000
PO-Revision-Date: 2015-07-10 10:31+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-10-09 11:50+0000
X-Generator: Launchpad (build 18227)
Language: it
 Informazioni Tutti i caratteri Indietro Copyright Descrizione FILE-CARATTERE FILE-USCITA Visualizzatore di caratteri Visualizzatore di caratteri GNOME Visualizzatore di caratteri consente di installare nuovi caratteri da file scaricati nel formato .ttf e anche altri formati. I caratteri possono essere installati per uso personale o per tutti gli utenti del computer. Visualizzatore di caratteri mostra tutti i caratteri installati sul proprio computer tramite delle miniature degli stessi: selezionando una miniautra vengono mostrate le varie dimensioni del carattere. Informazioni Installa Installazione non riuscita Installato Nome Esci DIMENSIONE Mostra la versione dell'applicazione Stile TESTO Testo per miniatura (predefinito: Aa) Impossibile visualizzare questo carattere. Dimensione miniatura (predefinito: 128) Tipo Versione Visualizza i caratteri presenti nel sistema carattere;caratteri;caratteri tipografici;tipo di carattere; Milo Casagrande <milo@ubuntu.com>
Andrea Zagli <azagli@libero.it>
Fabio Marzocca <thesaltydog@gmail.com>
Luca Ferretti <elle.uca@infinito.it>
Lapo Calamandrei <lapo@it.gnome.org>
Alessio Frusciante <algol@firenze.linux.it>

Launchpad Contributions:
  Andrea Zagli https://launchpad.net/~azagli-libero
  Marco Grillo https://launchpad.net/~mark0s-01-deactivatedaccount
  Milo Casagrande https://launchpad.net/~milo 