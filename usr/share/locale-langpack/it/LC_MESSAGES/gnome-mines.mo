��    L      |  e   �      p  $   q     �     �  	   �     �  
   �     �  #   �  #        :     @     G      L     m  !        �    �     �  <   �     	  
    	     +	     8	     D	     J	  "   S	     v	     �	     �	     �	     �	     �	     �	     �	  ;   
  S   A
  2   �
  [   �
  /   $     T     Z  
   f     q     }  $   �     �  #   �     �     �     �          -  �   K                    "  	   )     3     ;     A  	   H     R     V     ]  
   i     t     z     �     �     �     �     �     �     �  E  �  #   :     ^     |     �     �     �     �  2   �  )   �     #     *     9      >     _  3   r     �  @  �      �  B        Y     i     {     �     �     �  /   �  -   �                    (  !   C  
   e     p  R     u   �  G   H  |   �  =        K     W     e     s     |  /   �     �     �     �     �  !   �  #     "   B  �   e     P     ^     n     w  
        �     �     �     �     �     �     �     �     �  	   �  	   �     �     �  
          �  )  #   �     C   D                !   *   <   E          J      	       G   :   L              +      &      ,                    #   )   3   $               "         @   /             2   H       A      1              '      I   F   .   >                    0   K   ?   6            (               7   9                     B       ;   =       4   5       
             %   -   8    %u × %u, %u mine %u × %u, %u mines <b>%d</b> mine <b>%d</b> mines A_ppearance Big board Big game Board size Change _Difficulty Clear explosive mines off the board Clear hidden mines from a minefield Close Custom Date Do you want to start a new game? Enable animations Enable automatic placing of flags GNOME Mines GNOME Mines is a puzzle game where you search for hidden mines. Flag the spaces with mines as quickly as possible to make the board a safer place. You win the game when you've flagged every mine on the board. Be careful not to trigger one, or the game is over! Height of the window in pixels If you start a new game, your current progress will be lost. Keep Current Game Main game: Medium board Medium game Mines New Game Number of columns in a custom game Number of rows in a custom game Paused Percent _mines Play _Again Print release version and exit Resizing and SVG support: Score: Select Theme Set to false to disable theme-defined transition animations Set to true to automatically flag squares as mined when enough squares are revealed Set to true to be able to mark squares as unknown. Set to true to enable warning icons when too many flags are placed next to a numbered tile. Size of the board (0-2 = small-large, 3=custom) Size: Small board Small game St_art Over Start New Game The number of mines in a custom game The theme to use The title of the tile theme to use. Time Use _animations Use the unknown flag Warning about too many flags Width of the window in pixels You can select the size of the field you want to play on at the start of the game. If you get stuck, you can ask for a hint: there's a time penalty, but that's better than hitting a mine! _About _Best Times _Cancel _Close _Contents _Height _Help _Mines _New Game _OK _Pause _Play Again _Play Game _Quit _Resume _Scores _Show Warnings _Use Question Flags _Width minesweeper; translator-credits true if the window is maximized Project-Id-Version: gnome-mines
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-mines&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-02-11 17:13+0000
PO-Revision-Date: 2016-02-12 02:20+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: tp@lists.linux.it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 19:21+0000
X-Generator: Launchpad (build 18115)
X-Project-Style: gnome
Language: it
 %u × %u, %u mina %u × %u, %u mine <b>%d</b> mina <b>%d</b> mine A_spetto Campo grande Campo grande Dimensione campo Modifica _difficoltà Trova tutte le mine nascoste senza farle esplodere Scova le mine nascoste in un campo minato Chiudi Personalizzata Data Vuoi iniziare una nuova partita? Abilita animazioni Abilita il posizionamento automatico delle bandiere Mine di GNOME Mine di GNOME è un gioco nel quale bisogna trovare le mine nascoste. Contrassegna gli spazi con le mine usando una bandiera il più velocemente possibile per rendere il campo un posto sicuro. La partita viene vinta quando tutte le mine sono state trovate. Attenzione a non farle esplodere altrimenti il gioco è finito! Altezza della finestra in pixel. Se inizi una nuova partita, tutti i progressi fatti saranno persi. Continua questa Gioco principale: Campo medio Campo medio Mine Nuova partita Numero di colonne in una partita personalizzata Numero di righe in una partita personalizzata In pausa Percentuale _mine Ri_gioca Stampa la versione ed esce Ridimensionamento e supporto SVG: Punteggio: Seleziona tema Imposstare a FALSO per disabilitare le animazioni di transizione definite dal tema Impostare a VERO per fare in modo che vengano segnalati i riquadri minati quando ne sono stati liberati a sufficienza Impostare a VERO per essere in grado di marcare una cella come incerta. Impostare a VERO per abilitare l'icona di avviso quando vengono posizionate troppe bandierine vicino a una casella numerata. Dimensione del campo (0-2 = piccola-grande, 3=personalizzata) Dimensione: Campo piccolo Campo piccolo Ri_parti Nuova partita Il numero di mine in una partita personalizzata Il tema da usare Il nome del tema da usare. Tempo Usa _animazioni Usare la bandierina di incertezza Avviso se ci sono troppe bandierine Larghezza della finestra in pixel. È possibile selezionare la dimensione del campo nel quale giocare all'inizio della partita. Se ci si trova in difficoltà si può chiedere un suggerimento: c'è una penalità sul tempo, ma è sempre meglio che far esplodere una mina! I_nformazioni _Tempi migliori A_nnulla _Chiudi _Contenuti _Altezza A_iuto _Mine _Nuova partita _OK _Pausa Ri_gioca _Gioca _Esci _Riprendi _Punteggi _Mostra avvisi _Usa punto interrogativo _Larghezza minesweeper;mine; Claudio Arseni <claudio.arseni@ubuntu.com>
Milo Casagrande <milo@milo.name>
Gruppo traduzione Italiano di Ubuntu <gruppo-traduzione@ubuntu-it.org>
Francesco Marletta <francesco.marletta@tiscali.it>
Fabrizio Stefani <f.stef@it.gnome.org>

Launchpad Contributions:
  Claudio Arseni https://launchpad.net/~claudio.arseni
  Gianfranco Frisani https://launchpad.net/~gfrisani
  Milo Casagrande https://launchpad.net/~milo VERO se la finestra è massimizzata 