��    7      �  I   �      �     �     �     �     �  )   
     4  (   @     i          �  "   �  E   �       %   +     Q     Z     b  )   �     �  *   �     �  /   �     #  	   6  8   @  "   y     �     �     �     �     �     �       D   9  `   ~     �     �  )   	     A	  *   S	  #   ~	  	   �	     �	  D   �	  D   
     R
  !   q
  ,   �
  B   �
  
          '         >  +   _  �  �     n     �      �  #   �  +   �       4   "     W     o      �  1   �  I   �     )  9   @     z  	   �  !   �  4   �     �  0     	   7  5   A     w  	   �  >   �  #   �  )   �     $     6     C  ,   ]      �     �  U   �  e        �     �  )   �     �  .   �  )   %     O     \  L   |  K   �       0   4  J   e  E   �  
   �       .   
     9  2   Y         -              0                	      1              4      %       '      *      6                 !   &          #         7   
   "       (      5   /   +              )   2                           ,   $         .            3                         (current) Unix password: Authentication failed. Can't set PAM_TTY=%s Cannot get username Causes the screensaver to exit gracefully Checking… Command to invoke from the logout button Don't become a daemon Enable debugging code Enter new Unix password: Error while changing NIS password. If the screensaver is active then deactivate it (un-blank the screen) Incorrect password. Launch screensaver and locker program Log _Out MESSAGE Message to show in the dialog No longer permitted to access the system. No password supplied Not permitted to gain access at this time. Not used Password has been already used. Choose another. Password unchanged Password: Query the length of time the screensaver has been active Query the state of the screensaver Retype new Unix password: S_witch User… Screensaver Show debugging output Show the logout button Show the switch user button Sorry, passwords do not match Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Time has expired. Turn the screensaver on (blank the screen) Unable to establish service %s: %s
 Username: Version of this application You are required to change your password immediately (password aged) You are required to change your password immediately (root enforced) You have the Caps Lock key on. You must choose a longer password You must wait longer to change your password Your account has expired; please contact your system administrator _Password: _Unlock failed to register with the message bus not connected to the message bus screensaver already running in this session Project-Id-Version: gnome-screensaver 2.9x
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-01-04 14:27+0000
PO-Revision-Date: 2016-01-05 00:35+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Italiano <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:43+0000
X-Generator: Launchpad (build 18115)
Language: 
 Password UNIX (attuale): Autenticazione non riuscita. Impossibile impostare PAM_TTY=%s Impossibile ottenere il nome utente Causa l'uscita non forzosa del salvaschermo Controllo... Comando da invocare col pulsante di termine sessione Non diventare un demone Abilitare il codice di debug Inserire la nuova password UNIX: Errore durante il cambiamento della password NIS. Se il salvaschermo è in esecuzione lo arresta (unblanking dello schermo) Password non corretta. Lancia il programma per salvaschermo e per blocco schermo _Termina sessione MESSAGGIO Messaggio da mostrare nel dialogo L'accesso al sistema è stato perennemente revocato. Nessuna password fornita Per ora non è più concesso ottenere l'accesso. Non usato La password è stata già usata. Sceglierne un'altra. Password non cambiata Password: Interroga l'intervallo di tempo di esecuzione del salvaschermo Interroga lo stato del salvaschermo Digitare di nuovo la nuova password UNIX: _Cambia utente... Salvaschermo Mostra l'outptut di debug Mostra il pulsante per terminare la sessione Mostra il pulsante cambia utente Le password non corrispondono Comunica al processo salvaschermo in esecuzione di bloccare immediatamente lo schermo Il salvaschermo è rimasto attivo per %d secondo.
 Il salvaschermo è rimasto attivo per %d secondi.
 Il salvaschermo è attivo
 Il salvaschermo è inattivo
 l salvaschermo non è attivo al momento.
 Tempo scaduto! Avvia il salvaschermo (blanking dello schermo) Impossibile stabilire il servizio %s: %s
 Nome utente: Versione di questa applicazione È necessario cambiare immediatamente la propria password (password scaduta) È necessario cambiare immediatamente la propria password (imposto da root) Il tasto BlocMaiusc è attivo. È necessario scegliere una password più lunga. È necessario attendere ancora prima di poter cambiare la propria password Il proprio account è scaduto; contattare l'amministratore di sistema Pass_word: S_blocca registrazione col bus di messaggi non riuscita non connesso al bus di messaggi salvaschermo già in esecuzione in questa sessione 