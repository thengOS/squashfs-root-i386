��    I      d  a   �      0     1  4  N     �  R   �  g   �  Z   W	  '   �	     �	     �	     �	     
  	   '
  1   1
  	   c
  	   m
  (   w
  )   �
     �
     �
     �
  #      '   $  "   L     o     �     �     �     �     �     �     �      �          	             !   /  '   Q  %   y  	   �     �  '   �     �  P   �     1     H     X     e  
   t  ;     3   �  Q   �     A     P  &   g     �      �  #   �      �  *   	      4      U  /   v     �  =   �  	         
               #  (   0     Y  �  i  "     n  A     �  a   �  u   ,  ^   �  '        )     7     R  !   q  
   �  =   �  	   �  
   �  .   �  /      )   P     z     �  3   �  2   �           )     D  	   \     f     �     �     �     �     �     �     �     �       !     <   1  3   n     �  	   �  +   �     �  L   �     4     M     `     n     �  3   �  4   �  P   �     F  "   X  '   {     �  %   �  )   �  !     ,   *  >   W  )   �  9   �     �  Q     	   l     v     �     �     �  1   �     �         )      +   I   E   A       #   D   =      "          ;          C   *           0      /              H            9   4      	      
              >   <   8       $           -                   B   ,          3       ?              :   &          5   2   7                @          6                           G   1      '   %   F   .   !       (     - the GNOME session manager %s [OPTION...] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can't recover.
Please log out and try again. A problem has occurred and the system can't recover. All extensions have been disabled as a precaution. A problem has occurred and the system can't recover. Please contact a system administrator A session named ‘%s’ already exists AUTOSTART_DIR Add Startup Program Additional startup _programs: Allow logout Browse… Choose what applications to start when you log in Co_mmand: Comm_ent: Could not connect to the session manager Could not create ICE listening socket: %s Could not display help document Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don't prompt for user confirmation Edit Startup Program Enable debugging code Enabled Failed to execute %s
 GNOME GNOME dummy GNOME on Wayland Icon Ignoring any existing inhibitors Log out No description No name Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Select Command Session %d Session names are not allowed to contain ‘/’ characters Session names are not allowed to start with ‘.’ Session names are not allowed to start with ‘.’ or contain ‘/’ characters Session to use Show extension warning Show the fail whale dialog for testing Startup Applications Startup Applications Preferences The startup command cannot be empty The startup command is not valid This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME This session logs you into GNOME, using Wayland Version of this application _Automatically remember running applications when logging out _Continue _Log Out _Log out _Name: _New Session _Remember Currently Running Applications _Remove Session Project-Id-Version: gnome-session 3.7.x
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-08-22 10:46+0000
PO-Revision-Date: 2015-07-10 03:31+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italiano <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-10-09 10:49+0000
X-Generator: Launchpad (build 18227)
Language: it
  - il gestore di sessioni di GNOME %s [OPZIONE...] COMANDO

Esegue COMANDO durante l'inibizione di alcune funzionalità di sessione.

  -h, --help        Mostra questo aiuto
  --version         Mostra la versione del programma
  --app-id ID       L'id dell'applicazione da usare
                    durante l'inibizione (opzionale)
  --reason MOTIVO   Il motivo dell'inibizione (opzionale)
  --inhibit ARG     Elenco separato da virgola di elementi da inibire:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Non esegue COMANDO e attende per sempre

Se non è specificata alcuna opzine per --inhibit, viene usato idle.
 %s richiede un parametro
 Si è verificato un problema impossibile da recuperare.
Terminare la sessione e provare di nuovo. Si è verificato un problema impossibile da recuperare. Per precauzione, tutte le estensioni sono state disabilitate. Si è verificato un problema impossibile da recuperare. Contattare l'amministratore di sistema Un sessione chiamata «%s» esiste già DIR_AUTOSTART Aggiunta programma d'avvio _Programmi d'avvio aggiuntivi: Consente di terminare la sessione Esplora… Sceglie quali applicazioni avviare quando si esegue l'accesso Co_mando: Comm_ento: Impossibile connettersi al gestore di sessione Impossibile creare il socket di ascolto ICE: %s Impossibile mostrare il documento d'aiuto Personalizzata Sessione personalizzata Disabilita il controllo sull'accelerazione hardware Non carica le applicazioni specificate dall'utente Non richiede conferma all'utente Modifica programma d'avvio Abilita codice di debug Abilitato Esecuzione di %s non riuscita
 GNOME GNOME dummy GNOME su Wayland Icona Ignora ogni inibitore esistente Termina la sessione Nessuna descrizione Nessun nome Non risponde Oh no! Qualcosa è andato storto. Non tiene conto delle directory standard di avvio automatico Selezionare una sessione personalizzata da lanciare Spegni Programma Programma chiamato con opzioni in conflitto Riavvia Rifiutate nuove connessioni client perché la sessione è in via di arresto
 Applicazioni memorizzate Rino_mina sessione NOME_SESSIONE Selezione comando Sessione %d I nomi delle sessioni non contenere caratteri «/» I nomi delle sessioni non possono iniziare con «.» I nomi delle sessioni non possono iniziare con «.» o contenere caratteri «/» Sessione da usare Mostra gli avvisi delle estensioni Mostra il dialogo "fail whale" per test Applicazioni d'avvio Preferenze delle applicazioni d'avvio Il comando di avvio non può essere vuoto Il comando di avvio non è valido Consente di selezionare una sessione salvata Questo programma sta bloccando la terminazione della sessione. Questa sessione esegue l'accesso in GNOME Questa sessione esegue l'accesso in GNOME, usando Wayland Versione di questa applicazione Memorizzare _automaticamente le applicazioni in esecuzione terminando la sessione _Continua _Termina sessione _Termina la sessione N_ome: _Nuova session _Memorizza applicazioni attualmente in esecuzione _Rimuovi sessione 