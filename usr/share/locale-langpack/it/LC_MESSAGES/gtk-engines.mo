��    0      �  C         (     )     9  
   K     V     \     p     x     �     �     �     �  !   �     �     �     �     �     �  "        )     /     8     E     T     `  1   �     �     �     �  
   �     �  	   �     �     �          %     5     E     T     g     �     �  ?   �     �     �     �  p   �     T  �  b     U	     p	  
   �	     �	     �	     �	     �	  	   �	  '   �	     
     
  '   -
     U
     \
     b
     i
     o
  6   �
     �
  	   �
     �
     �
     �
  6     U   :     �     �     �     �     �  
   �     �      �     �       !   ,     N  '   k  ,   �     �     �  K   �          !     *  �   0     �     /   +              -         )   %                                   *                        $                            0   ,      "      .   &         '             #             (          	                   !                 
    3d-ish (button) 3d-ish (gradient) Animations Arrow Cell Indicator Size Classic Colorize Scrollbar Contrast Disable focus drawing Dot Edge Thickness Enable Animations on Progressbars Flat Full Glossy Gummy Handlebox Marks If set, button corners are rounded Inset Inverted Inverted Dot Inverted Slash Mark Type 1 Mark Type for Scrollbar Buttons Mark Type for Scrollbar Handles, Handleboxes, etc Menubar Style None Nothing Paned Dots Radius Rectangle Relief Style Requires style Glossy or Gummy Rounded Buttons Scrollbar Color Scrollbar Marks Scrollbar Type Scrollbutton Marks Sets the Color of Scrollbars Shadow Shaped Size of check- and radiobuttons inside treeviews. (Bug #351764) Slash Some Style This option allows to disable the focus drawing. The primary purpose is to create screenshots for documentation. Toolbar Style Project-Id-Version: gtk-engines 2.13.x
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gtk-engines&component=general
POT-Creation-Date: 2015-12-03 22:41+0000
PO-Revision-Date: 2009-03-17 03:11+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:46+0000
X-Generator: Launchpad (build 18115)
 Tridimensionale (pulsante) Tridimensionale (gradiente) Animazioni Freccia Dimensione indicatore cella Classico Colorare barra di scorrimento Contrasto Disabilitare rappresentazione del focus Punto Spessore del bordo Abilita animazioni su barre avanzamento Piatto Pieno Glossy Gummy Contrassegni maniglie Se impostato, gli angoli dei pulsanti sono arrotondati Inserito Invertito Punto invertito Slash invertito Contrassegno tipo 1 Tipo di contrassegno per pulsanti di barra scorrimento Tipo di contrassegno per maniglie di barre di scorrimento, maniglie di caselle, ecc.. Stile barra menù Nessuno Niente Punti paned Raggio Rettangolo Stile rilievo Richiede lo stile Glossy o Gummy Pulsanti arrotondati Colore barra scorrimento Contrassegni barra di scorrimento Tipo di barra di scorrimento Contrassegni di pulsanti di scorrimento Imposta il colore delle barre di scorrimento Ombra Sagomato Dimensione dei pulsanti radio/check all'interno dei treeview (bug #351764). Slash Qualcuno Stile Questa opzione consente di disabilitare la rappresentazione del focus. Lo scopo principale è quello di catturare schermate per la documentazione. Stile barra strumenti 