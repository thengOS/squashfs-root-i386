��    (      \  5   �      p     q     �     �     �     �  %   �  $     @   )     j          �     �     �     �     �     �               $     7  /   I     y     �     �     �     �     �     �     �       !     #   7  "   [  �  ~  R   T     �     �  
   �  
   �  �  �     ~
     �
     �
  &   �
  )   �
  9     *   E  T   p     �     �               8     O     j     y     �      �     �     �  ;   �     0     A     [     c     v     �     �     �     �  '   �  &     %   ;  �  a  g   U     �     �     �     �                	   !                                       #                      
          "                                   %                   $       &      (         '          (replace text) <b>Input/Output</b> <b>Options</b> Allow type with more _freedom Allow type with more freedom Auto _restore keys with invalid words Auto restore keys with invalid words Auto send PreEdit string to Application when mouse move or click Capture _mouse event Capture mouse event Choose file to export Choose file to import Choose input method Choose output charset Delete _all Enable Macro Enable _macro Enable _spell check Enable spell check IBus-Unikey Setup If enable, you can decrease mistake when typing Input method: Macro table definition Options Options for Unikey Output charset:  Process W at word begin Process _W at word begin Replace with Run Setup... Run setup utility for IBus-Unikey Use oà, _uý (instead of òa, úy) Use oà, uý (instead of òa, úy) Vietnamese Input Method Engine for IBus using Unikey Engine
Usage:
  - Choose input method, output charset, options in language bar.
  - There are 4 input methods: Telex, Vni, STelex (simple telex) and STelex2 (which same as STelex, the difference is it use w as ư).
  - And 7 output charsets: Unicode (UTF-8), TCVN3, VNI Win, VIQR, CString, NCR Decimal and NCR Hex.
  - Use <Shift>+<Space> or <Shift>+<Shift> to restore keystrokes.
  - Use <Control> to commit a word. When typing a word not in Vietnamese,
it will auto restore keystroke into original Word _Edit macro _Export... _Import... Project-Id-Version: ibus-unikey
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-06-11 17:18+0700
PO-Revision-Date: 2013-02-05 12:12+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:44+0000
X-Generator: Launchpad (build 18115)
 (testo da sostituire) <b>Ingresso/Uscita</b> <b>Opzioni</b> Consentire la digitazione in _libertà Consente la digitazione con più libertà _Ripristinare automaticamente tasti con parole non valide Ripristinare i tasti con parole non valide Invia automaticamente stringa PreEdit all'applicazione al movimento o clic del mouse Re_gistrare eventi del mouse Registrare gli eventi del mouse Scelta file su cui esportare Scelta file da importare Scelta metodo di input Scelta caratteri in uscita Elimina _tutte Abilita macro Abilitare _macro Abilitare _controllo ortografico Abilitare controllo ortografico Configurazione IBus-Unikey Se abilitato è possibile ridurre gli errori di digitazione Metodo di input: Tabella definizione macro Opzioni Opzioni per Unikey Caratteri in uscita:  Elabora E a inizio parola Elaborare _W a inizio parola Sostituisci con Esegui configurazione... Esegue la configurazione di IBus-Unikey Usare oà, _uý (al posto di òa, úy) Usare oà, uý (al posto di òa, úy) Motore di input vietnamita per IBus attraverso lo Unikey Engine
Uso:
  - Scegliere il metodo di input, la codifica di output e le opzioni nella barra della lingua.
  - Ci sono 4 metodi di input: Telex, Vni, STelex (telex semplice) e STelex2 (simile a STelex, usa w per ư).
  - E 7 codifiche di output: Unicode (UTF-8), TCVN3, VNI Win, VIQR, CString, NCR Decimal e NCR Hex.
  - Usare <Maiusc>+<Spazio> o <Maiusc>+<Maiusc> per ripristinare i tasti premuti.
  - Usare <Control> per inviare una parola. Quando viene digitata una parola non in vietnamita,
viene ripristinata la pressione dei tasti originale Parola _Modifica macro E_sporta... _Importa... 