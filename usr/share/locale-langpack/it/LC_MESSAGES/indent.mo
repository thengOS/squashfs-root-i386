��    8      �  O   �      �  +   �  %     /   +     [  /   t     �     �     �     �  2     2   ;     n     �     �     �     �     �  1   �     0  G   K     �     �     �     �     �  (   �  '        8     I     ]  5   |     �     �     �     �     	     .	     6	     J	  '   W	  !   	     �	  )   �	     �	     �	  "   
     .
  <   G
  :   �
  0   �
  *   �
  :        V  &   b  ]   �  �  �  +   �  '   �  2   �     	  /   '      W  )   x  '   �  &   �  A   �  A   3  -   u  *   �     �     �  *   �  )     ;   H  &   �  L   �     �  (        0     @     R  -   d  )   �     �     �  -   �  :        V     p     �  (   �  &   �  
   �  #        '  *   7  "   b     �  =   �     �     �  #   
     .  R   M  F   �  ;   �  3   #  M   W     �  &   �  [   �     -                       *         .   7       
                       &       (   )                         8   ,      2         /              	      +   '   0   $       #   1       !                        4           %                               6   3                "      5      stack[%d] =>   stack: %d   ind_level: %d
 %s: missing argument to parameter %s
 %s: option ``%s'' requires a numeric parameter
 %s: unknown option "%s"
 (Lines with comments)/(Lines with code): %6.3f
 CANNOT FIND '@' FILE! Can't close output file %s Can't open backup file %s Can't open input file %s Can't preserve modification time on backup file %s Can't preserve modification time on output file %s Can't stat input file %s Can't write to backup file %s EOF encountered in comment Error Error closing input file %s Error reading input file %s File %s contains NULL-characters: cannot proceed
 File %s is too big to read File named by environment variable %s does not exist or is not readable GNU indent %s
 Internal buffering error Line broken Line broken 2 ParseStack [%d]:
 Profile contains an unterminated comment Profile contains unpalatable characters Read profile %s
 Stmt nesting error. System problem reading file %s There were %d non-blank output lines and %d comments
 Unexpected end of file Unknown code to parser Unmatched 'else' Unterminated character constant Unterminated string constant Warning Zero-length file %s command line indent:  Strange version-control value
 indent:  Using numbered-existing
 indent: %s:%d: %s: indent: Can't make backup filename of %s
 indent: Fatal Error:  indent: System Error:  indent: Virtual memory exhausted.
 indent: can't create %s
 indent: can't have filenames when specifying standard input
 indent: only one input file when output file is specified
 indent: only one input file when stdout is used
 indent: only one output file (2nd was %s)
 old style assignment ambiguity in "=%c".  Assuming "= %c"
 option: %s
 set_option: internal error: p_type %d
 usage: indent file [-o outfile ] [ options ]
       indent file1 file2 ... fileN [ options ]
 Project-Id-Version: indent-2.2.8
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-01-31 17:27+0100
PO-Revision-Date: 2010-12-11 23:13+0000
Last-Translator: Sergio Zanchetta <primes2h@ubuntu.com>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:47+0000
X-Generator: Launchpad (build 18115)
   stack[%d] =>   stack: %d   ind_level: %d
 %s: argomento mancante al parametro %s
 %s: l'opzione "%s" richiede un parametro numerico
 %s: opzione "%s" sconosciuta
 (Righe con commenti)/(Righe con codice): %6.3f
 IMPOSSIBILE TROVARE IL FILE "@". Impossibile chiudere il file di output %s Impossibile aprire il file di backup %s Impossibile aprire il file di input %s Impossibile preservare l'orario di modifica sul file di backup %s Impossibile preservare l'orario di modifica sul file di output %s Impossibile fare lo stat del file di input %s Impossibile scrivere sul file di backup %s Incontrato EOF in un commento Errore Errore nella chiusura del file di input %s Errore nella lettura del file di input %s Il file %s contiene caratteri NULL: impossibile continuare
 Il file %s è troppo grande da leggere Il file nominato dalla variabile d'ambiente %s non esiste o non è leggibile GNU indent %s
 Errore interno di riempimento del buffer Riga interrotta Riga interrotta 2 ParseStack [%d]:
 Il profilo contiene un commento non terminato Il profilo contiene caratteri non graditi Letto il profilo %s
 Errore di annidazione stmt. Problema di sistema nella lettura del file %s Erano presenti %d righe non vuote di output e %d commenti
 Fine del file inaspettata Analizzato codice sconosciuto "else" non corrispondente Costante di tipo carattere non terminata Costante di tipo stringa non terminata Attenzione Il file %s ha lunghezza pari a zero riga di comando indent:  strano valore di version-control
 indent:  numbered-existing in uso
 indent: %s:%d: %s: indent: impossibile creare il nome del file di backup per %s
 indent: errore fatale:  indent: errore di sistema:  indent: memoria virtuale esaurita.
 indent: impossibile creare %s
 indent: non è possibile avere nomi di file quando si specifica lo standard input
 indent: solo un file di input quando è specificato il file di output
 indent: solo un file di input quando viene usato lo stdout
 indent: solo un file di output (il secondo era %s)
 ambiguità nell'assegnamento di vecchio stile in "=%c". Viene assunto "= %c"
 opzione: %s
 set_option: errore interno: p_type %d
 uso: indent file [-o fileout ] [ opzioni ]
       indent file1 file2 ... fileN [ opzioni ]
 