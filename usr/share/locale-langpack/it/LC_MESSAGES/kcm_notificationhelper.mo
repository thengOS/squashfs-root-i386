��          �      ,      �  :   �     �  5   �               8  )   H     r     �     �     �     �     �               )  �  H  :   �     :  D   P     �      �     �  3   �            $   1     V  #   h     �     �     �  ,   �                                                	             
                 (C) 2009-2010 Jonathan Thomas, (C) 2009-2014 Harald Sitter Application crashes Configure the behavior of Kubuntu Notification Helper Harald Sitter Incomplete language support Jonathan Thomas Kubuntu Notification Helper Configuration Notification type: Popup notifications only Proprietary Driver availability Required reboots Restricted codec availability Show notifications for: Tray icons only Upgrade information Use both popups and tray icons Project-Id-Version: kubuntu-notification-helper
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-04-01 17:07+0000
PO-Revision-Date: 2015-03-31 19:46+0000
Last-Translator: Valter Mura <valtermura@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:48+0000
X-Generator: Launchpad (build 18115)
 (C) 2009-2010 Jonathan Thomas, (C) 2009-2014 Harald Sitter Crash di applicazioni Configura il comportamento dell'Assistente alle notifiche di Kubuntu Harald Sitter Supporto della lingua incompleto Jonathan Thomas Configurazione Assistente alle notifiche di Kubuntu Tipo di notifica: Solo avvisi a comparsa Disponibilità di driver proprietari Riavvio richiesto Disponibilità di codec proprietari Mostra notifiche per: Solo icone nel vassoio Informazioni di aggiornamento Usa notifiche a comparsa e icone nel vassoio 