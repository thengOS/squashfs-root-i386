��    (      \  5   �      p  "   q  ,   �     �     �     �     �  "        $  /   9      i  )   �     �     �     �     �  :   �  4   '     \     i  ]   �  $   �       	   '  6   1     h  *   v     �     �  2   �            )   2     \  2   w  6   �  !   �  !     	   %     /  �  A  !   �	  2   
     9
     M
     `
     u
  %   ~
     �
  =   �
  �   �
  /   �          ,     :     K  S   S  C   �     �    �  v   �  .   v      �  
   �  <   �       :        W     w  1   �     �  #   �  3   �  #   2  9   V  <   �      �  &   �          "                                                                      '       !   %       
   #      "                  (   	   &                                          $              (C) 2007 - 2008 Anthony Mercatante <b>Incorrect password, please try again.</b> <b>Warning: </b> Anthony Mercatante Command not found! Command: Do not display « ignore » button Do not keep password Do not show the command to be run in the dialog EMAIL OF TRANSLATORSYour emails Fake option for KDE's KdeSu compatibility Forget passwords Harald Sitter Jonathan Riddell KdeSudo Makes the dialog transient for an X app specified by winid Manual override for automatic desktop file detection Martin Böhm NAME OF TRANSLATORSYour names No command arguments supplied!
Usage: kdesudo [-u <runas>] <command>
KdeSudo will now exit... Please enter password for <b>%1</b>. Please enter your password. Priority: Process priority, between 0 and 100, 0 the lowest [50] Robert Gruber Specify icon to use in the password dialog Sudo frontend for KDE The command to execute The comment that should be displayed in the dialog Use existing DCOP server Use realtime scheduling Use target UID if <file> is not writeable Wrong password! Exiting... Your user is not allowed to run sudo on this host! Your user is not allowed to run the specified command! Your username is unknown to sudo! needs administrative privileges.  realtime: sets a runas user Project-Id-Version: kdesudo
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-05 00:10+0000
PO-Revision-Date: 2014-02-20 08:21+0000
Last-Translator: Valter Mura <valtermura@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:23+0000
X-Generator: Launchpad (build 18115)
 C) 2007 - 2008 Anthony Mercatante <b>Password non corretta: riprova, per favore.</b> <b>Attenzione: </b> Anthony Mercatante Comando non trovato! Comando: Non mostrare il pulsante « ignora » Non ricordare la password Non mostrare il comando da eseguire nella finestra di dialogo ,,,,manfrommars@libero.it,bugman@ubuntu.com,milo.casagrande@gmail.com,,blk_revenge@hotmail.com,valtermura@gmail.com,,,,,,manfrommars@libero.it,bugman@ubuntu.com,milo.casagrande@gmail.com,,blk_revenge@hotmail.com,valtermura@gmail.com, Opzione fasulla per la compatibilità con KdeSu Dimentica le password Harald Sitter Jonathan Riddell KdeSudo Rende la finestra di dialogo transitoria per un'applicazione X specificata da winid Annullamento manuale per il rilevamento automatico dei file desktop Martin Böhm ,Launchpad Contributions:,Erick,Guybrush88,Man from Mars,Maurizio Moriconi,Milo Casagrande,Pibbaccos,Thomas Scalise,Valter Mura,bp, ,Launchpad Contributions:,Erick,Guybrush88,Man from Mars,Maurizio Moriconi,Milo Casagrande,Pibbaccos,Thomas Scalise,Valter Mura,bp Nessun argomento fornito al comando!
Utilizzo: kdesudo [-u <esegui_come>] <comando>
KdeSudo terminerà l'esecuzione... Per favore, immetti la password per <b>%1</b>. Per favore, immetti la password. Priorità: Priorità del processo, tra 0 e 100, 0 è la più bassa [50] Robert Gruber Specificare l'icona da usare nella finestra della password Interfaccia utente sudo per KDE Il comando da eseguire Il commento da mostrare nella finestra di dialogo Usa server DCOP esistente Usa il pianificatore in tempo reale Usa UID di destinazione se <file> non è scrivibile Password errata! Uscita in corso... L'utente non è abilitato a eseguire sudo su questo host! L'utente non è abilitato a eseguire il comando specificato! Nome utente sconosciuto da sudo! richiede privilegi di amministrazione  tempo reale: imposta un utente di esecuzione 