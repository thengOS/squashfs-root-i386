��          �      \      �     �     �                     '  &   6     ]     f     w     �  (   �  ,   �     �  	          !   &     H     U  �  ]  !        :     I     Z     f     y  .   �     �     �     �     �  1     4   =  #   r     �     �  ,   �     �     �                                  
                     	                                            Automatic login in %d seconds Change _Language Change _Session Default Failsafe xterm Login as Guest No response from server, restarting... Password Select _Host ... Select _Language ... Select _Session ... Select the host for your session to use: Select the language for your session to use: Select your session manager: Shut_down Username Verifying password.  Please wait. _Preferences _Reboot Project-Id-Version: ldm 2.1.1
Report-Msgid-Bugs-To: sbalneav@ltsp.org
POT-Creation-Date: 2013-11-24 08:57+0200
PO-Revision-Date: 2011-09-05 20:32+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:26+0000
X-Generator: Launchpad (build 18115)
Language: it
 Accesso automatico fra %d secondi Cambia _lingua Cambia _sessione Predefinito Xterm di emergenza Accedi come ospite Nessuna risposta dal server, riavvio in corso. Password Seleziona l'_host... Seleziona la _lingua... Seleziona la _sessione... Selezionare l'host da utilizzare per la sessione: Selezionare la lingua da utilizzare per la sessione: Selezionare il gestore di sessione: _Arresta Nome utente Verifica della password in corso. Attendere. Preferen_ze _Riavvia 