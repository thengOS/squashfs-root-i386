��          �            h     i     ~     �  %   �     �     �     �     �       	   %  H   /     x     |     �  �  �     7     T     \  5   _     �  /   �  /   �  /   �  /   (     X  \   k     �     �     �     	                                                              
             Bad format parameter Cancel No Not enough resources to create thread Ok Ouch!  Got SIGABRT, dying..
 Ouch!  Got SIGQUIT, dying..
 Ouch!  Got SIGSEGV, dying..
 Ouch!  Got SIGTERM, dying..
 TOP LEVEL Unable to load filename: the string %ls has no multibyte representation. Yes no_key yes_key Project-Id-Version: cwidget
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-02-22 00:06+0000
PO-Revision-Date: 2012-09-09 13:43+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:26+0000
X-Generator: Launchpad (build 18115)
 Formato dei parametri errato Annulla No Non ci sono abbastanza risorse per allocare il thread OK Ricevuto il segnale SIGABRT. Uscita immediata.
 Ricevuto il segnale SIGQUIT. Uscita immediata.
 Ricevuto il segnale SIGSEGV. Uscita immediata.
 Ricevuto il segnale SIGTERM. Uscita immediata.
 LIVELLO PRINCIPALE Impossibile caricare il nome del file: la stringa %ls non ha una rappresentazione multibyte. Sì n s 