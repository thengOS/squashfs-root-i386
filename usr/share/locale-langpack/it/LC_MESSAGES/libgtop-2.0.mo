��    )      d  ;   �      �     �     �     �     �     �     �  	             !     :     C  	   ^     h     y     �     �     �     �     �     �  	                  ,     1     G  F   L     �     �     �  
   �     �     �     �          !     4  ,   O     |     �  �  �     ]     c     o  !   �     �     �  	   �     �      	      	     )	  	   E	     O	     ]	  ,   u	     �	     �	     �	     �	     �	  	   �	     	
     
     )
     .
     D
  Y   I
     �
     �
     �
  
   �
     �
     �
          /     C     V  -   r     �  )   �         %           '         )               "       
                                                              !                                                     $      	   (                 #   &    Abort Alarm clock Background read from tty Background write to tty Bad argument to system call Broken pipe Bus error CPU limit exceeded Child status has changed Continue Don't fork into background EMT error Enable debugging Enable verbose output File size limit exceeded Floating-point exception Hangup I/O now possible Illegal instruction Information request Interrupt Invoked from inetd Keyboard stop Kill Profiling alarm clock Quit Run '%s --help' to see a full list of available command line options.
 Segmentation violation Stop Termination Trace trap Urgent condition on socket User defined signal 1 User defined signal 2 Virtual alarm clock Window size change read %d byte read %d bytes read %lu byte of data read %lu bytes of data read data size wrote %d byte wrote %d bytes Project-Id-Version: libgtop
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-03 23:17+0000
PO-Revision-Date: 2009-06-30 04:01+0000
Last-Translator: Alessio Frusciante <Unknown>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:00+0000
X-Generator: Launchpad (build 18115)
 Abort Alarm clock Lettura in background dalla tty Scrittura in background sulla tty Bad argument to system call Broken pipe Bus error Superato il limite della CPU Lo stato del figlio è cambiato Continue Non fare fork in background EMT error Abilita debug Abilita output prolisso Superato il limite nella dimensione dei file Floating-point exception Hangup I/O adesso possibile Illegal instruction Information request Interrupt Invocato da inetd Keyboard stop Kill Profiling alarm clock Quit Eseguire '%s --help' per vedere una lista completa delle opzioni per la riga di comando.
 Segmentation violation Stop Termination Trace trap Urgent condition on socket Segnale definito dall'utente 1 Segnale definito dall'utente 2 Virtual alarm clock Window size change letto %d byte letti %d byte letto %lu byte di dati letti %lu byte di dati dimensione dei dati letti scrittura di %d byte scrittura di %d byte 