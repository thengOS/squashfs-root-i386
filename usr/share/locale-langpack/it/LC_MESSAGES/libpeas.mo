��    #      4  /   L           	     $  #   *  %   N     t     �     �     �     �     �     �     �          
           -     N     V     c     o     �  \   �  ?   �  #   2  '   V     ~     �     �     �     �     �     �     �     �  
  �     �     �  0      -   1     _  0   ~  )   �     �     �  	   �     	     	     &	     -	     <	  0   Q	     �	     �	  
   �	     �	     �	  b   �	  I   5
  %   
  0   �
  
   �
     �
     �
     �
        
             $     0                         #            
          "                                                                !            	                                  - libpeas demo application About Additional plugins must be disabled An additional plugin must be disabled An error occurred: %s Dependency '%s' failed to load Dependency '%s' was not found Disable Plugins E_nable All Enabled Failed to load Peas Gtk Plugin Plugin Manager Plugin Manager View Plugin loader '%s' was not found Plugins Pr_eferences Preferences Run from build directory Show Builtin The '%s' plugin depends on the '%s' plugin.
If you disable '%s', '%s' will also be disabled. The following plugins depend on '%s' and will also be disabled: The plugin '%s' could not be loaded There was an error displaying the help. View _About _Cancel _Close _Disable All _Enabled _Help _Preferences _Quit Project-Id-Version: libpeas master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libpeas&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-02-24 00:33+0000
PO-Revision-Date: 2015-11-10 03:51+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:58+0000
X-Generator: Launchpad (build 18115)
Language: it
 - applicazione demo per libpeas Informazioni È necessario disabilitare dei plugin aggiuntivi Deve essere disabilitato un plugin aggiuntivo Si è verificato un errore: %s Caricamento non riuscito della dipendenza «%s» Non è stata trovata la dipendenza «%s» Disabilita plugin A_bilita tutti Abilitato Caricamento non riuscito GTK Peas Plugin Gestore plugin Vista gestore plugin Non è stato trovato il loader del plugin «%s» Plugin Preferen_ze Preferenze Esegui dalla directory di build Mosta incorporati Il plugin «%s» dipende dal plugin «%s».
Disabilitando «%s» verrà disabilitato anche «%s». I seguenti plugin dipendono da «%s» e verranno ugualmente disabilitati: Impossibile caricare il plugin «%s» Si è verificato un errore nel mostrare l'aiuto. Visualizza _Informazioni A_nnulla _Chiudi _Disabilita tutti _Abilitato A_iuto Preferen_ze _Esci 