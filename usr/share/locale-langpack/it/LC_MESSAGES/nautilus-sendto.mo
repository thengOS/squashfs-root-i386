��    	      d      �       �      �   )   �   2        F  ,   T  #   �  "   �  0   �  �  �     �  <   �  <   �     #  4   3  )   h  2   �  5   �                                          	    Archive Could not parse command-line options: %s
 Expects URIs or filenames to be passed as options
 Files to send No mail client installed, not sending files
 Output version information and exit Run from build directory (ignored) Use XID as parent to the send dialogue (ignored) Project-Id-Version: nautilus-sendto 1.0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-03 23:48+0000
PO-Revision-Date: 2014-07-07 15:08+0000
Last-Translator: Gianvito Cavasoli <gianvito@gmx.it>
Language-Team: Italiano <tp@linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:34+0000
X-Generator: Launchpad (build 18115)
Language: 
 Archivio Impossibile analizzare le opzioni della riga di comando: %s
 Attesi degli URI o nomi file da essere passati come opzioni
 File da inviare Nessun client mail installato, nessun invio di file
 Mostra l'informazione di versione ed esce Avviare dalla directory di compilazione (ignorato) Usare XID come parente al dialogo di invio (ignorato) 