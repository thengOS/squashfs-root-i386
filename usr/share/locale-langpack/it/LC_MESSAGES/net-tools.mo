��    �     �  �        `)  P   a)  9   �)  J   �)  A   7*  H   y*  +   �*     �*     +  $   !+  %   F+  %   l+  "   �+  $   �+  6   �+  G   ,  B   Y,     �,     �,     �,  :   �,  :   '-  :   b-  9   �-  G   �-  A   .  O   a.  B   �.  8   �.  P   -/  J   ~/  5   �/  :   �/  @   :0  Q   {0  E   �0  G   1  9   [1  B   �1  5   �1  0   2  F   ?2  7   �2  5   �2  K   �2  ,   @3  $   m3  (   �3  $   �3  $   �3  L   4  B   R4  ?   �4  D   �4  2   5  B   M5  L   �5  K   �5     )6  @   G6      �6  *   �6  4   �6  N   	7  !   X7  G   z7  F   �7  K   	8  A   U8  K   �8  O   �8  U   39      �9  K   �9  &   �9  *   :  0   H:  5   y:  +   �:  *   �:  ,   ;  @   3;  P  t;  #   �<  9   �<  .   #=     R=  C   i=  N   �=  +   �=     (>      D>  =   e>  %   �>  7   �>  #   ?     %?  ;   D?     �?  )   �?      �?      �?  1   �?  ?   .@     n@  ?   �@  "   �@     �@     A  P   A  Q   fA  P   �A  P   	B  
   ZB  	   eB  
   oB     zB     �B     �B     �B     �B  	   �B     �B     �B     �B     �B  ,   C  ,   HC  ;   uC  "   �C     �C     �C  -   D  !   1D     SD     lD     �D  /   �D  -   �D  -   �D  +   $E     PE     cE  '   �E     �E     �E     �E  1   �E  7   $F     \F     tF  !   �F  /   �F     �F  4   �F     .G     DG  *   ^G  %   �G  =   �G  1   �G     H  -   =H  %   kH     �H  ,   �H  *   �H  +   I  8   0I     iI  #   ~I     �I     �I     �I     �I     �I  "   J     0J     DJ     ]J     {J     �J     �J  *   �J  +   K     .K     JK     fK     �K  /   �K  K   �K      L  7   9L  $   qL  E   �L     �L     �L     
M  $   M     ?M  5   ^M     �M     �M     �M  1   �M     N      N     4N     ON     dN     }N     �N  4   �N  :   �N  0   "O     SO     mO     �O     �O     �O  G   �O  }   P     �P     �P     �P     �P     �P     �P     �P     Q     Q     ,Q  
   :Q     EQ  	   RQ     \Q  %   cQ     �Q  b   �Q     R     R     7R     SR     hR  Q   �R  /   �R  #   S     &S  
   4S     ?S     MS  ,   `S  
   �S     �S  
   �S     �S  	   �S  	   �S  
   �S     �S     �S     �S     �S  	   T     T     T  M   +T  C   yT  @   �T     �T  /   U     ?U     EU     QU  "   XU     {U  	   �U  	   �U     �U     �U      �U     �U     �U     V     V     %V     +V  "   7V     ZV     pV  +   �V  ,   �V     �V  #   �V     W     (W     -W  O   :W  N   �W     �W     �W     X     #X     )X     EX     ^X  (   vX     �X     �X     �X  	   �X  	   �X     �X     �X  
   �X     �X     �X  '   Y     +Y     2Y     >Y  $   SY  &   xY  $   �Y  %   �Y  "   �Y  
   Z     Z     %Z  2   .Z     aZ     yZ  %   �Z     �Z     �Z     �Z  $   �Z     �Z     [  7   [  <   H[  7   �[     �[     �[     �[     \  	   \  	   "\     ,\     3\     :\     C\     L\     [\     w\     �\     �\  	   �\  <   �\  #   ]  #   5]     Y]     e]     j]     r]     y]     }]     �]  W   �]  @   ^  F   I^  $   �^  W   �^  2   _  7   @_  +   x_  D   �_  H   �_  N   2`     �`     �`  *   �`  .   �`  /   a  -   5a  /   ca  1   �a  S   �a     b  &   0b  (   Wb  	   �b     �b     �b     �b  ,   �b  &   c  !   *c      Lc     mc      �c  3   �c  '   �c  )   d  /   0d  #   `d     �d     �d     �d     �d  0   �d     #e     >e     Ne     je  @   se     �e     �e     �e     �e     �e  
   f     f     %f     ;f     Of  <   cf  7   �f  ,   �f  &   g  '   ,g  %   Tg  #   zg  %   �g     �g     �g     h     #h     >h     Wh     uh  )   �h     �h     �h     �h     �h     �h     i  !   i     Ai     Yi  "   xi     �i     �i     �i  ,   �i      j     j     6j  +   Nj  (   zj  "   �j      �j  *   �j     k     /k      Ok     pk  *   �k  .   �k  '   �k  )   l  )   :l     dl     vl     �l     �l     �l     �l     �l     �l     
m  )   "m     Lm     [m     rm  N   zm  !   �m  "   �m     n     ,n     Jn     hn  &   �n     �n  �  �n  Q   qp  9   �p  Y   �p  A   Wq  H   �q  +   �q     r     *r  )   Er  *   or  *   �r  '   �r  )   �r  9   s  W   Qs  @   �s     �s     t      #t  ?   Dt  @   �t  A   �t  E   u  U   Mu  G   �u  U   �u  C   Av  9   �v  [   �v  P   w  2   lw  >   �w  E   �w  L   $x  K   qx  L   �x  D   
y  G   Oy  4   �y  0   �y  L   �y  >   Jz  <   �z  Q   �z  1   {  '   J{  1   r{  $   �{  $   �{  X   �{  M   G|  D   �|  K   �|  3   &}  B   Z}  L   �}  K   �}     6~  A   T~      �~  *   �~  4   �~  W     !   o  P   �  J   �  K   -�  K   y�  [   ŀ  N   !�  _   p�  '   Ё  F   ��     ?�  "   [�  6   ~�  3   ��  /   �  /   �  (   I�  @   r�  k  ��  .   �  =   N�  3   ��     ��  V   څ  N   1�  3   ��  0   ��  (   �  Q   �  0   `�  >   ��  )   Ї  +   ��  >   &�     e�  '   u�  &   ��  &   Ĉ  3   �  <   �     \�  C   y�  "   ��     ��     ��  U   �  Q   ]�  X   ��  V   �  
   _�     j�  
   x�  
   ��     ��     ��     ��     ��  
   ǋ     ҋ     �     �      �  0   =�  3   n�  F   ��  *   �     �     3�  ?   P�  %   ��     ��     ύ     �  H   �  ?   M�  E   ��  ,   ӎ      �  '   �  +   :�     f�     �     ��  2   ��  G   �     -�     D�  #   Z�  (   ~�     ��  A   Đ     �     �  4   ;�  5   p�  I   ��  <   �      -�  1   N�  *   ��     ��  9   Ē  -   ��  .   ,�  ;   [�     ��  -   ��  #   ޓ     �     �     7�     D�  %   X�     ~�     ��      ��     ͔  "   �  !   �  )   1�  +   [�     ��  %   ��  !   ˕      �  8   �  T   G�  "   ��  H   ��  .   �  S   7�     ��     ��     ��  +   ӗ     ��  B   �     `�     u�     ��  2   ��     ݘ     �  #   �     *�     ?�     W�  +   k�  C   ��  R   ۙ  @   .�     o�     ��     ��     ǚ     �  \   �  �   O�     ��     �     �     !�     6�     E�     Z�     w�     ��     ��  
   ��     ��  	   Μ     ؜  )   ߜ     	�  f   �     ��     ��     ��     ؝     �  7   �  E   >�  +   ��     ��  
   ��     ɞ     ܞ  :   �  
   ,�     7�  
   =�     H�  	   P�     Z�     c�     o�     ��     ��     ��  	   ��     ��     ��  M   ԟ  C   "�  A   f�     ��  @   ��     �     �     �  !   �     <�  	   E�  	   O�     Y�  %   `�      ��     ��  "   ��     ء     �     ��     ��  &   �     7�     N�  /   f�  0   ��     Ǣ  '   Ӣ     ��     �     �  O   (�  H   x�     ��  !   �     �     �  )   �  !   @�  $   b�  )   ��     ��     ��     ��  	   Ƥ  	   Ф     ڤ     �  
   �     ��     �  +   �     C�     J�     V�  3   o�  )   ��  ,   ͥ  -   ��  <   (�  
   e�     p�     }�  /   ��     ��     Φ  2   �     �     #�  
   '�  (   2�     [�     q�  5   z�  >   ��  M   �     =�     V�     s�     ��     ��  	   ��     ��     Ĩ     ˨     Ԩ     ݨ  &   �     �  #   2�  $   V�  	   {�  J   ��  !   Щ  .   �     !�     .�     3�     ?�     F�  +   J�     v�  W   ��  B   ��  J   0�  "   {�  U   ��  1   ��  5   &�  +   \�  L   ��  S   լ  N   )�     x�     ��  4   ��  9   ٭  9   �  8   M�  :   ��  <   ��  f   ��     e�  !   }�  .   ��     ί  +   ܯ  ,   �  ,   5�  .   b�  )   ��  ,   ��  *   �     �  ,   3�  E   `�  7   ��  4   ޱ  9   �  1   M�  #   �  &   ��     ʲ  )   �  =   �  !   O�     q�      ��     ��  >   ��     ��     �     �     .�  !   :�     \�     i�     w�     ��     ��  P   ��  T   �  ;   [�  )   ��  ,   ��  )   �  '   �  )   @�  '   j�  '   ��  &   ��     �     ��  &   �     ;�  1   P�     ��     ��     ��     ��     ȷ     ܷ  (   ��     #�  (   ?�  -   h�  !   ��     ��     Ƹ  4   ָ     �      '�     H�  C   e�  3   ��      ݹ  +   ��  *   *�      U�     v�      ��     ��  /   պ  :   �  1   @�  ;   r�  ;   ��     �     �     �     *�      B�      c�  !   ��  !   ��     ȼ  +   �     �     �     9�  L   E�  -   ��  .   ��  &   �  (   �  )   ?�  '   i�  /   ��     ��     V  B      �      �       �   �      j  H   �   �       }   �   \  W      �   E   �      �   �  �   �   Z  
          �   +  �       �  Q  �         d                     �  c   �   E  �      �   O        J      z   �  �   �  �              �   h        ?   /       �   �   ,   f   �        �  �  �   �                    �     �   �      �      �  �       �  �       4  �  �       �  r               }  �  �  �      �         ~   �  �   �   �   �         �         �              n   �   �              �  >      �   �   x  M      �        �  C          m   �     9   h  l  �        �           U   �       B   �   �             I  �  f  �      ^   �   �   ;  �  T      �  �  P      �   J   �  =     �   �   �  6   _   u          =  	  z     �       �  �   �   7               9      �      "  y   w    �       �            �          �   8      �      �          �           $   �           �  �             �     K      �  �   �    �   �  �              u   �   �   �       t       �           q  t  �       F   [                  #       �   �      �  (   �   :     �   �   �   �  5   N                  g              �  �      �   �  o  ~  o   �   l   p       s  �      �   �  k   Y   �  i     A  .   �  y     �  �  O  �   4   �      �  �  �  +       �  e  �       Y      �   �   �  �  �   L  �      D   m  �           K   �  w   a  &       �   �  �  8   |   �  �   ]           A   �     G      �  �  '   $  �   `  P   �  -   �  �  Z   �      �   �   �  �       �  5      �              C       L       )             F    �   �   �   @  b      �  �   �  �  �               �   *          e      �   �       �       
   �   `   �  7  �   T   �  3       *          �   [  �   �   �  �   a               �   �   /  �   �   #  �   !  �             �  -    ^  �          \          _  �       @   M   G   .  V     �     �  >   c     �   X   �  (  D     U          �  2            6      0  	   |  �  k  R   S  b   1  �       v     �   g               q       x   �   �          ]  <  '  {       i  �   �       ,  �   3      H  �   r          :      �  �  �   �  �   �   W  �  �       �  �   �   �   I       ?  �   1   �  2  !   Q   �         �  X    n  �   �  �       �    &  %  %   R  �         "   �  N       v   �     �  �        )  �      j   {  d       �   �  ;   �   �   �        p  s   S       0       <       �  �  �         �        
Proto Recv-Q Send-Q Local Address           Foreign Address         State       
Proto RefCnt Flags       Type       State         I-Node 
Unless you are using bind or NIS for host lookups you can change the DNS
                               [mod] [dyn] [reinstate] [[dev] If]
                               [netmask N] [mss Mss] [window W] [irtt I]
                   [nibble NN] [trigger NN]
              compressed:%lu
           %s addr:%s            EtherTalk Phase 2 addr:%s
           IPX/Ethernet 802.2 addr:%s
           IPX/Ethernet 802.3 addr:%s
           IPX/Ethernet II addr:%s
           IPX/Ethernet SNAP addr:%s
           [ [i|o]seq ] [ [i|o]key KEY ] [ [i|o]csum ]
           [ mode { ipip | gre | sit } ] [ remote ADDR ] [ local ADDR ]
           [ ttl TTL ] [ tos TOS ] [ nopmtudisc ] [ dev PHYS_DEV ]
           collisions:%lu            econet addr:%s
           inet6 addr: %s/%d         --numeric-hosts          don't resolve host names
         --numeric-ports          don't resolve port names
         --numeric-users          don't resolve user names
         -A, -p, --protocol       specify protocol family
         -C, --cache              display routing cache instead of FIB

         -D, --use-device         read <hwaddr> from given device
         -F, --fib                display Forwarding Information Base (default)
         -M, --masquerade         display masqueraded connections

         -N, --symbolic           resolve hardware names
         -a                       display (all) hosts in alternative (BSD) style
         -a, --all, --listening   display all sockets (default: connected)
         -c, --continuous         continuous listing

         -d, --delete             delete a specified entry
         -e, --extend             display other/more information
         -f, --file               read new entries from file or from /etc/ethers

         -g, --groups             display multicast group memberships
         -i, --device             specify network interface (e.g. eth0)
         -i, --interfaces         display interface table
         -l, --listening          display listening server sockets
         -n, --numeric            don't resolve names
         -o, --timers             display timers
         -p, --programs           display PID/Program name for sockets
         -r, --route              display routing table
         -s, --set                set a new ARP entry
         -s, --statistics         display networking statistics (like SNMP)
         -v, --verbose            be verbose
        ADDR := { IP_ADDRESS | any }
        KEY  := { DOTTED_QUAD | NUMBER }
        TOS  := { NUMBER | inherit }
        TTL  := { 1..255 | inherit }
        domainname [-v] {nisdomain|-F file}   set NIS domainname (from file)
        hostname -V|--version|-h|--help       print info and exit

        hostname [-v]                         display hostname

        hostname [-v] [-d|-f|-s|-a|-i|-y|-n]  display formatted name
        inet6_route [-FC] flush      NOT supported
        inet6_route [-vF] add Target [gw Gw] [metric M] [[dev] If]
        inet_route [-vF] add {-host|-net} Target[/prefix] [gw Gw] [metric M]
        inet_route [-vF] add {-host|-net} Target[/prefix] [metric M] reject
        ipmaddr -V | -version
        ipmaddr show [ dev STRING ] [ ipv4 | ipv6 | link | all ]
        iptunnel -V | --version

        netstat [-vnNcaeol] [<Socket> ...]
        netstat { [-veenNac] -i | [-cnNe] -M | -s }

        nodename [-v] {nodename|-F file}      set DECnet node name (from file)
        plipconfig -V | --version
        rarp -V                               display program version.

        rarp -d <hostname>                    delete entry from cache.
        rarp -f                               add entries from /etc/ethers.
        rarp [<HW>] -s <hostname> <hwaddr>    add entry to cache.
        route [-v] [-FC] {add|del|flush} ...  Modify routing table for AF.

        route {-V|--version}                  Display version/author and exit.

        route {-h|--help} [<AF>]              Detailed usage syntax for specified AF.
      - no statistics available -     -F, --file            read hostname or NIS domainname from given file

     -a, --alias           alias names
     -d, --domain          DNS domain name
     -f, --fqdn, --long    long host name (FQDN)
     -i, --ip-address      addresses for the hostname
     -n, --node            DECnet node name
     -s, --short           short host name
     -y, --yp, --nis       NIS/YP domainname
     dnsdomainname=hostname -d, {yp,nis,}domainname=hostname -y

    This command can read or set the hostname or the NIS domainname. You can
   also read the DNS domain or the FQDN (fully qualified domain name).
   Unless you are using bind or NIS for host lookups you can change the
   FQDN (Fully Qualified Domain Name) and the DNS domain name (which is
   part of the FQDN) in the /etc/hosts file.
   <AF>=Address family. Default: %s
   <AF>=Use '-6|-4' or '-A <af>' or '--<af>'; default: %s
   <AF>=Use '-A <af>' or '--<af>'; default: %s
   <HW>=Hardware Type.
   <HW>=Use '-H <hw>' to specify hardware address type. Default: %s
   <Socket>={-t|--tcp} {-u|--udp} {-w|--raw} {-x|--unix} --ax25 --ipx --netrom
   Checksum in received packet is required.
   Checksum output packets.
   Drop packets out of sequence.
   List of possible address families (which support routing):
   List of possible address families:
   List of possible hardware types (which support ARP):
   List of possible hardware types:
   Sequence packets on output.
   [[-]broadcast [<address>]]  [[-]pointopoint [<address>]]
   [[-]dynamic]
   [[-]trailers]  [[-]arp]  [[-]allmulti]
   [add <address>[/<prefixlen>]]
   [del <address>[/<prefixlen>]]
   [hw <HW> <address>]  [metric <NN>]  [mtu <NN>]
   [mem_start <NN>]  [io_addr <NN>]  [irq <NN>]  [media <type>]
   [multicast]  [[-]promisc]
   [netmask <address>]  [dstaddr <address>]  [tunnel <address>]
   [outfill <NN>] [keepalive <NN>]
   [txqueuelen <NN>]
   [up|down] ...

   arp [-v]          [-i <if>] -d  <host> [pub]               <-Delete ARP entry
   arp [-v]   [<HW>] [-i <if>] -Ds <host> <if> [netmask <nm>] pub          <-''-

   arp [-v]   [<HW>] [-i <if>] -s  <host> <hwaddr> [temp]            <-Add entry
   arp [-vnD] [<HW>] [-i <if>] -f  [<filename>]            <-Add entry from file
  Bcast:%s   Mask:%s
  P-t-P:%s   Path
  Timer  User  User       Inode       interface %s
  users %d %s	nibble %lu  trigger %lu
 %s (%s) -- no entry
 %s (%s) at  %s: %s/ip  remote %s  local %s  %s: ERROR while getting interface flags: %s
 %s: ERROR while testing interface flags: %s
 %s: You can't change the DNS domain name with this command
 %s: address family not supported!
 %s: bad hardware address
 %s: can't open `%s'
 %s: error fetching interface information: %s
 %s: hardware type not supported!
 %s: illegal option mix.
 %s: invalid %s address.
 %s: name too long
 %s: you must be root to change the domain name
 %s: you must be root to change the host name
 %s: you must be root to change the node name
 %u DSACKs for out of order packets received %u DSACKs received %u DSACKs sent for old packets %u DSACKs sent for out of order packets %u ICMP messages failed %u ICMP messages received %u ICMP messages sent %u ICMP packets dropped because socket was locked %u ICMP packets dropped because they were out-of-window %u SYN cookies received %u SYN cookies sent %u SYNs to LISTEN sockets ignored %u acknowledgments not containing data received %u active connections openings %u active connections rejected because of time stamp %u bad SACKs received %u bad segments received. %u congestion window recovered using DSACK %u congestion windows fully recovered %u congestion windows partially recovered using Hoe heuristic %u congestion windows recovered after partial ack %u connection resets received %u connections aborted due to memory pressure %u connections aborted due to timeout %u connections established %u connections reset due to early user close %u connections reset due to unexpected SYN %u connections reset due to unexpected data %u delayed acks further delayed because of locked socket %u delayed acks sent %u dropped because of missing route %u failed connection attempts %u fast retransmits %u forward retransmits %u forwarded %u fragments created %u fragments dropped after timeout %u fragments failed %u fragments received ok %u incoming packets delivered %u incoming packets discarded %u input ICMP message failed. %u invalid SYN cookies received %u of bytes directly received from backlog %u of bytes directly received from prequeue %u outgoing packets dropped %u packet headers predicted %u packet reassembles failed %u packet receive errors %u packets directly queued to recvmsg prequeue. %u packets dropped from out-of-order queue because of socket buffer overrun %u packets dropped from prequeue %u packets header predicted and directly queued to user %u packets pruned from receive queue %u packets pruned from receive queue because of socket buffer overrun %u packets reassembled ok %u packets received %u packets sent %u packets to unknown port received. %u passive connection openings %u passive connections rejected because of time stamp %u predicted acknowledgments %u reassemblies required %u requests sent out %u resets received for embryonic SYN_RECV sockets %u resets sent %u retransmits lost %u sack retransmits failed %u segments received %u segments retransmited %u segments send out %u timeouts after SACK recovery %u times recovered from packet loss due to SACK data %u times recovered from packet loss due to fast retransmit %u times the listen queue of a socket overflowed %u total packets received %u with invalid addresses %u with invalid headers %u with unknown protocol (Cisco)-HDLC (No info could be read for "-p": geteuid()=%d but you should be root.)
 (Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
 (auto) (incomplete) (only servers) (servers and established) (w/o servers) 16/4 Mbps Token Ring 16/4 Mbps Token Ring (New) 6-bit Serial Line IP <from_interface> <incomplete>  AMPR AX.25 AMPR NET/ROM AMPR ROSE ARCnet AX.25 not configured in this system.
 Active AX.25 sockets
 Active IPX sockets
Proto Recv-Q Send-Q Local Address              Foreign Address            State Active Internet connections  Active NET/ROM sockets
 Active UNIX domain sockets  Active X.25 sockets
 Adaptive Serial Line IP Address                  HWtype  HWaddress           Flags Mask            Iface
 Address deletion not supported on this system.
 Address family `%s' not supported.
 Appletalk DDP BROADCAST  Bad address.
 Base address:0x%x  Broadcast tunnel requires a source address.
 CCITT X.25 CLOSE CLOSE_WAIT CLOSING CONN SENT CONNECTED CONNECTING Cannot create socket DARPA Internet DEBUG  DGRAM DISC SENT DISCONNECTING Default TTL is %u Dest         Source          Device  LCI  State        Vr/Vs  Send-Q  Recv-Q
 Dest       Source     Device  State        Vr/Vs    Send-Q  Recv-Q
 Destination               Router Net                Router Node
 Device not found Don't know how to set addresses for family %d.
 ESTAB ESTABLISHED Econet Entries: %d	Skipped: %d	Found: %d
 Ethernet FIN_WAIT1 FIN_WAIT2 FREE Failed to get type of [%s]
 Fiber Distributed Data Interface Forwarding is %s Frame Relay Access Device Frame Relay DLCI Generic EUI-64 HIPPI HWaddr %s   Hardware type `%s' not supported.
 ICMP input histogram: ICMP output histogram: INET (IPv4) not configured in this system.
 INET6 (IPv6) not configured in this system.
 IPIP Tunnel IPX not configured in this system.
 IPv4 Group Memberships
 IPv6 IPv6-in-IPv4 Iface   MTU Met   RX-OK RX-ERR RX-DRP RX-OVR    TX-OK TX-ERR TX-DRP TX-OVR Flg
 Iface   MTU Met   RX-OK RX-ERR RX-DRP RX-OVR   TX-OK TX-ERR TX-DRP TX-OVR Flg
 Interface       RefCnt Group
 Interface %s not initialized
 Interrupt:%d  IrLAP Kernel AX.25 routing table
 Kernel IP routing table
 Kernel Interface table
 Keys are not allowed with ipip and sit.
 LAPB LAST_ACK LISTEN LISTENING LOOPBACK  Local Loopback MASTER  MULTICAST  Media:%s Memory:%lx-%lx  NET/ROM not configured in this system.
 NOARP  NOTRAILERS  No ARP entry for %s
 No routing for address family `%s'.
 No support for ECONET on this system.
 No support for INET on this system.
 No support for INET6 on this system.
 No usable address families found.
 Novell IPX POINTOPOINT  PROMISC  Please don't supply more than one address family.
 Point-to-Point Protocol Problem reading data from %s
 Quick ack mode was activated %u times RAW RDM RECOVERY ROSE not configured in this system.
 RTO algorithm is %s RUNNING  RX bytes:%llu (%lu.%lu %s)  TX bytes:%llu (%lu.%lu %s)
 RX: Packets    Bytes        Errors CsumErrs OutOfSeq Mcasts
 Ran %u times out of system memory during packet sending Resolving `%s' ...
 Result: h_addr_list=`%s'
 Result: h_aliases=`%s'
 Result: h_name=`%s'
 SABM SENT SEQPACKET SLAVE  STREAM SYN_RECV SYN_SENT Serial Line IP Setting domainname to `%s'
 Setting hostname to `%s'
 Setting nodename to `%s'
 TCP ran low on memory %u times TIME_WAIT TX: Packets    Bytes        Errors DeadLoop NoRoute  NoBufs
 This kernel does not support RARP.
 Too much address family arguments.
 UNIX Domain UNK. UNKNOWN UNSPEC UP  Unknown address family `%s'.
 Unknown media type.
 Usage:
  arp [-vn]  [<HW>] [-i <if>] [-a] [<hostname>]             <-Display ARP cache
 Usage:
  ifconfig [-a] [-v] [-s] <interface> [[<AF>] <address>]
 Usage: hostname [-v] {hostname|-F file}      set hostname (from file)
 Usage: inet6_route [-vF] del Target
 Usage: inet_route [-vF] del {-host|-net} Target[/prefix] [gw Gw] [metric M] [[dev] If]
 Usage: ipmaddr [ add | del ] MULTIADDR dev STRING
 Usage: iptunnel { add | change | del | show } [ NAME ]
 Usage: plipconfig [-a] [-i] [-v] interface
 Usage: rarp -a                               list entries in cache.
 Usage: route [-nNvee] [-FC] [<AF>]           List kernel routing tables
 User       Dest       Source     Device  State        Vr/Vs    Send-Q  Recv-Q
 VJ 6-bit Serial Line IP VJ Serial Line IP WARNING: at least one error occured. (%d)
 Warning: Interface %s still in ALLMULTI mode.
 Warning: Interface %s still in BROADCAST mode.
 Warning: Interface %s still in DYNAMIC mode.
 Warning: Interface %s still in MULTICAST mode.
 Warning: Interface %s still in POINTOPOINT mode.
 Warning: Interface %s still in promisc mode... maybe other application is running?
 Where: NAME := STRING
 Wrong format of /proc/net/dev. Sorry.
 You cannot start PPP with this program.
 [UNKNOWN] address mask replies: %u address mask request: %u address mask requests: %u arp: %s: hardware type without ARP support.
 arp: %s: kernel only supports 'inet'.
 arp: %s: unknown address family.
 arp: %s: unknown hardware type.
 arp: -N not yet supported.
 arp: cannot open etherfile %s !
 arp: cannot set entry on line %u of etherfile %s !
 arp: cant get HW-Address for `%s': %s.
 arp: device `%s' has HW address %s `%s'.
 arp: format error on line %u of etherfile %s !
 arp: in %d entries no match found.
 arp: invalid hardware address
 arp: need hardware address
 arp: need host name
 arp: protocol type mismatch.
 cannot determine tunnel mode (ipip, gre or sit)
 cannot open /proc/net/snmp compressed:%lu  destination unreachable: %u disabled domain name (which is part of the FQDN) in the /etc/hosts file.
 echo replies: %u echo request: %u echo requests: %u enabled error parsing /proc/net/snmp family %d  generic X.25 getdomainname()=`%s'
 gethostname()=`%s'
 getnodename()=`%s'
 hw address type `%s' has no handler to set address. failed.
 ifconfig: Cannot set address for this protocol family.
 ifconfig: `--help' gives usage information.
 ifconfig: option `%s' not recognised.
 in_arcnet(%s): invalid arcnet address!
 in_ether(%s): invalid ether address!
 in_fddi(%s): invalid fddi address!
 in_hippi(%s): invalid hippi address!
 ip: %s is invalid IPv4 address
 ip: %s is invalid inet address
 ip: %s is invalid inet prefix
 ip: argument is wrong: %s
 keepalive (%2.2f/%ld/%d) missing interface information netmask %s  netstat: unsupported address family %d !
 no RARP entry for %s.
 off (0.00/%ld/%d) on %s
 on (%2.2f/%ld/%d) on%d (%2.2f/%ld/%d) problem reading data from %s
 rarp: %s: unknown hardware type.
 rarp: %s: unknown host
 rarp: cannot open file %s:%s.
 rarp: cannot set entry from %s:%u
 rarp: format error at %s:%u
 redirect: %u redirects: %u route: %s: cannot use a NETWORK as gateway!
 route: Invalid MSS/MTU.
 route: Invalid initial rtt.
 route: Invalid window.
 route: netmask doesn't match route address
 rresolve: unsupport address family %d !
 slattach: /dev/%s already locked!
 slattach: cannot write PID file
 slattach: setvbuf(stdout,0,_IOLBF,0) : %s
 slattach: tty name too long
 slattach: tty_hangup(DROP): %s
 slattach: tty_hangup(RAISE): %s
 slattach: tty_lock: (%s): %s
 slattach: tty_lock: UUCP user %s unknown!
 slattach: tty_open: cannot get current state!
 slattach: tty_open: cannot set %s bps!
 slattach: tty_open: cannot set 8N1 mode!
 slattach: tty_open: cannot set RAW mode!
 source quench: %u source quenches: %u time exceeded: %u timeout in transit: %u timestamp replies: %u timestamp reply: %u timestamp request: %u timestamp requests: %u timewait (%2.2f/%ld/%d) ttl != 0 and noptmudisc are incompatible
 txqueuelen:%d  unkn-%d (%2.2f/%ld/%d) unknown usage: netstat [-veenNcCF] [<Af>] -r         netstat {-V|--version|-h|--help}
 warning, got bogus igmp line %d.
 warning, got bogus igmp6 line %d.
 warning, got bogus raw line.
 warning, got bogus tcp line.
 warning, got bogus udp line.
 warning, got bogus unix line.
 warning: no inet socket available: %s
 wrong parameters: %u Project-Id-Version: net-tools
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2007-06-30 12:28+0900
PO-Revision-Date: 2012-10-02 19:42+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:13+0000
X-Generator: Launchpad (build 18115)
 
Proto CodaRic CodaInv Indirizzo locale        Indirizzo remoto       Stato       
Proto RefCnt Flag        Tipo       Stato         I-Node 
Si può modificare il DNS a meno che non si usino bind o NIS per le ricerche degli host
                               [mod] [dyn] [reinstate] [[dev] If]
                               [netmask N] [mss Mss] [window W] [irtt I]
                   [nibble NN] [trigger NN]
              compressi:%lu
           indirizzo %s:%s            indirizzo EtherTalk Phase 2:%s
           indirizzo IPX/Ethernet 802.2:%s
           indirizzo IPX/Ethernet 802.3:%s
           indirizzo IPX/Ethernet II:%s
           indirizzo IPX/Ethernet SNAP:%s
           [ [i|o]seq ] [ [i|o]key CHIAVE ] [ [i|o]csum ]
           [ modalità { ipip | gre | sit } ] [ INDIRIZZO remoto ] [ INDIRIZZO locale ]
           [ ttl TTL ] [ tos TOS ] [ nopmtudisc ] [ dev SCHEDA ]
           collisioni:%lu            indirizzo econet:%s
           indirizzo inet6: %s/%d         --numeric-hosts          non risolve i nomi degli host
         --numeric-ports          non risolve i nomi delle porte
         --numeric-users          non risolve i nomi degli utenti
         -A, -p, --protocol       specifica la famiglia di protocolli
         -C, --cache              Mostra la cache d'instradamento al posto della FIB

         -D, --use-device         legge <hwaddr> da un dato dispositivo
         -F, --fib                Mostra la Forwarding Information Base (predefinito)
         -M, --masquerade         mostra le connessioni mascherate

         -N, --symbolic           risolve i nomi hardware
         -a                       visualizza (tutti) gli host nello stile alternativo (BSD)
         -a, --all, --listening   mostra tutti i sockets (predefinito: connessi)
         -c, --continuous         elenco continuo

         -d, --delete             elimina una voce specificata
         -e, --extend             mostra altre/ulteriori informazioni
         -f, --file               legge nuove voci da file o da /etc/ethers

         -g, --groups             mostra l'appartenenza ai gruppi multicast
         -i, --device             specifica l'interfaccia di rete (es. eth0)
         -i, --interfaces         mostra la tabella delle interfacce
         -l, --listening          mostra i socket in ascolto dei server
         -n, --numeric            non risolve i nomi
         -o, --timers             mostra i timer
         -p, --programs           Mostra PID/Nome del programma per i socket
         -r, --route              mostra la tabella di routing
         -s, --set                imposta una nuova voce ARP
         -s, --statistics         mostra le statistiche di networking (come SNMP)
         -v, --verbose            messaggi estesi
        INDIR := { INDIRIZZO_IP | any }
        CHIAVE  := { QUARTETTO_PUNTATO | NUMERO }
        TOS  := { NUMERO | inherit }
        TTL  := { 1..255 | inherit }
        domainname [-v] {dominionis|-F file}   imposta il nome del dominio NIS (da file)
        hostname -V|--version|-h|--help       stampa le informazioni ed esce

        hostname [-v]                         visualizza l'hostname

        hostname [-v] [-d|-f|-s|-a|-i|-y|-n]  visualizza il nome formattato
        inet6_route [-FC] flush      NON supportato
        inet6_route [-vF] add Target [gw Gw] [metric M] [[dev] If]
        inet_route [-vF] add {-host|-net} Target[/prefix] [gw Gw] [metric M]
        inet_route [-vF] add {-host|-net} Target[/prefix] [metric M] reject
        ipmaddr -V | -version
        ipmaddr show [ dev STRINGA ] [ ipv4 | ipv6 | link | all ]
        iptunnel -V | --version

        netstat [-vnNcaeol] [<Socket> ...]
        netstat { [-veenNac] -i | [-cnNe] -M | -s }

        nodename [-v] {nomenodo|-F file}      imposta il nome del nodo DECnet (da file)
        plipconfig -V | --version
        rarp -V                               mostra la versione del programma.

        rarp -d <hostname>                    rimuove la voce dalla cache.
        rarp -f                               aggiunge voci da /etc/ethers.
        rarp [<HW>] -s <hostname> <hwaddr>    aggiunge una voce alla cache.
        route [-v] [-FC] {add|del|flush} ...  Modifica la tabella di instradamento per AF.

        route {-V|--version}                  Mostra versione/autore ed esce.

        route {-h|--help} [<AF>]              Sintassi di uso dettagliata per l'AF specificata.
      - nessuna statistica disponibile -     -F, --file legge l'hostname o il domainname NIS dal file fornito

     -a, --alias nomi alias
     -d, --domain nome dominio DNS
     -f, --fqdn, --long    nome lungo dell'host (FQDN)
     -i, --ip-address      indirizzi per l'hostname
     -n, --node            nome del nodo DECnet
     -s, --short           nome breve dell'host
     -y, --yp, --nis nome dominio NIS/YP
     dnsdomainname=hostname -d, {yp,nis,}domainname=hostname -y

    Questo comando può leggere o impostare l'hostname o il nome del dominio NIS.
   È anche possibile leggere il dominio DNS o il FQDN (fully qualified domain name).
   A meno di usare bind o NIS per le ricerche degli host, è possibile cambiare
   il FQDN (fully qualified domain name) e il nome del dominio DNS (che fa parte 
   del FQDN) nel file /etc/hosts,
   <AF>=Famiglia di indirizzi. Predefinito: %s
   <AF>=Usare "-6|-4" o "-A <af>" o "--<af>"; predefinito: %s
   <AF>=Usare "-A <af>" o "--<af>"; predefinito: %s
   <HW>=Tipo di hardware.
   <HW>=Usare "-H <hw>" per specificare il tipo di indirizzo hardware. Predefinito: %s
   <Socket>={-t|--tcp} {-u|--udp} {-w|--raw} {-x|--unix} --ax25 --ipx --netrom
   È richiesto il checksum nel pacchetto ricevuto.
   Effettua il checksum dei pacchetti in uscita.
   Tralascia i pacchetti fuori sequenza.
   Elenco delle possibili famiglie di indirizzi (che supportano l'instradamento):
   Elenco delle possibili famiglie di indirizzi:
   Elenco dei possibili tipi di hardware (che supportano ARP):
   Elenco dei possibili tipi di hardware:
   Mette in sequenza i pacchetti in uscita.
   [[-]broadcast [<indirizzo>]] [[-]pointopoint [<indirizzo>]]
   [[-]dynamic]
   [[-]trailers] [[-]arp] [[-]allmulti]
   [add <indirizzo>[/<lunghprefisso>]]
   [del <indirizzo>[/<lunghprefisso>]]
   [hw <HW> <indirizzo>]  [metric <NN>]  [mtu <NN>]
   [mem_start <NN>] [io_addr <NN>] [irq <NN>] [media <tipo>]
   [multicast]  [[-]promisc]
   [netmask <indirizzo>] [dstaddr <indirizzo>] [tunnel <indirizzo>]
   [outfill <NN>] [keepalive <NN>]
   [txqueuelen <NN>]
   [up|down] ...

   arp [-v]          [-i <if>] -d  <host> [pub]               <-Cancella una voce ARP
   arp [-v]   [<HW>] [-i <if>] -Ds <host> <if> [netmask <nm>] pub          <-''-

   arp [-v]   [<HW>] [-i <if>] -s  <host> <hwaddr> [temp]            <-Aggiunge una voce
   arp [-vnD] [<HW>] [-i <if>] -f  [<filename>]            <-Aggiunge una voce da file
  Bcast:%s   Maschera:%s
  P-t-P:%s   Percorso
  Tempi  Utente  Utente       Inode       interfaccia %s
  utenti %d %s	nibble %lu  trigger %lu
 %s (%s) -- nessuna voce
 %s (%s) associato a  %s: %s/ip  remoto %s  locale %s  %s: ERRORE leggendo i flag dell'interfaccia: %s
 %s: ERRORE nel testare i flag dell'interfaccia: %s
 %s: Non si può modificare il nome del dominio DNS con questo comando
 %s: famiglia di indirizzi non supportata.
 %s: indirizzo hardware errato
 %s: impossibile aprire "%s"
 %s: errore nel recuperare le informazioni dell'interfaccia: %s
 %s: tipo di hardware non supportato.
 %s: opzioni non valide.
 %s: indirizzo %s non valido.
 %s: nome troppo lungo
 %s: sono necessari i privilegi di root per cambiare il nome del dominio
 %s: sono necessari i privilegi di root per cambiare l'hostname
 %s: sono necessari i privilegi di root per cambiare il nome del nodo
 %u DSACK ricevuti per pacchetti fuori ordine %u DSACK ricevuti %u DSACK inviati per pacchetti obsoleti %u DSACK inviati per pacchetti fuori ordine %u messaggi ICMP falliti %u messaggi ICMP ricevuti %u messaggi ICMP inviati %u pacchetti ICMP scartati causa blocco del socket %u pacchetti ICMP scartati perché fuori della finestra di trasmissione %u cookie SYN ricevuti %u cookie SYN inviati %u SYN verso socket LISTEN ignorati %u conferme ricevute non contenenti dati %u connessioni attive aperte %u connessioni attive rifiutate a causa della marcatura temporale %u SACK errati ricevuti %u segmenti errati ricevuti. %u finestre di congestione ripristinate usando DSACK %u finestre di congestione completamente ripristinate %u finestre di congestione ripristinate parzialmente usando euristica Hoe %u finestre di congestione ripristinate dopo un ack parziale %u reset di connessione ricevuti %u connessioni annullate per scarsità di memoria %u connessioni annullate per tempo scaduto %u connessioni stabilite %u connessioni reimpostate per chiusura anticipata utente %u connessioni reimpostate per SYN non atteso %u connessioni reimpostate per dati non attesi %u ack ritardati e nuovamente ritardati per socket bloccato %u ack ritardati inviati %u scartato a causa di instradamento mancante %u tentativi di connessione falliti %u ritrasmissioni rapide %u ritrasmissioni inoltrate %u inoltrati %u frammenti creati %u frammenti scartati dopo il timeout %u frammenti falliti %u frammenti ricevuti ok %u pacchetti entranti consegnati %u pacchetti entranti scartati %u messaggi ICMP in input falliti. %u cookie SYN non validi ricevuti %u byte ricevuti direttamente dal backlog %u byte ricevuti direttamente dalla precoda %u pacchetti uscenti scartati %u intestazioni di pacchetto previste %u pacchetti riassemblati falliti %u errori sui pacchetti ricevuti %u pacchetti accodati direttamente alla precoda recvmsg. %u pacchetti scartati dalla coda dei fuori uso per riempimento del buffer del socket %u pacchetti perduti dalla precoda %u intestazioni di pacchetto previste e accodate direttamente all'utente %u pacchetti eliminati dalla coda di ricezione %u pacchetti scartati dalla coda di ricezione per riempimento del buffer del socket %u pacchetti riassemblati ok %u pacchetti ricevuti %u pacchetti inviati %u pacchetti ricevuti su porta sconosciuta. %u connessioni passive aperte %u connessioni passive rifiutate a causa della marcatura temporale %u conferme previste %u richieste di riassemblamento %u richieste inviate %u reset ricevuti per i socket embrionici SYN_RECV %u reset inviati %u ritrasmissioni perse %u ritrasmissioni sack non riuscite %u segmenti ricevuti %u segmenti ritrasmessi %u segmenti inviati Tempo scaduto %u volte dopo ripristino SACK Ripristinato %u volte per perdita di pacchetti a causa di dati SACK Ripristinato %u volte per perdita di pacchetti a causa della ritrasmissione veloce La coda di ascolto di un socket è stata sovraccaricata %u volte %u pacchetti totali ricevuti %u con indirizzi non validi %u con intestazioni non valide %u con protocollo sconosciuto (Cisco)-HDLC (Nessuna informazione può essere letta per "-p": geteuid()=%d ma si dovrebbe essere root.)
 (Non tutti i processi potrebbero essere identificati, le informazioni sui processi non propri
 non saranno mostrate, per visualizzarle tutte bisogna avere privilegi di root.)
 (auto) (incompleto) (solo server) (server e stabiliti) (senza server) Token Ring 16/4 Mbps Token Ring 16/4 Mbps (nuovo) Serial Line IP a 6 bit <interfaccia_di_partenza> <incompleto>  AMPR AX.25 AMPR NET/ROM AMPR ROSE ARCnet AX.25 non configurato in questo sistema.
 Socket AX.25 attivi
 Socket IPX attivi
Proto Recv-Q Send-Q Indirizzo locale              Indirizzo esterno            Stato Connessioni Internet attive  Socket NET/ROM attivi
 Socket in dominio UNIX attivi  Socket X.25 attivi
 Serial Line IP adattativo Indirizzo TipoHW IndirizzoHW Flag Maschera Interfaccia
 La cancellazione dell'indirizzo non è supportata su questo sistema.
 Famiglia di indirizzi "%s" non supportata.
 Appletalk DDP BROADCAST  Indirizzo errato.
 Indirizzo base:0x%x  Il tunnel di trasmissione richiede un indirizzo sorgente.
 CCITT X.25 CLOSE CLOSE_WAIT CLOSING CONN SENT CONNESSO CONNESSIONE Impossibile creare socket DARPA Internet DEBUG  DGRAM DISC SENT DISCONNESSIONE TTL predefinito è %u Destin.      Sorgente        Device  LCI  Stato        Vr/Vs  Send-Q  Recv-Q
 Destin.    Sorgente   Device  Stato        Vr/Vs    Send-Q  Recv-Q
 Destinazione             Rete del router         Nodo del router
 Dispositivo non trovato Non si conosce come impostare gli indirizzi per la famiglia %d.
 ESTAB ESTABLISHED Econet Voci: %d	Saltate: %d	Trovate: %d
 Ethernet FIN_WAIT1 FIN_WAIT2 LIBERO Impossibile ottenere il tipo di [%s]
 Fiber Distributed Data Interface Inoltro è %s Dispositivo di accesso frame relay Frame Relay DLCI EUI-64 generico HIPPI IndirizzoHW %s   Tipo di hardware "%s" non supportato.
 Istogramma input ICMP: Istogramma output ICMP: INET (IPv4) non configurato in questo sistema.
 INET6 (IPv6) non configurato in questo sistema.
 Tunnel IPIP IPX non configurato in questo sistema.
 Gruppo di appartenza IPv4
 IPv6 IPv6 in IPv4 Iface   MTU Met   RX-OK RX-ERR RX-DRP RX-OVR    TX-OK TX-ERR TX-DRP TX-OVR Flg
 Iface MTU Met RX-OK RX-ERR RX-DRP RX-OVR TX-OK TX-ERR TX-DRP TX-OVR Flg
 Interfaccia     RefCnt Gruppo
 interfaccia %s non inizializzata
 Interrupt:%d  IrLAP Tabella d'instradamento del kernel AX.25
 Tabella di routing IP del kernel
 Tabella dell'interfaccia del kernel
 Non sono permesse chiavi con ipip e sit.
 LAPB LAST_ACK LISTEN LISTENING LOOPBACK  Loopback locale MASTER  MULTICAST  Media:%s Memoria:%lx-%lx  NET/ROM non configurato in questo sistema.
 NOARP  NOTRAILERS  Nessuna voce ARP per %s
 Nessun percorso per la famiglia di indirizzi "%s".
 ECONET non supportato su questo sistema.
 Nessun supporto per INET in questo sistema.
 Nessun supporto per INET6 in questo sistema.
 Non è stata trovata nessuna famiglia di indirizzi usabile.
 Novell IPX POINTOPOINT  PROMISC  Non fornire più di una famiglia di indirizzi.
 Point-to-Point Protocol Problema leggendo dati da %s
 La modalità ack rapidi è stata attivata %u volte RAW RDM RIPRISTINO ROSE non configurato in questo sistema.
 l'algoritmo RTO è %s RUNNING  Byte RX:%llu (%lu.%lu %s)  Byte TX:%llu (%lu.%lu %s)
 RICEZ: Pacchetti    Byte        Errori ErrCsum FuoriSeq Mcast
 È stata esaurita la memoria di sistema %u volte durante l'invio di pacchetti Risoluzione di "%s" ...
 Risultato: h_addr_list="%s"
 Risultato: h_aliases="%s"
 Risultato: h_name="%s"
 SABM INVIATO SEQPACKET SLAVE  STREAM SYN_RECV SYN_SENT Serial Line IP Impostazione nome del dominio in "%s"
 Impostazione hostname in "%s"
 Impostazione nome del nodo in "%s"
 TCP ha terminato la memoria %u volte TIME_WAIT INVIO: Pacchetti    Byte        Errori CicliMorti SenzaInstrad  SenzaBuff
 Questo kernel non supporta RARP.
 Troppi argomenti della famiglia di indirizzi.
 Dominio UNIX UNK. SCONOSCIUTO UNSPEC UP  Famiglia di indirizzi non conosciuta "%s".
 Tipo di supporto sconosciuto.
 Uso:
  arp [-vn]  [<HW>] [-i <if>] [-a] [<hostname>]             <-Mostra la cache ARP
 Uso:
  ifconfig [-a] [-v] [-s] <interfaccia> [[<AF>] <indirizzo>]
 Uso: hostname [-v] {hostname|-F file}        imposta l'hostname (da file)
 Uso: inet6_route [-vF] del Target
 Uso: inet_route [-vF] del {-host|-net} Target[/prefix] [gw Gw] [metric M] [[dev] If]
 Uso: ipmaddr [ add | del ] MULTIADDR dev STRINGA
 Uso: iptunnel { add | change | del | show } [ NOME ]
 Uso: plipconfig [-a] [-i] [-v] interfaccia
 Uso: rarp -a                                    elenca le voci nella cache.
 Uso: route [-nNvee] [-FC] [<AF>]           Elenca le tabelle di routing del kernel
 Utente     Dest       Sorgente   Device  Stato        Vr/Vs    Send-Q  Recv-Q
 VJ Serial Line IP a 6 bit VJ Serial Line IP ATTENZIONE: si è presentato almeno un errore. (%d)
 Attenzione: l'interfaccia %s è ancora in modo ALLMULTI.
 Attenzione: l'nterfaccia %s è ancora in modo BROADCAST.
 Attenzione: l'interfaccia %s è ancora in modo DYNAMIC.
 Attenzione: l'interfaccia %s è ancora in modo MULTICAST.
 Attenzione: l'interfaccia %s è ancora in modo POINTOPOINT.
 Attenzione: l'interfaccia %s è ancora in modalità promiscua: sono in esecuzione altre applicazioni?
 Dove:  NOME := STRINGA
 Formato errato di /proc/net/dev.
 Impossibile avviare PPP con questo programma.
 [SCONOSCIUTO] risposte della maschera degli indirizzi: %u richieste della maschera degli indirizzi: %u richieste della maschera degli indirizzi: %u arp: %s: tipo di hardware senza supporto ARP.
 arp: %s: il kernel supporta solo "inet".
 arp: %s: famiglia di indirizzi sconosciuta.
 arp: %s: tipo di hardware non conosciuto.
 arp: -N non ancora supportato.
 arp: impossibile aprire il file di rete %s.
 arp: impossibile impostare la voce alla riga %u del file di rete %s.
 arp: impossibile ottenere l'indirizzo HW per "%s": %s.
 arp: il dispositivo "%s" ha l'indirizzo HW %s "%s".
 arp: errore di formato alla riga %u del file di rete %s.
 arp: nessuna corrispondenza trovata tra %d voci.
 arp: indirizzo hardware non valido
 arp: necessario un indirizzo hardware
 arp: necessario un hostname
 arp: tipo protocollo non corrispondente.
 impossibile determinare il modo del tunnel (ipip, gre o sit)
 impossibile aprire /proc/net/snmp compressi:%lu  destinazione irraggiungibile: %u disabilitato nome del dominio (che è parte del FQDN) nel file /etc/hosts.
 risposte di echo: %u richiesta di echo: %u richieste di echo: %u abililitato errore analizzando /proc/net/snmp famiglia %d  X.25 generico getdomainname()="%s"
 gethostname()="%s"
 getnodename()="%s"
 il tipo di indirizzo hw "%s" non ha gestori per impostare l'indirizzo. Fallito.
 ifconfig: Non è possibile impostare l'indirizzo per questa famiglia di protocolli.
 ifconfig: "--help" visualizza le informazioni di utilizzo.
 ifconfig: opzione "%s" non riconosciuta.
 in_arcnet(%s): indirizzo arcnet non valido.
 in_ether(%s): indirizzo ether non valido
 in_fddi(%s): indirizzo fddi non valido
 in_hippi(%s): indirizzo hippi non valido
 ip: %s è un indirizzo IPv4 non valido
 ip: %s è un indirizzo inet non valido
 ip: %s è un prefisso inet non valido
 ip: argomento errato: %s
 keepalive (%2.2f/%ld/%d) informazioni dell'interfaccia mancanti maschera di rete %s  netstat: famiglia di indirizzi non supportata %d
 nessuna voce RARP per %s.
 off (0.00/%ld/%d) su %s
 on (%2.2f/%ld/%d) on%d (%2.2f/%ld/%d) problema leggendo dati da %s
 rarp: %s: tipo di hardware sconosciuto.
 rarp: %s: host sconosciuto
 rarp: impossibile aprire il file %s:%s.
 rarp: impossibile impostare la voce da %s:%u
 rarp: errore di formato in %s:%u
 rediretto: %u ridirezioni: %u route: %s: impossibile usare una RETE come gateway.
 route: MSS/MTU non valido.
 route: rtt iniziale non valido.
 route: finestra non valida.
 route: la netmask non corrisponde con l'indirizzo di instradamento
 rresolve: famiglia di indirizzi %d non supportata.
 slattach: /dev/%s già bloccato
 slattach: impossibile scrivere il file PID
 slattach: setvbuf(stdout,0,_IOLBF,0) : %s
 slattach: nome tty troppo lungo
 slattach: tty_hangup(DROP): %s
 slattach: tty_hangup(RAISE): %s
 slattach: tty_lock: (%s): %s
 slattach: tty_lock: utente UUCP %s sconosciuto
 slattach: tty_open: impossibile ottenere lo stato attuale
 slattach: tty_open: impossibile impostare %s bps
 slattach: tty_open: impossibile impostare la modalità 8N1
 slattach: tty_open: impossibile impostare la modalità RAW
 sorgente distrutta: %u sorgenti distrutte: %u tempo scaduto: %u timeout in transito: %u marcatura temporale risposte: %u marcatura temporale risposta: %u marcatura temporale richiesta: %u marcatura temporale richieste: %u timewait (%2.2f/%ld/%d) ttl != 0 e noptmudisc non sono compatibili
 txqueuelen:%d  %d-sconosciuto (%2.2f/%ld/%d) sconosciuto uso: netstat [-veenNcCF] [<Af>] -r         netstat {-V|--version|-h|--help}
 attenzione, trovata riga %d falsata di igmp.
 attenzione, trovata riga %d falsata di igmp6.
 attenzione, trovata riga falsata raw.
 attenzione trovata riga falsata di tcp.
 attenzione, trovata riga falsata di ugp.
 attenzione, trovata riga falsata unix.
 attenzione: nessun socket inet disponibile: %s
 parametri errati: %u 