��    Z      �     �      �  g   �     !  *   :  2   e  &   �  5   �  /   �     %	  1   =	  B   o	     �	  	   �	     �	     �	  *   �	  =   "
  2   `
  )   �
  D   �
       '        D     b     �  $   �  !   �     �     �  /        K  9   Q     �  %   �  7   �  6     $   :     _     r     �     �     �     �     �     �  3     =   ;  2   y  �   �  �  v  R   s     �  8   �            <   1     n  "   ~     �  ,   �  =   �     #     4  $   O     t     �  $   �  ?   �     �               1     9     Y  !   j     �     �     �     �      �     �  #        <  
   Y     d  %   w     �     �     �     �  �  �  S   �     �  .   �  *   .  +   Y  0   �  3   �     �  7     E   >     �     �     �  )   �  :   �  Q     U   m  7   �  d   �  (   `  ?   �  -   �  -   �  &   %  3   L  0   �  $   �  (   �  4   �     4  K   ;     �  -   �  @   �  <     &   O     v  -   �     �  #   �     �  #   �  &   #     J  6   i  V   �  6   �  �   .     !  j   #     |#  J   �#     �#     �#  H   �#     B$  6   R$     �$  >   �$  H   �$     !%      4%  2   U%     �%     �%  '   �%  J   �%      &  
   6&     A&  
   X&  #   c&     �&  %   �&  
   �&  %   �&     �&     '  ,   '  (   J'  "   s'     �'     �'     �'  '   �'     (     (     6(     F(     3   T                      *   I       P   1   X   K   >                                
   (   2   J   :      .   F                     L                       	   A                   <   E   B                    =       #      '      N             +   Q   M      %   D              H   /              @          ;   !      ,      $   8   9      U   C              &   0   4   6       W   S   7      ?   O             )   V      -   Z   "           R   G         Y   5          PID   | Name                 | Port    | Flags
  ------+----------------------+---------+-----------
 %-14s - eject the CDROM
 %-14s - prints a list of clients attached
 %-14s - queries the server for certain parameters
 %-14s - reconfigure server parameters
 %-14s - reinitialising the keyboard and the trackpad
 %-14s - save the current configuration to disk
 %-14s - suspend to RAM
 %s (version %s) - control client for pbbuttonsd.
 %s - daemon to support special features of laptops and notebooks.
 %s, version %s <unknown> Buffer overflow Can't attach card '%s': %s
 Can't create message port for server: %s.
 Can't find pmud on port 879. Trigger PMU directly for sleep.
 Can't get devmask from mixer [%s]; using default.
 Can't get volume of master channel [%s].
 Can't install PMU input handler. Some functionality may be missing.
 Can't load card '%s': %s
 Can't open %s. Eject CDROM won't work.
 Can't open ADB device %s: %s
 Can't open PMU device %s: %s
 Can't open card '%s': %s
 Can't open framebuffer device '%s'.
 Can't open mixer device [%s]. %s
 Can't register mixer: %s
 Card '%s' has no '%s' element.
 Current battery cycle: %d, active logfile: %s.
 ERROR ERROR: Have problems reading configuration file [%s]: %s
 ERROR: Missing arguments
 ERROR: Not enough memory for buffer.
 ERROR: Problems with IPC, maybe server is not running.
 ERROR: Unexpected answer from server, actioncode %ld.
 ERROR: tag/data pairs not complete.
 File doesn't exist File not a block device File not a file Help or version info INFO Memory allocation failed.
 Memory allocation failed: %s
 Messageport not available Mixer element '%s' has no playback volume control.
 No event devices available. Please check your configuration.
 Option 'ALSA_Elements' contains no valid elements
 Options:
    -%c, --help               display this help and exit
    -%c, --version            display version information and exit
    -%c, --ignore             ignore config return and error values
 Options:
   -%c, --help               display this help text and exit
   -%c, --version            display version information and exit
   -%c, --quiet              suppress welcome message
   -%c, --detach[=PIDFILE]   start %s as background process and
                            optional use an alternative pid-file
                            (default: %s)
   -%c, --configfile=CONFIG  use alternative configuration file
                            (default: %s)
see configuration file for more options.
 Orphaned server port found and removed. All running clients have to be restarted.
 Permission denied Please check your CDROM settings in configuration file.
 Private Tag Registration failed SECURITY: %s must be owned by the same owner as pbbuttonsd.
 Script '%s' %s
 Script must be write-only by owner Server already running Server didn't send an answer and timed out.
 Server is already running. Sorry, only one instance allowed.
 Server not found Setting of %s failed: %s.
 Sorry, Couldn't get data for %s: %s. Supported commands:
 Supported tags:
 The leading 'TAG_' could be omited.
 Too many formatsigns. Max three %%s allowed in TAG_SCRIPTPMCS.
 Unknown PowerBook Usage:
 Usage: %s [OPTION]... 
 WARNING WARNING: tag %s not supported.
 argument invalid can't be launched - fork() failed doesn't exist failed - unknown error code format error function not supported lauched but exitcode is not null lauched but exited by signal lauched but killed after %d seconds launched and exited normally open error pbbcmd, version %s pmud support compiled in and active.
 read-only value skipped because it's not secure unknown tag write-only value Project-Id-Version: pbbuttonsd
Report-Msgid-Bugs-To: matthiasgrimm@users.sourceforge.net
POT-Creation-Date: 2007-07-07 19:36+0200
PO-Revision-Date: 2007-10-02 20:39+0000
Last-Translator: Nicola Piovesan <Unknown>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:35+0000
X-Generator: Launchpad (build 18115)
   PID | Nome | Porta | Flags
  ------+----------------------+---------+-----------
 %-14s - espelli il CDROM
 %-14s - mostra una lista dei client attaccati
 %-14s - chiedi al server alcuni parametri
 %-14s - riconfigura i parametri del server
 %-14s - reinizializza la tastiera e il trackpad
 %-14s - salva la configurazione corrente sul disco
 %-14s - sospendi sulla RAM
 %s (versione %s) - client di controllo per pbbuttonsd.
 %s - daemon per le funzionalità speciali dei laptop e dei notebook.
 %s, versione %s <sconosciuto> Overflow del buffer Impossibile attaccare la scheda '%s': %s
 Impossibile crare la porta dei messaggi per il server: %s
 Impossibile trovare pmud sulla porta 879. Imposta PMU direttamente per lo sleep.
 Impossibile ottenere la devmask dal mixer [%s]; utilizzo le impostazioni di default.
 Impossibile ottenere il volume del canale master [%s].
 Impossibile installare il gestore di input PMU. Alcune funzionalità potrebbero risultare mancanti.
 Impossibile caricare la scheda '%s': %s
 Impossibile aprire %s. L'espulsione del CDROM non funzionerà.
 Impossibile aprire il dispositivo ADM %s: %s
 Impossibile aprire il dispositivo PMU %s: %s
 Impossibile aprire la scheda '%s': %s
 Impossibile aprire la periferica framebuffer '%s'.
 Impossibile aprire la periferica mixer [%s]. %s
 Impossibile registrare il mixer: %s
 La scheda '%s' non ha un elemento '%s'.
 Ciclo batteria attuale: %d, file di log attivo: %s.
 ERRORE ERRORE: Ci sono stati problemi leggendo il file di configurazione [%s]: %s
 ERRORE: argomenti mancanti
 Errore: Memoria insufficiente per il buffer.
 Errore: Problemi con IPC, forse il server non è in esecuzione.
 ERRORE: Risposta inattesa dal server, codice di azione %ld.
 ERRORE: coppie tag/dati non complete.
 Il file non esiste Il file non è un dispositivo di tipo 'block' Il file non è un file Aiuto o informazioni sulla versione INFORMAZIONE Fallita allocazione della memoria.
 Fallita allocazione della memoria: %s
 Porta messaggi non disponibile L'elemento mixer '%s' non ha un controllo del volume.
 Nessuna periferica di eventi disponibile. Per favore controlla la tua configurazione.
 L'opzione "ALSA_Elements' non contiene elmenti validi
 Opzioni:
    -%c, --help mostra questo messaggio di aiuto ed esci
    -%c, --version mostra informazioni riguardo la versione ed esci
    -%c, --ignore ignora il ritorno dei valori di configurazione e degli errori
 Opzioni:
   -%c, --help mostra questo testo di aiuto ed esci
   -%c, --version mostra informazioni riguardo la versione ed esci
   -%c, --quiet non mostrare il messaggio di benvenuto
   -%c, --detach[=FILEPID] avvia %s come processo in background e
                            opzionalmente usa un altro file per il pid
                            (default: %s)
   -%c, --configfile=CONFIG usa un altro file di configurazione
                            (default: %s)
leggi il file di configurazione per ulteriori opzioni.
 Una porta server orfana è stata trovata e rimossa. Tutti i client in esecuzione devono essere riavviati.
 Permesso negato Per favore contrlla le tue impostazioni CDROM nel file di configurazione.
 Tag privato Registrazione fallita SICUREZZA: %s deve essere di proprietà del proprietario di pbbuttonsd.
 Script '%s' %s
 Lo script deve essere scrivibile solo dal proprietario Il server è gi Il server non ha mandato una risposta ed è scaduto il tempo.
 Il server è già in esecuzione. Scusa, è consentita una sola istanza.
 Server non trovato Impostazione di %s fallite: %s.
 Spiacente, impossibile ottenere i dati per %s: %s. Comandi supportati
 Tag supportati:
 Il 'TAG_' iniziale può essere omesso.
 Troppi segni di formato. Al massimo tre %%s consentiti in TAG_SCRIPTPMCS.
 PowerBook sconosciuto Utilizzo:
 Uso: %s [OPZIONE]... 
 ATTENZIONE ATTENZIONE: tag %s non supportato.
 argomento non valido impossibile eseguire - fallito fork() non esiste fallito - codice d'errore sconosciuto errore di formato funzione non supportata eseguito ma il codice di uscita non è nullo eseguito ma uscito a causa di un segnale eseguito ma ucciso dopo %d secondi eseguito ed uscito normalmente errore di apertura pbbcmd, versione %s supporto per pmud compilato ed attivo.
 valore di sola lettura saltato perché non sicuro tag sconosciuto valore di sola scrittura 