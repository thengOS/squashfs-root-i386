��          �      ,      �     �     �     �     �  �   �  �   �  ~        �  /   �     �     �     �  ?        C     Q  
   c    n     r     z      �     �  �   �  �   �  �     	   �  ;   �     �     �     	  4   '	  
   \	     g	  
   y	        	                                                                  
    %s (%s) <small><b>Action:</b></small> <small><b>Vendor:</b></small> <small><b>_Details</b></small> An application is attempting to perform an action that requires privileges. Authentication as one of the users below is required to perform this action. An application is attempting to perform an action that requires privileges. Authentication as the super user is required to perform this action. An application is attempting to perform an action that requires privileges. Authentication is required to perform this action. Authenticate Authentication dialog was dismissed by the user Click to edit %s Click to open %s Select user... Your authentication attempt was unsuccessful. Please try again. _Authenticate _Password for %s: _Password: Project-Id-Version: policykit-gnome 0.9
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=policykit-gnome&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-04 00:05+0000
PO-Revision-Date: 2012-01-23 18:34+0000
Last-Translator: Gianvito Cavasoli <gianvito@gmx.it>
Language-Team: Italiana <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:41+0000
X-Generator: Launchpad (build 18115)
 %s (%s) <small><b>Azione:</b></small> <small><b>Fornitore:</b></small> <small><b>_Dettagli</b></small> Un'applicazione sta tentando di compiere un'azione che richiede dei privilegi. È richiesto autenticarsi come uno degli utenti elencati sotto per eseguire tale azione. Un'applicazione sta tentando di compiere un'azione che richiede dei privilegi. È richiesto autenticarsi come super utente per eseguire tale azione. Un'applicazione sta tentando di compiere un'azione che richiede dei privilegi. È richiesto autenticarsi per eseguire tale azione. Autentica Il dialogo di autenticazione è stato annullato dall'utente Fare clic per modificare %s Fare clic per aprire %s Seleziona utente... Tentativo di autenticazione non riuscito. Riprovare. _Autentica Pass_word per %s: Pass_word: 