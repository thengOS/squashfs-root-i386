��    �      �  �   �	        �     *    r   9    �  Z   �    '  �  �  _  �  }  �  �   f  �   Z  @  �  �   )   m   �   r   B!  \   �!  �  "  j  �#  �   I%  [   C&  `   �&  N    '  \   O'  �  �'    �,  �   �1  p  V2    �4  @   �5  '  6    87     T8     k8  %   ~8  *   �8     �8     �8  B   9  E   D9  L   �9     �9     �9  $   �9  !   :     @:     X:  .   n:  =   �:  0   �:     ;  -   (;     V;     r;  ,   �;  !   �;  (   �;     <     "<     A<     ]<     s<      �<     �<     �<     �<     �<     =  #   <=  -   `=      �=  %   �=  #   �=  0   �=  H   *>  .   s>     �>     �>     �>     �>     �>  =   ?  C   @?  (   �?  '   �?  +   �?  .   @  B   0@  9   s@  3   �@     �@  #   �@     A     =A     SA     sA      �A     �A     �A     �A  '   �A  &   #B  #   JB     nB     �B     �B     �B     �B  0   �B  1   C     =C  #   TC     xC  %   �C  #   �C  <   �C  &   D  O   DD  �   �D  3   E  X   �E  /   F     <F  @   SF  (   �F     �F     �F  "   �F  �   G     �G  =   �G  $   *H     OH  U   nH  *   �H  d   �H  <   TI  g   �I     �I     J     4J  '   RJ     zJ  9   �J  �   �J  +   ]K  )   �K     �K  @   �K     L     ,L      ?L     `L  .   wL  /   �L  �  �L    |N  �  �O  �   �S  c  T  c   wV  �  �V  �  {Y  �  ]  �  �^    �`  �   �a  |  hb  �   �c  l   �d  s   e  j   �e    �e  �  h    �i  f   �j  l   "k  ]   �k  b   �k  u  Pl    �q  �   �w  :  �x  9  �{  K   }  Y  g}  8  �~     �     �  /   *�  ;   Z�     ��  -   ��  [   �  M   >�  Z   ��     �     ��  (   �  -   ;�  1   i�     ��  ;   ��  I   ��  7   :�  1   r�  7   ��  '   ܃  *   �  ;   /�  4   k�  3   ��  '   Ԅ  @   ��      =�     ^�     v�  #   ��  $   ��  +   Ӆ  (   ��  #   (�      L�  $   m�  H   ��  )   ۆ  8   �  /   >�  =   n�  W   ��  C   �     H�     b�     z�     ��     ��  U   ��  Q   �  ,   g�  ,   ��  5   ��  /   ��  K   '�  2   s�  2   ��     ي  )   ��     #�     C�  &   ^�     ��  $   ��      ɋ     �  %   	�  (   /�  -   X�  2   ��     ��     Ռ     �     �     �  5   5�  6   k�      ��  ,   Í  #   ��  3   �  "   H�  S   k�  %   ��  A   �    '�  B   B�  e   ��  -   �     �  >   2�  &   q�     ��     ��      ґ  �   �     ��  ;   ��  "   ��     �  R   ;�  (   ��  b   ��  :   �  e   U�     ��     ؔ     �  %   �     4�  7   R�  �   ��  )   �  )   ;�     e�  @   ~�     ��     ږ     �     
�  <   �  =   \�     v           �          \   =              �           Q   I   c          �   �   y   G          ~   w      �      p           h   �      #   a          �   L           F       �   /   n   m             �   0              d      l   ?      4   9   ^       o          E   3   �   [   i   �   _   D   �          H   Y   b       r   J   e             >   �       W   8          .   f       2   !           }   6       ;   Z   �       O   j   `   (       )   
   U   s       �   x   �           �   A          K   5       +          �   &   �   	   <   7           :   @                      $      %   g           1   k       �   "   �   �   X          �   P   u   t   -   N       ,   �      R          T           �   �      �   ]   z   C   |   M   *       '               S   �   {      q       B   V              
Add one or more files to the topmost or named patch.  Files must be
added to the patch before being modified.  Files that are modified by
patches already applied on top of the specified patch cannot be added.

-p patch
	Patch to add files to.
 
Apply patch(es) from the series file.  Without options, the next patch
in the series file is applied.  When a number is specified, apply the
specified number of patches.  When a patch name is specified, apply
all patches up to and including the specified patch.  Patch names may
include the patches/ prefix, which means that filename completion can
be used.

-a	Apply all patches in the series file.

-f	Force apply, even if the patch has rejects.

-q	Quiet operation.

-v	Verbose operation.

--leave-rejects
	Leave around the reject files patch produced, even if the patch
	is not actually applied.

--interactive
	Allow the patch utility to ask how to deal with conflicts. If
	this option is not given, the -f option will be passed to the 
	patch program.

--color[=always|auto|never]
	Use syntax coloring.
 
Create a new patch with the specified file name, and insert it after the
topmost patch in the patch series file.
 
Create mail messages from all patches in the series file, and either store
them in a mailbox file, or send them immediately. The editor is opened
with a template for the introductory message. Please see the file
%s for details.

--mbox file
	Store all messages in the specified file in mbox format. The mbox
	can later be sent using formail, for example.

--send
	Send the messages directly using %s.

--from, --subject
	The values for the From and Subject headers to use.

--to, --cc, --bcc
	Append a recipient to the To, Cc, or Bcc header.
 
Edit the specified file(s) in \$EDITOR (%s) after adding it (them) to
the topmost patch.
 
Fork the topmost patch.  Forking a patch means creating a verbatim copy
of it under a new name, and use that new name instead of the original
one in the current series.  This is useful when a patch has to be
modified, but the original version of it should be preserved, e.g.
because it is used in another series, or for the history.  A typical
sequence of commands would be: fork, edit, refresh.

If new_name is missing, the name of the forked patch will be the current
patch name, followed by \"-2\".  If the patch name already ends in a
dash-and-number, the number is further incremented (e.g., patch.diff,
patch-2.diff, patch-3.diff).
 
Generate a dot(1) directed graph showing the dependencies between
applied patches. A patch depends on another patch if both touch the same
file or, with the --lines option, if their modifications overlap. Unless
otherwise specified, the graph includes all patches that the topmost
patch depends on.
When a patch name is specified, instead of the topmost patch, create a
graph for the specified patch. The graph will include all other patches
that this patch depends on, as well as all patches that depend on this
patch.

--all	Generate a graph including all applied patches and their
	dependencies. (Unapplied patches are not included.)

--reduce
	Eliminate transitive edges from the graph.

--lines[=num]
	Compute dependencies by looking at the lines the patches modify.
	Unless a different num is specified, two lines of context are
	included.

--edge-labels=files
	Label graph edges with the file names that the adjacent patches
	modify.

-T ps	Directly produce a PostScript output file.
 
Global options:

--trace
	Runs the command in bash trace mode (-x). For internal debugging.

--quiltrc file
	Use the specified configuration file instead of ~/.quiltrc (or
	/etc/quilt.quiltrc if ~/.quiltrc does not exist).  See the pdf
	documentation for details about its possible contents.

--version
	Print the version number and exit immediately. 
Grep through the source files, recursively, skipping patches and quilt
meta-information. If no filename argument is given, the whole source
tree is searched. Please see the grep(1) manual page for options.

-h	Print this help. The grep -h option can be passed after a
	double-dash (--). Search expressions that start with a dash
	can be passed after a second double-dash (-- --).
 
Import external patches.

-p num
	Number of directory levels to strip when applying (default=1)

-n patch
	Patch filename to use inside quilt. This option can only be
	used when importing a single patch.

-f	Overwite/update existing patches.
 
Initializes a source tree from an rpm spec file or a quilt series file.

-d	optional path prefix (sub-directory).

-v	verbose debug output.
 
Integrate the patch read from standard input into the topmost patch:
After making sure that all files modified are part of the topmost
patch, the patch is applied with the specified strip level (which
defaults to 1).

-p strip-level
	The number of pathname components to strip from file names
	when applying patchfile.
 
Please remove all patches using \`quilt pop -a' from the quilt version used to create this working tree, or remove the %s directory and apply the patches from scratch.\n 
Print a list of applied patches, or all patches up to and including the
specified patch in the file series.
 
Print a list of patches that are not applied, or all patches that follow
the specified patch in the series file.
 
Print an annotated listing of the specified file showing which
patches modify which lines.
 
Print or change the header of the topmost or specified patch.

-a, -r, -e
	Append to (-a) or replace (-r) the exiting patch header, or
	edit (-e) the header in \$EDITOR (%s). If none of these options is
	given, print the patch header.
	
--strip-diffstat
	Strip diffstat output from the header.

--strip-trailing-whitespace
	Strip trailing whitespace at the end of lines of the header.

--backup
	Create a backup copy of the old version of a patch as patch~.
 
Print the list of files that the topmost or specified patch changes.

-a	List all files in all applied patches.

-l	Add patch name to output.

-v	Verbose, more user friendly output.

--combine patch
	Create a listing for all patches between this patch and
	the topmost applied patch. A patch name of \"-\" is equivalent
	to specifying the first applied patch.

 
Print the list of patches that modify the specified file. (Uses a
heuristic to determine which files are modified by unapplied patches.
Note that this heuristic is much slower than scanning applied patches.)

-v	Verbose, more user friendly output.
 
Print the name of the next patch after the specified or topmost patch in
the series file.
 
Print the name of the previous patch before the specified or topmost
patch in the series file.
 
Print the name of the topmost patch on the current stack of applied
patches.
 
Print the names of all patches in the series file.

-v	Verbose, more user friendly output.
 
Produces a diff of the specified file(s) in the topmost or specified
patch.  If no files are specified, all files that are modified are
included.

-p n	Create a -p n style patch (-p0 or -p1 are supported).

-u, -U num, -c, -C num
	Create a unified diff (-u, -U) with num lines of context. Create
	a context diff (-c, -C) with num lines of context. The number of
	context lines defaults to 3.

--no-timestamps
	Do not include file timestamps in patch headers.

--no-index
	Do not output Index: lines.

-z	Write to standard output the changes that have been made
	relative to the topmost or specified patch.

-R	Create a reverse diff.

-P patch
	Create a diff for the specified patch.  (Defaults to the topmost
	patch.)

--combine patch
	Create a combined diff for all patches between this patch and
	the patch specified with -P. A patch name of \"-\" is equivalent
	to specifying the first applied patch.

--snapshot
	Diff against snapshot (see \`quilt snapshot -h').

--diff=utility
	Use the specified utility for generating the diff. The utility
	is invoked with the original and new file name as arguments.

--color[=always|auto|never]
	Use syntax coloring.

--sort	Sort files by their name instead of preserving the original order.
 
Refreshes the specified patch, or the topmost patch by default.
Documentation that comes before the actual patch in the patch file is
retained.

It is possible to refresh patches that are not on top.  If any patches
on top of the patch to refresh modify the same files, the script aborts
by default.  Patches can still be refreshed with -f.  In that case this
script will print a warning for each shadowed file, changes by more
recent patches will be ignored, and only changes in files that have not
been modified by any more recent patches will end up in the specified
patch.

-p n	Create a -p n style patch (-p0 or -p1 supported).

-u, -U num, -c, -C num
	Create a unified diff (-u, -U) with num lines of context. Create
	a context diff (-c, -C) with num lines of context. The number of
	context lines defaults to 3.

--no-timestamps
	Do not include file timestamps in patch headers.
	
--no-index
	Do not output Index: lines.

--diffstat
	Add a diffstat section to the patch header, or replace the
	existing diffstat section.

-f	Enforce refreshing of a patch that is not on top.

--backup
	Create a backup copy of the old version of a patch as patch~.

--sort	Sort files by their name instead of preserving the original order.

--strip-trailing-whitespace
	Strip trailing whitespace at the end of lines.
 
Remove one or more files from the topmost or named patch.  Files that
are modified by patches on top of the specified patch cannot be removed.

-p patch
	Patch to remove files from.
 
Remove patch(es) from the stack of applied patches.  Without options,
the topmost patch is removed.  When a number is specified, remove the
specified number of patches.  When a patch name is specified, remove
patches until the specified patch end up on top of the stack.  Patch
names may include the patches/ prefix, which means that filename
completion can be used.

-a	Remove all applied patches.

-f	Force remove. The state before the patch(es) were applied will
	be restored from backup files.

-R	Always verify if the patch removes cleanly; don't rely on
	timestamp checks.

-q	Quiet operation.

-v	Verbose operation.
 
Remove the specified or topmost patch from the series file.  If the
patch is applied, quilt will attempt to remove it first. (Only the
topmost patch can be removed right now.)

-n	Delete the next patch after topmost, rather than the specified
	or topmost patch.
 
Rename the topmost or named patch.

-p patch
	Patch to rename.
 
Take a snapshot of the current working state.  After taking the snapshot,
the tree can be modified in the usual ways, including pushing and
popping patches.  A diff against the tree at the moment of the
snapshot can be generated with \`quilt diff --snapshot'.

-d	Only remove current snapshot.
 
Upgrade the meta-data in a working tree from an old version of quilt to the
current version. This command is only needed when the quilt meta-data format
has changed, and the working tree still contains old-format meta-data. In that
case, quilt will request to run \`quilt upgrade'.
        quilt --version %s: I'm confused.
 Appended text to header of patch %s\n Applied patch %s (forced; needs refresh)\n Applying patch %s\n Cannot add symbolic link %s\n Cannot diff patches with -p%s, please specify -p0 or -p1 instead\n Cannot refresh patches with -p%s, please specify -p0 or -p1 instead\n Cannot use --strip-trailing-whitespace on a patch that has shadowed files.\n Commands are: Conversion failed\n Converting meta-data to version %s\n Delivery address '%s' is invalid
 Diff failed, aborting\n Directory %s exists\n Display name '%s' contains invalid characters
 Display name '%s' contains non-printable or 8-bit characters
 Display name '%s' contains unpaired parentheses
 Failed to back up file %s\n Failed to copy files to temporary directory\n Failed to create patch %s\n Failed to import patch %s\n Failed to insert patch %s into file series\n Failed to patch temporary files\n Failed to remove file %s from patch %s\n Failed to remove patch %s\n Failed to rename %s to %s: %s
 File %s added to patch %s\n File %s disappeared!
 File %s exists\n File %s is already in patch %s\n File %s is not being modified\n File %s is not in patch %s\n File %s may be corrupted\n File %s modified by patch %s\n File %s removed from patch %s\n File \`%s' is located below \`%s'\n File series fully applied, ends at patch %s\n Fork of patch %s created as %s\n Fork of patch %s to patch %s failed\n Importing patch %s (stored as %s)\n Interrupted by user; patch %s was not applied.\n More recent patches modify files in patch %s. Enforce refresh with -f.\n More recent patches modify files in patch %s\n No next patch\n No patch removed\n No patches applied\n Nothing in patch %s\n Now at patch %s\n Option \`-n' can only be used when importing a single patch\n Options \`-c patch', \`--snapshot', and \`-z' cannot be combined.\n Patch %s appears to be empty, removing\n Patch %s appears to be empty; applied\n Patch %s does not apply (enforce with -f)\n Patch %s does not exist; applied empty patch\n Patch %s does not remove cleanly (refresh it or enforce with -f)\n Patch %s exists already, please choose a different name\n Patch %s exists already, please choose a new name\n Patch %s exists already\n Patch %s exists. Replace with -f.\n Patch %s is already applied\n Patch %s is applied\n Patch %s is currently applied\n Patch %s is not applied\n Patch %s is not in series file\n Patch %s is not in series\n Patch %s is now on top\n Patch %s is unchanged\n Patch %s needs to be refreshed first.\n Patch %s not applied before patch %s\n Patch %s not found in file series\n Patch %s renamed to %s\n Patch is not applied\n Refreshed patch %s\n Removed patch %s\n Removing patch %s\n Removing trailing whitespace from line %s of %s
 Removing trailing whitespace from lines %s of %s
 Renaming %s to %s: %s
 Renaming of patch %s to %s failed\n Replaced header of patch %s\n Replacing patch %s with new version\n SYNOPSIS: %s [-p num] [-n] [patch]
 The %%prep section of %s failed; results may be incomplete\n The -v option will show rpm's output\n The quilt meta-data in %s are already in the version %s format; nothing to do\n The quilt meta-data in this tree has version %s, but this version of quilt can only handle meta-data formats up to and including version %s. Please pop all the patches using the version of quilt used to push them before downgrading.\n The topmost patch %s needs to be refreshed first.\n The working tree was created by an older version of quilt. Please run 'quilt upgrade'.\n USAGE: %s {-s|-u} section file [< replacement]
 Unpacking archive %s\n Usage: quilt [--trace[=verbose]] [--quiltrc=XX] command [-h] ... Usage: quilt add [-p patch] {file} ...\n Usage: quilt annotate {file}\n Usage: quilt applied [patch]\n Usage: quilt delete [patch | -n]\n Usage: quilt diff [-p n] [-u|-U num|-c|-C num] [--combine patch|-z] [-R] [-P patch] [--snapshot] [--diff=utility] [--no-timestamps] [--no-index] [--sort] [--color] [file ...]\n Usage: quilt edit file ...\n Usage: quilt files [-v] [-a] [-l] [--combine patch] [patch]\n Usage: quilt fold [-p strip-level]\n Usage: quilt fork [new_name]\n Usage: quilt graph [--all] [--reduce] [--lines[=num]] [--edge-labels=files] [patch]\n Usage: quilt grep [-h|options] {pattern}\n Usage: quilt header [-a|-r|-e] [--backup] [--strip-diffstat] [--strip-trailing-whitespace] [patch]\n Usage: quilt import [-f] [-p num] [-n patch] patchfile ...\n Usage: quilt mail {--mbox file|--send} [--from ...] [--to ...] [--cc ...] [--bcc ...] [--subject ...]\n Usage: quilt new {patchname}\n Usage: quilt next [patch]\n Usage: quilt patches {file}\n Usage: quilt pop [-afRqv] [num|patch]\n Usage: quilt previous [patch]\n Usage: quilt push [-afqv] [--leave-rejects] [num|patch]\n Usage: quilt refresh [-p n] [-f] [--no-timestamps] [--no-index] [--diffstat] [--sort] [--backup] [--strip-trailing-whitespace] [patch]\n Usage: quilt remove [-p patch] {file} ...\n Usage: quilt rename [-p patch] new_name\n Usage: quilt series [-v]\n Usage: quilt setup [-d path-prefix] [-v] {specfile|seriesfile}\n Usage: quilt snapshot [-d]\n Usage: quilt top\n Usage: quilt unapplied [patch]\n Usage: quilt upgrade\n Warning: trailing whitespace in line %s of %s
 Warning: trailing whitespace in lines %s of %s
 Project-Id-Version: quilt
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-06-27 17:14+0000
PO-Revision-Date: 2013-03-01 08:16+0000
Last-Translator: Gianfranco Frisani <gfrisani@libero.it>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:14+0000
X-Generator: Launchpad (build 18115)
 
Aggiunge almeno un file al posto più alto o alla patch nominata. I file devono essere
aggiunti alla patch prima di apportare le modifiche. I file modificati da
patch già applicate prima della patch specificata non possono essere aggiunti.

-p patch
	Patch a cui aggiungere i file.
 
Applica una o più patch dal file series. Senza opzioni, viene applicata la patch
successiva nel file series. Quando viene specificato un numero, applica il 
numero specificato di patch. Quando viene specificato un nome di patch, 
applica tutte le patch al di sopra di quella specificata, inclusa quest'ultima.
In nomi di patch possono includere il prefisso patches/, che significa che può
essere usato il completamento del nome.

-a	Applica tutte le patch nel file series.

-f	Forza l'applicazione, anche se la patch è stata rifiutata.

-q	Operazione invisibile all'utente.

-v	Operazione con informazioni dettagliate.

--leave-rejects
	Conserva i file di rifiuto prodotti dalla patch, anche se quest'ultima
	non è stata infine applicata.

--interactive
	Consente la raccolta di informazioni da parte di utility patch circa la
	risoluzione di conflitti. Se questa opzione non viene fornita, verrà 
	inviata al programma patch l'opzione -f. 

--color[=always|auto|never]
	Usa la colorazione della sintassi.
 
Crea una nuova patch con il nome del file specificato e la inserisce dopo
la patch al posto più alto del file series della patch.
 
Crea messaggi di posta elettronica da tutte le patch presenti nel file series, e
li memorizza in un file mailbox o li invia immediatamente. Il gestore viene avviato
con un modello per il messaggio introduttivo. Per maggiori informazioni
consultare il file %s.

--mbox file
	Memorizza tutti i messaggi nel file specificato in formato mbox. Questo
	file può successivamente essere inviato usando, per esempio, formail.

--send
	Invia direttamente i messaggi usando %s.

--from, --subject
	I valori da usare per le testate From e Oggetto.

--to, --cc, --bcc
	Aggiunge un destinatario alla testata To, Cc, o Bcc.
 
Modifica i file specificati in \$EDITOR (%s) dopo averli aggiunti nella patch
al posto più alto.
 
Duplica la patch al posto più in alto. Duplicare una patch significa creare una 
sua copia integrale con un nuovo nome, e usare il nuovo nome al posto di
quello originale nel series attuale. Ciò è utile quando è necessario modificare
una patch, ma deve essere conservata la versione originale, per esempio
in quanto viene usata in altri series o per conservare la cronologia. Una tipica
sequenza di comandi è: fork, edit, refresh.

Se manca il nuovo nome, il nome della patch duplicata sarà il nome corrente,
seguito da \"-2\". Se il nome della patch termina già con trattino-e-numero,
il numero verrà aumentato (per es. patch.diff,
patch-2.diff, patch-3.diff).
 
Genera un grafico direzionato dot(1) che mostra le dipendenze tra
le patch applicate. Una patch dipende da un'altra se entrambe interessano 
il medesimo file o, con l'opzione --lines, se le loro modifiche si sovrappongono.
A meno che non sia diversamente specificato, il grafico include sia tutte le 
altre patch dalle quali dipende questa patch che quelle dipendenti da
quest'ultima.

--all	Genera un grafico che include tutte le patch applicate e le loro
	dipendenze.  (Le patch non applicate non sono incluse).

-- reduce
	Elimina gli spigoli transitivi dal grafico.

--lines[=num]
	Calcola le dipendenze in base alle righe modificate dalla patch.
	A meno che non sia specificato un diverso numero, vengono incluse
	due righe di contesto.

--edge-labels=files
	Etichetta gli spigoli del grafico con i nomi di file modificati dalle
	patch adiacenti.

-T ps	Produce direttamente un file di output PostScript.
 
Opzioni globali:

--trace
	Per il debug interno; esegue il comando nella modalità trace di bash (-x).

--quiltrc file
	Usa un file di configurazione specificato invece di ~/.quiltrc (o
	/etc/quilt.quiltrc se ~/quiltrc non esiste). Consultare la documentazione
	pdf per avere informazioni a proposito del suo contenuto.

--version
	Stampa il numero della versione e termina la sua esecuzione immediatamente. 
Effettua una ricerca ricorsiva tra i file sorgenti, ignorando i metadati di patch e quilt. 
Se non viene fornito alcun nome di file come argomento, viene esaminato l'intero 
albero sorgente. Consultare la pagina del manuale di grep(1) per ulteriori informazioni. 

-h	Stampare questa documentazione. L'opzione -h di grep può essere fornita dopo
	un doppio trattino (--). Le espressioni di ricerca che iniziano con un trattino
	possono essere fornite dopo un ulteriore doppio trattino (-- --).
 
Importa patch esterne.

-p num
	Numero di livelli di directory da eliminare (per impostazione predefinita=1)

-n patch
	Nome di file della patch da usare in quilt. L'opzione può essere usata
	solo nell'importazione di una singola patch.

-f	Sovrascrive o aggiorna le patch esistenti.
 
Inizializza un albero sorgente a partire da un file spec rpm o da un file series di quilt.

-d	Prefisso del percorso, opzionale (sotto-directory)

-v	Stampa informazioni di debug
 
Integra la patch letta dall'input standard con la patch al posto più in alto:
Dopo che sono stati modificati tutti i file della patch al posto più in alto,
quest'ultima viene applicata con il livello di strip specificato (per
impostazione predefinita è 1).

-p strip-level
	Il numero di componenti del percorso da togliere dai nomi dei file
	quando viene applicato patchfile.
 
Rimuovere tutte le patch usando \`quilt pop -a' dalla versione di quilt utilizzata per creare questo albero in funzione o rimuovere la directory %s e applicare le patch partendo dall'inizio.\n 
Stampa una lista delle patch applicate o di tutte le patch del file series
superiori a quella specificata.
 
Stampa un elenco delle patch non applicate, o di tutte le patch che
seguono la patch specificata nel file series.
 
Stampa un elenco in cui sono annotate le patch che modificano
il file specificato e le linee modificate.
 
Stampa o modifica la testata della patch al posto più in alto o di quella specificata.

-a, -r, -e
	Accoda (-a) o sostituisce (-r) la testata della patch all'uscita, o
	modifica (-e) la testata in \$EDITOR (%s). Se nessuna di queste opzioni viene
	fornita, stampa la testata della patch.
	
--strip-diffstat
	Elimina l'output diffstat dalla testata.

--strip-trailing-whitespace
	Elimina lo spazio in bianco finale delle righe della testata.

--backup
	Crea una copia di backup della versione precedente di una patch come patch~.
 
Stampa la lista dei file modificati dalla patch selezionata o da quella al posto più alto.

-a	Elenca tutti i file nelle patch applicate.

-l	Aggiunge il nome della patch all'output.

-v	Verbose, un output più amichevole.

--combine patch
	Crea un elenco di tutte le patch tra quella selezionata
	e quella applicata al posto più alto. Il nome \"-\" assegnato
	ad una patch rappresenta la prima patch applicata.

 
Stampa una lista delle patch che modificano il file specificato (viene usato
l'algoritmo euristico per determinare i file modificati dalle patch non applicate.
L'algoritmo euristico è più lento della scansione delle patch applicate.)

-v	Verbose, output più amichevole.
 
Stampa il nome della patch che segue quella specificata o quella al posto più alto
nel file series.
 
Stampa i nome della patch precedente prima di quella specificata o
di quella più in alto nel file series.
 
Stampa il nome della patch al posto più in alto nella pila corrente delle
patch applicate.
 
Stampa i nomi di tutte le patch presenti nel file series.

-v	Prolisso, stampa più informazioni
 
Produce una diff del file o dei file specificati nella patch al posto più in
alto o in quella specificata. Se non è specificato nessun file, vengono
inclusi tutti i file modificati.

-p n 	Crea una patch stile -p n (sono supportati -p0 e -p1).

-u, -U num, -c, -C num
	Crea una diff unificata (-u, -U) con un numero di righe di contesto. Crea
	una diff di contesto (-c, -C) con un numero di righe di contesto. Il numero
	predefinito di righe di contesto è 3.

--no-timestamps
	Non include una marca temporale nella testata della patch.

--no-index
	Non effettua l'output dell'indice: righe.

-z 	Invia all'output standard le modifiche effettuate
	relative alla patch al posto più in alto o a quella specificata.

-R 	Crea una diff inversa.

-P patch
	Crea una diff per la patch specificata. (Per impostazione predefinita
	la patch al posto più in alto).

--combine patch
	Crea una diff combinata per tutte le patch tra la presente e
	quella specificata con -P. Un nome di patch di \"-\" è equivalente
	a specificare la prima patch applicata.

--snapshot
	Diff contro snapshot (consultare \'quilt snapshot -h').

--diff=utility
	Usa l'utilità specificata per generare la diff. L'utilità
	è invocata con i nomi di file originale e nuovo come argomenti.

--color[=always|auto|never]
	Usa la colorazione della sintassi.

--sort	Ordina i file per nome invece di mantenere l'ordine originale.
 
Aggiorna la patch specificata o, per impostazione predefinita, la patch al posto
più in alto. La documentazione che precede l'attuale patch nel file delle patch
viene conservata.

È possibile aggiornare patch che non occupano il posto più in alto. Se alcune
patch più in alto di quella da aggiornare modificano lo stesso file, per impostazione
predefinita lo script abortisce. È ancora possibile aggiornare le patch  con -f. In
questo caso questo script emetterà un avviso per ciascun file ombreggiato, le
modifiche delle patch più recenti verranno ignorate, e verranno completate nella
patch specificata solo le modifiche nei file non modificati da patch più recenti.

-p n	Crea un patch stile -p n (-p0 o -p1 sono supportati).

-u, -U num, -c, -C num
	Crea una diff unificata (-u, -U) con un numero di righe di contesto pari a num.
	Crea una diff di contesto (-c, -C) con un numero di righe di contesto pari a num.
	Il numero predefinito di righe di contesto è 3.

--no-timestamps
	Non include marche temporali del file nelle intestazioni della patch.
	
--no-index
	Non emette un output dell'Indice: righe.

--diffstat
	Aggiunge una sezione diffstat all'intestazione della patch o sostituisce la
	sezione diffstat esistente.

-f	Forza l'aggiornamento della patch che non è al posto più in alto.

--backup
	Crea una copia di backup della vecchia versione della patch con nome patch~.

--sort	elenca i file per nome invece di conservare l'ordine originale.

--strip-trailing-whitespace
	Elimina gli spazi in bianco al termine delle righe.
 
Rimuove uno o più file dalla patch al posto più in alto o specificata. Non è possibile 
rimuovere file modificati da patch al di sopra di quella specificata.

-p patch
	Patch dalla quale rimuovere file.
 
Rimuove una o più patch dal gruppo di patch applicate. Senza opzioni, viene 
rimossa la patch al posto più in alto. Quando viene specificato un numero,
rimuove il numero di patch specificato. Quando viene specificato il nome di
patch, rimuove le patch fin quando quella specificata non si trovi al posto più
in alto del gruppo. I nomi di patch possono includere il prefisso patches/ e 
questo significa che può essere usato il completamento del nome di file.

-a	Rimuove tutte le patch applicate.

-f	Forza la rimozione. Lo stato precedente all'applicazione delle patch
	verrà ripristinato dai file di backup.

-R	Verificare sempre se la patch è stata rimossa correttamente; non 
	fare affidamento sui controlli delle marche temporali.

-q	Operazione invisibile all'utente.

-v	Operazione con informazioni dettagliate.
 
Rimuove dal file series la patch specificata o al posto più alto. Se la
patch è applicata, quilt tenterà prima di rimuoverla. (solo la patch
al posto più alto può essere rimossa subito)

-n	Cancella la patch successiva a quella al posto più alto invece che
	quella specificata o quella al posto più alto.
 
Rinomina la prima patch o quella indicata.

-p patch
	Patch da rinominare
 
Acquisisce una schermata dello stato funzionante attuale. Successivamente,
è possibile modificare l'albero nei soliti modi, compreso l'inserimento e 
l'estrazione di patch. È possibile generare una diff che opera sull'albero nel 
momento in cui viene acquisita la schermata con \`quilt diff --snapshot'.

-d	Rimuove solo l'attuale schermata.
 
Aggiorna i metadati in un albero in funzione da una precedente versione di quilt
alla versione corrente. Questo comando è necessario solo se è stato modificato
il formato dei metadati di quilt e l'albero in funzione contiene ancora i metadati
precedenti. In tal caso, è necessario eseguire \`quilt upgrade'.
        quilt --version %s: Situazione confusa.
 Aggiunge il testo alla testata della patch %s\n Applicata la patch %s (forzata; è necessario un refresh)\n Applicazione della patch %s\n Impossibile aggiungere il link simbolico %s\n Impossibile controllare le differenze tra le patch utilizzando -p%s, utilizzare -p0 o -p1\n Non è possibile aggiornare le patch con -p%s, specificare invece -p0 p -p1\n Non è possibile usare --strip-trailing-whitespace su una patch che ha file ombreggiati.\n I comandi sono: Conversione non riuscita\n Conversione meta-dati alla versione %s\n L'indirizzo di consegna  '%s'  non è valido
 Controllo delle differenze non riuscito, uscita\n La directory %s esiste\n Il nome visualizzato  «%s» contiene caratteri non validi
 Il nome visualizzato  «%s» contiene caratteri non stampabili o a 8 bit
 Il nome visualizzato «%s» contiene parentesi spaiate
 Creazione del file di backup di %s non riuscita\n Copia dei file nella cartella temporanea non riuscita\n Creazione della patch %s non riuscita\n Importazione della patch %s non riuscita\n Inserimento della patch %s nel file di serie non riuscito\n Impossibile applicare la patch sui file temporanei\n Rimozione del file %s dalla patch %s non riuscita\n Rimozione della patch %s\n non riuscita Non è riuscita l'operazione di ridenominazione di %s in %s: %s
 File %s aggiunto alla patch %s\n Il file %s è sparito!
 Il file %s esiste\n Il file %s è già nella patch %s\n Il file %s non è stato modificato\n Il file %s non è presente nella patch %s\n Il file %s potrebbe essere danneggiato\n File %s modificato dalla patch %s\n File %s rimosso dalla patch %s\n Il file "%s" è situato sotto "%s"\n Il file series è stato completamente applicato, termina alla patch %s\n Duplicato della patch %s creato come %s\n Duplicazione della patch %s alla patch %s non riuscito\n Importazione della patch %s (salvata come %s)\n Interrotto dall'utente; la patch %s non è stata applicata.\n Patch più recenti modificano i file nella patch %s. Forzare un aggiornamento con -f.\n I file nella patch %s sono modificati da altre patch più recenti\n Non ci sono altre patch\n Nessuna patch rimossa\n Nessuna patch applicata\n La patch %s è vuota\n Ora alla patch %s\n L'opzione «-n» può essere solo usata durante l'importazione di una singola patch\n Le opzioni \ "-c patch", \ "--snapshot", e \ "-z" non possono essere combinate.\n La patch %s sembra essere vuota, rimozione\n La patch %s sembra essere vuota; applicata\n La patch %s non è stata applicata (forzare con -f)\n La patch %s non esiste; applicata patch vuota\n La patch %s non viene rimossa correttamente (aggiornare o forzare con -f)\n La patch %s esiste già: scegliere un altro nome\n La patch %s esiste già, scegliere un altro nome\n La patch %s è già esistente\n La patch %s esiste. Sostituirla con -f.\n La patch %s è già applicata\n La patch %s è applicata\n La patch %s è attualmente applicata\n La patch %s non è applicata\n La patch %s non è nel file series\n La patch %s non è nella serie\n La patch %s è ora applicata\n La patch %s non è stata modificata\n La patch %s richiede prima un refresh.\n Patch %s non applicata prima della patch %s\n La patch %s non è stata trovata nel file series\n Patch %s rinominata in %s\n La patch non è applicata\n Patch %s aggiornata\n Patch %s\n rimossa Rimozione della patch %s\n Rimozione dello spazio in bianco dalla riga %s di %s
 Rimozione dello spazio in bianco dalle righe %s di %s
 Ridenominazione di %s in %s: %s
 Rinomina della patch %s in %s non riuscita\n Testata della patch %s sostituita\n Sostituzione della patch %s con la nuova versione\n Sinossi: %s [-p num] [-n] [patch]
 La sezione %%prep di %s non è riuscita; i risultati potrebbero essere incompleti\n L'opzione -v mostra l'output di rpm\n I meta-dati quilt in %s sono già nel formato %s, nulla da fare\n I metadati di quilt in questo albero sono nella versione %s, ma questa versione di quilt è in grado di gestire solo formati di metadati fino alla versione %s compresa. Togliere tutte le patch utilizzando la versione di quilt usata per immetterle prima di effettuare il downgrade.\n La patch al posto più in alto %s necessita prima di un refresh.\n L'albero in funzione è stato creato da una precedente versione di quilt: eseguire 'quilt upgrade'.\n Uso: %s {-s|-u} section file [< replacement]
 Estrazione archivio %s\n Uso: quilt [--trace[=verbose]] [--quiltrc=XX] comando [-h] ... Uso: quilt add [-p patch] {file} ...\n Uso: quilt annotate {file}\n Uso: quilt applied [patch]\n Uso: quilt delete [patch | -n]\n Uso: quilt diff [-p n] [-u|-U num|-c|-C num] [--combine patch|-z] [-R] [-P patch] [--snapshot] [--diff=utility] [--no-timestamps] [--no-index] [--sort] [--color] [file ...]\n Uso: quilt edit file ...\n Uso: quilt files [-v] [-a] [-l] [--combine patch] [patch]\n Uso: quilt fold [-p strip-level]\n Uso: quilt fork [nuovo_nome]\n Uso: quilt graph [--all] [--reduce] [--lines[=num]] [--edge-labels=file] [patch]\n Uso: quilt grep [-h|opzioni] {pattern}\n Uso: quilt header [-a|-r|-e] [--backup] [--strip-diffstat] [--strip-trailing-whitespace] [patch]\n Uso: quilt import [-f] [-p num] [-n patch] patchfile ...\n Uso: quilt mail {--mbox file|--send} [--from ...] [--to ...] [--cc ...] [--bcc ...] [--subject ...]\n Uso: quilt new {nomepatch}\n Uso: quilt next [patch]\n Uso: quilt patches {file}\n Uso: quilt pop [-afRqv] [num|patch]\n Uso: quilt previous [patch]\n Uso: quilt push [-afqv] [--leave-rejects] [num|patch]\n Uso: quilt refresh [-p n] [-f] [--no-timestamps] [--no-index] [--diffstat] [--sort] [--backup] [--strip-trailing-whitespace] [patch]\n Uso: quilt remove [-p patch] {file} ...\n Uso: quilt rename [-p patch] nuovo_nome\n Uso: quilt series [-v]\n Uso: quilt setup [-d path-prefix] [-v] {file_spec|file_series}\n Uso: quilt snapshot [-d]\n Uso: quilt top\n Uso: quilt unapplied [patch]\n Uso: quilt upgrade\n Attenzione: spazio in bianco al termine della riga %s di %s
 Attenzione: spazio in bianco al termine delle righe %s di %s
 