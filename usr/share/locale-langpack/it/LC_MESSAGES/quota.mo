��    =        S   �      8  \   9     �     �     �     �  	   �     �     �  	   �  -      G   .  
   v     �  4   �  <   �  "   
     -  *   F  %   q     �  %   �  -   �  .     T   4  S   �  $   �     	     	     '	     ;	     Y	  %   y	     �	     �	     �	  2   �	     
     .
     F
     R
  1   f
  %   �
     �
     �
     �
  $        (  2   :     m  $   �     �     �     �     �     �     �     �                 �    F   �  (        ,     B     H  	   O     Y     g  	   j  8   t  a   �            A   9  X   {  5   �  "   
  ?   -  3   m  %   �  ;   �  ;     9   ?  p   y  o   �  "   Z     }     �     �  #   �  -   �  1        9     S     s  9   �  &   �  %   �          "  =   <  A   z     �     �     �  2   
     =  ?   N     �  .   �     �     �     �  %        '     ,     C     Y     `     h     ,   "         3   +       6         (   :       #                                        )      <       9      1   *              
   5                  	       '   &   %      4           =                    -         7                       !   /       2   8   $      ;                 .   0        
-u, --user                    edit user data
-g, --group                   edit group data
 
Can open directory %s: %s
     %8llu    %8llu    %8llu #%-7d %-8.8s %8llu     %d	%llu	%llu
 %s %s (%s):
 As you wish... Canceling check of this file.
 Bad file magic or version (probably not quotafile with bad endianity).
 Block %u:  Block limit reached on Cannot create file for %ss for new format on %s: %s
 Cannot get exact used space... Results might be inaccurate.
 Cannot get name of new quotafile.
 Cannot open file %s: %s
 Cannot open old format file for %ss on %s
 Cannot open old quota file on %s: %s
 Cannot read block %u: %s
 Cannot read header of old quotafile.
 Cannot read information about old quotafile.
 Cannot rename new quotafile %s to name %s: %s
 Cannot turn %s quotas off on %s: %s
Kernel won't know about changes quotacheck did.
 Cannot turn %s quotas on on %s: %s
Kernel won't know about changes quotacheck did.
 Checked %d directories and %d files
 Compiled with:%s
 Corrupted blocks:  Duplicated entries. EXT2_IOC_GETFLAGS failed: %s
 Error checking device name: %s
 Error while syncing quotas on %s: %s
 File limit reached on Found an invalid UUID: %s
 Headers checked.
 Headers of file %s checked. Going to load data...
 In block grace period on In file grace period on Leaving %s
 Not enough memory.
 Not found any corrupted blocks. Congratulations.
 Old file found removed during check!
 Old file not found.
 Over file quota on Quota utilities version %s.
 Renaming new files to proper names.
 Skipping %s [%s]
 Something weird happened while scanning. Error %d
 Substracted %lu bytes.
 Unknown action should be performed.
 blocks cannot open %s: %s
 done
 error (%d) while opening %s
 files fsname mismatch
 grace limit none quota Project-Id-Version: quota
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-16 18:19+0100
PO-Revision-Date: 2014-08-05 10:29+0000
Last-Translator: Gianfranco Frisani <gfrisani@libero.it>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:15+0000
X-Generator: Launchpad (build 18115)
 
-u, --user modifica dati utente
-g, --group modifica dati del gruppo
 
Impossibile aprire la directory %s: %s
     %8llu %8llu %8llu #%-7d %-8.8s %8llu     %d	%llu	%llu
 %s %s (%s):
 Come richiesto ... Controllo di questo file cancellato.
 Versione o numero magico errati (probabilmente non è un file quota con cattivo ordine dei byte)
 Blocco %u:  Limite di blocco raggiunto su Impossibile creare il file per %s per il nuovo formato su %s: %s
 Impossibile ottenere l'esatto spazio usato... I risultati potrebbero essere inaccurati.
 Impossibile ottenere un nome per il nuovo file quota
 Impossibile aprire il file %s: %s
 Impossibile aprire il file con il vecchio formato per %s su %s
 Impossibile aprire il vecchio file quota su %s: %s
 Impossibile leggere il blocco %u: %s
 Impossibile leggere l'intestazione del vecchio file quota.
 Impossibile leggere le informazioni sul vecchio file quota
 Impossibile rinominare il nuovo file quota da %s a %s:%s
 Impossibile rimuovere limiti %s su %s: %s
Il kernel non conosce le modifiche effettuate dal controllo di quota.
 Impossibile attivare limiti %s su %s: %s
Il kernel non conosce le modifiche effettuate dal controllo di quota.
 %d cartelle e %d file controllati
 Compilato con:%s
 Blocchi danneggiati:  Voci duplicate. EXT2_IOC_GETFLAGS non riuscito: %s
 Errore nel verificare il nome del device: %s
 Errore durante sincronizzazione limiti su %s: %s
 Limite del file raggiunto Trovato un UUID non valido: %s
 Intestazioni controllate.
 Intestazioni del file %s controllate. Carico dei dati...
 Nel periodo di tolleranza di blocco su Nel periodo di tolleranza del file su Lasciare %s
 Memoria non sufficiente.
 Nessun blocco danneggiato è stato trovato. Congratulazioni.
 Il vecchio file trovato è stato eliminato durante il controllo!
 Vecchio file non trovato.
 Oltre limite quota su Utilità quota versione %s.
 Ridenominazione dei nuovi file con nomi corretti.
 Salto di %s[%s]
 Qualcosa di strano è successo durante la scansione. Errore %d
 %lu byte sottratti.
 Sta per essere eseguita un'azione sconosciuta
 blocchi Impossibile aprire %s: %s
 Completato
 errore (%d) durante l'apertura di %s
 file discordanza di fsname
 periodo di tolleranza limite nessuno quota 