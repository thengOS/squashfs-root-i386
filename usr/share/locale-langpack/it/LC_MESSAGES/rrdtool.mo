��          �   %   �      P  '  Q  A   y  @   �  D   �  =   A  :     l   �     '     A  ,   [     �  %   �  ,   �  -   �      '  &   H     o     �  t   �  �   $  �   �  �   W	  �   �	  �   �
  0     �  G  9  �  @   -  O   n  H   �  A     A   I  y   �          !  .   ?     n  )   �  .   �  /   �  !     *   8     c     �  o   �  �     �   �  �   T  �   �  �   �  0   !                         
                                                                     	                               		[CDEF:vname=rpn-expression]
		[VDEF:vdefname=rpn-expression]
		[PRINT:vdefname:format]
		[GPRINT:vdefname:format]
		[COMMENT:text]
		[SHIFT:vname:offset]
		[TEXTALIGN:{left|right|justified|center}]
		[TICK:vname#rrggbb[aa][:[fraction][:legend]]]
		[HRULE:value#rrggbb[aa][:legend]]
		[VRULE:value#rrggbb[aa][:legend]]
		[LINE[width]:vname[#rrggbb[aa][:[legend][:STACK]]]]
		[AREA:vname[#rrggbb[aa][:[legend][:STACK]]]]
		[PRINT:vname:CF:format] (deprecated)
		[GPRINT:vname:CF:format] (deprecated)
		[STACK:vname[#rrggbb[aa][:legend]]] (deprecated)
  * cd - changes the current directory

	rrdtool cd new directory
  * ls - lists all *.rrd files in current directory

	rrdtool ls
  * mkdir - creates a new directory

	rrdtool mkdir newdirectoryname
  * pwd - returns the current working directory

	rrdtool pwd
  * quit - closing a session in remote mode

	rrdtool quit
  * resize - alter the length of one of the RRAs in an RRD

	rrdtool resize filename rranum GROW|SHRINK rows
 %s: illegal option -- %c
 %s: invalid option -- %c
 %s: option `%c%s' doesn't allow an argument
 %s: option `%s' is ambiguous
 %s: option `%s' requires an argument
 %s: option `--%s' doesn't allow an argument
 %s: option `-W %s' doesn't allow an argument
 %s: option `-W %s' is ambiguous
 %s: option requires an argument -- %c
 %s: unrecognized option `%c%s'
 %s: unrecognized option `--%s'
 * graph - generate a graph from one or several RRD

	rrdtool graph filename [-s|--start seconds] [-e|--end seconds]
 * graphv - generate a graph from one or several RRD
           with meta data printed before the graph

	rrdtool graphv filename [-s|--start seconds] [-e|--end seconds]
 * restore - restore an RRD file from its XML form

	rrdtool restore [--range-check|-r] [--force-overwrite|-f] filename.xml filename.rrd
 RRDtool %s  Copyright by Tobias Oetiker <tobi@oetiker.ch>
               Compiled %s %s

Usage: rrdtool [options] command command_options
 RRDtool is distributed under the Terms of the GNU General
Public License Version 2. (www.gnu.org/copyleft/gpl.html)

For more information read the RRD manpages
 Valid commands: create, update, updatev, graph, graphv,  dump, restore,
		last, lastupdate, first, info, fetch, tune
		resize, xport, flushcached
 Valid remote commands: quit, ls, cd, mkdir, pwd
 Project-Id-Version: rrdtool
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-04-06 16:14+0000
PO-Revision-Date: 2015-10-26 12:58+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:05+0000
X-Generator: Launchpad (build 18115)
 		[CDEF:vname=espressione-rpn]
		[VDEF:vdefname=espressione-rpn]
		[PRINT:vdefname:formato]
		[GPRINT:vdefname:formato]
		[COMMENT:testo]
		[SHIFT:vname:offset]
		[TEXTALIGN:{left|right|justified|center}]
		[TICK:vname#rrggbb[aa][:[frazione][:legenda]]]
		[HRULE:value#rrggbb[aa][:legenda]]
		[VRULE:value#rrggbb[aa][:legenda]]
		[LINE[width]:vname[#rrggbb[aa][:[legenda][:STACK]]]]
		[AREA:vname[#rrggbb[aa][:[legenda][:STACK]]]]
		[PRINT:vname:CF:formato] (sconsigliato)
		[GPRINT:vname:CF:formato] (sconsigliato)
		[STACK:vname[#rrggbb[aa][:legend]]] (sconsigliato)
  * cd - cambia l'attuale directory

	rrdtool cd nuova_directory
  * ls - elenca tutti i file *.rrd presenti nell'attuale directory

	rrdtool ls
  * mkdir - crea una nuova directory

	rrdtool mkdir nome_nuova_directry
  * pwd - restituisce l'attuale directory di lavoro

	rrdtool pwd
  * quit - chiude una sessione in modalità remota

	rrdtool quit
  * resize - altera la larghezza di uno dei RRA in un RRD

	rrdtool resize nomefile numero-rra GROW|SHRINK numero di file
 %s: opzione illecita -- %c
 %s: opzione non valida -- %c
 %s: l'opzione "%c%s" non ammette un argomento
 %s: l'opzione "%s" è ambigua
 %s: l'opzione "%s" richiede un argomento
 %s: l'opzione "--%s" non ammette un argomento
 %s: l'opzione "-W %s" non ammette un argomento
 %s: l'opzione "-W %s" è ambigua
 %s: l'opzione richiede un argumento -- %c
 %s: opzione "%c%s" sconosciuta
 %s: opzione "--%s" sconosciuta
 * graph - genera un grafico da una o più RRD

	rrdtool graph nomefile [-s|--start secondi] [-e|--end secondi]
 * graphv - genera un grafico da una o più RRD
          con i meta-dati stampati prima del grafico

	rrdtool graphv nomefile [-s|--start secondi] [-e|--end secondi]
 * restore - ripristina un file RRD  dal formato XML dello stesso

	rrdtool restore [--range-check|-r] [--force-overwrite|-f] nome_file.xml nome_file.rrd
 RRDtool %s  Copyright by Tobias Oetiker <tobi@oetiker.ch>
               Compilato %s %s

Uso: rrdtool [opzioni] comando opzioni_comando
 RRDtool è distribuito nei termini della GNU General
Public License Version 2 (www.gnu.org/copyleft/gpl.html).

Per maggiori informazioni leggere la pagina di manuale di RRD.
 Comandi validi: create, update, updatev, graph, graphv,  dump, restore,
		last, lastupdate, first, info, fetch, tune
		resize, xport, flushcached
 Comandi remoti validi: quit, ls, cd, mkdir, pwd
 