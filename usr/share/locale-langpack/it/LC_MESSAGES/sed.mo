��    \      �     �      �  �   �  ,   �  5   	  N   7	  7   �	  \   �	  _   
  `   {
  u   �
  i   R  b   �  V     Y   v  ~   �  �   O  D   �  %   $     J     a  5   {  B   �     �  e     w   w     �          #     ?  $   W     |     �     �     �     �     �     �     �  #   	     -     H     P     Y     l     ~     �     �  H   �     �       !   1     S     h  (   }     �     �  #   �     �       $   5     Z     y  #   �  B   �  2   �     -      A     b     �     �  *   �  *   �          (     8  #   F  #   j  &   �     �     �  ,   �          )  -   >     l     {     �     �     �     �     �     �  �    &  �  -      ?   N  W   �  ?   �  X   &  c     \   �  r   @     �  e   3  Y   �  `   �  �   T  [   �  E   1  +   w     �     �  I   �  a   '      �   g   �   �   	!     �!  +   �!  #   �!  7   �!  *   *"     U"     u"     �"     �"     �"     �"  *   �"     	#  (   #  "   @#     c#     l#  4   u#     �#     �#     �#     �#  P   $     V$     q$  4   �$     �$     �$  E   �$  +   A%      m%  "   �%     �%  *   �%  1   �%  ,   ,&  !   Y&  ,   {&  U   �&  '   �&     &'     A'  '   ^'  '   �'     �'  =   �'  .   �'  $   .(     S(     d(  #   q(  #   �(  )   �(     �(  *   �(  7   &)     ^)     y)  6   �)     �)     �)     �)      *     *     .*     H*  1   b*           C          	   5             !   <      J   +                 R   #   
   3   L   S           E   M       *   B                     F      :   X      2   I                 H   .                 >                  ?       %          $   Q   9               T   4               1       [                -             V       @              G   =       ,   U   \   O   7   N      )   &           P   (   ;   A   Z       W   '   /   0       K   D      8   Y   "   6          
If no -e, --expression, -f, or --file option is given, then the first
non-option argument is taken as the sed script to interpret.  All
remaining arguments are names of input files; if no input files are
specified, then the standard input is read.

       --help     display this help and exit
       --version  output version information and exit
   --follow-symlinks
                 follow symlinks when processing in place
   --posix
                 disable all GNU extensions.
   -R, --regexp-perl
                 use Perl 5's regular expressions syntax in the script.
   -b, --binary
                 open files in binary mode (CR+LFs are not processed specially)
   -e script, --expression=script
                 add the script to the commands to be executed
   -f script-file, --file=script-file
                 add the contents of script-file to the commands to be executed
   -i[SUFFIX], --in-place[=SUFFIX]
                 edit files in place (makes backup if SUFFIX supplied)
   -l N, --line-length=N
                 specify the desired line-wrap length for the `l' command
   -n, --quiet, --silent
                 suppress automatic printing of pattern space
   -r, --regexp-extended
                 use extended regular expressions in the script.
   -s, --separate
                 consider files as separate rather than as a single continuous
                 long stream.
   -u, --unbuffered
                 load minimal amounts of data from the input files and flush
                 the output buffers more often
   -z, --null-data
                 separate lines by NUL characters
 %s: -e expression #%lu, char %lu: %s
 %s: can't read %s: %s
 %s: file %s line %lu: %s
 %s: warning: failed to get security context of %s: %s %s: warning: failed to set default file creation context to %s: %s : doesn't want any addresses E-mail bug reports to: <%s>.
Be sure to include the word ``%s'' somewhere in the ``Subject:'' field.
 GNU sed home page: <http://www.gnu.org/software/sed/>.
General help using GNU software: <http://www.gnu.org/gethelp/>.
 Invalid back reference Invalid character class name Invalid collation character Invalid content of \{\} Invalid preceding regular expression Invalid range end Invalid regular expression Jay Fenlason Ken Pizzini Memory exhausted No match No previous regular expression Paolo Bonzini Premature end of regular expression Regular expression too big Success Tom Lord Trailing backslash Unmatched ( or \( Unmatched ) or \) Unmatched [ or [^ Unmatched \{ Usage: %s [OPTION]... {script-only-if-no-other-script} [input-file]...

 `e' command not supported `}' doesn't want any addresses can't find label for jump to `%s' cannot remove %s: %s cannot rename %s: %s cannot specify modifiers on empty regexp cannot stat %s: %s command only uses one address comments don't accept any addresses couldn't attach to %s: %s couldn't edit %s: is a terminal couldn't edit %s: not a regular file couldn't follow symlink %s: %s couldn't open file %s: %s couldn't open temporary file %s: %s couldn't write %d item to %s: %s couldn't write %d items to %s: %s delimiter character is not a single-byte character error in subprocess expected \ after `a', `c' or `i' expected newer version of sed extra characters after command incomplete command invalid reference \%d on `s' command's RHS invalid usage of +N or ~N as first address invalid usage of line address 0 missing command multiple `!'s multiple `g' options to `s' command multiple `p' options to `s' command multiple number options to `s' command no input files no previous regular expression number option to `s' command may not be zero option `e' not supported read error on %s: %s strings for `y' command are different lengths unexpected `,' unexpected `}' unknown command: `%c' unknown option to `s' unmatched `{' unterminated `s' command unterminated `y' command unterminated address regex Project-Id-Version: sed 4.2.0
Report-Msgid-Bugs-To: bug-gnu-utils@gnu.org
POT-Creation-Date: 2012-12-22 14:36+0100
PO-Revision-Date: 2016-03-15 08:41+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:16+0000
X-Generator: Launchpad (build 18115)
Language: it
 
Se non è usata nessuna delle opzioni -e, --expression, -f o --file allora il
primo argomento che non è una opzione sarà usato come lo script sed da
interpretare. Tutti gli argomenti rimanenti sono nomi di file di input; se non
sono specificati file di input sarà letto lo standard input.

       --help     mostra questo aiuto ed esce
       --version  stampa le informazioni sulla versione ed esce
   --follow-symlinks
                 segue i link simbolici quando viene utilizzato -i
   --posix
                 disabilita tutte le estensioni GNU.
   -R, --regexp-perl
                 usa la sintassi Perl 5 per le espressioni regolari
   -b, --binary
                 apre i file in modo binario (lasciando le sequenze CR+LF immutate)
   -e script, --expression=script
                 aggiunge lo script ai comandi da eseguire
   -f script-file, --file=file-script
                 aggiunge il contenuto di file-script ai comandi da eseguire
   -i[SUFFISSO], --in-place[=SUFFISSO]
                 Modifica i file direttamente (crea backup se viene fornito il SUFFISSO)
   -l N, --line-length=N
                 specifica la lunghezza delle linee generate dal comando `l'
   -n, --quiet, --silent
                 sopprime la stampa automatica del pattern space
   -r, --regexp-extended
                 usa la sintassi di `egrep' per le espressioni regolari
   -s, --separate
                 considera i file di input come separati invece che come un
                 unico file lungo.
   -u, --unbuffered
                 carica e visualizza i dati una a pezzetti piu' piccoli
   -z, --null-data
                 Separa le righe con caratteri NUL
 %s: espressione -e #%lu, carattere %lu: %s
 %s: impossibile leggere %s: %s
 %s: file %s riga %lu: %s
 %s: attenzione: recupero del contesto di sicurezza di %s non riuscito: %s %s: attenzione: impostazione del contesto predefinito di creazione del file a %s non riuscita: %s : non accetta indirizzi Segnalare eventuali bug a: <%s>.
Assicurarsi di includere la parola ``%s'' nell'oggetto del messaggio.
 Sito web di GNU sed: <http://www.gnu.org/software/sed/>.
Aiuto generico sull'uso di software GNU: <http://www.gnu.org/gethelp/>.
 Riferimento non valido Nome non valido per una classe di caratteri Carattere di ordinamento non valido numero di ripetizioni specificato tra graffe non valido Espressione regolare precedente non valida Fine dell'intervallo non valida Espressione regolare non valida Jay Fenlason Ken Pizzini Memoria esaurita Nessuna corrispondenza trovata Occorre un'espressione regolare precedente Paolo Bonzini Fine prematura dell'espressione regolare Espressione regolare troppo grande Successo Tom Lord Barra rovesciata alla fine dell'espressione regolare `(' o `\(' non bilanciata `)' o `\)' non bilanciata `[' non bilanciata `\{' non bilanciata Utilizzo: %s [OPZIONE]... {script-se-nessun-altro-specificato} [input-file]...

 comando `e' non supportato `}' non accetta indirizzi impossibile trovare un'etichetta per il salto a `%s' impossibile rimuovere %s: %s impossibile rinominare %s: %s non è possibile specificare dei modificatori per l'espressione vuota impossibile ottenere informazioni su %s: %s il comando usa solo un indirizzo i commenti non accettano indirizzi impossibile accedere a %s: %s impossibile modificare %s: è un terminale impossibile modificare %s: non è un file normale impossibile seguire il link simbolico %s: %s impossibile aprire il file %s: %s impossibile aprire il file temporaneo %s: %s impossibile scrivere %d elemento su %s: %s impossibile scrivere %d elementi su %s: %s il carattere delimitatore è multi-byte errore in un sottoprocesso atteso \ dopo `a', `c' o `i' attesa una versione piu' recente di sed ci sono altri caratteri dopo il comando comando incompleto riferimento non valido \%d nel secondo membro del comando `s' impossibile usare +N o ~N come primo indirizzo utilizzo non valido dell'indirizzo 0 manca il comando `!' multipli opzioni `g' multiple al comando `s' opzioni `p' multiple al comando `s' opzioni numeriche multiple al comando `s' nessun file in ingresso occorre un'espressione regolare precedente l'opzione numerica del comando `s' non può essere zero opzione `e' non supportata errore di lettura su %s: %s le stringhe per il comandi `y' hanno lunghezze diverse `,' inattesa `}' inattesa comando sconosciuto: `%c' opzione di `s' sconosciuta `{' non bilanciata comando `s' non terminato comando `y' non terminato espressione regolare non terminata nell'indirizzo 