��    !      $  /   ,      �  E   �  Q   /     �     �  
   �     �     �     �     �     �  
   �  "        %     >     V     j     �     �     �     �     �     �     �  �        �     �          )     B     S     j     {  �  �  E   P  Q   �     �     �     �     	  !   	     <	     E	     b	     k	  !   w	  &   �	     �	     �	     �	     
     
     0
     F
     V
     g
     {
  �   �
     �     �     �     �     �     �          2                                     !                           
   	                                                                                            <a href="https://twitter.com/ubuntu_mate" class="caption">Twitter</a> <a href="https://www.facebook.com/UbuntuMATEedition" class="caption">Facebook</a> About Announcements Appearance Assistive technologies Banshee Media Player Chromium Customization options Development Discussion Everything you need for the office Eye of MATE Image Viewer Find even more software Firefox Web Browser GDebi Package Installer GIMP Image Editor Included software Join the community Language support LibreOffice Calc LibreOffice Impress LibreOffice Writer LibreOffice is a free office suite packed with everything you need to create documents, spreadsheets and presentations. Compatible with Microsoft Office file formats, it gives you all the features you need, without the price tag. Rhythmbox Music Player Shotwell Photo Manager Supported software Synaptic Package Manager Thunderbird Mail Ubuntu Software Center VLC Media Player Welcome to Ubuntu MATE Project-Id-Version: ubiquity-slideshow-ubuntu
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-03-24 23:23+0200
PO-Revision-Date: 2016-04-06 08:28+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:35+0000
X-Generator: Launchpad (build 18115)
 <a href="https://twitter.com/ubuntu_mate" class="caption">Twitter</a> <a href="https://www.facebook.com/UbuntuMATEedition" class="caption">Facebook</a> Informazioni Annunci Aspetto Tecnologie assistive Riproduttore multimediale Banshee Chromium Opzioni di personalizzazione Sviluppo Discussione Tutto il necessario per l'ufficio Visualizzatore di immagini Eye of MATE Ancora più software Browser web Firefox Installatore pacchetti GDebi Editor di immagini GIMP Programmi inclusi Entra nella comunità Supporto lingue LibreOffice Calc LibreOffice Impress LibreOffice Writer LibreOffice è una suite da ufficio libera e gratuita comprensiva di tutto ciò che ti può servire per creare documenti, fogli di calcolo e presentazioni. Compatibile con i formati di Microsoft Office, ha tutto le funzionalità di cui hai bisogno. Lettore musicale Rhythmbox Gestore di foto Shotwell Programmi supportati Gestore pacchetti Synaptic Email Thunderbird Ubuntu Software Center Riproduttore multimediale VLC Benvenuti in Ubuntu MATE 