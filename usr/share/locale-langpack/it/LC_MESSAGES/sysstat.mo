��    *      l  ;   �      �     �  %   �  9   �        &   7     ^  %   w    �  -   �  )   �  2   �     +  )   F     p  %   y     �  $   �  "   �     �  .     -   A  "   o  '   �  -   �     �     �  !        *     ?  8   Z     �  .   �     �  #   �  .   	     N	  $   f	  1   �	  ?   �	  >   �	     <
  �  P
  "     2   *  ?   ]     �  .   �     �  <     %  D  D   j  -   �  K   �  #   )  *   M     x  -     0   �  0   �  ,        <  E   W  O   �  &   �  D     E   Y     �     �  2   �     �       U   4     �  2   �     �  .   �  :   '  "   b  -   �  6   �  I   �  O   4     �                	                                                         '              #   $   !   
         )                               *           (       %      &                                  "    	-B	Paging statistics
 	-H	Hugepages utilization statistics
 	-I { <int> | SUM | ALL | XALL }
		Interrupts statistics
 	-R	Memory statistics
 	-S	Swap space utilization statistics
 	-W	Swapping statistics
 	-b	I/O and transfer rate statistics
 	-m { <keyword> [,...] | ALL }
		Power management statistics
		Keywords are:
		CPU	CPU instantaneous clock frequency
		FAN	Fans speed
		FREQ	CPU average clock frequency
		IN	Voltage inputs
		TEMP	Devices temperature
		USB	USB devices plugged into the system
 	-q	Queue length and load average statistics
 	-u [ ALL ]
		CPU utilization statistics
 	-w	Task creation and system switching statistics
 	[Unknown activity format] -f and -o options are mutually exclusive
 Average: Cannot append data to that file (%s)
 Cannot find disk data
 Cannot find the data collector (%s)
 Cannot handle so many processors!
 Cannot open %s: %s
 Cannot write data to system activity file: %s
 Cannot write system activity file header: %s
 End of data collecting unexpected
 End of system activity file unexpected
 Error while reading system activity file: %s
 Host:  Inconsistent input data
 Invalid system activity file: %s
 List of activities:
 Main options and reports:
 Not reading from a system activity file (use -f option)
 Not that many processors!
 Options are:
[ -h ] [ -k | -m ] [ -t ] [ -V ]
 Other devices not listed here Requested activities not available
 Requested activities not available in file %s
 Size of a long int: %d
 System activity data file: %s (%#x)
 Usage: %s [ options ] [ <interval> [ <count> ] ]
 Usage: %s [ options ] [ <interval> [ <count> ] ] [ <outfile> ]
 Using a wrong data collector from a different sysstat version
 sysstat version %s
 Project-Id-Version: sysstat-9.1.6
Report-Msgid-Bugs-To: sysstat <at> orange.fr
POT-Creation-Date: 2015-12-21 08:54+0100
PO-Revision-Date: 2012-04-14 14:34+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-10-09 11:08+0000
X-Generator: Launchpad (build 18227)
 	-B	Statistiche sulla paginazione
 	-H	Statistiche sull'utilizzo delle pagine grandi
 	-I { <int> | SUM | ALL | XALL }
		Statistiche sugli interrupt
 	-R	Statistiche sulla memoria
 	-S	Statistiche sull'uso dello spazio di swap
 	-W	Statistiche sullo swap
 	-b	Statistiche sull'I/O e sulla velocità di trasferimento
 	-m { <parolachiave> [,...] | ALL }
		Statistiche consumo energetico
		Parole chiavi sono:
		CPU	Frequenza di clock istantanea della CPU
		FAN	Velocità ventola
		FREQ	Frequenza di clock media della CPU
		IN	Tensione in ingresso
		TEMP	Temperatura device
		USB	Device USB collegati al sistema
 	-q	Statistiche sulla media della lunghezza della coda e del carico
 	-u [ ALL ]
		Statistiche sull'uso della CPU
 	-w	Statistiche sulla creazione di attività e sui cambiamenti del sistema
 	[Formato di attività sconosciuto] Le opzioni -f e -o si escludono a vicenda
 Media: Impossibile aggiungere dati a quel file (%s)
 Impossibile trovare i dati concernenti il disco
 Impossibile trovare il collettore dei dati (%s)
 Impossibile gestire così tanti processori.
 Impossibile aprire %s: %s
 Impossibile scrivere dati nel file delle statistiche del sistema: %s
 Impossibile scrivere l'intestazione del file delle statistiche del sistema: %s
 Fine inattesa della raccolta dei dati
 Il file delle statistiche del sistema è terminato in modo inatteso
 Errore durante la lettura del file delle statistiche del sistema: %s
 Host:  Dati in ingresso inconsistenti
 File delle statistiche del sistema non valido: %s
 Elenco delle statistiche:
 Opzioni e rapporti principali:
 La lettura non avviene da un file delle statistiche del sistema (usare l'opzione -f)
 Non così tanti processori.
 Le opzioni sono:
[ -h ] [ -k | -m ] [ -t ] [ -V ]
 Altri device non indicati qui Le statistiche richieste non sono disponibili
 Le statistiche richieste non sono disponibili nel file %s
 Dimensione di un «long int»: %d
 File delle statistiche del sistema: %s (%#x)
 Uso: %s [ opzioni ] [ <intervallo> [ <iterazioni> ] ]
 Uso: %s [ opzioni ] [ <intervallo> [ <iterazioni> ] ] [ <filedioutput> ]
 Utilizzo di un collettore di dati errato da una versione differente di sysstat
 sysstat versione %s
 