��          �      �       0  %   1     W     s     �     �  "   �  1   �  3     .   J  7   y  0   �  -   �  �    0   �  "   �       "   >     a  )   �  N   �  R   �  I   M  T   �  F   �  G   3                                         	             
    Check if the package system is locked Get current global keyboard Get current global proxy Set current global keyboard Set current global proxy Set current global proxy exception System policy prevents querying keyboard settings System policy prevents querying package system lock System policy prevents querying proxy settings System policy prevents setting global keyboard settings System policy prevents setting no_proxy settings System policy prevents setting proxy settings Project-Id-Version: ubuntu-system-service
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-11-04 00:35+0000
PO-Revision-Date: 2012-09-17 17:12+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:35+0000
X-Generator: Launchpad (build 18115)
 Controlla se il sistema di pacchetti è bloccato Ottiene l'attuale tastiera globale Ottiene l'attuale proxy globale Imposta l'attuale tastiera globale Imposta l'attuale proxy globale Imposta l'attuale eccezione proxy globale La politica di sistema impedisce di interrogare le impostazioni della tastiera La politica di sistema impedisce di interrogare il blocco del sistema di pacchetti La politica di sistema impedisce di interrogare le impostazioni del proxy La politica di sistema impedisce di assegnare le impostazioni globali della tastiera La politica di sistema impedisce di assegnare le impostazioni no_proxy La politica di sistema impedisce di assegnare le impostazioni del proxy 