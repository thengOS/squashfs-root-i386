��          4      L       `   j  a      �  �  �  g  �     �                    Usage:
tasksel install <task>...
tasksel remove <task>...
tasksel [options]
	-t, --test          test mode; don't really do anything
	    --new-install   automatically install some tasks
	    --list-tasks    list tasks that would be displayed and exit
	    --task-packages list available packages in a task
	    --task-desc     returns the description of a task
 apt-get failed Project-Id-Version: newtasksel 2.01
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-01-02 02:46+0000
PO-Revision-Date: 2015-12-22 01:20+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:37+0000
X-Generator: Launchpad (build 18115)
Language: it
 Usage:
tasksel install <task>...
tasksel remove <task>...
tasksel [OPZIONI]
	-t, --test          Modalità di prova, non installa nulla
	    --new-install   Installa automaticamente alcuni task
	    --list-tasks    Elenca i task ed esce
	    --task-packages Elenca i pacchetti disponibili in un task
	    --task-desc     Restituisce la descrizione di un task
 apt-get ha dato errore 