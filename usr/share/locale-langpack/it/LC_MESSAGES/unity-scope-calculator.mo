��          \      �       �   	   �   
   �      �      �   3   �   �   ,  !   �  �       �     �     �  	     @     �   O  @   .                                       Calculate Calculator Open in calculator Result Sorry, there are no results that match your search. This is an Ubuntu search plugin that enables Calculator results to be displayed in the Dash underneath the Info header. If you do not wish to search this content source, you can disable this search plugin. calculator;result;calculate;calc; Project-Id-Version: unity-scope-calculator
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-28 09:22+0000
PO-Revision-Date: 2014-04-10 07:11+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:07+0000
X-Generator: Launchpad (build 18115)
 Calcola Calcolatrice Apri nella calcolatrice Risultato Non ci sono risultati che corrispondono alla ricerca effettuata. Questo è un plugin di ricerca di Ubuntu che permette di visualizzare nella Dash i risultati della Calcolatrice , sotto l'intestazione «Info». Per non effettuare la ricerca su questi contenuti, disattivare questo plugin. calculator;result;calculate;calc;calcola;calcolatrice;risultato; 