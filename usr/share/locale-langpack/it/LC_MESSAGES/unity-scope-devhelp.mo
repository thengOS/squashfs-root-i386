��          \      �       �      �      �      �   ;   �      !  �   5       �  ,     �     �     �  K        M  �   _  *   Y                                       Devhelp Search Devhelp Show Sorry, there are no Devhelp results that match your search. Technical Documents This is an Ubuntu search plugin that enables information from Devhelp to be searched and displayed in the Dash underneath the Code header. If you do not wish to search this content source, you can disable this search plugin. devhelp;dev;help;doc; Project-Id-Version: unity-scope-devhelp
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-28 09:22+0000
PO-Revision-Date: 2014-04-10 07:12+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:23+0000
X-Generator: Launchpad (build 18115)
 Devhelp Cerca Devhelp Mostra Non ci sono risultati da Devhelp che corrispondono alla ricerca effettuata. Documenti tecnici Questo è un plugin di ricerca di Ubuntu che permette a informazioni provenienti da Devhelp di essere ricercate e visualizzate nella Dash, sotto l'intestazione «Codice». Per non effettuare la ricerca su questi contenuti, disattivare questo plugin. devhelp;dev;help;doc;aiuto;documentazione; 