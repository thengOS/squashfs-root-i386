��    	      d      �       �      �      �      �      �   4        H  �   \     D  �  R  
                  /  C   @     �    �      �        	                                      Manpages Open Search Manpages Search for Manpages Sorry, there are no Manpages that match your search. Technical Documents This is an Ubuntu search plugin that enables information from local manpages to be searched and displayed in the Dash underneath the Code header. If you do not wish to search this content source, you can disable this search plugin. manpages;man; Project-Id-Version: unity-scope-manpages
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-24 16:55+0000
PO-Revision-Date: 2014-04-10 07:13+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:23+0000
X-Generator: Launchpad (build 18115)
 Pagine man Apri Cerca le pagine man Cerca pagine man Non c'è alcuna pagina man che corrisponde alla ricerca effettuata. Documenti tecnici Questo è un plugin di ricerca di Ubuntu che permette a informazioni provenienti dalle pagine man locali di essere ricercate e visualizzate nella Dash, sotto l'intestazione «Codice». Per non effettuare la ricerca su questi contenuti, disattivare questo plugin. manpages;man;pagine man;manuale; 