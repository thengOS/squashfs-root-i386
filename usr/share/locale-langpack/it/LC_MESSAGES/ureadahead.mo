��          �   %   �      P     Q     k     �     �      �     �     �          $     ?     Z     y     �  1  �     �     �     �  "     4   :     o      v     �  6   �  (   �  1     �  D     �       8   +  $   d  8   �     �     �     �  3   	  0   K	  "   |	     �	     �	  U  �	     /     N  "   i  /   �  F   �  
          &   +  D   R  4   �  P   �                                             	                       
                                                         %s: illegal argument: %s
 Error mapping into memory Error retrieving chunk extents Error retrieving file stat Error retrieving page cache info Error unmapping from memory Error while reading Error while tracing Failed to set CPU priority Failed to set I/O priority File vanished or error reading Ignored far too long path Ignored relative path PATH should be the location of a mounted filesystem for which files should be read.  If not given, the root filesystem is assumed.

If PATH is not given, and no readahead information exists for the root filesystem (or it is old), tracing is performed instead to generate the information for the next boot. Pack data error Pack too old Read required files in advance Unable to determine pack file name Unable to obtain rotationalness for device %u:%u: %s [PATH] detach and run in the background dump the current pack file how to sort the pack file when dumping [default: path] ignore existing pack and force retracing maximum time to trace [default: until terminated] Project-Id-Version: ureadahead
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-11-29 15:05+0000
PO-Revision-Date: 2010-03-27 17:29+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:47+0000
X-Generator: Launchpad (build 18115)
 %s: argomento non valido: %s
 Errore nell'eseguire mmap Errore nel recuperare gli extent per la porzione di file Erorre nel ricevere lo stat del file Errore nel recuperare informazioni sulla chace di pagina Errore nell'eseguire munmap Errore durante la lettura Errore durante il tracciamento Impostazione della priorità della CPU non riuscita Impostazione della priorità di I/O non riuscita File mancante o errore nel leggere Percorso troppo lungo ignorato Percorso relativo ignorato PERCORSO è la posizione dove è montato il file system di cui leggere i file. Se non viene fornito, viene usato il file system root.

Se PERCORSO non viene fornito e non esistono informazioni di ureadahead per il file system root (o se è datato), viene eseguito un tracciamento dei file per generare informazioni utili al successivo avvio. Errore di dati nel file "pack" File "pack" troppo vecchio Legge i file richiesti in anticipo Impossibile determinare il nome del file "pack" Impossibile ottenere informazioni sulla rotazione del device %u:%u: %s [PERCORSO] Stacca e avvia in background Esegue il dump del file "pack" attuale Come ordinare il file "pack" durante il dump [predefinito: percorso] Ignora i pacchetti esistenti e forza il tracciamento Tempo massimo per eseguire il tracciamento [predefinito: fino alla terminazione] 