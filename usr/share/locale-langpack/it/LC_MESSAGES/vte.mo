��          �      \      �  $   �  )   �        6   9     p  /   �  (   �     �  +   �     $     B  $   ^  -   �  +   �  7   �       &   -  /   T     �  �  �  5   v  ;   �     �  E        N  O   a  1   �  &   �  Q   
  -   \  2   �  -   �  :   �  .   &	  L   U	     �	  6   �	  B   �	     8
                                  	                      
                                           Attempt to set invalid NRC map '%c'. Attempt to set invalid wide NRC map '%c'. Could not open console.
 Could not parse the geometry spec passed to --geometry Duplicate (%s/%s)! Error (%s) converting data for child, dropping. Error compiling regular expression "%s". Error creating signal pipe. Error reading PTY size, using defaults: %s. Error reading from child: %s. Error setting PTY size: %s. Got unexpected (key?) sequence `%s'. No handler for control sequence `%s' defined. Unable to convert characters from %s to %s. Unable to send data to child, invalid charset convertor Unknown pixel mode %d.
 Unrecognized identified coding system. _vte_conv_open() failed setting word characters can not run %s Project-Id-Version: vte
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=vte&component=general
POT-Creation-Date: 2007-10-06 10:12+0000
PO-Revision-Date: 2016-02-08 00:36+0000
Last-Translator: Francesco Marletta <francesco.marletta@tiscali.it>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:07+0000
X-Generator: Launchpad (build 18115)
 Tentativo di impostare una mappa "%c" NRC non valida. Tentativo di impostare una mappa NRC ampia non valida "%c". Impossibile aprire la console.
 Impossibile analizzare la specifica di geometria passata a --geometry Duplicato (%s/%s). Errore (%s) nel convertire i dati per il processo figlio, rimarranno invariati. Errore nel compilare l'espressione regolare "%s". Errore nel creare la pipe dei segnali. Errore nella lettura della dimensione PTY, sarà usato il valore predefinito: %s. Errore nella lettura dal processo figlio: %s. Errore nell'impostazione della dimensione PTY: %s. Ricevuta una sequenza (tasto?) inattesa "%s". Nessun gestore definito per la sequenza di controllo "%s". Impossibile convertire i caratteri da %s a %s. Impossibile inviare dati al figlio, convertitore di set caratteri non valido Modo pixel %d sconosciuto.
 «Sistema di codifica identificato» non riconosciuto. _vte_con_open() ha fallito nell'impostare i caratteri della parola impossibile eseguire %s 