��    x      �  �   �      (
  ]   )
  K   �
  9   �
  ?        M     Z     o     �  8   �  1   �  5     ;   8  '   t  %   �  '   �  )   �  ?     =   T  >   �  <   �  C     9   R  9   �  @   �  $     ;   ,  ?   h  +   �  G   �  <     9   Y  7   �  >   �  9   
  #   D  9   h  A   �  B   �  B   '  7   j  C   �  C   �  @   *  8   k  /   �  8   �  >     G   L  -   �  >   �  B     2   D  ;   w  ?   �  G   �  A   ;  G   }  A   �  B        J     e     z     �     �     �     �     �  &   
  '   1     Y     \     s     �  /   �     �  I   �       
     %   )     O     W  �   g  E   �  d  E  &   �  "   �  B   �     7      U  6   v     �  $   �  $   �  -     -   3  &   a  .   �     �     �     �  $   �  '   $  "   L  "   o  �   �         &   7   #   ^      �   0   �   7   �   !   !     '!  %   ?!     e!  ?   y!  <   �!  �   �!  (   �"  �  �"  ]   �$  K   �$  9   *%  ?   d%     �%     �%     �%  $   �%  8   	&  1   B&  5   t&  B   �&  )   �&  %   '  '   ='  )   e'  E   �'  D   �'  E   (  F   `(  H   �(  D   �(  9   5)  H   o)  %   �)  G   �)  :   &*  *   a*  G   �*  E   �*  D   +  /   _+  ?   �+  ?   �+  $   ,  9   4,  I   n,  C   �,  C   �,  5   @-  A   v-  A   �-  A   �-  ?   <.  .   |.  ?   �.  K   �.  Q   7/  4   �/  F   �/  8   0  -   >0  >   l0  F   �0  F   �0  E   91  G   1  F   �1  8   2     G2     _2     u2     �2     �2  )   �2  $   �2     3  0   ,3  1   ]3     �3     �3     �3     �3  5   �3      4  L   4     ]4     n4  (   �4     �4     �4  C  �4  [   6  �  q6  .    9  !   /9  @   Q9     �9     �9  B   �9     :  -   ':  -   U:  A   �:  @   �:  +   ;  5   2;     h;  %   ~;     �;  &   �;  7   �;  )   !<  '   K<  �   s<     �<  2   =  *   G=     r=  =   �=  7   �=  )   >     0>  -   I>     w>  >   �>  A   �>  �   ?  ,   �?     _             (      !   :   Q           k   n   l                  E   -   $   C   q              T      <             K   =       p   w   5      J   U   c      `   u          0                 6   h       x   @          g   t   v           M          H          #       b              +      )      [      e       O   .   /   R       ]   1   9   8   m   X   B   3      Z   S   D   "   d      P   L   j   ^       N         f   s   I   F                   '                          \       
          o   G   i       W      4           Y   	       ,   r   V   *   >   ?   ;           &           a   2   %   A   7       
Copyright (C) 1992, 1997, 1998, 1999, 2009, 2010, 2011, 2012 Free Software
Foundation, Inc.
 
Copyright (C) 1992, 1997, 1998, 1999, 2010 Free Software Foundation, Inc.
 
Copyright (C) 1994, 1997 Free Software Foundation, Inc.
 
Copyright (C) 1997, 1998, 1999 Free Software Foundation, Inc.
 
Debugging:
 
Formatting output:
 
Operation modes:
 
Word mode options:
 
Written by Franc,ois Pinard <pinard@iro.umontreal.ca>.
 
Written by Wayne Davison <davison@borland.com>.
       --help             display this help then exit
       --version          display program version then exit
   %d %.0f%% changed   %d %.0f%% changed   %d %.0f%% common   %d %.0f%% common   %d %.0f%% deleted   %d %.0f%% deleted   %d %.0f%% inserted   %d %.0f%% inserted   -0, --debugging   output many details about what is going on
   -1, --no-deleted           inhibit output of deleted words
   -2, --no-inserted          inhibit output of inserted words
   -3, --no-common            inhibit output of common words
   -=, --use-equals       replace spaces by equal signs in unidiffs
   -A, --auto-pager           automatically calls a pager
   -C, --copyright            display copyright then exit
   -O, --item-regexp=REGEXP   compare items as defined by REGEXP
   -P                     same as -p
   -S, --string[=STRING]   take note of another user STRING
   -T, --initial-tab       produce TAB instead of initial space
   -U                     same as -p and -u
   -V, --show-links        give file and line references in annotations
   -W, --word-mode            compare words instead of lines
   -a, --auto-pager           automatically calls a pager
   -c, --context-diffs    force output to context diffs
   -d, --diff-input           use single unified diff as input
   -e, --echo-comments    echo comments to standard error
   -h                     (ignored)
   -h, --help                 display this help then exit
   -i, --ignore-case          fold character case while comparing
   -k, --less-mode            variation of printer mode for "less"
   -l, --less-mode            variation of printer mode for "less"
   -l, --paginate          paginate output through `pr'
   -m, --avoid-wraps          do not extend fields through newlines
   -n, --avoid-wraps          do not extend fields through newlines
   -o, --old-diffs        output old-style diffs, no matter what
   -o, --printer              overstrike as for printers
   -p, --patch-format     generate patch format
   -p, --printer              overstrike as for printers
   -q, --quiet                inhibit the `mdiff' call message
   -s, --statistics           say how many words deleted, inserted etc.
   -s, --strip-comments   strip comment lines
   -t, --expand-tabs       expand tabs to spaces in the output
   -t, --terminal             use termcap as for terminal displays
   -u, --unidiffs         force output to unidiffs
   -v, --verbose          report a few statistics on stderr
   -v, --version              display program version then exit
   -w, --start-delete=STRING  string to mark beginning of delete region
   -x, --end-delete=STRING    string to mark end of delete region
   -y, --start-insert=STRING  string to mark beginning of insert region
   -z, --end-insert=STRING    string to mark end of insert region
   -z, --terminal             use termcap as for terminal displays
  %d cluster,  %d clusters,  %d file,  %d files,  %d item
  %d items
  %d member
  %d members
  %d member,  %d members,  %d overlap
  %d overlaps
 %s (for regexp `%s') %s: %d word %s: %d words %s: input program killed by signal %d
 %s: output program killed by signal %d
 '
 , %d item
 , %d items
 , clustering , done
 If FILE is not specified, read standard input.
 Launching `%s Mandatory arguments to long options are mandatory for short options too.
 Read summary: Reading %s Report bugs to <wdiff-bugs@gnu.org>.
 Sorting Sorting members This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 This program also tells how `mdiff' could have been called directly.
 This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
 Try `%s --help' for more information.
 Usage: %s [OPTION]... FILE1 FILE2
 Usage: %s [OPTION]... FILE1 FILE2
   or: %s -d [OPTION]... [FILE]
 Usage: %s [OPTION]... [FILE]
 Usage: %s [OPTION]... [FILE]...
 With no FILE, or when FILE is -, read standard input.
 Work summary: cannot use -t, termcap not available cannot use -z, termcap not available context diff missing `new' header at line %ld context diff missing `old' header at line %ld could not access the termcap data base could not find a name for the diff at line %ld directories not supported error redirecting stream failed to execute %s ignoring option %s (not implemented) invalid unified diff header at line %ld malformed context diff at line %ld malformed unified diff at line %ld mdiff - Studies multiple files and searches for similar sequences, it then
produces possibly detailed lists of differences and similarities.
 missing file arguments no suitable temporary directory exists only one file may be standard input only one filename allowed options -123RSYZ meaningful only when two inputs select a terminal through the TERM environment variable terminal type `%s' is not defined too many file arguments try `%s --help' for more information
 unable to open `%s' unify - Transforms context diffs into unidiffs, or vice-versa.
 wdiff - Compares words in two files and report differences.
 wdiff - Compute word differences by internally launching `mdiff -W'.
This program exists mainly to support the now oldish `wdiff' syntax.
 word merging for two files only (so far) Project-Id-Version: wdiff 1.2.0-b1
Report-Msgid-Bugs-To: wdiff-bugs@gnu.org
POT-Creation-Date: 2014-04-14 22:15+0200
PO-Revision-Date: 2013-12-19 01:21+0000
Last-Translator: Marco Colombo <Unknown>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:40+0000
X-Generator: Launchpad (build 18115)
Language: it
 
Copyright (C) 1992, 1997, 1998, 1999, 2009, 2010, 2011, 2012 Free Software
Foundation, Inc.
 
Copyright (C) 1992, 1997, 1998, 1999, 2010 Free Software Foundation, Inc.
 
Copyright (C) 1994, 1997 Free Software Foundation, Inc.
 
Copyright (C) 1997, 1998, 1999 Free Software Foundation, Inc.
 
Debugging:
 
Formattazione dell'output:
 
Modi di operazione:
 
Opzioni di controllo delle parole:
 
Scritto da François Pinard <pinard@iro.umontreal.ca>.
 
Scritto da Wayne Davison <davison@borland.com>.
       --help             stampa questo aiuto ed esce
       --version          stampa la versione del programma ed esce
   %d %.0f%% cambiata   %d %.0f%% cambiate   %d %.0f%% comune   %d %.0f%% comuni   %d %.0f%% rimossa   %d %.0f%% rimosse   %d %.0f%% inserita   %d %.0f%% inserite   -0, --debugging   stampa diversi dettagli su quanto sta succedendo
   -1, --no-deleted           inibisce l'output delle parole rimosse
   -2, --no-inserted          inibisce l'output delle parole inserite
   -3, --no-common            inibisce l'output delle parole in comune
   -=, --use-equals       usa "=" al posto di spazi in formato unificato
   -A, --auto-pager           chiama automaticamente un impaginatore
   -C, --copyright            mostra il copyright ed esce
   -O, --item-regexp=REGEXP   confronta gli elementi in base alla REGEXP
   -P                     uguale a -p
   -S, --string[=STRINGA]  prendi nota della STRINGA scelta dall'utente
   -T, --initial-tab       usa tabulazioni invece di spazi
   -U                     uguale a -p e -u
   -V, --show-links        fornisci il nome del file e i numeri di riga
   -W, --word-mode            confronta le parole invece che le righe
   -a, --auto-pager           chiama automaticamente un impaginatore
   -c, --context-diffs    crea diff contestuali
   -d, --diff-input           usa una diff unificata come input
   -e, --echo-comments    riporta commenti sullo standard error
   -h                     (ignorato)
   -h, --help                 stampa questo aiuto ed esce
   -i, --ignore-case          ignora differenze tra maiuscole e minuscole
   -k, --less-mode            variante del modo di stampa di "less"
   -l, --less-mode            variante del modo di stampa di "less"
   -l, --paginate          impagina l'output con "pr"
   -m, --avoid-wraps          non estende i campi oltre i newline
   -n, --avoid-wraps          non estende i campi oltre i newline
   -o, --old-diffs        crea in ogni caso diff in vecchio stile
   -o, --printer              sovraimponi come per le stampanti
   -p, --patch-format     genera formato patch
   -p, --printer              sovraimponi come per le stampanti
   -q, --quiet                inibilsce il messaggio di apertura di "mdiff"
   -s, --statistics           riporta il numero di parole rimosse, inserite, ecc.
   -s, --strip-comments   togli le righe di commento
   -t, --expand-tabs       espandi le tabulazioni in spazi nell'output
   -t, --terminal             usa termcap come terminale
   -u, --unidiffs         crea diff unificate
   -v, --verbose          riporta alcune statistiche su stderr
   -v, --version              stampa la versione del programma ed esce
   -w, --start-delete=STRINGA marcatore d'inizio della regione rimossa
   -x, --end-delete=STRINGA   marcatore di fine della regione rimossa
   -y, --start-insert=STRINGA marcatore d'inizio della regione inserita
   -z, --end-insert=STRINGA   marcatore di fine della regione inserita
   -z, --terminal             use termcap come terminale
  %d gruppo,  %d gruppi,  , %d file  , %d file  %d elemento
  %d elementi
  %d membro
  %d membri
  %d membro,  %d membri,  %d sovrapposizione
  %d sovrapposizioni
 %s (per l'espressione regolare `%s') %s: %d parola %s: %d parole %s: programma di input terminato dal segnale %d
 %s: programma di output terminato dal segnale %d
 "
 , %d elemento
 , %d elementi
 , raggruppamento , fatto
 Se nessun FILE è indicato, leggi lo standard input.
 Apertura di "%s Argomenti obbligatori per le opzioni lunghe lo sono anche per quelle corte.
 Letto riepilogo: Lettura di %s in corso Segnalare i bug a <wdiff-bugs@gnu.org>.
 Ordinamento Ordinamento dei membri This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Questo è software libero; si veda il sorgente per le condizioni di copiatura.
NON c'è alcuna garanzia; neppure di COMMERCIABILITÀ o IDONEITÀ AD UNO
SCOPO PARTICOLARE.
 Questo programma riporta anche come sarebbe stato possibile chiamare "mdiff"
direttamente.
 Questo programma è software libero; potete redistribuirlo e/o modificarlo
secondo i termini della GNU General Public License come pubblicata
dalla Free Software Foundation; o la versione 3, o (a vostra scelta)
ogni versione successiva.

Questo programma è distribuito con la speranza che possa essere utile,
ma SENZA ALCUNA GARANZIA; anche senza la garanzia implicita di
COMMERCIABILITÀ o di IDONEITÀ AD UNO SCOPO PARTICOLARE.
Si consulti la GNU General Public License per maggiori dettagli.

Dovreste aver ricevuto una copia della GNU General Public License con questo
questo programma. In caso contrario, consultare <http://www.gnu.org/licenses/>.
 Usare "%s --help" per ulteriori informazioni.
 Uso: %s [OPZIONE]... FILE1 FILE2
 Uso: %s [OPZIONE]... FILE1 FILE2
  o: %s -d [OPZIONE]... [FILE]
 Uso: %s [OPZIONE]... [FILE]
 Uso: %s [OPZIONE]... [FILE]...
 Se nessun FILE è indicato, o FILE è -, leggi lo standard input.
 Riepilogo di lavoro: impossibile usare -t, termcap non disponibile impossibile usare -z, termcap non disponibile intestazione "new"' mancante nella diff contestuale alla riga %ld intestazione "old" mancante nella diff contestuale alla riga %ld impossibile accedere al database di termcap impossibile trovare un nome per la diff alla riga %ld directory non gestite errore nella redirezione dello stream esecuzione di %s non riuscita opzione %s ignorata (non implementata) intestazione di diff unificata non valida alla riga %ld diff contestuale malformata alla riga %ld diff unificata malformata alla riga %ld mdiff - Analizza diversi file, ricerca sequenze simile, e produce un elenco,
possibilmente dettagliato, di differenze e similarità.
 argomenti di file mancanti non esiste alcuna directory temporanea appropriata solo un file può essere lo standard input solo un nome di file permesso le opzioni -123RSYZ hanno senso solo quando ci sono due input scegli un terminale usando la variabile d'ambiente TERM il tipo di terminale "%s" non è definito troppi argomenti di file usare "%s --help" per ulteriori informazioni
 impossibile aprire "%s" unify - Trasforma diff contestuali in unificate, o viceversa.
 wdiff - Confronta la parole in due file e riporta le differenze.
 wdiff - Calcola le differenze tra le parole lanciando internamente "mdiff -W".
Questo programma esiste solo per gestire la precedente sintassi di wdiff.
 unione di parole solo per due file (per ora) 