��    G     T    �$      �0  :   �0     1  (   )1     R1  ;   a1  %   �1  @   �1  8   2  �   =2  V   3  R   ^3  M   �3  O   �3  ?   O4  N   �4  G   �4  ;   &5  >   b5  C   �5  �   �5  P   �6  <   �6  M   7  �   ]7  O   �7  F   48  O   {8  L   �8  H   9  N   a9  <   �9  Q   �9  8   ?:  j   x:  f   �:  O   J;  �   �;  C   G<  Q   �<  =   �<  9   =  B   U=  O   �=  H   �=  M   1>  Q   >  Q   �>  ?   #?  I   c?  J   �?  I   �?  T   B@  J   �@  ?   �@  K   "A  ?   nA  5   �A  ?   �A  C   $B  0   hB  T   �B  �   �B  8   �C  G   �C  =   D  A   @D  �   �D  H   	E  A   RE  N   �E  J   �E  P   .F  N   F  �   �F  M   bG  D   �G  @   �G  4   6H  >   kH  D   �H  >   �H  >   .I  R   mI  Y   �I  @   J  Q   [J  6   �J  ;   �J  @    K  I   aK  J   �K  O   �K  R   FL  Q   �L  G   �L  F   3M  A   zM  �   �M  R   FN  ;   �N  U   �N  S   +O  �   O  >   P  F   FP  R   �P  9   �P  P   Q  Q   kQ  J   �Q  L   R  �   UR  >   �R     S     #S     4S  I   CS  �   �S  
   BT     MT  B   UT  B   �T  O   �T     +U  L   �U  R   �U  <   KV  F   �V  ?   �V  O   W  O   _W  >   �W  x   �W  ;   gX  ;   �X  A   �X  O   !Y  9   qY  E   �Y  M   �Y  H   ?Z  @   �Z  ;   �Z  B   [  N   H[  G   �[  E   �[  3   %\  Q   Y\  �   �\  S   .]  Q   �]  A   �]  �   ^  <   �^  H   �^  M   -_  8   {_  T   �_  5   	`  >   ?`  P   ~`  D   �`  C   a  A   Xa  "   �a  $   �a  '   �a  3   
b     >b     Jb     ^b     kb     �b     �b     �b  (   �b     �b  %   
c  )   0c  '   Zc  $   �c     �c     �c     �c  &   �c     d      d  !   5d     Wd  $   vd  8   �d  <   �d  0   e      Be  /   ce     �e     �e     �e  "   �e  f   f     tf     �f     �f  =   �f     g     (g  '   Bg  (   jg     �g  !   �g     �g  $   �g  #   h  ,   3h  '   `h  5   �h  *   �h  0   �h  B   i  /   ]i  )   �i  .   �i  6   �i  :   j     Xj  2   pj     �j     �j     �j     �j  M   k  ,   Uk     �k  ,   �k  ,   �k  '   �k  -   "l      Pl  (   ql  (   �l  7   �l  &   �l  #   "m     Fm     fm     �m     �m  	   �m     �m     �m  b   �m  2   )n  L   \n     �n     �n  )   �n     �n  '   o     9o     So     po     �o     �o     �o  a   �o  Y   *p  8   �p  <   �p  9   �p  -   4q  <   bq     �q     �q  [   �q  (   8r     ar     �r      �r     �r  3   �r  3   s  �   :s     �s     �s     �s  %   t     >t     Yt     lt  	   �t     �t     �t     �t     �t  "   �t  #   "u     Fu     au  ,   }u  +   �u     �u  1   �u  0   #v  >   Tv  *   �v  P   �v  H   w  .   Xw  $   �w     �w  ;   �w      x  $   $x  (   Ix  -   rx  .   �x  3   �x     y  6   y     Uy  :   my  J   �y     �y     z  )   z     8z  
   Xz     cz  !   iz  *   �z  %   �z  D   �z  *   !{     L{     e{  $   {{  %   �{     �{  6   �{  (   |  !   A|  (   c|     �|     �|      �|     �|     }  ,   }     L}  N   h}     �}     �}  "   �}  *   �}      "~  !   C~     e~  '   r~  (   �~     �~  4   �~  0   	  1   :  )   l  !   �  0   �  >   �     (�     A�  2   \�     ��     ��     ��     ߀  8   �     %�     4�     C�     ]�     {�  5   ��     ΁     �     ��     �     (�     D�     [�     x�     ��  7   ��     �  '   ��  $   �      C�     d�     v�  "   ��  '   ��     ܃  4   �  8   #�     \�  
   e�  �   p�     =�     J�  :   Q�     ��     ��  *   ��     �     �     �     �     '�  0   =�     n�  7   ��     ��  J   Ά     �     3�     N�  4   _�     ��     ��  #   ć     �     ��     �     �     8�     A�     \�     {�     ��     ��     ��  *   ؈  5   �     9�  9   F�     ��     ��  &   ��  w   މ  c   V�     ��  
   ъ  -   ܊  =   
�  #   H�     l�     ��     ��  +   Ë     �     	�     �     9�  -   H�  b   v�  N   ٌ  E   (�     n�  8   ��  "   ��  ;   ��     �  )   )�     S�     a�     r�  1   ��     ��  ,   ʎ     ��  &   �  (   *�     S�     b�  ,   q�  -   ��  +   ̏  <   ��  k   5�  &   ��     Ȑ     �  \    �  2   ]�  	   ��     ��  -   ��  /   Б      �  $   �     2�  +   O�  3   {�     ��  1   ʒ  2   ��  ,   /�  ;   \�     ��  "   ��     Γ  $   �     �  "   )�  @   L�     ��     ��     ��     ϔ     ܔ  /   �     !�     3�     E�     W�     d�  6   ��  (   ��     �  !   ��     �     5�  )   U�     �     ��  Q   ��  L   ��  )   L�  L   v�     ×  |   ˗  X   H�  #   ��  *   Ř     �  $   ��  3   �  *   R�  "   }�     ��  5   ��  5   ��  �   *�  ^   ��  3   
�  3   >�     r�     ��     ��     ��     ��  !   Л     �  #   ��  (   "�     K�     R�  	   Z�     d�  )   q�     ��     ��     Ü  '   ݜ     �     !�     =�  :   E�      ��     ��     ��          ֝  �  �  =   ˟     	�  *   �     I�  0   X�  %   ��  x   ��  8   (�  �   a�  �   /�  z   ��  �   2�  �   ��  ?   ;�  �   {�  O   �  A   c�  w   ��  v   �  �   ��  �   f�  @   ��  z   6�  �   ��  z   A�  �   ��  x   ?�  y   ��  v   2�  �   ��  D   /�  y   t�  C   �  �   2�  w   ĭ  w   <�  �   ��  K   ��  �   ߯  I   e�  D   ��  L   ��  y   A�  |   ��  �   8�  �   ��  �   E�  {   ɳ  w   E�  N   ��  N   �  �   [�  F   �  u   +�  L   ��  L   �  6   ;�  O   r�  y   ·  5   <�  �   r�  �   ��  B   ��  {   ù  N   ?�  C   ��  �   Һ  G   ]�  ?   ��  }   �  O   c�  z   ��  z   .�  �   ��  v   D�  I   ��  G   �  ?   M�  z   ��  P   �  v   Y�  w   ��  {   H�  �   ��  M   L�  �   ��  J    �  v   k�  x   ��  �   [�  N   ��  �   4�  �   ��  �   ��  w   S�  M   ��  H   �  �   b�  �   ��  8   |�  �   ��  |   8�  �   ��  L   E�  P   ��  ~   ��  <   b�  �   ��  ~   "�  K   ��     ��  �   m�  M   ��     M�     [�     m�  ]   ~�  �   ��     ��     ��  G   ��  G   ��  }   6�  �   ��  y   K�     ��  G   E�  �   ��  D   �  {   S�  {   ��  H   K�  x   ��  @   �  B   N�  O   ��  }   ��  C   _�  G   ��  �   ��  D   n�  H   ��  ?   ��  C   <�  ~   ��  E   ��  |   E�  9   ��  |   ��  �   y�  �   ��  |   �  @   ��  �   =�  8   ��  I   �  F   O�  A   ��  �   ��  @   p�  <   ��  z   ��  <   i�  M   ��  E   ��  (   :�  2   c�  $   ��  @   ��  
   ��     �     �     #�     =�     A�     `�  )   |�      ��  ,   ��  0   ��  '   %�  $   M�     r�     ��     ��  /   ��     ��     ��  .   �  (   D�  1   m�  @   ��  ?   ��  4    �  )   U�  >   �  %   ��  $   ��     	�  +   (�  k   T�  '   ��     ��  $   �  >   ,�  $   k�  !   ��  9   ��  =   ��     *�  .   J�     y�  )   ��  +   ��  ,   ��  %   �  9   =�  (   w�  7   ��  G   ��  4    �  /   U�  '   ��  T   ��  D   �      G�  C   h�  !   ��     ��     ��     �  j   �  +   ��     ��  ,   ��  +   �  )   -�  ,   W�  !   ��  )   ��  *   ��  E   ��  3   A�  *   u�  $   ��  $   ��     ��     ��     �     �     &�  j   5�  ;   ��  H   ��     %�     >�  /   W�     ��  0   ��     ��     ��  #   �     8�     S�     n�  U   ��  U   ��  ;   0�  B   l�  B   ��  <   ��  J   /�  +   z�  0   ��  u   ��  8   M�  ,   ��     ��  .   ��  )   ��  E   &�  E   l�  �   ��     c�  !   ��  $   ��  /   ��  !   ��     �  )   6�     `�  !   o�     ��     ��     ��  $   ��  %   �     *�  !   G�  )   i�  2   ��  "   ��  >   ��  0   (�  C   Y�  .   ��  ]   ��  Q   *�  3   |�  -   ��  !   ��  B    �  &   C�  ,   j�  8   ��  5   ��  8   �  I   ?�  /   ��  @   ��     ��  9   �  T   T�     ��     ��  $   ��  &   ��     �     �  ,   $�  4   Q�  4   ��  S   ��  :   �     J�     b�  '   {�  0   ��  +   ��  K      ;   L  .   �  6   �  $   �  &    +   : )   f    � 8   � !   � D       L    Z 3   h =   � 9   � 1       F 2   S 3   �    � 9   � 8    5   A 4   w !   � A   � H    $   Y    ~ I   �    �            6 B   B    �    �    �    �    � 6   � #   2    V "   o    �    � #   � $   � "       / 6   L    � ;   � *   � ,   	    1	    K	 '   k	 %   �	    �	 D   �	 E   
    [
    g
 �   u
    U    g 9   v    �    � 6   �         )    :    M    k 5   �    � @   �     Q   .     � #   �    � ?   �        2 (   M    v    �    � (   �    � )   � '   !    I    g     '   � /   � C   �    4 C   @    �    � 2   � �   � t   x    � 
    -    A   A (   �    �    � +   � 8       O    k "   �    � 1   � }   � d   f L   �     M   1 7    J   �     /       ?    Q    e 4   }    � 6   �    � 0    2   L        � 0   � 1   � ?    O   F u   � *    .   7    f _   } B   �         ( =   1 8   o    � 1   �    � ,    F   4    { C   � D   � %   " I   H    � )   �    � +   � &    ,   > K   k     � &   �    �        # @   A    �    �    �    �    � I    )   X    � %   �    �    � 2   � )   *     T  P   c  K   �  )    ! Q   *!    |! z   �!    " 2   �" 9   �"    �" "   �" P   # A   n# +   �# &   �# C   $ C   G$ �   �$ p   % K   �% K   �%    &    7&    9&    R&    m& 1   �& 
   �& *   �& (   �&    '    !' 
   *'    5' 5   H'    ~'    �'    �' -   �' !   ( !   &(    H( J   Q( %   �(    �(    �(    �(    �(           d      �   �  `       @  �  �  4  �     Z     �  !   �   &  �   t   w   *       F  �  �       =       �         P  �      �   �    �   �   �       f              [  �   �               b      �   I  �     [   �   Q   �      %  p            �  �   �    �   �  �         �        \   �             �    {           4  "   .  �            p   �      �  �  I     �       @      �  �      (      �  �           .       �  P         -  �      �              .          #  Y   �                            �       �       �  h   �      �  y   S   �  �          �  k  k         s           �  �   �       n   '  ~   5   ;   �  �            T   �              o  ]   �       0       �       )   �   �  	  �   �  �         �   �  (  |  C  �  �  1      �  �   T  B   �  9  �       7   {             �      �      m  �  �      <  �   �   C      9   A   m   t    -  !  E   e  b   K  a    �  �   0          f   U       r        �               @      W  &  /  �         g       �  �  c          >  �   �       �      �     �     �   �      O          �   �  �   �       Y  �   �  �   O     H   �   �   
  �  B         �   ]  0  8  �      	   	  �     $  *      �  �      �   w  L         �      �  �     7  �  �  �       �      �   �      �  �               �   �     H  �  R        (   �  "          �          ?           �   2   G            4   �      �         �  E      �       1  �      C   9  �      i         n    �   +  |   �   /           l  <             �   D   r   i   u   �   �   �              3   �       �             �  L   �  �           ^      $  �   �   F        �   N  �     6  �         D  �  _   v       �        �  �  �   3  j             �  �   �          h    �  �  ,       7  y  �   Z   �  �       G   �      �   ~      �  �   �  �               �       �      �      �           S  �   *      �  o   �          �       �  !      =  8      �   �     �  =  g  V        �   +  �  �         �   
   �  A          K   <  �   �  6         6      �   #      �                a   �   d      �   �  �   /          �       �   �   x   �   �   �  �   �   �       z      W   #   �    �   A          �   �          �  `                X   �  �  >   2  ?  �  _     �  M  v  �   �   �   x      �           B  �   �   %     &   �            1   �   E  F   ^       �  G  :   �   +   �  8       �  "  D  '      �   J  V           �       �   ?  �           e   M       R       J   )  5      $   }   ,  q     �  '   :          ;  �  �  �  >      �  j  z   �         �      :  �           )     �  �       �   3              �         �       �  �   l   u  �  �  \  �   2  
          �       s  X  5      �  c   �   -   N   �  �  ;          q        �       �       �   %        ,      Q      �  �          �       U  }  �    
    The file is already fully retrieved; nothing to do.

 
%*s[ skipping %sK ] 
%s received, redirecting output to %s.
 
%s received.
 
Originally written by Hrvoje Niksic <hniksic@xemacs.org>.
 
REST failed, starting from scratch.
        --accept-regex=REGEX        regex matching accepted URLs
        --ask-password              prompt for passwords
        --auth-no-challenge         send Basic HTTP authentication information
                                     without first waiting for the server's
                                     challenge
        --backups=N                 before writing file X, rotate up to N backup files
        --bind-address=ADDRESS      bind to ADDRESS (hostname or IP) on local host
        --body-data=STRING          send STRING as data. --method MUST be set
        --body-file=FILE            send contents of FILE. --method MUST be set
        --ca-certificate=FILE       file with the bundle of CAs
        --ca-directory=DIR          directory where hash list of CAs is stored
        --certificate-type=TYPE     client certificate type, PEM or DER
        --certificate=FILE          client certificate file
        --config=FILE               specify config file to use
        --connect-timeout=SECS      set the connect timeout to SECS
        --content-disposition       honor the Content-Disposition header when
                                     choosing local file names (EXPERIMENTAL)
        --content-on-error          output the received content on server errors
        --crl-file=FILE             file with bundle of CRLs
        --cut-dirs=NUMBER           ignore NUMBER remote directory components
        --default-page=NAME         change the default page name (normally
                                     this is 'index.html'.)
        --delete-after              delete files locally after downloading them
        --dns-timeout=SECS          set the DNS lookup timeout to SECS
        --egd-file=FILE             file naming the EGD socket with random data
        --exclude-domains=LIST      comma-separated list of rejected domains
        --follow-ftp                follow FTP links from HTML documents
        --follow-tags=LIST          comma-separated list of followed HTML tags
        --ftp-password=PASS         set ftp password to PASS
        --ftp-stmlf                 use Stream_LF format for all binary FTP files
        --ftp-user=USER             set ftp user to USER
        --ftps-clear-data-connection    cipher the control channel only; all the data will be in plaintext
        --ftps-fallback-to-ftp          fall back to FTP if FTPS is not supported in the target server
        --ftps-implicit                 use implicit FTPS (default port is 990)
        --ftps-resume-ssl               resume the SSL/TLS session started in the control connection when
                                         opening a data connection
        --header=STRING             insert STRING among the headers
        --hsts-file                 path of HSTS database (will override default)
        --http-password=PASS        set http password to PASS
        --http-user=USER            set http user to USER
        --https-only                only follow secure HTTPS links
        --ignore-case               ignore case when matching files/directories
        --ignore-length             ignore 'Content-Length' header field
        --ignore-tags=LIST          comma-separated list of ignored HTML tags
        --input-metalink=FILE       download files covered in local Metalink FILE
        --keep-session-cookies      load and save session (non-permanent) cookies
        --limit-rate=RATE           limit download rate to RATE
        --load-cookies=FILE         load cookies from FILE before session
        --local-encoding=ENC        use ENC as the local encoding for IRIs
        --max-redirect              maximum redirections allowed per page
        --metalink-over-http        use Metalink metadata from HTTP response headers
        --method=HTTPMethod         use method "HTTPMethod" in the request
        --no-cache                  disallow server-cached data
        --no-check-certificate      don't validate the server's certificate
        --no-config                 do not read any config file
        --no-cookies                don't use cookies
        --no-dns-cache              disable caching DNS lookups
        --no-glob                   turn off FTP file name globbing
        --no-hsts                   disable HSTS
        --no-http-keep-alive        disable HTTP keep-alive (persistent connections)
        --no-if-modified-since      don't use conditional if-modified-since get
                                     requests in timestamping mode
        --no-iri                    turn off IRI support
        --no-passive-ftp            disable the "passive" transfer mode
        --no-proxy                  explicitly turn off proxy
        --no-remove-listing         don't remove '.listing' files
        --no-use-server-timestamps  don't set the local file's timestamp by
                                     the one on the server
        --no-warc-compression       do not compress WARC files with GZIP
        --no-warc-digests           do not calculate SHA1 digests
        --no-warc-keep-log          do not store the log file in a WARC record
        --password=PASS             set both ftp and http password to PASS
        --post-data=STRING          use the POST method; send STRING as the data
        --post-file=FILE            use the POST method; send contents of FILE
        --prefer-family=FAMILY      connect first to addresses of specified family,
                                     one of IPv6, IPv4, or none
        --preferred-location        preferred location for Metalink resources
        --preserve-permissions      preserve remote file permissions
        --private-key-type=TYPE     private key type, PEM or DER
        --private-key=FILE          private key file
        --progress=TYPE             select progress gauge type
        --protocol-directories      use protocol name in directories
        --proxy-password=PASS       set PASS as proxy password
        --proxy-user=USER           set USER as proxy username
        --random-file=FILE          file with random data for seeding the SSL PRNG
        --random-wait               wait from 0.5*WAIT...1.5*WAIT secs between retrievals
        --read-timeout=SECS         set the read timeout to SECS
        --referer=URL               include 'Referer: URL' header in HTTP request
        --regex-type=TYPE           regex type (posix)
        --regex-type=TYPE           regex type (posix|pcre)
        --reject-regex=REGEX        regex matching rejected URLs
        --rejected-log=FILE         log reasons for URL rejection to FILE
        --remote-encoding=ENC       use ENC as the default remote encoding
        --report-speed=TYPE         output bandwidth as TYPE.  TYPE can be bits
        --restrict-file-names=OS    restrict chars in file names to ones OS allows
        --retr-symlinks             when recursing, get linked-to files (not dir)
        --retry-connrefused         retry even if connection is refused
        --save-cookies=FILE         save cookies to FILE after session
        --save-headers              save the HTTP headers to file
        --secure-protocol=PR        choose secure protocol, one of auto, SSLv2,
                                     SSLv3, TLSv1 and PFS
        --show-progress             display the progress bar in any verbosity mode
        --spider                    don't download anything
        --start-pos=OFFSET          start downloading from zero-based position OFFSET
        --strict-comments           turn on strict (SGML) handling of HTML comments
        --trust-server-names        use the name specified by the redirection
                                     URL's last component
        --unlink                    remove file before clobber
        --user=USER                 set both ftp and http user to USER
        --waitretry=SECONDS         wait 1..SECONDS between retries of a retrieval
        --warc-cdx                  write CDX index files
        --warc-dedup=FILENAME       do not store records listed in this CDX file
        --warc-file=FILENAME        save request/response data to a .warc.gz file
        --warc-header=STRING        insert STRING into the warcinfo record
        --warc-max-size=NUMBER      set maximum size of WARC files to NUMBER
        --warc-tempdir=DIRECTORY    location for temporary files created by the
                                     WARC writer
        --wdebug                    print Watt-32 debug output
     %s (env)
     %s (system)
     %s (user)
     %s: certificate common name %s doesn't match requested host name %s.
     %s: certificate common name is invalid (contains a NUL character).
    This may be an indication that the host is not who it claims to be
    (that is, it is not the real %s).
     eta %s     in    -4,  --inet4-only                connect only to IPv4 addresses
   -6,  --inet6-only                connect only to IPv6 addresses
   -A,  --accept=LIST               comma-separated list of accepted extensions
   -B,  --base=URL                  resolves HTML input-file links (-i -F)
                                     relative to URL
   -D,  --domains=LIST              comma-separated list of accepted domains
   -E,  --adjust-extension          save HTML/CSS documents with proper extensions
   -F,  --force-html                treat input file as HTML
   -H,  --span-hosts                go to foreign hosts when recursive
   -I,  --include-directories=LIST  list of allowed directories
   -K,  --backup-converted          before converting file X, back up as X.orig
   -K,  --backup-converted          before converting file X, back up as X_orig
   -L,  --relative                  follow relative links only
   -N,  --timestamping              don't re-retrieve files unless newer than
                                     local
   -O,  --output-document=FILE      write documents to FILE
   -P,  --directory-prefix=PREFIX   save files to PREFIX/..
   -Q,  --quota=NUMBER              set retrieval quota to NUMBER
   -R,  --reject=LIST               comma-separated list of rejected extensions
   -S,  --server-response           print server response
   -T,  --timeout=SECONDS           set all timeout values to SECONDS
   -U,  --user-agent=AGENT          identify as AGENT instead of Wget/VERSION
   -V,  --version                   display the version of Wget and exit
   -X,  --exclude-directories=LIST  list of excluded directories
   -a,  --append-output=FILE        append messages to FILE
   -b,  --background                go to background after startup
   -c,  --continue                  resume getting a partially-downloaded file
   -d,  --debug                     print lots of debugging information
   -e,  --execute=COMMAND           execute a `.wgetrc'-style command
   -h,  --help                      print this help
   -i,  --input-file=FILE           download URLs found in local or external FILE
   -k,  --convert-links             make links in downloaded HTML or CSS point to
                                     local files
   -l,  --level=NUMBER              maximum recursion depth (inf or 0 for infinite)
   -m,  --mirror                    shortcut for -N -r -l inf --no-remove-listing
   -nH, --no-host-directories       don't create host directories
   -nc, --no-clobber                skip downloads that would download to
                                     existing files (overwriting them)
   -nd, --no-directories            don't create directories
   -np, --no-parent                 don't ascend to the parent directory
   -nv, --no-verbose                turn off verboseness, without being quiet
   -o,  --output-file=FILE          log messages to FILE
   -p,  --page-requisites           get all images, etc. needed to display HTML page
   -q,  --quiet                     quiet (no output)
   -r,  --recursive                 specify recursive download
   -t,  --tries=NUMBER              set number of retries to NUMBER (0 unlimits)
   -v,  --verbose                   be verbose (this is the default)
   -w,  --wait=SECONDS              wait SECONDS between retrievals
   -x,  --force-directories         force creation of directories
   Issued certificate has expired.
   Issued certificate not yet valid.
   Self-signed certificate encountered.
   Unable to locally verify the issuer's authority.
  (%s bytes)  (unauthoritative)
  [following] %d redirections exceeded.
 %s
 %s (%s) - %s saved [%s/%s]

 %s (%s) - %s saved [%s]

 %s (%s) - Connection closed at byte %s.  %s (%s) - Data connection: %s;  %s (%s) - Read error at byte %s (%s). %s (%s) - Read error at byte %s/%s (%s).  %s (%s) - written to stdout %s[%s/%s]

 %s (%s) - written to stdout %s[%s]

 %s ERROR %d: %s.
 %s URL: %s %2d %s
 %s has sprung into existence.
 %s request sent, awaiting response...  %s subprocess %s subprocess failed %s subprocess got fatal signal %d %s: %s must only be used once
 %s: %s, closing control connection.
 %s: %s: Failed to allocate %ld bytes; memory exhausted.
 %s: %s: Failed to allocate enough memory; memory exhausted.
 %s: %s: Invalid %s; use `on', `off' or `quiet'.
 %s: %s: Invalid WARC header %s.
 %s: %s: Invalid boolean %s; use `on' or `off'.
 %s: %s: Invalid byte value %s
 %s: %s: Invalid header %s.
 %s: %s: Invalid number %s.
 %s: %s: Invalid progress type %s.
 %s: %s: Invalid restriction %s,
    use [unix|vms|windows],[lowercase|uppercase],[nocontrol],[ascii].
 %s: %s: Invalid time period %s
 %s: %s: Invalid value %s.
 %s: %s:%d: unknown token "%s"
 %s: %s:%d: warning: %s token appears before any machine name
 %s: %s; disabling logging.
 %s: Cannot read %s (%s).
 %s: Cannot resolve incomplete link %s.
 %s: Couldn't find usable socket driver.
 %s: Error in %s at line %d.
 %s: Invalid --execute command %s
 %s: Invalid URL %s: %s
 %s: No certificate presented by %s.
 %s: Syntax error in %s at line %d.
 %s: The certificate of %s has been revoked.
 %s: The certificate of %s has expired.
 %s: The certificate of %s hasn't got a known issuer.
 %s: The certificate of %s is not trusted.
 %s: The certificate of %s is not yet activated.
 %s: The certificate of %s was signed using an insecure algorithm.
 %s: The certificate signer of %s was not a CA.
 %s: Unknown command %s in %s at line %d.
 %s: WGETRC points to %s, which doesn't exist.
 %s: Warning: Both system and user wgetrc point to %s.
 %s: aprintf: text buffer is too big (%d bytes), aborting.
 %s: cannot stat %s: %s
 %s: cannot verify %s's certificate, issued by %s:
 %s: corrupt time-stamp.
 %s: illegal option -- `-n%c'
 %s: invalid option -- '%c'
 %s: missing URL
 %s: no certificate subject alternative name matches
	requested host name %s.
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: unable to resolve bind address %s; disabling bind.
 %s: unable to resolve host address %s
 %s: unknown/unsupported file type.
 %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 ' (no description) (try:%2d) , %s (%s) remaining , %s remaining --convert-links or --convert-file-only can be used together only if outputting to a regular file.
 -O not supported for metalink download. Ignoring.
 -k or -r can be used together with -O only if outputting to a regular file.
 ==> CWD not needed.
 ==> CWD not required.
 Address family for hostname not supported All requests done Already have correct symlink %s -> %s

 Argument buffer too small Authentication selected: %s
 BODY data file %s missing: %s
 Bad port number Bad value for ai_flags Bind error (%s).
 Both --no-clobber and --convert-file-only were specified, only --convert-file-only will be used.
 Both --no-clobber and --convert-links were specified, only --convert-links will be used.
 CDX file does not list checksums. (Missing column 'k'.)
 CDX file does not list original urls. (Missing column 'a'.)
 CDX file does not list record ids. (Missing column 'u'.)
 Can't be verbose and quiet at the same time.
 Can't timestamp and not clobber old files at the same time.
 Cannot back up %s as %s: %s
 Cannot convert links in %s: %s
 Cannot convert timestamp to http format. Falling back to time 0 as last modification time.
 Cannot get REALTIME clock frequency: %s
 Cannot initiate PASV transfer.
 Cannot open %s: %s Cannot open cookies file %s: %s
 Cannot parse PASV response.
 Cannot specify both --ask-password and --password.
 Cannot specify both --inet4-only and --inet6-only.
 Cannot specify both -k or --convert-file-only and -O if multiple URLs are given, or in combination
with -p or -r. See the manual for details.

 Cannot unlink %s (%s).
 Cannot write to %s (%s).
 Cannot write to WARC file.
 Cannot write to temporary WARC file.
 Certificate must be X.509
 Checksum matches.
 Checksum mismatch for file %s.
 Compile:  Computing checksum for %s
 Connecting to %s:%d...  Connecting to %s|%s|:%d...  Connecting to [%s]:%d...  Continuing in background, pid %d.
 Continuing in background, pid %lu.
 Continuing in background.
 Control connection closed.
 Conversion from %s to UTF-8 isn't supported
 Converted links in %d files in %s seconds.
 Converting links in %s...  Cookie coming from %s attempted to set domain to  Copyright (C) %s Free Software Foundation, Inc.
 Could not create temporary file. Skipping signature download.
 Could not download all resources from %s.
 Could not find Metalink data in HTTP response. Downloading file using HTTP GET.
 Could not find acceptable digest for Metalink resources.
Ignoring them.
 Could not initialize SSL. It will be disabled. Could not open CDX file for output.
 Could not open WARC file.
 Could not open downloaded file for signature verification.
 Could not open downloaded file.
 Could not open temporary WARC file.
 Could not open temporary WARC log file.
 Could not open temporary WARC manifest file.
 Could not read CDX file %s for deduplication.
 Could not seed PRNG; consider using --random-file.
 Creating symlink %s -> %s
 Data matches signature, but signature is not trusted.
 Data transfer aborted.
 Debugging support not compiled in. Ignoring --debug flag.
 Digests are disabled; WARC deduplication will not find duplicate records.
 Directories:
 Directory    Disabling SSL due to encountered errors.
 Download quota of %s EXCEEDED!
 Download:
 ERROR ERROR: Cannot open directory %s.
 ERROR: Failed to load CRL file '%s': (%d)
 ERROR: Failed to open cert %s: (%d).
 ERROR: GnuTLS requires the key and the cert to be of the same type.
 ERROR: Redirection (%d) without location.
 Encoding %s isn't valid
 Error closing %s: %s
 Error in handling the address list.
 Error in proxy URL %s: Must be HTTP.
 Error in server greeting.
 Error in server response, closing control connection.
 Error initializing X509 certificate: %s
 Error matching %s against %s: %s
 Error opening GZIP stream to WARC file.
 Error opening WARC file %s.
 Error parsing certificate: %s
 Error parsing proxy URL %s: %s.
 Error while matching %s: %d
 Error writing to %s: %s
 Error writing warcinfo record to WARC file.
 Exiting due to error in %s
 FINISHED --%s--
Total wall clock time: %s
Downloaded: %d files, %s in %s (%s)
 FTP options:
 FTPS options:
 Failed reading proxy response: %s
 Failed to download %s. Skipping resource.
 Failed to unlink symlink %s: %s
 Failed writing HTTP request: %s.
 File         File %s already there; not retrieving.
 File %s already there; not retrieving.

 File %s exists.
 File %s not modified on server. Omitting download.

 File %s retrieved but checksum does not match. 
 File %s retrieved but signature does not match. 
 File `%s' already there; not retrieving.
 File has already been retrieved.
 Found %d broken link.

 Found %d broken links.

 Found exact match in CDX file. Saving revisit record to WARC.
 Found no broken links.

 GNU Wget %s built on %s.

 GNU Wget %s, a non-interactive network retriever.
 GPGME data_new_from_mem: %s
 GPGME op_verify: %s
 GPGME op_verify_result: NULL
 Giving up.

 GnuTLS: unimplemented 'secure-protocol' option value %d
 HSTS options:
 HTTP options:
 HTTPS (SSL/TLS) options:
 HTTPS support not compiled in IPv6 addresses not supported Incomplete or invalid multibyte sequence encountered
 Index of /%s on %s:%d Interrupted by a signal Invalid IPv6 numeric address Invalid PORT.
 Invalid UTF-8 sequence: %s
 Invalid back reference Invalid character class name Invalid collation character Invalid content of \{\} Invalid dot style specification %s; leaving unchanged.
 Invalid host name Invalid name of the symlink, skipping.
 Invalid preceding regular expression Invalid pri value. Assuming %d.
 Invalid range end Invalid regular expression Invalid regular expression %s, %s
 Invalid signature. Rejecting resource.
 Invalid user name Last-modified header invalid -- time-stamp ignored.
 Last-modified header missing -- time-stamps turned off.
 Length:  Length: %s License GPLv3+: GNU GPL version 3 or later
<http://www.gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
 Link         Link:  Loaded %d record from CDX.

 Loaded %d records from CDX.

 Loaded CA certificate '%s'
 Loaded CRL file '%s'
 Loading robots.txt; please ignore errors.
 Locale:  Location: %s%s
 Logged in!
 Logging and input file:
 Logging in as %s ...  Logically impossible section reached in getftp() Login incorrect.
 Mail bug reports and suggestions to <bug-wget@gnu.org>
 Malformed status line Mandatory arguments to long options are mandatory for short options too.

 Memory allocation failure Memory allocation problem
 Memory exhausted Metalink headers found. Switching to Metalink mode.
 Name or service not known No URLs found in %s.
 No address associated with hostname No certificate found
 No data received.
 No error No headers, assuming HTTP/0.9 No match No matches on pattern %s.
 No previous regular expression No such directory %s.

 No such file %s.
 No such file %s.

 No such file or directory %s.

 Non-recoverable failure in name resolution Not descending to %s as it is excluded/not-included.
 Not sure     OpenSSL: unimplemented 'secure-protocol' option value %d
 Opening WARC file %s.

 Output will be written to %s.
 Parameter string not correctly encoded Parsing system wgetrc file (env SYSTEM_WGETRC) failed.  Please check
'%s',
or specify a different file using --config.
 Parsing system wgetrc file failed.  Please check
'%s',
or specify a different file using --config.
 Password for user %s:  Password:  Please report this issue to bug-wget@gnu.org
 Please send bug reports and questions to <bug-wget@gnu.org>.
 Premature end of regular expression Processing request in progress Proxy tunneling failed: %s Read error (%s) in headers.
 Recursion depth %d exceeded max. depth %d.
 Recursive accept/reject:
 Recursive download:
 Regular expression too big Rejecting %s.
 Remote file does not exist -- broken link!!!
 Remote file exists and could contain further links,
but recursion is disabled -- not retrieving.

 Remote file exists and could contain links to other resources -- retrieving.

 Remote file exists but does not contain any link -- not retrieving.

 Remote file exists.

 Remote file is newer than local file %s -- retrieving.

 Remote file is newer, retrieving.
 Remote file no newer than local file %s -- not retrieving.
 Removed %s.
 Removing %s since it should be rejected.
 Removing %s.
 Request canceled Request not canceled Required attribute missing from Header received.
 Resolving %s...  Resource type %s not supported, ignoring...
 Retrying.

 Reusing existing connection to %s:%d.
 Reusing existing connection to [%s]:%d.
 Saving to: %s
 Scheme missing Server did not accept the 'PBSZ 0' command.
 Server did not accept the 'PROT %c' command.
 Server error, can't determine system type.
 Server file no newer than local file %s -- not retrieving.

 Server ignored If-Modified-Since header for file %s.
You might want to add --no-if-modified-since option.

 Servname not supported for ai_socktype Signature validation suceeded.
 Skipping directory %s.
 Specifying both --start-pos and --continue is not recommended; --continue will be disabled.
 Spider mode enabled. Check if remote file exists.
 Startup:
 Success Symlinks not supported, skipping symlink %s.
 Syntax error in Set-Cookie: %s at position %d.
 System error Temporary failure in name resolution The certificate has expired
 The certificate has not yet been activated
 The certificate's owner does not match hostname %s
 The server refuses login.
 The sizes do not match (local %s) -- retrieving.
 The sizes do not match (local %s) -- retrieving.

 This version does not have support for IRIs
 To connect to %s insecurely, use `--no-check-certificate'.
 Trailing backslash Try `%s --help' for more options.
 Unable to delete %s: %s
 Unable to establish SSL connection.
 Unable to get cookie for %s
 Unable to parse metalink file %s.
 Unable to read signature content from temporary file. Skipping.
 Unhandled errno %d
 Unknown authentication scheme.
 Unknown error Unknown host Unknown system error Unknown type `%c', closing control connection.
 Unmatched ( or \( Unmatched ) or \) Unmatched [ or [^ Unmatched \{ Unsupported algorithm '%s'.
 Unsupported listing type, trying Unix listing parser.
 Unsupported quality of protection '%s'.
 Unsupported scheme %s Unterminated IPv6 numeric address Usage: %s NETRC [HOSTNAME]
 Usage: %s [OPTION]... [URL]...
 Username/Password Authentication Failed.
 Using %s as listing tmp file.
 WARC options:
 WARC output does not work with --continue or --start-pos, they will be disabled.
 WARC output does not work with --no-clobber, --no-clobber will be disabled.
 WARC output does not work with --spider.
 WARC output does not work with timestamping, timestamping will be disabled.
 WARNING WARNING: combining -O with -r or -p will mean that all downloaded content
will be placed in the single file you specified.

 WARNING: timestamping does nothing in combination with -O. See the manual
for details.

 WARNING: using a weak random seed.
 Warning: wildcards not supported in HTTP.
 Wgetrc:  When downloading signature:
%s: %s.
 Will not retrieve dirs since depth is %d (max %d).
 Write failed, closing control connection.
 Wrote HTML-ized index to %s [%s].
 Wrote HTML-ized index to %s.
 You cannot specify both --body-data and --body-file.
 You cannot specify both --post-data and --post-file.
 You cannot use --post-data or --post-file along with --method. --method expects data through --body-data and --body-file options You must specify a method through --method=HTTPMethod to use with --body-data or --body-file.
 Your OpenSSL version is too old to support TLSv1.1
 Your OpenSSL version is too old to support TLSv1.2
 _open_osfhandle failed ` ai_family not supported ai_socktype not supported cannot create pipe cannot restore fd %d: dup2 failed connected.
 couldn't connect to %s port %d: %s
 cwd_count: %d
cwd_start: %d
cwd_end: %d
 done.
 done.   done.     failed: %s.
 failed: No IPv4/IPv6 addresses for host.
 failed: timed out.
 fake_fork() failed
 fake_fork_child() failed
 gmtime failed. This is probably a bug.
 idn_decode failed (%d): %s
 idn_encode failed (%d): %s
 ignored ioctl() failed.  The socket could not be set as blocking.
 locale_to_utf8: locale is unset
 memory exhausted nothing to do.
 time unknown        unspecified Project-Id-Version: wget 1.16.3.124
Report-Msgid-Bugs-To: bug-wget@gnu.org
POT-Creation-Date: 2016-06-14 08:19+0000
PO-Revision-Date: 2016-06-21 06:00+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <tp@lists.linux.it>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:22+0000
X-Generator: Launchpad (build 18115)
Language: it
 
    Il file è già interamente scaricato; niente da fare.

 
%*s[ %sK ignorato ] 
%s ricevuti, output ridirezionato su %s.
 
%s ricevuto.
 
Scritto da Hrvoje Niksic <hniksic@xemacs.org>.
 
REST non riuscito, riavvio da capo.
        --accept-regex=REGEX          Espressione regolare per gli URL da
                                     accettare
        --ask-password                Chiede la password
        --auth-no-challenge           Invia informazioni di autenticazione Basic
                                     HTTP senza prima aspettare la richiesta
                                     dal server
        --backups=N                        Prima di salvare il file X, torna a N
                                     backup precedenti
        --bind-address=INDIRIZZO      Lega INDIRIZZO (nome host o IP) all'host
                                     locale
        --body-data=STRINGA           Invia STRINGA come dati, --method deve
                                     essere impostato
        --body-file=FILE              Invia i contenuti del FILE, --method deve
                                     essere impostato
        --ca-certificate=FILE         File con il bundle dei CA
        --ca-directory=DIR            Directory dov'è memorizzato l'elenco delle
                                     Autorità di Certificazione (CA)
        --certificate-type=TIPO       Tipo di certificato del client, PEM o DER
        --certificate=FILE            File certificato del client
        --config=FILE                 Specifica il file di configurazione da
                                     usare
        --connect-timeout=SECONDI     Imposta il timeout di connessione a
                                     SECONDI
        --content-disposition         Onora l'intestazione Content-Disposition
                                     quando vengono scelti nomi di file locali
                                     (SPERIMENTALE)
        --content-on-error            Mostra i contenuti ricevuti quando si
                                     verificano errori lato server
        --crl-file=FILE               File con il bundle dei CRL
        --cut-dirs=NUMERO             Ignora NUMERO componenti delle directory
                                     remote
        --default-page=NOME           Modifica il nome della pagina predefinita
                                     (solitamente "index.html")
        --delete-after                Elimina localmente i file dopo averli
                                     scaricati
        --dns-timeout=SECONDI         Imposta il timeout per la risoluzione del
                                     DNS a SECONDI
        --egd-file=FILE               File col nome del socket EGD con dati
                                     casuali
        --exclude-domains=ELENCO      Elenco separato da virgole di domini
                                     rifiutati
        --follow-ftp                  Segue i collegamenti FTP dai documenti
                                     HTML
        --follow-tags=ELENCO          Elenco separato da virgole di tag HTML che
                                     vengono seguiti
        --ftp-password=PASS           Imposta la password ftp a PASS
        --ftp-stmlf                   Usa il formato Stream_LF per i file FTP
                                     binari
        --ftp-user=UTENTE             Imposta l'utente ftp a UTENTE
        --ftps-clear-data-connection  Cifra solamente il canale di controllo,
                                     tutti i dati saranno in chiaro
        --ftps-fallback-to-ftp        Usa FTP se FTPS non è supportato dal
                                     server
        --ftps-implicit               Usa FTPS implicito
                                     (porta predefinita a 990)
        --ftps-resume-ssl             Ripristina la sessione SSL/TLS avviata
                                     nella connessione di controllo quando
                                     viene aperta una connessione dati
        --header=STRINGA              Inserisce STRINGA tra le intestazioni
        --hsts-file                   Percorso al database HSTS
                                     (soprascrive quello predefinito)
        --http-passwd=PASSWORD        Imposta la password http a PASSWORD
        --http-user=UTENTE            Imposta l'utente http a UTENTE
        --https-only                  Segue solo i collegamenti HTTPS sicuri
        --ignore-case                 Ignora maiuscole/minuscole in file e
                                     directory
        --ignore-length               Ignora il campo Content-Length nelle
                                     intestazioni
        --ignore-tags=ELENCO          Elenco separato da virgole di tag HTML che
                                     vengono ignorati
        --metalink-file               Scarica gli URL trovati nel FILE metalink
                                     locale o esterno
        --keep-session-cookies        Carica e salva i cookie per la sessione
                                     (non permanenti)
        --limit-rate=VELOCITÀ         Limita la velocità di scaricamento a
                                     VELOCITÀ
        --load-cookies=FILE           Carica i cookie da FILE prima della
                                     sessione
        --local-encoding=COD          Usa COD come codifica locale per gli IRI
        --max-redirect                Numero massimo di ridirezioni per pagina
        --metalink-over-http          Usa meta-dati Metalink dalle intestazioni
                                     nella risposta HTTP
        --method=METODOHTTP           Usa METODOHTTP nell'intestazione
        --no-cache                    Non permette la cache dei dati lato
                                     server
        --no-check-certificate        Non verifica il certificato del server
        --no-config                   Non legge alcun file di configurazione
        --no-cookies                  Non usa i cookie
        --no-dns-cache                Disattiva la cache di risoluzione del DNS
        --no-glob                     Disabilita il "globbing" FTP sui nomi dei
                                     file
        --no-hsts                     Disabilita HSTS
        --no-http-keep-alive          Disabilita l'HTTP keep-alive (connessioni
                                     persistenti)
        --no-if-modified-since        Non usa richieste if-modified-since nella
                                     modalità timestamping
        --no-iri                      Disattiva la gestione di IRI
        --no-passive-ftp              Disabilita la modalità di trasferimento
                                     passiva
        --no-proxy                    Disattiva esplicitamente l'uso del proxy
        --no-remove-listing           Non elimina i file ".listing"
        --no-use-server-timestamps         Non imposta l'orario del file locale con
                                     quello del server
        --no-warc-compression         Non comprime i file WARC con GZIP
        --no-warc-digests             Non calcola i digest SHA1
        --no-warc-keep-log            Non archivia il file di registro in un
                                     record WARC
        --password=PASSWORD           Imposta la password ftp e http a PASSWORD
        --post-data=STRINGA           Usa il metodo POST e spedisce STRINGA come
                                     dati
        --post-file=FILE              Usa il metodo POST e invia i contenuti del
                                     FILE
        --prefer-family=FAMIGLIA      Si connette prima agli indirizzi della
                                     FAMIGLIA specificata (IPv6, IPv4 o none)
        --preferred-location          Posizione preferita per le risorse
                                     Metalink
        --preserve-permissions        Preserva i permessi remoti dei file
        --private-key-type=TIPO       Tipo di chiave privata, PEM o DER
        --private-key=FILE            File della chiave privata
        --progress=TIPO               Sceglie il TIPO di misurazione
                                     dell'avanzamento
        --protocol-directories        Usa il nome del protocollo nelle directory
        --proxy-passwd=PASSWORD       Imposta la password per il proxy a
                                     PASSWORD
        --proxy-user=UTENTE           Imposta il nome utente per il proxy a
                                     UTENTE
        --random-file=FILE            File con dati casuali per inizializzare
                                     SSL PRNG
        --random-wait                 Aspetta 0.5*ATTESA...1.5*ATTESA secondi
                                     tra gli scaricamenti
        --read-timeout=SECONDI        Imposta il timeout di lettura a SECONDI
        --referer=URL                 Include l'intestazione "Referer: URL"
                                     nella richiesta HTTP
        --regex-type=TIPO             Tipo di espressione regolare (posix)
        --regex-type=TIPO             Tipo di espressione regolare
                                     (posix o pcre)
        --reject-regex=REGEX          Espressione regolare per gli URL da
                                     rifiutare
        --rejected-log=FILE           Registra i motivi per cui gli URL sono
                                     stati rifiutati su FILE
        --remote-encoding=COD         Usa COD come codifica remota predefinita
        --report-speed=TIPO           Banda in uscita definita come TIPO
                                     (può essere "bits")
        --restrict-file-names=SO      Limita i caratteri nei nomi dei file a
                                     quelli permessi dal sistema operativo SO
                                     indicato
        --retr-symlinks               Scarica i file (non le directory) puntati
                                     dai collegamenti simbolici quando in
                                     modalità ricorsiva
        --retry-connrefused           Riprova anche se la connessione è
                                     rifiutata
        --save-cookies=FILE           Salva i cookie su FILE dopo la sessione
        --save-headers                Salva le intestazioni HTTP su file
        --secure-protocol=PROT        Sceglie il protocollo sicuro; uno tra
                                     auto, SSLv2, SSLv3, TLSv1 e PFS
        --show-progress               Visualizza la barra di avanzamento in
                                     qualsiasi modo prolisso
        --spider                      Non scarica niente
        --start-pos=OFFSET            Avvia lo scaricamento alla posizione a
                                     base zero OFFSET
        --strict-comments             Tratta i commenti HTML in modalità strict
                                     (SGML)
        --trust-server-names          Usa il nome indicato dall'ultimo
                                     componente dell'URL di re-direzione
        --unlink                      Rimuove il file prima di sovrascrivere
        --user=UTENTE                 Imposta il nome utente ftp e http a UTENTE
        --waitretry=SECONDI           Aspetta 1...SECONDI tra i tentativi di
                                     scaricamento
        --warc-cdx                    Scrive file indice CDX
        --warc-dedup=NOMEFILE         Non archivia i record elencati nel file
                                     CDX specificato
        --warc-file=FILENAME          Salva i dati richiesta/risposta in un file
                                     .warc.gz
        --warc-header=STRINGA         Inserisce STRINGA nel record warcinfo
        --warc-max-size=NUMERO        Imposta la dimensione massima dei file
                                     WARC a NUMERO
        --warc-tempdir=DIRECTORY      Posizione per il file temporanei creati
                                     dal processo di scrittura WARC
        --wdebug                      Stampa le informazioni di debug Watt-32
     %s (env)
     %s (sistema)
     %s (utente)
     %s: il nome comune di certificato %s non corrisponde al nome dell'host
    richiesto %s.
     %s: il nome comune di certificato non è valido (contiene un carattere NUL).
    Questo può indicare che l'host non è chi si dichiara di essere
    (cioè non è il vero %s).
     prev %s     in    -4,  --inet4-only                  Si connette solo a indirizzi IPv4
   -6,  --inet6-only                  Si connette solo a indirizzi IPv6
   -A,  --accept=ELENCO               Elenco separato da virgole di estensioni
                                     accettate
   -B,  --base=URL                    Risolve i collegamenti nel file HTML di
                                     input (-i -F) come relativi all'URL
   -D,  --domains=ELENCO              Elenco separato da virgole di domini
                                     accettati
   -E,  --adjust-extension            Salva i documenti HTML/CSS con
                                     l'estensione corretta
   -F,  --force-html                  Tratta il file di input come HTML
   -H,  --span-hosts                  Visita anche altri host quando in modalità
                                     ricorsiva
   -I,  --include-directories=ELENCO  Elenco di directory consentite
   -K,  --backup-converted            Salva il file X come X.orig prima di
                                     convertirlo
   -K,  --backup-converted            Salva il file X come X_orig prima di
                                     convertirlo
   -L,  --relative                    Segue solo i collegamenti relativi
   -N,  --timestamping                Non scarica file più vecchi di quelli
                                     locali
   -O   --output-document=FILE        Scrive i documenti su FILE
   -P,  --directory-prefix=PREFISSO   Salva i file in PREFISSO/...
   -Q,  --quota=NUMERO                Imposta la quota di scaricamento a NUMERO
   -R,  --reject=ELENCO               Elenco separato da virgole di estensioni
                                     rifiutate
   -S,  --server-response             Stampa la risposta del server
   -T,  --timeout=SECONDI             Imposta tutti i timeout a SECONDI
   -U,  --user-agent=AGENTE           Si identifica come AGENTE invece che come
                                     Wget/VERSIONE
   -V,  --version                     Visualizza la versione ed esce
   -X,  --exclude-directories=ELENCO  Elenco di directory non consentite
   -a,  --append-output=FILE          Accoda i messaggi su FILE
   -b,  --background                  Va in background dopo l'avvio
   -c,  --continue                    Riprende a scaricare un file parzialmente
                                     scaricato
   -d,  --debug                       Stampa le informazioni di debug
   -e,  --execute=COMANDO             Esegue COMANDO come se fosse scritto in
                                     ".wgetrc"
   -h,  --help                        Stampa questo aiuto
   -i,  --input-file=FILE             Scarica gli URL trovati nel FILE locale o
                                     esterno
   -k,  --convert-links               Punta i collegamenti nei file HTML o CSS a
                                     file locali
   -l,  --level=NUMERO                Profondità massima di ricorsione
                                     (inf o 0 = illimitata)
   -m,  --mirror                      Scorciatoia per:
                                     -N -r -l inf --no-remove-listing
   -nH, --no-host-directories         Non crea le directory host
   -nc, --no-clobber                  Non avvia lo scaricamento di file già
                                     esistenti (soprascrivendoli)
   -nd, --no-directories              Non crea directory
   -np, --no-parent                   Non risale alla directory superiore
   -nv, --no-verbose                  Meno prolisso, ma non silenzioso
   -o,  --output-file=FILE            Registra i messaggi su FILE
   -p,  --page-requisites             Scarica tutte le immagini, ecc...
                                     necessarie per visualizzare la pagina HTML
   -q,  --quiet                       Silenzioso (nessun output)
   -r,  --recursive                   Scaricamento ricorsivo
   -t,  --tries=NUMERO                Imposta il NUMERO di tentativi
                                     (0 = illimitati)
   -v,  --verbose                     Prolisso (predefinito)
   -w,  --wait=SECONDI                Aspetta SECONDI tra i vari scaricamenti
   -x,  --force-directories           Forza la creazione di directory
   Il certificato rilasciato è scaduto.
   Il certificato rilasciato non è ancora valido.
   Trovato certificato auto-firmato.
   Impossibile verificare localmente l'autorità dell'emittente.
  (%s byte)  (non autorevole)
  [segue] superate %d ridirezioni.
 %s
 %s (%s) - %s salvato [%s/%s]

 %s (%s) - %s salvato [%s]

 %s (%s) - Connessione chiusa al byte %s.  %s (%s) - Connessione dati: %s;  %s (%s) - Errore di lettura al byte %s (%s). %s (%s) - Errore di lettura al byte %s/%s (%s).  %s (%s) - scritto su stdout %s[%s/%s]

 %s (%s) - scritto su stdout %s[%s]

 %s ERRORE %d: %s.
 %s URL: %s %2d %s
 %s è venuto in esistenza.
 Richiesta %s inviata, in attesa di risposta...  sotto-processo %s sotto-processo %s non riuscito il sotto-processo %s ha ricevuto il segnale %d %s: %s può essere usata solo una volta
 %s: %s, chiusura della connessione di controllo.
 %s: %s: allocazione di %ld byte non riuscita; memoria esaurita.
 %s: %s: allocazione di memoria non riuscita; memoria esaurita.
 %s: %s: %s non valido, usare "on", "off" o "quiet".
 %s: %s: intestazione WARC %s non valida.
 %s: %s: valore logico %s non valido, usare "on" oppure "off".
 %s: %s: valore di byte %s non valido
 %s: %s: intestazione %s non valida.
 %s: %s: numero %s non valido.
 %s: %s: tipo di avanzamento %s non valido.
 %s: %s: restrizione %s non valida,
    usare [unix|vms|windows],[lowercase|uppercase],[nocontrol],[ascii].
 %s: %s: periodo di tempo %s non valido
 %s: %s: valore %s non valido.
 %s: %s:%d: termine "%s" sconosciuto
 %s: %s:%d: attenzione: %s appare prima di un nome di macchina
 %s: %s; registrazione disabilitata.
 %s: impossibile leggere %s (%s).
 %s: impossibile risolvere il collegamento incompleto %s.
 %s: impossibile trovare un driver per i socket utilizzabile.
 %s: errore in %s alla riga %d.
 %s: comando %s passato a --execute non valido
 %s: URL non valido %s: %s
 %s: nessun certificato presentato da %s.
 %s: errore di sintassi in %s alla riga %d.
 %s: Il certificato di %s è stato revocato.
 %s: Il certificato di %s è scaduto.
 %s: il certificate di %s non ha un emittente conosciuto.
 %s: il certificato di %s non è fidato.
 %s: il certificato di %s non è stato ancora attivato.
 %s: il certificato di %s non è stato firmato con un algoritmo sicuro.
 %s: il firmatario del certificato %s non è una CA.
 %s: comando sconosciuto %s in %s alla riga %d.
 %s: WGETRC punta a %s, che non esiste.
 %s: Attenzione: il file wgetrc di sistema e quello personale puntano entrambi a %s.
 %s: aprintf: buffer di testo troppo grande (%d byte), interruzione.
 %s: stat di %s non riuscita: %s
 %s: impossibile verificare il certificato di %s, rilasciato da %s:
 %s: marca temporale danneggiata.
 %s: opzione illecita -- "-n%c"
 %s: opzione non valida -- %c
 %s: URL mancante
 %s: nessuno dei nomi alternativi indicati nel certificato corresponde al
    nome dell'host richiesto %s.
 %s: l'opzione "%c%s" non accetta argomenti
 %s: l'opzione "%s" è ambigua
 %s: l'opzione "%s" è ambigua; possibilità: %s: l'opzione "--%s" non accetta argomenti
 %s: l'opzione "%s" richiede un argomento
 %s: l'opzione "-W %s" non accetta argomenti
 %s: l'opzione "-W %s" è ambigua
 %s: l'opzione "%s" richiede un argomento
 %s: l'opzione richiede un argomento -- %c
 %s: impossibile risolvere l'indirizzo di bind %s; bind disabilitato.
 %s: impossibile risolvere l'indirizzo dell'host %s
 %s: tipo di file sconosciuto/non gestito.
 %s: opzione "%c%s" non riconosciuta
 %s: opzione "--%s" non riconosciuta
 " (nessuna descrizione) (tentativo:%2d) , %s (%s) rimanenti , %s rimanenti --convert-links o --convert-file-only possono essere usati assieme solo in scrittura su un file regolare.
 -O non supportato per scaricare metalink, verrà ignorato.
 -k o -r può essere usato con -O solo in scrittura su un file regolare.
 ==> CWD non necessario.
 ==> CWD non necessario.
 Famiglia indirizzo del nome host non supportata Tutte le richieste eseguite Collegamento simbolico già esistente %s -> %s

 Parametro buffer troppo piccolo Autenticazione selezionata: %s
 File dati %s del BODY mancante: %s
 Numero di porta non valido Valore errato per ai_flags Errore di bind (%s).
 Specificati sia --no-clobber che --convert-links, solo --convert-links verrà usato.
 Specificati sia --no-clobber che --convert-links, solo --convert-links verrà usato.
 Il file CDX non riporta i checksum (colonna "k" mancante).
 Il file CDX non riporta gli URL originali (colonna "a" mancante).
 Il file CDX non riporta gli ID dei record (colonna "u" mancante).
 Impossibile essere prolisso e silenzioso allo stesso tempo.
 Impossibile registrare le date senza allo stesso tempo modificare i file.
 Impossibile fare il backup di %s in %s: %s
 Impossibile convertire i collegamenti in %s: %s
 Impossibile convertire la marcatura temporale nel formato http. Viene usato il tempo 0 come ultima data di modifica.
 Impossibile ottenere la frequenza di clock REALTIME: %s
 Impossibile iniziare il trasferimento PASV.
 Impossibile aprire %s: %s Impossibile aprire il file dei cookies %s: %s
 Impossibile analizzare la risposta PASV.
 Impossibile specificare --ask-password e --password simultaneamente.
 Impossibile specificare --inet4-only e --inet6-only simultaneamente.
 Impossibile specificare -k o --convert-file-only e -O simultaneamente se sono forniti URL multipli
o in combinazione con -p o -r. Consultare il manuale per maggiori dettagli.

 Impossibile rimuovere %s (%s).
 Impossibile scrivere in %s (%s).
 Impossibile scrivere nel file WARC.
 Impossibile scrivere nel file WARC temporaneo.
 Il certificato deve essere X.509
 Il checksum corrisponde.
 Il checksum del file %s non corrisponde.
 Compilazione:  Calcolo checksum per %s in corso
 Connessione a %s:%d...  Connessione a %s|%s|:%d...  Connessione a [%s]:%d...  Prosecuzione in background, pid %d.
 Prosecuzione in background, pid %lu.
 Prosecuzione in background.
 Connessione di controllo chiusa.
 Conversione da %s a UTF-8 non supportata
 Convertiti collegamenti in %d file in %s secondi.
 Conversione collegamenti in %s...  Cookie proveniente da %s ha tentato di impostare il dominio a  Copyright (C) %s Free Software Foundation, Inc.
 Impossibile creare un file temporaneo: scaricamento firma saltato.
 Impossibile scaricare tutte le risorse da %s.
 Impossibile trovare dati Metalink nella risposta HTTP. Il file viene scaricato con HTTP GET.
 Impossibile trovare un digest valido per le risorse Metalink.
Verranno ignorate.
 Impossibile inizializzare SSL, verrà disabilitato. Impossibile aprire il file CDX per l'output.
 Impossibile aprire il file WARC.
 Impossibile aprire il file scaricato per la verifica della firma.
 Impossibile aprire il file scaricato.
 Impossibile aprire il file WARC temporaneo.
 Impossibile aprire il file di registro WARC temporaneo.
 Impossibile aprire il file manifest WARC temporaneo.
 Impossibile leggere il file CDX %s per de-duplicazione.
 Impossibile inizializzare PRNG; considerare l'utilizzo di --random-file.
 Creazione del collegamento simbolico %s → %s
 I dati corrispondono alla firma, ma la firma non è affidabile.
 Trasferimento dati interrotto.
 Supporto al debug non attivato, --debug verrà ignorato.
 I digest sono disabilitati: la de-duplicazione WARC non rileverà record duplicati.
 Directory:
 Directory    SSL disabilitato a causa di errori.
 Quota di scaricamento di %s SUPERATA!
 Scaricamento:
 ERRORE ERRORE: Impossibile aprire la directory %s.
 ERRORE: Impossibile caricare il file CRL "%s": (%d)
 ERRORE: Impossibile aprire il certificato %s: (%d).
 ERRORE: GnuTLS richiede che la chiave e la certificazione siano dello stesso tipo.
 ERRORE: ridirezione (%d) senza posizione di destinazione.
 Codifica %s non valida
 Errore chiudendo %s: %s
 Errore nel gestire l'elenco indirizzi.
 Errore nell'URL del proxy %s: deve essere HTTP.
 Errore nel codice di benvenuto del server.
 Errore nella risposta del server, chiusura della connessione di controllo.
 Errore durante l'inizializzazione del certificato X509: %s
 Errore nella corrispondenza di %s con %s: %s.
 Errore nell'aprire lo stream GZIP verso il file WARC.
 Errore nell'aprire il file WARC %s.
 Errore analizzando il certificato: %s
 Errore analizzando l'URL del proxy %s: %s.
 Errore cercando la corrispondenza %s: %d
 Errore scrivendo in %s: %s
 Errore nello scrivere il record warcinfo sul file WARC.
 Uscita causata dall'errore in %s
 TERMINATO --%s--
Tempo totale: %s
Scaricati: %d file, %s in %s (%s)
 Opzioni FTP:
 Opzioni FTP:
 Lettura della risposta del proxy non riuscita: %s.
 Scaricamento di %s non riuscito, la risorsa verrà ignorata.
 Rimozione del collegamento simbolico %s non riuscita: %s
 Scrittura della richiesta HTTP non riuscita: %s.
 File         Il file %s è già presente, non viene scaricato.
 Il file %s è già presente, non viene scaricato.

 Il file %s esiste.
 File %s non modificato sul server, non viene scaricato.

 Recuperato il file %s, ma il checksum non corrisponde. 
 Recuperato il file %s, ma la firma non corrisponde. 
 Il file "%s" è già presente, non viene scaricato.
 Il file è già stato scaricato.
 Trovato %d collegamento rotto.

 Trovati %d collegamenti rotti.

 Trovata corrispondenza esatta nel file CDX. Salvataggio record su WARC.
 Nessun collegamento rotto trovato.

 GNU Wget %s compilato su %s.

 GNU Wget %s, un programma non interattivo per scaricare file dalla rete.
 GPGME data_new_from_mm: %s
 GPGME op_verify: %s
 GPGME op_verify_result: NULL
 Rinuncio.

 GnuTLS: valore %d dell'opzione "secure-protocol" non implementata
 Opzioni HSTS:
 Opzioni HTTP:
 Opzioni HTTPS (SSL/TLS):
 Gestione di HTTPS non compilata Indirizzo IPv6 non supportato Incontrata sequenza multibyte incompleta o non valida
 Indice della directory /%s su %s:%d Interrotto da un segnale Indirizzo numerico IPv6 non valido PORT non valido.
 Sequenza UTF-8 non valida: %s
 Riferimento all'indietro non valido Nome classe del carattere non valido Carattere di collazione non valido Contenuto di \{\} non valido Stile di progresso %s non valido; lasciato invariato.
 Nome dell'host non valido Il nome del collegamento simbolico non è valido, saltato.
 Espressione regolare precedente non valida Valore pri non valido, viene ipotizzato %d.
 Limite massimo non valido Espressione regolare non valida Espressione regolare %s non valida, %s
 Firma non valida: risorsa rifiutata.
 Nome utente non valido Intestazione Last-modified non valida -- marche temporali ignorate.
 Intestazione Last-modified mancante -- marche temporali disattivate.
 Lunghezza:  Lunghezza: %s Licenza GPLv3+: GNU GPL versione 3 o successiva
<http://www.gnu.org/licenses/gpl.html>.
Questo è software libero: siete liberi di modificarlo e redistribuirlo.
Non c'è ALCUNA GARANZIA, negli estremi permessi dalla legge.
 Collegam.         Collegamento:  Caricato %d record da CDX.

 Caricati %d record da CDX.

 Caricato certificato CA "%s"
 Caricato il file CRL "%s"
 Caricamento di robots.txt; ignorare eventuali errori.
 Locale:  Posizione: %s%s
 Accesso eseguito.
 File di registro e di input:
 Accesso come utente %s ...  Raggiunta sezione logicamente impossibile in getftp() Accesso non corretto.
 Inviare segnalazioni di bug e suggerimenti a <bug-wget@gnu.org>
 Riga di stato malformata Gli argomenti obbligatori per le opzioni lunghe lo sono anche per quelle corte.

 Errore di allocazione di memoria Problema di allocazione di memoria
 Memoria esaurita Trovata intestazione Metalink: attivata la modalità Metalink.
 Nome o servizio sconosciuto Nessun URL trovato in %s.
 Nessun indirizzo associato col nome host Nessun certificato trovato
 Nessun dato ricevuto.
 Nessun errore Nessuna intestazione, si assume HTTP/0.9 Nessuna corrispondenza Nessun corrispondenza con il modello %s.
 Nessuna espressione regolare precedente La directory %s non esiste.

 Il file %s non esiste.
 Il file %s non esiste.

 Il file o la directory %s non esiste.

 Errore irreversibile nella risoluzione del nome Non si discende nella directory %s perché è esclusa/non inclusa.
 Incerto     OpenSSL: valore %d dell'opzione "secure-protocol" non implementata
 Apertura file WARC %s.

 L'output sarà scritto su %s.
 Stringa del parametro non codificata correttamente Analisi del file wgetrc di sistema (env SYSTEM_WGETRC) non riuscita. Controllare
"%s"
o specificare un altro file utilizzando --config.
 Analisi del file wgetrc di sistema non riuscita. Controllare
"%s"
o specificare un altro file utilizzando --config.
 Password per l'utente %s:  Password:  Segnalare questo problema a bug-wget@gnu.org
 Inviare segnalazioni di bug e suggerimenti a <bug-wget@gnu.org>.
 Fine prematura dell'espressione regolare Elaborazione richiesta in corso Tunnel proxy non riuscito: %s Errore di lettura nelle intestazioni (%s).
 La profondità di ricorsione %d eccede il massimo (%d).
 Accetto/Rifiuto ricorsivo:
 Scaricamento ricorsivo:
 Espressione regolare troppo grande %s rifiutato.
 Il file remoto non esiste -- collegamento rotto!
 Il file remoto esiste e potrebbe contenere ulteriori collegamenti,
ma la ricorsione è disabilitata -- non viene scaricato.

 Il file remoto esiste e potrebbe contenere collegamenti ad altre risorse -- scaricamento in corso.

 Il file remoto esiste ma non contiene collegamenti -- non viene scaricato.

 Il file remoto esiste.

 Il file remoto è più recente del file locale %s -- scaricamento in corso.

 Il file remoto è più recente, scaricamento in corso.
 Il file remoto è più vecchio del file locale %s -- non viene scaricato.
 %s rimosso.
 Rimozione di %s poiché deve essere rifiutato.
 Rimozione di %s.
 Richiesta annullata Richiesta non annullata Manca un attributo richiesto nello header ricevuto.
 Risoluzione di %s...  Risorsa di tipo %s non supportata, verrà ignorata...
 Altro tentativo in corso.

 Riutilizzo della connessione esistente a %s:%d.
 Riutilizzo della connessione esistente a [%s]:%d.
 Salvataggio in: %s
 Schema mancante Il server non ha accettato il comando "PBSZ 0".
 Il server non ha accettato il comando "PROT %c".
 Errore del server, impossibile determinare il tipo di sistema.
 Il file del server è più vecchio del file locale %s -- non viene scaricato.

 Il server ha ignorato l'intestazione If-Modified-Since per il file %s.
Aggiungere l'opzione --no-if-modified-since.

 Nome server non supportato per ai_socktype Verifica della firma effettuata con successo.
 Directory %s saltata.
 Specificare sia --start-pos che --continue non è consigliato: --continue verrà disabilitato.
 Modalità spider abilitata. Controllare se il file remoto esiste.
 Avvio:
 Successo Collegamenti simbolici non gestiti, collegamento %s saltato.
 Errore di sintassi in Set-Cookie: %s alla posizione %d.
 Errore di sistema Risoluzione del nome temporaneamente non riuscita Il certificato è scaduto
 Il certificato non è ancora stato attivato
 Il proprietario del certificato non corrisponde al nome dell'host %s.
 Il server rifiuta l'accesso.
 Le dimensioni non coincidono (locale %s) -- scaricamento in corso.
 Le dimensioni non coincidono (locale %s) -- scaricamento in corso.

 Questa versione non gestisce gli IRI
 Per connettersi a %s in modo non sicuro, usare "--no-check-certificate".
 Backslash finale Usare "%s --help" per ulteriori opzioni.
 Impossibile rimuovere %s: %s
 Impossibile stabilire una connessione SSL.
 Impossibile ottenere il cookie per %s
 Impossibile analizzare il file metalink %s.
 Impossibile leggere il contenuto della firma dal file temporaneo, saltato.
 Codice di errore %d non gestito
 Schema di autenticazione sconosciuto.
 Errore sconosciuto Host sconosciuto Errore di sistema sconosciuto Tipo "%c" sconosciuto, chiusura della connessione di controllo.
 ( o \( senza corrispondenza ) o \) senza corrispondenza [ o [^ senza corrispondenza \{ senza corrispondenza Algoritmo "%s" non supportato.
 Tipo di elencazione non gestito, si prova un parser di elencazioni Unix.
 Qualità di protezione "%s" non gestita.
 Schema %s non gestito Indirizzo numerico IPv6 non terminato Uso: %s NETRC [HOSTNAME]
 Uso: %s [OPZIONI]... [URL]...
 Autenticazione nome utente/password non riuscita.
 Usato %s come file di elenco temporaneo.
 Opzioni WARC:
 L'output WARC non funziona con --continue o --start-pos: verranno disabilitati.
 L'output WARC non funziona con --no-clobber, --no-clobber verrà ignorato.
 L'output WARC non funziona con --spider.
 L'output WARC non funziona con la registrazione delle date: verrà disabilitata.
 AVVERTIMENTO ATTENZIONE: l'uso di -O con -r o -p fa sì che tutto ciò che viene scaricato
verrà messo nel singolo file specificato.

 ATTENZIONE: non è possibile registrare la data dei file in combinazione con -O.
Consultare il manuale per maggiori dettagli.

 ATTENZIONE: si sta usando un seme casuale debole.
 Attenzione: i metacaratteri non sono supportati in HTTP.
 Wgetrc:  Nello scaricare la firma:
%s: %s.
 Le directory non verranno scaricate perché la loro profondità è %d (max %d).
 Scrittura non riuscita, chiusura della connessione di controllo.
 Indice in formato HTML scritto in %s [%s].
 Indice in formato HTML scritto in %s.
 Impossibile specificare --body-data e --body-file simultaneamente.
 Impossibile specificare --post-data e --post-file simultaneamente.
 Impossibile usare --post-data o --post-file assieme a --method. --method richiede i dati tramite le opzioni --body-data e --body-file È necessario specificare attraverso --method=HTTPMethod un metodo da utilizzare con --body-data o --body-file.
 La versione installata di OpenSSL è troppo vecchia per supportare TLSv1.1
 La versione installata di OpenSSL è troppo vecchia per supportare TLSv1.2
 _open_osfhandle non riuscita " ai_family non supportata ai_socktype non supportato impossibile creare la pipe impossibile ripristinare fd %d: dup2 non riuscita connesso.
 impossibile connettersi a %s porta %d: %s
 cwd_count: %d
cwd_start: %d
cwd_end: %d
 fatto.
 fatto.   fatto.     non riuscito: %s.
 non riuscito: nessun indirizzo IPv4/IPv6 per l'host.
 non riuscito: tempo scaduto.
 fake_fork() non riuscita
 fake_fork_child() non riuscita
 gmtime non riuscita, potrebbe essere un bug.
 idn_decode non riuscita (%d): %s
 idn_encode non riuscita (%d): %s
 ignorato ioctl() non riuscita. Il socket non può essere impostato come bloccante.
 locale_to_utf8: locale non impostata
 memoria esaurita niente da fare.
 data sconosciuta        non specificato 