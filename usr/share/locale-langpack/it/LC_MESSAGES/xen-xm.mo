��          t      �                      .  "   A      d  7   �  c   �  )   !  R   K  0   �  �  �     w     }     �  #   �  "   �  9   �  g   %  )   �  g   �  5              	                              
                   %(2)s Internal error: %(1)s. Permission denied. The %(1)s handle %(2)s is invalid. The method %(1)s is unsupported. The method %(1)s takes %(2)s argument(s) (%(3)s given). The network you specified already has a PIF attached to it, and so another one may not be attached. This map already contains %(1)s -> %(2)s. Value "%(2)s" for %(1)s is not supported by this server.  The server said "%(3)s". You attempted an operation that was not allowed. Project-Id-Version: xen
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-03-31 17:40+0100
PO-Revision-Date: 2012-04-05 10:47+0000
Last-Translator: Milo Casagrande <milo.casagrande@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:40+0000
X-Generator: Launchpad (build 18115)
 %(2)s Errore interno: %(1)s Permesso negato. L'handle %(1)s %(2)s non è valido. Il metodo %(1)s non è supportato. Il metodo %(1)s richiede %(2)s argomento/i (e non %(3)s). La rete specificata presenta già un PIF attaccato a essa; non è quindi possibile attaccarne un altro. Questa mappa contiene già %(1)s -> %(2)s Il valore "%(2)s" per %(1)s non è supportato da questo server. La rispota del server è stata "%(3)s". È stata tentata una operazione che non era permessa. 