��    8      �  O   �      �     �     �     �  #   �     "  "   :      ]     ~     �     �  
   �     �     �     �     	               0     5     N     ]     b     f  	   }  	   �     �     �     �  	   �     �     �  !        8     I     R     c     h     �      �     �     �  #   �               '  	   /  
   9     D     Q  	   S     ]     d     r     w     �  �  �     F
     U
     \
  ,   l
     �
  2   �
  !   �
       !   -     O     e     s     �     �     �     �     �     �     �     �          	       	   $  	   .     8     U     t  	   �     �     �  1   �                     1     6  "   V  ,   y  #   �     �  *   �     �  %        +     4     A     S     a  
   c     n     u     �     �     �     *                           "      	             .   $   /                                 7   &       1           %      2              '   4       0   ,   8   -   #                +           5         )           (             !                           
                 3   6     FLAG Values:
  FLAGS %s version %s
 %s: cannot allocate space for file
 %s: cannot open %s: %s
 %s: cannot read attrs on "%s": %s
 %s: device %lld is already open
 %s: failed to open %s: %s
 %s: size check failed
 %s: unknown flag
 ,real-time 0x%lx  %lu pages (%llu : %lu)
 AG Directory creation failed EXT [-ais] [off len] [-drsw] [off len] [-f] [-r] [-S seed] [off len] [-r] [off len] [-v] [N] [N] | [-rwx] [off len] [command] [off len] cannot reserve space close the current open file command "%s" not found
 directory directory create error directory createname error error allocating space for a file exit the program external extra arguments
 fifo help for one or all commands illegal block size %d
 illegal directory block size %d
 illegal sector size %d
 internal no files are open, try 'help open'
 none open the file specified by path primary read-only read-write regular file s secondary socket symbolic link sync unknown option -%c %s
 xfs Project-Id-Version: xfsprogs
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-11-27 10:38+1100
PO-Revision-Date: 2012-09-16 15:20+0000
Last-Translator: Roberto Di Girolamo <roberto.digirolamo@gmail.com>
Language-Team: Italian <it@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:23+0000
X-Generator: Launchpad (build 18115)
  FLAG Valore:
  FLAGS %s versione %s
 %s: impossibile allocare spazio per il file
 %s: imppossibile aprire %s:%s
 %s: impossibile leggere gli attributi su "%s": %s
 %s: dispositivo %lld già aperto
 %s: apertura fallita di %s: %s
 %s: controllo dimensione fallita
 %s: flag sconosciuto
 , tempo reale 0x%lx %lu pagine (%llu : %lu)
 AG Creaione directory fallita EXT [-ais] [off len] [-drsw] [off len] [-f] [-r] [-S seed] [off len] [-r] [off len] [-v] [N] [N] | [-rwx] [off len] [comando] [off len] impossibile riservare spazio chiudi il corrente file aperto comando "%s" non trovato
 directory errore creazione directory errore creazione nome directory errore nell'allocazione dello spazio per il ifile esci dal programma esterno argomenti extra
 fifo aiuto per uno o tutti i comandi dimensione illegale del blocco %d
 dimensione illegale del blocco directory %d
 dimensione illegale del settore %d
 interno nessun files è aperto, prova 'help open'
 nessuno apri il file specificato dal percorso primaria sola lettura lettura-scrittura file regolare s secondaria socket collegamento simbolico sync opzione sconosciuta -%c %s
 xfs 