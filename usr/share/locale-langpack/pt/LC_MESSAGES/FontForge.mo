��    �      �  K  �      �     �      �  v   	     �     �     �     �  +   �     �  
   �  ,        5  8   D     }  E   �  
   �     �  	   �                 
   '     2  
   G     R     [     a     j     z     �     �      �     �  w   �     N  �   \     �     
       	   (     2     R  @   W  /   �     �     �     �     �                    .     ;     B     Y     p     y     �  
   �     �     �     �     �     �  	   �     �        J        [  
   s     ~     �     �     �     �     �  �   �  n   �  O        k     t     }     �     �     �     �     �     �     �     �     �     �     �     �               (     <     N     ^     q     �  	   �     �     �      �     �  	   �     �     	          2     ?  P   L     �     �     �     �           :      G      W      k      {      �      �      �      �      �   
   �      �      !     !     !!     /!     @!  g   L!     �!     �!     �!     �!     �!      "     "     "     ("     4"  
   D"     O"     c"     y"     �"     �"  �   �"  �   1#     �#  
   �#     �#     �#     �#  m   �#  O   g$  2   �$  2   �$  �   %  B   �%  $   ,&  �   Q&  �   �&  S   �'  C   (  q   c(  :  �(  8  *     I+     Y+     f+     y+     �+     �+     �+  -   �+  	   �+     �+     �+     �+     ,     	,     ,     ,     ,      ,     3,     9,  
   B,     M,  ,   U,     �,     �,     �,     �,     �,     �,  	   �,     �,     �,     �,     �,     �,  	   �,     �,     �,     -     -     -     -     &-     :-     A-     R-     X-     _-     e-     m-     p-  	   �-     �-  	   �-     �-     �-     �-     �-     �-     �-     �-     �-     �-     �-     �-     �-     �-     �-     .     .     !.     2.     ;.  �  D.  *   0  #   @0  x   d0     �0     �0     1  "   51  '   X1     �1     �1  1   �1     �1  I   �1     )2  E   @2     �2     �2     �2     �2     �2     �2     �2      3  
   "3     -3     63     <3     E3     V3     ^3  %   r3  2   �3  '   �3  �   �3     �4  �   �4     v5     �5     �5  
   �5  &   �5     �5  E   �5  G   .6  #   v6  *   �6     �6     �6  	   �6     �6     �6     7     %7     .7     J7  	   e7     o7     x7     �7     �7     �7     �7     �7     �7     �7     8     8  N   -8     |8     �8     �8     �8     �8     �8     �8  $   �8  �   9     �9  [   ^:     �:     �:     �:     �:     �:     �:     �:     �:      ;     ;     ;      ;     @;     F;     Y;     p;     �;     �;     �;     �;     �;     �;     <     /<  
   @<  4   K<  H   �<     �<     �<     �<     =  !    =     B=     S=  i   f=  &   �=  &   �=  %   >  %   D>  %   j>     �>     �>     �>     �>     �>     �>     ?     ?     -?     A?     T?     b?  #   n?     �?     �?  $   �?     �?  �   �?     {@     �@     �@     �@     �@     �@     �@     �@     �@     A     A     *A     HA     dA     wA     �A  �   �A  �   IB     �B     �B     �B     C  !   C     :C  a   �C  1   D  1   ND  �   �D  ?   eE  &   �E  �   �E    yF  e   �G  O   �G  w   AH  ;  �H  9  �I     /K     ?K     QK  "   gK     �K     �K     �K  8   �K     �K  
   �K     �K  	   L     L     L     L  	   $L     .L     4L     EL     NL     WL     dL  -   mL  
   �L     �L     �L     �L     �L     �L     �L     �L     �L     �L     �L     �L     M     M     :M     BM     PM     YM     bM     oM     �M     �M  	   �M     �M     �M     �M     �M     �M  
   �M     �M  
   �M     N     N     N     N     +N     0N     5N     :N     ?N     EN     JN     ON     fN     iN     �N     �N     �N     �N  	   �N     8   �   ;       �   �   W          �   �           �       q   �   �      �   M       j       .   �   �           �   g   2   �   U       3      �   �   �   �      �       �   �   O       K   m   �      �      �       %   �   5   �   �      �   �   �   �   S   �   -   o      l       '   P                   L   ,   x   �          �       �   �   �   �           �       �               6   $   v   >   �   �   �   =          �   �       &   �   �   h   �               	       �   T           �   �   �       �   �   �   w   `   �       ^   �           �   �   �   �   �       �   �           {              /   �           �              [       y           �   �       �   �   �   �      B       V   i      G   n         Q   F           �       |             "   �   �   �   Z   �              r   D   �   H       +      9          �   �   �   R   A   e       Y          �   X   0   �   �      �      �   �      �      N       �   �   I   �   u   z   �   �       ?   a   �               !   1   �       �   �   �   #   (      �   )   �   <       �   
           �   �   f       }   �   c           d   �   �   �       7       k       :       �   �   �      �   �   C   �   b   _   t              �   J   4   E   @   �   �   �   p   �   ]   *   ~       �   s   �       �   �           \     Adding a size will create it.  Removing a size will delete it. A glyph name should contain only alphanumerics, periods and underscores
Do you want to use this name in spite of that? Add Base Anchor... Add Entry Anchor... Add Exit Anchor... Add Mark Anchor... Additional arguments for autotrace program: All All Glyphs All characters in the value must be in ASCII Anchor Control Anchor Control for class %.100s in glyph %.100s as %.20s Anchor Control... Are you sure you want to replace Å?
The ring will not join to the A. Auto Width AutoWidth failure on %s
 Automatic Autotracing... BDF Info... Bad Name Bad Number Bad default baseline Base Glyph Base Lig Bases Brāhmī CJK Ideographic C_lear C_opy Reference Can't Parallel Can't create temporary directory Can't find autotrace Can't find autotrace program (set AUTOTRACE environment variable) or download from:
  http://sf.net/projects/autotrace/ Can't find mf Can't find mf program -- metafont (set MF environment variable) or download from:
  http://www.tug.org/
  http://www.ctan.org/
It's part of the TeX distribution Can't run mf Color|Choose... Color|Default Component Coordinate along which to space Cor: Corrections must be between -128 and 127 (and should be smaller) Could not read (or perhaps find) mf output file Couldn't open file Couldn't open file %.200s Cu_t Current Glyph Cyrillic Default All Default Baseline Default This Delete Detaching Anchor Point Duplicate Anchor Class E_lement Entries Exits Expor_t... Feature Flip Horizontally Flip Vertically Flip _Horizontally Flip _Vertically Font|_New French Côte d'Ivoire French Réunion From the list below, select the baselines for which you
will provide data. Generate Mac _Family... Glagolitic Glyph _Info... Gothic Gurmukhi Height Horizontal Baselines Horizontal Extents for %c%c%c%c If any of the above baselines are active then you should
specify which one is the default baseline for each script
in the font, and specify how to position glyphs in this
script relative to all active baselines In lookup subtable %.30s you refer to a glyph named %.80s, which is not in the font yet. Was this intentional? In lookup subtable %.30s you replace a glyph with itself. Was this intentional? Language Linear A Linear B Lookup subtable: Ma_x: Mac Mag: Mark Marks Max Max (ascent) MetaFont exited with an error Min Min (descent) Must be a number New Alternate List New Multiple List New O_utline Window New Pair Position New Positioning New _Bitmap Window New _Metrics Window Next _Defined Glyph No Change No Class No Kern Pairs No kerning pairs found in %.200s Non-existant glyph Not ASCII Not enough lines Nothing to trace Only kern glyphs closer Out of Range Pixel Sizes: Please identify a glyph by name, and FontForge will add an anchor to that glyph. Point sizes on a 100 dpi screen Point sizes on a 120 dpi screen Point sizes on a 72 dpi screen Point sizes on a 75 dpi screen Point sizes on a 96 dpi screen PostScript® Pr_eferences... Prev Defined Gl_yph Property|New... Provide a glyph name Recalculate Bitmaps Recen_t Regenerate _Bitmap Glyphs... Remo_ve Undoes Remove This Glyph Replace Å Rotate 180° Rotate 90° CCW Rotate 90° CW Rotate _180° Rotate _90° CCW S_ave as... Script '%c%c%c%c' claims baseline '%c%c%c%c' as its default, but that baseline is not currently active. Script|Arabic Script|Aramaic Script|Armenian Script|Bengali Script|Georgian Script|Greek Script|Hebrew Script|Latin Select _All Selected Glyphs Separation Set Feature Extents Set Vertical Width... Set Width... Set _Vertical Width... Set _Width... Set the minimum and maximum values by which
the glyphs in this script extend below and
above the baseline when modified by a feature. Set the minimum and maximum values by which
the glyphs in this script extend below and
above the baseline. This may vary by language Skew Skew Ratio Skew... Space Regions Strike Information for %.90s The AFM file contains metrics information that many word-processors will read when using a PostScript® font. The PFM file contains information Windows needs to install a PostScript® font. The X coordinate of the anchor point in this glyph The Y coordinate of the anchor point in this glyph The glyph is rasterized at the size above, but it
may be difficult to see the alignment errors
that can happen at small pixelsizes. This allows
you to expand each pixel to show potential problems
better. The glyph, %.80s, already contains an anchor in this class, %.80s. The glyph, %.80s, is not in the font The size at which the current glyph is rasterized.
For small pixelsize you may want to use the magnification
factor below to get a clearer view. The size at which the current glyph is rasterized.
For small pixelsize you may want to use the magnification
factor below to get a clearer view.

The pulldown list contains the pixelsizes at which there
are device table corrections. The tfm and enc files contain information TeX needs to install a PostScript® font. These two lines share a common endpoint, I can't make them parallel This anchor was attached to point %d, but that's not a point I can move. I'm detaching the anchor from the point. This is the number of pixels by which the anchor
should be moved horizontally when the glyph is
rasterized at the above size.  This information
is part of the device table for this anchor.
Device tables are particularly important at small
pixelsizes where rounding errors will have a
proportionally greater effect. This is the number of pixels by which the anchor
should be moved vertically when the glyph is
rasterized at the above size.  This information
is part of the device table for this anchor.
Device tables are particularly important at small
pixelsizes where rounding errors will have a
proportionally greater effect. Unicode _Value: Use FreeType Vertical Baselines Vertical Extents for %c%c%c%c Warnings Win X You must specify a glyph name for subtable %s Z_oom out Zoom _in _Bigger Pixel Size _Cancel _Close _Copy _Edit _File _Fit _Generate Fonts... _Goto _Height: _Import... _Layers _Maximum distance between points in a region _Metrics _Min: _Next Glyph _No _OK _Open _Palettes _Paste _Prev Glyph _Quit _Redo _Revert File _Right→ _Rotate 90° CW _Save _Separation: _Shades _Size: _Skew... _Smaller Pixel Size _Tools _Transformations _Undo _Up↑ _View _Window _X _X Resource Editor... _X near¹ _Y _Y near¹ _Yes base cursive entry cursive exit icfb icft ideo idtp mark math romn writing system|Script ° ° Clockwise ° Withershins ΤεΧ ΤεΧ Base (8r) ←_Left ↓_Down Project-Id-Version: fontforge
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2012-12-28 15:35+0000
PO-Revision-Date: 2014-04-24 13:11+0000
Last-Translator: Felipe_Kley <fkley.cnpa@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 16:23+0000
X-Generator: Launchpad (build 18115)
  Ao adicionar um tamanho isto será criado  Eliminar um tamanho irá apagá-lo O nome do glifo pode conter caracteres alfanuméricos, pontos e traços subscritos.
Quer usar este nome em vez daquele ? Adicionar Âncora Base... Adicionar Âncora de Entrada... Adicionar Âncora de Saida... Adicionar Âncora de Marcação... Argumentos adicionais para o autotrace: Todos Todos os símbolos Todos os caracteres do valor devem estar em ASCII Controlo de Âncora Controlo de Âncora para a classe %.100s no hieróglifo %.100s como %.20s Controlo de Âncora... Tem a certeza que deseja substituir Å?
O anel não se juntará ao A. Auto-largura Falha da auto-largura em %s
 Automático Auto-traçando... Informação BDF... Nome Errado Número incorrecto Linha base predefinida inviável Glifo Base Lig Base Bases Brāhmī CJK Ideográfico _Limpar C_opiar Referência Não é possível colocar em paralelo Não é possível criar um directório temporário Não é possível encontrar o autotrace Não é possível encontrar o programa autotrace (defina a variável de ambiente AUTOTRACE) ou transfira-o a partir de:
  http://sf.net/projects/autotrace/ Não é possível encontrar mf Não é possível encontrar o programa mf -- metafont (configure a variável de ambiente MF) ou transfira-o a partir de:
  http://www.tug.org/
  http://www.ctan.org/
É parte da distribuição de TeX Não é possível executar mf Cor|Escolher... Cor|Padrão Componente Coordenar em conjunto quais a espaçar Corr: As correcções devem estar entre -128 e 127 (e devem ser inferiores) Não foi possível ler (ou talves encontrar) o ficheiro de saída de mf Não foi possível abrir o ficheiro Não foi possível abrir o ficheiro %.200s Cor_tar Símbolo actual Cirílico Por omissão, todos Linha base predefinida Por omissão, este Eliminar Soltando o ponto de Âncora Duplicar Classe de Âncora E_lemento Entradas Saídas Expor_tar... Funcionalidade Inverter na horizontal Inverter na vertical Inverter na _Horizontal Inverter na _Vertical Nova|_Fonte Francês Costa do Marfim Reunião Francesa Da lista em baixo, seleccione as linhas base para as quais
ira fornecer dados. Gerar Mac _Família... Glagolítico _Info do Glifo... Gótico Gurmukhi Altura Linhas base horizontais Extensões Horizontais para %c%c%c%c Se alguma das linhas bases em cima estiverem activas 
então terá de especificar a que corresponde a cada escrita
na fonte e especificar como posicionar os glifos nesta escrita
em relação a todas as linhas base. Na sub-tabela de referência %.30s referiu-se a um nome de glifo %.80s, o qual ainda não está na fonte. Isto foi intencional? Na sub-tabela de referência %.30s substituiu um glifo por ele mesmo. Isto foi intencional? Idioma Linear A Linear B Sub-tabela de procura: Ma_x: Mac Amp: Marcar Marcas Máx. Máx. (ascendente) O Metafont terminou com um erro Mín. Min. (descendente) Tem que ser um número Nova Lista Alternativa Nova Lista Múltipla Nova Janela de Contorno Nova posição de par Novo Posicionamento Nova Janela de Mapas de _Bits Nova Janela de _Métricas Glifo _Definido Seguinte Sem alterações Sem Classe Não existem pares para o espaçamento de caracteres Não foram encontrados pares para o espaçamento de caracteres em %.200s Símbolo não existente Não é ASCII Não há linhas suficientes Nada para traçar... Apenas espçar glifos mais juntos Fora dos limites Tamanho em Pixeis: Por favor identifique um símbolo pelo seu nome e o FontForge adicionará uma âncora para esse símbolo. Tamanho em pontos num ecrã de 100 dpi Tamanho em pontos num ecrã de 120 dpi Tamanho em pontos num ecrã de 72 dpi Tamanho em pontos num ecrã de 75 dpi Tamanho em pontos num ecrã de 96 dpi PostScript® Pr_eferências Glifo Definido Anterior Property|Nova... Fornecer um nome de símbolo Recalcular Bitmaps Recen_te Regenerar Glifos de _Bitmap... Remo_ver Dezfazeres Remover Este Glifo Substituir Å Rodar 180º Rodar 90º no sentido anti-horário Rodar 90º no sentido horário Rodar _180º Rodar _90º no sentido anti-horário Gu_ardar como... A escrita '%c%c%c%c' alega que a linha base '%c%c%c%c' é a predefinição dela, porém esta linha base não está actualmente activa. Script|Arábico Script|Aramaico Script|Arménio Script|Bengali Script|Georgiano Script|Grego Script|Hebraico Script|Latim Selecionar _Tudo Símbolos seleccionados Separação Definir a função Extensões Definir Largura Vertical... Definir Largura... Definir Largura _Vertical... Definir _Largura... Definir os valores mínimo e o máximo pelos quais
os glifos nesta escrita se estendem a cima e a 
baixo da linha base quando modificados por uma
fucnionalidade Definir os valores minimo e o máximo pelos quais
os glifos neste escrita se estendem a cima e a 
baixo da linha base. Isto varia com o Idioma Enviesar Rácio de Enviesamento Enviesar... Regiões de Espaço Descubrir informação para %.90s O ficheiro AFM contém informação de medidas que muitos processadores de texto vão ler quando usarem uma fonte PostScript®. O ficheiro PFM contém informação que o Windows necessita para instalar uma fonte PostScript®. A coordenada X do ponto da âncora neste símbolo A coordenada Y do ponto de âncora neste símbolo O glifo é rasterizado pelo tamanho em cima, mas
pode ser difícil ver os erros de  alinhamento que 
podem ocorrem em tamanhos de pixel pequenos.
Isto permite-lhe expandir cada pixel e mostrar-lhe
melhor os potenciais problemas. O símbolo, %.80s, já contém uma âncora nesta classe, %.80s. O símbolo, %.80s, não está na fonte O tamanho segundo o qual o o glifo actual será
rasterizado. Para tamanhos de pixel pequenos 
pode quer usar o factor de magnificação em 
baixo para uma vista mais clara. O tamanho segundo o qual o o glifo actual será
rasterizado. Para tamanhos de pixel pequenos 
pode quer usar o factor de magnificação em 
baixo para uma vista mais clara.

A lista descendente contém os tamanhos de pixel
que têm correcções com a Tabela de dispositivo. Os ficheiros tfm e enc contêm informação que o Tex necessita para instalar uma fonte PostScript®. Estas duas linhas têm um extremo comum, não é possível torná-las paralelas Esta âncora estava fixada no ponto %d, mas este não é um ponto que eu possa mover. Vou soltar a âncora deste ponto. Este é o nº de pixels pelo qual a âncora deve 
ser movida horizontalmente quando o glifo é 
rasterizado com o tamanho acima. Esta Info
é parte da Tabela de dispositivo de âncora. 
Estas Tabelas são importantes nos tamanhos
de pixel mais pequenos onde o arredondar 
pode ter um efeito proporcionalmente maior. Este é o nº de pixels pelo qual a âncora deve 
ser movida verticalmente quando o glifo é 
rasterizado com o tamanho acima. Esta Info
é parte da Tabela de dispositivo de âncora. 
Estas Tabelas são importantes nos tamanhos
de pixel mais pequenos onde o arredondar 
pode ter um efeito proporcionalmente maior. _Valor Unicode: Utilizar FreeType Linhas base verticais Extensões Verticais para %c%c%c%c Avisos Win X Tem de especificar um nome de glifo para a sub-tabela %s Afastar Aprox_imar _Aumentar o Tamanho dos Pixeis _Cancelar _Fechar _Copiar _Editar _Ficheiro Caber _Gerar Fontes... _Ir Para _Altura: _Importar... _Camadas Distância _Máxima entre pontos numa região _Métricas _Min: Glifo Segui_nte _Não _OK _Abrir _Paletas _Colar Glifo Anterior _Sair _Refazer _Reverter ficheiro _Direita→ _Rodar 90º no sentido horário _Gravar _Separação: _Sombras _Tamanho Envie_sar... _Diminuir o Tamanho dos Pixeis _Ferramentas _Transformações Desfa_zer _Cima↑ _Ver _Janelas _X Editor de Recurso _X... _X perto¹ _Y _Y perto¹ _Sim base entrada cursiva saída cursiva icfb icft ideo idtp marca mate romn writing system|Escrita ° ° No sentido horário ° No sentido anti-horário ΤεΧ Base ΤεΧ (8r) ←_Esquerda ↓_Baixo 