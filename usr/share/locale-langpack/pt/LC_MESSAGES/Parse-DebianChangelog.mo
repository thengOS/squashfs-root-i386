��    '      T  5   �      `  ,   a  '   �  (   �  	   �  z   �     d     w     �     �     �     �     	     !     ;     R  !   i     �  $   �     �  -   �  "     "   6  #   Y     }  &   �     �     �     �  (        .     J     i       +   �     �  .   �  ,   �  A   ,  �  n  1   7
  0   i
  0   �
  	   �
  ^   �
     4     H  (   f     �     �     �  *   �  "     $   6  #   [  ;     (   �  $   �     	  ?   )  $   i  /   �  0   �     �  3     #   C     g     �  .   �  8   �  '         (     C  4   c     �  ,   �  )   �  Z                                            
                    %                          !                 '                              	   $                        &   "         #    'since' option specifies most recent version 'until' option specifies oldest version Copyright (C) 2005 by Frank Lichtenheld
 FATAL: %s This is free software; see the GNU General Public Licence version 2 or later for copying conditions. There is NO warranty. WARN: %s(l%s): %s
 WARN: %s(l%s): %s
LINE: %s
 bad key-value after `;': `%s' badly formatted heading line badly formatted trailer line badly formatted urgency value can't close file %s: %s can't load IO::String: %s can't lock file %s: %s can't open file %s: %s changelog format %s not supported couldn't parse date %s fatal error occured while parsing %s field %s has blank lines >%s< field %s has newline then non whitespace >%s< field %s has trailing newline >%s< found blank line where expected %s found change data where expected %s found eof where expected %s found start of entry where expected %s found trailer where expected %s handle is not open ignored option -L more than one file specified (%s and %s) no changelog file specified output format %s not supported repeated key-value %s too many arguments unknown key-value key %s - copying to XS-%s unrecognised line you can only specify one of 'from' and 'since' you can only specify one of 'to' and 'until' you can't combine 'count' or 'offset' with any other range option Project-Id-Version: libparse-debianchangelog-perl
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-08-25 20:38+0000
PO-Revision-Date: 2014-07-26 18:38+0000
Last-Translator: Alexandre Fidalgo <alexandremagnos15@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:52+0000
X-Generator: Launchpad (build 18115)
 opção 'since' especifica a versão mais recente opção 'until' especifica a versão mais antiga Direitos de Autor (C) 2005 by Frank Lichtenheld
 FATAL: %s Este é um software livre; veja a GPLv2 ou posterior para condições de cópia. Sem garantia. AVISO: %s(l%s): %s
 AVISO: %s(l%s): %s
LINHA: %s
 valor de chave inválido após `;': `%s' linha de titulo mal formatada linha de trailer má formatada valor urgente mal formatado não é possível fechar o ficheiro %s: %s não pode carregar a sequência %s não pode bloquear o ficheiro %s: %s não consegue abrir ficheiro %s: %s o formato do registo de alterações de %s não e suportado não foi possível análisar os dados %s ocorreu um erro fatal ao analizar %s campo %s tem linhas vazias >%s< campo %s tem uma nova linha e depois um espaço não vazio >%s< campo %s tem uma linha trailing >%s< encontrada linha em branco onde era esperado %s encontrado alteração de dados onde esperado %s encontrado eof onde esperado %s encontrado início de entrada onde era esperado: %s encontrado trailer onde esperado %s identificador não é aberto opção -L ignorada mais do que um ficheiro especificado (%s e %s) nenhum ficheiro de registo de alteração foi encontrado o formato de saida %s não é suportado valor de chave repetido %s número excessivo de argumentos valor de chave desconhecido %s - copiando para XS-%s linha irreconhecível apenas pode especificar um de 'fo' e 'desde' apenas pode especificar um 'to' e 'until' não pode continuar a combinar 'count' ou 'offset' com qualquer outra opção de intervalo 