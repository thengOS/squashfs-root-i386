��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  O   \  >   �  P   �  ,   <  1   i     �     �  )   �  \   �     Y               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accountsservice
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2013-01-17 09:48-0500
PO-Revision-Date: 2012-06-04 12:36+0000
Last-Translator: Mykas0 <Mykas0@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:01+0000
X-Generator: Launchpad (build 18115)
 Autenticação é necessária para alterar a configuração do ecrã de entrada Autenticação é necessária para alterar dados de utilizador Autenticação é necessária para alterar os seus próprios dados de utilizador Alterar a configuração do ecrã de entrada Alterar a sua própria informação de utilizador Ativar código de depuração Gerir contas de utilizar Apresentar informação de versão e sair Providencia interfaces D-Bus para consultar e manipular
informação de conta de utilizador. Substituir instância existente 