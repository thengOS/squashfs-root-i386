��    3     �	  �  L      �  9   �  J   �  '   6  (   ^  $   �  .   �  5   �  ?     6   Q  :   �  :   �     �  )        >  '   \  5   �  -   �  9   �  8   "  )   [  &   �  "   �  "   �     �            .   (  "   W     z  .   �  1   �  #   �       I     /   _  &   �     �     �  	   �     �  &   �        ,   3   *   `   -   �      �   $   �   !   �       !     9!  +   S!     !     �!  )   �!  C   �!  >   �!  "   9"     \"  0   d"  1   �"  2   �"  $   �"     #     8#     O#     n#     v#     |#  !   �#  !   �#  !   �#  !   �#  '   $  
   3$  
   >$  
   I$  
   T$  
   _$  
   j$  
   u$  	   �$  =   �$     �$  4   �$     	%     &%     ,%     ;%     I%  !   a%  %   �%  �   �%     +&     8&     S&     Z&     v&     |&  	   �&     �&  	   �&     �&     �&     �&     �&     '     4'     N'     e'     �'  
   �'     �'     �'     �'     �'     �'  !   �'     (     )(     /(     1(     G(  $   K(     p(     r(     �(     �(     �(     �(     �(     �(     �(     �(  %   �(  "   )     @)     N)     W)     n)     �)  +   �)  &   �)  +   �)     *     *  )    *  6   J*  "   �*     �*  	   �*  
   �*     �*  	   �*     �*     +      !+     B+  -   ^+  &   �+     �+     �+     �+     �+  	   ,  
   ,     ,  
   3,     >,     X,  (   d,     �,     �,     �,     �,  ,   �,     �,  O   -  #   V-  $   z-     �-  5   �-  -   �-  6   #.     Z.     q.  &   �.  6   �.     �.  '   /  )   )/  /   S/  1   �/     �/  )   �/  +   �/  1   *0  4   \0  )   �0     �0     �0  !   �0     1  )   "1     L1     e1  	  m1     �:  �   �:     r;     �;     �;     �;     �;     �;  !   �;  %   �;  %   <  !   7<  =   Y<  #   �<  !   �<     �<     �<     �<  +   =     A=  -   O=  2   }=     �=     �=     �=  !   �=     >     0>     E>     S>  D   i>  $   �>  $   �>     �>     ?     ,?     I?     d?  4   v?  %   �?     �?     �?  /   �?     -@     6@     D@     Y@     f@     u@     �@     �@     �@     �@     �@     �@     �@     A     .A     @A  	   PA     ZA     bA     wA     �A  
   �A      �A     �A     �A     �A      B  +   B     EB     dB  #   uB     �B     �B  ,   �B     �B     C     C      =C  ?   ^C  J   �C     �C     �C     D     !D      &D     GD     eD     yD     �D     �D     �D     �D     �D  +   �D  !   #E     EE     ]E  �  zE  F   G  J   ^G  )   �G  &   �G     �G  (   H  A   BH  9   �H  @   �H  3   �H  3   3I     gI  1   }I     �I  0   �I  L   �I  /   LJ  C   |J  A   �J  .   K  &   1K  "   XK  "   {K     �K     �K     �K  6   �K  %   L     ;L  .   DL  ;   sL  *   �L     �L  d   �L  L   LM  %   �M  (   �M     �M     �M     �M  /   
N     :N  ,   MN  *   zN  -   �N     �N  1   �N  9   $O     ^O     yO  5   �O     �O     �O  (   �O  T   P  J   \P  &   �P     �P  >   �P  P   Q  C   fQ  1   �Q  "   �Q      �Q  2    R     SR     \R     cR  #   jR     �R     �R     �R  $   �R     S     S     "S     +S     4S     =S     FS     OS  C   WS  	   �S  @   �S  #   �S     
T  	   T  	   T     $T     ;T  %   WT  �   }T     U     $U     >U     JU     eU     jU  	   wU     �U  	   �U     �U     �U  #   �U  #   �U     V     ;V     VV     rV     �V     �V     �V  $   �V     �V     �V  "   �V  &    W     GW     cW     iW     kW     ~W  1   �W     �W     �W  $   �W     �W     �W      X     &X     CX  	   EX     OX  +   VX  *   �X     �X     �X  "   �X  (   �X     Y  6   #Y  1   ZY  .   �Y     �Y     �Y  1   �Y  >   �Y  !   ;Z     ]Z     dZ     vZ  )   �Z     �Z  "   �Z  "   �Z  0    [  &   1[  :   X[  .   �[     �[  )   �[  )    \     *\     /\     @\     P\     p\     }\     �\  &   �\     �\     �\     �\     �\  1   ]  $   6]  R   []  #   �]     �]  #   �]  G   ^  7   ]^  L   �^     �^     �^  *   _  E   A_  #   �_  )   �_  ?   �_  I   `  ?   _`  $   �`  8   �`  7   �`  ?   5a  >   ua  8   �a     �a     b  &    b  #   Gb  8   kb     �b     �b  	  �b  "   �k  �   l  5   �l  %    m     &m     *m     /m     @m  %   Em  $   km  $   �m      �m  4   �m  -   n     9n     Yn     `n      xn  .   �n     �n  4   �n  8   	o     Bo     ]o  3   {o  .   �o     �o     �o     p     9p  Y   Xp  2   �p  D   �p  .   *q  #   Yq  "   }q  1   �q      �q  =   �q  +   1r     ]r     yr  -   �r  	   �r     �r  "   �r     �r     s  #   s  (   Bs  $   ks  !   �s     �s     �s  	   �s  "   �s  (   t     .t     Dt  
   Tt  %   _t     �t     �t     �t     �t  %   �t     �t      u     4u     Fu  3   bu  !   �u     �u  '   �u     �u     v  0   *v  "   [v  (   ~v  '   �v  (   �v  L   �v  Y   Ew     �w  '   �w     �w  
   �w  #   �w  !   x     8x  !   Nx  "   px     �x     �x     �x     �x  8   �x  *   !y     Ly     dy        J      '   �   m   �       �   �         )  7           H      �   �       �   �   �               s       *       2              �   �      �   �     �   	   �   �       �   p   �   S   �   a       �      F                       =       �   b       o   �   &  5   #  �   Z       y   }   �   �       "   B   �         �       ,   �   g   '      �   �   �   %     �               !  1          �   �       �       �   �   ,  w   �             4   �   �   u       �   �   �   �         G   �   �   !       9   f   �             2     �   $  &   .   �   /       �   l   )       �   ?          �       �         �   \   v         >   �       �   n             |              X   %   �   �           �   x       �   �   3  "  �   {           -   �      e   �   W       I   U       �   
  c   �   �           �     �   �   �       �             R   z   D        �   �   �   A              �   �       �   i   �           �   �   �   $   �   �   /          *  �   �   C   V   �   �              �          �       �   �       �   �   �   �   �       �   (     K   L   M   N      P   Q   3   �   +                              �     �   �      (      �             <   j          �   
   :   T   [   8   �   �       �          1     ;       .                    �        q   r       @   �      �       �   ^      �   O   �     0  �           �   +      �   d       k   �              �   h          #           �     6           Y     �   	  �   0   _   �   E           ~       �   ]       �   t   �   `         �   �   -  �    
Some of these may not be available on selected hardware
 === PAUSE ===                                                             PAUSE command ignored (no hw support)
          please, try the plug plugin %s
      -d,--disconnect     disconnect
      -e,--exclusive      exclusive connection
      -i,--input          list input (readable) ports
      -l,--list           list current connections of each port
      -o,--output         list output (writable) ports
      -r,--real #         convert real-time-stamp on queue
      -t,--tick #         convert tick-time-stamp on queue
      -x, --removeall
      sender, receiver = client:port pair
    aconnect -i|-o [-options]
    aconnect [-options] sender receiver
   -d,--dest addr : write to given addr (client:port)
   -i, --info : print certain received events
   -p,--port # : specify TCP port (digit or service name)
   -s,--source addr : read from given addr (client:port)
   -v, --verbose : print verbose messages
   Clemens Ladisch <clemens@ladisch.de>   Copyright (C) 1999 Takashi Iwai
   Jaroslav Kysela <perex@perex.cz>   Subdevice #%i: %s
   Subdevices: %i/%i
   Tim Janik   client mode: aseqnet [-options] server_host
   server mode: aseqnet [-options]
  !clip    * Connection/disconnection between two ports
  * List connected ports (no subscription action)
  * Remove all exported connections
  [%s %s, %s]  can't play WAVE-files with sample %d bits in %d bytes wide (%d channels)  can't play WAVE-files with sample %d bits wide %s is not a mono stream (%d channels)
 %s!!! (at least %.3f ms long)
 %s: %s
 (default) (unplugged) **** List of %s Hardware Devices ****
 + -        Change volume -f cd (16 bit little endian, 44100, stereo)
 -f cdr (16 bit big endian, 44100, stereo)
 -f dat (16 bit little endian, 48000, stereo)
 0-9        Set volume to 0%-90% ; '        Toggle left/right capture < >        Toggle left/right mute Aborted by signal %s...
 Access type not available Access type not available for playback: %s
 All Authors: B          Balance left and right volumes Broken configuration for playback: no configurations available: %s
 Broken configuration for this PCM: no configurations available Buffer size range from %lu to %lu
 CAPTURE Can't recovery from suspend, prepare failed: %s
 Can't recovery from underrun, prepare failed: %s
 Can't use period equal to buffer size (%lu == %lu) Cannot create process ID file %s: %s Cannot open WAV file %s
 Cannot open file "%s". Cannot open mixer device '%s'. Capture Card: Center Channel %2d: Control event : %5d
 Channel %2d: Note Off event: %5d
 Channel %2d: Note On event : %5d
 Channel %2d: Pitchbender   : %5d
 Channel %d doesn't match with hw_parmas Channel 10 Channel 11 Channel 12 Channel 13 Channel 14 Channel 15 Channel 16 Channel 9 Channel numbers don't match between hw_params and channel map Channels %i Channels count (%i) not available for playbacks: %s
 Channels count non available Chip: Connected From Connecting To Connection failed (%s)
 Connection is already subscribed
 Copyright (C) 1999-2000 Takashi Iwai
 Debugging options:
  -g, --no-color          toggle using of colors
  -a, --abstraction=NAME  mixer abstraction level: none/basic Device name: Disconnection failed (%s)
 Done.
 End        Set volume to 0% Error Esc     Exit Esc: Exit F1 ? H  Help F1:  Help F2 /    System information F2:  System information F3      Show playback controls F4      Show capture controls F5      Show all controls F6 S    Select sound card F6:  Select sound card Failed. Restarting stream.  Front Front Left Front Right HW Params of device "%s":
 Help Invalid WAV file %s
 Invalid number of periods %d
 Invalid parameter for -s option.
 Invalid test type %s
 Item: L L       Redraw screen LFE Left    Move to the previous control M M          Toggle mute Max peak (%li samples): 0x%08x  Mono No enough memory
 No subscription is found
 Not a WAV file: %s
 O Off On Page Up/Dn Change volume in big steps Period size range from %lu to %lu
 Periods = %u
 Playback Playback device is %s
 Playback open error: %d,%s
 Playing Playing Creative Labs Channel file '%s'...
 Press F6 to select another sound card. Q W E      Increase left/both/right volumes R Rate %d Hz,  Rate %iHz not available for playback: %s
 Rate doesn't match (requested %iHz, get %iHz, err %d)
 Rate set to %iHz (requested %iHz)
 Rear Rear Left Rear Right Recognized sample formats are: Recording Requested buffer time %u us
 Requested period time %u us
 Right   Move to the next control Sample format non available Sample format not available for playback: %s
 Sample rate doesn't match (%d) for %s
 Select File Setting of hwparams failed: %s
 Setting of swparams failed: %s
 Side Side Left Side Right Sine wave rate is %.4fHz
 Sound Card Space      Toggle capture Sparc Audio Sparc Audio doesn't support %s format... Status(DRAINING):
 Status(R/W):
 Status:
 Stereo Stream parameters are %iHz, %s, %i channels
 Suspended. Trying resume.  Suspicious buffer position (%li total): avail = %li, delay = %li, buffer = %li
 Tab     Toggle view mode (F3/F4/F5) The available format shortcuts are:
 The sound device was unplugged. This sound device does not have any capture controls. This sound device does not have any controls. This sound device does not have any playback controls. Time per period = %lf
 Transfer failed: %s
 Try `%s --help' for more information.
 Unable to determine current swparams for playback: %s
 Unable to install hw params: Unable to parse channel map string: %s
 Unable to set avail min for playback: %s
 Unable to set buffer size %lu for playback: %s
 Unable to set buffer time %u us for playback: %s
 Unable to set channel map: %s
 Unable to set hw params for playback: %s
 Unable to set nperiods %u for playback: %s
 Unable to set period time %u us for playback: %s
 Unable to set start threshold mode for playback: %s
 Unable to set sw params for playback: %s
 Undefined channel %d
 Unknown option '%c'
 Unsupported WAV format %d for %s
 Unsupported bit size %d.
 Unsupported sample format bits %d for %s
 Up/Down    Change volume Usage:
 Usage: %s [OPTION]... [FILE]...

-h, --help              help
    --version           print current version
-l, --list-devices      list all soundcards and digital audio devices
-L, --list-pcms         list device names
-D, --device=NAME       select PCM by name
-q, --quiet             quiet mode
-t, --file-type TYPE    file type (voc, wav, raw or au)
-c, --channels=#        channels
-f, --format=FORMAT     sample format (case insensitive)
-r, --rate=#            sample rate
-d, --duration=#        interrupt after # seconds
-M, --mmap              mmap stream
-N, --nonblock          nonblocking mode
-F, --period-time=#     distance between interrupts is # microseconds
-B, --buffer-time=#     buffer duration is # microseconds
    --period-size=#     distance between interrupts is # frames
    --buffer-size=#     buffer duration is # frames
-A, --avail-min=#       min available space for wakeup is # microseconds
-R, --start-delay=#     delay for automatic PCM start is # microseconds 
                        (relative to buffer size if <= 0)
-T, --stop-delay=#      delay for automatic PCM stop is # microseconds from xrun
-v, --verbose           show PCM structure and setup (accumulative)
-V, --vumeter=TYPE      enable VU meter (TYPE: mono or stereo)
-I, --separate-channels one file for each channel
-i, --interactive       allow interactive operation from stdin
-m, --chmap=ch1,ch2,..  Give the channel map to override or follow
    --disable-resample  disable automatic rate resample
    --disable-channels  disable automatic channel conversions
    --disable-format    disable automatic format conversions
    --disable-softvol   disable software volume control (softvol)
    --test-position     test ring buffer position
    --test-coef=#       test coefficient for ring buffer position (default 8)
                        expression for validation is: coef * (buffer_size / 2)
    --test-nowait       do not wait for ring buffer - eats whole CPU
    --max-file-time=#   start another output file when the old file has recorded
                        for this many seconds
    --process-id-file   write the process ID here
    --use-strftime      apply the strftime facility to the output file name
    --dump-hw-params    dump hw_params of the device
    --fatal-errors      treat all errors as fatal
 Usage: alsamixer [options] Useful options:
  -h, --help              this help
  -c, --card=NUMBER       sound card number or id
  -D, --device=NAME       mixer device name
  -V, --view=MODE         starting view mode: playback/capture/all Using 16 octaves of pink noise
 Using max buffer size %lu
 VOC View: WAV file(s)
 WAVE Warning: format is changed to %s
 Warning: format is changed to MU_LAW
 Warning: format is changed to S16_BE
 Warning: format is changed to U8
 Warning: rate is not accurate (requested = %iHz, got = %iHz)
 Warning: unable to get channel map
 Wave doesn't support %s format... Woofer Write error: %d,%s
 You need to specify %d files Z X C      Decrease left/both/right volumes accepted[%d]
 aconnect - ALSA sequencer connection manager
 aseqnet - network client/server on ALSA sequencer
 audio open error: %s bad speed value %i buffer to small, could not use
 can't allocate buffer for silence can't get address %s
 can't get client id
 can't malloc
 can't open sequencer
 can't play WAVE-file format 0x%04x which is not PCM or FLOAT encoded can't play WAVE-files with %d tracks can't play loops; %s isn't seekable
 can't play packed .voc files can't set client info
 cannot enumerate sound cards cannot load mixer controls cannot open mixer capture stream format change? attempting recover...
 card %i: %s [%s], device %i: %s [%s]
 client %d: '%s' [type=%s]
 closing files..
 command should be named either arecord or aplay dB gain: disconnected
 enter device name... fatal %s: %s info error: %s invalid card index: %s
 invalid destination address %s
 invalid sender address %s
 invalid source address %s
 kernel malloc error mute no soundcards found... nonblock setting error: %s not enough memory ok.. connected
 options:
 overrun pause push error: %s pause release error: %s raw data read error read error (called from line %i) read error: %s read/write error, state = %s readv error: %s sequencer opened: %d:%d
 service '%s' is not found in /etc/services
 snd_pcm_mmap_begin problem: %s status error: %s stdin O_NONBLOCK flag setup failed
 suspend: prepare error: %s too many connections!
 try `alsamixer --help' for more information
 unable to install sw params: underrun unknown abstraction level: %s
 unknown blocktype %d. terminate. unknown length of 'fmt ' chunk (read %u, should be %u at least) unknown length of extensible 'fmt ' chunk (read %u, should be %u at least) unknown option: %c
 unrecognized file format %s usage:
 user value %i for channels is invalid voc_pcm_flush - silence error voc_pcm_flush error was set buffer_size = %lu
 was set period_size = %lu
 write error write error: %s writev error: %s wrong extended format '%s' wrong format tag in extensible 'fmt ' chunk xrun(DRAINING): prepare error: %s xrun: prepare error: %s xrun_recovery failed: %d,%s
 Project-Id-Version: alsa-utils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-10-27 17:34+0100
PO-Revision-Date: 2010-10-08 23:39+0000
Last-Translator: Pedro Flores <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 15:56+0000
X-Generator: Launchpad (build 18115)
 
Alguns destes podem não estar disponíveis no hardware seleccionado
 === PAUSA ===                                                             PAUSA comando ignorado (sem suporte hw)
          por favor, tente o plugin %s
      -d,--disconnect desligar
      -e,--exclusive ligação exclusiva
      -i,--input listar portas de entrada (passíveis de leitura)
      -l,--list listar ligações actuais para cada porta
      -o,--output listar portas de saida (passíveis de escrita)
      -r,--real # converter real-time-stamp na fila
      -t,--tick # converter tick-time-stamp na fila
      -x, --removeall
      remetente, detinatário = par cliente:porta
    aconnect -i|-o [-opções]
    aconnect [-opções] remetente destinatário
   -d,--dest endereço : escrever para o endereço fornecido (cliente:porta)
   -i, --info : exibir certos eventos recebidos
   -p,--port # : especificar porta TCP (digite ou nome do serviço)
   -s, --source endereço : ler do endereço dado (cliente:porta)
   -v, --verbose : exibir mensagens detalhadas
   Clemens Ladisch <clemens@ladisch.de>   Copyright (C) 1999 Takashi Iwai
   Jaroslav Kysela <perex@perex.cz>   Subdispositivo #%i: %s
   Subdispositivos: %i/%i
   Tim Janik   modo cliente: aseqnet [-opções] máquina_servidor
   modo servidor: aseqnet [-opções]
  !clip    * Conecção/desconecção entre duas portas
  * Listar portas ligadas (nenhuma acção de subscrição)
  * Remover todas as ligações exportadas
  [%s %s, %s]  incapaz de reproduzir ficheiros WAVE com %d bits de amostragem em %d bytes de extensão (%d canais)  incapaz de reproduzir ficheiros WAVE com %d bits de amostragem de extensão %s não é um fluxo mono (%d canais)
 %s!!! (pelo menos %.3f ms de duração)
 %s: %s
 (predefinido) (desligado) **** Lista de Dispositivos de Hardware %s ****
 + - Mudar o volume -f cd (16 bit little endian, 44100, stereo)
 -f cdr (16 bit big endian, 44100, stereo)
 -f dat (16 bit little endian, 48000, stereo)
 0-9 Definir volume para 0%-90% ; '        Alterna entre captura esquerda/direita < >        Alterna entre silenciar canal esquerdo/direito Abortado pelo sinal %s...
 Tipo de acesso não disponível Tipo de acesso não disponivel para reprodução: %s
 Todos Autores: B Equilibro do volume esquerdo e direito Configuração para reprodução danificada: nenhuma configuração disponível: %s
 Configuração para este PCM danificada: nenhuma configuração disponivel Tamanho do buffer vai desde %lu a %lu
 CAPTURA Incapaz de recuperar da suspensão, a preparação falhou: %s
 Impossível recuperar da falha por insuficiência, a preparação falhou em: %s
 Incapaz de utilizar periodo igual ao tamanho de buffer (%lu == %lu) Não é possível criar o processo ID file %s: %s Impossível abrir ficheiro WAV %s
 Impossível abrir ficheiro "%s". Não foi possível abrir o dispositivo mixer '%s'. Capturar Placa: Centro Canal %2d: Evento de controlo: %5d
 Canal %2d: Note Off event: %5d
 Canal %2d: Note On event : %5d
 Canal %2d: Pitchbender: %5d
 Canal %d não coincide com hw_parmas Canal 10 Canal 11 Canal 12 Canal 13 Canal 14 Canal 15 Canal 16 Canal 9 Número de canais não coincidem entre hw_params e o mapa de canais Canais %i Contagem de canais (%i) não disponível para reproduções: %s
 Contagem de canais não disponível Chip: Ligado De A Ligar A Conexão falhada (%s)
 Conexão já foi subscrita
 Copyright (C) 1999-2000 Takashi Iwai
 Opções de depuração:
  -g, --no-color alternar utilização de cores
  -a, --abstraction=NAME abstração do nível do misturador: none/basic Nome do dispositivo: Desconexão falhada (%s)
 Terminado.
 Fim Definir volume para 0% Erro Esc     Sair Esc: Sair F1 ? H  Ajuda F1: Ajuda F2 /    Informação de sistema F2: Informação do Sistema F3 Mostrar controlo de reprodução F4 Mostrar controlo de capturação F5 Mostrar todos os controlos F6 S Escolher placa de som F6: Selecionar placa de som Falhado. A reiniciar o fluxo.  Frente Frente Esquerda Frente Direita Parâmetros HW do dispositivo "%s":
 Ajuda Ficheiro WAV inválido %s
 Número de períodos inválido %d
 Parâmetro inválido para opção -s.
 Tipo de teste inválido %s
 Item: E L Redesenhar ecrã LFE Deslizar para a esquerda para o controlo anterior S M Aciona Mute Pico máximo (%li amostras): 0x%08x  Mono Memória insuficiente
 Nenhuma subscrição encontrada
 Não é um ficheiro WAV: %s
 O Desligado Ligado Página Up/Dn Muda volume em grandes passos Intervalo de período vai desde %lu a %lu
 Períodos = %u
 Reprodução Dispositivo de reprodução é %s
 Erro de abertura de reprodução: %d,%s
 A Reproduzir A reproduzir ficheiro de Canal Creative Labs '%s' ...
 Pressione F6 para seleccionar outra palca de som. Q W E aumentam o volume esquerdo/ambos/direito D Taxa %d Hz,  Taxa %iHz não disponível para reprodução: %s
 Taxa não corresponde (solicitada %iHz, obtida %iHz, erro %d)
 Taxa definida %iHz (pedida %iHz)
 Atrás Traseira Esquerda Traseira Direita Os formatos da amostra reconhecidos são: A gravar Tempo de buffer requisitado %u US
 Tempo de período requerido %u US
 Deslizar para a direita para o próximo controlo Formato de amostragem não disponível Formato de amostra não disponível para reprodução: %s
 Taxa de amostragem não coincide (%d) para %s
 Selecionar Ficheiro Definição de parâmetros hw falhou: %s
 Definição de parâmetros sw falhou: %s
 Lado Lateral Esquerda Lateral Direita Taxa da onda de seno é %.4fHz
 Placa de Som Barra de Espaço ativa captura Audio Sparc Sparc Audio não suporta formato %s... Estado(A ESVAZIAR):
 Estado(R/W):
 Estado:
 Estéreo Os parâmetros de fluxo são %iHz, %s, %i canais
 Suspendido. A tentar recuperação.  Posição do buffer suspeita (%li total): disp. = %li, atraso = %li, buffer = %li
 Tab Alternar modo de ver (F3/F4/F5) Os atalhos disponíveis são:
 O dispositivo de som foi desligado. Este dispositivo de som não tem qualquer tipo de controlos de captura. Este dispositivo de som não tem quaisqueres controlos. Este dispositivo de som não tem qualquer tipo de controlos de reprodução. Tempo por periodo = %lf
 Falhou transferência: %s
 Tente '%s --help' para mais informação.
 Incapaz de determinar os actuais parametros sw para reprodução: %s
 Impossivel instalar parâmetros hw: Incapaz de analisar o mapa de canais: %s
 Impossível ajustar minutos disponíveis para reprodução: %s
 Não foi possível definir o tamanho do buffer %lu para reprodução: %s
 Impossivel definir tempo de buffer %u US para reprodução: %s
 Incapaz de definir o canal mapa: %s
 Incapaz de definir parâmetros hw para reprodução: %s
 Impossivel definir nperíodos %u para reprodução: %s
 Impossivel definir perodo de tempo %u US para reprodução: %s
 Impossível definir modo limite inicial para reprodução: %s
 Incapaz de definir parâmetros sw para reprodução: %s
 Canal não definido %d
 Opção desconhecida '%c'
 Formato WAV não suportado %d para %s
 Tamnaho de bits não suportado %d.
 Bits do formato de amostragem %d não suportado para %s
 Cima/Baixo para mudar o volume Uso:
 Usage: %s [OPTION]... [FILE]...

-h, --help              help
    --version           print current version
-l, --list-devices      list all soundcards and digital audio devices
-L, --list-pcms         list device names
-D, --device=NAME       select PCM by name
-q, --quiet             quiet mode
-t, --file-type TYPE    file type (voc, wav, raw or au)
-c, --channels=#        channels
-f, --format=FORMAT     sample format (case insensitive)
-r, --rate=#            sample rate
-d, --duration=#        interrupt after # seconds
-M, --mmap              mmap stream
-N, --nonblock          nonblocking mode
-F, --period-time=#     distance between interrupts is # microseconds
-B, --buffer-time=#     buffer duration is # microseconds
    --period-size=#     distance between interrupts is # frames
    --buffer-size=#     buffer duration is # frames
-A, --avail-min=#       min available space for wakeup is # microseconds
-R, --start-delay=#     delay for automatic PCM start is # microseconds 
                        (relative to buffer size if <= 0)
-T, --stop-delay=#      delay for automatic PCM stop is # microseconds from xrun
-v, --verbose           show PCM structure and setup (accumulative)
-V, --vumeter=TYPE      enable VU meter (TYPE: mono or stereo)
-I, --separate-channels one file for each channel
-i, --interactive       allow interactive operation from stdin
-m, --chmap=ch1,ch2,..  Give the channel map to override or follow
    --disable-resample  disable automatic rate resample
    --disable-channels  disable automatic channel conversions
    --disable-format    disable automatic format conversions
    --disable-softvol   disable software volume control (softvol)
    --test-position     test ring buffer position
    --test-coef=#       test coefficient for ring buffer position (default 8)
                        expression for validation is: coef * (buffer_size / 2)
    --test-nowait       do not wait for ring buffer - eats whole CPU
    --max-file-time=#   start another output file when the old file has recorded
                        for this many seconds
    --process-id-file   write the process ID here
    --use-strftime      apply the strftime facility to the output file name
    --dump-hw-params    dump hw_params of the device
    --fatal-errors      treat all errors as fatal
 Utilização: alsamixer [opções] Opções úteis:
  -h, --help Ajuda
  -c, --card=NUMBER placa de som número ou id
  -D, --device=NAME nome dispositivo misturador
  -C, --view=MODE inicia modo de visualização: playback/capture/all A usar 16 oitavas de ruído cor-de-rosa (pink noise)
 Usando tamanho máximo de buffer %lu
 VOC Ver: Ficheiro(s) WAV
 WAVE Atenção: formato é mudado para %s
 Aviso: formato alterado para MU_LAW
 Aviso: formato alterado para S16_BE
 Aviso: formato alterado para U8
 Aviso: taxa inexacta (pedida = %iHz, obtida = %iHz)
 Atenção: incapaz de obter o mapa de canais
 Wave não suporta formato %s... Woofer Erro de escrita: %d,%s
 Tem que especificar %d ficheiros Z X C Diminuem o volume esquerdo/ambos/direito aceite[%d]
 aconnect - gestor de ligação de sequenciador ALSA
 aseqnet - cliente/servidor de rede em sequenciador ALSA
 erro de abertura audio: %s valor de velocidade %i errado o buffer é demasiado pequeno, não pode ser usado
 não é possível alocar buffer para silêncio incapaz de obter endereço %s
 incapaz de obter id do cliente
 incapaz de alocar memória
 incapaz de abrir sequenciador
 incapaz de reproduzir o ficheiro WAVE 0x%04x que não está em codificação PCM ou FLOAT incapaz de reproduzir ficheiros WAVE com %d pistas não é possível reproduzir iterações; %s não são procuráveis
 impossível reproduzir arquivos .voc agrupados incapaz de definir info do cliente
 impossível enumerar placas de som não foi possível carregar os controlos do mixer não foi possível abrir o mixer capturar mudança no formato do fluxo? a tentar recuperar...
 placa %i: %s [%s], dispositivo %i: %s [%s]
 cliente %d: '%s' [tipo=%s]
 a fechar ficheiros..
 o comando dever-se-ia chamar arecord ou aplay ganho dB: desconectado
 introduza o nome do dispositivo... fatal %s: %s erro de informação: %s indexação da placa inválida: %s
 endereço %s de destinatário inválido
 endereço %s de remetente inválido
 endereço de origem inválido %s
 kernel erro de alocação de memória silenciar nenhuma placa de som encontrada... erro de configuração de não-bloco: %s memória insuficiente ok.. conectado
 opções:
 tempo de duração excedido (overrun) pausa erro de push: %s pausa erro de lançamento: %s Dados em bruto erro de leitura erro de leitura (chamado da linha %i) erro de leitura: %s erro leitura/escrita, estado =%s erro de readv: %s sequenciador aberto: %d:%d
 serviço '%s' não foi encontrado em /etc/services
 snd_pcm_mmap_início problema: %s erro de estado: %s stdin O_NONBLOCK configuração falhou
 suspendido: a preparar erro: %s demasiadas conexões!
 Tente `alsamixer --help' para mais informação
 incapaz de instalar parametros sw: tempo de duração inatingido (underrun) Nível de abstração desconhecido: %s
 tipo de bloco desconhecido %d. terminar. duração desconhecida do pedaço 'fmt ' (leu %u, deveria ser pelo menos %u) duração desconhecida do pedaço extensível 'fmt ' (leu %u , deveria ser pelo menos %u) opção desconhecida: %c
 formato %s do ficheiro não reconhecido uso:
 utilizador o valor %i para canais é inválido voc_pcm_flush - erro de silêncio erro de voc_pcm_flush foi definido o buffer_size = %lu
 foi definido o period_size  = %lu
 erro de escrita erro de escrita: %s erro de writev: %s formato extendido '%s' errado formato da etiqueta no pedaço extensível 'fmt ' errado xrun(A ESVAZIAR): erro de preparação: %s xrun: preparar erro: %s xrun_recovery falhou: %d,%s
 