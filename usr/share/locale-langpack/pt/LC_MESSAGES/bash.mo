��    @        Y         �     �     �  	   �     �     �  &   �     �  .   
     9     X     o     �     �  /   �     �  -   �     !     ;     S     f      ~     �     �     �     �     �       &   1     X     n     �     �     �     �     �     �     �     �     	     "	  9   +	     e	      �	  !   �	  !   �	     �	     �	     
     
  	   4
     >
     W
  
   _
     j
     �
  4   �
     �
  
   �
     �
  	   �
  -     -   /     ]  �  m     @     H  	   b     l       :   �     �  9   �  *   %  !   P     r     �     �  0   �  "   �  :        U     o     �     �  &   �     �     �          =  !   Q  !   s  (   �     �     �     �               #     6     K     ]  &   w  #   �  	   �  W   �      $  2   E  +   x  &   �     �     �            
   +     6     S     Y     l     �  5   �     �     �     �       <     <   S     �     /      $      <          =   3   9   (           ?          6             1   ;   
          :                    ,      	               >   -   0      #                    &                                    "       !   *               5   @      7             '      +   .   4   8   2              )          %             line  %s can be invoked via  %s is %s
 %s out of range %s: %s out of range %s: cannot assign to non-numeric index %s: cannot create: %s %s: cannot destroy array variables in this way %s: cannot execute binary file %s: cannot execute: %s %s: cannot open: %s %s: cannot read: %s %s: command not found %s: error retrieving current directory: %s: %s
 %s: file is too large %s: first non-whitespace character is not `"' %s: illegal option -- %c
 %s: invalid action name %s: invalid option %s: invalid option name %s: invalid signal specification %s: is a directory %s: missing colon separator %s: not a regular file %s: not found %s: numeric argument required %s: option requires an argument %s: option requires an argument -- %c
 %s: readonly function %s: readonly variable %s: restricted %s: usage:  Aborting... HOME not set OLDPWD not set Unknown error `%s': cannot unbind `%s': not a valid identifier `%s': unknown function name argument bash_execute_unix_command: cannot find keymap for command can only be used in a function cannot open shared object %s: %s cannot use `-f' to make functions cannot use more than one of -anrw expression expected invalid hex number invalid number invalid octal number line %d:  line editing not enabled logout
 loop count no closing `%c' in %s no command found only meaningful in a `for', `while', or `until' loop read error: %d: %s restricted too many arguments warning:  warning: -C option may not work as you expect warning: -F option may not work as you expect write error: %s Project-Id-Version: bash
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-02-11 11:19-0500
PO-Revision-Date: 2008-05-11 23:56+0000
Last-Translator: Susana Pereira <susana.pereira@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 15:59+0000
X-Generator: Launchpad (build 18115)
  linha  %s pode ser invocado via  %s é %s
 %s fora do alcance %s: %s fora do alcance %s: não é possível atribuir a um indicie não numérico %s: impossível criar: %s %s: não se pode destruir variáveis de matriz deste modo %s: impossível executar ficheiro binário %s: não se consegue executar: %s %s: não se consegue abrir: %s %s: impossível ler: %s %s: comando não reconhecido %s: erro ao retirar a directoria actual: %s: %s
 %s: o ficheiro é demasiado grande %s: o primeiro caracter que não é um espaço não é `"' %s: opção ilegal -- %c
 %s: nome de acção inválido %s: opção inválida %s: nome de opção inválido %s: especificação de sinal inválida %s: é uma directoria %s: falta o separador vírgula %s: não é um ficheiro regular %s: não encontrado %s: argumento numérico requerido %s: a opção requer um argumento %s: a opção requer um argumento -- %c
 %s: função só de leitura %s: variável só de leitura %s: restrito %s: modo de uso:  A abortar... HOME não definido OLDPWD não definido Erro desconhecido `%s': impossível separar '%s': não é um identificador válido '%s': nome de função desconhecido argumento bash_execute_unix_command: não é possível encontrar o mapa de teclado para o comando só pode ser usado numa função não se consegue abrir o objecto partilhado %s: %s não se pode usar '-f' para fazer funções não se pode usar mais que um de -anrw esperada uma expressão número hexadecimal inválido número inválido número octal inválido linha %d:  edição de linha não ativo sair
 contagem de ciclos no há um `%c' final em %s nenhum comando encontrado apenas faz sentido num loop 'for', 'while' ou 'until' erro de leitura: %d: %s restrito demasiados argumentos aviso:  aviso: a opção -C pode não funcionar como está à espera aviso: a opção -F pode não funcionar como está à espera erro de escrita: %s 