��    -      �  =   �      �     �  "   �  @        _     v     |  
   �  4   �  .   �  9   �  +   -     Y     `     v     �     �     �  	   �     �     �     �            @   '     h     t  �   y          $     )     1  	   6     @     H     \     t     �  &   �     �     �     �  $     /   ,     \  �  e      �	  8   
  G   J
  !   �
     �
     �
     �
  4   �
  2     L   ;  B   �     �  #   �     �          5     T  	   `     j     �     �     �     �  C   �          %  �   +     �     �     �     �     �               3     P  !   e  /   �  	   �  #   �  !   �  "     0   *     [           
                                                     %                     )          *   "       !          -      ,      &                $         	   '      (       #             +                       Byobu Configuration Menu  file exists, but is not a symlink <Tab>/<Alt-Tab> between elements | <Enter> selects | <Esc> exits Add to default windows Apply Archive Byobu Help Byobu currently does not launch at login (toggle on) Byobu currently launches at login (toggle off) Byobu will be launched automatically next time you login. Byobu will not be used next time you login. Cancel Change Byobu's colors Change escape sequence Change escape sequence: Change keybinding set Choose Command:  Create new window(s): Create new windows ERROR: Invalid selection Error: Escape key: ctrl- Extract the archive in your home directory on the target system. File exists Help If you are using the default set of keybindings, press\n<F5> to activate these changes.\n\nOtherwise, exit this screen session and start a new one. Manage default windows Menu Message Okay Presets:  Profile Remove file? [y/N]  Run "byobu" to activate Select a color:  Select a screen profile:  Select window(s) to create by default: Title:  Toggle status notifications Toggle status notifications: Which profile would you like to use? Which set of keybindings would you like to use? Windows: Project-Id-Version: byobu
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2009-11-23 20:48-0600
PO-Revision-Date: 2011-02-13 16:29+0000
Last-Translator: Tiago Silva <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:40+0000
X-Generator: Launchpad (build 18115)
Language: pt
  Menu de configuração do Byobu  o ficheiro existe, mas não é uma ligação simbólica <Tab>/<Alt-Tab> navega pelos elementos | <Enter> selecciona | <Esc> sai Adicionar às janelas por defeito Aplicar Arquivo Ajuda do Byobu O Byobu não é iniciado a quando do login (activar) O Byobu é iniciado a quando do login (desactivar) O Byobu vai ser iniciado automaticamente da próxima vez que efectuar login. O Byobu não vai ser utilizado da próxima vez que efectuar login. Cancelar Alterar o esquema de cores do Byobu Altere a sequência de escape Altere a sequência de escape: Alterar o mapeamento de teclas Seleccionar Comando:  Criar nova(s) janela(s): Criar novas janelas ERRO: Selecção inválida Erro: Tecla de escape: ctrl- Extraia o arquivo no seu directório pessoal no sistema de destino. O ficheiro existe Ajuda Se está a usar o mapeamento de teclas por defeito, pressione\n<F5> para activas as alterações.\n\nCaso contrário, abandone esta sessão do screen e inicie uma nova. Gerir janelas por defeito Menu Mensagem Ok Pré-definições:  Perfil Remover ficheiro? [y/N]  Execute "byobu" para activar Selecionar uma cor:  Seleccionar um perfil do screen:  Seleccionar a(s) janela(s) a criar por defeito: Título:  Activar as notificações de estado Activar notificações de estado: Qual o perfil que deseja utilizar? Qual o mapeamento de teclas que deseja utilizar? Janelas: 