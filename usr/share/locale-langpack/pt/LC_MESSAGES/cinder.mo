��          �      �       H     I  ,   h  H   �  '   �  &        -  0   =     n  *   �     �  #   �  ,   �  =      �  ^  #   5  0   Y  W   �  '   �  +   
     6  /   G     w  .   �     �  1   �  0     >   G        
                                       	                  An unknown exception occurred. Backup driver reported an error: %(message)s Bad or unexpected response from the storage volume backend API: %(data)s Connection to glance failed: %(reason)s Not authorized for image %(image_id)s. Not authorized. Policy doesn't allow %(action)s to be performed. Resource could not be found. Service %(service_id)s could not be found. The results are invalid. User does not have admin privileges Volume driver reported an error: %(message)s read_deleted can only be one of 'no', 'yes' or 'only', not %r Project-Id-Version: Cinder
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2016-03-23 06:37+0000
PO-Revision-Date: 2016-03-31 08:54+0000
Last-Translator: Ricardo Sousa <Unknown>
Language-Team: Portuguese (http://www.transifex.com/projects/p/openstack/language/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:17+0000
X-Generator: Launchpad (build 18115)
Generated-By: Babel 1.3
 Ocorreu uma exceção desconhecida. O driver de backup reportou um erro: %(message)s Resposta inválida ou inesperada da API do backend do volume de armazenamento: %(data)s Falha de conexão ao glance: %(reason)s Não autorizado para a imagem %(image_id)s. Não autorizado. As políticas não permitem executar %(action)s O recurso não foi encontrado. O serviço %(service_id)s não foi encontrado. Os resultados são inválidos. Utilizador não tem privilégios de administrador O driver de volume reportou um erro: %(message)s read_deleted só pode ser 'não', 'sim' ou 'somente' e não %r 