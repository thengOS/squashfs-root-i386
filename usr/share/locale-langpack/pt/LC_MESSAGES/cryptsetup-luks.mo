��    A      $  Y   ,      �     �  �   �     f     �     �     �     �     �     �     �  7   �     6     Q     V     m     |     �     �     �  )   �       ?     4   S     �  %   �  &   �     �  -   	     9	     A	  "   X	  6   {	  >   �	     �	     
  &   '
  M   N
  +   �
  6   �
  #   �
     #  .   3  '   b     �     �     �     �     �       G     ,   _     �     �  "   �     �     �     �                  (   3     \     v  1   �  �  �     \  �   s     S     s     �      �     �  %   �     �       8        Q     o  +   t     �     �  "   �     �     �  4        I  8   \  @   �  .   �  -     ,   3     `  1   {     �  !   �  (   �  7     H   9     �  #   �  .   �  W   �  ;   F  <   �  4   �     �  ,   
  ,   7  #   d     �  &   �     �  !   �     �  ^     3   {     �     �  -   �  !        %     =      Q     r     w  :   �     �     �  B   
     +   %   $   /   1      &                   =   <      @      ?   *   ;              (   
      A       >             #      0       ,                                   .         "                                        '                   9   -   )   	   :   4   !           5   8   7   2                     3          6    
<action> is one of:
 
<name> is the device to create under %s
<device> is the encrypted device
<key slot> is the LUKS key slot number to modify
<key file> optional key file for the new key for luksAddKey action
 %s is not a LUKS partition
 %s: requires %s as arguments <device> <device> <key slot> <device> <name>  <device> [<new key file>] <name> <name> <device> Align payload at <n> sector boundaries - for luksFormat Argument <action> missing. BITS Can't open device: %s
 Command failed Command successful.
 Create a readonly mapping Display brief usage Do not ask for confirmation Failed to obtain device mapper directory. Help options: How many sectors of the encrypted data to skip at the beginning How often the input of the passphrase can be retried Key %d not active. Can't wipe.
 Key size must be a multiple of 8 bits PBKDF2 iteration time for LUKS (in ms) Print package version Read the key from a file (can be /dev/random) SECTORS Show this help message Shows more detailed error messages The cipher used to encrypt the disk (see /proc/crypto) The hash used to create the encryption key from the passphrase The size of the device The size of the encryption key The start offset in the backend device This is the last keyslot. Device will become unusable after purging this key. This will overwrite data on %s irrevocably. Timeout for interactive passphrase prompt (in seconds) Unable to obtain sector size for %s Unknown action. Verifies the passphrase by asking for it twice [OPTION...] <action> <action-specific>] add key to LUKS device create device dump LUKS partition information formats a LUKS device key %d active, purge first.
 key %d is disabled.
 key material section %d includes too few stripes. Header manipulation?
 memory allocation error in action_luksFormat modify active device msecs open LUKS device as mapping <name> print UUID of LUKS device remove LUKS mapping remove device resize active device secs show device status tests <device> for LUKS partition header unknown hash spec in phdr unknown version %d
 wipes key with number <key slot> from LUKS device Project-Id-Version: cryptsetup
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2007-01-02 16:49+0100
PO-Revision-Date: 2015-01-12 03:28+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:25+0000
X-Generator: Launchpad (build 18115)
 
<acção> é uma de:
 
<nome> é o dispositivo para criar sob %s
<dispositivo> é um dispositivo encriptado
<key slot> é o número do slot de uma chave LUKS para modificar
<key file> ficheiro chave opcional para uma ação nova para luksAddKey
 %s não é uma partição LUKS
 %s: requer %s como argumentos <dispositivo> <dispositivo> <ranhura da chave> <dispositivo> <nome>  <dispositivo> [<novo ficheiro chave>] <nome> <nome> <dispositivo> Alinhe payload em <n> limites do setor - para luksFormat Argumento <acção> em falta. BITS Não foi possível abrir o dispositivo: %s
 Comando falhou Comando bem sucedido.
 Criar um mapeamento só de leitura Mostrar uso breve Não pedir confirmação Falha ao obter a directoria do dispositivo mapeador. Opções de ajuda: Quantos sectores dos dados encriptados saltar ao início Número de tentativas permitido para a escrita da palavra-passe. Chave %d inactiva. Não foi possível apagar.
 Tamanho da chave deve ser múltiplo de 8 bits Tempo de iteração PBKDF2 para LUKS (em ms) Imprimir versão do pacote Ler a chave de um ficheiro (pode ser /dev/random) SECTORES Apresentar esta mensagem de ajuda Mostra mensagens de erro mais detalhadas A cifra usada para encriptar o disco (ver /proc/crypto) A hash usada para criar a chave de encriptação a partir da frase-passe O tamanho do dispositivo O tamanho da chave de encriptação Início do deslocamento no backend dispositivo Este é o último keyslot. O dispositivo ficará inutilizável após purgar esta chave. Isto vai sobrescrever os dados em %s de forma irrevogável. Tempo máximo de espera para linha de comando da frase-passe Não foi possível obter o tamanho do sector para %s Acção desconhecida. Verificar a frase-passe pedindo-a duas vezes [OPCÃO...] <acção> <acção especifíca>] adicionar chave ao dispositivo LUKS criar dispositivo mostar informação da partição LUKS formata um dispositivo LUKS chave %d activa, limpe primeiro.
 chave %d está desactivada.
 A chave material da secção %d não inclui stripes suficientes. Manipulação de cabeçalho?
 erro de alocação de memória em action_luksFormat modificar dispositivo activo milisegs abrir dispositivo LUKS como mapeamento <nome> imprimir UUID do dispositivo LUKS remover mapeamento LUKS remover dispositivo redimensionar dispositivo activo segs mostrar o estado do dispositivo testa o <dispositivo> para o cabeçalho de partição LUKS hash spec desconhecida em phdr versão desconhecida %d
 apaga a chave com o número <ranhura da chave> do dispositivo LUKS 