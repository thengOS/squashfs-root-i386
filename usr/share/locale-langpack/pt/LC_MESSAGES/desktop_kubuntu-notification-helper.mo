��          �      <      �  $   �  +   �  F     4   I  L   ~  F   �  4     "   G     j     |     �     �     �     �     �       0   #  �  T  "     '   5  H   ]  1   �  Q   �  L   *  ?   w  &   �     �     �                5     K     d     z  &   �     
                                   	                                                             CommentA system restart is required CommentAdditional drivers can be installed CommentAn application has crashed on your system (now or in the past) CommentControl the notifications for system helpers CommentExtra packages can be installed to enhance application functionality CommentExtra packages can be installed to enhance system localization CommentSoftware upgrade notifications are available CommentSystem Notification Helper NameApport Crash NameDriver Enhancement NameLocalization Enhancement NameNotification Helper NameOther Notifications NameReboot Required NameRestricted Install NameUpgrade Hook X-KDE-KeywordsNotify,Alerts,Notification,popups Project-Id-Version: kubuntu-notification-helper
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-04-01 17:07+0000
PO-Revision-Date: 2014-05-09 15:21+0000
Last-Translator: Pedro Saraiva <pdro.saraiva@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:48+0000
X-Generator: Launchpad (build 18115)
 É necessário reiniciar o sistema Drivers adicionais podem ser instalados Uma aplicação deixou de funcionar no seu sistema (agora ou no passado) Controlo de notificações para sistema de ajudas Pacotes extras podem ser instalados para aumentar a funcionalidade da aplicação Pacotes extras podem ser instalados para melhorar a localização do sistema Notificações de atualização de software estão disponíveis Assistente de notificação do sistema Erro no Apport Driver Enhancement Localization Enhancement Assistente de notificação Outras Notificações É Necessário Reiniciar Instalação Restrita Actualizar Hook Notifique,Alertas,notificação,popups 