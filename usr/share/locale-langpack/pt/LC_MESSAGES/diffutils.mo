��    '      T  5   �      `     a  0   �  (   �     �     �          2     I     f     �  $   �     �     �     �     �             #   ?     c  �   ~     0     8     K     `     r     �     �  4   �     �     �       "     %   6     \     j     y     �     �  �  �      L	  1   m	  2   �	  $   �	  $   �	     
     7
  )   V
  %   �
     �
  '   �
     �
          "     4  %   I  #   o  #   �  #   �  �   �     �  #   �     �     �     �          4  <   J     �     �     �  #   �  0   �     !     2     F  +   Y     �                      %           
               '                                            $   	             "             !       #                                             &    %s %s differ: byte %s, line %s
 %s %s differ: byte %s, line %s is %3o %s %3o %s
 --from-file and --to-file both specified Binary files %s and %s differ
 Compare two files byte by byte. Files %s and %s differ
 Invalid back reference Invalid character class name Invalid collation character Invalid content of \{\} Invalid preceding regular expression Invalid range end Invalid regular expression Memory exhausted No match No newline at end of file No previous regular expression Premature end of regular expression Regular expression too big SKIP values may be followed by the following multiplicative suffixes:
kB 1000, K 1024, MB 1,000,000, M 1,048,576,
GB 1,000,000,000, G 1,073,741,824, and so on for T, P, E, Z, Y. Success Trailing backslash Unknown system error Unmatched ( or \( Unmatched ) or \) Unmatched [ or [^ Unmatched \{ Usage: %s [OPTION]... FILE1 [FILE2 [SKIP1 [SKIP2]]]
 cmp: EOF on %s
 conflicting width options memory exhausted options -l and -s are incompatible pagination not supported on this host program error stack overflow standard output too many file label options write failed Project-Id-Version: diffutils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2013-03-24 11:05-0700
PO-Revision-Date: 2010-12-31 17:46+0000
Last-Translator: Almufadado <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:43+0000
X-Generator: Launchpad (build 18115)
 %s %s difere: byte %s, linha %s
 %s %s difere: byte %s, linha %s é %3o %s %3o %s
 -de-ficheiro e --para-ficheiro ambas especificadas Ficheiros binários %s e %s diferem
 Comparar dois ficheiros byte a byte. Ficheiros %s e %s diferem
 Referência de recuo inválida Nome de classe de caracteres é inválido Caractere de agrupamento é inválido Conteúdo de \{\} inválido Expressão regular precedente inválida Final de alcance inválido Expressão regular inválida Memória esgotada Sem correspondência Nenhuma linha nova no fim do ficheiro Nenhuma expressão regular anterior Fim prematuro da expressão regular Expressão regular demasiado grande SKIP valores podem ser seguidos pelos sufixos multiplicativos seguintes:
kB 1000, K 1024, MB 1,000,000, M 1,048,576,
GB 1,000,000,000, G 1,073,741,824, e assim por diante para T, P, E, Z, Y. Sucesso Backslash ('\') de arrasto no final Erro de Sistema Desconhecido ( ou \( não correspondido ) ou \) não correspondido [ ou [^ não correspondido \{ não correspondido Utilização: %s [OPÇÃO]... FILE1 [FILE2 [SKIP1 [SKIP2]]]
 cmp: EOF ligado %s
 opções de largura em conflito memoria esgotada opções -l e -s são incompativeis a paginação não é suportada neste anfitrião erro de programa transbordo da pilha Saída por defeito demasiadas opções na etiqueta de ficheiro erro na escrita 