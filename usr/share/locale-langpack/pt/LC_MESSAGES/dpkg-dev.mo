��          �      <      �  #   �     �     �       r     !   �     �     �     �     �  E   �  E   2  :   x     �  #   �  *   �        �  <  (   �     $  "   ;     ^  �   n  :         ;     V     i     v  T   �  S   �  D   0  $   u  .   �  T   �  %   	                                     
                                                      	          %s is not a supported variable name Debian %s version %s.
 cannot combine %s and %s dpkg-genchanges fakeroot not found, either install the fakeroot
package, specify a command with the -r option, or run this as root gain-root-commmand '%s' not found host architecture source changed by source package source version unknown Debian architecture %s, you must specify GNU system type, too unknown GNU system type %s, you must specify Debian architecture, too unknown default GNU system type for Debian architecture %s unknown option or argument %s unknown substitution variable ${%s} using a gain-root-command while being root write error on control data Project-Id-Version: dpkg
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-25 13:28+0100
PO-Revision-Date: 2008-06-02 09:06+0000
Last-Translator: João Mouro <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 16:13+0000
X-Generator: Launchpad (build 18115)
 %s não é um nome de variavel suportado Debian %s versão %s.
 Não é possível combinar %s e %s dpkg-genchanges o fakeroot não foi encontrado, deverá instalar o pacote
fakeroot, especificar um comando com a opção -r ou executar isto como superutilizador comando para passar a superutilizador '%s' não encontrado arquitectura do anfitrião fonte alterada por pacote fonte versão da fonte arquitectura Debian desconhecida %s. Deve especificar também o tipo de sistema GNU. tipo de sistema GNU desconhecido %s, deve também especificar a arquitetura Debian. Tipo de sistema GNU por omissão desconhecido para a arquitectura %s Opção ou argumento %s desconhecido variável de substituição desconhecida ${%s} está a usar um comando para se tornar superutilizador quando já é superutilizador erro de escrita nos dados de controle 