��    "      ,  /   <      �     �     
       
        #  .   :     i  	   |     �     �     �  !   �     �     �     �     �          .  ,   E     r     �     �     �     �  $   �     �     �       �   '     �     �     �     �  �  �     �     �     �  
   �     �  N   �     .  
   K     V     t     �  %   �     �     �     �  $   �  #   	  #   @	  D   d	     �	     �	  &   �	     �	     
  2   
     >
  "   Q
     t
  �   �
     6     C  	   U     _                                                                                 	             
         !                                             "          Add input method Addon Advance Appearance Available Input Method Cannot load currently used user interface info Clear font setting Configure Current Input Method Default Default keyboard layout Didn't install related component. Empty Global Config Input Method Input Method Configuration Input Method Default Input method settings: Keyboard layout to use when no input window: Keyboard layout: Language No configuration option for %s. Only Show Current Language Other Please press the new key combination Search Addon Search Input Method Show Advance Option The first input method will be inactive state. Usually you need to put <b>Keyboard</b> or <b>Keyboard - <i>layout name</i></b> in the first place. Unknown Unspecified _Cancel _OK Project-Id-Version: fcitx-configtool
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2013-10-18 03:07-0400
PO-Revision-Date: 2016-04-13 19:54+0000
Last-Translator: Ivo Xavier <ivoxavier.8@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:00+0000
X-Generator: Launchpad (build 18115)
 Adicionar método de entrada Addon Avançar Aparência Método de entrada disponível Impossível carregar informação do interface de utilizador usada actualmente Limpar definições da fonte Configurar Método de introdução atual Predefinição Esquema de teclado predefinido Sem componente relacionado instalado. Vazio Configuração global Método de entrada Configuração do método de entrada Método de introdução predefinido Definições do método de entrada: Esquema de teclado para utilizar quando não janela de introdução: Esquema de teclado: Idioma Sem opção de configuração para %s. Mostra só o idioma atual Outro Por favor, pressione a nova combinação de teclas Procurar por Addon Metodo de Introdução de Pesquisa Mostrar opções avançadas O primeiro método de introdução estará inativado. Habitualmente precisa de colocar o <b>Teclado</b> ou <b><i>Nome de esquema</i> - Teclado</b> em primeiro lugar. Desconhecido Não especificado _Cancelar _OK 