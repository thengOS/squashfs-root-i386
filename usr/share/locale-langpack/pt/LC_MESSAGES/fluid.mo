��    p      �  �         p	  	   q	     {	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  
   �	     �	     
     
     
     
  
   &
     1
     9
     A
     F
     N
     V
     c
     l
  	   s
     }
     �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
            	               
   %  	   0     :     ?     E  
   L     W     i     }     �     �     �  	   �     �     �     �     �     �     �  
   �     �  	   �     �                    (     =     K     T     d     p     |  	   �  	   �     �     �     �  
   �     �     �                  A     b     {          �     �     �     �     �     �     �    �  �  �  
   p     {     �     �     �  	   �     �     �     �     �  	   �     �     �     �     �     �                        	   .     8  
   M     X     `     f     l     u     }  	   �     �     �     �     �     �     �     �     �     �     �  
   �     	  	     	        #     ,     @     B     D     F     L     Q     V  	   \     f     i     u     �     �     �     �  	   �     �     �     �     �     �     �  	   �  	   �                    !     2     ;     O  
   X     c     t     }     �     �     �  	   �     �     �     �       
        $     ,     E     N     T     a  #   s  "   �     �     �     �          	               %     5     >     R     U    X     W       f          G           *   #   R      e                     Z   ?      a       9   j   3   V   $   L   :   T   %   N       F   @             4      p      o       5   m   Y   0      2   _      k          B          O   `   \   
   E   	   -   '   <   .   l   D   d           >      /   ]   )       b             6   h   J   K   ;   C   +      I   g   "                    1   P                     S   7          X   8   (   [       c           U   H   i   ,       n         =           &   M                 ^               A      Q                   !    # Copies: &About FLUID... &Align &Both &Bottom &Center &Copy &Delete &Down &Edit &File &Group &Height &Help &Horizontal &Huge &Insert... &Large &Later &Layout &Left &Make Same Size &Manual... &Medium &Middle &New &New... &Normal &On FLUID... &Open... &Paste &Print... &Quit &Redo &Revert... &Right &Save &Small &Sort &Space Evenly &Tiny &Top &Undo &Vertical &Width &Write Strings... 1 2 3 @-1-> @-12 @-18 @-1<- @-3square A4 About FLUID Alignment: Browse... C&ut Close Copies Dup&licate Execute &Again... Execute &Command... From: GUI Height: Image: Inactive: Label: Letter Maximum Size: Maximum: Minimum Size: Minimum: Page Size: Pages Position: Pr&operties... Print Printer Properties Printer: Pro&ject Settings... Properties... Relative Sa&ve A Copy... Save &As... Select &All Select &None Selection Shortcut: Show Source Code... Size: Text Text/Image The height of the widget. The label style for the widget. The label text for the widget. The maximum value of the widget. The minimum value of the widget. The width of the widget. To: Ung&roup Value: Values: View License... Width: Write &Code... X: Y: usage: %s <switches> name.fl
 -c : write .cxx and .h and exit
 -cs : write .cxx and .h and strings and exit
 -o <name> : .cxx output filename, or extension if <name> starts with '.'
 -h <name> : .h output filename, or extension if <name> starts with '.'
%s
 Project-Id-Version: fltk1.1
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-10-20 06:40+0000
PO-Revision-Date: 2007-04-01 03:57+0000
Last-Translator: Marco da Silva <marcodasilva@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:27+0000
X-Generator: Launchpad (build 18115)
 # Cópias: &Sobre FLUID.. &Alinhar &Ambos &Baixo &Centrado &Copiar &Apagar &Baixo &Editar &Ficheiro A&grupar &Altura &Ajuda &Horizontal &Enorme &Inserir... &Grande &Mais tarde &Disposição &Esquerda &Fazer Mesmo Tamanho &Manual... &Médio &Meio &Novo &Novo... &Normal &No FLUID... &Abrir... &Colar &imprimir... &Sair &Refazer &Reverter... &Direita &Guardar &Pequeno &Ordenar &Espaçar Uniformente &Pequenino &Topo &Desfazer &Vertical &Largura &Escrever Linhas... 1 2 3 @-1-> @-12 @-18 @-1<- @-3square A4 Sobre FLUID Alinhamento: Explorar... &Cortar Fechar Cópias Dup&licar Executar &Outra vez Executar &Comando De: GUI Altura: Imagem: Inactivo: Etiqueta: Carta Tamanho máximo: Máximo: Tamanho mínimo: Mínimo: Tamanho da Página: Páginas Posição: Pr&opriedades... Imprimir Propriedades da Impressora Impressora: Definições do Projecto... Propriedades... Relativo. Gr&avar Copia Grav&ar Como Seleccion&ar Tudo Seleccionar &Nada Selecção Atalho: Mostrar Código Fonte... Tamanho: Texto Texto/Imagem Altura do widget: O estilo da etiqueta para o widget. O texto da etiqueta para o widget. O valor máximo do widget. O valor mínimo do widget. Largura do widget: Para: Desag&rupar Valor: Valores: Ver Licença... Largura: Escrever &Código.. X: Y: uso: %s <switches> nome.fl

 -c : escrever .cxx e .h e sair

 -cs : escrever .cxx e .h e strings e sair

 -o <nome> :  nome ficheiro de saida .cxx, ou extensão se <nome> começar com '.'

 -h <nome> :  nome ficheiro de saida .h,  ou extensão se <nome> começar com '.'

%s
 