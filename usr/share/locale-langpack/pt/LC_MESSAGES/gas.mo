��    /      �  C           6     5   P     �     �      �     �     �  $        -  3   @  \  t     �  #   �               %  #   5     Y  ,   h  4   �     �  (   �     
	      *	  $   K	     p	  2   �	  $   �	  $   �	     
  2   
  1   L
  &   ~
     �
     �
     �
                )  "   9      \     }  '   �      �     �     �  �    ;   �  $   �           7  "   O     r     �  /   �     �  9   �  �  )     �  /   �     �               %     E  .   T  ;   �     �  3   �  &     0   9  (   j  %   �  ;   �  '   �  '        E  0   S  .   �  -   �  %   �  "     '   *  #   R     v     �  *   �  %   �      �  9     $   B     g     �         -          +   
   "   ,       $                  (          #                                  .                 *   !   %                        &                                 /                    '      )                  	                              emulate output (default %s)
   --help                  show this message and exit
 ".else" without matching ".if" ".elseif" after ".else" ".elseif" without matching ".if" ".endif" without ".if" %s: data size %ld
 --hash-size needs a numeric argument Can't set GP value GNU assembler version %s (%s) using BFD version %s
 Options:
  -a[sub-option...]	  turn on listings
                      	  Sub-options [default hls]:
                      	  c      omit false conditionals
                      	  d      omit debugging directives
                      	  g      include general info
                      	  h      include high-level source
                      	  l      include assembly
                      	  m      include macro expansions
                      	  n      omit forms processing
                      	  s      include symbols
                      	  =FILE  list to FILE (must be last sub-option)
 Report bugs to %s
 Usage: %s [option...] [asmfile...]
 alias = %s
 bfd-target = %s
 canonical = %s
 character following name is not '#' cpu-type = %s
 emulations not handled in this configuration end of file after a one-character quote; \0 inserted end of file in comment end of file in comment; newline inserted end of file in escape character end of file in multiline comment end of file in string; '%c' inserted end of file inside conditional end of file not at end of a line; newline inserted expected comma after name in .symver failed to read instruction table %s
 format = %s
 here is the "else" of the unterminated conditional here is the start of the unterminated conditional ignoring incorrect section type for %s invalid identifier for ".ifdef" invalid listing option `%c' missing close quote; (assumed) missing emulation mode name missing name missing operand multiple emulation names specified no file name following -t option symbol '%s' is already defined unknown escape '\%c' in string; ignored unrecognized emulation name `%s' unrecognized option -%c%s unrecognized section type Project-Id-Version: binutils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-11-04 15:27+1030
PO-Revision-Date: 2009-08-26 21:24+0000
Last-Translator: Carlos Manuel <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:02+0000
X-Generator: Launchpad (build 18115)
                           emula o output (por omissão %s)
   --help mostra esta mensagem e sai
 ".else" sem correspondente ".if" ".elseif" após ".else" ".elseif" sem correspondente ".if" ".endif" sem ".if" %s: tamanho de dados %ld
 --hash-size necessita de um argumento numérico Não se pode definir o valor GP GNU assembler versão %s (%s) usando a versão %s do BFD
 Opções:
  -a[sub-opção...]	 ligar listagens
                      	  Sub-options [default hls]:
                      	  c      omitir condicionantes falsas
                      	  d      omitir diretivas de depuração
                      	  g      includir informação geral
                      	  h      includir fonte de alto-nível
                      	  l      incluir reunir
                      	  m      includir macro expansões
                      	  n      omitir formas processamento
                      	  s      includir símbolos
                      	  =FILE  listar para FICHEIRO (deve ser a última sub-opção)
 Reportar erros a %s
 Utilização: %s [opção...] [ficheiroasm...]
 alias = %s
 bfd-target = %s
 canonical = %s
 caracter após nome não é '#' cpu-type = %s
 emulações não tratadas nesta configuração fim do ficheiro após citação de um caracter; \0 inserido fim do ficheiro em comentário fim do ficheiro em comentário; nova linha inserida fim do ficheiro no caractere de escape fim do ficheiro em comentário de várias linhas fim do ficheiro em string; '%c' inserido fim de ficheiro dentro de condicional fim do ficheiro não é o fim de linha; nova linha inserida esperada vírgula após nome em .symver falhou a ler tabela de instruções %s
 formato = %s
 aqui é está "else" da condiçao não terminada aqui é o começo da condição não terminada ignorando tipo de secção incorrecto para %s identificador inválido para ".ifdef" opção de listagem inválida '%c' fecho de citação em falta; (assumido) nome do modo de emulação em falta nome em falta operando em falta vários nomes de emulação especifícados sem nome de ficheiro após opção -t símbolo '%s' já está definido Símbolo desconhecido de escape '\%c' na string; ignorado nome de emulação desconhecido `%s' opção ñao reconhecida -%c%s desconhecido tipo de secção 