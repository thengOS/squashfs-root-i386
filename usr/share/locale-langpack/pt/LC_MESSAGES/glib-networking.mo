��            )   �      �     �     �  #   �  #   �  #     #   1  #   U     y  "   �  &   �  $   �               +     A     X  $   p  &   �     �  -   �     	  ^   )     �  "   �  .   �     �  N         O  �  l  "   i     �  '   �  +   �  -   �  +   (	  -   T	  $   �	  3   �	  #   �	  (   �	     (
  3   0
     d
     }
     �
  ;   �
  *   �
  (     3   G  $   {  `   �  
     '     6   4  "   k  h   �     �                                                                                                        	             
                     Certificate has no private key Connection is closed Could not create TLS connection: %s Could not parse DER certificate: %s Could not parse DER private key: %s Could not parse PEM certificate: %s Could not parse PEM private key: %s Error performing TLS close: %s Error performing TLS handshake: %s Error reading data from TLS socket: %s Error writing data to TLS socket: %s Module No certificate data provided Operation would block PKCS#11 Module Pointer PKCS#11 Slot Identifier Peer failed to perform TLS handshake Peer requested illegal TLS rehandshake Proxy resolver internal error. Server did not return a valid TLS certificate Server required TLS certificate Several PIN attempts have been incorrect, and the token will be locked after further failures. Slot ID TLS connection closed unexpectedly TLS connection peer did not send a certificate The PIN entered is incorrect. This is the last chance to enter the PIN correctly before the token is locked. Unacceptable TLS certificate Project-Id-Version: 3.8
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=glib&keywords=I18N+L10N&component=network
POT-Creation-Date: 2016-05-19 08:00+0000
PO-Revision-Date: 2015-09-30 22:29+0000
Last-Translator: Pedro Albuquerque <Unknown>
Language-Team: Português <palbuquerque73@openmailbox.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:01+0000
X-Generator: Launchpad (build 18115)
Language: pt
 Certificado não tem chave privada A ligação está fechada Impossível criar uma ligação TLS: %s Impossível processar o certificado DER: %s Impossível processar a chave privada DER: %s Impossível processar o certificado PEM: %s Impossível processar a chave privada PEM: %s Erro ao terminar a ligação TLS: %s Erro ao estabelecer a ligação TLS (handshake): %s Erro ao ler dados do socket TLS: %s Erro ao escrever dados no socket TLS: %s Módulo Não foram indicados quaisquer dados de certificado Operação iria bloquear Ponteiro de módulo PKCS#11 Identificador de slot PKCS#11 O destino falhou ao estabelecer a ligação (handshake) TLS Destino requereu novo handshake TLS ilegal Erro interno do solucionador de proxies. O servidor não devolveu um certificado TLS válido O servidor requer um certificado TLS Foram introduzidos vários PINs incorretos e o símbolo será trancado caso ocorram mais falhas. ID de slot Ligação TLS terminada inesperadamente O parceiro de ligação TLS não enviou um certificado O PIN introduzido está incorreto. Esta é a última oportunidade para introduzir corretamente o PIN antes de que o símbolo seja trancado. Certificado TLS inaceitável 