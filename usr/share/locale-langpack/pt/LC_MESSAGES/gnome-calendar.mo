��    x      �  �   �      (
     )
     I
     O
     U
     k
     q
     z
     |
     
     �
     �
     �
     �
     �
  *   �
     �
               !     <     M     _     n     �     �     �  	   �     �     �     �     �  $   �  )        :     >     K     \     s     x     �     �  �   �     Q     _     l  �   x     W     ^     {     �     �     �     �     �     �     �  	   �                          $     '     7      <  &   ]     �     �     �     �     �     �     �     �     �            '   &     N     U     [     a     x  >   �     �     �     �  )   �       �   "     �     �     
          5     A     a     f     m  
   s     ~     �     �     �     �     �     �          /     J     d     ~     �     �     �     �               1  2  D  "   w     �     �     �     �     �     �     �     �     �  	   �     �          &  8   :  
   s     ~     �     �     �     �     �          #     :     N     k     x     �     �     �  $   �  )   �     �     �            	   4     >     Q     d  �   l           1     A  l   N     �     �     �     �       
              %     ?     Q     f     r     �     �  	   �     �     �     �  #   �  /   �  "   	     ,     K     Z     a     d     r     �     �  $   �     �  =   �                    #     ?  >   T     �     �     �  (   �  
   �  �   �     �     �     �          '  %   9     _     c     k     q     ~     �     �     �     �     �     �     �     �     �     �     �                       #      0      4   I  A                    C   .       p   a   #   i   E      "   2   9   8                 &   +       *   n                       t      N   P      V   K   
          ]   G   m                      A   o   5   \   U              -   l   w   s              7      x       ?           g   Z           /   1   >   (      r         M       W              '   _   ^      e       [           B   Y      @           %       k      X   :   L   Q   F   b   )   c   J   v       O       H   $       <       f   0   ;       6   `   !   S               	   ,   j   R           =   q      3      T      D       d                 4   u   I   h    %s (this calendar is read-only) %s AM %s PM - Calendar management 00:00 00:00 PM : AM Access, and manage calendar Account Add Add Calendar Add Eve_nt… Add Event… Add new events to this calendar by default All day Another event deleted Calendar Calendar <b>%s</b> removed Calendar Address Calendar Settings Calendar files Calendar for GNOME Calendar management Calendar name Calendar;Event;Reminder; Calendars Cancel Click to set up Color Connect Copyright © %d The Calendar authors Copyright © %d–%d The Calendar authors Day Delete event Display calendar Display version number Done Edit Calendar Edit Details… Ends Enter the address of the calendar that you want to add. If the calendar belongs to one of your online accounts, you can add it through the <a href="GOA">online account settings</a>. Event deleted From File… From Web… GNOME Calendar is a simple and beautiful calendar application designed to perfectly fit the GNOME desktop. By reusing the components which the GNOME desktop is build on, Calendar nicely integrates with the GNOME ecosystem. Google List of the disabled sources Location Manage your calendars Microsoft Exchange Midnight Month New Event from %s to %s New Event on %s New Local Calendar… No events No results found Noon Notes Off On Online Accounts Open Open calendar on the passed date Open calendar showing the passed event Open online account settings Other event Other %d events Other events Overview PM Password Remove Calendar Save Search for events Select a calendar file Settings Sources disabled last time Calendar ran Starts Title Today Try a different search Type of the active view Type of the active window view, default value is: monthly view Undo Unnamed Calendar Unnamed event Use the entry above to search for events. User We aim to find the perfect balance between nicely crafted features and user-centred usability. No excess, no lacks. You'll feel comfortable using Calendar, like you've been using it for ages! Window maximized Window maximized state Window position Window position (x and y). Window size Window size (width and height). Year _About _Quit _Search… _Synchronize event date format%B %d ownCloud shortcut windowClose window shortcut windowGeneral shortcut windowGo back shortcut windowGo forward shortcut windowMonth view shortcut windowNavigation shortcut windowNew event shortcut windowNext view shortcut windowPrevious view shortcut windowSearch shortcut windowShortcuts shortcut windowShow help shortcut windowShow today shortcut windowView shortcut windowYear view translator-credits Project-Id-Version: gnome-calendar master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-calendar&keywords=I18N+L10N&component=General
POT-Creation-Date: 2016-05-18 08:25+0000
PO-Revision-Date: 2016-05-25 20:00+0000
Last-Translator: Tiago S. <Unknown>
Language-Team: Português <gnome_pt@yahoogroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 19:34+0000
X-Generator: Launchpad (build 18115)
Language: pt
 %s (esta agenda é só de leitura) %s AM %s PM - gestão calendário 00:00 00:00 PM : AM Aceder e gerir calendário Conta Adicionar Adicionar calendário Adicionar eve_nto… Adicionar evento… Adicionar novos eventos a esta agenda por predefinição Todo o dia Outro evento eliminado Calendário Calendário <b>%s</b> removido Endereço do calendário Definições do calendário Ficheiros do calendário Calendário do GNOME Gestão de calendário Nome do calendário Calendário;Evento;Lembrete; Calendários Cancelar Clique para configurar Cor Ligar Copyright © %d Os autores da Agenda Copyright © %d–%d Os autores da Agenda Dia Eliminar evento Mostrar calendário Mostrar número da versão Terminado Editar calendário Editar detalhes… Termina Insira o endereço da agenda que quer adicionar. Se a agenda pertence a uma das suas contas online, pode adicioná-la através das <a href="GOA">definições de contas online</a>. Evento eliminado Com ficheiro… Com a Web… Calendário do GNOME é uma app simples e bonita, desenhada para uma integração perfeita no desktop GNOME. Google Lista de origens desativadas Localização Gira as suas agendas Microsoft Exchange Meia-noite Mês Novo Evento de %s até %s Novo evento em %s Nova agenda local… Sem eventos Sem resultados Meio dia Notas Desligado Ligado Contas online Abrir Abrir calendário numa data passada Abrir o calendário mostrando um evento passado Abrir definições de conta online Outro evento Outros %d eventos Outros eventos Resumo PM Palavra-passe Remover calendário Guardar Procurar eventos Selecione um ficheiro do calendário Definições Origens desativadas desde a última execução do Calendário Inicia Título Hoje Tente uma procura diferente Tipo da visão ativa Tipo da visão da janela ativa, sendo o padrão: visão mensal Desfazer Calendário sem nome Evento sem nome Use a caixa acima para procurar eventos. Utilizador Procuramos atingir o equilíbrio perfeito entre funcionalidade e utilização centrada no utilizador. Sem excessos, sem faltas. Sentir-se-á confortável ao usar o calendário, tal como se a usasse há décadas! Janela maximizada Estado de janela maximizada Posição da janela Posição da janela (x e y) Tamanho da janela Tamanho da janela (largura e altura). Ano _Acerca _Sair _Procurar… _Sincronizar %B %d ownCloud Fechar a janela Geral Recuar Avançar Vista do mês Navegação Novo evento Próxima vista Vista anterior Procurar Atalhos Mostrar ajuda Mostrar hoje Ver Vista de ano Tiago S. <almosthumane@portugalmail.pt>, 2014.
Pedro Albuquerque <palbuquerque73@gmail.com>

Launchpad Contributions:
  Ivo Xavier https://launchpad.net/~ivoxavier
  Pedro Albuquerque https://launchpad.net/~palbuquerque73o
  Sérgio Cardeira https://launchpad.net/~cardeira-sergio
  Tiago S. https://launchpad.net/~tiagofsantos81 