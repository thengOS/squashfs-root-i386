��            )         �     �  	   �     �  	   �     �     �     �     �  �     �   �     �     �     �  	   �     �     �  E   �     �          !     '     ,  !   L     n     �     �     �  	   �     �     �  �  �     �     �  
   �     �               9     P    m  �   �	     w
     �
     �
  	   �
     �
     �
  ^   �
               1     8  $   >  !   c  %   �     �     �  "   �     �     �  E                                                                                     	      
                                                About All Fonts Back Copyright Description FONT-FILE OUTPUT-FILE Font Viewer GNOME Font Viewer GNOME Font Viewer also supports installing new font files downloaded in the .ttf and other formats. Fonts may be installed only for your use or made available to all users on the computer. GNOME Font Viewer shows you the fonts installed on your computer for your use as thumbnails. Selecting any thumbnails shows the full view of how the font would look under various sizes. Info Install Install Failed Installed Name Quit Run '%s --help' to see a full list of available command line options. SIZE Show the application's version Style TEXT Text to thumbnail (default: Aa) This font could not be displayed. Thumbnail size (default: 128) Type Version View fonts on your system [FILE...] fonts;fontface; translator-credits Project-Id-Version: 3.8
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-font-viewer&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 22:21+0000
PO-Revision-Date: 2015-12-04 14:44+0000
Last-Translator: Tiago S. <Unknown>
Language-Team: Portuguese <gnome_pt@yahoogroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:15+0000
X-Generator: Launchpad (build 18115)
Language: pt
 Sobre Todas as Fontes Retroceder Direitos de Autor Descrição FICHEIRO-FONTE FICHEIRO-SAÍDA Visualizador de Fontes Visualizador de fontes GNOME O Visualizador de Tipos de Letra GNOME também suporta a instalação de novos ficheiros de tipos de letra transferidos em .ttf ou noutros formatos. Os tipos de letra podem ser instalados somente para o seu uso ou tornados disponíveis para todos os utilizadores do computador. O Visualizador de Tipo de Letra GNOME mostra-lhe os tipos de letra instalados no seu computador para si como miniaturas. Seleccione qualquer uma das miniaturas para ter uma visão completa de como o tipo de letra aparenta em diversos tamanhos. Informação Instalar Falha ao Instalar Instalada Nome Sair Execute '%s --help' para ver uma lista completa das opções da linha de comando disponíveis. TAMANHO Mostrar versão da app Estilo TEXTO Texto para amostragem (omissão: Aa) Incapaz de apresentar esta fonte. Texto para amostragem (omissão: 128) Tipo Versão Visualize as fontes no seu sistema [FICHEIRO...] fontes;tipos de fonte; Duarte Loreto <happyguy_pt@hotmail.com>

Launchpad Contributions:
  André Sousa Parente https://launchpad.net/~andresousaparente
  Duarte Loreto https://launchpad.net/~happyguy-pt-hotmail
  Hugo.Batel https://launchpad.net/~hugo-batel
  Ivo Xavier https://launchpad.net/~ivoxavier
  Tiago S. https://launchpad.net/~tiagosdot 