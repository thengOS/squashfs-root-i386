��    @        Y         �  *   �  �  �     H  6   M  .   �  �   �     9  �   H     �  ;   
	     F	     N	  
   W	     b	     h	     �	     �	     �	     �	     �	     �	     �	  !   �	     
     
     0
     7
     <
     P
     a
     n
  O   �
     �
     �
     �
  	   �
                 	   (  	   2  	   <     F     S     Y     b     p     x     �     �     �     �  #   �     �               9  !   T     v     �     �     �     �  �    )     �  +     �  :   �  *   4  �   _     �  �   �     �  ?   �          #     ,     <  '   C     k  	   ~     �     �     �     �  &   �  )   �          #     >  	   F     P     n          �  R   �                 
   "     -     @     G  	   V  
   `  
   k     v     �  
   �     �     �  	   �     �  	   �  "   �     �     �                    &     .     C  
   T     _  5  k  )   �        /   4                       ?   $   :                      ,                 2         #   ;              <   6   1         %   -   9   @           	      (         +                      8       3      )          5      "                                     *   &   >      .           
       !   0   7       '   =       A matching game played with Mahjongg tiles A solitaire version of the classic Eastern tile game. Tiles are stacked on the board at the start of the game. The goal is to remove all the tiles in as little time as possible. Select two matching tiles and they will disappear from the board, but you can only select a tile if there is an empty space to its left or right at the same level. Be careful: tiles that look alike may actually be slightly different. Date Disassemble a pile of tiles by removing matching pairs Do you want to start a new game with this map? Each puzzle has at least one solution.  You can undo your moves and try and find the solution, restart this game, or start a new one. GNOME Mahjongg GNOME Mahjongg features a variety of starting layouts, some easy and some difficult. If you get stuck, you can ask for a hint, but this adds a large time penalty. Height of the window in pixels If you continue playing the next game will use the new map. Layout: Mahjongg Main game: Maps: Match tiles and clear the board Moves Left: New Game OK Pause the game Paused Preferences Print release version and exit Receive a hint for your next move Redo your last move There are no more moves. Tiles: Time Undo your last move Unpause the game Use _new map Width of the window in pixels You can also try to reshuffle the game, but this does not guarantee a solution. _About _Background color: _Close _Contents _Continue playing _Help _Layout: _Mahjongg _New Game _New game _Preferences _Quit _Restart _Restart Game _Scores _Shuffle _Theme: _Undo game;strategy;puzzle;board; mahjongg map nameCloud mahjongg map nameConfounding Cross mahjongg map nameDifficult mahjongg map nameEasy mahjongg map nameFour Bridges mahjongg map nameOverpass mahjongg map namePyramid's Walls mahjongg map nameRed Dragon mahjongg map nameThe Ziggurat mahjongg map nameTic-Tac-Toe translator-credits true if the window is maximized Project-Id-Version: 3.12
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-mahjongg&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 22:17+0000
PO-Revision-Date: 2016-04-01 09:32+0000
Last-Translator: Duarte Loreto <Unknown>
Language-Team: Português <palbuquerque73@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:21+0000
X-Generator: Launchpad (build 18115)
Language: pt
 Um jogo de emparelhar peças de Mahjongg. Uma versão solitária do clássico jogo oriental de peças. As peças são empilhadas num tabuleiro antes de começar um jogo. O objetivo é remover todas as peças no mínimo de tempo possível. Selecione duas peças idênticas e elas desaparecerão do tabuleiro, mas só pode selecionar uma peça se tiver espaço à esquerda ou à direita desta, no mesmo patamar. Tenha atenção: as peças que parecem iguais por vezes apresentam pequenas diferenças. Data Desfaça uma pilha de peças removendo os pares idênticos Deseja iniciar um novo jogo com este mapa? Cada jogo tem pelo menos uma solução. Pode desfazer as suas jogadas e tentar encontrar a solução, reiniciar este jogo ou iniciar um novo. GNOME Mahjongg O GNOME Mahjongg dispõe de uma variedade de disposições, algumas fáceis e algumas difíceis. Se ficar bloqueado pode pedir uma dica, mas isso adiciona uma grande penalização de tempo. Altura da janela em pixels Se continuar a jogar o próximo jogo irá utilizar o novo mapa. Disposição: Mahjongg Jogo principal: Mapas: Junte peças iguais e limpe o tabuleiro Jogadas restantes: Novo jogo Aceitar Pausar o jogo Em pausa Preferências Imprimir versão de lançamento e sair Obter uma dica para a sua próxima jogada Refazer a sua última jogada Não existem mais jogadas. Peças: Duração Desfazer a sua última jogada Continuar o jogo Utilizar o _novo mapa Largura da janela em pixels Também pode tentar voltar a dispor o jogo mas isso não é garantia de solução. _Sobre _Cor de fundo: _Fechar _Conteúdo _Continuar a jogar A_juda _Disposição: _Mahjongg _Novo jogo _Novo jogo _Preferências _Sair _Reiniciar _Reiniciar o jogo _Pontuações _Baralhar _Tema: _Desfazer jogo;estratégia;puzzle;tabuleiro; Núvem Cruz confusa Difícil Fácil Quatro pontes Viaduto Paredes da pirâmide Dragão vermelho O Ziggurat Tic-Tac-Toe Duarte Loreto <happyguy_pt@hotmail.com>
Pedro Albuquerque <palbuquerque73@gmail.com

Launchpad Contributions:
  António Miranda https://launchpad.net/~antoniom0910-deactivatedaccount
  Bruno Nova https://launchpad.net/~brunonova
  Duarte Loreto https://launchpad.net/~happyguy-pt-hotmail
  Gonçalo Silva https://launchpad.net/~goncaloluisilva-t-deactivatedaccount-deactivatedaccount-deactivatedaccount
  Pedro Albuquerque https://launchpad.net/~palbuquerque73-b
  Pedro Albuquerque https://launchpad.net/~pmralbuquerque
  Tiago S. https://launchpad.net/~tiagosdot verdadeiro se a janela estiver maximizada 