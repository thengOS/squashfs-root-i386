��    7      �  I   �      �     �     �     �     �  )   
     4  (   @     i          �  "   �  E   �       %   +     Q     Z     b  )   �     �  *   �     �  /   �     #  	   6  8   @  "   y     �     �     �     �     �     �       D   9  `   ~     �     �  )   	     A	  *   S	  #   ~	  	   �	     �	  D   �	  D   
     R
  !   q
  ,   �
  B   �
  
          '         >  +   _  �  �     Y     u     �  %   �  3   �       1        F     `  $   ~  $   �  S   �       4   5     j     |  !   �  ,   �     �  /   �       ?   :     z     �  9   �  &   �  &        *     B  $   V  '   {  *   �      �  [   �  b   K  "   �  %   �  1   �     )  *   8  )   c     �     �  N   �  N   �     I  '   Z  >   �  <   �     �       (     +   D  ?   p         -              0                	      1              4      %       '      *      6                 !   &          #         7   
   "       (      5   /   +              )   2                           ,   $         .            3                         (current) Unix password: Authentication failed. Can't set PAM_TTY=%s Cannot get username Causes the screensaver to exit gracefully Checking… Command to invoke from the logout button Don't become a daemon Enable debugging code Enter new Unix password: Error while changing NIS password. If the screensaver is active then deactivate it (un-blank the screen) Incorrect password. Launch screensaver and locker program Log _Out MESSAGE Message to show in the dialog No longer permitted to access the system. No password supplied Not permitted to gain access at this time. Not used Password has been already used. Choose another. Password unchanged Password: Query the length of time the screensaver has been active Query the state of the screensaver Retype new Unix password: S_witch User… Screensaver Show debugging output Show the logout button Show the switch user button Sorry, passwords do not match Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Time has expired. Turn the screensaver on (blank the screen) Unable to establish service %s: %s
 Username: Version of this application You are required to change your password immediately (password aged) You are required to change your password immediately (root enforced) You have the Caps Lock key on. You must choose a longer password You must wait longer to change your password Your account has expired; please contact your system administrator _Password: _Unlock failed to register with the message bus not connected to the message bus screensaver already running in this session Project-Id-Version: 3.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-01-04 14:27+0000
PO-Revision-Date: 2016-04-06 08:42+0000
Last-Translator: Ivo Xavier <ivoxavier.8@gmail.com>
Language-Team: Portuguese <gnome_pt@yahoogroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:43+0000
X-Generator: Launchpad (build 18115)
Language: pt
 Palavra-passe Unix (atual): Autenticação falhada. Incapaz de definir PAM_TTY=%s Incapaz de obter o nome de utilizador Força o protetor de ecrã a terminar graciosamente A verificar… Comando a invocar pelo botão de terminar sessão Não executar como daemon Ativar código de depuração Introduza a nova palavra-passe Unix: Erro ao alterar a palavra-passe NIS. Se a proteção de ecrã estiver ativa, desativá-la (o ecrã deixa de estar preto) Palavra-passe incorreta. Iniciar o protetor de ecrã e o programa de bloqueio _Terminar sessão MENSAGEM Mensagem a apresentar no diálogo Não tem permissões para aceder ao sistema. Sem palavra-passe indicada Sem permissão para obter acesso neste momento. Não está em utilização A palavra-passe já foi utilizada anteriormente. Escolha outra. Palavra-passe inalterada Palavra-passe: Consultar à quanto tempo o protetor de ecrã está ativo Consulta o estado do protetor de ecrã Reintroduza a nova palavra-passe Unix: _Alternar utilizador… Proteção de ecrã Apresentar resultados da depuração Apresentar o botão de terminar sessão Apresentar o botão de alternar utilizador As palavras-passe não coincidem Indica ao processo de proteção de ecrã em execução para bloquear o ecrã imediatamente O protetor de ecrã está ativo há %d segundo.
 O protetor de ecrã está ativo há %d segundos.
 O protetor de ecrã está ativado
 O protetor de ecrã está desativado
 O protetor de ecrã atualmente não está ativo.
 Tempo expirou. Ativar a proteção de ecrã (ecrã preto) Incapaz de estabelecer o serviço %s: %s
 Utilizador: Versão desta apa É obrigatório alterar de imediato a sua palavra-passe (palavra-passe antiga) É obrigatório alterar a sua palavra-passe imediatamente (forçado pelo root) Caps Lock ativo. Tem de escolher uma palavra-passe maior Tem de esperar mais antes de poder alterar a sua palavra-passe A sua conta expirou; contacte o administrador do seu sistema _Palavra-passe: _Desbloquear falha ao se registar no bus de mensagens não se encontra ligado ao bus de mensagens proteção de ecrã já se encontra em execução nesta sessão 