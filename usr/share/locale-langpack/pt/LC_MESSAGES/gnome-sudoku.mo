��    $      <  5   \      0     1     ?  
   M     X     h     u     �  �   �     �     �     �  +   �     �       1        :     O     m     t     z     �     �     �     �     �     �     �     �     �     �     �  
   �  	   �            �  7               -     9     L     Y     m  �   �     l	      	     �	  /   �	     �	      
  9   
     A
     [
     w
  	   ~
  	   �
     �
     �
  	   �
     �
     �
     �
     �
     �
  	   �
        
             !    )  )   �                                     $                                                               "          #             	                     !                            
        Create Puzzle Custom Puzzle Difficulty Easy Difficulty GNOME Sudoku Hard Difficulty Height of the window in pixels If you like to play on paper, you can print games out. You can choose how many games you want to print per page and what difficulty of games you want to print: as a result, GNOME Sudoku can act a renewable Sudoku book for you. Medium Difficulty Number of Sudokus to print Play _Again Set the number of sudokus you want to print Show release version Sudoku Test your logic skills in this number grid puzzle Very Hard Difficulty Width of the window in pixels _About _Back _Cancel _Create your own puzzle _Easy _Hard _Help _Medium _New Puzzle _Number of puzzles _Pause _Print _Quit _Resume _Very Hard _Warnings translator-credits true if the window is maximized Project-Id-Version: gnome-sudoku
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-03 22:29+0000
PO-Revision-Date: 2016-04-06 13:33+0000
Last-Translator: Ivo Xavier <ivoxavier.8@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:48+0000
X-Generator: Launchpad (build 18115)
 Criar puzzle Puzzle personalizado Dificuldade Dificuldade fácil Sudoku GNOME Dificuldade elevada Altura da janela em pixels Se gosta de jogar em papel, pode imprimir os jogos. Pode escolher quantos jogos quer imprimir por página e qual a dificuldade dos mesmos. Assim, o GNOME Sudoku pode funcionar como um livro de quebra cabeças sempre renovável. Dificuldade média Número de Sudokus para imprimir _Jogar novamente Defina o número de sudokus que deseja imprimir Mostrar versão de lançamento Sudoku Teste as suas capacidades lógicas neste puzzle numérico Dificuldade muito elevada Largura da janela em pixels _Sobre _Anterior _Cancelar _Criar seu próprio puzzle _Fácil _Difícil _Ajuda _Médio _Novo puzzle _Número de puzzles _Pausa _Imprimir _Sair _Continuar _Muito Difícil _Avisos Launchpad Contributions:
  Filipe André Pinho https://launchpad.net/~pinhoadas
  Ivo Xavier https://launchpad.net/~ivoxavier
  IvoGuerreiro https://launchpad.net/~ivoguerreiro
  Luis Fontes https://launchpad.net/~mail-fontes
  Pedro Albuquerque https://launchpad.net/~pmralbuquerque
  Ricardo Sousa https://launchpad.net/~ricardosousa
  Tiago Silva https://launchpad.net/~tiagosilva verdadeiro se a janela estiver maximizada 