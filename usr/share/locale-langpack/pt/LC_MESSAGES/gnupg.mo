��    �     �  �  �:      �N  -   �N  �   �N     MO     eO     �O     �O     �O     �O     �O  2   P  7   EP  2   }P  D   �P  .   �P  I   $Q     nQ     �Q     �Q     �Q     �Q     �Q     R     -R     JR  +   \R  &   �R  #   �R     �R     �R     S     (S     AS  $   ^S     �S     �S  &   �S     �S  ,   �S     T     <T     OT     lT     ~T     �T     �T  B   �T     	U  (   U  .   EU  3   tU     �U     �U     �U     �U     �U     V  '   )V     QV     mV     �V     �V     �V  "   �V  %   �V  &   W  !   5W  %   WW  "   }W  #   �W  '   �W      �W     X     *X     ?X     \X     qX     �X  (   �X  $   �X     �X     �X  #   Y     5Y     TY     sY  !   �Y     �Y     �Y  ,   �Y  4   Z  �   RZ     -[     D[     Y[     w[     �[  '   �[     �[     �[     \     \     '\      :\  '   [\     �\  "   �\     �\     �\  -   �\  (   ]  0   C]  H   t]  	  �]     �^     �^     �^     �^     _  A   1_  /   s_  0   �_  \   �_  1   1`     c`  D   x`  -   �`  0   �`  .   a  *   Ka  +   va     �a     �a     �a     �a     �a     b     (b  3   ?b     sb  4   {b  -   �b  �   �b  .   �c     �c  $   �c  	   �c     �c  :   d     Hd     gd     �d     �d  ,   �d  (   �d  8   e  #   Je  &   ne  (   �e  &   �e     �e     �e     f     )f  6   2f  -   if  %   �f  3   �f  G   �f  @   9g  >   zg  +   �g  =   �g     #h     +h     ;h     Ch     Uh     kh     {h     �h     �h     �h      �h  /   �h  �   i     �i  H   �i  -   Dj  8   rj  &   �j  5   �j  .   k  5   7k  *   mk  .   �k  0   �k  &   �k  ,   l     Ll  
   fl     ql  8   �l  #   �l     �l     �l  "   m  .   $m  �   Sm  "   �m  (   n  $   8n  E   ]n  �   �n  ;   9o  �   uo     )p     Gp     bp  $   �p     �p     �p  �   �p     �q     �q     �q     �q     r      r     9r     Or     br     rr     �r  %   �r     �r     �r     �r     �r  .   s  �   ?s  *   �s      �s  0   t  <   Pt     �t  "   �t  ,   �t  /   �t  1   (u  ,   Zu  2   �u     �u      �u  (   �u       v  
   Av  *   Lv     wv     �v     �v     �v     �v     �v     �v     w  �   1w  "   �w     �w     �w     x     (x     Fx      fx  1   �x  %   �x     �x  �   �x  E   �y  E   �y  o   *z      �z      �z  �   �z  8   l{  '   �{  >   �{  �   |  c   �|  /    }  N   0}  E   }  ,   �}  "   �}  #   ~  +   9~  )   e~  &   �~  �   �~    �  '   ��  �   Ȁ     [�     {�     ��     ��     ��     Ł     ށ  +   ��  ,   &�  3   S�     ��  1   ��     ł  (   ۂ  #   �  +   (�  "   T�  +   w�  "   ��      ƃ     �  ?   ��  %   ;�     a�     t�     ��     ��     ��     Մ  .   �  (   "�     K�     `�  /   z�  0   ��    ۅ  !   ��     �     �  !   '�     I�  %   b�     ��     ��     ��     ��  j   ؇  �   C�  9   Ј  O   
�  /   Z�  5   ��  .   ��  -   �    �  �   .�     �     
�     %�  !   ;�     ]�  $   y�  (   ��  �   ǌ     ��  -   ۍ  �   	�     َ     ��     �      �  *   =�  +   h�     ��      ��     Ώ     �  #   �     '�  	   B�  %   L�     r�  7   ��     Đ  r   D�  1   ��  =   �  I   '�  4   q�     ��  #   Œ  =   �     '�  ?   G�  1   ��  N   ��  I   �  5   R�  5   ��  H   ��  -   �  H   5�     ~�  <   ��  )   ֕  C    �  :   D�  .   �  <   ��  F   �  2   2�  1   e�  5   ��  *   ͗  7   ��  ,   0�      ]�  2   ~�     ��  h  ��  *   �  &   F�  1   m�  #   ��  2   ß  >   ��  �   5�  *   �  =   �  0   P�  M   ��  "   ϡ  &   �  !   �  3   ;�  %   o�  D  ��  ,   ڤ  5   �     =�     M�  2   ^�  _   ��     �  
   �  	   �     �     '�     8�     D�  %   ]�  "   ��     ��  )   Ʀ  +   �     �     5�     D�     Y�     f�     t�  
   ��     ��     ��     ȧ     Ч     ا     �     �     ��     �     �     +�     B�     I�  (   _�     ��     ��     ��      Ш     �     �  ,   ,�  '   Y�  .   ��  %   ��  2   ֩     	�     �     /�     D�     a�     x�     ��  5   ��     ت     �     ��  ;   
�  <   F�     ��     ��     ��     ֫     �     �     �     5�  :   K�     ��     ��     ��     Ҭ     �  &   ��      �     6�  -   E�  (   s�     ��     ��  /   ѭ     �     �     )�  1   E�     w�     ��     ��     ��     ͮ     �  ,   ��     +�     I�     U�     ^�  
   v�     ��     ��  $   ��  =   ʯ  %   �     .�      G�     h�  #   ��     ��     ư     ݰ     ��     �     3�  '   O�  &   w�     ��  &   ��  &   �     �     +�     3�     ?�     K�     W�  "   s�  K   ��  %   �  $   �  &   -�  $   T�     y�  D   ��     ۳  '   �     
�     �     -�     ?�     K�     [�     k�     }�  $   ��     ��  @   ��     ��      �  $   �     3�  !   K�     m�     �     ��  D   ��  +   �  /   �     K�     T�     r�  "   ��  %   ��  %   Ͷ     �  $   ��      �     1�     ?�  .   V�     ��     ��     ��     ׷     �     �     $�     4�     O�  +   e�     ��     ��  $   ��  &   ظ  $   ��  '   $�  ,   L�  ,   y�     ��     ��  >   ͹      �     -�     E�     e�     ��  "   ��  #   ú      �  !   �     *�     H�     c�     �  $   ��  "   ��  +   �  )   �  #   7�     [�  /   {�     ��  "   ˼     �  "   	�  %   ,�     R�      f�  !   ��  !   ��  (   ˽  +   ��      �  !   =�  4   _�  0   ��     ž     ݾ  )   ��     %�     9�  &   P�  G   w�  H   ��  B   �     K�     k�     ��     ��     ��     ��     ��     �     �     ?�     ^�     v�     ��  	   ��     ��     ��     ��     �     �     .�     H�     M�  .   \�     ��     ��  (   ��  2   ��  !   �      $�     E�  ,   _�     ��     ��     ��     ��  
   ��  $   ��     ��     �  )   �     <�  %   [�  ,   ��     ��  &   ��     ��  1   �     3�     C�     S�     q�     ��  %   ��  %   ��     ��     �     �  
   -�     8�     F�      T�  #   u�  &   ��  )   ��     ��  $   	�  <   .�     k�     ��     ��  ,   ��  ;   ��  M   %�     s�  -   ��     ��     ��     ��     �  "   �  -   A�     o�  +   ��  *   ��     ��  !   ��  $   �     D�     Y�     \�     a�  I   p�  F   ��     �     �     /�     N�     a�  #   y�  #   ��  ,   ��     ��  *   ��  2   (�  .   [�     ��     ��     ��  *   ��     �     
�     (�     ?�     M�  $   m�     ��  #   ��  &   ��     ��     �  3   �  %   K�  *   q�  %   ��  -   ��     ��  0   �     5�  0   J�     {�     ��  
   ��     ��     ��  9   ��     �  5   ,�  -   b�  "   ��     ��      ��     ��     �      �  5   2�  $   h�     ��  4   ��  %   ��  *   �  (   .�  0   W�     ��  .   ��  -   ��  *   �  �   0�  +   ��  2   ��  /    �  %   P�  F   v�     ��  #   ��     ��  (   �     :�  -   O�  /   }�  ,   ��  "   ��  )   ��     '�     F�      a�     ��  #   ��  #   ��  !   ��  -   �     4�  I   S�     ��     ��     ��     ��     �      �     @�  !   Y�     {�     ��     ��     ��  #   ��     ��      �     �     *�     >�      R�     s�     ��     ��     ��     ��     ��  6   ��     2�  #   N�      r�     ��     ��     ��     ��     ��  G   ��     )�     B�      U�     v�     ��     ��  -   ��  ,   ��     �     .�     M�     ^�     z�     ��     ��     ��     ��     ��  D   ��  F   0�  E   w�  D   ��  E   �  A   H�  7   ��  6   ��  7   ��  =   1�  >   o�     ��  $   ��  7   ��  <   (�  7   e�  F   ��     ��     ��  9   �      N�     o�  
   ��  $   ��     ��  #   ��     ��  #   �     *�  *   A�  *   l�  �  ��  D   6�  �   {�     �     1�  #   O�      s�  )   ��     ��  '   ��  7   �  =   >�  8   |�  C   ��  ;   ��  D   5�     z�  "   ��     ��  !   ��      ��  $   �     C�     `�     }�  ,   ��  +   ��  )   ��      �  '   4�     \�      {�     ��     ��     ��     ��     �     /�  *   N�  %   y�     ��  "   ��     ��     ��     �  '   (�  `   P�     ��  /   ��  4   ��  @   2�     s�     {�     ��     ��     ��     ��  6   ��     .�     K�     i�     n�     ��  "   ��  '   ��  -   ��  /   �  ,   M�  #   z�  ,   ��  )   ��  "   ��  %   �  *   >�  1   i�     ��  +   ��     ��  2   ��  1   %�  '   W�     �  &   ��     ��  +   ��  +   �  1   7�  $   i�  +   ��  =   ��  :   ��     3�     4�     S�  #   p�     ��     ��  )   ��  #   ��  %   �     ?�     Y�     r�  (   ��  /   ��     ��  -   �     3�     R�  0   k�  .   ��  9   ��  F   �  -  L�     z�     ��  ,   ��  1   ��  "   ��  :   �  +   R�  6   ~�  p   ��  H   &�     o�  G   ��  @   ��  >   �  D   P�  (   ��  ,   ��  
   ��     ��     �  %   *�  "   P�  "   s�     ��  8   ��  
   ��  9   ��  0   1�  �   b�  7   >�     v�  4   ~�     ��     ��  @   ��  &   �  "   7�  !   Z�  #   |�  9   ��  .   ��  @   	�  '   J�  )   r�  *   ��  ,   ��     ��          (  
   >  5   I  )     !   �  .   �  8   �  5   3 6   i F   � 8   �      "   ' 	   J     T %   u    �    �    �    �    � #    .   4 �   c    . L   H 2   � E   � ,    S   ; 6   � ?   � /    :   6 3   q /   � 3   �    	    $    7 9   Q    �    �    � 7   � @    �   H %   � /   � %   .	 D   T	 �   �	 Q   K
 �   �
 "   c    � !   � 2   �    �     �   '    �        .    D !   c    �    �    �    �    �     <        ] %   j    � 0   � B   � �    .   � "   � H    F   b    � 9   � C   � ?   B A   � 5   � 3   � ,   . )   [ (   � 0   � 
   � :   �    % #   ?    c !   ~ )   � $   � '   �      �   8 "   �    �     !    #   @ +   d &   � =   � 6   � "   , �   O Q   
 T   \ �   � *   L .   w �   � "   > 6   a L   � �   � z   z 5   � V   + B   � 4   � *   � 7   % .   ] .   � (   � �   �   � /   � �    '   �    �    �         *  #   ;     _  .   w  4   �  :   �     ! 6   &!    ]! -   q! ,   �! A   �! <   " >   K" +   �" A   �"    �" S   # 0   b#    �#    �#    �# $   �# '   �# %   #$ >   I$ 1   �$    �$    �$ ;   �$ C   /% 3  s% +   �&    �&    �&    �&    ' 5   .'    d'    ~'    �' "   �' ~   �' �   B( A   ) I   D) 1   �) 9   �) +   �) 1   &*   X* �   w+    R,    k,    �,    �,    �, -   �, .   - 	  5-    ?. 0   _.   �.    �/ $   �/    �/ %   �/ /   0 0   J0    {0 ,   �0    �0 &   �0 .   1 1   =1    o1 &   w1    �1 <   �1 {   �1    o2 .   �2 H   3 ]   g3 1   �3 7   �3 (   /4 I   X4 !   �4 F   �4 ,   5 W   85 R   �5 H   �5 =   ,6 J   j6 9   �6 L   �6    <7 C   S7 /   �7 J   �7 :   8 6   M8 =   �8 b   �8 ;   %9 E   a9 ?   �9 9   �9 D   !: ;   f: %   �: =   �:   ; �  < +   �A 2   �A =   ,B 4   jB B   �B M   �B �   0C -   �C E   ,D I   rD J   �D ,   E 9   4E -   nE ?   �E =   �E �  F (   �H 9   I 
   WI 	   bI 7   lI {   �I /    J    PJ    cJ    sJ    �J 	   �J    �J +   �J /   �J 5   K 3   CK @   wK "   �K &   �K !   L    $L !   ;L    ]L    vL    �L "   �L    �L    �L    �L    �L    	M    "M    <M    UM    kM    M %   �M <   �M    �M    N    &N .   BN &   qN 0   �N S   �N 7   O 6   UO 5   �O @   �O    P    "P    :P '   VP &   ~P    �P +   �P F   �P    2Q 	   QQ    [Q =   vQ N   �Q +   R $   /R ,   TR    �R    �R !   �R !   �R     S D   S     dS    �S    �S    �S    �S .   �S *   T    =T ;   PT &   �T    �T 9   �T A   U    EU 
   eU     pU >   �U     �U %   �U    V    .V     CV (   dV 6   �V '   �V    �V 
   �V    W     W !   .W    PW &   ]W ;   �W "   �W    �W $   �W '    X 4   HX "   }X    �X    �X )   �X )   �X &   (Y 3   OY (   �Y )   �Y 3   �Y 2   
Z (   =Z    fZ    oZ 
   |Z    �Z *   �Z +   �Z `   �Z 7   O[ +   �[ )   �[ *   �[    \ N   &\    u\ /   |\    �\    �\    �\     ]    ]    3]     C]    d] (   t]    �] N   �]    �] 
   �] $   ^    +^ #   G^    k^    ~^    �^ N   �^ 1   �^ 6   -_    d_ (   m_    �_ +   �_ 2   �_ )   ` 	   6` '   @`    h`    |` "   �` 2   �` 1   �`    a &   4a $   [a '   �a $   �a    �a *   �a    b @   -b    nb    b +   �b 1   �b .   �b )   $c /   Nc /   ~c    �c %   �c B   �c 1   (d    Zd $   zd "   �d *   �d $   �d $   e )   7e +   ae !   �e    �e '   �e     �e -   f ,   Ff ?   sf 7   �f 1   �f +   g =   Ig *   �g ,   �g     �g 0    h 1   1h ,   ch $   �h (   �h -   �h 2   i 8   ?i "   xi ,   �i :   �i 6   j    :j )   Wj 5   �j    �j $   �j ,   �j S   k T   ok K   �k !   l    2l    Ll #   gl .   �l 0   �l +   �l )   m /   Am *   qm #   �m ,   �m -   �m    n *   ,n !   Wn    yn $   �n    �n    �n    �n    �n /   	o &   9o    `o &   io (   �o &   �o +   �o !   p 4   .p    cp $   fp    �p    �p 
   �p /   �p :   �p    q 7   q +   Qq 0   }q :   �q E   �q .   /r    ^r ;   ur    �r '   �r (   �r    s :   6s G   qs F   �s      t    !t    ?t    Lt    Zt    jt )   yt ,   �t ,   �t +   �t &   )u 2   Pu @   �u    �u !   �u %   v ?   +v A   kv T   �v %   w :   (w #   cw &   �w $   �w    �w "   �w :   x    Nx 0   kx /   �x &   �x (   �x /   y    Ly    ky    ny    sy R   �y Q   �y    (z    7z %   Oz    uz    �z '   �z &   �z @   �z    3{ (   F{ 0   o{ 7   �{    �{ )   �{ )   | 9   I|    �| !   �|    �|    �| &   �| +   �|    )} *   H} 0   s}    �}    �} C   �} 1   ~ 4   N~ 1   �~ I   �~ #   �~ A   # #   e <   � (   �    �    �    �    0� T   L� )   �� L   ˀ D   � &   ]� (   �� %   ��    Ӂ 
   � .   �� F   $� -   k�    �� =   ��    � -   � ,   >� 8   k� &   �� 7   ˃ 3   � 3   7� �   k� 2   � -   :� 3   h� 3   �� K   Ѕ    � -   4� "   b� *   �� #   �� 4   Ԇ =   	� 7   G� 6   � ?   �� 8   �� .   /� 7   ^� 8   �� .   ψ /   �� 0   .� 9   _� /   �� P   ɉ    � +   ,� -   X� &   �� %   �� )   ӊ "   �� /    �    P� (   o� !   ��    �� &   ϋ    ��    � )   �    E�    e� <   ��    �� +   ܌ (   �    1�    F�    c� A   �� -    >   �� .   /�    ^�    v�    ��    �� 	   �� W   �� !   �    6� %   O� !   u�    �� (   �� 8   ̏ 5   �     ;� +   \�    �� $   �� #   Ɛ    �    �    �    6�    O� D   R� D   �� D   ܑ E   !� J   g� 9   �� 9   � D   &� >   k� F   �� E   � #   7� 2   [� A   �� O   Д >    � K   _�    �� *   ˕ I   �� D   @�    ��    �� 4   ��    ݖ .   ��     &� 1   G�    y� 1   �� 5   ŗ            �  �   �   �  0  O  �  �    Y    �       0  �  �   g  �     ~          T  �          T  ~   r   H         �  �           i  �  �  �   �        �      '  x    w  �          �  �      i          �      �  �  �  )  �   �    /       2    �          �  �   �  �  �  _  �   4  �  o       �   �       K   t  �   �  �       �    �    c       �      `  J  �  �  Q    Z   �   �  �  �  L    �   .         B      �       5    �  �      l     �   -  �           {     :          =  �      �  g          �  �   R          H                  J   *        .  �  �      �  �       ^   �         �  7   �  a  �  �      7  �       v      z  d  �  �   k       �  �          �      m       "       �  �      x      [   �  �                �  �      	  �          n  m  �               �        �      �  �     �   �   [  A     b  ]   �      �  l  *  v               X      +   8  �  %  �  �      �  �   y           B  �      @   $  i  &   ,  �  e  Z                �  �   �   G                h  '     �      O  �    �      @        =      �   �    �      |       j   �  �  �       o             "  l  s  �   �   �   
      i       �  �  �   u  �      �  �  V      �   �   �               �      �  	  �  P                     �          <         �  6   ?      F  �  <  L  �   �          �  !  
              �  (       �  E  m  �    �      �  w   y  h  x   �   �          �          B   s  �  �      �  O   �  9   \  D          M  �  )   �   B      �       �               �   f         ;  �  �  8   �  =   9  �  �   �      Q      {  �  �   �   �  u  ]  �  �      �      s           r  �      G   V     �  �      v  S  '      (  +  �  �       �  �  �  !        <  ~  W  C   a   V        �  ?          �  t       )  �  e  |      �       S   {     E        �  �   W      T   C  �       �  S      ^  �  A          �                  �  #   �   �      $  �            4  �     }  K  =  n  �   �  ]  �  
      �       �   �  6  �        �    Q      |  *   M   :  �  �  k  �         �  o  L     �  	   L      �  �       l   {      U        w  [      K          w        �  Q     F  �   �    �  �  5  �  �                     �  �          �  �          �      �   n  q   �     �  �   /  �   �      �      �      �  H  @  J    �   
      �  !   d  �  3  p  ^  �      T  ,      \  �      q  R  �      �  �  �   >  �   N         �   K  2   �  X       o  �   �  :   �     1  �  3  &              "            �      �    ?  �   �  b    d    �       V              X  �       -   �  A  &      $   ,           �   �  �   e  k  E   `  u        z  -        ;  *          h   7     �   b  �  r  �   N  �  1  `  v  �  6          �  �   ,    �  r  �       �    �         !  �  �          �  F   �     �          �  p  �  �  �   �  p   �  C      �    M  p  �      P   S    �    z        �           c  j  &  }  �  �  (  �  b   �   F  D  �  I  �       R          �    c  -    ^  �  �  	  ]      �  �   �  `       �  �  �  �  �   �      �  t  �      �       %  �          �     �  _  _       �  C          �   U      �   5   s  %           +      #  Y      �      �  c      �  u   �  �  �  �       �   �          �   >  �       �   R            f                  )      j      �      9  �  '  �  �  �   1  �   \       8  J            /  �     2     �   �  �  �  �     �           �  #  �  �  �  z   �   g  �    U    �   M  %  j            .  ~  W   D   �   �      �  �         �      �  �       O              �  m        �   ?       G  �  D  �  �   �   �    �       �   �  6     h  I   :  4   0  0   g  �   �  �  �          �  �       �   �      Y      9  7    �   �  5  k      E                         �  d         �  q                �      �    G  �  f  �      �  2  a  �  (    �              �  �  n   �  �        �   |      _  ;   y  y  x  �  �  �  +  �   �      �                  �  Y   �  ;  $  Z      �      �  �  �     �  Z  N   3          >   X  �  [  �          �  �   �   <  a  �  .      P  �      �      �  �    N      q  �       A  }   #  �  >  �  �     �   �   �      �   3  e   �                 �  �        @  t  �      �    "  f      �  �  �                  �  �  W  4  �     \  �       �  �    �  �           U   H  /   �       �   �  I  �               P      �   �   I  �  }      1   8  �   �  �             �  �                     
Enter the user ID.  End with an empty line:  
Not enough random bytes available.  Please do some other work to give
the OS a chance to collect more entropy! (Need %d more bytes)
 
Supported algorithms:
               imported: %lu              unchanged: %lu
            new subkeys: %lu
           new user IDs: %lu
           not imported: %lu
           w/o user IDs: %lu
          "%s": preference for cipher algorithm %s
          "%s": preference for compression algorithm %s
          "%s": preference for digest algorithm %s
          It is not certain that the signature belongs to the owner.
          The signature is probably a FORGERY.
          There is no indication that the signature belongs to the owner.
         new signatures: %lu
       Card serial no. =       Subkey fingerprint:       secret keys read: %lu
       skipped new keys: %lu
       user IDs cleaned: %lu
      Subkey fingerprint:     signatures cleaned: %lu
    (%c) Finished
    (%c) Toggle the authenticate capability
    (%c) Toggle the encrypt capability
    (%c) Toggle the sign capability
    (%d) DSA (sign only)
    (%d) Elgamal (encrypt only)
    (%d) RSA (encrypt only)
    (%d) RSA (sign only)
    (0) I will not answer.%s
    (1) I have not checked at all.%s
    (1) Signature key
    (2) Encryption key
    (2) I have done casual checking.%s
    (3) Authentication key
    (3) I have done very careful checking.%s
    new key revocations: %lu
   Unable to sign.
   secret keys imported: %lu
  (non-exportable)  (non-revocable)  Primary key fingerprint:  secret keys unchanged: %lu
 %d Admin PIN attempts remaining before card is permanently locked
 %d bad signatures
 %d signatures not checked due to errors
 %d signatures not checked due to missing keys
 %d user IDs without valid self-signatures detected
 %s ...
 %s does not yet work with %s
 %s encrypted data
 %s encryption will be used
 %s is the new one
 %s is the unchanged one
 %s keysizes must be in the range %u-%u
 %s makes no sense with %s!
 %s not allowed with %s!
 %s.
 %s/%s encrypted for: "%s"
 %s: directory does not exist!
 %s: error reading free record: %s
 %s: error reading version record: %s
 %s: error updating version record: %s
 %s: error writing dir record: %s
 %s: error writing version record: %s
 %s: failed to append a record: %s
 %s: failed to create hashtable: %s
 %s: failed to create version record: %s %s: failed to zero a record: %s
 %s: invalid file version %d
 %s: invalid trustdb
 %s: invalid trustdb created
 %s: keyring created
 %s: not a trustdb file
 %s: skipped: %s
 %s: skipped: public key already present
 %s: skipped: public key is disabled
 %s: trustdb created
 %s: unknown suffix
 %s: version record with recnum %lu
 %s:%d: deprecated option "%s"
 %s:%d: invalid export options
 %s:%d: invalid import options
 %s:%d: invalid keyserver options
 %s:%d: invalid list options
 %s:%d: invalid verify options
 (unless you specify the key by fingerprint)
 (you may have used the wrong program for this task)
 * The `sign' command may be prefixed with an `l' for local signatures (lsign),
  a `t' for trust signatures (tsign), an `nr' for non-revocable signatures
  (nrsign), or any combination thereof (ltsign, tnrsign, etc.).
 --clearsign [filename] --decrypt [filename] --edit-key user-id [commands] --encrypt [filename] --lsign-key user-id --output doesn't work for this command
 --sign --encrypt [filename] --sign --symmetric [filename] --sign [filename] --sign-key user-id --store [filename] --symmetric --encrypt [filename] --symmetric --sign --encrypt [filename] --symmetric [filename] -k[v][v][v][c] [user-id] [keyring] ... this is a bug (%s:%d:%s)
 1 bad signature
 1 signature not checked due to a missing key
 1 signature not checked due to an error
 1 user ID without valid self-signature detected
 @
(See the man page for a complete listing of all commands and options)
 @
Examples:

 -se -r Bob [file]          sign and encrypt for user Bob
 --clearsign [file]         make a clear text signature
 --detach-sign [file]       make a detached signature
 --list-keys [names]        show keys
 --fingerprint [names]      show fingerprints
 @
Options:
  @Commands:
  Admin commands are allowed
 Admin commands are not allowed
 Admin-only command
 Answer "yes" (or just "y") if it is okay to generate the sub key. Answer "yes" if it is okay to delete the subkey Answer "yes" if it is okay to overwrite the file Answer "yes" if you really want to delete this user ID.
All certificates are then also lost! Answer "yes" if you want to sign ALL the user IDs Answer "yes" or "no" Are you sure that you want to sign this key with your
key "%s" (%s)
 Are you sure you still want to add it? (y/N)  Are you sure you still want to revoke it? (y/N)  Are you sure you still want to sign it? (y/N)  Are you sure you want to delete it? (y/N)  Are you sure you want to replace it? (y/N)  Authenticate CA fingerprint:  CRC error; %06lX - %06lX
 Can't check signature: %s
 Can't edit this key: %s
 Cardholder's given name:  Cardholder's surname:  Certificates leading to an ultimately trusted key:
 Certify Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit?  Change (N)ame, (C)omment, (E)mail or (Q)uit?  Change the preferences of all user IDs (or just of the selected ones)
to the current list of preferences.  The timestamp of all affected
self-signatures will be advanced by one second.
 Changing expiration time for the primary key.
 Cipher:  Command expects a filename argument
 Comment:  Compression:  Create a revocation certificate for this signature? (y/N)  Critical preferred keyserver:  Critical signature notation:  Critical signature policy:  Current allowed actions:  DSA key %s requires a %u bit or larger hash
 DSA key %s uses an unsafe (%u bit) hash
 DSA requires the hash length to be a multiple of 8 bits
 Delete this good signature? (y/N/q) Delete this invalid signature? (y/N/q) Delete this key from the keyring? (y/N)  Delete this unknown signature? (y/N/q) Deleted %d signature.
 Deleted %d signatures.
 Detached signature.
 Digest:  Do you really want to delete the selected keys? (y/N)  Do you really want to delete this key? (y/N)  Do you really want to do this? (y/N)  Do you really want to revoke the entire key? (y/N)  Do you want to issue a new signature to replace the expired one? (y/N)  Do you want to promote it to a full exportable signature? (y/N)  Do you want to promote it to an OpenPGP self-signature? (y/N)  Do you want to sign it again anyway? (y/N)  Do you want your signature to expire at the same time? (Y/n)  ERROR:  Email address:  Encrypt Enter Admin PIN:  Enter New Admin PIN:  Enter New PIN:  Enter PIN:  Enter new filename Enter passphrase
 Enter passphrase:  Enter the name of the key holder Enter the new passphrase for this secret key.

 Enter the required value as shown in the prompt.
It is possible to enter a ISO date (YYYY-MM-DD) but you won't
get a good error response - instead the system tries to interpret
the given value as an interval. Enter the size of the key Enter the user ID of the addressee to whom you want to send the message. Enter the user ID of the designated revoker:  Error: Combined name too long (limit is %d characters).
 Error: Double spaces are not allowed.
 Error: Login data too long (limit is %d characters).
 Error: Only plain ASCII is currently allowed.
 Error: Private DO too long (limit is %d characters).
 Error: The "<" character may not be used.
 Error: URL too long (limit is %d characters).
 Error: invalid characters in preference string.
 Error: invalid formatted fingerprint.
 Error: invalid length of preference string.
 Error: invalid response.
 Features:  File `%s' exists.  Give the name of the file to which the signature applies Go ahead and type your message ...
 Good signature from "%s" Hash:  Hint: Select the user IDs to sign
 Hit return when ready or enter 'c' to cancel:  How carefully have you verified the key you are about to sign actually belongs
to the person named above?  If you don't know what to answer, enter "0".
 I have checked this key casually.
 I have checked this key very carefully.
 I have not checked this key at all.
 IDEA cipher unavailable, optimistically attempting to use %s instead
 If you like, you can enter a text describing why you issue this
revocation certificate.  Please keep this text concise.
An empty line ends the text.
 If you want to use this untrusted key anyway, answer "yes". In general it is not a good idea to use the same key for signing and
encryption.  This algorithm should only be used in certain domains.
Please consult your security expert first. Invalid character in comment
 Invalid character in name
 Invalid command  (try "help")
 Invalid passphrase; please try again Invalid selection.
 Is this correct? (y/N)  It's up to you to assign a value here; this value will never be exported
to any 3rd party.  We need it to implement the web-of-trust; it has nothing
to do with the (implicitly created) web-of-certificates. Key %s is already revoked.
 Key does not expire at all
 Key expires at %s
 Key generation canceled.
 Key generation failed: %s
 Key has been compromised Key is no longer used Key is protected.
 Key is revoked. Key is superseded Key is valid for? (0)  Key not changed so no update needed.
 Keyring Keyserver no-modify Language preferences:  Login data (account name):  Make off-card backup of encryption key? (Y/n)  N  to change the name.
C  to change the comment.
E  to change the email address.
O  to continue with key generation.
Q  to quit the key generation. NOTE: %s is not available in this version
 NOTE: %s is not for normal use!
 NOTE: a key's S/N does not match the card's one
 NOTE: creating subkeys for v3 keys is not OpenPGP compliant
 NOTE: key has been revoked NOTE: no default option file `%s'
 NOTE: old default options file `%s' ignored
 NOTE: primary key is online and stored on card
 NOTE: secondary key is online and stored on card
 NOTE: sender requested "for-your-eyes-only"
 NOTE: simple S2K mode (0) is strongly discouraged
 NOTE: trustdb not writable
 Name may not start with a digit
 Name must be at least 5 characters long
 Need the secret key to do this.
 NnCcEeOoQq No corresponding signature in secret ring
 No help available No help available for `%s' No reason specified No subkey with index %d
 No such user ID.
 No user ID with hash %s
 No user ID with index %d
 Not a valid email address
 Note that this key cannot be used for encryption.  You may want to use
the command "--edit-key" to generate a subkey for this purpose.
 Note: This key has been disabled.
 Note: This key has expired!
 Nothing deleted.
 Nothing to sign with key %s
 OpenPGP card no. %s detected
 OpenPGP card not available: %s
 PIN callback returned error: %s
 PIN for CHV%d is too short; minimum length is %d
 PIN not correctly repeated; try again Please correct the error first
 Please decide how far you trust this user to correctly verify other users' keys
(by looking at passports, checking fingerprints from different sources, etc.)
 Please don't put the email address into the real name or the comment
 Please enter a domain to restrict this signature, or enter for none.
 Please enter a new filename. If you just hit RETURN the default
file (which is shown in brackets) will be used. Please enter an optional comment Please enter name of data file:  Please enter the depth of this trust signature.
A depth greater than 1 allows the key you are signing to make
trust signatures on your behalf.
 Please enter the passphrase; this is a secret sentence 
 Please fix this possible security flaw
 Please insert the card and hit return or enter 'c' to cancel:  Please note that the factory settings of the PINs are
   PIN = `%s'     Admin PIN = `%s'
You should change them using the command --change-pin
 Please note that the shown key validity is not necessarily correct
unless you restart the program.
 Please remove selections from the secret keys.
 Please remove the current card and insert the one with serial number:
   %.*s
 Please repeat the last passphrase, so you are sure what you typed in. Please report bugs to <gnupg-bugs@gnu.org>.
 Please select at most one subkey.
 Please select exactly one user ID.
 Please select the type of key to generate:
 Please select what kind of key you want:
 Please select where to store the key:
 Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
 Please specify how long the signature should be valid.
         0 = signature does not expire
      <n>  = signature expires in n days
      <n>w = signature expires in n weeks
      <n>m = signature expires in n months
      <n>y = signature expires in n years
 Please use the command "toggle" first.
 Please wait, entropy is being gathered. Do some work if it would
keep you from getting bored, because it will improve the quality
of the entropy.
 Possible actions for a %s key:  Preferred keyserver:  Primary key fingerprint: Private DO data:  Pubkey:  Public key is disabled.
 Quit without saving? (y/N)  RSA modulus missing or not of size %d bits
 RSA prime %s missing or not of size %d bits
 RSA public exponent missing or larger than %d bits
 Real name:  Really create the revocation certificates? (y/N)  Really create? (y/N)  Really delete this self-signature? (y/N) Really move the primary key? (y/N)  Really remove all selected user IDs? (y/N)  Really remove this user ID? (y/N)  Really revoke all selected user IDs? (y/N)  Really revoke this user ID? (y/N)  Really sign all user IDs? (y/N)  Really sign? (y/N)  Really update the preferences for the selected user IDs? (y/N)  Really update the preferences? (y/N)  Repeat passphrase
 Repeat passphrase:  Repeat this PIN:  Replace existing key? (y/N)  Replace existing keys? (y/N)  Requested keysize is %u bits
 Reset Code is too short; minimum length is %d
 Reset Code not or not anymore available
 Save changes? (y/N)  Secret key is available.
 Secret parts of primary key are not available.
 Secret parts of primary key are stored on-card.
 Select the algorithm to use.

DSA (aka DSS) is the Digital Signature Algorithm and can only be used
for signatures.

Elgamal is an encrypt-only algorithm.

RSA may be used for signatures or encryption.

The first (primary) key must always be a key which is capable of signing. Sex ((M)ale, (F)emale or space):  Sign Sign it? (y/N)  Signature does not expire at all
 Signature expires at %s
 Signature made %s using %s key ID %s
 Signature notation:  Signature policy:  SsEeAaQq Subkey %s is already revoked.
 Syntax: gpg [options] [files]
Sign, check, encrypt or decrypt
Default operation depends on the input data
 The random number generator is only a kludge to let
it run - it is in no way a strong RNG!

DON'T USE ANY DATA GENERATED BY THIS PROGRAM!!

 The self-signature on "%s"
is a PGP 2.x-style signature.
 The signature is not valid.  It does make sense to remove it from
your keyring. The signature will be marked as non-revocable.
 There are no preferences on a PGP 2.x-style user ID.
 This command is not allowed while in %s mode.
 This is a secret key! - really delete? (y/N)  This is a signature which binds the user ID to the key. It is
usually not a good idea to remove such a signature.  Actually
GnuPG might not be able to use this key anymore.  So do this
only if this self-signature is for some reason not valid and
a second one is available. This is a valid signature on the key; you normally don't want
to delete this signature because it may be important to establish a
trust connection to the key or another key certified by this key. This key belongs to us
 This key has been disabled This key has expired! This key is due to expire on %s.
 This key is not protected.
 This key may be revoked by %s key %s This key was revoked on %s by %s key %s
 This signature can't be checked because you don't have the
corresponding key.  You should postpone its deletion until you
know which key was used because this signing key might establish
a trust connection through another already certified key. This signature expired on %s.
 This would make the key unusable in PGP 2.x.
 To build the Web-of-Trust, GnuPG needs to know which keys are
ultimately trusted - those are usually the keys for which you have
access to the secret key.  Answer "yes" to set this key to
ultimately trusted
 Total number processed: %lu
 URL to retrieve public key:  Uncompressed Unknown signature type `%s'
 Usage: gpg [options] [files] (-h for help) Usage: gpgv [options] [files] (-h for help) User ID "%s" is expired. User ID "%s" is not self-signed. User ID "%s" is revoked. User ID "%s" is signable.   User ID "%s": %d signature removed
 User ID is no longer valid WARNING:  WARNING: "%s" is a deprecated option
 WARNING: %s overrides %s
 WARNING: 2 files with confidential information exists.
 WARNING: This is a PGP 2.x-style key.  Adding a designated revoker may cause
         some versions of PGP to reject this key.
 WARNING: This is a PGP2-style key.  Adding a photo ID may cause some versions
         of PGP to reject this key.
 WARNING: This key has been revoked by its owner!
 WARNING: This key is not certified with a trusted signature!
 WARNING: This key is not certified with sufficiently trusted signatures!
 WARNING: This subkey has been revoked by its owner!
 WARNING: Using untrusted key!
 WARNING: We do NOT trust this key!
 WARNING: Weak key detected - please change passphrase again.
 WARNING: `%s' is an empty file
 WARNING: a user ID signature is dated %d seconds in the future
 WARNING: encrypted message has been manipulated!
 WARNING: forcing compression algorithm %s (%d) violates recipient preferences
 WARNING: forcing symmetric cipher %s (%d) violates recipient preferences
 WARNING: invalid size of random_seed file - not used
 WARNING: key %s contains preferences for unavailable
 WARNING: message was encrypted with a weak key in the symmetric cipher.
 WARNING: message was not integrity protected
 WARNING: multiple signatures detected.  Only the first will be checked.
 WARNING: nothing exported
 WARNING: options in `%s' are not yet active during this run
 WARNING: program may create a core file!
 WARNING: recipients (-r) given without using public key encryption
 WARNING: secret key %s does not have a simple SK checksum
 WARNING: signature digest conflict in message
 WARNING: the signature will not be marked as non-revocable.
 WARNING: unable to %%-expand notation (too large).  Using unexpanded.
 WARNING: unable to remove temp directory `%s': %s
 WARNING: unable to remove tempfile (%s) `%s': %s
 WARNING: unsafe ownership on configuration file `%s'
 WARNING: unsafe ownership on homedir `%s'
 WARNING: unsafe permissions on configuration file `%s'
 WARNING: unsafe permissions on homedir `%s'
 WARNING: using insecure memory!
 WARNING: using insecure random number generator!!
 We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
 When you sign a user ID on a key, you should first verify that the key
belongs to the person named in the user ID.  It is useful for others to
know how carefully you verified this.

"0" means you make no particular claim as to how carefully you verified the
    key.

"1" means you believe the key is owned by the person who claims to own it
    but you could not, or did not verify the key at all.  This is useful for
    a "persona" verification, where you sign the key of a pseudonymous user.

"2" means you did casual verification of the key.  For example, this could
    mean that you verified the key fingerprint and checked the user ID on the
    key against a photo ID.

"3" means you did extensive verification of the key.  For example, this could
    mean that you verified the key fingerprint with the owner of the key in
    person, and that you checked, by means of a hard to forge document with a
    photo ID (such as a passport) that the name of the key owner matches the
    name in the user ID on the key, and finally that you verified (by exchange
    of email) that the email address on the key belongs to the key owner.

Note that the examples given above for levels 2 and 3 are *only* examples.
In the end, it is up to you to decide just what "casual" and "extensive"
mean to you when you sign other keys.

If you don't know what the right answer is, answer "0". You are about to revoke these signatures:
 You are using the `%s' character set.
 You can't change the expiration date of a v3 key
 You can't delete the last user ID!
 You did not specify a user ID. (you may use "-r")
 You don't want a passphrase - this is probably a *bad* idea!

 You don't want a passphrase - this is probably a *bad* idea!
I will do it anyway.  You can change your passphrase at any time,
using this program with the option "--edit-key".

 You have signed these user IDs on key %s:
 You may not add a designated revoker to a PGP 2.x-style key.
 You may not add a photo ID to a PGP2-style key.
 You may not make an OpenPGP signature on a PGP 2.x key while in --pgp2 mode.
 You must select at least one key.
 You must select at least one user ID.
 You must select exactly one key.
 You need a Passphrase to protect your secret key.

 You selected this USER-ID:
    "%s"

 You should specify a reason for the certification.  Depending on the
context you have the ability to choose from this list:
  "Key has been compromised"
      Use this if you have a reason to believe that unauthorized persons
      got access to your secret key.
  "Key is superseded"
      Use this if you have replaced this key with a newer one.
  "Key is no longer used"
      Use this if you have retired this key.
  "User ID is no longer valid"
      Use this to state that the user ID should not longer be used;
      this is normally used to mark an email address invalid.
 Your current signature on "%s"
has expired.
 Your current signature on "%s"
is a local signature.
 Your decision?  Your selection?  Your selection? (enter `?' for more information):  Your system can't display dates beyond 2038.
However, it will be correctly handled up to 2106.
 [User ID not found] [filename] [not set] [revocation] [self-signature] [uncertain] `%s' already compressed
 `%s' is not a regular file - ignored
 `%s' is not a valid character set
 `%s' is not a valid long keyID
 `%s' is not a valid signature expiration
 access to admin commands is not configured
 add a key to a smartcard add a photo ID add a revocation key add a subkey add a user ID armor header:  armor: %s
 assuming %s encrypted data
 assuming signed data in `%s'
 bad MPI bad URI bad certificate bad key bad passphrase bad public key bad secret key bad signature be somewhat more quiet binary caching keyring `%s'
 can't access %s - invalid OpenPGP card?
 can't close `%s': %s
 can't connect to `%s': %s
 can't create `%s': %s
 can't create directory `%s': %s
 can't disable core dumps: %s
 can't do this in batch mode
 can't do this in batch mode without "--yes"
 can't gen prime with pbits=%u qbits=%u
 can't generate a prime with less than %d bits
 can't handle public key algorithm %d
 can't handle text lines longer than %d characters
 can't lock `%s': %s
 can't open `%s'
 can't open `%s': %s
 can't open signed data `%s'
 can't open the keyring can't read `%s': %s
 can't stat `%s': %s
 can't use a symmetric ESK packet due to the S2K mode
 can't write `%s': %s
 canceled cancelled by user
 cannot appoint a PGP 2.x style key as a designated revoker
 cannot avoid weak key for symmetric cipher; tried %d times!
 card is permanently locked!
 card reader not available
 change URL to retrieve key change a CA fingerprint change a card's PIN change card holder's name change card holder's sex change data on a card change the expiration date for the key or selected subkeys change the language preferences change the login name change the ownertrust change the passphrase check signatures checking created signature failed: %s
 checking the trustdb
 checksum error cipher algorithm %d%s is unknown or disabled
 completes-needed must be greater than 0
 conflicting commands
 could not parse keyserver URL
 create a public key when importing a secret key create ascii armored output created: %s creation timestamp missing
 data not saved; use option "--output" to save it
 dearmoring failed: %s
 decrypt data (default) decryption failed: %s
 decryption okay
 delete selected subkeys delete selected user IDs delete signatures from the selected user IDs deleting keyblock failed: %s
 disable key disabled do not make any changes enable key enarmoring failed: %s
 encrypt data encrypted with unknown algorithm %d
 encrypting a message in --pgp2 mode requires the IDEA cipher
 encryption only with symmetric cipher error creating `%s': %s
 error creating keyring `%s': %s
 error creating passphrase: %s
 error getting current key info: %s
 error getting new PIN: %s
 error in trailer line
 error reading `%s': %s
 error reading application data
 error reading fingerprint DO
 error reading keyblock: %s
 error reading secret keyblock "%s": %s
 error retrieving CHV status from card
 error writing keyring `%s': %s
 error writing public keyring `%s': %s
 error writing secret keyring `%s': %s
 existing key will be replaced
 expired expired: %s expires: %s export keys export keys to a key server exporting secret keys not allowed
 external program calls are disabled due to unsafe options file permissions
 failed to initialize the TrustDB: %s
 failed to rebuild keyring cache: %s
 failed to store the creation date: %s
 failed to store the fingerprint: %s
 failed to store the key: %s
 failed to use default PIN as %s: %s - disabling further default use
 female fetch the key specified in the card URL file close error file create error file delete error file exists file open error file read error file rename error file write error flag the selected user ID as primary forced forcing symmetric cipher %s (%d) violates recipient preferences
 full general error generate PGP 2.x compatible messages generate a new key pair generate a revocation certificate generate new keys generating key failed
 generating new key
 generating the deprecated 16-bit checksum for secret key protection
 gpg-agent is not available in this session
 gpg-agent protocol version %d is not supported
 iImMqQsS import keys from a key server import/merge keys importing secret keys not allowed
 input line %u too long or missing LF
 input line longer than %d characters
 invalid invalid S2K mode; must be 0, 1 or 3
 invalid argument invalid armor invalid armor header:  invalid armor: line longer than %d characters
 invalid clearsig header
 invalid dash escaped line:  invalid default preferences
 invalid export options
 invalid hash algorithm `%s'
 invalid import options
 invalid keyring invalid keyserver options
 invalid list options
 invalid min-cert-level; must be 1, 2, or 3
 invalid packet invalid passphrase invalid personal cipher preferences
 invalid personal compress preferences
 invalid personal digest preferences
 invalid radix64 character %02X skipped
 invalid root packet detected in proc_tree()
 invalid structure of OpenPGP card (DO 0x93)
 invalid value
 invalid verify options
 it is strongly suggested that you update your preferences and
 key "%s" not found on keyserver
 key "%s" not found: %s
 key %s: "%s" %d new signatures
 key %s: "%s" %d new subkeys
 key %s: "%s" %d new user IDs
 key %s: "%s" %d signature cleaned
 key %s: "%s" %d signatures cleaned
 key %s: "%s" %d user ID cleaned
 key %s: "%s" %d user IDs cleaned
 key %s: "%s" 1 new signature
 key %s: "%s" 1 new subkey
 key %s: "%s" 1 new user ID
 key %s: "%s" not changed
 key %s: PGP 2.x style key - skipped
 key %s: already in secret keyring
 key %s: can't locate original keyblock: %s
 key %s: can't read original keyblock: %s
 key %s: direct key signature added
 key %s: doesn't match our copy
 key %s: invalid self-signature on user ID "%s"
 key %s: invalid subkey binding
 key %s: invalid subkey revocation
 key %s: new key - skipped
 key %s: no subkey for key binding
 key %s: no subkey for key revocation
 key %s: no user ID
 key %s: not protected - skipped
 key %s: public key "%s" imported
 key %s: public key not found: %s
 key %s: removed multiple subkey binding
 key %s: removed multiple subkey revocation
 key %s: secret key imported
 key %s: secret key not found: %s
 key %s: secret key with invalid cipher %d - skipped
 key %s: secret key without public key - skipped
 key %s: skipped subkey
 key %s: skipped user ID "%s"
 key %s: unsupported public key algorithm
 key already exists
 key export failed: %s
 key generation completed (%d seconds)
 key has been created %lu second in future (time warp or clock problem)
 key has been created %lu seconds in future (time warp or clock problem)
 key is not flagged as insecure - can't use it with the faked RNG!
 key operation not possible: %s
 keyring `%s' created
 keyserver error keyserver internal error
 keyserver receive failed: %s
 keyserver refresh failed: %s
 keyserver search failed: %s
 keyserver send failed: %s
 keysize invalid; using %u bits
 keysize rounded up to %u bits
 list all available data list and check key signatures list key and user IDs list keys list keys and fingerprints list keys and signatures list preferences (expert) list preferences (verbose) list secret keys make a detached signature male malformed CRC
 malformed GPG_AGENT_INFO environment variable
 malformed user id marginal marginals-needed must be greater than 1
 max-cert-depth must be in the range from 1 to 255
 menu to change or unblock the PIN move a backup key to a smartcard move a key to a smartcard moving a key signature to the correct place
 nN nested clear text signatures
 network error never never      new configuration file `%s' created
 next trustdb check due at %s
 no no = sign found in group definition `%s'
 no default secret keyring: %s
 no entropy gathering module detected
 no keyserver known (use option --keyserver)
 no need for a trustdb check
 no remote program execution supported
 no secret key
 no secret subkey for public subkey %s - ignoring
 no signed data
 no such user id no valid OpenPGP data found.
 no valid addressees
 no writable keyring found: %s
 no writable public keyring found: %s
 no writable secret keyring found: %s
 not a detached signature
 not an OpenPGP card not encrypted not forced not processed not supported note: random_seed file is empty
 note: random_seed file not updated
 okay, we are the anonymous recipient.
 old encoding of the DEK is not supported
 old style (PGP 2.x) signature
 only accept updates to existing keys operation is not possible without initialized secure memory
 option file `%s': %s
 original file name='%.*s'
 ownertrust information cleared
 passphrase not correctly repeated; try again please enter an optional but highly suggested email address please see http://www.gnupg.org/documentation/faqs.html for more information
 please use "%s%s" instead
 please wait while key is being generated ...
 premature eof (in CRC)
 premature eof (in trailer)
 premature eof (no CRC)
 print the card status problem handling encrypted packet
 problem with the agent - disabling agent use
 prompt before overwriting protection algorithm %d%s is not supported
 public and secret key created and signed.
 public key %s not found: %s
 public key decryption failed: %s
 public key encrypted data: good DEK
 public key not found qQ quit quit this menu quoted printable character in armor - probably a buggy MTA has been used
 re-distribute this key to avoid potential algorithm mismatch problems
 reading from `%s'
 reading options from `%s'
 reading public key failed: %s
 reading stdin ...
 reason for revocation:  remove keys from the public keyring remove keys from the secret keyring remove unusable parts from key during export resource limit response does not contain the RSA modulus
 response does not contain the RSA public exponent
 response does not contain the public key data
 revocation comment:  revoke key or selected subkeys revoke selected user IDs revoke signatures on the selected user IDs revoked revoked by your key %s on %s
 rounded up to %u bits
 save and quit search for keys on a key server secret key already stored on a card
 secret key not available secret key parts are not available
 secret parts of key are not available
 select subkey N select user ID N selected certification digest algorithm is invalid
 selected cipher algorithm is invalid
 selected compression algorithm is invalid
 selected digest algorithm is invalid
 set preference list for the selected user IDs show admin commands show all notations during signature verification show key fingerprint show revoked and expired subkeys in key listings show selected photo IDs show this help sign a key sign a key locally sign or edit a key sign selected user IDs [* see below for related commands] sign selected user IDs locally sign selected user IDs with a non-revocable signature sign selected user IDs with a trust signature signature verification suppressed
 signatures created so far: %lu
 signed by your key %s on %s%s%s
 signing failed: %s
 signing: skipped: public key already set
 skipped: public key already set as default recipient
 skipped: secret key already present
 skipping block of type %d
 standalone revocation - use "gpg --import" to apply
 standalone signature of class 0x%02x
 subpacket of type %d has critical bit set
 symmetric encryption of `%s' failed: %s
 system error while calling external program: %s
 take the keys from this keyring the given certification policy URL is invalid
 the given preferred keyserver URL is invalid
 the given signature policy URL is invalid
 the signature could not be verified.
Please remember that the signature file (.sig or .asc)
should be the first file given on the command line.
 there is a secret key for public key "%s"!
 this key has already been designated as a revoker
 this may be caused by a missing self-signature
 this message may not be usable by %s
 this platform requires temporary files when calling external programs
 timestamp conflict toggle the signature force PIN flag too many cipher preferences
 too many entries in pk cache - disabled
 trust database error trust record %lu is not of requested type %d
 trust record %lu, req type %d: read failed: %s
 trust record %lu, type %d: write failed: %s
 trustdb rec %lu: lseek failed: %s
 trustdb rec %lu: write failed (n=%d): %s
 trustdb transaction too large
 trustdb: lseek failed: %s
 trustdb: read failed (n=%d): %s
 trustdb: sync failed: %s
 unable to execute external program
 unable to execute program `%s': %s
 unable to execute shell `%s': %s
 unable to read external program response: %s
 unable to set exec-path to %s
 unable to use the IDEA cipher for all of the keys you are encrypting to.
 unexpected data unimplemented cipher algorithm unimplemented pubkey algorithm unknown cipher algorithm unknown compress algorithm unknown configuration item `%s'
 unknown digest algorithm unknown key protection algorithm
 unknown packet type unknown pubkey algorithm unknown signature class unknown version unnatural exit of external program
 unspecified unsupported URI unusable pubkey algorithm unusable public key unusable secret key update all keys from a keyserver update failed: %s
 update secret failed: %s
 update the trust database usage: gpg [options]  use as output file use canonical text mode use option "--delete-secret-keys" to delete it first.
 use strict OpenPGP behavior use this user-id to sign or decrypt user ID "%s" is already revoked
 user ID: "%s"
 using cipher %s
 using default PIN as %s
 validity: %s verbose verification of Admin PIN is currently prohibited through this command
 verify CHV%d failed: %s
 verify a signature verify the PIN and list all data waiting for lock on `%s'...
 weak key weak key created - retrying
 weird size for an encrypted session key (%d)
 will not run with insecure memory due to %s
 writing direct signature
 writing key binding signature
 writing new key
 writing public key to `%s'
 writing secret key to `%s'
 writing self signature
 writing to `%s'
 writing to stdout
 wrong secret key used yY you can only clearsign with PGP 2.x style keys while in --pgp2 mode
 you can only detach-sign with PGP 2.x style keys while in --pgp2 mode
 you can only encrypt to RSA keys of 2048 bits or less in --pgp2 mode
 you can only make detached or clear signatures while in --pgp2 mode
 you can update your preferences with: gpg --edit-key %s updpref save
 you can't sign and encrypt at the same time while in --pgp2 mode
 you cannot appoint a key as its own designated revoker
 you cannot use --symmetric --encrypt while in %s mode
 you cannot use --symmetric --encrypt with --s2k-mode 0
 you cannot use --symmetric --sign --encrypt while in %s mode
 you cannot use --symmetric --sign --encrypt with --s2k-mode 0
 you found a bug ... (%s:%d)
 you may not use %s while in %s mode
 you may not use cipher algorithm `%s' while in %s mode
 you may not use compression algorithm `%s' while in %s mode
 you may not use digest algorithm `%s' while in %s mode
 you must use files (and not a pipe) when working with --pgp2 enabled.
 |AN|New Admin PIN |A|Please enter the Admin PIN |A|Please enter the Admin PIN%%0A[remaining attempts: %d] |FD|write status info to this FD |NAME|encrypt for NAME |N|New PIN |N|set compress level N (0 disables) |RN|New Reset Code |[file]|make a clear text signature |[file]|make a signature |algo [files]|print message digests ||Please enter the PIN ||Please enter the PIN%%0A[sigs done: %lu] ||Please enter the Reset Code for the card Project-Id-Version: gnupg
Report-Msgid-Bugs-To: translations@gnupg.org
POT-Creation-Date: 2015-12-20 08:53+0100
PO-Revision-Date: 2016-02-15 18:31+0000
Last-Translator: Pedro Morais <Unknown>
Language-Team: pt <morais@kde.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:42+0000
X-Generator: Launchpad (build 18115)
Language: pt
 
Insira o identificador do utilizador. Termine com uma linha vazia:  
Não há bytes aleatórios suficientes. Por favor, faça outro trabalho para
que o sistema possa recolher mais entropia! (São necessários mais %d bytes)
 
Algoritmos suportados:
               importados: %lu              não modificados: %lu
            novas subchaves: %lu
           novos IDs de utilizadores: %lu
           não importadas: %lu
           sem IDs de utilizadores: %lu
          "%s": preferência para algoritmo cifragem %s
          "%s": preferência para algoritmo de compressão %s
          "%s": Preferência para algoritmo de digest %s
          Não se tem certeza de que a assinatura pertence ao dono.
          A assinatura é provavelmente uma FALSIFICAÇÃO.
          Não há indicação de que a assinatura pertence ao dono.
         novas assinaturas: %lu
       Numero de série do cartão=       Impressão da subchave:       chaves secretas lidas: %lu
       ignorei novas chaves: %lu
       IDs de utilizador limpos: %lu
      Impressão da subchave:     Assinaturas limpas: %lu
    (%c) terminado
    (%c) Alternar a capacidade de autenticar
    (%c) Alternar a capacidade de encriptar
    (%c) Alternar a capacidade de assinar
    (%d) DSA (apenas assinatura)
    (%d) Elgamal (apenas encriptação)
    (%d) RSA (apenas cifragem)
    (%d) RSA (apenas assinatura)
    (0) Não vou responder.%s
    (1) Não verifiquei.%s
    (1) Chave de assinatura
    (2) Chave de encriptação
    (2) Verifiquei por alto.%s
    3) Chave de autenticação
    (3) Verifiquei com bastante cuidado.%s
    novas revogações de chaves: %lu
   Não foi possível assinar.
   chaves secretas importadas: %lu
  (não-exportável)  (não-revogável)  Impressão da chave primária:  chaves secretas não modificadas: %lu
 Restam %d tentativa(s) do PIN de administração antes do cartão ser permanentemente bloqueado
 %d assinaturas incorrectas
 %d assinaturas não verificadas devido a erros
 %d assinaturas não verificadas por falta de chaves
 %d IDs de utilizadores sem auto-assinaturas válidas detectados
 %s ...
 %s ainda não trabalha com %s
 dados cifrados com %s
 será utilizada a cifragem %s
 %s é o novo
 %s é o não modificado
 %s o tamanho das chaves deve estar no intervalo %u-%u
 %s não faz sentido com %s!
 %s não é permitido com %s!
 %s.
 %s/%s cifrado para: "%s"
 %s: diretoria inexistente!
 %s: erro ao ler registo livre: %s
 %s: erro ao ler registo de versão: %s
 %s: erro a actualizar registo de versão: %s
 %s: erro ao escrever registo de diretório: %s
 %s: erro ao escrever registo de versão: %s
 %s: falha ao anexar um registo: %s
 %s: falha ao criar tabela de dispersão: %s
 %s: falha ao criar registo de versão: %s %s: falha ao zerar um registo: %s
 %s: versão de ficheiro inválida %d
 %s: base de dados de confiança inválida
 %s: base de dados de confiança inválida criada
 %s: porta-chaves criado
 %s: não é um base de dados de confiança
 %s: ignorado: %s
 %s: ignorado: a chave pública já está presente
 %s: ignorado: a chave pública está desactivada
 %s: base de dados de confiança criada
 %s: sufixo desconhecido
 %s: registo de versão com recnum %lu
 %s:%d: opção depreciada "%s"
 %s:%d: opções de exportação inválidas
 %s:%d: opções de importação inválidas
 %s:%d: opções do servidor de chaves inválidas
 %s:%d: lista de opções inválidas
 %s:%d: opção de verificação inválidas
 (a não ser que escolha a chave pela sua impressão digital)
 (você pode ter usado o programa errado para esta tarefa)
 *  O comando `sign' pode ser adicionado de um prefixo `l' para assinaturas locais (lsign),
  um `t' para assinaturas de confiança (tsign), um `nr' para assinaturas não revogáveis
  (nrsign), ou com qualquer um destes combinados (ltsign, tnrsign, etc.).
 --clearsign [nome_do_ficheiro] --decrypt [nome_do_ficheiro] --edit-key id-utilizador [comandos] --encrypt [nome_do_ficheiro] --lsign-key id-utilizador --output não funciona para este comando
 --sign --encrypt [nome_do_ficheiro] --sign --symmetric [nome_do_ficheiro] --sign [nome_do_ficheiro] --sign-key id-utilizador --store [nome_do_ficheiro] --symmetric --encrypt [nome_do_ficheiro] --symmetric --sign --encrypt [nome do ficheiro] --symmetric [nome_do_ficheiro] -k[v][v][v][c] [id-utilizador] [porta-chaves] ... isto é um bug (%s:%d:%s)
 1 assinatura incorrecta
 1 assinatura não verificada por falta de chave
 1 assinatura não verificada devido a um erro
 1 ID de utilizador sem auto-assinatura válida detectado
 @
(Veja a página man para uma lista completa de comandos e opções)
 @
Exemplos:

 -se -r Bob [ficheiro]      assinar e cifrar para o utilizador Bob
 --clearsign [ficheiro]     criar uma assinatura em texto puro
 --detach-sign [ficheiro]   criar uma assinatura separada
 --list-keys [nomes]        mostrar chaves
 --fingerprint [nomes]      mostrar impressões digitais
 @
Opções:
  @Comandos:
  Comandos de administração são permitidos
 Comandos de administração não são permitidos
 Comando apenas de administração
 Responda "sim" (ou apenas "s") se quiser gerar a subchave. Responda "sim" se quiser remover a subchave Responda "sim" se quiser escrever por cima do ficheiro Responda "sim" se quiser realmente remover este ID de utilizador.
Todos os certificados também serão perdidos! Responda "sim" se quiser assinar TODAS as identificações de utilizador Responda "sim" ou "não" Tem a certeza de que quer assinar esta chave com a sua
chave "%s" (%s)
 Tem a certeza de que quer adicioná-la de qualquer forma? (s/N)  Tem a certeza de que quer revogá-la de qualquer forma? (s/N)  Você tem certeza de que quer adicioná-la de qualquer forma? (s/N)  Tem a certeza que quer apagá-la? (y/N)  Tem a certeza que quer substituí-la? (y/N)  Autenticar impressão digital CA:  erro de CRC; %06lX - %06lX
 Impossível verificar assinatura: %s
 Impossível editar esta chave: %s
 Primeiro nome do dono do cartão:  Apelido do dono do cartão:  Certificados que levam a uma chave confiada plenamente:
 Certificar Mudar (N)ome, (C)omentário, (E)ndereço ou (O)k/(S)air?  Mudar (N)ome, (C)omentário, (E)mail ou (S)air?  Muda as preferências de todos os identificadores de utilizadores
(ou apenas dos seleccionados) para a lista actual de preferências.
O 'timestamp' de todas as auto-assinaturas afectuadas será avançado
em um segundo.
 Modificar a data de validade para uma chave primária.
 Cifra:  O comando espera um nome de ficheiro como argumento
 Comentário:  Compressão:  Gerar um certificado de revogação para esta assinatura? (s/N)  Servidor de chaves critico preferido:  Notação de assinatura crítica:  Politica de assinatura crítica:  Acções permitidas presentemente:  A chave DSA %s necessita de uma hash de %u bits ou maior
 A chave DSA %s usa uma hash insegura (%u bit)
 DSA necessita que o tamanho da hash seja um múltiplo de 8 bits
 Apagar esta assinatura válida? (s/N/q) Apagar esta assinatura inválida? (s/N/q) Remover esta chave do porta-chaves? (y/N)  Apagar esta assinatura desconhecida? (s/N/q) %d assinatura removida.
 %d assinaturas removidas.
 Assinatura separada.
 'Digest':  Quer realmente remover as chaves selecionadas? (y/N)  Quer realmente remover esta chave? (y/N)  Quer realmente fazer isto? (y/N)  Quer realmente revogar a chave inteira? (y/N)  Quer que a sua assinatura expire na mesma altura? (s/N)  Quer promovê-la a uma assinatura exportável? (s/N)  Quer promovê-la a uma auto-assinatura OpenPGP? (s/N)  Tem certeza de que quer assiná-la novamente de qualquer forma? (s/N)  Quer que a sua assinatura expire na mesma altura? (S/n)  ERRO:  Endereço de correio eletrónico:  Encriptar Indique o PIN de Administrador:  Indique o Novo PIN de Administrador:  Introduza o Novo PIN:  Indique o PIN:  Digite novo nome de ficheiro Insira a frase secreta
 Digite a frase secreta:  Digite o nome do possuidor da chave Digite a nova frase para esta chave secreta.

 Digite o valor necessário conforme pedido.
É possível digitar uma data ISO (AAAA-MM-DD) mas você não terá uma boa
reacção a erros - o sistema tentará interpretar o valor dado como um intervalo. Insira o tamanho da chave Digite o ID de utilizador do destinatário para quem quer enviar a
mensagem. Insira o ID de utilizador do revogador escolhido:  Erro: Nome combinado demasiado longo (o limite é de %d caracteres).
 Erro: Espaços duplos não são permitidos.
 Erro: dados de início de sessão demasiado longos (o limite é de %d caracteres).
 Erro: Somente ASCII simples é permitido actualmente.
 Erro: DO privado demasiado longo (limite é de %d caracteres).
 Erro: O carácter "<" não pode ser utilizado.
 Erro: URL demasiado longo (o limite é de %d caracteres).
 Erro: caracteres inválidos na string de opções.
 Erro: formato de impressão digital inválido.
 Erro: comprimento inválido da string de opções.
 Erro: resposta inválida.
 Características:  Arquivo `%s' já existe.  Dê o nome para o ficheiro ao qual a assinatura se aplica Digite a sua mensagem ...
 Assinatura correcta de "%s" Dispersão:  Sugestão: Selecione os IDs de utilizador para assinar
 Prima enter quando estiver pronto ou escreva 'c' para cancelar:  Com que cuidado é que verificou que chave que está prestes a assinar pertence
à pessoa correcta? Se não sabe o que responder, escolha "0".
 Verifiquei marginalmente esta chave.
 Eu verifiquei esta chave muito cuidadosamente.
 Eu ainda não verifiquei esta chave.
 Cifra IDEO não disponível, a tentar utilizar %s em substituição
 Se desejar, pode inserir uma texto descrevendo a razão pela qual criou
este certificado de revogação. Por favor mantenha este texto conciso.
Uma linha vazia termina o texto.
 Se você quiser usar esta chave, não de confiança, assim mesmo, responda "sim". Em geral não é uma boa ideia utilizar a mesma chave para assinar e para
cifrar.  Este algoritmo só deve ser utilizado em alguns domínios.
Por favor consulte primeiro o seu perito em segurança. Caracter inválido no comentário
 Caracter inválido no nome
 Comando inválido (tente "help")
 Frase secreta inválida; por favor tente novamente Opção inválida.
 Isto está correcto? (y/N)  Você decide que valor usar aqui; este valor nunca será exportado para
terceiros. Precisamos dele implementar a rede de confiança, que não tem
nada a ver com a rede de certificados (implicitamente criada). A chave "%s" já foi revogada.
 A chave nunca expira
 A chave expira em %s
 Geração de chave cancelada.
 A geração de chaves falhou: %s
 A chave foi comprometida A chave já não é utilizada A chave é protegida.
 A chave está revogada. A chave foi substituída A chave é valida por? (0)  Chave não alterada, nenhuma actualização é necessária.
 Porta-chaves sem alteração do servidor de chaves Preferências de língua:  Conteúdo do inicio de sessão (nome da conta):  Criar um backup da chave de encroiptação fora do cartão? (y/n)  N  para mudar o nome.
C  para mudar o comentário.
E  para mudar o endereço de email
O  para continuar a geração da chave.
S  para interromper a geração da chave. NOTA: %s não está disponível nesta versão
 NOTA: %s não é para uso normal!
 NOTA: um dos S/N da chave não coincide com um dos presentes no cartão
 NOTA: a criação de sub-chave para chaves v3 não respeito o OpenPGP
 NOTA: a chave foi revogada NOTA: ficheiro de opções por omissão `%s' inexistente
 NOTA: o ficheiro antigo de opções por omissão `%s' foi ignorado
 NOTA: A chave primária já está online e presente no cartão
 NOTA: A chave secundária já está online e presente no cartão
 NOTA: o remetente solicitou "apenas-para-seus-olhos"
 NOTA: o modo S2K simples (0) não é recomendável
 NOTA: não é possível escrever na trustdb
 O nome não pode começar com um dígito
 O nome deve ter pelo menos 5 caracteres
 A chave secreta é necessária para fazer isto.
 NnCcEeOoSs Nenhuma assinatura correspondente no porta-chaves secreto
 Nenhuma ajuda disponível Nenhuma ajuda disponível para `%s' Nenhum motivo especificado Nenhuma sub-chave com índice %d
 Identificador de utilizador inexistente.
 Nenhum ID de utilizador com hash %s
 Nenhum ID de utilizador com índice %d
 Endereço eletrónico inválido
 Note que esta chave não pode ser utilizada para encriptação. Você pode usar
o comando "--edit-key" para gerar uma chave secundária para esse fim.
 Nota: Esta chave foi desactivada.
 Nota: Esta chave expirou!
 Nada removido.
 Nada para assinar com a chave %s
 Placa OpenPGP número %s detectado
 Cartão OpenPGP não está disponível: %s
 A chamada do PIN devolveu um erro: %s
 O PIN para CHV%d é demasiado curto; o tamanho mínimo é %d
 O PIN não foi repetido correctamente; tente novamente Por favor corrija primeiro o erro
 Por favor decida quanto confia neste utilizador para
verificar correctamente as chaves de outros utilizadores
(vendo passaportes, verificando impressões digitais de outras fontes, etc)
 Por favor não coloque o endereço de email no nome verdadeiro ou no comentário
 Por favor indique um dominio para restringir esta assinatura, ou ENTER para nenhum.
 Por favor digite um novo nome de ficheiro. Se você apenas carregar em RETURN
o ficheiro por omissão (que é mostrado entre parênteses) será utilizado. Por favor digite um comentário (opcional) Por favor digite o nome do ficheiro de dados:  Por favor indique o nivel de confiança da assinatura.
Um nivel maior que 1 permite a esta chave a criação de assinaturas de confiança em seu nome.
 Por favor digite a frase secreta 
 Por favor conserte esta possível falha de segurança
 Por favor insira o cartão e  pressione enter ou escreva 'c' para cancelar:  É de notar que a configuração inicial dos PINs é
   PIN = `%s' PIN de Administrador = `%s'
Deverá alterá-la utilizando o comando --change-pin
 Não se esqueça que a validade de chave mostrada não é necessáriamente a
correcta a não ser que reinicie o programa.
 Por favor remova as selecções das chaves secretas.
 Por favor remova o cartão actual e insira o cartão com o número de série:
   %.*s
 Por favor repita a frase secreta, para ter certeza do que digitou. Por favor comunique bugs para <gnupg-bugs@gnu.org>.
 Por favor selecione pelo menos uma chave.
 Seleccione exactamente um identificador de utilizador.
 Por favor seleccione o tipo de chave a gerar:
 Por favor selecione o tipo de chave desejado:
 Por favor escolha onde guardar a chave:
 Por favor especifique por quanto tempo a chave deve ser válida.
         0 = chave não expira
      <n>  = chave expira em n dias
      <n>w = chave expira em n semanas
      <n>m = chave expira em n meses
      <n>y = chave expira em n anos
 Por favor especifique por quanto tempo a assinatura deve ser válida.
         0 = assinatura não expira
      <n>  = assinatura expira em n dias
      <n>w = assinatura expira em n semanas
      <n>m = assinatura expira em n meses
      <n>y = assinatura expira em n anos
 Por favor utilize o comando "toggle" primeiro.
 Por favor aguarde, entropia está a ser recolhida. Faça algum trabalho
para não se aborrecer, uma vez que irá melhorar a qualidade
da entropia.
 Acções possíveis para uma chave %s:  Servidor de chaves preferido:  Impressão da chave primária: Dados DO privados:  Chave pública:  A chave pública está desativada.
 Sair sem gravar? (y/N)  Módulo RSA em falta ou sem o tamanha %d bits
 Primo RSA %s em falta ou não tem i tamanha %d bits
 expoente público RSA inexistente ou superior que %d bits
 Nome completo:  Realmente criar os certificados de revogação? (s/N)  Criar mesmo? (y/N)  Realmente remover esta auto-assinatura? (s/N) Mover realmente esta chave primária? (y/N)  Realmente remover todas as identificações de utilizador? (y/N)  Remover realmente esta identificação de utilizador? (y/N)  Revocar mesmo todos os IDs de utilizador seleccionados? (y/N)  Revocar mesmo este ID de utilizador? (y/N)  Assinar realmente todas as identificações de utilizador? (y/N)  Assinar mesmo? (y/N)  Confirmar o actualizar das preferências para os utilizadores seleccionados? (y/N)  Confirmar o actualizar das preferências? (y/N)  Repita a frase secreta
 Repita a frase secreta:  Repita este PIN:  Substituir a chave existente? (s/N)  Substituir as chaves existentes? (s/N)  O tamanho de chave pedido é %u bits
 Código de reset é muito curto; comprimento mínimo é de %d
 Código para reset não ou não mais disponível
 Gravar alterações?  (Y/N)  Chave secreta disponível.
 Componentes secretas da chave primária não disponíveis.
 Componentes secretas da chave primária são guardadas no cartão.
 Seleccione o algoritmo a usar.

DSA (aka DSS) é o Algoritmo de Assinatura Digital e apenas pode ser utilizado
para assinaturas.

Elgamal é um algoritmo apenas de encriptação.

RSA pode ser usado para assinaturas ou encriptação.

A primeira chave (primária) deve ser sempre uma chave capaz de assinar. Sexo ((M)asculino, (F)eminino ou espaço):  Iniciar Sessão Assinar? (y/N)  A assinatura não expira nunca
 A assinatura expira em %s
 Assinatura feita em %s usando a chave %s com o ID %s
 Notação de assinatura:  Politica de assinatura:  SsEeAaQq A subchave "%s" já foi revogada.
 Sintaxe: gpg [opções] [ficheiros]
assina, verifica, cifra ou decifra
a operação por omissão depende dos dados de entrada
 O gerador de números aleatórios é apenas um "remendo"
para poder funcionar - não é de modo algum um bom gerador de números aleatórios!

NÃO USE NENHUM DADO GERADO POR ESTE PROGRAMA!

 A sua auto-assinatura em "%s"
é uma assinatura do tipo PGP 2.x.
 A assinatura não é válida. Faz sentido removê-la do seu porta-chaves. A assinatura será marcada como não-revogável.
 Não há preferências no ID de utilizador tipo PGP 2.x.
 Este comando não é permitido no modo %s.
 Esta chave é secreta! - apagar realmente? (y/N)  Esta é uma assinatura que liga o ID de utilizador à chave. Geralmente
não é uma boa idéia remover tal assinatura. É possível que o GnuPG
não consiga mais usar esta chave. Faça isto apenas se por alguma
razão esta auto-assinatura não for válida e há uma segunda disponível. Esta é uma assinatura válida na chave; normalmente não é desejável
remover esta assinatura porque ela pode ser importante para estabelecer
uma conexão de confiança à chave ou a outra chave certificada por esta. Esta chave pertence-nos
 Esta chave foi desactivada Esta chave expirou! Esta chave vai expirar em %s.
 Esta chave não é protegida.
 Esta chave pode ser revogada pela %s chave %s Esta chave foi revogada em %s por %s chave %s
 Esta assinatura não pode ser verificada porque você não tem a chave
correspondente. Você deve adiar sua remoção até saber que chave foi usada
porque a chave desta assinatura pode estabelecer uma conexão de confiança
através de outra chave já certificada. Esta assinatura expirou em %s.
 Isto tornaria a chave inutilizável no PGP 2.x.
 Para construir a Teia-de-Confiança ('Web-of-Trust'), o GnuPG precisa de
saber quais são as chaves em que deposita confiança absoluta - normalmente
estas são as chaves a que tem acesso à chave privada.  Responda "sim" para
que esta chave seja de confiança absoluta.
 Número total processado: %lu
 URL para adquirir a chave pública:  Não comprimido Tipo de assinatura desconhecido `%s'
 Uso: gpg [opções] [ficheiros] (-h para ajuda) Uso: gpgv [opções] [ficheiros] (-h para ajuda) ID de utilizador "%s" expirou. ID de utilizador "%s" não é auto-assinado. Utilizador "%s" está revocado. ID de utilizador "%s" é assinável.   ID do utilizador "%s": %d assinatura removida
 O identificador do utilizador já não é válido AVISO:  AVISO: "%s" é uma opção depreciada
 AVISO: %s sobrepõe %s
 AVISO: existem 2 ficheiros com informações confidenciais.
 AVISO: Esta chave é do tipo PGP 2.x. Se adicionar um revogador designado
       algumas versão do PGP podem rejeitá-la.
 AVISO: Esta chave é do tipo PGP2. Se adicionar um identificador fotográfico
       algumas versão do PGP podem rejeitá-la.
 AVISO: Esta chave foi revogada pelo seu dono!
 AVISO: Esta chave não está certificada com uma assinatura confiável!
 AVISO: Esta chave não está certificada com assinaturas suficientemente
       confiáveis!
 AVISO: Esta subchave foi revogada pelo seu dono!
 AVISO: A utilizar uma chave que não é de confiança!
 AVISO: Nós NÃO confiamos nesta chave!
 AVISO: Chave fraca detectada - por favor mude a frase secreta novamente.
 AVISO: `%s' é um ficheiro vazio
 AVISO: a assintura do ID do utilizador tem data %d segundos no futuro
 CUIDADO: a mensagem cifrada foi manipulada!
 AVISO: forçar o algoritmo de compressão %s (%d) viola as preferências do recipiente
 AVISO: forçar a cifra simétrica %s (%d) viola as preferências do destinatário
 AVISO: o ficheiro random_seed tem um tamanho inválido - não utilizado
 AVISO: chave %s contém preferências para não disponíveis
 AVISO: A mensagem foi cifrada com uma chave fraca na cifragem simétrica.
 AVISO: a mensagem não tinha a sua integridade protegida
 AVISO: várias assinaturas detectadas.  Apenas a primeira será verificada.
 AVISO: nada exportado
 AVISO: opções em `%s' ainda não estão activas nesta execução
 AVISO: O programa pode criar um ficheiro core!
 AVISO: destinatários (-r) dados sem utilizar uma cifra de chave pública
 AVISO: a chave secreta %s não tem um checksum SK simples
 AVISO: conflito no 'digest' de assinatura da mensagem
 AVISO: a assinatura não será marcada como não-revogável.
 AVISO: impossível expandir-%% a url de política (demasiado grande).  A utilizar não expandida.
 AVISO: incapaz de remover a directoria temporária `%s':%s
 AVISO: não é possível remover o ficheiro temporário(%s) `%s': %s
 AVISO: propriedade insegura no ficheiro de configuração `%s'
 AVISO: directoria pessoal com permissões inseguras `%s'
 AVISO. permissões pouco seguras no ficheiro de configuração `%s'
 AVISO: permissões poucos seguras no directório home `%s'
 AVISO: a utilizar memória insegura!
 AVISO: a utilizar gerador de números aleatórios inseguro!!
 Precisamos gerar muitos bytes aleatórios. É uma boa ideia realizar outra
actividade (escrever no teclado, mover o rato, usar os discos) durante a
geração dos números primos; isso dá ao gerador de números aleatórios
uma hipótese maior de ganhar entropia suficiente.
 Quando assina uma chave de identificação de um utilizador, deve primeiro
verificar que a chave pertence realmente à pessoa em questão. É útil para
terceiros saberem com que cuidado é que efectuou esta verificação.

"0" significa que não deseja declarar a forma com verificou a chave

"1" significa que acredita que a chave pertence à pessoa em questão, mas
    não conseguiu ou não tentou verificar. Este grau é útil para quando
    assina a chave de uma utilizador pseudo-anónimo.

"2" significa que efectuou uma verificação normal da chave. Por exemplo,
    isto pode significar que verificou a impressão digital da chave e
    verificou o identificador de utilizador da chave contra uma identificação
    fotográfica.

"3" significa que efectuou uma verificação exaustiva da chave. Por exemplo,
    isto pode significar que efectuou a verificação pessoalmente, e que 
    utilizou um documento, com fotografia, difícil de falsificar 
    (como por exemplo um passaporte) que o nome do dono da chave é o
    mesmo do que o identificador da chave, e que, finalmente, verificou
    (através de troca de e-mail) que o endereço de email da chave pertence
    ao done da chave.

Atenção: os exemplos dados para os níveis 2 e 3 são *apenas* exemplos.
Compete-lhe a si decidir o que considera, ao assinar chaves, uma verificação
"normal" e uma verificação "exaustiva".

Se não sabe qual é a resposta correcta, responda "0". Está prestes a revogar estas assinaturas:
 Você está usando o conjunto de caracteres `%s'.
 Você não pode modificar a data de validade de uma chave v3
 Você não pode remover o último ID de utilizador!
 Não especificou um identificador de utilizador. (pode usar "-r")
 Você não quer uma frase secreta - provavelmente isto é uma *má* idéia!

 Você não quer uma frase secreta - provavelmente isto é uma *má* idéia!
Vou continuar assim mesmo. Você pode mudar sua frase secreta a
qualquer hora, usando este programa com a opção "--edit-key".

 Assinou estes IDs de utilizador na chave %s:
 Não pode adicionar um revogador designado a uma chave tipo PGP 2.x.
 Não pode adicionar um identificador fotográfico a uma chave tipo PGP2.
 Não pode criar uma assinatura OpenPGP numa chave PGP 2.x no modo --pgp2.
 Você deve selecionar pelo menos uma chave.
 Você precisa selecionar pelo menos um ID de utilizador.
 Você deve selecionar exactamente uma chave.
 Você precisa de uma frase secreta para proteger a sua chave.

 Você selecionou este identificador de utilizador:
    "%s"

 Deve especificar uma razão para a emissão do certificado. Dependendo no
contexto, pode escolher as seguintes opções desta lista:
  "A chave foi comprometida"
     Utilize esta opção se tem razões para acreditar que indivíduos não
     autorizados obtiveram acesso à sua chave secreta.
  "A chave foi substituida"
     Utilize esta opção se substituiu esta chave com uma mais recente.
  "A chave já não é utilizada"
     Utilize esta opção se já não utiliza a chave.
  "O identificador do utilizador já não é válido"
     Utilize esta opção para comunicar que o identificador do utilizador
     não deve ser mais utilizado; normalmente utilizada para indicar
     que um endereço de email é inválido.
 A sua assinatura atual em "%s"
expirou.
 A sua assinatura actual em "%s"
é uma assinatura local.
 Decisão?  Opção?  A sua selecção? (digite `?' para mais informação):  O seu sistema não consegue mostrar datas para além de 2038.
No entanto, estas vão ser tratadas correctamente até 2106.
 [Identificação de Utilizador não encontrada] [nome_do_ficheiro] [não definido] [revogação] [auto-assinatura] [incerto] %s' já comprimido
 `%s' não é um ficheiro normal - ignorado
 `%s' não é um conjunto de caracteres válido
 `%s' não é um identificador longo de chave válido
 `%s' não é uma assinatura válida de expiração
 o acesso aos comandos de administração não está configurado
 adicionar uma chave a um smartcard adiciona um identificador fotográfico adiciona uma chave de revocação adicionar uma subchave adiciona um novo ID de utilizador cabeçalho de armadura:  armadura: %s
 a assumir dados cifrados %s
 a assumir dados assinados em `%s'
 MPI incorreto URI incorrecto certificado incorrecto chave incorrecta frase secreta incorrecta chave pública incorrecta chave secreta incorrecta assinatura incorrecta ser mais silencioso binário a colocar porta chaves `%s' em cache
 não é possível aceder %s - cartão de OpenPGP inválido?
 impossível fechar `%s': %s
 impossível ligar a `%s': %s
 impossível criar `%s': %s
 não é possível criar a directoria `%s': %s
 impossível desactivar core dumps: %s
 impossível fazer isso em modo não-interactivo
 impossível realizar esta operação em modo não-interactivo sem utilizar "--yes"
 não é possível gerar um primo com pbits=%u qbits=%u
 não é possível gerar um primo com menos de %d bits
 impossível manipular algoritmo de chave pública %d
 impossível manipular linhas de texto maiores que %d caracteres
 impossível bloquear `%s': %s
 impossível abrir `%s'
 impossível abrir `%s': %s
 impossível abrir dados assinados `%s'
 não é possível abrir o porta-chaves impossível ler `%s': %s
 impossível verificar o estado de `%s': %s
 não é possível utilizar o pacote ESK simétrico devido ao modo S2K
 impossível escrever `%s': %s
 cancelado cancelado pelo utilizador
 não pode escolher uma chave do tipo PGP 2.x como revogadora
 impossível evitar chave fraca para criptografia simétrica;
tentei %d vezes!
 o cartão está bloqueado permanentemente!
 leitor de cartões não disponível
 alterar o endereço URL para receber a chave mostra a impressão digital CA alterar o pin de um cartão alterar o nome do dono do cartão muda o género do dono do cartão alterar os dados de um cartão Modificar a data de validade de uma chave ou subchaves seleccionadas muda as preferências de língua muda o nome de login muda os valores de confiança muda a frase secreta verificar assinaturas verificação da assinatura criada falhou: %s
 a verificar a base de dados de confiança
 erro de "checksum" algoritmo de cifra %d%s é desconhecido ou foi desactivado
 completes-needed deve ser maior que 0
 comandos em conflito
 não foi possível processar o URL do servidor de chaves
 criar uma chave pública aquando da importação da chave secreta criar saída com armadura ascii Criada: %s marcação temporal inexistente
 dados não gravados; use a opção "--output" para gravá-los
 retirada de armadura falhou: %s
 decifrar dados (acção por omissão) decifragem falhou: %s
 decifragem correcta
 apagar as subchaves seleccionada apaga os IDs de utilizador seleccionados Apagar assinaturas dos IDs de utilizador seleccionados remoção do bloco de chave falhou: %s
 desactivar chave desativado não fazer alterações activar chave criação de armadura falhou: %s
 cifrar dados cifrado com algoritmo desconhecido %d
 cifrar uma mensagem no modo --pgp2 necessita da cifra IDEA
 cifrar apenas com cifra simétrica erro ao criar `%s': %s
 erro ao criar porta-chaves `%s': %s
 erro na criação da frase secreta: %s
 erro ao adquirir a informação de chave actual: %s
 erro na criação do novo PIN: %s
 erro na última linha
 erro na leitura de `%s': %s
 erro na leitura dos dados da aplicação
 erro na leitura da impressão digital DO
 erro na leitura do bloco de chave: %s
 erro na leitura do bloco de chave secreto "%s": %s
 erro ao receber o estado CHV do cartão
 erro na escrita do porta-chaves `%s': %s
 erro ao escrever no porta-chaves público `%s': %s
 erro ao escrever no porta-chaves secreto `%s': %s
 a chave existente irá ser substituída
 expirada expirada: %s expira: %s exportar chaves exportar chaves para um servidor de chaves não é permitido exportar chaves secretas
 chamadas a programas externos estão desactivadas devido a permições de opções não seguras
 falha ao inicializar a base de dados de confiança: %s
 falha ao criar 'cache' do porta-chaves: %s
 falha ao guardar a data de criação: %s
 falha ao guardar a impressão digital: %s
 falha ao guardar a chave: %s
 Falha ao utilizar PIN padrão como %s: %s - a desativar uso posterior padrão
 mulher obter a chave específica para a URL do cartão erro ao fechar ficheiro erro na criação do ficheiro erro na remoção do ficheiro o ficheiro já existe erro na abertura do ficheiro erro de leitura erro na renomeação do ficheiro erro de escrita Marcar o ID do utilizador como primário forçado ao forçar a cifra simétrica %s (%d) viola as preferências do destinatário
 total erro geral gerar mensagens PGP 2.x compatíveis gerar um novo par de chaves gerar um certificado de revogação gerar novas chaves falha ao criar chave
 gerando uma nova chave
 a gerar a 'checksum' (depreciada) de 16-bit para protecção da chave secreta
 o gpg-agent não está disponível nesta sessão
 a versão %d do protocolo gpg-agent não é suportada
 iImMqQsS importar chaves de um servidor de chaves importar/fundir chaves importar chaves secretas não é permitido
 linha de entrada %u demasiado longa ou falta o LF
 linha de entrada maior que %d caracteres
 inválida modo S2K inválido: deve ser 0, 1 ou 3
 argumento inválido armadura inválida cabeçalho de armadura inválido:  armadura inválida: linha maior que %d caracteres
 cabeçalho de assinatura em texto puro inválido
 linha com hífen inválida:  preferências por omissão inválidas
 opções de exportação inválidas
 algoritmo de dispersão inválido `%s'
 opções de importação inválidas
 porta-chaves inválido opções inválidas do servidor de chaves
 lista de opções inválidas
 nível mínimo de certificação inválido: deve ser  1, 2 ou 3
 pacote inválido frase-secreta inválida preferências pessoais de cifra inválidas
 preferências pessoais de compressão inválidas
 preferências pessoais de 'digest' inválidas
 caracter radix64 inválido %02X ignorado
 pacote raiz inválido detectado em proc_tree()
 estrutura inválida da placa OpenPGP (DO 0x93)
 valor inválido
 opções de verificação inválidas
 é fortemente aconselhável que actualize as suas preferências e
 chave "%s" não encontrada no servidor de chaves
 chave "%s" não encontrada: %s
 chave %s: "%s" %d novas assinaturas
 chave %s: "%s" %d novas subchaves
 chave %s: "%s" %d novos IDs de utilizador
 chave %s: "%s" %d novas assinaturas
 chave %s: "%s" %d novas assinaturas
 chave %s: "%s" %d ID de utilizador limpo
 chave %s: "%s" %d IDs de utilizador limpos
 chave %s: "%s" 1 nova assinatura
 chave %s: "%s" 1 nova subchave
 chave %s: "%s" 1 novo ID de utilizador
 chave %s: "%s" sem alterações
 chave %s: estilo da chave PGP 2.x - ignorada
 chave %s: já está no porta-chaves secreto
 chave %s: impossível localizar o bloco de chaves original: %s
 chave %s: impossível ler bloco de chaves original: %s
 chave %s: assinatura directa de chave adicionada
 chave %s: não corresponde à nossa cópia
 chave %s: auto-assinatura inválida no ID de utilizador "%s"
 chave %s: ligação de subchave inválida
 chave %s: revogação de subchave inválida
 chave %s: chave nova - ignorada
 chave %s: sem subchave para ligação de chaves
 chave %s: sem subchave para revogação de chave
 chave %s: sem identificação de utilizador
 chave %s: não protegida - ignorada
 chave %s: chave pública "%s" importada
 chave %s: chave pública não encontrada: %s
 chave %s: apagada ligação múltipla de subchave
 chave %s: removida revogação múltiplace de subchaves
 chave %s: chave secreta importada
 chave %s: chave secreta não encontrada: %s
 chave %s: chave secreta com cifra inválida %d - ignorada
 chave %s: chave secreta sem chave pública - ignorada
 chave %s: subchave ignorada
 chave %s: ignorado ID de utilizador "%s"
 chave %s: algoritmo de chave pública não suportado
 chave já existe
 a exportação de chaves falhou: %s
 Geração de chave completada (%d segundos)
 a chave foi criada %lu segundo no futuro
(viagem no tempo ou problema no relógio)
 a chave foi criada %lu segundos no futuro
(viagem no tempo ou problema no relógio)
 a chave não está marcada insegura - impossível usá-la com o RNG falso!
 Operação chave impossível: %s
 porta-chaves `%s' criado
 erro do servidor de chaves erro interno no servidor de chaves
 a recepção do servidor de chaves falhou: %s
 actualização do servidor de chaves falhou: %s
 A procura do servidor de chaves falhou: %s
 O envio do servidor de chaves falhou: %s
 tamanho de chave inválido; a utilizar %u bits
 tamanho da chave arredondado para %u bits
 mostrar todos os dados disponíveis listar e verificar as assinaturas das chaves lista chave e identificadores de utilizadores listar as chaves listar as chaves e as impressões digitais listar as chaves e as assinaturas lista preferências (perito) lista preferências (detalhadamente) listar as chaves secretas fazer uma assinatura separada homem CRC malformado
 variável de ambiente GPG_AGENT_INFO inválida
 identificador de utilizador malformado marginal marginals-needed deve ser maior que 1
 max-cert-depth deve estar entre 1 e 255
 menu para alterar ou desbloquear o PIN mover uma chave de backup para um smartcard mover uma chave para um smartcard a mover a assinatura da chave para o local correcto
 nN assinaturas em texto puro aninhadas
 erro na rede nunca nunca      criado um novo ficheiro de configuração `%s'
 proxima verificação da base de dados de confiança a %s
 não nenhum sinal = encontrado na definição de grupo `%s'
 sem porta-chaves público por omissão: %s
 nenhum módulo de recolha de entropia detectado
 servidor de chaves desconhecido (use opção - keyserver)
 não é necessária uma verificação da base de dados de confiança
 execução remota de programas não suportada
 nenhuma chave secreta
 não há sub-chave secreta para a sub-chave pública "%s"!
 não há dados assinados
 identificador de utilizador inexistente nenhum dado OpenPGP válido encontrado.
 nenhum endereço válido
 não foi encontrada nenhum porta-chaves onde escrever: %s
 nenhum porta-chaves público com permissões de escrita encontrado: %s
 nenhum porta-chaves secreto com permissões de escrita encontrado: %s
 não é uma assinatura separada
 não é uma placa com OpenPGP não cifrado não forçado não processado não suportado nota: o ficheiro random_seed está vazio
 nota: ficheiro random_seed não actualizado
 certo, nós somos o destinatário anónimo.
 codificação antiga do DEK não suportada
 formato de assinatura antigo (PGP2.x)
 apenas aceitar atualizações em chaves existentes a operação não é possível sem memória segura inicializada
 ficheiro de opções `%s': %s
 nome do ficheiro original='%.*s'
 informações de 'ownertrust' limpas
 a frase secreta não foi repetida corretamente; tente outra vez por favor digite um endereço de email (opcional mas recomendado) por favor veja http://www.gnupg.org/documentation/faqs.html para mais informações
 por favor utilize "%s%s" em vez dela
 aguarde por favor enquanto a chave está a ser criada ...
 fim de ficheiro prematuro (no CRC)
 fim de ficheiro prematuro (no trecho)
 fim de ficheiro prematuro (sem CRC)
 imprimir o estado do cartão problema ao tratar pacote cifrado
 problema com o agente - a desactivar a utilização deste
 perguntar antes de sobrepôr algoritmo de protecção %d%s não é suportado
 chaves pública e privada criadas e assinadas.
 chave pública %s não encontrada: %s
 decifragem de chave pública falhou: %s
 dados cifrados com chave pública: DEK válido
 chave pública não encontrada qQ sair sair deste menu caracter "quoted printable" na armadura - provavelmente um MTA com bugs foi usado
 redistribua esta chave para evitar problemas de não coincidência de algoritmos
 lendo de `%s'
 a ler opções de `%s'
 leitura da chave pública falhou: %s
 lendo do "stdin" ...
 motivo da revocação:  remover chaves do porta-chaves público remover chaves do porta-chaves secreto remover partes não utilizáveis da chave durante a exportação limite de recursos a resposta não contém os módulos RSA
 a resposta não contém o expoente público RSA
 a resposta não contém os dados para a chave pública
 comentário da revocação:  revoga chave ou as subchaves selecionadas revogar os IDs de utilizador selecionados revogar as assinaturas nos IDs de utilizador selecionados revogada revogado pela sua chave %s em %s
 arredondado para %u bits
 gravar e sair procurar chaves num servidor de chaves chave secreta já foi guardada num cartão
 chave secreta não disponível partes da chave secreta não disponíveis
 Componentes secretas da chave não disponíveis
 selecionar sub-chave N seleciona ID de utilizador N o algoritmo de "digest" de certificação selecionado é inválido
 o algoritmo de cifragem selecionado é inválido
 o algoritmo de compressão selecionado é inválido
 o algoritmo de "digest" selecionado é inválido
 configurar a lista de preferências para os IDs d utilizador selecionados mostrar comandos de administração mostrar todas as notações durante a verificação de assinatura mostrar impressão digital da chave mostrar subchaves revogadas e expiradas nas listas de chaves mostrar IDs de fotografía seleccionados mostra esta ajuda assinar uma chave assinar uma chave localmente assinar ou editar uma chave assinar os IDs de utilizador seleccionados [* ver abaixo para comandos relacionados] assina os IDs dos utilizadores localmente assina os IDs de utilizador seleccionados com uma assinatura não revogável assinar os IDs de utilizador selecionados com uma assinatura fiável verificação de assinatura suprimida
 assinaturas já criadas até agora: %lu
 assinado pela sua chave %s em %s%s%s
 assinatura falhou: %s
 a assinar: ignorado: a chave pública já está presente
 ignorado: chave pública já colocada como destinatário por omissão
 ignorado: a chave secreta já está presente
 ignorando bloco do tipo %d
 revocação solitária - utilize "gpg --import" para aplicar
 assinatura de classe 0x%02x
 subpacote do tipo %d tem bit crítico ligado
 Encriptação simétrica de `%s' falhou: %s
 erro de sistema enquanto chamava o programa externo: %s
 remover as chaves deste anel de chaves a URL de política de certificação dada é inválida
 o URL do Servidor de chaves preferido é inválido
 a URL de política de assinatura dada é inválida
 a assinatura não pode ser verificada.
Não se esqueça que o ficheiro com a assinatura (.sig ou .asc)
deve ser o primeiro a ser dado na linha de comando.
 há uma chave secreta para a chave pública "%s"!
 esta chave já foi designada como revocadora
 isto pode ser causado por falta de auto-assinatura
 esta mensagem poderá não ser utilizável pelo %s
 esta plataforma requer ficheiros temporários ao chamar programas externos
 conflito de "timestamp" activar o atributo de assinatura PIN forçada demasiadas preferências de cifra
 entradas demais no cache pk - desactivado
 erro na base de dados de confiança registo de confiança %lu não é do tipo pedido %d
 registo de confiança %lu, tipo req %d: falha na leitura: %s
 registo de confiança %lu, tipo %d: escrita falhou: %s
 base de dados de confiança rec %lu: lseek falhou: %s
 base de dados de confiança rec %lu: escrita falhou (n=%d): %s
 transação de base de dados de confiança muito grande
 base de dados de confiança: lseek falhou: %s
 base de dados de confiança: leitura falhou (n=%d): %s
 base de dados de confiança: sincronização falhou: %s
 não é possível executar o programa externo
 não é possível executar o programa `%s': %s
 não foi possível executar o terminal '%s': %s
 não é possível ler a resposta do programa externo: %s
 não foi possível alterar o exec-path para %s
 impossível utilizar a cifra IDEA para todas as chaves para que está a cifrar.
 dados inesperados algoritmo de criptografia não implementado algoritmo de chave pública não implementado algoritmo de criptografia desconhecido algoritmo de compressão desconhecido item de configuração `%s' desconhecido
 algoritmo de "digest" desconhecido algoritmo de protecção da chave desconhecido
 formato de pacote desconhecido algoritmo de chave pública desconhecido classe de assinatura desconhecida versão desconhecida saída anormal de um programa externo
 Não especificado URI não suportado algoritmo de chave pública inutilizável chave pública não utilizável chave secreta não utilizável actualizar todas as chaves a partir de um servidor de chaves actualização falhou: %s
 actualização da chave secreta falhou: %s
 actualizar a base de dados de confiança uso: gpg [opções]  usar como ficheiro de saída usar modo de texto canônico utilize a opção "--delete-secret-keys" para a apagar primeiro.
 utilizar o comportamento exclusivo do OpenPGP usar este identificador de utilizador para
assinar ou decifrar o utilizador com o id "%s" já está revocado
 ID de utilizador: "%s"
 usando a cifra %s
 A usar PIN padrão como %s
 validade: %s detalhado a verificação do PIN de Administrador é actualmente proibida através deste comando
 A verificação CHV%d falhou: %s
 verificar uma assinatura verifica o PIN e lista todos os dados à espera de bloquear em `%s'...
 chave fraca chave fraca criada - tentando novamente
 tamanho estranho para uma chave de sessão cifrada (%d)
 não vai funcionar com memória insegura devido a %s
 a escrever a assinatura directa
 a escrever a assinatura ligada a uma chave
 A escrever a nova chave
 a escrever chave pública para `%s'
 a escrever chave privada para `%s'
 a escrever a auto-assinatura
 a escrever para `%s'
 a escrever em "stdout"
 chave secreta incorrecta sS só pode assinar à vista com chaves do tipo PGP 2.x no modo --pgp2
 só pode assinar-desligar com chaves do tipo PGP 2.x no modo --pgp2
 no modo --pgp2 só pode cifrar com chaves RSA de 2048 bits ou menos
 só pode fazer assinaturas separadas ou em texto puro no modo --pgp2
 pode actualizar as suas preferências com: gpg --edit-key %s updpref save
 não pode assinar e cifrar ao mesmo tempo no modo --pgp2
 não pode escolher uma chave como revogadora de si mesmo
 não pode utilizar --symetric --encrypt enquanto estiver no modo %s
 não é possível usar --symmetric --encrypt com --s2K-mode 0
 não é possível usar --symmetric --sign --encrypt durante o modo %s
 não é possível usar --symmetric --sign --encrypt com --s2k-mode 0
 você encontrou um bug ... (%s:%d)
 não pode utilizar %s enquanto estiver no modo %s
 não pode utilizar o algoritmo de cifra `%s' enquanto no modo %s
 não pode utilizar o algoritmo de compressão `%s' enquanto estiver no modo %s
 não pode utilizar o algoritmo %s enquanto estiver no modo %s
 deve utilizar ficheiros (e não um 'pipe') quando trabalho no modo --pgp2.
 |AN|Novo PIN de Administração ||Por favor, insira o PIN de Administrador |A|Por favor digite o PIN%%0A de administrador [tentativas restantes: %d] |DF|escrever informações de estado para o
descritor de ficheiro DF |NOME|cifrar para NOME |N|Novo PIN |N|estabelecer nível de compressão N
(0 desactiva) |RN|Novo Código de Reset |[ficheiro]|fazer uma assinatura em texto puro |[ficheiro]|fazer uma assinatura |algo [ficheiros]|imprimir "digests" de mensagens ||Por favor, insira o PIN ||Por favor introduduz o PIN%%0A[sigs feito: %lu] ||Por favor, insira o código de reset para o cartão 