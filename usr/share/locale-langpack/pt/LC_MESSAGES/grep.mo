��    s      �  �   L      �	  �   �	  �   �
  �   \  �  T     $  /  9    i  �  �  Q  @  �  �  k  &  G  �     �  0   �          <  ,   X     �  ,   �  ,   �  '   �  -   %      S  (   t  (   �     �     �       q        z     ~  *   �     �  Q   �  ?   $     d     {     �     �  $   �     �               /     <     E  :   d     �     �  #   �     �       3        N     V  &   i     �     �     �     �     �  (   �         3     @  ;   W  3   �  /   �  +   �  '   #   #   K      o      �      �      �      �   4   �      !  "   .!  !   Q!     s!  0   �!  -   �!      �!     "     ("     @"  $   O"     t"     �"     �"     �"     �"     �"     #  $   #     D#     Q#     b#  >   v#     �#     �#  P   �#  ,   :$  *   g$     �$     �$     �$     �$     �$     �$     �$  B   %     Q%  �  ]%  �   )'  �   �'  �   �(  �  �)     �+  /  �+  '  �,  �  .  Q  �/  �  !1  �  �2  G  A4     �5  6   �5      �5     �5  -   6     H6  -   f6  -   �6  )   �6  0   �6  "   7  *   @7  (   k7  %   �7  %   �7     �7     �7     b8     f8  G   y8  !   �8  P   �8  P   49     �9  )   �9  "   �9     �9  '   :     5:     Q:     n:     �:     �:  ,   �:  ?   �:     ;     %;  #   =;  "   a;     �;  <   �;     �;     �;  *   �;     )<     F<     c<     �<     �<  4   �<     �<    =     >  :   )>  2   d>  .   �>  *   �>  &   �>  "   ?     ;?     Z?     w?     �?     �?  ?   �?  6   �?  (   @  -   H@     v@  4   �@  :   �@  6   �@  )   5A     _A     uA  #   �A     �A     �A     �A     B  .   B     KB     fB  '   �B     �B     �B     �B  >   �B     $C  #   DC  S   hC  *   �C  4   �C     D     /D     BD     UD  &   mD  $   �D     �D  R   �D     E         S   K   %       P           '   O   F       ?       p       V   i   N   !   e   &       $                                M   ;   D   +   @            Z   -                 m   E             s       j   7      R   9         f   =   b       0   <      C   ,       
                  d       n      (   1   J               T   ^   	   ]          c       A      L   :   Y   W             B      h   "   )       8   4   H   `          I   G   k   >   [      .   2           3       X                 \   U   #   o         g   a   6   r       /   q              l       _   *           5       Q    
Context control:
  -B, --before-context=NUM  print NUM lines of leading context
  -A, --after-context=NUM   print NUM lines of trailing context
  -C, --context=NUM         print NUM lines of output context
 
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

 
Miscellaneous:
  -s, --no-messages         suppress error messages
  -v, --invert-match        select non-matching lines
  -V, --version             display version information and exit
      --help                display this help text and exit
 
Output control:
  -m, --max-count=NUM       stop after NUM matches
  -b, --byte-offset         print the byte offset with output lines
  -n, --line-number         print line number with output lines
      --line-buffered       flush output on every line
  -H, --with-filename       print the file name for each match
  -h, --no-filename         suppress the file name prefix on output
      --label=LABEL         use LABEL as the standard input file name prefix
 
Report bugs to: %s
       --include=FILE_PATTERN  search only files that match FILE_PATTERN
      --exclude=FILE_PATTERN  skip files and directories matching FILE_PATTERN
      --exclude-from=FILE   skip files matching any file pattern from FILE
      --exclude-dir=PATTERN  directories that match PATTERN will be skipped.
   -E, --extended-regexp     PATTERN is an extended regular expression (ERE)
  -F, --fixed-strings       PATTERN is a set of newline-separated strings
  -G, --basic-regexp        PATTERN is a basic regular expression (BRE)
  -P, --perl-regexp         PATTERN is a Perl regular expression
   -I                        equivalent to --binary-files=without-match
  -d, --directories=ACTION  how to handle directories;
                            ACTION is 'read', 'recurse', or 'skip'
  -D, --devices=ACTION      how to handle devices, FIFOs and sockets;
                            ACTION is 'read' or 'skip'
  -r, --recursive           like --directories=recurse
  -R, --dereference-recursive  likewise, but follow all symlinks
   -L, --files-without-match  print only names of FILEs containing no match
  -l, --files-with-matches  print only names of FILEs containing matches
  -c, --count               print only a count of matching lines per FILE
  -T, --initial-tab         make tabs line up (if needed)
  -Z, --null                print 0 byte after FILE name
   -NUM                      same as --context=NUM
      --color[=WHEN],
      --colour[=WHEN]       use markers to highlight the matching strings;
                            WHEN is 'always', 'never', or 'auto'
  -U, --binary              do not strip CR characters at EOL (MSDOS/Windows)
  -u, --unix-byte-offsets   report offsets as if CRs were not there
                            (MSDOS/Windows)

   -e, --regexp=PATTERN      use PATTERN for matching
  -f, --file=FILE           obtain PATTERN from FILE
  -i, --ignore-case         ignore case distinctions
  -w, --word-regexp         force PATTERN to match only whole words
  -x, --line-regexp         force PATTERN to match only whole lines
  -z, --null-data           a data line ends in 0 byte, not newline
   -o, --only-matching       show only the part of a line matching PATTERN
  -q, --quiet, --silent     suppress all normal output
      --binary-files=TYPE   assume that binary files are TYPE;
                            TYPE is 'binary', 'text', or 'without-match'
  -a, --text                equivalent to --binary-files=text
 %s home page: <%s>
 %s home page: <http://www.gnu.org/software/%s/>
 %s%s argument '%s' too large %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 ' 'egrep' means 'grep -E'.  'fgrep' means 'grep -F'.
Direct invocation as either 'egrep' or 'fgrep' is deprecated.
 (C) (standard input) -P supports only unibyte and UTF-8 locales Binary file %s matches
 Example: %s -i 'hello world' menu.h main.c

Regexp selection and interpretation:
 General help using GNU software: <http://www.gnu.org/gethelp/>
 Invalid back reference Invalid character class name Invalid collation character Invalid content of \{\} Invalid preceding regular expression Invalid range end Invalid regular expression Memory exhausted Mike Haertel No match No previous regular expression PATTERN is, by default, a basic regular expression (BRE).
 Packaged by %s
 Packaged by %s (%s)
 Premature end of regular expression Regular expression too big Report %s bugs to: %s
 Search for PATTERN in each FILE or standard input.
 Success Trailing backslash Try '%s --help' for more information.
 Unknown system error Unmatched ( or \( Unmatched ) or \) Unmatched [, [^, [:, [., or [= Unmatched \{ Usage: %s [OPTION]... PATTERN [FILE]...
 Valid arguments are: When FILE is -, read standard input.  With no FILE, read . if a command-line
-r is given, - otherwise.  If fewer than two FILEs are given, assume -h.
Exit status is 0 if any line is selected, 1 otherwise;
if any error occurs and -q is not given, the exit status is 2.
 Written by %s and %s.
 Written by %s, %s, %s,
%s, %s, %s, %s,
%s, %s, and others.
 Written by %s, %s, %s,
%s, %s, %s, %s,
%s, and %s.
 Written by %s, %s, %s,
%s, %s, %s, %s,
and %s.
 Written by %s, %s, %s,
%s, %s, %s, and %s.
 Written by %s, %s, %s,
%s, %s, and %s.
 Written by %s, %s, %s,
%s, and %s.
 Written by %s, %s, %s,
and %s.
 Written by %s, %s, and %s.
 Written by %s.
 ` ambiguous argument %s for %s character class syntax is [[:space:]], not [:space:] conflicting matchers specified exceeded PCRE's backtracking limit exceeded PCRE's line length limit exhausted PCRE JIT stack failed to allocate memory for the PCRE JIT stack failed to return to initial working directory input file %s is also the output input is too large to count internal PCRE error: %d internal error internal error (should never happen) invalid %s%s argument '%s' invalid argument %s for %s invalid character class invalid content of \{\} invalid context length argument invalid matcher %s invalid max count invalid suffix in %s%s argument '%s' lseek failed memory exhausted no syntax specified others, see <http://git.sv.gnu.org/cgit/grep.git/tree/AUTHORS> recursive directory loop regular expression too big support for the -P option is not compiled into this --disable-perl-regexp binary the -P option only supports a single pattern unable to record current working directory unbalanced ( unbalanced ) unbalanced [ unfinished \ escape unknown binary-files type unknown devices method warning: %s: %s warning: GREP_OPTIONS is deprecated; please use an alias or script write error Project-Id-Version: grep 2.5g
Report-Msgid-Bugs-To: bug-grep@gnu.org
POT-Creation-Date: 2016-04-21 21:37-0700
PO-Revision-Date: 2016-03-31 08:56+0000
Last-Translator: Ivo Xavier <ivoxavier.8@gmail.com>
Language-Team: Portuguese <translation-team-pt@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:43+0000
X-Generator: Launchpad (build 18115)
Language: pt
 
Controlo de contexto:
  -B, --before-context=NUM  print NUM lines of leading context
  -A, --after-context=NUM   print NUM lines of trailing context
  -C, --context=NUM         print NUM lines of output context
 
Licença GPLv3+: GNU GPL versão 3 ou posterior <http://gnu.org/licenses/gpl.html>.
Isto é software livre: você é livre para modificá-lo e distribuí-lo.
Não tem GARANTIA, até ao limite garantido por lei.

 
Miscellaneous:
  -s, --no-messages         suppress error messages
  -v, --invert-match        select non-matching lines
  -V, --version             display version information and exit
      --help                display this help text and exit
 
Controlo de saída:
  -m, --max-count=NUM       stop after NUM matches
  -b, --byte-offset         print the byte offset with output lines
  -n, --line-number         print line number with output lines
      --line-buffered       flush output on every line
  -H, --with-filename       print the file name for each match
  -h, --no-filename         suppress the file name prefix on output
      --label=LABEL         use LABEL as the standard input file name prefix
 
Relate os erros (bugs) em: %s
       --include=FILE_PATTERN  search only files that match FILE_PATTERN
      --exclude=FILE_PATTERN  skip files and directories matching FILE_PATTERN
      --exclude-from=FILE   skip files matching any file pattern from FILE
      --exclude-dir=PATTERN  directories that match PATTERN will be skipped.
   -E, --extended-regexp PADRÃO é uma expressão regular estendida (ERE)
  -F, --fixed-strings PADRÃO é  um conjunto de strings separados por nova linha
  -G, --basic-regexp PADRÃO é uma expressão regular básica (BRE)
  -P, --perl-regexp PADRÃO é uma expressão regular de sintaxe Perl
   -I                        equivalent to --binary-files=without-match
  -d, --directories=ACTION  how to handle directories;
                            ACTION is 'read', 'recurse', or 'skip'
  -D, --devices=ACTION      how to handle devices, FIFOs and sockets;
                            ACTION is 'read' or 'skip'
  -r, --recursive           like --directories=recurse
  -R, --dereference-recursive  likewise, but follow all symlinks
   -L, --files-without-match  print only names of FILEs containing no match
  -l, --files-with-matches  print only names of FILEs containing matches
  -c, --count               print only a count of matching lines per FILE
  -T, --initial-tab         make tabs line up (if needed)
  -Z, --null                print 0 byte after FILE name
   -NUM                      same as --context=NUM
      --color[=WHEN],
      --colour[=WHEN]       use markers to highlight the matching strings;
                            WHEN is 'always', 'never', or 'auto'
  -U, --binary              do not strip CR characters at EOL (MSDOS/Windows)
  -u, --unix-byte-offsets   report offsets as if CRs were not there
                            (MSDOS/Windows)

   -e, --regexp=PATTERN      utilizar o PADRÃO para filtrar.
  -f, --file=FILE           obter o PADRÃO de um FICHEIRO
  -i, --ignore-case         ignorar maiúsculas
  -w, --word-regexp         forçar o PADRÃO a filtrar palavras completas
  -x, --line-regexp         forçar o PADRÃO a filtrar linhas completas
  -z, --null-data           uma linha termina no byte 0 e não numa nova linha
   -o, --only-matching       show only the part of a line matching PATTERN
  -q, --quiet, --silent     suppress all normal output
      --binary-files=TYPE   assume that binary files are TYPE;
                            TYPE is 'binary', 'text', or 'without-match'
  -a, --text                equivalent to --binary-files=text
 %s página inicial: <%s>
 %s página inicial: <http://www.gnu.org/software/%s/>
 %s%s argumento '%s' muito grande %s: opção inválida -- '%c'
 %s: opção '%c%s' não permite um argumento
 %s: opção '%s' é ambígua
 %s: opção '%s' é ambígua; possibilidades: %s: opção '--%s' não permite um argumento
 %s: a opção '--%s' requer um argumento
 %s: a opção '-W %s' não permite um argumento
 %s: a opção '-W %s' é ambígua
 %s: a opção '-W %s' requer um argumento
 %s: opção requer um argumento -- '%c'
 %s: a opção '%c%s' é desconhecida
 %s: a opção '--%s' é desconhecida
 " 'egrep' significa 'grep -E'.  'fgrep' significa 'grep -F'.
Invocação direta de ambos como 'egrep' or 'fgrep' está obsoleta.
 (C) (entrada standard) -P só oferece suporte a locais unibyte (um caractere por byte) e UTF-8 Ficheiro binário %s corresponde
 Exemplo: %s -i 'Olá Mundo' menu.h main.c

Selecção Regexp e interpretação:
 Ajuda geral sobre a utilização de software GNU: <http://www.gnu.org/gethelp/>
 Referência anterior inválida Nome de classe de caracteres é inválido Carácter de agrupamento inválido Conteúdo de \{\} inválido Expressão regular precedente inválida Fim de  intervalo inválido Expressão regular inválida Memória esgotada Mike Haertel Sem correspondência(s) Não há nenhuma expressão regular anterior O PADRÃO é, por norma, uma expressão regular básica (ERB).
 Empacotado por %s
 Empacotado por %s (%s)
 Fim prematuro da expressão regular Expressão regular demasiado longa Reportar %s erros para: %s
 Procurar por um PADRÃO em cada FICHEIRO ou entrada padrão
 Sucesso Barra invertida no final Tente '%s --help' para mais informação.
 Erro desconhecido do sistema ( ou \( sem correspondência ) ou \) sem correspondência Sem igual [, [^, [:, [., ou [= \{ sem correspondência Utilização: %s [OPÇÃO]... PADRÃO [FICHEIRO]...
 Argumentos válidos são: When FILE is -, read standard input.  With no FILE, read . if a command-line
-r is given, - otherwise.  If fewer than two FILEs are given, assume -h.
Exit status is 0 if any line is selected, 1 otherwise;
if any error occurs and -q is not given, the exit status is 2.
 Escrito por %s e %s.
 Escrito por %s, %s, %s,
%s, %s, %s, %s,
%s, %s, e outros.
 Escrito por %s, %s, %s,
%s, %s, %s, %s,
%s, e %s.
 Escrito por %s, %s, %s,
%s, %s, %s, %s,
e %s.
 Escrito por %s, %s, %s,
%s, %s, %s, e %s.
 Escrito por %s, %s, %s,
%s, %s, e %s.
 Escrito por %s, %s, %s,
%s, e %s.
 Escrito por %s, %s, %s,
e %s.
 Escrito por %s, %s, and %s.
 Escrito por %s.
 " argumento ambíguo %s para %s sintaxe da classe do carácter é [[:space:], não é [:space:] foram especificadas expressões que entram em conflito excedeu o limite de backtracking do PCRE excedeu o limite de comprimento da linha PCRE pilha PCRE JIT esgotada não conseguiu alocar memória para a pilha JIT PCRE não é possível voltar à directoria de trabalho inicial Ficheiro de entrada %s é também o ficheiro de saída a entrada é demasiado grande para contar erro interno PCRE: %d erro interno erro interno (nunca deve acontecer) inválido %s%s argumento '%s' argumento inválido %s para %s classe de carateres inválida conteúdo invalido de \{\} argumento de comprimento do contexto inválido Correspondência errada %s contagem máxima inválida sufixo inválido em %s%s argumento '%s' lseek falhou memória esgotada nenhuma sintaxe especificada outros, ver <http://git.sv.gnu.org/cgit/grep.git/tree/AUTHORS> ciclo recursivo de directórios expressão regular demasiado grande o suporte para a opção -P não foi compilado neste binário --disable-perl-regexp a opção -P só suporta um único padrão incapaz de registar o directório de trabalho actual ( não emparelhado ) não emparelhado [ não emparelhado \ escape não terminado tipo de ficheiro binário desconhecido método de dispositivos desconhecido aviso: %s: %s aviso: a variável de ambiente GREP_OPTIONS está obsoleta; use um alias ou script erro de escrita 