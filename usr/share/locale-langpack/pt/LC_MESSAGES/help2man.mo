��    2      �  C   <      H     I     O     k  !   �     �     �     �  &   �          #  �   0  �   �  �   �  	   k     u     �     �     �     �     �     �  Y  �     	     	  "   %	  �   H	     �	     
     
  �   
  �   �
  �   W     D  1   S     �     �     �  �   �  B   �  1   �       5         V     \     j  �  v  )   U          �  �  �     '  "   -     P  6   n     �  ,   �     �  3        F     L  �   \  �     �   �     �     �     �     �     �     �  	   �  	   �  s  �     b     h  1   z  �   �     v     {     �  �   �  �   a  �   �     �  1   �          '  *   /  �   Z  C   L  7   �     �  =   �            -      D   �  Q   .   0%     _%     b%                       	   (                                             1   +   2   -          
      &      %   #          $                   !               0                  *   '                   "   ,           .   )            /    %B %Y %s \- manual page for %s %s %s: can't create %s (%s) %s: can't get `%s' info from %s%s %s: can't open `%s' (%s) %s: can't unlink %s (%s) %s: error writing to %s (%s) %s: no valid information found in `%s' AUTHOR AVAILABILITY Additional material may be included in the generated output with the
.B \-\-include
and
.B \-\-opt\-include
options.  The format is simple:

    [section]
    text

    /pattern/
    text
 Any
.B [NAME]
or
.B [SYNOPSIS]
sections appearing in the include file will replace what would have
automatically been produced (although you can still override the
former with
.B \-\-name
if required).
 Blocks of verbatim *roff text are inserted into the output either at
the start of the given
.BI [ section ]
(case insensitive), or after a paragraph matching
.BI / pattern /\fR.
 COPYRIGHT DESCRIPTION ENVIRONMENT EXAMPLES Environment Examples FILES Files GNU %s %s

Copyright (C) 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2009, 2010,
2011, 2012, 2013, 2014, 2015 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Written by Brendan O'Dea <bod@debian.org>
 Games INCLUDE FILES Include file for help2man man page Lines before the first section or pattern which begin with `\-' are
processed as options.  Anything else is silently ignored and may be
used for comments, RCS keywords and the like.
 NAME OPTIONS Options Other sections are prepended to the automatically produced output for
the standard sections given above, or included at
.I other
(above) in the order they were encountered in the include file.
 Patterns use the Perl regular expression syntax and may be followed by
the
.IR i ,
.I s
or
.I m
modifiers (see
.BR perlre (1)).
 Placement of the text within the section may be explicitly requested by using
the syntax
.RI [< section ],
.RI [= section ]
or
.RI [> section ]
to place the additional text before, in place of, or after the default
output respectively.
 REPORTING BUGS Report +(?:[\w-]+ +)?bugs|Email +bug +reports +to SEE ALSO SYNOPSIS System Administration Utilities The full documentation for
.B %s
is maintained as a Texinfo manual.  If the
.B info
and
.B %s
programs are properly installed at your site, the command
.IP
.B info %s
.PP
should give you access to the complete manual.
 The latest version of this distribution is available on-line from: The section output order (for those included) is: This +is +free +software Try `--no-discard-stderr' if option outputs to stderr Usage User Commands Written +by `%s' generates a man page out of `--help' and `--version' output.

Usage: %s [OPTION]... EXECUTABLE

 -n, --name=STRING       description for the NAME paragraph
 -s, --section=SECTION   section number for manual page (1, 6, 8)
 -m, --manual=TEXT       name of manual (User Commands, ...)
 -S, --source=TEXT       source of program (FSF, Debian, ...)
 -L, --locale=STRING     select locale (default "C")
 -i, --include=FILE      include material from `FILE'
 -I, --opt-include=FILE  include material from `FILE' if it exists
 -o, --output=FILE       send output to `FILE'
 -p, --info-page=TEXT    name of Texinfo manual
 -N, --no-info           suppress pointer to Texinfo manual
 -l, --libtool           exclude the `lt-' from the program name
     --help              print this help, then exit
     --version           print version number, then exit

EXECUTABLE should accept `--help' and `--version' options and produce output on
stdout although alternatives may be specified using:

 -h, --help-option=STRING     help option string
 -v, --version-option=STRING  version option string
 --version-string=STRING      version string
 --no-discard-stderr          include stderr when parsing option output

Report bugs to <bug-help2man@gnu.org>.
 help2man \- generate a simple manual page or other Project-Id-Version: help2man
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-06-16 00:01+1000
PO-Revision-Date: 2015-04-01 17:48+0000
Last-Translator: António Miranda <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:47+0000
X-Generator: Launchpad (build 18115)
 %B %Y %s \- página de manual para %s %s %s: impossível criar %s (%s) %s: não foi possível obter `%s' informação de %s%s %s: impossível abrir `%s' (%s) %s: impossível desfazer o atalho de %s (%s) %s: erro ao escrever em %s (%s) %s: nenhuma informação válida encontrada em `%s' AUTOR DISPONIBILIDADE Additional material may be included in the generated output with the
.B \-\-include
and
.B \-\-opt\-include
options.  The format is simple:

    [section]
    text

    /pattern/
    text
 Qualquer
.B [NAME]
ou
.B [SYNOPSIS]
seções que apareçam no ficheiro incluir substituirá o que seria
automaticamente produzido (contudo, ainda pode substituir o
anterior com
.B \-\-name
if required).
 Blocks of verbatim *roff text are inserted into the output either at
the start of the given
.BI [ section ]
(case insensitive), or after a paragraph matching
.BI / pattern /\fR.
 DIREITOS DE AUTOR DESCRIÇÃO AMBIENTE EXEMPLOS Ambiente Exemplos FICHEIROS Ficheiros GNU %s %s

Copyright (C) 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2009, 2010,
2011, 2012, 2013, 2014, 2015 Free Software Foundation, Inc.
Isto é software gratuito; veja a fonte para as condições de difusão. NÃO há
garantia; nem para MEnot even for COMERCIALIZAÇÃO ou FITNESS PARA UM PROPÓSITO PARTICULAR.

Escrito por Brendan O'Dea <bod@debian.org>
 Jogos INCLUIR FICHEIROS Incluir ficheiro para help2man página de manual. Linhas antes da primeira seção ou padrão começam com `\-' são
processadas como opções. Tudo o resto é silenciosamente ignorado e pode
ser usado para comentários, palavras-chave RCS e o gosto.
 NOME OPÇÕES Opções Outras secções são prefixadas para automaticamente produzirem saída para
para as secções padrões dadas acima, ou incluídas em
.I outras
(acima) na ordem em que foram encontradas no arquivo de inclusão.
 Patterns use the Perl regular expression syntax and may be followed by
the
.IR i ,
.I s
or
.I m
modifiers (see
.BR perlre (1)).
 A posição de textos dentro da secção pode ser explicitamente solicitado usando
a sintaxe
.RI [secção <]
.RI [= secção]
ou
.RI [> secção]
para colocar o texto adicional antes de, em lugar de, ou após o padrão
de saída, respetivamente.
 REPORTAR ERROS Report +(?:[\w-]+ +)?bugs|Email +bug +reports +to VER TAMBÉM SINOPSE Utilitários de Administração de Sistema A documentação total para
.B %s
é mantida como um manual Texinfo. Se a
informação de .B 
e
os programas de .B %s
estiverem devidamente instalados no seu site, o comando
.IP
.B info %s
.PP
deverá fornecer-lhe acesso ao manual completo.
 A última versão desta distribuição está disponível online em: A ordem de saída seção (para aqueles incluídos) é: Isto +é +software +livre Tente `--no-discard-stderr' se a opções saírem para stderr Utilização Comandos de Utilizador Escrito +por `%s' generates a man page out of `--help' and `--version' output.

Usage: %s [OPTION]... EXECUTABLE

 -n, --name=STRING       description for the NAME paragraph
 -s, --section=SECTION   section number for manual page (1, 6, 8)
 -m, --manual=TEXT       name of manual (User Commands, ...)
 -S, --source=TEXT       source of program (FSF, Debian, ...)
 -L, --locale=STRING     select locale (default "C")
 -i, --include=FILE      include material from `FILE'
 -I, --opt-include=FILE  include material from `FILE' if it exists
 -o, --output=FILE       send output to `FILE'
 -p, --info-page=TEXT    name of Texinfo manual
 -N, --no-info           suppress pointer to Texinfo manual
 -l, --libtool           exclude the `lt-' from the program name
     --help              print this help, then exit
     --version           print version number, then exit

EXECUTABLE should accept `--help' and `--version' options and produce output on
stdout although alternatives may be specified using:

 -h, --help-option=STRING     help option string
 -v, --version-option=STRING  version option string
 --version-string=STRING      version string
 --no-discard-stderr          include stderr when parsing option output

Report bugs to <bug-help2man@gnu.org>.
 help2man \- gera uma simples página de manual ou outro 