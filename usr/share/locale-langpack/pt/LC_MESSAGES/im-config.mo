��    G      T  a   �        �     t   �       �   2  n   �  "  D  e   g
  X   �
  O   &     v    �    �     �  J   �  �     Q   �  B   �  K   9  ;   �     �     �  x   �  �  v  	  �  �     9   �  �   �  ?   f  {  �  D   "  X   g  F  �            -   +  :   Y  R   �  N   �    6  {   T  �   �  L   l  }   �  <   7  s   t  $   �  0      (   >   %   g   (   �   %   �   &   �   )   !  ,   -!  '   Z!  &   �!  3   �!  %   �!     "      "     0"  6   5"     l"     s"  �  �"  F   R$  >   �$  :   �$     %     1%  �  D%  �   �&  t   ~'     �'  �   (  v   �(  L  ")  k   o+     �+  e   �+     Q,    j,  C  �-     �/  V   �/  �   B0  Z   �0  J   .1  O   y1  B   �1  )   2  !   62  �   X2  �  �2    �4  �   �5  9   e6  �   �6  H   C7  �  �7  E   ::  Z   �:  S  �:     /<  "   8<  7   [<  7   �<  X   �<  L   $=  R  q=  �   �?  �   L@  K   �@  �   BA  B   �A  }   7B  -   �B  /   �B  &   C  +   :C  '   fC  $   �C  -   �C  3   �C  6   D  %   LD  $   rD  2   �D  (   �D     �D  5   �D     5E  7   :E  
   rE     }E  �  �E  I   �G  =   �G  A   H     `H     yH     .   )   A             6             #             :          =   &   @   D   ;   	      /      3       !       1          +       2                                          F   E   B      (      ,      %          7   >   $         C              G          0         "                         ?   
      '      5       *   <       -      8   4   9          
The $IM_CONFIG_XINPUTRC_TYPE is modified by im-config.

Restart the X session to activate the new $IM_CONFIG_XINPUTRC_TYPE.
$IM_CONFIG_RTFM $IM_CONFIG_ID
(c) Osamu Aoki <osamu@debian.org>, GPL-2+
See im-config(8), /usr/share/doc/im-config/README.Debian.gz. $IM_CONFIG_MSG
$IM_CONFIG_MSGA $IM_CONFIG_MSG
$IM_CONFIG_MSGA
  Available input methods:$IM_CONFIG_AVAIL
Unless you really need them all, please make sure to install only one input method tool. $IM_CONFIG_MSG
Automatic configuration selects: $IM_CONFIG_AUTOMATIC
$IM_CONFIG_AUTOMATIC_LONG
$IM_CONFIG_RTFM $IM_CONFIG_MSG
In order to enter non-ASCII native characters, you must install one set of input method tools:
 * ibus and its assocoated packages (recommended)
   * multilingual support
   * GUI configuration
 * fcitx and its assocoated packages
   * multilingual support with focus on Chinese
   * GUI configuration
 * uim and its assocoated packages
   * multilingual support
   * manual configuration with the Scheme code
   * text terminal support even under non-X environments
 * any set of packages which depend on im-config
$IM_CONFIG_MSGA $IM_CONFIG_MSG
Manual configuration selects: $IM_CONFIG_ACTIVE
$IM_CONFIG_ACTIVE_LONG
$IM_CONFIG_RTFM $IM_CONFIG_RTFM
See im-config(8) and /usr/share/doc/im-config/README.Debian.gz for more. *** This is merely a simulated run and no changes are made. ***

$IM_CONFIG_MSG Bogus Configuration Chinese input method (gcin)
 * Required for all: gcin
 * Language specific input conversion support:
  * Traditional Chinese: gcin-chewing
  * Japanese: gcin-anthy
 * Application platform support:
  * GNOME/GTK+: gcin-gtk3-immodule
  * KDE/Qt: gcin-qt4-immodule Current configuration for the input method:
 * Active configuration: $IM_CONFIG_ACTIVE (normally missing)
 * Normal automatic choice: $IM_CONFIG_AUTOBASE (normally ibus or fcitx or uim)
 * Override rule: $IM_CONFIG_PREFERRED_RULE
 * Current override choice: $IM_CONFIG_PREFERRED ($IM_CONFIG_LC_CTYPE)
 * Current automatic choice: $IM_CONFIG_AUTOMATIC
 * Number of valid choices: $IM_CONFIG_NUMBER (normally 1)
The override rule is defined in /etc/default/im-config.
The configuration set by im-config is activated by re-starting X. Custom Configuration Custom configuration is created by the user or administrator using editor. Do you explicitly select the ${IM_CONFIG_XINPUTRC_TYPE}?

 * Select NO, if you do not wish to update it. (recommended)
 * Select YES, if you wish to update it. E: $IM_CONFIG_NAME is bogus configuration for $IM_CONFIG_XINPUTRC. Doing nothing. E: Configuration for $IM_CONFIG_NAME not found at $IM_CONFIG_CODE. E: Configuration in $IM_CONFIG_XINPUTRC is manually managed. Doing nothing. E: Script for $IM_CONFIG_NAME not found at $IM_CONFIG_CODE. E: X server must be available. E: zenity must be installed. Explicit selection is not required to enable the automatic configuration if the active one is default/auto/cjkv/missing. Flexible Input Method Framework (fcitx)
 * Required for all: fcitx
 * Language specific input conversion support:
   * Simplified Chinese: fcitx-pinyin or fcitx-sunpinyin or fcitx-googlepinyin
   * Generic keyboard translation table: fcitx-table* packages
 * Application platform support:
   * GNOME/GTK+: fcitx-frontend-gtk2 and fcitx-frontend-gtk3 (both)
   * KDE/Qt4: fcitx-frontend-qt4 HIME Input Method Editor (hime)
 * Required for all: hime
 * Language specific input conversion support:
  * Traditional Chinese: hime-chewing
  * Japanese: hime-anthy
 * Application platform support:
  * GNOME/GTK+: hime-gtk3-immodule
  * KDE/Qt: hime-qt4-immodule Hangul (Korean) input method
 * XIM: nabi
 * GNOME/GTK+: imhangul-gtk2 and imhangul-gtk3
 * KDE/Qt4: qimhangul-qt4
 * GUI companion: imhangul-status-applet I: Script for $IM_CONFIG_NAME started at $IM_CONFIG_CODE. If a daemon program for the previous configuration is re-started by the X session manager, you may need to kill it manually with kill(1). Input Method Configuration (im-config, ver. $IM_CONFIG_VERSION) Intelligent Input Bus (IBus)
 * Required for all: ibus
 * Language specific input conversion support:
   * Japanese: ibus-mozc (best) or ibus-anthy or ibus-skk
   * Korean: ibus-hangul
   * Simplified Chinese: ibus-pinyin or ibus-sunpinyin or ibus-googlepinyin
   * Traditional Chinese: ibus-chewing
   * Thai: ibus-table-thai
   * Vietnamese: ibus-unikey or ibus-table-viqr
   * X Keyboard emulation: ibus-xkbc
   * Generic keyboard translation table: ibus-m17n or ibus-table* packages
 * Application platform support:
   * GNOME/GTK+: ibus-gtk and ibus-gtk3 (both)
   * KDE/Qt: ibus-qt4
   * Clutter: ibus-clutter
   * EMACS: ibus-el Keeping the $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC as missing. Keeping the $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC unchanged as $IM_CONFIG_ACTIVE. Mallit input method framework
 * Required for everything: maliit-framework
 * Keyboards part of (maliit-plugins):
   * reference keyboard: maliit-keyboard
   * QML keyboard: nemo-keyboard
 * Application platform support:
   * GTK2: maliit-inputcontext-gtk2
   * GTK3: maliit-inputcontext-gtk3
   * Qt4: maliit-inputcontext-qt4 Missing Missing configuration file. Non existing configuration name is specified. Removing the $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC. Select $IM_CONFIG_XINPUTRC_TYPE. The user configuration supersedes the system one. Setting the $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC to $IM_CONFIG_ACTIVE. Smart Common Input Method (SCIM)
 * Required for all: scim
 * Language specific input conversion support:
   * Japanese: scim-mozc (best) or scim-anthy or scim-skk
   * Korean: scim-hangul
   * Simplified Chinese: scim-pinyin or scim-sunpinyin
   * Traditional Chinese: scim-chewing
   * Thai: scim-thai
   * Vietnamese: scim-unikey
   * Generic keyboard translation table: scim-m17 or scim-table* packages
 * Application platform support:
   * GNOME/GTK+: scim-gtk-immodule
   * KDE/Qt4: scim-qt-immodule
   * Clutter: scim-clutter-immodule Thai input method with thai-libthai
 * GNOME/GTK+: gtk-im-libthai and gtk3-im-libthai
 * No XIM nor KDE/Qt4 support (FIXME) The $IM_CONFIG_XINPUTRC_TYPE has been manually modified.
Remove the $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC manually to use im-config.
$IM_CONFIG_RTFM This activates the bare XIM with the X Keyboard Extension for all softwares. This does not set any IM from im-config.
This is the automatic configuration choice if no required IM packages are installed. X input method for Chinese with Sunpinyin
 * XIM: xsunpinyin X input method for Japanese with kinput2
 * XIM: one of kinput2-* packages
 * kanji conversion server: canna or wnn activate Chinese input method (gcin) activate Flexible Input Method Framework (fcitx) activate HIME Input Method Editor (hime) activate Hangul (Korean) input method activate IM with @-mark for most locales activate Intelligent Input Bus (IBus) activate Mallit input method framework activate Smart Common Input Method (SCIM) activate Thai input method with thai-libthai activate XIM for Chinese with Sunpinyin activate XIM for Japanese with kinput2 activate the bare XIM with the X Keyboard Extension activate universal input method (uim) description do not set any IM from im-config name remove IM $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC select system configuration universal input method (uim)
 * Required for all: uim
 * Language specific input conversion support:
   * Japanese: uim-mozc (best) or uim-anthy or uim-skk
   * Korean: uim-byeoru
   * Simplified Chinese: uim-pinyin
   * Traditional Chinese: uim-chewing
   * Vietnamese: uim-viqr
   * General-purpose M17n: uim-m17nlib
 * Application platform support:
   * XIM: uim-xim
   * GNOME/GTK+: uim-gtk2.0 and uim-gtk3 (both)
   * KDE/Qt4: uim-qt
   * EMACS: uim-el use $IM_CONFIG_DEFAULT_MODE mode (bogus content in $IM_CONFIG_DEFAULT) use $IM_CONFIG_DEFAULT_MODE mode (missing $IM_CONFIG_DEFAULT ) use $IM_CONFIG_DEFAULT_MODE mode set by $IM_CONFIG_DEFAULT use auto mode only under CJKV user configuration Project-Id-Version: im-config
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-05-26 23:27+0000
PO-Revision-Date: 2016-01-20 18:58+0000
Last-Translator: Bruno Ramalhete <bram.512@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:56+0000
X-Generator: Launchpad (build 18115)
 
The $IM_CONFIG_XINPUTRC_TYPE é modificado pela im-config.

Reiniciar a sessão X para ativar a nova $IM_CONFIG_XINPUTRC_TYPE.
$IM_CONFIG_RTFM $IM_CONFIG_ID
(c) Osamu Aoki <osamu@debian.org>, GPL-2+
Ver im-config(8), /usr/share/doc/im-config/README.Debian.gz. $IM_CONFIG_MSG
$IM_CONFIG_MSGA $IM_CONFIG_MSG
$IM_CONFIG_MSGA
  Métodos de entrada disponíveis:$IM_CONFIG_AVAIL
Excepto se precisar de todos, instale somente um método de entrada.. $IM_CONFIG_MSG
Seleção da configuração automática: $IM_CONFIG_AUTOMATIC
$IM_CONFIG_AUTOMATIC_LONG
$IM_CONFIG_RTFM $IM_CONFIG_MSG
Para introduzir caracteres não ASCII, tem de instalar um conjunto de ferramentas de método de introdução:
 * ibus e os seus pacotes associados (recomendado)
   * suporte multilingue
   * configuração de GUI
 * fcitx e os seus pacotes associados
   * suporte multilingue com incidência em Chinês
   * configuração de GUI
 * uim e os seus pacotes associados
   * suporte multilingue
   * configuração manual com código Scheme code
   * suporte  de texto terminal, mesmo em ambientes não-X
 * qualquer conjunto de pacotes que dependa do im-config
$IM_CONFIG_MSGA $IM_CONFIG_MSG
Seleção da configuração manual: $IM_CONFIG_ACTIVE
$IM_CONFIG_ACTIVE_LONG
$IM_CONFIG_RTFM $IM_CONFIG_RTFM *** Isto é simplesmente uma execução simulada e não são feitas alterações. ***

$IM_CONFIG_MSG Configuração Fictícia Método de entrada Chinês (gcin)
 * Requer para tudo: gcin
 * Suporte de idioma específico de conversão de entrada:
  * Chinês Tradicional: gcin-chewing
  * Japonês: gcin-anthy
 * Suporte de plataforma de Aplicação:
  * GNOME/GTK+: gcin-gtk3-immodule
  * KDE/Qt: gcin-qt4-immodule Configuração atual para o método de entrada:
 * Configuração Ativa: $IM_CONFIG_ACTIVE (falha normalmente)
 * Escolha automática normal: $IM_CONFIG_AUTOBASE (normalmente ibus ou fcitx ou uim)
 * Regra de sobreposição: $IM_CONFIG_PREFERRED_RULE
 * Escolha de sobreposição atual: $IM_CONFIG_PREFERRED ($IM_CONFIG_LC_CTYPE)
 * Escolha automática atual: $IM_CONFIG_AUTOMATIC
 * Número de escolhas válidas: $IM_CONFIG_NUMBER (normally 1)
A regra de sobreposição é definida em /etc/default/im-config.
A configuração selecionada por im-config é ativada re-iniciando X. Configuração Personalizada Configuração personalizada é criada pelo utilizador ou administrador usando editor. Escolheu especificamente ${IM_CONFIG_XINPUTRC_TYPE}?

 * Escolha não, se não quer atualizar (recomendado)
 * Escolha sim, se deseja atualizar. E: $IM_CONFIG_NAME é uma configuração fictícia de $IM_CONFIG_XINPUTRC. Nada foi feito. E: Configuração para $IM_CONFIG_NAME não encontrada em $IM_CONFIG_CODE. E: Configuração em $IM_CONFIG_XINPUTRC é gerida manualmente. Nada foi feito. E: Script para $IM_CONFIG_NAME não encontrado em $IM_CONFIG_CODE. E: O servidor X tem que estar disponível E: zenity tem que estar instalado Seleção explícita não é requerido para autorizar a configuração automática se a atualmente ativa for por defeito/automático/cjkv/estiver em falta Flexible Input Method Framework (fcitx)
 * Requerido para todos: fcitx
 * suporte de conversão de introdução específica do idioma:
   * Chinês simplificado: fcitx-pinyin ou fcitx-sunpinyin ou fcitx-googlepinyin
   * Tabela de tradução de teclado genérico: pacotes fcitx-table*
 * Suporte da plataforma da aplicação:
   * GNOME/GTK+: fcitx-frontend-gtk2 e fcitx-frontend-gtk3 (ambos)
   * KDE/Qt4: fcitx-frontend-qt4 HIME Input Method Editor (hime)
 * Requer para tudo: hime
 * Suporte de Idioma específico de conversão de entrada:
  * Chinês Tradicional: hime-chewing
  * Japonês: hime-anthy
 * Suporte de plataforma de Aplicação:
  * GNOME/GTK+: hime-gtk3-immodule
  * KDE/Qt: hime-qt4-immodule Hangul (Korean) método de entrada 
  * XIM: nabi
  * GNOME / GTK +: imhangul-gtk2 e imhangul-gtk3
  * KDE/Qt4: qimhangul-qt4
  * GUI companheiro: imhangul-status-applet I: Script para $IM_CONFIG_NAME inciou em $IM_CONFIG_CODE. Se um programa daemon para a configuração anterior for reiniciado pelo gestor da sessão X, poderás precisar de desliga-la manualmente usando o comando kill (1) Configuração do método de entrada im-config, ver. $IM_CONFIG_VERSION) Intelligent Input Bus (IBus)
 * Requerido para todos: ibus
 * suporte de conversão de introdução específica do idioma:
   * Japonês: ibus-mozc (o melhor) ou ibus-anthy ou ibus-skk
   * Coreano: ibus-hangul
   * Chinês simplificado: ibus-pinyin ou ibus-sunpinyin ou ibus-googlepinyin
   * Chinês tradicional: ibus-chewing
   * Tailandês: ibus-table-thai
   * Vietnamita: ibus-unikey ou ibus-table-viqr
   * Emulação de teclado X: ibus-xkbc
   * Tabela de tradução de teclado genérico: pacotes ibus-m17n ou ibus-table*
 * Suporte da plataforma da aplicação:
   * GNOME/GTK+: ibus-gtk e ibus-gtk3 (ambos)
   * KDE/Qt: ibus-qt4
   * Clutter: ibus-clutter
   * EMACS: ibus-el Manter a $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC está em falta. Manter a  $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC inalterada como  $IM_CONFIG_ACTIVE. Mallit estrutura de método de entrada
 * Requer para tudo: maliit-framework
 * Teclados parte de (maliit-plugins):
   * teclado de referência: maliit-keyboard
   * teclado QML: nemo-keyboard
 * Suporte de plataforma de Aplicação:
   * GTK2: maliit-inputcontext-gtk2
   * GTK3: maliit-inputcontext-gtk3
   * Qt4: maliit-inputcontext-qt4 Em Falta Falta fiicheiro de configuração. Nome de configuração não existente foi especificado. A remover $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC. Selecionar $IM_CONFIG_XINPUTRC_TYPE. A configuração de utilizador substitui o sistema. Definir $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC para $IM_CONFIG_ACTIVE. Método de Entrada Comum Inteligente (SCIM)
 * Requerido para todos: scim
 * Suporte a conversão de entrada de idioma específico:
   * Japonês: scim-mozc (o melhor) ou scim-anthy ou scim-skk
   * Coreano: scim-hangul
   * Chinês simplificado: scim-pinyin ou scim-sunpinyin
   * Chinês tradicional: scim-chewing
   * Tailandês: scim-thai
   * Vietnamita: scim-unikey
   * Tabela de tradução de teclado genérico: scim-m17 ou pacotes scim-table*
 * Suporte à plataforma de aplicativos:
   * GNOME/GTK+: scim-gtk-immodule
   * KDE/Qt4: scim-qt-immodule
   * Clutter: scim-clutter-immodule Método de entrada tailandês com thai-libthai
  * GNOME / GTK +: gtk-im-libthai e gtk3-im-libthai
  * No XIM nem KDE/Qt4 apoio (FIXME) The $IM_CONFIG_XINPUTRC_TYPE foi modificado manualmente.
Remove o $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC manualmente para poderes usar o im-config.
$IM_CONFIG_RTFM Isso activa o XIM nua com a Extensão de Teclado X para todos os softwares. Isto não define qualquer IM a partir de im-config.
Esta é a escolha automática de configuração se nenhum pacote de mensagens instantâneas necessário estiverem instalados. Método de entrada X para Chinês com Sunpinyin
 * XIM: xsunpinyin Método de entrada X para Japonês com kinput2
 * XIM: um dos pacotes kinput2-*
 * kanji servidor de conversão: canna ou wnn ativar método de introdução chinês (gcin) activar Flexible Input Method Framework (fcitx) Activar método de entrada HIME (hime) activar método de entrada Hangul (coreano) ativar IM com @-mark para muitos locais activar Intelligent Input Bus (IBus) ativar Mallit estrutura de método de entrada activar Método de Entrada Inteligente Comum (SCIM) activar método de entrada tailandês com thai-libthai activa XIM para Chinês com Sunpinyin activa XIM para Japonês com kinput2 ativar o XIM bruto com a Extensão de teclado do X ativar método de introdução universal descrição não definir qualquer método de entrada de im-config nome remover IM $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC selecionar configuração do sistema método de entrada universal (uim)
  * Obrigatório para todos: uim
  * Linguagem específica apoio de conversão de entrada:
    * Japonês: uim-mozc (melhor) ou uim-anthy ou uim-skk
    * Coreano: uim-byeoru
    * Chinês Simplificado: uim-pinyin
    * Chinês Tradicional: uim-mastigação
    * Vietnamita: uim-VIQR
    * M17n de uso geral: uim-m17nlib
  * Suporte à plataforma de aplicação:
    * XIM: uim-xim
    * GNOME / GTK +: uim-gtk2.0 e uim-gtk3 (ambos)
    * KDE/Qt4: uim-qt
    * EMACS: uim-el usar modo $IM_CONFIG_DEFAULT_MODE (conteúdo falso em $IM_CONFIG_DEFAULT) usar modo $IM_CONFIG_DEFAULT_MODE (falta $IM_CONFIG_DEFAULT ) usar modo $IM_CONFIG_DEFAULT_MODE definido por $IM_CONFIG_DEFAULT use modo auto sobre CJKV configuração do utilizador 