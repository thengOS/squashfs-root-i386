��          �      l      �  /   �          *     E     K     g     �     �  (   �  '   �     �     �          5     F     N     a     w  "   �     �  �  �  2   W  0   �     �     �  $   �  /        3     B  .   P  &        �  (   �     �                "     5     J  #   a     �                                                                   	                        
       %s: option ``%s'' requires a numeric parameter
 Can't open input file %s EOF encountered in comment Error Error reading input file %s File %s is too big to read GNU indent %s
 Line broken Profile contains an unterminated comment Profile contains unpalatable characters Read profile %s
 System problem reading file %s Unexpected end of file Unmatched 'else' Warning indent: %s:%d: %s: indent: Fatal Error:  indent: System Error:  indent: Virtual memory exhausted.
 option: %s
 Project-Id-Version: indent
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-01-31 17:27+0100
PO-Revision-Date: 2016-03-31 08:28+0000
Last-Translator: Ricardo Sousa <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:47+0000
X-Generator: Launchpad (build 18115)
 %s: opção ``%s'' requer um parâmetro numérico
 Não é possível abrir o ficheiro de entrada %s EOF encontrado no comentário Erro Erro ao ler o ficheiro de entrada %s O ficheiro %s é demasiado grande para ser lido GNU indent %s
 Linha partida O perfil contém um comentário não terminado O perfil contém caracteres ilegíveis Perfil de leitura %s
 Problema de sistema ao ler o ficheiro %s Fim de ficheiro inesperado 'else' sem correspondência Aviso indent: %s:%d: %s: indent: Erro fatal:  indent: System Error:  indent: Memória virtual esgotada.
 opção: %s
 