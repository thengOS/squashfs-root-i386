��          �      ,      �  :   �     �  5   �               8  )   H     r     �     �     �     �     �               )  �  H  :        =  >   S     �     �     �  5   �       %     %   @     f  !        �     �     �  2   �                                                	             
                 (C) 2009-2010 Jonathan Thomas, (C) 2009-2014 Harald Sitter Application crashes Configure the behavior of Kubuntu Notification Helper Harald Sitter Incomplete language support Jonathan Thomas Kubuntu Notification Helper Configuration Notification type: Popup notifications only Proprietary Driver availability Required reboots Restricted codec availability Show notifications for: Tray icons only Upgrade information Use both popups and tray icons Project-Id-Version: kubuntu-notification-helper
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-04-01 17:07+0000
PO-Revision-Date: 2015-04-02 23:52+0000
Last-Translator: Ivo Xavier <ivoxavier.8@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:48+0000
X-Generator: Launchpad (build 18115)
 (C) 2009-2010 Jonathan Thomas, (C) 2009-2014 Harald Sitter Falhas na aplicação Configurar o comportamento das Notificações de Ajuda Kubuntu Harald Sitter Suporte ao idioma incompleto Jonathan Thomas Configuração das Notificações de Ajuda do Kubuntu Tipo de notificação: Apenas as notificações de mensagens Controlador Proprietário disponível Requer reinicialização Disponibilidade de codec restrito Mostrar notificaçoẽs para: Apenas os ícones da bandeja Atualizar informação Usar tanto as mensagens como os ícones da bandeja 