��    #      4  /   L           	  ;     '   S     {     �     �     �     �     �            �        �     �     �     �     �  (        >     P  �   a  �        �     �     �  "   �     	       !   ,     N  
   `     k  N   t  _   �  �  #     �	  O   �	  '   ,
     T
     l
  	   �
     �
  $   �
     �
     �
     �
  �   �
     �     �     �     �  "   �  4        Q     d  �   y  �   7     �          &  '   -     U     ^     z     �     �     �  ]   �  ]   !                                      	                       "                 
      #                                                                 !        Account name: Allow the user to read and write Landscape Client settings. Attempting to disable landscape client. Attempting to register at %s Authentication failed Disable Disabling client failed Disabling client was successful Don't have an account? Failure. Find out more... If you click "Disable" the Landscape client on this machine will be disabled.  You can reenable it later by revisiting this dialog. Install Install Landscape client? Invalid host name. Landscape - dedicated server Landscape - hosted by Canonical Landscape Management Service Preferences Landscape Service Landscape client Landscape is a remote administration service from Canonical.  If you allow it, a Landscape server can monitor this computer's performance and send administration commands. Landscape is an easy-to-use commercial systems management and monitoring service offered by Canonical that helps administrators manage multiple machines efficiently. Landscape server hostname: Landscape service: None Only ASCII characters are allowed. Register Registering client failed Registering client was successful Registration Key: Sign up... Success. System policy prevents you from reading and writing Landscape Client Settings. You need to install Landscape client to be able to configure it. Do you want to install it now? Project-Id-Version: landscape-client
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-03-10 19:33+0000
PO-Revision-Date: 2013-02-20 22:05+0000
Last-Translator: Filipe André Pinho <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:58+0000
X-Generator: Launchpad (build 18115)
 Nome da conta: Permitir ao utilizador ler e escrever nas configurações do cliente Landscape. A tentar desativar o cliente landscape. A tentar registar em %s Falha na autenticação Desativar Desativação de cliente falhou Desativação de cliente com sucesso Não tem uma conta? Falha. Descubra mais... Se clicar em "Desativar", o cliente Landscape neste computador ficará inactivo. Pode voltar a ativá-lo mais tarde visitando novamente este diálogo. Instalar Instalar o cliente Landscape? Nome de anfitrião inválido. Landscape - servidor dedicado Landscape - alojado pela Canonical Preferências do serviço de de gestão do Landscape Serviço Landscape Cliente de Landscape O Landscape é um serviço remoto de administração da Canonical. Se o permitir, um servidor Landscape pode monitorizar a performance deste computador e enviar comandos de administração. O Landscape é um serviço de gestão comercial de sistemas e monitorização de uso fácil, oferecido pela Canonical que ajuda aos administradores na gestão eficiente de várias máquinas. Nome do servidor Landscape: Serviço Landscape: Nenhum Apenas são permitidos caracteres ASCII Registar Falha no registo do cliente Registo do cliente bem sucedido Chave de registo: Registe-se... Sucesso. As políticas do sistema impedem-lhe de ler e escrever nas definições do cliente Landscape. Necessita de instalar o cliente Landscape para poder configurá-lo. Deseja instalá-lo agora? 