��    �      |  �   �
      p     q     �  	   �     �  #   �  "   �     	  	   #     -     >     G  
   O     Z  
   b     m     u     �  !   �  <   �       6         U  %   v     �  #   �     �  ,   �     $  ,   B  ,   o  '   �  -   �      �  (     (   <  !   e      �     �     �  9   �     "  
   $  -   /  3   ]  1   �  0   �     �               -     E     ]     n     �     �      �  �   �     t  @   y  ,   �     �     �  
             &     A     _  `   y     �     �  	     8     2   D     w     �     �  !   �  #   �  6   �     0     A     O  !   ^     �     �     �     �     �     �     �  "        6     H     W     s     �     �     �     �      �            !  "   7  )   Z     �  $   �     �     �  
   �     �  '        4     A     Y  '   x     �     �     �     �     �     �       &   (     O     a  )   p     �  	   �     �     �  +   �  #   
     .     B     G     ^     o     �  #   �  8   �  -   �     (     H     c     y     �     �     �  
   �     �     �     	          &     :     Y     l     y     �     �  #   �     �     �     �            0      P      e   6   q      �      �   �  �   &   s"     �"     �"     �"  %   �"  $   �"     ##     ?#     K#     \#     e#  
   m#     x#  
   �#     �#     �#     �#  $   �#  ?   �#     0$  A   J$  "   �$  4   �$     �$  "   %     '%  /   F%     v%  -   �%  /   �%  )   �%  0   &  "   O&  *   r&  *   �&  )   �&      �&  %   '  %   9'  9   _'     �'  
   �'  0   �'  3   �'  1   (  0   =(     n(     �(     �(     �(     �(     �(     �(     )     3)  1   B)  �   t)  
   .*  Q   9*  1   �*  
   �*      �*     �*     �*     +  '   /+     W+  h   w+     �+  ,   �+     ,  H   &,  @   o,      �,     �,     �,  -   �,  $   --  5   R-     �-     �-     �-  '   �-  &   �-     .     %.  %   ;.     a.     t.     �.  *   �.     �.     �.  #   �.      /  &   //     V/     v/     �/  '   �/  )   �/     �/  .   	0  ;   80     t0  )   �0     �0     �0     �0     1  0   !1     R1     `1      y1  *   �1     �1     �1     �1     �1     2     2     <2  $   W2     |2     �2  :   �2     �2  	   �2     �2      3  1   '3  ,   Y3     �3     �3     �3     �3     �3     �3  5   	4  =   ?4  =   }4     �4     �4     �4     5  '   /5     W5     i5  
   {5     �5     �5     �5     �5     �5     �5     6     6     &6     >6     N6  0   i6     �6     �6  #   �6      �6     7     37     H7  3   T7  $   �7     �7     !         �           �   L   A       �       3      f   7   i       �   N   (   �   <      �   O   �       �       W   �   �   ?          V   ]          p   C   2   s   �           �      �             }      l      c   �   x   t           ~      j   �   "       B   �   K   �   �   �       u   -   *       E   h                  e   a            I   J   .   �              �   �   �   b   �              �   �   {   �   @       ;   g       4   R   
   6   &       y   9   U   	   v           k       [       o   �   Y   H   G      5   ^   ,   F   �   �   #   =      _       T   Z   d   �              \           D   /   �               �   '   1           >   �           �       P           r   $          M       �   �   Q   n   �   :   8   w      �              %       X   0   q   `   �                            z   S   �   +              )   �   m   �   �   �       |       	Executing builtin `%s' [%s]
 	Repeat count: %d
 	Running
 	Waiting for command
 	Waiting for job [%d] to terminate
 	Waiting for termination of jobs:   - not supported protocol  [cached] !<shell-command> %.0f B/s %.0fb/s %.1f KiB/s %.1fK/s %.2f MiB/s %.2fM/s %d address$|es$ found %ld $#l#byte|bytes$ cached %lld $#ll#byte|bytes$ transferred %lld $#ll#byte|bytes$ transferred in %ld $#l#second|seconds$ %s%d error$|s$ detected
 %s: %s: file already exists and xfer:clobber is unset
 %s: cannot create local session
 %s: command `%s' is not compiled in.
 %s: date-time parse error
 %s: date-time specification missed
 %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: received redirection to `%s'
 %s: regular expression `%s': %s
 %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 %sTotal: %d director$y|ies$, %d file$|s$, %d symlink$|s$
 ' (commands) **** FXP: giving up, reverting to plain copy
 **** FXP: trying to reverse ftp:fxp-passive-source
 **** FXP: trying to reverse ftp:fxp-passive-sscn
 **** FXP: trying to reverse ftp:ssl-protect-fxp
 , no size limit Access failed:  Ambiguous command `%s'.
 Closing HTTP connection Closing idle connection Commands queued: Connecting to %s%s (%s) port %u Connecting... Connection idle Could not parse HTTP status line Define or undefine alias <name>. If <value> omitted,
the alias is undefined, else is takes the value <value>.
If no argument is given the current aliases are listed.
 Done Failed to change mode of `%s' because no old mode is available.
 Failed to change mode of `%s' to %04o (%s).
 Fatal error File cannot be accessed File moved File moved to ` Getting directory contents Getting file list (%lld) [%s] Getting files information Group commands together to be executed as one command
You can launch such a group in background
 Hit EOF Hit EOF while fetching headers Interrupt Invalid time format. Format is <time><unit>, e.g. 2h30m. Invalid time unit letter, only [smhd] are allowed. Link <file1> to <file2>
 Login failed Making directory `%s' Making symbolic link `%s' to `%s' Mode of `%s' changed to %04o (%s).
 Module for command `%s' did not register the command.
 No address found Not connected Now executing: Old directory `%s' is not removed Old file `%s' is not removed Operation not supported POST method failed Peer closed connection Queue is stopped. Received all Received all (total) Received not enough data, retrying Receiving body... Receiving data Removing old directory `%s' Removing old file `%s' Removing old local file `%s' Running connect program Sending data Sending request... Socket error (%s) - reconnecting Store failed - you have to reput Too many redirections Total %d $file|files$ transferred
 Transfer of %d of %d $file|files$ failed
 Transferring file `%s' Try `help %s' for more information.
 Unknown command `%s'.
 Unknown system error Usage: %s
 Valid arguments are: Verify command failed without a message Verifying... Waiting for response... Warning: chdir(%s) failed: %s
 Warning: discarding incomplete command
 [%d] Done (%s) [re]nlist [<args>] ` `%s' at %lld %s%s%s%s alias [<name> [<value>]] ambiguous argument %s for %s ambiguous variable name anon - login anonymously (by default)
 bookmark [SUBCMD] cache [SUBCMD] cannot create socket of address family %d cat [-b] <files> cd <rdir> cd ok, cwd=%s
 chmod [OPTS] mode file... copy: destination file is already complete
 copy: received redirection to `%s'
 du [options] <dirs> eta: execlp(%s) failed: %s
 exit [<code>|bg] extra server response file name missed in URL file size decreased during transfer ftp over http cannot work without proxy, set hftp:proxy. ftp:fxp-force is set but FXP is not available get [OPTS] <rfile> [-o <lfile>] invalid argument %s for %s invalid boolean value invalid boolean/auto value invalid floating point number invalid number kill all|<job_no> lcd <ldir> lftp [OPTS] <site> ln [-s] <file1> <file2> ls [<args>] memory exhausted mget [OPTS] <files> mirror [OPTS] [remote [local]] module name [args] more <files> mput [OPTS] <files> mrm <files> mv <file1> <file2> no closure defined for this setting no such %s service no such variable non-option arguments found pget [OPTS] <rfile> [-o <lfile>] put [OPTS] <lfile> [-o <rfile>] queue [OPTS] [<cmd>] quote <cmd> recls [<args>]
Same as `cls', but don't look in cache
 this encoding is not supported total Project-Id-Version: lftp
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-06-17 17:10+0300
PO-Revision-Date: 2014-07-26 17:57+0000
Last-Translator: IvoGuerreiro <ivoguerreiro@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:54+0000
X-Generator: Launchpad (build 18115)
 	Executando comando interno `%s' [%s]
 	Repetir contagem: %d
 	A executar
 	À espera de um comando
 	À espera que a tarefa [%d] termine
 	À espera que terminem as tarefas:   - protocolo não suportado  [na cache] !<shell-command> %.0f B/s %.0fb/s %.1f KiB/s %.1fK/s %.2f MiB/s %.2fM/s %d endereço$|es$ encontrado %ld $#l#byte|bytes$ cached %lld $#ll#byte|bytes$ transferido(s) %lld $#ll#byte|bytes$ transferidos em %ld $#l#segundo|segundos$ %s%d erro$|s$ detectados
 %s: %s: o ficheiro já existe e xfer:clobber não está definido
 %s: não pode criar sessão local
 %s: comando `%s' não está compilado internamente.
 %s: erro da análise data-hora
 %s: data-tempo faltou especificar
 %s: opção inválida -- '%c'
 %s: a opção '%c%s' não permite um argumento
 %s: a opção '%s' é ambígua
 %s: opção '%s' é ambígua; possibilidades: %s: a opção '--%s' não permite um argumento
 %s: a opção '--%s' requer um argumento
 %s: a opção '-W %s' não permite um argumento
 %s: a opção '-W %s' é ambígua
 %s: a opção '-W %s' requer um argumento
 %s: a opção requer um argumento -- '%c'
 %s: recebido redireccionamento para `%s'
 %s: expressão regular `%s': %s
 %s: a opção '%c%s' é desconhecida
 %s: a opção '--%s' é desconhecida
 %sTotal: %d director$y|ies$, %d file$|s$, %d symlink$|s$
 " (comandos) **** FXP: a desistir, voltar para cópia normal
 **** FXP: a tentar reverter ftp:fxp-passive-source
 **** FXP: a tentar reverter ftp:fxp-passive-sscn
 **** FXP: a tentar reverter ftp:ssl-protect-fxp
 , nenhum limite de tamanho Falhou o acesso:  Comando ambíguo `%s'.
 A fechar ligação HTTP A fechar ligação inactiva Comandos em fila: A conetar a %s%s (%s) porta %u A estabelecer ligação... idle Ligação Não foi possível parsear a linha de estado HTTP Definir ou indefinir alias <name>. Se <value> omitido,
o alias é indefinido, ou é necessário o valor <value>
Se nenhum argumento for fornecido os aliases correntes ficaram listados.
 Concluído Falha ao mudar o modo de `%s'  porque não está disponível nenhum modo antigo.
 Falha a mudança do modo de `%s' para %04o (%s).
 Erro fatal O ficheiro não pode ser acedido Ficheiro movido Ficheiro movido para ` Obter conteúdos do diretório A obter a lista de ficheiro (%lld) [%s] Obter informação do ficheiros Agrupa comandos para serem executados como um comando
Estes grupos podem ler lançados em segundo plano
 EOF atingido EOF atingido enquanto buscava os cabeçalhos Interromper Formato de tempo inválido.  O formato é <tempo><unidade>, p.ex. 2h30m. Letra de unidade de tempo inválida, só são permitidas [smhd]. Link <ficheiro1> to <ficheiro2>
 Inicio de sessão falhou Criar diretorio `%s' A criar a ligação simbólica `%s' para `%s' Modo de `%s' mudado para %04o (%s).
 O módulo para o comando`%s' não aceitou o comando.
 Nenhum endereço encontrado Não ligado A executar: Diretorio antigo `%s' não foi removido Ficheiro antigo `%s' não foi removido Operação não suportada O método POST falhou O par da ligação fechou a ligação Fila está parada. Recebidos todos Recebidos todos (total) Não recebi dados suficientes, a re-tentar A receber corpo... A receber dados A remover o directório antigo `%s' A remover o ficheiro antigo `%s' A remover o ficheiro local antigo `%s' A executr programa de ligação A enviar dados A enviar pedido... Erro de socket (%s) - a ligar novamente Armazenamento falhou - tem que re-colocar Demasidados redireccionamentos Foram transferidos %d $ficheiro|ficheiros$  .
 Falhou a transferência de %d de %d $ficheiro|ficheiros$ .
 Transferindo ficheiro `%s' Para mais informações tente `help %s'.
 Comando desconhecido `%s'.
 Erro desconhecido de sistema Utilização: %s
 Os argumentos válidos são: A verificação do comando falhou, sem mensagens A verficar... À espera de resposta... Atenção: chdir(%s) falhou: %s
 Atenção: descartando comando incompleto
 [%d] Concluído (%s) [re]nlist [<args>] " `%s' em %lld %s%s%s%s alias [<nome> [<valor>]] argumento ambíguo %s para %s nome de variável ambíguo anon - login anónimo (por padrão)
 bookmark [SUBCMD] cache [SUBCMD] não é possível criar socket da familia de endereços %d cat [-b] <ficheiros> cd <rdir> cd ok, cwd=%s
 chmod [OPTS] modo de ficheiro... cópia: o ficheiro de destino já está completo
 cópia: recebido redirecionamento para `%s'
 du [opções] <dirs> eta: o execlp(%s) falhou: %s
 exit [<code>|bg] resposta adicional do servidor falta o nome de ficheiro na URL tamanho do ficheiro diminuiu durante a transferência FTP sobre HTTP não funciona sem um proxy, defina hftp:proxy. ftp:fxp-force está definido mas o FXP não está disponível get [OPTS] <rfile> [-o <lfile>] argumento %s inválido para %s valor boleano inválido valor lógico/auto inválido número de vírgula flutuante inválido número inválido kill all|<job_no> lcd <ldir> lftp [OPTS] <site> ln [-s] <file1> <file2> ls [<args>] memória esgotada mget [OPTS] <files> mirror [OPTS] [remote [local]] module name [args] more <ficheiros> mput [OPTS] <ficheiros> mrm <ficheiros> mv <ficheiro1> <ficheiro2> não há fecho definido para esta configuração o serviço %s não existe não existe tal variável encontrados argumentos não-opção pget [OPTS] <rfile> [-o <lfile>] put [OPTS] <lfile> [-o <rfile>] queue [OPTS] [<cmd>] quote <cmd> recls [<args>]
O mesmo que `cls', mas vê no cache
 esta codificação não é suportada total 