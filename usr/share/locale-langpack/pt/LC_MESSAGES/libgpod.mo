��    m      �  �   �      @	      A	     b	  	   p	  *   z	  0   �	     �	     �	  /   �	     
  ,   1
  A   ^
     �
      �
  H   �
      (  J   I  .   �     �  $   �  $     (   *  O   S  I   �  #   �       !   +     M  #   k  !   �     �  	   �     �     �  5     /   7     g     o  U   �  I   �  	   .     8     D     P     ]     i  
   w     �     �  -   �     �     �     �     �            #   &  4   J  3     +   �  9   �  9        S     `  =   p  D   �  F   �  "   :     ]     s  .   �  D   �  E   �     ;     D  .   M     |  *   �  X   �          #     +  :   A  �   |  N   o  ?   �  1  �     0     >     L  8   U  (   �  C   �  �   �  Q   �     �  .   �  Y     N   e  ,   �  T   �  =   6  5   t  5   �  H   �  \   )  R   �  ^   �  D   8  �  }  "        =  	   L  <   V  S   �     �     �  8   �  $   +  A   P  T   �  +   �  $      C   8   (   |   R   �   F   �      ?!  %   [!  %   �!  9   �!  c   �!  ]   E"  4   �"     �"  ,   �"      #     :#      T#      u#     �#     �#     �#  ?   �#  9   !$  	   [$  0   e$  w   �$  X   %  	   g%     q%     }%     �%     �%     �%  
   �%      �%  !   �%  7   &     <&     I&     U&     b&     u&     �&  /   �&  W   �&  R   '  4   n'  ?   �'  ?   �'     #(     =(  K   Z(  S   �(  X   �(  ,   S)     �)     �)  ;   �)  a   �)  d   U*     �*     �*  0   �*     
+  6   +  ]   J+     �+     �+     �+  c   �+  A  T,  r   �-  I   	.  V  S.     �/     �/  	   �/  I   �/  3   0  _   Q0  �   �0  \   F1     �1  6   �1  �   �1  n   `2  /   �2  ]   �2  L   ]3  ?   �3  ?   �3  Y   *4  h   �4  W   �4  u   E5  Y   �5        #       i   <       f       [      -   _                     T   d   !       5   (      ,   h         :      "       O      V   2                  \           P      ;   e       m   *       M      L      J   	          S   Z   .   ?   +   k   `      3              X   E               =   g   1       U         Q   '   6              A   ^              %   C   /   )   4       D   K   @   j   I      $       W      8           B   a       7              G   F          l           >   b      N   
   9       ]   H   Y             0   &          R           c    '%s' could not be accessed (%s). <No members>
 <Unnamed> Artwork support not compiled into libgpod. Cannot remove Photo Library playlist. Aborting.
 Color Color U2 Control directory not found: '%s' (or similar). Could not access file '%s'. Could not access file '%s'. Photo not added. Could not find corresponding track (dbid: %s) for artwork entry.
 Could not find on iPod: '%s'
 Could not open '%s' for writing. Destination file '%s' does not appear to be on the iPod mounted at '%s'. Device directory does not exist. Encountered unknown MHOD type (%d) while parsing the iTunesDB. Ignoring.

 Error adding photo (%s) to photo database: %s
 Error creating '%s' (mkdir)
 Error opening '%s' for reading (%s). Error opening '%s' for writing (%s). Error reading iPod photo database (%s).
 Error reading iPod photo database (%s).
Will attempt to create a new database.
 Error reading iPod photo database, will attempt to create a new database
 Error reading iPod photo database.
 Error removing '%s' (%s). Error renaming '%s' to '%s' (%s). Error when closing '%s' (%s). Error while reading from '%s' (%s). Error while writing to '%s' (%s). Error: '%s' is not a directory
 Grayscale Grayscale U2 Illegal filename: '%s'.
 Illegal seek to offset %ld (length %ld) in file '%s'. Insufficient number of command line arguments.
 Invalid Itdb_Track ID '%d' not found.
 Length of smart playlist rule field (%d) not as expected. Trying to continue anyhow.
 Library compiled without gdk-pixbuf support. Picture support is disabled. Master-PL Mini (Blue) Mini (Gold) Mini (Green) Mini (Pink) Mini (Silver) Mobile (1) Mountpoint not set. Mountpoint not set.
 Music directory not found: '%s' (or similar). Nano (Black) Nano (Blue) Nano (Green) Nano (Pink) Nano (Silver) Nano (White) No 'F..' directories found in '%s'. Not a OTG playlist file: '%s' (missing mhpo header). Not a Play Counts file: '%s' (missing mhdp header). Not a iTunesDB: '%s' (missing mhdb header). Number of MHODs in mhip at %ld inconsistent in file '%s'. Number of MHODs in mhyp at %ld inconsistent in file '%s'. OTG Playlist OTG Playlist %d OTG playlist file '%s': reference to non-existent track (%d). OTG playlist file ('%s'): entry length smaller than expected (%d<4). OTG playlist file ('%s'): header length smaller than expected (%d<20). Path not found: '%s' (or similar). Path not found: '%s'. Photo Library Photos directory not found: '%s' (or similar). Play Counts file ('%s'): entry length smaller than expected (%d<12). Play Counts file ('%s'): header length smaller than expected (%d<96). Playlist Podcasts Problem creating iPod directory or file: '%s'. Shuffle Specified album '%s' not found. Aborting.
 Unexpected error in itdb_photodb_add_photo_internal() while adding photo, please report. Unexpected mhsd index: %d
 Unknown Unknown command '%s'
 Unknown smart rule action at %ld: %x. Trying to continue.
 Usage to add photos:
  %s add <mountpoint> <albumname> [<filename(s)>]
  <albumname> should be set to 'NULL' to add photos to the master photo album
  (Photo Library) only. If you don't specify any filenames an empty album will
  be created.
 Usage to dump all photos to <output_dir>:
  %s dump <mountpoint> <output_dir>
 Usage to list all photos IDs to stdout:
  %s list <mountpoint>
 Usage to remove photo IDs from Photo Library:
  %s remove <mountpoint> <albumname> [<ID(s)>]
  <albumname> should be set to 'NULL' to remove photos from the iPod
  altogether. If you don't specify any IDs, the photoalbum will be removed
  instead.
  WARNING: IDs may change when writing the PhotoDB file.
 Video (Black) Video (White) Video U2 Warning: could not find photo with ID <%d>. Skipping...
 Wrong number of command line arguments.
 You need to specify the iPod model used before photos can be added. Your iPod does not seem to support photos. Maybe you need to specify the correct iPod model number? It is currently set to 'x%s' (%s/%s). header length of '%s' smaller than expected (%d < %d) at offset %ld in file '%s'. iPod iTunes directory not found: '%s' (or similar). iTunesDB '%s' corrupt: Could not find playlists (no mhsd type 2 or type 3 sections found) iTunesDB '%s' corrupt: Could not find tracklist (no mhsd type 1 section found) iTunesDB '%s' corrupt: mhsd expected at %ld. iTunesDB ('%s'): header length of mhsd hunk smaller than expected (%d<32). Aborting. iTunesDB corrupt: hunk length 0 for hunk at %ld in file '%s'. iTunesDB corrupt: no MHOD at offset %ld in file '%s'. iTunesDB corrupt: no SLst at offset %ld in file '%s'. iTunesDB corrupt: no section '%s' found in section '%s' starting at %ld. iTunesDB corrupt: number of mhip sections inconsistent in mhyp starting at %ld in file '%s'. iTunesDB corrupt: number of tracks (mhit hunks) inconsistent. Trying to continue.
 iTunesDB possibly corrupt: number of playlists (mhyp hunks) inconsistent. Trying to continue.
 iTunesStats file ('%s'): entry length smaller than expected (%d<18). Project-Id-Version: libgpod
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-04-13 08:40+0000
PO-Revision-Date: 2007-03-16 23:57+0000
Last-Translator: Marco Rodrigues <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:52+0000
X-Generator: Launchpad (build 18115)
 '%s' não pôde ser acessado (%s). <Sem membros>
 <SemNome> O suporte para a arte de capa não foi compilado no libgpod. Não foi possível remover a lista de reprodução biblioteca de fotos. Abortando.
 Cor Cor U2 Arquivo de controlo não encontrado: '%s' (ou parecido). Impossível aceder ao ficheiro '%s'. Impossível aceder ao ficheiro: '%s'. Fotografia não adicionada. Não foi possível encontrar a faixa correspondente (dbid: %s) para a arte de capa.
 Não foi possível encontrar no iPod: '%s'
 Impossível abrir '%s' para escrita. Ficheiro de destino '%s' parece não estar no iPod montado em '%s'. O diretório não existe no dispositivo. Foi encontrado um tipo MHOD desconhecido (%d) ao analisar a iTunesBD. A ignorar.

 Erro ao adicionar fotografia (%s) à base de dados de fotografias: %s
 Erro ao criar '%s' (mkdir)
 Erro ao abrir '%s' para leitura (%s). Erro ao abrir '%s' para escrita (%s). Erro ao ler a base de dados de fotografias do iPod (%s).
 Erro ao ler a base de dados de fotografias do iPod (%s).
Irá tentar criar uma nova base de dados.
 Erro ao ler a base de dados de fotografias do iPod, irá tentar criar uma nova base de dados
 Erro ao ler a base de dados de fotografias do iPod.
 Erro ao remover '%s' (%s). Erro ao mudar o nome de '%s' para '%s' (%s). Erro ao fechar '%s' (%s). Erro ao ler de '%s' (%s). Erro ao escrever para '%s' (%s). Erro: '%s' não é uma arquivo.
 Escala de cinzentos Escala de cinzentos U2 Nome de ficheiro ilegal: '%s'.
 Avanço ilegal na posição %ld (tamanho %ld) no ficheiro '%s'. Número insuficiente de argumentos na linha de comandos.
 Inválida Identificação Itdb_Track '%d' não encontrad.
 Tamanho do campo regra da lista de reprodução inteligente (%d) não como a esperada. A tentar continuar mesmo assim.
 Libraria compilada sem suportar o gdk-pixbuf. Suporte para fotografia está desactivado. PL-Mestre Mini (Azul) Mini (Ouro) Mini (Verde) Mini (Cor de rosa) Mini (prata) Móvel (1) Ponto de montagem não definido. Ponto de montagem não definido.
 Arquivo de música não encontrado: '%s' (ou parecido). Nano (Preto) Nano (Azul) Nano (Verde) Nano (Cor de Rosa) Nano (Cinzento) Nano (Branco) Nenhumas directorias 'F..' encontradas em '%s'. Não é um ficheiro do tipo lista de reprodução OTG: '%s' (cabeçalho mhpo em falha). Não é um ficheiro de Contagem de reproduções: '%s' (cabeçalho mhdp em falta). Não é uma iTunesBD '%s' (cabeçalho mhdb em falta) Número de MHODs no mhip no %ld inconsistente no ficheiro '%s'. Número de MHODs no mhyp no %ld inconsistente no ficheiro '%s'. Lista de reprodução OTG Lista de reprodução OTG %d Lista de reprodução OTG ('%s'): referência a uma pista inexistente (%d). Entrada do arquivo de lista de reprodução OTG ('%s') menor que o esperado (%d<4). Lista de reprodução OTG ('%s'): tamanho do cabeçalho menor do que o esperado (%d<20). Caminho não encontrado: '%s' (ou parecido). Caminho não encontrado: '%s'. Libraria de fotografias Arquivo de fotografias não encontrado: '%s' (ou parecido). Ficheiro de Contagem de reproduções ('%s'): tamanho da entrada menor do que a esperada (%d<12). Ficheiro de Contagem de reproduções ('%s'): tamanho do cabeçalho menor do que o esperado (%d<96). Lista de reprodução Podcasts Erro ao criar arquivo ou ficheiro de iPod: '%s'. Misturar Albúm específicado '%s' não encontrado. A abortar.
 Erro inesperado em itdb_photodb_add_photo_internal() ao adicionar a foto, por favor, reporte. Índice mhsd não esperado: %d
 Desconhecido Comando desconhecido '%s'
 Opção da regra da lista de reprodução inteligente desconhecida em %ld: %x. Tentando continuar.
 Uso para adicionar fotos:
    %s add <ponto de montagem> <nome do álbum> [<nome do(s) arquivo(s)>]
    <nome do álbum> deve estar definido como 'NULL' para adicionar fotos
    apenas ao álbum de fotos principal (Biblioteca de Fotos). Se você não
    especificar nenhum nome de arquivo, um álbum vazio será criado.
 Uso para depositar todas as fotos em <diretório de saída>:
  %s dump <ponto de montagem> <diretório de saída>
 Uso para listar todas as fotos na saída padrão:
  %s list <mountpoint>
 Uso para remover IDs de fotos da biblioteca de fotos:
  %s remove <ponto de montagem> <nome do álbum> [<ID(s)>]
  <nome do álbum> deve ser definido como 'NULL' para remover as fotos do
  iPod. Se você não especificar nenhum IDs, o álbum de fotos será removido.
  ATENÇÃO: IDs podem mudar ao escrever informações no arquivo PhotoDB.
 Vídeo (Preto) Vídeo (Branco) Vídeo U2 Atenção: não foi possível encontrar a foto com ID <%d>. Ignorando...
 Número errado de argumentos na linha de comandos.
 Você necessita de especificar o modelo do iPod utilizado antes de poder adicionar fotografias. Parece que seu iPod não suporta fotos. Talvez você precise especificar o modelo correto de iPod. Atualmente ele está definido como 'x%s' (%s/%s). tamanho do cabeçalho de '%s' menor que o esperado (%d < %d) no offset %ld no ficheiro '%s'. iPod Arquivo do iTunes não encontrado: '%s' (ou parecido). iTunesBD '%s' corrumpida: Incapaz de encontrar as listas de reprodução (nenhuma secção mhsd do tipo 1 ou tipo 3 encontradas) iTunesBD '%s' corrumpida: Incapaz de encontrar a lista das pistas (nenhuma secção mhsd do tipo 1 encontrada) iTunesBD '%s' corrumpida: mhsd esperada em %ld. iTunesDB ('%s'): tamanho do cabeçalho do bloco mhsd menor que o esperado (%d<32). Abortando. iTunesDB corrompido: tamanho de bloco 0 para o bloco em %ld no arquivo '%s'. iTunesBD corrumpida: nehum MHOD no offset %ld no ficheiro '%s'. iTunesBD corrumpida: nehum SLst no offset %ld no ficheiro '%s'. iTunesBD corrumpida: nenhuma secção '%s' encontrada na secção '%s' a começar em %ld. iTunesBD corrumpida: o número de secções mhip inconsistem em mhyp a começar em %ld no ficheiro '%s'. iTunesBD corrumpida: número de pistas (mhit hunks) inconsistente. A tentar continuar.
 iTunesBD possivelmente corrumpida: número de listas de reprodução (mhyp hunks) inconsistente. A tentar continuar.
 Ficheiro de iTunesEstatiticas ('%s'): tamanho da entrada menor do que a esperada (%d<18). 