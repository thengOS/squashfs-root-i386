��    ;      �  O   �        �   	  c   �  �     a   �  K   �  �   D  '  A     i	     �	  *   �	  �  �	  -   �  &   �     �  .     )   E  )   o  -   �  I   �               >  "   L      o  &   �  -   �  -   �          3  #   C  6   g     �     �     �     �     �  &     O   +  -   {     �     �  #   �  "   �  %     "   4  %   W     }     �     �     �     �     �  5   �     3     G     [     r     �  �  �  �   ?  p   �  �   I  d   �  J   T    �  (  �     �     �  ;   �  s  0  2   �  +   �       :   !  <   \  )   �  /   �  _   �     S      e     �  -   �  &   �  1   �  2     4   P     �     �  #   �  9   �       $   0     U     ]     s  +   �  a   �  +        B     T  +   h  %   �  (   �  %   �  (   	   &   2      Y      n      �      �      �   :   �      �      !      !      7!     X!     7   6   )      -   &                     (   *   8                      1   ;         "   
          $           9   4                              3          +   2          5              /                     :   !                    0   #   %   	      ,   '                .              --allow-unassigned   Toggle IDNA AllowUnassigned flag (default off)
      --usestd3asciirules  Toggle IDNA UseSTD3ASCIIRules flag (default off)
       --debug              Print debugging information
      --quiet              Silent operation
       --no-tld             Don't check string for TLD specific rules
                             Only for --idna-to-ascii and --idna-to-unicode
   -h, --help               Print help and exit
  -V, --version            Print version and exit
   -n, --nfkc               Normalize string according to Unicode v3.2 NFKC
   -p, --profile=STRING     Use specified stringprep profile instead
                             Valid stringprep profiles: `Nameprep',
                             `iSCSI', `Nodeprep', `Resourceprep', 
                             `trace', `SASLprep'
   -s, --stringprep         Prepare string according to nameprep profile
  -d, --punycode-decode    Decode Punycode
  -e, --punycode-encode    Encode Punycode
  -a, --idna-to-ascii      Convert to ACE according to IDNA (default mode)
  -u, --idna-to-unicode    Convert from ACE according to IDNA
 Cannot allocate memory Charset `%s'.
 Code points prohibited by top-level domain Command line interface to the internationalized domain name library.

All strings are expected to be encoded in the preferred charset used
by your locale.  Use `--debug' to find out what this charset is.  You
can override the charset used by setting environment variable CHARSET.

To process a string that starts with `-', for example `-foo', use `--'
to signal the end of parameters, as in `idn --quiet -a -- -foo'.

Mandatory arguments to long options are mandatory for short options too.
 Conflicting bidirectional properties in input Error in stringprep profile definition Flag conflict with profile Forbidden leading or trailing minus sign (`-') Forbidden unassigned code points in input Input already contain ACE prefix (`xn--') Input does not start with ACE prefix (`xn--') Internationalized Domain Name (IDN) convert STRINGS, or standard input.

 Invalid input Malformed bidirectional string Missing input No top-level domain found in input Non-digit/letter/hyphen in input Output would be too large or too small Output would exceed the buffer space provided Prohibited bidirectional code points in input Prohibited code points in input Punycode failed String not idempotent under ToASCII String not idempotent under Unicode NFKC normalization String preparation failed String size limit exceeded Success System dlopen failed System iconv failed Try `%s --help' for more information.
 Type each input string on a line by itself, terminated by a newline character.
 Unicode normalization failed (internal error) Unknown error Unknown profile Usage: %s [OPTION]... [STRINGS]...
 could not convert from %s to UTF-8 could not convert from UCS-4 to UTF-8 could not convert from UTF-8 to %s could not convert from UTF-8 to UCS-4 could not do NFKC normalization idna_to_ascii_4z: %s idna_to_unicode_8z4z (TLD): %s idna_to_unicode_8z4z: %s input error malloc only one of -s, -e, -d, -a, -u or -n can be specified punycode_decode: %s punycode_encode: %s stringprep_profile: %s tld_check_4z (position %lu): %s tld_check_4z: %s Project-Id-Version: libidn
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-08-01 15:20+0200
PO-Revision-Date: 2007-04-09 03:22+0000
Last-Translator: Mykas0 <Mykas0@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:00+0000
X-Generator: Launchpad (build 18115)
       --allow-unassigned   Alterna IDNA AllowUnassigned flag (default off)
      --usestd3asciirules  Alterna IDNA UseSTD3ASCIIRules flag (default off)
       --debug              Imprime informação para debugging
      --quiet              Operação silenciosa
       --no-tld             Não verifica por sequência para regras específicas do TLD
                             Apenas para --idna-to-ascii e --idna-to-unicode
   -h, --help               Imprime a ajuda e sai
  -V, --version            Imprime a versão e sai
   -n, --nfkc               Normaliza a string segundo o Unicode v3.2 NFKC
   -p, --profile=STRING     Utilize preferencialmente o perfil stringprep
                             Perfis válidos stringprep: `Nameprep',
                             `iSCSI', `Nodeprep', `Resourceprep', 
                             `trace', `SASLprep'
   -s, --stringprep         Prepara a string segundo o perfil nameprep
  -d, --punycode-decode    Descodifica Punycode
  -e, --punycode-encode    Codifica Punycode
  -a, --idna-to-ascii      Converte para ACE segundo IDNA (modo por defeito)
  -u, --idna-to-unicode    Converte de ACE segundo IDNA
 Não pode alocar memória Charset `%s'.
 Pontos de código proibidos por domínio de nível superior Interface de linha de comandos para o nome da biblioteca de domínio internacional.

Todas as sequências estão esperadas para serem codificadas no conjunto de caracteres preferidos 
pelo seu local Utilizse `--debug' para saber qual o seu conjunto de caracteres.  Pode
substituir o conjunto de caracteres utilizado pela configuração da variável ambiente CHARSET.

Para processar uma sequência que começa com  `-', por exemplo `-foo', utilize `--'
para sinalizar o fim dos parâmetros, come em `idn --quiet -a -- -foo'.

Argumentos obrigatórios para as opções longas são obrigatórios para as opções curtas também.
 Propriedades bidirecionais  em conflito na entrada Erro na definição de perfil em stringprep Conflito da flag com o perfil Sinal de negativo no início ou final não permitido (`-') Códigos de ponto não atribuídos na entrada são proibidos Entrada já contem o prefixo ACE (`xn--') Entrada não começa com o prefixo ACE (`xn--') Nome de domínio internacionalizado (IDN) converter STRINGS, ou entrada de estandardização.

 Entrada inválida String bidireccional mal formada Entrada perdida Nenhum domínio de nível superior na entrada Não numérico/letra/hífen na entrada Saída seria demasido grande ou demasiado pequena O resultado iria exceder a área alocada no buffer Pontos de código bidirecionais proibidos na entrada Pontos de código na entrada Punycode falhou String não idempotente sob ToASCII String não idempotente sob a normalização Unicode NFKC A preparação da string falhou Limite do tamanho da string excedido Sucesso Sistema dlopen falhou Sistema iconv falhou Tente '%s --help' para mais informações.
 Introduza cada string de entrada numa linha individual, terminando com o símbolo de nova linha.
 Normalização unicode falou (erro interno) Erro desconhecido Perfil desconhecido Utilização: %s [OPÇÃO]... [STRINGS]...
 não pôde converter de %s para UTF-8 não pôde converter de UCS-4 para UTF-8 não pôde converter de UTF-8 para %s não pôde converter de UTF-8 para UCS-4 não pôde fazer a normalziação NFKC idna_to_ascii_4z: %s idna_to_unicode_8z4z (TLD): %s idna_to_unicode_8z4z: %s erro de entrada malloc apenas um de-s, -e, -d, -a, -u or -n pode ser especificado punycode_decode: %s punycode_encode: %s stringprep_profile: %s tld_check_4z (posição %lu): %s tld_check_4z: %s 