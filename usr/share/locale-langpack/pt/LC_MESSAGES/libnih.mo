��    )      d  ;   �      �     �     �     �     �     �     �          )     C     X     w  
   �     �  #   �     �  &   �  <     	   N     X     i     y     �     �     �  �   �     u  &   �     �     �     �     �               4     :     F     _  1   z  #   �     �  �  �     �	     �	     �	     �	     �	     �	     
     .
     J
     `
  $   �
  
   �
     �
     �
     �
  ,   �
  E   %  
   k     v     �     �     �  %   �  &   �  �     +   �  +        8  #   W     {     �     �      �     �     �     �       =   )  '   g  "   �                             (          $              &                                                     #          
      %          !      '              )              "      	                           %s [OPTION]... %s commands:
 %s options:
 %s: illegal argument: %s
 %s: invalid command: %s
 %s: invalid option: -%c
 %s: invalid option: --%s
 %s: missing argument: %s
 %s: missing command
 %s: unexpected argument: --%s
 COMMAND [OPTION]... [ARG]... Commands:
 Directory loop detected Error while reading from descriptor Expected token For a list of commands, try `%s help'. For more information on a command, try `%s COMMAND --help'.
 Options:
 Other commands:
 Other options:
 Report bugs at <%s>
 Report bugs to <%s>
 The %s property is read-only The %s property is write-only This is free software; see the source for copying conditions.  There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Trailing slash in file Try `%s --help' for more information.
 Unable to watch directory Unable to write pid file Unexpected token Unknown stanza Unterminated block Unterminated quoted string Usage [OPTION]... display list of commands display this help and exit increase output to include informational messages output version information and exit reduce output to errors only Project-Id-Version: libnih
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2010-12-23 21:53+0000
PO-Revision-Date: 2016-03-15 12:57+0000
Last-Translator: Ivo Xavier <ivoxavier.8@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:49+0000
X-Generator: Launchpad (build 18115)
 %s [OPÇÃO]... %s comandos:
 %s opções:
 %s: argument inválido: %s
 %s: comando inválido: %s
 %s: opção inválida: -%c
 %s: opção inválida: --%s
 %s: argumento em falta: %s
 %s: comando em falta
 %s: argumento inesperado: --%s
 COMANDO [OPÇÃO]... [ARGUMENTOS]... Comandos:
 Detetado ciclo no diretório Erro ao ler do descritor Símbolo esperado Para uma lista de comandos, tente `%s help'. Para mais informações sobre um comando, tente `%s COMMAND --help'.
 Opções:
 Outros comandos:
 Outras opções:
 Reportar erros em <%s>
 Reportar erros para <%s>
 A propriedade %s é apenas de leitura A propriedadde %s é apenas de escrita Este software é livre; queira consultar o código-fonte para conhecer as condições de cópia. Não é dada qualquer garantia; nem mesmo de "MERCHANTABILITY" ou "FITNESS FOR A PARTICULAR PURPOSE". Foi encontrada uma barra no fim do ficheiro Tente `%s --help' para mais informações.
 Incapaz de observar diretório Impossível escrever o ficheiro pid Símbolo inesperado Estrofe desconhecida Bloqueio indeterminado Expressão citada não terminada Utilização [OPÇÃO]... mostra lista de comandos mostrar esta ajuda e sair aumentar a saída para também incluir mensagens informativas mostrar informações da versão e sair reduzir a saída apenas para erros 