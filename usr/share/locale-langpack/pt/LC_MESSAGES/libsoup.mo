��    %      D  5   l      @  3   A  %   u     �  #   �  "   �  "     '   %     M     j     �  '   �     �     �     �          4     J     [  "   p     �     �     �     �     �     �  "     #   2  &   V  %   }  "   �  2   �  #   �       !   9     [     y  �  �  8   �	  0   �	  %    
  &   &
  "   M
  '   p
  +   �
  "   �
  $   �
       .   ,  *   [  #   �  #   �     �     �     �       '   6     ^     v     �     �     �  (   �  (     )   *  -   T  ,   �  ,   �  .   �  +     "   7  %   Z      �     �                   "            #                  $                          !                                                      %                                        
   	       Can't create a TLS server without a TLS certificate Can't import non-socket as SoupSocket Can't import unconnected socket Cannot truncate SoupBodyInputStream Connection terminated unexpectedly Could not import existing socket:  Could not listen on address %s, port %d Could not parse HTTP request Could not parse HTTP response Could not parse URI '%s' Failed to completely cache the resource Hostname has no base domain Hostname is an IP address Incorrect WebSocket "%s" header Invalid '%s' URI: %s Invalid WebSocket key Invalid hostname Invalid seek request Network stream unexpectedly closed No URI provided Not an HTTP URI Not enough domains Operation was cancelled Operation would block Output buffer is too small Server ignored WebSocket handshake Server rejected WebSocket handshake Server requested unsupported extension Server requested unsupported protocol Server returned incorrect "%s" key The server did not accept the WebSocket handshake. Unrecognized HTTP response encoding Unsupported URI scheme '%s' Unsupported WebSocket subprotocol Unsupported WebSocket version WebSocket handshake expected Project-Id-Version: 3.8
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libsoup&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-04-22 14:57+0000
PO-Revision-Date: 2016-04-28 07:08+0000
Last-Translator: Pedro Albuquerque <Unknown>
Language-Team: Português <palbuquerque73@openmailbox.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:32+0000
X-Generator: Launchpad (build 18115)
Language: pt
 Impossível criar um servidor TLS sem um certificado TLS Impossível importar não socket como SoupSocket Impossível importar socket desligado Incapaz de truncar SoupBodyInputStream Ligação terminou inesperadamente Impossível importar socket existente:  Impossível ouvir no endereço %s, porta %d Incapaz de processar o pedido HTTP Incapaz de processar a resposta HTTP Incapaz de processar o URI '%s' Falha ao colocar o recurso totalmente em cache Nome de máquina não possui domínio base Nome da máquina é um endereço IP Cabeçalho WebSocket "%s" incorreto URI '%s' inválido: %s Chave WebSocket inválida Nome de máquina inválido Pedido de procura inválido Fluxo de rede terminado inesperadamente Nenhum URI especificado Não é um URI HTTP Domínios insuficientes A operação foi cancelada A operação iria bloquear Buffer de resultado é demasiado pequeno O servidor ignorou o handshake WebSocket O servidor rejeitou o handshake WebSocket O servidor pediu uma extensão não suportada O servidor pediu um protocolo não suportado O servidor devolveu uma chave "%s" incorreta O servidor não aceitou o handshake WebSocket. Codificação de resposta HTTP desconhecida Esquema de URI '%s' não suportado Subprotocolo WebSocket não suportado Versão WebSocket não suportada Esperado handshake WebSocket 