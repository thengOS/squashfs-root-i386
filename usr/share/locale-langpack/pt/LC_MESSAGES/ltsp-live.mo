��          �   %   �      P  &   Q     x     �     �  (   �  (   �                0  %   H     n     ~     �     �     �     �     �     �     �  &   
     1  R   8  #   �     �  K   �  �    )   �             "   )  )   L  (   v     �  "   �     �  7   �       	   1  	   ;     E     b     ~     �     �     �  $   �     �  e   	  .   l	  ,   �	  T   �	                                                           
              	                                                    Adding LTSP network to Network Manager Configuring DNSmasq Configuring LTSP Creating the guest users Enabling LTSP network in Network Manager Extracting thin client kernel and initrd Failed Installing the required packages LTSP-Live configuration LTSP-Live should now be ready to use! Network devices None Ready Restarting Network Manager Restarting openbsd-inetd Start LTSP-Live Starting DNSmasq Starting NBD server Starting OpenSSH server Starts an LTSP server from the live CD Status The selected network interface is already in use.
Are you sure you want to use it? Unable to configure Network Manager Unable to parse config Welcome to LTSP Live.
Please choose a network interface below and click OK. Project-Id-Version: edubuntu-netboot
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2012-07-18 13:50-0400
PO-Revision-Date: 2014-04-24 08:27+0000
Last-Translator: Natan de Oliveira Pereira da Silva <natan673@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:19+0000
X-Generator: Launchpad (build 18115)
 A adicionar a rede LTSP ao gestor de rede A configurar o DNSmasq A configurar LTSP A criar os utilizadores convidados A habilitar a rede LTSP no gestor de rede Extraindo kernel do thin client e initrd Falha A instalar os pacotes necessários Configuração do LTSP-Live O LTSP-Live agora estará preparado para ser utilizado! Dispositivos de rede Nenhum(a) Preparado A reiniciar o gestor de rede A reiniciar o openbsd-inetd Iniciar LTSP-Live A iniciar o DNSmasq A iniciar o servidor NBD A iniciar o servidor OpenSSH Inicia um servidor LTSP pelo live CD Estado A interface de rede seleccionada já se encontra em utilização.
Tem a certeza que quer utilizá-la? Não foi possível configurar o gestor de rede Não foi possível analizar a configuração Bem-vindo ao LTSP Live.
Por favor, escolha abaixo uma interface de rede e clique OK. 