��    4      �  G   \      x     y     �  	   �  "   �     �     �     �           	  %        @     I     X     h     �     �     �  (   �     �  &     *   6     a  #   v  #   �     �     �  1   �          *  #   A  6   e     �  $   �     �     �  '        -  -   0  6   ^     �  (   �  1   �  )   	     :	     =	  ,   J	  "   w	  "   �	  )   �	     �	     
  �  
     �     �  	   �  "   �     �               6     C  %   U     {     �     �     �     �     �     �  .        I  ,   e  1   �     �  )   �  0        <     Y  4   s     �     �  )   �  9   �     3  '   S  
   {  $   �  '   �     �  9   �  >     '   Q  /   y  8   �  *   �            8      &   Y     �  (   �  '   �     �     0   	   
                         &             )               !                                       '   "   4      -   3       $       %      *                 2   /         .         +   #              ,                             1          (    
# Directories
 
# Files   Date %s   uid = %d, gid = %d, mode = 0%o.
  (built-in):  (from '%s', line %lu):
  (name might be truncated)  files,   impossibilities  impossibilities in %lu directories.
  so far. #  Also makes: #  Builtin rule #  Failed to be updated. #  File does not exist. #  File has been updated. #  File has not been updated. #  File is an intermediate prerequisite. #  File is very old. #  Implicit rule search has been done. #  Implicit rule search has not been done. #  Last modified %s
 #  Modification time never checked. #  Needs to be updated (-q is set). #  Successfully updated. #  recipe to execute *** Archive member '%s' may be bogus; not deleted *** Break.
 *** Deleting file '%s' *** Deleting intermediate file '%s' *** [%s] Archive member '%s' may be bogus; not deleted *** [%s] Deleting file '%s' Cleaning up temporary batch file %s
 Current time Invalid file operation: %s Member '%s'%s: %ld bytes at %ld (%ld).
 No Recipe was specified for file '%s' at %s:%lu, Recursive variable '%s' references itself (eventually) Removing intermediate files...
 attempt to use unsupported feature: '%s' but '%s' is now considered the same file as '%s'. lbr$ini_control() failed with status = %d no open: %s: %s touch archive member is not available on VMS touch: '%s' is not a valid archive touch: Archive '%s' does not exist touch: Member '%s' does not exist in '%s' unterminated variable reference write: %s: %s Project-Id-Version: make
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-10-05 12:25-0400
PO-Revision-Date: 2007-03-22 12:16+0000
Last-Translator: Marco Rodrigues <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:58+0000
X-Generator: Launchpad (build 18115)
 
# Arquivos
 
# Ficheiros   Data %s   uid = %d, gid = %d, mode = 0%o.
  (embutido):  (de '%s', linha %lu):
  (nome pode estar cortado)  ficheiros,   impossibilidades  impossibilidade em diretórios %lu.
  até agora. #  Também faz: # regra embutida # Falhou a ser atualizado #  Ficheiro não existente. #  Ficheiro foi atualizado. # Ficheiro não foi atualizado # Ficheiro é um pré-requisito intermediário #  Ficheiro é muito velho. # Regra implícita de pequisa foi realizada. # Regra implícita de pequisa não foi realizada. # Última vez modificado %s
 # Modificação da hora nunca verificada. # Necessita de ser atulizado (-q está definida) # Atualização bem sucedida #  fórmula para executar *** Arquivo membro '%s' pode ser falso; não apagado *** Break.
 *** A apagar ficheiro '%s' *** A apagar ficheiro intermediário '%s' *** [%s] Arquivo membro '%s' pode ser falso; não apagado *** [%s] A apagar ficheiro '%s' A limpar ficheiro temporário batch %s
 Hora atual Operação de ficheiro inválida: %s Membro '%s'%s: %ld bytes at %ld (%ld).
 Não Fórmula foi especificada para o ficheiro '%s' em %s:%lu, Variável recursiva '%s' refere-se a ela mesma (eventualmente) Removendo ficheiros intermediários...
 tentar usar funcionalidade não suportada: '%s' mas '%s' é agora considerado o mesmo ficheiro que '%s'. lbr$ini_control() falhou com o status = %d não abrir: %s: %s touch arquivo membro não se encontra disponível em VMS touch: '%s' não é um arquivo válido touch: Arquivo "%s" não existe touch: O membro '%s' não existe em '%s' variável de referência indetermindada escrever: %s: %s 