��    	      d      �       �      �   )   �   2        F  ,   T  #   �  "   �  0   �  �  �     �  6   �  C   �     *  ;   =  '   y  :   �  9   �                                          	    Archive Could not parse command-line options: %s
 Expects URIs or filenames to be passed as options
 Files to send No mail client installed, not sending files
 Output version information and exit Run from build directory (ignored) Use XID as parent to the send dialogue (ignored) Project-Id-Version: 3.8
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-03 23:48+0000
PO-Revision-Date: 2016-05-31 13:22+0000
Last-Translator: António Lima <amrlima@gmail.com>
Language-Team: gnome_pt@yahoogroups.com
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:34+0000
X-Generator: Launchpad (build 18115)
X-Project-Style: gnome
Language: pt
 Arquivo Incapaz de processar opções de linha de comando: %s
 Espera que URIs ou nomes de ficheiros sejam passados como opções
 Ficheiros a enviar Nenhum cliente de email instalado, ficheiros não enviados
 Retornar informação de versão e sair Executar a partir de diretório de compilação (ignorado) Utilizar XID como pai para o diálogo de envio (ignorado) 