��          �   %   �      p     q     �  +   �  @   �  5        M  ,   _     �     �     �  $   �  	   �  2   �  $   2     W     Z     l  #   �  !   �     �  %   �               8     <     E     Y  �  h           "  4   C  I   x  7   �     �  F   
  "   Q     t     �  &   �     �  :   �  5   	     M	     R	  -   p	  %   �	  '   �	     �	  9   
  &   >
     e
     �
  	   �
     �
     �
                    
                                                                                          	             <b>%s</b>
Hardware present: %s <b>%s</b>
Invalid Driver! <b>Currently Installed Windows Drivers:</b> <span size="larger" weight="bold">Select <i>inf</i> file:</span> Are you sure you want to remove the <b>%s</b> driver? Configure Network Could not find a network configuration tool. Driver is already installed. Error while installing. Install Driver Is the ndiswrapper module installed? Location: Module could not be loaded. Error was:

<i>%s</i>
 Ndiswrapper driver installation tool No No file selected. Not a valid driver .inf file. Please drag an '.inf' file instead. Root or sudo privileges required! Select inf File Unable to see if hardware is present. Windows Wireless Drivers Wireless Network Drivers Yes _Install _Install New Driver _Remove Driver Project-Id-Version: ndisgtk
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-10-02 17:50+0200
PO-Revision-Date: 2009-08-29 01:20+0000
Last-Translator: Pedro Flores <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:44+0000
X-Generator: Launchpad (build 18115)
 <b>%s</b>
Hardware presente: %s <b>%s</b>
Controlador Inválido! <b>Controladores Windows actualmente instalados:</b> <span size="maior" weight="negrito">Seleccione ficheiro<i>inf</i>:</span> Tem a certeza que quer remover o controlador <b>%s</b>? Configurar Rede Não foi possível encontrar uma ferramenta de configuração de rede. O controlador já está instalado. Ocorreu um erro ao instalar. Instalar controlador O módulo ndiswrapper está instalado? Localização: O módulo não pôde ser carregado. O erro foi:
<i>%s</i>
 Ferramenta de instalação de controlador Ndiswrapper Não Nenhum ficheiro seleccionado. Não é um ficheiro controlador .inf válido. Por favor arraste um ficheiro '.inf'. Privilégios Root ou sudo necessários! Seleccione ficheiro inf Não é possível verificar se o hardware está presente. Controladores de Rede sem Fios Windows Controladores de Rede sem Fios Sim _Instalar _Instalar Novo Controlador _Remover Controlador 