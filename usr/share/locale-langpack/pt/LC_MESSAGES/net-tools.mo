��    B     ,
  �  <      �  9   �  J   3  +   ~  6   �  G   �  B   )  :   l  :   �  :   �  9     G   W  A   �  O   �  B   1  8   t  P   �  J   �  5   I  :     @   �  Q   �  E   M   G   �   9   �   B   !  5   X!  0   �!  F   �!  7   "  5   >"  K   t"  ,   �"  $   �"  (   #  $   ;#  $   `#  L   �#  B   �#  ?   $  D   U$      �$  *   �$  4   �$  N   %  !   j%  G   �%  F   �%  K   &  A   g&  K   �&  O   �&  U   E'  K   �'  *   �'  0   (  5   C(  +   y(  *   �(  ,   �(  @   �(  P  >)  #   �*  9   �*  .   �*     +  C   3+  N   w+  +   �+     �+      ,  =   /,  %   m,  7   �,  #   �,     �,  ;   -     J-  )   Z-      �-      �-  1   �-  ?   �-     8.  ?   U.  "   �.     �.     �.  P   �.  Q   0/  P   �/  P   �/     $0     +0     20     80     O0     k0     �0     �0  ,   �0  ,   �0  ;   1  "   B1     e1     1  !   �1     �1     �1     �1  /   �1  -   +2  -   Y2     �2     �2     �2  1   �2  7   3     93     Q3     e3     �3     �3  *   �3  %   �3  =   4  1   C4     u4     �4     �4  #   �4     �4     5     5     05     =5  "   R5     u5     �5     �5     �5     �5     �5     6     86     T6     q6      �6  7   �6  $   �6     7     "7     67  $   F7     k7     �7     �7     �7     �7     �7     �7     	8     8     88     R8     j8  G   �8  }   �8     I9     V9     e9     9     �9     �9     �9     �9     �9     �9     :  Q   (:  /   z:     �:  ,   �:     �:  
   �:     �:  	   �:  	   ;  
   ;     ;  	   #;     -;     ;;  '   M;  '   u;  /   �;     �;     �;  "   �;  	   <  	   <     <     <     7<     H<     ^<     u<  O   �<  N   �<     ,=     J=  (   b=     �=     �=  	   �=     �=  &   �=  $   �=  %   >     ,>     J>     N>     R>     [>  7   o>     �>     �>     �>     �>  	   ?  	   ?     ?     ?     &?     /?     K?     e?  	   ?  #   �?     �?     �?     �?  W   �?  @   '@  F   h@  7   �@  +   �@  D   A  H   XA  *   �A  .   �A  /   �A  -   +B  /   YB  1   �B  S   �B     C  &   &C     MC     fC     C  ,   �C  &   �C  !   �C      D     0D      LD  3   mD  '   �D  )   �D  /   �D  #   #E     GE     fE     �E     �E  0   �E     �E  @   F     CF     TF     eF     wF     �F     �F  <   �F  7   �F  ,   *G  &   WG     ~G     �G     �G  )   �G     �G     H     H     H     -H     AH  !   _H     �H     �H  "   �H     �H     �H     I     I     %I     9I     KI     bI  )   zI     �I     �I  N   �I  !   J  "   4J     WJ     uJ     �J     �J     �J  �  �J  =   �L  \   �L  +   M  8   GM  E   �M  B   �M  A   	N  >   KN  D   �N  >   �N  Q   O  R   `O  \   �O  B   P  @   SP  E   �P  M   �P  4   (Q  2   ]Q  @   �Q  N   �Q  F    R  A   gR  ?   �R  F   �R  -   0S  7   ^S  O   �S  C   �S  2   *T  K   ]T  +   �T  %   �T  *   �T  $   &U  $   KU  k   pU  B   �U  0   V  E   PV      �V  *   �V  4   �V  f   W  !   ~W  M   �W  I   �W  Y   8X  P   �X  ^   �X  L   BY  g   �Y  R   �Y  &   JZ  >   qZ  :   �Z  "   �Z  )   [  ,   8[  @   e[  `  �[  0   ]  :   8]  3   s]     �]  \   �]  N   ^  8   m^  *   �^  (   �^  M   �^  /   H_  =   x_  )   �_  #   �_  ?   `     D`  '   T`  $   |`  $   �`  1   �`  ?   �`     8a  E   Ua  "   �a     �a     �a  @   �a  Q   &b  N   xb  V   �b     c     %c     3c     ?c     Zc     vc     �c     �c  7   �c  7   �c  <   .d  +   kd  %   �d  !   �d  %   �d      e     &e     Ce  @   ^e  @   �e  ;   �e     f     7f     Sf  9   nf  6   �f     �f     �f     g     .g  #   Lg  /   pg  /   �g  M   �g  6   h  ,   Uh     �h     �h  ,   �h  #   �h     
i     &i     Fi     Ti  4   ji     �i     �i     �i      �i  &   j  #   9j      ]j  #   ~j     �j     �j  #   �j  H   k  (   Kk     tk     �k     �k  .   �k     �k     l     %l     9l     Xl     tl     �l     �l     �l     �l     �l     m  ^   0m  �   �m     An     Nn     _n     |n     �n     �n     �n     �n     �n  !   �n     o  9   2o  ;   lo     �o  1   �o     �o  
   �o     p     
p     p     p     %p     +p  
   7p     Bp  ,   [p  ,   �p  3   �p     �p     �p  +   �p  	   (q  	   2q     <q     Bq     aq     qq     �q     �q  O   �q  N   r     ^r     ~r  ,   �r     �r     �r  	   �r     �r  "   �r  '   !s  *   Is     ts     �s     �s  
   �s     �s  F   �s     t     t     3t     Ot     gt  	   tt     ~t  	   �t     �t  *   �t  *   �t  %   �t  	   u     u     >u     Cu     Pu  Z   nu  I   �u  p   v  7   �v  2   �v  P   �v  _   @w  (   �w  4   �w  5   �w  5   4x  5   jx  7   �x  b   �x     ;y  /   Uy  &   �y  #   �y  $   �y  ,   �y  &   "z  .   Iz  (   xz     �z  .   �z  E   �z  D   5{  =   z{  3   �{  6   �{  %   #|  )   I|  #   s|  &   �|  B   �|     }  @   }     [}     m}     }     �}     �}     �}  R   �}  V   #~  0   z~  )   �~     �~  &   �~       3   ,     `          �     �     �     �  )   �  "   �  1   7�  ?   i�     ��     ɀ     ߀     ��     �      �     3�     P�  *   o�     ��     ��  U   ��  )   �  -   ?�     m�     ��     ��     ǂ     �         �   7  �   �   J   �   �       �   �   �   |   �   �   �   T   l   5          �   �   �   (   �   �   
   �     �         $     @  u       �       �       �   �                �      �     v                  �   1  i   K                �   7   �   d   �   �                            -   �   (      �      �               �   >  e   {   	   �   H       �   �   )   #  '  #           G           �   �                  �   Y   V   
  �   �       �   :      :   �   !      =       2  +            �   �   w   8           �   �   �         C   �   �   B  �          P       �   �                �       �   �   E   1   ;  �       �           �   �   �   �   �           �   �   3  q       *      �       $  ~           �       �   &   %   �   �       9     �   �   �       D       /  z           =  6      n   m   �   R   �       �   @      \   S   �       0          k         Z         �   a   ,   !          �   `   �       �   c   ]   �               �   -          X              <  <   4   ?   �   �   F      �   �        U   �   �   s         �   5  �     �   r   �   f   &        �      �   A       �   �     �   *   >   �       L   O   h   }   �       ,  �           4  �   �   �   [      �   �   2       '       �   B          �                  I         b   )          8  ?      ^   t   +      �   g           �   o       	            3   9   6      �   N   �       �       Q   M   �   A  �        "              .    _                     �   p          �      .   �   /   �       �   �   �   %             �       j   �   y   �      ;   0  �       "       �   �          W   x    
Proto RefCnt Flags       Type       State         I-Node 
Unless you are using bind or NIS for host lookups you can change the DNS
                   [nibble NN] [trigger NN]
           [ [i|o]seq ] [ [i|o]key KEY ] [ [i|o]csum ]
           [ mode { ipip | gre | sit } ] [ remote ADDR ] [ local ADDR ]
           [ ttl TTL ] [ tos TOS ] [ nopmtudisc ] [ dev PHYS_DEV ]
         --numeric-hosts          don't resolve host names
         --numeric-ports          don't resolve port names
         --numeric-users          don't resolve user names
         -A, -p, --protocol       specify protocol family
         -C, --cache              display routing cache instead of FIB

         -D, --use-device         read <hwaddr> from given device
         -F, --fib                display Forwarding Information Base (default)
         -M, --masquerade         display masqueraded connections

         -N, --symbolic           resolve hardware names
         -a                       display (all) hosts in alternative (BSD) style
         -a, --all, --listening   display all sockets (default: connected)
         -c, --continuous         continuous listing

         -d, --delete             delete a specified entry
         -e, --extend             display other/more information
         -f, --file               read new entries from file or from /etc/ethers

         -g, --groups             display multicast group memberships
         -i, --device             specify network interface (e.g. eth0)
         -i, --interfaces         display interface table
         -l, --listening          display listening server sockets
         -n, --numeric            don't resolve names
         -o, --timers             display timers
         -p, --programs           display PID/Program name for sockets
         -r, --route              display routing table
         -s, --set                set a new ARP entry
         -s, --statistics         display networking statistics (like SNMP)
         -v, --verbose            be verbose
        ADDR := { IP_ADDRESS | any }
        KEY  := { DOTTED_QUAD | NUMBER }
        TOS  := { NUMBER | inherit }
        TTL  := { 1..255 | inherit }
        domainname [-v] {nisdomain|-F file}   set NIS domainname (from file)
        hostname -V|--version|-h|--help       print info and exit

        hostname [-v]                         display hostname

        hostname [-v] [-d|-f|-s|-a|-i|-y|-n]  display formatted name
        iptunnel -V | --version

        netstat [-vnNcaeol] [<Socket> ...]
        netstat { [-veenNac] -i | [-cnNe] -M | -s }

        nodename [-v] {nodename|-F file}      set DECnet node name (from file)
        plipconfig -V | --version
        rarp -V                               display program version.

        rarp -d <hostname>                    delete entry from cache.
        rarp -f                               add entries from /etc/ethers.
        rarp [<HW>] -s <hostname> <hwaddr>    add entry to cache.
        route [-v] [-FC] {add|del|flush} ...  Modify routing table for AF.

        route {-V|--version}                  Display version/author and exit.

        route {-h|--help} [<AF>]              Detailed usage syntax for specified AF.
     -F, --file            read hostname or NIS domainname from given file

     -d, --domain          DNS domain name
     -f, --fqdn, --long    long host name (FQDN)
     -i, --ip-address      addresses for the hostname
     -n, --node            DECnet node name
     -s, --short           short host name
     -y, --yp, --nis       NIS/YP domainname
     dnsdomainname=hostname -d, {yp,nis,}domainname=hostname -y

    This command can read or set the hostname or the NIS domainname. You can
   also read the DNS domain or the FQDN (fully qualified domain name).
   Unless you are using bind or NIS for host lookups you can change the
   FQDN (Fully Qualified Domain Name) and the DNS domain name (which is
   part of the FQDN) in the /etc/hosts file.
   <AF>=Address family. Default: %s
   <AF>=Use '-6|-4' or '-A <af>' or '--<af>'; default: %s
   <AF>=Use '-A <af>' or '--<af>'; default: %s
   <HW>=Hardware Type.
   <HW>=Use '-H <hw>' to specify hardware address type. Default: %s
   <Socket>={-t|--tcp} {-u|--udp} {-w|--raw} {-x|--unix} --ax25 --ipx --netrom
   Checksum in received packet is required.
   Checksum output packets.
   Drop packets out of sequence.
   List of possible address families (which support routing):
   List of possible address families:
   List of possible hardware types (which support ARP):
   List of possible hardware types:
   Sequence packets on output.
   [[-]broadcast [<address>]]  [[-]pointopoint [<address>]]
   [[-]dynamic]
   [[-]trailers]  [[-]arp]  [[-]allmulti]
   [add <address>[/<prefixlen>]]
   [del <address>[/<prefixlen>]]
   [hw <HW> <address>]  [metric <NN>]  [mtu <NN>]
   [mem_start <NN>]  [io_addr <NN>]  [irq <NN>]  [media <type>]
   [multicast]  [[-]promisc]
   [netmask <address>]  [dstaddr <address>]  [tunnel <address>]
   [outfill <NN>] [keepalive <NN>]
   [txqueuelen <NN>]
   [up|down] ...

   arp [-v]          [-i <if>] -d  <host> [pub]               <-Delete ARP entry
   arp [-v]   [<HW>] [-i <if>] -Ds <host> <if> [netmask <nm>] pub          <-''-

   arp [-v]   [<HW>] [-i <if>] -s  <host> <hwaddr> [temp]            <-Add entry
   arp [-vnD] [<HW>] [-i <if>] -f  [<filename>]            <-Add entry from file
  Path
  Timer  User  User       Inode      %s	nibble %lu  trigger %lu
 %s (%s) -- no entry
 %s (%s) at  %s: %s/ip  remote %s  local %s  %s: ERROR while getting interface flags: %s
 %s: ERROR while testing interface flags: %s
 %s: You can't change the DNS domain name with this command
 %s: address family not supported!
 %s: bad hardware address
 %s: can't open `%s'
 %s: hardware type not supported!
 %s: illegal option mix.
 %s: invalid %s address.
 %s: name too long
 %s: you must be root to change the domain name
 %s: you must be root to change the host name
 %s: you must be root to change the node name
 %u ICMP messages failed %u ICMP messages received %u ICMP messages sent %u ICMP packets dropped because socket was locked %u ICMP packets dropped because they were out-of-window %u SYN cookies received %u SYN cookies sent %u active connections openings %u bad SACKs received %u bad segments received. %u congestion window recovered using DSACK %u congestion windows fully recovered %u congestion windows partially recovered using Hoe heuristic %u congestion windows recovered after partial ack %u connection resets received %u connections established %u delayed acks sent %u dropped because of missing route %u failed connection attempts %u fast retransmits %u forward retransmits %u forwarded %u fragments created %u fragments dropped after timeout %u fragments failed %u fragments received ok %u incoming packets delivered %u incoming packets discarded %u input ICMP message failed. %u invalid SYN cookies received %u outgoing packets dropped %u packet headers predicted %u packet reassembles failed %u packet receive errors %u packets dropped from prequeue %u packets header predicted and directly queued to user %u packets pruned from receive queue %u packets reassembled ok %u packets received %u packets sent %u packets to unknown port received. %u passive connection openings %u reassemblies required %u requests sent out %u resets sent %u retransmits lost %u segments received %u segments retransmited %u segments send out %u total packets received %u with invalid addresses %u with invalid headers %u with unknown protocol (No info could be read for "-p": geteuid()=%d but you should be root.)
 (Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
 (incomplete) (only servers) (servers and established) (w/o servers) <from_interface> <incomplete>  Active AX.25 sockets
 Active Internet connections  Active NET/ROM sockets
 Active UNIX domain sockets  Active X.25 sockets
 Address                  HWtype  HWaddress           Flags Mask            Iface
 Address deletion not supported on this system.
 Bad address.
 Broadcast tunnel requires a source address.
 CLOSE CLOSE_WAIT CLOSING CONN SENT CONNECTED CONNECTING DGRAM DISC SENT DISCONNECTING Default TTL is %u Detected reordering %u times using FACK Detected reordering %u times using SACK Don't know how to set addresses for family %d.
 ESTAB ESTABLISHED Entries: %d	Skipped: %d	Found: %d
 FIN_WAIT1 FIN_WAIT2 FREE Failed to get type of [%s]
 Forwarding is %s ICMP input histogram: ICMP output histogram: IPv4 Group Memberships
 Iface   MTU Met   RX-OK RX-ERR RX-DRP RX-OVR    TX-OK TX-ERR TX-DRP TX-OVR Flg
 Iface   MTU Met   RX-OK RX-ERR RX-DRP RX-OVR   TX-OK TX-ERR TX-DRP TX-OVR Flg
 Interface %s not initialized
 Kernel Interface table
 Keys are not allowed with ipip and sit.
 LAST_ACK LISTEN LISTENING No ARP entry for %s
 No support for ECONET on this system.
 No support for INET on this system.
 No support for INET6 on this system.
 Problem reading data from %s
 RAW RDM RECOVERY RTO algorithm is %s Ran %u times out of system memory during packet sending Resolving `%s' ...
 Result: h_addr_list=`%s'
 Result: h_aliases=`%s'
 Result: h_name=`%s'
 SABM SENT SEQPACKET STREAM SYN_RECV SYN_SENT Setting domainname to `%s'
 Setting hostname to `%s'
 Setting nodename to `%s'
 TIME_WAIT This kernel does not support RARP.
 UNK. UNKNOWN Unknown media type.
 Usage:
  arp [-vn]  [<HW>] [-i <if>] [-a] [<hostname>]             <-Display ARP cache
 Usage:
  ifconfig [-a] [-v] [-s] <interface> [[<AF>] <address>]
 Usage: hostname [-v] {hostname|-F file}      set hostname (from file)
 Usage: iptunnel { add | change | del | show } [ NAME ]
 Usage: plipconfig [-a] [-i] [-v] interface
 Usage: rarp -a                               list entries in cache.
 Usage: route [-nNvee] [-FC] [<AF>]           List kernel routing tables
 WARNING: at least one error occured. (%d)
 Warning: Interface %s still in ALLMULTI mode.
 Warning: Interface %s still in BROADCAST mode.
 Warning: Interface %s still in DYNAMIC mode.
 Warning: Interface %s still in MULTICAST mode.
 Warning: Interface %s still in POINTOPOINT mode.
 Warning: Interface %s still in promisc mode... maybe other application is running?
 Where: NAME := STRING
 Wrong format of /proc/net/dev. Sorry.
 address mask replies: %u address mask request: %u address mask requests: %u arp: %s: hardware type without ARP support.
 arp: %s: kernel only supports 'inet'.
 arp: %s: unknown address family.
 arp: %s: unknown hardware type.
 arp: -N not yet supported.
 arp: cannot open etherfile %s !
 arp: cannot set entry on line %u of etherfile %s !
 arp: cant get HW-Address for `%s': %s.
 arp: device `%s' has HW address %s `%s'.
 arp: format error on line %u of etherfile %s !
 arp: in %d entries no match found.
 arp: invalid hardware address
 arp: need hardware address
 arp: need host name
 arp: protocol type mismatch.
 cannot determine tunnel mode (ipip, gre or sit)
 destination unreachable: %u domain name (which is part of the FQDN) in the /etc/hosts file.
 echo replies: %u echo request: %u echo requests: %u getdomainname()=`%s'
 gethostname()=`%s'
 getnodename()=`%s'
 hw address type `%s' has no handler to set address. failed.
 ifconfig: Cannot set address for this protocol family.
 ifconfig: `--help' gives usage information.
 ifconfig: option `%s' not recognised.
 keepalive (%2.2f/%ld/%d) missing interface information netmask %s  netstat: unsupported address family %d !
 no RARP entry for %s.
 off (0.00/%ld/%d) on %s
 on (%2.2f/%ld/%d) on%d (%2.2f/%ld/%d) problem reading data from %s
 rarp: %s: unknown hardware type.
 rarp: %s: unknown host
 rarp: cannot open file %s:%s.
 rarp: cannot set entry from %s:%u
 rarp: format error at %s:%u
 redirect: %u redirects: %u source quench: %u source quenches: %u time exceeded: %u timeout in transit: %u timewait (%2.2f/%ld/%d) ttl != 0 and noptmudisc are incompatible
 unkn-%d (%2.2f/%ld/%d) unknown usage: netstat [-veenNcCF] [<Af>] -r         netstat {-V|--version|-h|--help}
 warning, got bogus igmp line %d.
 warning, got bogus igmp6 line %d.
 warning, got bogus raw line.
 warning, got bogus tcp line.
 warning, got bogus udp line.
 warning, got bogus unix line.
 wrong parameters: %u Project-Id-Version: net-tools
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2007-06-30 12:28+0900
PO-Revision-Date: 2014-11-03 22:49+0000
Last-Translator: Luís Louro <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:13+0000
X-Generator: Launchpad (build 18115)
 
Proto RefCnt Opções       Tipo       Estado         I-Nodo 
A não ser que esteja a usar o bind ou NIS para resolução de endereços pode mudar o DNS
                   [nibble NN] [trigger NN]
           [ [i|o]seq ] [ [i|o]key CHAVE ] [ [i|o]csum ]
           [ mode { ipip | gre | sit } ] [ END remoto ] [ END local ]
           [ ttl TTL ] [ tos TOS ] [ nopmtudisc ] [ dev DISP_FIS ]
         --numeric-hosts          não resolve nomes de máquinas
         --numeric-ports          não resolve nomes de portos
         --numeric-users          não resolve nomes de utilizadores
         -A, -p, --protocol especifica a família de protocolo
         -C, --cache              mostra a cache de encaminhamento em vez da FIB

         -D, --use-device lê <endereço de hardware> a partir do dispositivo dado
         -F, --fib                mostra a Base de Informação de Encaminhamento (omissão)
         -M, --masquerade         mostra ligações com máscaras

         -N, --symbolic           não resolve nomes de hardware
         -a mostra (todos) os anfitriões no estilo alternativo (BSD)
         -a, --all, --listening   mostra todos os sockets (omissão: ligados)
         -c, --continuous         listagem continua

         -d, --delete apaga a entrada especificada
         -e, --extend             mostra outra/mais informação
         -f, --file lê novas entradas a partir do ficheiro ou de /etc/ethers

         -g, --groups             mostrar pertença a grupos multicast
         -i,  --device especifica a interface de rede (p.e. eth0)
         -i, --interfaces         mostra a tabela de interfaces
         -l, --listening          mostra sockets de servidor à escuta
         -n, --numeric não se resolvem nomes
         -o, --timers             mostra temporizadores
         -p, --programs           mostra o PID/Nome do programa para os sockets
         -r, --route              mostra a tabela de encaminhamento
         -s, --set estabelece uma nova entrada ARP
         -s, --statistics         mostrar estatísticas da rede (como SNMP)
         -v, --verbose descrição completa
        END := { ENDEREÇO_IP | any }
        CHAVE  := { DOTTED_QUAD | NUMERO }
        TOS  := { NUMERO | inherit }
        TTL  := { 1..255 | inherit }
        domainname [-v] {domínio nis|-F ficheiro} configurar o nome de domínio NIS (a partir do ficheiro)
        hostname -V|--version|-h|--help mostra informação e sai

        hostname [-v] mostra o nome da máquina

        hostname [-v] [-d|-f|-s|-a|-i|-y|-n] mostrar nome com formato
        iptunnel -V | --version

        netstat [-vnNcaeol] [<Socket> ...]
        netstat { [-veenNac] -i | [-cnNe] -M | -s }

        nodename [-v] {nome do nó|-F ficheiro} configurar o nome de nó DECnet (a partir do ficheiro)
        plipconfig -V | --version
        rarp -V                               mostrar a versão do programa.

        rarp -d <anfitrião>                    apaga a entrada da cache.
        rarp -f                               adicionar entradas a partir de /etc/ethers.
        rarp [<HW>] -s <nome-anfitrião> <end-hw>    adicionar entrada à cache.
        route [-v] [-FC] {add|del|flush} ...  Modificar a tabela de encaminhamento para o AF.

        route {-V|--version}                  Mostrar versão/autor e sair.

        route {-h|--help} [<AF>]              Sintaxe detalhada de utilização para o AF especificado.
     -F, --file lê o nome da máquina ou o nome de domínio NIS do ficheiro dado

     -d, --domain nome de domínio DNS
     -f, --fqdn, --long nome completo para o anfitrião (FQDN)
     -i, --ip-address endereços para o nome do anfitrião
     -n, --node nome de nó DECnet
     -s, --short nome curto do anfitrião
     -y, --yp, --nis nome de domínio NIS/YP
     dnsdomainname=hostname -d, {yp,nis,}domainname=hostname -y

    Este comando pode ler ou definir o hostname ou o nome de domínio NIS. Você pode
   também ler o domínio de DNS ou o FQDN (fully qualified domain name).
   A menos que esteja a usar o bind ou NIS para encontrar hosts pode mudar o
   FQND (Fully Qualified Domain Name) e o nome de domínio de DNS (que é
   parte do FQDN) no ficheiro /etc/hosts.
   <AF>=Família de endereços. Por omissão: %s
   <AF>=Use '-6|-4' ou '-A <af>' ou '--<af>'; omissão: %s
   <AF>=Use '-A <af>' ou '--<af>'; por omissão: %s
   <HW>=Tipo de Hardware.
   <HW>=Utilize '-H <hw>' para especificar o tipo de endereço de hardware. Por omissão: %s
   <Socket>={-t|--tcp} {-u|--udp} {-w|--raw} {-x|--unix} --ax25 --ipx --netrom
   É necessária a soma de controlo do pacote recebido.
   Soma de controlo dos pacotes de saída.
   Descartar pacotes fora de sequência.
   Lista de possíveis famílias de endereços (que suportam encaminhamento):
   Lista de famílias de endereços possíveis:
   Lista dos possíveis tipos de hardware (que suportam ARP):
   Lista de tipos possíveis de hardware:
   Sequência de pacotes na saída.
   [[-]broadcast [<endereço>]]  [[-]pointopoint [<endereço>]]
   [[-]dynamic]
   [[-]trailers] [[-]arp] [[-]allmulti]
   [add <endereço>[/<tam-prefixo>]]
   [del <endereço>[/<tam-prefixo>]]
   [hw <HW> <endereço>] [metric <NN>] [mtu <NN>]
   [mem_start <NN>]  [io_addr <NN>]  [irq <NN>]  [media <tipo>]
   [multicast]  [[-]promisc]
   [netmask <endereço>]  [dstaddr <endereço>]  [tunnel <endereço>]
   [outfill <NN>] [keepalive <NN>]
   [txqueuelen <NN>]
   [up|down] ...

   arp [-v] [-i <if>] -d <máquina> [pub] <-Apagar a entrada ARP
   arp [-v]   [<HW>] [-i <if>] -Ds <host> <if> [netmask <nm>] pub          <-''-

   arp [-v] [<HW>] [-i <if>] -s <máquina> <hwaddr> [temp] <-Adicionar entrada
   arp [-vnD] [<HW>] [-i <if>] -f [<nome do ficheiro>] <-Adicionar entrada do ficheiro
  Rota
  Temporizador  Utilizador  I-nodo de utilizador      %s	nibble %lu  trigger %lu
 %s (%s) -- no há entradas
 %s (%s) em  %s: %s/ip  remoto %s  local %s  %s: ERRO enquanto obtinha as opções de interface: %s
 %s: ERRO enquanto testava as opções de interface: %s
 %s: Não pode mudar o nome de domínio DNS com este comando
 %s: família de endereços não suportada!
 %s: endereço de hardware incorrecto
 %s: não é possível abrir `%s'
 %s: Tipo de hardware não suportado!
 %s: mistura ilegal de opções.
 %s: endereço %s inválido.
 %s: nome demasiado grande
 %s: tem que ser superutilizador para alterar o nome de domínio
 %s: tem que ser superutilizador para alterar o nome da máquina
 %s: tem que ser superutilizador para alterar o nome do nó
 %u mensagens ICMP falhadas %u mensagens ICMP recebidas %u mensagens ICMP enviadas %u pacotes ICMP descartados pois o socket estava trancado %u pacotes ICMP descartados por estares fora da janela %u cookies SYN recebidos %u cookies SYN enviados %u ligações activas abertas %u SACKs inválidos recebidos %u segmentos incorrectos recebidos. %u janela de congestão recuperada usando DSACK %u janelas de congestão totalmente recuperadas %u janelas de congestão parcialmente recuperadas usando a heurística de Hoe %u janelas de congestão recuperadas após ack parcial %u reinicializações de ligação recebidas %u ligações estabelecidas %u acks atrasados enviados %u descartados devido a uma rota inexistente %u tentativas de ligação falhadas %u retransmissões rápidas %u reencaminhar retransmissões %u reenviados %u fragmentos criados %u fragmentos descartados após esgotamento de tempo %u fragmentos falhados %u fragmentos bem recebidos %u pacotes recebidos entregues %u pacotes recebidos descartados %u mensagens ICMP de entrada falharam. %u cookies SYN inválidos recebidos %u pacotes de saída descartados %u cabeçalhos de pacotes previstos %u pacotes mal reassemblados %u pacotes recebidos com erros %u pacotes descartados da pré-fila %u cabeçalhos de pacotes previstos e directamente na fila do utilizador %u pacotes limpos  da fila de recepção %u pacotes bem reassemblados %u pacotes recebidos %u pacotes enviados %u pacotes recebidos para portos desconhecidos %u ligações passivas abertas %u reassemblagens requeridas %u pedidos enviados %u reinicializações enviadas %u retransmissões perdidas %u segmentos recebidos %u segmentos retransmitidos %u segmentos enviados %u total de pacotes recebidos %u com endereços inválidos %u cabeçalhos inválidos %u com protocolo desconhecido (Não foi possível ler informação para "-p": geteuid()=%d mas deveria ser superutilizador)
 (Nem todos os processos puderam ser identificados, não será mostrada informação
 sobre processos que não lhe pertencem. Tem que ser o superutilizador para poder ver tudo.)
 (incompleto) (só servidores) (servidores e estabelecidos) (sem servidores) <da_interface> <incompleto>  Sockets AX.25 activos
 Ligações de Internet Activas  Sockets NET/ROM activos
 Sockets de domínio UNIX activos  Sockets X.25 activos
 Endereço TipoHW EndereçoHW Opções Máscara Interface
 A remoção de endereços não é suportada neste sistema.
 Endereço incorrecto.
 O túnel de difusão requer um endereço origem,
 FECHAR CLOSE_WAIT FECHAR LIG ENVIADA LIGADO LIGANDO DGRAM DES ENVIADO DESLIGANDO O TTL por omissão é %u Detectaram-se %u reordenações ao usar FACK Detectaram-se %u reordenações ao usar SACK Não sei configurar endereços para a família %d.
 ESTAB ESTABELECIDO Entradas: %d	Ignoradas: %d	Encontradas: %d
 FIN_WAIT1 FIN_WAIT2 LIVRE Falha ao obter o tipo de [%s]
 O reenvio é %s histograma de entrada ICMP: Histograma de saída ICMP: Membros do Grupo IPV4
 Iface   MTU Met   RX-OK RX-ERR RX-DRP RX-OVR    TX-OK TX-ERR TX-DRP TX-OVR Flg
 Iface   MTU Met   RX-OK RX-ERR RX-DRP RX-OVR   TX-OK TX-ERR TX-DRP TX-OVR Flg
 Interface %s não inicializada
 Tabela da Interface kernel
 Não são permitidas chaves com ipip e sit.
 LAST_ACK ESCUTA ESCUTANDO Nenhuma entrada ARP para %s
 Este sistema não suporta ECONET.
 Este sistema não tem suporte ao INET.
 Este sistema não tem suporte para INET6.
 Problema ao ler dados de %s
 RAW RDM RECUPERADO O algoritmo RTO é %s A memória de sistema esgotou-se %u vezes enquanto se enviavam pacotes A resolver `%s' ...
 Resultado: h_addr_list=`%s'
 Resultado: h_aliases= `%s'
 Resultado: h_name=`%s'
 SABM ENVIADO SEQPACOTE STREAM SYN_RECEB SYN_ENVI A configurar o nome de domínio como `%s'
 A configurar o nome da máquina para `%s'
 A configurar o nome de nó para `%s'
 TIME_WAIT Este kernel não suporta RARP.
 DESC DESCONHECIDO Tipo de média desconhecido.
 Utilização:
  arp [-vn] [<HW>] [-i <if>] [-a] [<nome da máquina>] <-Mostra a cache ARP
 Utilização:
  ifconfig [-a] [-v] [-s] <interface> [[<AF>] <endereço>]
 Utilização: hostname [-v] {nome da máquina|-F ficheiro} configurar o nome da máquina (a partir do ficheiro)
 Usage: iptunnel { add | change | del | show } [ NOME ]
 Utilização: plipconfig [-a] [-i] [-v] interface
 Utilização: rarp -a                               lista as entradas em cache.
 Utilização: route [-nNvee] [-FC] [<AF>]           Listar tabelas de encaminhamento do kernel
 AVISO: Ocorreu pelo menos um erro. (%d)
 Aviso: A interface %s ainda está em modo ALLMULTI.
 Aviso: A interface %s ainda está em modo BROADCAST.
 Aviso: A interface %s ainda está em modo dinâmico.
 Aviso: A interface %s ainda está em modo MULTICAST.
 Aviso: A interface %s ainda está em modo PONTOAPONTO.
 Aviso: A interface %s ainda está em modo promíscuo... talvez esteja outra aplicação a correr?
 Onde: NOME := EXPRESSÃO
 Formato de /proc/net/dev incorrecto. Desculpe.
 respostas de máscara de endereço: %u pedido de máscara de endereço: %u pedidos de máscara de endereço: %u arp: %s: tipo de hardware não suporta ARP.
 arp: %s: o kernel só suporta 'inet'.
 arp: %s: família de endereços desconhecida.
 arp: %s: tipo de hardware desconhecido.
 arp: -N ainda não suportado.
 arp: não é possível abrir o etherfile %s !
 arp: não é possível mudar a entrada na linha %u do etherfile %s !
 arp: não é possível obter o endereço de hardware para `%s': %s.
 arp: o dispositivo `%s' tem o endereço de hardware %s `%s'.
 arp: erro de formato na linha %u do etherfile %s !
 arp: não se encontrou coincidências em %d entradas.
 arp: endereço de hardware inválido
 arp: necessário o endereço de hardware
 arp: precisa do nome do anfitrião
 arp: tipo de protocolo não coincide.
 não foi possível determinar o modo do túnel (ipip, gre ou sit)
 destino inacessível: %u nome de domínio (que é parte do FQDN) no ficheiro /etc/hosts.
 respostas eco: %u pedido de eco: %u pedidos de eco: %u getdomainname()=`%s'
 gethostname()=`%s'
 getnodename()=`%s'
 tipo de endereço hw `%s' não tem um gestor para configurar o endereço. falhou.
 ifconfig: Não foi possível configurar o endereço para esta família de protocolos.
 ifconfig: `--help' mostra informação e ajuda.
 ifconfig: opção `%s' não reconhecida.
 manter vivo (%2.2f/%ld/%d) informação sobre a interface perdida máscara de rede %s  netstat: família de endereços %d não suportada!
 nenhuma entrada RARP para %s.
 desactivo (0.00/%ld/%d) em %s
 activo (%2.2f/%ld/%d) activo%d (%2.2f/%ld/%d) problemas ao ler dados de %s
 rarp: %s: tipo de hardware desconhecido.
 rarp: %s: anfitrião desconhecido
 rarp: não foi possível abrir o ficheiro %s:%s.
 rarp: não é possível configurar a entrada a partir de %s:%u
 rarp: erro de formato em %s:%u
 redireccionamento: %u redireccionamentos %u fonte extinta: %u extinção da origem: %u tempo excedido: %u tempo limite em transito: %u tempo de espera (%2.2f/%ld/%d) ttl != 0 e noptmudisc são incompatíveis
 desco-%d (%2.2f/%ld/%d) desconhecido utilização: netstat [-veenNcCF] [<Af>] -r         netstat {-V|--version|-h|--help}
 aviso, a linha igmp %d está incorrecta.
 aviso, a linha %d de igmp6 está incorrecta.
 aviso, linha raw incorrecta.
 aviso, linha tcp incorrecta.
 aviso, linha udp incorrecta.
 aviso, linha unix incorrecta.
 parâmetros incorrectos: %u 