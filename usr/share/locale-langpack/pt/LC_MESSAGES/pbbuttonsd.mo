��    <      �  S   �      (  *   )  1   T  B   �     �     �  *   �  =     )   ]     �  '   �     �     �       !        A     [  /   {  9   �     �     �                5     S  3   m  =   �  2   �  �   	  R   �	     /
  8   A
     z
     �
  <   �
     �
  "   �
     
  =   !     _     p  ?   �     �     �     �     �  !        *     8     T     a      x     �  #   �     �  
   �  %        (     8     X  �  i  .     7   6  ]   n     �  #   �  ;   �  U   ;  3   �  %   �  :   �  *   &  *   Q  "   |  3   �  %   �  ,   �  =   &  =   d     �  2   �     �        "   ,  "   O  H   r  W   �  ;     �   O  s        {  S   �     �     �  N        S  6   c  #   �  X   �          4  X   J     �     �     �     �  )   �     '  %   3     Y     i  ,   �     �  -   �     �       $   %     J     b     �         &             9                    3   (       6      .   $      0           +                        )          :   <          1                          2                '   "   4          -       /              5   7              
   8             *   %   	             ;      #   ,       !    %-14s - prints a list of clients attached
 %s (version %s) - control client for pbbuttonsd.
 %s - daemon to support special features of laptops and notebooks.
 %s, version %s Can't attach card '%s': %s
 Can't create message port for server: %s.
 Can't find pmud on port 879. Trigger PMU directly for sleep.
 Can't get volume of master channel [%s].
 Can't load card '%s': %s
 Can't open %s. Eject CDROM won't work.
 Can't open ADB device %s: %s
 Can't open PMU device %s: %s
 Can't open card '%s': %s
 Can't open mixer device [%s]. %s
 Can't register mixer: %s
 Card '%s' has no '%s' element.
 Current battery cycle: %d, active logfile: %s.
 ERROR: Have problems reading configuration file [%s]: %s
 File doesn't exist File not a block device File not a file Help or version info Memory allocation failed: %s
 Messageport not available Mixer element '%s' has no playback volume control.
 No event devices available. Please check your configuration.
 Option 'ALSA_Elements' contains no valid elements
 Options:
    -%c, --help               display this help and exit
    -%c, --version            display version information and exit
    -%c, --ignore             ignore config return and error values
 Orphaned server port found and removed. All running clients have to be restarted.
 Permission denied Please check your CDROM settings in configuration file.
 Private Tag Registration failed SECURITY: %s must be owned by the same owner as pbbuttonsd.
 Script '%s' %s
 Script must be write-only by owner Server already running Server is already running. Sorry, only one instance allowed.
 Server not found Supported commands:
 Too many formatsigns. Max three %%s allowed in TAG_SCRIPTPMCS.
 Unknown PowerBook Usage:
 Usage: %s [OPTION]... 
 argument invalid can't be launched - fork() failed doesn't exist failed - unknown error code format error function not supported lauched but exitcode is not null lauched but exited by signal lauched but killed after %d seconds launched and exited normally open error pmud support compiled in and active.
 read-only value skipped because it's not secure write-only value Project-Id-Version: pbbuttonsd
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2007-07-07 19:36+0200
PO-Revision-Date: 2011-04-15 12:25+0000
Last-Translator: Carlos Manuel <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:35+0000
X-Generator: Launchpad (build 18115)
 %-14s - mostra uma lista de clientes anexados
 %s (versão %s) - cliente de controlo para pbbuttonsd.
 %s - serviço para suportar as características de computadores portáteis e de secretária.
 %s, versão %s Incapaz de anexar a placa '%s': %s
 Não pode criar um porto de mensagens para o servidor: %s.
 Incapaz de encontrar o pmud no porto 879. Activar o PMU directamente para suspender.
 Incapaz de obter o volume do canal principal [%s].
 Incapaz de carregar a placa '%s': %s
 Não consegue abrir %s. Ejectar o CDROM não funcionará.
 Incapaz de abrir o dispositivo ADB %s: %s
 Incapaz de abrir o dispositivo PMU %s: %s
 Incapaz de abrir a placa '%s': %s
 Incapaz de abrir o dispositivo misturador [%s]. %s
 Incapaz de registar o misturador: %s
 A placa '%s' não tem nenhum elemento '%s'.
 Ciclo de bateria actual: %d, ficheiro de registo activo: %s.
 ERRO: Problemas ao ler o ficheiro de configuração [%s]: %s
 O ficheiro não existe O ficheiro não é um dispositivo de bloqueamento. O ficheiro não é um ficheiro Ajuda ou informação da versão Alojamento de memória falhou: %s
 Porta de mensagem não disponível O elemento misturador '%s' não tem controlo de volume de reprodução.
 Nenhuns dispositivos de evento disponíveis. Por favor verifique a sua configuração.
 A opção 'Elementos_ALSA' não contém elementos válidos
 Opções:
    -%c, --help mostrar este ajuda e sair
    -%c, --version mostrar informação da versão e sair
    -%c, --ignore ignorar configuração e retornar os valores dos erros
 Foi encontrado e removido um porto do servidor orfão. Todos os clientes em execução tiveram de ser reiniciados.
 Permissão negada Por favor verifique as configurações do seu CDROM no ficheiro de configuração.
 Etiqueta Privada Falha ao registar SEGURANÇA: o %s tem de ser possuído pelo mesmo utilizador que o pbbuttonsd.
 Script '%s' %s
 O script deve ser apenas de escrita pelo proprietário O servidor já se encontra a correr O servidor já se encontra em execução. Desculpe, apenas uma instância é permitida.
 Servidor não foi encontrado Comandos suportados:
 Demasiados sinais dos formatos. No máximo três %%s são permitidos no TAG_SCRIPTPMCS.
 PowerBook desconhecido Utilização:
 Utilização: %s [OPÇÃO]... 
 argumento inválido incapaz de ser iniciado - o fork() falhou não existe falhou - erro de código desconhecido erro de formato função não suportada iniciou mas o código de saída não é nulo iniciado mas saiu pelo sinal iniciou mas foi encerrou passados %d segundos Iniciou e saiu normalmente erro de abertura suporte de pmud compilado e activo.
 valor apenas de leitura ignorado porque não é seguro valor apenas de escrita 