��    -      �  =   �      �     �     �       	   
  	             ,     3  	   6     @     `     h     o  	   w  	   �     �     �  
   �     �  #   �     �     �       
   )     4     O     U     g     k     n     �     �     �     �     �     �     �     �                    "     %     +  �  D     �     �       	     	             *     1  	   4     >     ^     f     m  	   u  
     	   �     �  
   �     �  '   �     �  ,   	     1	     ?	      T	     u	     {	  	   �	     �	     �	     �	     �	  
   �	  
   
     
  	   #
     -
     >
     U
     \
  	   c
     m
     t
     y
     (                                                #      $   *   
      )                                     "       -      	          !          ,                       &         %   +                 '              %8llu    %8llu    %8llu #%-7d %-8.8s %02d:%02d %8llu     %d	%llu	%llu
 %ddays %s %s (%s):
 %s (uid %d): Permission denied
 %s: %s
 %udays %uhours %uminutes %useconds 0seconds Block %u is truncated.
 Block %u:  Bugs to %s
 Cannot change permission of %s: %s
 Compiled with:%s
 Error checking device name: %s
 Error with %s.
 Filesystem Found an invalid UUID: %s
 Group Headers checked.
 OFF ON Reference to illegal block %u Substracted %lu bytes.
 Unknown option '%c'.
 User done
 error (%d) while opening %s
 files getgroups(): %s
 group %s does not exist.
 limit none off on quota user %s does not exist.
 Project-Id-Version: quota
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-16 18:19+0100
PO-Revision-Date: 2011-04-21 22:44+0000
Last-Translator: Mykas0 <Mykas0@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:15+0000
X-Generator: Launchpad (build 18115)
     %8llu    %8llu    %8llu #%-7d %-8.8s %02d:%02d %8llu     %d	%llu	%llu
 %ddias %s %s (%s):
 %s (uid %d): Permissão negada
 %s: %s
 %udias %uhoras %uminutos %usegundos 0segundos O bloco %u está truncado.
 Bloco %u:  Erros para %s
 Não pode mudar a permissão de %s: %s
 Compilado com:%s
 Erro ao verificar o nome do dispositivo: %s
 Erro com %s.
 Sistema de ficheiros Encontrou um UUID inválido: %s
 Grupo Cabeçalhos verificados.
 DESLIGADO LIGADO Referência ao bloco ilegal %u Subtraiu %lu bytes.
 Opção desconhecida '%c'.
 Utilizador terminado
 erro (%d) ao abrir %s
 ficheiros getgroups(): %s
 grupo %s não existe.
 limite nenhum desligado ligado cota utilizador %s não existe.
 