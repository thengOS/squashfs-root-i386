��         ,  �  <       +     	+     +     .+  
   @+  	   K+     U+  &   h+  2   �+  +   �+      �+     ,      -,     N,     ^,     m,     y,     |,     �,     �,     �,  (   �,  &   �,  5   #-  2   Y-  3   �-     �-     �-     �-     �-     �-  !   .     3.     S.     i.     w.     �.     �.     �.     �.  !   /     )/      C/  !   d/  "   �/     �/  #   �/  M   �/     60     E0  
   X0     c0     r0     0     �0     �0     �0  	   �0     �0     �0     �0     �0     1  <   !1  9   ^1  ?   �1  I   �1  >   "2  <   a2  >   �2  ?   �2  F   3  U   d3  G   �3  6   4  J   94  ?   �4  D   �4  $   	5     .5     45  
   @5     K5     Z5     a5  
   r5  	   }5  ,   �5     �5  )   �5     �5  !   6     76     R6     `6     �6     �6     �6  	   �6     �6     �6      7     7     37     G7  2   ^7  A   �7     �7     �7  1   
8  )   <8  (   f8  $   �8     �8  6   �8  #   9     &9     B9      Y9     z9     �9     �9     �9  ;   �9      
:     +:     D:  $   Z:  "   :     �:     �:  "   �:     �:     ;     !;  "   >;     a;     };     �;     �;     �;  &   �;  !   �;     !<  #   5<     Y<      v<  !   �<     �<  *   �<     =  !   #=     E=  %   R=     x=     �=     �=     �=     �=     �=     
>     >     9>  !   X>     z>  ,   �>     �>     �>     �>     ?     ?     0?     @?     ]?     z?  '   �?  )   �?  !   �?  !   	@     +@     @@     V@      l@     �@     �@  "   �@  ,   �@     �@     A     A  !   0A     RA     UA     iA     �A     �A  !   �A  ,   �A     B     B  5   1B     gB     tB     �B  #   �B  '   �B     �B  .   �B     .C     =C     PC  &   ^C  H   �C  $   �C  '   �C     D  *   ;D     fD     }D      �D     �D     �D     �D  #   E     3E     PE  6   mE     �E     �E     �E      �E     F  '   F     GF  *   \F  %   �F     �F  !   �F  
   �F     �F  .   �F     G     9G  )   YG  ,   �G     �G  
   �G     �G  *   �G  )   H  (   GH  0   pH  /   �H  )   �H  (   �H  :   $I  9   _I  J   �I  D   �I  C   )J  F   mJ  E   �J  )   �J  #   $K     HK     ^K     {K  $   �K     �K  4   �K  %   L     +L  &   FL     mL     �L     �L  	   �L  +   �L     �L  &   �L  )   &M  (   PM  *   yM  '   �M  )   �M  (   �M  (   N  +   HN  /   tN  .   �N  +   �N  9   �N  &   9O  #   `O     �O  ;   �O      �O  "   �O  (   !P  .   JP  /   yP     �P     �P     �P  '   �P     &Q  !   BQ  !   dQ  %   �Q     �Q  "   �Q     �Q     R     R     &R     @R  "   \R  *   R  %   �R     �R  !   �R  	   �R     S  ?   S  "   QS  2   tS     �S  #   �S     �S     �S  9   �S  >   /T  $   nT     �T  7   �T  D   �T     U  '   <U  %   dU  -   �U     �U     �U     �U     	V  G   V     eV     {V     �V     �V     �V     �V  #   �V     W      'W  *   HW      sW     �W     �W     �W  (   �W     X  4   (X  )   ]X  #   �X  &   �X     �X  "   �X     Y  &   2Y     YY  3   uY  $   �Y  ,   �Y     �Y  )   Z     9Z     VZ  /   pZ  ;   �Z  0   �Z     [     %[  #   9[  1   ][  )   �[  	   �[     �[     �[     �[     \     .\  '   @\     h\     p\  $   �\  %   �\  #   �\  !   �\     ]     .]     K]     ]]  
   p]     {]     �]     �]     �]     �]     �]     ^     $^     C^     ^^     v^     �^     �^     �^     �^     �^     �^  3   _  -   9_  $   g_  <   �_     �_  2   �_  '   `     ;`     O`  8   h`     �`  0   �`     �`  0   a  *   @a  +   ka     �a  3   �a  %   �a  #   b     0b     Mb  )   jb  -   �b     �b  0   �b      c  #   .c  .   Rc  /   �c  6   �c  6   �c     d     5d  >   Td     �d  +   �d  '   �d  -   e     2e     Re  )   oe  s   �e     f     )f     Bf  1   \f     �f     �f     �f     �f  V   �f     g  +   #g     Og  (   _g     �g  )   �g  /   �g  &   �g  &   h     Ch     _h     ~h     �h  W   �h     i     &i     Di     Qi     hi     �i     �i  :   �i     �i     �i  $   �i  &   j  5   Ej     {j  '   �j  V   �j     k  $   ,k  %   Qk  $   wk     �k     �k  	   �k     �k      �k      l     &l     Fl  �  _l     7n     Un     in  
   }n     �n     �n  2   �n  ;   �n  (   o  '   Ao      io  (   �o     �o     �o     �o     �o  "   �o     p      p     3p  /   Bp  4   rp  E   �p  =   �p  :   +q     fq     zq     �q     �q     �q  .   �q  *   �q     r     2r  (   Ar     jr     �r  &   �r  !   �r  #   �r     s  $   ,s  &   Qs  '   xs     �s  $   �s  N   �s     1t     Bt     Zt     ht     yt     �t      �t     �t     �t     �t     �t     �t      �t     u     =u  H   Pu  7   �u  G   �u  W   v  J   qv  H   �v  E   w  L   Kw  P   �w  c   �w  ^   Mx  7   �x  M   �x  F   2y  S   yy  *   �y     �y     �y  	   z  	   z  	   !z     +z  
   ?z  	   Jz  ,   Tz     �z  1   �z  #   �z  (   �z  ,   {     F{  &   _{  "   �{  '   �{     �{      �{     |     .|  "   H|      k|     �|     �|  6   �|  H   �|     F}  !   b}  4   �}  '   �}  (   �}  /   
~     :~  =   Y~  (   �~  *   �~     �~  0     (   6      _  )   �     �  H   �  (   �     7�     W�  %   o�  !   ��      ��     ؀  %   �     �     6�     O�  /   m�  #   ��     ��     Ӂ     �  (   �  ,   /�  4   \�     ��  +   ��  ,   ܂  ,   	�  (   6�  !   _�  0   ��  *   ��  $   ݃     �  2   �      F�  '   g�  +   ��  '   ��     �     �  &   �  *   <�  #   g�  '   ��  2   ��  C   �     *�     9�  #   F�     j�     ��     ��     ��  *   ц  '   ��  .   $�  -   S�  (   ��  )   ��     ԇ     �     �  ,   �     E�     K�  "   S�  F   v�  '   ��     �     ��  .   �     @�     C�     ]�     |�  %   ��  (   ��  .   �      �     4�  @   M�     ��     ��      ��  -   ؊  5   �     <�  4   L�     ��     ��     ��  .   ��  R   �  (   7�  5   `�  #   ��  5   ��     ��  $   �  )   2�     \�  +   {�  "   ��  0   ʍ  "   ��  .   �  8   M�     ��      ��  "   Ŏ  &   �     �  '   )�     Q�  .   h�  3   ��     ˏ     ߏ     ��     
�  :   �     I�  "   e�  /   ��  3   ��  "   �     �     �  7   6�  2   n�  1   ��  >   ӑ  =   �  ;   P�  :   ��  @   ǒ  ?   �  S   H�  J   ��  I   �  P   1�  O   ��  1   Ҕ  5   �     :�  #   W�      {�  2   ��  -   ϕ  ?   ��  0   =�  "   n�  0   ��  (        �     �  
   !�  3   ,�  *   `�  )   ��  2   ��  *   �  ,   �  )   @�  +   j�  ;   ��  5   Ҙ  ,   �  0   5�  /   f�  ,   ��  :   Ù  (   ��  '   '�     O�  K   n�  /   ��  )   �  7   �  0   L�  3   }�  %   ��  $   כ  #   ��  2    �  ,   S�  '   ��  )   ��  ,   Ҝ  &   ��  6   &�  -   ]�     ��     ��          ��  #    �  0   $�  (   U�     ~�  2   ��  	   ��       N   ў  &    �  J   G�  !   ��  .   ��     �     �  L   ��  Q   F�  ,   ��     Š  M   ֠  I   $�  )   n�  &   ��  '   ��  3   �  !   �  "   =�     `�     x�  L   ��     �     ��      �     1�     ?�     ]�  .   o�  #   ��  ,   £  5   �  /   %�  "   U�      x�  #   ��  /   ��      ��  9   �  /   H�  )   x�  /   ��  "   ҥ  '   ��  "   �  (   @�      i�  7   ��  #   ¦  3   �     �  /   0�     `�  #   ��  5   ��  D   ڧ  =   �     ]�     w�  9   ��  ;   Ǩ  ,   �  
   0�  +   ;�  +   g�     ��     ��     ɩ  3   ܩ     �     �  -   %�  (   S�  &   |�  %   ��     ɪ     �     �     �     3�     F�     U�  #   n�  /   ��  2   «     ��     �  #   ,�  "   P�     s�     ��     ��  (   Ǭ  (   �     �      �     /�  D   ?�  :   ��  )   ��  E   �     /�  0   J�  #   {�     ��     ��  ?   ծ     �  +   5�  %   a�  3   ��  .   ��  9   �     $�  A   D�  -   ��     ��  %   ԰  $   ��  =   �  .   ]�  $   ��  B   ��  +   ��  >    �  D   _�  @   ��  @   �  @   &�     g�     ��  X   ��  !   ��  ,   �  (   G�  *   p�  &   ��  #   ´  .   �  ~   �  0   ��      ŵ  $   �  =   �     I�     V�     i�     ��  d   ��  
   �  -   �     <�  *   N�     y�  4   ��  8   ȷ  8   �  K   :�     ��  !   ��  !   Ǹ  !   �  a   �     m�     ��     ��      ��     Թ     �     �  :   �     Y�     n�  $   ��  +   ��  C   պ     �  2   .�  S   a�  #   ��  ,   ٻ  )   �  (   0�  $   Y�  ,   ~�     ��  
   ��  $   ��  &   �     
�     *�         �     7       �   y       �  �   �  6   �   �      �  E     W      �   ,  >      �   .      �   "      J   �  �   �  k      _   v  �   �   ;   �   �                 �   �  �  �     P  �   �   �   h   �  �   y      n   \      �       	  +  
  �   o  ]  �       -   �        |   �  f      �       %   �      �   �                 s   7         ;  �       �   *  �   �       %         �         �   J      �   �  C   e       8       Q  �  �             V   �   }   \  �   !       �       A  �       �  v   x  +   �               1           �   �        �   9   S      �       �  �       �   �     �   B   �  �  }  -      �  �  5  �     �   �     �          �   �   b   &      �  �   j      �     N         �         �       �  p  �  �      �      3      n  �   L   �   �  �      �  Z  K   �  O  [  ]       �          g     �   �      �   �   3       �  r      T             $      "       :           =      �   �   V          �   �          �  �   R  �   c   K  �  l  ?  �  �   L  �  �  �  �   Q      �   #      �       {   �       �  �  �  �      1  �  �      �   �  �  <  �  �   �           �   x   >   �  �              h                X  u   �   �  4   @   �      �   
   t  `   �   �  M  �  �       �   �       �  �       5   �      �  �  k          �  a      �  Y   �       �   �  �  �  w   �   �  �  l   c      ,   G   �                 (               �  �         !          �  �   �  '  �  �  H   {  I         (  �  �   =      �  8  [     q       U  �   �   g           �   f   �       @  �         �   �  G  �      ?   u     �  E  �  �  �       D   �       �  t                  �  o   '       Z              M     w  r       �   �  )      �   �  ~    �   �   �     �   �       �  �  �  B  �     .       �      W       �   z   �      4     �   |     p   �   D  F     �       /  �   &   i   �  A   �  0     F   �  #           �      �      �         �   �   :              �   2         �           �      �       �  �   ^      �   /   9  �   �   ^   �  m  �      O         �  �  e      �  �  N  �   $      I         s  �   m       �   �   �      q  U   z      �          �                     �  �  �       ~       Y  �   H  �      �              a       �  �     �  S       0  �       �  b  �  R       )   _  �       2        `     �          �   �  �  P          �  d    �   i    *   �  �            �  d                 �      �   �       j               �   X               �          �   T  6    �  	   �      <   C  �   

RPM build errors:
  (MISSING KEYS:  (UNTRUSTED KEYS:  failed -   on file  ! only on numbers
 %%changelog entries must start with *
 %%changelog not in descending chronological order
 %%patch without corresponding "Patch:" tag
 %%semodule requires a file path
 %%{buildroot} can not be "/"
 %%{buildroot} couldn't be empty
 %3d<%*s(empty)
 %3d>%*s(empty) %a %b %d %Y %c %s %d defined multiple times
 %s cannot be installed
 %s created as %s
 %s failed: %x
 %s field must be present in package: %s
 %s has invalid numeric value, skipped
 %s has too large or too small integer value, skipped
 %s has too large or too small long value, skipped
 %s is a Delta RPM and cannot be directly installed
 %s saved as %s
 %s: %s
 %s: %s: %s
 %s: Fread failed: %s
 %s: Fwrite failed: %s
 %s: can't load unknown tag (%d).
 %s: cannot read header at 0x%x
 %s: failed to encode
 %s: line: %s
 %s: not an armored public key.
 %s: open failed: %s
 %s: public key read failed.
 %s: read manifest failed: %s
 %s: rpmReadSignature failed: %s %s: rpmWriteSignature failed: %s
 %s: writeLead failed: %s
 %s:%d: Argument expected for %s
 %s:%d: Got a %%else with no %%if
 %s:%d: Got a %%endif with no %%if
 %s:%d: bad %%if condition
 && and || not suported for strings
 '%s' type given with other types in %%semodule %s. Compacting types to '%s'.
 (invalid type) (invalid xml type) (no error) (no state)     (not a blob) (not a number) (not an OpenPGP signature) (not base64) (unknown %3d)  (unknown) ) )  * / not suported for strings
 - not suported for strings
 - only on numbers
 --allfiles may only be specified during package installation --allmatches may only be specified during package erasure --excludedocs may only be specified during package installation --hash (-h) may only be specified during package installation and erasure --ignorearch may only be specified during package installation --ignoreos may only be specified during package installation --ignoresize may only be specified during package installation --includedocs may only be specified during package installation --justdb may only be specified during package installation and erasure --nodeps may only be specified during package installation, erasure, and verification --percent may only be specified during package installation and erasure --prefix may only be used when installing new packages --relocate and --excludepath may only be used when installing new packages --replacepkgs may only be specified during package installation --test may only be specified during package installation and erasure : expected following ? subexpression <dir> <old>=<new> <package>+ <packagefile>+ <path> <source package> <specfile> <tarball> ======================== active %d empty %d
 ? expected in expression A %% is followed by an unparseable macro
 Architecture is excluded: %s
 Architecture is not included: %s
 Archive file not in header Bad CSA data
 Bad arch/os number: %s (%s:%d)
 Bad dirmode spec: %s(%s)
 Bad exit status from %s (%s)
 Bad file: %s: %s
 Bad magic Bad mode spec: %s(%s)
 Bad owner/group: %s
 Bad package specification: %s
 Bad source: %s: %s
 Bad syntax: %s(%s)
 Bad/unreadable  header Base modules '%s' and '%s' have overlapping types
 Build options with [ <specfile> | <tarball> | <source package> ]: Building for target %s
 Building target platforms: %s
 Common options for all rpm modes and executables: Conversion of %s to long integer failed.
 Copyright (C) 1998-2002 - Red Hat, Inc.
 Could not canonicalize hostname: %s
 Could not exec %s: %s
 Could not generate output filename for package %s: %s
 Could not open %%files file %s: %m
 Could not open %s file: %s
 Could not open %s: %s
 Couldn't create pipe for %s: %m
 Couldn't download %s
 Couldn't exec %s: %s
 Couldn't fork %s: %s
 Database options: Dependency tokens must begin with alpha-numeric, '_' or '/' Directory not found by glob: %s
 Directory not found: %s
 Downloading %s to %s
 Duplicate %s entries in package: %s
 Duplicate locale %s in %%lang(%s)
 Empty file classifier
 Enter pass phrase:  Error executing scriptlet %s (%s)
 Error parsing %%setup: %s
 Error parsing %s: %s
 Error parsing tag field: %s
 Error reading %%files file %s: %m
 Exec of %s failed (%s): %s
 Executing "%s":
 Executing(%s): %s
 Execution of "%s" failed.
 Failed build dependencies:
 Failed to determine a policy name: %s
 Failed to encode policy file: %s
 Failed to find %s:
 Failed to get policies from header
 Failed to open tar pipe: %m
 Failed to read  policy file: %s
 Failed to read spec file from %s
 Failed to rename %s to %s: %m
 File %s does not appear to be a specfile.
 File %s is not a regular file.
 File %s is smaller than %u bytes
 File %s: %s
 File capability support not built in
 File listed twice: %s
 File must begin with "/": %s
 File needs leading "/": %s
 File not found by glob: %s
 File not found: %s
 Finding  %s: %s
 Header size too big Ignoring invalid regex %s
 Incomplete data line at %s:%d
 Incomplete default line at %s:%d
 Install/Upgrade/Erase options: Installed (but unpackaged) file(s) found:
%s Installing %s
 Internal error Internal error: Bogus tag %d
 Invalid %s token: %s
 Invalid capability: %s
 Invalid date %u Invalid patch number %s: %s
 Macro %%%s failed to expand
 Macro %%%s has empty body
 Macro %%%s has illegal name (%%define)
 Macro %%%s has illegal name (%%undefine)
 Macro %%%s has unterminated body
 Macro %%%s has unterminated opts
 Missing %s in %s %s
 Missing '(' in %s %s
 Missing ')' in %s(%s
 Missing module path in line: %s
 NO  NOT OK No "Source:" tag in the spec file
 No compatible architectures found for build
 No file attributes configured
 No patch number %u
 No source number %u
 Non-white space follows %s(): %s
 OK OS is excluded: %s
 OS is not included: %s
 Package already exists: %s
 Package check "%s" failed.
 Package has no %%description: %s
 Pass phrase check failed or gpg key expired
 Pass phrase is good.
 Please contact %s
 Policy module '%s' duplicated with overlapping types
 Preparing... Processing files: %s
 Processing policies: %s
 Query options (with -q or --query): Query/Verify package selection options: RPM version %s
 Recognition of file "%s" failed: mode %06o %s
 Retrieving %s
 Signature options: Spec options: Symlink points to BuildRoot: %s -> %s
 This program may be freely redistributed under the terms of the GNU GPL
 Too many args in data line at %s:%d
 Too many args in default line at %s:%d
 Too many arguments in line: %s
 Unable to create immutable header region.
 Unable to open %s: %s
 Unable to open icon %s: %s
 Unable to open spec file %s: %s
 Unable to open stream: %s
 Unable to open temp file: %s
 Unable to read icon %s: %s
 Unable to reload signature header.
 Unable to write package: %s
 Unable to write temp header
 Unknown file digest algorithm %u, falling back to MD5
 Unknown file type Unknown icon type: %s
 Unknown option %c in %s(%s)
 Unknown payload compression: %s
 Unknown system: %s
 Unsupported payload (%s) in package %s
 Unterminated %c: %s
 Unusual locale length: "%s" in %%lang(%s)
 Verify options (with -V or --verify): Version required Versioned file name not permitted Wrote: %s
 YES You must set "%%_gpg_name" in your macro file
 ] expected at end of array argument is not an RPM package
 arguments to --prefix must begin with a / arguments to --root (-r) must begin with a / bad date in %%changelog: %s
 bad format bad option '%s' at %s:%d
 build binary package from <source package> build binary package only from <specfile> build binary package only from <tarball> build source and binary packages from <specfile> build source and binary packages from <tarball> build source package only from <specfile> build source package only from <tarball> build through %build (%prep, then compile) from <specfile> build through %build (%prep, then compile) from <tarball> build through %install (%prep, %build, then install) from <source package> build through %install (%prep, %build, then install) from <specfile> build through %install (%prep, %build, then install) from <tarball> build through %prep (unpack sources and apply patches) from <specfile> build through %prep (unpack sources and apply patches) from <tarball> buildroot already specified, ignoring %s
 cannot add record originally at %u
 cannot create %s: %s
 cannot get %s lock on %s/%s
 cannot open %s: %s
 cannot open Packages database in %s
 cannot re-open payload: %s
 cannot use --prefix with --relocate or --excludepath create archive failed on file %s: %s
 create archive failed: %s
 creating a pipe for --pipe failed: %m
 debug file state machine debug rpmio I/O delete package signatures directory display final rpmrc and macro configuration display known query tags display the states of the listed files do not accept i18N msgstr's from specfile do not execute %%post scriptlet (if any) do not execute %%postun scriptlet (if any) do not execute %%pre scriptlet (if any) do not execute %%preun scriptlet (if any) do not execute %check stage of the build do not execute %clean stage of the build do not execute any %%triggerin scriptlet(s) do not execute any %%triggerpostun scriptlet(s) do not execute any %%triggerprein scriptlet(s) do not execute any %%triggerun scriptlet(s) do not execute any scriptlet(s) triggered by this package do not execute any stages of the build do not execute package scriptlet(s) do not install documentation do not reorder package installation to satisfy dependencies do not verify build dependencies do not verify package dependencies don't check disk space before installing don't import, but tell if it would work or not don't install, but tell if it would work or not don't verify files in package don't verify group of files don't verify mode of files don't verify modification time of files don't verify owner of files don't verify package architecture don't verify package dependencies don't verify package operating system don't verify size of files don't verify symlink path of files dump basic file information empty tag format empty tag name erase (uninstall) package error reading from file %s
 error reading header from package
 error(%d) allocating new package instance
 error(%d) storing record #%d into %s
 error:  exclude paths must begin with a / exclusive exec failed
 failed to rebuild database: original database remains in place
 failed to remove directory %s: %s
 failed to replace old database with new database!
 failed to stat %s: %m
 failed to write all data to %s: %s
 fatal error:  file file %s conflicts between attempted installs of %s and %s file %s from install of %s conflicts with file from package %s file %s is not owned by any package
 file %s: %s
 files may only be relocated during package installation generate package header(s) compatible with (legacy) rpm v3 packaging gpg failed to write signature
 group %s does not contain any packages
 group %s does not exist - using root
 ignore ExcludeArch: directives from spec file import an armored public key incomplete %%changelog entry
 incorrect format: %s
 initialize database install all files, even configurations which might otherwise be skipped install documentation invalid dependency invalid package number: %s
 line %d: %s
 line %d: %s is deprecated: %s
 line %d: %s: %s
 line %d: Bad %%setup option %s: %s
 line %d: Bad %s number: %s
 line %d: Bad %s: qualifiers: %s
 line %d: Bad BuildArchitecture format: %s
 line %d: Bad arg to %%setup: %s
 line %d: Bad no%s number: %u
 line %d: Bad number: %s
 line %d: Bad option %s: %s
 line %d: Docdir must begin with '/': %s
 line %d: Empty tag: %s
 line %d: Epoch field must be an unsigned number: %s
 line %d: Error parsing %%description: %s
 line %d: Error parsing %%files: %s
 line %d: Error parsing %%policies: %s
 line %d: Error parsing %s: %s
 line %d: Illegal char '%c' in: %s
 line %d: Illegal char in: %s
 line %d: Illegal sequence ".." in: %s
 line %d: Malformed tag: %s
 line %d: Only noarch subpackages are supported: %s
 line %d: Package does not exist: %s
 line %d: Prefixes must not end with "/": %s
 line %d: Second %s
 line %d: Tag takes single token only: %s
 line %d: Too many names: %s
 line %d: Unknown tag: %s
 line %d: internal script must end with '>': %s
 line %d: interpreter arguments not allowed in triggers: %s
 line %d: script program must begin with '/': %s
 line %d: second %%prep
 line %d: second %s
 line %d: triggers must have --: %s
 line %d: unclosed macro or bad line continuation
 line %d: unsupported internal script: %s
 line: %s
 list all configuration files list all documentation files list files in package list keys from RPM keyring malformed %s: %s
 memory alloc (%u bytes) returned NULL.
 missing missing   %c %s missing ':' (found 0x%02x) at %s:%d
 missing architecture for %s at %s:%d
 missing architecture name at %s:%d
 missing argument for %s at %s:%d
 missing name in %%changelog
 missing second ':' at %s:%d
 missing { after % missing } after %{ net shared net shared     no arguments given no arguments given for parse no arguments given for query no arguments given for verify no dbpath has been set no dbpath has been set
 no description in %%changelog
 no package matches %s: %s
 no package provides %s
 no package requires %s
 no package triggers %s
 no packages given for erase no packages given for install normal normal         not installed  one type of query/verify may be performed at a time only installation and upgrading may be forced only one major mode may be specified only one of --excludedocs and --includedocs may be specified open of %s failed: %s
 operate on binary rpms generated by spec (default) operate on source rpm generated by spec override build root override target platform package %s (which is newer than %s) is already installed package %s is already installed package %s is intended for a %s operating system package %s is not installed
 package %s was already added, replacing with %s
 package %s was already added, skipping %s
 package has neither file owner or id lists
 parse error in expression
 print hash marks as package installs (good with -v) print percentages as package installs print the version of rpm being used provide less detailed output provide more detailed output query of specfile %s failed, can't parse
 query the package(s) triggered by the package query/verify all packages query/verify package(s) from install transaction query/verify package(s) in group query/verify package(s) owning file query/verify package(s) with header identifier query/verify package(s) with package identifier query/verify the package(s) which provide a dependency query/verify the package(s) which require a dependency read failed: %s (%d)
 reading symlink %s failed: %s
 rebuild database inverted lists from installed package headers record %u could not be read
 reinstall if the package is already present relocate files from path <old> to <new> relocate the package to <dir>, if relocatable relocations must begin with a / relocations must contain a = relocations must have a / following the = remove all packages which match <package> (normally an error is generated if <package> specified multiple packages) remove build tree when done remove sources when done remove specfile when done replace files in %s with files from %s to recover replaced replaced       rpm query mode rpm verify mode script disabling options may only be specified during package installation and erasure shared short hand for --replacepkgs --replacefiles sign package(s) sign package(s) (identical to --addsign) skip %%ghost files skip files with leading component <path>  skip straight to specified stage (only for c,i) source package contains no .spec file
 source package expected, binary found
 syntax error in expression
 syntax error while parsing &&
 syntax error while parsing ==
 syntax error while parsing ||
 trigger disabling options may only be specified during package installation and erasure types must match
 unable to read the signature
 unexpected ] unexpected query flags unexpected query format unexpected query source unexpected } unknown error %d encountered while manipulating package %s unknown tag unmatched (
 unpacking of archive failed%s%s: %s
 unrecognized db option: "%s" ignored.
 update the database, but do not modify the filesystem upgrade package(s) upgrade package(s) if already installed upgrade to an old version of the package (--force on upgrades does this automatically) use the following query format user %s does not exist - using root
 verify %files section from <specfile> verify %files section from <tarball> verify database files verify package signature(s) warning:  wrong color { expected after : in expression { expected after ? in expression | expected at end of expression } expected in expression Project-Id-Version: RPM
Report-Msgid-Bugs-To: rpm-maint@lists.rpm.org
POT-Creation-Date: 2014-09-18 14:01+0300
PO-Revision-Date: 2016-04-06 13:35+0000
Last-Translator: Ivo Xavier <ivoxavier.8@gmail.com>
Language-Team: Portuguese (http://www.transifex.com/projects/p/rpm/language/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:05+0000
X-Generator: Launchpad (build 18115)
Language: pt
 

Erros de criação do RPM:
  (FALTAM AS CHAVES:  (CHAVES SUSPEITAS:  falhou -   no ficheiro  ! só em números
 As entradas de %%changelog têm de começar por *
 O %%changelog não está na ordem cronológica descendente
 %%patch Patch sem correspondência: tag
 %%semodule path de arquivo necessário
 %%{buildroot} não pode ser "/"
 %%{buildroot} não pode estar em branco
 %3d<%*s(vazio)
 %3d>%*s(vazio) %a %d %b %Y %c %s %d tempos múltiplos definidos
 %s não pode ser instalado
 %s criado como %s
 %s falhou: %x
 O campo %s tem de estar presente no pacote: %s
 O %s tem um valor numérico inválido, foi ignorado
 O %s tem um valor inteiro demasiado elevado ou pequeno, foi ignorado
 O %s tem um valor demasiado elevado ou pequeno, foi ignorado
 %s é um RPM Delta e não pode ser instalado directamente
 %s gravado como %s
 %s: %s
 %s: %s: %s
 %s: O fread falhou: %s
 %s: O fwrite falhou: %s
 %s: não pode carregar tag desconhecida (%d).
 %s: não consigo ler o cabeçalho em 0x%x
 %s: falhou ao codificar
 %s: linha: %s
 %s: não é uma chave pública blindade
 %s: o acesso falhou: %s
 %s:falha ao ler chave pública
 %s: a leitura do manifesto falhou: %s
 %s: o rpmReadSignature falhou: %s %s: o rpmWriteSignature falhou: %s
 %s: o writeLead falhou: %s
 %s:%d: Argumentos esperados para %s
 %s:%d: Descobri um %%else sem um %%if
 %s:%d: Descobri um %%endif sem um %%if
 %s:%d: ruim %%se condição
 && e || não suportado para strings
 '%s' tipo vem com outros tipos em %%semodule %s. Compactando tipos para '%s'.
 (tipo inválido) (tipo de xml inválido) (nenhum erro) (sem estado)     (não é um blob) (não é um número) (não é uma assinatura OpenPGP) (não é um base64) (desconhecido %3d)  (desconhecido) ) )  * / não suportado para strings
 - não suportado para strings
 - só em números
 --allfiles pode ser especificado somente durante a instalção do pacote o --allmatches só pode ser indicado ao apagar o pacote o --excludedocs  só pode ser indicado durante a instalação do pacote --hash (-h) os pacotes só poderão ser especificados e apagados durante a instalação --ignorearch pode ser especificado somente durante a instalção do pacote --ignoreos pode ser especificado somente durante a instalção do pacote o --ignoresize só pode ser indicado durante a instalação do pacote --includedocs apenas pode ser especificado durante a instalação de pacotes o --justdb só pode ser indicado durante a instalação ou a remoção do pacote --nodeps só pode ser especificado durante a instalação, eliminação e a verificação do pacote --percentagem dos pacotes que só poderão ser especificados e apagados durante a instalação o --prefix só pode ser usado ao instalar pacotes novos o --relocate e o --excludepath só podem ser usados ao instalar pacotes novos o --replacepkgs só pode ser indicado durante a instalação do pacote --test  só pode ser especificado durante a instalação e a eliminação do pacote esperado um : a seguir à sub-expressão ? <dir> <velho>=<novo> <pacote>+ <pacote>+ <caminho> <pacote de código> <fichspec> <fichtar> ======================== activo %d vazio %d
 ? esperado na expressão Segue-se uma macro impossível de analisar ao %%
 A arquitectura está excluída: %s
 A arquitectura não está incluída: %s
 Ficheiro de arquivo não está no cabeçalho Dados de CSA inválidos
 Número de arq./SO errado: %s (%s:%d)
 Spec de dirmode inválido: %s(%s)
 Código de saída inválido do %s (%s)
 Ficheiro inválido: %s: %s
 Código de integridade inválido Spec de modo inválido: %s(%s)
 Dono/grupo inválido: %s
 Má especificação do pacote: %s
 Código-fonte inválido: %s: %s
 Sintaxe inválida: %s(%s)
 Cabeçalho inválido/ilegível Módulos base '%s' e '%s' tem sobreposição de tipos
 Opções de criação com [ <fich spec> | <fich tar> | <pacote fonte> ]: A construir para o alvo %s
 A construir plataformas alvo: %s
 Opções comuns para todos modos rpm e executáveis: Falha ao converter %s em longo inteiro
 Copyright (C) 1998-2002 - Red Hat, Inc.
 Não consegui canonizar o nome da máquina: %s
 Não consegui executar %s: %s
 Não consigo gerar o ficheiro de saída para o pacote %s: %s
 Não pode abrir %%files ficheiro %s: %m
 Não foi possível abrir o %s arquivo: %s
 Impossível abrir %s: %s
 Não foi possível criar a conexão para %s: %m
 Não foi possível fazer o  download %s
 Não consegui executar o %s: %s
 Não consegui executar à parte o %s: %s
 Opções da base de dados: Os tokens de dependências devem começar com alpha-numerico, '_' ou '/' Diretório não encontrado por glob: %s
 Diretório não encontrado: %s
 Downloading %s para %s
 Entradas %s duplicadas no pacote: %s
 Local duplicado %s em %%lang(%s)
 Classificador de ficheiro vazio
 Indique a palavra-chave:  Erro ao executar o scriptlet %s (%s)
 Erro ao analisar %%setup: %s
 Erro ao analisar %s: %s
 Erro ao analisar o campo: %s
 Erro na leitura dos arquivos %% arquivo %s: %m
 A execução de %s falhou (%s): %s
 A Executar "%s":
 A executar (%s): %s
 A execução do "%s" falhou.
 Falha na construção de dependências:
 Falha em determinar o nome da directiva: %s
 Falha na codificação dos ficheiros directivos: %s
 Não consegui encontrar o %s:
 Falha em obter as políticas do cabeçalho
 Não consegui abrir o 'pipe' para o tar: %m
 Falha ao ler os ficheiros de directivos: %s
 Falha na leitura do ficheiro spec de %s
 Falha ao renomear %s para %s: %m
 O ficheiro %s parece não ser do tipo specfile.
 O ficheiro %s não é um ficheiro normal.
 O ficheiro %s tem menos de %u bytes
 Ficheiro %s: %s
 Apoio a capacidade do arquivo não construído em
 Ficheiro listado duas vezes: %s
 O ficheiro tem de começar por "/": %s
 O ficheiro precisa de começar por "/": %s
 Ficheiro não encontrado pelo glob: %s
 Ficheiro não encontrado: %s
 Procurando  %s: %s
 Tamanho do cabeçalho demasiado grande Ignorando expressão regular inválida %s
 Linha de dados incompleta em %s:%d
 Linha por omissão incompleta no %s:%d
 Opções de Instalação/Actualização/Remoção: Arquivos instalados (mas não desempacotados) foram encontrados:
%s A instalar %s
 Erro interno Erro interno: Opção esquisita %d
 Elemento %s inválido: %s
 Capacidade inválida: %s
 Data inválida %u Número patch inválido %s: %s
 A macro %%%s não conseguiu ser expandida
 A macro %%%s tem o conteúdo em branco
 A macro %%%s tem um nome inválido (%%define)
 A macro %%%s tem um nome ilegal (%%undefine)
 A macro %%%s tem o conteúdo incompleto
 A macro %%%s tem as opções incompletas
 Falta um %s em %s %s
 Falta um '(' em %s %s
 Falta um ')' em %s(%s
 Procurandoo caminho de módulo na linha: %s
 NÃO  NÃO-OK Sem tag "Source:" no arquivo spec
 Não foram encontradas arquitecturas compatíveis para as quais criar
 Sem atributos de ficheiro configurados
 Sem número patch %u
 Sem número fonte %u
 Carácter sem ser espaço a seguir a %s(): %s
 OK O SO está excluído: %s
 O SO não está incluído: %s
 O pacote já existe: %s
 Verificação do pacote "%s" falhou.
 O pacote não tem uma %%description: %s
 A dica de senha falhou ou a chave gpk expirou
 A palavra-chave está correcta.
 Por favor contacte o %s
 Sobreposição de tipos com módulos directivos '%s' duplicados
 A preparar... Processando ficheiros: %s
 Directivas de processamento: %s
 Opções de pesquisa (com o -q ou o --query): Consultar/Verificar opções de seleção de pacotes: Versão RPM %s
 Reconhecimento de arquivo "%s" falhou: modo %06o %s
 A obter o %s
 Opções de assinatura: Opções Spec A 'symlink' aponta para a BuildRoot: %s -> %s
 Este programa pode ser redistribuido livremente sob os termos da licença GNU GPL
 Demasiados argumentos na linha no %s:%d
 Demasiados argumentos na linha por omissão no %s:%d
 Demasiados argumentos na linha: %s
 Não consegui criar região imutável do cabeçalho.
 Impossível de abrir %s: %s
 Não consegui abrir o ícone %s: %s
 Impossível abrir o ficheiro spec %s: %s
 Incapaz de abrir o stream: %s
 Incapaz de abrir o arquivo temporário: %s
 Não consegui ler o ícone %s: %s
 Não consegui reler o cabeçalho do assinatura.
 Não consegui gravar o pacote: %s
 Não consegui gravar o cabeçalho temporário
 Algoritmo de arquivo desconhecido %u, voltando para MD5
 Tipo de ficheiro desconhecido Tipo de ícone desconhecido: %s
 opção desconhecida %c em %s(%s)
 Compressão de carga desconhecida: %s
 Sistema desconhecido: %s
 Carga não suportada (%s) no pacote %s
 %c não terminado: %s
 Comprimento local incomum: "%s" em %%lang(%s)
 Opções de verificação (com o -V ou o --verify): Versão necessária Nome da versão não permitido Gravei: %s
 SIM Precisa definir o "%%_gpg_name" no seu ficheiro de macros
 ] esperado no fim do vector o argumento não é um pacote RPM
 argumentos para --prefix têm de começar com / os argumentos do --root (-r) têm de começar por / data inválida no %%changelog: %s
 mau formato má opção '%s' em %s:%d
 criar o pacote binário a partir do <pacote de código> criar só o pacote binário a partir do <fichspec> criar só o pacote binário a partir do <fichtar> criar os pacotes binários e de código a partir do <fichspec> criar os pacotes binários e de código a partir do <fichtar> criar só o pacote com código-fonte a partir do <fichspec> criar só o pacote com código-fonte a partir do <fichtar> criar através do %build (%prep e depois compilar) do <fichspec> criar através do %build (%prep e depois compilar) do <fichtar> criar através do %install (%prep, %build e depois instalar) do <pacote de código> criar através do %install (%prep, %build e depois instalar) do <fichspec> criar através do %install (%prep, %build e depois instalar) do <fichtar> criar através do %prep (desempacotar o código e aplicar patches) do <fichspec> criar através do %prep (desempacotar o código e aplicar patches) do <fichtar> O buildroot já foi especificado, a ignorar o %s
 não consigo adicionar o registo originalmente em %u
 não consigo criar o %s: %s
 não consigo trancar o %s no %s/%s
 não foi possível abrir %s: %s
 não consigo abrir a base de dados Packages em %s
 não consigo aceder de novo ao conteúdo: %s
 não é possível usar --prefix com --relocate ou --excludepath a criação do pacote falhou no ficheiro %s: %s
 a criação do arquivo falhou: %s
 A criar uma conexão para --conexão falhou: %m
 depurar máquina de estados de ficheiros depurar a E/S da rpmio Apgafar assinaturas de pacotes diretório mostra a configuração final do rpmrc e das macros mostrar as opções de pesquisa conhecidas mostrar os estados dos ficheiros listados não aceitar as mensagens de i18N do ficheiro spec não executar o script %%post (se existir) não executar o script %%postun (se existir) não executar o script %%pre (se existir) não executar o script %%preun (se existir) não execute a %check fase de verificação da compilação não execute a %clean fase de limpeza da compilação não executar nenhum dos scripts %%triggerin não executar nenhum dos scripts %%triggerpostun não executar nenhum dos scripts %%triggerprein não executar nenhum dos scripts %%triggerun não executar nenhum dos scripts activados por este pacote não executar nenhuma etapa da criação não executar nenhuns scripts do pacote não instalar a documentação não reorganiza a instalação dos pacotes para satisfazer as dependências não verificar as dependências de compilação não verificar as dependências do pacote não verificar o espaço no disco antes da instalação não importe, mas diga se irá funcionar ou não não instalar, mas mencionar se funcionaria ou não não verificar os ficheiros no pacote não verificar o grupo dos ficheiros não verificar o modo dos ficheiros não verificar hora de modificação dos ficheiros não verificar o proprietário dos ficheiros não verificar a arquitectura do pacote não verificar as dependências do pacote não verificar o sistema operativo do pacote não verificar o tamanho dos ficheiros não verificar as ligações simbólicas dos ficheiros apresentar a informação básica do ficheiro formato da opção em branco nome da opção em branco apagar (desinstalar) o pacote erro na leitura do ficheiro %s
 erro ao ler o cabeçalho do pacote
 erro(%d) ao criar uma nova instância do pacote
 erro(%d) ao guardar o registo #%d em %s
 erro:  as directorias de exclusão têm de começar por / exclusivo o exec falhou
 falhou a reconstrução da base de dados: a base de dados original mantém-se
 falha ao remover o directório %s: %s
 falha na substituição da base de dados antiga com a nova base de dados!
 falha ao efectuar stat de %s: %m
 falhou ao escrever todas as datas para %s: %s
 erro fatal:  ficheiro o ficheiro %s está em conflito com as tentativas de instalação do %s e %s o ficheiro %s da instalação do %s está em conflito com o ficheiro do pacote %s o ficheiro %s não pertence a nenhum pacote
 ficheiro %s: %s
 os ficheiros só podem ser mudados de sítio durante a instalação do pacote gerar cabeçalho(s) dos pacotes compatíveis com (legacy) o pacote rpm v3 o gpg não conseguiu gravar a assinatura
 o grupo %s não contém nenhum pacote
 o grupo %s não existe - a usar o root
 ignorar as directivas ExcludeArch: do ficheiro spec importe a chave pública blindada entrada de %%changelog incompleta
 formato incorrecto: %s
 inicializar a base de dados instale todos os ficheiros, até configurações que se não seriam saltados instalar documentação dependência inválida número do pacote inválido: %s
 linha %d: %s
 linha %d: %s é obsoleta: %s
 linha %d: %s: %s
 linha %d: Opção inválida do %%setup %s: %s
 linha %d: Número %s inválido: %s
 linha %d: Qualificadores %s: inválidos: %s
 linha %d: Formato da BuildArchitecture inválido: %s
 linha %d: Argumento inválido para %%setup: %s
 linha %d: número no%s errado: %u
 linha %d: Número inválido: %s
 linha %d: Opção inválida %s: %s
 linha %d: A docdir tem de começar por '/': %s
 linha %d: Opção em branco: %s
 linha %d: Campo Época deve ter um número sem sinal: %s
 linha %d: Erro ao analisar a %%description: %s
 linha %d: Erro ao analisar o %%files: %s
 linha %d: Erro ao analisar as %%políticas: %s
 linha %d: Erro ao analisar %s: %s
 linha %d: caractere ilegal '%c' em: %s
 linha %d: caractere ilegal em: %s
 linha %d: sequência ilegal ".." em: %s
 Linha %d: Opção inválida: %s
 linha %d: Apenas subpacotes noarch são suportados: %s
 linha %d: O pacote não existe: %s
 linha %d: Os prefixos não podem acabar em "/": %s
 linha %d: Segundo %s
 linha %d: Opção só recebe um parâmetro: %s
 linha %d: Demasiados nomes: %s
 linha %d: Opção desconhecida: %s
 linha %d: o script interno deve terminar com '>': %s
 linha %d: não é permitido interpretar argumentos nos gatilhos: %s
 linha %d: o programa de 'script' tem de começar por '/': %s
 linha %d: segundo %%prep
 linha %d: segundo %s
 linha %d: os 'triggers' (que activam) têm de ter --: %s
 linha %d: macro não fechado ou má continuação de linha
 linha %d: script interno não suportado: %s
 linha: %s
 listar todos os ficheiros de configuração listar todos os ficheiros de documentação listar os ficheiros no pacote listar as chaves de RPM malformado %s: %s
 a alocação de memória (%u bytes) devolveu NULL.
 em falta falta %c %s falta um ':' (encontrado um 0x%02x) em %s:%d
 falta a arquitectura para o %s em %s:%d
 falta o nome da arquitectura em %s:%d
 faltam argumentos para o %s em %s:%d
 falta o nome no %%changelog
 falta o segundo ':' em %s:%d
 falta um { depois do % falta um } depois do %{ partilhado na rede partilhado     nenhum argumento enviado Sem argumentos dados para análise. não foram indicados argumentos para a pesquisa nenhuns argumentos fornecidos para a verificação não foi definido o dbpath não foi definido o dbpath
 falta a descrição no %%changelog
 nenhum pacote coincide com %s: %s
 nenhum pacote oferece o %s
 nenhum pacote precisa do %s
 nenhum pacote activa o %s
 não foram indicados pacotes para apagar nenhuns pacotes fornecidos para instalar normal normal         não instalado  só pode ser realizado um tipo de pesquisa/verificação de cada vez apenas instalação e atualização poderão ser forçadas só pode ser especificado um 'major mode' somente uma das --excludedocs e --includedocs podem ser especificadas o acesso ao %s falhou: %s
 operar rpms binários gerados por spec (padrão) operar rpm em fonte gerado por spec ignorar a raiz de criação ignorar a plataforma-alvo pacote %s (que é mais recente do que %s) se encontra instalado o pacote %s já está instalado o pacote %s é para um sistema operativo %s pacote %s não se encontra instalado
 o pacote %s já está instalado, substituir com %s
 o pacote %s já está instalado, a ignorar %s
 o pacote nem tem um dono do ficheiro ou as listas de IDs
 erro de análise na expressão
 mostra cardinais enquanto o pacote instala (conveniente com o -v) mostra percentagens enquanto o pacote instala imprime a versão de rpm em uso devolver um resultado menos detalhado devolver um resultado mais detalhado a pesquisa do ficheiro spec %s falhou, não consigo analisar
 pesquisar o(s) pacote(s) activados pelo pacote pesquisar/verificar todos os pacotes pesquisar/verificar o(s) pacote(s) de transacção de instalação pesquisar/verificar o(s) pacote(s) no grupo pesquisar/verificar o(s) pacote(s) que contém(êm) o ficheiro pesquisar/verificar o(s) pacote(s) com o identificador do cabeçalho pesquisar/verificar o(s) pacote(s) com o identificador do pacote pesquisar/verificar o(s) pacote(s) que oferecem uma dependência pesquisar/verificar o(s) pacote(s) que precisa duma dependência falha na leitura: %s (%d)
 lendo symlink %s falhou: %s
 reconstruir as listas invertidas da base dados com os cabeçalhos dos pacotes instalados o registo %u não pôde ser lido
 re-instalar se o pacote já estiver presente muda os ficheiros de <velho> para <novo> muda o pacote para <dir>, se for possível os novos locais têm de começar por / os novos locais têm de conter um = os novos locais têm de ter um / a seguir ao = remover todos os pacotes que coincidem com <pacote> (normalmente é gerado um erro se <pacote> especificar pacotes múltiplos) apagar as directorias de criação quando acabar remover fontes quando finalizado apagar o ficheiro spec quando acabar substituir os ficheiros em %s por ficheiros de %s a recuperar substituído substituído       modo de pesquisa do rpm modo de verificação do rpm a desactivação de 'scripts' só pode ser indicado durante a instalação ou a remoção de pacotes partilhado abreviatura para --replacepkgs --replacefiles assinar pacote(s) assinar pacote(s) (identical to --addsign) ignorar ficheiros %%ghost ignorar os ficheiros com a componente inicial <dir>  saltar directamente para a etapa indicada (só para c,i) o pacote de código-fonte não contem um ficheiro .spec
 esperava-se um pacote com código-fonte, foi encontrado um pacote binário
 erro de sintaxe na expressão
 erro de sintaxe ao analisar o &&
 erro de sintaxe ao analisar o ==
 erro de sintaxe ao analisar o ||
 a desactivação dos 'triggers' só pode ser usado durante a instalação ou remoção de pacotes os tipos têm de coincidir
 incapaz de ler a assinatura
 ] inesperado opções de pesquisa inesperadas formato de pesquisa inesperado origem de pesquisa inesperada } inesperado encontrado o erro desconhecido %d ao manipular o pacote %s opção desconhecida ( não correspondido
 a abertura do pacote falhou%s%s: %s
 opção do db desconhecida: "%s" ignorada.
 actualizar a base de dados, mas não alterar o sistema de ficheiros actualizar pacote(s) actualizar pacote(s) se já estiverem instalado(s) actualizar para uma versão antiga do pacote (--force efectua isto automaticamente) usar o formato de pesquisa seguinte o utilizador %s não existe - a usar o root
 verificar a secção %files do <fichspec> verificar a secção %files do <fichtar> verificar ficheiros da base de dados verifique a(s) assinatura(s) do(s) pacote(s) aviso:  cor errada { esperado depois de : na expressão { esperado a seguir ao ? na expressão | esperado no fim da expressão } esperado na expressão 