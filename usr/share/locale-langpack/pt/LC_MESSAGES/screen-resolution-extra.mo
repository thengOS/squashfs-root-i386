��          |      �          &   !  '   H  A   p  >   �  >   �  +   0  9   \     �  6   �  �   �  v   �  �  D  2   �  3   -  5   a  H   �  Q   �  -   2  8   `  &   �  Q   �  �     �   	                   
                                    	    Change Screen Resolution Configuration Change the effect of Ctrl+Alt+Backspace Changing the Screen Resolution configuration requires privileges. Changing the effect of Ctrl+Alt+Backspace requires privileges. Could not connect to Monitor Resolution Settings DBUS service. Enable or disable the NVIDIA GPU with PRIME Enabling or disabling the NVIDIA GPU requires privileges. Monitor Resolution Settings Monitor Resolution Settings can't apply your settings. Monitor Resolution Settings has detected that the virtual resolution must be set in your configuration file in order to apply your settings.

Would you like Screen Resolution to set the virtual resolution for you? (Recommended) Please log out and log back in again.  You will then be able to use Monitor Resolution Settings to setup your monitors Project-Id-Version: screen-resolution-extra
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-02-18 12:57+0000
PO-Revision-Date: 2015-06-17 09:11+0000
Last-Translator: Ivo Xavier <ivoxavier.8@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:34+0000
X-Generator: Launchpad (build 18115)
 Alterar a configuração da resolução do monitor Alterar o efeito da combinação Ctrl+Alt+Backspace Alterar a resolução do monitor requer privilégios. Alterar o efeito da combinação Ctrl+Alt+Backspace requer privilégios. Não foi possível ligar ao serviço DBUS definições de resolução do monitor. Ative ou desative o GPU da NVIDIA com o PRIME Ativar ou desativar o GPU da NVIDIA requer privilégios. Definições de resolução do monitor As definições de resolução do monitor não podem aplicar as suas definições As definições de resolução do monitor detetou que a resolução virtual deve ser definida no seu ficheiro de configuração de maneira a aplicar as suas definições.

Quer que seja definida a resolução virtual por si? (Recomendado) Por favor, termine sessão e entre novamente. Poderá assim utilizar a sua resolução do monitor para configurar os seus monitores 