��    2      �  C   <      H  "   I  -   l     �     �     �     �     �     �  ,        E  ,   c  '   �  -   �      �  (     (   0     Y     y     �     �     �  	   �     �     �  !   �     �          !     ?     P  2   _     �     �     �  )   �     �          .     6     <     M     `     e     v     y     �     �     �     �  �  �  )   k
  6   �
     �
     �
           $     8     R  /   q     �  /   �  )   �  0     "   L  *   o  *   �  $   �  $   �  
             '  	   /     9     A  *   Y     �     �  0   �     �       J        h     �     �  .   �     �  "   �     !     )     /     B     W     ]     n  #   s     �     �     �     �        /       !                                   '         &         #          +         (   	   
               ,       *                "                 %      )   0       2                                 1         -      $   .       %s is probably not a shell archive %s looks like raw C code, not a shell archive %s: No `end' line %s: Not a regular file %s: Short file %s: Write error %s: illegal option -- %c
 %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous
 %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 (binary) (compressed) (empty) (gzipped) (text) Cannot access %s Cannot get current directory name Cannot open file %s Created %d files
 Found no shell commands in %s MD5 check failed No input files PLEASE avoid -X shars on Usenet or public networks Please unpack part 1 first! Saving %s (%s) Starting file %s
 Too many directories for mkdir generation Unknown system error You have unpacked the last part archive empty exit immediately extraction aborted help memory exhausted no printf formatting error:  %s
 quit standard input text yes Project-Id-Version: GNU sharutils
Report-Msgid-Bugs-To: bug-gnu-utils@gnu.org
POT-Creation-Date: 2015-05-30 08:36-0700
PO-Revision-Date: 2014-04-24 13:32+0000
Last-Translator: IvoGuerreiro <ivoguerreiro@gmail.com>
Language-Team: Portugese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:17+0000
X-Generator: Launchpad (build 18115)
Language: pt
 %s provavelmente nao  um arquivo de shell %s parece ser cdigo C em bruto, no um arquivo de shell %s: Falta a linha `end' %s: Não é um ficheiro normal %s: Ficheiro de dimenso reduzida %s: Erro de escrita %s: opção ilegal -- %c
 %s: opção inválida -- '%c'
 %s: a opção '%c%s' não permite um argumento
 %s: a opção '%s' é ambígua
 %s: a opção '--%s' não permite um argumento
 %s: a opção '--%s' requer um argumento
 %s: a opção '-W %s' não permite um argumento
 %s: a opção '-W %s' é ambígua
 %s: a opção '-W %s' requer um argumento
 %s: a opção requer um argumento -- '%c'
 %s: opção '%c%s' não reconhecida
 %s: opção '--%s' não reconhecida
 (binário) (comprimido) (vazio) (gzipado) (texto) No  possvel aceder a %s Incapaz de obter nome do directório atual No  possvel abrir o ficheiro %s %d ficheiros criados
 No foi encontrado nenhum comando de shell, em %s Verificação MD5 falhou Sem ficheiros de entrada Por favor, evite -X em arquivos de shell com grande difuso (redes pblicas) Desempacote primeiro a parte1! Gravando %s (%s) Arrancando o ficheiro %s
 Demasiados directórios para a geração mkdir Erro desconhecido de sistema Você desempacotou a última parte arquivo vazio sair imediatamente extracção abortada ajuda memória exausta não Erro de formatação do printf: %s
 sair entrada standard texto sim 