��          �   %   �      `  $   a  $   �  ,   �  E   �  9     /   X  !   �  '   �     �     �  &     +   (  0   T  #   �     �  2   �  -   �  /   "  H   R  "   �  6   �  /   �  5   %  2   [  &   �     �  �  �  "   {  +   �  2   �  N   �  H   L	  6   �	  (   �	  /   �	     %
     9
  (   Y
  7   �
  -   �
  %   �
       @   )  5   j  =   �  K   �  )   *  ;   T  /   �  =   �  8   �  -   7  #   e                
                                                          	                                                  #ARCCONT attribute already specified %1 already used as a substitute name ArcDataF notation %1 not defined in meta-DTD architectural content specified with #ARCCONT not allowed by meta-DTD document element must be instance of %1 element type form element %1 invalid in meta-DTD because excluded element %1 unfinished in meta-DTD element type %1 not defined in meta-DTD invalid digit %1 invalid value %1 for #ARCCONT invalid value %1 for ArcSupr attribute length of value %1 for quantity is too long meta-DTD does not allow element %1 at this point meta-DTD entity %1 must be external missing substitute name no ArcDTD architecture support attribute specified no declaration for meta-DTD general entity %1 no declaration for meta-DTD parameter entity %1 no system identifier could be generated for meta-DTD for architecture %1 no value specified for quantity %1 only value of nArcIndr for ArcIndr attribute supported reference in architecture to non-existent ID %1 substitute for non-existent architecture attribute %1 substitute name %1 is not the name of an attribute substitute name for %1 already defined unrecognized quantity name %1 Project-Id-Version: opensp
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2005-11-05 09:50+0000
PO-Revision-Date: 2014-04-24 13:28+0000
Last-Translator: IvoGuerreiro <ivoguerreiro@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:03+0000
X-Generator: Launchpad (build 18115)
 atributo #ARCCONT já especificado %1 já em utilização para nome substituto notação %1 ArcDataF não defenidas para meta-DTD conteúdo arquitetônico especificado com #ARCCONT não permitido por meta-DTD elemento do documento deve ser instância %1formulário tipo de elemento elemento %1 inválido para meta DTD devido a exclusão elemento %1 não terminado para meta-DTD tipo de elemento %1 não defenido para meta-DTD digíto invalido %1 Valor %1 invalido para #ARCCONT Valor inválido %1 para atributo ArcSupr comprimento do valor %1 demasiado longo para quantidade meta-DTD não permite elemento %1 neste ponto entidade %1 meta-DTD deve ser externa falta o nome do subtítulo sem suporte do atributo de arquitectura especificado para ArcDTD Sem declaração para entidade geral %1 para meta-DTD sem declaração para entidade de parâmetro %1 para meta-DTD nenhum identificador de sistema gerado para meta-DTD para a arquitectura %1 sem valor especificado para quantidade %1 único valor de nArcindr para atributo de ArcIndr suportado referência não existente na arquitetura ID %1 substituição para atributo de arquitetura %1 não existente nome do subtítulo para %1 não é o nome de um atributo nome do subtítulo para %1 já está definido quantidade não reconhecida nome %1 