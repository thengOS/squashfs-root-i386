��    @        Y         �  "   �     �  <   �  >     $   @  2   e  B   �  2   �  >     G   M  =   �  :   �          (  $   <  Z   a     �     �  )   �     !	  w   @	     �	     �	  *   �	     
  #   7
  !   [
  "   }
      �
     �
     �
  )   �
  &     $   D  !   i     �  C   �  *   �           1  /   J  -   z     �  ;   �     �       !     $   @     e     |      �     �     �     �     �          2  .   P  -        �  9   �          #  �  C       &   -  P   T  \   �  8     F   ;  X   �  K   �  <   '  o   d  V   �  M   +  '   y  !   �  8   �  v   �  %   s  (   �  <   �  )   �  �   )  #   �     �  .   �  !   )  /   K  %   {  &   �  $   �  !   �       A   ,  <   n  )   �  !   �     �  S     :   c  $   �     �  2   �  4        K  G   ^     �     �  (   �  4   �     ,  "   B  '   e      �     �     �  %   �  *   �  +   %  2   Q  0   �  #   �  E   �  *     )   J     1   &   $                     6          ,   #   5             3            @   ;             -   8   <       2       :      7             ?      /                (       4   "      =                         .   !      *   +                    %               
          9   	                     '           >   )   0       !
Ensure that it has been loaded.
 %s: ASSERT: Invalid option: %d
 %s: Could not allocate memory for subdomainbase mount point
 %s: Errors found in combining rules postprocessing. Aborting.
 %s: Errors found in file. Aborting.
 %s: Failed to compile regex '%s' [original: '%s']
 %s: Failed to compile regex '%s' [original: '%s'] - malloc failed
 %s: Illegal open {, nesting groupings not allowed
 %s: Internal buffer overflow detected, %d characters exceeded
 %s: Regex grouping error: Invalid close }, no matching open { detected
 %s: Regex grouping error: Invalid number of items between {}
 %s: Sorry. You need root privileges to run this program.

 %s: Unable to add "%s".   %s: Unable to find  %s: Unable to parse input line '%s'
 %s: Unable to query modules - '%s'
Either modules are disabled or your kernel is too old.
 %s: Unable to remove "%s".   %s: Unable to replace "%s".   %s: Unable to write entire profile entry
 %s: Unable to write to stdout
 %s: Warning! You've set this program setuid root.
Anybody who can run this program can update your AppArmor profiles.

 %s: error near                %s: error reason: '%s'
 (ip_mode) Found unexpected character: '%s' Addition succeeded for "%s".
 AppArmor parser error, line %d: %s
 Assert: 'hat rule' returned NULL. Assert: `addresses' returned NULL. Assert: `netrule' returned NULL. Assert: `rule' returned NULL. Bad write position
 Couldn't copy profile Bad memory address
 Couldn't merge entries. Out of Memory
 ERROR in profile %s, failed to load
 Error: Could not allocate memory
 Error: Out of Memory
 Exec qualifier 'i' invalid, conflicting qualifier already specified Exec qualifier 'i' must be followed by 'x' Found unexpected character: '%s' Memory allocation error. Network entries can only have one FROM address. Network entries can only have one TO address. Out of memory
 PANIC bad increment buffer %p pos %p ext %p size %d res %p
 Permission denied
 Profile already exists
 Profile does not match signature
 Profile doesn't conform to protocol
 Profile doesn't exist
 Removal succeeded for "%s".
 Replacement succeeded for "%s".
 Unable to open %s - %s
 Unknown error
 Warning (line %d):  `%s' is not a valid ip address. `%s' is not a valid netmask. `/%d' is not a valid netmask. md5 signature given without execute privilege. missing an end of line character? (entry: %s) ports must be between %d and %d profile %s: has merged rule %s with multiple x modifiers
 unable to create work area
 unable to serialize profile %s
 Project-Id-Version: apparmor-parser
Report-Msgid-Bugs-To: <apparmor@lists.ubuntu.com>
POT-Creation-Date: 2005-03-31 13:39-0800
PO-Revision-Date: 2016-04-13 10:49+0000
Last-Translator: Ivo Xavier <ivoxavier.8@gmail.com>
Language-Team: Portuguese <opensuse-pt@opensuse.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:13+0000
X-Generator: Launchpad (build 18115)
Language: pt
 !
Garanta que foi carregado.
 %s: ASSERÇÃO: Opção inválida: %d
 %s: Não é possível alocar memória para o ponto de montagem de subdomainbase
 %s: Foram encontrados erros no pós-processamento de combinação de regras. A interromper.
 %s: Foram encontrados erros no ficheiro. A interromper.
 %s: Falha na compilação da expressão regular '%s' [original: '%s']
 %s: Falha na compilação da expressão regular '%s' [original: '%s'] - falha no malloc
 %s: { de abertura ilegal, o encadeamento de agrupamentos não é permitido
 %s: Detectado um excesso no buffer interno de %d caracteres
 %s: Erro de agrupamento de expressões regulares: Fecho inválido }, não foi encontrado uma } para emparelhar
 %s: Erro de agrupamento de expressões regulares: Número inválido de itens entre {}
 %s: Lamento. Necessita de privilégios de root para executar este programa.

 %s: Não é possível adicionar "%s".   %s: Não foi possível encontrar  %s: Não foi possível analisar a linha de entrada '%s'
 %s: Não é possível interrogar módulos - '%s'
Os módulos estão desactivados ou o seu kernel é demasiado antigo.
 %s: Não é possível remover "%s".   %s: Não é possível substituir "%s".   %s: Não é possível escrever a entrada completa de perfil
 %s: Não é possível escrever em stdout
 %s: Aviso! Definiu este programa como setuid root.
Qualquer utilizador que possa executar este programa pode actualizar os seus perfis AppArmor.

 %s: erro próximo de                %s: razão do erro: '%s'
 (ip_mode) Encontrado caracter inesperado: '%s' Adição bem sucedida para "%s".
 Erro de interpretação AppArmor, linha %d: %s
 Asserção: 'hat rule' devolveu NULL. Asserção: `addresses' devolveu NULL. Asserção: `netrule' devolveu NULL. Asserção: `rule' devolveu NULL. Posição de escrita errada
 Não foi possível copiar o perfil. Endereço de memória errado
 Não é possível intercalar as entradas. Memória Esgotada
 ERRO no perfil %s, falhou o carregamento
 Erro: Não pôde alocar memória
 Erro: Fora da Memória
 Qualificador de execução 'i' inválido, qualificador em conflito já especificado Qualificador de execução 'i' deve ser seguido por um 'x' Encontrado caracter inesperado: '%s' Erro de alocação de memória. Entradas de rede apenas podem ter um endereço DE. Entradas de rede apenas podem ter um endereço PARA. Memória esgotada
 PÂNICO buffer de incremento errado %p pos %p ext %p tamanho %d res %p
 Permissão negada
 O perfil já existe
 O perfil não coincide com a assinatura
 O perfil não está em conformidade com o protocolo
 O perfil não existe
 Remoção bem sucedida para "%s".
 Substituição bem sucedida para "%s".
 Não é possível abrir %s - %s
 Erro desconhecido
 Aviso (linha %d):  `%s' não é um endereço ip válido. `%s' não é uma máscara de rede válida. `/%d' não é uma máscara de rede válida. assinatura md5 dada sem privilégio de execução. caracter de fim de linha em falta? (entrada: %s) os portos devem estar entre %d e %d o perfil %s: tem a regra de fusão %s com múltiplos modificadores x
 não é possível criar área de trabalho
 não é possível serializar o perfil %s
 