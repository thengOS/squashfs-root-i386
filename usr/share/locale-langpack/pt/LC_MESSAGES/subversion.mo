��    �      \    �      �     �     �                *  '   K  (   s  *   �  %   �  +   �  $        >  	   J     T  )   Z  	   �  
   �  
   �     �     �     �     �     �     �        #   4     X     v     �     �     �     �     �  a   �  U   R  $   �  !   �  7   �     '     <  n   I  =   �  '   �  b     e   �  +   �  7        K     `     f     w     �      �     �     �               /     <     I     Y  !   i  .   �     �  ,   �           "     3  	   <     F  -   `     �     �     �     �     �  �   �  
   �     �     �  	   �  4   �  '     �   5      �  *   �       &   .     U  (   l     �  &   �     �     �  W        \     |     �  >   �  !   �               $     6  S   I  M   �     �  =   �  ?   ;  "   {  &   �  $   �  4   �           :   $   W      |      �      �   A   �   @   �      8!  "   D!     g!  2   �!  
   �!     �!     �!     �!     �!     "  "   4"  ,   W"  "   �"  (   �"  *   �"     �"     #     #     7#  2   N#  $   �#  &   �#     �#  
   �#  *   �#      $  2   >$  ,   q$      �$     �$  ,   �$     �$  )   %     @%  0   X%     �%     �%     �%  )   �%     &  $   &     @&     W&     n&     �&     �&  ,   �&  !   �&     '      '  ]   ('  �   �'  S   ;(  T   �(  f   �(  P   K)  4   �)  n   �)  F   @*  5   �*  W   �*  �   +  \   �+  �   ,  `   �,  i   	-  6   s-  �   �-  =   ].  H   �.     �.     �.     /     0/     L/  �   h/     Z0  `   o0  �  �0  %   �2     �2     �2     3     3  .   03  4   _3  8   �3  5   �3  5   4  1   94     k4  
   w4  
   �4  ,   �4     �4     �4     �4     �4     5      5     95      N5  %   o5  '   �5  .   �5  )   �5     6     /6  "   B6     e6  	   q6  #   {6  a   �6  j   7  $   l7  &   �7  :   �7     �7     8     8  K   �8  4   �8  k   9  k   �9  3   �9  :   !:     \:     q:     y:  !   �:  *   �:  *   �:  #   ;  !   (;     J;  %   f;     �;     �;     �;     �;  ,   �;  6   <     ><  7   \<  *   �<     �<     �<  	   �<     �<  7   �<     5=     O=     f=     {=     �=  �   �=  	   b>      l>  )   �>     �>  F   �>  6   ?  �   B?  '   �?  1   @     N@  *   l@     �@  ,   �@     �@  0   �@     +A  &   EA  u   lA  -   �A     B     0B  ?   ?B  .   B     �B     �B     �B     �B  [   �B  X   ZC     �C  J   �C  K   D  0   gD  *   �D  )   �D  =   �D     +E  &   HE  .   oE     �E     �E  )   �E  J   �E  R   /F     �F  ,   �F     �F  A   �F     G  	   ,G  $   6G     [G  %   nG  !   �G  -   �G  -   �G  "   H  2   5H  /   hH     �H     �H  "   �H     �H  N   �H  1   KI  )   }I     �I  
   �I  0   �I  &    J  5   'J  /   ]J  !   �J     �J  <   �J     �J  ,   K     CK  ?   _K  #   �K     �K     �K  0    L  !   1L  :   SL  !   �L  %   �L      �L  $   �L  "   M  H   ?M  ,   �M     �M     �M  ]   �M  �   /N  j   �N  Y   cO  �   �O  \   ?P  2   �P  �   �P  K   RQ  I   �Q  f   �Q  �   OR  [   S  �   ]S  t   T  o   �T  =   �T  �   ;U  ?   V  O   TV     �V     �V     �V     �V     W    )W     >X  h   UX     }   =   c   '   �   Y   �                  �      -   >             M   �   &           `   l           �   �   �       �      H   �   7       x   P   �       �       8      O      �   �   �               0   I   i          q       �       R   �   D   p       E   �   T   �      1       �   
               �   4   �       )   e   �   �   (   ]   3   ?   �          !   /   �   �   �       �   2          k   �   ^           h   �           W      �   ,   .         �   �   t      �       *   �   �   �   o       �       w           f   �   �   �   {   �   L   �   �       	   m   �   "       6         �   Q       F   B      $      U   a           �   �   �                   5   ;   ~   �   :   K   �           |   �   �       #   A   @   �   �       9   �      \   �   u               �      �   [   �      �          �   �   �           %   n   y      g          s           J       N   _   z       �       j      r   Z       �   �   �   �   v   S   �   <   �   V   C          d          �       X           �           b   G   +           
Error: no prefixes supplied.
 
Recovery completed.
    %ld => (dropped)
  (from %s:%ld) '%s' does not appear to be a URL '%s' is a URL when it should be a path
 '%s' is a URL, probably should be a path '%s' is not a directory in filesystem '%s' '%s' is not a file in filesystem '%s' 'incremental' option only valid in XML mode 'verbose' option invalid in XML mode (no author) (no date) Added Bad parent pool passed to svn_make_pool() Bogus URL Bogus UUID Bogus date Bogus filename Bogus mime-type Can't open stderr Changed paths:
 Checksum: %s
 Conflict Current Base File: %s
 Conflict Previous Base File: %s
 Conflict Previous Working File: %s
 Conflict Properties File: %s
 Copied From Rev: %ld
 Copied From URL: %s
 Copied: %s (from rev %ld, %s)
 Created: %s
 Deleted Deltifying revision %ld... Describe the usage of this program or its subcommands.
usage: svndumpfilter help [SUBCOMMAND...]
 Destination directory exists; please remove the directory or use --force to overwrite Do not display filtering statistics. Don't filter revision properties. Excluding (and dropping empty revisions for) prefixes:
 Excluding prefixes:
 Expires: %s
 Failed to get exclusive repository access; perhaps another process
such as httpd, svnserve or svn has it open? File already exists: filesystem '%s', revision %ld, path '%s' File not found: revision %ld, path '%s' Filter out nodes with given prefixes from dumpstream.
usage: svndumpfilter exclude PATH_PREFIX...
 Filter out nodes without given prefixes from dumpstream.
usage: svndumpfilter include PATH_PREFIX...
 First revision cannot be higher than second Including (and dropping empty revisions for) prefixes:
 Including prefixes:
 Index Invalid URL '%s' Invalid configuration value Invalid copy source path '%s' Invalid revision number supplied Invalid revision specifier Last Changed Author: %s
 Last Changed Date Last Changed Rev: %ld
 Lock Created Lock Expires Lock Owner: %s
 Lock Token: %s
 Lock comment contains a zero byte Missing propname and repository path arguments Missing propname argument Missing propname or repository path argument Missing repository path argument Missing revision Modified Name: %s
 No such XML tag attribute No valid copyfrom revision in filtered stream Node Kind: directory
 Node Kind: file
 Node Kind: none
 Node Kind: unknown
 Only one revision allowed Output the content of specified files or URLs.
usage: cat TARGET[@REV]...

  If specified, REV determines in which revision the target is first
  looked up.
 Owner: %s
 Path '%s' is not a file Path '%s' isn't locked.
 Path: %s
 Property '%s' not found on path '%s' in revision %ld Property '%s' not found on revision %ld Put files and directories under version control, scheduling
them for addition to repository.  They will be added in next commit.
usage: add PATH...
 REVISION   PATH
--------   ----
 REVISION   PATH <ID>
--------   ---------
 Ran out of unique names Remove revisions emptied by filtering. Removed lock on '%s'.
 Renumber revisions left after filtering. Repository Root: %s
 Repository URL required when importing Repository UUID: %s
 Repository argument required
 Repository lock acquired.
Please wait; recovering the repository may take some time...
 Revision %ld committed as %ld.
 Revision %ld skipped.
 Revision: %ld
 Revisions must not be greater than the youngest revision (%ld) Revisions renumbered as follows:
 Schedule: add
 Schedule: delete
 Schedule: normal
 Schedule: replace
 Subcommand '%s' doesn't accept option '%s'
Type 'svndumpfilter help %s' for usage.
 Subcommand '%s' doesn't accept option '%s'
Type 'svnlook help %s' for usage.
 Text Last Updated The following repository access (RA) modules are available:

 The following repository back-end (FS) modules are available:

 The latest repos revision is %ld.
 This is an empty revision for padding. Too many arguments to import command Transaction '%s' is not based on a revision; how odd Transaction '%s' removed.
 Type '%s --help' for usage.
 Type 'svnversion --help' for usage.
 URL: %s
 UUID Token: %s
 Version file format not correct Waiting on repository lock; perhaps another process has it open?
 When specifying working copy paths, only one target may be given Write error Wrong or unexpected property value XML data was not well-formed You must specify exactly one of -d, -i, -t or -X.
 be verbose daemon mode disable automatic properties display this help display update information do no interactive prompting do not cache authentication tokens do not cross copies while traversing history do not output the trailing newline do not print differences for added files do not print differences for deleted files don't unlock the targets done.
 enable automatic properties force operation to run force read only, overriding repository config file force validity of log message source give output suitable for concatenation ignore externals definitions inetd mode last changed rather than current revisions maximum number of log entries operate on a revision property (use with -r or -t) operate on a revision property (use with -r) operate on single directory only output in XML pass contents of file ARG as additional args print as little as possible print differences against the copy source print extra information read user configuration files from directory ARG relocate via URL-rewriting root of directory to serve show details for copies show full paths instead of indenting them show help on a subcommand show node revision ids for each path specify a password ARG specify a username ARG specify revision number ARG specify transaction name ARG subcommand argument required
 treat value as being in charset encoding ARG try operation but make no changes tunnel mode unknown usage: svnadmin help [SUBCOMMAND...]

Describe the usage of this program or its subcommands.
 usage: svnadmin list-dblogs REPOS_PATH

List all Berkeley DB log files.

WARNING: Modifying or deleting logfiles which are still in use
will cause your repository to be corrupted.
 usage: svnadmin list-unused-dblogs REPOS_PATH

List unused Berkeley DB log files.

 usage: svnadmin lstxns REPOS_PATH

Print the names of all uncommitted transactions.
 usage: svnadmin rmlocks REPOS_PATH LOCKED_PATH...

Unconditionally remove lock from each LOCKED_PATH.
 usage: svnadmin rmtxns REPOS_PATH TXN_NAME...

Delete the named transaction(s).
 usage: svnlook author REPOS_PATH

Print the author.
 usage: svnlook cat REPOS_PATH FILE_PATH

Print the contents of a file.  Leading '/' on FILE_PATH is optional.
 usage: svnlook changed REPOS_PATH

Print the paths that were changed.
 usage: svnlook date REPOS_PATH

Print the datestamp.
 usage: svnlook diff REPOS_PATH

Print GNU-style diffs of changed files and properties.
 usage: svnlook dirs-changed REPOS_PATH

Print the directories that were themselves changed (property edits)
or whose file children were changed.
 usage: svnlook help [SUBCOMMAND...]

Describe the usage of this program or its subcommands.
 usage: svnlook history REPOS_PATH [PATH_IN_REPOS]

Print information about the history of a path in the repository (or
the root directory if no path is supplied).
 usage: svnlook info REPOS_PATH

Print the author, datestamp, log message size, and log message.
 usage: svnlook lock REPOS_PATH PATH_IN_REPOS

If a lock exists on a path in the repository, describe it.
 usage: svnlook log REPOS_PATH

Print the log message.
 usage: svnlook tree REPOS_PATH [PATH_IN_REPOS]

Print the tree, starting at PATH_IN_REPOS (if supplied, at the root
of the tree otherwise), optionally showing node revision ids.
 usage: svnlook uuid REPOS_PATH

Print the repository's UUID.
 usage: svnlook youngest REPOS_PATH

Print the youngest revision number.
 use ARG as diff command use ARG as external editor use ARG as merge command use ARG as the newer target use ARG as the older target use a different EOL marker than the standard
                             system marker for files with the svn:eol-style
                             property set to 'native'.
                             ARG may be one of 'LF', 'CR', 'CRLF' use strict semantics wait instead of exit if the repository is in
                             use by another process Project-Id-Version: subversion
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-03-14 07:37+0000
PO-Revision-Date: 2014-07-28 03:18+0000
Last-Translator: Alexandre Fidalgo <alexandremagnos15@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 17:07+0000
X-Generator: Launchpad (build 18115)
 
Erro: nenhum prefixo foi fornecido.
 
Completada recuperação.
    %ld => (removida)
  (de %s:%ld) '%s' não parece ser um URL '%s' é um URL quando deveria ser um caminho\
 '%s' é um URL, provavelmente deveria ser um caminho '%s' não é um directório no sistema de ficheiros '%s' '%s' não é um ficheiro no sistema de ficheiros '%s' A opção 'incremental' é apenas válida no modo XML A opção 'verbose' é apenas válida no modo XML (sem autor) (sem data) Adicionado Pool pai passado para svn_make_pool() errado URL erróneo UUID não reconhecido Data errónea Nome de ficheiro erróneo Tipo mime erróneo Não é possível abrir o stderr Caminhos alterados:
 Soma de Controlo (Checksum): %s
 Ficheiro Base Actual do Conflito: %s
 Ficheiro Base Anterior ao Conflito: %s
 Ficheiro de Trabalho Anterior ao Conflito: %s
 Ficheiro de Propriedades do Conflito: %s
 Cópia da Revisão: %ld
 Cópia do URL: %s
 Copiado: %s (da revisão %ld, %s)
 Criado: %s
 Eliminado A executar delta de revisão %ld... Descreve como usar este programa ou os seus subcomandos.
uso: svndumpfilter help [SUBCOMANDO...]
 Directório destino existente; por favor remova o directório ou use a opção --force para sobrescreve-lo Não mostra estatísticas de filtro. Não filtra propriedades de revisões. A excluir (e a descartar revisões vazias para) prefixos:
 A excluir prefixos:
 Expira: %s
 Falha a requerer acesso exclusivo ao repositório; talvez outro processo
como o 'httpd', 'svnserve' ou o 'svn' o esteja a usar? O ficheira já existe: sistema de ficheiros '%s', revisão %ld, caminho'%s' Ficheiro não encontrado: revision %ld, caminho '%s' Filtra de 'dumpstream' nós com os prefixos fornecidos.
usage: svndumpfilter exclude PREFIXO_DO_CAMINHO...
 Filtra de 'dumpstream' nós sem os prefixos fornecidos.
usage: svndumpfilter include PREFIXO_DO_CAMINHO...
 Primeira revisão não pode ser maior que a segunda A incluir (e a descartar revisões vazias para) prefixos:
 A incluir prefixos:
 Índice URL inválido '%s' Valor de configuração inválido Caminho de origem de cópia inválido '%s' Número de revisão fornecido é inválido Especificador de revisão inválido Autor da Última Alteração: %s
 Data da Última Alteração Revisão da Última Alteração: %ld
 Bloqueio Criado Bloqueio Expirou Dono do Bloqueio: %s
 Sinal de bloqueio: %s
 O comentário do bloqueio contém zero bytes Argumento 'propname' e caminho do repositorio em falta Argumento 'propname' em falta Argumento 'propname' ou caminho do repositorio em falta Argumento caminho do repositório em falta Revisão em falta Alterado Nome: %s
 Atributo XML inexistente Nenhuma revisão válida de origem no 'stream' filtrado Tipo do Nó: directório
 Tipo do Nó: ficheiro
 Tipo do Nó: nenhum
 Tipo do Nó: desconhecido
 Apenas uma revisão permitida Gera saída com o conteúdo do ficheiro ou URL especificado.
uso: cat ALVO[@REV]...

  Se especificado, REV determina em qual revisão o alvo é 
  primeiramente procurado.
 Dono: %s
 Caminho '%s' não é um ficheiro O caminho '%s' não está com bloqueado.
 Caminho: %s
 A propriedade '%s' não foi encontrada no caminho '%s' na revisão %ld A propriedade '%s' não foi encontrada na revisão %ld Coloca ficheiros e diretórios sobre controlo de versão, agendando-os para
serem incluídos no repositório.  Estes serão incluídos no próximo 'commit'.
uso: add CAMINHO...
 REVISÃO    CAMINHO
-------    -------
 REVISÃO    CAMINHO <ID>
-------    ------------
 Esgotaram-se os nomes únicos Remove revisões esvaziadas por filtragem. Removido bloqueio de '%s'.
 Renumera revisões esvaziadas por filtragem. Raiz do repositório: %s
 O URL do repositório é necessário ao importar UUID do repositório: %s
 Necessário argumento do repositório
 Acesso exclusivo ao repositorio adquirido.
Espere por favor; a recuperação de um repositório pode ser demorada...
 Realizado 'commit' da revisão %ld como %ld.
 Revisão %ld não considerada.
 Revisão: %ld
 Revisões não podem ser maiores que a revisão mais nova (%ld) Revisões renumeradas como mostrado a seguir:
 Agendado: adicionar
 Agendado: remover
 Agendado: normal
 Agendado: substituir
 Subcomando '%s' não aceita opção '%s'
Digite 'svndumpfilter help %s' para ajuda de uso.
 Sub-comando '%s' não aceita a opção '%s'
Digite 'svnlook help %s' para ajuda de uso.
 Texto da Última Alteração Estão disponíveis os seguintes módulos de acesso a repositório (RA):

 Os seguintes módulos de acesso ao repositório (FS) estão disponíveis:

 A revisão mais recente do repositório é %ld.
 Esta é uma revisão vazia para preencher. Demasiados argumentos no comando 'import' Transacção '%s' não é baseada numa revisão; que estranho Transacção '%s' removida.
 Digite '%s --help' para ajuda de uso.
 Digite 'svnversion --help' para ajuda de uso.
 URL: %s
 Sinal de UUID: %s
 Formato do ficheiro de versão incorrecto À espera do lock do repositório; talvez outro processo o esteja a usar?
 Ao especificar caminhos de cópia de trabalho, somente um alvo
deve ser fornecido Erro de escrita Valor de propriedade errado ou não esperado XML não estava bem formatado Deve especificar exactamente uma das opções: -d, -i, -t ou -X.
 seja verboso modo demo desabilita propriedades automáticas mostrar esta ajuda mostra informações de atualização não usa processamento interativo não guarda sinais de autenticação em cache não cruzar cópias ao percorrer o histórico não gerar uma nova linha no final não mostra diferenças para ficheiros adicionados não mostra diferenças para ficheiros apagados Não desbloquear os alvos concluido.
 habilita propriedades automáticas força operação a correr forçar apenas leitura, ignorando o ficheiro de configuração do repositório força a validade da fonte da mensagem de registo gerar saída própria para concatenação ignora definições externas modo inetd última alteração ao invés da revisão actual número máximo de entradas de registo opera numa propriedade de revisão (use com -r ou -t) opera numa propriedade de revisão (use com -r) opera apenas sobre um directório saída em XML passa o conteúdo do ficheiro ARG como argumentos adicionais imprime o mínimo possível mostra diferenças contra a cópia de origem mostra informações extras lê arquivos de configuração do utilizador do directório ARG reposiciona via regravação de URL raiz do directório a servir mostra detalhes para as cópias mostra caminhos completos em vez de indentá-los mostra ajuda sobre um sub-comando mostra identificadores de revisões de nó em cada caminho específica uma palavra-chave ARG específica um nome de utilizador ARG específica revisão número ARG específica transacção número ARG requerido argumento de subcomando
 tratar valor como estando na codificação de conjunto de caracteres ARG tenta a operação mas não faz alterações modo do túnel desconhecido uso: svnadmin help [SUBCOMANDO...]

Descreve como usar este programa ou os seus subcomandos.
 uso: svnadmin list-dblogs CAMINHO_REPO

Lista todos os ficheiros de registo da Berkeley DB.

AVISO: Alterar ou apagar ficheiros de registo que ainda estão em uso
deixarão o repositório corrompido.
 uso: svnadmin list-unused-dblogs CAMINHO_REPO

Lista os ficheiros de registo da Berkeley DB não usados.

 uso: svnadmin lstxns CAMINHO_REPO

Mostra os nomes de todas as transacções sem commit.
 utilização: svnadmin rmlocks CAMINHO_REPO CAMINHO_BLOQUEADO...

Remove incondicionalmente o bloqueio de cada CAMINHO_BLOQUEADO
 uso: svnadmin rmtxns CAMINHO_REPO NOME_TRANS...

Apaga a(s) transacção(ões) indicada(s).
 uso: svnlook author CAMINHO_REPO

Mostra o autor.
 uso: svnlook cat CAMINHO_REPO CAMINHO_FICHEIRO

Mostra o conteúdo de um ficheiro.  Iniciar CAMINHO_FICHEIRO com '/' é opcional.
 uso: svnlook changed CAMINHO_REPO

Mostra os caminhos que foram alterados.
 uso: svnlook date CAMINHO_REPO

Mostra o registro de data ('datestamp').
 uso: svnlook diff CAMINHO_REPO

Mostra 'diff' no estilo GNU dos ficheiros e propriedades modificadas.
 uso: svnlook dirs-changed CAMINHO_REPO

Mostra os directórios que foram eles mesmos modificados (edição de
propriedades) ou aqueles cujos ficheiros filhos foram modificados.
 uso: svnlook help [SUBCOMMANDO...]

Descreve o uso deste programa ou dos seus subcomandos.
 uso: svnlook history CAMINHO_REPO [CAMINHO_NO_REPO]

Mostra informações sobre o histórico de um caminho no repositório (ou
o directório raiz se o caminho não for disponibilizado).
 uso: svnlook info CAMINHO_REPO

Mostra o autor, data/hora, tamanho da mensagem de registo,
e a mensagem de registo.
 uso: svnlook bloqueia REPOS_PATH PATH_IN_REPOS

Se existir um bloqueio no caminho no repositório, descreva-o.
 uso: svnlook log CAMINHO_REPO

Mostra a mensagem de registo.
 uso: svnlook tree CAMINHO_REPO [CAMINHO_NO_REPO]

Mostra a árvore, começando em CAMINHO_NO_REPO (se fornecido, 
ou na raiz da árvore caso contrário), opcionalmente mostra os identificadores
de revisão dos nós.
 uso: svnlook uuid CAMINHO_REPO

Mostra o UUID do repositório.
 uso: svnlook youngest CAMINHO_REPO

Mostra o número da revisão mais recente.
 usa ARG como comando 'diff' usa ARG como editor externo usa ARG como comando 'merge' usa ARG como novo alvo usa ARG como alvo antigo usa um marcador de fim de linha (EOL) diferente do padrão
                             marcador do sistema para ficheiros com
                             propriedade svn:eol-style activada para 'native'.
                             ARG pode ser um entre 'LF', 'CR' e 'CRLF' usa semântica estrita aguarde ao invés de sair se o repositório está em
                             uso por outro processo 