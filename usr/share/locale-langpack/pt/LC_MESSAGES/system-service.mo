��          �      �       0  %   1     W     s     �     �  "   �  1   �  3     .   J  7   y  0   �  -   �  �    1   �     �          %     B  (   ]  D   �  J   �  B     =   Y  I   �  H   �                                         	             
    Check if the package system is locked Get current global keyboard Get current global proxy Set current global keyboard Set current global proxy Set current global proxy exception System policy prevents querying keyboard settings System policy prevents querying package system lock System policy prevents querying proxy settings System policy prevents setting global keyboard settings System policy prevents setting no_proxy settings System policy prevents setting proxy settings Project-Id-Version: ubuntu-system-service
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-11-04 00:35+0000
PO-Revision-Date: 2012-10-25 21:16+0000
Last-Translator: Filipe André Pinho <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:35+0000
X-Generator: Launchpad (build 18115)
 Verificar se o sistema de pacotes está bloqueado Obter teclado global atual Obter proxy global atual Definir teclado global atual Definir proxy global atual Definir excepção de proxy global atual A política do sistema impede a consulta das definições de teclado A política do sistema impede a consulta do bloqueio do sistema de pacotes A política do sistema impede a consulta das definições de proxy A política do sistema impede a definição do teclado global A política do sistema impede a configuração das definições sem proxy A política do sistema impede a configuração das definições de proxy 