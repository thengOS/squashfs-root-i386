��          �      l      �  K   �  O   -  2   }  :   �  2   �  8     J   W  )   �     �  1   �          &  :   9  %   t  Q   �     �  4     W   <  !   �  (   �  �  �  S   �  v     B   ~  L   �  C   	  J   R	  c   �	  +   
     -
  D   H
     �
     �
  E   �
  6     a   >  %   �  =   �  c     0   h  5   �                              	                                                
                    Authentication is required to change the passphrase for an encrypted device Authentication is required to lock an encrypted device unlocked by another user Authentication is required to mount the filesystem Authentication is required to mount/unmount the filesystem Authentication is required to set up a loop device Authentication is required to unlock an encrypted device Authentication is required to unmount a filesystem mounted by another user Change passphrase for an encrypted device Delete loop devices Lock an encrypted device unlocked by another user Manage loop devices Mount a filesystem Mount a filesystem from a device plugged into another seat Mount a filesystem on a system device Mount/unmount filesystems defined in the fstab file with the x-udisks-auth option Unlock an encrypted device Unlock an encrypted device plugged into another seat Unlock an encrypted device specified in the crypttab file with the x-udisks-auth option Unlock an encrypted system device Unmount a device mounted by another user Project-Id-Version: udisks
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-04-01 20:16+0000
PO-Revision-Date: 2014-07-26 16:36+0000
Last-Translator: Alexandre Fidalgo <alexandremagnos15@gmail.com>
Language-Team: Portuguese (http://www.transifex.com/projects/p/udisks/language/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:13+0000
X-Generator: Launchpad (build 18115)
Language: pt
 É necessária a autenticação para alterar a senha para um dispositivo encriptado É necessária autenticação para bloquear um dispositivo encriptado que tenha sido desbloqueado por outro utilizador A autenticação é necessária para montar o sistema de ficheiros A autenticação é necessária para montar/desmontar o sistema de ficheiros A autenticação é necessária para configurar um dispositivo loop A autenticação é necessária para desbloquear um dispositivo encriptado A autenticação é necessária para desmontar um sistema de ficheiros montado por outro utilizador Mude a senha para um dispositivo encriptado Eliminar dispositivos loop Bloquear um dispositivo encriptado desbloqueado por outro utilizador Gerir dispositivos loop Montar sistema de ficheiros Montar sistema de ficheiros de um dispositivo inserido em outro banco Montar sistema de ficheiros num dispositivo de sistema Montar/desmontar sistemas de ficheiros definidos no ficheiro fstab com a opção de x-udisks-auth Desbloquear um dispositivo encriptado Desbloquear um dispositivo encriptado inserido em outro banco Desbloquear um dispositivo encriptado especificado no ficheiro crypttab com a opção x-udisks-auth Desbloquear um dispositivo de sistema encriptado Desmontar um dispositivo montado por outro utilizador 