��    -      �  =   �      �     �     �  0   �     /     4     ;     C     R     h          �     �  	   �     �  k   �  a   G  $   �  "   �     �     �                    %     (  m   :  	   �  &   �     �     �     �  E   �     9     J     X     s     �  	   �     �  	   �     �  	   �  k   �  e   E  �  �     W
     h
  !   |
  
   �
     �
  	   �
     �
  !   �
     �
     �
          /     D     M  m   \  c   �      .  )   O     y     �     �     �     �     �     �  q   �     m  &   |  	   �     �     �  e   �     =     W     g      {     �     �     �     �  	   �     �  t     g   y                    *           
                                                               +   %      (             ,      !          "                '             )   	   -      &           #      $             %s (Default) - Unity Greeter Are you sure you want to shut down the computer? Back Cancel Domain: Email address: Enter password for %s Failed to authenticate Failed to start session Goodbye. Would you like to… Guest Session Hibernate High Contrast If you have an account on an RDP or Citrix server, Remote Login lets you run applications from that server. If you have an account on an RDP server, Remote Login lets you run applications from that server. Incorrect e-mail address or password Invalid password, please try again Log In Logging in… Login Login Screen Login as %s OK Onscreen keyboard Other users are currently logged in to this computer, shutting down now will also close these other sessions. Password: Please enter a complete e-mail address Restart Retry Retry as %s Run '%s --help' to see a full list of available command line options. Run in test mode Screen Reader Select desktop environment Server type not supported. Session Options Set Up… Show release version Shut Down Suspend Username: You need an Ubuntu Remote Login account to use this service. Visit uccs.canonical.com to set up an account. You need an Ubuntu Remote Login account to use this service. Would you like to set up an account now? Project-Id-Version: unity-greeter
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-03-24 02:58+0000
PO-Revision-Date: 2016-02-18 09:29+0000
Last-Translator: Ivo Xavier <ivoxavier.8@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:03+0000
X-Generator: Launchpad (build 18115)
 %s (Predefinido) Anfitrião do Unity Quer mesmo desligar o computador? Retroceder Cancelar Domínio: Email: Introduza a palavra-passe para %s A autenticação falhou Falha ao iniciar a sessão Adeus. Gostaria de ... Sessão de convidado Hibernar Alto contraste Se tem uma conta num servidor RDP ou Citrix, o login remoto permite-lhe executar aplicações desse servidor. Se tem uma conta num servidor RDP, o login remoto permite-lhe executar aplicações desse servidor. Palavra-passe ou email incorreto Palavra-passe inválida, tente novamente. Iniciar sessão A iniciar sessão... Iniciar sessão Ecrã de início de sessão Iniciar sessão como %s Aceitar Teclado no ecrã Há outros utilizadores com sessão iniciada neste computador, se o desligar agora encerrará as outras sessões. Palavra-passe: Por favor, introduza um email completo Reiniciar Tentar novamente Tentar novamente como %s Execute '%s --help' para consultar uma lista completa das opções de linha de comandos disponíveis. Executar em modo de teste Leitor de ecrã Selecione o desktop Tipo de servidor não suportado. Opções de sessão Configurar... Mostrar versão de lançamento Desligar Suspender Nome de utilizador: Precisa de uma conta Ubuntu de login remoto para usar este serviço. Visite uccs.canonical.com para criar uma conta. Precisa de uma conta Ubuntu de login remoto para usar este serviço. Gostaria de criar uma conta agora? 