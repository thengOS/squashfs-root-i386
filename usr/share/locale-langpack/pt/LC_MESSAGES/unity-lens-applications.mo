��    8      �  O   �      �     �     �     �               ,     0     9     G  	   T     ^     m  	   u          �     �     �     �     �     �     �     �  	   �     �     �     �  
   �                    $     )     5     C     K     Y     n     �     �     �     �     �     �     �     �            �   =  �   7  �   +	  �   �	     �
  	   �
  
   �
       �       �     �     �     �                         /     ?     K  	   _  
   i     t     {     �     �     �  	   �     �  
   �     �  
   �     �     �     �     �     
                .     4     B  
   V     a     u     �     �     �     �     �  
   �     �       6        D  0   L  �   }  �     �   �  �   A     �     �  
   �     �         8   1             &               $                      *                    5   6   %      4       (   7                   /   
                 '      2           !   	   0   +       #                                   -       3                .      "   )              ,       Accessibility Accessories All Applications All Available Applications Applications Buy Commands Customization Dash plugins Developer Developer Site Disable Education Enable Fonts Free Free Download Games Graphics Hardware requirements History Install Installed Installed on Internet Launch Local apps Media More suggestions Office Paid Recent apps Recently used Results Run a command Running Applications Science & engineering Search applications Search plugins Search running applications Search search plugins Size %s Software center Sources Still no easter egg in Unity System There is no easter egg in Unity This is an Ubuntu search plugin that enables information from available search plugins to be searched and displayed in the Dash underneath the Dash plugins header. If you do not wish to search this content source, you can disable this search plugin. This is an Ubuntu search plugin that enables information from local applications to be searched and displayed in the Dash underneath the Applications header. If you do not wish to search this content source, you can disable this search plugin. This is an Ubuntu search plugin that enables information from local binaries to be searched and displayed in the Dash. If you do not wish to search this content source, you can disable this search plugin. This is an Ubuntu search plugin that enables information from running applications to be searched and displayed in the Dash underneath the Applications header. If you do not wish to search this content source, you can disable this search plugin. Type Uninstall Version %s scope Project-Id-Version: unity-lens-applications
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-17 15:48+0000
PO-Revision-Date: 2015-12-09 22:33+0000
Last-Translator: Ivo Xavier <ivoxavier.8@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:05+0000
X-Generator: Launchpad (build 18115)
 Acessibilidade Acessórios Todas as apps Todas as apps disponíveis Apps Comprar Comandos Personalização Plugins do Dash Programador Site do programador Desativar Educação Ativar Tipos de letra Grátis Grátis Jogos Gráficos Requesitos de hardware Histórico Instalar Instaladas Instalado em Internet Iniciar Apps locais Media Mais sugestões Produtividade Pagas Apps recentes Usadas recentemente Resultados Executar um comando Apps em execução Ciência e engenharia Procurar apps Pocurar puglins Procurar apps em execução Procurar scopes Tamanho %s Centro de Software Fontes Ainda não existem funcionalidades escondidas no Unity Sistema Não existem funcionalidades escondidas no Unity Este é um scope do Ubuntu que procura informações das scopes instaladas para que possam ser mostradas no Dash. Se pretender, pode desligar este scope. Este é um scope do Ubuntu que procura informações em apps locais para que possam ser mostradas no Dash. Se pretender pode desligar este scope. Este é um scope do Ubuntu que procura informações dos binários locais para que possam ser mostradas no Dash. Se pretender pode desligar este scope. Este é um scope do Ubuntu que procura as apps ativas para que possam ser mostradas no Dash. Se pretender, pode desligar este scope. Tipo Desinstalar Versão %s scope 