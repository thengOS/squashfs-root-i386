��    C      4  Y   L      �     �  
   �     �     �     �     �     �     �     �     �     �     �          
       	     	   &     0     >     D     M     ]     m     |     �     �     �     �     �     �     �     �     �     �     �     �  	   �     �                               %  	   +     5     :     H     O     X  	   o     y     �  <   �     �  �   �     �     �     �     �     �     �     	     	  	   	  	   	  �  !	     �
     �
     �
     �
     �
     �
     �
                              %  
   ,     7  
   @     K     [     j  	   p     z     �     �     �     �     �     �     �     �     �                         0     @     V     b     i     n     |     �     �     �     �     �     �     �     �     �     �     �       8        M  �   U     �     �               (     5     D     I     Q     ^     @   8          %      (   C       "   B   .       :           4   #       /          9          <   )                   1      $   ?       =         5   +         A   '          ;   2   0   ,   -       &                                *                             3      
   7                 >   	          6       !       %s, %u items 1 hour ago 100MB 100kB 10MB 1GB 1MB 1kB >1GB A month ago April Audio August Contents December Documents Downloads Earlier today Email February Files & Folders Filesystem type Five hours ago Folders Format Four hours ago Friday Home Images Invalid Month January July June Last 30 days Last 7 days Last modified Last year March May Monday November October Open Other Past hour Path Presentations Recent Saturday Search Files & Folders September Show in Folder Size Sorry, there are no files or folders that match your search. Sunday This is an Ubuntu search plugin that enables local files to be searched and displayed in the Dash underneath the Files & Folders header. If you do not wish to search this content source, you can disable this search plugin. Three hours ago Three weeks ago Thursday Total capacity %s Tuesday Two hours ago Type Videos Wednesday Yesterday Project-Id-Version: unity-lens-files
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-17 18:41+0000
PO-Revision-Date: 2015-12-31 23:27+0000
Last-Translator: Ivo Xavier <ivoxavier.8@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:05+0000
X-Generator: Launchpad (build 18115)
 %s, %u itens Há uma hora 100MB 100kB 10MB 1GB 1MB 1kB >1GB Há um mês abril Áudio agosto Conteúdos dezembro Documentos Transferências Mais cedo hoje Email fevereiro Ficheiros e pastas Tipo do sistema de ficheiros Há cinco horas Pastas Formato Há quatro horas Sexta-feira Pasta pessoal Imagens Mês Inválido janeiro julho junho Últimos 30 dias Últimos 7 dias Última modificação Último ano março maio Segunda-feira novembro outubro Abrir Outros Última hora Localização Apresentações Recentes Sábado Procurar ficheiros e pastas setembro Mostrar na pasta Tamanho Sem ficheiros ou pastas que coincidam com a sua procura. Domingo Este é um scope do Ubuntu que procura ficheiros no computador para que possam ser mostrados no "Dash". Se pretender, pode desligar este scope. Há três horas Há três semanas Quinta-feira Capacidade total %s Terça-feira Há duas horas Tipo Vídeos Quarta-feira Ontem 