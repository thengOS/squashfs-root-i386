��          \      �       �      �      �      �   ;   �      !  �   5       �  ,     �     �     �  G        M  �   b     @                                       Devhelp Search Devhelp Show Sorry, there are no Devhelp results that match your search. Technical Documents This is an Ubuntu search plugin that enables information from Devhelp to be searched and displayed in the Dash underneath the Code header. If you do not wish to search this content source, you can disable this search plugin. devhelp;dev;help;doc; Project-Id-Version: unity-scope-devhelp
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-28 09:22+0000
PO-Revision-Date: 2014-04-10 16:01+0000
Last-Translator: IvoGuerreiro <ivoguerreiro@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:23+0000
X-Generator: Launchpad (build 18115)
 Devhelp Procurar no Devhelp Mostrar Desculpe, não há resultados Devhelp que coincidam com a sua pesquisa. Documentos técnicos Este é um plugin de busca Ubuntu que permite que os resultados do Devhelp possam ser exibido no Dash em baixo do code do cabeçalho. Se não desejar pesquisar essa fonte de conteúdo, pode desativar este plugin de busca. devhelp;dev;ajuda;doc; 