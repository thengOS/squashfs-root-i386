��    	      d      �       �      �      �      �      �   4        H  �   \     D  �  R                    )  =   ?     }  �   �     �        	                                      Manpages Open Search Manpages Search for Manpages Sorry, there are no Manpages that match your search. Technical Documents This is an Ubuntu search plugin that enables information from local manpages to be searched and displayed in the Dash underneath the Code header. If you do not wish to search this content source, you can disable this search plugin. manpages;man; Project-Id-Version: unity-scope-manpages
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-24 16:55+0000
PO-Revision-Date: 2014-04-11 11:05+0000
Last-Translator: IvoGuerreiro <ivoguerreiro@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:23+0000
X-Generator: Launchpad (build 18115)
 Manpages Abrir Procurar Manpages Procurar por Manpages Desculpe, não há Manpages que coincidam com a sua pesquisa. Documentos técnicos Este é um plugin de busca do Ubuntu permite que informações de manpages locais serem pesquisadas ​​e exibidas no Dash em baixo do cabeçalho. Se você não quiser pesquisar esta fonte de conteúdos, pode desativar este plugin de busca. manpages;man; 