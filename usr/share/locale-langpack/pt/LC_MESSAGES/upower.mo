��    
      l      �       �   #   �   #        9      R     s  &   �     �      �  "   �  �    1   �  ,   �      �  %        E  (   a     �  -   �  ,   �                               
          	    Dump all parameters for all objects Enumerate objects paths for devices Exit after a small delay Exit after the engine has loaded Get the wakeup data Monitor activity from the power daemon Monitor with detail Show extra debugging information Show information about object path Project-Id-Version: upower
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-06-15 11:24+0000
PO-Revision-Date: 2013-03-14 11:24+0000
Last-Translator: Pedro Flores <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:49+0000
X-Generator: Launchpad (build 18115)
 Eliminar todos os parâmetros em todos os objetos Enumerar caminhos de objetos em dispositivos Sair depois de uma pequena pausa Sair após o motor ter sido carregado Obter os dados do despertar Monitorar atividade do génio de energia Monitorar com detalhe Mostrar informação de depuração adicional Mostrar informação sobre caminho de objeto 