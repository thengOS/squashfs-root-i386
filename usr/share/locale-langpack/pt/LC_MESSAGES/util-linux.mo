��          t      �                       0     7     F     L     Z     l     z  �  �  �  R     +     <     L     R  
   c     n     �     �     �  d  �            	                    
                            %s succeeded.
 Formatting ...  Read:  Verifying ...  done
 get blocksize get size in bytes set read-only set read-write usage: %s [-h] [-v] [-b blksize] [-e edition] [-N endian] [-i file] [-n name] dirname outfile
 -h         print this help
 -v         be verbose
 -E         make all warnings errors (non-zero exit status)
 -b blksize use this blocksize, must equal page size
 -e edition set edition number (part of fsid)
 -N endian  set cramfs endianness (big|little|host), default host
 -i file    insert a file image into the filesystem (requires >= 2.4.0)
 -n name    set name of cramfs filesystem
 -p         pad by %d bytes for boot code
 -s         sort directory entries (old option, ignored)
 -z         make explicit holes (requires >= 2.3.39)
 dirname    root of the filesystem to be compressed
 outfile    output file
 Project-Id-Version: util-linux
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-11-02 11:43+0100
PO-Revision-Date: 2008-09-12 20:25+0000
Last-Translator: Susana Pereira <susana.pereira@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 16:05+0000
X-Generator: Launchpad (build 18115)
 %s com sucesso.
 A formatar ...  Ler:  A verificar ...  terminado
 obter o tamanho do bloco obter o tamanho em bytes definir como só-leitura definir como leitura-escrita Utilização:

%s [-h] [-v] [-b blksize] [-e edition] [-N endian] [-i file] [-n name] dirname outfile

-h         : apresentar esta ajuda
-v         : apresentar mais informação de saída
-E         : tornar todos os avisos em erros (estado de saída não-zero)
-b blksize : usar este blocksize, tem de ser igual ao page size
-e edition : definir número da edition (parte do fsid)
-N endian  : definir endian para o cramfs (big|little|host), predef. host
-i file    : inserir o file de imagem no sis. de ficheiros (requer >= 2.4.0)
-n name    : definir name para o sis. de ficheiros cramfs
-p         : deixar %d bytes de espaço para o boot code
-s         : ordenar itens do directório (opção antiga, ignorada)
-z         : criar buracos explicitos (requer >= 2.3.39)
dirname    : root do sis. de ficheiros a ser comprimida
outfile    : ficheiro para a saída
 