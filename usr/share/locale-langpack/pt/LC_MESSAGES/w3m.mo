��            )   �      �     �     �     �     �     �     �               )     <     O     b     u     �     �     �     �  +   �  &   �     $  /   =  0   m     �     �     �  $   �            �  4     �     �     �     �          &  
   4     ?     P     e     �     �     �     �     �  #   �        ,   #  (   P     y  4   �  A   �  %   	     1	  $   K	  %   p	     �	  #   �	           	                                                                                          
                                      ----------------   Back         (b)   Bookmark     (B)   Edit Source  (e)   Help         (h)   Option       (o)   Quit         (q)   Reload       (r)   Save Image   (I)   Save Source  (S)   Select Tab   (t)   View Frame   (f)   View Image   (i)   View Source  (v)  ASCII External Viewer Setup Indent for HTML rendering Number of pixels per character (4.0...32.0) Number of pixels per line (4.0...64.0) Number of remembered URL Number of remembered lines when used as a pager Open link on new tab if target is _blank or _new Render frames automatically Save URL history Tab width in characters Treat argument without scheme as URL Use URL history Use _self as default target Project-Id-Version: w3m
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-04-29 16:18+0900
PO-Revision-Date: 2015-04-09 22:39+0000
Last-Translator: Tiago Silva <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:21+0000
X-Generator: Launchpad (build 18115)
  ----------------   Recuar (b)   Marcar Favorito (B)   Editar Código Fonte (v)   Ajuda (h)   Opção (o)   Sair (q)   Actualizar (r)   Guardar Imagem (I)   Guardar Código Fonte (v)   Seleccionar Separador (t)   Ver Moldura (f)   Ver Imagem (i)   Ver Código Fonte (v)  ASCII Configuração Visualizador Externo Entrada para renderização HTML Número de pixeis por caractér (4.0...32.0) Número de pixeis por linha (4.0...64.0) Número de URL em memória Número de linhas em memória ao usar como paginador Abrir ligação em novo separador se o destino for _blank ou _new Renderizar fotogramas automaticamente Guardar histórico de URL Largura de tabulação em caracteres Tratar argumento sem esquema como URL Usar histórico de URL Usar _self como destino por defeito 