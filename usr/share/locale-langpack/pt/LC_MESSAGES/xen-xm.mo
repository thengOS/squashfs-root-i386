��          t      �                      .  "   A      d  7   �  c   �  )   !  R   K  0   �  �  �     h     n     �  *   �  "   �  ;   �  Z   "  %   }  \   �  ,               	                              
                   %(2)s Internal error: %(1)s. Permission denied. The %(1)s handle %(2)s is invalid. The method %(1)s is unsupported. The method %(1)s takes %(2)s argument(s) (%(3)s given). The network you specified already has a PIF attached to it, and so another one may not be attached. This map already contains %(1)s -> %(2)s. Value "%(2)s" for %(1)s is not supported by this server.  The server said "%(3)s". You attempted an operation that was not allowed. Project-Id-Version: xen
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-03-31 17:40+0100
PO-Revision-Date: 2012-06-04 12:31+0000
Last-Translator: Mykas0 <Mykas0@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:40+0000
X-Generator: Launchpad (build 18115)
 %(2)s Erro interno: %(1)s. Permissão recusada. O controlador %(1)s de %(2)s é inválido. O método %(1)s não é suportado. O método %(1)s carece de %(2)s argumento(s) (%(3)s dados). A rede que especificou já tem um PIF ligado a ela, e portanto outro não pode ser ligado. Este mapa já contém %(1)s -> %(2)s. O valor "%(2)s" para %(1)s não é suportado por este servidor. O servidor devolveu "%(3)s". Tentou uma operação que não é permitida. 