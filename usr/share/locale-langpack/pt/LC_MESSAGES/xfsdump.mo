��          �   %   �      @  
   A     L     a     w     �  4   �  4   �  0     '   <  
   d     o     �     �     �     �     �     �  !   �           (  
   I     T     ]  +   p  �  �     9     I     a     t     �  5   �  J   �  6     /   U     �     �  	   �     �     �     �            0   ,  .   ]  '   �  	   �     �     �  .   �                                                                            	                                      
             (default)  (timeout in %u sec)  (timeout in %u sec)
 (timeout in %d sec) -%c argument missing
 -%c argument must be between %u and %u: ignoring %u
 all map windows in use. Check virtual memory limits
 cannot specify source files and stdout together
 could not fstat stdin (fd %d): %s (%d)
 end dialog examining new media
 hangup
 keyboard interrupt
 keyboard quit
 media change aborted
 media change declined media changed no destination file(s) specified
 no source file(s) specified
 please change media in drive %u
 terminate
 timeout
 using %s strategy
 win_map(): try to select a different win_t
 Project-Id-Version: xfsdump
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-01-08 05:09+0000
PO-Revision-Date: 2008-04-13 02:42+0000
Last-Translator: Mykas0 <Mykas0@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:22+0000
X-Generator: Launchpad (build 18115)
  (por omissão)  (cessação em%u segs)  (fim em %u segs)
 (fim em %d segs) -%c argumento em falta
 -%c argumento deve estar entre %u e %u: a ignorar %u
 todas as janelas de mapa em uso. Verifique os limites da memória virtual
 não pode especificar ficheiros-fonte e stdout juntos
 consegui executar fstat stdin (fd %d): %s (%d)
 diálogo de fim a examinar o novo media
 desligar
 interrupção de teclado
 saída de teclado
 mudança de media abortado
 mudança de media declinada media alterado não há ficheiro(s) de destino especificado(s)
 não há ficheiro(s) de fonte especificado(s)
 por favor altere o media na unidade %u
 terminar
 fim
 a usar estratégia %s
 win_map(): tente escolher uma win_t diferente
 