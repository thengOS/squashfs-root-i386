��          \      �       �   &   �   1   �      "  $   9     ^     z     �  �  �  2   2  >   e  #   �  8   �  (        *     :                                       %s:  could not write to logfile "%s".
 Aborting XFS copy -- logfile error -- reason: %s
 All copies completed.
 Check logfile "%s" for more details
 See "%s" for more details.
 lseek64 error write error Project-Id-Version: xfsprogs
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-11-27 10:38+1100
PO-Revision-Date: 2007-03-23 20:19+0000
Last-Translator: Marco Rodrigues <Unknown>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:23+0000
X-Generator: Launchpad (build 18115)
 %s: impossível escrever no ficheiro de log "%s".
 A abortar cópia XFS -- erro de ficheiro de log -- razão: %s
 Todas as cópias foram efectuadas.
 Verificar ficheiro de log "%s" para mais informações.
 Consultar "%s" para mais informações.
 erro de lseek64 erro de escrita 