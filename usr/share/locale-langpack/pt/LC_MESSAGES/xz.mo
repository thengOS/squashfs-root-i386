��   �   0   �  �   �     �     �  �      L  �   M  7    �  W  -      F   N     �     �  7   �  �   �  �   �  �   *  �     �  �  H   j     �  E   3  �   y      >   &  9   e  �   �  �   5  �   �  �   <  �   
  �   �  l   \     �     �     �          5     U     o     �     �  z   �     8      R      l   .   ~   6   �      �      �      !     !  !   &!  !   H!  '   j!     �!     �!     �!  *   �!  /   "  %   L"     r"  /   �"  ,   �"     �"  4   �"     ,#     H#     f#     ~#     �#      �#      �#  h   �#  <   _$     �$  :   �$  $   �$     %  2   *%     ]%  $   z%  /   �%  I   �%     &  3   -&  =   a&  d   �&      '  O   %'  .   u'  /   �'     �'  A   �'  )   1(     [(     d(  8   }(     �(     �(  (   �(  I   )  !   Y)  '   {)  '   �)  9   �)     *      *  0   #*     T*  <   Y*  -   �*  @   �*  /   +  7   5+  D   m+  &   �+  '   �+     ,  %   	,     /,     G,  
   U,  
   `,  
   k,  
   v,  
   �,  	   �,  	   �,  	   �,  	   �,  	   �,  	   �,  	   �,  "   �,  *   �,      -  A   4-  Q   v-  *   �-  @   �-  !   4.     V.  �  Z.  �   -0  q  	1  �  {4  9   o6  \   �6     7     &7  =   :7  �   x7  v   78    �8  �   �9  �  z:  ^   [<  �   �<  H   Q=  �   �=  2  F>  @   y?  <   �?  �   �?  }   �@  �    A  �   �A  �   �B  �   ^C  V   (D     D     �D     �D     �D     �D     �D     E     0E     NE  c   jE     �E     �E     �E  /   F  >   @F     F     �F     �F     �F  /   �F  7   G  6   ?G  "   vG  $   �G  #   �G  8   �G  =   H  5   YH     �H  1   �H  <   �H     I  K   4I  (   �I  %   �I     �I     �I     J  "   'J  )   JJ  l   tJ  A   �J     #K  ;   <K  6   xK     �K  :   �K  ,   	L  6   6L  /   mL  U   �L     �L  <   M  J   HM  s   �M  (   N  S   0N  <   �N  >   �N  $    O  D   %O  -   jO     �O  *   �O  ?   �O  $   P     4P  /   IP  ]   yP  $   �P  /   �P  -   ,Q  P   ZQ  +   �Q     �Q  U   �Q     2R  M   :R  4   �R  D   �R  -   S  8   0S  R   iS  %   �S  .   �S     T  )   T     CT     [T     mT     }T     �T     �T     �T     �T     �T     �T     �T     �T     U     U  -   &U  7   TU     �U  ]   �U  a   V  =   eV  F   �V  (   �V     W     ^   =       K   I       |   .           �          �   1              U   u      +          _              5   8       q       #   d       9   `   (       3   c          ]       -   [   �   y   a          J   "   ~   �   j            Z       %           s      0                          x          $   e       A          E      k           ,   }       R       O   B   �       )   ?       @   
       �       L   t   f   G             �       <              !   {   n   l   >   V   Q   2      Y   \       m   M   '           F          D   o      r   ;      p      X   w   v   7   z       	      N         �   i   4           b   *   H   W       6   g      �   �       T   &       h               P   /   :       �      C   S         W     W  �       0  %W  2          ����XW  0               �����W  9          �����W  +               ���� 
  --delta[=OPTS]      Delta filter; valid OPTS (valid values; default):
                        dist=NUM   distance between bytes being subtracted
                                   from each other (1-256; 1) 
  --lzma1[=OPTS]      LZMA1 or LZMA2; OPTS is a comma-separated list of zero or
  --lzma2[=OPTS]      more of the following options (valid values; default):
                        preset=PRE reset options to a preset (0-9[e])
                        dict=NUM   dictionary size (4KiB - 1536MiB; 8MiB)
                        lc=NUM     number of literal context bits (0-4; 3)
                        lp=NUM     number of literal position bits (0-4; 0)
                        pb=NUM     number of position bits (0-4; 2)
                        mode=MODE  compression mode (fast, normal; normal)
                        nice=NUM   nice length of a match (2-273; 64)
                        mf=NAME    match finder (hc3, hc4, bt2, bt3, bt4; bt4)
                        depth=NUM  maximum search depth; 0=automatic (default) 
  --x86[=OPTS]        x86 BCJ filter (32-bit and 64-bit)
  --powerpc[=OPTS]    PowerPC BCJ filter (big endian only)
  --ia64[=OPTS]       IA-64 (Itanium) BCJ filter
  --arm[=OPTS]        ARM BCJ filter (little endian only)
  --armthumb[=OPTS]   ARM-Thumb BCJ filter (little endian only)
  --sparc[=OPTS]      SPARC BCJ filter
                      Valid OPTS for all BCJ filters:
                        start=NUM  start offset for conversions (default=0) 
 Basic file format and compression options:
 
 Custom filter chain for compression (alternative for using presets): 
 Operation modifiers:
 
 Other options:
 
With no FILE, or when FILE is -, read standard input.
       --block-size=SIZE
                      when compressing to the .xz format, start a new block
                      after every SIZE bytes of input; 0=disabled (default)       --info-memory   display the total amount of RAM and the currently active
                      memory usage limits, and exit       --memlimit-compress=LIMIT
      --memlimit-decompress=LIMIT
  -M, --memlimit=LIMIT
                      set memory usage limit for compression, decompression,
                      or both; LIMIT is in bytes, % of RAM, or 0 for defaults       --no-adjust     if compression settings exceed the memory usage limit,
                      give an error instead of adjusting the settings downwards       --no-sparse     do not create sparse files when decompressing
  -S, --suffix=.SUF   use the suffix `.SUF' on compressed files
      --files[=FILE]  read filenames to process from FILE; if FILE is
                      omitted, filenames are read from the standard input;
                      filenames must be terminated with the newline character
      --files0[=FILE] like --files but use the null character as terminator       --robot         use machine-parsable messages (useful for scripts)       --single-stream decompress only the first stream, and silently
                      ignore possible remaining input data       CheckVal %*s Header  Flags        CompSize    MemUsage  Filters   -0 ... -9           compression preset; default is 6; take compressor *and*
                      decompressor memory usage into account before using 7-9!   -F, --format=FMT    file format to encode or decode; possible values are
                      `auto' (default), `xz', `lzma', and `raw'
  -C, --check=CHECK   integrity check type: `none' (use with caution),
                      `crc32', `crc64' (default), or `sha256'   -Q, --no-warn       make warnings not affect the exit status   -V, --version       display the version number and exit   -e, --extreme       try to improve compression ratio by using more CPU time;
                      does not affect decompressor memory requirements   -h, --help          display the short help (lists only the basic options)
  -H, --long-help     display this long help and exit   -h, --help          display this short help and exit
  -H, --long-help     display the long help (lists also the advanced options)   -k, --keep          keep (don't delete) input files
  -f, --force         force overwrite of output file and (de)compress links
  -c, --stdout        write to standard output and don't delete input files   -q, --quiet         suppress warnings; specify twice to suppress errors too
  -v, --verbose       be verbose; specify twice for even more verbose   -z, --compress      force compression
  -d, --decompress    force decompression
  -t, --test          test compressed file integrity
  -l, --list          list information about .xz files   Blocks:
    Stream     Block      CompOffset    UncompOffset       TotalSize      UncompSize  Ratio  Check   Blocks:             %s
   Check:              %s
   Compressed size:    %s
   Memory needed:      %s MiB
   Minimum XZ Utils version: %s
   Number of files:    %s
   Ratio:              %s
   Sizes in headers:   %s
   Stream padding:     %s
   Streams:
    Stream    Blocks      CompOffset    UncompOffset        CompSize      UncompSize  Ratio  Check      Padding   Streams:            %s
   Uncompressed size:  %s
  Operation mode:
 %s MiB of memory is required. The limit is %s. %s MiB of memory is required. The limiter is disabled. %s file
 %s files
 %s home page: <%s>
 %s:  %s: Cannot remove: %s %s: Cannot set the file group: %s %s: Cannot set the file owner: %s %s: Cannot set the file permissions: %s %s: Closing the file failed: %s %s: Error reading filenames: %s %s: Error seeking the file: %s %s: File already has `%s' suffix, skipping %s: File has setuid or setgid bit set, skipping %s: File has sticky bit set, skipping %s: File is empty %s: File seems to have been moved, not removing %s: Filename has an unknown suffix, skipping %s: Filter chain: %s
 %s: Input file has more than one hard link, skipping %s: Invalid filename suffix %s: Invalid multiplier suffix %s: Invalid option name %s: Invalid option value %s: Is a directory, skipping %s: Is a symbolic link, skipping %s: Not a regular file, skipping %s: Null character found when reading filenames; maybe you meant to use `--files0' instead of `--files'? %s: Options must be `name=value' pairs separated with commas %s: Read error: %s %s: Seeking failed when trying to create a sparse file: %s %s: Too small to be a valid .xz file %s: Unexpected end of file %s: Unexpected end of input when reading filenames %s: Unknown file format type %s: Unsupported integrity check type %s: Value is not a non-negative decimal integer %s: With --format=raw, --suffix=.SUF is required unless writing to stdout %s: Write error: %s --list does not support reading from standard input --list works only on .xz files (--format=xz or --format=auto) Adjusted LZMA%c dictionary size from %s MiB to %s MiB to not exceed the memory usage limit of %s MiB Cannot establish signal handlers Cannot read data from standard input when reading filenames from standard input Compressed data cannot be read from a terminal Compressed data cannot be written to a terminal Compressed data is corrupt Compression and decompression with --robot are not supported yet. Decompression will need %s MiB of memory. Disabled Empty filename, skipping Error restoring the O_APPEND flag to standard output: %s File format not recognized Internal error (bug) LZMA1 cannot be used with the .xz format Mandatory arguments to long options are mandatory for short options too.
 Maximum number of filters is four Memory usage limit for compression:     Memory usage limit for decompression:   Memory usage limit is too low for the given filter setup. Memory usage limit reached No No integrity check; not verifying file integrity None Only one file can be specified with `--files' or `--files0'. Report bugs to <%s> (in English or Finnish).
 Strms  Blocks   Compressed Uncompressed  Ratio  Check   Filename The .lzma format supports only the LZMA1 filter The environment variable %s contains too many arguments The exact options of the presets may vary between software versions. The sum of lc and lp must not exceed 4 Total amount of physical memory (RAM):  Totals: Try `%s --help' for more information. Unexpected end of input Unknown error Unknown-11 Unknown-12 Unknown-13 Unknown-14 Unknown-15 Unknown-2 Unknown-3 Unknown-5 Unknown-6 Unknown-7 Unknown-8 Unknown-9 Unsupported LZMA1/LZMA2 preset: %s Unsupported filter chain or filter options Unsupported options Unsupported type of integrity check; not verifying file integrity Usage: %s [OPTION]... [FILE]...
Compress or decompress FILEs in the .xz format.

 Using a preset in raw mode is discouraged. Valid suffixes are `KiB' (2^10), `MiB' (2^20), and `GiB' (2^30). Writing to standard output failed Yes Project-Id-Version: xz-utils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-04 03:00+0000
PO-Revision-Date: 2014-08-04 15:21+0000
Last-Translator: IvoGuerreiro <ivoguerreiro@gmail.com>
Language-Team: Portuguese <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Launchpad-Export-Date: 2016-06-27 18:40+0000
X-Generator: Launchpad (build 18115)
 
  --delta[=OPTS]      Filtro de diferença; OPTS válido (valores válidos; padrão):
                        dist=NUM   diferença entre bytes sendo subtraídos
                                   de cada um (1-256; 1) 
  --lzma1[=OPÇÕES] LZMA1 ou LZMA2; OPÇÕES é uma lista, separada por vírgula, de zeros ou
  --lzma2[=OPÇÕES] mais das opções a seguir (valores válidos; padrão):
                      preset=PRE recompõe opções de uma pré-definição (0-9[e])
                      dict=NUM tamanho do dicionário (4KiB - 1536MiB; 8MiB)
                      lc=NUM número de bits no contexto literal (0-4; 3)
                      lp=NUM número de bits na posição literal (0-4; 0)
                      pb=NUM número de bits posicionados (0-4; 2)
                      mode=MODO modo de compressão (fast, normal; normal)
                      nice=NUM comprimento de uma boa combinação (2-273; 64);
                      mf=NOME pesquisador de combinação (hc3, hc4, bt2, bt3, bt4; bt4)
                      depth=NUM profundidade máxima de busca; 0=automático (padrão) 
  --x86[=OPÇÕES] filtro BCJ para x86 (32-bit e 64-bit)
  --powerpc[=OPÇÕES] filtro BCJ para PowerPC (somente maior ordem de bits)
  --ia64[=OPÇÕES] filtro BCJ para IA-64 (Itanium)
  --arm[=OPÇÕES] filtro BCJ para ARM (somente menor ordem de bits)
  --armthumb[=OPÇÕES] filtro BCJ para AMR-Thumb
  --sparc[=OPÇÕES] filtro BCJ para SPARC
                      OPÇÕES válidas para todos os filtros BCJ:
                        start=NUM inicial deslocamento para conversões (padrão=0) 
 Formato básico de ficheiro e opções de compressão:
 
 Cadeia de filtro personalizado para compressão (alternativa ou usando pré-definições): 
 Modificadores de operação:
 
 Outras opções:
 
Sem FICHEIRO, ou quando FICHEIRO é -, ler entrada padrão.
       --block-size=TAMANHO
                      ao compactar para o formato .xz, inicia um novo bloco
                      depois de cada TAMANHO bytes de entrada; 0=desabilitado (padrão)       --info-memory mostra o tamanho total de RAM e os limites actuais de
                      uso de memória, e sai       --memlimit-compress=LIMITE
      --memlimit-decompress=LIMITE
  -M, --memlimit=LIMITE
                      define o limite de memória utilizado para compressão, decompressão,
                      ou ambos; LIMITE é definido em bytes, % de RAM, ou 0 para padrão       --no-adjust se as configurações de compressão excederem o limite de uso de memória,
                      retorna um erro ao invés de ajudar as configurações para menor uso       --no-sparse não cria ficheiros espaçados ao decomprimir
  -S, --suffix=.SUF usar o sufixo `.SUF' nos ficheiros comprimidos
      --files[=FICHEIRO] lê nomes de ficheiros a partir de FICHEIRO; se FICHEIRO é
                      omitido, os nomes dos ficheiros serão lidos à partir da entrada padrão;
                      nomes dos ficheiros devem terminar com o caracteres de nova linha
      --files0[=FICHEIRO] como --files mas usa o caractere nulo como terminador       --robot         use mensagens que podem ser analisadas por computador (útil em scripts)       --single-stream descompacta somente a primeira transmissão e silenciosamente
                      ignora possíveis dados de entrada restantes       VerifValor %*s Cabeçalho Sinalizadores TamComp UsoMemoria Filtros   -0 ... -9 pré-definição de compressão; padrão é 6; leve em conta o
                      uso de memória pelo compactador *e* descompactador antes de utilizar 7-9!   -F, --format=FORMATO formato do ficheiro a codificar ou descodificar; valores possíveis são:
                      `auto' (padrão), `xz', `lzma', e `raw'
  -C, --check=VERIFICAR tipo de integridade para verificar: `nome' (use com cuidado),
                      `crc32', `crc64' (padrão), ou `sha256'   -Q, --no-warn       os avisos não afectarão o estado ao sair   -V, --version       apresentar o número da versão e sair   -e, --extreme tenta melhorar a taxa de compressão utilizando mais tempo de processamento;
                      não afecta os requisitos de memória do descompactador   -h, --help          apresentar apenas a ajuda básica das opções
  -H, --long-help     apresentar a ajuda completa e sair   -h, --help          apresentar apenas a ajuda básica das opções e sair
  -H, --long-help     apresentar a ajuda completa incluindo opções avançadas   -k, --keep          manter (não apagar) ficheiros de entrada
  -f, --force         forçar a re-escrita de ficheiros de saída e (des)comprimir ligações
  -c, --stdout        escrever para a saída padrão e não apagar ficheiros de entrada   -q, --quiet         suprimir avisos; especificar duas vezes para também suprimir erros
  -v, --verbose       ser verboso; especificar duas vezes para ainda mais verboso   -z, --Comprimir força a compressão
  -d, --Descomprimir força a descompressão
  -t, --teste testa a integridade do ficheiro comprimido
  -l, --lista lista informações relativas ao ficheiros .xz   Blocos:
    Fluxo Bloco DeslocComp DeslocDescomp TamTotal TamDescomp Taxa Verificado   Blocos:%s
   Verificar: %s
   Tamanho comprimido::    %s
   Memória necessária: %s MiB
   VersãomMinima XZ Utils: %s
   Número de ficheiros: %s
   Racio:              %s
   Tamanhos em títulos:   %s
   Stream preenchimento: %s
   Streams:
  Fluxo Blocos DeslocComp DeslocDescomp TamComp TamDescomp Taxa Verificada Preenchimento   Fluxos: %s
   Tamanho descomprimido:  %s
  Modo de operação:
 %s MiB de memória necessária. O limite é %s. %s MiB de memória necessária. O limitador está desactivado. %s ficheiro
 %s ficheiros
 Página web de %s : <%s>
 %s:  %s: Não consegue remover: %s %s: Impossível definir o grupo do ficheiro: %s %s: Não consegue definir proprietário do ficheiro: %s %s: Impossível definir as permissões do ficheiro: %s %s: Falhou a fechar o ficheiro: %s %s: Erro a ler nomes de ficheiro: %s %s: Erro ao procurar o ficheiro: %s %s: O ficheiro já tem o sufixo `%s', a passar à frente %s: O ficheiro tem um bit setuid ou setgid definido, saltando %s: O ficheiro tem um bit "sticky" definido, saltando %s: O ficheiro está vazio %s: ficheiro parece ter sido movido, não apagado %s: Nome de ficheiro tem um sufixo desconhecido, a descartar %s: Cadeia de Filtros: %s
 %s: O ficheiro de entrada tem mais do que um ligação permanente, saltando %s: Sufixo de nome de ficheiro inválido %s: Sufixo de multiplicador inválido %s: Nome de opção inválida %s: Valor de opção inválida %s: É uma pasta, saltando %s: ´Is a symbolic link, skipping %s: Não é um ficheiro regular, saltando %s: Carácter nulo encontrado ao ler nomes de ficheiros; será que quis usar `--files0' em vez de `--files'? %s: Opções devem ser pares `nome=valor' separados por vírgulas %s: Erro de leiturar: %s %s: Procura falhada quando feita a um ficheiro disperso: %s %s: Demasiado pequeno para ser um ficheiro .xz válido %s: Fim inesperado de ficheiro %s: Fim de entrada inesperado ao ler os nomes de ficheiros %s: Tipo de formato de ficheiro desconhecido %s: Tipo de verificção de integridade não suportado %s: O Valor é um inteiro decimal não-negativo %s: Com --format=raw, --suffix=.SUF é necessário a menos que se escreva para stdout %s: Erro de leitura: %s --lista não suporta leitura a partir da entrada predefinida --lista funciona apenas com ficheiros .xz (--formato=xz ou --formato=auto) Ajustado LZMA%c tamanho do dicionário de %sMiB para %s MiB para não exceder o limite de uso de memória de %s MiB Não pode estabelecer gestores de sinais Não pode ler dados de entrada padrão ao ler nomes de ficheiros da entrada padrão Dados comprimidos não podem ser lidos a  partir do terminal Dados comprimidos não podem ser escritos a partir do terminal Dados comprimidos estão corrompidos Compressão e descompressão com --robot não são suportadas ainda. Descompressão precisará %s MiB de memória. Desactivado Nome de ficheiro vazio, a passar à frente Erro ao restaurar a bandeira O_APPEND para a saída padrão: %s Formato de ficheiro não reconhecido Erro interno ("bug") LZMA1 não pode ser utilizado com o formato .xz Argumentos mandatórios para opções longas são mandatórios para opções curtas também.
 Número máximo de filtros é quatro Limite de uso de memória para compressão:     Limite de uso de memória para compressão:   O limite de uso de memória é muito baixa para a configuração do filtro dado. Limite de utilização da memória atingido Não Nenhuma verificação de integridade; não está a verificar integridade de ficheiros Nenhuma Apenas um ficheiro pode ser especificado com `--ficheiros' ou `--ficheiros0'. Relatar erros para <%s> (em Inglês ou Finlandês).
 Repro. Blocos Comprimido Descomprimido Rácio Confirmar NomeFicheiro O formato .lzma apenas suporta o filtro LZMA1 A variável de ambiente %s contém demasiados argumentos As opções exactas das pré-definições podem variar entre versões de software. A soma de lc e lp não deve exceder 4 A quantidade total de memória física (RAM):  Totais: Tente `%s --help' para mais informação. Fim de dados inesperado Erro desconhecido Desconhecido-11 Desconhecido-12 Desconhecido-13 Desconhecido-14 Desconhecido-15 Desconhecido-2 Desconhecido-3 Desconhecido-5 Desconhecido-6 Desconhecido-7 Desconhecido-8 Desconhecido-9 Predefinição LZMA1/LZMA2 não suportada: %s Opções de cadeias de filtros ou filtro não suportado Opções não suportadas Verificação de integridade de tipo desconhecido; a não verificar a integridade de ficheiro Utilização: %s [OPÇÃO]... [FICHEIRO]...
Comprimir ou descomprimir FICHEIROs no formato .xz.

 É desencorajado usar uma pré-definição  de modo em bruto. Os sufixos válidos são `KiB' (2^10), `MiB' (2^20), and `GiB' (2^30). Escrita falhou para a saída predefinida Sim PRIu32 PRIu64 The selected match finder requires at least nice=% Value of the option `%s' must be in the range [%, %] O detector escolhido necessita pelo menos da prioridade=% Valor da opção `%s' deve estar na gama [%, %] 