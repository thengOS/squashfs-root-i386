��    �     T  �  �      �(     �(     �(  "   �(  C   �(     6)  *   E)  �   p)  k   �)  g   b*  7   �*  6   +     9+  2   P+  F   �+  6   �+     ,  !   ,  9   7,  '   q,  1   �,     �,     �,     �,     -  c   -     �-  7   �-  '   �-  ,   �-  /   .  #   M.  (   q.  F   �.  '   �.  .   	/  )   8/  ,   b/     �/     �/     �/     �/     �/     �/     0     70     G0  *   a0  :   �0  -   �0  $   �0  #   1  >   >1  %   }1  1   �1  &   �1  !   �1  !   2  2   @2  9   s2  4   �2     �2     �2     3  0   33  8   d3     �3  /   �3  '   �3  G   4  !   P4  1   r4  ;   �4     �4     �4  *   �4  0   '5     X5  )   k5  7   �5  7   �5  )   6  -   /6  /   ]6  2   �6  >   �6  >   �6  1   >7  3   p7  1   �7  3   �7  1   
8     <8  ?   W8  )   �8  E   �8     9      9  Y   <9  &   �9  &   �9  1   �9  2   :      I:  -   j:  :   �:  $   �:     �:     ;  (   .;     W;  $   r;  !   �;      �;  #   �;     �;     <  ,   5<  R   b<  &   �<     �<     �<     =  "   2=     U=  !   r=  )   �=  $   �=  !   �=      >     &>     >>  $   W>  1   |>     �>     �>     �>     �>     ?  g   ?     �?  
   �?     �?  A   �?  !   �?     @     0@     G@  #   \@     �@  .   �@     �@     �@     �@     A     A  +   A  -   GA  ,   uA      �A     �A  J   �A  -   ,B  !   ZB  �   |B  1   7C     iC     �C     �C     �C  "   �C  2   D      6D     WD  %   lD  .   �D  /   �D  '   �D  /   E  !   IE  0   kE     �E     �E     �E     �E  |   F  �   �F  }   7G  }   �G  y   3H  2   �H  =   �H     I     7I  \   PI  #   �I     �I  :   �I     *J  (   DJ  #   mJ  !   �J  $   �J  K   �J     $K     CK  6   _K     �K  )   �K  &   �K  $   �K     $L  $   >L  N   cL  2   �L     �L  0   M  +   4M  $   `M  .   �M  >   �M  E   �M  I   9N     �N  *   �N  0   �N  9   �N     *O  <   DO  !   �O  "   �O  .   �O  )   �O  "   P     BP     TP     tP  +   �P     �P  $   �P  "   �P  "   Q  &   ?Q  %   fQ  "   �Q     �Q     �Q  &   �Q  "   R  *   /R  "   ZR  ,   }R  !   �R  -   �R  %   �R      S  '   >S     fS     �S  4   �S     �S  @   �S     -T     9T     TT  *   fT  W   �T     �T     U  3   U     EU  !   cU  ?   �U     �U  _   �U     CV     VV  *   eV  5   �V  +   �V  +   �V     W  6   :W  )   qW  3   �W  /   �W  "   �W  '   "X  *   JX  &   uX  C   �X  )   �X  2   
Y  4   =Y  )   rY     �Y  .   �Y  B   �Y  :   (Z  :   cZ  +   �Z  4   �Z  (   �Z  '   ([      P[  *   q[  !   �[  5   �[  !   �[     \     4\     G\     g\  +   �\  Q   �\  _   �\  T   ^]  0   �]  '   �]     ^     &^     @^  &   S^     z^     �^     �^  i   �^     5_  !   N_      p_     �_  !   �_  2   �_  7   �_  
   $`  C   /`  C   s`     �`     �`  )   �`  D   �`  (   Da  0   ma  ,   �a     �a  8   �a  5   b  �   Hb  �   9c  1   d  /   ?d     od  %   �d  .   �d  (   �d  &   
e  R   1e  0   �e  ^   �e  Q   f  H   ff  G   �f  (   �f  3    g     Tg  -   lg  F   �g  (   �g     
h  8   h     Jh     hh  L   xh  O   �h     i  &   4i  (   [i     �i     �i  /   �i  O   �i     *j     :j     Nj     hj     ~j     �j     �j     �j  <   �j     k      k  K   :k  B   �k  %   �k  5   �k  8   %l     ^l  G   |l  w   �l  w   <m  s   �m  n   (n     �n     �n     �n  /   �n  ,   �n     &o  p   =o  w   �o  E   &p  S   lp  &   �p  &   �p  -   q  .   <q     kq  	   yq  (   �q     �q  o   �q  K   :r  w  �r  )   �v     (w     6w  5   Kw  0   �w     �w  %   �w     �w  "   �w     x  �   0x     �x  P    y  1   Qy  B   �y  ,   �y  <   �y  *   0z  (   [z  +   �z  .   �z  5   �z  F   {  F   \{     �{  ;   �{  8   �{  (   6|  :   _|  :   �|  A   �|  6   }  1   N}  B   �}  G   �}  J   ~  .   V~  ;   �~  9   �~  9   �~  :   5  %   p  -   �  "   �  -   �  1   �  8   G�  ;   ��  G   ��      �  8   %�  Q   ^�     ��  ?   ́  3   �  @   @�  "   ��  S   ��     ��     �     .�  A   3�  '   u�  �  ��     J�     O�  )   X�  J   ��     ͅ  7   �  �   �  k   ��  g   �  ;   s�  7   ��     �  7   �  O   :�  =   ��     Ȉ  #   ވ  <   �  -   ?�  7   m�     ��     ��     Չ     �  y   �     ��  P   ��  8   �  ?   #�  3   c�  *   ��  B     J   �  ;   P�  A   ��  5   Ό  8   �  .   =�  -   l�  0   ��     ˍ     ܍  '   �  $   �     :�  ,   O�  8   |�  D   ��  >   ��  8   9�  8   r�  L   ��  :   ��  J   3�  8   ~�  .   ��  .   �  :   �  P   P�  @   ��  $   �  ,   �  +   4�  =   `�  D   ��      �  @   �  .   E�  f   t�  6   ۓ  D   �  C   W�     ��     ��  2   ��  3   �     �  <   0�  J   m�  F   ��  :   ��  ?   :�  T   z�  ;   ϖ  A   �  D   M�  A   ��  A   ԗ  ?   �  ?   V�  >   ��  !   ՘  L   ��  :   D�  H   �     ș  %   �  P   �  )   ^�  0   ��  <   ��  :   ��  +   1�  /   ]�  A   ��  )   ϛ  *   ��  '   $�  +   L�  #   x�  #   ��  $   ��  !   �  #   �  &   +�     R�  5   r�  ^   ��  (   �  !   0�  %   R�     x�  !   ��  (   ��  #   ٞ  +   ��  -   )�  &   W�  $   ~�     ��     ��  /   ڟ  <   
�     G�     _�     x�     ��     ��  q   ��  !   3�  	   U�     _�  I   f�  ,   ��     ݡ  %   ��     "�  *   @�     k�  3   ��     ��     բ     �     �     �  +   �  ;   I�  /   ��      ��      ֣  d   ��  /   \�  %   ��  �   ��  7   ��  "   ܥ  !   ��     !�     >�  (   X�  B   ��  '   Ħ     �  ,   �  =   4�  F   r�  *   ��  8   �  %   �  4   C�     x�  !   ��  "   ��      ۨ  �   ��  �   ��  �   f�  �   �  �   ��  2   S�  G   ��     ά     �  t    �  $   u�  $   ��  :   ��     ��  1   �  -   F�  $   t�  #   ��  J   ��  #   �  $   ,�  9   Q�     ��  9   ��  +   �  '   �     8�  %   R�  _   x�  ?   ذ      �  8   9�  /   r�  3   ��  0   ֱ  F   �  I   N�  N   ��     �  4   ��  ?   /�  ?   o�     ��  M   ͳ  '   �     C�  .   b�  5   ��  '   Ǵ     �  '   �     )�  0   I�     z�  %   ��  '   ��  '   �  -   �  +   9�  ,   e�  "   ��     ��  (   ϶  )   ��  0   "�  &   S�  1   z�  -   ��  1   ڷ  &   �     3�  0   S�      ��     ��  @   ¸     �  T   "�     w�  !   ��     ��  5   Ĺ  i   ��     d�     ~�  <   ��  +   κ  +   ��  E   &�     l�  i   ��     ��     �  7   "�  1   Z�  *   ��  *   ��     �  >   �  N   A�  H   ��  C   ٽ  9   �  @   W�  F   ��  ?   ߾  [   �  3   {�  E   ��  K   ��  <   A�  /   ~�  D   ��  S   ��  P   G�  U   ��  >   ��  J   -�  ;   x�  C   ��  ?   ��  A   8�  %   z�  ;   ��  #   ��      �      �  &   :�  (   a�  4   ��  g   ��  �   '�  b   ��  =   �  <   L�  #   ��  '   ��     ��  1   ��  (   �     E�  &   c�  �   ��     %�  '   D�  "   l�     ��  #   ��  8   ��  F   ��  	   A�  J   K�  J   ��     ��     ��  4   �  J   9�  1   ��  :   ��  3   ��     %�  V   6�  S   ��    ��  �   ��  +   ��  +   �  $   /�  7   T�  7   ��  >   ��  6   �  d   :�  9   ��  l   ��  m   F�  k   ��  f    �  :   ��  4   ��     ��  .   �  O   ?�  6   ��     ��  G   ��  !   �     6�  S   K�  Q   ��  (   ��  +   �  3   F�     z�     ��  <   ��  T   ��     ?�     Q�      m�  !   ��  &   ��     ��     ��     �  ;   -�     i�  #   �  B   ��  N   ��  0   5�  >   f�  ?   ��  (   ��  S   �  �   b�  �   ��  �   x�  |   ��     {�     ��     ��  8   ��  '   ��     �  r   1�  �   ��  R   %�  b   x�  +   ��  +   �  4   3�  5   h�     ��  	   ��  2   ��  .   ��  �   �  h   ��  �  �  -   ��     ��     ��  =   ��  <   (�  '   e�  :   ��     ��  #   ��     ��  �   �  !   ��  a   ��  4   R�  D   ��  /   ��  @   ��  0   =�  .   n�  ,   ��  -   ��  @   ��  G   9�  G   ��     ��  ;   ��  =    �  +   ^�  9   ��  >   ��  G   �  ?   K�  6   ��  R   ��  I   �  U   _�  1   ��  K   ��  A   3�  A   u�  D   ��  1   ��  .   .�     ]�  .   }�  2   ��  F   ��  J   &�  P   q�  +   ��  :   ��  \   )�     ��  R   ��  ;   ��  V   2�  .   ��  d   ��     �     3�     S�  P   Z�  ,   ��     S   �   �                 �  �          �   �   6   �   j          $      �   R  <              �  �       �  �   �   n    J  �  �      )  �   U              �  `                      �      �  w              �       t      �  �  c          �   �  %   �   6            �                �  0   �          d  }   �   #  @  a     �  +   �  v   /  /   �  �  f  �  �   |  �  h  7   �       �      �       �      �   �      �              ;   �  1   �     %  q       �   �  ]   �  �  R   �   a   �  �   �   .   �  7  O  t   y      �   8             P  y   X  �  >   �  �   �         K      1  �  [   �   V  Z   �  E           �  +         `      u   i          H       �  �   8  l      (  u  m  �     _   z   	  L  G       "   T   �         P       X       r   �   �   �  :            =   �   �       �  C      �  �   �     �   �            �  G  �   i   �  m   D  �       '   B  �   �  �  �  2   _  �  �   �   �     �   �  H  �    �       Y   )   T  �          5  $    �  �   �     E  �   h     !   �      �      �  �      �  j  �      �   M   �   ?      �   �     �   }      �      �     w   5     &       �          �  ,          �   9   �       �       �   �       Q   �   �       �                    �          F           �       '  �   �      �  �  C   �   9  �           �  �   �      	   �  q      �  s  �  �      @      K   �   c       �  �   �      �          ,   {   �  W       .          g  �  g       �       \       �    �   ;  x   b   2  �  e  �  4   �   �          �             n   �  �   x    �   W  *  �       Z     �   U  ?      !  �  4      �  o   k         �  e   I   �       <               �  �   [      A    �   F   �   ]     �         #       |   �   �          �   z  B      �   �       �  �       �   
  p   &  �   0  Q  b  �   �       \      �                �   �     �   �   d           v  N  o  :   *   �           �  �   �       S  l   �     �   �  >  -  �                  M    �         ^              �  �  �   ~   V       �                   p  �   J           �   �   O           k     �  ^   A   �           �  {              �  -   
   ~  D       �  �   (   �   �   �   �  L          f       �       �   �   �  "    N   �           Y      �  I      �  �           �   �      =  r          �  �   �   �           �   �   �           �  �   s   �  �       3   �   3  �   �            
%s
 "%s": %s %s option must be used by itself.
 '\%o' is not an ASCII character and thus isn't allowed in key names (no value set) - Tool to manipulate a GConf configuration --ignore-schema-defaults is only relevant with --get, --all-entries, --dump, --recursive-list, --get-list-size or --get-list-element
 --recursive-list should not be used with --get, --set, --unset, --all-entries, --all-dirs, or --search-key
 --set_schema should not be used with --get, --set, --unset, --all-entries, --all-dirs, or --search-key
 <%s> provided but current element does not have type %s <%s> provided but parent <entry> does not have a value <li> has wrong type %s A node has unknown "type" attribute `%s', ignoring A toplevel node in XML file `%s' is <%s> rather than <entry>, ignoring Adding client to server's list failed, CORBA error: %s Adding source `%s'
 Attached schema `%s' to key `%s'
 Attribute "%s" is invalid on <%s> element in this context Backend `%s' failed to return a vtable
 Backend `%s' missing required vtable member `%s'
 Bad XML node: %s Bad address `%s' Bad address `%s': %s Bad key or directory name Bypass server, and access the configuration database directly. Requires that gconfd is not running. CORBA error: %s Can't add notifications to a local configuration source Can't get and set/unset simultaneously
 Can't get type and set/unset simultaneously
 Can't have a period '.' right after a slash '/' Can't have two slashes '/' in a row Can't overwrite existing read-only value Can't read from or write to the XML root directory in the address "%s" Can't set and get/unset simultaneously
 Can't toggle and get/set/unset simultaneously
 Can't use --all-dirs with --get or --set
 Can't use --all-entries with --get or --set
 Can't write to file `%s': %s Cannot find directory %s
 Cannot set schema as value
 Car Type: %s
 Cdr Type: %s
 Change GConf mandatory values Change GConf system values Client options: Config file '%s' is empty Configuration server couldn't be contacted Contacting LDAP server: host '%s', port '%d', base DN '%s' Corrupt data in configuration source database Could not connect to session bus: %s Could not connect to system bus: %s Could not create file '%s', probably because it already exists Could not flush file '%s' to disk: %s Could not flush saved state file '%s' to disk: %s Could not lock temporary file '%s': %s Could not make directory "%s": %s Could not make directory `%s': %s Could not move aside old saved state file '%s': %s Could not open lock directory for %s to remove locks: %s
 Could not open saved state file '%s' for writing: %s Could not remove "%s": %s
 Could not remove file %s: %s
 Could not stat `%s': %s Could not write saved state file '%s' fd: %d: %s Couldn't find the XML root directory in the address `%s' Couldn't get value Couldn't interpret CORBA value for list element Couldn't locate backend module for `%s' Couldn't make sense of CORBA value received in set request for key `%s' Couldn't open path file `%s': %s
 Couldn't resolve address for configuration source Created Evolution/LDAP source using configuration file '%s' D-BUS error: %s DESCRIPTION Daemon failed to acquire gconf service:
%s Daemon failed to connect to the D-BUS daemon:
%s Default Value: %s
 Didn't find car and cdr for XML pair node Didn't understand XML node <%s> inside an XML list node Didn't understand XML node <%s> inside an XML pair node Didn't understand `%s' (expected integer) Didn't understand `%s' (expected real number) Didn't understand `%s' (expected true or false) Didn't understand `%s' (extra trailing characters) Didn't understand `%s' (extra unescaped ')' found inside pair) Didn't understand `%s' (extra unescaped ']' found inside list) Didn't understand `%s' (list must end with a ']') Didn't understand `%s' (list must start with a '[') Didn't understand `%s' (pair must end with a ')') Didn't understand `%s' (pair must start with a '(') Didn't understand `%s' (wrong number of elements) Directory operation on key Directory/file permissions for XML source at root %s are: %o/%o Document `%s' has no top level <%s> node
 Document `%s' has the wrong type of root node (<%s>, should be <%s>)
 Document `%s' is empty?
 Don't understand type `%s'
 Dump to standard output an XML description of all entries under a directory, recursively. Duplicate entry `%s' in `%s', ignoring Element <%s> is not allowed below <%s> Element <%s> is not allowed inside a <%s> element Element <%s> is not allowed inside current element Encoded value is not valid UTF-8 Entry with no name in XML file `%s', ignoring Error associating schema name '%s' with key name '%s': %s
 Error checking existence of `%s': %s Error compiling regex: %s
 Error finding metainfo: %s Error getting default value for `%s': %s Error getting metainfo: %s Error getting new value for "%s": %s Error getting schema at '%s': %s
 Error getting value for `%s': %s Error initializing module `%s': %s
 Error listing dirs in `%s': %s Error listing dirs: %s
 Error loading some configuration sources: %s Error obtaining new value for `%s' after change notification from backend `%s': %s Error obtaining new value for `%s': %s Error opening module `%s': %s
 Error querying LDAP server: %s Error reading "%s": %s
 Error reading saved state file: %s Error releasing lockfile: %s Error removing directory "%s": %s Error removing schema name from '%s': %s
 Error saving GConf tree to '%s': %s
 Error setting schema for `%s': %s Error setting value for `%s': %s Error setting value: %s Error setting value: %s
 Error syncing configuration data: %s Error syncing the XML backend directory cache: %s Error syncing: %s Error syncing: %s
 Error unsetting "%s": %s Error unsetting `%s': %s Error unsetting `%s': %s
 Error while parsing options: %s.
Run '%s --help' to see a full list of available command line options.
 Error writing file "%s": %s Error: %s
 Exiting Expected (%s,%s) pair, got a pair with one or both values missing Expected `%s' got `%s' for key %s Expected bool, got %s Expected float, got %s Expected int, got %s Expected list of %s, got list of %s Expected list, got %s Expected pair of type (%s,%s) got type (%s,%s) Expected pair, got %s Expected schema, got %s Expected string, got %s FILENAME Failed Failed reading default value for schema: %s Failed to access configuration source(s): %s
 Failed to activate configuration server: %s
 Failed to clean up file '%s': %s Failed to close file `%s': %s Failed to close gconfd logfile; data may not have been properly saved (%s) Failed to close new saved state file '%s': %s Failed to contact LDAP server: %s Failed to contact configuration server; the most common cause is a missing or misconfigured D-Bus session bus daemon. See http://projects.gnome.org/gconf/ for information. (Details - %s) Failed to convert IOR '%s' to an object reference Failed to convert object to IOR Failed to create file `%s': %s Failed to create or open '%s' Failed to delete "%s": %s Failed to delete old file `%s': %s Failed to flush client add to saved state file: %s Failed to get IOR for client: %s Failed to get a lock Failed to get all entries in `%s': %s Failed to get bus name for daemon, exiting: %s Failed to get configuration file path from '%s' Failed to get connection to session: %s Failed to get object reference for ConfigServer Failed to get value for `%s': %s
 Failed to give up lock on XML directory "%s": %s Failed to init GConf: %s
 Failed to link '%s' to '%s': %s Failed to load file "%s": %s Failed to load source "%s": %s Failed to lock '%s': probably another process has the lock, or your operating system has NFS file locking misconfigured (%s) Failed to log addition of listener %s (%s); will not be able to restore this listener on gconfd restart, resulting in unreliable notification of configuration changes. Failed to log addition of listener to gconfd logfile; won't be able to re-add the listener if gconfd exits or shuts down (%s) Failed to log removal of listener to gconfd logfile; might erroneously re-add the listener if gconfd exits or shuts down (%s) Failed to log removal of listener to logfile (most likely harmless, may result in a notification weirdly reappearing): %s Failed to move new saved state file into place: %s Failed to move temporary file "%s" to final location "%s": %s Failed to open "%s": %s
 Failed to open `%s': %s
 Failed to open gconfd logfile; won't be able to restore listeners after gconfd shutdown (%s) Failed to open saved state file: %s Failed to parse XML file "%s" Failed to register server object with the D-BUS bus daemon Failed to remove '%s': %s Failed to remove lock directory `%s': %s Failed to remove lock file `%s': %s Failed to rename `%s' to `%s': %s Failed to restore `%s' from `%s': %s Failed to restore original saved state file that had been moved to '%s': %s Failed to set mode on `%s': %s Failed to shut down backend Failed to spawn the configuration server (gconfd): %s
 Failed to stat `%s': %s Failed to sync XML cache contents to disk Failed to sync one or more sources: %s Failed to unset breakage key %s: %s
 Failed to write "%s": %s
 Failed to write XML data to `%s': %s Failed to write byte to pipe file descriptor %d so client program may hang: %s Failed to write client add to saved state file: %s Failed to write file `%s': %s Failed to write some configuration data to disk
 Failure during recursive unset of "%s": %s
 Failure listing entries in `%s': %s
 Failure shutting down configuration server: %s Fatal error: failed to get object reference for ConfigDatabase GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL is set, not installing schemas
 GCONF_DISABLE_MAKEFILE_SCHEMA_UNINSTALL is set, not uninstalling schemas
 GConf Error: %s
 GConf server is not in use, shutting down. GConf warning: failure listing pairs in `%s': %s GConf won't work without dynamic module support (gmodule) GSettings Data Conversion Get a specific element from a list key, numerically indexed. Get the long doc string for a key Get the name of the default source Get the name of the schema applied to this key Get the number of elements in a list key. Get the short doc string for a key GetIOR failed: %s Got %d entries using filter: %s Got a malformed message. Ignore schema defaults when reading values. Ignoring XML node `%s': %s Ignoring XML node with name `%s': %s Ignoring bad car from XML pair: %s Ignoring bad cdr from XML pair: %s Ignoring schema name `%s', invalid: %s Incorrect type for list element in %s Initializing Markup backend module Initializing XML backend module Installation options: Installed schema `%s' for locale `%s'
 Integer `%s' is too large or small Invalid UTF-8 in gettext domain for schema Invalid UTF-8 in locale for schema Invalid UTF-8 in long description for schema Invalid UTF-8 in owner for schema Invalid UTF-8 in short description for schema Invalid UTF-8 in string value in '%s' Invalid cdr_type "%s" on <%s> Invalid first-element type "%s" on <%s> Invalid list_type "%s" on <%s> Invalid ltype "%s" on <%s> Invalid type (list, pair, or unknown) in a list node Key %s is not a list.
 Key `%s' listed as schema for key `%s' actually stores type `%s' Key is NULL Key operation on directory Key type options: Key/directory may not end with a slash '/' Launch the configuration server (gconfd). (Normally happens automatically when needed.) Line %d character %d: %s List Type: %s
 List contains a badly-typed node (%s, should be %s) List index is out of bounds.
 List index must be non-negative.
 List type must be a primitive type: string, int, float or bool
 Listener ID %lu doesn't exist Load from the specified file an XML description of values and set them relative to a directory. Load/Save options: Long Desc: %s
 Migrates user settings from GConf to dconf Missing both car and cdr values from pair in XML file Missing car from pair of values in XML file Missing cdr from pair of values in XML file Must begin with a slash '/' Must set the GCONF_CONFIG_SOURCE environment variable
 Must specify a PCRE regex to search for.
 Must specify a key from which to get list element.
 Must specify a key or keys on the command line
 Must specify a key or keys to get
 Must specify a key or keys to get type
 Must specify a key pattern to search for.
 Must specify a key to lookup size of.
 Must specify a schema name followed by the key name to apply it to
 Must specify a type when setting a value
 Must specify alternating keys/values as arguments
 Must specify key (schema name) as the only argument
 Must specify keys to unapply schema from
 Must specify list index.
 Must specify one or more directories to dump.
 Must specify one or more directories to get key/value pairs from.
 Must specify one or more directories to get subdirs from.
 Must specify one or more directories to recursively list.
 Must specify one or more keys as arguments
 Must specify one or more keys to recursively unset.
 Must specify one or more keys to unset.
 Must specify some directories to break
 Must specify some keys to break
 Must specify some schema files to install
 No "%s" attribute on element <%s> No "filter" attribute specified on <template> in '%s' No "type" attribute for <%s> node No "value" attribute for node No '/' in key "%s" No <template> specified in '%s' No D-BUS daemon running
 No LDAP server or base DN specified in '%s' No configuration files found. Trying to use the default configuration source `%s' No configuration source addresses successfully resolved. Can't load or store configuration data No configuration sources in the source path. Configuration won't be saved; edit %s%s No database available to save your configuration No doc string stored in schema at '%s'
 No schema known for `%s'
 No schema stored at '%s'
 No such file `%s'
 No text is allowed inside element <%s> No value found for key %s
 No value set for `%s'
 No value to set for key: `%s'
 No writable configuration sources successfully resolved. May be unable to save some configuration changes Not a boolean value: %s
 Not running within active session Notification on %s doesn't exist OWNER Object Activation Framework error Operation not allowed without configuration server Outermost element in menu file must be <gconf> not <%s> Owner: %s
 Pair car type must be a primitive type: string, int, float or bool
 Pair cdr type must be a primitive type: string, int, float or bool
 Parse error Permission denied Print all key/value pairs in a directory. Print all subdirectories and entries under a directory, recursively. Print all subdirectories in a directory. Print the data type of a key to standard output. Print the value of a key to standard output. Print version Privileges are required to change GConf mandatory values Privileges are required to change GConf system values Properly installs schema files on the command line into the database. Specify a custom configuration source in the GCONF_CONFIG_SOURCE environment variable, or set set the variable to an empty string to use the default configuration source. Properly uninstalls schema files on the command line from the database. GCONF_CONFIG_SOURCE environment variable should be set to a non-default configuration source or set to the empty string to use the default. Quoted string doesn't begin with a quotation mark Quoted string doesn't end with a quotation mark Read error on file `%s': %s
 Received invalid value in set request Received list from gconfd with a bad list type Received request to drop all cached data Received request to sync synchronously Recursively unset all keys at or below the key/directory names on the command line Remove any schema name applied to the given keys Remove directory operation is no longer supported, just remove all the values in the directory Resolved address "%s" to a partially writable configuration source at position %d Resolved address "%s" to a read-only configuration source at position %d Resolved address "%s" to a writable configuration source at position %d Return 0 if gconfd is running, 2 if not. Return 0 if the directory exists, 2 if it does not. Returning exception: %s Root node of '%s' must be <evoldap>, not <%s> Run '%s --help' to see a full list of available command line options.
 SIGHUP received, reloading all databases SOURCE Schema `%s' specified for `%s' stores a non-schema value Schema contains invalid UTF-8 Schema options: Schema specifies type list but doesn't specify the type of the list elements Schema specifies type pair but doesn't specify the type of the car/cdr elements Search for a key, recursively. Searching for entries using filter: %s Server couldn't resolve the address `%s' Server options: Server ping error: %s Set a key to a value and sync. Use with --type. Set a schema and sync. Use with --short-desc, --long-desc, --owner, and --type. Short Desc: %s
 Show client options Show installation options Show key type options Show load/save options Show schema options Show server options Show test options Shut down gconfd. DON'T USE THIS OPTION WITHOUT GOOD REASON. Shutdown error: %s
 Shutdown request received Some client removed itself from the GConf server when it hadn't been added. Specify a configuration source to use rather than the default path Specify a schema file to be installed Specify a several-line description to go in a schema. Specify a short half-line description to go in a schema. Specify the owner of a schema Specify the schema name followed by the key to apply the schema name to Specify the type of the car pair value being set, or the type of the value a schema describes. Unique abbreviations OK. Specify the type of the cdr pair value being set, or the type of the value a schema describes. Unique abbreviations OK. Specify the type of the list value being set, or the type of the value a schema describes. Unique abbreviations OK. Specify the type of the value being set, or the type of the value a schema describes. Unique abbreviations OK. Success Test options: Text contains invalid UTF-8 The '/' name can only be a directory, not a key The GConf daemon is currently shutting down. Toggles a boolean key. Torture-test an application by setting and unsetting a bunch of keys inside the directories on the command line. Torture-test an application by setting and unsetting a bunch of values of different types for keys on the command line. Trying to break your application by setting bad values for key:
  %s
 Trying to break your application by setting bad values for keys in directory:
  %s
 Two <car> elements given for same pair Two <cdr> elements given for same pair Two <default> elements below a <local_schema> Two <longdesc> elements below a <local_schema> Type mismatch Type: %s
 Unable to open saved state file '%s': %s Unable to parse XML file '%s' Unable to remove directory `%s' from the XML backend cache, because it has not been successfully synced to disk Unable to restore a listener on address '%s', couldn't resolve the database Unable to store a value at key '%s', as the configuration server has no writable databases. There are some common causes of this problem: 1) your configuration path file %s/path doesn't contain any databases or wasn't found 2) somehow we mistakenly created two gconfd processes 3) your operating system is misconfigured so NFS file locking doesn't work in your home directory or 4) your NFS client machine crashed and didn't properly notify the server on reboot that file locks should be dropped. If you have two gconfd processes (or had two at the time the second was launched), logging out, killing all copies of gconfd, and logging back in may help. If you have stale locks, remove ~/.gconf*/*lock. Perhaps the problem is that you attempted to use GConf from two machines at once, and ORBit still has its default configuration that prevents remote CORBA connections - put "ORBIIOPIPv4=1" in /etc/orbitrc. As always, check the user.* syslog for details on problems gconfd encountered. There can only be one gconfd per home directory, and it must own a lockfile in ~/.gconfd and also lockfiles in individual storage locations such as ~/.gconf Uninstalled schema `%s' from locale `%s'
 Unknown error Unknown error %s: %s Unknown value "%s" for "%s" attribute on element <%s> Unload a set of values described in an XML file. Unloading XML backend module. Unloading text markup backend module. Unset Unset the keys on the command line Usage: %s <dir>
 Usage: %s <dir>
  Merges a markup backend filesystem hierarchy like:
    dir/%%gconf.xml
        subdir1/%%gconf.xml
        subdir2/%%gconf.xml
  to:
    dir/%%gconf-tree.xml
 Value at '%s' is not a schema
 Value for `%s' set in a read-only source at the front of your configuration path Value type is only relevant when setting a value
 WARNING: <locale> node has no `name="locale"' attribute, ignoring
 WARNING: Failed to parse boolean value `%s'
 WARNING: Failed to parse default value `%s' for schema (%s)
 WARNING: Failed to parse float value `%s'
 WARNING: Failed to parse int value `%s'
 WARNING: Failed to parse string value `%s'
 WARNING: Invalid node <%s> in a <locale> node
 WARNING: You cannot set a default value for a schema
 WARNING: car_type can only be int, float, string or bool and not `%s'
 WARNING: cdr_type can only be int, float, string or bool and not `%s'
 WARNING: empty <applyto> node WARNING: failed to associate schema `%s' with key `%s': %s
 WARNING: failed to install schema `%s', locale `%s': %s
 WARNING: failed to parse type name `%s'
 WARNING: failed to uninstall schema `%s', locale `%s': %s
 WARNING: gconftool internal error, unknown GConfValueType
 WARNING: invalid or missing car_type or cdr_type for schema (%s)
 WARNING: invalid or missing list_type for schema (%s)
 WARNING: invalid or missing type for schema (%s)
 WARNING: key specified (%s) for schema under a <value> - ignoring
 WARNING: list_type can only be int, float, string or bool and not `%s'
 WARNING: multiple <locale> nodes for locale `%s', ignoring all past first
 WARNING: must have a child node under <value>
 WARNING: must specify both a <car> and a <cdr> in a <pair>
 WARNING: no <car_type> specified for schema of type pair
 WARNING: no <cdr_type> specified for schema of type pair
 WARNING: no <list_type> specified for schema of type list
 WARNING: no key specified for schema
 WARNING: node <%s> below <%s> not understood
 WARNING: node <%s> not understood
 WARNING: node <%s> not understood below <%s>
 WARNING: node <%s> not understood below <schema>
 We didn't have the lock on file `%s', but we should have When setting a list you must specify a primitive list-type
 When setting a pair you must specify a primitive car-type and cdr-type
 XML filename `%s' is a directory You must have at least one <locale> entry in a <schema>
 You must specify a configuration source with --config-source when using --direct
 [FILE...]|[KEY...]|[DIR...] `%c' is an invalid character in a configuration storage address `%c' is an invalid character in key/directory names couldn't contact ORB to resolve existing gconfd object reference couldn't create directory `%s': %s gconfd compiled with debugging; trying to load gconf.path from the source directory int|bool|float|string int|bool|float|string|list|pair none parsing XML file: lists and pairs may not be placed inside a pair starting (version %s), pid %u user '%s' Project-Id-Version: gconf
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-03 21:36+0000
PO-Revision-Date: 2011-07-18 21:33+0000
Last-Translator: Jônatas Pedraza <jonatas.nona@gmail.com>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:26+0000
X-Generator: Launchpad (build 18115)
 
%s
 "%s": %s A opção %s deve ser utilizada sozinha.
 "\%o" não é um caractere ASCII, logo não é permitido em nomes de chave (sem valor definido) - Ferramenta para manipular as configurações do GConf --ignore-schema-defaults só é relevante com --get, --all-entries, --dump, --recursive-list, --get-list-size ou --get-list-element
 --recursive-list não deve ser usado com --get, --set, --unset, --all-entries, --all-dirs, ou --search-key
 --set_schema não deve ser usado com --get, --set, --unset, --all-entries, --all-dirs, ou --search-key
 Foi fornecido <%s>, mas o elemento atual não tem o tipo %s Foi fornecido <%s>, mas o <entry> pai não tem um valor <li> tem um tipo %s errado Um nó tem atributo "tipo" desconhecido "%s", ignorando Um nó de nível superior no arquivo XML "%s" é <%s> e não <entry>, ignorando Falha ao incluir cliente na lista do servidor, erro CORBA: %s Incluindo fonte "%s"
 Esquema "%s" anexado à chave "%s"
 O atributo "%s" é inválido no elemento <%s> nesse contexto O suporte "%s" falhou ao retornar uma vtable
 O suporte "%s" não tem o membro vtable requerido "%s"
 Nó XML defeituoso: %s Endereço inválido "%s" Endereço inválido "%s": %s Chave ou diretório inválido Ignora o servidor e acessa o banco de dados de configuração diretamente. Requer que o gconfd não esteja em execução. Erro CORBA: %s Não é possível incluir notificações para uma fonte de configurações local Não é possível obter e definir/limpar ao mesmo tempo
 Não é possível obter o tipo e definir/limpar ao mesmo tempo
 Não pode ter um ponto "." logo após uma barra "/" Não pode ter duas barras "/" em uma linha Não é possível sobrescrever valor somente leitura já existente Não é possível ler ou escrever no diretório raiz XML no endereço "%s" Não é possível atribuir e definir/limpar ao mesmo tempo
 Não é possível alternar e obter/definir/limpar ao mesmo tempo
 Não é possível usar --all-dirs com --get ou --set
 Não é possível usar --all-entries com --get ou --set
 Não é possível escrever no arquivo "%s": %s Não foi possível localizar o diretório %s
 Não é possível definir um esquema como valor
 Tipo do car: %s
 Tipo do cdr: %s
 Modifica valores obrigatórios do GConf Modifica valores de sistema do GConf Opções do cliente: O arquivo de configuração "%s" está vazio Não foi possível contatar o servidor de configuração Contactando o servidor LDAP: máquina "%s", porta "%d", DN base "%s" Dados corrompidos no banco de dados da fonte de configuração Não foi possível conectar ao barramento da sessão: %s Não foi possível conectar ao barramento do sistema: %s Não foi possível criar o arquivo "%s", provavelmente porque ele já existe Não foi possível liberar o arquivo "%s" para o disco: %s Não foi possível liberar o arquivo de estado "%s" salvo para o disco: %s Não foi possível bloquear arquivo temporário "%s": %s Não foi possível criar o diretório "%s": %s Não foi possível criar o diretório "%s": %s Não foi possível mover arquivo de estado antigo "%s": %s Não foi possível abrir o diretório de bloqueio para %s remover as travas: %s
 Não foi possível abrir arquivo de estado "%s" para escrita: %s Não foi possível remover "%s": %s
 Não foi possível remover o arquivo %s: %s
 Não foi possível obter status de "%s": %s Não foi possível escrever arquivo de estado "%s" fd: %d: %s Não foi possível localizar o diretório raiz XML no endereço "%s" Não foi possível obter o valor Nao foi possível interpretar valor CORBA para elemento da lista Não foi possível localizar módulo para "%s" Não foi possível interpretar o valor CORBA recebido na requisição de definição para a chave "%s" Não foi possível abrir arquivo de caminhos "%s": %s
 Não foi possível resolver endereço para a fonte de configuração Fonte Evolution/LDAP criada usando o arquivo de configuração "%s" Erro D-BUS: %s DESCRIÇÃO O servidor falhou ao adquirir o serviço gconf:
%s O servidor falhou ao conectar no servidor D-BUS:
%s Valor padrão: %s
 Não foi possível localizar car e cdr para o nó de par XML Não foi possível entender o nó XML <%s> dentro de uma lista de nós XML Não foi possível entender o nó XML <%s> dentro de um nó de par XML Não foi possível entender "%s" (era esperado um inteiro) Não foi possível entender "%s" (era esperado um número real) Não foi possível entender "%s": esperava-se "true" (verdadeiro) ou "false" (falso) Não foi possível entender "%s" (caracteres finais extras) Não foi possível entender "%s" (")" extra localizado em um par) Não foi possível entender "%s" ("]" extra localizado em uma lista) Não foi possível entender "%s" (lista deve terminar com um "]") Não foi possível entender "%s" (lista deve começar com um "[") Não foi possível entender "%s" (par deve terminar com um ")") Não foi possível entender "%s" (par deve começar com um "(") Não foi possível entender "%s" (número de elementos errado) Operação de diretório em chave As permissões de diretório/arquivo para a fonte XML na raiz %s são: %o/%o O documento "%s" não possui nó <%s> de nível principal
 O documento "%s" tem o tipo errado de nó raiz (<%s>, deveria ser <%s>)
 O documento "%s" está vazio?
 Não é possível entender tipo "%s"
 Imprime uma descrição XML de todas as entradas num diretório, recursivamente. Entrada duplicada "%s" em "%s", ignorando O elemento <%s> não é permitido abaixo de <%s> O elemento <%s> não é permitido dentro de um elemento <%s> O elemento <%s> não é permitido dentro do elemento atual O valor codificado não é um UTF-8 válido Entrada sem nome no arquivo XML "%s", ignorando Erro ao associar nome de esquema "%s" com nome de chave "%s": %s
 Erro ao verificar existência de "%s": %s Erro ao compilar a expressão regular: %s
 Erro ao localizar metainformações: %s Erro ao obter o valor padrão para "%s": %s Erro ao obter metainformações: %s Erro ao obter o valor para "%s": %s Erro ao obter o esquema em "%s": %s
 Erro ao obter o valor de "%s": %s Erro ao iniciar o módulo "%s": %s
 Erro ao listar diretórios em "%s": %s Erro ao listar diretórios: %s
 Erro ao carregar algumas fontes de configuração: %s Erro ao obter um novo valor para "%s" após a notificação de alteração do suporte "%s": %s Erro ao obter o novo valor para "%s": %s Erro ao abrir o módulo "%s": %s
 Erro ao consultar o servidor LDAP: %s Erro ao ler "%s": %s
 Erro ao ler arquivo de estado: %s Falha ao liberar arquivo de bloqueio: %s Erro ao remover diretório "%s": %s Erro removendo nome de esquema de "%s": %s
 Erro ao salvar a árvore GConf para "%s": %s
 Erro ao atribuir esquema para "%s": %s Erro ao atribuir valor para "%s": %s Erro ao atribuir valor: %s Erro ao atribuir valor: %s
 Erro ao sincronizar dados de configuração: %s Erro ao sincronizar o diretório de cache de suporte XML: %s Erro ao sincronizar: %s Erro ao sincronizar: %s
 Erro ao limpar "%s": %s Erro ao limpar "%s": %s Erro ao limpar "%s": %s
 Erro de sintaxe nas opções: %s.
Execute "%s --help" para ver a lista completa de opções de linha de comando.
 Erro ao gravar o arquivo "%s": %s Erro: %s
 Saindo Par (%s,%s) esperado, recebido um par com um ou ambos os valores faltando Esperado "%s", recebido "%s" para a chave %s Booleano esperado, recebido %s Ponto flutuante esperado, recebido %s Inteiro esperado, recebido %s Lista de %s esperada, recebida lista de %s Lista esperada, recebido %s Par do tipo (%s,%s) esperado, recebido tipo (%s,%s) Par esperado, recebida %s Esquema esperado, recebido %s Texto esperado, recebido %s ARQUIVO Falha Falha ao ler valor padrão para esquema: %s Falha ao acessar a(s) fonte(s) de configuração(ões): %s
 Falha ao ativar servidor de configuração: %s
 Falha ao limpar arquivo "%s": %s Falha ao fechar arquivo "%s": %s Falha ao fechar arquivo de registro do gconfd; os dados podem não ter sido salvos corretamente (%s) Falha ao fechar novo arquivo de estado "%s": %s Falha ao contatar o servidor LDAP: %s Falha ao acessar o servidor de configuração, a causa mais comum é a falta de configuração ou desconfiguração da sessão D-BUs para troca de informações. Acesse http://projects.gnome.org/gconf/ para mais informações (Detalhes - %s) Falha ao converter IOR "%s" em uma referência a objeto Falha ao converter objeto para IOR Falha ao criar o arquivo "%s": %s Falha ao criar ou abrir "%s" Falha ao excluir "%s": %s Falha ao excluir arquivo antigo "%s": %s Falha ao descarregar inclusão de cliente no arquivo de estado: %s Falha ao obter o IOR para o cliente: %s Falha ao obter um bloqueio Falha ao obter todas as entradas em "%s": %s Falha ao obter o nome do barramento para o daemon, saindo: %s Falha ao obter o arquivo do caminho de configuração a partir de "%s" Falha ao obter uma conexão à sessão: %s Falha ao obter a referência de objeto para ConfigServer Falha ao obter o valor para "%s": %s
 Falha ao liberar bloqueio no diretório XML "%s": %s Falha ao iniciar o GConf: %s
 Falha ao vincular "%s" a "%s": %s Falha ao carregar arquivo "%s": %s Falha ao carregar fonte "%s": %s Falha ao bloquear "%s": provavelmente um outro processo possui o bloqueio ou seu sistema operacional tem o bloqueio de arquivos NFS desconfigurado (%s) Falha ao registrar inclusão do atendente %s (%s); não será possível restaurar este atendente ao reiniciar o gconfd, tendo como resultado uma notificação não confiável de alterações de configuração. Falha ao registrar adição do atendente no arquivo de registro do gconfd; o atendente não será adicionado novamente se o gconfd sair ou for desligado (%s) Falha ao registrar remoção do atendente no arquivo de registro do gconfd; pode ocorrer erroneamente uma nova adição do atendente se o gconfd sair ou for desligado (%s) Falha ao registrar remoção de atendente no arquivo de registro (provavelmente inofensivo, pode resultar em uma notificação reaparecendo de forma estranha): %s Falha ao mover novo arquivo de estado no local: %s Falha ao mover o arquivo temporário "%s" para o destino final "%s": %s Falha ao abrir "%s": %s
 Falha ao abrir "%s": %s
 Falha ao abrir arquivo de registro do gconfd; não será possível restaurar atendentes após o gconfd desligar (%s) Falha ao abrir arquivo de estado: %s Falha ao analisar o arquivo XML "%s" Falha ao registrar objeto de servidor com o servidor D-BUS Falha ao remover "%s": %s Falha ao remover diretório de bloqueios "%s": %s Falha ao remover arquivo de bloqueio "%s": %s Falha ao renomear "%s" para "%s": %s Falha ao restaurar "%s" de "%s": %s Falha ao restaurar arquivo de estado original que foi movido para "%s": %s Falha ao definir o modo em "%s": %s Falha ao desligar módulo de suporte Falha ao criar o servidor de configuração (gconfd): %s
 Falha ao obter status "%s": %s Falha ao sincronizar o conteúdo do cache XML com o disco Falha ao sincronizar uma ou mais fontes: %s Falha ao limpar chave de quebra %s: %s
 Falha ao gravar "%s": %s
 Falha ao gravar dados XML em "%s": %s Falha ao gravar byte no canal descritor de arquivo %d. Logo, o programa cliente pode travar: %s Falha ao escrever inclusão de cliente no arquivo de estado: %s Falha ao gravar arquivo "%s": %s Falha ao gravar alguns dados de configuração no disco
 Falha durante a limpar recursivamentr "%s": %s
 Falha ao construir a lista de entradas em "%s": %s
 Falha ao desligar servidor de configuração: %s Erro fatal: falha ao obter a referência de objeto para ConfigDatabase GCONF_DISABLE_MAKEFILE_SCHEMA_INSTALL definida, não instalando esquemas
 GCONF_DISABLE_MAKEFILE_SCHEMA_UNINSTALL definida; não desinstalando esquemas
 Erro do GConf: %s
 O servidor GConf não está sendo usado, desligando. Aviso do GConf: falha ao construir a lista de pares em "%s": %s GConf não funciona sem suporte a módulos dinâmicos (gmodule) Conversão de dados GSettings Obtém um determinado elemento de uma lista de chaves indexada numericamente. Obtém a descrição longa de uma chave Obtém o nome da fonte padrão Obtém o nome do esquema aplicado a esta chave Obtém o número de elementos em uma lista de chaves. Obtém a descrição curta de uma chave GetIOR falhou: %s %d entradas obtidas usando o filtro: %s Obteve uma mensagem malformada. Ignora os padrões do esquema ao ler os valores. Ignorando nó XML "%s": %s Ignorando nó XML com o nome "%s": %s Ignorando car defeituoso do par XML: %s Ignorando cdr defeituoso do par XML: %s Ignorando nome de esquema "%s", inválido: %s Tipo incorreto para elemento da lista em %s Iniciando o módulo de suporte de marcação Iniciando o módulo de suporte XML Opções de instalação: Instalado esquema "%s" para locale "%s"
 O inteiro "%s" é muito grande ou pequeno UTF-8 inválido no domínio gettext para esquema UTF-8 inválido no locale para esquema UTF-8 inválido na descrição longa para esquema UTF-8 inválido no proprietário para esquema UTF-8 inválido na descrição curta para esquema UTF-8 inválido no valor texto em "%s" cdr_type "%s" inválido em <%s> Tipo do primeiro elemento "%s" inválido em <%s> list_type "%s" inválido em <%s> ltype "%s" inválido em <%s> Tipo inválido (lista, par ou desconhecido) em uma lista de nós A chave %s não é uma lista.
 A chave "%s", listada como esquema para a chave "%s", realmente armazena o tipo "%s" A chave é NULA Operação de chave em diretório Opções de tipo de chave: Chave/diretório não pode terminar com uma barra "/" Executa o servidor de configuração (gconfd). (Normalmente acontece automaticamente quando necessário.) Linha %d caractere %d: %s Tipo de lista: %s
 Lista contém um nó com tipo inválido (%s, deveria ser %s) O índice da lista está fora dos limites.
 O índice da lista deve ser não-negativo.
 Tipo de lista deve ser um tipo primitivo: string, int, float ou bool
 ID de atendente %lu não existe Carrega do arquivo especificado uma descrição XML dos valores e os define em relação a um diretório. Carregar/salvar opções: Descr. longa: %s
 Migra configurações do usuário do GConf para o dconf Faltam car e cdr do par de valores no arquivo XML Falta car do par de valores no arquivo XML Falta cdr do par de valores no arquivo XML Deve começar com uma barra "/" A variável de ambiente GCONF_CONFIG_SOURCE deve ser definida
 É necessário especificar uma expressão regular PCRE para efetuar pesquisa.
 É necessário especificar uma chave de onde obter o elemento de lista.
 É necessário especificar uma chave ou chaves na linha de comando
 É necessário especificar uma ou mais chaves para obter
 É necessário especificar uma ou mais chaves para obter o tipo
 É necessário especificar um padrão de chave para efetuar pesquisa.
 É necessário especificar uma chave para pesquisar o tamanho.
 É necessário especificar um nome de esquema seguido por um nome de chave onde aplicá-lo
 Um tipo deve ser especificado ao atribuir um valor
 É necessário especificar chaves/valores alternados como argumentos
 É necessário especificar a chave (nome do esquema) como único argumento
 É necessário especificar uma chave para remover o esquema
 É necessário especificar o índice da lista.
 É necessário especificar um ou mais diretórios para descarregar.
 É necessário especificar um ou mais diretórios de onde obter pares chave/valor.
 É necessário especificar um ou mais diretórios de onde obter subdiretórios.
 É necessário especificar um ou mais diretórios para utilizar uma lista recursiva.
 É necessário especificar uma ou mais chaves como argumentos
 É necessário especificar uma ou mais chaves para limpar recursivamente.
 É necessário especificar uma ou mais chaves para limpar.
 É necessário especificar alguns diretórios para serem quebrados
 É necessário especificar algumas chaves para serem quebradas
 É necessário especificar alguns arquivos de esquema a instalar
 Nenhum atributo "%s" no elemento <%s> Nenhum atributo "filter" especificado em <template> em "%s" Sem atributo "tipo" para o nó <%s> Sem atributo "valor" para o nó Nenhuma "/" na chave "%s" Nenhum <template> especificado em "%s" Servidor D-BUS não está em execução
 Nenhum servidor LDAP no DN base especificado em "%s" Não foram localizados arquivos de configuração. Tentando usar a fonte de configuração padrão "%s" Nenhum endereço fonte de configuração foi resolvido com sucesso. Não é possível carregar ou armazenar dados de configuração Nenhuma fonte de configuração no caminho de fonte. A configuração não será salva; edite %s%s Sem banco de dados disponível para salvar sua configuração Nenhuma linha de documentação digitada no esquema em "%s"
 Nenhum esquema conhecido para "%s"
 Não existe esquema armazenado em "%s"
 Nenhum arquivo "%s"
 Nenhum texto é permitido dentro do elemento <%s> Nenhum valor localizado para a chave %s
 Sem valor definido para "%s"
 Sem valor para definir na chave: "%s"
 Nenhuma fonte de configuração com permissão de escrita foi resolvida com sucesso. Pode não ser possível salvar algumas alterações de configuração Não é um valor booleano: %s
 Não executando dentro da sessão ativa Notificação sobre %s não existe PROPRIETÁRIO Erro do Object Activation Framework Operação não permitida sem servidor de configuração O elemento mais externo no arquivo de menu deve ser <gconf>, não <%s> Dono: %s
 Tipo de car de par deve ser um tipo primitivo: string, int, float ou bool
 Tipo de cdr de par deve ser um tipo primitivo: string, int, float ou bool
 Erro de análise Permissão negada Imprime todos os pares chave/valor em um diretório. Imprime todos os subdiretórios e entradas num diretório, recursivamente. Imprime todos os subdiretórios de um diretório. Imprime o tipo de dado de uma chave para a saída padrão. Imprime o valor de uma chave para a saída padrão. Imprimir versão Privilégios especiais são necessários para modificar valores obrigatórios do GConf Privilégios especiais são necessários para modificar valores de sistema do GConf Instala corretamente arquivos de esquema pela linha de comando no banco de dados. Especifica uma fonte de configuração personalizada na variável de ambiente GCONF_CONFIG_SOURCE, ou define a variável como um texto vazio para usar a fonte de configuração padrão. Desinstala corretamente arquivos de esquema pela linha de comando no banco de dados. A variável de ambiente GCONF_CONFIG_SOURCE deve ser definida como uma fonte de configuração não-padrão ou como texto vazio para usar o padrão. A cadeia entre aspas não começa com aspas A cadeia entre aspas não termina com aspas Erro de leitura no arquivo "%s": %s
 Valor inválido recebido na requisição de definição Lista recebida do gconfd com um tipo de lista inválido Recebida a requisição para descartar todos os dados do cache Recebida a requisição para sincronizar sincronamente Limpar recursivamente todas as chaves em ou abaixo dos nomes de chave/diretório na linha de comando Remove qualquer nome de esquema aplicado às chaves dadas A operação de remoção de diretório não é mais suportada, apenas remova todos os valores no diretório Endereço "%s" resolvido para uma fonte de configuração com permissões parciais de escrita na posição %d Endereço "%s" resolvido para uma fonte de configuração com permissões apenas de leitura na posição %d O endereço "%s" resolvido para uma fonte de configuração com permissões de escrita na posição %d Retorna 0 se o gconfd está executando, 2 caso contrário. Retorna 0 se o diretório existe, 2 caso contrário. Retornando exceção: %s Nó raiz de "%s" deve ser <evoldap>, não <%s> Execute "%s --help" para ver a lista completa de opções de linha de comando.
 SIGHUP recebido, recarregando todos os bancos de dados FONTE O esquema "%s" especificado para "%s" armazena um valor de não-esquema O esquema contém UTF-8 inválido Opções de esquema: O esquema especifica o tipo lista mas não especifica o tipo dos elementos da lista O esquema especifica o tipo par, mas não especifica o tipo dos elementos car/cdr Pesquisar por uma chave, recursivamente. Procurando por entradas usando o filtro: %s O servidor não conseguiu resolver o endereço "%s" Opções do servidor: Erro no ping do servidor: %s Define uma chave como um valor e sincroniza. Use com --type. Define um esquema e sincroniza. Use com --short-desc, --long-desc, --owner e --type. Descr. curta: %s
 Mostrar opções do cliente Mostrar opções de instalação Mostrar opções de tipo de chave Mostrar as opções de carregar/salvar Mostrar as opções do esquema Mostrar opções do servidor Mostrar opções de teste Desliga o gconfd. NÃO USE ESTA OPÇÃO SEM UMA BOA RAZÃO. Erro ao desligar: %s
 Recebida requisição para desligar Algum cliente se excluiu do servidor GConf sem ter sido incluído. Especifica uma fonte de configuração a ser usada no lugar do caminho padrão Especifica um arquivo de esquema a ser instalado Especifica uma descrição com várias linhas para um esquema. Especifica uma breve descrição de meia linha para um esquema. Especifica o proprietário de um esquema Especifica o nome do esquema seguido pela chave à qual se deseja aplicar este nome Especifica o tipo do valor do car do par sendo definido ou o tipo do valor que um esquema descreve. Abreviações únicas são permitidas. Especifica o tipo do valor do cdr do par sendo definido ou o tipo do valor que um esquema descreve. Abreviações únicas são permitidas. Especifica o tipo do valor de lista sendo definido ou o tipo do valor que um esquema descreve. Abreviações únicas são permitidas. Especifica o tipo do valor sendo definido ou o tipo do valor que um esquema descreve. Abreviações únicas são permitidas. Sucesso Opções de teste: Texto contém UTF-8 inválido O nome "/" pode ser apenas um diretório, não uma chave O servidor GConf está sendo desligado. Alterna uma chave booleana. Testa intensivamente um aplicativo definindo e removendo várias chaves dentro de diretórios na linha de comando. Testa intensivamente um aplicativo definindo e removendo vários valores de tipos diferentes para as chaves na linha de comando. Tentando quebrar o seu aplicativo definindo valores inválidos para a chave:
  %s
 Tentando quebrar o seu aplicativo definindo valores inválidos para as chaves no diretório:
  %s
 Dois elementos <car> dados para o mesmo par Dois elementos <cdr> dados para o mesmo par Dois elementos <default> abaixo de um <local_schema> Dois elementos <longdesc> abaixo de um <local_schema> Tipos incompatíveis Tipo: %s
 Não é possível abrir arquivo de estado "%s": %s Não foi possível analisar o arquivo XML "%s" Não é possível remover o diretório "%s" do cache do módulo de suporte XML, pois ele não foi sincronizado corretamente com o disco Não é possível restaurar um atendente no endereço "%s", não foi possível resolver o banco de dados Não é possível armazenar um valor na chave "%s" pois o servidor de configuração não possui bancos de dados com permissões de escrita. Existem algumas causas comuns para este problema: 1) seu arquivo de caminho de configuração %s/caminho não contém bases de dados ou não foi localizado 2) de alguma forma nós criamos acidentalmente duas instâncias do gconfd 3) seu sistema operacional está desconfigurado fazendo com que o bloqueio de arquivos NFS não funcione no seu diretório pessoal ou 4) sua máquina cliente de NFS travou e não notificou o servidor na reinicialização que os bloqueios de arquivos deviam ser desfeitos. Se você tem dois processos do gconfd (ou tinha dois quando o segundo foi iniciado), sair da sessão, matar todas as cópias do gconfd e entrar novamente na sessão pode ajudar. Se você tem arquivos de bloqueio deixados para trás, remova ~/.gconf*/*lock. Talvez o problema seja porque você tentou usar o GConf de duas máquinas ao mesmo tempo e o ORBit ainda está configurado para evitar conexões CORBA remotas - coloque "ORBIIOPIPv4=1" no /etc/orbitrc. Como sempre, confira o syslog user.* para obter detalhes sobre os problemas encontrados pelo gconf. Só pode haver um gconfd por diretório pessoal e ele precisa ser dono de um arquivo de bloqueio no ~/.gconfd e também arquivos de bloqueio em locais onde guarda configurações individuais como o ~/.gconf Esquema "%s" desinstalado da localidade "%s"
 Erro desconhecido Erro desconhecido %s: %s Valor "%s" desconhecido para o atributo "%s" no elemento <%s> Descarrega um conjunto de valores descritos num arquivo XML. Descarregando o módulo de suporte XML. Descarregando o módulo de suporte de marcação de texto. Limpar Limpa as chaves na linha de comando Uso: %s <dir>
 Uso: %s <dir>
  Mescla uma hierarquia de sistema de arquivos de suporte em etiquetas do tipo:
    dir/%%gconf.xml
        subdir1/%%gconf.xml
        subdir2/%%gconf.xml
  para:
    dir/%%gconf-tree.xml
 Valor em "%s" não é um esquema
 valor para "%s" definido em uma fonte somente-leitura no início do seu caminho de configuração Tipo do valor só é relevante ao atribuir um valor
 AVISO: nó <locale> não possui atributo "name="locale"", ignorando
 AVISO: Falha ao analisar o valor booleano "%s"
 AVISO: Falha ao analisar valor padrão "%s" para o esquema (%s)
 AVISO: Falha ao analisar o ponto flutuante "%s"
 AVISO: Falha ao analisar o valor "%s" interno
 AVISO: Falha ao analisar o valor texto "%s"
 AVISO: Nó inválido <%s> em um nó <locale>
 AVISO: Você não pode definir um valor padrão para um esquema
 AVISO: tipo de car só pode ser int, float, string ou bool e não "%s"
 AVISO: tipo de cdr só pode ser int, float, string ou bool e não "%s"
 AVISO: nó <applyto> vazio AVISO: falha ao associar esquema "%s" com a chave "%s": %s
 AVISO: falha ao instalar esquema "%s" da localidade "%s": %s
 AVISO: falha ao analisar nome do tipo "%s"
 AVISO: falha ao desinstalar esquema "%s" locale "%s": %s
 AVISO: Erro interno do gconftool, GConfValueType desconhecido
 AVISO: tipo de car ou de cdr inválido ou faltante para o esquema (%s)
 AVISO: tipo de lista inválido ou faltante para o esquema (%s)
 AVISO: tipo inválido ou faltante para o esquema (%s)
 AVISO: a chave (%s) especificada para esquema está abaixo de <value> - ignorando
 AVISO: tipo de lista só pode ser int, float, string ou bool e não "%s"
 AVISO: múltiplos nós <locale> para o locale "%s", ignorando todos após o primeiro
 AVISO: deve haver um nó filho abaixo de <value>
 AVISO: devem ser especificados tanto um <car> quanto um <cdr> em um <pair>
 AVISO: não foi especificado <car_type> para esquema de tipo par
 AVISO: não foi especificado <cdr_type> para esquema de tipo par
 AVISO: não foi especificado <list_type> para esquema de tipo lista
 AVISO: nenhuma chave especificada para o esquema
 AVISO: nó <%s> abaixo de <%s> não entendido
 AVISO: nó <%s> não entendido
 AVISO: nó <%s> não entendido abaixo de <%s>
 AVISO: nó <%s> não entendido abaixo de <schema>
 Nós não possuimos o bloqueio no arquivo "%s", mas deveríamos tê-lo Ao atribuir a uma lista você deve especificar um tipo de lista primitivo
 Ao atribuir a uma par você deve especificar um tipo de car e de cdr primitivos
 O nome do arquivo XML "%s" é um diretório Você deve ter pelo menos um item <locale> em um <schema>
 É necessário especificar uma fonte de configuração com --config-source ao usar --direct
 [ARQ...]|[CHAVE...]|[DIR...] "%c" é um caractere inválido em um endereço de armazenamento de configurações "%c" é um caractere inválido em nomes de chave/diretório não foi possível contatar ORB para resolver a referência ao objeto gconfd existente não foi possível criar o diretório "%s": %s gconfd foi compilado com opção de depuração; tentando carregar gconf.caminho do diretório fonte int|bool|float|string int|bool|float|string|list|pair nenhum analisando arquivo XML: listas e pares não podem ser colocados dentro de um par iniciando (versão %s), pid %u usuário "%s" 