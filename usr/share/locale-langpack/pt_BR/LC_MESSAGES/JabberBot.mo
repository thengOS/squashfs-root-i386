��          �   %   �      p  <   q  9   �  @   �  C   )     m  N   s     �  Z   �  ?   .  X   n  #   �     �  )   �  .   !  t   P  E   �  ?     4   K     �     �     �  $   �  #   �  $   �  [     *   y  /   �  �  �  :   y	  F   �	  H   �	  U   D
     �
  _   �
     �
  ^     A   y  \   �  *        C  6   S  4   �  v   �  G   6  H   ~  A   �     	          8  -   G  ,   u  -   �  i   �  (   :  L   c                                                           
                                                     	           %(command)s - %(description)s

Usage: %(command)s %(params)s A serious error occured while processing your request:
%s An internal error has occured, please contact the administrator. Credentials check failed, you may be unable to see all information. Error Following detailed information on page "%(pagename)s" is available::

%(data)s Full-text search Hello there! I'm a MoinMoin Notification Bot. Available commands:

%(internal)s
%(xmlrpc)s Here's the page "%(pagename)s" that you've requested:

%(data)s Last author: %(author)s
Last modification: %(modification)s
Current version: %(version)s Please specify the search criteria. Search text Submit this form to perform a wiki search That's the list of pages accesible to you:

%s The "help" command prints a short, helpful message about a given topic or function.

Usage: help [topic_or_function] The "ping" command returns a "pong" message as soon as it's received. This command may take a while to complete, please be patient... This command requires a client supporting Data Forms Title search Unknown command "%s"  Wiki search You are not allowed to use this bot! You must set a (long) secret string You must set a (long) secret string! You've specified a wrong parameter list. The call should look like:

%(command)s %(params)s Your request has failed. The reason is:
%s searchform - perform a wiki search using a form Project-Id-Version: moin
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2007-07-17 03:14+0200
PO-Revision-Date: 2009-02-25 03:15+0000
Last-Translator: André Gondim <Unknown>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:11+0000
X-Generator: Launchpad (build 18115)
 %(command)s - %(description)s

Uso: %(command)s %(params)s Um sério erro ocorreu durante o processamento da sua requisição:
%s Um erro interno ocorreu, por favor entre em contato com o administrador. Verificação de credenciais falhou, você deve ser incapaz de ver toda informação. Erro As seguintes informações detalhadas na página "%(pagename)s" estão disponíveis::

%(data)s Texto completo da pesquisa Olá aí! Eu sou o bot notificador do MoinMoin comandos disponíveis:

%(internal)s
%(xmlrpc)s Aqui está a página "%(pagename)s" que você requeriu:

%(data)s Último autor: %(author)s
Última modificação: %(modification)s
Versão atual: %(version)s Por favor especifique o critério de busca Pesquisar texto Envie este formulário para realizar uma busca na wiki Esta é a lista de páginas acessíveis a você:

%s O comando "help" mostra uma curta e útil mensagem sobre um dado tópico ou função.

Uso: help [tópico ou função] O comando "ping" retorna uma mensagem de eco assim que ela é recebida. Este comando pode demorar para ser executado, por favor seja paciente... Este comando requer formulários de dados suportados pelo cliente Título da pesquisa Comando desconhecido "%s"  Pesquisar Wiki Você não tem permissão para usar este bot! Você deve definir uma frase secreta (longa) Você deve definir uma senha secreta (longa)! Você especificou uma lista de parâmetros errada. A chamada deveria parecer com:

%(command)s %(params)s Sua requisição falhou. A razão é:
%s formulário de pesquisa - realiza uma pesquisa na wiki usando um formulário 