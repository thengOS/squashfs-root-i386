��    o      �  �         `	  �  a	  �   I  R     �   l  �   ,  �        �       4   $  �  Y     �     �            =   &     d     f     i     l     o     r     y     �  d   �  #  �          -  �   C     �  '   �  !   �          (  �   -     �  �   �  1   j  	   �  �   �  L   I     �  
   �  &   �     �     �  S   �  5   1  �   g     ?     H     Q     ^     k     z     �  �  �  /   4  -   d  +   �  _   �  �      �   !     �!      "  �   "  )   �"     �"     �"     #      #     &#     +#     7#     H#     b#     g#     p#     |#     �#  	   �#     �#     �#     �#     �#     �#     �#     �#     �#     �#     �#  !   �#  $   $     C$  I   I$  (   �$  %   �$     �$     �$     %  
   %     %     %  &   %     F%  (   O%     x%     �%     �%     �%     �%  �  �%  �  t'  �   a*  R   .+  �   �+  �   A,  �   -     �-     .  4   9.  �  n.     ,4     <4     L4     ^4  B   m4     �4     �4     �4     �4     �4     �4     �4     �4  n   �4    O5     X6     r6  �   �6     '7  0   47  "   e7     �7     �7  �   �7     28  �   A8  1   �8  	   9  �    9  _   �9     /:  
   8:  +   C:     o:     x:  k   }:  5   �:  �   ;     <     #<     +<     9<     G<     W<     d<  �  j<  3   [>  1   �>  7   �>  W   �>    Q?     T@  	   UA     _A  �   gA  )   KB     uB     {B     �B     �B     �B     �B     �B     �B     �B     �B     �B     C     C  	   C     C     0C     5C     >C     CC     PC     YC     ^C     uC     �C  $   �C  '   �C     �C  e   �C  0   PD  %   �D     �D     �D     �D  
   �D     �D     �D  )   �D     E  '   E     >E     OE     TE     YE     ]E     '   Y   +          =          .   "                  ^      2       0   a   ;   S   1           b   n       d   l          -   ]         `   M      W          E      I       P          	       m   T   #          
   )   D   [   $   C       f   j                   4   <       O       F       6   5   i   *   ?         c                    L   B      (   @   X              g   o                J   8       Z   V          %   H   R                  A       9       &   e   h   k          :       !   3          K   7   /   Q   _   N   G   >   \             U   ,                Call                               Included entries
 C<E<lt>formatE<gt>({ since =E<gt> '2.0' })>  3.1, 3.0, 2.2
 C<E<lt>formatE<gt>({ until =E<gt> '2.0' })>  1.3, 1.2
 C<E<lt>formatE<gt>({ from =E<gt> '2.0' })>   3.1, 3.0, 2.2, 2.1, 2.0
 C<E<lt>formatE<gt>({ to =E<gt> '2.0' })>     2.0, 1.3, 1.2
 C<E<lt>formatE<gt>({ count =E<gt> 2 }>>      3.1, 3.0
 C<E<lt>formatE<gt>({ count =E<gt> -2 }>>     1.3, 1.2
 C<E<lt>formatE<gt>({ count =E<gt> 3,
		      offset=E<gt> 2 }>>      2.2, 2.1, 2.0
 C<E<lt>formatE<gt>({ count =E<gt> 2,
		      offset=E<gt> -3 }>>     2.0, 1.3
 C<E<lt>formatE<gt>({ count =E<gt> -2,
		      offset=E<gt> 3 }>>      3.0, 2.2
 C<E<lt>formatE<gt>({ count =E<gt> -2,
		      offset=E<gt> -3 }>>     2.2, 2.1

     # the following is semantically equivalent
    my $chglog = Parse::DebianChangelog->init();
    $chglog->parse( { infile => 'debian/changelog' } );
    $chglog->html( { outfile => 'changelog.html' } );

     my $changes = $chglog->dpkg_str( { since => '1.0-1' } );
    print $changes;

     my $chglog = Parse::DebianChangelog->init( { infile => 'debian/changelog',
                                                 HTML => { outfile => 'changelog.html' } );
    $chglog->html;

   pod2usage(   -msg     => $message_text ,
               -exitval => $exit_status  ,
               -verbose => $verbose_level,
               -output  => $filehandle,
               -textdomain => $textdomain );

   pod2usage( { -message => gettext( $message_text ) ,
               -exitval => $exit_status  ,
               -verbose => $verbose_level,
               -output  => $filehandle,
               -textdomain => $textdomain } );

   pod2usage($exit_status);

   pod2usage($message_text);

   setlocale(LC_MESSAGES,'');
  textdomain('prog');

  Options:
    --help, -h                  print usage information
    --version, -V               print version information
    --file, -l <file>           changelog file to parse, defaults
                                to 'debian/changelog'
    -F<changelogformat>         ignored if changelogformat = 'debian'
                                for compatibility with dpkg-dev
    -L<libdir>                  ignored for compatibility with dpkg-dev
    --format <outputformat>     see man page for list of available
                                output formats, defaults to 'dpkg'
                                for compatibility with dpkg-dev
    --since, -s, -v <version>   include all changes later than version
    --until, -u <version>       include all changes earlier than version
    --from, -f <version>        include all changes equal or later
                                than version
    --to, -t <version>          include all changes up to or equal
                                than version
    --count, -c, -n <number>    include <number> entries from the top
                                (or the tail if <number> is lower than 0)
    --offset, -o <number>       change the starting point for --count,
                                counted from the top (or the tail if
                                <number> is lower than 0)
    --all                       include all changes

 (and B<dpkg_str>) (and B<html_str>) (and B<rfc822_str>) (and B<xml_str>) (works exactly like the C<-v> option of dpkg-parsechangelog). * 1. 2. 3. 4. AUTHOR Alias for init. BUGS Both methods only support the common output options described in section L<"COMMON OUTPUT OPTIONS">. C<--all> overrides all other range selecting options. C<--count> overrides all other range selection options except for C<--all>.  The range selecting options can be mixed together, but only one of C<--since> and C<--from> and one of C<--until> and C<--to> can be specified at the same time. COMMON OUTPUT OPTIONS COPYRIGHT AND LICENSE Can be used to delete all information about errors ocurred during previous L<parse> runs. Note that C<parse()> also calls this method. Changes Copyright (C) 2005 by Frank Lichtenheld Creates a new object, no options. DESCRIPTION Date Description of the Debian changelog format in the Debian policy: L<http://www.debian.org/doc/debian-policy/ch-source.html#s-dpkgchangelog>. Distribution For a more extensive documentation of the range selecting options and some (hopefully enlightening) examples see L<Parse::DebianChangelog/"COMMON OUTPUT OPTIONS">. Frank Lichtenheld, E<lt>frank@lichtenheld.deE<gt> Functions If neither C<changelogfile> nor C<-l E<lt>fileE<gt>> are specified, F<debian/changelog> will be used. If two different files are specified the program will abort. If the filename is C<-> the program reads the changelog from standard input. METHODS Maintainer Maintainer (name B<and> email address) Methods NAME NOTE: This format isn't stable yet and may change in later versions of this module. Parse::DebianChangelog, Parse::DebianChangelog::Entry Returns all error messages from the last L<parse> run.  If called in scalar context returns a human readable string representation. If called in list context returns an array of arrays. Each of these arrays contains SEE ALSO SYNOPSIS See L<dpkg>. See L<html>. See L<rfc822>. See L<xml>. Source The changelog is converted to a somewhat nice looking HTML file with some nice features as a quick-link bar with direct links to every entry.  NOTE: This is not configurable yet and was specifically designed to be used on L<http://packages.debian.org/>. This is planned to be changed until version 1.0. The used Parse::DebianChangelog module already supports configuration, however, this isn't exposed by this program yet. The method C<html_str> is an alias for C<html>. The method C<xml_str> is an alias for C<xml>. The output formats supported are currently: This method supports the common output options described in section L<"COMMON OUTPUT OPTIONS">. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version. Urgency Version You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA add_filter, delete_filter, replace_filter all an error description apply_filters count data data2rfc822 data2rfc822_mult date of the (first) entry dpkg dpkg_str find_closes from get_dpkg_changes get_error get_parse_errors html html_str init init_filters is_empty new not yet documented offset outfile package name (in the first entry) packages' version (from first entry) parse parsechangelog - parse Debian changelogs and output them in other formats parsechangelog [options] [changelogfile] person that created the (first) entry print_style reset_parse_errors rfc822 rfc822_str since style target distribution (from first entry) template the line number where the error occurred the original line to until xml xml_str Project-Id-Version: libparse-debianchangelog-perl
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-08-25 20:38+0000
PO-Revision-Date: 2010-04-14 17:28+0000
Last-Translator: Tiago Hillebrandt <tiagohillebrandt@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:52+0000
X-Generator: Launchpad (build 18115)
             Chamada                               Entradas incluidas
 C<E<lt>formatE<gt>({ since =E<gt> '2.0' })>  3.1, 3.0, 2.2
 C<E<lt>formatE<gt>({ until =E<gt> '2.0' })>  1.3, 1.2
 C<E<lt>formatE<gt>({ from =E<gt> '2.0' })>   3.1, 3.0, 2.2, 2.1, 2.0
 C<E<lt>formatE<gt>({ to =E<gt> '2.0' })>     2.0, 1.3, 1.2
 C<E<lt>formatE<gt>({ count =E<gt> 2 }>>      3.1, 3.0
 C<E<lt>formatE<gt>({ count =E<gt> -2 }>>     1.3, 1.2
 C<E<lt>formatE<gt>({ count =E<gt> 3,
		      offset=E<gt> 2 }>>      2.2, 2.1, 2.0
 C<E<lt>formatE<gt>({ count =E<gt> 2,
		      offset=E<gt> -3 }>>     2.0, 1.3
 C<E<lt>formatE<gt>({ count =E<gt> -2,
		      offset=E<gt> 3 }>>      3.0, 2.2
 C<E<lt>formatE<gt>({ count =E<gt> -2,
		      offset=E<gt> -3 }>>     2.2, 2.1

     # o seguinte é semânticamente o mesmo
    my $chglog = Parse::DebianChangelog->init();
    $chglog->parse( { infile => 'debian/changelog' } );
    $chglog->html( { outfile => 'changelog.html' } );

     my $changes = $chglog->dpkg_str( { since => '1.0-1' } );
    print $changes;

     my $chglog = Parse::DebianChangelog->init( { infile => 'debian/changelog',
                                                 HTML => { outfile => 'changelog.html' } );
    $chglog->html;

   pod2usage(   -msg     => $message_text ,
               -exitval => $exit_status  ,
               -verbose => $verbose_level,
               -output  => $filehandle,
               -textdomain => $textdomain );

   pod2usage( { -message => gettext( $message_text ) ,
               -exitval => $exit_status  ,
               -verbose => $verbose_level,
               -output  => $filehandle,
               -textdomain => $textdomain } );

   pod2usage($exit_status);

   pod2usage($message_text);

   setlocale(LC_MESSAGES,'');
  textdomain('prog');

  Opções:
    --help, -h mostra informações de uso
    --version, -V mostra informações da versão
    --file, -l <file> analisa o arquivo de registro de alterações, padrão
                                para 'debian/changelog'
    -F<formato do registro de alterações> ignorado se o formato do registro de alterações = 'debian'
                                para compatibilidade com dpkg-dev
    -L<libdir> ignorado para compatibilidade com dpkg-dev
    --format <formato de saída> veja o manual man para uma lista dos tipos de saída disponíveis
                                formatos de saída, padrões para 'dpkg'
                                para ter compatibilidade com dpkg-dev
    --since, -s, -v <versão> inclui todas as alterações desde esta versão
    --until, -u <versão> inclui todas as alterações antes desta versão
    --from, -f <versão> inclui todas as alterações desta versão
                                a esta versão
    --to, -t <versão> inclui todas as alterações acima ou igual
                                a esta versão
    --count, -c, -n <número> inclui <número> de entradas desde o topo
                                (ou o rodapé se <número> for menor que 0)
    --offset, -o <número> altera o ponto inicial do --count,
                                contados desde o topo (ou do rodapé se
                                <número> é menor que 0
    --all inclui todas as alterações

 (e B<dpkg_str>) (e B<html_str>) (e B<rfc822_str>) (e B<xml_str>) (funciona exatamente como a opção C<-v> do dpkg-parsechangelog). * 1. 2. 3. 4. AUTOR Pseudônimo para init. BUGS Ambos os métodos suportam apenas as opções comum de saída descritas na seção L<"COMMON OUTPUT OPTIONS">. C<--all> sobrepõe todas as demais opções. C<--count> sobrepõe todas as demais opções exceto para C<--all>. As opções podem ser combinadas juntas, mas apenas uma de C<--since> e C<--from> e uma de C<--until> e C<--to> podem ser especificadas ao mesmo tempo. OPÇÕES DE SAÍDA COMUNS DIREITOS AUTORAIS E LICENÇA Pode ser utilizado para apagar todas as informações sobre erros ocorridos antes de executar L<parse>. Note que C<parse()> também chama este método. Alterações Direitos Autorais (C) 2005 por Frank Lichtenheld Cria um novo objeto, sem opções. DESCRIÇÃO Data Descrição do formato do registro de alterações Debian na política Debian: L<http://www.debian.org/doc/debian-policy/ch-source.html#s-dpkgchangelog> Distribuição Para uma documentação mais extensa sobre a seleção de opções e mais (esclaredores esperamos) exemplos ver L<Parse::DebianChangelog/"COMMON OUTPUT OPTIONS">. Frank Lichtenheld, E<lt>frank@lichtenheld.deE<gt> Funções Se nem C<changelogfile> ou C<-l E<lt>fileE<gt>> são especificados, F<debian/changelog> será usado. Se dois diferentes arquivos são especificados o programa será abortado. Se o nome do arquivo é C <-> o programa lê o registro de alterações do dispositivo padrão. MÉTODOS Mantenedor Mantenedor (nome B<and> endereço de email) Métodos NOME NOTA: Este formato não é estável ainda e pode sofrer alterações em versões posteriores deste módulo. Parse::DebianChangelog, Parse::DebianChangelog::Entry Retorna todos as mensagens de erro deste a ultima execução de L<parse>. Se chamado em um contexto escalar retorna uma linha humanamente legível. Se chamado num contexto de lista retorna uma matriz de matrizes. Cada uma destes matrizes contém. VEJA TAMBÉM SINOPSE Veja L<dpkg>. Veja L<html>. Veja L<rfc822>. Veja L<xml>. Fonte O registro de mudanças é convertido para um arquivo HTML de boa visualização com algumas características interessantes como uma barra de acesso rápido com caminho direto para cada entrada. Nota: Isto não é configurável ainda e foi especificamente projetado para ser usado no L<http://packages.debian.org/>. Está planejado ser alterado até a versão 1.0. O módulo usado Parse::DebianChangelog já suporta configuração, no entanto, isto ainda não está aparecendo para este programa. O método C<html_str> é um pseudônimo de C<html>. O método C<xml_str> é um pseudônimo de C<xml>. As saídas de formatos suportadas são respectivamente: Este método suporta as saídas comuns descritas na seção L<"COMMON OUTPUT OPTIONS">. Este programa é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes. Este programa é um software livre que pode ser redistribuído e/ou modificado sob os termos da Licença Pública Geral GNU publicada pela Free Software Foundation; ou pela versão 2 da referida Licença ou (a seu critério) por qualquer versão posterior. Urgência Versão Você deve receber uma cópia da Licença GNU (Licença Pública Geral) junto com este programa; caso contrário, envie uma mensagem para Free Software Foundation, Inc., rua Franklin, n.51, 5˙ Andar, Boston, MA 02110-1301 USA. add_filter, delete_filter, replace_filter todos um erro na descrição apply_filters contagem dados data2rfc822 data2rfc822_mult Data da (primeira) entrada dpkg dpkg_str find_closes de get_dpkg_changes get_error get_parse_errors html html_str init init_filters is_empty novo ainda não documentado deslocamento arquivo de saída nome do pacote (na primeira entrada) versão do pacote (da primeira entrada) analizar parsechangelog - analiza arquivo de novidades (changelogs) do Debian e faz saídas em outros formatos parsechangelog [opções] [arquivo_de_novidades] Pessoa que criou a (primeira) entrada print_style reset_parse_errors rfc822 rfc822_str desde estilo distribuição alvo (da primeira entrada) modelo o numero da linha onde o erro aconteceu a linha original para até xml xml_str 