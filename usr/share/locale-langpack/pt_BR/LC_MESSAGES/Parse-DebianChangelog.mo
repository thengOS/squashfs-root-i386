��    &      L  5   |      P  ,   Q  '   ~  (   �  	   �  z   �     T     g     �     �     �     �     �          +     B  !   Y     {  $   �     �  -   �  "     "   &  #   I     m  &   �     �     �  (   �          '     F     \  +   o     �  .   �  ,   �  A   	  �  K  /   	
  -   9
  )   g
  	   �
  �   �
          1  )   O  !   y     �     �  *   �  '   �  )   $  )   N  0   x  $   �  +   �  %   �  <      #   ]  .   �  .   �  -   �  2     (   @     i  *   }  %   �  &   �     �       7   '     _  6   v  4   �  S   �                                         
   #                 $                                           &                              	                            %   !         "    'since' option specifies most recent version 'until' option specifies oldest version Copyright (C) 2005 by Frank Lichtenheld
 FATAL: %s This is free software; see the GNU General Public Licence version 2 or later for copying conditions. There is NO warranty. WARN: %s(l%s): %s
 WARN: %s(l%s): %s
LINE: %s
 bad key-value after `;': `%s' badly formatted heading line badly formatted trailer line badly formatted urgency value can't close file %s: %s can't load IO::String: %s can't lock file %s: %s can't open file %s: %s changelog format %s not supported couldn't parse date %s fatal error occured while parsing %s field %s has blank lines >%s< field %s has newline then non whitespace >%s< field %s has trailing newline >%s< found blank line where expected %s found change data where expected %s found eof where expected %s found start of entry where expected %s found trailer where expected %s ignored option -L more than one file specified (%s and %s) no changelog file specified output format %s not supported repeated key-value %s too many arguments unknown key-value key %s - copying to XS-%s unrecognised line you can only specify one of 'from' and 'since' you can only specify one of 'to' and 'until' you can't combine 'count' or 'offset' with any other range option Project-Id-Version: libparse-debianchangelog-perl
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-08-25 20:38+0000
PO-Revision-Date: 2009-02-23 13:07+0000
Last-Translator: André Gondim <Unknown>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:52+0000
X-Generator: Launchpad (build 18115)
 opção 'since' especifica versão mais recente opcão 'until' especifica versão mais antiga Copyright (C) 2005 por Frank Lichtenheld
 FATAL: %s Este é um software livre; veja a GNU General Public License versão 2 ou posterior para condições de copia. NÃO há garantia. AVISO: %s(l%s): %s
 AVISO: %s(l%s): %s
LINHA: %s
 valor-chave inválido depois de `;': `%s' linha de cabeçalho mal formatada linha trailer mal formada valor urgente mal formatado não foi possível fechar o arquivo %s: %s não é possível carregar IO:String %s não é possível bloquear arquivo %s: %s não foi possível abrir o arquivo %s: %s formato do log de alterações %s não suportado não foi possível analizar dados %s ocorreu um erro fatal enquanto analisava %s campo %s possui linhas em branco >%s< campo %s tem nova linha então nenhum espaço em branco >%s< campo %s precisa de nova linha >%s< linha em branco escontrada onde %s é esperado encontrado dado alterado onde  %s era esperado fim de arquivo encontrado onde %s é esperado encontrado início da entrada onde era esperado %s trailer encontrado aonde era esperado %s opção -L ignorada mais que um arquivo especificado (%s e %s) nenhum arquivo changelog especificado formato de saída %s não é suportado valor de chave %s repetido excesso de parâmetros valor-chave desconhecido valor %s - copiando para XS-%s linha não reconhecida você só pode especificar uma dessas 'from' e 'since' você só pode especificar uma dessas 'to' e 'until' você não pode combinar 'count' ou 'offset' com nenhuma outra opção de intervalo 