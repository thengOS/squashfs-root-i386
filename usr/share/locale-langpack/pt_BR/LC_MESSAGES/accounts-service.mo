��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  K   �  C   �  E   A  (   �  "   �      �     �  %     Z   8     �               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
POT-Creation-Date: 2013-01-17 09:48-0500
PO-Revision-Date: 2012-07-23 22:13+0000
Last-Translator: Rafael Fontenelle <Unknown>
Language-Team: Portuguese (Brazil) (http://www.transifex.com/projects/p/freedesktop/language/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:01+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
 Autenticação é necessária para alterar configurações da tela de login Autenticação é necessária para alteração de dados de usuário Autenticação é necessária para alterar dados do próprio usuário Alterar configurações da tela de login Alterar dados do próprio usuário Habilitar depuração de código Gerenciar contas de usuários Exibir informação da versão e sair Fornece interfaces de D-Bus para consultar e manipular
informações de conta de usuário. Substitui instância existente 