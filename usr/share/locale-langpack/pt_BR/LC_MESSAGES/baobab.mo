��    9      �  O   �      �     �               $     7  '   H  ;   p  �   �     V  !   c     �  +   �     �     �     �     �     �  %        .  4   I     ~     �     �      �     �     �     
          &     2     A  "   J      m     �     �     �     �      �     �     �     	     	  (   	     A	     M	     Z	  	   c	     m	     t	     |	     �	     �	     �	     �	     �	     �	  ;  �	          8     G     X     i  5   x  B   �  �   �     �  0   �     �  >        C  
   J  	   U  +   _  %   �  7   �  &   �  =        N     l      �  %   �     �     �                    *  
   @  $   K  -   p     �     �     �     �     �     �               *  *   7     b     t     �     �     �  	   �  .   �     �     �     �     �     �  �       .                3            ,                              6                7   9          	             -       %             +   !                      $         (       &           #   '          8   /   
   4          "       1      )            2       *   0       5            "%s" is not a valid folder %d day %d days %d item %d items %d month %d months %d year %d years A graphical tool to analyze disk usage. A list of URIs for partitions to be excluded from scanning. A simple application which can scan either specific folders (local or remote) or volumes and give a graphical representation including each directory size or percentage. Active Chart Apparent sizes are shown instead. Baobab Check folder sizes and available disk space Close Computer Contents Could not analyze disk usage. Could not analyze volume. Could not detect occupied disk sizes. Could not scan folder "%s" Could not scan some of the folders contained in "%s" Devices and locations Disk Usage Analyzer Excluded partitions URIs Failed to move file to the trash Failed to open file Failed to show help Folder Go to _parent folder Home folder Mo_ve to Trash Modified Print version information and exit Recursively analyze mount points Rings Chart Scan Folder… Select Folder Size The GdkWindowState of the window The initial size of the window Today Treemap Chart Unknown Which type of chart should be displayed. Window size Window state Zoom _in Zoom _out _About _Cancel _Copy Path to Clipboard _Help _Open _Open Folder _Quit storage;space;cleanup; translator-credits Project-Id-Version: baobab
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=baobab&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 14:31+0000
PO-Revision-Date: 2015-12-04 01:27+0000
Last-Translator: Enrico Nicoletto <liverig@gmail.com>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 17:41+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
 "%s" não é uma pasta válida %d dia %d dias %d item %d itens %d mês %d meses %d ano %d anos Uma ferramenta gráfica para analisar o uso de disco. Uma lista de URIs das partições a serem excluídas da varredura. Um aplicativo simples que pode varrer tanto pastas específicas (sejam estas locais ou remotas) quanto volumes e fornece uma representação gráfica incluindo cada tamanho ou porcentagem de diretórios. Gráfico ativo Tamanhos aparentes são mostrados, em vez disso. Baobab Verifique o tamanho de pastas e o espaço disponível em disco Fechar Computador Conteúdo Não foi possível analisar o uso de disco. Não foi possível analisar o volume. Não foi possível detectar tamanhos de disco ocupados. Não foi possível varrer a pasta "%s" Não foi possível varrer algumas das pastas contidas em "%s" Dispositivos e localizações Analisador de uso de discos URI's das partições excluídas Falha ao mover arquivo para a lixeira Falha ao abrir o arquivo Falha ao mostrar a ajuda Pasta Ir para a pasta _pai Pasta pessoal _Mover para a lixeira Modificado Exibe informações da versão e sai Analisar recursivamente os pontos de montagem Gráfico de anéis Varrer pasta… Selecione uma pasta Tamanho O GdkWindowState da janela O tamanho inicial da janela Hoje Gráfico Treemap Desconhecido Qual tipo de gráfico deveria ser exibido. Tamanho da janela Estado da janela Amp_liar Redu_zir _Sobre _Cancelar _Copiar caminho para a área de transferência Aj_uda A_brir _Abrir pasta Sa_ir armazenamento;espaço;limpeza; Alexandre Hautequest <hquest@fesppr.br>
Ariel Bressan da Silva <ariel@conectiva.com.br>
Evandro Fernandes Giovanini <evandrofg@ig.com.br>
Francisco Petrúcio Cavalcante Junior <fpcj@impa.br>
Gustavo Maciel Dias Vieira <gustavo@sagui.org>
Welther José O. Esteves <weltherjoe@yahoo.com.br>
Goedson Teixeira Paixão <goedson@debian.org>
Guilherme de S. Pastore <gpastore@colband.com.br>
Luiz Fernando S. Armesto <luiz.armesto@gmail.com>
Leonardo Ferreira Fontenelle <leonardof@gnome.org>
Og Maciel <ogmaciel@gnome.org>
Hugo Doria <hugodoria@gmail.com>
Djavan Fagundes <dnoway@gmail.com>
Krix Apolinário <krixapolinario@gmail.com>
Antonio Fernandes C. Neto <fernandesn@gnome.org>
Rodrigo Padula de Oliveira <contato@rodrigopadula.com>
André Gondim (In memoriam)<In memoriam>
Enrico Nicoletto <liverig@gmail.com>
Rafael Fontenelle <rafael.f.f1@gmail.com>

Launchpad Contributions:
  Enrico Nicoletto https://launchpad.net/~liverig
  Florencio Neves https://launchpad.net/~florencioneves
  Jonh Wendell https://launchpad.net/~wendell
  Neliton Pereira Jr. https://launchpad.net/~nelitonpjr
  Pablo Diego Moço https://launchpad.net/~pablodm89
  Rafael Fontenelle https://launchpad.net/~rffontenelle
  Tiago Hillebrandt https://launchpad.net/~tiagohillebrandt 