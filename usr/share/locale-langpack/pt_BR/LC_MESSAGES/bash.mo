��         T  �  �       �+  *   �+     �+  $   �+  
   �+     �+     �+     ,     $,     9,     Y,     p,  	   �,     �,     �,     �,     �,     �,     �,     -      (-     I-     P-     d-     |-  /   �-  ;   �-  $   .  :   ).     d.     {.  (   �.  "   �.     �.     �.     /  3   ./      b/  &   �/  &   �/  /   �/  /   0     10     G0  .   ]0     �0     �0     �0     �0     �0     1  "   )1     L1     `1     q1     �1  /   �1     �1     �1     2  -   2     B2     X2     u2     �2     �2     �2     �2     �2  !   3     $3  )   A3     k3     �3     �3     �3     �3     �3      �3  !   4     =4      P4     q4     �4     �4     �4     �4     �4     �4     5     5     75     Q5     j5     �5     �5     �5  &   �5     �5     6     ,6     B6  &   Q6  3   x6     �6     �6     �6     �6     7     7     &7     47  9   K7  #   �7     �7     �7     �7     �7  �  �7  H  �;     	?     ?     )?  F  5?     |@     �@     �@     �@  	   �@  	   �@     �@  �
  �@     �K  a  �K    M  ]  O  �  yP  T  BT    �V  �  �Z    [^  +  nc  L  �e  �   �f  ?  �g     �j     �j     �j  t  �j  �   dn  �  Wo  �   Du  g  �u  �   Kx    Cy  �   Gz  g  9{  �   �}  u  �~  �  
�  �  ��  w  ��     �  |   �  �   ��  �   c�  
   �     ��     �     +�     F�  �   Y�     %�     <�     U�      p�     ��     ��     ��  	   ��     ��     ӊ  	   �     �  N   ��    G�  ,  W�  =  ��  f        )�  B  8�  �  {�     �  P   �  -  W�     ��  �  ��  �   &�  �  Ҥ  �  ��  �  ��  �  0�    ݭ  �  �  F   s�  F   ��  L  �  �  N�  S   �     i�     q�  >  ��  �  ÷  ?  ��    ϼ  K   ߾  D   +�     p�  �   ��  	   d�  �  n�     0�     8�     I�     ]�     r�  O  ~�  *   ��  
   ��     �     "�     ;�  O  T�  �  ��  B   z�  E   ��     �     �     '�     5�     D�  X   X�     ��  *   ��     ��     �     �  �   %�     �     #�  
   ;�     F�     W�     i�  %   ��  $   ��  '   ��     ��     �     (�     B�  !   a�     ��     ��     ��     ��  '   ��  0   �  .   9�     h�  9   ��     ��     ��     ��  $   ��     !�     5�     F�     T�  &   ]�  '   ��  9   ��     ��  	   ��     �  !   �     =�  3   K�     �  =   ��  -   ��     
�  '   *�  &   R�  *   y�  *   ��  )   ��  )   ��  %   #�  %   I�      o�  1   ��  #   ��  1   ��  &   �  5   ?�     u�     ��  !   ��  !   ��  :   ��      �      ;�  1   \�  �   ��  �   $�  #   ��  '   ��  $   �     ;�  $   H�  #   m�  '   ��     ��  .   ��     ��     �     %�     ;�     R�     r�     ��     ��     ��     ��  ,   ��  %   �  ,   5�  %   b�     ��  @   ��     ��     ��     ��  ,   �     0�  #   D�     h�  @   n�     ��     ��     ��  -   ��  ,   #�  '   P�  .   x�  ,   ��  &   ��  0   ��  6   ,�  P   c�  (   ��     ��  )   ��     $�     5�  ?   O�  T   ��     ��     ��     �  8   �  V   R�  &   ��  '   ��     ��     �     0�  (   =�     f�     y�     ��     ��  "   ��     ��  5   ��  O   �     i�     {�     ��  	   ��     ��     ��     ��  
   ��  
   ��  +   ��  9    �  ;   Z�  $   ��     ��  Y   ��     3�     R�     ^�     j�     ��      ��     ��     ��     ��  H   �     M�     \�     y�     ��  "   ��  +   ��     ��     �  4   �  
   F�  D   Q�  ?   ��  ,   ��     �     �  !   .�  "   P�  "   s�     ��     ��  	   ��  o   ��     =�  [   P�  1   ��  /   ��  )   �  3   8�     l�  &   ��  2   ��  5   ��  ,   �  
   C�  
   N�  1   Y�  I   ��  4   ��  .   
�  8   9�  (   r�  ,   ��  ,   ��  0   ��  )   &�  	   P�     Z�      f�  "   ��     ��     ��     ��     ��  &   ��  =   !�     _�     z�  '   ��     ��     ��  ,   ��  )   �     D�  $   c�     ��     ��     ��     ��     ��  "   ��     ��     �     �  ,   8�     e�     |�  %   ��  .   ��  -   ��  7   �  6   T�  2   ��  1   ��  *   ��  ,   �  ,   H�  ;   u�  #   ��     ��     ��  !   ��     �  6   .�  *   e�  "   ��     ��  6   ��  	   
�     �  -   $�  -   R�  !   ��     ��  '   ��  '   ��     �      �  �  /�  4   �     :�  %   N�  
   t�     �     ��  '   ��     ��  &   ��     �     %�  	   >�     H�  *   \�  )   ��     ��     ��  *   ��     �  #   #�     G�     N�     c�  &   ��  :   ��  H   ��  '   .�  <   V�  (   ��     ��  4   ��  0   �     @�     ]�      |�  5   ��  )   ��  D   ��  1   B�  =   t�  =   ��     ��     
�  ?   %�  +   e�     ��      ��  &   ��  -   ��     $�  4   >�     s�     ��  1   ��     ��  (   ��     &�     >�     Z�  -   v�     ��  "   ��     ��     ��       *   &     Q     n  ,   �  !   �  5   �         0    P    f    � 2   � &   � 0   �    & *   ;    f    ~ "   � &   �    �    �        & (   C "   l    � %   �    � #   �     &   - (   T !   } '   �    � 3   � G       P    p    � 	   �    �    �    �    � 0   
 .   ;    j    l    �    � 6  � %  �
            , W  ? )   �    �    � $   �        " "   ? I  b 	   � n  � a  % \  �    �! �  �% l  �( �  %- �  1 �  �6 w  J9 �   �: '  |; 
   �>    �>    �> 6  �> 
  C �  D �   �J �  kK   �M �   	O �   P �  �P   �S �  �T �  wV E  _X �  �\    K^ �   k^ �   �^ �   �_ "   i`    �`    �`    �`    �` �   �`    �a    �a    �a '   	b    1b    Gb    Nb    bb    xb    �b    �b    �b S   �b �  c F  �g l  �h r   bj    �j �  �j �  �r    +t ^   0t 9  �t    �z �  �z �   �~ �  O �  G� �  :� �  �   � �  � W   ȋ O    � ^  p� �  ύ M   �� 
   ��    � u  � %  �� {  �� ^  3� P   �� e   �    I�   _�    e� �  n�    /�    6�    E�    Z�    n� �  � 0   
� 	   ;� *   E�    p�    �� p  �� `  � P   |� S   ͬ    !�    6�    M�    _� $   s� k   ��    � ,   $�    Q�    p�    �� �   �� #   �� (   �� 
   �    ��    � #   � +   A� +   m� 3   �� #   Ͱ    � &   � "   8� ;   [� &   �� #   ��    �    � (   � 2   4� /   g�    �� =   �� 	   ��    �� +   � =   =� #   {�    ��    ��    ̳ :   ܳ 2   � H   J�    �� 	   �� $   �� ,   Դ    � :   � %   J� J   p� E   �� %   � =   '� =   e� 7   �� ?   ۶ =   � >   Y� <   �� <   շ 3   � @   F� ?   �� P   Ǹ ?   � D   X�    �� *   �� 0   ޹ %   � =   5� "   s�     �� >   �� �   �� �   �� )   c� (   �� &   ��    ݼ )   � %   � ,   :�    g� 8   m� "   ��    ɽ    �    �    �    ;� '   J�    r�    �� $   �� 3   �� '   �� 4   � )   R�    |� F   ��    ҿ    ۿ    � ,   ��    *� 0   B�    s� F   y�    �� #   �� )   �� -   $� /   R� 0   �� 1   �� 1   �� 3   � 5   K� 6   �� V   �� +   �    ;� (   W�    ��    �� K   �� [   ��    W�    o�    �� @   �� V   �� &   4� +   [�    ��    ��    �� *   ��    ��    �    ,�    D� .   _�    �� >   �� O   ��    *�    ?�    Q� 
   X�    c�     ��    ��    ��    �� <   �� =   �� D   =� )   ��    �� W   ��     !�    B�    O� $   \�     �� #   ��    ��    ��    �� U   �    h� )   ��    ��    �� (   �� =   �� (   =�    f� 9   t�    �� R   �� N   � 7   ]�    ��    �� &   �� $   �� "   �    2�    H� 	   c� q   m�    �� Y   �� 1   Q� 4   �� 6   �� 4   ��    $� 1   =� /   o� A   �� /   ��    � 
   � 7   %� U   ]� 4   �� 1   �� 8   � 5   S� 9   �� 9   �� =   �� >   ;� 	   z�    �� %   �� %   ��    ��    �    �    ,� )   <� :   f�    ��    �� 6   ��    � "   0� 1   S� /   �� "   �� )   ��    �    �    (�    ;�    A� "   a�     �� 
   ��    �� ,   ��    ��    � 5   /� 5   e� ,   �� <   �� ;   � 7   A� 7   y� -   �� /   �� 0   � @   @� &   ��    ��    �� !   �� 0   �� ?   %� 2   e� +   �� &   �� 7   ��    #�    +� 9   9� 9   s� !   ��    �� '   �� 3   � )   ?�    i�       -   �       �     F       �  �       �   /    �  �   �  �  �  �   �              �        �   m  �         �   @      *  �   W       U     =             �  Q   �   [   �   f               �      �   s           P              �   _          w       �   �  =  �      �           
  �  b  �              k  �   |  j   �   :   �  v  �  �  �   �       <      �  .   �  �              �  �      �  �   D      �  (   c  P  �      `   �      �           �      ]   r   �   �       V          n   �  >       �      	  f  W      �   �       �   R     �      �   �  �   �   
   �  i           �            1   I         e     x      �   �  �  �  �   )   �  �  �     �  Z    �  �  "       ,  w                  �   M  �   �   �  �  �       �  �  �   !   �  �  �       �      3   5  �    �       �   �   K   �      \   g  �      $  j  l  �  �   �   A  `  8      �      �          G      �      �   �    6   �  �   �   �         �  J          d  �   �          �  �  F  �  ;  �              �       #               �          @   ?  �  �  X   �  [         +           C   N  t   �   �   �   �      �  a   �      �   �   �  �       �   	   /       �   �  �  �  n      �   �   �   �           �   �  �  Y       �  �   �       �  �   Y  �  �  N       g   �  4   Z         �   �   �           �   2  �       �  �       9      q  �   &  }          L      e   >  X  ~          �   �  4  ]  �  I  �  E   �      u       �    �  �   �  �  x       �   �   �  �      H        �   �       �   u            ;       t  8   �  �   �         �  0   l   �  �  p   �  �                     �   �  +  R   '      h   �   H           v   �   �  '       9  6      .  �   S       {           �      V   M   o   �   �  �   �        �  D   ^  $   �  7         z     B  q   �        �  �          �  1  �  (  �       �       "         �   A             �      �      �   �   �   �   5         �   L       T   7              �   &   ^       �   �        p  �  �   :      !  �   �   �   �       #  E        �                         �   �         S              b   y   \  K  %      U  -  2   k   ?           �  �       �  <        �  �      %      �   �       �  �   J           �  {      z   Q              h  �   *   �  C     �      O            i  ~   �      a  G   T  �  �   �     O      �            �          }     o         �  �      �  �  3  _   �       m   s  ,                 �       �      �   r  �     )  0  �  �   �  �  �   �                       �   B   d          c   y  |       �   �      timed out waiting for input: auto-logout
 	-%s or -o option
 
malloc: %s:%d: assertion botched
   (wd: %s)  (core dumped)  line  $%s: cannot assign in this way %c%c: invalid option %d: invalid file descriptor: %s %s can be invoked via  %s has null exportstr %s is %s
 %s is a function
 %s is a shell builtin
 %s is a shell keyword
 %s is aliased to `%s'
 %s is hashed (%s)
 %s is not bound to any keys.
 %s out of range %s%s%s: %s (error token is "%s") %s: %s %s: %s out of range %s: %s: bad interpreter %s: %s: cannot open as FILE %s: %s: invalid value for trace file descriptor %s: %s: must use subscript when assigning associative array %s: %s:%d: cannot allocate %lu bytes %s: %s:%d: cannot allocate %lu bytes (%lu bytes allocated) %s: ambiguous job spec %s: ambiguous redirect %s: arguments must be process or job IDs %s: bad network path specification %s: bad substitution %s: binary operator expected %s: cannot allocate %lu bytes %s: cannot allocate %lu bytes (%lu bytes allocated) %s: cannot assign fd to variable %s: cannot assign list to array member %s: cannot assign to non-numeric index %s: cannot convert associative to indexed array %s: cannot convert indexed to associative array %s: cannot create: %s %s: cannot delete: %s %s: cannot destroy array variables in this way %s: cannot execute binary file %s: cannot execute: %s %s: cannot get limit: %s %s: cannot modify limit: %s %s: cannot open temp file: %s %s: cannot open: %s %s: cannot overwrite existing file %s: cannot read: %s %s: cannot unset %s: cannot unset: readonly %s %s: command not found %s: error retrieving current directory: %s: %s
 %s: expression error
 %s: file is too large %s: file not found %s: first non-whitespace character is not `"' %s: hash table empty
 %s: history expansion failed %s: host unknown %s: illegal option -- %c
 %s: inlib failed %s: integer expression expected %s: invalid action name %s: invalid array origin %s: invalid associative array key %s: invalid callback quantum %s: invalid file descriptor specification %s: invalid limit argument %s: invalid line count %s: invalid option %s: invalid option name %s: invalid service %s: invalid shell option name %s: invalid signal specification %s: invalid timeout specification %s: is a directory %s: job %d already in background %s: job has terminated %s: line %d:  %s: missing colon separator %s: no completion specification %s: no job control %s: no such job %s: not a function %s: not a regular file %s: not a shell builtin %s: not an array variable %s: not an indexed array %s: not dynamically loaded %s: not found %s: numeric argument required %s: option requires an argument %s: option requires an argument -- %c
 %s: parameter null or not set %s: readonly function %s: readonly variable %s: restricted %s: restricted: cannot redirect output %s: restricted: cannot specify `/' in command names %s: substring expression < 0 %s: unary operator expected %s: unbound variable %s: usage:  (( expression )) (core dumped)  (wd now: %s)
 . filename [arguments] /dev/(tcp|udp)/host/port not supported without networking /tmp must be a valid directory name : <no current directory> ABORT instruction Aborting... Add directories to stack.
    
    Adds a directory to the top of the directory stack, or rotates
    the stack, making the new top of the stack the current working
    directory.  With no arguments, exchanges the top two directories.
    
    Options:
      -n	Suppresses the normal change of directory when adding
    	directories to the stack, so only the stack is manipulated.
    
    Arguments:
      +N	Rotates the stack so that the Nth directory (counting
    	from the left of the list shown by `dirs', starting with
    	zero) is at the top.
    
      -N	Rotates the stack so that the Nth directory (counting
    	from the right of the list shown by `dirs', starting with
    	zero) is at the top.
    
      dir	Adds DIR to the directory stack at the top, making it the
    	new current working directory.
    
    The `dirs' builtin displays the directory stack.
    
    Exit Status:
    Returns success unless an invalid argument is supplied or the directory
    change fails. Adds a directory to the top of the directory stack, or rotates
    the stack, making the new top of the stack the current working
    directory.  With no arguments, exchanges the top two directories.
    
    Options:
      -n	Suppresses the normal change of directory when adding
    	directories to the stack, so only the stack is manipulated.
    
    Arguments:
      +N	Rotates the stack so that the Nth directory (counting
    	from the left of the list shown by `dirs', starting with
    	zero) is at the top.
    
      -N	Rotates the stack so that the Nth directory (counting
    	from the right of the list shown by `dirs', starting with
    	zero) is at the top.
    
      dir	Adds DIR to the directory stack at the top, making it the
    	new current working directory.
    
    The `dirs' builtin displays the directory stack. Alarm (profile) Alarm (virtual) Alarm clock Arithmetic for loop.
    
    Equivalent to
    	(( EXP1 ))
    	while (( EXP2 )); do
    		COMMANDS
    		(( EXP3 ))
    	done
    EXP1, EXP2, and EXP3 are arithmetic expressions.  If any expression is
    omitted, it behaves as if it evaluates to 1.
    
    Exit Status:
    Returns the status of the last command executed. BPT trace/trap Bad system call Bogus signal Broken pipe Bus error CPU limit Child death or stop Common shell variable names and usage.
    
    BASH_VERSION	Version information for this Bash.
    CDPATH	A colon-separated list of directories to search
    		for directories given as arguments to `cd'.
    GLOBIGNORE	A colon-separated list of patterns describing filenames to
    		be ignored by pathname expansion.
    HISTFILE	The name of the file where your command history is stored.
    HISTFILESIZE	The maximum number of lines this file can contain.
    HISTSIZE	The maximum number of history lines that a running
    		shell can access.
    HOME	The complete pathname to your login directory.
    HOSTNAME	The name of the current host.
    HOSTTYPE	The type of CPU this version of Bash is running under.
    IGNOREEOF	Controls the action of the shell on receipt of an EOF
    		character as the sole input.  If set, then the value
    		of it is the number of EOF characters that can be seen
    		in a row on an empty line before the shell will exit
    		(default 10).  When unset, EOF signifies the end of input.
    MACHTYPE	A string describing the current system Bash is running on.
    MAILCHECK	How often, in seconds, Bash checks for new mail.
    MAILPATH	A colon-separated list of filenames which Bash checks
    		for new mail.
    OSTYPE	The version of Unix this version of Bash is running on.
    PATH	A colon-separated list of directories to search when
    		looking for commands.
    PROMPT_COMMAND	A command to be executed before the printing of each
    		primary prompt.
    PS1		The primary prompt string.
    PS2		The secondary prompt string.
    PWD		The full pathname of the current directory.
    SHELLOPTS	A colon-separated list of enabled shell options.
    TERM	The name of the current terminal type.
    TIMEFORMAT	The output format for timing statistics displayed by the
    		`time' reserved word.
    auto_resume	Non-null means a command word appearing on a line by
    		itself is first looked for in the list of currently
    		stopped jobs.  If found there, that job is foregrounded.
    		A value of `exact' means that the command word must
    		exactly match a command in the list of stopped jobs.  A
    		value of `substring' means that the command word must
    		match a substring of the job.  Any other value means that
    		the command must be a prefix of a stopped job.
    histchars	Characters controlling history expansion and quick
    		substitution.  The first character is the history
    		substitution character, usually `!'.  The second is
    		the `quick substitution' character, usually `^'.  The
    		third is the `history comment' character, usually `#'.
    HISTIGNORE	A colon-separated list of patterns used to decide which
    		commands should be saved on the history list.
 Continue Create a coprocess named NAME.
    
    Execute COMMAND asynchronously, with the standard output and standard
    input of the command connected via a pipe to file descriptors assigned
    to indices 0 and 1 of an array variable NAME in the executing shell.
    The default NAME is "COPROC".
    
    Exit Status:
    Returns the exit status of COMMAND. Define or display aliases.
    
    Without arguments, `alias' prints the list of aliases in the reusable
    form `alias NAME=VALUE' on standard output.
    
    Otherwise, an alias is defined for each NAME whose VALUE is given.
    A trailing space in VALUE causes the next word to be checked for
    alias substitution when the alias is expanded.
    
    Options:
      -p	Print all defined aliases in a reusable format
    
    Exit Status:
    alias returns true unless a NAME is supplied for which no alias has been
    defined. Define shell function.
    
    Create a shell function named NAME.  When invoked as a simple command,
    NAME runs COMMANDs in the calling shell's context.  When NAME is invoked,
    the arguments are passed to the function as $1...$n, and the function's
    name is in $FUNCNAME.
    
    Exit Status:
    Returns success unless NAME is readonly. Display directory stack.
    
    Display the list of currently remembered directories.  Directories
    find their way onto the list with the `pushd' command; you can get
    back up through the list with the `popd' command.
    
    Options:
      -c	clear the directory stack by deleting all of the elements
      -l	do not print tilde-prefixed versions of directories relative
    	to your home directory
      -p	print the directory stack with one entry per line
      -v	print the directory stack with one entry per line prefixed
    	with its position in the stack
    
    Arguments:
      +N	Displays the Nth entry counting from the left of the list shown by
    	dirs when invoked without options, starting with zero.
    
      -N	Displays the Nth entry counting from the right of the list shown by
    	dirs when invoked without options, starting with zero.
    
    Exit Status:
    Returns success unless an invalid option is supplied or an error occurs. Display information about builtin commands.
    
    Displays brief summaries of builtin commands.  If PATTERN is
    specified, gives detailed help on all commands matching PATTERN,
    otherwise the list of help topics is printed.
    
    Options:
      -d	output short description for each topic
      -m	display usage in pseudo-manpage format
      -s	output only a short usage synopsis for each topic matching
    	PATTERN
    
    Arguments:
      PATTERN	Pattern specifiying a help topic
    
    Exit Status:
    Returns success unless PATTERN is not found or an invalid option is given. Display information about command type.
    
    For each NAME, indicate how it would be interpreted if used as a
    command name.
    
    Options:
      -a	display all locations containing an executable named NAME;
    	includes aliases, builtins, and functions, if and only if
    	the `-p' option is not also used
      -f	suppress shell function lookup
      -P	force a PATH search for each NAME, even if it is an alias,
    	builtin, or function, and returns the name of the disk file
    	that would be executed
      -p	returns either the name of the disk file that would be executed,
    	or nothing if `type -t NAME' would not return `file'.
      -t	output a single word which is one of `alias', `keyword',
    	`function', `builtin', `file' or `', if NAME is an alias, shell
    	reserved word, shell function, shell builtin, disk file, or not
    	found, respectively
    
    Arguments:
      NAME	Command name to be interpreted.
    
    Exit Status:
    Returns success if all of the NAMEs are found; fails if any are not found. Display or execute commands from the history list.
    
    fc is used to list or edit and re-execute commands from the history list.
    FIRST and LAST can be numbers specifying the range, or FIRST can be a
    string, which means the most recent command beginning with that
    string.
    
    Options:
      -e ENAME	select which editor to use.  Default is FCEDIT, then EDITOR,
    		then vi
      -l 	list lines instead of editing
      -n	omit line numbers when listing
      -r	reverse the order of the lines (newest listed first)
    
    With the `fc -s [pat=rep ...] [command]' format, COMMAND is
    re-executed after the substitution OLD=NEW is performed.
    
    A useful alias to use with this is r='fc -s', so that typing `r cc'
    runs the last command beginning with `cc' and typing `r' re-executes
    the last command.
    
    Exit Status:
    Returns success or status of executed command; non-zero if an error occurs. Display or manipulate the history list.
    
    Display the history list with line numbers, prefixing each modified
    entry with a `*'.  An argument of N lists only the last N entries.
    
    Options:
      -c	clear the history list by deleting all of the entries
      -d offset	delete the history entry at offset OFFSET.
    
      -a	append history lines from this session to the history file
      -n	read all history lines not already read from the history file
      -r	read the history file and append the contents to the history
    	list
      -w	write the current history to the history file
    	and append them to the history list
    
      -p	perform history expansion on each ARG and display the result
    	without storing it in the history list
      -s	append the ARGs to the history list as a single entry
    
    If FILENAME is given, it is used as the history file.  Otherwise,
    if $HISTFILE has a value, that is used, else ~/.bash_history.
    
    If the $HISTTIMEFORMAT variable is set and not null, its value is used
    as a format string for strftime(3) to print the time stamp associated
    with each displayed history entry.  No time stamps are printed otherwise.
    
    Exit Status:
    Returns success unless an invalid option is given or an error occurs. Display or set file mode mask.
    
    Sets the user file-creation mask to MODE.  If MODE is omitted, prints
    the current value of the mask.
    
    If MODE begins with a digit, it is interpreted as an octal number;
    otherwise it is a symbolic mode string like that accepted by chmod(1).
    
    Options:
      -p	if MODE is omitted, output in a form that may be reused as input
      -S	makes the output symbolic; otherwise an octal number is output
    
    Exit Status:
    Returns success unless MODE is invalid or an invalid option is given. Display possible completions depending on the options.
    
    Intended to be used from within a shell function generating possible
    completions.  If the optional WORD argument is supplied, matches against
    WORD are generated.
    
    Exit Status:
    Returns success unless an invalid option is supplied or an error occurs. Display process times.
    
    Prints the accumulated user and system times for the shell and all of its
    child processes.
    
    Exit Status:
    Always succeeds. Display the list of currently remembered directories.  Directories
    find their way onto the list with the `pushd' command; you can get
    back up through the list with the `popd' command.
    
    Options:
      -c	clear the directory stack by deleting all of the elements
      -l	do not print tilde-prefixed versions of directories relative
    	to your home directory
      -p	print the directory stack with one entry per line
      -v	print the directory stack with one entry per line prefixed
    	with its position in the stack
    
    Arguments:
      +N	Displays the Nth entry counting from the left of the list shown by
    	dirs when invoked without options, starting with zero.
    
      -N	Displays the Nth entry counting from the right of the list shown by
	dirs when invoked without options, starting with zero. Done Done(%d) EMT instruction Enable and disable shell builtins.
    
    Enables and disables builtin shell commands.  Disabling allows you to
    execute a disk command which has the same name as a shell builtin
    without using a full pathname.
    
    Options:
      -a	print a list of builtins showing whether or not each is enabled
      -n	disable each NAME or display a list of disabled builtins
      -p	print the list of builtins in a reusable format
      -s	print only the names of Posix `special' builtins
    
    Options controlling dynamic loading:
      -f	Load builtin NAME from shared object FILENAME
      -d	Remove a builtin loaded with -f
    
    Without options, each NAME is enabled.
    
    To use the `test' found in $PATH instead of the shell builtin
    version, type `enable -n test'.
    
    Exit Status:
    Returns success unless NAME is not a shell builtin or an error occurs. Evaluate arithmetic expression.
    
    The EXPRESSION is evaluated according to the rules for arithmetic
    evaluation.  Equivalent to "let EXPRESSION".
    
    Exit Status:
    Returns 1 if EXPRESSION evaluates to 0; returns 0 otherwise. Evaluate arithmetic expressions.
    
    Evaluate each ARG as an arithmetic expression.  Evaluation is done in
    fixed-width integers with no check for overflow, though division by 0
    is trapped and flagged as an error.  The following list of operators is
    grouped into levels of equal-precedence operators.  The levels are listed
    in order of decreasing precedence.
    
    	id++, id--	variable post-increment, post-decrement
    	++id, --id	variable pre-increment, pre-decrement
    	-, +		unary minus, plus
    	!, ~		logical and bitwise negation
    	**		exponentiation
    	*, /, %		multiplication, division, remainder
    	+, -		addition, subtraction
    	<<, >>		left and right bitwise shifts
    	<=, >=, <, >	comparison
    	==, !=		equality, inequality
    	&		bitwise AND
    	^		bitwise XOR
    	|		bitwise OR
    	&&		logical AND
    	||		logical OR
    	expr ? expr : expr
    			conditional operator
    	=, *=, /=, %=,
    	+=, -=, <<=, >>=,
    	&=, ^=, |=	assignment
    
    Shell variables are allowed as operands.  The name of the variable
    is replaced by its value (coerced to a fixed-width integer) within
    an expression.  The variable need not have its integer attribute
    turned on to be used in an expression.
    
    Operators are evaluated in order of precedence.  Sub-expressions in
    parentheses are evaluated first and may override the precedence
    rules above.
    
    Exit Status:
    If the last ARG evaluates to 0, let returns 1; let returns 0 otherwise. Evaluate conditional expression.
    
    This is a synonym for the "test" builtin, but the last argument must
    be a literal `]', to match the opening `['. Execute a simple command or display information about commands.
    
    Runs COMMAND with ARGS suppressing  shell function lookup, or display
    information about the specified COMMANDs.  Can be used to invoke commands
    on disk when a function with the same name exists.
    
    Options:
      -p	use a default value for PATH that is guaranteed to find all of
    	the standard utilities
      -v	print a description of COMMAND similar to the `type' builtin
      -V	print a more verbose description of each COMMAND
    
    Exit Status:
    Returns exit status of COMMAND, or failure if COMMAND is not found. Execute arguments as a shell command.
    
    Combine ARGs into a single string, use the result as input to the shell,
    and execute the resulting commands.
    
    Exit Status:
    Returns exit status of command or success if command is null. Execute commands as long as a test does not succeed.
    
    Expand and execute COMMANDS as long as the final command in the
    `until' COMMANDS has an exit status which is not zero.
    
    Exit Status:
    Returns the status of the last command executed. Execute commands as long as a test succeeds.
    
    Expand and execute COMMANDS as long as the final command in the
    `while' COMMANDS has an exit status of zero.
    
    Exit Status:
    Returns the status of the last command executed. Execute commands based on conditional.
    
    The `if COMMANDS' list is executed.  If its exit status is zero, then the
    `then COMMANDS' list is executed.  Otherwise, each `elif COMMANDS' list is
    executed in turn, and if its exit status is zero, the corresponding
    `then COMMANDS' list is executed and the if command completes.  Otherwise,
    the `else COMMANDS' list is executed, if present.  The exit status of the
    entire construct is the exit status of the last command executed, or zero
    if no condition tested true.
    
    Exit Status:
    Returns the status of the last command executed. Execute commands based on pattern matching.
    
    Selectively execute COMMANDS based upon WORD matching PATTERN.  The
    `|' is used to separate multiple patterns.
    
    Exit Status:
    Returns the status of the last command executed. Execute commands for each member in a list.
    
    The `for' loop executes a sequence of commands for each member in a
    list of items.  If `in WORDS ...;' is not present, then `in "$@"' is
    assumed.  For each element in WORDS, NAME is set to that element, and
    the COMMANDS are executed.
    
    Exit Status:
    Returns the status of the last command executed. Execute commands from a file in the current shell.
    
    Read and execute commands from FILENAME in the current shell.  The
    entries in $PATH are used to find the directory containing FILENAME.
    If any ARGUMENTS are supplied, they become the positional parameters
    when FILENAME is executed.
    
    Exit Status:
    Returns the status of the last command executed in FILENAME; fails if
    FILENAME cannot be read. Execute conditional command.
    
    Returns a status of 0 or 1 depending on the evaluation of the conditional
    expression EXPRESSION.  Expressions are composed of the same primaries used
    by the `test' builtin, and may be combined using the following operators:
    
      ( EXPRESSION )	Returns the value of EXPRESSION
      ! EXPRESSION		True if EXPRESSION is false; else false
      EXPR1 && EXPR2	True if both EXPR1 and EXPR2 are true; else false
      EXPR1 || EXPR2	True if either EXPR1 or EXPR2 is true; else false
    
    When the `==' and `!=' operators are used, the string to the right of
    the operator is used as a pattern and pattern matching is performed.
    When the `=~' operator is used, the string to the right of the operator
    is matched as a regular expression.
    
    The && and || operators do not evaluate EXPR2 if EXPR1 is sufficient to
    determine the expression's value.
    
    Exit Status:
    0 or 1 depending on value of EXPRESSION. Execute shell builtins.
    
    Execute SHELL-BUILTIN with arguments ARGs without performing command
    lookup.  This is useful when you wish to reimplement a shell builtin
    as a shell function, but need to execute the builtin within the function.
    
    Exit Status:
    Returns the exit status of SHELL-BUILTIN, or false if SHELL-BUILTIN is
    not a shell builtin.. Exit %d Exit a login shell.
    
    Exits a login shell with exit status N.  Returns an error if not executed
    in a login shell. Exit for, while, or until loops.
    
    Exit a FOR, WHILE or UNTIL loop.  If N is specified, break N enclosing
    loops.
    
    Exit Status:
    The exit status is 0 unless N is not greater than or equal to 1. Exit the shell.
    
    Exits the shell with a status of N.  If N is omitted, the exit status
    is that of the last command executed. File limit Floating point exception GNU bash, version %s (%s)
 GNU bash, version %s-(%s)
 GNU long options:
 Group commands as a unit.
    
    Run a set of commands in a group.  This is one way to redirect an
    entire set of commands.
    
    Exit Status:
    Returns the status of the last command executed. HFT input data pending HFT monitor mode granted HFT monitor mode retracted HFT sound sequence has completed HOME not set Hangup I have no name! I/O ready Illegal instruction Information request Interrupt Killed License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
 Modify or display completion options.
    
    Modify the completion options for each NAME, or, if no NAMEs are supplied,
    the completion currently being executed.  If no OPTIONs are given, print
    the completion options for each NAME or the current completion specification.
    
    Options:
    	-o option	Set completion option OPTION for each NAME
    	-D		Change options for the "default" command completion
    	-E		Change options for the "empty" command completion
    
    Using `+o' instead of `-o' turns off the specified option.
    
    Arguments:
    
    Each NAME refers to a command for which a completion specification must
    have previously been defined using the `complete' builtin.  If no NAMEs
    are supplied, compopt must be called by a function currently generating
    completions, and the options for that currently-executing completion
    generator are modified.
    
    Exit Status:
    Returns success unless an invalid option is supplied or NAME does not
    have a completion specification defined. Move job to the foreground.
    
    Place the job identified by JOB_SPEC in the foreground, making it the
    current job.  If JOB_SPEC is not present, the shell's notion of the
    current job is used.
    
    Exit Status:
    Status of command placed in foreground, or failure if an error occurs. Move jobs to the background.
    
    Place the jobs identified by each JOB_SPEC in the background, as if they
    had been started with `&'.  If JOB_SPEC is not present, the shell's notion
    of the current job is used.
    
    Exit Status:
    Returns success unless job control is not enabled or an error occurs. Null command.
    
    No effect; the command does nothing.
    
    Exit Status:
    Always succeeds. OLDPWD not set Parse option arguments.
    
    Getopts is used by shell procedures to parse positional parameters
    as options.
    
    OPTSTRING contains the option letters to be recognized; if a letter
    is followed by a colon, the option is expected to have an argument,
    which should be separated from it by white space.
    
    Each time it is invoked, getopts will place the next option in the
    shell variable $name, initializing name if it does not exist, and
    the index of the next argument to be processed into the shell
    variable OPTIND.  OPTIND is initialized to 1 each time the shell or
    a shell script is invoked.  When an option requires an argument,
    getopts places that argument into the shell variable OPTARG.
    
    getopts reports errors in one of two ways.  If the first character
    of OPTSTRING is a colon, getopts uses silent error reporting.  In
    this mode, no error messages are printed.  If an invalid option is
    seen, getopts places the option character found into OPTARG.  If a
    required argument is not found, getopts places a ':' into NAME and
    sets OPTARG to the option character found.  If getopts is not in
    silent mode, and an invalid option is seen, getopts places '?' into
    NAME and unsets OPTARG.  If a required argument is not found, a '?'
    is placed in NAME, OPTARG is unset, and a diagnostic message is
    printed.
    
    If the shell variable OPTERR has the value 0, getopts disables the
    printing of error messages, even if the first character of
    OPTSTRING is not a colon.  OPTERR has the value 1 by default.
    
    Getopts normally parses the positional parameters ($0 - $9), but if
    more arguments are given, they are parsed instead.
    
    Exit Status:
    Returns success if an option is found; fails if the end of options is
    encountered or an error occurs. Print the name of the current working directory.
    
    Options:
      -L	print the value of $PWD if it names the current working
    	directory
      -P	print the physical directory, without any symbolic links
    
    By default, `pwd' behaves as if `-L' were specified.
    
    Exit Status:
    Returns 0 unless an invalid option is given or the current directory
    cannot be read. Quit Read lines from a file into an array variable.
    
    A synonym for `mapfile'. Read lines from the standard input into an indexed array variable.
    
    Read lines from the standard input into the indexed array variable ARRAY, or
    from file descriptor FD if the -u option is supplied.  The variable MAPFILE
    is the default ARRAY.
    
    Options:
      -n count	Copy at most COUNT lines.  If COUNT is 0, all lines are copied.
      -O origin	Begin assigning to ARRAY at index ORIGIN.  The default index is 0.
      -s count 	Discard the first COUNT lines read.
      -t		Remove a trailing newline from each line read.
      -u fd		Read lines from file descriptor FD instead of the standard input.
      -C callback	Evaluate CALLBACK each time QUANTUM lines are read.
      -c quantum	Specify the number of lines read between each call to CALLBACK.
    
    Arguments:
      ARRAY		Array variable name to use for file data.
    
    If -C is supplied without -c, the default quantum is 5000.  When
    CALLBACK is evaluated, it is supplied the index of the next array
    element to be assigned and the line to be assigned to that element
    as additional arguments.
    
    If not supplied with an explicit origin, mapfile will clear ARRAY before
    assigning to it.
    
    Exit Status:
    Returns success unless an invalid option is given or ARRAY is readonly or
    not an indexed array. Record lock Remove directories from stack.
    
    Removes entries from the directory stack.  With no arguments, removes
    the top directory from the stack, and changes to the new top directory.
    
    Options:
      -n	Suppresses the normal change of directory when removing
    	directories from the stack, so only the stack is manipulated.
    
    Arguments:
      +N	Removes the Nth entry counting from the left of the list
    	shown by `dirs', starting with zero.  For example: `popd +0'
    	removes the first directory, `popd +1' the second.
    
      -N	Removes the Nth entry counting from the right of the list
    	shown by `dirs', starting with zero.  For example: `popd -0'
    	removes the last directory, `popd -1' the next to last.
    
    The `dirs' builtin displays the directory stack.
    
    Exit Status:
    Returns success unless an invalid argument is supplied or the directory
    change fails. Remove each NAME from the list of defined aliases.
    
    Options:
      -a	remove all alias definitions.
    
    Return success unless a NAME is not an existing alias. Remove jobs from current shell.
    
    Removes each JOBSPEC argument from the table of active jobs.  Without
    any JOBSPECs, the shell uses its notion of the current job.
    
    Options:
      -a	remove all jobs if JOBSPEC is not supplied
      -h	mark each JOBSPEC so that SIGHUP is not sent to the job if the
    	shell receives a SIGHUP
      -r	remove only running jobs
    
    Exit Status:
    Returns success unless an invalid option or JOBSPEC is given. Removes entries from the directory stack.  With no arguments, removes
    the top directory from the stack, and changes to the new top directory.
    
    Options:
      -n	Suppresses the normal change of directory when removing
    	directories from the stack, so only the stack is manipulated.
    
    Arguments:
      +N	Removes the Nth entry counting from the left of the list
    	shown by `dirs', starting with zero.  For example: `popd +0'
    	removes the first directory, `popd +1' the second.
    
      -N	Removes the Nth entry counting from the right of the list
    	shown by `dirs', starting with zero.  For example: `popd -0'
    	removes the last directory, `popd -1' the next to last.
    
    The `dirs' builtin displays the directory stack. Replace the shell with the given command.
    
    Execute COMMAND, replacing this shell with the specified program.
    ARGUMENTS become the arguments to COMMAND.  If COMMAND is not specified,
    any redirections take effect in the current shell.
    
    Options:
      -a name	pass NAME as the zeroth argument to COMMAND
      -c		execute COMMAND with an empty environment
      -l		place a dash in the zeroth argument to COMMAND
    
    If the command cannot be executed, a non-interactive shell exits, unless
    the shell option `execfail' is set.
    
    Exit Status:
    Returns success unless COMMAND is not found or a redirection error occurs. Report time consumed by pipeline's execution.
    
    Execute PIPELINE and print a summary of the real time, user CPU time,
    and system CPU time spent executing PIPELINE when it terminates.
    
    Options:
      -p	print the timing summary in the portable Posix format
    
    The value of the TIMEFORMAT variable is used as the output format.
    
    Exit Status:
    The return status is the return status of PIPELINE. Resume for, while, or until loops.
    
    Resumes the next iteration of the enclosing FOR, WHILE or UNTIL loop.
    If N is specified, resumes the Nth enclosing loop.
    
    Exit Status:
    The exit status is 0 unless N is not greater than or equal to 1. Resume job in foreground.
    
    Equivalent to the JOB_SPEC argument to the `fg' command.  Resume a
    stopped or background job.  JOB_SPEC can specify either a job name
    or a job number.  Following JOB_SPEC with a `&' places the job in
    the background, as if the job specification had been supplied as an
    argument to `bg'.
    
    Exit Status:
    Returns the status of the resumed job. Return a successful result.
    
    Exit Status:
    Always succeeds. Return an unsuccessful result.
    
    Exit Status:
    Always fails. Return from a shell function.
    
    Causes a function or sourced script to exit with the return value
    specified by N.  If N is omitted, the return status is that of the
    last command executed within the function or script.
    
    Exit Status:
    Returns N, or failure if the shell is not executing a function or script. Return the context of the current subroutine call.
    
    Without EXPR, returns "$line $filename".  With EXPR, returns
    "$line $subroutine $filename"; this extra information can be used to
    provide a stack trace.
    
    The value of EXPR indicates how many call frames to go back before the
    current one; the top frame is frame 0.
    
    Exit Status:
    Returns 0 unless the shell is not executing a shell function or EXPR
    is invalid. Returns the context of the current subroutine call.
    
    Without EXPR, returns  Running Segmentation fault Select words from a list and execute commands.
    
    The WORDS are expanded, generating a list of words.  The
    set of expanded words is printed on the standard error, each
    preceded by a number.  If `in WORDS' is not present, `in "$@"'
    is assumed.  The PS3 prompt is then displayed and a line read
    from the standard input.  If the line consists of the number
    corresponding to one of the displayed words, then NAME is set
    to that word.  If the line is empty, WORDS and the prompt are
    redisplayed.  If EOF is read, the command completes.  Any other
    value read causes NAME to be set to null.  The line read is saved
    in the variable REPLY.  COMMANDS are executed after each selection
    until a break command is executed.
    
    Exit Status:
    Returns the status of the last command executed. Send a signal to a job.
    
    Send the processes identified by PID or JOBSPEC the signal named by
    SIGSPEC or SIGNUM.  If neither SIGSPEC nor SIGNUM is present, then
    SIGTERM is assumed.
    
    Options:
      -s sig	SIG is a signal name
      -n sig	SIG is a signal number
      -l	list the signal names; if arguments follow `-l' they are
    	assumed to be signal numbers for which names should be listed
    
    Kill is a shell builtin for two reasons: it allows job IDs to be used
    instead of process IDs, and allows processes to be killed if the limit
    on processes that you can create is reached.
    
    Exit Status:
    Returns success unless an invalid option is given or an error occurs. Set and unset shell options.
    
    Change the setting of each shell option OPTNAME.  Without any option
    arguments, list all shell options with an indication of whether or not each
    is set.
    
    Options:
      -o	restrict OPTNAMEs to those defined for use with `set -o'
      -p	print each shell option with an indication of its status
      -q	suppress output
      -s	enable (set) each OPTNAME
      -u	disable (unset) each OPTNAME
    
    Exit Status:
    Returns success if OPTNAME is enabled; fails if an invalid option is
    given or OPTNAME is disabled. Set export attribute for shell variables.
    
    Marks each NAME for automatic export to the environment of subsequently
    executed commands.  If VALUE is supplied, assign VALUE before exporting.
    
    Options:
      -f	refer to shell functions
      -n	remove the export property from each NAME
      -p	display a list of all exported variables and functions
    
    An argument of `--' disables further option processing.
    
    Exit Status:
    Returns success unless an invalid option is given or NAME is invalid. Set variable values and attributes.
    
    Obsolete.  See `help declare'. Shell commands matching keyword ` Shell commands matching keywords ` Shell options:
 Shift positional parameters.
    
    Rename the positional parameters $N+1,$N+2 ... to $1,$2 ...  If N is
    not given, it is assumed to be 1.
    
    Exit Status:
    Returns success unless N is negative or greater than $#. Signal %d Specify how arguments are to be completed by Readline.
    
    For each NAME, specify how arguments are to be completed.  If no options
    are supplied, existing completion specifications are printed in a way that
    allows them to be reused as input.
    
    Options:
      -p	print existing completion specifications in a reusable format
      -r	remove a completion specification for each NAME, or, if no
    	NAMEs are supplied, all completion specifications
      -D	apply the completions and actions as the default for commands
    	without any specific completion defined
      -E	apply the completions and actions to "empty" commands --
    	completion attempted on a blank line
    
    When completion is attempted, the actions are applied in the order the
    uppercase-letter options are listed above.  The -D option takes
    precedence over -E.
    
    Exit Status:
    Returns success unless an invalid option is supplied or an error occurs. Stopped Stopped (signal) Stopped (tty input) Stopped (tty output) Stopped(%s) Suspend shell execution.
    
    Suspend the execution of this shell until it receives a SIGCONT signal.
    Unless forced, login shells cannot be suspended.
    
    Options:
      -f	force the suspend, even if the shell is a login shell
    
    Exit Status:
    Returns success unless job control is not enabled or an error occurs. TIMEFORMAT: `%c': invalid format character Terminated The mail in %s has been read
 There are running jobs.
 There are stopped jobs.
 These shell commands are defined internally.  Type `help' to see this list.
Type `help name' to find out more about the function `name'.
Use `info bash' to find out more about the shell in general.
Use `man -k' or `info' to find out more about commands not in this list.

A star (*) next to a name means that the command is disabled.

 Trap signals and other events.
    
    Defines and activates handlers to be run when the shell receives signals
    or other conditions.
    
    ARG is a command to be read and executed when the shell receives the
    signal(s) SIGNAL_SPEC.  If ARG is absent (and a single SIGNAL_SPEC
    is supplied) or `-', each specified signal is reset to its original
    value.  If ARG is the null string each SIGNAL_SPEC is ignored by the
    shell and by the commands it invokes.
    
    If a SIGNAL_SPEC is EXIT (0) ARG is executed on exit from the shell.  If
    a SIGNAL_SPEC is DEBUG, ARG is executed before every simple command.  If
    a SIGNAL_SPEC is RETURN, ARG is executed each time a shell function or a
    script run by the . or source builtins finishes executing.  A SIGNAL_SPEC
    of ERR means to execute ARG each time a command's failure would cause the
    shell to exit when the -e option is enabled.
    
    If no arguments are supplied, trap prints the list of commands associated
    with each signal.
    
    Options:
      -l	print a list of signal names and their corresponding numbers
      -p	display the trap commands associated with each SIGNAL_SPEC
    
    Each SIGNAL_SPEC is either a signal name in <signal.h> or a signal number.
    Signal names are case insensitive and the SIG prefix is optional.  A
    signal may be sent to the shell with "kill -signal $$".
    
    Exit Status:
    Returns success unless a SIGSPEC is invalid or an invalid option is given. Type `%s -c "help set"' for more information about shell options.
 Type `%s -c help' for more information about shell builtin commands.
 Unknown Signal # Unknown Signal #%d Unknown error Unknown status Urgent IO condition Usage:	%s [GNU long option] [option] ...
	%s [GNU long option] [option] script-file ...
 Use "%s" to leave the shell.
 Use the `bashbug' command to report bugs.
 User signal 1 User signal 2 Window changed Write arguments to the standard output.
    
    Display the ARGs on the standard output followed by a newline.
    
    Options:
      -n	do not append a newline
    
    Exit Status:
    Returns success unless a write error occurs. You have mail in $_ You have new mail in $_ [ arg... ] [[ expression ]] `%c': bad command `%c': invalid format character `%c': invalid symbolic mode character `%c': invalid symbolic mode operator `%c': invalid time format specification `%s': cannot unbind `%s': invalid alias name `%s': invalid keymap name `%s': missing format character `%s': not a pid or valid job spec `%s': not a valid identifier `%s': unknown function name `)' expected `)' expected, found %s `:' expected for conditional expression add_process: pid %5ld (%s) marked as still alive add_process: process %5ld (%s) in the_pipeline alias [-p] [name[=value] ... ] all_local_variables: no function context at current scope argument argument expected array variable support required attempted assignment to non-variable bad array subscript bad command type bad connector bad jump bad substitution: no closing "`" in %s bad substitution: no closing `%s' in %s bash_execute_unix_command: cannot find keymap for command bg [job_spec ...] break [n] bug: bad expassign token builtin [shell-builtin [arg ...]] caller [expr] can only `return' from a function or sourced script can only be used in a function cannot allocate new file descriptor for bash input from fd %d cannot create temp file for here-document: %s cannot duplicate fd %d to fd %d cannot duplicate named pipe %s as fd %d cannot find %s in shared object %s: %s cannot make child for command substitution cannot make child for process substitution cannot make pipe for command substitution cannot make pipe for process substitution cannot open named pipe %s for reading cannot open named pipe %s for writing cannot open shared object %s: %s cannot redirect standard input from /dev/null: %s cannot reset nodelay mode for fd %d cannot set and unset shell options simultaneously cannot set terminal process group (%d) cannot simultaneously unset a function and a variable cannot suspend cannot suspend a login shell cannot use `-f' to make functions cannot use more than one of -anrw case WORD in [PATTERN [| PATTERN]...) COMMANDS ;;]... esac child setpgid (%ld to %ld) command [-pVv] command [arg ...] command_substitute: cannot duplicate pipe as fd 1 compgen [-abcdefgjksuv] [-o option]  [-A action] [-G globpat] [-W wordlist]  [-F function] [-C command] [-X filterpat] [-P prefix] [-S suffix] [word] complete [-abcdefgjksuv] [-pr] [-DE] [-o option] [-A action] [-G globpat] [-W wordlist]  [-F function] [-C command] [-X filterpat] [-P prefix] [-S suffix] [name ...] completion: function `%s' not found compopt [-o|+o option] [-DE] [name ...] conditional binary operator expected continue [n] coproc [NAME] command [redirections] could not find /tmp, please create! cprintf: `%c': invalid format character current deleting stopped job %d with process group %ld describe_pid: %ld: no such pid directory stack empty directory stack index dirs [-clpv] [+N] [-N] disown [-h] [-ar] [jobspec ...] division by 0 dynamic loading not available echo [-n] [arg ...] echo [-neE] [arg ...] empty array variable name enable [-a] [-dnps] [-f filename] [name ...] error getting terminal attributes: %s error importing function definition for `%s' error setting terminal attributes: %s eval [arg ...] exec [-cl] [-a name] [command [arguments ...]] [redirection ...] exit [n] expected `)' exponent less than 0 export [-fn] [name[=value] ...] or export -p expression expected expression recursion level exceeded false fc [-e ename] [-lnr] [first] [last] or fc -s [pat=rep] [command] fg [job_spec] file descriptor out of range filename argument required for (( exp1; exp2; exp3 )); do COMMANDS; done for NAME [in WORDS ... ] ; do COMMANDS; done forked pid %d appears in running job %d free: called with already freed block argument free: called with unallocated block argument free: start and end chunk sizes differ free: underflow detected; mh_nbytes out of range function name { COMMANDS ; } or name () { COMMANDS ; } future versions of the shell will force evaluation as an arithmetic substitution getcwd: cannot access parent directories getopts optstring name [arg] hash [-lr] [-p pathname] [-dt] [name ...] hashing disabled help [-dms] [pattern ...] here-document at line %d delimited by end-of-file (wanted `%s') history [-c] [-d offset] [n] or history -anrw [filename] or history -ps arg [arg...] history position history specification hits	command
 identifier expected after pre-increment or pre-decrement if COMMANDS; then COMMANDS; [ elif COMMANDS; then COMMANDS; ]... [ else COMMANDS; ] fi initialize_job_control: getpgrp failed initialize_job_control: line discipline initialize_job_control: setpgid invalid arithmetic base invalid base invalid character %d in exportstr for %s invalid hex number invalid number invalid octal number invalid signal number job %d started without job control job_spec [&] jobs [-lnprs] [jobspec ...] or jobs -x command [args] kill [-s sigspec | -n signum | -sigspec] pid | jobspec ... or kill -l [sigspec] last command: %s
 let arg [arg ...] limit line %d:  line editing not enabled local [option] name[=value] ... logout
 logout [n] loop count make_here_document: bad instruction type %d make_local_variable: no function context at current scope make_redirection: redirection instruction `%d' out of range malloc: block on free list clobbered malloc: failed assertion: %s
 mapfile [-n count] [-O origin] [-s count] [-t] [-u fd] [-C callback] [-c quantum] [array] migrate process to another CPU missing `)' missing `]' missing hex digit for \x missing unicode digit for \%c network operations not supported no `=' in exportstr for %s no closing `%c' in %s no command found no help topics match `%s'.  Try `help help' or `man -k %s' or `info %s'. no job control no job control in this shell no match: %s no other directory no other options allowed with `-x' not currently executing completion function not login shell: use `exit' octal number only meaningful in a `for', `while', or `until' loop pipe error pop_scope: head of shell_variables not a temporary environment scope pop_var_context: head of shell_variables not a function context pop_var_context: no global_variables context popd [-n] [+N | -N] power failure imminent print_command: bad connector `%d' printf [-v var] format [arguments] progcomp_insert: %s: NULL COMPSPEC programming error pushd [-n] [+N | -N | dir] pwd [-LP] read [-ers] [-a array] [-d delim] [-i text] [-n nchars] [-N nchars] [-p prompt] [-t timeout] [-u fd] [name ...] read error: %d: %s readarray [-n count] [-O origin] [-s count] [-t] [-u fd] [-C callback] [-c quantum] [array] readonly [-aAf] [name[=value] ...] or readonly -p realloc: called with unallocated block argument realloc: start and end chunk sizes differ realloc: underflow detected; mh_nbytes out of range recursion stack underflow redirection error: cannot duplicate fd register_alloc: %p already in table as allocated?
 register_alloc: alloc table is full with FIND_ALLOC?
 register_free: %p already in table as free?
 restricted return [n] run_pending_traps: bad value in trap_list[%d]: %p run_pending_traps: signal handler is SIG_DFL, resending %d (%s) to myself save_bash_input: buffer already exists for new fd %d select NAME [in WORDS ... ;] do COMMANDS; done set [-abefhkmnptuvxBCHP] [-o option-name] [--] [arg ...] setlocale: %s: cannot change locale (%s) setlocale: %s: cannot change locale (%s): %s setlocale: LC_ALL: cannot change locale (%s) setlocale: LC_ALL: cannot change locale (%s): %s shell level (%d) too high, resetting to 1 shift [n] shift count shopt [-pqsu] [-o] [optname ...] sigprocmask: %d: invalid operation source filename [arguments] start_pipeline: pgrp pipe suspend [-f] syntax error syntax error in conditional expression syntax error in conditional expression: unexpected token `%s' syntax error in expression syntax error near `%s' syntax error near unexpected token `%s' syntax error: `((%s))' syntax error: `;' unexpected syntax error: arithmetic expression required syntax error: invalid arithmetic operator syntax error: operand expected syntax error: unexpected end of file system crash imminent test [expr] time [-p] pipeline times too many arguments trap [-lp] [[arg] signal_spec ...] trap_handler: bad signal %d true type [-afptP] name [name ...] typeset [-aAfFgilrtux] [-p] name[=value] ... umask [-p] [-S] [mode] unalias [-a] name [name ...] unexpected EOF while looking for `]]' unexpected EOF while looking for matching `%c' unexpected EOF while looking for matching `)' unexpected argument `%s' to conditional binary operator unexpected argument `%s' to conditional unary operator unexpected argument to conditional binary operator unexpected argument to conditional unary operator unexpected token %d in conditional command unexpected token `%c' in conditional command unexpected token `%s' in conditional command unexpected token `%s', conditional binary operator expected unexpected token `%s', expected `)' unknown unknown command error until COMMANDS; do COMMANDS; done value too great for base variables - Names and meanings of some shell variables wait: pid %ld is not a child of this shell wait_for: No record of process %ld wait_for_job: job %d is stopped waitchld: turning on WNOHANG to avoid indefinite block warning:  warning: %s: %s warning: -C option may not work as you expect warning: -F option may not work as you expect while COMMANDS; do COMMANDS; done write error: %s xtrace fd (%d) != fileno xtrace fp (%d) xtrace_set: %d: invalid file descriptor xtrace_set: NULL file pointer { COMMANDS ; } Project-Id-Version: bash 2.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-02-11 11:19-0500
PO-Revision-Date: 2014-02-18 03:12+0000
Last-Translator: Halley Pacheco de Oliveira <Unknown>
Language-Team: Brazilian Portuguese <ldp-br@bazar.conectiva.com.br>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 15:59+0000
X-Generator: Launchpad (build 18115)
 fim do tempo aguardando por entrada: auto-deslogar
 	-%s ou -o opção
 
malloc: %s:%d: garantia arruinada
   (wd: %s)  (imagem do núcleo gravada)  linha  $%s: impossível atribuir desta maneira %c%c: opção inválida %d: descritor de arquivo inválido: %s %s pode ser invocado via  %s possui exportstr nulo %s é %s
 %s é uma função
 %s é um comando interno do interpretador
 %s é uma palavra chave do interpretador
 %s está apelidado como `%s'
 %s está hasheado (%s)
 %s não está vinculado à nenhuma tecla.
 %s fora da faixa %s%s%s: %s (token com erro é "%s") %s: %s %s: %s fora da faixa %s: %s: interpretador inválido %s: %s: impossível abrir como ARQUIVO %s: %s: valor inválido para rastrear descritor de arquivo %s: %s: subscrito é necessário para atribuições de vetor associativo %s: %s:%d: impossível alocar %lu bytes %s: %s:%d: impossível alocar %lu bytes (%lu bytes alocados) %s: especificação de trabalho ambígua %s: redirecionamento ambíguo %s: argumentos devem ser processos ou IDs de tarefas %s: especificação de caminho de rede inválido %s: substituição incorreta %s: esperado operador binário %s: impossível alocar %lu bytes %s: impossível alocar %lu bytes (%lu bytes alocados) %s: impossível definir fd como variável %s: impossível atribuir uma lista a um membro de uma matriz (array) %s: impossível atribuir a índice não numérico %s: não é possível converter vetor associativo em indexado %s: não é possível converter vetor indexado em associativo %s: impossível criar: %s %s: impossível apagar: %s %s: não é possível destruir variáveis de matriz desta forma %s: impossível executar o arquivo binário %s: impossível executar: %s %s: impossível ler o limite: %s %s: impossível modificar o limite: %s %s: impossível abrir arquivo temporário: %s %s: impossível abrir: %s %s: não é possível sobrescrever arquivo existente %s: não é possível ler: %s %s: impossível desconfigurar %s: impossível desconfigurar: somente leitura %s %s: comando não encontrado %s: erro lendo diretório atual: %s: %s
 %s: erro na expressão
 %s: arquivo é muito grande %s: arquivo não encontrado %s: primeiro caracter não-branco não é `"' %s: tabela de hash vazia
 %s: expansão do histórico falhou %s: servidor desconhecido %s: opção ilegal -- %c
 %s: inlib falhou %s: esperado expressão de número inteiro %s: nome de ação inválido %s: origem do vetor inválida %s: chave inválida para o vetor associativo %s: quantum de callback inválido %s: especificação de descritor de arquivo inválida %s: argumento limite inválido %s: número de linhas inválido %s: opção inválida %s: nome de opção inválida %s: serviço inválido %s: opção do interpretador de comandos inválida %s: especificação de sinal inválida %s: especificação de limite de tempo inválida %s: é um diretório %s: trabalho %d já está em segundo plano %s: o trabalho terminou %s: linha %d:  %s: faltando separador dois pontos %s: sem especificação para completar %s: sem controle de trabalho %s: trabalho não existe %s: não é uma função %s: não é um arquivo comum %s: não é um comando embutido do shell %s: não é uma variável de vetor %s: não é uma matriz indexada %s: não foi carregado dinâmicamente %s: não encontrado %s: necessário argumento numérico %s: opção requer um argumento %s: opção requer um argumento -- %c
 %s: parâmetro nulo ou não inicializado %s: função somente para leitura %s: a variável permite somente leitura %s: restrito %s: restrito: não é possível redirecionar saída %s: restrição: não é permitido especificar `/' em nomes de comandos %s: expressão de substring < 0 %s: esperado operador unário %s: variável não vinculada %s: uso:  (( expressão )) (imagem do núcleo gravada)  (wd agora: %s)
 . arquivo [argumentos] /dev/(tcp|udp)/host/port não suportado sem rede /tmp precisa ser um nome válido de diretório : <sem diretório atual> Instrução ABORT Interrompendo... Adicionar diretórios à pilha.
    
    Adiciona um diretório ao topo da pilha de diretórios, ou rotaciona
    a pilha, fazendo do novo topo da pilha o diretório atual.
    Sem argumentos, troca os dois primeiros lugares da pilha entre si.
    
    Opções:
      -n	Suprime a mundança normal de diretório quando forem
    	adicionados diretórios à pilha, para que só a pilha seja
    	manipulada.
        
    Argumentos:
      +N	Rotaciona a pilha para que o enésimo diretório (contando
    	da esquerda da lista exibida por `dirs', começando por zero)
    	esteja no topo.
      -N	Rotaciona a pilha para que o enésimo diretório (contando
    	da direita da lista exibida por `dirs', começando com zero)
    	esteja no topo.
      
      dir	Adiciona DIR ao topo da pilha de diretórios, fazendo
    	dele o novo diretório atual em funcionamento.
      
    O comando embutido `dirs' exibe a pilha de diretórios.
    
    Status de saída:
    Retorna bem-sucedido a menos que um argumento inválido seja
    fornecido ou que a mudança de diretório falhe. Adiciona um diretório ao topo da pilha de diretórios, ou roda a pilha, 
    fazendo do topo da pilha a pasta atual.  Se chamado sem argumentos,
    troca os dois primeiros diretórios.
    
    Opções:
      -n	Omite a mudança de pasta ao adicionar diretórios à pilha,
    	de modo que apenas a pilha seja manipulada.
    
    Argumentos:
      +N	Roda a pilha de modo que a N-ésima pasta (a partir da
    	esquerda da lista mostrada por `dirs', começando do zero)
    	esteja no topo.
    
      -N	Roda a pilha de modo que a N-ésima pasta (a partir da
    	direita da lista mostrada por `dirs', começando do zero)
    	esteja no topo.
    
      dir	Adiciona DIR ao topo da pilha de pastas, fazendo dele a
    	nova pasta atual.
    
    O comando interno `dirs' exibe a pilha de diretórios. Alarme (profile) Alarme virtual de tempo Relógio de alarme Aritmética para loop.
    
    Equivalente a
    	(( EXP1 ))
    	while (( EXP2 )); do
    		 COMANDOS
    		((EXP3))
    	done
    EXP1, EXP2 e EXP3 são expressões aritméticas. Se alguma expressão
    for omitida, ele se comporta como se definido com valor 1.
    
    Status de saída:
    Retorna o status do último comando executado. BPT Rastreamento/Captura (BPT trace/trap) Chamada incorreta do sistema Sinal falso `Pipe' partido (Escrita sem leitura) Erro do barramento Tempo limite de CPU excedido Processo filho parado ou terminado Nomes de variáveis comuns do shell e seu uso.
    
    BASH_VERSION	Informação da versão deste Bash.
    CDPATH	Uma lista de diretórios, separados por dois-pontos, para
    		pesquisar por diretórios dados como argumentos em `cd'.
    GLOBIGNORE	Uma lista de padrões, separados por dois-pontos, descrevendo
    		nomes de arquivos a serem ignorados pela expansão do caminho.
    HISTFILE	O nome do arquivo onde o histórico de comandos é armazenado.
    HISTFILESIZE	O número máximo de linhas que este arquivo pode conter.
    HISTSIZE	O número máximo de linhas de histórico que um shell em
    		execução pode acessar.
    HOME	O caminho completo para o seu diretório de login.
    HOSTNAME	O nome do host atual.
    HOSTTYPE	O tipo de CPU que está executando esta versão do Bash.
    IGNOREEOF	Controla a ação do shell ao receber um caractere EOF como
    		única entrada. Se ativado, então o seu valor é o número de
    		caracteres que podem ser vistos em sequência em uma linha vazia
    		antes que o shell finalize (padrão 10). Se desativado, EOF
    		sinaliza o fim da entrada.
    MACHTYPE	Uma string descrevendo o sistema atual em que o Bash está
    		sendo executando.
    MAILCHECK	Com que frequência, em segundos, o Bash verifica por novas
    		mensagens de correio.
    MAILPATH	Uma lista de nomes de arquivos, separados por dois-pontos,
    		onde o Bash verifica por novas mensagens de correio.
    OSTYPE	A versão do Unix que está executando esta versão do Bash.
    PATH	Uma lista de diretórios, separados por dois-pontos, a serem
    		pesquisados ao procurar por comandos.
    PROMPT_COMMAND	Um comando a ser executado antes da impressão de cada
    		prompt primário.
    PS1		A string do prompt primário.
    PS2		A string do prompt secundário.
    PWD	O caminho completo do diretório atual.
    SHELLOPTS	Uma lista das opções habilitadas do shell, separadas por
    		dois pontos.
    TERM	O nome do tipo do terminal atual.
    TIMEFORMAT	O formato de saída para estatísticas de tempo exibidas
    		pela palavra reservada `time'.
    auto_resume	Se tiver valor não-nulo, significa que uma palavra de
    		comando aparecendo sozinha numa linha é procurada primeiro na
    		lista dos jobs parados. Se encontrada lá, aquele job é passado
    		para o primeiro-plano. Um valor de `exact' significa que a
    		palavra de comando deve coincidir exatamente com um comando
    		na lista de jobs parados. Um valor de `substring' significa
    		que a palavra de comando deve concidir com uma substring do
    		job. Qualquer outro valor significa que o comando deve ser
    		um prefixo de um job parado.
    histchars	Caracteres que controlam a expansão e a `substituição
    		rápida' do histórico. O primeiro caractere é o de `substituição
    		do histórico', normalmente `!'. O segundo é o de `substituição
    		rápida', normalmente `^'. O terceiro é o caractere de `comentário
    		do histórico', normalmente `#'.
    HISTIGNORE	Uma lista de padrões, separados por dois-pontos, usada para
    		dedicir qual comando deve ser salvo na lista histórico.
 Continuar Criar um coprocesso chamado NOME.
    
    Execute COMANDO assincronamente, com a entrada e saída padrão do
    comando conectado via pipe aos arquivos descritores associados
    aos índices 0 e 1 de uma matriz variável chamada NOME no shell
    em execução.
    O NOME padrão é "COPROC".
    
    Status de saída:
    Retorma o status de saída de COMANDO. Define ou mostra apelidos.
    
    Sem argumentos, `alias' imprime na saída padrão a
    lista de apelidos na forma reusável `alias NOME=VALOR' .
    
    Por outro lado, um apelido é definido para cada NOME cujo VALOR é dado.
    Um espaço à direita em VALOR faz com que seja verificada a substituição
    de apelido na próxima palavra quando o apelido é expandido.
    
    Opções:
      -p	Imprime todos os apelidos definidos em um formato reusável
    
    Estado de saída:
    alias retorna `verdadeiro' a não ser que fornecido um NOME para o qual
    nenhum apelido tenha sido definido. Cria uma função shell chamada NAME. Quando chamada como um comando simples,
    NAME roda COMMANDs na contexto da chamada do shell. Quando NAME é chamada,
    os argumentos $1,..., $n são passados para a funções e o nome da função
    é $FUNCNAME
    
    Status de Saída:
    Returna sucesso a não ser que NAME seja apenas para leitura. Exibir a pilha de diretórios.
    
    Exibe a lista dos diretórios atualmente registrados. Diretórios
    são inseridos na lista com o comando `pushd'. Você pode retirá-los
    da lista com o comando `popd'.
    
    Opções:
      -c	apaga a pilha de diretórios, excluindo todos os seus
    	elementos.
      -l	não imprime as versões com prefixo `~' de diretórios
    	relacionados ao seu diretório home.
      -p	imprime a pilha de diretórios com uma entrada por linha.
      -v	imprime a pilha de diretórios com uma entrada por linha
    	prefixada com a sua posiação na pilha.
    
    Argumentos:
      +N	Exibe a enésima entrada, contando a partir da esquerda
    	da lista exibida por dirs, quando invocado sem opções,
    	começando de zero.
    
      -N	Exibe a enésima entrada, contando da direita da lista
    	mostrada por dirs, quando invocado sem opções, começando
    	por zero.
    
    Status de saída:
    Retorna bem-sucedido, a menos que uma opção inválida seja
    fornecida ou que um erro ocorra. Exibe informação sobre comandos builtin.
    
    Exibe um breve resumo dos comandos builtin. Se PATTERN for
    especificado, retorna ajuda detalhada de todos os comandos que coicidirem com PATTERN.
    caso contrário a lista de tópicos de ajuda é impressa.
    
    Opções:
      -d	imprime uma breve descrição para cada tópico
      -m	mostra o uso no formato de uma pseudo-manpage
      -s	imprime apenas uma pequena sinopse de uso para cada tópico correspondente
    	PATTERN
    
    Argumentos:
      PATTERN	Padrão especificando um tópico de ajuda
    
    Estados de saída:
    Retorna sucesso a menos que o PATTERN não seja encontrado ou uma opção inválida é inserida. Exibe informação sobre o tipo de comando.
    
    Para cada NOME, indica como ele seria interpretado se usado como um
    nome de comando.
    
    Opções:
      -a	exibe todos os locais contendo um executável com o nome NOME;
    	inclui alias, builtins, e funções, se e somente se
    	a opção `-p' não for usada também
      -f	omite a busca por funções do shell
      -P	força um CAMINHO para buscar cada NOME, mesmo se ele for um
    	alias, builtin, ou função, e retorna o nome do arquivo do disco
    	que seria executado
      -p	retorna qualquer nome de arquivo do disco que seria executado,
    	ou nada se `type -t NOME' não retornar `file'.
      -t	imprime um única palavra a qual é um `alias', `keyword',
    	`function', `builtin', `file' ou `', se NOME é um alias, palavra
    	reserveda do shell, função do shell, builtin do shell, arquivo do disco,
    	ou não encontrado, respectivamente
    
    Argumentos:
      NOME	Nome do comando para ser interpretado.
    
    Estados de saída:
    Retorna sucesso se todos os NOMEs forem encontrados; falha se qualquer
    um não for encontrado. Mostra ou executa comandos do histórico.
    
    fc é usado para listar ou editar e re-executar comandos do
    histórico. PRIMEIRO e ÚLTIMO podem ser números especificando a
    faixa, ou PRIMEIRO pode ser uma string, que representa o comando
    mais recente começando com esta string.
    
    Opções:
      -e ENOME	selecionar qual editor usar. O padrão é FCEDIT,
    		ou então EDITOR, e então vi
      -l 	listar linhas ao invés de editar
      -n	omitir números de linhas quando listando
      -r	inverter a ordem das linhas (mais novas listadas primeiro)
    
    Com o formato `fc -s [pat=rep ...] [comando]', COMANDO é
    re-executado após a substituição VELHO=NOVO ter sido executada.
    
    Um apelido útil é r='fc -s', de tal forma que digitando `r cc' é
    executado o último comando começando com `cc' e digitando `r' é
    re-executado o último comando.
    
    Estado de saída:
    Retorna sucesso ou estado de comando executado;
    não-zero se um erro correr. Exibe ou manipula o histórico
    
    Exibe o histórico com numeração de linhas, prefixando cada entrada modificada com um '*'. Um argumento N mostra somenta as ultimas N entradas.
    
    Opções:
      -c	 limpa o histórico apagando todas as entradas.
      -d offset	 apaga a entrada do histórico no offset OFFSET.
    
      -a	 adiciona linhas do histórico desta sessão ao arquivo de historico.
      -n	 lê todas as linhas do histórico ainda não lidas do arquivo de historico.
      -r	 le o arquivo de historico e adiciona o conteúdo a lista do
    	histórico
      -w	 grava o histórico atual para o arquivo de historico
    	e o adiciona à lista do histórico
      -p	realiza a expansão do histórico para cada ARG e exibe o resultado
    	sem armazena-lo na lista do histórico
      -s	adiciona os ARGs à lista do histórico como uma unica entrada
    
    Se FILENAME é dado, este é usado como o arquivo de histórico. Por outro lado, se $HISTFILE tem um valor, este é usado, ou então ~/.bash_history.
    
    Se a variável $HISTTIMEFORMAT está setada e não é nula, seu valor é usado
    como um formato de string para strftime(3) para imprimir o time stamp associado
    com cada entrada do histórico exibida. Nenhum time stamp é impresso de outra maneira.
    
    Status de saída:
    Retorna sucesso a menos que seja dada uma opção inválida ou um erro ocorra. Exibe ou altera a máscara de modo do arquivo.
    
    Altera a máscara de criação de arquivo do usuário para MODE. Se MODE for omitido, mostra
    o valor atual para a máscara.
    
    Se começar com um dígito, MODE será interpretado como um número octal;
    caso contrário será uma string simbolizando o modo, como em chmod(1)
    
    Opções:
      -p	 se MODE for omitido, gera uma saída num formato que pode ser reusado como entrada
      -S	 gera a saída como uma string simbolizando o modo, caso contrário um número octal será gerado
    
    Resultado da saída:
    Retorna sucesso a não ser que MODE é inválido ou um opção inválida foi informada. Exibe possíveis complementações dependendo das opções.
    
    Destinado a ser usado de dentro de uma função shell gerando possíveis
    complementações.  Se o argumento opcional WORD for fornecido, correspondências contra
    WORD são geradas.
    
    Estado de saída:
    Retorna sucesso a não ser que uma opção inválida seja fornecida ou um erro ocorra. Exibe tempos de processo.
    
    Imprime o usuário e os tempos acumulados do sistema para o shell e todos os seus processos filhos.
    
    Status de saída:
    Sempre com sucesso. Mostra a lista dos diretórios lembrados atualmente.  Os diretórios entram na
    lista com o comando `pushd'; você pode voltar pela lista com o comando
    `popd'.
    
    Opções:
      -c	limpa a pilha de diretórios, apagando todos os elementos
      -l	não abreviar com til diretórios relativos à sua pasta pessoal
      -p	imprime a pilha de diretórios com uma entrada por linha
      -v	imprime a pilha de diretórios com uma entrada por linha precedida
    	por sua posição na pilha
    
    Argumentos:
      +N	Mostra a N-ésima entada a partir da esquerda da lista que é
    	exibida quando dirs é chamado sem opções, começando de zero.
    
      -N	Mostra a N-ésima entada a partir da direita da lista que é
    	exibida quando dirs é chamado sem opções, começando de zero. Concluído Concluído(%d) Instrução EMT Habilita e desabilita comandos nativos do shell.
    
    Habilita e desabilita comandos nativos do shell. Desabilitando-os
    é possível executar um comando no disco que tenha o mesmo nome que
    um comando nativo do shell sem ter que usar o caminho completo.
    
    Opções:
      -a	imprime uma lista de comandos nativos mostrando se estão
      	ou não habilitados
      -n	desabilita cada NOME ou mostra uma lista de comandos
      	nativos desabilitados
      -p	imprime a lista de comandos nativos em um formato reusável
      -s	imprime apenas os nomes dos comandos nativos `especiais'
      	do Posix
    
    Opções de controle de carga dinâmica:
      -f	Carrega o comando nativo NOME do objeto NOME-DO-ARQUIVO
      	compartilhado
      -d	Remove um comando nativo carregado com -f
    
    Sem opções, cada NOME é habilitado.
    
    Para usar o `test' encontrado em $PATH ao invés da versão nativa
    digite, `enable -n test'.
    
    Estado de saída:
    Retorna sucesso a não ser que NOME não seja um comando nativo ou
    um erro ocorra. Validação de expressão aritmética.
    
    A EXPRESSÃO é avaliada de acordo com as regras de validação
    aritmética.  Equivalente a "let EXPRESSÃO".
    
    Estado de Saída:
    Retorna 1 se a EXPRESSÃO for avaliada para 0; retorna 0 caso contrário. Avaliar expressões aritméticas.
    
    Avalie cada ARG como uma expressão aritmética. A avaliação é feita em
    números inteiros de tamanho fixo, sem que se procure por transbordamentos,
    embora a divisão por zero seja presa e sinalizada como erro.  A seguinte lista
    de operadores está agrupada em níveis de operadores de igual precedência
    Os níveis estão listados em ordem decrescente de precedência.
    
    	id++, id--	pós-incremento, pós-decrescimento de variável
    	++id, --id	pré-incremento, pré-decrescimento de variável
    	-, +		menos e mais unários
    	!, ~		negação lógica e bit a bit
    	**		exponenciação
    	*, /, %		multiplicação, divisão, resto
    	+, -		adição, subtração
    	<<, >>		deslocamento bit a bit para a esquerda e direita
    	<=, >=, <, >	comparação
    	==, !=		igualdadem desigualdade
    	&		E bit a bit
    	^		OU exclusivo bit a bit
    	|		OU bit a bit
    	&&		E lógico
    	||		OU lógico
    	expr ? expr : expr
    			operador condicional
    	=, *=, /=, %=,
    	+=, -=, <<=, >>=,
    	&=, ^=, |=	atribuição
    
    Variáveis do shell são permitidas como operandos. O nome da variável
    é substituída por seu valor (coagidas a um inteiro de tamanho fixo) dentro
    de uma expressão. A variável não precisa ter seu atributo de número inteiro
    ligada para ser usada em uma expressão.
    
    Operadores são avaliados em ordem de precedência. Subexpressões em
    Parênteses são avaliadas primeiro e podem substituir as regras de
    precedência acima.
    
    Estado de saída:
    Se o último ARG for calculado como zero, faz retornos serem igual a 1; caso contrário, retornos são 0. Avaliar expressão condicional
    
    Esse é um sinônimo para o "teste" embutido, mas o último argumento deve
    ser um literal `]', combinando com a abertura `['. Executa um comando simples ou mostra informação sobre comandos.
    
    Executa COMANDO com ARGS suprimindo busca de função shell, ou
    mostra informação sobre COMANDOS especificados. Pode ser usado para
    invocar comandos em disco quando existe uma função com o mesmo
    nome.
    
    Opções:
      -p	usa um valor padrão para PATH que garante encontrar
    	todos os utilitários padrões
      -v	imprime uma descrição de COMANDO similar ao nativo `type'
      -V	imprime uma descrição mais detalhada de cada COMANDO
    
    Estado de saída:
    Retorna o estado de saída de COMANDO ou falha se COMANDO não é
    encontrado. Executa argumentos como um comando shell.
    
    Combina ARGs em uma única string, usa o resultado como
    entrada para o shell e executa os comandos resultantes.
    
    Estado de saída:
    Retorna estado de saída do comando ou sucesso se o comando
    é nulo. Executa comandos até que o teste não seja concluído.
    
    Expande e executa COMMANDS até que o comando final de
    `until' tenha um status de saída igual a zero.
    
    Status de Saída:
    Retorna o status do último comando executado. Executa comandos até que o teste seja concluído.
    
    Expande e executa COMMANDS até que o comando final de
    `while' tenha um status de saída igual a zero.
    
    Status de Saída:
    Retorna o status do último comando executado. Execute comandos baseados em condicional.
    
        A lista de `COMANDOS if' é executada. Se o seu status de
        saída for zero, então a lista de `COMANDOS then' é executada.
        Caso contrário, cada lista de `COMANDOS elif' é executada, e
        se o status de saída for zero, a lista de `COMANDOS then'
        correspondente é executada e comando if encerrado. Caso
        contrário, a lista de `COMANDOS else' é executada, se
        presente. O status de saída da construção inteira é o status
        de saída do último comando exectuado, ou zero se nenhum teste
        condicional resultar verdadeiro.
        
        Status de saída:
        Retorna o status do último comando executado. Executar comandos baseado em correspondência de padrão.
    
    Executar seletivamente COMMANDS baseados sobre WORD correspondendo PATTERN.  A
    `|' é usada para separar múltiplos padrões.
    
    Estado de saída:
    Retorna o estado do último comando executado. Executa comandos para cada membro da lista;
    
    O laço `for' executa uma sequência de comandos para cada membro
    de uma lista de itens. Se `em WORDS ...;' não estiver presente, então `em "$@"' é
    assumido. Para cada elemento em WORDS, NAME é definido como aquele elemento, e
    os COMMANDS são executados.
    
    Status de saída:
    Retorna o status do último comando executado. Executa comandos de um arquivo no interpretador de comandos (shell) atual.
    
    Lê e executa comandos de um NOMEARQUIVO no shell atual. As entradas
    do $PATH são usadas para encontrar o diretório contendo NOMEARQUIVO.
    Se algum ARGUMENTO for fornecido, eles serão posicionados como 
    parâmetros quando o NOMEARQUIVO for executado.
    
    Estado de Saída:
    Retorna o estado do último comando executado no NOMEARQUIVO; falha se
    NOMEARQUIVO não puder ser lido. Executar o comando condicional
    
    Retorna um status de 0 ou 1, dependendo da avaliação da expressão
    condicional EXPRESSÃO. As expressões são compostas das mesmas
    primárias usadas pelo comando embutido `test', e podem ser
    combinadas usando os seguintes operadores:
    
      (EXPRESSÃO)	Retorna o valor da (EXPRESSÃO)
      !EXPRESSÃO		Verdadeiro se EXPRESSÃO for falsa; senão, falso
      EXPR1 && EXPR2	Verdadeiro se EXPR1 e EXPR2 forem ambas verdadeiras;
      		senão, falso
      EXPR1 || EXPR2	Verdadeiro se EXPR1 ou EXPR2 forem, qualquer uma
      		delas, verdadeiras; senão, falso
    
    Quando os operadores `==' e `!=' são usados, a string à direita
    do operador é usada como padrão, e a comparação de padrões é
    efetuada. Quando o operador `=~' é usado, a string à direita do
    operador é comparada como uma expressão normal.
        
    Os operadores && e || não avaliam a EXPR2 se a EXPR1 é suficiente
    para determinar o valor da expressão.
        
    Status de saída:
    0 ou 1, dependendo do valor de EXPRESSÃO. Executa comandos nativos do shell.
    
    Executa SHELL-BUILTIN com argumentos ARGs sem executar
    comando de busca. Isto é útil quando você deseja reimplementar
    um comando nativo do shell como uma função shell, mas necessita
    executar o comando nativo dentro desta função.
    
    Estado de saída:
    Retorna o estado de saída do comando nativo, ou `falso' se o
    comando não é nativo do shell. Fim da execução com status %d Sai de uma sessão shell.
    
    Sai de uma sessão shell com estado de saída N. Retorna um erro se
    não executado em um shell. Sai de laços `for', `while' ou `until'.
    
    Sai de um laço FOR, WHILE ou UNTIL. Se N é especificado,
    quebra N fechando o laço.
    
    Estado de saída:
    O estado de saída é 0 a não ser que N não seja maior ou igual a 1. Sai do shell.
    
    Sai do shell com estado de N. Se N é omitido, o estado de saída
    é aquele do último comando executado. Tamanho limite do arquivo excedido Exceção de ponto flutuante GNU bash, versão %s (%s)
 GNU bash, versão %s-(%s)
 opções-longas-GNU:
 Roda um conjunto de comandos em um grupo. Essa é um forma de redirecionar um
    conjunto inteiro de comandos.
    
    Status da Saída:
    Retorna o status do último comando executado. entrada de dados HFT pendente modo monitor HFT autorizado modo monitor HFT rescindido a seqüência de som HFT foi completada HOME não configurado Hangup Eu não tenho nome! Entrada/Saída pronta Instrução ilegal Requisição de informação Interromper Morto (Killed) Licença GPLv3+: GNU GPL versão 3 ou posterior <http://gnu.org/licenses/gpl.html>
 Modificar ou exibir opções de completamento.
    
    Modifica as opções de completamento para cada NOME ou, se nenhum
    NOME é fornecido, a do completamento atualmente em execução. Se
    nenhuma OPÇÃO é dada, imprime as opções de completamento para
    cada NOME ou a atual especificação de completamento.
    
    Opções:
    	-o opção	Atribui a opção de completamento OPÇÃO para cada NOME
    	-D		Muda as opções para o comando de completamento "padrão"
    	-E		Muda as opções para o comando de completamento "vazio"
    
    Usar `+o', em vez de `-o', desativa a opção especificada.
    
    Argumentos:
    
    Cada NOME se refere a um comando para o qual a especificação de
    completamento deve ter sido previamente definida usando o comando
    embutido `complete'. Se nenhum NOME é fornecido, compopt deve ser
    chamado por uma função que esteja gerando completamentos, e as
    opções para esse completamento em andamento serão modificadas.
    
    Status de saída:
    Retorna bem-sucedido a menos que uma opção inválida seja
    fornecida ou que NOME não tenha uma especificação de
    completamento definida. Mover trabalho para o primeiro plano.
    
    Põe um trabalho identificado por JOB_SPEC no primeiro plano, fazendo
    deste o trabalho atual. Se JOB_SPEC não está presente, o shell é usado
    como trabalho atual.
    
    Estado de saída:
    Estado do comando posto em primeiro plano,
    ou falha se um erro ocorrer. Mover tarefas para o plano de fundo.
    
    Ordene as tarefas identificadas por cada JOB_SPEC no plano de fundo, como se eles
    tivessem iniciados with `&'. Se JOB_SPEC não está presente, o parecer do shell
    da tarefa atual é usado.
    
    Status de Saída:
    Retorna sucesso a menos que o controle de tarefas não estiver ativado ou um erro ocorrer. Comando nulo.
    
    Sem efeito; o comando não faz nada.
    
    Estado de saída:
    Sempre é bem sucedido. OLDPWD não configurado Argumentos de opção de análise.
    
    Getopts é usado por procedimentos do shell para analisar parâmetros
    posicionais como opções.
    
    OPTSTRING contém as letras de opção a serem reconhecidas; se uma
    letra é seguida por um sinal de dois-pontos, espera-se que a opção
    tenha um argumento, que pode ser separado dele por um espaço em
    branco.
    
    Cada vez que é invocado, getopts colocará a próxima opção na
    variável de ambiente $name, inicializando nome caso não exista, e o
    índice do próximo argumento a se processar na variável de ambiente
    OPTIND. OPTIND é inicializada com 1 toda vez que o shell ou shell
    script é invocado. Quando a opção requer um argumento, getopts
    coloca este argumento na variável de ambiente OPTARG.
    
    getopts reporta erro de duas maneiras. Se o primeiro caractere de
    OPTSTRING é um sinal de dois-pontos (:), getopts usa relatório de
    erro silencioso. Neste modo, nenhuma mensagem de erro é impressa.
    Se uma opção inválida é vista, getopts põe o caractere de opção
    encontrado em OPTARG. Se um argumento requerido não é encontrado,
    getopts põe um ':' em NOME e habilita OPTARG para o caractere de
    opção encontrado. Se getopts não está em modo silencioso e uma
    opção válida é encontrada, getopts põe '?' em NOME, desabilita
    OPTARG e uma mensagem de diagnóstico é impressa.
    
    Se a variável de ambiente OPTERR tem o valor 0, getopts desabilita
    a impressão da mensagem de erro, da mesma forma se o primeiro
    caractere de OPTSTRING não é um sinal de dois-pontos. OPTERR tem o
    valor 1 por padrão.
    
    Getopts normalmente analisa os parâmetros posicionais ($0 - $9),
    mas se mais argumentos são dados, eles são analisados.
    
    Estado de saída:
    Retorna sucesso se uma opção é encontrada; falha se o fim das
    opções é encontrado ou um erro ocorre. Imprime o nome do diretório atual.
    
    Opções:
      -L	imprime o valor de $PWD se ele é o diretório de trabalho
    	atual
      -P	imprime o diretório físico, sem qualquer link simbólico
    
    Por padrão, `pwd' comporta-se como se `-L' estivesse especificado.
    
    Estado de saída:
    Retorna '0' a não ser que uma opção inválida é dada ou o diretório
    atual não pode ser lido. Sair Leia as linhas de um arquivo em uma variável de matriz.
    
    Um sinônimo para `mapfile'. Ler linhas da entrada padrão para uma variável de matriz indexada.
    
    Lê linhas da entrada padrão para a variável de matriz indexada
    MATRIZ, ou do descritor de arquivos FD se a opção -u é fornecida.
    A variável ARQUIVOMAPA (MAPFILE) é a matriz padrão.
    
    Opções:
      -n contagem	Copia, no máximo, o número de linhas CONTAGEM. Se
    	CONTAGEM for zero, todas as linhas são copiadas.
      -O origem	Começa a atribuir à MATRIZ no índice ORIGEM. O índice
    	padrão é 0.
      -s contagem	Descarta a leitura do número de linhas CONTAGEM.
      -t		Remove o código de nova-linha de cada linha lida.
      -u fd		Lê linhas do descritor de arquivos FD em vez de
    	ler da entrada padrão.
      -C retornochamada	Avalia RETORNOCHAMADA toda vez que uma
    	QUANTIDADE de linhas é lida.
      -c quantidade	Especifica o número de linhas lidas entre cada
    	RETORNOCHAMADA.
    
    Argumentos:
      MATRIZ		Nome da variável de matriz usada para guardar dados.
    
    Se -C é fornecida sem -c, a quantidade padrão é 5000. Quando
    RETORNOCHAMADA é avaliado, é fornecido o índice do próximo
    elemento da matriz a ser atribuído e a linha a ser atribuída
    àquele elemento como argumentos adicionais.
    
    Se não for fornecido com uma origem explícita, ARQUIVOMAPA irá
    apagar o conteúdo da MATRIZ antes que elementos lhe sejam
    atribuídos.
    
    Status de saída:
    Retorna bem-sucedido a menos que uma opção inválida seja dada,
    ou que MATRIZ seja somente-leitura ou não seja uma matriz
    indexada. Registro bloqueado (lock) Remover diretórios da pilha.
    
    Remove entradas da pilha de diretórios. Sem argumentos, remove
    o diretório do topo da pilha e muda para o novo topo da lista.
    
    Opções:
      -n	Suprime a mudança normal de diretório quando diretórios
    	são removidos, para que só a pilha seja manipulada.
    
    Argumentos:
      +N	Remove a enésima entrada contando da esquerna da lista
    	exibida por `dirs', começando por zero. Por exemplo:
    	`popd +0' remove o primeiro diretório, `popd +1' o segundo.
    
      -N	Remove a enésima entrada contando da direita da lista
    	exibida por `dirs', começando por zero. Por exemplo:
    	`popd -0' remove o último diretório, `popd -1' o penúltimo.
    
    O comando embutido `dirs' exibe a pilha de diretórios.
    
    Status de saída:
    Retorna bem-sucedido a menos que um argumento inválido seja
    fornecido ou que a mudança de diretório falhe. Remove cada NOME da lista de apelidos definidos.
    
    Opções:
      -a	remove todas as definições de apelido.
    
    Retorna sucesso a não ser um NOME não seja um
    apelido existente. Remove tarefas do shell atual.
    
    Remove cada argumento JOBSPEC da tabela de tarefas ativas. Sem
    Nenhum JOBSPEC, o shell usa sua noção da tarefa atual.
    
    Opções:
      -a	remove todas as tarefas se JOBSPEC não for fornecido
      -h	marca cada JOBSPEC para que SIGHUP não seja enviado à tarefa se o
    shell recebe um SIGHUP
      -r	remove somente tarefas em execução
    
    Status de saída:
    Retorna sucesso a menos que uma opção inválida ou JOBSPEC seja fornecida. Remove entradas da pilha de diretórios.  Se chamado sem argumentos, remove o
    diretório do topo da pilha, e muda para o novo diretório do topo.
    
    Opções:
      -n	Omite a mudança de pasta ao remover diretórios da pilha,
    	de modo que apenas a pilha seja manipulada.
    
    Argumentos:
      +N	Remove a N-ésima pasta a partir da esquerda da lista
    	mostrada por `dirs', começando do zero.  Por exemplo: `popd +0'
    	remove o primeiro diretório, `popd +1' o segundo.
    
      -N	Remove a N-ésima pasta a partir da direita da lista
    	mostrada por `dirs', começando do zero.  Por exemplo: `popd -0'
    	remove o último diretório, `popd -1' o penúltimo.
    
    O comando interno `dirs' exibe a pilha de diretórios. Substitui o shell pelo comando especificado.
    
    Executa COMANDO, substituindo o shell pelo programa especificado.
    ARGUMENTOS tornam-se os argumentos para COMANDO. Se COMANDO não é
    especificado, qualquer redirecionamento tem efeito no shell atual.
    
    Opções:
      -a nome	passa NOME como o argumento número zero para COMANDO
      -c		executa COMANDO com um ambiente vazio
      -l		põe um hífen no argumento número zero para COMANDO
    
    Se o comando não pode ser executado, um shell não-interativo sai,
    a não ser que a opção `execfail' esteja habilitada. 
    
    Estado de saída:
    Retorna sucesso a não ser que COMANDO não seja encontrado ou um
    redirecionamento de erro ocorra. Relatar o tempo consumido pela execução do pipeline.
    
    Execute PIPELINE e imprima um resumo do tempo real, tempo de CPU
    do usuário, e tempo de CPU do sistema gasto executando o PIPELINE
    quando ele terminar.
        
    Opções:
      -p	imprime o resumo da medição do tempo no formato Posix portátil
    
    O valor da variável FORMATODEHORA é usado como o formato de saída.
    
    Status de saída:
    O status retornado é o status do PIPELINE. Continua laços `for', `while' ou `until'.
    
    Continua na próxima iteração de um laço FOR, WHILE ou UNTIL.
    Se N é especificado, continua na N-ésima repetição do laço.
    
    Estado de saída:
    O estado de saída é 0 a não ser que N não seja maior ou igual a 1. Continuar o job em primeiro plano.
    
    Equivalente ao argumento JOB_SPEC do comando `fg'. Continua
    um job parado ou em plano de fundo. JOB_SPEC pode especificar
    ou um nome de job, ou um número de job. O JOB_SPEC seguido de um
    `&' coloca o job em plano de fundo, como se a especificação do
    job tivesse sido fornecida por um argumento de `bg'.
    
    Status de saída:
    Retorna o status do job que continuou. Retorna um resultado de sucesso.
    
    Estado de saída:
    Sempre é bem sucedido. Retorna um resultado de insucesso.
    
    Estado de saída:
    Sempre falha. Retorna uma função do shell.
    
    Faz com que uma função ou script-fonte finalize com o valor de retorno
    especificado por N. Se N for omitido, o status de retorno é o do último
    comando executado dentro da função ou script.
    
    Status de Saída:
    Retorna N, or falha se o shell não está executando uma função ou script. Retorna o contexto da chamada de subrotina atual.
    
    Sem EXPR, retorna "$line $filename". Com EXPR, retorna
    "$line $subroutine $filename"; esta informação extra pode ser usada
    para acompanhamento da pilha.
    
    O valor de EXPR indica quantos quadros de chamada vieram antes do
    atual; o quadro do topo é o quadro 0.
    
    Estado de saída:
    Retorna '0' a não ser que o shell não esteja executando uma função
    shell ou EXPR seja inválida. Devolve o contexto da chamada atual de subrotina.
    
    Sem EXPR, devolve  Executando Falha de segmentação Selecione palavras de uma lista e execute comandos.
    
    As PALAVRAS são expandidas, gerando uma lista de palavras. O
    conjunto de palavras expandidas é impresso no erro padrão,
    cada um precedido de um número. Se 'em PALAVRAS' não está
    presente, 'em "$@"' é presumido. O prompt PS3 é então exibido
    e uma linha da entrada padrão é lida. Se a linha consiste do
    número correspondente a uma das palavras exibidas, então a
    variável NOME recebe aquela palavra. Se a linha for vazia,
    PALAVRAS e o prompt são reexibidos. Se EOF é lido, o comando
    termina. Qualquer outro valor faz com que NOME seja definido
    como nulo. A linha lida é salva na variável RESPONDER. 
    COMANDOS são executados após cada seleção até que um comando
    break seja executado.
    
    Status de saída:
    Retorna o status do último comando executado. Envia um sinal a uma tarefa.
    
    Envia aos processos identificados por PID ou JOBSPEC o sinal nomeado por
    SIGSPEC ou SIGNUM. Se nem SIGSPEC ou SIGNUM estiver presente, então
    SIGTERM é assumido.
    
    Opções:
      -s sig	SIG é o nome do sinal
      -n sig	SIG é o número do sinal
      -l	lista os nomes de sinais; se os argumentos sucedem -l eles são
      	assumidos como números de sinais para os quais nomes devem ser listados
    
    Kill é comando interno do shell por 2 razões: Ele permite que IDs de tarefas sejam usados
    ao invés de IDs de processos, e permite que processos sejam terminados se o limite
    de processos que voce pode criar for atingido.
    
    Saída de status:
    Retorna sucesso a menos que uma opção inválida seja dada ou um erro ocorra. Definir ou redefinir opções do shell.
    Altera a configuração de cada opção do shell OPTNAME. Sem qualquer argumento,
    lista todas as opções do shell com uma indicação se está ou não definido.
    
    Opções:
      -o	restringe OPTNAME para aqueles definidos para usar com `set -o'
      -p	imprime cada opção do shell com uma indicação do seu status
      -q	supre a saída
      -s	ativa (define) cada OPTNAME
      -u	desativa (redefine) cada OPTNAME
      
    Status de Saída:
    Retorna sucesso se OPTNAME estive ativado; falha se uma opção inválida
    for inserida ou se OPTNAME estiver desativado. Conjunto de atributos de exportação para as variáveis ​​do shell.
    
    Marca cada NOME para exportação automática para o ambiente e em seguida
    executa os comandos.  Se VALOR for fornecido, atribui VALOR antes de exportar.
    
    Opções:
      -f	referem-se a funções shell
      -n	remove a propriedade de exportação de cada NOME
      -p	mostra uma lista de todas as funções e variáveis exportadas
    
    Um argumento `--' disabilita o processamento de outra função.
    
    Estados de Saída:
    Retorna sucesso a menos que uma opção ou NOME inválido seja fornecido. Define valores e atributos de variável.
    
    Obsoleto. Veja `help declare'. Comandos do shell que casem com a palavra-chave ` Comandos do shell que casem com as palavras-chave ` Opções da `shell':
 Deslocamento de parâmetros posicionais.
    
    Renomear os parâmetros posicionais $N+1,$N+2 ... para $1,$2 ...  Se N não for
    determinado, é assumido o valor 1.
    
    Estado de Saída:
    Retorna sucesso a menos que N seja negativo ou maior que $#. Sinal %d Especificar como os argumentos são completados por Readline.
    
        Para cada NOME, especifica como os argumentos dever ser
        completados. Se nenhuma opção é fornecida, as especificações
        de completamento existentes são impressas de tal maneira que
        permita a sua reutilização como entrada.
        
        Opções:
          -p	imprime as especificações de completamento num formato
        	reutilizável
          -r	remove uma especificação de completamento para cada NOME,
        	ou, se nenhum NOME é fornecido, todas as especificações de
        	completamento
          -D	aplica os completamentos e ações como padrão para os
        	comandos sem um completamento específico definido
          -E	aplica os completamentos e ações em comandos "vazios" --
        	tentativa de completamento numa linha vazia
        
        Quando um completamento é tentado, as ações são aplicadas na
        ordem em que as opções de letras maiúsculas estão listadas
        acima. A opção -D tem prioridade sobre -E.
        
        Status de saída:
        Retorna bem-sucedido a menos que uma opção inválida seja
        fornecida ou que um erro ocorra. Parado Parado (sinal) Parado (entrada tty) Parado (saída tty) Interrompido(%s) Supender execução do shell.
    
    Supender a execução deste shell até que ele recebe um sinal SIGCONT
    A menos que seja forçado, logins no shell não podem ser suspendidos.
    
    Opções:
      -f	forçar suspender, mesmo se o shell for um shell para login
    
    Status de Saída:
    Retorna sucesso a menos que o controle de trabalho esteja desativado ou ocorra algum erro. TIMEFORMAT: `%c': caractere de formato inválido Terminado As mensagens de correio em %s foram lidas
 Há trabalhos em execução.
 Existem trabalhos parados.
 Esses comandos do shell são definidos internamente. Digite `help' para ver essa lista.
Digite `help nome' para saber mais sobre a função `nome'.
 Use `info bash' para saber mais sobre shell em geral.
Use `man -k' ou `info' para saber mais sobre comandos que não estão nessa lista.


Um asterísco (*) próximo ao nome significa que o comando está desabilitado.

 Sinais de armadilha e outros eventos.
    
    Define e ativa manipuladores para serem executados quando o shell recebe sinais
    ou outras condições.
    
    ARG é um comando a ser lido e executado quando o shell recebe o(s)
    sinal(s) SIGNAL_SPEC.  Se ARG estiver ausente (e um único SIGNAL_SPEC
    for fornecido) ou `-', cada sinal especificado for restaurada para seu valor
    original.  Se ARG for a string nula, cada SIGNAL_SPEC é ignorado pelo
    shell e pelos comandos que ele invoca.
    
    Se um SIGNAL_SPEC for EXIT (0) ARG é executado na saída do shell.  Se
    um SIGNAL_SPEC for DEBUG, ARG é executado depois de cada comando simples.  Se
    um SIGNAL_SPEC for RETURN, ARG é executado a cada vez que uma função shell ou um
    script executado pelo . ou embutidos de origem terminam de executar.  Um SIGNAL_SPEC
    de ERR significa executar ARG a cada vez que uma falha do comando possa provocar que o
    shell saia quando a opção -e estiver habilitada.
    
    Se nenhum argumento for fornecido, armadilha imprime a lista de comandos associados
    com cada sinal.
    
    Opções:
      -l	imprime uma lista de nomes de sinal e seus números correspondentes
      -p	exibe os comandos de armadilha associados com cada SIGNAL_SPEC
    
    Cada SIGNAL_SPEC é um nome de sinal em <signal.h> ou um número de sinal.
    Nomes de sinal não diferenciam maiúsculas de minúsculas e o prefixo SIG é opcional.  Um
    sinal pode ser enviado ao shell com "kill -signal $$".
    
    Estado de saída:
    Retorna sucesso a não ser que SIGSPEC seja inválida ou uma opção inválida seja dada. Digite `%s -c "help set"' para mais informações sobre as opções da `shell'.
 Digite `%s -c help' para mais informações sobre os comandos internos do `shell'.
 Sinal desconhecido # Sinal desconhecido #%d Erro desconhecido Status desconhecido Condição urgente de Entrada/Saída Utilização:	%s [opção-longa-GNU] [opção] ...
	%s [opção-longa-GNU] [opção] arquivo-de-script ...
 Use "%s" para sair da `shell'.
 Use o comando `bashbug' para relatar erros.
 Sinal 1 definido pelo usuário Sinal 2 definido pelo usuário Janela mudada Escreve argumentos na saída padrão
    
    Mostra os ARGs na saída padrão seguido por
    uma nova linha.
    
    Opções:
      -n	não inclui uma nova linha
    
    Estado de saída:
    Retorna sucesso a não ser que um erro de escrita ocorra. Você tem mensagem de correio em $_ Você tem mensagem nova de correio em $_ [ ARG... ] [[ expressão ]] `%c': comando inválido `%c': caracter de formato inválido `%c': caracter de modo simbólico inválido `%c': operador de modo simbólico inválido '%c': especificação de formato inválido de tempo `%s': não é possível desvincular `%s': apelido (alias) inválido `%s': nome de mapa de teclas inválido `%s': caracter de formato faltando `%s': não é um pid ou especificação de trabalho válida `%s': não é um identificador válido `%s': nome de função desconhecida esperado `)' esperado `)', encontrado %s `:' esperado para expressão condicional add_process: pid %5ld (%s) marcado como ainda vivo add_process: processo %5ld (%s) em the_pipeline alias [-p] [NOME[=VALOR] ... ] all_local_variables: sem contexto de função no escopo atual argumento esperado argumento suporte a variáveis tipo vetor necessário tentativa de atribuição para algo que não é uma variável índice da matriz (array) incorreto tipo de comando inválido conector inválido salto inválido substituição incorreta: faltando "`" de fechamento em %s substituição inválida: sem `%s' de fechar em %s bash_execute_unix_command: mapa de teclas não encontrado para o comando bg [job_spec ...] break [n] bug: marcador de expassign inválido builtin [COMANDO-INTERNO-DA-SHELL [ARG ...]] caller [expr] pode apenas `retornar' de uma funçao ou script executado. pode ser usado apenas em uma função impossível alocar novo descritor de arquivo para entrada do bash do fd %d não foi possível criar arquivo temporário para o here-document: %s impossível duplicar fd %d para fd %d não foi possível duplicar encanamento nomeado %s como fd %d não é possível encontrar %s no objeto compartilhado %s: %s impossível fazer filho para substituição de comandos não foi possível gerar filho para substituição de processos impossível gerar encanamento para substituição de comandos impossível fazer encanamento para substituição de processos não foi possível abrir encanamento nomeado %s para leitura não foi possível abrir encanamento nomeado %s para escrita não é possível abrir objeto compartilhado %s: %s não foi possível redirecionar entrada padrão de /dev/null: %s não é possível redefinir o modo sem espera para o arquivo %d impossível configurar e desconfigurar opções do interpretador simultaneamente não foi possível definir o grupo do processo do terminal (%d) não pode simultaneamente desconfigurar uma função e uma variável impossível suspender impossível suspender uma sessão de login não é possível usar `-f' para fazer funções impossível usar mais que um de -anrw case PALAVRA in [PADRÃO [| PADRÃO]...) COMANDOS ;;]... esac processo filho setpgid (%ld a %ld) command [-pVv] COMANDO [ARG ...] command_substitute: impossível duplicar encanamento como fd 1 compgen [-abcdefgjksuv] [-o opção]  [-A ação] [-G caminho-global] [-W lista-de-palavras]  [-F função] [-C comando] [-X filtro-de-caminho] [-P prefixo] [-S suffixo] [palavra] complete [-abcdefgjksuv] [-pr] [-DE] [-o opção] [-A ação] [-G globpat] [-W lista-de-palavras]  [-F função] [-C comando] [-X filtro-de-caminho] [-P prefixo] [-S sufixo] [nome ...] completion: função `%s' não encontrada compopt [-o|+o opção] [-DE] [nome ...] operador condicional binário esperado continue [n] coproc [NOME] command [redirecionamentos] /tmp não encontrado, por favor crie! cprintf: `%c': caracter de formato inválido atual deletando trabalhos parados %d com grupo de processo %ld describe_pid: %ld: pid não existe pilha de diretórios vazia índice na pilha de diretórios dirs [-clpv] [+N] [-N] disown [-h] [-ar] [jobspec ...] divisão por 0 carregamento dinâmico não disponível echo [-n] [ARG ...] echo [-neE] [ARG ...] nome vazio para variável tipo vetor enable [-a] [-dnps] [-f nome-de-arquivo] [nome ...] erro ao obter atributos do terminal: %s erro ao importar a definição da função para `%s' erro ao definir atributos do terminal: %s eval [ARG ...] exec [-cl] [-a nome] [comando [argumentos ...]] [redirecionamento ...] exit [n] ')' esperado expoente menor que 0 export [-fn] [nome[=valor] ...] ou export -p esperado uma expressão excedido o nível de recursividade da expressão falso fc [-e enome] [-lnr] [primeiro] [último] ou fc -s [pat=rep] [comando] fg [JOB-ESPECIFICADO] descritor de arquivos fora da faixa argumento de nome de arquivo obrigatório for (( exp1; exp2; exp3 )); do COMANDOS; done for NOME [in PALAVRAS ... ] ; do COMANDOS; done forçado pid %d aparece no trabalho executado %d free: chamado com argumento de bloco já liberado free: chamado com argumento de bloco não alocado free: tamanhos dos pedaços inicial e final diferem free: esvaziamento detectado; mh_nbytes fora da faixa function nome { COMANDOS ; } ou nome () { COMANDOS ; } versões futuras do shell forçarão a avaliação como uma substituição aritmética getcwd: impossível acessar diretórios pai getopts OPÇÕES NOME [ARG] hash [-lr] [-p caminho] [-dt] [nome ...] hashing desabilitado help [-dms] [padrão ...] here-document na linha %d delimitado por final do arquivo (era pedido `%s') history [-c] [-d offset] [n] ou history -anrw [nome-de-arquivo] ou history -ps arg [arg...] posição do histórico especificação de histórico vezes	comando
 identificador esperado após pré-incremento ou pré-decremento. if COMANDOS; then COMANDOS; [ elif COMANDOS; then COMANDOS; ]... [ else COMANDOS; ] fi initialize_job_control: getpgrp falhou initialize_job_control: disciplina em linha initialize_job_control: setpgid base aritmética inválida base inválida caracter inválido %d em exportstr para %s número hexadecimal inválido número inválido número octal inválido número de sinal inválido trabalho %d iniciado sem controle de trabalhos job_spec [&] jobs [-lnprs] [JOB-ESPECIFICADO ...] ou jobs -x COMANDO [ARGS] kill [-s sigspec | -n signum | -sigspec] pid | jobspec ... ou kill -l [sigspec] último comando: %s
 let ARG [ARG ...] limite linha %d:  edição de linha desativada local [opção] nome[=valor] ... sair
 sair [n] contador de laço make_here_document: o tipo da instrução está incorreto %d make_local_variable: sem contexto de função no escopo atual make_redirection: instrução de redirecionamento `%d' fora da faixa malloc: bloqueio em lista livre criticada malloc: falhou garantia: %s
 mapfile [-n cont] [-O origem] [-s cont] [-t] [-u fd] [-C retorno] [-c quantum] [matriz] migrar o processo para outra CPU faltando `)' faltando `]' faltando dígito hexadecimal para \x digito unicode faltando para \%c operações de rede não suportadas sem `=' em exportstr para %s sem `%c' fechando %s nenhum comando encontrado nenhum tópico da ajuda casa com `%s'. Tente `help help' ou `man -k %s' ou `info %s'. sem controle de trabalho nenhum controle de trabalho nesta `shell' sem resultados: %s nenhum outro diretório nenhuma outra opção permitida com `-x' função de completação não está atualmente em execução não é uma sessão de login: use `exit' número octal tem sentido somente em um laço `for', `while' ou `until' erro de pipe pop_scope: cabeçalho de shell_variables não é um escopo de ambiente temporário pop_var_context: cabeçalho de shell_variables não é um contexto de função pop_var_context: não é contexto de variáveis globais popd [-n] [+N | -N] falha iminente de energia print_command: conector incorreto `%d' printf [-v var] formato [argumentos] progcomp_insert: %s: NULL COMPSPEC erro de programação pushd [-n] [+N | -N | dir] pwd [-LP] read [-ers] [-a matriz] [-d delim] [-i texto] [-n nchars] [-N nchars] [-p prompt] [-t timeout] [-u fd] [nome ...] erro de leitura: %d: %s readarray [-n cont] [-O origem] [-s cont] [-t] [-u fd] [-C retorno] [-c quantum] [matriz] readonly [-aAf] [name[=value] ...] or readonly -p realloc: chamado com argumento de bloco não alocado realloc: tamanhos dos pedaços inicial e final diferem realloc: subcarga detectada; mh_nbytes fora da faixa pilha de recursão vazia erro de redirecionamento: impossível duplicar fd register_alloc: %p já na tabela como alocado?
 register_alloc: tabela de alocação está cheia com FIND_ALLOC?
 register_free: %p já na tabela como liberado?
 restrito return [n] run_pending_traps: valor inválido na trap_list[%d]: %p run_pending_traps: gerenciador de sinal é SIG_DFL, reenviando %d (%s) para mim mesmo save_bash_input: buffer já existe para o novo fd %d select NOME [in PALAVRAS ... ;] do COMANDOS; done set [-abefhkmnptuvxBCHP] [-o option-name] [--] [arg ...] setlocale: %s: impossível alterar localização (%s) setlocale: %s: impossível alterar localização (%s): %s setlocale: LC_ALL: impossível alterar localização (%s) setlocale: LC_ALL: impossível alterar localização (%s): %s nível do interpretador (%d) muito alto, reconfigurando como 1 shift [n] contagem de deslocamento shopt [-pqsu] [-o] [nome-opção ...] sigprocmask: %d: operação inválida source arquivo [argumentos] start_pipeline: pipe pgrp suspend [-f] erro de sintaxe erro de sintaxe na expressão condicional erro de sintaxe na expressão condicional: `%s' inesperado erro de sintaxe na expressão erro de sintaxe próximo a `%s' erro de sintaxe próximo do `token' não esperado `%s' erro de sintaxe: `((%s))' erro de sintaxe: `;' não esperado erro de sintaxe: expressão aritmética requerida erro de sintaxe: operador aritmético inválido erro de sintaxe: operando esperado erro de sintaxe: fim prematuro do arquivo falha iminente do sistema test [EXPR] time [-p] pipeline times número excessivo de argumentos trap [-lp] [[arg] signal_spec ...] trap_handler: sinal inválido %d verdadeiro type [-afptP] nome [nome ...] typeset [-aAfFgilrtux] [-p] name[=value] ... umask [-p] [-S] [modo] unalias [-a] nome [nome ...] fim do arquivo inesperado enquanto porcurava por `]]' fim do arquivo inesperado enquanto procurava por `%c' fim do arquivo inesperado procurando por `)' argumento inesperado `%s' para operador condicional binário argumento inesperado `%s' para operador condicional unário argumento inesperado para operador condicional binário argumento inespedrado para operador condicional unário símbolo inesperado %d em comando condicional símbolo inesperado `%c' em comando condicional símbolo inesperado `%s' em comandoc condicional símbolo inesperado `%s', operador condicional binário esperado símbolo inesperado `%s', esperava `)' desconhecido erro de comando desconhecido until COMANDOS; do COMANDOS; done valor muito grande para esta base de numeração variables - Nomes e significados de algumas variáveis do shell wait: pid %ld não é um filho deste interpretador wait_for: Não há registro do processo %ld wait_for_job: trabalho %d está parado waitchld: ativando WNOHANG para evitar bloco indefinido aviso:  aviso: %s: %s aviso: A opção -C pode não funcionar como você espera aviso: A opção -F pode não funcionar como você espera while COMANDOS; do COMANDOS; done erro de escrita: %s xtrace fd (%d) != fileno xtrace fp (%d) xtrace_set: %d: o descritor de arquivo é inválido xtrace_set: o ponteiro de arquivo é NULL { COMANDOS ; } 