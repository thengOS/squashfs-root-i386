��    -      �  =   �      �     �  "   �  @        _     v     |  
   �  4   �  .   �  9   �  +   -     Y     `     v     �     �     �  	   �     �     �     �            @   '     h     t  �   y          $     )     1  	   6     @     H     \     t     �  &   �     �     �     �  $     /   ,     \  �  e     
  +   8
  D   d
     �
     �
     �
     �
  ;   �
  9     F   Y  7   �     �     �     �       &   3     Z  	   c     m     �     �     �     �  8   �            �   "     �     �                    #     *     B     ^     p  -   �  	   �  !   �  "   �  )   
  8   4     m           
                                                     %                     )          *   "       !          -      ,      &                $         	   '      (       #             +                       Byobu Configuration Menu  file exists, but is not a symlink <Tab>/<Alt-Tab> between elements | <Enter> selects | <Esc> exits Add to default windows Apply Archive Byobu Help Byobu currently does not launch at login (toggle on) Byobu currently launches at login (toggle off) Byobu will be launched automatically next time you login. Byobu will not be used next time you login. Cancel Change Byobu's colors Change escape sequence Change escape sequence: Change keybinding set Choose Command:  Create new window(s): Create new windows ERROR: Invalid selection Error: Escape key: ctrl- Extract the archive in your home directory on the target system. File exists Help If you are using the default set of keybindings, press\n<F5> to activate these changes.\n\nOtherwise, exit this screen session and start a new one. Manage default windows Menu Message Okay Presets:  Profile Remove file? [y/N]  Run "byobu" to activate Select a color:  Select a screen profile:  Select window(s) to create by default: Title:  Toggle status notifications Toggle status notifications: Which profile would you like to use? Which set of keybindings would you like to use? Windows: Project-Id-Version: byobu
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2009-11-23 20:48-0600
PO-Revision-Date: 2011-12-14 12:35+0000
Last-Translator: Tiago Hillebrandt <tiagohillebrandt@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:40+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
  Configuração de menus Byobu  arquivo existe, mas não o link simbólico <Tab>/<Alt-Tab> entre elementos | <Enter> seleciona | <Esc> finaliza Adicionar as janelas padrões Aplicar Arquivo Ajuda Byobu Byobu atualmente não é executado ao logar (tornar ligado) Byobu atualmente é executado ao logar (tornar desligado) Byobu será executado automaticamente na próxima vez que você logar. Byobu não será usado na próxima vez que você logar. Cancelar Alterar cores do Byobu Alterar sequência de escape Alterar sequência de escape: Alterar o conjunto de teclas de atalho Escolher Comando:  Criar nova janela(s): Criar novas janelas Erro: Seleção inválida Erro: Tecla de escape: crtl- Extrair o arquivo na pasta pessoal como alvo do sistema. O arquivo já existe Ajuda Caso você esteja utilizando o conjunto padrão de atalhos de teclado, pressione \n<F5> para ativar estas alterações.\n\n Ou então, saia desta seção de usuário e inicie uma nova sessão. Gerenciar janelas padrões Menu Mensagem Ok Pré-definições:  Perfil Remover arquivo? [s/N]  Execute "byobu" para ativar Selecione a cor:  Selecione a tele de perfil:  Selecione a janela(s) para criar por padrão: Título:  Alternar notificações de estado Alternar notificações de estado: Qual o perfil que você gostaria de usar? Qual conjunto de teclas de atalho você deseja utilizar? Janelas: 