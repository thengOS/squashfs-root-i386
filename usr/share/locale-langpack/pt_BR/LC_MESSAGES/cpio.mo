��   *  0   �	  �  �                   T  �   U           5     <  .   I  .   x  %   �     �     �  0   �     )  "   C     f     |  0   �  !   �     �     �  /   
     :     Z     u     �  d   �     	     !     8  v   W  >   �  +     -   9     g     ~  ,   �     �  %   �  ,     -   8      f  (   �     �     �     �     �                 4      J      j      �      �   "   �   4   �   I   �   /   2!  /   b!     �!  .   �!     �!  J   �!  3   F"  E   z"     �"  7   �"  �   #  �   �#  G   $$     l$  "   �$  
   �$  '   �$  (   �$     %     #%  $   +%     P%     l%     {%     �%     �%     �%     �%      �%  i   &  2   �&     �&  '   �&  )   '  $   ,'     Q'  `   X'  7   �'  (   �'  (   (  >   C(     �(     �(  T   �(     )  3   )      P)     q)     v)     {)  \   �)  '   �)     *     *  E   7*  5   }*  C   �*  +  �*     #,     3,  ?   P,  B   �,  B   �,     -     1-     D-     [-     r-     �-  &   �-  2   �-     �-     .  s   .  A   �.     �.     �.     �.     �.     /     /     :/     A/  ;   H/  &   �/  8   �/  9   �/  :   0  /   Y0  0   �0  1   �0     �0     1     1     1  @   11     r1  D   �1  #   �1  &   �1  F   2  J   W2  6   �2     �2  ,   �2  '    3  !   H3     j3     �3  2   �3  ;   �3     4     4     4     $4     +4     G4  ,   `4      �4  $   �4  0   �4  3   5  I   85     �5  +   �5  &   �5  R   �5  ,   H6  6   u6  q   �6     7  /   =7     m7  Z   �7  6   �7     8     '8     ;8     N8     c8  0   o8     �8     �8  !   �8  +   �8  �   9     �9  "   �9  v   �9  6   H:  .   :     �:  ;   �:  3   ;  /   5;  +   e;  '   �;  #   �;     �;     �;     <  ]   )<  	   �<     �<     �<     �<     �<  "   �<     �<     =  B   (=  +   k=     �=     �=     �=     �=     �=     >     >     (>     >>  ,   V>  4   �>     �>  %   �>     �>     ?      ?     4?     B?     X?  -   x?  	   �?  !   �?  	   �?  I   �?     &@     *@     E@  $   Y@     ~@     �@  5   �@  e   �@     BA     ]A     pA     �A     �A     �A     �A     �A  	   �A     �A     B  	   B     B     0B     IB     _B     uB  
   zB  
   �B  
   �B     �B  %   �B     �B     �B     �B     C     (C     BC     RC     XC     _C     rC  *   vC  ;   �C  !   �C     �C  .   D  D   GD     �D  �  �D  �   �F     fG     ~G     �G  2   �G  .   �G  0   �G     &H     CH  6   ]H     �H  %   �H     �H     �H  F   I  0   HI     yI  "   �I  2   �I  -   �I  "   J     9J  -   XJ  ^   �J  $   �J  #   
K  +   .K  l   ZK  I   �K  2   L  6   DL     {L  !   �L  -   �L     �L  )   	M  -   3M  .   aM      �M  ,   �M     �M     �M     N  #   N     =N     NN      mN  $   �N  $   �N     �N     �N  .   �N  ;   O  S   HO  3   �O  3   �O  $   P  3   )P     ]P  T   |P  8   �P  o   
Q  &   zQ  S   �Q  �   �Q  �   �R  L   1S  )   ~S  ,   �S     �S  #   �S  $   T      ,T     MT  4   UT  (   �T     �T     �T     �T  !   U     *U     EU     XU  h   wU  7   �U  *   V  2   CV  )   vV  &   �V     �V  e   �V  ;   9W  &   uW  )   �W  K   �W     X  (   2X  P   [X     �X  +   �X  &   �X     Y     Y     Y  n   &Y  *   �Y     �Y  &   �Y  N   Z  >   SZ  Q   �Z  n  �Z     S\     g\  D   �\  \   �\  L   #]  !   p]     �]     �]     �]     �]     ^  (   $^  ;   M^     �^     �^  �   �^  C   S_     �_     �_     �_     �_     �_  $   �_     	`     `  L   `  6   k`  L   �`  N   �`  K   >a  ;   �a  ;   �a  A   b     Db     ^b     fb     yb  E   �b     �b  I   �b  +   0c  (   \c  `   �c  k   �c  A   Rd  "   �d  +   �d  ,   �d  /   e     @e     [e  9   re  E   �e     �e     f     f     f  #    f  &   Df  5   kf  .   �f  5   �f  ?   g  <   Fg  [   �g  +   �g  /   h  ,   ;h  e   hh  2   �h  @   i  �   Bi      �i  L   �i     4j  w   Fj  >   �j     �j     k     1k     Ik     fk  5   zk     �k     �k  )   �k  0   l  �   9l     �l  -   �l  |   (m  B   �m  ;   �m     $n  9   :n  1   tn  -   �n  )   �n  %   �n  !   $o     Fo     do     ~o  n   �o     �o     p  #   $p     Hp     ]p  %   _p     �p     �p  R   �p  :   q  "   Kq  "   nq     �q     �q     �q     �q     �q  )   r  "   5r  8   Xr  ?   �r     �r  4   �r  !   !s     Cs     `s     }s     �s  #   �s  4   �s     t  (   t     <t  X   Ot     �t  &   �t     �t  (   �t  "   u     >u  <   Ou  �   �u  $   v     4v     Ov     kv  )   {v     �v     �v     �v     �v  %   �v     w  
   +w     6w     Lw     gw     �w     �w  
   �w  
   �w     �w     �w  ,   �w     x  0   #x     Tx     ox     �x     �x     �x     �x     �x     �x  3   �x  I   'y  &   qy     �y  <   �y  C   �y     3z     �   �            �       �   �       �   �       	               [   �       #   U   �      �      �   �       ,   �   �   �   a   .          3   &   ^   
  !  :   �       W   �   �   �   �   �   �       �   C   "        �   �   �   $       �       z   r   �          �   c   #  `   �   �               �       �   |   0   �     �         �           @   <      �       �   �       �   �      �          �         !   9   �   �     v   7   J   h       u       4   �       K           i   P   �   m               �   k   �       '  �   �     f   �       �   �       �   �   �           �          �   \   O   I      �   �   )     �   V   ;   M   D          G             s   �   �          �       �      g   8   �   6                       �                   B   
   �                  	  �     %   �                           �     �         �       �       �             2   �   *   �   x   �   �   �   �   1   �   H   �   �      E       "       (   S       L   �   �   j       �       �      �   Q            _   ~   �   F       �           q       �       �   �   p   �   �   &  Z   -            �       ]     �          {   �   ?   �   (      �   �       �   �   n   o                   �   �   )      N     T   }     �      %  l           �   �       w              �   >   �   �       �   �   +   �          5     �       *  Y   �       e   /                     $   �   y   '     �       �       �   t   �   R        �   �      d   X      =       �   �           �                  b   A      Cz  $  8  Kz         C   �����z         -          ���� 
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

 
Report bugs to: %s
   or:   [OPTION...] %.*s: ARGP_HELP_FMT parameter must be positive %.*s: ARGP_HELP_FMT parameter requires a value %.*s: Unknown ARGP_HELP_FMT parameter %lu block
 %lu blocks
 %s home page: <%s>
 %s home page: <http://www.gnu.org/software/%s/>
 %s is meaningless with %s %s is not a character special file %s is not a directory %s linked to %s %s not created: newer or same age version exists %s not dumped: not a regular file %s: Cannot %s %s: Cannot change mode to %s %s: Cannot change ownership to uid %lu, gid %lu %s: Cannot create symlink to %s %s: Cannot hard link to %s %s: Cannot seek to %s %s: Cannot symlink to %s %s: Read error at byte %s, while reading %lu byte %s: Read error at byte %s, while reading %lu bytes %s: Too many arguments
 %s: Warning: Cannot %s %s: Warning: Cannot seek to %s %s: Warning: Read error at byte %s, while reading %lu byte %s: Warning: Read error at byte %s, while reading %lu bytes %s: Wrote only %lu of %lu byte %s: Wrote only %lu of %lu bytes %s: checksum error (0x%lx, should be 0x%lx) %s: field width not sufficient for storing %s %s: file name too long %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous
 %s: option '%s' requires an argument
 %s: option '--%s' doesn't allow an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option requires an argument -- '%c'
 %s: rmtclose failed %s: rmtioctl failed %s: rmtopen failed %s: symbolic link too long %s: truncating %s %s: truncating inode number %s: unknown file type %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 ' (C) (PROGRAM ERROR) No version known!? (PROGRAM ERROR) Option should have been recognized!? --append is used but no archive file name is given (use -F or -O options) --no-preserve-owner cannot be used with --owner --owner cannot be used with --no-preserve-owner --stat requires file names -F can be used only with --create or --extract -T reads null-terminated names A list of filenames is terminated by a null character instead of a newline ARGP_HELP_FMT: %s value is less than or equal to %s Append SIZE bytes to FILE. SIZE is given by previous --length option. Append to an existing archive. Archive file is local, even if its name contains colons Archive filename to use instead of standard input. Optional USER and HOST specify the user and host names in case of a remote archive Archive filename to use instead of standard output. Optional USER and HOST specify the user and host names in case of a remote archive Archive format is not specified in copy-pass mode (use --format option) Archive format multiply defined Archive value %.*s is out of range BLOCK-SIZE Both -I and -F are used in copy-in mode Both -O and -F are used in copy-out mode Byte count out of range COMMAND Cannot connect to %s: resolve failed Cannot execute remote shell Cannot open %s Command dumped core
 Command exited successfully
 Command failed with status %d
 Command stopped on signal %d
 Command terminated
 Command terminated on signal %d
 Control warning display. Currently FLAG is one of 'none', 'truncate', 'all'. Multiple options accumulate. Create all files relative to the current directory Create file of the given SIZE Create leading directories where needed Create the archive (run in copy-out mode) Creating intermediate directory `%s' DEVICE Dereference  symbolic  links  (copy  the files that they point to instead of copying the links). Display executed checkpoints and exit status of COMMAND Do not change the ownership of the files Do not print the number of blocks copied Do not strip file system prefix components from the file names Enable debugging info Error parsing number near `%s' Execute ARGS. Useful with --checkpoint and one of --cut, --append, --touch, --unlink Execute COMMAND Extract files from an archive (run in copy-in mode) Extract files to standard output FILE FLAG FORMAT File %s shrunk by %s byte, padding with zeros File %s shrunk by %s bytes, padding with zeros File %s was modified while being copied File creation options: File statistics options: Fill the file with the given PATTERN. PATTERN is 'default' or 'zeros' Found end of tape.  Load next tape and press RETURN.  Found end of tape.  To continue, type device/file name when ready.
 GNU `cpio' copies files to and from archives

Examples:
  # Copy files named in name-list to the archive
  cpio -o < name-list [> archive]
  # Extract files from the archive
  cpio -i [< archive]
  # Copy files named in name-list to destination-directory
  cpio -p destination-directory < name-list
 Garbage command Garbage in ARGP_HELP_FMT: %s General help using GNU software: <http://www.gnu.org/gethelp/>
 Generate sparse file. Rest of the command line gives the file map. In the verbose table of contents listing, show numeric UID and GID Interactively rename files Invalid byte count Invalid operation code Invalid seek direction Invalid seek offset Invalid size: %s Invalid value for --warning option: %s Link files instead of copying them, when  possible Main operation mode: Malformed number %.*s Mandatory or optional arguments to long options are also mandatory or optional for any corresponding short options. Manipulate a tape drive, accepting commands from a remote process Mode already defined NAME NUMBER Negative size: %s Not enough arguments Number out of allowed range: %s OFFSET OPTION Only copy files that do not match any of the given patterns Operation modifiers valid in any mode: Operation modifiers valid in copy-in and copy-out modes: Operation modifiers valid in copy-in and copy-pass modes: Operation modifiers valid in copy-out and copy-pass modes: Operation modifiers valid only in copy-in mode: Operation modifiers valid only in copy-out mode: Operation modifiers valid only in copy-pass mode: Operation not supported PATTERN Packaged by %s
 Packaged by %s (%s)
 Perform given action (see below) upon reaching checkpoint NUMBER Premature eof Print STRING when the end of a volume of the backup media is reached Print a "." for each file processed Print a table of contents of the input Print contents of struct stat for each given file. Default FORMAT is:  Read additional patterns specifying filenames to extract or list from FILE Read error at byte %lld in file %s, padding with zeros Read file names from FILE Removing leading `%s' from hard link targets Removing leading `%s' from member names Replace all files unconditionally Report %s bugs to: %s
 Report bugs to %s.
 Reset the access times of files after reading them Retain previous file modification times when creating files Run in copy-pass mode SECS SIZE STRING Seek direction out of range Seek offset out of range Seek to the given offset before writing data Set date for next --touch option Set the I/O block size to 5120 bytes Set the I/O block size to BLOCK-SIZE * 512 bytes Set the I/O block size to the given NUMBER of bytes Set the ownership of all files created to the specified USER and/or GROUP Size of a block for sparse file Substituting `.' for empty hard link target Substituting `.' for empty member name Swap both halfwords of words and bytes of halfwords in the data. Equivalent to -sS Swap the bytes of each halfword in the files Swap the halfwords of each word (4 bytes) in the files Synchronous execution actions. These are executed when checkpoint number given by --checkpoint option is reached. Synchronous execution options: To continue, type device/file name when ready.
 Too many arguments Truncate FILE to the size specified by previous --length option (or 0, if it is not given) Try `%s --help' or `%s --usage' for more information.
 Unexpected arguments Unknown date format Unknown field `%s' Unknown system error Unlink FILE Update the access and modification times of FILE Usage: Use given archive FORMAT Use remote COMMAND instead of rsh Use the old portable (ASCII) archive format Use this FILE-NAME instead of standard input or output. Optional USER and HOST specify the user and host names in case of a remote archive Valid arguments are: Verbosely list the files processed When reading a CRC format archive, only verify the CRC's of each file in the archive, don't actually extract the files Write files with large blocks of zeros as sparse files Write to file NAME, instead of standard output Written by %s and %s.
 Written by %s, %s, %s,
%s, %s, %s, %s,
%s, %s, and others.
 Written by %s, %s, %s,
%s, %s, %s, %s,
%s, and %s.
 Written by %s, %s, %s,
%s, %s, %s, %s,
and %s.
 Written by %s, %s, %s,
%s, %s, %s, and %s.
 Written by %s, %s, %s,
%s, %s, and %s.
 Written by %s, %s, %s,
%s, and %s.
 Written by %s, %s, %s,
and %s.
 Written by %s, %s, and %s.
 Written by %s.
 You must specify one of -oipt options.
Try `%s --help' or `%s --usage' for more information.
 [ARGS...] [USER][:.][GROUP] [[USER@]HOST:]FILE-NAME [destination-directory] ` `%s' exists but is not a directory ambiguous argument %s for %s blank line ignored cannot generate sparse files on standard output, use --file option cannot get the login group of a numeric UID cannot link %s to %s cannot make directory `%s' cannot open %s cannot open `%s' cannot read checksum for %s cannot remove current %s cannot seek cannot seek on output cannot set time on `%s' cannot swap bytes of %s: odd number of bytes cannot swap halfwords of %s: odd number of halfwords cannot unlink `%s' control magnetic tape drive operation created file is not sparse device major number device minor number device number error closing archive exec/tcp: Service not available failed to return to initial working directory file mode file name contains null character file size genfile manipulates data files for GNU paxutils test suite.
OPTIONS are:
 gid give a short usage message give this help list hang for SECS seconds (default 3600) incorrect mask (near `%s') inode number internal error: tape descriptor changed from %d to %d invalid archive format `%s'; valid formats are:
crc newc odc bin ustar tar (all-caps also recognized) invalid argument %s for %s invalid block size invalid count value invalid group invalid header: checksum error invalid user memory exhausted modification time name size no tape device specified number of links operation operation [count] premature end of archive premature end of file print program version rdev rdev major rdev minor read error rename %s ->  requested file length %lu, actual %lu set debug level set debug output file name set the program name standard input is closed standard output is closed stat(%s) failed stdin stdout too many arguments uid unable to record current working directory use device as the file name of the tape drive to operate on use remote COMMAND instead of rsh virtual memory exhausted warning: archive header has reverse byte-order warning: skipped %ld byte of junk warning: skipped %ld bytes of junk write error Project-Id-Version: cpio 2.3.911
Report-Msgid-Bugs-To: bug-cpio@gnu.org
POT-Creation-Date: 2010-03-10 15:04+0200
PO-Revision-Date: 2016-02-19 02:05+0000
Last-Translator: Fábio Henrique F. Silva <Unknown>
Language-Team: Brazilian Portuguese <ldp-br@bazar.conectiva.com.br>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 16:09+0000
X-Generator: Launchpad (build 18115)
 
Licença GPLv3+: GNU GPL versão 3 ou posterior <http://gnu.org/licenses/gpl.html>.
Este é um software livre: você é livre para modificá-lo ou redistribuí-lo.
Não há NENHUMA GARANTIA, até os limites permitidos por lei.

 
Relatar erro para: %s
   ou:   [OPÇÃO...] %.*s: ARGP_HELP_FMT o parâmetro deve ser positivo %.*s: ARGP_HELP_FMT parâmetro requer um valor %.*s: o parâmetro ARGP_HELP_FMT é desconhecido %lu bloqueio
 %lu bloqueios
 %s página inicial: <%s>
 %s página inicial: <http://www.gnu.org/software/%s/>
 %s não tem sentido com %s O %s não é um arquivo tipo caracter '%s não é um diretório %s vinculado a %s %s não criado: ele é mais novo ou da mesma data da versão existente %s não descarregado: não é um arquivo regular %s: Não é possível %s %s: Impossível mudar modo para %s %s: Impossível alterar dono para uid %lu, gid %lu %s: Impossível criar link simbólico para %s %s: Impossível fazer link para %s %s: Impossível saltar para %s %s: Impossível fazer link simbólico para %s %s: Erro de leitura no byte %s, lendo %lu byte %s: Erro de leitura no byte %s, lendo %lu bytes %s: Número excessivo de argumentos
 %s: Atenção: Não é possível %s %s: Aviso: Não é possível saltar para %s %s: Aviso: Erro de leitura no byte %s, lendo %lu byte %s: Aviso: Erro de leitura no byte %s, lendo %lu bytes %s: Gravados apenas %lu de %lu bytes %s: Gravados apenas %lu de %lu bytes %s: erro de validação (0x%lx, deveria ser 0x%lx) %s: tamanho do campo é insuficiente para armazenar %s %s: nome do arquivo muito longo %s: opção -- '%c' é inválida
 %s: opção '%c%s' não permite um argumento
 %s: opção '%s' é ambígua
 %s: opção '%s' precisa de um argumento
 %s: opção '--%s' não permite um argumento
 %s: opção '-W %s' não permite um argumento
 %s: opção '-W %s' é ambígua
 %s: opção necessita de argumentos -- '%c'
 %s: rmtclose falhou %s: rmtioctl falhou %s: rmtopen falhou %s: vínculo simbólico muito longo %s: truncando %s %s: truncando número do inode %s: tipo de arquivo desconhecido %s: opção não reconhecida '%c%s'
 %s: opção não reconhecida '--%s'
 ' © (ERRO NO PROGRAMA) Nenhuma versão conhecida!? (ERRO NO PROGRAMA) a opção deveria ter sido reconhecida!? --acréscimo é usado mas nenhum nome de arquivo é dado (use as opções -F ou -O) --no-preserve-owner não pode ser usado com --owner --owner não pode ser usado com --no-preserve-owner --stat nomes dos arquivos requeridos -F pode ser usado somente com --create ou --extract -T lê nomes sem terminações A lista do arquivo de nomes termina com um caracter nulo instanciado numa nova linha ARGP_HELP_FMT: valor é menor que %s ou é igual para %s Anexar o TAMANHO de bytes para ARQUIVO. Este TAMANHO é determinado pela visualização com a opção --length. Anexar ao fim de um arquivo existente. Arquivamento de arquivos é local, até mesmo quando o seu nome contém dois pontos Arquive o nome do arquivo para usar ao invés da entrada padrão. Opcionais, USUÁRIO e HOST especificam os nomes do usuário e host, no caso de um arquivamento remoto Nome do pacote para usar ao invés da saída padrão. Opcionalmente USER e HOST especificam o usuário e nomes de host, no caso de um pacote remoto Formato de arquivo não especificado no modo copy-pass (use --format option) Formato de arquivo multiplamente definido Valor do arquivo %.*s está fora dos limites TAMANHO-BLOCO -I e -F são usados no modo copy-in -O e -F são usados no modo copy-out Contagem de bytes fora do limite COMANDO Não é possível conectar comm %s falha ao resolver Não foi possível executar shell remoto Não é possível abrir %s Comando fez dump do core
 Saída do comando bem sucedida
 O comando falhou com o status %d
 Comando parou no sinal %d
 Comando terminado
 Comando terminado no sinal %d
 Visor de controle de advertência. FLAG é um dos 'none', ' truncate', ' all'. Acumula várias opções. Cria todos os arquivos relativos ao diretório corrente Criar um arquivo de um determinado TAMANHO Criar os diretórios principais quando necessário Crie o arquivo (execute no modo copy-out) Criando diretório intermediário `%s' DISPOSITIVO Desreferencie os links simbólicos (copie os arquivos que eles apontam ao invés de copiar os links). Exibir checkpoints executados e status de saída de COMMAND Não altere a propriedade dos arquivos Não imprime o número de blocos copiados Não tire os componentes do sistema de arquivo prefixo dos nomes de arquivo Habilitar informação do debug Erro ao analizar o próximo número `%s' Executa ARGS. Útil com --checkpoint e com um --cut, --append, --touch, --unlink Execute o COMANDO Extrai os arquivos (execute o modo copy-in) Extrair arquivos para a saída padrão ARQUIVO FLAG FORMATO Arquivo %s encolhido por %s byte, preenchido com zeros Arquivo %s encolhido por %s bytes, preenchido com zeros O arquivo %s foi modificado ao ser copiado Criando opção de arquivos: Estátisticas das opções de arquivo: Preenche o arquivo com um determinado PADRÃO. PADRÃO é 'padrão' ou 'zeros' Fim de fita encontrado. Coloque a próxima fita e tecle ENTER  Fim da fita. Para continuar, digite o nome do dispositivo/arquivo quando pronto.
 GNU `cpio' copia arquivos para e de arquivos

Exemplos:
  # Copia os arquivos com o nome em uma lista-de-nomes, para um único arquivo
   cpio -o < lista-de-nomes [ > arquivo ]
  # Extrai arquivos de um arquivo
  cpio -i [ < arquivo ]
  # Copia os arquivos contidos na lista-de-arquivos pata o diretorio-de-destino
  cpio -p diretorio-de-destino < lista-de-arquivos
 Comando sem sentido Lixo em ARGP_HELP_FMT: %s Ajuda geral para uso de software GNU: <http://www.gnu.org/gethelp/>
 Gerar espaços no arquivos. Resto de uma determinada da linha de comando no mapa do arquivo. Na tabela detalhada dos conteúdos listados, mostra o UID e o GID numéricos Renomear arquivos interativamente Contagem de bytes inválida Código de operação inválido Direção de pesquisa inválida Intervalo de pesquisa inválido Tamanho inválido: %s Valor inválido da opção --warning: %s Crie atalhos ao invés de copiar arquivos, quando possível Modo principal de operação: Número mal formado %.*s Argumentos obrigatórios ou opcionais para opções longas são também obrigatórios ou opcionais para qualquer opção abreviada correspondente. Controla um drive de fita, aceitando comandos de um processo remoto Modo já definido NOME NÚMERO Tamanho negativo: %s Sem argumento suficiente Número fora do limite permitido: %s DESLOCAMENTO OPÇÃO Copia somente arquivos que não combinam em qualquer dos padrões fornecidos Modificadores de operação válidos em qualquer modo: Modificadores de operação válidos nos modo copiar-entrada e copiar-saída Modificadores de operação válidos nos modos copiar-entrada e copiar-passar: Modificadores de operação válidos nos modo copiar-saída e copiar-passar Modificadors de operação válidos somente no modo copy-in Modificadores de operação validos somente em modo copy-in Modificadores de operação válidos apenas no modo copiar-passar Operação não suportada PADRÃO Empacotado por %s
 Empacotado por %s (%s)
 Executar a ação dada (veja abaixo) ao encontrar o checkpoint NUMBER Eof inesperado Imprime STRING quando o final do volume ou mídia de backup é alcançada Imprime um "." para cada arquivo processado Imprime a tabela do conteúdo de entrada Imprimir os conteúdos da estrutura de estatísticas para cada arquivo dado. FORMAT padrão é:  Leia os padrões adicionais com nomes de arquivos especificados para extrair ou listar a partir de ARQUIVO. Erro de leitura no byte %lld do arquivo %s, preenchendo com zeros Ler nomes de arquivos para ARQUIVO Removendo `%s' inicial dos hard links alvos Removendo `%s' inicial dos nomes dos membros Substituir todos os arquivos incondicionalmente Relatar %s erros para: %s
 Relatar erro para %s.
 Reinicie o acesso de vezes dos arquivos depois de lê-los Guarde as modificações prévias do arquivo quando criar os arquivos Execute no modo copy-pass SECS TAMANHO STRING Procurar direção fora dos limites Procurar deslocamento fora dos limites Buscar pelo deslocamento dado antes de escrever dados Ajustar a data para a próxima opção --touch Configurado o tamanho do bloco de E/S para 5120 bytes Seta o tamanho dos blocos de E/S para TAMANHO-BLOCO * 512 bytes Configure o tamanho do bloco de E/S dando o NÚMERO de bytes Defina a propriedade de todos os arquivos criados para os especificados USUÁRIO e/ou GRUPO Tamanho de um bloco para espaçar o arquivo Substituindo `.' por um alvo de hard link vazio Substituindo `.' por um nome de membro vazio Alterne ambas as meias palavras das palavras e bytes das meias palavras nos dados. Equivalente ao -sS Alterne os bytes de cada meia palavra nos arquivos Alterne as meias palavras de cada palavra (4 bytes) nos arquivos Ações de execução síncrona. Estas são executadas quando o número do checkpoint dado pela opção --checkpoint é alcançado. Execução simultânea opções: Para continuar, digite o nome do dispositivo/arquivo quanto estiver pronto.
 Muitos argumentos Truncar FILE para o tamanho de arquivo especificado anteriormente pela opção --length (ou 0, se não tiver sido dado) Tente `%s --help' ou `%s --usage' para maiores informações.
 Argumentos inesperados Formato de data desconhecido Campo `%s' desconhecido Erro de sistema desconhecido Desvincular ARQUIVO Atualizar horários de acesso e modificação de FILE Utilização: Use determinado arquivo FORMATO Use o COMANDO remoto instanceado para rsh Use o velho portátil (ASCII) formato de arquivo Utilize esse NOME-DE-ARQUIVO ao invés da saída e entrada padrão. Opcional USUÁRIO e HOST especifica usuário e nomes de hosts no caso de um arquivamento remoto Os argumentos válidos são: Listar detalhadamente os arquivos processados Ao ler um pacote no formato CRC, será verificado apenas o CRC de cada arquivo no pacote, os arquivos não serão extraídos Escreve arquivos com largos blocos de zeros como arquivos esparsos Escrever para o NOME do arquivo, no lugar da saída padrão Escrito por %s e %s.
 Escrito por %s, %s, %s,
%s, %s, %s, %s,
%s, %s e outros.
 Escrito por %s, %s, %s,
%s, %s, %s, %s,
%s e %s.
 Escrito por %s, %s, %s,
%s, %s, %s, %s
e %s.
 Escrito por %s, %s, %s,
%s, %s, %s e %s.
 Escrito por %s, %s, %s,
%s, %s e %s.
 Escrito por %s, %s, %s,
%s e %s.
 Escrito por %s, %s, %s
e %s.
 Escrito por %s, %s e %s.
 Escrito por %s.
 Você deve especificar umas das opções -oipt.
Tente `%s --help' ou `%s --usage' para maiores informações.
 [ARGUMENTOS...] [USUÁRIO][:.][GRUPO] [[USUARIO@]MAQUINA:]NOME-DO-ARQUIVO [diretório-destino] ` '%s' existe mas não é um diretório %s é um argumento ambíguo %s linha em branco ignorada impossível separar os arquivos gerado na saída padrão, use está opção --file não foi possível obter o grupo a partir do UID numérico não é possível vincular %s a %s não pôde criar o diretório '%s' não foi possível abrir %s impossivel abrir `%s' não pôde validar %s não pôde remover %s Não é possível pesquisar não foi possível se posicionar na saida impossível ajustar a hora em `%s' não pôde trocar os bytes de %s: número de bytes impar não pôde trocar as palavras de %s: número ímpar de palavras não pode desvincular '%s' controle de operação da unidade de fita magnética arquivo criado não está esparso maior número do dispositivo menor número do dispositivo número do dispositivo erro ao fechar o arquivo exec/tcp: Serviço não disponível falhou em retornar ao diretório inicial de trabalho modo arquivo nome do arquivo contém caracteres nulos tamanho do arquivo genfile manipulou arquivos de dados para a suíte de teste GNU paxutils.
OPÇÕES são:
 gid determinar o uso de uma mensagem curta determinar esta lista de ajuda travar para SECS segundos (padrão 3600) máscara incorreta (próximo `%s') número do inode erro interno: o descritor da fita foi alterado de %d para %d formato do arquivo inválido `%s'; os formatos válidos são:
crc newc odc bin ustar tar (todas maiúisculas também reconhecidas) %s é um argumento inválido para %s tamanho do bloco inválido valor do contador inválido grupo inválido cabeçalho inválido: erro de validação usuário inválido memória insuficiente data de modificação tamanho do nome dispositivo de fita não especificado número de links operação operação [contador] Final de arquivo prematuro fim de arquivo prematuro imprimir versão do programa rdev rdev maior rdev menor erro de leitura renomeia %s ->  tamanho de arquivo requisitado %lu, real %lu definir nível de depuração definir nome do arquivo de saída da depuração ajustar o nome do programa a entrada padrão está fechada a saida padrão está fechada falha inesperada(%s) stdin stdout número excessivo de argumentos uid incapaz de registrar o diretório atual de trabalho use dispositivos como o nome do arquivo da unidade de fita para operar em use o COMMANDO remoto ao invés do rsh a memória virtual encheu aviso: o cabeçalho do arquivo tem os bytes em ordem reversa aviso: %ld byte ignorado do lixo aviso: %ld bytes ignorados do lixo erro de escrita PRIuMAX File %s grew, % new byte not copied File %s grew, %<PRIuMAX> new bytes not copied Arquivo %s cresceu, % novo byte não copiado Arquivo %s cresceu, % novos bytes não copiados 