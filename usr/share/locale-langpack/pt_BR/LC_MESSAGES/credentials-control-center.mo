��          �   %   �      P      Q     r     �  #   �  =   �  @   �     5     B     O     \     l  '   �     �  #   �  1   �  2        A  F   P  $   �  "   �  [   �  :   ;  X   v  S   �  '   #  �  K  -        L     _  (   t  >   �  8   �     	     %	     5	     A	     P	  0   p	     �	  &   �	  2   �	  3   
     8
  `   F
  +   �
  #   �
  T   �
  6   L  Z   �  L   �  %   +                                                                                                    	               
           - Edit Web credentials settings Add account… All applications Another instance is already running Are you sure that you wish to remove this Ubuntu Web Account? Control whether this application integrates with Online Accounts Edit Options Grant access Legal notice Online Accounts Online Accounts preferences Online account credentials and settings Options Output version information and exit Please authorize Ubuntu to access your %s account Please authorize Ubuntu to access your %s account: Remove Account Run '%s --help' to see a full list of available command line options.
 Select to configure a new %s account Show accounts that integrate with: The Web Account which manages the integration of %s with your applications will be removed. The following applications integrate with your %s account: There are currently no account providers available which integrate with this application There are currently no applications installed which integrate with your %s account. Your online %s account is not affected. Project-Id-Version: gnome-control-center-signon
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-01 12:56+0000
PO-Revision-Date: 2013-03-19 17:23+0000
Last-Translator: Tiago Hillebrandt <tiagohillebrandt@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:14+0000
X-Generator: Launchpad (build 18115)
  - Editar configurações das credenciais web Adicionar conta... Todos os aplicativos Outra instância já está em execução Você tem certeza de que deseja remover esta conta web Ubuntu? Controla se esse aplicativo se integra as contas on-line Editar opções Conceder acesso Aviso legal Contas on-line Preferências de contas on-line Configurações e credenciais das contas on-line Opções Exibir informações da versão e sair Por favor autorize o Ubuntu a acessar sua conta %s Por favor autorize o Ubuntu a acessar sua conta %s: Remover conta Execute "%s --help" para obter uma lista completa de opções de linha de comando disponíveis.
 Selecione para configurar uma nova conta %s Mostrar contas que se integram com: Uma conta web que gerencia a integração de %s com seus aplicativos será removida. Os seguintes aplicativos se integram com sua conta %s: Atualmente não há fornecedores de conta disponíveis que se integram com este aplicativo Atualmente não há aplicativos instalados que se integram com sua conta %s. Sua conta on-line %s não é afetada. 