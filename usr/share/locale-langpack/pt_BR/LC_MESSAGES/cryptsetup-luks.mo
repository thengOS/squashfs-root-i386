��    A      $  Y   ,      �     �  �   �     f     �     �     �     �     �     �     �  7   �     6     Q     V     m     |     �     �     �  )   �       ?     4   S     �  %   �  &   �     �  -   	     9	     A	  "   X	  6   {	  >   �	     �	     
  &   '
  M   N
  +   �
  6   �
  #   �
     #  .   3  '   b     �     �     �     �     �       G     ,   _     �     �  "   �     �     �     �                  (   3     \     v  1   �  �  �     g  �   |     T  #   t     �     �     �  $   �     �       ?        [     w  +   |     �     �  (   �              '   2     Z  C   m  &   �  &   �  5   �  ,   5     b  -   }     �     �  )   �  >   �  N   ;     �     �  )   �  _   �  6   K  :   �  3   �     �  1     +   8  #   d     �  7   �     �  #   �       [   0  3   �     �     �  0   �  !        <     T     h     �     �  6   �     �       :        +   %   $   /   1      &                   =   <      @      ?   *   ;              (   
      A       >             #      0       ,                                   .         "                                        '                   9   -   )   	   :   4   !           5   8   7   2                     3          6    
<action> is one of:
 
<name> is the device to create under %s
<device> is the encrypted device
<key slot> is the LUKS key slot number to modify
<key file> optional key file for the new key for luksAddKey action
 %s is not a LUKS partition
 %s: requires %s as arguments <device> <device> <key slot> <device> <name>  <device> [<new key file>] <name> <name> <device> Align payload at <n> sector boundaries - for luksFormat Argument <action> missing. BITS Can't open device: %s
 Command failed Command successful.
 Create a readonly mapping Display brief usage Do not ask for confirmation Failed to obtain device mapper directory. Help options: How many sectors of the encrypted data to skip at the beginning How often the input of the passphrase can be retried Key %d not active. Can't wipe.
 Key size must be a multiple of 8 bits PBKDF2 iteration time for LUKS (in ms) Print package version Read the key from a file (can be /dev/random) SECTORS Show this help message Shows more detailed error messages The cipher used to encrypt the disk (see /proc/crypto) The hash used to create the encryption key from the passphrase The size of the device The size of the encryption key The start offset in the backend device This is the last keyslot. Device will become unusable after purging this key. This will overwrite data on %s irrevocably. Timeout for interactive passphrase prompt (in seconds) Unable to obtain sector size for %s Unknown action. Verifies the passphrase by asking for it twice [OPTION...] <action> <action-specific>] add key to LUKS device create device dump LUKS partition information formats a LUKS device key %d active, purge first.
 key %d is disabled.
 key material section %d includes too few stripes. Header manipulation?
 memory allocation error in action_luksFormat modify active device msecs open LUKS device as mapping <name> print UUID of LUKS device remove LUKS mapping remove device resize active device secs show device status tests <device> for LUKS partition header unknown hash spec in phdr unknown version %d
 wipes key with number <key slot> from LUKS device Project-Id-Version: cryptsetup
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2007-01-02 16:49+0100
PO-Revision-Date: 2009-08-05 16:31+0000
Last-Translator: André Gondim <Unknown>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:25+0000
X-Generator: Launchpad (build 18115)
 
<action> é um de:
 
<nome> é o dispositivo criado sob %s
<dispositivo> é um dispositivo criptografado
<porta> é o número da porta LUKS a ser modificada
<arquivo chave> arquivo chave opcional para a nova chave na ação luksAddKey
 %s não é uma partição LUKS
 %s: necessita de %s como argumentos <dispositivo> <dispositivo> <espaço chave> <dispositivo> <nome>  <dispositivo> [<novo arquivo chave>] <nome> <nome> <dispositivo> Alinhar a carga útil em <n> sectores limites - para luksFormat Falta o argumento <ação>. BITS Não foi possível abrir o dispositivo: %s
 Comando falhou Comando executado com sucesso.
 Criar um mapeamento para somente leitura Exibir uso breve Não perguntar por confirmação Falha ao obter device mapper directory' Opções de ajuda: Quantos setores de dados encriptados a serem preservados ao início Quantas vezes a frase pode ser tentada Chave %d inativa. Impossível limpar.
 O tamanho da chave precisa ser um múltiplo de 8 bits PBKDF2 tempo de iteração para LUKS (em ms) Imprimir versão do pacote Ler a chave do arquivo (pode ser /dev/random) SETORES Mostrar esta mensagem de ajuda Mostrar mensagens de erro mais detalhadas A cifragem usada para criptografar o disco (veja /proc/crypto) O hash utilizado para criar a chave de encriptação a partir da frase secreta O tamanho do dispositivo O tamanho da chave encriptada O início do deslocamento num dispositivo Esse é o último espaço-chave. O dispositivo será inutilizado após a remoção dessa chave. Isto irá sobrescrever os dados em %s definitivamente. Tempo de espera para prompt de frase secreta (em segundos) Não foi possível obter o tamanho do setor para %s Ação desconhecida. Verifica a frase secreta perguntando-o duas vezes [OPÇÃO...] <ação> <ação-específica>] adicionar chave ao dispositivo LUKS criar dispositivo realiza um dump da informação dobre a partição LUKS formata um dispositivo LUKS chave %d ativa, eliminar primeiro.
 chave %d está desabilitada.
 material essencial na seção %d inclui muito poucas listras. Manipulação de cabeçalho?
 erro na alocação de memória em action_luksFormat modificar dispositivo ativo milisegundos abrir dispositivo LUKS como um mapeamento <nomr> imprimir UUID do dispositivo LUKS remover mapeamento LUKS remover dispositivo redimensionar dispositivo ativo segs mostrar estado do dispositivo testa <dispositivo> para cabeçalho de partição LUKS hash spec desconhecido em phdr versão %d desconhecida
 lipa chave com número <espaço chave> do dispositivo LUKS 