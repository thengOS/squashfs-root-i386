��          �      �       H  $   I  F   n  4   �  L   �  4   7  "   l     �     �     �     �     �        0     �  C  "     P   5  9   �  N   �  ?     %   O     u     �     �     �     �     �  &   �                               
                       	           CommentA system restart is required CommentAn application has crashed on your system (now or in the past) CommentControl the notifications for system helpers CommentExtra packages can be installed to enhance application functionality CommentSoftware upgrade notifications are available CommentSystem Notification Helper NameApport Crash NameNotification Helper NameOther Notifications NameReboot Required NameRestricted Install NameUpgrade Hook X-KDE-KeywordsNotify,Alerts,Notification,popups Project-Id-Version: kubuntu-notification-helper
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-04-01 17:07+0000
PO-Revision-Date: 2013-04-09 12:39+0000
Last-Translator: Neliton Pereira Jr. <nelitonpjr@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:48+0000
X-Generator: Launchpad (build 18115)
 É necessário reiniciar o sistema Um aplicativo foi encerrado abruptamente no seu sistema (agora ou anteriormente) Controlar as notificações para os auxiliares de sistema Pacotes extras podem ser instalados para melhor a funcionalidade do aplicativo Notificações de atualização de software estão disponíveis Sistema de auxílio de notificações Apport Crash Auxiliar de notificações Outras notificações Reinicialização necessária Instalação restrita Atualizar gancho Notificar,Alertas,Notificação,popups 