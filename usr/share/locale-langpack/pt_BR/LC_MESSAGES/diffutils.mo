��    �        3  �      �  �   �     �  h  �  C     B   F  B   �  9   �  8     y   ?  7   �  6   �  5   (  3   ^  G   �  E   �  n      I   �  @   �  G     @   b  L   �  G   �  D   8  C   }  M   �  8     K   H  M   �  y   �  A   \  �   �  �  ]  �     i   �     !   0   A      r   0   �      �      �   ,   �   ,   !  ,   ?!  '   l!  -   �!      �!  (   �!  (   "     5"     R"     r"     �"     �"  (   �"  P   �"  D   #  E   W#  (   �#  C   �#  @   
$  :   K$  C   �$  M   �$  ~   %  ;   �%  J   �%  C   &  A   b&  3   �&  L   �&  :   %'  L   `'  >   �'  ;   �'  7   ((  4   `(  3   �(  K   �(  H   )  0   ^)  L   �)  F   �)  B   #*  1   f*  �   �*  H    +  J   i+  F   �+  �   �+  H   �,  H   �,  �   -  6   �-  7   �-  J   .  E   S.  ;   �.  L   �.  5   "/  @   X/  7   �/  =   �/  <   0  M   L0  @   �0  ?   �0  >   1  <   Z1  6   �1  K   �1  <   2  M   W2  3   �2     �2  !   �2     3  !   63     X3     x3     �3  F   �3  =   �3  &   4     @4     _4  ?   w4  K   �4     5     5     75     S5  $   k5     �5     �5  	   �5  I   �5     6     "6     /6     86     R6     q6     �6     �6     �6  #   �6     �6     �6     �6     7  �   %7  :   �7     8  q   8     �8     �8     �8  %   �8     �8     �8     
9     9     .9  "   ;9  4   ^9     �9  .   �9     �9  ;   �9  3   2:  /   f:  +   �:  '   �:  #   �:     ;     .;     J;     Z;     \;  )   o;  )   �;     �;     �;      �;     <     '<  	   A<  n  K<      �=     �=     �=     �=  1   >  2   9>  0   l>  )   �>  1   �>  -   �>     '?     8?  "   F?  %   i?     �?     �?     �?     �?  	   �?     �?     �?     �?     �?     @     @     @     "@     0@     L@     `@  
   o@     z@  �  �@  �   aB     GC  �  _C  F   �D  F   <E  1   �E  ?   �E  @   �E  �   6F  #   �F  2   �F  1   G  /   BG  E   rG  N   �G  u   H  W   }H  C   �H  V   I  =   pI  W   �I  S   J  >   ZJ  >   �J  R   �J  @   +K  Q   lK  I   �K  �   L  K   �L  �   �L  �  �M  �   uO  k   WP  +   �P  <   �P     ,Q  6   FQ     }Q  !   �Q  -   �Q  -   �Q  -   R  '   ;R  .   cR      �R  (   �R  ,   �R     	S  $   (S  $   MS     rS     tS  +   wS  U   �S  c   �S  H   ]T  .   �T  P   �T  U   &U  O   |U  N   �U  \   V  �   xV  2   W  A   7W  S   yW  Q   �W  6   X  b   VX  M   �X  Q   Y  D   YY  G   �Y  -   �Y  .   Z  -   CZ  L   qZ  N   �Z  1   [  S   ?[  R   �[  S   �[     :\  �   U\  N   �\  <   ']  S   d]  �   �]  J   K^  L   �^  �   �^  9   p_  9   �_  V   �_  N   ;`  =   �`  O   �`  0   a  E   Ia  <   �a  5   �a  6   b  V   9b  0   �b  1   �b  2   �b  5   &c  9   \c  :   �c  8   �c  Z   
d  4   ed  .   �d  #   �d      �d  (   e  %   7e     ]e     ie  Z   ye  U   �e  /   *f  #   Zf  $   ~f  D   �f  M   �f  (   6g  .   _g  -   �g  $   �g  /   �g  "   h  $   4h  	   Yh  S   ch     �h     �h     �h  /   �h     i     8i     Ki     ^i     vi  #   �i     �i  $   �i     �i     �i  �   j  =   �j     k  {   k     �k     �k     �k  *   �k     �k     l     *l     El     `l  0   vl  D   �l  '   �l  9   m     Nm  9   dm  1   �m  -   �m  )   �m  %   (n  !   Nn     pn     �n     �n     �n  !   �n  4   �n  5   o  $   Ho      mo  )   �o  /   �o      �o  
   	p  �  p     �q     �q     �q     �q  4   r  :   Pr  2   �r  >   �r  D   �r  <   Bs     s     �s  *   �s  (   �s     �s     t     t     .t  	   >t      Ht     it     qt     �t     �t     �t     �t     �t  3   �t     !u     ;u     Ou     `u     �       �       �          `   x   �           %   X   �   p   �   y   �           �   �   �   �       $   K   �   N   �   �   !       ]      �   �   5   �   ?   Y           u   =       +   &   .   r   �   �   F   �       �   b   �       <           �   }   �   �       E   �   �   �   V   �       f       �   G               o   �   �   �       �   e       ~       i   �      �   '   �   �           �   (       4       �   �         a   )   �   �   �   �          �   B      �   �   �   �           J   �          S       �   �       �   �   �   �   �       w       g               \   A       P           �   �         L   #       �   �      �   �   �      ,       �   �   �   	   �       j   9       :          -              7   �   �       �       �           *   �   �             U       M   �   8   O   |   0   s       v   �      H          �          z   m             3   W   �       �   T   q   �   D   k   �   �   d              ;   {           c           t           
       �       �       �       h   �   "   Z   R           n           C   I   /       �       1      �   �   @               >            �      2   _           [      �   ^       Q   �   6   l   �               
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

 
Report bugs to: %s
 
The default output format is a somewhat human-readable representation of
the changes.

The -e, -E, -x, -X (and corresponding long) options cause an ed script
to be output instead of the default.

Finally, the -m (--merge) option causes diff3 to do the merge internally
and output the actual merged file.  For unusual input, this is more
robust than using ed.
     --GTYPE-group-format=GFMT   format GTYPE input groups with GFMT     --LTYPE-line-format=LFMT    format LTYPE input lines with LFMT     --binary                    read and write data in binary mode     --diff-program=PROGRAM   use PROGRAM to compare files     --diff-program=PROGRAM  use PROGRAM to compare files     --from-file=FILE1           compare FILE1 to all operands;
                                  FILE1 can be a directory     --help                   display this help and exit     --help                  display this help and exit     --help                 display this help and exit     --help               display this help and exit     --horizon-lines=NUM  keep NUM lines of the common prefix and suffix     --ignore-file-name-case     ignore case when comparing file names     --label LABEL             use LABEL instead of file name
                                (can be repeated)     --left-column             output only the left column of common lines     --line-format=LFMT          format all input lines with LFMT     --no-ignore-file-name-case  consider case when comparing file names     --normal                  output a normal diff (the default)     --speed-large-files  assume large files and many scattered small changes     --strip-trailing-cr         strip trailing carriage return on input     --strip-trailing-cr      strip trailing carriage return on input     --strip-trailing-cr     strip trailing carriage return on input     --suppress-blank-empty    suppress space or tab before empty output lines     --suppress-common-lines   do not output common lines     --tabsize=NUM             tab stops every NUM (default 8) print columns     --tabsize=NUM            tab stops at every NUM (default 8) print columns     --to-file=FILE2             compare all operands to FILE2;
                                  FILE2 can be a directory     --unidirectional-new-file   treat absent first files as empty   Both GFMT and LFMT may contain:
    %%  %
    %c'C'  the single character C
    %c'\OOO'  the character with octal code OOO
    C    the character C (other characters represent themselves)   GFMT (only) may contain:
    %<  lines from FILE1
    %>  lines from FILE2
    %=  lines common to FILE1 and FILE2
    %[-][WIDTH][.[PREC]]{doxX}LETTER  printf-style spec for LETTER
      LETTERs are as follows for new group, lower case for old group:
        F  first line number
        L  last line number
        N  number of lines = L-F+1
        E  F-1
        M  L+1
    %(A=B?T:E)  if A equals B then T else E   LFMT (only) may contain:
    %L  contents of line
    %l  contents of line, excluding any trailing newline
    %[-][WIDTH][.[PREC]]{doxX}n  printf-style spec for input line number   These format options provide fine-grained control over the output
    of diff, generalizing -D/--ifdef. %s %s differ: byte %s, line %s
 %s %s differ: byte %s, line %s is %3o %s %3o %s
 %s home page: <%s>
 %s home page: <http://www.gnu.org/software/%s/>
 %s: diff failed:  %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: recursive directory loop %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 ' (C) --from-file and --to-file both specified -3, --easy-only             like -e, but incorporate only nonoverlapping changes -A, --show-all              output all changes, bracketing conflicts -B, --ignore-blank-lines     ignore changes whose lines are all blank -D option not supported with directories -E, --ignore-tab-expansion      ignore changes due to tab expansion -E, --ignore-tab-expansion   ignore changes due to tab expansion -E, --show-overlap          like -e, but bracket conflicts -F, --show-function-line=RE   show the most recent line matching RE -H, --speed-large-files      assume large files, many scattered small changes -L, --label=LABEL           use LABEL instead of file name
                                (can be repeated up to three times) -N, --new-file                  treat absent files as empty -S, --starting-file=FILE        start with FILE when comparing directories -T, --initial-tab             make tabs line up by prepending a tab -T, --initial-tab           make tabs line up by prepending a tab -W, --ignore-all-space       ignore all white space -W, --width=NUM               output at most NUM (default 130) print columns -X                          like -x, but bracket conflicts -X, --exclude-from=FILE         exclude files that match any pattern in FILE -Z, --ignore-trailing-space     ignore white space at line end -Z, --ignore-trailing-space  ignore white space at line end -a, --text                      treat all files as text -a, --text                   treat all files as text -a, --text                  treat all files as text -b, --ignore-space-change       ignore changes in the amount of white space -b, --ignore-space-change    ignore changes in the amount of white space -b, --print-bytes          print differing bytes -c, -C NUM, --context[=NUM]   output NUM (default 3) lines of copied context -d, --minimal                try hard to find a smaller set of changes -d, --minimal            try hard to find a smaller set of changes -e, --ed                      output an ed script -e, --ed                    output ed script incorporating changes
                                from OLDFILE to YOURFILE into MYFILE -i, --ignore-case               ignore case differences in file contents -i, --ignore-case            consider upper- and lower-case to be the same -i, --ignore-initial=SKIP         skip first SKIP bytes of both inputs -i, --ignore-initial=SKIP1:SKIP2  skip first SKIP1 bytes of FILE1 and
                                      first SKIP2 bytes of FILE2 -l, --left-column            output only the left column of common lines -l, --verbose              output byte numbers and differing byte values -m, --merge                 output actual merged file, according to
                                -A if no other options are given -n, --bytes=LIMIT          compare at most LIMIT bytes -n, --rcs                     output an RCS format diff -o, --output=FILE            operate interactively, sending output to FILE -p, --show-c-function         show which C function each change is in -q, --brief                   report only when files differ -r, --recursive                 recursively compare any subdirectories found -s, --quiet, --silent      suppress all normal output -s, --report-identical-files  report when two files are the same -s, --suppress-common-lines  do not output common lines -t, --expand-tabs             expand tabs to spaces in output -t, --expand-tabs            expand tabs to spaces in output -u, -U NUM, --unified[=NUM]   output NUM (default 3) lines of unified context -v, --version                output version information and exit -v, --version               output version information and exit -v, --version              output version information and exit -v, --version            output version information and exit -w, --ignore-all-space          ignore all white space -w, --width=NUM              output at most NUM (default 130) print columns -x, --exclude=PAT               exclude files that match PAT -x, --overlap-only          like -e, but incorporate only overlapping changes -y, --side-by-side            output in two columns Binary files %s and %s differ
 Common subdirectories: %s and %s
 Compare FILES line by line. Compare three files line by line. Compare two files byte by byte. David Hayes David MacKenzie Exit status is 0 if inputs are the same, 1 if different, 2 if trouble. Exit status is 0 if successful, 1 if conflicts, 2 if trouble. File %s is a %s while file %s is a %s
 Files %s and %s are identical
 Files %s and %s differ
 General help using GNU software: <http://www.gnu.org/gethelp/>
 If --from-file or --to-file is given, there are no restrictions on FILE(s). Invalid back reference Invalid character class name Invalid collation character Invalid content of \{\} Invalid preceding regular expression Invalid range end Invalid regular expression Len Tower Mandatory arguments to long options are mandatory for short options too.
 Memory exhausted Mike Haertel No match No newline at end of file No previous regular expression Only in %s: %s
 Packaged by %s
 Packaged by %s (%s)
 Paul Eggert Premature end of regular expression Randy Smith Regular expression too big Report %s bugs to: %s
 Richard Stallman SKIP values may be followed by the following multiplicative suffixes:
kB 1000, K 1024, MB 1,000,000, M 1,048,576,
GB 1,000,000,000, G 1,073,741,824, and so on for T, P, E, Z, Y. Side-by-side merge of differences between FILE1 and FILE2. Success The optional SKIP1 and SKIP2 specify the number of bytes to skip
at the beginning of each file (zero by default). Thomas Lord Torbjorn Granlund Trailing backslash Try '%s --help' for more information. Unknown system error Unmatched ( or \( Unmatched ) or \) Unmatched [ or [^ Unmatched \{ Usage: %s [OPTION]... FILE1 FILE2
 Usage: %s [OPTION]... FILE1 [FILE2 [SKIP1 [SKIP2]]]
 Usage: %s [OPTION]... FILES
 Usage: %s [OPTION]... MYFILE OLDFILE YOURFILE
 Written by %s and %s.
 Written by %s, %s, %s,
%s, %s, %s, %s,
%s, %s, and others.
 Written by %s, %s, %s,
%s, %s, %s, %s,
%s, and %s.
 Written by %s, %s, %s,
%s, %s, %s, %s,
and %s.
 Written by %s, %s, %s,
%s, %s, %s, and %s.
 Written by %s, %s, %s,
%s, %s, and %s.
 Written by %s, %s, %s,
%s, and %s.
 Written by %s, %s, %s,
and %s.
 Written by %s, %s, and %s.
 Written by %s.
 ` block special file both files to be compared are directories cannot interactively merge standard input character special file cmp: EOF on %s
 conflicting output style options conflicting tabsize options conflicting width options directory ed:	Edit then use both versions, each decorated with a header.
eb:	Edit then use both versions.
el or e1:	Edit then use the left version.
er or e2:	Edit then use the right version.
e:	Discard both versions then edit a new one.
l or 1:	Use the left version.
r or 2:	Use the right version.
s:	Silently include common lines.
v:	Verbosely include common lines.
q:	Quit.
 failed to reopen %s with mode %s fifo incompatible options input file shrank internal error: invalid diff type in process_diff internal error: invalid diff type passed to output internal error: screwup in format of diff blocks invalid diff format; incomplete last line invalid diff format; incorrect leading line chars invalid diff format; invalid change separator memory exhausted message queue options -l and -s are incompatible pagination not supported on this host program error read failed regular empty file regular file semaphore shared memory object socket stack overflow standard output stderr stdin stdout symbolic link too many file label options typed memory object unknown stream weird file write failed Project-Id-Version: diffutils 2.8.3
Report-Msgid-Bugs-To: bug-diffutils@gnu.org
POT-Creation-Date: 2013-03-24 11:05-0700
PO-Revision-Date: 2015-12-27 05:48+0000
Last-Translator: Halley Pacheco de Oliveira <Unknown>
Language-Team: Brazilian Portuguese <ldp-br@bazar.conectiva.com.br>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:43+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
 
Licença GPLv3+: GNU GPL versão 3 ou posterior <http://gnu.org/licenses/gpl.html>.
Este é um software livre: você é livre para modificá-lo ou redistribuí-lo.
Não há NENHUMA GARANTIA, até os limites permitidos por lei.

 
Relatar erro para: %s
 
O formato padrão de saída é uma representação das alterações
mais ou menos legível por humanos.

As opções -e, -E, -x, -X (e seus correspondentes longos) fazem com que o script
ed seja exibido em lugar do padrão.

Finalmente, a opção -m (--merge) faz com que diff3 faça a mesclagem internamente
e exiba a o arquivo atual já mesclado. Para entradas incomuns, isto é melhor do
que usar ed.
     --GTYPE-group-format=GFMT formata grupos de entrada GTYPE como FMT     --LTYPE-line-format=LMFT formata linhas de entrada LTYPE como LFMT     --binary lê e escreve dados em modo binário     --diff-program=PROGRAMA usa PROGRAMA para comparar arquivos     --diff-program=PROGRAMA  use PROGRAMA para comparar arquivos     --from-file=ARQUIVO1  compara ARQUIVO1 a todos os operandos;
                                  ARQUIVO1 pode ser um diretório     --help exibir esta ajuda e sair     --help                  exibe esta ajuda e sai     --help                 exibe esta ajuda e sai     --help               exibe esta ajuda e sai     --horizon-lines=NUM  mantém NUM linhas do prefixo e sufixo comum     --ignore-file-name-case ignora capitulação ao comparar nomes de arquivos     --label RÓTULO         usa RÓTULO em vez do nome do arquivo
                                (pode ser repetido)     --left-column             saída de apenas uma coluna à esquerda das linhas comuns     --line-format=LFMT formata todas as linhas de entrada como LFMT     --no-ignore-file-name-case considera a capitulação ao comparar nomes de arquivos     --normal                  gera um diff normal (o padrão)     --speed-large-files assume arquivos grandes e pequenas mudanças muito fragmentadas     --strip-trailing-cr         remover caractere com código de retorno na entrada     --strip-trailing-cr remove as quebras de linhas da entrada     --strip-trailing-cr remove as quebras de linhas da entrada     --suppress-blank-empty suprime espaço ou tab antes de linhas de saída vazias     --suppress-common-lines   não gerar saída de linhas comuns     --tabsize=NUM             a tabulação termina a cada (padrão é 8) colunas     --tabsize=NUM tabulação ocupa NUM colunas de impressão (padrão 8)     --to-file=ARQUIVO2   compara todos os operandos a ARQUIVO2;
                                  ARQUIVO2 pode ser um diretório     --unidirectional-new-file trata primeiros arquivos ausentes como vazios   Ambos GFMT e LFMT podem conter:
    %%  %
    %c'C'  o carácter individual C
    %c'\OOO'  o carácter com o código octal OOO
    C    o carácter C (outros carácteres representam eles mesmos)   GFMT pode conter (apenas):
    %<  linhas do FILE1
    %>  linhas do FILE2
    %=  linhas comuns ente FILE1 e FILE2
    %[-][WIDTH][.[PREC]]{doxX}LETTER  estilo de impressão especificado para LETTER
      LETTERs são da seguinte forma para novo grupo, minúsculas para grupo velho:
        F  número da primeira linha
        L  número da última linha
        N  número de linhas = L-F+1
        E  F-1
        M  L+1
    %(A=B?T:E)  se A igual B então T senão E   LFMT (somente) pode conter:
    %L conteúdo da linha
    %l conteúdo da linha, com exclusão de qualquer nova linha posterior
    %[-][WIDTH][.[PREC]]{doxX}n printf-style especificação para o número da linha de entrada   Estas opções de formato fornecem um controle fino sobre a saída
    do diff, generalizando -D/--ifdef. %s e %s são diferentes: byte %s, linha %s
 %s e %s são diferentes: byte %s, linha %s é %3o %s %3o %s
 %s página inicial: <%s>
 %s página inicial: <http://www.gnu.org/software/%s/>
 %s: diff falhou:  %s: opção é inválida -- '%c'
 %s: opção '%c%s' não permite um argumento
 %s: opção '%s' é ambígua; possibilidades: %s: opção '--%s' não permite um argumento
 %s: opção '--%s' requer um argumento
 %s: opção '-W %s' não permite um argumento
 %s: opção '-W %s' é ambígua
 %s: opção '-W %s' requer um argumento
 %s: opção necessita de argumentos -- '%c'
 %s: diretório laço recursivo %s: opção não reconhecida '%c%s'
 %s: opção não reconhecida '--%s'
 ' © ambos --from-file e --to-file especificados -3, --easy-only like -e, mas incorporar apenas as alterações que não se sobrepõem -A, --show-all              gerar saída de todas as mudanças, colocando conflitos entre colchetes -B, --ignore-blank-lines ignorar as mudanças nas linhas que são vazias a opção -D não é suportada com diretórios -E, --ignore-tab-expansion ignora alterações devido a expansão de tabulação -E, --ignore-tab-expansion ignorar as alterações devido à expansão de tabulação -E, --show-overlap          semelhante a -e, mas põe conflitos entre colchetes -F, --show-function-line=RE   mostra a linha mais recente que corresponde a RE -H, --speed-large-files assumir arquivos grandes, várias e pequenas alterações espalhadas -L, --label=RÓTULO           use RÓTULO ao invés do nome do arquivo
                                (pode ser repetido até três vezes) -N, --new-file trata arquivos ausentes como vazios -S, --starting-file=FILE começa com FILE ao comparar diretórios -T, --initial-tab             fazer a tabulação alinhar inserindo um tab no antes -T, --initial-tab           alinha tabulações pré-posicionando uma tabulação -W, --ignore-all-space ignora todos espaços em branco -W, --width=NUM               limita a saída a no máximo NUM (padrão 130) colunas de impressão -X                          semelhante -x, mas põe conflitos entre colchetes -X, --exclude-from=FILE exclui arquivos que correspondem qualquer padrão em FILE -Z, --ignore-trailing-space ignora espaço em branco no fim da linha -Z, --ignore-trailing-space ignora espaços em branco no final da linha -a, --text trata todos os arquivos como texto -a, --text considera todos arquivos como texto -a, --text trata todos os arquivos como texto -b, --ignore-space-change ignora alterações no total de espaços em branco -b, --ignore-space-change ignora mudanças na quantidade de espaços em branco -b, --print-bytes          exibe bytes diferentes -c, -C NUM, --context [= NUM​​] gera NUM (padrão 3) linhas de contexto copiado -d, --minimal tentar forçar a localização de pequenos conjuntos de alterações -d, --minimal            tentar com afinco encontrar um conjunto de mudanças menor -e, --ed gera um ed script e, --ed exibe o script ed incorporando as mudanças
                                de ARQUIVOANTIGO para SEUARQUIVO em MEUARQUIVO -i, --ignore-case ignora diferenças de capitulação em conteúdos de arquivo -i, --ignore-case não diferencia maiúsculas de minúsculas -i, --ignore-initial=SKIP         pula os primeiros SKIP bytes de ambas as entradas -i, --ignore-initial=SKIP1:SKIP2  pula os primeiros SKIP1 bytes de FILE1 e
                                      os primeiros SKIP2 bytes de FILE2 -l, --left-column saída de apenas uma coluna à esquerda de linhas comuns -l, --verbose              exibe números de byte e  valores byte diferentes -m, --merge arquivo de saída resultante da fusão atual, de acordo com
                                -A se nenhuma outra opção for dada -n, --bytes=LIMIT          compara no máximo LIMIT bytes -n, --rcs                     gera um diff em formato RCS -o, --output=ARQUIVO            operar interativamente, enviando a saída para ARQUIVO -p, --show-c-function         mostra em qual função C cada alteração está -q, --brief reporta apenas quando os arquivos são diferentes -r, --recursive compara quaisquer subdiretórios encontrados de forma recursiva -s, --quiet, --silent suprime toda saída normal -s, --report-identical-files reporta quando dois arquivos são iguais -s, --suppress-common-lines suprimir linhas comuns na saída -t, --expand-tabs converte tabs em espaços na saída -t, --expand-tabs expande tabs para espaços na saída -u, -U NUM, --unificad [= NUM​​] gera NUM (padrão 3) linhas de contexto unificado -v, --version mostra as informações da versão -v, --version  mostra as informações da versão -v, --version mostra informação da versão e sai -v, -version resulta em informação da versão e sai -w, --ignore-all-space ignora todos os espaços em branco -w, --width=NUM exibe no máximo NUM colunas (padrão 130) -x, --exclude=PAT exclui arquivos que correspondem a PAT -x, --overlap-only          semelhante a -e, mas incorpora apenas alterações sobrepostas -y, --side-by-side            saída em duas colunas Os arquivos binários %s e %s são diferentes
 Subdiretórios idênticos: %s e %s
 Compara ARQUIVOS linha por linha Comparar três arquivos linha por linha. Comparar dois arquivos byte por byte. David Hayes David MacKenzie Estado de saída é 0 se as entradas são as mesmas, 1 se diferente, 2 se ocorrer um erro. O estado de saída é 0 se bem sucedido, 1 se houver conflitos, 2 se ocorrer um erro. O arquivo %s é %s enquanto o arquivo %s é %s
 Os aquivos %s e %s são idênticos
 Os arquivos %s e %s são diferentes
 Ajuda geral para uso de software GNU: <http://www.gnu.org/gethelp/>
 Se --from-file ou --to-file é informado, não há restrições em ARQUIVO(s) A referência retroativa não é válida O nome da classe de caracteres não é válido O caracter de classificação não é válido O counteúdo de \{\} não é válido A expressão regular precedente não é válida O fim do intervalo não é válido A expressão regular não é válida Len Tower Argumentos obrigatórios para opções longas também o são para opções curtas.
 Memória esgotada Mike Haertel Sem correspondente Falta o caracter nova linha no final do arquivo Sem expressão regular prévia Somente em %s: %s
 Empacotado por %s
 Empacotado por %s (%s)
 Paul Eggert Fim prematuro da expressão regular Randy Smith A expressão regular é muito grande Relatar %s erros para: %s
 Richard Stallman Os valores do SALTO podem ser seguidos por um
dos sufixos multiplicadores mostrados abaixo:
kB 1000, K 1024, MB 1,000,000, M 1,048,576,
GB 1,000,000,000, G 1,073,741,824,
e assim por diante para T, P, E, Z, Y. Mescla lado a lado das diferenças entre ARQUIVO1 e ARQUIVO2. Sucesso O SKIP1 que é opcional e o SKIP2 especificam  o número de bytes para pular
no início de cada arquivo (zero por padrão). Thomas Lord Torbjorn Granlund Contrabarra final Tente '%s --help' para mais informações. Erro de sistema desconhecido ( or \( sem correspondente ) or \) sem correspondente [ ou [^ sem correspondente \{ sem correspondente Utilização: %s [OPÇÃO]... ARQUIVO1 ARQUIVO2
 Utilização: %s [OPÇÃO]... ARQUIVO1 [ARQUIVO2 [SALTO1 [SALTO2]]]
 Utilização: %s [OPÇÃO]... ARQUIVOS
 Uso: %s [OPÇÃO]... MEUARQUIVO ARQUIVOANTIGO SEUARQUIVO
 Escrito por %s e %s.
 Escrito por %s, %s, %s,
%s, %s, %s, %s,
%s, %s e outros.
 Escrito por %s, %s, %s,
%s, %s, %s, %s,
%s e %s.
 Escrito por %s, %s, %s,
%s, %s, %s, %s
e %s.
 Escrito por %s, %s, %s,
%s, %s, %s e %s.
 Escrito por %s, %s, %s,
%s, %s e %s.
 Escrito por %s, %s, %s,
%s e %s.
 Escrito por %s, %s, %s
e %s.
 Escrito por %s, %s e %s.
 Escrito por %s.
 ` arquivo do tipo especial de bloco os dois arquivos a serem comparados são diretórios impossível mesclar interativamente a entrada padrão arquivo do tipo especial de caracter cmp: Fim de arquivo (EOF) em %s
 opções de estilo de saída conflitantes opções de tamanho de tabulação conflitantes opções de largura conflitantes diretório ed:	Editar, e em seguida, usar as duas versões, cada uma com um cabeçalho.
eb:	Editar e usar as duas versões.
el ou e1:	Editar e usar a versão da esquerda.
er ou e2:	Editar e usar a versão da direita.
e:	Descartar as duas versões, em seguida, editar uma nova.
l ou 1:	Use a versão da esquerda.
r ou 2:	Use a versão da direita.
s:	Incluir silenciosamente linhas comuns.
v:	Detalhadamente incluir linhas comuns.
q:	Sair.
 falha ao reabrir %s com modo %s fila opções incompatíveis o arquivo de entrada diminuiu erro interno: tipo de diff inválido em process_diff erro interno: tipo de diff inválido passado para a saída erro interno: confusão no formato dos blocos diff formato de diff inválido; a última linha não está completa formato de diff inválido; caracteres incorretos na linha de início formato de diff inválido; separador de diferença inválido memória esgotada fila de mensagem as opções -l e -s não são compatíveis este computador não suporta paginação erro do programa falha de leitura arquivo regular vazio arquivo regular semáforo objeto em memória compartilhada soquete estouro da pilha saída padrão saída de erro (stderr) stdin (entrada padrão) stdout (saída padrão) vínculo simbólico opções de rótulo de arquivo em número excessivo objeto de memória tipado stream desconhecida arquivo estranho erro de escrita 