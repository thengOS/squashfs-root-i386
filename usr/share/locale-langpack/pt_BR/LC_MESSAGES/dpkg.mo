��    �     T  	  �<      �P  ?   �P  a   �P  k   SQ  �   �Q  =   @R  &   ~R    �R  -   �S  .   �S     T  .   'T     VT      rT  4   �T     �T  5   �T  -   U  .   HU  0   wU  1   �U  $   �U  4   �U  /   4V     dV  q   |V  (   �V  $   W     <W     \W  :   |W  6   �W  1   �W  1    X  !   RX  (   tX     �X     �X     �X  .   �X     Y  S   Y     qY     �Y     �Y  )   �Y  9   �Y  .   Z  '   LZ  &   tZ     �Z  3   �Z  5   �Z     [     ![  P   .[  \   [  v   �[  [   S\  U   �\  [   ]     a]     v]     �]     �]     �]  Z   �]     ^     $^  �   3^     �^     _  =   -_     k_     �_     �_  R   �_  6   �_  �  *`  =   b  R   Db  1   �b  D   �b  A   c  '   Pc  &   xc  (   �c  '   �c  !   �c     d  +   !d  4   Md  !   �d  ,   �d  9   �d  -   e     9e  #   We     {e  $   �e     �e  "   �e     �e  !   f  #   ?f     cf     xf     �f     �f  )   �f     �f  3   �f  "   2g  2   Ug  1   �g  -   �g     �g      �g  /   h  '   Jh     rh     �h  .   �h     �h  -   �h  R   i  H   ri  Q   �i  A   j  e   Oj  f   �j  3   k  I   Pk  8   �k  7   �k  <   l  )   Hl     rl  (   �l  B   �l     �l  (   m     1m     Lm     _m      mm     �m  5   �m  1   �m     n     !n  #   :n  !   ^n     �n     �n  K   �n  7   o  5   >o  -   to     �o  !   �o     �o  '   �o  -    p      Np  5   op     �p      �p     �p  $   �p  ,   #q  .   Pq  .   q     �q  H   �q  /   r  6   Gr  "   ~r  #   �r  (   �r  >   �r  $   -s      Rs     ss     �s     �s     �s  �   �s     ft  #   st  :  �t  -   �u      v     v     "v     4v  *   Av  <   lv  #   �v  -   �v  -   �v  �   )w     �w     �w  [   �w  �  >x  g  9|    �}    �~  -   �  <   �  .   2�     a�  0   x�  3   ��  1   ݀     �     �     7�  �   C�  1   ��  *   *�  J   U�  2   ��  �  ӂ  &   x�  2   ��  E   ҄  V   �     o�  5   ��  .   ��  -   �  +   �  /   G�     w�  %   ��  /   ��  *   ۆ  &   �  !   -�  ]   O�     ��  !   ��  M   ԇ     "�  2   8�  �   k�  n  J�  �  ��  m  Q�  "   ��     �  4   �  .   6�  5   e�  3   ��     ϐ  G   ߐ  8   '�  ,   `�  C   ��     ё  (   �     �     �     0�  3   9�  )   m�  -   ��  :   Œ  V    �      W�     x�  *   ��     ��  &   Γ  -   ��  ,   #�  	   P�     Z�     p�  *   ��     ��     ɔ      �  H   �     P�  �   W�  �   (�  g   ۖ  j   C�  �   ��  �   ��  4   W�  5   ��  �     �   ��  �   U�  u   �  &   w�  A   ��  {   ��  !   \�  ,   ~�  *   ��  .   ֝     �  �   $�      ��  5   ɞ  ,   ��  $   ,�  N   Q�  *   ��  ;   ˟  ,   �  �   4�     ��  u   ��  �   5�     �     �     &�  9   3�  (   m�  X   ��  3   �  ?   #�  8   c�  2   ��  1   ϣ  ,   �  :   .�  4   i�  5   ��  !   Ԥ  4   ��  *   +�  A   V�  M   ��  -   �     �     (�  #   B�     f�     ��      ��  1   ��  8   ��  6   &�     ]�  &   y�  1   ��  !   ҧ  !   ��  5   �     L�     i�     �     ��     ��     ��  	   Ш  9   ڨ  .   �  #   C�  ,   g�     ��  ?   ��     �  %   �     9�  %   X�     ~�  -   ��  1   ª     ��     
�  )   )�  1   S�  &   ��     ��  =   ɫ  7   �  !   ?�  .   a�  *   ��  "   ��  &   ެ     �      �  @   7�  +   x�  ;   ��  3   �     �     1�  A   L�     ��  &   ��  -   Ԯ  0   �  (   3�  ?   \�  )   ��  -   Ư  "   ��     �  4   -�  E   b�     ��     ð  ,   ް  "   �  !   .�     P�  5   o�      ��     Ʊ     ձ  -   ��  5   "�  ,   X�  B   ��  B   Ȳ  +   �  G   7�  $   �      ��     ų     ޳  &   ��  8   �     M�  7   i�     ��  D   ��  c   �     h�     t�  *   ��  "   ��  3   ߵ  -   �  0   A�  &   r�     ��     ��     ˶     �  "   ��      �     7�  (   =�  =   f�     ��  /   ��  )   �  *   �  J   >�     ��  "   ��     ��     ָ     �  -   �     ;�  )   Y�  %   ��     ��     ��     ¹     ߹     ��     �      �     4�  )   I�  2   s�     ��     ��     ߺ  !   ��  !    �  #   B�     f�  %   ��  %   ��  '   ϻ  ,   ��     $�     =�     \�  *   y�     ��  !   ��     ۼ  !   ��     �      4�  -   U�  #   ��  #   ��  +   ˽  %   ��  $   �     B�      ^�     �     ��     ��  "   ��  :   �     �  F   9�     ��     ��  .   ��     �  $   �     *�     E�     [�     s�     ��  #   ��     ��     ��  !   �  "   &�  :   I�     ��  5   ��      ��  2   ��     -�  %   <�     b�  0   ��  *   ��  $   ��  /   �     3�  6   B�  V   y�  "   ��  (   ��     �  <   ,�  .   i�  )   ��  (   ��  +   ��     �  ,   3�  )   `�  4   ��  3   ��  2   ��  4   &�  ;   [�  <   ��  6   ��  :   �  7   F�  @   ~�  0   ��  ;   ��  ?   ,�  $   l�  A   ��  =   ��  >   �  $   P�  0   u�  !   ��  2   ��     ��  b   �     y�  c   ��     ��  :   �  <   T�  =   ��  _   ��  5   /�  0   e�     ��  H   ��  *   ��     $�     0�     P�     j�  )   ��  )   ��  $   ��  �   ��  &   }�  &   ��  '   ��      ��  7   �  t   L�  2   ��  -   ��  '   "�  4   J�  .   �  )   ��  G   ��      �  	   7�     A�  k   U�  /   ��     ��  >   �  %   E�  $   k�  #   ��  !   ��  "   ��  ^   ��  @   X�     ��     ��     ��  +   ��  7    �  !   8�     Z�     t�     ��     ��     ��     ��  O   ��  8   6�  *   o�     ��     ��     ��     ��  3   ��     �     $�  7   ?�  5   w�  
   ��     ��  |   ��  
   K�     V�  m   v�  0   ��  G   �     ]�  )   }�  !   ��  �   ��     R�     h�     v�  5   ��     ��     ��  "   ��  4   �     L�  !   j�  C   ��  =   ��  %   �     4�      B�  (   c�     ��  %   ��  7   ��     �  I   �  1   i�     ��  G   ��  P   ��  G   B�  ?   ��  8   ��  #   �  7   '�  #   _�  +   ��  2   ��  Y   ��  f   <�  L   ��  M   ��  &   >�  K   e�  3   ��  =   ��     #�     A�  =   _�     ��  4   ��  F   ��  1   .�  ;   `�  &   ��     ��     ��     �  %   �  .   A�  2   p�  .   ��  .   ��  ;   �  ;   =�      y�     ��  D   ��  *   ��     �     6�  <   M�  +   ��     ��  &   ��  -   ��  7   �     V�     ^�  .   d�     ��     ��  "   ��     ��     ��     �  )   �     D�  &   a�  '   ��  "   ��  "   ��     ��  6   �     F�     e�  7   w�  B   ��  H   ��  &   ;�     b�  $   ��  0   ��     ��  .   ��  6   �     T�  $   f�  4   ��     ��     ��  O   ��  
   B�  
   M�     X�  
   x�  %   ��     ��  %   ��  *   ��     �     �     (�  -   =�  +   k�     ��  )   ��  7   ��  +   �  !   C�  C   e�  $   ��  4   ��  F   �  @   J�  1   ��  >   ��     ��  ?   �  ,   V�  R   ��  '   ��  $   ��  	   #�     -�  Z   ;�  4   ��  2   ��  '   ��     &�  )   B�  .   l�  D   ��  G   ��  Y   (�  Q   ��  ^   ��  %   3�  +   Y�  !   ��  ;   ��  2   ��  %   �  %   <�  1   b�  +   ��  N   ��     �  3   )�  *   ]�     ��     ��  "   ��  $   ��  2   �  +   8�  +   d�  ,   ��     ��     ��  "   ��  $   �     :�  !   T�  *   v�  "   ��     ��  *   ��  0   �  0   <�  &   m�  3   ��  4   ��  5   ��  )   3�  ,   ]�     ��  D   ��  *   ��  4   �     A�     Y�     w�  3   ��  '   ��  &   ��  #   �  #   7�  %   [�  (   ��  .   ��  9   ��  .   �  0   B�  '   s�  *   ��  .   ��     ��  !   �     4�  (   J�  (   s�  4   ��  4   ��  [   �  ,   b�  4   ��  #   ��  ,   ��  "   �  "   8�  /   [�  /   ��  +   ��  +   ��  -     *   A  <   l  &   �  !   �     �         , 6   D     { 2   � !   � 3   � 0   % &   V B   } #   � .   �        1    N     g 1   �    �    � !   � 3   
    > *   ^    �    � 6   �     � &        9 .   Z    � 4   � 1   � +       4    J "   g    � "   �    �    �    � (       0    L "   k #   � S   � M    (   T "   } I   �    � 4   � (   .	 "   W	    z	    �	    �	    �	 B   �	 `   
 9   q
 '   �
 #   �
    �
 ;    `   K   � D   � v    �   { �    C   � (   �   % /   8 4   h    � 5   �    � (    >   0 "   o 9   � 9   � :    /   A 4   q &   � ;   � 5   	    ? w   V .   � *   � "   ( %   K A   q =   � 5   � 8   ' "   ` )   �    �     �    � /   �    + M   7    �    � !   � =   � >    ,   R 9    +   �    � .   � /   .    ^    b W   o j   � �   2 d   � h   4 h   �            0    =    P h   b    �    � �   � (   �    � L   
    W    m    } e   � ;   � �  , M   ! r   k! H   �! P   '" O   x" *   �" )   �" .   # -   L# *   z#    �# +   �# 9   �# !   $ ?   ;$ E   {$ 0   �$ %   �$ +   % %   D% +   j% #   �% )   �% #   �% (   & *   1&    \&    x&    �&    �& *   �& "   �& <   ' -   J' Q   x' Q   �' .   (    K(    Z( 5   y( -   �( #   �(    ) 4   )    M) 1   d) R   �) Y   �) Z   C* I   �* o   �* �   X+ 2   �+ N   , 8   ^, :   �, ?   �, /   -    B- 9   [- H   �-    �- 3   �- !   -.    O.    b.     r.    �. :   �. >   �.    )/     @/ 6   a/ )   �/ &   �/ #   �/ M   0 >   [0 <   �0 ;   �0 !   1 !   51    W1 '   t1 5   �1 $   �1 :   �1 !   22 $   T2    y2 (   �2 7   �2 :   �2 '   13 #   Y3 L   }3 >   �3 K   	4 /   U4 '   �4 <   �4 J   �4 %   55     [5    |5    �5 
   �5    �5 �   �5    {6 )   �6 w  �6 6   .8    e8    t8    �8    �8 /   �8 H   �8 ?   #9 ;   c9 =   �9 �   �9    :    �: �   �: �  (; �  ? �   �@ �  �A 0   =C X   nC =   �C    D I   D =   fD C   �D    �D    �D    E �   E :   �E 4   F V   BF C   �F �  �F *   �H 9   �H Q   7I h   �I '   �I C   J .   ^J <   �J ,   �J 9   �J    1K *   >K ?   iK 3   �K ,   �K -   
L e   8L    �L !   �L M   �L    M D   )M �   nM B  WN �  �P t  NR *   �T #   �T =   U ,   PU 9   }U 9   �U    �U Q   V @   _V 0   �V b   �V    4W 6   TW    �W    �W 
   �W J   �W 2   X 8   8X /   qX Y   �X 1   �X    -Y /   IY    yY (   �Y ?   �Y 5   �Y 	   (Z !   2Z    TZ <   nZ    �Z %   �Z &   �Z H   [    [[ �   b[ �   6\ t   �\ r   p] �   �] �   �^ .   �_ 1   �_ �   ` �   �` �   �a t   �b $   c G   3c �   {c &   d 1   4d 3   fd ?   �d    �d �   �d $   �e =   �e >   �e #   4f O   Xf /   �f X   �f 3   1g �   eg    �g �   �g �   �h    `i    li    xi K   �i ,   �i _   �i :   _j H   �j @   �j 9   $k 9   ^k 1   �k D   �k 6   l 5   Fl "   |l 9   �l .   �l A   m ^   Jm 2   �m    �m    �m %   n    6n    Tn    cn 8   �n ?   �n :   �n &   6o /   ]o A   �o .   �o .   �o ;   -p #   ip    �p    �p     �p    �p    q    !q D   2q 4   wq (   �q 4   �q %   
r ^   0r 2   �r Q   �r 1   s Q   Fs    �s B   �s F   �s     >t 1   _t 8   �t D   �t 2   u ,   Bu V   ou H   �u 7   v ?   Gv <   �v /   �v 0   �v &   %w #   Lw Q   pw <   �w \   �w Q   \x 5   �x *   �x [   y 8   ky B   �y 5   �y D   z 1   bz T   �z B   �z G   ,{ 9   t{ 0   �{ E   �{ I   %| $   o|    �| F   �| S   �| 9   N} 9   �} 9   �} %   �}    "~ !   B~ ,   d~ 4   �~ -   �~ F   �~ I   ; /   � Q   � ,   � $   4�    Y�    x� E   �� L   р &   � b   E� +   �� R   ԁ �   '�    ��     �� 3   ۂ *   � <   :� 7   w� 2   �� 1   �     �    5�    O�    c� *   |�    ��    �� 2   Ȅ R   ��    N� 6   l� +   �� .   υ [   ��    Z� #   o�    ��    �� !   ͆ 4   � '   $� M   L� ,   ��    Ǉ    ۇ    � $    � !   %�    G�    L�    ^� 1   s� >   ��    � !   � !   %�     G� 2   h� )   ��    ŉ *   �� .   � 2   :� -   m� !   �� "   �� @   �� 5   !�    W�    i� >   �� &   ȋ    �    � /   )� /   Y� $   �� 9   �� *   � 0   �    D�    _�    �    ��    �� &   ō a   �    N� P   h� *   �� !   � 0   � )   7� $   a�    ��    ��    ��    Ώ -   � *   � %   =� /   c� 4   �� '   Ȑ D   � !   5� H   W� &   �� B   Ǒ    
� $   �    @� =   _� >   �� ,   ܒ >   	�    H� V   Z� �   �� 0   5� :   f�    �� L   �� 4   �� 2   3� /   f� 4   �� (   ˕ 8   �� 3   -� >   a� J   �� H   � B   4� N   w� M   Ɨ I   � O   ^� D   �� [   � 9   O� M   �� P   י 1   (� R   Z� N   �� G   �� +   D� >   p� (   �� E   ؛    � A   >� %   �� x   �� %   � >   E� N   �� H   ӝ m   � ?   �� 8   ʞ    � T   "� 1   w�    �� #   �� !   ٟ    �� -   � -   G� )   u� �   �� .   K� /   z� -   �� (   ء D   � �   F� 7   Ӣ /   � )   ;� 7   e� 3   �� ,   ѣ l   ��    k� 	   ��    �� p   �� ,   �    >� C   K� ,   �� +   �� +   � (   � %   =� n   c� I   Ҧ !   �    >�    O� $   a� P   �� .   ק    �    $�    7�    R�    i�    �� W   �� S   �� @   K�    ��    ��    ��    ĩ >   Щ    � "   .� I   Q� D   ��    �    � �   �    ��     �� �   �� M   D� M   �� "   � 4   � )   8� �   b� !   �    �    !� ;   A�    }�    �� '   �� A   ݮ 2   � >   R� D   �� M   ֯ 7   $�    \� -   k� <   �� #   ְ )   �� A   $�    f� M   �� ,   ѱ    �� M   � [   ]� \   �� K   � F   b� )   �� ?   ӳ ,   � ;   @� /   |� b   �� m   � M   }� S   ˵ 3   � I   S� 5   �� ;   Ӷ    �     )� I   J�    �� =   �� S   � ?   <� v   |� 2   �    &� &   E�    l� 6   �� 1   ¹ -   �� 2   "� 2   U� G   �� G   к $   �    =� U   I� 7   �� &   ׻    �� I   � 9   e�    �� 3   �� 6   � @   "�    c�    k� ;   q� 
   ��    �� ,   ׽    �    �    3� 5   O� "   �� L   �� +   �� 0   !� 2   R�    �� 7   ��    ٿ    �� @   
� Q   K� O   �� 0   ��     � 0   ?� <   p�    �� Q   �� =   �    U� %   d� U   �� $   ��    � Z   #�    ~�    �� 9   ��    �� 1   ��    '� .   D� C   s�    ��    ��    �� 9   �� 4   #�    X� /   x� E   �� :   �� '   )� K   Q� &   �� Q   �� F   � 6   ]� E   �� H   ��    #� H   9� 4   �� d   �� -   � (   J�    s�    �� e   �� =   � ?   ?� 6   �    �� -   �� +   � I   /� E   y� V   �� U   � h   l� *   �� 9    � +   :� a   f� B   �� /   � 3   ;� 4   o� L   �� Q   �� '   C� <   k� 0   ��    �� '   �� .   � 2   K� <   ~� A   �� :   �� 8   8�    q� #   �� +   �� 4   �� *   � 1   @� =   r� 8   �� -   �� =   � <   U� 6   �� +   �� >   �� =   4� >   r� -   �� 2   ��    � P   0� >   �� @   ��    � '   � '   E� B   m� *   �� /   �� ,   � (   8� 4   a� 1   �� 6   �� A   �� H   A� @   �� -   �� 7   �� ?   1� "   q� (   ��    �� +   �� 1   	� 8   ;� 8   t� l   �� 2   � C   M� ,   �� 3   �� 2   �� 2   %� G   X� A   �� =   �� 8    � 9   Y� ;   �� Q   �� (   !� 9   J� *   �� 8   �� '   �� J   � ?   [� c   �� :   �� M   :� <   �� ;   �� N   � 1   P� C   �� -   �� ,   �� ,   !� 4   N� 3   ��    �� (   �� /   �� >   .� +   m� 2   ��    ��    �� 4   �� &   4� )   [� "   �� *   ��    �� 8   �� 5   &� 2   \�    �� '   �� -   �� &   � .   (�    W�    q�    �� 1   �� "   �� /   �� 1   )� 3   [� d   �� W   �� :   L� $   �� R   ��    �� 1   � +   :� +   f�    ��     ��    ��    �� b   �� n   S� E   �� ,   � %   5�    [� K   v� w   ��    �      p  F   h  8  F  ;           '  �   �          "              �  g  =          o  }  a  �  a   �  {      �  �  /    M  �   E  s       r      n  �      �  "              ~  K  �          M      �  �              W  �      �   
              k  )  �    �  �      �      �        f  �  !  �   1  �          m  *   �  �  �   H  �      �      �           ,                �  >      �  �   �  1  k  �    �   �                   �   �          �  +  �   �     Y  �  *  C  �      �  �  �  �  �  *            Y           �  I  w  �  �  �  ;    &  �  V  c  �  �  3      \           �  �  �        �  �  �  �           l  �  �      �      �     �  �  3      <  4  �  �  	    {  d                �         �      �  H   �   U      l           �   ;  �                =  W  Q  �  �  v      �      M   �      1   �  �   :    �  ^   �  �   �     n  �  �      e       �   O  h   �   �  �      S       |   �  �   !   �  �      �     �      q  �      �  r  _    +  �  x   O   d  <      5            �  �  [  �            �          �      W  �      �   �      �      #   E  (      $  2       D     G  2  S  �  �  b          g      �  }  B   S    �  �  ?   c  X  �  �   �      �     =  �  �      O  �  �  	   �          �  �                   %      |    �  &  �          y  �  @  �  �   N  �       �  _  x  �  g   �  6   �       0  �  }  �         K     S  �    �  �       �    �  �   >     �  k  {   �  �   .                        �   ?      �                       )      �      a  V      �   7  �   �      z   �  �        /       g    �  �  �  �  Z              R          �  �  �  (  �    V  ,           �         $  9    �  �   �     9  �  T  �  m  M  @  �   �  ;  j  �       �   %   �      �          y     `         �      �   P  �      r  �  �   �      �          �  �        q   �  1  o  x            #  �   �        �  �  �       �  �   �  �    u  ]  �   .          �  �      V      �   �  �  *  -  j     �   �      �  �  �   \  |          �  �          6  �      �                �   q  �      �  �          L   u  F        �  �  -   �  f  �     C  w  �  F  |  :               �  �      .        (   <  c  �       �   &   N   �  �  ~  �   �       .       A  [      �       �      �  :  �        �            e  }       �   �  ,  s    �  r   C  t  �  P  �        �  �  �              �                   �  Y  "  v  �  5  �  a  T  6  �  �        _   G  �    �      w  �     z  D  �   �      �   �  �  ~  z      )  �  �       �  �   ^  �      �                   /  ?       ^  �       �   �   @   �  J   �  �    u   �   �  �      �  �      �  �      K   N  �  v   �        9   �  Q  +          �  �   �  �   2      �  �  �      J    B  �  �          d  �  �  �          l  �     �  �    p   O  "  �  e  K  �  �     i      �      E        �  �  �  ]      �   N  �  �      Q   =       2  �         �   0          �       C   �   �             x  �   '  �      �  J      n   &  >  �   �  �                4          �       �  8         7  U   �  G   z      �   �  4        [     �   :  R      X   d   �        �  \      �          �        Z   %  �       �   I      �  �          v  �       8  �   �  �  [  t  4   �  `      �   �  �  J  u  A  �       �  @      �      -      ]   �  �      �  �      �      B  \  �     ^  
  �   �           L  �  R  �      �   �  %      Q  �  
   �     �          w      ,  �  X    �  �  �      #  I       �  �   I  ]  U                        {  �       t  �      t       �  �   �          �  �  �    �  �         �      �           �          	             U  ~   (  0       �      l  �  �   �      5          �  �  �   h  �   �  `  L  q      �      �  �  )        A   �   �      s  �       f   X  �  �  y        �      P  E   �      G  �   �      �  !  �  f      D      !  h      �    b  �   n  �        �   �   7  H  m  9  A  �  �  Z  p  #  B  '       �   7       �   �  '  >  �           �        �   �      �  i  �   �  �  p        `           �  8  �   �  �     0      +  �  �        �   k   �  �        j      �  D      i   �  �  c   �   y      e  -  
      j   o   �   T   5       �  �   �  �  �      �   �  3  �  b   �       3          �  i  R   H  �                 �  �            m   6  �       �  s  �          �      �          �       $   $      �   �  �  /       �  Z      �  P       �           ?            W   �  o  �   T  <     �  Y          b      �   L      �  	    _   
 packages' pending triggers which are or may be unresolvable:
 
Configuration file '%s', does not exist on system.
Installing new config file as you requested.
 
Debugging options can be mixed using bitwise-or.
Note that the meanings and values are subject to change.
 
WARNING - use of options marked [!] can seriously damage your installation.
Forcing options marked [*] are enabled by default.
      Version in package is the same as at last installation.
      not a plain file          %.255s
    What would you like to do about it ?  Your options are:
    Y or I  : install the package maintainer's version
    N or O  : keep your currently-installed version
      D     : show the differences between the versions
      Z     : start a shell to examine the situation
   %.250s (version %.250s) is present and %s.
   %.250s (version %.250s) is to be installed.
   %.250s is %s.
   %.250s is installed, but is version %.250s.
   %.250s is not installed.
   %.250s is to be deconfigured.
   %.250s is to be installed, but is version %.250s.
   %.250s is to be removed.
   %.250s is unpacked, but has never been configured.
   %.250s is unpacked, but is version %.250s.
   %.250s latest configured version is %.250s.
   %.250s provides %.250s and is present and %s.
   %.250s provides %.250s and is to be installed.
   %.250s provides %.250s but is %s.
   %.250s provides %.250s but is to be deconfigured.
   %.250s provides %.250s but is to be removed.
   %s (%s) provides %s.
   -?, --help                       Show this help message.
      --version                    Show the version.

   Package %s awaits trigger processing.
   Package %s is not configured yet.
   Package %s is not installed.
   Package %s is to be removed.
   Package %s which provides %s awaits trigger processing.
   Package %s which provides %s is not configured yet.
   Package %s which provides %s is not installed.
   Package %s which provides %s is to be removed.
   Version of %s on system is %s.
   Version of %s to be configured is %s.
   link currently absent   link currently points to %s   slave %s: %s  %7jd bytes, %5d lines   %c  %-20.127s %.127s
  %d in %s:   %d package, from the following section:  %d packages, from the following sections:  %s (%jd bytes)
  %s (%s) breaks %s and is %s.
  %s (not a plain file)
  ==> Keeping old config file as default.
  ==> Package distributor has shipped an updated version.
  ==> Using current old file as you requested.
  ==> Using new config file as default.
  ==> Using new file as you requested.
  Package %s: part(s)   The default action is to install the new version.
  The default action is to keep your current version.
  and   depends on   new debian package, version %d.%d.
 size %jd bytes: control archive=%jd bytes.
  old debian package, version %d.%d.
 size %jd bytes: control archive=%jd, main archive=%jd.
 %d expected program not found in PATH or not executable
%s %d expected programs not found in PATH or not executable
%s %d file or directory currently installed.)
 %d files and directories currently installed.)
 %d requested control component is missing %d requested control components are missing %s %s (Multi-Arch: %s) is not co-installable with %s which has multiple installed instances %s (subprocess): %s
 %s - priority %d %s breaks %s %s conflicts with %s %s corrupt: %s %s debugging option, --debug=<octal> or -D<octal>:

 Number  Ref. in source   Description
 %s depends on %s %s enhances %s %s forcing options - control behaviour when problems found:
  warn but continue:  --force-<thing>,<thing>,...
  stop with error:    --refuse-<thing>,<thing>,... | --no-force-<thing>,...
 Forcing things:
 %s is locked by another process %s is missing %s is not properly installed; ignoring any dependencies on it %s pre-depends on %s %s recommends %s %s suggests %s %s/%s has been changed (manually or by a script); switching to manual updates only %s/%s is dangling; it will be updated with best choice %s:
    Part format version:            %d.%d
    Part of package:                %s
        ... version:                %s
        ... architecture:           %s
        ... MD5 checksum:           %s
        ... length:                 %jd bytes
        ... split every:            %jd bytes
    Part number:                    %d/%d
    Part length:                    %jd bytes
    Part offset:                    %jd bytes
    Part file size (used portion):  %jd bytes

 %s: conffile '%.250s' is not a plain file or symlink (= '%s') %s: conffile '%.250s' resolves to degenerate filename
 ('%s' is a symlink to '%s') %s: config file '%s' is a circular link
 (= '%s') %s: dependency problems, but configuring anyway as you requested:
%s %s: dependency problems, but removing anyway as you requested:
%s %s: error binding input to bzip2 stream %s: error binding input to gzip stream %s: error binding output to bzip2 stream %s: error binding output to gzip stream %s: error while cleaning up:
 %s
 %s: error: %s
 %s: failed to link '%.250s' to '%.250s': %s %s: failed to remove '%.250s' (before overwrite): %s %s: failed to remove '%.250s': %s %s: failed to remove old backup '%.250s': %s %s: failed to remove old distributed version '%.250s': %s %s: failed to rename '%.250s' to '%.250s': %s %s: internal bzip2 read error %s: internal bzip2 read error: '%s' %s: internal bzip2 write error %s: internal bzip2 write error: '%s' %s: internal gzip read error %s: internal gzip read error: '%s' %s: internal gzip write error %s: internal gzip write error: %s %s: internal gzip write error: '%s' %s: lzma close error %s: lzma error: %s %s: lzma read error %s: lzma write error %s: outside error context, aborting:
 %s
 %s: pass-through copy error: %s %s: too many nested errors during error recovery!!
 %s: unable to open %s for hash: %s %s: unable to readlink conffile '%s'
 (= '%s'): %s %s: unable to stat config file '%s'
 (= '%s'): %s %s: unrecoverable fatal error, aborting:
 %s
 %s: warning: %s
 %s:%s:%d:%s: internal error: %s
 '%.255s' contains no control component '%.255s' '%.255s' is not a debian format archive '%.50s' is not allowed for %s '%s' clashes with '%s' '%s' contains user-defined Priority value '%s' '%s' does not take a value '%s' field, invalid package name '%.255s': %s '%s' field, missing architecture name, or garbage where architecture name expected '%s' field, missing package name, or garbage where package name expected '%s' field, reference to '%.255s':
 '%c' is obsolete, use '%c=' or '%c%c' instead '%s' field, reference to '%.255s':
 bad version relationship %c%c '%s' field, reference to '%.255s':
 implicit exact match on version number, suggest using '=' instead '%s' field, reference to '%.255s':
 version value starts with non-alphanumeric, suggest adding a space '%s' field, reference to '%.255s': error in version '%s' field, reference to '%.255s': invalid architecture name '%.255s': %s '%s' field, reference to '%.255s': version contains '%c' '%s' field, reference to '%.255s': version unterminated '%s' field, syntax error after reference to package '%.255s' '%s' is not a valid architecture name: %s '%s' needs a value '%s' not found in PATH or not executable (Noting disappearance of %s, which has been completely replaced.)
 (Reading database ...  (no 'control' file in control archive!)
 (no description available) (total %jd bytes)
 , core dumped -%c option does not take a value -%c option takes a value --%s --pending does not take any non-option arguments --%s --recursive needs at least one path argument --%s needs <name> --%s needs <name> <path> --%s needs a .deb filename argument --%s needs a <directory> argument --%s needs a <file> argument --%s needs a single argument --%s needs a target directory.
Perhaps you should be using dpkg --install ? --%s needs a valid package name but '%.250s' is not: %s --%s needs at least one package archive file argument --%s needs at least one package name argument --%s needs four arguments --%s option does not take a value --%s option takes a value --%s requires a positive octal argument --%s requires one or more part file arguments --%s takes at most two arguments --%s takes at most two arguments (.deb and directory) --%s takes exactly one argument --%s takes exactly two arguments --%s takes no arguments --%s takes one package name argument --%s takes only one argument (.deb filename) --auto requires exactly one part file argument --auto requires the use of the --output option --compare-versions bad relation --compare-versions takes three arguments: <version> <relation> <version> --install needs <link> <name> <path> <priority> --search needs at least one file name pattern argument --slave needs <link> <name> <path> --slave only allowed with --install --split needs a source filename argument --split takes at most a source filename and destination prefix --update given but %s does not exist --update is useless for --remove ... it looks like that went OK .../%s ; however:
 <compress> from tar -cf <deb> is the filename of a Debian format archive.
<cfile> is the name of an administrative file component.
<cfield> is the name of a field in the main 'control' file.

 <decompress> <link> and <path> can't be the same <link> is the symlink pointing to %s/<name>.
  (e.g. /usr/bin/pager)
<name> is the master name for this link group.
  (e.g. pager)
<path> is the location of one of the alternative target files.
  (e.g. /usr/bin/less)
<priority> is an integer; options with higher numbers have higher priority in
  automatic mode.

 <package status and progress file descriptor> <standard error> <standard input> <standard output> Adding '%s'
 Allow installation of conflicting packages Alternative %s unchanged because choice %s is not available. Always install missing config files Always use the new config files, don't prompt Always use the old config files, don't prompt Another process has locked the database for writing, and might currently be
modifying it, some of the following problems might just be due to that.
 Architecture Authenticating %s ...
 Commands:
  --check-supported                Check if the running dpkg supports triggers.

 Commands:
  --install <link> <name> <path> <priority>
    [--slave <link> <name> <path>] ...
                           add a group of alternatives to the system.
  --remove <name> <path>   remove <path> from the <name> group alternative.
  --remove-all <name>      remove <name> group from the alternatives system.
  --auto <name>            switch the master link <name> to automatic mode.
  --display <name>         display information about the <name> group.
  --query <name>           machine parseable version of --display <name>.
  --list <name>            display all targets of the <name> group.
  --get-selections         list master alternative names and their status.
  --set-selections         read alternative status from standard input.
  --config <name>          show alternatives for the <name> group and ask the
                           user to select which one to use.
  --set <name> <path>      set <path> as alternative for <name>.
  --all                    call --config on all alternatives.

 Commands:
  -s|--split <file> [<prefix>]     Split an archive.
  -j|--join <part> <part> ...      Join parts together.
  -I|--info <part> ...             Display info about a part.
  -a|--auto -o <complete> <part>   Auto-accumulate parts.
  -l|--listq                       List unmatched pieces.
  -d|--discard [<filename> ...]    Discard unmatched pieces.

 Commands:
  [--add] <file>           add a diversion.
  --remove <file>          remove the diversion.
  --list [<glob-pattern>]  show file diversions.
  --listpackage <file>     show what package diverts the file.
  --truename <file>        return the diverted file.

 Comparison operators for --compare-versions are:
  lt le eq ne ge gt       (treat empty version as earlier than any version);
  lt-nl le-nl ge-nl gt-nl (treat empty version as later than any version);
  < << <= = >= >> >       (only for compatibility with control file syntax).

 Configure any package which may help this one Debian %s package management program query tool version %s.
 Debian %s package trigger utility version %s.
 Debian %s version %s.
 Debian '%s' package archive backend version %s.
 Debian '%s' package management program version %s.
 Debian '%s' package split/join tool; version %s.
 Deleted %s.
 Dependencies and conflicts Description Desired=Unknown/Install/Remove/Purge/Hold
| Status=Not/Inst/Conf-files/Unpacked/halF-conf/Half-inst/trig-aWait/Trig-pend
|/ Err?=(none)/Reinst-required (Status,Err: uppercase=bad)
 Do not perform safe I/O operations when unpacking Errors were encountered while processing:
 Exit status:
  0 = ok
  1 = with --auto, file is not a part
  2 = trouble
 File '%.250s' is not part of a multipart archive.
 Format syntax:
  A format is a string that will be output for each package. The format
  can include the standard escape sequences \n (newline), \r (carriage
  return) or \\ (plain backslash). Package information can be included
  by inserting variable references to package fields using the ${var[;width]}
  syntax. Fields will be right-aligned unless the width is negative in which
  case left alignment will be used.
 Generally helpful progress information Ignoring request to remove shared diversion '%s'.
 Ignoring request to rename file '%s' owned by diverting package '%s'
 Information about %d package was updated.
 Information about %d packages was updated.
 Insane amounts of drivel Install a package even if it fails authenticity check Install even if it would break another package Installing new version of config file %s ...
 Invocation and status of maintainer scripts Junk files left around in the depot directory:
 Leaving '%s'
 Lots of dependencies/conflicts output Lots of drivel about eg the dpkg/info directory Lots of output for each configuration file Lots of output for each file processed Lots of output regarding triggers More than one copy of package %s has been unpacked
 in this run !  Only configuring it once.
 Name No diversion '%s', none removed.
 Note: root's PATH should usually contain /usr/local/sbin, /usr/sbin and /sbin Nothing to configure. Offer to replace config files with no new versions Options:
  --admindir=<directory>           Use <directory> instead of %s.
  --load-avail                     Use available file on --show and --list.
  -f|--showformat=<format>         Use alternative format for --show.

 Options:
  --altdir <directory>     change the alternatives directory.
  --admindir <directory>   change the administrative directory.
  --log <file>             change the log file.
  --force                  allow replacing files with alternative links.
  --skip-auto              skip prompt for alternatives correctly configured
                           in automatic mode (relevant for --config only)
  --verbose                verbose operation, more output.
  --quiet                  quiet operation, minimal output.
  --help                   show this help message.
  --version                show the version.
 Options:
  --depotdir <directory>           Use <directory> instead of %s/%s.
  -S|--partsize <size>             In KiB, for -s (default is 450).
  -o|--output <file>               Filename, for -j (default is
                                     <package>_<version>_<arch>.deb).
  -Q|--npquiet                     Be quiet when -a is not a part.
  --msdos                          Generate 8.3 filenames.

 Options:
  --package <package>      name of the package whose copy of <file> will not
                             be diverted.
  --local                  all packages' versions are diverted.
  --divert <divert-to>     the name used by other packages' versions.
  --rename                 actually move the file aside (or back).
  --admindir <directory>   set the directory with the diversions file.
  --test                   don't do anything, just demonstrate.
  --quiet                  quiet operation, minimal output.
  --help                   show this help message.
  --version                show the version.

 Output for each configuration file Output for each file processed Overwrite a diverted file with an undiverted version Overwrite a file from one package with another Overwrite one package's directory with another's file PATH is missing important programs, problems likely PATH is not set Package %s is on hold, not touching it.  Use --force-hold to override.
 Package %s listed more than once, only processing once.
 Package '%s' does not contain any files (!)
 Package which in state not-installed has conffiles, forgetting them Packages not yet reassembled:
 Part %d of package %s filed (still want  Path Pre-Depends field Priority Process even packages with wrong or no architecture Process even packages with wrong versions Process incidental packages even when on hold Processing was halted because there were too many errors.
 Putting package %s together from %d part:  Putting package %s together from %d parts:  Recorded info about %s from %s.
 Remove an essential package Remove packages which require installation Removing '%s'
 Replace a package with a lower version Replacing available packages info, using %s.
 Selecting previously unselected package %s.
 Selection Set all force options Setting up %s (%s) ...
 Silly amounts of output regarding triggers Skip invalid line: %s Skip unknown alternative %s. Skipping unselected package %s.
 Splitting package %s into %d part:  Splitting package %s into %d parts:  Status The following packages are awaiting processing of triggers that they
have activated in other packages.  This processing can be requested using
dselect or dpkg --configure --pending (or dpkg --triggers-only):
 The following packages are in a mess due to serious problems during
installation.  They must be reinstalled for them (and any packages
that depend on them) to function properly:
 The following packages are missing the list control file in the
database, they need to be reinstalled:
 The following packages are missing the md5sums control file in the
database, they need to be reinstalled:
 The following packages are only half configured, probably due to problems
configuring them the first time.  The configuration should be retried using
dpkg --configure <package> or the configure menu option in dselect:
 The following packages are only half installed, due to problems during
installation.  The installation can probably be completed by retrying it;
the packages can be removed using dselect or dpkg --remove:
 The following packages do not have an architecture:
 The following packages have an illegal architecture:
 The following packages have an unknown foreign architecture, which will
cause dependency issues on front-ends. This can be fixed by registering
the foreign architecture with dpkg --add-architecture:
 The following packages have been triggered, but the trigger processing
has not yet been done.  Trigger processing can be requested using
dselect or dpkg --configure --pending (or dpkg --triggers-only):
 The following packages have been unpacked but not yet configured.
They must be configured using dpkg --configure or the configure
menu option in dselect for them to work:
 There is %d choice for the alternative %s (providing %s). There are %d choices for the alternative %s (providing %s). There is no program which provides %s. There is only one alternative in link group %s (providing %s): %s This is free software; see the GNU General Public License version 2 or
later for copying conditions. There is NO warranty.
 Trigger activation and processing Try to (de)install things even when not root Turn all dependency problems into warnings Turn dependency version problems into warnings Type 'exit' when you're done.
 Type dpkg-deb --help for help about manipulating *.deb files;
Type dpkg --help for help about installing and deinstalling packages. Type dpkg-split --help for help. Type dpkg-trigger --help for help about this utility. Updating available packages info, using %s.
 Usage: %s [<option> ...] <command>

 Usage: %s [<options> ...] <trigger-name>
       %s [<options> ...] <command>

 Use --help for help about diverting files. Use --help for help about overriding file stat information. Use --help for help about querying packages. Use dpkg --info (= dpkg-deb --info) to examine archive files,
and dpkg --contents (= dpkg-deb --contents) to list their contents.
 Version Version of dpkg with working %s support not yet configured.
 Please use 'dpkg --configure dpkg', and then try again.
 When adding, default is --local and --divert <original>.distrib.
When removing, --package or --local and --divert must match if specified.
Package preinst/postrm scripts should always specify --package and --divert.
 [default=N] [default=Y] [no default] admindir must be inside instdir for dpkg to work properly also configuring '%s' (required by '%s') alternative %s (part of link group %s) doesn't exist; removing from list of alternatives alternative %s can't be master: it is a slave of %s alternative %s can't be slave of %s: it is a master alternative alternative %s can't be slave of %s: it is a slave of %s alternative %s for %s not registered; not removing alternative %s for %s not registered; not setting alternative link %s is already managed by %s alternative link %s is already managed by %s (slave of %s) alternative link is not absolute as it should be: %s alternative name (%s) must not contain '/' and spaces alternative path %s doesn't exist alternative path is not absolute as it should be: %s alternatives ('|') not allowed in %s field ambiguous package name '%s' with more than one installed instance an override for '%s' already exists, but --force specified so will be ignored an override for '%s' already exists; aborting any diversion of %s any diversion of %s to %s ar member name '%s' length too long ar member size %jd too large architecture<unknown> architecture '%s' is illegal: %s architecture '%s' is reserved and cannot be added archive '%.250s' contains two control members, giving up archive contained object '%.255s' of unknown type 0x%x archive control member size archive has invalid format version: %s archive has malformatted control member size '%s' archive has no newlines in header archive information header member archive is format version %d.%d; get a newer dpkg-deb archive magic version number archive member header archive part numbers archive part offset archive parts number archive total size auto mode automatic updates of %s/%s are disabled; leaving it alone awaiting trigger processing by another package blank line in value of field '%.*s' broken due to failed removal or installation broken due to postinst failure bulk available update requires write access to dpkg status area can't install unknown choice %s can't mmap package info file '%.255s' can't remove old postrm script can't stat package info file '%.255s' cannot access archive cannot append ar member file (%s) to '%s': %s cannot append split package part '%s' to '%s': %s cannot append to '%s' cannot close decompressor pipe cannot compute MD5 hash for file '%s': %s cannot compute MD5 hash for tar file '%.255s': %s cannot copy '%s' into archive '%s': %s cannot copy '%s' to '%s': %s cannot copy archive member from '%s' to decompressor pipe: %s cannot copy extracted data for '%.255s' to '%.255s': %s cannot divert file '%s' to itself cannot extract control file '%s' from '%s': %s cannot extract split package part '%s': %s cannot open '%.255s' (in '%.255s') cannot open archive part file '%.250s' cannot read info directory cannot remove '%.250s' cannot remove architecture '%s' currently in use by the database cannot remove non-foreign architecture '%s' cannot remove old backup config file '%.250s' (of '%.250s') cannot remove old config file '%.250s' (= '%.250s') cannot remove old files list cannot rename '%s' to '%s' cannot satisfy pre-dependencies for %.250s (wanted due to %.250s) cannot scan directory '%.255s' cannot scan updates directory '%.255s' cannot see how to satisfy pre-dependency:
 %s cannot skip archive control member from '%s': %s cannot skip archive member from '%s': %s cannot skip file '%.255s' (replaced or excluded?) from pipe: %s cannot skip padding for file '%.255s': %s cannot skip split package header for '%s': %s cannot stat '%.255s' (in '%.255s') cannot stat file '%s' cannot zap possible trailing zeros from dpkg-deb: %s character '%c' not allowed (only letters, digits and characters '%s') compressed data is corrupt compressing control member conffile '%.250s' does not appear in package conffile '%.250s' is not stattable conffile '%s' is not a plain file conffile difference visualizer conffile filename '%s' contains trailing white spaces conffile name '%s' is duplicated conffile shell configuration error: %s:%d: %s conflicting actions -%c (--%s) and -%c (--%s) conflicting diversions involving '%.250s' or '%.250s' conflicting packages - not installing %.250s considering deconfiguration of %s, which would be broken by %s ... considering deconfiguration of essential
 package %s, to enable %s considering removing %s in favour of %s ... control directory has bad permissions %03lo (must be >=0755 and <=0775) control directory is not a directory control file '%s' does not exist control file contains %c control member corrupt info database format file '%s' corrupted filesystem tarfile - corrupted package archive could not open log '%s': %s could not stat old file '%.250s' so not deleting it: %s couldn't open '%i' for stream current alternative %s is unknown, switching to %s for link group %s cycle found while processing triggers:
 chain of packages whose triggers are or may be responsible: data member decompressing archive member dependency problems - leaving unconfigured dependency problems - not removing dependency problems prevent configuration of %s:
%s dependency problems prevent removal of %s:
%s deprecated compression type '%s'; use xz instead discarding obsolete slave link %s (%s) diversion by %s from: %s
 diversion by %s to: %s
 diversion of %s by %s diversion of %s to %s by %s divert-to may not contain newlines diverted by %s to: %s
 done
 downgrading %.250s from %.250s to %.250s dpkg not recorded as installed, cannot check for %s support!
 dpkg status database dpkg-deb: building an unknown package in '%s'.
 dpkg-deb: building package '%s' in '%s'.
 duplicate awaited trigger package '%.255s' duplicate file trigger interest for filename '%.250s' and package '%.250s' duplicate path %s duplicate pending trigger '%.255s' duplicate slave link %s duplicate slave name %s duplicate value for '%s' field duplicate value for user-defined field '%.*s' empty file details field '%s' empty string from fgets reading conffiles empty trigger names are not permitted empty value for %s epoch epoch in version is negative epoch in version is not number epoch in version is too big error error checking '%s' error closing %.250s error closing configuration file '%.255s' error closing files list file for package '%.250s' error closing find's pipe error closing/writing '%.255s' error creating device '%.255s' error creating directory '%.255s' error creating hard link '%.255s' error creating new backup file '%s' error creating pipe '%.255s' error creating symbolic link '%.255s' error ensuring '%.250s' doesn't exist error executing hook '%s', exit code %d error formatting string into varbuf variable error in show format: %s error installing new file '%s' error opening conffiles file error opening configuration directory '%s' error reading %.250s error reading %s from file %.255s error reading conffiles file error reading dpkg-deb tar output error reading find's pipe error reading from dpkg-deb pipe error reading triggers deferred file '%.250s' error removing old backup file '%s' error setting ownership of '%.255s' error setting ownership of symlink '%.255s' error setting permissions of '%.255s' error setting timestamps of '%.255s' error trying to open %.250s error un-catching signal %s: %s
 error while writing '%s' error writing '%s' error writing to '%s' error writing to architecture list error writing to stderr, discovered before conffile prompt failed to allocate memory failed to allocate memory for new entry in list of failed packages: %s failed to chdir to '%.255s' failed to chdir to directory failed to chdir to directory after creating it failed to chroot to '%.250s' failed to close after read: '%.255s' failed to create directory failed to create pipe failed to dup for fd %d failed to dup for std%s failed to fdopen find's pipe failed to fstat ar member file (%s) failed to fstat archive failed to fstat diversions file failed to fstat statoverride file failed to make temporary file (%s) failed to open configuration file '%.255s' for reading: %s failed to open diversions file failed to open package info file '%.255s' for reading failed to open statoverride file failed to open trigger interest list file '%.250s' failed to read failed to read '%.255s' (in '%.255s') failed to read archive '%.255s' failed to remove incorporated update file %.255s failed to remove my own update file %.255s failed to rewind temporary file (%s) failed to rewind trigger interest file '%.250s' failed to seek failed to stat (dereference) existing symlink '%.250s' failed to stat (dereference) proposed new symlink target '%.250s' for symlink '%.250s' failed to stat temporary file (%s) failed to unlink temporary file (%s), %s failed to write failed to write %s database record about '%.50s' to '%.250s' failed to write details of '%.50s' to '%.250s' failed to write filename to tar pipe (%s) fgets gave an empty string from '%.250s' field name '%.*s' must be followed by colon field width is out of range file '%.250s' has invalid format version: %s file '%.250s' is corrupt - %.250s missing file '%.250s' is corrupt - bad MD5 checksum '%.250s' file '%.250s' is corrupt - bad archive header magic file '%.250s' is corrupt - bad archive part number file '%.250s' is corrupt - bad digit (code %d) in %s file '%.250s' is corrupt - bad magic at end of first header file '%.250s' is corrupt - bad magic at end of second header file '%.250s' is corrupt - bad number of archive parts file '%.250s' is corrupt - bad padding character (code %d) file '%.250s' is corrupt - missing newline after %.250s file '%.250s' is corrupt - no slash between archive part numbers file '%.250s' is corrupt - nulls in info section file '%.250s' is corrupt - second member is not data member file '%.250s' is corrupt - size is wrong for quoted part number file '%.250s' is corrupt - too short file '%.250s' is corrupt - wrong number of parts for quoted sizes file '%.250s' is format version %d.%d; get a newer dpkg-split file '%.250s' is not a debian binary archive (try dpkg-split?) file '%.250s' is not an archive part file '%s' is corrupt; out of range integer in %s file '%s' is not an archive part
 file details field '%s' not allowed in status file file format not recognized file looks like it might be an archive which has been
 corrupted by being downloaded in ASCII mode file may not contain newlines file triggers record mentions illegal package name '%.250s' (for interest in file '%.250s'): %.250s filename "%s" is not absolute files '%.250s' and '%.250s' are not parts of the same file files list file for package '%.250s' contains empty filename files list file for package '%.250s' is missing final newline files list file for package '%.250s' missing; assuming package has no files currently installed files list for package '%.250s' is not a regular file find for --recursive returned unhandled error %i find for dpkg --recursive forcing reinstallation of alternative %s because link group %s is broken foreign/allowed/same/no in quadstate field fork failed format version followed by junk format version has no dot format version number format version with empty major component format version with empty minor component generated corrupt ar header for '%s' header is too long, making part too long; the package name or version
numbers must be extraordinarily long, or something; giving up ignoring breakage, may proceed anyway! ignoring conflict, may proceed anyway! ignoring dependency problem with %s:
%s ignoring pre-dependency problem! ignoring request to remove %.250s which isn't installed ignoring request to remove %.250s, only the config
 files of which are on the system; use --purge to remove them too illegal architecture name in specifier '%s:%s': %s illegal awaited package name '%.250s': %.250s illegal package name at line %d: %.250s illegal package name in awaited trigger '%.255s': %s illegal package name in specifier '%s%s%s': %s illegal pending trigger name '%.255s': %s info database format (%d) is bogus or too new; try getting a newer dpkg installation of %.250s installed installed %s script installing %.250s would break %.250s, and
 deconfiguration is not permitted (--auto-deconfigure might help) installing %.250s would break existing software internal error (bug) invalid character '%c' in archive '%.250s' member '%.16s' size invalid character '%c' in field width invalid character in revision number invalid character in version number invalid compressor parameters: %s invalid integer for --%s: '%.250s' invalid or unknown syntax in trigger name '%.250s' (in trigger interests for package '%.250s') invalid package name '%.250s' in triggers deferred file '%.250s' invalid package name (%.250s) invalid status junk after %s line not terminated while trying to read %s line too long or not terminated while trying to read %s link %s is both primary and slave local diversion from: %s
 local diversion of %s local diversion of %s to %s local diversion to: %s
 locally diverted to: %s
 long filenames maintainer script '%.50s' has bad permissions %03lo (must be >=0555 and <=0775) maintainer script '%.50s' is not a plain file or symlink maintainer script '%.50s' is not stattable malloc failed (%zu bytes) manual mode master file master link may have trouble removing %s, as it provides %s ... may not be empty string memory usage limit reached mismatch on divert-to
  when removing '%s'
  found '%s' mismatch on package
  when removing '%s'
  found '%s' missing %s missing closing brace mixed non-coinstallable and coinstallable package instances present; most probably due to an upgrade from an unofficial dpkg multi-arch multiple Conflicts and Replaces multiple non-coinstallable package instances present; most probably due to an upgrade from an unofficial dpkg multiple statoverrides present for file '%.250s' must be called from a maintainer script (or with a --by-package option) must start with an alphanumeric must start with an alphanumeric character name %s is both primary and slave need --display, --query, --list, --get-selections, --config, --set, --set-selections, --install, --remove, --all, --remove-all or --auto need an action option new %s script newline in field name '%.*s' newlines prohibited in update-alternatives files (%s) no alternatives for %s no override present no package information in '%.255s' no package named '%s' is installed, cannot configure no packages found matching %s no path found matching pattern %s no, %s is essential, will not deconfigure
 it in order to enable %s no, cannot proceed with %s (--auto-deconfigure will help):
%s not checking contents of control area not installed not installed but configs remain not removing %s since it's not a symlink not replacing %s with a link nothing after colon in version number null package name in --%s comma-separated list '%.250s' number of archive parts obsolete '--%s' option; unavailable packages are automatically cleaned up obsolete option '--%s'; please use '--%s' instead old %s script old conffile '%.250s' was an empty directory (and has now been deleted) old file '%.250s' is the same as several new files! (both '%.250s' and '%.250s') old version of package has overly-long info file name starting '%.250s' open component '%.255s' (in %.255s) failed in an unexpected way operation requires read/write access to dpkg status area out of memory for new cleanup entry out of memory for new cleanup entry with many arguments out of memory for new error context overriding problem because --force enabled: package %.250s is already installed and configured package %.250s is not ready for configuration
 cannot configure (current status '%.250s') package %.250s is not ready for trigger processing
 (current status '%.250s' with no pending triggers) package %s %s cannot be configured because %s is at a different version (%s) package %s cannot be configured because %s is not ready (current status '%s') package %s contained list as info file package %s requires reinstallation, but will remove anyway as you requested package %s requires reinstallation, will not remove package %s was on hold, processing it anyway as you requested package '%s' is not available package '%s' is not installed package '%s' is not installed and no information is available package architecture package architecture (%s) does not match system (%s) package contains overly-long control info file name (starting '%.50s') package control info contained directory '%.250s' package control info rmdir of '%.250s' didn't say not a dir package control information extraction package diverts others to: %s
 package field value extraction package file MD5 checksum package filesystem archive extraction package has field '%s' but is architecture all package has field '%s' but is missing architecture package has status %s but triggers are awaited package has status %s but triggers are pending package has status triggers-awaited but no triggers awaited package has status triggers-pending but no triggers pending package may not contain newlines package name package name has characters that aren't lowercase alphanums or '-+.' package not in database at line %d: %.250s package signature verification package version number parsing file '%.255s' near line %d package '%.255s':
 %.255s parsing file '%.255s' near line %d:
 %.255s part %d is missing part file '%.250s' is not a plain file part size is far too large or is not positive part size must be at least %d KiB (to allow for header) passed
 paste pre-dependency problem - not installing %.250s priority priority must be an integer priority of %s is out of range: %s priority of %s: %s read error in %.250s read error in '%.250s' read error in configuration file '%.255s' read error on standard input read error on stdin at conffile prompt reading files list for package '%.250s' reading package info file '%.255s' reading statoverride file '%.250s' reassembled package file regarding %s containing %s, pre-dependency problem:
%s regarding %s containing %s:
%s removal of %.250s removing architecture '%s' currently in use by database removing manually selected alternative - switching %s to auto mode rename involves overwriting '%s' with
  different file '%s', not allowed rename: remove duplicate old link '%s' renaming %s link from %s to %s renaming %s slave link from %s to %s requested operation requires superuser privilege rm command for cleanup root or null directory is listed as a conffile searched, but found no packages (files matching *.deb) section<unknown> setting up automatic selection of %s several package info entries found, only one allowed shell command to move files showing file on pager skip creation of %s because associated file %s (of link group %s) doesn't exist slave file slave link slave link same as main link %s slave name source file '%.250s' not a plain file split package reassembly statoverride file contains empty line statoverride file is missing final newline status status logger stripping trailing / subprocess %s failed with wait status code %d subprocess %s returned error exit status %d subprocess %s was interrupted subprocess %s was killed by signal (%s)%s symbolic link '%.250s' size has changed from %jd to %zd syntax error in file triggers file '%.250s' syntax error in statoverride file syntax error in triggers deferred file '%.250s' at character '%s'%s takes one argument, the trigger name target is directory - cannot skip control file check there are several versions of part %d - at least '%.250s' and '%.250s' there is no script in the new version of the package - giving up to return to automatic updates use '%s --auto %s' too few values in file details field '%s' (compared to others) too many errors, stopping too many values in file details field '%s' (compared to others) too-long line or missing newline in '%.250s' trigger interest file '%.250s' syntax error; illegal package name '%.250s': %.250s trigger name contains invalid character trigger records not yet in existence triggered triggers area triggers ci file '%.250s' contains illegal trigger syntax in trigger name '%.250s': %.250s triggers ci file contains unknown directive '%.250s' triggers ci file contains unknown directive syntax triggers data directory not yet created triggers looping, abandoned truncated triggers deferred file '%.250s' trying script from the new package instead ... trying to overwrite '%.250s', which is also in package %.250s %.250s trying to overwrite '%.250s', which is the diverted version of '%.250s' trying to overwrite '%.250s', which is the diverted version of '%.250s' (package: %.100s) trying to overwrite directory '%.250s' in package %.250s %.250s with nondirectory trying to overwrite shared '%.250s', which is different from other instances of package %.250s two commands specified: --%s and --%s unable to (re)open input part file '%.250s' unable to access dpkg status area unable to access dpkg status area for bulk available update unable to change ownership of target file '%.250s' unable to check existence of '%.250s' unable to check file '%s' lock status unable to check for existence of archive '%.250s' unable to chown backup symlink for '%.255s' unable to clean up mess surrounding '%.255s' before installing another version unable to close file '%s' unable to close new triggers deferred file '%.250s' unable to close updated status of '%.250s' unable to create '%.255s' unable to create file '%s' unable to create new file '%.250s' unable to create temporary directory unable to create triggers state directory '%.250s' unable to delete control info file '%.250s' unable to delete old directory '%.250s': %s unable to delete used-up depot file '%.250s' unable to discard '%.250s' unable to execute %s (%s) unable to fill %.250s with padding unable to flush %.250s after padding unable to flush file '%s' unable to flush new file '%.250s' unable to flush updated status of '%.250s' unable to fstat part file '%.250s' unable to fstat source file unable to fsync updated status of '%.250s' unable to get file descriptor for directory '%s' unable to ignore signal %s before running %.250s unable to install '%.250s' as '%.250s' unable to install (supposed) new info file '%.250s' unable to install new info file '%.250s' as '%.250s' unable to install new triggers deferred file '%.250s' unable to install new version of '%.255s' unable to install updated status of '%.250s' unable to lock %s unable to make backup link of '%.255s' before installing new version unable to make backup symlink for '%.255s' unable to move aside '%.255s' to install new version unable to open '%.255s' unable to open directory '%s' unable to open file '%s' unable to open files list file for package '%.250s' unable to open lock file %s for testing unable to open new depot file '%.250s' unable to open output file '%.250s' unable to open source file '%.250s' unable to open temp control directory unable to open triggers ci file '%.250s' unable to open triggers deferred file '%.250s' unable to open/create new triggers deferred file '%.250s' unable to open/create status database lockfile unable to open/create triggers lockfile '%.250s' unable to read depot directory '%.250s' unable to read file triggers file '%.250s' unable to read filedescriptor flags for %.250s unable to read link '%.255s' unable to read part file '%.250s' unable to remove '%s' unable to remove backup copy of '%.250s' unable to remove copied source file '%s' unable to remove newly-extracted version of '%.250s' unable to remove newly-installed version of '%.250s' unable to remove newly-installed version of '%.250s' to allow reinstallation of backup copy unable to remove obsolete info file '%.250s' unable to rename new depot file '%.250s' to '%.250s' unable to reopen part file '%.250s' unable to restore backup version of '%.250s' unable to securely remove '%.250s' unable to securely remove '%.255s' unable to securely remove old file '%.250s': %s unable to seek to start of %.250s after padding unable to set buffering on %s database file unable to set close-on-exec flag for %.250s unable to set execute permissions on '%.250s' unable to set mode of target file '%.250s' unable to set ownership of triggers state directory '%.250s' unable to setenv for maintainer script unable to setenv for subprocesses unable to stat %s '%.250s' unable to stat %s '%.250s': %s unable to stat '%.250s' unable to stat '%.255s' (which I was about to install) unable to stat control directory unable to stat current installed conffile '%.250s' unable to stat file name '%.250s' unable to stat files list file for package '%.250s' unable to stat new distributed conffile '%.250s' unable to stat other new file '%.250s' unable to stat restored '%.255s' before installing another version unable to stat source file '%.250s' unable to stat triggers deferred file '%.250s' unable to sync directory '%s' unable to sync file '%.255s' unable to sync file '%s' unable to sync new file '%.250s' unable to truncate for updated status of '%.250s' unable to unlock %s unable to write file '%s' unable to write new file '%.250s' unable to write new triggers deferred file '%.250s' unable to write to status fd %d unable to write updated status of '%.250s' unbalanced quotes in '%s' unexpected bzip2 error unexpected data after package and selection at line %d unexpected end of file in %.250s unexpected end of file in %s in %.255s unexpected end of file or stream unexpected end of file while trying to read %s unexpected end of input unexpected end of line after package name at line %d unexpected end of line in package name at line %d unexpected end of line in statoverride file unknown argument '%s' unknown compression strategy unknown compression strategy '%s'! unknown compression type '%s'! unknown force/refuse option '%.*s' unknown option '%s' unknown option -%c unknown option --%s unknown wanted status at line %d: %.250s unpacked but not configured unsupported compression preset unsupported options in file header unsupported type of integrity check updates directory contains file '%.250s' whose name is too long (length=%d, max=%d) updates directory contains files with different length names (both %d and %d) user-defined field name '%.*s' too short verification on package %s failed! verification on package %s failed; but installing anyway as you requested version<none> version %.250s of %.250s already installed, skipping version number does not start with digit version string has embedded spaces version string is empty wait for subprocess %s failed warning while reading %s: %s while removing %.250s, directory '%.250s' not empty so not removed while removing %.250s, unable to remove directory '%.250s': %s - directory may be a mount point? will not downgrade %.250s from %.250s to %.250s, skipping yes, will deconfigure %s (broken by %s) yes, will remove %s in favour of %s yes/no in boolean field you do not have permission to lock the dpkg status database you must specify packages by their own names, not by quoting the names of the files they come in Project-Id-Version: dpkg 1.13
Report-Msgid-Bugs-To: debian-dpkg@lists.debian.org
POT-Creation-Date: 2015-12-25 13:29+0100
PO-Revision-Date: 2016-05-13 02:59+0000
Last-Translator: Felipe Augusto van de Wiel (faw) <Unknown>
Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 16:13+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
 
 gatilhos dos pacotes pendentes que são ou podem ser insolúveis:
 
O arquivo de configuração '%s' não existe no sistema.
Instalando novo arquivo de configuração como você pediu.
 
As opções de depuração podem ser misturadas utilizando a operação OU bit-a-bit.
Note que os significados e valores estão sujeitos a alteração.
 
AVISO - usar as opções marcadas com [!] pode danificar gravemente a sua instalação.
Opções de imposição marcadas com [*] estão habilitadas por padrão.
      A versão no pacote é a mesma que a da última instalação.
      não é um arquivo simples  %.255s
    O que você gostaria de fazer sobre isso? Suas opções são:
    Y ou I: instalar a versão do mantenedor do pacote
    N ou O: manter a versão actualmente instalada
      D: mostrar as diferenças entre as versões
      Z: iniciar um shell para examinar a situação
   %.250s (versão %.250s) está presente e %s.
   %.250s (versão %.250s) está para ser instalado.
   %.250s está %s.
   %.250s está instalado, mas sua versão é %.250s.
   %.250s não está instalado.
   %.250s está para ser desconfigurado.
   %.250s está para ser instalado, mas sua versão é %.250s.
   %.250s está para ser removido.
   %.250s está desempacotado, mas nunca foi configurado.
   %.250s está desempacotado, mas sua versão é %.250s.
   a versão mais recente configurada de %.250s é %.250s.
   %.250s fornece %.250s e está presente e %s.
   %.250s fornece %.250s e está para ser instalado.
   %.250s fornece %.250s mas está %s.
   %.250s fornece %.250s mas está para ser desconfigurado.
   %.250s fornece %.250s mas está para ser removido.
   %s (%s) fornece %s.
   -?, --help                       Exibe esta mensagem de ajuda
      --version                   Exibe esta versão.

   Pacote %s aguarda processamento de gatilho.
   Pacote %s não está configurado ainda.
   Pacote %s não está instalado.
   Pacote %s está para ser removido.
   Pacote %s, o qual fornece %s aguarda processamento de gatilho.
   Pacote %s, o qual fornece %s ainda não está configurado.
   Pacote %s, o qual fornece %s não está instalado.
   Pacote %s, o qual fornece %s está para ser removido.
   Versão de %s no sistema é %s.
   Versão de %s a ser configurada é %s.
   linque atualmente ausente   link atualmente aponta para %s   escravo %s: %s  %7jd bytes, %5d linhas   %c  %-20.127s %.127s
  %d em %s:   %d pacote, para a seguinte seção:  %d pacotes, para as seguintes seções:  %s (%jd bytes)
  %s (%s) quebra %s e é %s.
  %s (não é um arquivo simples)
  ==> Mantendo arquivo de configuração antigo como padrão.
  ==> O distribuidor do pacote lançou uma versão atualizada.
  ==> Usando arquivo atual como você pediu.
  ==> Usando novo arquivo de configuração como padrão.
  ==> Usando novo arquivo como você pediu.
  Pacote %s: fragmento(s)   A ação padrão é instalar a nova versão.
  A ação padrão é manter sua versão atual.
  e   depende de   novo pacote debian, versão %d.%d.
 tamanho %jd bytes: arquivo de controle=%jd bytes.
  antigo pacote debian, versão %d.%d.
 tamanho %jd bytes: arquivo de controle=%jd, arquivo principal=%jd.
 %d programa esperado não encontrado em CAMINHO ou não é um executável
%s %d programas esperados não encontrados em CAMINHO ou não são executáveis
%s %d arquivo ou diretório atualmente instalados.)
 %d arquivos e diretórios atualmente instalados.)
 %d componente de controle requerido está faltando %d componentes de controle requeridos estão faltando %s %s (multiarquitetura: %s) não é co-instalável com %s, que possui múltiplas instâncias instaladas %s (sub-processo): %s
 %s - prioridade %d %s quebra %s %s conflita com %s %s corrompido: %s %s opção de depuração, --debug=<octal> or -D<octal>:

 Referêcnia Número. Na descrição da fonte
 %s depende de %s %s melhora %s %s forçando opções - controle de comportamento quando problemas são encontrados:
  avisar mas continuar: --force-<coisa>,<coisa>,...
  parar com o erro: --refuse-<coisa>,<coisa>,... | --no-force-<coisa>,...
 Forçar coisas:
 %s está bloqueado por um outro processo %s está faltando %s não está propriamente instalado; ignorando quaisquer dependências dele %s pré-depende de %s %s recomenda %s %s sugere %s %s/%s foi alterado (manualmente ou por script); alterando para o modo de somente alterações manuais %s/%s está pendente; será atualizado com a melhor escolha %s:
    Versão do formato da parte:     %d.%d
    Parte do pacote:	            %s
        ... versão:                 %s
        ... arquitetura:            %s
        ... MD5 checksum:           %s
        ... comprimento:            %jd bytes
        ... dividir a cada:         %jd bytes
    Número da parte:                %d/%d
    Comprimento da parte:           %jd bytes
    Offset da parte:                %jd bytes
    Tamanho da arquivo da parte 
	(parte utilizada):  			%jd bytes

 %s: conffile '%.250s' não é um arquivo texto ou um link simbólico (= '%s') %s: arquivo de configuração '%.250s' decidiu corromper o nome do arquivo
 ('%s' é um link simbólico para '%s') %s: o arquivo de configuração '%s' é uma ligação circular
 (= '%s') %s: problemas de dependência, porém está configurando conforme solicitado:
%s %s: problemas de dependência, mas está sendo removido conforme solicitado:
%s %s falha ao ligar a entrada ao fluxo bzip2 %s: erro ao ligar a entrada ao fluxo gzip %s: saída de erro de ligação ao fluxo bzip2 %s: saída de erro de ligação ao fluxo gzip %s: erro enquanto efetuava a limpeza:
 %s
 %s: erro: %s
 %s: falha ao ligar '%.250s' to '%.250s': %s %s: falha ao remover '%.250s' (antes de sobrescrever): %s %s: falha ao remover '%.250s': %s %s: falha ao remover a cópia de segurança antiga '%.250s': %s %s: falha ao remover a antiga versão da distribuição  '%.250s': %s %s: falha ao renomear '%.250s' para '%.250s': %s %s: falha de leitura interna no bzip2 %s: falha de leitura interna no bzip2: '%s' %s: falha de escrita interna no bzip2 %s: falha de escrita interna no bzip2: '%s' %s: erro interno de leitura do gzip %s: erro interno de leitura do gzip: '%s' %s: erro interno de escrita do gzip %s: falha interna de escrita no gzip: %s %s: falha interna de escrita no gzip: '%s' %s: erro de fechamento lzma %s: erro lzma: %s %s: erro de leitura lzma %s: erro de gravação lzma %s: erro fora do contexto, abortando:
 %s
 %s: erro de cópia de passagem: %s %s: vários erros aninhados durante recuperação de erro!!
 %s: não foi possível abrir %s para hash: %s %s: incapaz de ler a ligação com o arquivo de configuração '%s'
 (= '%s'): %s %s: incapaz de obter os atributos do arquivo de configuração '%s'
 (= '%s'): %s %s: erro fatal irrecuperável, abortando:
 %s
 %s: aviso: %s
 %s:%s:%d:%s: erro interno: %s
 '%.255s' não contém componente de controle '%.255s' '%.255s' não é um formato de arquivo debian '%.50s' não tem permissão para %s '%s' conflita com '%s' '%s' contém valor '%s' para Prioridade user-defined '%s' não possui valor campo '%s', nome de pacote inválido '%.255s': %s campo '%s', nome de arquitetura faltando ou lixo onde nome da arquitetura esperado campo '%s', nome de pacote faltando, ou lixo no lugar em que se esperava o nome do pacote campo '%s', referência a '%.255s':
 '%c' está obsoleto, em seu lugar use '%c=' ou '%c%c' campo '%s', referência a '%.255s':
 relacionamento de versões ruim %c%c campo '%s', referência a '%.255s':
 combinação exata implícita com o número da versão, sugere-se usar '=' campo '%s', referência a '%.255s':
 valor da versão começa com caractere não-alfanumérico, sugere-se a adição
 de um espaço '%s' referencia do campo  '%.255s' erro na versão campo '%s', referência a '%.255s': nome de arquitetura inválida '%.255s': %s campo '%s', referência a '%.255s': versão contém '%c' campo '%s', referência a '%.255s': versão não terminada campo '%s', erro de sintaxe após referência a pacote '%.255s' '%s' não é um nome de arquitetura válido: %s '%s' precisa de um valor '%s' não encontrado em CAMINHO ou não é um executável (Percebendo desaparecimento de %s, que foi completamente substituído).
 (Lendo banco de dados ...  (nenhum arquivo 'control' no arquivo de controle!)
 (nenhuma descrição disponível) (total %jd bytes)
 , gerou um core opção -%c não aceita um valor opção -%c aceita um valor --%s --pending não aceita argumento que não seja opção --%s --recursive precisa de ao menos um caminho como argumento --%s precisa de <nome> --%s precisa de <nome> <caminho> --%s precisa de um nome de arquivo .deb como argumento --%s precisa de um argumento <diretório> --%s precisa de um argumento <arquivo> --%s precisa de um único argumento --%s precisa de um diretório-alvo.
Talvez você devesse usar dpkg --install? --%s precisa de um nome de pacote válido '%.250s' não é: %s --%s precisa de ao menos um arquivo de pacote como argumento --%s precisa de pelo menos um nome de pacote como argumento --%s precisa de quatro argumentos opção --%s não aceita um valor opção --%s aceita um valor --%s requer um argumento octal positivo --%s requer um ou mais argumentos de parte de arquivo --%s leva no máximo dois argumentos --%s aceita no máximo dois argumentos (.deb e diretório) --%s pega exatamente um argumento --%s pega exatamente dois argumentos --%s não aceita argumentos --%s pega um argumento do nome do pacote --%s aceita somente um argumento (nome do arquivo .deb) --auto requer exatamente um arquivo parcial como argumento --auto requer o uso da opção --output má relação em --compare-versions --compare-versions aceitam três argumentos: <versão> <relação> <versão> --install precisa de <ligação> <nome> <caminho> <prioridade> --search precisa de pelo menos um padrão de nome de arquivo como argumento --slave precisa de <ligação> <nome> <caminho> --slave somente permitido com --install --split necessita de um nome de arquivo fonte como argumento --split aceita no máximo um nome de arquivo fonte e um prefixo de destino --update realizado mas %s não existe --update é inutil para --remove ... parece que correu bem .../%s ; porém:
 <compressão> de tar -cf <deb> é o nome de arquivo de um arquivo no formato Debian.
<carq> é o nome de um componente de arquivo administrativo.
<ccampo> é o nome de um campo no arquivo 'control' principal.

 <descompressão> <link> e <caminho> não podem ser o mesmo <link> é a ligação simbólica apontando para %s/<nome>.
  (por exemplo: /usr/bin/pager)
<name> é o nome mestre para estre grupo de ligações.
  (por exemplo: pager)
<path> é a localização e um dos arquivos alvo alternativos.
  (por exemplo: /usr/bin/less)
<priority> é um inteiro; opções com números mais altos tem prioridade mais elevada no
  modo automático.

 <estado do pacote e progresso do descritor de arquivo> <erro padrão> <entrada padrão> <standard output> Adicionando '%s'
 Permitir a instalação de pacotes em conflitos Alternativa %s não alterada porque a escolha %s não está disponível. Sempre instalar arquivos de configuração que estejam faltando Sempre usar arquivos de configuração novos, sem perguntar Sempre usar arquivos de configuração antigos, sem perguntar Outro processo bloqueou o banco de dados para gravação, e pode estar atualmente
modificando ele, alguns dos prolemas a seguir podem estar relacionados a isso.
 Arquitetura Autenticando %s ...
 Comandos:
  --check-supported                Checa se o dpkg em execução tem suporte a
                                   gatilhos.

 Comandos:
  --install <linque> <nome> <caminho> <prioridade>
    [--slave <linque> <nome> <caminho>] ...
                           adiciona um grupo de alternativas ao sistema.
  --remove <nome> <caminho> remove o <caminho> da alternativa de grupo <nome>.
  --remove-all <nome> remove o grupo <nome> do sistema de alternativas.
  --auto <nome> troca para o linque mestre <nome> para o modo automático.
  --display <nome> exibe informação sobre o grupo <nome>.
  --query <nome> versão analisável da máquina de --display <nome>.
  --list <nome> exibe todos os alvos do grupo <nome>.
  --get-selections lista nomes alternativos mestre e o seu status.
  --set-selections lê status alternativos da entrada padrão.
  --config <nome> exibe alternativas para o grupo <nome> e solicita ao
                           usuário para selecionar qual deles utilizar.
  --set <nome> <caminho> define o <caminho> como alternativa para o <nome>.
  --all invoca --config em todas as alternativas.

 Comandos:
  -s|--split <arquivo> [<prefixo>]      Divide um arquivo.
  -j|--join <parte> <parte> ...         Junta partes.
  -I|--info <parte> ...                 Exibe informação sobre uma parte.
  -a|--auto -o <completo> <parte>       Auto-acumular partes.
  -l|--listq                            Lista peças que não combinam.
  -d|--discard [<nome-do-arquivo> ...]  Descarta peças que não combinam.

 Comandos:
  [--add] <file> adicionar um desvio.
  --remove <file> remover o desvio.
  --list [<glob-pattern>] mostra os desvios do arquivo.
  -- listpackage<file> mostrar o pacote que desvia do arquivo.
  --truename <file> retornar o arquivo desviado.

 Operadores de comparação para --compare-versions são:
  lt le eq ne ge gt       (trata versão vazia como anterior a qualquer
                            outra versão);
  lt-nl le-nl ge-nl gt-nl (trata versão vazia como posterior a qualquer
                            outra versão);
  < << <= = >= >> >       (somente para compatibilidade com a sintaxe do
                            arquivo de controle).

 Configurar qualquer pacote que possa ajudar esse Versão de ferramenta de consulta %s do programa de gerenciamento de pacotes Debian %s.
 Versão do utilitário do gatilho de pacote %s do Debian %s.
 Debian %s versão %s.
 Mecanismo ("backend") de repositório de pacotes Debian '%s' versão %s.
 Programa de gerenciamento de pacotes Debian '%s' versão %s.
 Ferramenta de divisão/junção de pacote Debian '%s'; versão %s.
 Excluído %s.
 Dependências e conflitos Descrição Desired=Unknown/Install/Remove/Purge/Hold
| Status=Not/Inst/Conf-files/Unpacked/halF-conf/Half-inst/trig-aWait/Trig-pend
|/ Err?=(none)/Reinst-required (Status,Err: uppercase=bad)
 Não execute operações segura se E/S quando descompactar Erros foram encontrados durante o processamento de:
 Estado da saída:
  0 = ok
  1 = com --auto, arquivo não é uma parte
  2 = problema
 Arquivo '%.250s' não é parte de um arquivo em múltiplas partes.
 Sintaxe de formato:
  Um formato é uma string que será gerada para cada pacote. O formato pode
  incluir as sequências de escape padrão \n (nova linha), \r (retorno de
  carro) ou \\ (barra invertida simple). Informação de pacotes podem ser
  incluídas inserindo referências variáveis ao campos do pacote usando a
  sintaxe ${variável[;tamanho]} (${var[;width]}). Campos serão alinhados à
  direita a menos que o tamanho seja negativo, neste caso o alinhamento à
  esquerda será usado.
 informação de progresso geralmente útil Ignorando pedido para remover desvio compartilhado '%s'.
 Ignorando pedido para renomear arquivo '%s' que pertence a pacote desviante '%s'
 Informações sobre o pacote %d foram atualizadas.
 Informações sobre os pacotes %d foram atualizadas
 Uma quantidade insana de dados inúteis Instalar um pacote mesmo que falhe a verificação de autenticidade Instalar mesmo que isso danifique outro pacote Instalando nova versão do arquivo de configuração %s ...
 Invocação e estado dos scripts do mantedor Arquivos lixo deixados para trás no diretório "depot":
 Saindo '%s'
 Lotes de saída de dependências/conflitos Muitos dados inútes sobre, por exemplo, o diretório dpkg/info Lotes de saída para cada arquivo de configuração Lotes de saída para cada arquivo processado Muita saída de dados relacionados a gatilhos Mais de uma cópia do pacote %s foi desempacotada
 nesta execução! Configurando-o somente uma vez.
 Nome Sem desvios '%s', nada removido.
 Nota: CAMINHO raiz normalmente deve conter /usr/local/sbin, /usr/sbin e /sbin Nada para configurar. Ofecer para substituir arquivos de configuração sem novas versões Opções:
  --admindir=<diretório>           Usa <diretório> ao invés de %s.
  --load-avail                     Usa arquivo disponível em --show e --list.
  -f|--showformat=<formato>        Usa formato alternativo para --show.

 Opções:
  --altdir <diretório> altera o diretório de alternativas.
  --admindir <diretório> altera o diretório administrativo.
  --log <arquivo> altera o arquivo do registro do histórico.
  --force possibilita substituir arquivos por linques alternativos.
  --skip-auto omite avisos para alternativas corretamente configuradas
                           no modo automático (aplicável apenas a --config)
  --verbose operação detalhada, mais saída.
  --quiet operação silenciosa, saída mínima.
  --help mostra esta mensagem de ajuda.
  --version mostra a versão.
 Opções:
  --depotdir <diretório>           Use <diretório> no lugar de %s/%s.
  -S|--partsize <tamanho>             Em KiB, para -s (padrão é 450).
  -o|--output <arquivo>               Nome do arquivo, para -j (padrão é
                                     <pacote>_<versão>_<arq>.deb).
  -Q|--npquiet                     Silencioso quando -a não é uma parte.
  --msdos                          Gera nomes de arquivo 8.3.

 Opções:
  --package <package> nome do pacotevcuja cópia de <file> não
                                                                                          vai ser desviado.
  --local todas as versões do pacotes estão desviadas.
  --divert <divert-to> Desvio de nome usado por outras versões do pacote.
  --rename realmente mover o arquivo de lado (ou atrás).
  --admindir <directory>  admindir definir o diretório com o arquivo de desvios.
  --test não faz nada, apenas demonstrar.
  --quiet uma operação silenciosa, a produção mínima.
  --help mostrar esta mensagem de ajuda.
  --version mostra a versão.

 Saída para cada arquivo de configuração Saída para cada arquivo processado Sobrescrever um arquio desviado com uma versão não-desviada Substituir um arquivo de um pacote com outro Substituir o diretório de um pacote com arquivo de outro Faltam programas importantes no PATH, eventuais problemas CAMINHO não está definido Pacote %s está sendo retido, não vou tocá-lo. Use --force-hold para sobrepor.
 Pacote %s listado mais de uma vez, processando somente uma vez.
 Pacote '%s' não contém quaisquer arquivos (!)
 Pacote que no estado não-instalado possui arquivos de configuração ("conffiles"), esquecendo-os Pacotes ainda não remontados:
 Parte %d do pacote %s preenchida (continua precisando  Caminho Campo de pré-dependências Prioridade Processar até mesmo pacotes  sem arquitetura ou onde elas estejam erradas Processar também arquivos com versões diferentes Processar pacotes incidentais mesmo quando em suspensão Processamento parou porque havia muitos erros.
 Juntando o pacote %s a partir da parte %d:  Juntando o pacote %s a partir das partes %d:  Informação registrada sobre %s a partir de %s.
 Remover um pacote essencial Remover para pacotes que requerem instalações Removendo '%s'
 Trocar um pocote por um de versão menor Substituindo informações de pacotes disponíveis, usando %s.
 Selecionando pacote %s previamente não selecionado.
 Seleção Setar todas as opções forçadas Configurando %s (%s) ...
 Quantidade insana de saída de dados relacionados a gatilhos Pular linha inválida: %s Descarta alternativa desconhecida %s. Ignorando pacote %s não selecionado.
 Dividindo o pacote %s em %d parte:  Dividindo o pacote %s em %d partes:  Estado Os pacotes a seguir estão aguardando processamento de gatilhos que eles
ativaram em outros pacotes. Este processamento pode ser executado usando
dselect ou dpkg --configure --pending (ou dpkg --triggers-only):
 Os pacotes a seguir estão bagunçados devido a sérios problemas durante
a instalação, e precisam ser reinstalados para que eles (e quaisquer
pacotes que dependam dele) funcionem corretamente:
 Os seguintes pacotes não possuem o arquivo de lista de controle no
banco de dados, eles precisam ser reinstalados:
 Os seguintes pacotes não possuem o arquivo de controle md5sum no
banco de dados, eles precisam ser reinstalados:
 Os pacotes a seguir estão apenas meio configurados, provavelmente devido a
problemas ao configurá-los pela primeira vez. A configuração deve ser feita
novamente usando dpkg --configure <pacote> ou a opção no menu do dselect:
 Os pacotes a seguir estão apenas meio instalados, devido a problemas durante
a instalação, que provavelmente estará completa se você tentar executá-la
novamente; os pacotes podem ser removidos com o dselect ou dpkg --remove:
 Os seguintes pacote não tem uma arquitetura:
 Os seguintes pacotes tem uma arquitetura ilegal:
 Os seguintes pacote tem uma arquitetura estrangeira desconhecida, que irá
causar erros de dependência nos aplicativos de frente. Isto pode ser 
corrigido registrando a arquitetura estrangeira com --add-architecture:
 Os pacotes a seguir foram disparados por gatilhos, mas o processamento de
gatilhos ainda não terminou. O processamento de gatilhos pode ser executado
usando dselect ou dpkg --configure --pending (ou dpkg --triggers-only):
 Os pacotes a seguir foram desempacotados mas ainda não foram configurados.
Eles precisam ser configurados usando dpkg --configure ou a opção do menu
do dselect de configuração para que funcionem:
 Há %d opção para a alternativa %s (providenciando %s). Há %d opções para a alternativa %s (providenciando %s). Não existe programa que fornece %s. Só existe uma alternativa no grupo de ligação %s (fornecendo %s): %s Este é um software livre; veja a Licença Pública Geral GNU (GPL) versão 2 ou
superior para ver as condições de cópia. NÃO há garantias.
 Gatilho de ativação e  processamento Tentar (des)instalar coisas mesmo não sendo root Tornar todos os problemas de dependência em avisos Tornar todos os problemas de dependência de versões em avisos Digite 'exit' quando terminar.
 Digite dpkg-deb --help para ajuda sobre manipulação de arquivos *.deb;
Digite dpkg --help para ajuda sobre instalação e desinstalação de pacotes. Digite dpkg-split --help para ajuda. Digite dpkg-trigger --help para ajuda sobre este utilitário. Atualizando informações de pacotes disponíveis, usando %s.
 Uso: %s [<opção> ...] <comando>

 Uso: %s [<opções> ...] <nome-do-gatilho>
     %s [<opções> ...] <comando>

 Use --help para ajuda sobre arquivos desviados. Use --help para ajuda sobre substituição de informações de estatísticas de arquivo. Usar --help para obter ajuda na pesquisa de pacotes Use dpkg --info (= dpkg-deb --info) para examinar arquivos de pacote,
e dpkg --contents (= dpkg-deb --contents) para listar seu conteúdo.
 Versão Versão do dpkg trabalhando com suporte %s não foi configurada ainda.
 Por favor use 'dpkg --configure dpkg' e então tente novamente.
 Ao adicionar, o padrão é  --local and --divert <original>.distrib
Ao remover --package ou --loca e --divert deve corresponder, se especificado
Pacote preinst/postrm scripts sempre deve especificar --package e --divert.
 [padrão=N] [padrão=Y] [sem padrão] admindir precisa estar dentro de instdir para o dpkg trabalhar corretamente configurando também '%s' (exigido por '%s') alternativa %s (parte do grupo de ligação %s) não existe; removendo da lista de alternativas %s alternativo não pode ser master: ele é um slave de %s alternativa %s não pode ser dependente de %s: é uma alternativa mestre alternativa %s não pode ser escrava de %s: é uma escrava de %s alternativa %s para %s não registrada; não foi removida alternativa %s para %s não registrada; não foi definida ligação alternativa %s já é gerenciada por %s ligação alternativa %s já é gerenciada por %s (dependente de %s) link alternativo não é absoluto como deveria ser: %s nome alternativo (%s) não deve conter '/' e espaços caminho alternativo %s não existe caminho alternativo não é absoluto como deveria ser: %s alternativas ('|') não permitidas no campo %s nome ambíguo do pacote '%s' com mais de uma instância instalada uma substituição para '%s' já existe, porém --force foi especificado então será ignorada uma substituição para '%s' já existe; abortando qualquer desvio de %s qualquer desvio de %s para %s nome de membro de ar '%s' muito longo membro de ar %jd muito grande <desconhecido> arquitetura '%s' é ilegal: %s arquitetura '%s' é reservada e não pode ser adicionada o arquivo '%.250s' contém dois membros de controle, desistindo arquivo continha objeto '%.255s' de tipo desconhecido 0x%x arquivar tamanho do membro de controle arquivo tem um formato de versão inválido: %s o arquivo tem um tamanho do membro de controle mal formatado '%s' arquivo não possui novas linhas no cabeçalho arquivar informações do membro do cabeçalho arquivo está no formato versão %d.%d; obter novo dpkg-deb arquivar número mágico de versão arquivar cabeçalho do membro números das partes do arquivo deslocamento da parte do arquivo número de partes do arquivo tamanho total do arquivo modo automático atualizações automáticas de %s/%s estão desabilitadas; ignorando processamento de gatilhos em espera por outro pacote linha em branco no valor do campo '%.*s' quebrado devido a falha na remoção ou instalação quebrado devido a falha no "postinst" atualização completa de disponibilidade requer acesso de escrita à área de estados do dpkg não é possível instalar opção desconhecida %s não foi possível executar "mmap" no arquivo de informações do pacote '%.255s' não foi possível remover script "postrm" antigo não foi possível executar "stat" no arquivo de informações do pacote '%.255s' impossível acessar arquivo não foi possível adicionar arquivo membro de ar (%s) em '%s': %s não foi possível adicionar parte do pacote dividido '%s' em '%s': %s não foi possível anexar a '%s' não é possível fechar o canal do descompressor não foi possível computar hash MD5 do arquivo '%s': %s não foi possível computar hash MD5 para o arquivo tar '%.255s': %s não foi possível copiar '%s' no arquivo '%s': %s não foi possível copiar '%s' para '%s': %s não é possível copiar o membro de arquivo de "%s" para o canal do descompressor: %s não foi possível copiar dados extraídos de '%.255s' para '%.255s': %s não é possível desviar o arquivo '%s' para ele mesmo não foi possível extrair arquivo de controle '%s' de '%s': %s não foi possível extrair parte do pacote dividido '%s': %s não foi possível abrir '%.255s' (em '%.255s') não é possível abrir arquivo parcial '%.250s' não foi possível ler diretório info não foi possível remover '%.250s' não foi possível remover arquitetura '%s' atualmente em uso pelo banco de dados não foi possível remover arquitetura não-estrangeira '%s' não foi possível remover antigo arquivo de configuração de backup '%.250s' (de '%.250s') não foi possível remover arquivo de configuração antigo '%.250s' (= '%.250s') não foi possível remover a antiga lista de arquivos não foi possível renomear '%s' para '%s' não foi possível satisfazer as pré-dependências para %.250s (requerido devido a %.250s) não foi possível fazer vasculhar o diretório '%.255s' não foi possível vasculhar diretório de atualizações '%.255s' não foi possível satisfazer pré-dependências:
 %s não foi possível ignorar membro de controle do arquivo de '%s': %s impossível ignorar membro do arquivo de '%s': %s não é possível ignorar arquivo "%.255s" (substituído ou excluído?) do canal: %s não foi possível ignorar o preenchimento do arquivo '%.255s': %s não foi possível pular divisão do cabeçalho do pacote para '%s': %s não foi possível executar "stat" '%.255s' (em '%.255s') não foi possível realizar stat no arquivo "%s" não é possível eliminar os possíveis zeros finais de dpkg-deb: %s caractere '%c' não permitido (apenas letras, dígitos e caracteres '%s') dados comprimidos estão corrompidos comprimindo membro de controle arquivo de configuração ("conffile") '%.250s' não aparece no pacote arquivo de configuração ("conffile") '%.250s' não permite a execução de "stat" Arquivo de configuração %s' não é um arquivo evidente Visualizador de diferenças em arquivos de configuração Arquivo de configuração '%s' contém espaços em branco nome do conffile '%s' está duplicado arquivo de configuração shell erro de configuração: %s:%d: %s ações conflitantes -%c (--%s) e -%c (--%s) desvios conflitantes envolvendo '%.250s' ou '%.250s' pacotes conflitantes - não instalando %.250s considerando desconfiguração de %s, que pode ser quebrada por %s ... considerando desconfiguração do pacote
 essencial %s, para habilitar %s considerando remoção de %s em favor de %s ... diretório de controle possui permissões ruins %03lo (devem ser >=0755 e <=0755) diretório de controle não é um diretório arquivo de controle '%s' não existe controle de arquivo contêm %c membro de controle arquivo de informações de formato de banco de dados "%s" corrompido arquivo tar do sistema de arquivos corrompido - arquivo de pacote corrompido não foi possível abri o log '%s': %s não foi possível obter os atributos do arquivo antigo '%.250s', portanto não será removido: %s não foi possível abrir '%i' para "stream" alternativa atual %s é desconhecida, alterando para %s para grupo de ligação %s encontrado ciclo durante o processamento dos gatilhos:
 cadeia de pacotes pelos quais os gatilhos são ou podem ser responsáveis: membro de dados descomprimindo membro do arquivo problemas de dependência - deixando desconfigurado problemas de dependência - não removendo problemas de dependência impedem a configuração de %s:
%s problemas de dependência impedem a remoção de %s:
%s tipo de compressão '%s' obsoleto; use xz no lugar descartando ligação dependente obsoleta %s (%s) desviado por %s a partir de: %s
 desviado por %s para: %s
 desvio de %s por %s desvio de %s e %s por %s desviar-para não pode conter novas linhas desviado por %s para: %s
 feito
 rebaixando versão de %.250s de %.250s para %.250s o dpkg não está marcado como instalado, não é possível verificar suporte %s!
 status do banco de dados dpkg dpkg-deb: construindo um pacote desconhecido em '%s'.
 dpkg-deb: construindo pacote '%s' em '%s'.
 gatilho em espera duplicado no pacote '%.255s' arquivo de gatilho interessados duplicado para o arquivo chamado '%.250s' e pacote '%.250s' caminho %s duplicado gatilho pendente duplicado '%.255s' ligação escrava %s duplicada nome do slave %s duplicado valor duplicado para o campo '%s' valor duplo para campo definido pelo usuário '%.*s' campo de detalhes de arquivo '%s' vazio string vazia vinda de "fgets" ao ler arquivos de configuração ("conffiles") nomes de gatilho vazios não são permitidos valor vazio para %s época época na versão é negativa época na versão não é um número época na versão é muito grande erro erro checar  '%s' erro fechando %.250s erro ao fechar arquivo de configuração '%.255s' erro fechando arquivo com lista de arquivos do pacote '%.250s' erro fechando o "pipe" do find erro fechando/escrevendo '%.255s' erro criando dispositivo '%.255s' erro criando diretório '%.255s' erro criando ligação fixa ("hard link") '%.255s' erro ao criar novo arquivo de backup '%s' erro criando pipe '%.255s' erro criando ligação simbólica '%.255s' erro ao se certificar que '%.250s' não existe erro ao executar gancho '%s', código de saída %d erro ao formatar a string na variável varbuf erro no formato de exibição: %s erro ao instalar novo arquivo '%s' erro abrindo arquivo de arquivos de configuração ("conffiles") erro na abertura do diretório de configuração '%s' erro lendo %.250s erro lendo %s de arquivo %.255s erro lendo arquivo de arquivos de configuração ("conffiles") erro lendo a saída do tar do dpkg-deb erro lendo o "pipe" do find erro lendo do "pipe" dpkg-deb erro lendo arquivo de gatilhos adiados '%.250s' erro ao remover o arquivo antigo de backup '%s' erro estabelecendo posse de '%.255s' erro estabelecendo posse da ligação simbólica '%.255s' erro estabelecendo permissões de '%.255s' erro estabelecendo registros de data de '%.255s' erro tentando abrir %.250s erro não pegando sinal %s: %s
 erro ao escrever em '%s' erro escrevendo '%s' erro na escrita de '%s' erro ao gravar na lista de arquitetura erro escrevendo para stderr, descoberto antes do prompt do arquivo de configuração ("conffile") falhou ao alocar memória falha na alocação de memória para nova entrada na lista de pacotes falhos: %s falhou ao mudar para o diretório '%.255s' falhou ao mudar para o diretório falhou ao mudar para o diretório após criá-lo falhou ao executar "chroot" para '%.250s' falhou ao fechar após ler: '%.255s' falhou ao criar diretório falhou ao criar "pipe" dup para fd %d falhou dup para std%s falhou falhou ao executar "fdopen" no "pipe" do find falha para fstat ar arquivo membro de (%s) falhou ao executar "fstat" no arquivo falha ao executar "fstat" no arquivo de desvios falhou ao executar "fstat" no arquivo "statoverride" falha ao criar arquivo temporário (%s) falha ao abrir o arquivo de configuração '%.255s' para leitura: %s falha ao abrir arquivo de desvios falhou ao abrir arquivo de informações do pacote '%.255s' para leitura falhou ao abrir arquivo "statoverride" falhou ao abrir arquivo de lista de gatilhos interessados '%.250s' falha na leitura falhou ao ler '%.255s' (em '%.255s') falhou ao ler arquivo '%.255s' falhou ao remover arquivo de atualização incorporado %.255s falhou ao remover meu próprio arquivo de atualização %.255s falhou ao voltar no arquivo temporário (%s) falhou ao retroceder arquivo de gatilhos interessados '%.250s' falha ao procurar falhou ao executar "stat" ("dereference") para ligação simbólica existente '%.250s' falhou ao executar "stat" ("dereference") no novo alvo de ligação simbólica proposto '%.250s' para ligação simbólica '%.250s' falha ao usar o stat no arquivo temporário (%s) falha ao remover ligação de arquivo temporário (%s), %s falha ao gravar falha ao escrever registro de banco de dados %s  sobre '%.50s' até '%.250s' falhou ao escrever detalhes de '%.50s' para '%.250s' falha ao escrever nome do arquivo no tar pipe (%s) fgets deu uma string vazia a partir de '%.250s' campo de nome '%.*s' precisa ser seguido de vírgula largura do campo está fora do intervalo arquivo '%.250s' tem um formato de versão inválido: %s arquivo '%.250s' está corrompido - %.250s faltando arquivo '%.250s' está corrompido - checksum md5 ruim '%.250s' o arquivo '%.250s' está corrompido - cabeçalho mágico do arquivo errado o arquivo '%.250s' está corrompido - número errado da parte do arquivo arquivo '%.250s' está corrompido - digito ruim (código %d) em %s arquivo '%.250s' está corrompido - magic ruim no final do primeiro cabeçalho arquivo '%.250s' está corrompido - "magic" ruim no fim do segundo cabeçalho o arquivo '%.250s' está corrompido - número errado de partes do arquivo arquivo '%.250s' está corrompido - caracter de preenchimento ruim (código %d) arquivo '%.250s' está corrompido - nova linha faltando após %.250s o arquivo '%.250s' está corrompido - nenhuma barra entre os números das partes do arquivo arquivo '%.250s' está corrompido - nulos na seção info arquivo '%.250s' está corrompido - segundo membro não é um membro de dados arquivo '%.250s' está corrompido - tamanho errado para o número parcial citado arquivo '%.250s' está corrompido - muito pequeno arquivo '%.250s' está corrompido - número errado de partes para tamanhos citados arquivo '%.250s' está na versão de formato %d.%d; obtenha um novo dpkg-split arquivo '%.250s' não é um arquivo binário debian (tente dpkg-split?) arquivo '%.250s' não é um arquivo parcial arquivo '%s' está corrompido; inteiro fora do intervalo em %s arquivo '%s' não é um arquivo parcial
 campo de detalhes de arquivo '%s' não permitido em arquivo de estado formato de arquivo desconhecido parece que o arquivo foi corrompido
 ao ser baixado em modo ASCII arquivo não pode conter novas linhas gatilhos de arquivo registram menções a nomes de pacotes ilegais '%.250s' (para interesse no arquivo '%.250s'): %.250s nome de arquivo "%s" não é absoluto arquivos '%.250s' e '%.250s' não são partes do mesmo arquivo arquivo com lista de arquivos do pacote '%.250s' contém nome de arquivo vazio Ausência de quebra de linha na lista de arquivos para o pacote '%.250s' lista de arquivos do pacote '%.250s' ausente; presumindo que o pacote não tem arquivos instalados atualmente lista de arquivos do pacote '%.250s' não é um arquivo regular procura por --recursive retornou erro não manipulado %i encontrar com dpkg --recursive forçando reinstalação de alternativa %s pois grupo de ligação %s está quebrado externo/permitido/igual/nenhum no campo quadstate fork falhou versão do formato seguida por lixo versão do formato não tem ponto número de versão do formato versão do formato com componente maior vazio versão do formato com componente menor vazio cabeçalho ar danificado gerado para '%s' cabeçalho muito longo, fazendo com que a parte fique muito longa; o nome do pacote ou número
da versão devem ser extraordinariamente grandes, ou coisa assim; desistindo ignorando quebra, pode prosseguir assim mesmo! ignorando conflito, pode continuar assim mesmo! ignorando problema de dependência com %s:
%s ignorando problema de pré-dependência! ignorando requisição para remover %.250s, que não está instalado ignorando requisição para remover %.250s, somente seus arquivos
 de configuração estão no sistema; use --purge para removê-los também nome ilegal da arquitetura no especificador '%s:%s': %s nome do pacote ilegal esperado '%.250s': %.250s nome de pacote ilegal na linha %d: %.250s nome de pacote ilegal no gatilho em espera '%.255s': %s nome ilegal de pacote no especificador '%s%s%s': %s nome de gatilho pendente ilegal '%.255s': %s formato da base de dados de informações (%d) é inválido ou novo demais. Tente obter um dpkg mais recente instalação de %.250s instalado script %s instalado instalando %.250s quebrará %.250s, e
 a desconfiguração não é permitida (--auto-deconfigure poderá ajudar) instalar %.250s quebrará software existente erro interno caractere inválido '%c' no arquivo '%.250s' membro '%.16s' tamanho caractere inválido '%c' na largura do campo caracteres inválidos no número de versão caracteres inválidos no número de versão parâmetros do compressor inválidos: %s inteiro inválido para --%s: '%.250s' sintaxe inválida ou desconhecida no nome do gatilho '%.250s' (no interesse do gatilho para o pacote '%.250s') nome de pacote inválido '%.250s' no arquivo de gatilhos adiados '%.250s' nome de pacote inválido (%.250s) estado inválido lixo depois de %s linha não terminou ao tentar ler %s linha muito longa ou não terminada apropriadamente enquanto tentava-se ler '%s' ligação %s é tanto primário quanto escravo desvio local a partir de: %s
 desvio local de %s desvio local de %s para %s Desvio local para: %s
 localmente desviado para: %s
 nome de arquivos longos script de mantenedor '%.50s' possui permissões ruins %03lo (devem ser >=0555 e <=0775) script de mantenedor '%.50s' não é um arquivo simples ou uma ligação simbólica script de mantenedor '%.50s' não permite a execução de "stat" malloc falhou (%zu bytes) modo manual arquivo principal link mestre pode haver problemas ao remover %s, uma vez que fornece %s ... pode não ser uma string vazia limite de uso de memória atingido incompatibilidade em desvio-para
  na remoção de '%s'
  encontrado '%s' incompatibilidade em pacote
  na remoção de '%s'
  encontrado '%s' %s faltando fecha-chaves ausente instâncias coinstaláveis e não-coinstaláveis presentes no pacote, muito provavelmente devido a uma atualização de um dpkg não-oficial multiarquitetura múltiplos Conflitos e Reescrita pacote com várias instâncias não-coinstaláveis presente, muito provavelmente devido a uma atualização de um dpkg não-oficial múltiplas sobreposições de estatísticas presentes para o arquivo '%.250s' deve ser chamado de um script de mantenedor (ou com uma opção --by-package) deve começar com um alfanumérico é necessário inciar com um caractere alfanumérico nome %s é tanto primário quanto escravo necessita --display, --query, --list, --get-selections, --config, --set, --set-selections, --install, --remove, --all, --remove-all ou --auto necessário uma opção de ação novo script %s nova linha no campo nome '%.*s' novas linhas proibidas em arquivos update-alternatives (%s) sem alternativas para %s nenhum substituição presente sem informações de pacote em '%.255s' nenhum pacote chamado '%s' está instalado, não posso configurar não foram encontrados pacotes correspondendo a %s não foram encontrados caminhos que correspondam ao padrão %s não, %s é essencial, não irá se desconfigurar
 para habilitar %s não, não foi possível proceder com %s (--auto-deconfigure irá ajudar):
%s não está verificando o conteúdo da área de controle não instalado não instalado mas a configuração permanece %s não foi removido, uma vez que não é um link simbólico %s não foi substituido por um link nada após vírgula em número de versão nome do pacote nulo em --%s lista separada por vírgulas '%.250s' número de partes do arquivo opção '--%s' obsoleta, pacotes indisponíveis serão limpos automaticamente opção obsoleta '--%s'; use '--%s' no lugar antigo script %s Antigo conffile '%.250s' estava em um diretório vazio (e agora foi removido) Antigo arquivo '%.250s' é o mesmo que diversos novos arquivos! (ambos '%.250s' e '%.250s') versão antiga do pacote possui nome de arquivo info demasiadamente longo iniciando '%.250s' abertura do componente '%.255s' (em %.255s) falhou de maneira não esperada operação requer acesso de leitura/escrita à área de estado do dpkg sem memória para nova entrada de limpeza sem memória para nova entrada de limpeza com tantos argumentos falta de memória para novo contexto de erro ignorando problema pois a opção --force está habilitada: pacote %.250s já está instalado e configurado pacote %.250s não está pronto para configuração
 não posso configurar (estado atual '%.250s') pacote %.250s não está pronto para processamento de gatilho
 (estado atual '%.250s' sem gatilhos pendentes) pacote %s %s não pode ser configurado pois %s tem uma versão diferente (%s) pacote %s não pode ser reconfigurado pois %s não está pronto (status atual '%s') pacote %s contem lista como arquivo de informação o pacote %s requer reinstalação, mas será removido conforme solicitado pacote %s requer re-instalação, não será removido pacote %s estava em espera, processando conforme solicitado pacote '%s' indisponível pacote '%s' não está instalado pacote '%s' não está instalado e nenhuma informação está disponível arquitetura do pacote arquitetura do pacote (%s) não combina com a do sistema (%s) pacote contém nome de arquivo de controle demasiadamente longo (iniciando '%.50s') informação de controle do pacote continha diretório '%.250s' remoção do diretório de informação de controle do pacote '%.250s' não disse que não se tratava de um diretório extração de informações de controle de pacotes pacote desvia outros para: %s
 valor do campo do pacote de extração arquivo de pacote MD5 checksum extração do arquivo do sistema de arquivos do pacote pacote tem campo "%s" mas é de arquitetura todos pacote tem campo '%s' mas falta a arquitetura pacote possui estado %s mas há gatilhos em espera pacote possui estado %s mas há gatilhos pendentes pacote possui estado "triggers-awaited" mas não há gatilhos em espera pacote possui estado "triggers-pending" mas não há gatilhos pendentes pacote não pode conter novas linhas nome pacote nome do pacote possui caracteres que não são alfanuméricos em caixa-baixa ou '-+.' pacote na linha %d não está no banco de dados: %.250s verificação de assinatura de pacotes número de versão do pacote analisando arquivo '%.255s' próximo à linha %d pacote '%.255s':
 %.255s analisando arquivo '%.255s' próximo à linha %d:
 %.255s parte %d está faltando arquivo parcial '%.250s' não é um arquivo simples tamanho da parcial é muito grande ou não é positivo tamanho deve ser pelo menos %d  KiB (para permitir o cabeçalho) passou
 colar problema de pré-dependência - %.250s não será instalado prioridade prioridade deve ser um inteiro prioridade de %s está fora do intervalo: %s prioridade de %s: %s erro de leitura em %.250s erro de leitura em '%.250s' erro de leitura no arquivo de configuração '%.255s' erro de leitura na entrada padrão erro de leitura em stdin no prompt do arquivo de configuração ("conffile") lendo lista de arquivos por pacote '%.250s' lendo arquivo de informação do pacote '%.255s' lendo arquivo de substituição de estado '%.250s' arquivo de pacote remontado sobre %s contendo %s, problema de pré-dependência:
%s acerca de %s contendo %s:
%s remoção de %.250s removendo arquitetura '%s' atualmente em uso pelo banco de dados removendo manualmente alternativa selecionada - trocando %s para modo automático renomear envolve sobrescrever '%s' com
  arquivo diferente '%s', não permitido rename: remove ligações velhas duplicadas '%s' renomeando link %s de %s para %s renomeando ligação dependente %s de %s para %s a operação solicitada requer privilégios de superusuário rm comando para limpeza diretório raiz ou nulo é listado como um arquivo de configuração ("conffile") procurei, mas não achei pacotes (arquivos casando com *.deb) <desconhecido> definindo seleção automática de %s muitas entradas de informação do pacote foram encontradas, somente uma é permitida comando de shell para mover arquivos Exibindo arquivo no paginador ignorando criação de %s pois arquivo associado %s (do grupo de ligação %s) não existe arquivo dependente ligação dependente ligação escrava é a mesma que a ligação principal %s nome de dependência arquivo fonte '%.250s' não é um arquivo simples reunião de pacote divididos arquivo "statoverride" contém linha em branco está faltando uma quebra de linha no final do arquivo statoverride estado Registrador do status removendo / do final sub-processo %s falhou com código de estado de espera %d sub-processo %s retornou estado de saída de erro %d subprocesso %s foi interrompido subprocesso %s foi finalizado pelo sinal (%s)%s tamanho da ligação simbólica '%.250s' foi alterado de %jd para %zd erro de sintaxe no arquivo de gatilhos de arquivo '%.255s' erro de sintaxe no arquivo statoverride erro de sintaxe no arquivo de gatilhos adiados '%.250s' no caractere '%s'%s possui um argumento, o nome da trigger alvo é um diretório - não é possível pular a checagem do arquivo de controle existem diversas versões da parte %d - pelo menos '%.250s' e '%.250s' não há script na nova versão do pacote - desistindo para retornar para as atualizações automáticas, use '%s --auto %s' poucos valores no campo de detalhes de arquivo '%s' (comparado a outros) muitos erros, parando muitos valores no campo de detalhes de arquivo '%s' (comparado a outros) linha muito longa ou nova linha faltando em '%.250s' erro de sintaxe no arquivo de gatilhos interessados '%.250s'; nome de pacote ilegal '%.250s': %.250s nome de gatilho contém caracteres inválidos registros de gatilhos ainda não existem disparado por gatilho área de gatilhos arquivo de gatilhos ci '%.250s' contém sintaxe de gatilho ilegal no nome do gatilho '%.250s': %.250s arquivo de gatilhos ci contém diretiva desconhecida '%.250s' arquivo de gatilhos ci contém diretiva de sintaxe desconhecida diretório de dados dos gatilhos ainda não foi criado gatilhos em "loop", abandonado arquivo de gatilhos adiados truncado '%.250s' tentando script do novo pacote em vez de... tentando sobrescrever '%.250s', que também está no pacote %.250s %.250s tentando sobrescrever '%.250s', que é a versão desviada de '%.250s' tentando sobrescrever '%.250s', que é a versão desviada de '%.250s' (pacote: %.100s) tentando sobrescrever diretório '%.250s' no pacote %.250s %.250s com não diretório tentando sobrescrever compartilhamento '%.250s', que é diferente de outras instâncias do pacote %.250s dois comandos especificados: --%s and --%s impossível (re)abrir arquivo parcial de entrada '%.250s' impossível acessar área de estado do dpkg impossível acessar a área de estados do dpkg para uma completa atualização de disponibilidade indiponível alterar a propriedade de arquivo de destino  '%.250s' impossível verificar a existência de '%.250s' indiponível conferir o arquivo '%s' olhar o status impossível checar a existência do arquivo '%.250s' impossível executar "chown" da ligação simbólica de backup para '%.255s' impossível limpar a sujeira em torno de '%.255s' antes de instalar outra versão não é possível fechar o arquivo '%s' impossível fechar novo arquivo de gatilhos adiados '%.250s' impossível fechar estado atualizado de '%.250s' impossível criar '%.255s' não foi possível criar o arquivo '%s' não foi possível criar novo arquivo '%.250s' não foi possível criar o diretório temporário. impossível criar diretório de estado dos gatilhos '%.250s' impossível excluir arquivo de informações de controle '%.250s' não foi possível apagar o diretório antigo '%.250s': %s impossível excluir arquivo "depot" acima usado '%.250s' impossível descartar '%.250s' não foi possível executar %s (%s) impossível preencher %.250s com enchimento impossível executar "flush" %.250s após enchimento não é possível de limpar o arquivo '%s' não foi possível esvaziar novo arquivo '%.250s' impossível executar "flush" do estado atualizado de '%.250s' impossível executar "fstat" no arquivo parcial '%.250s' impossível executar "fstat" no arquivo fonte impossível executar "fsync" do estado atualizado de '%.250s' incapaz de obter descritor de arquivo para o diretório '%s' incapaz de ignorar o sinal %s antes de executar %.250s impossível instalar '%.250s' como '%.250s' impossível instalar arquivo info (supostamente) novo '%.250s' impossível instalar novo arquivo info '%.250s' como '%.250s' impossível instalar novo arquivo de gatilhos adiados '%.250s' impossível instalar nova versão de '%.255s' impossível instalar estado atualizado de '%.250s' não é possível bloquear %s impossível criar ligação de backup de '%.255s' antes de instalar nova versão impossível criar ligação simbólica de backup para '%.255s' impossível mover do caminho '%.255s' para instalar nova versão indiponível abrir '%.255s' não é possível abrir diretório '%s' não foi possível abrir o arquivo '%s' impossível abrir arquivo com lista de arquivos do pacote '%.250s' incapaz de trancar o arquivo %s para teste impossível abrir novo arquivo "depot" '%.250s' impossível abrir arquivo de saída '%.250s' impossível abrir arquivo fonte '%.250s' impossível abrir diretório de controle temporário impossível abrir arquivo de gatilhos ci '%.250s' impossível abrir arquivo de gatilhos adiados '%.250s' impossível abrir/criar novo arquivo de gatilhos adiados '%.250s' impossível abrir/criar arquivo de bloqueio do banco de dados de estados impossível abrir/criar arquivo de bloqueio de gatilhos '%.250s' impossível ler o diretório "depot" '%.250s' impossível ler arquivo de gatilhos de arquivo '%.250s' impossível ler as "flags" do descritor de arquivos para %.250s impossível ler ligação '%.255s' impossível ler arquivo parcial '%.250s' não foi possível remover '%s' indiponível de retirar cópia do  '%.250s' não foi possível remover o arquivo copiado '%s' impossível remover versão recém-extraída de '%.250s' impossível remover versão recém-instalada de '%.250s' não consigo remover versão recém-instalada de '%.250s' para permitir a reinstalação da cópia de backup impossível remover arquivo info obsoleto '%.250s' impossível renomear novo arquivo "depot" de '%.250s' para '%.250s' impossível reabrir arquivo parcial '%.250s' impossível restaurar versão de backup de '%.250s' não foi possível remover com segurança '%.250s' não foi possível remover com segurança '%.255s' não foi possível remover com segurança o arquivo antigo '%.250s': %s impossível executar "seek" no começo de %.250s após enchimento incapaz de estabelecer buffer no arquivo de banco de dados %s impossível definir a "flag" 'close-on-exec' para %.250s impossível definir permissões de execução em '%.250s' indiponível definir o modo de arquivo de destino  '%.250s' impossível definir o proprietário do diretório de estado dos gatilhos '%.250s' incapaz de script setenv para mantenedor não foi possível usar o comando setenv para subprocesso impossível executar "stat" em %s '%.250s' não foi possível obter os atributos de %s '%.250s': %s impossível executar "stat" em '%.250s' impossível executar "stat" de '%.255s' (que eu estava prestes a instalar) não foi possível obter os atributos do diretório de controle impossível executar "stat" no arquivo de configuração ("conffile") atualmente instalado '%.250s' não foi possível usar o stat no nome do arquivo '%.250s' não foi possível obter os atributos da lista de arquivos do pacote '%.250s' indiponível iniciar a nova distribuição conflito '%.250s' impossível executar "fstat" em outro arquivo novo '%.250s' impossível executar "stat"'%.255s' restaurado antes de instalar outra versão indiponível executar arquivo de origem  '%.250s' impossível executar "stat" no arquivo de gatilhos adiados '%.250s' não é possível sincronizar diretório '%s' indiponível sincronizar o arquivo  '%.255s' não é possível sincronizar o arquivo '%s' não foi possível sincronizar novo arquivo '%.250s' impossível truncar o estado atualizado de '%.250s' não é possível bloquear %s não foi possível gravar o arquivo '%s' não foi possível gravar novo arquivo '%.250s' impossível escrever novo arquivo de gatilhos adiados '%.250s' não foi possível escrever no estado fd %d impossível escrever estado atualizado de '%.250s' aspas incorretas em '%s' falha inesperada no bzip2 dado inesperado após pacote e seleção na linha %d fim de arquivo não esperado em %.250s fim de arquivo inesperado em %s em %.255s fim inesperado de arquivo ou fluxo fim de arquivo inesperado ao tentar ler %s fim inesperado da entrada fim de linha inesperado após nome do pacote na linha %d fim de linha inesperado no nome do pacote na linha %d fim de linha não esperado no arquivo statoverride argumento desconhecido '%s' estratégia de compressão desconhecida estratégia de compressão desconhecida '%s'! tipo de compressão desconhecido '%s'! opção de forçar/recusar '%.*s' desconhecida opção desconhecida '%s' opção desconhecida -%c opção desconhecida --%s estado procurado desconhecido na linha %d: %.250s desempacotado mas não configurado preconfiguração de compressão não suportada opções no cabeçalho do arquivo não suportadas tipo de verificação de integridade não suportado diretório de atualizações contém arquivo '%.250s' cujo nome é muito longo (tamanho=%d, máx=%d) diretório de atualizações contém arquivos com nomes de tamanho diferentes (%d e %d) nome do campo '%.*s' definido pelo usuário é muito curto falha na verificação do pacote %s! falha na verificação do pacote %s, mas está sendo instalado conforme solicitado <nenhum> versão %.250s de %.250s já instalada, abortando número da versão não começa com dígito string de versão possui espaços embutidos string de versão está vazia espera por subprocesso %s falhou aviso durante a leitura %s: %s durante a remoção de %.250s, o diretório '%.250s' não estava vazio, portanto não foi removido enquanto removia %.250s, não foi possível remover o diretório '%.250s': %s - pode ser um ponto de montagem? não irá rebaixar versão de %.250s de %.250s para %.250s, abortando sim, irá desconfigurar %s (quebrado por %s) sim, %s será removido em favor de %s sim/não em campo booleano você não tem permissão para bloquear o banco de dados de estados do dpkg você deve especificar pacotes com seus próprios nomes, não pela citação dos nomes dos arquivos nos quais eles vêm 