��   �  0   �  #       �!     �!  �!      $     $     $     -$     <$     V$     h$     q$     �$  (   �$  (   �$  (   �$  %   %  +   9%     e%     z%     �%     �%     �%     �%     �%  ,   �%     �%      �%     &     1&     P&     n&     �&  ,   �&  (   �&  '   �&  2   '  /   Q'  *   �'  0   �'  '   �'  $   (      *(  2   K(  	   ~(     �(     �(  "   �(     �(     �(     )     ,)  +   D)     p)  2   �)  3   �)  ?   �)     **  	   2*     <*     V*     p*     �*     �*     �*     �*  	   �*     �*     �*     +  ,   +     <+     U+  !   p+  /   �+  +   �+  W   �+  #   F,     j,     {,     �,  !   �,     �,  	   �,  5   �,     #-     <-  O   M-  �   �-  *   T.     .     �.  -   �.     �.  1   �.     //     L/     j/     �/     �/     �/  3   �/  &   0     <0     U0  4   t0  #   �0  3   �0     1      1     :1     J1     ^1     u1     �1     �1     �1  0   �1     �1  O   �1  /   B2  $   r2  #   �2  	   �2     �2     �2     �2  1   3  .   53     d3     }3     �3     �3     �3  (   �3     4     )4     /4  I   I4  9   �4     �4     �4     �4  !    5     "5     95     E5     `5  
   w5  	   �5  "   �5     �5     �5  <   �5     6  *   "6     M6     ]6  &   l6     �6     �6     �6     �6  ?   �6     7     -7  0   E7     v7     �7     �7     �7  1   �7     8  (    8  *   I8     t8     �8     �8  	   �8     �8  4   �8  $   9  #   (9     L9  0   X9  4   �9     �9  #   �9      �9     :  ]   =:     �:  	   �:  	   �:  	   �:  "   �:     �:  )   ;     +;  '   A;     i;     �;     �;  !   �;  %   �;  %    <  )   &<      P<     q<     �<     �<  '   �<     �<  *   �<     !=     <=     Z=  #   r=  %   �=     �=  !   �=  -   �=  %   ">     H>  #   h>  +   �>  .   �>     �>  $   ?  ,   &?     S?  $   q?     �?     �?     �?     �?     @     @     +@     <@     U@  /   r@     �@     �@     �@  $   �@     A     *A     HA  /   cA     �A  $   �A     �A     �A     �A     B     %B     BB  &   ZB     �B     �B      �B     �B  $   �B  ,   C     1C     KC     WC  '   sC  #   �C  <   �C     �C     D     'D     @D     [D     uD     �D     �D     �D     �D     �D     �D     E     E     7E     UE     jE     yE     �E     �E     �E     �E     �E     �E     F     F  $   *F     OF     bF  )   }F  /   �F  /   �F     G     G     &G     8G     JG  (   iG  !   �G     �G     �G     �G     �G     H     (H  (   FH  !   oH  $   �H     �H  "   �H  #   �H     I     2I     BI  &   SI     zI     }I     �I     �I     �I     �I     �I     �I     �I     J     J     ,J     ?J     VJ     pJ     �J     �J     �J     �J  $   �J     �J     �J     K     K  -   ,K  1   ZK     �K     �K  J   �K     L  %   L  M   :L  =   �L  2   �L  ,   �L  -   &M  .   TM  *   �M  G   �M  9   �M  D   0N  ^   uN  <   �N  ;   O  5   MO  1   �O  E   �O  a   �O  9   ]P  \   �P  L   �P  P   AQ  %   �Q  +   �Q  3   �Q  R   R  ;   kR  $   �R  %   �R  1   �R  ;   $S     `S  4   ~S     �S     �S     �S     �S     T     %T     BT     ZT     gT  (   wT  '   �T  $   �T     �T  �  �T     �V     �V     �V      W     8W     JW     SW     bW     wW     �W  (   �W  %   �W     �W     �W     
X     X     X     "X     /X     <X  )   DX     nX  1   �X  "   �X  .   �X  ,   Y     =Y  (   SY  2   |Y  .   �Y  ,   �Y  8   Z  3   DZ  .   xZ  2   �Z  +   �Z  ,   [  (   3[  :   \[  	   �[     �[     �[  '   �[     \  #   \  #   @\  "   d\  ,   �\     �\  G   �\  3   ]  <   E]     �]     �]  "   �]  "   �]  "   �]     �]  %   ^     ?^     S^     h^     w^  	   �^     �^  9   �^  $   �^  3   �^  ,   /_  A   \_  :   �_  ^   �_  >   8`     w`     �`  !   �`  3   �`      a  	   a  C   $a     ha     �a  Z   �a  �   �a  )   �b  %   �b     c  4   .c     cc  /   �c  !   �c      �c  #   �c  '   d  !   ?d     ad  <   }d  '   �d     �d     �d  @   e  $   ^e  ?   �e     �e  2   �e     f     #f     @f     [f     yf  
   �f     �f  :   �f     �f  h   �f  ;   ag  +   �g  .   �g     �g     h     h  )   )h  9   Sh  >   �h  !   �h     �h  $   i     2i  "   Hi  ,   ki     �i     �i     �i  [   �i  B   3j  	   vj     �j     �j  $   �j     �j     �j  %   �j     %k     Ek  	   Xk  -   bk  !   �k  &   �k  A   �k     l  1   (l     Zl     nl  1   �l     �l      �l     �l     �l  M   m     Rm     mm  9   �m  '   �m     �m     n     )n  3   Fn     zn  I   �n  0   �n     o     o     4o     Co     Wo  G   wo  -   �o  .   �o  	   p  ?   &p  9   fp  #   �p  4   �p  4   �p      .q  e   Oq     �q     �q     �q     �q  "   �q     r  9   *r     dr  8   �r     �r  -   �r  %   s  .   )s  ?   Xs  0   �s  4   �s  *   �s  $   )t  (   Nt  )   wt  7   �t     �t  <   �t  )   *u  (   Tu  +   }u  +   �u  5   �u  '   v  3   3v  =   gv  5   �v  2   �v  6   w  G   Ew  I   �w  )   �w  8   x  A   :x  -   |x  1   �x  )   �x  -   y  *   4y  %   _y     �y     �y     �y  %   �y  )   z  ?   -z  -   mz     �z  *   �z  3   �z  +   {  *   D{  "   o{  >   �{  '   �{  ;   �{     5|  *   T|  4   |  %   �|  /   �|     
}  -   )}     W}  +   r}  !   �}     �}  +   �}  3   �}     3~     N~     Z~  #   w~  /   �~  T   �~           6  !   I  )   k     �     �     �     �     �     �     2�     B�     ]�  )   s�  !   ��     ��     ۀ  +   �     �     *�     D�     V�     e�     ��     ��     ��  -   ΁     ��  )   �  9   A�  C   {�  3   ��     �     
�     �     2�  1   G�  9   y�  1   ��     �     �      �  '   7�     _�  ,   �  8   ��  0   �  ;   �     R�  )   o�  (   ��  '        �     ��  -   �     @�     E�     `�     p�  +   ��     ��     Ɇ     ݆     ��     �     #�     <�  "   W�  )   z�  %   ��     ʇ     �     ��     �  6    �     W�     ^�     }�     ��  4   ��  ;   �     �  *   ,�  d   W�     ��  .   ҉  ]   �  R   _�  A   ��  >   �  =   3�  >   q�  2   ��  P   �  F   4�  Y   {�  m   Ռ  N   C�  G   ��  =   ڍ  :   �  Y   S�  o   ��  B   �  f   `�  O   Ǐ  c   �  )   {�  *   ��  7   А  _   �  D   h�  *   ��  8   ؑ  6   �  S   H�     ��  C   ��      ��  "    �     C�  !   U�  *   w�  *   ��     ͓     �     ��  -   �  -   @�  1   n�     ��     g   ?   �   	  �   M         �   �           '            �   u   &  �         `  W          R      
     �  �   �   h               �   �     �   �   �   �       �       X   �   *  G   (                    �          C   �   q          �  �       f      �   e  2      �   @  �      �   K  P      g  e   5   �   I  �   D  �   �   w            }   n   �   {  �  K   0  Z       �  r      �   p       2   �      B   �       5  {   >  *   �   /   �     �       G          �   ~      F            [       �   &   )  ,                f   H  �   �   �  �           �   �       �   �   E   -  I   V      �   J      }    t      �              S  c   �       �   �   	   ^                �       �   �       b  Z  #   .   �       \  0   /  �     o  m   �   w       Q  �       _       v                 ;       X                  �   �       C      7       �   F   �   �  �         �      d  
      E      �   �   M  s   �   �               �   r   �   $               �   )       h  �   "   >                   %           �      �   N   ]    8          ^      |           �           x   �   ~      Q      �   A       8       P  �      =   �   y   y      s  n  �   �   �  +  !                  ;  �   U  �       �     �       �   A  a  �           �   m  �     W          �   3   4       t   R   �   l  T   �       |         #    �   =         �       $      �   o          d   �   �   u      �           �       �      :  �   l   3    v  �                 -   �   �   O   %  �   b           7  �       �         j  +   �   �   �   L  :   k  `   �  �           i               �  ,   �  �  c    ?  j   <   9      �       �       �                '       �   Y   �       �   D   V   �   T            N       �   6  _              �       a   �   x  4      6   U           k       �   B  z      �       �   9   �   �   .  �       �   �   z       \            [          �   Y  q   @   ]                 �  i  <  �       !  S       1  �   1   �   H            �   "  J   L         (   O  p        ��     ��     ��  "  ("  <"  P"  d"  x"  �"  �"  �"  �"  �"  �"  #   #  4#  H#  \#  p#  �#  �#  �#  �#  �#  �#  ��            ����Ȕ  	         �����  	         �����           ����(�           ����<�                   ����I�            ����T�            ����a�            ����i�            ����s�            ����y�            ������            ������           ������            �����           �����           �����                   ����"�            ����-�            ����:�            ����B�            ����L�            ����R�            ���� 

Symbols from %s:

            %s..%s
     Build ID:      Linker version: %.*s
     OS: %s, ABI:      PC:      Provider:    %#06x: Parent %d: %s
   Data:                              %s
   Machine:                           %s
   OS/ABI:                            %s
   Type:                                 Version:                           %d %s
  ([0] not available)  Args:   Base:   Name:   Semaphore:   [%*zu] ???
  copy  error while freeing sub-ELF descriptor: %s
 %s %s diff: ELF header %s %s diff: program header count %s %s diff: section count %s %s differ: build ID content %s %s differ: build ID length %s %s differ: gap %s %s differ: program header %d %s %s differ: section [%zu,%zu] '%s' content %s %s differ: section [%zu] '%s' content %s %s differ: section [%zu] '%s' header %s %s differ: section [%zu] '%s' note '%s' content %s %s differ: section [%zu] '%s' note '%s' type %s %s differ: section [%zu] '%s' note name %s %s differ: section [%zu] '%s' number of notes %s %s differ: section [%zu], [%zu] name %s %s differ: symbol table [%zu,%zu] %s %s differ: symbol table [%zu] %s %s differ: unequal amount of important sections %s in %s
 %s is no regular file %s%s%s%s: Invalid operation %s%s%s: file format not recognized %s%s%s: no symbols %s: File format not recognized %s: no entry %s in archive!
 %s: not an archive file '%c' is only meaningful with the 'x' option '%s' is no archive '%s' is not an archive, cannot print archive index 'N' is only meaningful with the 'x' and 'd' options 'a', 'b', and 'i' are only allowed with the 'm' and 'r' options (bytes) (current) -F option specified twice -f option specified twice -o option specified twice .debug_line section missing .debug_ranges section missing <INVALID SECTION> <INVALID SYMBOL> <unknown> <unknown>: %d ADDRESS ARCHIVE Allow filename to be truncated if necessary. Also show function names Also show line table flags Also show symbol or section names Always set DT_NEEDED for following dynamic libs Be extremely strict, flag level 2 features. Binary has been created with GNU ld and is therefore known to be broken in certain ways Binary is a separate debuginfo file CORE (Core file) COUNT parameter required Callback returned failure Callbacks missing for ET_REL file Command Modifiers: Commands: Compare relevant parts of two ELF files for equality. Contents of section %s:
 Control options: Control treatment of gaps in loadable segments [ignore|match] (default: ignore) Copyright (C) %s Red Hat, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 Create, modify, and extract from archives. DYN (Shared object file) Delete files from archive. Display assembler code of executable sections Display content of archive. Display dynamic symbols instead of normal symbols Display only defined symbols Display only external symbols Display only undefined symbols Display relocation information. Display the ELF file header Display the dynamic segment Display the full contents of all sections requested Display the symbol index of an archive Display the symbol table Display versioning information Do not find symbol names for addresses in DWARF data Do not print anything if successful Do not replace existing files with extracted files. Do not sort the symbols ELF file does not match build ID ELF file opened ELF version not set EXEC (Executable file) Extract files from archive. FILE FILE... FILE1 FILE2 Find addresses from signatures found in COREFILE Find addresses in FILE Find addresses in files mapped as read from FILE in Linux /proc/PID/maps format Find addresses in files mapped into process PID Find addresses in the running kernel Force regeneration of symbol table. I/O error INVALID SECTION INVALID SYMBOL Ignore differences in build ID Ignore permutation of buckets in SHT_HASH section Include index for symbols from archive members Input selection options: Insert file after [MEMBER]. Insert file before [MEMBER]. Invalid format: %s Invalid number of parameters.
 Invalid value '%s' for --gaps parameter. Kernel with all modules LEVEL LZMA decompression failed Locate source files and line information for ADDRs (in a.out by default). MEMBER parameter required for 'a', 'b', and 'i' modifiers Miscellaneous: Missing file name.
 Mode selection: More than one operation specified Move files in archive. NONE (None) No DWARF information found No ELF program headers No backend No errors No modules recognized in core file No operation specified.
 No symbol table found Not an ELF file - it has the wrong magic bytes at the start
 OFFSET Output all differences, not just the first Output control: Output format: Output nothing; yield exit status only Output options: Output selection options: Output selection: PATH Pedantic checking of ELF files compliance with gABI/psABI spec. Preserve original dates. Print files in archive. Print name of the input file before every symbol Print size of defined symbols Provide verbose output. Quick append files to archive. REL (Relocatable file) Replace existing or insert new file into archive. Same as -b. Search path for separate debuginfo files Section syntax requires exactly one module See dwarf_errno See ebl_errno (XXX missing) See elf_errno See errno Shared library: [%s]
 Show absolute file names using compilation directory Show only base names of source files Sort symbols numerically by address Stand alone Suppress message when library has to be created. Treat addresses as offsets relative to NAME section. Unsupported relocation type Update only older files in archive. Use full path for file matching. Use instance [COUNT] of name. Use the output format FORMAT.  FORMAT can be `bsd', `sysv' or `posix'.  The default is `sysv' Written by %s.
 [ADDR...] [FILE...] [FILE]... [MEMBER] [COUNT] ARCHIVE [FILE...] address out of range address range overlaps an existing module archive name required archive/member file descriptor mismatch bzip2 decompression failed cannot add new section: %s cannot change mode of %s cannot change mode of output file cannot change modification time of %s cannot create EBL descriptor for '%s' cannot create ELF descriptor for '%s': %s cannot create ELF descriptor: %s cannot create hash table cannot create new file cannot create output file cannot determine number of sections: %s cannot disassemble cannot find debug file for module '%s': %s cannot find kernel modules cannot find kernel or modules cannot find symbol '%s' cannot generate Elf descriptor: %s
 cannot get ELF descriptor for %s: %s
 cannot get ELF header cannot get ELF header of '%s': %s cannot get content of section %zu in '%s': %s cannot get content of section %zu: %s cannot get data for symbol %zu
 cannot get data for symbol section
 cannot get program header count of '%s': %s cannot get program header entry %d of '%s': %s cannot get relocation: %s cannot get section count of '%s': %s cannot get section header of section %zu: %s cannot get symbol in '%s': %s cannot handle DWARF type description cannot insert into hash table cannot load data of '%s': %s cannot load kernel symbols cannot manipulate null section cannot open %.*s cannot open %s cannot open '%s' cannot open archive '%s' cannot open archive '%s': %s cannot open debug file '%s' for module '%s': %s cannot open input file cannot read %s: %s cannot read ELF core file: %s cannot read ELF header of %s(%s): %s cannot read ELF header: %s cannot read content of %s: %s cannot read data from file cannot read note section [%zu] '%s' in '%s': %s cannot rename output file cannot rename temporary file to %.*s cannot stat %s cannot stat '%s' cannot stat archive '%s' cannot write data to file cannot write output file: %s command option required corrupt .gnu.prelink_undo section data data/scn mismatch debug information too big descriptor is not for an archive duplicate symbol e_ident[%d] == %d is no known class
 e_ident[%d] == %d is no known data encoding
 e_ident[%zu] is not zero
 empty block error during output of data error while closing Elf descriptor: %s
 executable header not created first executables and DSOs cannot have zero program header offset
 failed reading '%s': %s failed to write %s file descriptor disabled file has no program header gzip decompression failed image truncated input file is empty invalid .debug_line section invalid CFI section invalid COUNT parameter %s invalid DWARF invalid DWARF version invalid ELF file invalid ELF header size: %hd
 invalid SDT probe descriptor
 invalid `Elf' handle invalid access invalid address range index invalid archive file invalid binary class invalid command invalid data invalid directory index invalid encoding invalid file invalid file descriptor invalid fmag field in archive header invalid line index invalid machine flags: %s
 invalid number of program header entries
 invalid number of program header table entries
 invalid number of section header table entries
 invalid offset invalid operand invalid operation invalid parameter invalid program header offset
 invalid program header position or size
 invalid program header size: %hd
 invalid reference value invalid section invalid section alignment invalid section entry size invalid section header invalid section header index
 invalid section header position or size
 invalid section header size: %hd
 invalid section header table offset
 invalid section index invalid section type for operation invalid size of destination operand invalid size of source operand invalid version memory exhausted more than one dynamic section present
 no no DWARF information no ELF file no address value no backend support available no block data no constant value no entries found no entry %s in archive
 no error no flag value no index available no location list value no matching address range no matching modules found no reference value no regular file no string data no such file no support library found for machine none not a valid ELF file not implemented offset out of range only one of -e, -p, -k, -K, or --core allowed only relocatable files can contain section groups out of memory position member %s not found program header only allowed in executables, shared objects, and core files r_offset is bogus relocation refers to undefined symbol section [%2d] '%s': _GLOBAL_OFFSET_TABLE_ symbol refers to bad section [%2d]
 section [%2d] '%s': cannot get dynamic section entry %zu: %s
 section [%2d] '%s': cannot get relocation %zu: %s
 section [%2d] '%s': cannot get section data
 section [%2d] '%s': cannot get symbol %d: %s
 section [%2d] '%s': cannot get symbol %zu: %s
 section [%2d] '%s': chain array too large
 section [%2d] '%s': entry %zu: %s value must point into loaded segment
 section [%2d] '%s': entry size does not match Elf32_Word
 section [%2d] '%s': extended index table too small for symbol table
 section [%2d] '%s': extended section index in section [%2zu] '%s' refers to same symbol table
 section [%2d] '%s': hash bucket reference %zu out of bounds
 section [%2d] '%s': hash chain reference %zu out of bounds
 section [%2d] '%s': invalid destination section type
 section [%2d] '%s': mandatory tag %s not present
 section [%2d] '%s': non-DSO file marked as dependency during prelink
 section [%2d] '%s': referenced as string table for section [%2d] '%s' but type is not SHT_STRTAB
 section [%2d] '%s': relocation %zu: offset out of bounds
 section [%2d] '%s': relocation %zu: only symbol '_GLOBAL_OFFSET_TABLE_' can be used with %s
 section [%2d] '%s': section group [%2zu] '%s' does not precede group member
 section [%2d] '%s': section with SHF_GROUP flag set not part of a section group
 section [%2d] '%s': sh_info not zero
 section [%2d] '%s': sh_info should be zero
 section [%2d] '%s': symbol %zu: invalid name value
 section [%2d] '%s': symbol table cannot have more than one extended index section
 section [%2u] '%s': entry size is does not match ElfXX_Sym
 section `sh_size' too small for data section header table must be present
 symbol 0 should have zero extended section index
 text relocation flag set but there is no read-only segment
 the archive '%s' is too large unknown ELF header version number e_ident[%d] == %d
 unknown SDT version %u
 unknown data encoding unknown error unknown machine type %d
 unknown object file type %d
 unknown object file version
 unknown option '-%c %s' unknown type unknown version unsupport ABI version e_ident[%d] == %d
 unsupported OS ABI e_ident[%d] == '%s'
 update() for write on read-only file yes Project-Id-Version: elfutils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-01-11 08:48+0100
PO-Revision-Date: 2013-09-23 23:36+0000
Last-Translator: gabriell nascimento <gabriellhrn@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 18:10+0000
X-Generator: Launchpad (build 18115)
 

Símbolos de %s:

            %s..%s
     ID de compilação:      Versão do vinculador: %.*s
     SO: %s, ABI:      PC:      Provedor:    %#06x: Pai %d: %s
   Dados: %s
   Máquina: %s
   OS/ABI:                            %s
   Tipo:                                 Versão: %d %s
  ([0] indisponível)  Args:   Base:   Nome:   Semáforo:   [%*zu] ???
  copiar  erro ao liberar o descritor sub-ELF: %s
 %s %s diferem: cabeçalho ELF %s %s diferem: contagem do cabeçalho de programa %s %s diferem: contagem de seção %s %s diferem: conteúdo da ID de compilação %s %s diferem: tamanho da ID de compilação %s %s diferem: lacuna %s %s diferem: cabeçalho de programa %d %s %s diferem: seção [%zu,%zu] conteúdo de '%s' %s %s diferem: seção [%zu] conteúdo de '%s' %s %s diferem: seção [%zu] cabeçalho '%s' %s %s diferem: seção [%zu] '%s' conteúdo da nota '%s' %s %s diferem: seção [%zu] '%s' tipo da nota '%s' %s %s diferem: seção [%zu] '%s' nome da nota %s %s diferem: seção [%zu] '%s' número de notas %s %s diferem: seção [%zu], nome de [%zu] %s %s diferem: tabela de símbolos [%zu,%zu] %s %s diferem: tabela de símbolos [%zu] %s %s diferem: quantidade desigual de seções importantes %s em %s
 %s não é um arquivo regular %s%s%s%s: operação inválida %s%s%s: formato de arquivo desconhecido %s%s%s: sem símbolos %s: Formato de arquivo desconhecido %s: nenhuma entrada %s no arquivo!
 %s: não é um arquivo compactado. '%c' significativo somente com a opção 'x' '%s' não é arquivo '%s' não é um arquivo, não foi possível imprimir índice de arquivo 'N' significativo somente com as opções 'x' e 'd' 'a', 'b', e 'i' somente permitidos com as opções 'm' e 'r' (bytes) (atual) opção -F especificada duas vezes opção -f especificada duas vezes opção -o especificada duas vezes secção .debug_line faltando seção .debug_ranges section ausente <SEÇÃO INVÁLIDA> <SÍMBOLO INVÁLIDO> <desconhecido> <desconhecido>: %d ENDEREÇO Arquivo Permitir modificação no nome de arquivo se necessário. Mostrar também os nomes da função Exibir também os sinalizadores da tabela de linhas Também mostrar nomes de símbolo ou seção Sempre definir DT_NEEDED para as seguintes bibliotecas dinâmicas Seja extremamente cuidadoso, sinalizador nível 2 atuando. Binário foi criado com GNU ld e por isto é conhecido por estar danificado em alguns aspectos Binário é um arquivo de informação de depuração distinto CORE (Arquivo nucleo) necessário parâmetro COUNT Chamada de retorno retornou falha Chamadas de retornos ausentes para o arquivo ET_REL Modificadores de comando: Comandos: Comparar partes relevantes de dois arquivos ELF para equivalência. Conteúdo da seção %s:
 Opções de controle: Controlar tratamento de lacunas em segmentos carregáveis [ignore|match] (padrão: ignore) Copyright (C) %s Red Hat, Inc. - Todos os direitos reservados.
Este software é livre; ver fonte para condições de distribuição. Garantia NÃO
disponível; incluindo FINS COMERCIAIS ou USO PARTICULAR.
 Criar, modificar, e extrair dos arquivos. DYN (Arquivo do objeto compartilhado) Apagar arquivos compactados. Mostrar o código assembler de seções executáveis Exibir conteúdo do arquivo. Exibir simbolos dinâmicos ao invés de normais Exibir somente simbolos definidos Exibir apenas símbolos externos Exibir apenas símbolos indefinidos Exibir informação de relocalização. Exibe o cabeçalho do arquivo ELF Exibir o segmento dinâmico Exibir o conteúdo integral de todas as seções solicitadas Exibir o índice de símbolo do arquivo Exibir a tabela de símbolo Exibir informação da versão Não encontrado nome de símbolo para o endereço em dados DWARF Não imprima nada em caso de sucesso Não substituir arquivos existentes com os arquivos extraídos. Não ordenar os símbolos arquivo ELF não corresponde ao ID de compilação arquivo ELF aberto versão do ELF não definida EXEC (Arquivo executável) Extrair arquivos compactados. ARQUIVO ARQUIVO... ARQUIVO1 ARQUIVO2 Encontre endereços de assinaturas encontradas em COREFILE Procurar endereços no ARQUIVO Encontrar endereços em arquivos mapeados como lidos a partir de ARQUIVO no formato Linux /proc/PID/maps Procurar endereços em arquivos mapeados no PID do processo Procurar endereços no kernel em execução Forçar restauração da tabela de caracteres. Erro de E/S SEÇÃO INVÁLIDA SÍMBOLO INVÁLIDO Ignorar diferenças de ID de compilação Ignorar alteração de preenchimentos na seção SHT_HASH Incluir sumário de simbolos dos membros do arquivo compactado Seleção de opções de entrada: Inserir arquivo após [MEMBER] Inserir arquivo após antes [MEMBER] Formato inválido: %s Número de parâmetros inválido.
 Valor inválido '%s' para parâmetro --gaps. Kernel com todos os módulos NÍVEL Falha ao descomprimir LZMA Localizar os arquivos de origem e informações de linha para ADDRs (em a.out por padrão). necessário parâmetro MEMBER para os modificadores 'a', 'b' e 'i' Diversos: Nome de arquivo em falta.
 Seleção de modo: Mais que uma operação especificada Mover arquivos compactados. NENHUM (Nenhum) Nenhuma informação DWARF encontrada Sem cabeçalhos de programa ELF Sem infraestrutura Sem erros Nenhum módulo reconhecido no arquivo central Nenhuma operação especificada.
 Nenhuma tabela de símbolos encontrada Não é um arquivo ELF - possui bits mágicos errados no início
 DESLOCAMENTO Gerar todas as diferenças, não apenas a primeia Controle de saída: Formato de saída Nenhuma saída; concedido status de saída apenas Opções de saída: Seleção de opções de saída: Seleção de saída: CAMINHO Verificação pedante de arquivos ELF atendem as especificações Gabi/psABI. Preservar datas originais. Imprimir arquivos compactados. Imprimir nome do arquivo de entrada antes de cada simbolo Imprimir tamanho de símbolos definidos Fornece uma saída detalhada. Anexar arquivos rapidamente. REL (Arquivo relocalocável) Substituir ou adicionar novos arquivos compactados. O mesmo que -b. Pesquisar caminho para arquivos de informações de depuração separados Sintaxe da secção requer exatamente um módulo Veja dwarf_errno Veja ebl_errno (XXX faltando) Veja elf_errno Ver número do erro Biblioteca compartilhada: [%s]
 Mostrar nomes absolutos de arquivos usando o diretório de compilação Mostrar nomes apenas a base de arquivos-fonte Ordenar símbolos numericamente pelo endereço Autônomo Não mostrar mensagem para criação necessária de biblioteca. Tratar endereços como offsets relativos à seção NAME. Tipo de realocação não suportado Atualizar somente arquivos compactados mais antigos. Usar caminho completo de pasta para indicar arquivo. Usar instância [COUNT] do nome. Use o formato de saída FORMAT. FORMAT pode ser 'bsd', 'sysv' ou 'posix'. O formato padrão é 'sysv' Escrito por %s.
 [ENDEREÇO...] [ARQUIVO...] [ARQUIVO]... [MEMBER] [COUNT] ARQUIVO [FILE...] endereço fora da faixa intervalo de endereço sobrepõe um módulo já existente nome do arquivo necessário incompatibilidade de descritor de arquivo arquivo/membro Falha ao descomprimir bzip2 não foi possível adicionar nova seção: %s não foi possível alterar modo de %s não pôde alterar o modo do arquivo de saída não foi possível alterar informações de modificação de %s não foi possível criar descritor EBL para '%s' não foi possível criar descritor ELF para '%s': %s Não foi possível criar descritor ELF: %s não foi possível criar tabela hash não foi possível criar um novo arquivo Não é possível criar arquivo de saída não foi possível determinar o número de seções: %s não pode desmontar não foi possível encontrar arquivo para o módulo '%s': %s não pôde procurar os módulos do kernel não pôde procurar o kernel ou módulos não foi possível procurar o símbolo '%s' não foi possível gerar descritor Elf: %s
 não foi possível obter o descritor ELF para %s: %s
 não foi possível obter cabeçalho ELF não foi possível obter cabeçalho ELF de '%s': %s não foi possível obter conteúdo de seção %zu em '%s': %s não foi possível obter conteúdo da seção %zu: %s não é possível obter dados para o símbolo %zu
 não é possível obter dados para a seção símbolo
 não foi possível obter contagem do cabeçalho de programa de '%s': %s não foi possível obter entrada do cabeçalho de programa %d de '%s': %s não foi possível obter realocação: %s não foi possível obter contagem de seção de '%s': %s não foi possível obter cabeçalho de seção da seção %zu: %s não foi possível obter símbolo em '%s': %s não consegue lidar com descrição do tipo DWARF não foi possível inserir na tabela hash não foi possível carregar dados de '%s': %s não pôde carregar os símbolos do kernel não pôde manipular uma seção nula não foi possível abrir %.*s não foi possível abrir %s não foi possível abrir "%s" não foi possível abrir arquivo '%s' não foi possível abrir arquivo '%s': %s não foi possível abrir o arquivo '%s' para o módulo '%s': %s não foi possível abrir o arquivo de entrada não foi possível ler %s: %s impossível ler núcleo do arquivo ELF: %s não foi possível ler cabeçalho ELF de %s(%s): %s não foi possível ler o cabeçalho ELF: %s não foi possível ler conteúdo de %s: %s não pôde ler os dados do arquivo não foi possível ler seção de notas [%zu] '%s' em '%s': %s não pôde renomear o arquivo de saída não foi possível renomear o arquivo temporário para %.*s impossível obter estado de %s não é possível verificar estado de '%s' não foi possível verificar o arquivo compactado %s não pôde gravar os dados no arquivo não foi possível gravar arquivo de saída: %s opção de comando necessária seção de dados .gnu.prelink_undo corrompida incompatibilidade data/scn informação de depuração é muito grande descritor não é para um arquivo Símbolo duplicado e_ident[%d] == %d não é classe conhecida
 e_ident[%d] == %d não são dados de codificação
 e_ident[%zu] não é zero
 bloco vazio erro durante saída de dados erro ao fechar o descritor Elf: %s
 cabeçalho executável não foi criado primeiro Executáveis ​​e DSOs não pode ter deslocamento de cabeçalho do programa zero
 falha ao ler '%s': %s falha ao gravar %s descritor do arquivo desabilitado arquivo não tem o cabeçalho do programa Falha ao descomprimir gzip imagem truncada arquivo de entrada está vazio secção .debug_line inválida seção CFI inválida parâmetro COUNT inválido %s DWARF inválido versão do DWARF inválida arquivo ELF inválido tamanho do cabeçalho ELF inválido: %hd
 descritor de sonda SDT inválido
 manipulador `Elf' inválido acesso inválido índice de intervalo de endereço inválido arquivo inválido classe binária inválida comando inválido dado inválido índice do diretório inválido codificação inválida arquivo inválido descritor de arquivo inválido campo fmag inválido no cabeçalho do arquivo linha de índice inválida sinalizadores de máquina inválidos: %s
 número inválido de entradas de cabeçalhos de programa
 número inválido de entradas na tabela de cabeçalhos do programa
 número de entradas da tabela de seção inválido
 deslocamento inválido operando inválido operação inválida parâmetro inválido deslocamento inválido de cabeçalho de programa
 posição ou tamanho do cabeçalho de programa inválido
 tamanho do cabeçalho de programa inválido: %hd
 valor de referência inválido seção inválida alinhamento da seção inválido tamanho inválido da entrada da seção seção de cabeçalho inválida índice de cabeçalhos da seção inválido
 tamanho ou posição do cabeçalho de seção inválido
 tamanho do cabeçalho de seção inválido: %hd
 deslocamento da tabela de cabeçalhos de seção inválido
 índice de seção inválido tipo de seção inválido para operação tamanho inválido do operando de destino tamanho inválido do operando de origem versão inválida memória insuficiente mais de uma seção dinâmica está presente
 não nenhuma informação DWARF sem arquivo ELF nenhum valor de endereço nenhum suporte a infraestrutura disponível sem bloco de dados sem valor constante nenhuma entrada encontrada nenhuma entrada %s no arquivo
 nenhum erro sem valor de sinalizador nenhum índice disponível lista de localização sem valores sem intervalo de endereço correspondente nenhum módulo compatível encontrado sem valor de referência sem arquivo regular não há dados string sem nenhum arquivo nenhuma biblioteca de suporte encontrada para máquina nenhum não é um arquivo ELF válido não implementado deslocamento fora do intervalo somente um de -e, -p, -k, -K, ou --core é permitido apenas arquivos realocáveis podem conter grupos de seção sem memória membro de posição %s não foi encontrado cabeçalho do programa aceito somente em executáveis, objetos compartilhados, e principais arquivos r_offset é inválido relocação se refere a símbolo não-definido seção [%2d] '%s': o símbolo _GLOBAL_OFFSET_TABLE_ se refere a uma seção inválida [%2d]
 seção [%2d] '%s': não foi possível obter entrada de seção dinâmica %zu: %s
 seção [%2d] '%s': não foi possível obter relocação %zu: %s
 seção [%2d] '%s': não foi possível obter dados da seção
 seção [%2d] '%s': não foi possível obter símbolo %d: %s
 seção [%2d] '%s': não foi possível obter símbolo %zu: %s
 seção [%2d] '%s': matriz de cadeia muito grande
 seção [%2d] '%s': entrada %zu: %s valor deve se referir ao segmento carregado
 seção [%2d] '%s': tamanho da entrada não corresponde ao Elf32_Word
 seção [%2d] '%s': tabela de índice estendido muito pequeno para a tabela de símbolos
 seção [%2d] '%s': índice de área estendida na seção [%2zu] '%s' refere-se à mesma tabela de símbolos
 seção [%2d] '%s': referência de preenchimento de hash %zu fora dos limites
 seção [%2d] '%s': referência de cadeia de hash %zu fora dos limites
 seção [%2d] '%s': destino inválido para o tipo de seção
 seção [%2d] '%s': rótulo obrigatório %s não presente
 seção [%2d] '%s': arquivo não-DSO marcado como dependência durante pré-vinculação
 Seção [%2d] '%s': referenciada como tabela de strings para a seção [%2d] '%s', mas tipo não é SHT_STRTAB
 seção [%2d] '%s': relocação %zu: deslocamento fora de limites
 seção [%2d] '%s': relocação %zu: somente o símbolo '_GLOBAL_OFFSET_TABLE_' pode ser usado com %s
 seção [%2d] '%s': grupo de sessões [%2zu] '%s' não precede membro de grupo
 seção [%2d] '%s': seção com grupo sinalizador SHF_GROUP não faz parte de um grupo de seções
 seção [%2d] '%s': sh_info não é zero
 seção [%2d] '%s': sh_info deve ser zero
 seção [%2d] '%s': símbolo %zu: valor nome inválido
 seção [%2d] '%s': símbolo da tabela não pode ter mais de uma seção estendida de índices
 seção [%2u] '%s': tamanho da entrada não corresponde a ElfXX_Sym
 seção `sh_size' muito pequena para dados tabela de cabeçalhos da seção precisa estar presente
 símbolo 0 deve ter índice zero na seção estendida
 sinalizador de relocação de texto setado, mas não há segmentos somente-leitura
 o arquivo '%s' é muito grande número da versão do cabeção ELF desconhecido e_ident[%d] == %d
 Versão %u  do SDT desconhecida
 codificação de dado desconhecida erro desconhecido tipo de máquina %d desconhecido
 tipo de arquivo de objeto %d desconhecido
 versão do arquivo de objeto desconhecida
 opção desconhecida '-%c %s' tipo desconhecido versão desconhecida versão ABI não suportada e_ident[%d] == %d
 ABI e_ident[%d] de SO == '%s' não suportado
 update() para escrever no arquivo somente leitura sim PRIx64 PRIxMAX PRIu32 <unknown>: %# offset %# lies outside section '%s' offset %# lies outside contents of '%s'  (% in [0].sh_size)  (% in [0].sh_link) %#0* <%s+%#> %s+%# <%s> %s+%#0* <%s> %# <%s> %#0* <%s> %s+%# %s+%#0* <desconhecido>: %# deslocamento %# fica fora da seção '%s' deslocamento %# fica fora do conteúdo de '%s'  (% in [0].sh_size)  (% in [0].sh_link) %#0* <%s+%#> %s+%# <%s> %s+%#0* <%s> %# <%s> %#0* <%s> %s+%# %s+%#0* 