��          �      L      �     �     �     �     �     �       3        M     [     v  7   �  ,   �     �          '  #   ?  
   c  	   n  �  x  #   V     z     |     �     �     �  D   �                 ;  @   \  A   �     �     �       "   8     [     j                                                 	                                     
              %d New Message %d New Messages : Compose New Message Contacts Evolution Indicator Inbox Only create notifications for new mail in an Inbox. Pla_y a sound Play a sound for new mail. Show a notification bubble. Show new message count in the message indicator applet. Shows new mail count in a message indicator. When New Mail Arrives When new mail arri_ves in _Display a notification _Indicate new messages in the panel any Folder any Inbox Project-Id-Version: evolution-indicator
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-06-24 15:14+0000
PO-Revision-Date: 2013-04-11 09:03+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 18:39+0000
X-Generator: Launchpad (build 18115)
 %d Nova Mensagem %d Novas Mensagens : Compor nova mensagem Contatos Indicador do Evolution Caixa de Entrada Somente criar notificações para novos e-mails na caixa de entrada. Reproduz_ir um som Reproduzir som para novo e-mail. Mostrar balão de notificação. Mostrar contador de novas mensagens no miniaplicativo indicador. Mostra a contagem de novas mensagens em um indicador de mensagem. Quando chega uma nova mensagem Quando no_vos e-mails chegarem _Mostrar uma notificação _Indicar novas mensagens no painel qualquer pasta qualquer caixa de entrada 