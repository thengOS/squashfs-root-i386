��    �     d  �  �*      �8     �8  L  	9  D   V:  m   �:  *   	;  1   4;  6   f;  4   �;  @   �;  .   <  4   B<  >   w<  K   �<  6   =  @   9=  E   z=  E   �=  5   >  +   <>  8   h>  7   �>  Q   �>  1   +?  +   ]?  E   �?  <   �?  A   @  H   N@  N   �@  )   �@  8   A  M   IA  D   �A  2   �A  *   B  *   :B  +   eB  =   �B     �B  e   �B  e   GC  -   �C  ;   �C  4   D  ,   LD  4   yD  5   �D  8   �D  )   E  3   GE  0   {E  H   �E  8   �E  -   .F  7   \F  )   �F  1   �F  2   �F  9   #G  8   ]G  7   �G  7   �G  7   H  0   >H  6   oH  9   �H  2   �H  3   I  >   GI  (   �I  /   �I  "   �I  /   J  4   2J  5   gJ  ?   �J     �J  @   �J  6   5K  -   lK  *   �K  5   �K  3   �K  7   /L  5   gL  +   �L  2   �L  1   �L     .M  B   KM  :   �M  6   �M  F    N  <   GN  -   �N  !   �N  '   �N  '   �N  8   $O  ;   ]O  @   �O  (   �O  )   P  *   -P  (   XP  J   �P  J   �P  -   Q  -   EQ  C   sQ     �Q  .   �Q  ,   �Q  %   $R      JR      kR  0   �R  ?   �R  &   �R  +   $S  (   PS  /   yS  .   �S     �S  &   �S  0   T      DT  4   eT  $   �T  &   �T  (   �T  %   U  "   5U     XU      xU  B   �U     �U     �U     V  .   0V  :   _V  6   �V  4   �V  %   W  F   ,W  K   sW  3   �W     �W  Q   X  V   `X      �X     �X  *   �X  !   Y  j   9Y  ?   �Y      �Y  !   Z  )   'Z     QZ  2   bZ     �Z  2   �Z  /   �Z  B   [  B   N[  "   �[  "   �[  ;   �[     \     (\  +   E\  (   q\  #   �\     �\  J   �\  D   #]  2   h]  2   �]  ?   �]  *   ^     9^  .   N^     }^  7   �^  ;   �^     _     )_     ;_     O_     \_  
   k_     v_     �_     �_     �_     �_     �_     �_     �_     �_  	   `     `  
   `     &`     B`  3   S`  :   �`  6   �`  4   �`  $   .a  D   Sa  J   �a  D   �a     (b     7b     Tb     ]b  ;   rb     �b  #   �b  B   �b     0c     Oc     gc  3   c  1   �c  )   �c  *   d  )   :d    dd  �   je     Nf     nf  <   �f  =   �f     g     $g  1   Cg  #   ug     �g     �g     �g  (   �g     �g     h     1h  &   Ih     ph     �h  !   �h     �h  !   �h     i  B   $i  <   gi  .   �i  ?   �i  9   j  2   Mj  -   �j  )   �j  *   �j  9   k  +   =k     ik  <   �k  :   �k     �k  -   l     @l  #   Yl  �   }l     ^m  #   ~m  $   �m     �m     �m  !   �m  
   !n     ,n     8n     Qn  ,   pn  !   �n     �n     �n      �n     o      1o     Ro     eo  &   �o     �o     �o     �o     �o  ;   p  �   Op  )   (q  M   Rq  &   �q  4   �q  0   �q  >   -r  9   lr  4   �r  D   �r  D    s      es     �s  >   �s  $   �s  ?   	t     It  4   `t  3   �t     �t     �t     �t     u      u     @u  L   Nu     �u     �u     �u  =   �u  '   'v  *   Ov  !   zv     �v     �v     �v  8   �v  8   "w      [w      |w     �w     �w     �w     �w  !   �w     x     .x     2x     Ix     Zx     zx     �x     �x  6   �x  ;   y     @y     Vy  ;   qy     �y     �y  0   �y     z  F   *z  #   qz     �z      �z     �z     �z  &   �z  *   "{  *   M{  (   x{  *   �{  *   �{  +   �{  +   #|  +   O|  )   {|  )   �|  (   �|  #   �|     }     ,}      <}      ]}     ~}  $   �}      �}  3   �}     ~     5~  ,   A~  )   n~  #   �~  #   �~     �~     �~          $     <     S     l     �     �     �     �     �     �      �     :�     U�  0   m�  !   ��     ��  )   Հ  '   ��     '�  "   C�      f�     ��     ��     ��  !   ȁ     �  !   �     &�  *   6�  -   a�     ��     ��  .   ǂ  2   ��  5   )�  4   _�  .   ��  *   Ã     �      �  2    �  0   S�     ��     ��     ��     ʄ      ݄     ��     �     -�     B�  %   W�     }�  *   ��     ą  3   م  *   �  "   8�  )   [�  -   ��     ��      Ά     �     �     !�  H   ;�  *   ��     ��     ͇  -   �  -   �  1   A�  )   s�     ��  D   ��  %   �  /   �    @�    L�  @   ̋  F   �    T�     m�  #   ��  (   ��     ׍  9   �     %�  �   C�     �  ?   �  &   G�  #   n�  %   ��  (   ��  ,   �  	   �     �     /�     J�     c�     y�  /   ��     Đ  '   ِ     �     �     /�  $   H�  )   m�  F   ��  :   ޑ  A   �  G   [�  5   ��  S   ْ  =   -�  $   k�     ��     ��     Ǔ     ړ     ��     �     1�     O�  $   k�  ?   ��     Д     ��     �  
   �     �     /�     L�     a�  ,   y�     ��     ��     ԕ     �     	�     �  (   )�  "   R�  '   u�  *   ��  (   Ȗ  $   �  5   �  "   L�  0   o�  "   ��  )   ×     �     	�     �     1�     >�     K�     g�     z�  �   ��  E   �  V   Q�  '   ��  4   Й  >   �  ;   D�  1   ��  I   ��  M   ��  ,   J�  M   w�  3   ś  ;   ��  +   5�  +   a�     ��  U   ��     ��  !   �     5�  S   O�  /   ��  )   ӝ  #   ��     !�  '   ?�  H   g�  
   ��  5   ��     �     �      �  +   :�  "   f�  !   ��  "   ��     Ο  2   �  2   �  @   Q�  &   ��     ��     נ     �  %   �     +�  0   2�  4   c�     ��     ��  '   ��  1   ݡ     �     +�     E�     T�     k�  H   ��     Ӣ     ��     �  4   (�  1   ]�  @   ��  =   У     �  &   )�  '   P�  #   x�  3   ��  4   Ф     �  0   $�     U�  @   t�  .   ��  2   �  -   �  1   E�  ,   w�     ��  $   ��     ٦     �     �     �  0   0�  "   a�      ��  "   ��  	   ȧ  '   ҧ  "   ��      �     >�     Q�  %   k�  ,   ��     ��  !   ܨ     ��     �     %�     C�     J�     h�     |�  )   ��  :   ��  0   ��  :   -�     h�  	   ��     ��     ��     ��  )   ª  @   �  4   -�     b�     x�    ��  "   ��  b  ԭ  =   7�  }   u�  8   �  3   ,�  D   `�  G   ��  M   ��  1   ;�  4   m�  E   ��  ]   �  :   F�  J   ��  O   ̲  B   �  E   _�  0   ��  K   ֳ  C   "�  l   f�  7   Ӵ  /   �  Z   ;�  I   ��  L   �  G   -�  \   u�  9   Ҷ  ;   �  z   H�  S   ÷  I   �  5   a�  0   ��  .   ȸ  >   ��  -   6�  n   d�  o   ӹ  .   C�  g   r�  H   ں  8   #�  8   \�  9   ��  @   ϻ  8   �  6   I�  H   ��  Q   ɼ  Q   �  ;   m�  >   ��  9   �  >   "�  6   a�  C   ��  7   ܾ  D   �  S   Y�  M   ��  d   ��  C   `�  E   ��  9   ��  @   $�  d   e�  .   ��  6   ��  -   0�  N   ^�  I   ��  I   ��  F   A�     ��  O   ��  D   ��  4   5�  -   j�  ?   ��  ;   ��  >   �  :   S�  /   ��  A   ��  ?    �  (   @�  Z   i�  <   ��  ;   �  S   =�  F   ��  ;   ��  $   �  4   9�  <   n�  9   ��  D   ��  L   *�  +   w�  1   ��  5   ��  2   �  T   >�  T   ��  0   ��  0   �  H   J�     ��  <   ��  ;   ��  (    �  &   I�  &   p�  <   ��  R   ��  1   '�  /   Y�  *   ��  8   ��  :   ��  *   (�  )   S�  <   }�  G   ��  @   �  ,   C�  9   p�  2   ��  7   ��  7   �  (   M�  )   v�  \   ��  #   ��  #   !�     E�  9   e�  A   ��  ?   ��  >   !�  (   `�  W   ��  \   ��  9   >�     x�  a   ��  i   ��  !   Y�     {�  *   ��     ��  t   ��  P   I�  %   ��  >   ��  =   ��     =�  2   O�     ��  9   ��  0   ��  Q   �  O   _�  ,   ��  +   ��  H   �     Q�  !   g�  :   ��  /   ��  /   ��  )   $�  ^   N�  X   ��  2   �  6   9�  E   p�  M   ��  )   �  7   .�      f�  K   ��  P   ��  '   $�  "   L�  '   o�     ��     ��  
   ��     ��     ��     ��     ��     	�     %�     =�     S�     \�     `�     l�     }�     ��     ��  <   ��  <   ��  :   .�  8   i�  (   ��  c   ��  \   /�  P   ��     ��      ��     �     �  I   /�     y�  $   ��  I   ��     �      �     8�  :   T�  =   ��  &   ��  '   ��  &   �  )  C�  �   m�  #   b�  /   ��  B   ��  B   ��  %   <�  3   b�  E   ��  +   ��     �     %�     7�  (   Q�     z�     ��     ��  0   ��     ��     �  "   5�  %   X�  %   ~�  -   ��  F   ��  <   �  7   V�  D   ��  [   ��  8   /�  A   h�  >   ��  8   ��  K   "�  6   n�     ��  D   ��  M   �     S�  2   m�     ��  (   ��  �   ��  -   ��  4   ��  >   (�     g�  $   ��  ,   ��     ��  
   ��     ��  .   �  /   <�  !   l�     ��     ��  )   ��     ��  %   �     2�     E�  3   a�     ��     ��  #   ��  '   ��  O   �  �   g�  ?   4�  f   t�  *   ��  F   �  E   M�  C   ��  7   ��  L   �  O   \�  M   ��  (   ��  )   #�  @   M�  +   ��  B   ��     ��  3   �  >   F�     ��  %   ��  !   ��     ��  )   ��  !   �  [   <�     ��  !   ��     ��  H   ��  ,   ;�  @   h�  +   ��  5   ��     �     %�  C   C�  C   ��  2   ��  2   ��     1�  "   N�  *   q�     ��  ,   ��     ��     ��  "   ��     �  -   3�  '   a�     ��     ��  F   ��  D   �     M�  &   f�  W   ��  *   ��     �  Q   *�  #   |�  T   ��  &   ��     �  )   7�     a�     |�  '   ��  -   ��  -   ��  )   �  -   @�  -   n�  ,   ��  .   ��  ,   ��  ,   %�  ,   R�  )   �  !   ��     ��     ��  )   ��  )     #   @  (   d  &   �  D   �  %   �      3   , .   ` (   � (   �    � $   � &   ! !   H     j "   � +   � "   � (   � %   & #   L /   p     � %   � )   �     :   1 0   l    � :   � =   �    . %   J 7   p !   �    � #   � ,     &   - ,   T    � ,   � /   �    �     ;   0 <   l =   � <   � +   $ 3   P    �    � 4   � 3   �    #	 %   4	    Z	    p	 $   �	    �	    �	    �	    �	 -   
 "   <
 5   _
    �
 O   �
 3   �
 !   3 -   U 1   �    � )   � '   �    $    B Q   b 5   �     �     4   & 0   [ 6   � ;   �    � I    ,   Q +   ~    � �  � X   R ^   � O  
    Z /   y 5   �    � ?   �    = �   Z    - V   E /   � .   � -   � 6   ) &   ` 	   �    � "   �    �    � $    6   -    d 6   � #   �    �    � #    3   4 O   h B   � O   � V   K O   � U   � M   H +   �    �     �         $   9     ^        � +   � a   �    J    Z    k    � (   � %   � "   � &    1   * &   \    �    �     �    �    � (   � B    ;   [ 8   � 8   � (   	 F   2 9   y A   � >   � J   4          �     �     �     �  %   �     !    +! �   >! K   �! a   !" &   �" >   �" O   �" I   9# =   �# ]   �# p   $ =   �$ _   �$ <   .% 8   k% /   �% 0   �%    & S   &    q& 2   �& 4   �& O   �& D   A' 0   �' /   �'    �' .   ( \   4(    �( 4   �(    �( #   �(    ) +   &) "   R)     u) !   �)    �) 8   �) 8   * e   H* -   �* $   �* #   +    %+ *   ;+    f+ 4   l+ <   �+    �+ %   �+ 9   , I   G,    �, !   �,    �,    �, %   
- M   0-     ~- #   �- "   �- <   �- /   #. T   S. P   �. %   �. 5   / 9   U/ 5   �/ ?   �/ =   0    C0 K   c0     �0 X   �0 .   )1 2   X1 .   �1 2   �1 %   �1    2 0   &2 *   W2    �2    �2    �2 @   �2 !   3 (   93 )   b3    �3 E   �3 @   �3 /    4    P4    h4 (   �4 6   �4 )   �4 $   5    35    S5 !   Y5    {5 "   �5    �5    �5 3   �5 D   	6 =   N6 I   �6 "   �6    �6    7    7 $   &7 )   K7 R   u7 2   �7     �7    8        ~       3         �         �  *  =  �  K   �   p      �          I    �   {  �  Q           J   �      5   �               8      �     :  �     �  �  x   g   �          �       b  �   _   
   �  R        �  G     �  a  H  �    t   g      �   \   !       �  �   6    n  I          u        �  �     z  �   �  �   V     �   �   �      }   U  +          v  )             {  -    d      �  N          o                      	       �                �  h   Z  H   "     �          F  !  �  �   ~  �   �   �           �   �  <  �      �   �  �    �   �      �   v   �  �      y  /       V  <  [          �   �      p   �                ~  K  f  k  :  �   �   a      �          0  ;  r   I   �      Y   �  �   �          �  �      �  �        �   .   �  �   o      D  W  �       �      �  �   �   8  /     ?   c  �  *  �   Q  U      �      �  S   {   �   �                �   �   �  W  �       e  �   �   �      u   ?            q    �  �   s  y       �   �  )       �   �   .  �  �      8   �           �      6   2      1    �   M  �         �   �  `     �   R          �  �   S        R  �  �  g  L   �   :           �   A   ]       "   �  J  #  �      �  �  �  $   �               x      #    4       d           �   7  Y  2      G  �   7  E  j   �   Y  �  �        �        ]  �   N                        v  F     �           C   �   �  k  �  O  �              E  �  �  �  ,   W     D   �    1  ?  ;  X  \  �   >   9  <               '  h  =   �  �  
  �          �   >        �    �  ;   �      �   �   %      F  �   k      '  �          �   �   A  ^       m   �  �  5  �  L  K  �   �  �    _  @  �    �  �      e  %   w  
  T   z      �   y          P      �   �  �   �   (               �           �          �  �       �                 �         �  N   �   �      �       �   w   D    3  �   �  4  �      p  t      &  O  ,  f  �  �   j  �  �      �      E   �   �  +   %  P   �            �   �  l   �  B    �  &           �  �  �        �       �       �   �      |   �  �   }  �  �   �  -   9      |      �   �       �  �   �  ^  �  /      @     �  �       =  @             9   ]          -  4  �          +  M      B      �  z       5  �       H  �          �   �  �   �       A  '   (      	  �  q   �  �        r  �      s  7       Z       d  �  h      �  i  �  �  �   �   �  $  �             �   �  �   �      �  �  �   �  a   �      �  0    �   �  �   P      T  �  �       ,  J  G   �  �   �   6      _          w  �      �   2          l  1          X   �  c      �      �   r      �  �  f   �   .  �  T       �   M       L  �   �      >      ^   "  Q  `  �  �       �  m  �  B  �      �  U         #     �  �   �  �          b             S  �    �  l        �  �     �   s   �   u  j     �   i       �  q      �   O         �      *   [               `      |    �   X              �      �     Z  �           n       �           i  �  }      �   �      	  0   �   \    m  !  V   t  �  �     �       �  �       x                  �      $        �      C  �  �          �       &  [  �  e                   3  n  o   b                  �           )  �  (      �                 c   �   C     �           
Caught SIGINT... bailing out.
 
However, if you HAVE changed your account details since starting the
fetchmail daemon, you need to stop the daemon, change your configuration
of fetchmail, and then restart the daemon.

The fetchmail daemon will continue running and attempt to connect
at each cycle.  No future notifications will be sent until service
is restored.       --auth        authentication type (password/kerberos/ssh/otp)
       --bad-header {reject|accept}
                    specify policy for handling messages with bad headers
       --bsmtp       set output BSMTP file
       --fastuidl    do a binary search for UIDLs
       --fetchdomains fetch mail for specified domains
       --fetchsizelimit set fetch message size limit
       --invisible   don't write Received & enable host spoofing
       --limitflush  delete oversized messages
       --lmtp        use LMTP (RFC2033) for delivery
       --nobounce    redirect bounces from user to postmaster.
       --nosoftbounce fetchmail deletes permanently undeliverable messages.
       --pidfile     specify alternate PID (lock) file
       --plugin      specify external command to open connection
       --plugout     specify external command to open smtp connection
       --port        TCP port to connect to (obsolete, use --service)
       --postmaster  specify recipient of last resort
       --principal   mail service principal
       --showdots    show progress dots even in logfiles
       --smtpname    set SMTP full name username@domain
       --softbounce  keep permanently undeliverable messages on server (default).
       --ssl         enable ssl encrypted session
       --sslcert     ssl client certificate
       --sslcertck   do strict server certificate check (recommended)
       --sslcertfile path to trusted-CA ssl certificate file
       --sslcertpath path to trusted-CA ssl certificate directory
       --sslcommonname  expect this CommonName from server (discouraged)
       --sslfingerprint fingerprint that must match that of the server's cert.
       --sslkey      ssl private key file
       --sslproto    force ssl protocol (SSL2/SSL3/TLS1)
       --syslog      use syslog(3) for most messages when running as a daemon
       --tracepolls  add poll-tracing information to Received header
     attacks by a dishonest service are possible.)
     cannot be sure you are talking to the
     prevent you logging in, but means you
     service that you think you are (replay
    Do linear search of UIDs during each poll (--fastuidl 0).
   %d UIDs saved.
   %d message  %d octets long deleted by fetchmail.   %d messages %d octets long deleted by fetchmail.   %d message  %d octets long skipped by fetchmail.   %d messages %d octets long skipped by fetchmail.   -?, --help        display this option help
   -B, --fetchlimit  set fetch limit for server connections
   -D, --smtpaddress set SMTP delivery domain to use
   -E, --envelope    envelope address header
   -F, --flush       delete old messages from server
   -I, --interface   interface required specification
   -K, --nokeep      delete new messages after retrieval
   -L, --logfile     specify logfile name
   -M, --monitor     monitor interface for activity
   -N, --nodetach    don't detach daemon process
   -P, --service     TCP service to connect to (can be numeric TCP port)
   -Q, --qvirtual    prefix to remove from local user id
   -S, --smtphost    set SMTP forwarding host
   -U, --uidl        force the use of UIDLs (pop3 only)
   -V, --version     display version info
   -Z, --antispam,   set antispam response values
   -a, --[fetch]all  retrieve old and new messages
   -b, --batchlimit  set batch limit for SMTP connections
   -c, --check       check for messages without fetching
   -d, --daemon      run as a daemon once per n seconds
   -e, --expunge     set max deletions between expunges
   -f, --fetchmailrc specify alternate run control file
   -i, --idfile      specify alternate UIDs file
   -k, --keep        save new messages after retrieval
   -l, --limit       don't fetch messages over given size
   -m, --mda         set MDA to use for forwarding
   -n, --norewrite   don't rewrite header addresses
   -p, --protocol    specify retrieval protocol (see man page)
   -q, --quit        kill daemon process
   -r, --folder      specify remote folder name
   -s, --silent      work silently
   -t, --timeout     server nonresponse timeout
   -u, --username    specify users's login on server
   -v, --verbose     work noisily (diagnostic output)
   -w, --warnings    interval between warning mail notification
   APOP secret = "%s".
   Address to be put in RCPT TO lines shipped to SMTP will be %s
   All available authentication methods will be tried.
   All messages will be retrieved (--all on).
   CRAM-MD5 authentication will be forced.
   Carriage-return forcing is disabled (forcecr off).
   Carriage-return forcing is enabled (forcecr on).
   Carriage-return stripping is disabled (stripcr off).
   Carriage-return stripping is enabled (stripcr on).
   Connection must be through interface %s.
   DNS lookup for multidrop addresses is disabled.
   DNS lookup for multidrop addresses is enabled.
   Default mailbox selected.
   Deletion interval between expunges forced to %d (--expunge %d).
   Delivered-To lines will be discarded (dropdelivered on)
   Delivered-To lines will be kept (dropdelivered off)
   Do binary search of UIDs during %d out of %d polls (--fastuidl %d).
   Do binary search of UIDs during each poll (--fastuidl 1).
   Domains for which mail will be fetched are:   End-to-end encryption assumed.
   Envelope header is assumed to be: %s
   Envelope-address routing is disabled
   Fetch message size limit is %d (--fetchsizelimit %d).
   Fetched messages will be kept on the server (--keep on).
   Fetched messages will not be kept on the server (--keep off).
   GSSAPI authentication will be forced.
   Host part of MAIL FROM line will be %s
   Idle after poll is disabled (idle off).
   Idle after poll is enabled (idle on).
   Interpretation of Content-Transfer-Encoding is disabled (pass8bits on).
   Interpretation of Content-Transfer-Encoding is enabled (pass8bits off).
   Kerberos V4 authentication will be forced.
   Kerberos V5 authentication will be forced.
   Listener connections will be made via plugout %s (--plugout %s).
   Local domains:   MIME decoding is disabled (mimedecode off).
   MIME decoding is enabled (mimedecode on).
   MSN authentication will be forced.
   Mail service principal is: %s
   Mail will be retrieved via %s
   Message size limit is %d octets (--limit %d).
   Message size warning interval is %d seconds (--warnings %d).
   Messages will be %cMTP-forwarded to:   Messages will be appended to %s as BSMTP
   Messages will be delivered with "%s".
   Messages with bad headers will be passed on.
   Messages with bad headers will be rejected.
   Multi-drop mode:    NTLM authentication will be forced.
   No SMTP message batch limit (--batchlimit 0).
   No UIDs saved from this host.
   No fetch message size limit (--fetchsizelimit 0).
   No forced expunges (--expunge 0).
   No interface requirement specified.
   No localnames declared for this host.
   No message size limit (--limit 0).
   No monitor interface specified.
   No plugin command specified.
   No plugout command specified.
   No poll trace information will be added to the Received header.
   No post-connection command.
   No pre-connection command.
   No prefix stripping
   No received-message limit (--fetchlimit 0).
   Nonempty Status lines will be discarded (dropstatus on)
   Nonempty Status lines will be kept (dropstatus off)
   Number of envelope headers to be skipped over: %d
   OTP authentication will be forced.
   Old messages will be flushed before message retrieval (--flush on).
   Old messages will not be flushed before message retrieval (--flush off).
   Only new messages will be retrieved (--all off).
   Options are as follows:
   Oversized messages will be flushed before message retrieval (--limitflush on).
   Oversized messages will not be flushed before message retrieval (--limitflush off).
   Pass-through properties "%s".
   Password = "%s".
   Password authentication will be forced.
   Password will be prompted for.
   Poll of this server will occur every %d interval.
   Poll of this server will occur every %d intervals.
   Poll trace information will be added to the Received header.
   Polling loop will monitor %s.
   Predeclared mailserver aliases:   Prefix %s will be removed from user id
   Protocol is %s   Protocol is KPOP with Kerberos %s authentication   RPOP id = "%s".
   Received-message limit is %d (--fetchlimit %d).
   Recognized listener spam block responses are:   Rewrite of server-local addresses is disabled (--norewrite on).
   Rewrite of server-local addresses is enabled (--norewrite off).
   SMTP message batch limit is %d.
   SSL encrypted sessions enabled.
   SSL key fingerprint (checked against the server key): %s
   SSL protocol: %s.
   SSL server CommonName: %s
   SSL server certificate checking enabled.
   SSL trusted certificate directory: %s
   SSL trusted certificate file: %s
   Selected mailboxes are:   Server aliases will be compared with multidrop addresses by IP address.
   Server aliases will be compared with multidrop addresses by name.
   Server connection will be brought up with "%s".
   Server connection will be taken down with "%s".
   Server connections will be made via plugin %s (--plugin %s).
   Server nonresponse timeout is %d seconds   Single-drop mode:    Size warnings on every poll (--warnings 0).
   Spam-blocking disabled
   This host will be queried when no host is specified.
   This host will not be queried when no host is specified.
   True name of server is %s.
  (%d body octets)  (%d header octets)  (%d octets)  (%d octets).
  (default)  (default).
  (forcing UIDL use)  (length -1)  (oversized)  (previously authorized)  (using default port)  (using service %s)  <empty>  and   flushed
  not flushed
  retained
 %cMTP connect to %s failed
 %cMTP error: %s
 %cMTP listener doesn't like recipient address `%s'
 %cMTP listener doesn't really like recipient address `%s'
 %d local name recognized.
 %d local names recognized.
 %d message (%d %s) for %s %d messages (%d %s) for %s %d message for %s %d messages for %s %d message waiting after expunge
 %d messages waiting after expunge
 %d message waiting after first poll
 %d messages waiting after first poll
 %d message waiting after re-poll
 %d messages waiting after re-poll
 %lu is unseen
 %s (log message incomplete)
 %s at %s %s at %s (folder %s) %s configuration invalid, LMTP can't use default SMTP port
 %s connection to %s failed %s error while fetching from %s@%s
 %s error while fetching from %s@%s and delivering to SMTP host %s
 %s fingerprints do not match!
 %s fingerprints match.
 %s key fingerprint: %s
 %s querying %s (protocol %s) at %s: poll completed
 %s querying %s (protocol %s) at %s: poll started
 %s's SMTP listener does not support ATRN
 %s's SMTP listener does not support ESMTP
 %s's SMTP listener does not support ETRN
 %s: The NULLMAILER_FLAGS environment variable is set.
This is dangerous as it can make nullmailer-inject or nullmailer's
sendmail wrapper tamper with your From:, Message-ID: or Return-Path: headers.
Try "env NULLMAILER_FLAGS= %s YOUR ARGUMENTS HERE"
%s: Abort.
 %s: The QMAILINJECT environment variable is set.
This is dangerous as it can make qmail-inject or qmail's sendmail wrapper
tamper with your From: or Message-ID: headers.
Try "env QMAILINJECT= %s YOUR ARGUMENTS HERE"
%s: Abort.
 %s: You don't exist.  Go away.
 %s: can't determine your host! %s: opportunistic upgrade to TLS failed, trying to continue
 %s: opportunistic upgrade to TLS failed, trying to continue.
 %s: upgrade to TLS failed.
 %s: upgrade to TLS succeeded.
 %s:%d: warning: found "%s" before any host names
 %s:%d: warning: unknown token "%s"
 %u is first unseen
 %u is unseen
 -- 
The Fetchmail Daemon --check mode enabled, not fetching mail
 ...rewritten version is %s.
 ATRN request refused.
 About to rewrite %s...
 All connections are wedged.  Exiting.
 Authentication required.
 Authorization OK on %s@%s
 Authorization failure on %s@%s%s
 BSMTP file open failed: %s
 BSMTP preamble write failed: %s.
 Bad base64 reply from server.
 Bad certificate: Subject Alternative Name contains NUL, aborting!
 Bad certificate: Subject CommonName contains NUL, aborting!
 Bad certificate: Subject CommonName too long!
 Both fetchall and keep on in daemon or idle mode is a mistake!
 Cannot find my own host in hosts database to qualify it!
 Cannot handle UIDL response from upstream server.
 Cannot open fetchids file %s for writing: %s
 Cannot rename fetchids file %s to %s: %s
 Cannot resolve service %s to port number.
 Cannot switch effective user id back to original %ld: %s
 Cannot switch effective user id to %ld: %s
 Certificate at depth %d:
 Certificate chain, from root to peer, starting at depth %d:
 Certificate/fingerprint verification was somehow skipped!
 Challenge decoded: %s
 Checking if %s is really the same node as %s
 Command not implemented
 Connection errors for this poll:
%s Copyright (C) 2002, 2003 Eric S. Raymond
Copyright (C) 2004 Matthias Andree, Eric S. Raymond,
                   Robert M. Funk, Graham Wilson
Copyright (C) 2005 - 2012 Sunil Shetye
Copyright (C) 2005 - 2013 Matthias Andree
 Could not decode OTP challenge
 Couldn't get service name for [%s]
 Couldn't unwrap security level data
 Credential exchange complete
 Cygwin socket read retry
 Cygwin socket read retry failed!
 DNS lookup Deity error Deleting fetchids file.
 Digest text buffer too small!
 ERROR: no support for getpassword() routine
 ESMTP CRAM-MD5 Authentication...
 ESMTP LOGIN Authentication...
 ESMTP PLAIN Authentication...
 ETRN support is not configured.
 ETRN syntax error
 ETRN syntax error in parameters
 EVP_md5() failed!
 Enter password for %s@%s:  Error creating security level request
 Error deleting %s: %s
 Error exchanging credentials
 Error releasing credentials
 Error writing to MDA: %s
 Error writing to fetchids file %s, old file left in place.
 Fetchmail comes with ABSOLUTELY NO WARRANTY. This is free software, and you
are welcome to redistribute it under certain conditions. For details,
please see the file COPYING in the source or documentation directory.
 Fetchmail could not get mail from %s@%s.
 Fetchmail saw more than %d timeouts while attempting to get mail from %s@%s.
 Fetchmail was able to log into %s@%s.
 Fetchmail will direct error mail to the postmaster.
 Fetchmail will direct error mail to the sender.
 Fetchmail will forward misaddressed multidrop messages to %s.
 Fetchmail will masquerade and will not generate Received
 Fetchmail will show progress dots even in logfiles.
 Fetchmail will treat permanent errors as permanent (drop messages).
 Fetchmail will treat permanent errors as temporary (keep messages).
 File %s must be a regular file.
 File %s must be owned by you.
 File %s must have no more than -rwx------ (0700) permissions.
 File descriptor out of range for SSL For help, see http://www.fetchmail.info/fetchmail-FAQ.html#R15
 GSSAPI error %s: %.*s
 GSSAPI error in gss_display_status called from <%s>
 GSSAPI support is configured, but not compiled in.
 Get response
 Get response return %d [%s]
 Guessing from header "%-.*s".
 Hdr not 60
 IMAP support is not configured.
 Idfile is %s
 If you want to use GSSAPI, you need credentials first, possibly from kinit.
 Inbound binary data:
 Incorrect FETCH response: %s.
 Invalid APOP timestamp.
 Invalid SSL protocol '%s' specified, using default (SSLv23).
 Invalid authentication `%s' specified.
 Invalid bad-header policy `%s' specified.
 Invalid protocol `%s' specified.
 Invalid userid or passphrase Issuer CommonName: %s
 Issuer Organization: %s
 KERBEROS v4 support is configured, but not compiled in.
 KERBEROS v5 support is configured, but not compiled in.
 Kerberos V4 support not linked.
 Kerberos V5 support not linked.
 LMTP delivery error on EOM
 Lead server has no name.
 Lock-busy error on %s@%s
 Logfile is %s
 MD5 being applied to data block:
 MD5 result is:
 MDA MDA died of signal %d
 MDA open failed
 MDA returned nonzero status %d
 Maximum GSS token size is %ld
 Mechanism field incorrect
 Merged UID list from %s: Message termination or close of BSMTP file failed: %s
 Messages inserted into list on server. Cannot handle this.
 New UID list from %s: No IP address found for %s No envelope recipient found, resorting to header guessing.
 No interface found with name %s No mail for %s
 No mailservers set up -- perhaps %s is missing?
 No messages waiting for %s
 No suitable GSSAPI credentials found. Skipping GSSAPI authentication.
 No, their IP addresses don't match
 Node %s not allowed: %s
 ODMR support is not configured.
 Old UID list from %s: OpenSSL reported: %s
 Option --all is not supported with %s
 Option --check is not supported with ETRN
 Option --check is not supported with ODMR
 Option --flush is not supported with %s
 Option --flush is not supported with ETRN
 Option --flush is not supported with ODMR
 Option --folder is not supported with ETRN
 Option --folder is not supported with ODMR
 Option --folder is not supported with POP3
 Option --keep is not supported with ETRN
 Option --keep is not supported with ODMR
 Option --limit is not supported with %s
 Options for retrieving from %s@%s:
 Out of memory!
 Outbound data:
 POP2 support is not configured.
 POP3 support is not configured.
 Parsing Received names "%-.*s"
 Parsing envelope "%s" names "%-.*s"
 Pending messages for %s started
 Please specify the service as decimal port number.
 Poll interval is %d seconds
 Polling %s
 Progress messages will be logged via syslog
 Protocol identified as IMAP2 or IMAP2BIS
 Protocol identified as IMAP4 rev 0
 Protocol identified as IMAP4 rev 1
 Query status=%d
 Query status=0 (SUCCESS)
 Query status=1 (NOMAIL)
 Query status=10 (SMTP)
 Query status=11 (DNS)
 Query status=12 (BSMTP)
 Query status=13 (MAXFETCH)
 Query status=2 (SOCKET)
 Query status=3 (AUTHFAIL)
 Query status=4 (PROTOCOL)
 Query status=5 (SYNTAX)
 Query status=6 (IOERR)
 Query status=7 (ERROR)
 Query status=8 (EXCLUDE)
 Query status=9 (LOCKBUSY)
 Queuing for %s started
 RPA Failed open of /dev/urandom. This shouldn't
 RPA Session key length error: %d
 RPA String too long
 RPA User Authentication length error: %d
 RPA _service_ auth fail. Spoof server?
 RPA authorisation complete
 RPA error in service@realm string
 RPA rejects you, reason unknown
 RPA rejects you: %s
 RPA status: %02X
 RPA token 2 length error
 RPA token 2: Base64 decode error
 RPA token 4 length error
 RPA token 4: Base64 decode error
 Realm list: %s
 Received BYE response from IMAP server: %s Received malformed challenge to "%s GSSAPI"!
 Releasing GSS credentials
 Repoll immediately on %s@%s
 Required APOP timestamp not found in greeting
 Required LOGIN capability not supported by server
 Required NTLM capability not compiled into fetchmail
 Required OTP capability not compiled into fetchmail
 Restricted user (something wrong with account) Routing message version %d not understood. SDPS not enabled. SMTP listener refused delivery
 SMTP listener rejected local recipient addresses:  SMTP server requires STARTTLS, keeping message.
 SMTP transaction SMTP: (bounce-message body)
 SSL connection failed.
 SSL is not enabled SSL support is not compiled in.
 Saved error is still %d
 Scratch list of UIDs: Secret pass phrase:  Sending credentials
 Server CommonName mismatch: %s != %s
 Server busy error on %s@%s
 Server certificate verification error: %s
 Server certificate:
 Server name not set, could not verify certificate!
 Server name not specified in certificate!
 Server rejected the AUTH command.
 Server requires integrity and/or privacy
 Server responded with UID for wrong message.
 Service challenge (l=%d):
 Service chose RPA version %d.%d
 Service has been restored.
 Service timestamp %s
 Session key established:
 Strange: MDA pclose returned %d and errno %d/%s, cannot handle at %s:%d
 String '%s' is not a valid number string.
 Subject Alternative Name: %s
 Subject CommonName: %s
 Subject: Fetchmail oversized-messages warning Subject: fetchmail authentication OK on %s@%s Subject: fetchmail authentication failed on %s@%s Subject: fetchmail sees repeated timeouts Success TLS is mandatory for this session, but server refused CAPA command.
 Taking options from command line%s%s
 The CAPA command is however necessary for TLS.
 The attempt to get authorization failed.
Since we have already succeeded in getting authorization for this
connection, this is probably another failure mode (such as busy server)
that fetchmail cannot distinguish because the server didn't send a useful
error message. The attempt to get authorization failed.
This probably means your password is invalid, but some servers have
other failure modes that fetchmail cannot distinguish from this
because they don't send useful error messages on login failure.

The fetchmail daemon will continue running and attempt to connect
at each cycle.  No future notifications will be sent until service
is restored. The following oversized messages remain on server %s account %s: The following oversized messages were deleted on server %s account %s: This could mean that your mailserver is stuck, or that your SMTP
server is wedged, or that your mailbox file on the server has been
corrupted by a server error.  You can run `fetchmail -v -v' to
diagnose the problem.

Fetchmail won't poll this mailbox again until you restart it.
 This is fetchmail release %s Timestamp syntax error in greeting
 Token Length %d disagrees with rxlen %d
 Token length error
 Try adding the --service option (see also FAQ item R12).
 Trying to connect to %s/%s... Trying to continue with unqualified hostname.
DO NOT report broken Received: headers, HELO/EHLO lines or similar problems!
DO repair your /etc/hosts, DNS, NIS or LDAP instead.
 Turnaround now...
 Unable to open kvm interface. Make sure fetchmail is SGID kmem. Unable to parse interface name from %s Unable to process ATRN request now
 Unable to queue messages for node %s
 Undefined protocol request in POP3_auth
 Unexpected non-503 response to LMTP EOM: %s
 Unicode:
 Unknown ETRN error %d
 Unknown Issuer CommonName
 Unknown ODMR error "%s"
 Unknown Organization
 Unknown Server CommonName
 Unknown login or authentication error on %s@%s
 Unknown system error Unwrapped security level flags: %s%s%s
 User authentication (l=%d):
 User challenge:
 Using service name [%s]
 Value of string '%s' is %s than %d.
 WARNING: Running as root is discouraged.
 Warning: "Maillennium POP3" found, using RETR command instead of TOP.
 Warning: Issuer CommonName too long (possibly truncated).
 Warning: Issuer Organization Name too long (possibly truncated).
 Warning: ignoring bogus data for message sizes returned by the server.
 Warning: multiple mentions of host %s in config file
 Warning: the connection is insecure, continuing anyways. (Better use --sslcertck!)
 We've run out of allowed authenticators and cannot continue.
 Write error on fetchids file %s: %s
 Writing fetchids file.
 Yes, their IP addresses match
 You have no mail.
 about to deliver with: %s
 activity on %s -noted- as %d
 activity on %s checked as %d
 activity on %s was %d, is %d
 analyzing Received line:
%s attempt to re-exec fetchmail failed
 attempt to re-exec may fail as directory has not been restored
 awakened at %s
 awakened by %s
 awakened by signal %d
 background bogus EXPUNGE count in "%s"! bogus message count in "%s"! bogus message count! can't even send to %s!
 can't raise the listener; falling back to %s cannot create socket: %s
 challenge mismatch
 client/server protocol client/server synchronization connected.
 connection failed.
 connection to %s:%s [%s/%s] failed: %s.
 could not decode BASE64 challenge
 could not decode BASE64 ready response
 could not decode initial BASE64 challenge
 could not get current working directory
 could not open %s to append logs to
 couldn't fetch headers, message %s@%s:%d (%d octets)
 couldn't find HESIOD pobox for %s
 couldn't find canonical DNS name of %s (%s): %s
 couldn't time-check %s (error %d)
 couldn't time-check the run-control file
 dec64 error at char %d: %x
 decoded as %s
 discarding new UID list
 dup2 failed
 end of input error writing message text
 execvp(%s) failed
 expunge failed
 fetchlimit %d reached; %d message left on server %s account %s
 fetchlimit %d reached; %d messages left on server %s account %s
 fetchmail: %s configuration invalid, RPOP requires a privileged port
 fetchmail: %s configuration invalid, specify positive port number for service or port
 fetchmail: %s fetchmail at %ld killed.
 fetchmail: Cannot detach into background. Aborting.
 fetchmail: Error: multiple "defaults" records in config file.
 fetchmail: another foreground fetchmail is running at %ld.
 fetchmail: background fetchmail at %ld awakened.
 fetchmail: can't accept options while a background fetchmail is running.
 fetchmail: can't check mail while another fetchmail to same host is running.
 fetchmail: can't find a password for %s@%s.
 fetchmail: can't poll specified hosts with another fetchmail running at %ld.
 fetchmail: elder sibling at %ld died mysteriously.
 fetchmail: error killing %s fetchmail at %ld; bailing out.
 fetchmail: error opening lockfile "%s": %s
 fetchmail: error reading lockfile "%s": %s
 fetchmail: fork failed
 fetchmail: interface option is only supported under Linux (without IPv6) and FreeBSD
 fetchmail: invoked with fetchmail: lock creation failed.
 fetchmail: malloc failed
 fetchmail: monitor option is only supported under Linux (without IPv6) and FreeBSD
 fetchmail: no mailservers have been specified.
 fetchmail: no other fetchmail is running
 fetchmail: removing stale lockfile
 fetchmail: socketpair failed
 fetchmail: thread sleeping for %d sec.
 fetchmail: warning: no DNS available to check multidrop fetches from %s
 foreground forwarding and deletion suppressed due to DNS errors
 forwarding to %s
 found Received address `%s'
 get_ifinfo: malloc failed get_ifinfo: sysctl (iflist estimate) failed get_ifinfo: sysctl (iflist) failed getaddrinfo("%s","%s") error: %s
 getaddrinfo(NULL, "%s") error: %s
 gethostbyname failed for %s
 id=%s (num=%d) was deleted, but is still present!
 id=%s (num=%u) was deleted, but is still present!
 incorrect header line found - see manpage for bad-header option
 interval not reached, not querying %s
 invalid IP interface address
 invalid IP interface mask
 kerberos error %s
 krb5_sendauth: %s [server says '%s']
 larger line accepted, %s is an alias of the mailserver
 line rejected, %s is not an alias of the mailserver
 line: %s lock busy on server lock busy!  Is another session active?
 mail expunge mismatch (%d actual != %d expected)
 mail from %s bounced to %s
 mailbox selection failed
 malloc failed
 mapped %s to local %s
 mapped address %s to local %s
 message %s@%s:%d was not the expected length (%d actual != %d expected)
 message has embedded NULs missing IP interface address
 missing or bad RFC822 header name %d: cannot create socket family %d type %d: %s
 name %d: connection to %s:%s [%s/%s] failed: %s.
 nameserver failure while looking for '%s' during poll of %s: %s
 nameserver failure while looking for `%s' during poll of %s.
 no Received address found
 no address matches; forwarding to %s.
 no address matches; no postmaster set.
 no local matches, forwarding to %s
 no recipient addresses matched declared local names non-null instance (%s) might cause strange behavior
 normal termination, status %d
 not swapping UID lists, no UIDs seen this query
 passed through %s matching %s
 poll of %s skipped (failed authentication or too many timeouts)
 post-connection command failed with status %d
 post-connection command terminated with signal %d
 pre-connection command failed with status %d
 pre-connection command terminated with signal %d
 principal %s in ticket does not match -u %s
 protocol error
 protocol error while fetching UIDLs
 re-poll failed
 reading message %s@%s:%d of %d realloc failed
 receiving message data
 recipient address %s didn't match any local name restarting fetchmail (%s changed)
 running %s (host %s service %s)
 search for unseen messages failed
 seen seen selecting or re-polling default folder
 selecting or re-polling folder %s
 server option after user options server recv fatal
 skipping message %s@%s:%d skipping message %s@%s:%d (%d octets) skipping poll of %s, %s IP address excluded
 skipping poll of %s, %s down
 skipping poll of %s, %s inactive
 sleeping at %s for %d seconds
 smaller smtp listener protocol error
 socket starting fetchmail %s daemon
 swapping UID lists
 terminated with signal %d
 timeout after %d seconds waiting for %s.
 timeout after %d seconds waiting for listener to respond.
 timeout after %d seconds waiting for server %s.
 timeout after %d seconds waiting to connect to server %s.
 timeout after %d seconds.
 undefined unknown unknown (%s) unsupported protocol selected.
 usage:  fetchmail [options] [server ...]
 warning: Do not ask for support if all mail goes to postmaster!
 warning: multidrop for %s requires envelope option!
 will idle after poll
 writing RFC822 msgblk.headers
 Project-Id-Version: fetchmail 6.3.24.1
Report-Msgid-Bugs-To: fetchmail-devel@lists.berlios.de
POT-Creation-Date: 2013-04-23 23:24+0200
PO-Revision-Date: 2013-12-19 10:13+0000
Last-Translator: Enrico Nicoletto <liverig@gmail.com>
Language-Team: Brazilian Portuguese <ldpbr-translation@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 16:21+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
 
Sinal SIGINT recebido... saindo.
 
Entretanto, se você MUDOU as informações de sua conta após a inicialização do 
daemon do fetchmail,  você precisa parar o daemon, mudar as configurações
do fetchmail, e reiniciar o daemon.

O daemon do fetchmail continuará sendo executado e tentando se conectar
a cada ciclo. Nenhuma notificação será enviada até o serviço
ser restaurado.       --auth tipo de autenticação (senha/kerberos/ssh/otp)
       --bad-header {reject|accept}
                    especifica a política para lidar com mensagens com cabeçalhos ruins
       --bsmtp       configura o arquivo de saída BSMTP
       --fastuids, faz uma busca binária por UIDLs
       --fetchdomains somente baixa e-mail de específicos domínios
       fetchlimit, configura o tamanho limite da mensagem a ser buscada
       --invisible   não escreve `Received' & ativa mascaramento da máquina
       --limitflush, apaga mensagens muito grades
       --lmtp        usa LMTP (RFC2033) para entrega
       --nobounce    redireciona mensagens erradas para o postmaster.
       -nosoftbounce fecthmail deleta mensagens que não podem ser entregues permanentemente.
       --pidfile especifica arquivo PID (lock) alternativo
       --plugin      especifica um comando externo para abrir uma conexão
       --plugout     especifica um comando externo para abrir uma conexão smtp
       --port Porta TCP na qual conectar (defasado, use --service)
       --postmaster  especifica o destinatário usado em último caso
       --principal, serviço de e-mail principal
       --showdots, mostra pontos de progresso até mesmo em arquivos de log
       --smtpname, configura o nome completo SMTP usuário@domínio
       --softbounce mantém as mensagens que não podem ser entregues permanentemente no servidor (padrão).
       --ssl         habilita sessão ssl criptografada
       --sslcert     certificado ssl do cliente
       --sslcertck faz a verificação restritiva de certificado do servidor (recomendado)
       --sslcertfile caminho para o arquivo de certificado SSL confiável
       --sslcertpath caminho para o diretório de certificado SSL confiável
       --sslcommonname  esperado nome comum do servidor (desencorajado)
       --sslfingerprint Identificação que deve coincidir com a do certificado do servidor.
       --sslkey      arquivo contendo a chave ssl privada
       --sslproto    forçar protocolo ssl (SSL2/SSL3/TLS1)
       --syslog      usa o syslog(3) para a maioria das mensagens quando
                    estiver rodando como servidor
       -tracepolls, adiciona informações de acompanhamento ao cabeçalho recebido
     de re-tentativa por parte de um serviço desonesto são possíveis.)
     você não pode ter certeza de estar falando com
     impedi-lo de se conectar, mas significa que
     o serviço que você pensa estar (ataques
    Faz busca linear de UIDs durante cada poll (--fastuidl 0).
   %d identificações de usuários gravadas.
   %d mensagem com  %d octetos removida pelo fetchmail.   %d mensagens com %d octetos removidas pelo fetchmail.   %d mensagem com  %d octetos ignorada pelo fetchmail.   %d mensagens  com %d octetos ignoradas pelo fetchmail.   -?, --help        mostra esta tela de ajuda
   -B, --fetchlimit  configura o limite de recepção para conexões com o
                    servidor
   -D, --smtpaddress configura o domínio de entrega do SMTP a ser usado
   -E, --envelope    cabeçalho do endereço de envelope
   -F, --flush       apaga mensagens antigas do servidor
   -I, --interface   especificação de interface exigida
   -K, --nokeep      apaga novas mensagens após a recuperação
   -L, --logfile     especifica o nome do arquivo de log
   -M, --monitor     monitora a atividade na interface
   -N, --nodetach    não desconecta o processo servidor do seu terminal
   -P, --service TCP, serviço no qual conectar (pode ser o número da porta TCP)
   -Q, --qvirtual    prefixo para remover da identificação de um usuário local
   -S, --smtphost    configura a máquina SMTP para reenvio
   -U, --uidl        força o uso de UIDLs (somente para pop3)
   -V, --version     mostra informações sobre a versão
   -Z, --antispam    configura o valor das respostas anti-spam
   -a, --[fetch]all recupera mensagens altigas e novas
   -b, --batchlimit  configura o limite de lote para conexões SMTP
   -c, --check       checa por mensagens sem baixá-las
   -d, --daemon      roda como um servidor uma vez a cada n segundos
   -e, --expunge     configura o máximo de exclusões entre exclusões definitivas
   -f, --fetchmailrc especifica arquivo de controle de execução alternativo
   -i, --idfile      especifica arquivo de identidades de usuários 
                    alternativo
   -k, --keep        não apaga novas mensagens após recuperação
   -l, --limit       não recupera mensagens acima de um dado tamanho
   --mda         configura o MDA a ser usado para reenvio
   -n, --norewrite   não reescreve os cabeçalhos das mensagens
   -p, --protocol    especifica o protocolo de retirada (ver página de 
                    manual)
   -q, --quit        elimina processo servidor
   -r, --folder      especifica o nome da pasta remota
   -s, --silent      trabalha silenciosamente
   -t, --timeout     tempo limite para não obtenção de resposta do servidor
   -u, --username    especifica a identificação do usuário no servidor
   -v, --verbose     trabalha barulhentamente (saídas para diagnóstico)
   -w, --warnings    intervalo entre avisos de notificação de e-mail
   Segredo APOP = "%s".
   Endereços a serem colocados nas linhas RCPT TO enviadas para SMTP serão %s
   Todos os métodos de autenticação disponíveis serão tentados.
   Todas as mensagens serão recuperadas (--all on).
   Será forçada autenticação do CRAM-MD5.
   Forçar os retornos de carro está desativado (forcecr off).
   Forçar os retornos de carro está ativado (forcecr on).
   Corte dos retornos de carro está desativado (Stripcr off).
   Corte dos retornos de carro está ativado (Stripcr on).
   A conexão precisa se dar pela interface %s.
   Pesquisa DNS para endereços multi-entrega está desabilitada.
   Pesquisa DNS para endereços multi-entrega está habilitada.
   Caixa de correio padrão selecionada.
   O intervalo de exclusões entre exclusões definitivas forçado para %d (--expunge %d).
   Linhas Entregue-Para serão rejeitadas (dropdelivered on)
   Linhas Entregue-Para serão mantidas (dropdelivered off)
   Fazer buscas binárias de UIDs durante %d de cada %d votações (--fastuidl %d).
   Fazer busca binária de UIDs durante cada votação (--fastuidl 1).
   Os domínios para os quais e-mails serão recebidos são:   Criptografia end-to-end assumida.
   Assume-se que o cabeçalho do envelope esteja: %s
   O roteamento de endereços de envelope está desabilitado
   O limite de mensagem recebida é %d (--fetchlimit %d).
   Mensagens recebidas serão mantidas no servidor (--keep ativado).
   Mensagens recebidas não serão mantidas no servidor (--keep desativado).
   Será forçada autenticação do GSSAPI.
   O nome da máquina na linha MAIL FROM será %s
   Ocioso após o poll está desabilitado (idle off).
   Ocioso após o poll está habilitado (idle on).
   Interpretação de Content-Transfer-Encoding está desativada (pass8bits ativada)
   Interpretação do Content-Transfer-Encoding está ativada (pass8bits desativada)
   Será forçada autenticação do Kerberos v4.
   Será forçada autenticação do Kerberos v5.
   As conexões com o cliente se darão via "plugout" %s (--plugout %s).
   Domínios locais:   Decodifitação MIME está desabilitada (mimedecode off).
   Decodificação MIME está ativada (mimedecode ativada).
   Será forçada autenticação do MSN.
   Serviço de e-mail principal é: %s
   Mensagens serão recuperadas via %s
   O tamanho limite por mensagem é %d octetos (--limit %d).
   O intervalo de aviso de tamanho de mensagem é de %d segundos (--warnings 
%d).
   As mensagens serão re-enviadas via %cMTP para:   As mensagens serão anexadas a %s como BSMTP
   As mensagens serão entregues com "%s".
   Mensagens com cabeçalhos ruins serão repassadas​.
   Mensagens com cabeçalhos inválidos serão rejeitados.
   Modo de múltipla entrega (multi-drop):    Será forçada autenticação do NTLM.
   Nenhum limite de lote de mensagens SMTP (--batchlimit 0).
   Nenhuma identificação de usuário gravada a partir desta máquina.
   Nenhum limite para tamanho de mensagens (--fetchsizelimit 0).
   Nenhuma exclusão forçada (--expunge 0).
   Não foi especificada nenhuma exigência de interface.
   Nenhum nome local declarado para esta máquina.
   Nenhum limite para tamanho de mensagens (--limit 0).
   Nenhuma interface de monitoramento foi especificada.
   Nenhum comando "plugin" especificado.
   Nenhum comando "plugout" especificado.
   Nenhuma informação de rastreamento de pesquisa será adicionada ao cabeçalho recebido.
   Nenhum comando de pós-conexão.
   Nenhum comando de pré-conexão.
   Nenhuma remoção de prefixo
   Nenhum limite para mensagem recebida (--fetchlimit 0).
   Linhas com Status Preenchido serão rejeitadas (dropstatus on)
   Linhas com Status Preenchido serão mantidas (dropstatus on)
   Número de cabeçalhos de envelope para serem ignorados: %d
   Será forçada autenticação do OTP.
   Mensagens antigas serão descartadas antes de recuperar mensagens (--flush ativado).
   Mensagens antigas não serão descartadas antes de recuperar mensagens (--flush ativado).
   Apenas novas mensagens serão recuperadas (--all off).
   As opções são:
   Mensagens muito longas serão descartadas antes de recuperar mensagens (--limitflush ativado).
   Mensagens muito longas não serão descartadas antes de recuperar mensagens (--limitflush desativado).
   Propriedades de passagem "%s".
   Senha = "%s".
   Será forçada autenticação de senha.
   A senha será solicitada.
   Pesquisa deste servidor ocorrerá a cada %d intervalo.
   Pesquisa deste servidor ocorrerá a cada %d intervalos.
   Informação do caminho de votação será adicionada no cabeçalho Recebido.
   Ciclo de consultas monitorará %s.
   Apelidos pré-declarados do servidor de correio eletrônico:   O prefixo %s será removido da identificação do usuário
   Protocolo é %s   Protocolo é KPOP com autenticação Kerberos %s   Identificação RPOP = "%s".
   O limite de mensagem recebida é %d (--fetchlimit %d).
   As respostas reconhecidas de blocos spam são:   Reescrita do endereço do servidor local está desabilitada (--norewrite off).
   Reescrita do endereço do servidor local está habilitada (--norewrite off).
   O limite de lote de mensagens SMTP é %d.
   Sessões SSL criptografadas habilitadas.
   chave de identificação SSL (checada contra a chave do servidor): %s
   Protocolo SSL: %s.
   Nome comum de servidor SSL: %s
   Verifiação do certificado SSL do servidor habilitada.
   Diretório de certificado SSL confiável: %s
   Arquivo de certificado de SSL confiável: %s
   As caixas de correio selecionadas são:   Apelidos do servidor serão comparados com endereções de multi-entrega pelo endereço IP.
   Apelidos do servidor serão comparados com endereços de múltipla entrega pelo nome.
   A conexão do servidor será efetuada com "%s".
   A conexão com o servidor será derrubada com "%s".
   As conexões com o servidor se darão via plugin %s (--plugin %s).
   O tempo máximo para não obtenção de resposta do servidor é %d segundos   Modo de entrega simples (single-drop):    Avisos de tamanho a cada recebimento (--warnings 0).
   Bloqueio de spam desabilitado
   Esta máquina será consultada quando nenhuma máquina for especificada.
   Esta máquina não será consultada quando nenhuma máquina for especificada.
   O verdadeiro nome do servidor é %s.
  (%d octetos no corpo da mensagem)  (%d octetos no cabeçalho da mensagem)  (%d octetos)  (%d octetos).
  (padrão)  (padrão).
  (forçando o uso de UIDL)  (comprimento -1)  (muito longo)  (autorizado anteriormente)  (usando porta padrão)  (usando serviço %s)  <vazio>  e   eliminada
  não eliminada
  retida
 Conexão %cMTP com %s falhou
 Erro %cMTP: %s
 Cliente %cMTP não gosta do endereço do destinatário `%s'
 Cliente %cMTP não gosta do endereço do destinatário `%s'
 %d nome local reconhecido.
 %d nomes locais reconhecidos.
 %d mensagem (%d %s) para %s %d mensagens (%d %s) para %s %d mensagem para %s %d mensagens para %s %d mensagem sendo aguardada depois da exclusão
 %d mensagens sendo aguardadas depois da exclusão
 %d mensagem aguardando após o primeiro poll
 %d mensagens aguardando após o primeiro poll
 %d mensagem aguardando após o re-poll
 %d mensagens aguardando após o re-poll
 %lu não foi visto
 %s (mensagem de log incompleta)
 %s em %s %s em %s (pasta %s) configuração de %s inválida, LMTP não pode usar a porta SMTP padrão
 %s conexão para %s falhou %s erro enquanto recebendo de %s@%s
 %s erro enquanto recebendo de %s@%s e entregando para a máquina SMTP %s
 %s impressão não combina!
 %s impressão combina.
 %s impressão de chave: %s
 %s consultando %s (protocolo %s) em %s: consulta completa
 %s consultando %s (protocolo %s) em %s: apanhamento iniciado
 Servidor SMTP de %s não suporta ATRN
 Servidor SMTP de %s não suporta ESMTP
 Servidor SMTP de %s não suporta ETRN
 %s: A variável de ambiente NULLMAILER_FLAGS está configurada.
Isso é perigoso pois pode ser feito um nullmailer-inject ou um envelope
nullmailer do sendmail nos cabeçalhos com o seu De:, ou Message-ID: ou Caminho-de-retorno:.
Tente "env NULLMAILER_FLAGS= %s SEUS ARGUMENTOS AQUI"
%s: Abortar.
 %s: A variável de ambiente QMAILINJECT está configurada.
Isso é perigoso pois pode ser feito um qmail-inject ou um envelope sendmail do qmail 
nos cabeçalhos De: ou Message-ID:.
Tente "env QMAILINJECT= %s SEUS ARGUMENTOS AQUI"
%s: Abortar.
 %s: Você não existe. Vá embora.
 %s: não foi possível determinar sua máquina! %s: a atualização oportuna para TLS falhou, tentando continuar.
 %s: a atualização oportuna para TLS falhou, tentando continuar.
 %s: a atualização para TLS falhou.
 %s: atualização para TLS concluída com sucesso.
 %s:%d: atenção: encontrado "%s" antes de qualquer nome de máquina
 %s:%d: warning: símbolo desconhecido "%s"
 %u é a primeira não vista
 %u é não vista
 -- 
O daemon do Fetchmail --check mode enabled, not fetching mail
 ...versão reescrita é %s.
 Solicitação ATRN rejeitado.
 Sobre reescrever %s...
 Todas as conexões estão travadas. Encerrando.
 Autenticação requerida.
 Autorização OK em %s@%s
 Falha de autorização em %s@%s%s
 Abertura de arquivo BSMTP falhou: %s
 Escrita preâmbula BSMTP falhou: %s.
 Reposta em base64 mal formulada do servidor.
 Certificado Ruim: Nome alternativo do assunto contém NUL, abortando!
 Certificado ruim: Nome Comum do Assunto é NULO, abortando!
 Certificado ruim: NomeComum do assunto e muito grande!
 fetchall e keep ao mesmo tempo em modo daemon ou espera é um erro!
 Não foi possível encontrar meu próprio host no banco de dados hosts para qualificá-lo!
 Não pode tratar resposta UIDL do servidor de upstream.
 Não foi possível abrir o arquivo fetchids %s para escrever: %s
 Não foi possível renomear o arquivo fetchids %s para %s: %s
 Impossível resolver serviço %s para número de porta.
 Não pode trocar o id efetivo do usuário de volta para o original %ld: %s
 Não pode trocar o id efetivo do usuário para %ld:%s
 Certificado em camada %d:
 Tabela de Certificado, da raíz ao peer, começando pela camada %d:
 Verificação de Certificado/impressão digital foi de alguma forma suspensa
 Desafio decodificado: %s
 Verificando se %s é realmente o mesmo nó que %s
 Comando não implementado
 Erros de conexão para esta colsulta:
%s Copyright (C) 2002, 2003 Eric S. Raymond
Copyright (C) 2004 Matthias Andree, Eric S. Raymond,
                   Robert M. Funk, Graham Wilson
Copyright (C) 2005 - 2012 Sunil Shetye
Copyright (C) 2005 - 2013 Matthias Andree
 Não foi possível decodificar o desafio OTP
 Não foi possível obter nome do serviço para [%s]
 Não foi possível desempacotar dados do nível de segurança
 Troca de credenciais completa
 Tentado ler socket Cygwin outra vez
 Falha ao tentar ler socket Cygwin outra vez
 Busca no DNS Erro fatal Excluindo arquivo fetchids.
 Buffer do texto para resumo é muito pequeno!
 ERRO: não há suporte à rotina getpassword()
 Autenticação ESMTP CRAM-MD5...
 Autenticação ESMTP LOGIN...
 Autenticação ESMTP PLAIN...
 O suporte a ETRN não está configurado.
 Erro de sintaxe ETRN
 Erro de sintaxe nos parâmetros ETRN
 EVP_md5() falhou!
 Digite a senha para %s@%s:  Erro criando solicitação de nível de segurança
 Erro ao excluir %s: %s
 Erro na troca de credenciais
 Erro na liberação de credenciais
 Occoreu um erro ao escrever no MDA: %s
 Erro de escrita no arquivo fetchids %s, o arquivo antigo foi mantido no lugar.
 O Fetchmail vêm sem NENHUMA GARANTIA. Isto é software livre, e você
pode redistribuí-lo sob certas condições. Para detalhes,
leia o arquivo COPYING no código-fonte ou diretório da documentação.
 O fetchmail não pôde receber o correio eletrônico de %s@%s.
 Fetchmail notou mais que %d vencimentos de temporização enquanto tentava baixar mensagens de %s@%s.
 Fetchmail conseguiu conectar-se à %s@%s.
 Fetchmail irá enviar mensagens de correio de erro para o postmaster.
 Fetchmail irá enviar mensagens de correio de erro para o remetente.
 Fetchmail reenviará mensagens multidrop mal endereçadas para %s.
 Fetchmail fará mascaramento e não gerará `Received'
 Fetchmail mostrará marcação de progresso até em arquivos de relatório.
 Fetchmail vai tratar erros permanentes como permanentes (descartar mensagens).
 Fetchmail vai tratar erros permanentes como temporários (manter mensagens).
 Arquivo %s deve ser um arquivo regular.
 Você têm que ser o dono do arquivo %s.
 Arquivo %s precisa de permissão superior à -rwx------ (0700).
 Descritor de arquivo fora da faixa para SSL Para ajuda, veja http://www.fetchmail.info/fetchmail-FAQ.html#R15
 Erro GSSAPI %s:%.*s
 Erro GSSAPI em gss_display_status chamado por <%s>
 O suporte a GSSAPI está configurado, mas não foi compilado.
 Resposta recebida
 Recebido retorno de resposta %d [%s]
 Adivinhar do cabeçalho "%-.*s".
 Hdr não é 60
 O suporte a IMAP não está configurado.
 Arquivo de identificação é %s
 Se você quer usar GSSAPI, inicialmente precisará de credenciais, possivelmente do kinit.
 Entrada de dados binários:
 Busca de resposta incorreta: %s.
 Timestamp APOP inválido.
 Protocolo SSL `%s' inválido foi especificado; usando padrão (SSLv23).
 Autenticação especificada `%s' inválida.
 Política inválida de cabeçalhos ruins `%s' foi especificada.
 Protocolo `%s' inválido foi especificado.
 Identificação de usuário ou frase-senha inválidos NomeComum do Emissor: %s
 Organização do Emissor: %s
 O suporte a KERBEROS v4 está configurado, mas não foi compilado.
 O suporte a KERBEROS v5 está configurado, mas não foi compilado.
 Suporte a Kerberos v4 não incluído (vinculado).
 Suporte a Kerberos v5 não incluído (vinculado).
 Erro na entrega LMTP no EOM
 Servidor líder não possui nome.
 Erro de travamento (lock-busy) para %s@%s
 Arquivo de registro é %s
 MD5 está sendo aplicado ao bloco de dados:
 resultado MD5 é:
 MDA O MDA foi encerrado pelo sinal %d
 Abertura MDA falhou
 O MDA retornou o status diferente de zero %d
 Tamanho máximo do símbolo GSS é %ld
 Campo mecanismo incorreto
 Lista de UID mesclada de %s: Falha no encerramento da mensagem ou fechamento do arquivo BSMTP : %s
 Mensagens inseridas numa lista no servidor. Não posso tratar isso.
 Nova lista de UID de %s: Nenhum endereço IP encontrado para %s Destinatário do envelope não encontrado, recorrendo a adivinhação pelo cabeçalho.
 Nenhuma interface encontrada com o nome %s Nenhuma mensagem para %s
 Nenhum servidor de correio eletrônico configurado -- talvez %s esteja faltando?
 Nenhuma mensagem esperando para %s
 Não foram encontradas credenciais GSSAPI adequadas. Pulando autenticação GSSAPI.
 Não, os endereços IP não coincidem
 Nó %s não permitido: %s
 O suporte a ODMR não está configurado.
 Lista de UID antiga de %s: OpenSSL informou: %s
 Opção --all não é suportada com %s
 Opção --check não é compatível com ETRN
 Opção --check não é compatível com ETRN
 Opção --flush não é suportada com %s
 Opção --flush não é compatível com ETRN
 Opção --flush não é compatível com ODMR
 Opção --folder não é suportada com ETRN
 Opção --folder não é compatível com ODMR
 Opção --folder não é suportada com POP3
 Opção --keep não é compatível com ETRN
 Opção --keep não é compatível com ODMR
 Opção --limit não é suportada com %s
 Opções para consulta de %s@%s:
 Sem memória!
 Saída de dados:
 O suporte a POP2 não está configurado.
 O suporte a POP3 não está configurado.
 Analisando nomes recebidos "%-.*s"
 Análise de envelope "%s" nomes "-%.*s"
 Mensagens pendentes para %s iniciadas
 Por favor, especifique o serviço como um número de porta decimal.
 Intervalo de consulta é %d segundos
 Baixando %s
 Mensagens de progresso serão gravadas pelo syslog
 Protocolo identificado como IMAP2 ou IMAP2BIS
 Protocolo identificado como IMAP4 rev 0
 Protocolo identificado como IMAP4 rev 1
 Situação da consulta=%d
 Situação da consulta=0 (SUCCESSO)
 Situação da consulta=1 (SEM-MENSAG)
 Situação da consulta=10 (SMTP)
 Situação da consulta=11 (DNS)
 Situação da consulta=12 (BSMTP)
 Situação da consulta=13 (OBTENÇÃO-MAX)
 Situação da consulta=2 (SOCKET)
 Situação da consulta=3 (FALHA-AUTENT)
 Situação da consulta=4 (PROTOCOLO)
 Situação da consulta=5 (SINTAXE)
 Situação da consulta=6 (ERRO-ENTRADA-SAÍDA)
 Situação da consulta=7 (ERRO)
 Situação da consulta=8 (EXCLUSÃO)
 Situação da consulta=9 (TRAVA-OCUPADO)
 Armazenamento para %s iniciado
 RPA falhou na abertura de /dev/urandom. Isto não deveria
 Erro no comprimento da chave da sessão RPA: %d
 Cadeia RPA muito longa
 Erro no comprimento da autenticação de usuário RPA: %d
 Autenticação de _serviço_ RPA falhou. Enganar o servidor?
 Autorização RPA completa
 Erro RPA na cadeia serviço@domínio
 Você foi rejeitado pelo RPA por motivos desconhecidos
 Você foi rejeitado pelo RPA: %s
 status RPA: %02X
 Erro no comprimento do token 2 RPA
 RPA token 2: Erro de decodificação Base64
 Erro no comprimento do símbolo 4 RPA
 RPA token 4: Erro de decodificação Base64
 Lista de domínios: %s
 Recebido resposta ADEUS do servidor IMAP: %s Desafio mal formado recebido para "%s GSSAPI"!
 Liberando credenciais GSS
 Tentar imediatamente em %s@%s
 Data/horário APOP exigidos não encontrados na saudação
 Capacidade de LOGIN exigida não é suportada pelo servidor
 Fetchmail não foi compilado com a capacidade NTLM requerida
 Fetchmail não foi compilado com a capacidade OTP requerida
 Usuário restrito (algo errado com a conta) Mensagem de roteamento versão %d não reconhecido. SDPS não habilitado. Cliente SMTP recusou a entrega
 Cliente SMTP rejeitou o endereço do destinatário:  Servidor SMTP requer STARTTLS, guardando mensagem.
 Transação SMTP SMTP: (corpo da mensagem de retorno)
 Conexão SSL falhou.
 SSL não está habilitado O suporte a SSL não foi compilado.
 Erro salvo é ainda %d
 Rascunho da lista de UIDs: Frase-senha secreta:  Enviando credenciais
 NomeComum do Servidor é diferente: %s != %s
 Erro de servidor ocupado em %s@%s
 Erro na verificação do certificado do servidor: %s
 Certificado do servidor:
 Nome do servidor não especificado, não é possível verificar o certificado!
 Nome do servidor não especificado no certificado!
 Servidor recusou o comando AUTH.
 Servidor requer integridade e/ou privacidade
 Servidor respondeu com UID para mensagem errada.
 Desafio de serviço (l=%d):
 Serviço escolheu a versão %d.%d do RPA
 O serviço foi restaurado com sucesso.
 Data/horário do serviço %s
 Chave de sessão estabelecida:
 Estranho: pclose do MDA retornou %d e errno %d/%s. Impossível manusear em %s:%d
 Cadeia '%s' não é uma cadeia de números válidos.
 Nome alternativo do Assunto: %s
 Nome Comum do Assunto: %s
 Assunto: Alerta do Fetchmail mensagens muito grandes Assunto: autenticação do fetchmail OK em %s@%s Assunto: falha de autenticação do fetchmail em %s@%s Subject: fetchmail vê estouros de tempo-limite repetitivos Sucesso TLS é obrigatório nesta sessão mas o servidor recusou o comando CAPA.
 Utilizando opções da linha de comando%s%s
 O comando CAPA é nessessário para o TLS.
 Falha ao tentar adquirir autorização.

Nós já conseguimos uma autorização para está

conexão, provavelmente outro tipo de falha (como servidor ocupado)

que o fetchmail não pode distinguir porque o servidor não enviou

uma mensagem de erro útil. A tentativa de autorização falhou.
Isso provavelmente indica que sua senha está incorreta, mas alguns servidores têm
outros tipos de falha que o fetchmail não consegue distinguir desta
porque eles não enviam uma mensagem de erro válida quando o login falha.

O serviço fetchmail continuará executando e tentando conectar a cada
ciclo. Nenhuma notificação futura será enviada até o serviço
ser reestabelecido. As seguintes mensagens acima do tamanho permitido continuam no servidor %s, na conta %s: As seguintes mensagens acima do tamanho permitido foram deletadas no servidor %s, na conta %s: Isto pode significar que seu servidor de email está travado, ou
que seu servidor SMTP está emperrado, ou que seu arquivo de caixa postal
no servidor foi corrompido por um erro do servidor. Você pode rodar
`fetchmail -v -v' para diagnosticar o problema.

Fetchmail não vai consultar mais esta caixa postal até que seja reiniciado.
 Este é o fetchmail versão %s Erro de sintaxe na data/horário da saudação
 Comprimento do símbolo %d não combina com rxlen %d
 Erro no comprimento do Token
 Tentando adicionar a opção --service (veja o FAQ item R12 ).
 Tentando conectar a %s/%s... Tentando continuar com nome de máquina não-qualificado.
NÃO relate falhasrecebidas de: cabeçalhos, linhas HELO/EHLO quebradas ou problemas similares!
Em vez disso, corrija seu /etc/hosts, DNS, NIS ou LDAP.
 Dando a volta agora...
 Não foi possível abrir a interface kvm. Certifique-se que fetchmail está SGID kmem. Impossível interpretar nome da interface de %s Impossível processar requisição ATRN agora
 Incapaz de armazenar mensagens para o nó %s
 Solicitação de protocolo não definida em POP3_auth
 Resposta não-503 para o EOM LMTP: %s
 Unicode:
 Erro ETRN desconhecido %d
 NomeComum do Emissor Desconhecido
 Erro ODMR "%s" desconhecido
 Organização Desconhecida
 Nome comum de servidor desconhecido
 Erro desconhecido de login ou autenticação em %s@%s
 Erro desconhecido do sistema Desempacotadas marcas de nível de segurança: %s%s%s
 Autenticação de usuário (l=%d):
 Desafio de usuário:
 Usando nome do serviço [%s]
 Valor da cadeia '%s' é %s que %d.
 AVISO: Executando como root não é aconselhável.
 Alerta: "Maillennium POP3" encontrado, usando o comando RETR ao invés de TOP.
 Aviso: NomeComum do emissor muito longo (possivelmente truncado).
 Aviso: Nome da Organização do Emissor muito longa (possivelmente  truncada).
 Aviso: ignorando dados errados para os tamanhos de mensagem retornados pelo servidor.
 Atenção: múltiplas entradas para a máquina %s no arquivo de configuração
 Aviso: A conexão é insegura, continuando de toda forma. (Melhor usar --sslcertck!)
 Executamos todos os autenticadores permitidos e não é possível continuar.
 Erro de escrita no arquivo fetchids %s: %s
 Escrevendo arquivo fetchids.
 Sim, os endereços IP coincidem
 Você não tem mensagens.
 prestes a entregar para: %s
 atividade em %s -percebida- como %d
 atividade em %s checada como %d
 atividade em %s foi %d, é %d
 analisando linha Received:
%s tentativa de re-executar fetchmail falhou.
 tentativa de re-execução poderá falhar visto que o diretório não foi restaurado com sucesso
 acordado em %s
 acordado por %s
 acordado pelo sinal %d
 segundo plano Contagem espúria de exclusões em "%s"! Contagem de mensagens falsas em "%s"! contador de mensagens sem sentido! não é possível nem mandar para %s!
 impossível executar o listener; voltando para %s Não foi possível criar o socket: %s
 erro no desafio
 protocolo cliente/servidor sincronização cliente/servidor conectado.
 Falha na conexão
 A conexão com %s:%s [%s/%s] falhou: %s
 não foi possível decodificar o pergunta de confirmação BASE64
 não foi possível decodificar resposta "pronto" de BASE64
 não foi possível decodificar o desafio BASE64 inicial
 não foi possível obter o diretório de trabalho atual
 não pode abrir %s para anexar aos logs
 não foi possível buscar cabeçalhos, mensagem %s@%s:%d (%d octetos)
 não foi possível encontrar caixa postal HESIOD para %s
 não foi possível encontrar o nome DNS canônico de %s (%s): %s
 não foi possível fazer checagem de horário de %s (erro %d)
 não foi possível checar o horário do arquivo de controle de execução
 erro dec64 no caractere %d: %x
 decodificado como %s
 descartando nova lista de UID
 dup2 falhou
 final da entrada erro ao escrever o texto da mensagem
 execvp(%s) falhou
 exclusão falhou.
 fetchlimit %d alcançado; %d mensagem deixada no servidor %s, na conta %s
 fetchlimit %d alcançado; %d mensagens deixada no servidor %s, na conta %s
 fetchmail: %s configuração inválida, RPOP requer uma porta privilegiada
 fetchmail: %s configuração inválida, especifique um número positivo para o serviço ou porta
 fetchmail: %s fetchmail em %ld morto.
 fetchmail: Não foi possível rodar em background. Abortando.
 fetchmail: Erro: múltiplos registros "padrões" no arquivo de configuração.
 fetchmail: outro fetchmail com prioridade maior está executando em %ld.
 fetchmail: fetchmail em segundo plano em %ld foi despertado.
 fetchmail: não é possível aceitar opções com outro fetchmail rodando em 
segundo plano.
 fetchmail: não é possível checar por mensagens enquanto outro fetchmail está rodando para a mesma máquina.
 fetchmail: não é possível encontrar uma senha para %s@%s.
 fetchmail: não é possível consultar hosts especificados com outro fetchmail rodando em %ld.
 fetchmail: irmão mais velho em %ld morreu misteriosamente.
 fetchmail: erro encerrando fetchmail %s em %ld; saindo.
 fetchmail: erro ao abrir arquivo lock "%s": %s
 fetchmail: erro lendo arquivo de trava "%s": %s
 fetchmail: fork falhou
 fetchmail: opção de interface é suportada apenas no Linux (sem IPv6) e FreeBSD.
 fetchmail: iniciado com fetchmail: falha na criação do arquivo de lock.
 fetchmail: falha de alocação de memória (malloc)
 fetchmail: opção monitor é suportada apenas pelo Linux (sem IPv6) e FreeBSD
 fetchmail: nenhum servidor de correio eletrônico foi especificado.
 fetchmail: nenhum outro fetchmail está rodando
 fetchmail: removendo arquivos de trava antigos
 fetchmail: socketpair falhou
 fetchmail: tarefa aguardando por %d segundos.
 fetchmail: warning: nenhum DNS disponível para checar buscas com múltiplas
entregas de %s
 primeiro plano reenvio e remoção suprimidos devido a erro de DNS
 repassando para %s
 encontrado endereço Received `%s'
 get_ifinfo:malloc falhou get_ifinfo: sysctl (iflist estimado) falhou get_ifinfo: sysctl (iflist) falhou erro getaddrinfo("%s","%s"): %s
 erro getaddrinfo(NULL, "%s"): %s
 gethostbyname falhou para %s
 id=%s (num=%d) foi excluído, mas ainda está presente!
 id=%s (num=%u) foi excluído, mas ainda está presente!
 Linha incorreta de cabeçalho foi encontrada - veja a página de manual para a opção de bad-header
 intervalo não atingido, não consultando %s
 endereço IP da interface inválido
 máscara IP da interface inválida
 erro no kerberos: %s
 krb5_sendauth: %s [servidor informa '%s']
 maior linha aceita, %s é um apelido do servidor de email
 linha rejeitada, %s não é um apelido do servidor de email
 linha %s arquivo de lock existente no servidor arquivo de travamento presente! Há outra sessão ativa?
 exclusões na conta de e-mail não correspondem (%d real != %d esperado)
 e-mail de %s retornado para %s
 seleção de caixa postal falhou
 A rotina malloc falhou
 %s mapeado localmente para %s
 Endereço %s mapeado para o local %s
 mensagem %s@%s:%d não foi do comprimento esperado (%d atual != %d esperado)
 a mensagem possui NULs inseridos faltando endereço IP da interface
 cabeçalho RFC822 ruim ou faltando Nome %d: não pode criar a familia de socket %d tipo %d: %s
 Nome %d: conexão ao %s:%s [%s/%s] falhou: %s.
 falha na resolução de nomes enquanto procurava por '%s' para a pesquisa de %s: %s
 falha na resolução de nomes ao procurar por `%s' durante a consulta de %s.↵
 nenhum endereço Received encontrado
 não combina com nada localmente, repassando para %s
 nenhum endereço combina. Nenhum postmaster configurado.
 não combina com nada localmente, repassando para %s
 nenhum endereço de destino combina com o valor local declarado instância não nula (%s) pode causar comportamento estranho
 encerramento normal, status %d
 lista de UID não está sendo trocada, nenhum UID foi visto nesta consulta
 passou por %s combinando com %s
 consulta de %s pulada (falha de autenticação ou muitos vencimentos de temporização)
 comando de pós-conexão falhou com status %d
 comando de pós conexão terminada com o sinal %d
 comando de pré-conexão falhou com status %d
 comando de pré conexão terminada com o sinal %d
 %s no bilhete não combina com -u %s
 erro de protocolo
 erro de protocolo durante a obtenção de UIDLs
 nova tentativa de baixar mensagens falhou
 lendo mensagem %s@%s:%d de %d A rotina realloc falhou
 recebendo dados da mensagem
 endereço do destinatário %s não combina com nenhum nome local reiniciando fetchmail (%s mudou)
 executando %s (máquina %s serviço %s)
 falha na busca por mensagens não vistas
 visto vistos selecionando ou tentando novamente baixar mensagens da pasta padrão
 selecionando ou tentando novamente baixar mensagens da pasta %s
 opções de servidor após opções de usuário recv fatal de servidor
 pulando a mensagem %s@%s:%d pulando a mensagem %s@%s:%d (%d octetos) pulando mensagens de %s, endereço IP de %s excluído
 pulando mensagens de %s, %s está parado
 pulando mensagens de %s, %s inativo
 dormindo em %s por %d segundos
 menor erro de escuta no protocolo smtp
 socket iniciando daemon do fetchmail %s.
 lista de UID trocada
 encerrado com o sinal %d
 tempo esgotado após %d segundos esperando por %s.
 tempo esgotado após %d segundos esperando pelo receptor responder.
 tempo esgotado após %d segundos esperando pelo servidor %s.
 tempo esgotado após %d segundos esperando para conectar ao servidor %s.
 tempo esgotado após %d segundos.
 não definido desconhecido desconhecido (%s) protocolo não suportado escolhido.
 uso: fetchmail [opções] [servidor ...]
 aviso: Não solicite suporte se todas as mensagens estão indo para o postmaster!
 aviso: multidrop para %s requer opção envelope!
 ficará ocioso após a pesquisa
 escrevendo cabeçalhos RFC822
 