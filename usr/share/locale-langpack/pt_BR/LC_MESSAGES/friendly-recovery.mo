��    /      �  C                A   /     q     �  &   �     �     �                :     K  �   b     �                2     O     e     s     �  "   �     �     �     �  +   �  ,   "     O  !   f     �     �     �     �  �   �     �  U   �     	     7	     N	    ]	     u
     ~
     �
  	   �
     �
     �
  
   �
  �  �
  #   �  ?   �     �  !        4     R     o     �  '   �     �  '   �  �     #   �  (   �       #   *     N     c     r  '   �  +   �     �     �       F     F   `     �  5   �      �          /     @  �   T     O  f   e  +   �  .   �     '  4  :     o     {     �     �  +   �  4   �  	   �     	           "          #       *         
      /          !                                                            -                     &   ,                      +         )                $       %      '   .   (                    Resume normal boot (Use arrows/PageUp/PageDown keys to scroll and TAB key to select) === Detailed disk usage === === Detailed memory usage === === Detailed network configuration === === General information === === LVM state === === Software RAID state === === System database (APT) === CPU information: Check all file systems Continuing will remount your / filesystem in read/write mode and mount any other filesystem defined in /etc/fstab.
Do you wish to continue? Database is consistent: Drop to root shell prompt Enable networking Finished, please press ENTER IP and DNS configured IP configured Network connectivity: No LVM detected (vgscan) No software RAID detected (mdstat) Physical Volumes: Read-only mode Read/Write mode Recovery Menu (filesystem state: read-only) Recovery Menu (filesystem state: read/write) Repair broken packages Revert to old snapshot and reboot Run in failsafe graphic mode Snapshot System mode: System summary The option you selected requires your filesystem to be in read-only mode. Unfortunately another option you selected earlier, made you exit this mode.
The easiest way of getting back in read-only mode is to reboot your system. Try to make free space Trying to find packages you don't need (apt-get autoremove), please review carefully. Unknown (must be run as root) Update grub bootloader Volume Groups: You are now going to exit the recovery mode and continue the boot sequence. Please note that some graphic drivers require a full graphical boot and so will fail when resuming from recovery.
If that's the case, simply reboot from the login screen and then perform a standard boot. no (BAD) none not ok (BAD) ok (good) unknown (must be run as root) unknown (read-only filesystem) yes (good) Project-Id-Version: friendly-recovery
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2012-03-08 14:38-0500
PO-Revision-Date: 2012-03-15 20:40+0000
Last-Translator: Hriostat <Unknown>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:23+0000
X-Generator: Launchpad (build 18115)
    Continuar inicialização normal (Use as setas/PageUp/PageDown para rolar e TAB para selecionar) === Uso detalhado de disco === === Uso detalhado da memória === === Uso detalhado da rede === === Informações gerais === === Estado do LVM === === Estado do programa RAID === === Banco de dados do sistema (APT) === Informações da CPU: Verificar todos os sistemas de arquivos Ao continuar seu sistema de arquivos será montado no modo leitura e escrita. Além disso, quaisquer outros sistemas de arquivo definidos em /etc/fstab também serão montados.
Deseja continuar? O banco de dados está consistente: Desistir e ir para terminal em modo root Habilitar rede Terminou, por favor pressione ENTER IP e DNS configurado IP configurado Conectividade de rede: Não foi detectada nenhuma LVM (vgscan) Nenhum RAID por programa detectado (mdstat) Volumes físicos: Modo somente-leitura Modo leitura/escrita Menu de recuperação (estado do sistema de arquivos: somente leitura) Menu de recuperação (estado do sistema de arquivos: leitura/escrita) Reparar pacotes quebrados Voltar para ponto de recuperação antigo e reiniciar Executar em modo gráfico seguro Ponto de recuperação Modo do sistema: Sumário do sistema A opção selecionada requer que o seu sistema de arquivos esteja no modo somente leitura. Infelizmente a outra opção que você havia selecionado o fez sair desse modo.
A maneira mais fácil de voltar ao modo somente leitura é reiniciar o sistema. Tente liberar espaço Tentando encontrar os pacotes que você não necessita (apt-get autoremove), revise-os cuidadosamente. Desconhecido (deve ser executado como root) Atualizar o carregador de inicialização grub Grupos de volumes: Agora você sairá do modo de recuperação e seguirá na sequência de inicialização. Note que alguns drivers gráficos requerem inicialização. Isso fará que eles travem ao voltar da recuperação.
Se esse for o caso, simplesmente reinicie a partir da tela de login e execute a inicialização padrão. não (RUIM) nenhum errado (RUIM) ok (bom) desconhecido (deve ser executado como root) desconhecido (arquivo de sistema de somente-leitura) sim (bom) 