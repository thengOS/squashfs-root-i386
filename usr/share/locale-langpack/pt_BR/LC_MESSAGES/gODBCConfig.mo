��    F      L  a   |                      	                     '  ,   4     a  &   s     �     �  $   �     �                    '     3  
   :     E     T     \  	   m     w  
   |     �     �     �     �     �  �   �    �     �
     �
     �
     �
     �
             )   7     a  	   g  
   q  �   |  �  -  E  �  �  %  �   �  �  �  ]     
   t       
   �  �   �  +   /     [  �   d     �          $     C     `  ,   y     �     �     �  D  �  �    �  �     O     Q  	   W     a     m     v     �  7   �     �  6   �          3  '   P  (   x  	   �     �     �     �     �  
   �     �     �               (  
   -     8  '   N     v     �     �  �   �  M  ]     �     �     �      �        #   #       G   0   h   
   �      �      �   �   �   �  �!  t  k#  �  �$  �   �&  �  �'  r   u)     �)  
   �)     *  �   *  ;   �*     �*  �   +     �+     �+      �+  #   ,     /,  2   I,     |,     �,     �,  W  �,  "  .                  @          :   <   7       4                  ?          %       ;   9           >         *            
   )   .   0      1          E   /       "   F      2                             D          A   -       5                       +          '   $       #         C   6              (   =          ,          B   &   3          	   !   8      About Add Application Browse Config Configure... Could not construct a property list for (%s) Could not load %s Could not write property list for (%s) Could not write to %s Could not write to (%s) Couldn't create pixmap from file: %s Couldn't find pixmap file: %s Credits DSN Database System Description Driver Driver Lib Driver Manager Drivers Enter a DSN name FileUsage Name ODBCConfig ODBCConfig - Credits ODBCConfig - Database Systems ODBCConfig - Drivers ODBCConfig - odbc.ini ODBCConfig - odbcinst.ini Open DataBase Connectivity (ODBC) was developed to be an Open and portable standard for accessing data. unixODBC implements this standard for Linux/UNIX. Perhaps the most common type of Database System today is an SQL Server

SQL Servers with Heavy Functionality
  ADABAS-D
  Empress
  Sybase - www.sybase.com
  Oracle - www.oracle.com

SQL Servers with Lite Functionality
  MiniSQL
  MySQL
  Solid

The Database System may be running on the local machine or on a remote machine. It may also store its information in a variety of ways. This does not matter to an ODBC application because the Driver Manager and the Driver provides a consistent interface to the Database System. Remove Select File Select a DSN to Remove Select a DSN to configure Select a driver to Use Select a driver to configure Select a driver to remove Select the DRIVER to use or Add a new one Setup Setup Lib System DSN System data sources are shared among all users of this machine.These data sources may also be used by system services. Only the administrator can configure system data sources. The Application communicates with the Driver Manager using the standard ODBC calls.

The application does not care; where the data is stored, how it is stored, or even how the system is configured to access the data.

The Application only needs to know the data source name (DSN)

The Application is not hard wired to a particular database system. This allows the user to select a different database system using the ODBCConfig Tool. The Driver Manager carries out a number of functions, such as:
1. Resolve data source names via odbcinst lib)
2. Loads any required drivers
3. Calls the drivers exposed functions to communicate with the database. Some functionality, such as listing all Data Source, is only present in the Driver Manager or via odbcinst lib). The ODBC Drivers contain code specific to a Database System and provides a set of callable functions to the Driver Manager.
Drivers may implement some database functionality when it is required by ODBC and is not present in the Database System.
Drivers may also translate data types.

ODBC Drivers can be obtained from the Internet or directly from the Database vendor.

Check http://www.unixodbc.org for drivers These drivers facilitate communication between the Driver Manager and the data server. Many ODBC drivers  for Linux can be downloaded from the Internet while others are obtained from your database vendor. This is the main configuration file for ODBC.
It contains Data Source configuration.

It is used by the Driver Manager to determine, from a given Data Source Name, such things as the name of the Driver.

It is a simple text file but is configured using the ODBCConfig tool.
The User data sources are typically stored in ~/.odbc.ini while the System data sources are stored in /etc/odbc.ini
 This is the program you are using now. This program allows the user to easily configure ODBC. Trace File Tracing Tracing On Tracing allows you to create logs of the calls to ODBC drivers. Great for support people, or to aid you in debugging applications.
You must be 'root' to set Unable to find a Driver line for this entry User DSN User data source configuration is stored in your home directory. This allows you configure data access without having to be system administrator gODBCConfig - Add DSN gODBCConfig - Appication gODBCConfig - Configure Driver gODBCConfig - Driver Manager gODBCConfig - New Driver gODBCConfig - ODBC Data Source Administrator http://www.unixodbc.org odbc.ini odbcinst.ini odbcinst.ini contains a list of all installed ODBC Drivers. Each entry also contains some information about the driver such as the file name(s) of the driver.

An entry should be made when an ODBC driver is installed and removed when the driver is uninstalled. This can be done using ODBCConfig or the odbcinst command tool. unixODBC consists of the following components

- Driver Manager
- GUI Data Manager
- GUI Config
- Several Drivers and Driver Config libs
- Driver Code Template
- Driver Config Code Template
- ODBCINST lib
- odbcinst (command line tool for install scripts)
- INI lib
- LOG lib
- LST lib
- TRE lib
- SQI lib
- isql (command line tool for SQL)

All code is released under GPL and the LGPL license.
 Project-Id-Version: unixodbc
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2003-12-02 14:45+0000
PO-Revision-Date: 2006-06-01 20:10+0000
Last-Translator: Alexandro Silva <Unknown>
Language-Team: Portuguese (Brazil) <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:46+0000
X-Generator: Launchpad (build 18115)
   Sobre Adicionar Aplicação Procurar Configuração Configurar... Não pode construir uma lista de propriedades para (%s) Não pode carregar %s Não pode escrever uma lista de propriedades para (%s) Não pode escrever para %s Não pode escrever para (%s) Não pode criar o pixmap do arquivo: %s Não pode localizar o arquivo pixmap: %s Créditos DSN Sistema de Banco de Dados Descrição Driver Driver Lib Gerenciador de Driver Drivers Entre com um nome DSN ArquivoUsado Nome ODBCConfig ODBCConfig - Creditos ODBCConfig - Sistemas de Banco de Dados ODBCConfig - Drivers ODBCConfig - odbc.ini ODBCConfig - odbcinst.ini Open DataBase Connectivity (ODBC) foi desenvolvido para ser um  padrão Aberto e portável para acesso a dados. unixODBC implementa este padrão para Linux/UNIX. Talvez o tipo mais comum de Sistema de Banco de Dados hoje é um Servidor SQL



Servidores SQL com Pesadas Funcionalidades

  ADABAS-D

  Empress

  Sybase - www.sybase.com

  Oracle - www.oracle.com



Servidores SQL com Leves Funcionalidades

  MiniSQL

  MySQL

  Solid



O Sistema de Banco de Dados podem funcionar em uma máquina local ou em uma máquina remota. Ele tambem pode armazenar informações em caminhos variados. Isto não é problema para uma aplicação ODBC porque o Gerenciador de  Driver e o Driver provê uma interface consistente para o sistema de Banco de Dados. Remover Selecione o Arquivo Selecione um DSN para Remover Selecione um DSN para configurar Selecione um driver para Uso Selecione um driver para configurar Selecione um driver para remover Selecione o DRIVER para usar ou Adicione um novo Configurar Configuração Lib Sistema DSN O Sistema fonte de dados  é compartilhado entre todos os usuários desta máquina.Estes data sources podem também ser usados pelos serviços do sistema. Somente o administrador pode configurar o sistema fonte de dados. A Aplicação comunica com o Gerenciador de Driver usando as chamadas padrões do ODBC.



A aplicação não cuida; quando o dado é armazenado, como é armazenado, ou como o sistema é configurado para acessar o dado.



A Aplicação somente precisa saber o nome da fonte de dados (DSN)



A Aplicação não têm um link para um banco de dados em particular. Isto permite que o usuário selecione diferentes banco de dados usando a ferramenta ODBCConfig. O Gerenciador de  Driver realiza um número de funcões, como:

1. Resolve o nome das fontes de dados via odbcinst lib)

2. Carrega alguns drivers solicitados

3. Chama os drivers das funções expostas para comunicar com o banco de dados. Algumas funcionalidades, como listar todas as Fontes de Dados, está presente somente no Gerenciados de Driver ou via odbcinst lib). Os Drivers ODBC contêm códigos especificos para um Sistema de Banco de Dados e provê um set de funções acessíveis pelo Gerenciador de Driver.

Drivers podem implementar algumas funcionalidades do banco de dados quando é solicitado pelo ODBC e não está presente no Sistema de Banco de Dados.

Drivers também podem ser traduzidos por tipos de dados.



Drivers ODBC podem ser obtidos da Internet ou diretamente do fabricante do Banco de Dados.



Procure por drivers em http://www.unixodbc.org Estes drivers facilitam a comunicação entre o Gerenciador de Driver e o servidor de dados. Muitos drivers ODBC para Linux podem ser baixados da Internet enquanto outros são obtidos do fabricante do banco de dados . Este é o arquivo de configuração principal do ODBC.

Ele contem a configuração das Fontes de Dados.



Ele é usado pelo Gerenciador de Driver para determinar, de um Nome de Fonte de Dados dado, coisas como o nome do  Driver.



É um arquivo de texto simples mas é configurado usando a ferramenta ODBCConfig.

O Usuário Fonte de dados é tipicamente armazenado no ~/.odbc.ini enquanto a fonte de dados do Sistema é armazenado no /etc/odbc.ini
 Este é o programa que você está usando agora. Este programa permite que o usuário configure facilmente o ODBC. Arquivo de Análise Analisando Analisando On O tracing permite que você crie logs das chamados dos drivers ODBC. Bom para suporte, ou para ajudar no debugging de aplicações.

Você deve ser 'root' para definir Impossível localizar uma linha do Driver para esta entrada Usuário DSN O usuário de configuração da fonte de dados é armazenado em seu diretório home. Isto permiti que você configure o acesso aos dados sem ser um administrador do sistema. gODBCConfig - Adicionar DSN gODBCConfig - Aplicação gODBCConfig - Configure o Driver gODBCConfig - Gerenciador de Driver gODBCConfig - Novo Driver gODBCConfig - Administrador da Fonte de Dados ODBC http://www.unixodbc.org odbc.ini odbcinst.ini odbcinst.ini comtem uma lista de todos os Drivers ODBC instalados. Cada entrada tambem comtem algumas informações sobre o driver coisas como o nome(s) do driver.



Uma entrada será feita quando um driver ODBC é instalado e removido quando o driver desinstalado. Isto pode ser feito usando o ODBCConfig ou a ferramenta de comando odbcinst. unixODBC consiste dos seguintes componentes:



- Gerenciado de Driver

- Interface Gráfica do Gerenciador de Dados

- Interface Gráfica de Configuração

- Diversos Drivers e libs de Configuração de Driver

- Modelo de Código do Driver Code

- Modelo de Código de Configuração do Driver

- ODBCINST lib

- odbcinst (ferramenta de linha de comando para instalar scripts)

- INI lib

- LOG lib

- LST lib

- TRE lib

- SQI lib

- isql (ferramenta de linha de comando para o SQL)



Todo o código é liberado sob as licenças GPL e LGPL.
 