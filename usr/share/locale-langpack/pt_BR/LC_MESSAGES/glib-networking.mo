��            )   �      �     �     �  #   �  #   �  #     #   1  #   U     y  "   �  &   �  $   �               +     A     X  $   p  &   �     �  -   �     	  ^   )     �  "   �  .   �     �  N         O     l  0   �     �  )   �  /   	  1   3	  /   e	  1   �	  #   �	  $   �	  #   
  &   4
     [
  $   c
     �
     �
     �
  (   �
  -   �
  $   &  &   K  !   r  X   �     �  #   �  '        A  X   a     �                                                                                                        	             
                     Certificate has no private key Connection is closed Could not create TLS connection: %s Could not parse DER certificate: %s Could not parse DER private key: %s Could not parse PEM certificate: %s Could not parse PEM private key: %s Error performing TLS close: %s Error performing TLS handshake: %s Error reading data from TLS socket: %s Error writing data to TLS socket: %s Module No certificate data provided Operation would block PKCS#11 Module Pointer PKCS#11 Slot Identifier Peer failed to perform TLS handshake Peer requested illegal TLS rehandshake Proxy resolver internal error. Server did not return a valid TLS certificate Server required TLS certificate Several PIN attempts have been incorrect, and the token will be locked after further failures. Slot ID TLS connection closed unexpectedly TLS connection peer did not send a certificate The PIN entered is incorrect. This is the last chance to enter the PIN correctly before the token is locked. Unacceptable TLS certificate Project-Id-Version: glib-networking master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=glib&keywords=I18N+L10N&component=network
POT-Creation-Date: 2016-05-19 08:00+0000
PO-Revision-Date: 2013-04-16 20:36+0000
Last-Translator: Antonio Fernandes C. Neto <Unknown>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:01+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
 O certificado não contém nenhuma chave privada A conexão está encerrada Não foi possível criar conexão TLS: %s Não foi possível analisar certificado DER: %s Não foi possível analisar chave privada DER: %s Não foi possível analisar certificado PEM: %s Não foi possível analisar chave privada PEM: %s Erro ao executar fechamento TLS: %s Erro executando negociação TLS: %s Erro ao ler dados do socket TLS: %s Erro ao gravar dados do socket TLS: %s Módulo Nenhum dado de certificado fornecido A operação bloquearia PKCS#11 Module Pointer PKCS#11 Slot Identifier Peer falhou ao realizar negociação TLS O peer requisitou uma negociação TLS ilegal Erro interno do resolvedor de proxy. Servidor não retornou certificado TLS O servidor requer certificado TLS O PIN foi digitado várias vezes incorretamente, por isso o token será bloqueado agora. Slot ID Conexão TLS fechou inesperadamente Conexão TLS não enviou um certificado O PIN digitado está incorreto. Esta é a última chance de digitar o PIN corretamente antes que o token seja bloqueado. Certificado TLS inaceitável 