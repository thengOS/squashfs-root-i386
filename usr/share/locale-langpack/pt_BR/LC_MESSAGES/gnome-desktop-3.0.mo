��          �   %   �      0     1     4     :     @     I     V     f     y     �     �     �     �     �  $   �  D        H     b     n  %   �  F   �  �   �  *   �  i   �    X     k     n     t  	   {     �     �     �     �     �     �     �     �  *     !   ,  H   N     �     �  !   �  5   �  L   	  �   l	  *   :
  ~   e
                                                             
                        	                                          %R %R:%S %a %R %a %R:%S %a %b %e, %R %a %b %e, %R:%S %a %b %e, %l:%M %p %a %b %e, %l:%M:%S %p %a %l:%M %p %a %l:%M:%S %p %l:%M %p %l:%M:%S %p CRTC %d cannot drive output %s CRTC %d does not support rotation=%d CRTC %d: trying mode %dx%d@%dHz with output at %dx%d@%dHz (pass %d)
 Trying modes for CRTC %d
 Unspecified cannot clone to output %s could not assign CRTCs to outputs:
%s none of the selected modes were compatible with the possible modes:
%s output %s does not have the same parameters as another cloned output:
existing mode = %d, new mode = %d
existing coordinates = (%d, %d), new coordinates = (%d, %d)
existing rotation = %d, new rotation = %d output %s does not support mode %dx%d@%dHz required virtual size does not fit available size: requested=(%d, %d), minimum=(%d, %d), maximum=(%d, %d) Project-Id-Version: gnome-desktop
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-desktop&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 22:18+0000
PO-Revision-Date: 2015-12-04 14:52+0000
Last-Translator: Rafael Neri <Unknown>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:58+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
 %R %R:%S %a, %R %a, %R:%S %a, %e de %b, %R %a, %e de %b, %R:%S %a, %e de %b, %l:%M %p %a, %e de %b, %l:%M:%S %p %a, %l:%M %p %a, %l:%M:%S %p %R %R:%S CRTC %d não pode se conectar à saída %s CRTC %d não suporta rotação=%d CRTC %d: tentando o modo %dx%d@%dHz com saída em %dx%d@%dHz (passo %d)
 Tentando modos para CRTC: %d
 Não especificado não pode clonar para a saída %s não foi possível atribuir CRTCs para as saídas:
%s nenhum dos modos selecionados foram compatíveis com os modos possíveis:
%s a saída %s não possui os mesmos parâmetros que a outra saída clonada:
modo atual = %d, novo modo = %d
coordenadas atuais = (%d,%d), novas coordenadas = (%d,%d)
rotação atual = %d, nova rotação = %d a saída %s não suporta o modo %dx%d@%dHz o tamanho virtual requisitado não se encaixa no tamanho disponível: requisitado=(%d, %d), mínimo=(%d, %d), máximo=(%d, %d) 