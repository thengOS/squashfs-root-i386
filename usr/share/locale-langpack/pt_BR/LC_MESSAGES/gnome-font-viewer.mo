��            )         �     �  	   �     �  	   �     �     �     �     �  �     �   �     �     �     �  	   �     �     �  E   �     �          !     '     ,  !   L     n     �     �     �  	   �     �     �  )  �     
             	   '     1     =     Z     q  �   �  �   �	     |
     �
     �
  	   �
     �
     �
  ]   �
                ?     F  "   L  "   o  "   �     �     �     �     �  #   �  �                                                                                    	      
                                                About All Fonts Back Copyright Description FONT-FILE OUTPUT-FILE Font Viewer GNOME Font Viewer GNOME Font Viewer also supports installing new font files downloaded in the .ttf and other formats. Fonts may be installed only for your use or made available to all users on the computer. GNOME Font Viewer shows you the fonts installed on your computer for your use as thumbnails. Selecting any thumbnails shows the full view of how the font would look under various sizes. Info Install Install Failed Installed Name Quit Run '%s --help' to see a full list of available command line options. SIZE Show the application's version Style TEXT Text to thumbnail (default: Aa) This font could not be displayed. Thumbnail size (default: 128) Type Version View fonts on your system [FILE...] fonts;fontface; translator-credits Project-Id-Version: gnome-font-viewer
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-font-viewer&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 22:21+0000
PO-Revision-Date: 2015-12-04 14:45+0000
Last-Translator: Enrico Nicoletto <liverig@gmail.com>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:16+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
 Sobre Todas as fontes Voltar Copyright Descrição ARQUIVO-FONTE ARQUIVO-SAÍDA Visualizador de fontes Visualizador de fontes do GNOME O visualizador de fontes do GNOME também provê suporte a instalação de novos arquivos de fontes baixadas em .ttf e em outros formatos. Fontes também podem ser instaladas apenas para seu uso ou estar disponíveis para todos os usuários no computador. O visualizador de fontes do GNOME mostra a você as fontes instaladas em seu computador para seu uso como miniaturas. Ao selecionar qualquer miniatura é mostrado a você visão completa de como a fonte pareceria sob vários tamanhos. Informação Instalar Instalação falhou Instalado Nome Sair Execute '%s --help' para ver uma lista completa de opções de linha de comando disponíveis. TAMANHO Mostra a versão do aplicativo Estilo TEXTO Texto para miniatura (padrão: Aa) Esta fonte não pôde ser exibida. Tamanho da miniatura (padrão: Aa) Tipo Versão Ver fontes no seu sistema [ARQUIVO...] fontes;tipo de fonte;tipo da fonte; Alexandre Hautequest <hquest@fesppr.br>
Ariel Bressan da Silva <ariel@conectiva.com.br>
Francisco Petrúcio Cavalcante Junior <fpcj@impa.br>
Evandro Fernandes Giovanini <evandrofg@ig.com.br>
Welther José O. Esteves <weltherjoe@yahoo.com.br>
Afonso Celso Medina <medina@maua.br>
Guilherme de S. Pastore <gpastore@colband.com.br>
Luiz Fernando S. Armesto <luiz.armesto@gmail.com>
Leonardo Ferreira Fontenelle <leonardof@gnome.org>
Og Maciel <ogmaciel@gnome.org>
Rodrigo Flores <rodrigomarquesflores@gmail.com>
Hugo Doria <hugodoria@gmail.com>
Djavan Fagundes <dnoway@gmail.com>
Krix Apolinário <krixapolinario@gmail.com>
Antonio Fernandes C. Neto <fernandesn@gnome.org>
Rodrigo Padula de Oliveira <contato@rodrigopadula.com>
André Gondim (In memoriam) <In memoriam>
Breno Felipe Morais de Santana <breno_info@globomail.com>
Rafael Ferreira <rafael.f.f1@gmail.com>
Enrico Nicoletto <liverig@gmail.com>

Launchpad Contributions:
  Breno Felipe Morais de Santana https://launchpad.net/~breno-info
  Enrico Nicoletto https://launchpad.net/~liverig
  Fábio Nogueira https://launchpad.net/~fnogueira
  Holverat Bortolossi https://launchpad.net/~holverat
  Rafael Fontenelle https://launchpad.net/~rffontenelle
  Tiago Hillebrandt https://launchpad.net/~tiagohillebrandt 