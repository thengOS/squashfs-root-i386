��    L      |  e   �      p  $   q     �     �  	   �     �  
   �     �  #   �  #        :     @     G      L     m  !        �    �     �  <   �     	  
    	     +	     8	     D	     J	  "   S	     v	     �	     �	     �	     �	     �	     �	     �	  ;   
  S   A
  2   �
  [   �
  /   $     T     Z  
   f     q     }  $   �     �  #   �     �     �     �          -  �   K                    "  	   )     3     ;     A  	   H     R     V     ]  
   i     t     z     �     �     �     �     �     �     �  S  �  $   H     m     �     �     �     �     �  0   �  !        2     9     G      L     m  3   �     �  g  �     /  A   M     �     �     �     �     �  	   �  +   �  *        8     @     V  $   g  "   �     �     �  N   �       E   �  ~   �  :   `     �     �     �     �     �  +   �       &   !     H     M     _     |     �  �   �     �     �  	   �     �  	   �     �     �     �  
   �     �     �     �     �       
             &  !   6     X     a  *  o  )   �     C   D                !   *   <   E          J      	       G   :   L              +      &      ,                    #   )   3   $               "         @   /             2   H       A      1              '      I   F   .   >                    0   K   ?   6            (               7   9                     B       ;   =       4   5       
             %   -   8    %u × %u, %u mine %u × %u, %u mines <b>%d</b> mine <b>%d</b> mines A_ppearance Big board Big game Board size Change _Difficulty Clear explosive mines off the board Clear hidden mines from a minefield Close Custom Date Do you want to start a new game? Enable animations Enable automatic placing of flags GNOME Mines GNOME Mines is a puzzle game where you search for hidden mines. Flag the spaces with mines as quickly as possible to make the board a safer place. You win the game when you've flagged every mine on the board. Be careful not to trigger one, or the game is over! Height of the window in pixels If you start a new game, your current progress will be lost. Keep Current Game Main game: Medium board Medium game Mines New Game Number of columns in a custom game Number of rows in a custom game Paused Percent _mines Play _Again Print release version and exit Resizing and SVG support: Score: Select Theme Set to false to disable theme-defined transition animations Set to true to automatically flag squares as mined when enough squares are revealed Set to true to be able to mark squares as unknown. Set to true to enable warning icons when too many flags are placed next to a numbered tile. Size of the board (0-2 = small-large, 3=custom) Size: Small board Small game St_art Over Start New Game The number of mines in a custom game The theme to use The title of the tile theme to use. Time Use _animations Use the unknown flag Warning about too many flags Width of the window in pixels You can select the size of the field you want to play on at the start of the game. If you get stuck, you can ask for a hint: there's a time penalty, but that's better than hitting a mine! _About _Best Times _Cancel _Close _Contents _Height _Help _Mines _New Game _OK _Pause _Play Again _Play Game _Quit _Resume _Scores _Show Warnings _Use Question Flags _Width minesweeper; translator-credits true if the window is maximized Project-Id-Version: gnome-mines
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-mines&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-02-11 17:13+0000
PO-Revision-Date: 2016-02-12 02:24+0000
Last-Translator: Rafael Fontenelle <Unknown>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 19:21+0000
X-Generator: Launchpad (build 18115)
X-Project-Style: gnome
Language: pt_BR
 %u × %u, %u mina %u × %u, %u minas <b>%d</b> mina <b>%d</b> minas A_parência Tabuleiro grande Jogo grande Tamanho do tabuleiro Alterar _dificuldade Elimine minas explosivas para fora do tabuleiro. Limpe as minas de um campo minado Fechar Personalizado Data Você quer iniciar um novo jogo? Habilitar _animações Habilitar o posicionamento automático de bandeiras Minas do GNOME O Minas do GNOME é um jogo de quebra-cabeças onde você deve procurar por minas escondidas. Sinalize os espaços que possuem minas o mais rápido possível para fazer do tabuleiro um local mais seguro. Você vence o jogo quando colocou bandeiras em cada mina do tabuleiro. Só tenha cuidado em não disparar uma delas, pois aí você perderá (fim de jogo)! A altura da janela em pixels. Se você iniciar um novo jogo, seu progresso atual será perdido. Guardar o jogo atual Jogo principal: Tabuleiro médio Jogo médio Minas Novo jogo Número de colunas em um jogo personalizado Número de linhas em um jogo personalizado Pausado Porcen_tagem de minas Jogar _novamente Exibe a versão de lançamento e sai Redimensionamento e suporte a SVG: Pontuação: Selecionar tema Defina como falso para desativar animações de transição definido pelo tema Defina como verdadeiro para sinalizar os quadrados automaticamente como minados quando os quadrados suficientes forem revelados Defina como verdadeiro para poder marcar espaços como desconhecidos. Defina como verdadeiro para habilitar ícones de aviso quando muitas bandeiras forem colocadas próximos a uma peça numerada. Tamanho do tabuleiro (0-2 = pequeno-grande, 3=customizado) Tamanho: Tabuleiro pequeno Jogo pequeno Reco_meçar Iniciar um novo jogo O número de minas em um jogo personalizado O tema para usar O título do tema de pedras para usar. Temp Usar _animações Usar a bandeira desconhecida Aviso sobre muitas bandeiras A largura da janela em pixels. Você pode selecionar o tamanho do campo que deseja jogar no início do jogo. Caso você fique preso, você pode solicitar uma dica: há uma penalidade de tempo, mas isto é melhor do que bater em uma mina! _Sobre Mel_hores tempos _Cancelar Fec_har S_umário A_ltura Aj_uda _Minas _Novo jogo _OK _Pausar _Jogar novamente _Iniciar jogo Sai_r Co_ntinuar _Pontuações M_ostrar avisos _Usar bandeiras de interrogação Lar_gura campo minado; Sandro Nunes Henrique <sandro@conectiva.com.br>
Cândida Nunes da Silva <candida@zaz.com.br>
Gustavo Noronha Silva <kov@debian.org>
Alexandre Folle de Menezes <afmenez@terra.com.br>
Maurício de Lemos Rodrigues Collares Neto <mauricioc@myrealbox.com>
Welther José O. Esteves <weltherjoe@yahoo.com.br>
Raphael Higino <In memoriam>
Og Maciel <ogmaciel@ubuntu.com>
Andre Noel <andrenoel@ubuntu.com>
Vladimir Melo <vmelo@gnome.org>
Daniel S. Koda <danielskoda@gmail.com>
Felipe Borges <felipe10borges@gmail.com>
Sérgio Cipolla <secipolla@gmail.com>
Rodrigo Padula de Oliveira <contato@rodrigopadula.com>
Rafael Ferreira <rafael.f.f1@gmail.com>
Enrico Nicoletto <liverig@gmail.com>

Launchpad Contributions:
  Enrico Nicoletto https://launchpad.net/~liverig
  Rafael Fontenelle https://launchpad.net/~rffontenelle verdadeiro se a janela estiver maximizada 