��          �   %   �      p  l   q     �  0   �     #  ,   ?  ,   l  ,   �  '   �  -   �        (   =  (   f     �     �     �  �   �  ?   �     	  )        A     X     l     �     �     �     �     �  �  �  o   �     	  1   	     J	  /   i	  /   �	  /   �	  '   �	  0   !
  "   R
  *   u
  *   �
  $   �
  $   �
           K        h  2   u     �     �     �     �                    0                                     
         	                                                                               -h, --help          display this help and exit
  -v, --version       display version information and exit
 %s home page: <%s>
 %s home page: <http://www.gnu.org/software/%s/>
 %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 ' Copyright (C) %d Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
 General help using GNU software: <http://www.gnu.org/gethelp/>
 Hello, world! Print a friendly, customizable greeting.
 Report %s bugs to: %s
 Report bugs to: %s
 Unknown system error Usage: %s [OPTION]...
 ` hello, world memory exhausted write error Project-Id-Version: hello 2.9
Report-Msgid-Bugs-To: bug-hello@gnu.org
POT-Creation-Date: 2014-11-16 11:53+0000
PO-Revision-Date: 2015-12-05 00:19+0000
Last-Translator: Rafael Ferreira <Unknown>
Language-Team: Brazilian Portuguese <ldpbr-translation@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:33+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
   -h, --help          mostra esta ajuda e sai
  -v, --version       mostra informações sobre a versão e sai
 Página do %s: <%s>
 Página do %s: <http://www.gnu.org/software/%s/>
 %s: opção inválida -- "%c"
 %s: a opção "%c%s" não permite um argumento
 %s: a opção "%s" é ambígua; possibilidades: %s: a opção "--%s" não permite um argumento
 %s: a opção "%s" requer um argumento
 %s: a opção "-W %s" não permite um argumento
 %s: a opção "-W %s" é ambígua
 %s: a opção "-W %s" requer um argumento
 %s: a opção requer um argumento -- "%c"
 %s: opção "%c%s" não reconhecida
 %s: opção "--%s" não reconhecida
 " Copyright (C) %d Free Software Foundation, Inc.
Licença GPLv3+: GNU GPL versão 3 ou posterior <http://gnu.org/licenses/gpl.html>
Este é um software livre; você é livre para alterar e redistribuí-lo.
Há NENHUMA GARANTIA, na extensão permitida pela lei.
 Ajuda geral sobre o uso de softwares do GNU: <http://www.gnu.org/gethelp/>
 Olá, mundo! Mostra um cumprimento amistoso e personalizável.
 Relate erros do %s para: %s.
 Relate erros para: %s
 Erro de sistema desconhecido Uso: %s [OPÇÃO]...
 " olá, mundo memória esgotada erro de gravação 