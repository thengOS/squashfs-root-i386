��    =        S   �      8     9     N  ?   h     �     �  ,   �     �     �                    !  4   >     s     �     �     �     �     �     �     �     �  .   �     (     C     H  C   M     �  �   �  `   ?     �     �  w   �     /	  P   J	     �	     �	  �   �	     �
     �
     �
  	             *     2     B     O     a     o     �  	   �     �     �  
   �     �     �     �     �     �     �  �  �     �     �  P   �     3     8  8   L     �     �     �     �     �  #   �  5   �          !     *     1     8     S     o     x     |  5   �  #   �     �     �  N   �     I  �   b  x        �     �  �   �     E  f   d      �  &   �              3  $   G     l     y     �     �     �     �     �     �     �  	   �            
             $     +     4     :     >         8   <   ,   4   .            (   5   1      =      7   !                            "                 6                               ;       
             *       9                &       0              '   :   #   -   $   2      +                       	       %                 3      )   /    Add phrases in front Add phrases in the front. Always input numbers when number keys from key pad is inputted. Auto Auto move cursor Automatically move cursor to next character. Big5 Candidate per page Chewing Chewing component Chi Choose phrases from backward Choose phrases from the back, without moving cursor. ConfigureApply ConfigureCancel ConfigureClose ConfigureSave Easy symbol input Easy symbol input. Editing Eng Esc clean all buffer Escape key cleans the text in pre-edit-buffer. Force lowercase in En mode Full Half Hsu's keyboard selection keys, 1 for asdfjkl789, 2 for asdfzxcv89 . Hsu's selection key Ignore CapsLock status and input lowercase by default.
It is handy if you wish to enter lowercase by default.
Uppercase can still be inputted with Shift. In plain Zhuyin mode, automatic candidate selection and related options are disabled or ignored. Keyboard Keyboard Type Keys used to select candidate. For example "asdfghjkl;", press 'a' to select the 1st candidate, 's' for 2nd, and so on. Maximum Chinese characters Maximum Chinese characters in pre-edit buffer, including inputing Zhuyin symbols Number of candidate per page. Number pad always input number Occasionally, the CapsLock status does not match the IM, this option determines how these status be synchronized. Valid values:
"disable": Do nothing.
"keyboard": IM status follows keyboard status.
"IM": Keyboard status follows IM status. Peng Huang, Ding-Yi Chen Plain Zhuyin mode Select Zhuyin keyboard layout. Selecting Selection keys Setting Space to select Syncdisable Syncinput method Synckeyboard Sync between CapsLock and IM UTF8 dachen_26 default dvorak dvorak_hsu eten eten26 gin_yieh hanyu hsu ibm Project-Id-Version: ibus-chewing 1.4.11
Report-Msgid-Bugs-To: Ding-Yi Chen <dchen at redhat.com>
POT-Creation-Date: 2016-01-21 05:14+0000
PO-Revision-Date: 2015-12-05 14:33+0000
Last-Translator: Tiago Hillebrandt <tiagohillebrandt@gmail.com>
Language-Team: Portuguese (Brazil)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:39+0000
X-Generator: Launchpad (build 18115)
Language: pt-BR
 Adicionar frases na frente Adicionar frases na frente Sempre inserir números quando as chaves de números a partir do pad é inserida Auto Auto mover o cursor Mover automaticamente o cursor para o próximo carácter Big5 Candidato por página Chewing Componente Chewing Chi Escolher frases de tras para frente Escolehr frases a partir do final, sem mover o cursor Aplicar Cancelar Fechar Salvar Entrada de símbolo fácil Entrada de símbolo fácil. Editando Eng Esc limpar todo o buffer Tecla Escape limpa o texto no buffer de pré-edição Forçar letra minúscula no modo En Cheio Meio teclas de seleção do teclado de Hsu , 1 para asdfjkl789, 2 para asdfzxcv89 . tela de seleção do Hsu Ignorar o estado CapsLock e inserir letra minúscula por padrão.
É fácil se você deseja inserir letras minúsculas por padrão.
A letra maiúscula pode ainda ser inserida com o Shift. Em modo simples de Zhuyin, a seleção de candidato automática e opções relacionadas são desabilitadas ou ignoradas. Teclado Tipo de Teclado Teclas usadas para selecionar candidato. Por exemplo "asdfghjkl;", pressione  'a' para selecionar o 1o. candidato, 's' para o 2o. e assim por diante. Máximo de Caracteres chineses Máximo de caracteres chineses no buffer de pré-edição, incluindo os simbolos Zhuyin de inserção. Número de candidato por página Pad de números sempre insere números Geralmente o status do CapsLock não coincide com o IM, esta opção determina como estes status são sincronizados. Valores válidos:
"disable": Não faz nada.¶
"keyboard": status do IM segue o status do teclado.¶
"IM": Status do Teclado segue o status do IM. Peng Huang, Ding-Yi Chen Modo Simples Zhuyin Selecionar layout do teclado Zhuyin. Selecionando Teclas de seleção Configuração Espaço para selecionar Desabilitar método de entrada Teclado Sinc entre o CapsLock e IM UTF8 dachen_26 padrão dvorak dvorak_hsu eten eten26 gin_yieh hanyu hsu ibm 