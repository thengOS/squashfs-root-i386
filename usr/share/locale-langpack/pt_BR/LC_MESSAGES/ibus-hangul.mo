��          �      l      �  
   �     �     �               3     H     `     z     �  
   �     �  C   �     �     �       �   '     �     �     �  �  �  
   �     �     �  	   �     �                5     V     ]     c     p  h   �     �          +  �   F     �  '      
   (                                      	                                        
                    <b>Etc</b> <b>Hanja key</b> <b>Keyboard Layout</b> Advanced Automatic _reordering Commit in _word unit Configure hangul engine Enable/Disable Hanja mode Hangul Hanja Hanja lock IBus Hangul Preferences IBus daemon is not running.
Hangul engine settings cannot be saved. IBusHangul Setup Korean Input Method Korean input method Press any key which you want to use as hanja key. The key you pressed is displayed below.
If you want to use it, click "Ok" or click "Cancel" Select Hanja key Set IBus Hangul Preferences Setup Project-Id-Version: ibus-hangul
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-03 22:49+0000
PO-Revision-Date: 2013-09-14 00:47+0000
Last-Translator: Vinicius Almeida <vinicius.algo@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:38+0000
X-Generator: Launchpad (build 18115)
 <b>Etc</b> <b>tecla Hanja</b> <b>Layout do teclado</b> Avançado _Reordenação automática Submeter em unidade _word Configurar motor hangul Habilitar/Desabilitar modo Hanja Hangul Hanja Travar Hanja Preferências do IBus Hangul O servidor IBus não está executando.
As configurações do mecanismo Hangul não puderam ser gravadas. Configuração IBusHangul Método de entrada coreano Método de entrada coreano Pressione qualquer tecla que você deseja usar com uma tecla hanja. A tecla pressionada é mostrada abaixo.
Se você quiser usa-la, cliique "Ok" ou clique "Cancel" Selecione tecla Hanja Configurar preferências do IBus Hangul Configurar 