��    K      t  e   �      `     a     v     �     �     �     �     �     �  	           d   /  1   �     �     �     �     �     �               !     *     A  
   V     a     o  &   �  .   �  (   �     	     	     .	     6	     ;	     Q	     V	     b	     r	     �	     �	     �	     �	     �	  
   �	     �	     �	     �	     �	  	   

     
     
     /
     J
     O
     ^
     r
     �
     �
     �
     �
     �
     �
       
        *     3     ?     R     [     _     d     h     z     �     �  �  �          �     �     �     �     �            	   4      >  f   _  1   �     �     �            )   2     \     j     r     {     �     �     �     �  .   �  H     /   ]     �     �     �     �     �     �     �       #        ;     N     T     \     c  
   y     �     �     �     �     �     �     �     �                  '   :  &   b     �     �     �     �  $   �      �               !     -     H     Q     U     Z     ^     t     �     �             ,   A       #      $      )          "   E   @      :   	                  6      &       4                       2   9      !   <   1   5   ;         7              C   F       K   ?   
          8      H   *         '      G                 -   =   %       >   D          I   +             /   0          B         .          3           (           J            <b>Bopomofo mode</b> <b>Correct pinyin</b> <b>Dictionary option</b> <b>Initial state</b> <b>Input Custom</b> <b>Other</b> <b>Pinyin mode</b> <b>Selection mode</b> <b>UI</b> <big><b>IBus Pinyin %s</b></big> <small>
<b>Authors:</b>
Peng Huang
BYVoid
Peng Wu

<b>Contributors:</b>
koterpilla, Zerng07
</small> <small>Copyright (c) 2009-2010 Peng Huang</small> ABC About Auto commit Bopomofo (debug) Bopomofo input method (debug) Bopomofo mode Chinese Chinese: Commit first candidate Commit original text Dictionary Double pinyin Edit custom phrases Enable Auxiliary Select Keys F1 .. F10 Enable Auxiliary Select Keys Numbers on Keypad Enable Guidekey for Candidates Selection Enable correct pinyin Enable fuzzy syllable English Eten Feature of Enter key: Full Full pinyin Full/Half width Full/Half width punctuation Fuzzy syllable General GinYieh Half Half/full width: Horizontal IBM Incomplete Bopomofo Incomplete pinyin Keyboard Mapping: Language: MSPY Number of candidates: Orientation of candidates: PYJJ Pinyin (debug) Pinyin input method Pinyin input method (debug) Pinyin input method for IBus Pinyin mode Preferences Punctuations: Selection Keys: Show raw input of Double Pinyin Simplfied/Traditional Chinese Simplified Standard Traditional Use custom phrases Vertical XHE ZGPY ZRM [,] [.] flip page [-] [=] flip page [Shift] select candidate http://ibus.googlecode.com Project-Id-Version: ibus-pinyin
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-09-20 13:06+0000
PO-Revision-Date: 2013-03-20 00:16+0000
Last-Translator: Neliton Pereira Jr. <nelitonpjr@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:36+0000
X-Generator: Launchpad (build 18115)
 <b>Modo Bopomofo</b> <b>Pinyin correto</b> <b>Opção de dicionário</b> <b>Estado inicial</b> <b>Entrada personalizada</b> <b>Outro</b> <b>Modo Pinyin</b> <b>Modo de seleção</b> <b>IU</b> <big><b>IBus Pinyin %s</b></big> <small>
<b>Autores:</b>
Peng Huang
BYVoid
Peng Wu

<b>Contribuidores:</b>
koterpilla, Zerng07
</small> <small>Copyright (c) 2009-2010 Peng Huang</small> ABC Sobre Submeter automaticamente Bopomofo (depuração) Método de entrada Bopomofo (depuração) Modo Bofomofo Chinês Chinês: Submeter primeiro candidato Submeter texto original Dicionário Pinyin duplo Editar frases personalizadas Ativar teclas de seleção auxiliares F1...F10 Ativar números do teclado numérico como teclas de seleção auxiliares Ativar teclas-guia para seleção de candidatos Ativar Pinyin correto Ativar sílaba aproximada Inglês Eten Funcionalidade da tecla Enter: Completa Pinyin completo Largura inteira/meia Largura de pontuação inteira/meia Sílaba aproximada Geral GinYieh Metade Largura inteira/meia: Horizontal IBM Bopomofo incompleto Pinyin incompleto Mapeamento do teclado: Idioma: MSPY Número de candidatos: Orientação dos candidatos: PYJJ Pinyin (depuração) Método de entrada Pinyin Método de entrada Pinyin (depuração) Método de entrada do Pinyin para IBus Modo Pinyin Preferências Pontuações: Teclas de seleção: Exibir entrada bruta de Pinyin duplo Chinês simplificado/tradicional Simplificado Padrão Tradicional Usar frases personalizadas Vertical XHE ZGPY ZRM [,] [.] virar página [-] [=] virar página [Shift] selecionar candidato http://ibus.googlecode.com 