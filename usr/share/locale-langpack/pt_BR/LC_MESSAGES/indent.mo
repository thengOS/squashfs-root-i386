��    8      �  O   �      �  +   �  %     /   +     [  /   t     �     �     �     �  2     2   ;     n     �     �     �     �     �  1   �     0  G   K     �     �     �     �     �  (   �  '        8     I     ]  5   |     �     �     �     �     	     .	     6	     J	  '   W	  !   	     �	  )   �	     �	     �	  "   
     .
  <   G
  :   �
  0   �
  *   �
  :        V  &   b  ]   �  �  �  +   �  *   �  2   �       6   5  $   l  )   �  &   �  )   �  I     I   V  8   �  *   �          "  $   '  "   L  ;   o  '   �  P   �     $     3     L     Z     j  .   |  )   �     �  $   �  &   	  >   0     o  &   �     �  $   �  "   �               -  /   >  %   n     �  2   �     �     �  #   	     -  Q   K  O   �  :   �  7   (  G   `     �  $   �  w   �     -                       *         .   7       
                       &       (   )                         8   ,      2         /              	      +   '   0   $       #   1       !                        4           %                               6   3                "      5      stack[%d] =>   stack: %d   ind_level: %d
 %s: missing argument to parameter %s
 %s: option ``%s'' requires a numeric parameter
 %s: unknown option "%s"
 (Lines with comments)/(Lines with code): %6.3f
 CANNOT FIND '@' FILE! Can't close output file %s Can't open backup file %s Can't open input file %s Can't preserve modification time on backup file %s Can't preserve modification time on output file %s Can't stat input file %s Can't write to backup file %s EOF encountered in comment Error Error closing input file %s Error reading input file %s File %s contains NULL-characters: cannot proceed
 File %s is too big to read File named by environment variable %s does not exist or is not readable GNU indent %s
 Internal buffering error Line broken Line broken 2 ParseStack [%d]:
 Profile contains an unterminated comment Profile contains unpalatable characters Read profile %s
 Stmt nesting error. System problem reading file %s There were %d non-blank output lines and %d comments
 Unexpected end of file Unknown code to parser Unmatched 'else' Unterminated character constant Unterminated string constant Warning Zero-length file %s command line indent:  Strange version-control value
 indent:  Using numbered-existing
 indent: %s:%d: %s: indent: Can't make backup filename of %s
 indent: Fatal Error:  indent: System Error:  indent: Virtual memory exhausted.
 indent: can't create %s
 indent: can't have filenames when specifying standard input
 indent: only one input file when output file is specified
 indent: only one input file when stdout is used
 indent: only one output file (2nd was %s)
 old style assignment ambiguity in "=%c".  Assuming "= %c"
 option: %s
 set_option: internal error: p_type %d
 usage: indent file [-o outfile ] [ options ]
       indent file1 file2 ... fileN [ options ]
 Project-Id-Version: indent 2.2.8
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-01-31 17:27+0100
PO-Revision-Date: 2010-12-06 18:53+0000
Last-Translator: André Gondim <Unknown>
Language-Team: Brazilian Portuguese <ldp-br@bazar.conectiva.com.br>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:47+0000
X-Generator: Launchpad (build 18115)
   stack[%d] =>   stack: %d   ind_level: %d
 %s: faltando argumento para parâmetro %s
 %s: opção ``%s'' requer um parâmetro numérico
 %s: opção desconhecida "%s"
 (Linhas com comentários)/(Linhas com código): %6.3f
 IMPOSSÍVEL ENCONTRAR O ARQUIVO '@'! Impossível fechar o arquivo de saída %s Impossível abrir arquivo de backup %s Impossível abrir o arquivo de entrada %s Impossível preservar o horário de modificação do arquivo de backup %s Impossível conservar o horário de modificação do arquivo de saída %s Impossível obter informações do arquivo de entrada %s Impossível gravar no arquivo de backup %s EOF encontrado em comentário Erro Erro ao fechar entrada do arquivo %s Erro lendo o arquivo de entrada %s Arquivo %s contem caractere nulo: não pode ser processado
 O arquivo %s é grande de mais para ler Arquivo nomeado pela variável de ambiente %s não existe ou não está legível GNU indent %s
 Erro interno nos buffers Linha cortada Linha cortada 2 ParseStack [%d]:
 O Perfil contém um comentário não terminado O Perfil contém caracteres inaceitáveis Ler perfil %s
 Erro de aninhamento de instruções. Problema de sistema lendo o arquivo %s Encontradas %d linhas de saída não-vazias e %d comentários
 Final de arquivo inesperado Código desconhecido para o analisador 'else' sem o 'if' Constante de caracter sem terminador Constante de string sem terminador Aviso Tamanho zero do arquivo %s linha de comando indent:  Valor estranho do controle de versão
 indent:  Usando existentes-numeradas
 indent: %s:%d: %s: indent: Impossível criar um nome de backup de %s
 indent: Erro Fatal:  indent: Erro do Sistema:  indent: Memória virtual esgotada.
 indent: impossível criar %s
 indent: não se pode ter nomes de arquivo quando se especifica a entrada padrão
 indent: apenas um arquivo de entrada quando se especifica um arquivo de saída
 indent: apenas um arquivo de entrada quando se usa stdout
 indent: apenas um arquivo de saída (o segundo era %s)
 ambigüidade na designação estilo antigo em "=%c".  Assumindo "= %c"
 opção: %s
 set_option: erro interno: p_type %d
 utilizar: indent arquivo [-o arquivo_de_saida ] [ opções ]
       indent arquivo1 arquivo2 ... arquivoN [ opções ]
 