��          �            h  ^   i  (   �     �     �  &   �  `   $     �  :   �  #   �  4   �     "     >     J  2   `  �  �  b   O  0   �     �     �  +   �  d        �  <   �  0   �  F   �     @     ^     m  6   �           	   
                                                                  Common name for the supplied codes. This may be the same as --name (only applies to ISO 3166). Copyright © 2007-2014 Tobias Quathamer
 FILE LOCALE Name for the supplied codes (default). Official name for the supplied codes. This may be the same as --name (only applies to ISO 3166). STANDARD Separate entries with a NULL character instead of newline. Show program version and copyright. Translation to LANGUAGE Copyright © YEAR YOUR-NAME
 Use this locale for output. [ISO codes] isoquery %(version)s
 isoquery: Internal error. Please report this bug.
 Project-Id-Version: isoquery
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-05-08 10:15+0200
PO-Revision-Date: 2015-01-08 16:19+0000
Last-Translator: Fábio Nogueira <fnogueira@protonmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:32+0000
X-Generator: Launchpad (build 18115)
 Nome comum para os códigos fornecidos. Pode ser o mesmo --name (Aplica-se somente para ISO 3166). Direitos autorais © 2007-2014 Tobias Quathamer
 ARQUIVO LOCALE Nome para os códigos fornecidos (padrão). Nome oficial para os códigos fornecidos. Pode ser o mesmo --name (Aplica-se somente para ISO 3166). PADRÃO Separar entradas com caractere NULL ao invés de nova linha. Mostrar versão do programa e direitos autorais. Tradução to Português brasileiro Copyright © 2015 Fábio Nogueira
 Use este local para a saída. [Códigos ISO] isoquery %(version)s
 isoquery: Erro interno. Por favor, reporte este erro.
 