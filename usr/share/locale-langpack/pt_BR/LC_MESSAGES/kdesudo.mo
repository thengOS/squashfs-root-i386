��    (      \  5   �      p  "   q  ,   �     �     �     �     �  "        $  /   9      i  )   �     �     �     �     �  :   �  4   '     \     i  ]   �  $   �       	   '  6   1     h  *   v     �     �  2   �            )   2     \  2   w  6   �  !   �  !     	   %     /  �  A  "   
  2   $
     W
     f
     y
  	   �
  !   �
     �
  5   �
  �     4   �          #     1     B  F   J  =   �     �  j  �  f   G  )   �     �     �  :        >  3   L  %   �     �  /   �     �        @   .     o  ?   �  F   �  1     )   C     m     |                                                                      '       !   %       
   #      "                  (   	   &                                          $              (C) 2007 - 2008 Anthony Mercatante <b>Incorrect password, please try again.</b> <b>Warning: </b> Anthony Mercatante Command not found! Command: Do not display « ignore » button Do not keep password Do not show the command to be run in the dialog EMAIL OF TRANSLATORSYour emails Fake option for KDE's KdeSu compatibility Forget passwords Harald Sitter Jonathan Riddell KdeSudo Makes the dialog transient for an X app specified by winid Manual override for automatic desktop file detection Martin Böhm NAME OF TRANSLATORSYour names No command arguments supplied!
Usage: kdesudo [-u <runas>] <command>
KdeSudo will now exit... Please enter password for <b>%1</b>. Please enter your password. Priority: Process priority, between 0 and 100, 0 the lowest [50] Robert Gruber Specify icon to use in the password dialog Sudo frontend for KDE The command to execute The comment that should be displayed in the dialog Use existing DCOP server Use realtime scheduling Use target UID if <file> is not writeable Wrong password! Exiting... Your user is not allowed to run sudo on this host! Your user is not allowed to run the specified command! Your username is unknown to sudo! needs administrative privileges.  realtime: sets a runas user Project-Id-Version: kdesudo
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-05 00:10+0000
PO-Revision-Date: 2014-02-20 08:21+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:23+0000
X-Generator: Launchpad (build 18115)
 (C) 2007 - 2008 Anthony Mercatante <b>Senha incorreta, por favor tente novamente.</b> <b>Aviso: </b> Anthony Mercatante Comando não encontrado! Commando: Não mostrar botão « ignorar » Não manter a senha Não mostrar o comando para ser executado no diálogo ,,,,efraim@naos.com.br,marioancelmo@gmail.com,,tiagohillebrandt@gmail.com,wasare@gmail.com,,,,,,efraim@naos.com.br,marioancelmo@gmail.com,,tiagohillebrandt@gmail.com,wsreis@gmail.com,,raphaelsouzafreitas@gmail.com Opção falsa para a compatibilidade do KdeSu do KDE Esquecer senhas Harald Sitter Jonathan Riddell KdeSudo Faz o diálogo transiente para um aplicativo X especificado pelo winid Sobrescrever manual para detectar arquivo desktop automático Martin Böhm ,Launchpad Contributions:,Anderson R. Piram,André Gondim,Efraim Queiroz,Mario A. C. Silva (Exp4nsion),Rogênio Belém,Tiago Hillebrandt,Wanderson Santiago dos Reis,nerling, ,Launchpad Contributions:,Anderson R. Piram,André Gondim,Efraim Queiroz,Mario A. C. Silva (Exp4nsion),Rogênio Belém,Tiago Hillebrandt,Wanderson Santiago dos Reis,nerling,raphaelsfreitas Nenhum parâmetro do comando informado!
Uso: kdesudo [-u <runas>] <comando>
KdeSudo irá agora sair... Por favor informe a senha para <b>%1</b>. Por favor informe a sua senha. Prioridade: Prioridade do processo, entre 0 e 100, 0 a mais baixa [50] Robert Gruber Especifique o ícone para usar no diálogo de senha Interface gráfica do sudo para o KDE O comando para executar O comentário que deve ser mostrado no diálogo Usar o servidor DCOP existente Usar escalonamento em tempo real Representar com o UID se <file> não tiver permissão de escrita Senha incorreta! Saindo... Seu usuário não tem permissão para executar sudo neste host! Seu usuário não tem permissão para executar o comando especificado! Seu nome de usuário é desconhecido para o sudo! precisa de privilégios administrativos.  em tempo real: atribui um usuário runas 