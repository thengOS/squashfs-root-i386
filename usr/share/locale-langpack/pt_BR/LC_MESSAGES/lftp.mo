��    �       W        x%     y%     �%  	   �%     �%  #   �%  "   �%  �  &  �  
)  	  �.     �/  �   0  	   1     1     01     91  
   A1     L1  
   T1     _1     g1     }1  !   �1  <   �1     �1  '   2      +2     L2     k2  "   �2  "   �2     �2     �2     �2     3     (3  G   >3     �3  /   �3  6   �3     4  ,   4     I4  "   c4     �4  /   �4     �4      �4     5  *   *5     U5     m5      �5  %   �5     �5  #   �5  *   6     :6     W6     s6  #   �6     �6  ,   �6     �6  ,   7  ,   >7  '   k7  -   �7      �7  (   �7  (   8  !   48      V8  $   w8  ,   �8  .   �8     �8     9     89  <   P9  (   �9  #   �9  ;   �9  9   :     P:  
   R:  -   ]:  3   �:  1   �:  0   �:     ";     6;     F;      O;  *   p;     �;  *   �;     �;     �;  >   �;     <<  <   P<  +   �<  �   �<    D=  j  F>     �?  j   �?     9@     Q@     m@     �@     �@     �@  	   �@  &   �@  /   �@     "A     BA     `A     nA     ~A      �A     �A     �A     �A  ,   
B  *   7B  �   bB     	C     #C  /   9C     iC     yC  )   ~C  R   �C  =  �C     9E  @   ME  ,   �E     �E     �E     �E  
   �E     �E     F  �  !F     �G     �G     H     H  `   5H     �H     �H     �H     �H  	   �H     �H     I     )I  4   ;I  8   pI  2   �I  T  �I  @   1L     rL     �L     �L  ;   �L  o   �L  �   gM  �   2N     0O     >O      KO  ,   lO     �O     �O  !   �O     �O  #   P  6   (P     _P     mP     ~P     �P     �P  <   �P     �P     
Q  >   Q  !   XQ     zQ     �Q     �Q  
   �Q     �Q     �Q  -   �Q  <   $R     aR     |R     �R     �R     �R  "   �R  *   �R  %   S     8S     JS     YS     lS  S   �S  0   �S     T     (T     ?T     \T  ^  wT     �U     �U     V  ,   V  (   IV  D   rV  0   �V  /   �V  1   W  0   JW     {W  ;  �W  9   �X  �   Y     Z     Z     %Z     6Z  ,   IZ  6   vZ  �  �Z     J\     ^\  '   n\  "   �\  %   �\     �\     �\      ]     #]      :]  �  []     a     1a     Ka     da  �   wa     b     b  "   /b  )   Rb     |b  %   �b  $   �b     �b     �b     
c  k  c  \  �d  
   �e     �e     f     f     <f     [f  9   qf     �f     �f     �f     �f     g  !   !g     Cg     Zg     ug     �g     �g     �g     �g     �g     h    +h     3i  :   Ni  ;   �i  A   �i  3   j  �   ;j  f   k  g   vk     �k     �k  '   l     9l  Z   Fl     �l     �l     �l     �l      m     4m  '   Sm     {m  !   �m     �m      �m  :   �m  .   $n     Sn  3   fn     �n  !   �n     �n     �n     o     o  "   o    =o     [q     tq     �q  &   �q      �q     �q  �  r     �s  X  �s  )   �t     &u     Cu     ^u  d   yu     �u  	   �u     �u     v     v     8v  -   Pv  +   ~v  7   �v     �v  7   �v  #   /w     Sw  %   aw     �w     �w     �w     �w     �w     �w    x     	y     y     0y  #   Hy  8   ly  -   �y     �y  @   �y  =   )z     gz     �z     �z  #   �z     �z     �z     �z     {     5{     H{     ^{     y{     �{     �{     �{     �{     �{     |  
   %|     0|     F|     Y|     q|     }|     �|     �|     �|  1   �|     }  (   }     D}     Q}     e}     q}     �}  #   �}     �}     �}     �}      �}     ~     .~  $   M~     r~  )   �~  $   �~     �~     �~  ,         1     R     r     �     �     �     �  6   �     �     ,�     L�     a�     s�     ��     ��     ��     ɀ     ـ     �     �     !�     @�     F�     b�     �     ��     ��     ��  �    &   p�     ��     ��     Ã  %   ݃     �  �  "�  �  �  c  ��     "�  +  >�     j�     v�     ��     ��  
   ��     ��  
   ��     ��     ��     ܏  %   ��  ?   �     _�  4   k�  $   ��     Ő     �  $   �  &   &�  #   M�     q�     ��     ��      ��  N   Ց     $�  5   A�  A   w�  "   ��  3   ܒ     �  #   +�      O�  /   p�     ��  $   ��  #   �  7   �     @�      Z�  *   {�  6   ��     ݔ  )   ��  8   '�  $   `�     ��     ��  ,   ��  "   �  -   �     <�  -   Z�  -   ��  '   ��  .   ޖ      �  (   .�  ,   W�  '   ��      ��  .   ͗  6   ��  (   3�  $   \�  $   ��     ��  9   Ę  9   ��  ;   8�  J   t�  G   ��     �  
   	�  1   �  3   F�  1   z�  0   ��     ݚ     ��     �     �  )   /�     Y�  3   n�     ��     ��  W   ͛     %�  B   8�  =   {�  �   ��  n  h�  z  מ      R�  �   s�     	�  #   "�     F�     d�     }�     ��  	   ��  +   ��  6   �      �     =�     [�     i�     y�  3   ��     ̢      �     �  1   %�  -   W�  �   ��     P�  #   n�  M   ��     �  
   �  0   ��  W   -�  h  ��     �  P   �  1   T�  
   ��     ��     ��     ȧ     ק     �    	�     �  %   .�  "   T�     w�  c   ��     �     �  /   �     F�     d�  "   p�  "   ��     ��  =   ʫ  J   �  D   S�  c  ��  H   ��  "   E�     h�      }�  K   ��  �   �  �   ��    w�     ��     ��  &   ��  .   Ҳ     �     !�  )   9�  !   c�  ,   ��  6   ��     �     ��     �  )   /�     Y�  S   r�     ƴ     մ  X   �  (   @�  '   i�     ��     ��     ��     ǵ     �  -   ��  E   +�  !   q�     ��     ��     ��     ׶  <   �  4   +�  /   `�     ��     ��     ��     Ƿ  _   �  <   G�      ��     ��  &   ø  $   �  �  �     ��      ��  !   ޺  2    �  *   3�  O   ^�  8   ��  3   �  9   �  6   U�     ��    ��  B   ǽ    
�     '�     <�     K�     ^�  9   w�  C   ��  �  ��     ��     ��  '   ��  (   ��  .   �     ?�     X�  "   m�     ��  1   ��  �  ��  %   ��      ��     �     2�  �   F�     ��     �  ,   )�  +   V�     ��  4   ��  )   ��     ��     �     ,�  |  I�    ��     ��     ��     �  &   �  !   ;�     ]�  ?   x�     ��     ��     ��     ��     �  )   ;�     e�     ��  )   ��  !   ��     ��     �     #�      =�     ^�    y�     ��  O   ��  >   �  D   B�  D   ��  �   ��  p   ��  �   �     ��     ��  '   ��     �  z   �     ��  #   ��  (   ��     ��  (   �     A�  *   ^�     ��      ��     ��      ��  =   ��  '   9�     a�  B   v�     ��  !   ��  %   ��     �     .�     0�  &   F�  �  m�     =�     V�     v�  +   ��  0   ��     ��  K  �     S�  �  f�  8   �  +   K�  ,   w�  ,   ��  u   ��     G�     [�     r�     ��  "   ��     ��  :   ��  ,   �  :   :�     u�  :   ��  ,   ��     ��  0   �  !   A�     c�     �     ��     ��     ��  �   ��     ��     ��  %   �  3   3�  C   g�  @   ��     ��  K   ��  H   J�  4   ��  &   ��     ��  .    �  4   /�     d�      w�  !   ��     ��     ��  $   ��  $   �  #   8�     \�  $   n�  )   ��     ��     ��     ��     �     %�     <�     Z�  (   l�     ��     ��  "   ��  1   ��     �  +   8�     d�     t�     ��     ��     ��  +   ��     ��     �  '   .�  -   V�     ��  &   ��  <   ��      �  ,   �  %   E�  '   k�     ��  0   ��  5   ��      �     2�  !   B�  4   d�     ��     ��  =   ��     �  !   �     ;�     S�  +   i�     ��     ��  &   ��     ��     ��  .    �     /�  $   K�     p�  (   v�      ��     ��     ��     ��     	�     �  �     �  �  �   �           A  �   �   y   s  �      �  T  �           �       �       J      �   �                        .       v   U   W  �  ~   �   !       w   L  y  �   �          �           �       �   �       v  j  �   �       x               B   d  �      l       !      �  1  �  4  �   �   �   m      ?     �   �   �   �      _      D  �   �  q       �   �  }  
           �  L   :           n       �  r      �       Y   �   o    #   �             t   n  [       5  Y  &       6  -   _       �   	  [     b  R          �       r   �   )   i   x  �  �                �  ?              �  �   �          ~  �   1   �   i  z        �       s   p  q    c  �   �      �   l  �       �  ]       �       �   �       �   �     ^  �  �   �   �   �  �   �      e  �          X  `  �   g  0       �         P     �   @  �   (       c         
             �       �      �   "  �   Z  Q   �       �  3       �  w  �  �   �     "          �             �        �       �       4   �  �   k  �   O               �   �   A       d       u   <   �   �   �  �      =      E  �  �  �   #      *   I      &        ^   �   �  $             }   J   �       %  �  	   `      9     �           �   �   �  �  �   �   +   S   �  �   T   j     �       �   F  �      Q  �   >   Z       �  >      /      $   �   {   %   h       �       �  �      *                 P      �   )  �   �        �   �   \      �  m       8   �           N            a  I   �       �     <  M  9   �   -     '  �   �  b   �      �  �               M      �   /   �      V      k      �     �  �   �   2      z   |           :      �       t  �   p   2   �      E   .  h  �       +  3  �      �          K    7  o           G   �      g   �       �   D   �   �  X          U  �   �  �     =   V       @   C   5   H   �   �   �  ,   ]  0      C  K   a      e       �   �        �   �      �   �               �   �   G      ;   8      �   O      ;  �   �   �         �   6         f      W   �   �   \   |      7   �        (       S  �  �  �   f   ,      �    {  '       �              �   �       F             �           �              �  N   �      �  R   u  �   B  H   	Executing builtin `%s' [%s]
 	Repeat count: %d
 	Running
 	Waiting for command
 	Waiting for job [%d] to terminate
 	Waiting for termination of jobs:  
       queue [-n num] <command>

Add the command to queue for current site. Each site has its own command
queue. `-n' adds the command before the given item in the queue. It is
possible to queue up a running job by using command `queue wait <jobno>'.

       queue --delete|-d [index or wildcard expression]

Delete one or more items from the queue. If no argument is given, the last
entry in the queue is deleted.

       queue --move|-m <index or wildcard expression> [index]

Move the given items before the given queue index, or to the end if no
destination is given.

Options:
 -q                  Be quiet.
 -v                  Be verbose.
 -Q                  Output in a format that can be used to re-queue.
                     Useful with --delete.
 
Mirror specified remote directory to local directory

 -c, --continue         continue a mirror job if possible
 -e, --delete           delete files not present at remote site
     --delete-first     delete old files before transferring new ones
 -s, --allow-suid       set suid/sgid bits according to remote site
     --allow-chown      try to set owner and group on files
     --ignore-time      ignore time when deciding whether to download
 -n, --only-newer       download only newer files (-c won't work)
 -r, --no-recursion     don't go to subdirectories
 -p, --no-perms         don't set file permissions
     --no-umask         don't apply umask to file modes
 -R, --reverse          reverse mirror (put files)
 -L, --dereference      download symbolic links as files
 -N, --newer-than=SPEC  download only files newer than specified time
 -P, --parallel[=N]     download N files in parallel
 -i RX, --include RX    include matching files
 -x RX, --exclude RX    exclude matching files
                        RX is extended regular expression
 -v, --verbose[=N]      verbose operation
     --log=FILE         write lftp commands being executed to FILE
     --script=FILE      write lftp commands to FILE, but don't execute them
     --just-print, --dry-run    same as --script=-

When using -R, the first directory is local and the second is remote.
If the second directory is omitted, basename of first directory is used.
If both directories are omitted, current local and remote directories are used.
 
lftp now tricks the shell to move it to background process group.
lftp continues to run in the background despite the `Stopped' message.
lftp will automatically terminate when all jobs are finished.
Use `fg' shell command to return to lftp if it is still running.
  - not supported protocol  -w <file> Write history to file.
 -r <file> Read history from file; appends to current history.
 -c  Clear the history.
 -l  List the history (default).
Optional argument cnt specifies the number of history lines to list,
or "all" to list all entries.
  [cached] !<shell-command> %.0f B/s %.0fb/s %.1f KiB/s %.1fK/s %.2f MiB/s %.2fM/s %d address$|es$ found %ld $#l#byte|bytes$ cached %lld $#ll#byte|bytes$ transferred %lld $#ll#byte|bytes$ transferred in %ld $#l#second|seconds$ %s (filter) %s failed for %d of %d director$y|ies$
 %s failed for %d of %d file$|s$
 %s is a built-in alias for %s
 %s is an alias to `%s'
 %s ok, %d director$y|ies$ created
 %s ok, %d director$y|ies$ removed
 %s ok, %d file$|s$ removed
 %s ok, `%s' created
 %s ok, `%s' removed
 %s%d error$|s$ detected
 %s: %d - no such job
 %s: %s - no such cached session. Use `scache' to look at session list.
 %s: %s - not a number
 %s: %s. Use `set -a' to look at all variables.
 %s: %s: file already exists and xfer:clobber is unset
 %s: %s: no files found
 %s: -m: Number expected as second argument.  %s: -n: Number expected.  %s: -n: positive number expected.  %s: BUG - deadlock detected
 %s: GetPass() failed -- assume anonymous login
 %s: No queue is active.
 %s: Operand missed for `expire'
 %s: Operand missed for size
 %s: Please specify meta-info file or URL.
 %s: argument required.  %s: bookmark name required
 %s: cannot create local session
 %s: command `%s' is not compiled in.
 %s: date-time parse error
 %s: date-time specification missed
 %s: import type required (netscape,ncftp)
 %s: invalid block size `%s'
 %s: invalid option -- '%c'
 %s: no current job
 %s: no old directory for this site
 %s: no such bookmark `%s'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: received redirection to `%s'
 %s: regular expression `%s': %s
 %s: some other job waits for job %d
 %s: spaces in bookmark name are not allowed
 %s: summarizing conflicts with --max-depth=%i
 %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 %s: wait loop detected
 %s: warning: summarizing is the same as using --max-depth=0
 %sModified: %d file$|s$, %d symlink$|s$
 %sNew: %d file$|s$, %d symlink$|s$
 %sRemoved: %d director$y|ies$, %d file$|s$, %d symlink$|s$
 %sTotal: %d director$y|ies$, %d file$|s$, %d symlink$|s$
 ' (commands) **** FXP: giving up, reverting to plain copy
 **** FXP: trying to reverse ftp:fxp-passive-source
 **** FXP: trying to reverse ftp:fxp-passive-sscn
 **** FXP: trying to reverse ftp:ssl-protect-fxp
 , maximum size %ld
 , no size limit =1 =0|>1 Accepted connection from [%s]:%d Accepted data connection from (%s) port %u Access failed:  Account is required, set ftp:acct variable Added job$|s$ Ambiguous command `%s'.
 Ambiguous command `%s'. Use `help' to see available commands.
 Ambiguous command.  Attach the terminal to specified backgrounded lftp process.
 Cannot bind a socket for torrent:port-range Change current local directory to <ldir>. The previous local directory
is stored as `-'. You can do `lcd -' to change the directory back.
 Change current remote directory to <rdir>. The previous remote directory
is stored as `-'. You can do `cd -' to change the directory back.
The previous directory for each site is also stored on disk, so you can
do `open site; cd -' even after lftp restart.
 Change the mode of each FILE to MODE.

 -c, --changes        - like verbose but report only when a change is made
 -f, --quiet          - suppress most error messages
 -v, --verbose        - output a diagnostic for every file processed
 -R, --recursive      - change files and directories recursively

MODE can be an octal number or symbolic mode (see chmod(1))
 Changing remote directory... Close idle connections. By default only with current server.
 -a  close idle connections with all servers
 Closing HTTP connection Closing aborted data socket Closing control socket Closing data socket Closing idle connection Commands queued: Connected Connecting data socket to (%s) port %u Connecting data socket to proxy %s (%s) port %u Connecting to %s%s (%s) port %u Connecting to peer %s port %u Connecting... Connection idle Connection limit reached Could not parse HTTP status line Created a stopped queue.
 DNS resolution not trusted. Data connection established Data connection peer has mismatching address Data connection peer has wrong port number Define or undefine alias <name>. If <value> omitted,
the alias is undefined, else is takes the value <value>.
If no argument is given the current aliases are listed.
 Delaying before reconnect Delaying before retry Delete specified job with <job_no> or all jobs
 Deleted job$|s$ Done Execute commands recorded in file <file>
 Execute site command <site_cmd> and output the result
You can redirect its output
 Expand wildcards and run specified command.
Options can be used to expand wildcards to list of files, directories,
or both types. Type selection is not very reliable and depends on server.
If entry type cannot be determined, it will be included in the list.
 -f  plain files (default)
 -d  directories
 -a  all types
 FEAT negotiation... Failed to change mode of `%s' because no old mode is available.
 Failed to change mode of `%s' to %04o (%s).
 Fatal error Fetching headers... File cannot be accessed File moved File moved to ` File name missed.  Gets the specified file using several connections. This can speed up transfer,
but loads the net heavily impacting other users. Use only if you really
have to transfer the file ASAP.

Options:
 -c  continue transfer. Requires <lfile>.lftp-pget-status file.
 -n <maxconn>  set maximum number of connections (default is is taken from
     pget:default-n setting)
 -O <base> specifies base directory where files should be placed
 Getting directory contents Getting file list (%lld) [%s] Getting files information Getting meta-data: %s Group commands together to be executed as one command
You can launch such a group in background
 Handshaking... Hit EOF Hit EOF while fetching headers Host name lookup failure Interrupt Invalid IPv4 numeric address Invalid IPv6 numeric address Invalid command.  Invalid range format. Format is min-max, e.g. 10-20. Invalid time format. Format is <time><unit>, e.g. 2h30m. Invalid time unit letter, only [smhd] are allowed. LFTP is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LFTP.  If not, see <http://www.gnu.org/licenses/>.
 LFTP | Version %s | Copyright (c) 1996-%d Alexander V. Lukyanov
 Launch shell or shell command
 Libraries used:  Link <file1> to <file2>
 List cached sessions or switch to specified session number
 List remote file names.
By default, nlist output is cached, to see new listing use `renlist' or
`cache flush'.
 List remote files. You can redirect output of this command to file
or via pipe to external command.
By default, ls output is cached, to see new listing use `rels' or
`cache flush'.
See also `help cls'.
 Load module (shared object). The module should contain function
   void module_init(int argc,const char *const *argv)
If name contains a slash, then the module is searched in current
directory, otherwise in directories specified by setting module:path.
 Logging in... Login failed MLSD is disabled by ftp:use-mlsd MLST and MLSD are not supported by this site Making data connection... Making directory `%s' Making symbolic link `%s' to `%s' Mirroring directory `%s' Mode of `%s' changed to %04o (%s).
 Module for command `%s' did not register the command.
 Moved job$|s$ No address found No queued job #%i.
 No queued jobs match "%s".
 No queued jobs.
 No such command `%s'. Use `help' to see available commands.
 Not connected Now executing: Object is not cached and http:cache-control has only-if-cached Old directory `%s' is not removed Old file `%s' is not removed Operation not supported POST method failed Password:  Peer closed connection Persist and retry Print current remote URL.
 -p  show password
 Print help for command <cmd>, or list of available commands
 Proxy protocol unsupported Queue is stopped. Received all Received all (total) Received last chunk Received not enough data, retrying Received valid info about %d IPv6 peer$|s$ Received valid info about %d peer$|s$ Receiving body... Receiving data Receiving data/TLS Remove remote directories
 Remove remote files
 -r  recursive directory removal, be careful
 -f  work quietly
 Removes specified files with wildcard expansion
 Removing old directory `%s' Removing old file `%s' Removing old local file `%s' Rename <file1> to <file2>
 Repeat specified command with a delay between iterations.
Default delay is one second, default command is empty.
 -c <count>  maximum number of iterations
 -d <delay>  delay between iterations
 --while-ok  stop when command exits with non-zero code
 --until-ok  stop when command exits with zero code
 --weak      stop when lftp moves to background.
 Resolving host address... Retrying mirror...
 Running connect program SITE CHMOD is disabled by ftp:use-site-chmod SITE CHMOD is not supported by this site Same as `cat <files> | more'. if PAGER is set, it is used as filter
 Same as cat, but filter each file through bzcat
 Same as cat, but filter each file through zcat
 Same as more, but filter each file through bzcat
 Same as more, but filter each file through zcat
 Seeding in background...
 Select a server, URL or bookmark
 -e <cmd>            execute the command just after selecting
 -u <user>[,<pass>]  use the user/password for authentication
 -p <port>           use the port for connection
 -s <slot>           assign the connection to this slot
 <site>              host name, URL or bookmark name
 Send bug reports and questions to the mailing list <%s>.
 Send the command uninterpreted. Use with caution - it can lead to
unknown remote state and thus will cause reconnect. You cannot
be sure that any change of remote state because of quoted command
is solid - it can be reset by reconnect at any time.
 Sending commands... Sending data Sending data/TLS Sending request... Server reply matched ftp:retry-530, retrying Server reply matched ftp:retry-530-anonymous, retrying Set variable to given value. If the value is omitted, unset the variable.
Variable name has format ``name/closure'', where closure can specify
exact application of the setting. See lftp(1) for details.
If set is called with no variable then only altered settings are listed.
It can be changed by options:
 -a  list all settings, including default values
 -d  list only default values, not necessary current ones
 Shows lftp version
 Shutting down:  Skipping directory `%s' (only-existing) Skipping file `%s' (only-existing) Skipping symlink `%s' (only-existing) Sleep time left:  Sleeping forever Socket error (%s) - reconnecting Sorry, no help for %s
 Store failed - you have to reput Summarize disk usage.
 -a, --all             write counts for all files, not just directories
     --block-size=SIZ  use SIZ-byte blocks
 -b, --bytes           print size in bytes
 -c, --total           produce a grand total
 -d, --max-depth=N     print the total for a directory (or file, with --all)
                       only if it is N or fewer levels below the command
                       line argument;  --max-depth=0 is the same as
                       --summarize
 -F, --files           print number of files instead of sizes
 -h, --human-readable  print sizes in human readable format (e.g., 1K 234M 2G)
 -H, --si              likewise, but use powers of 1000 not 1024
 -k, --kilobytes       like --block-size=1024
 -m, --megabytes       like --block-size=1048576
 -S, --separate-dirs   do not include size of subdirectories
 -s, --summarize       display only a total for each argument
     --exclude=PAT     exclude files that match PAT
 Switching passive mode off Switching passive mode on Switching to NOREST mode TLS negotiation... There are running jobs and `cmd:move-background' is not set.
Use `exit bg' to force moving to background or `kill all' to terminate jobs.
 Timeout - reconnecting Too many redirections Total %d $file|files$ transferred
 Transfer of %d of %d $file|files$ failed
 Transferring file `%s' Try `%s --help' for more information
 Try `help %s' for more information.
 Turning on sync-mode Unknown command `%s'.
 Unknown system error Upload <lfile> with remote name <rfile>.
 -o <rfile> specifies remote file name (default - basename of lfile)
 -c  continue, reput
     it requires permission to overwrite remote files
 -E  delete local files after successful transfer (dangerous)
 -a  use ascii mode (binary is the default)
 -O <base> specifies base directory or URL where files should be placed
 Upload files with wildcard expansion
 -c  continue, reput
 -d  create directories the same as in file names and put the
     files into them instead of current directory
 -E  delete local files after successful transfer (dangerous)
 -a  use ascii mode (binary is the default)
 -O <base> specifies base directory or URL where files should be placed
 Usage: %s
 Usage: %s %s[-f] files...
 Usage: %s <cmd>
 Usage: %s <jobno> ... | all
 Usage: %s <user|URL> [<pass>]
 Usage: %s [-d #] dir
 Usage: %s [-e cmd] [-p port] [-u user[,pass]] <host|url>
 Usage: %s [-e] <file|command>
 Usage: %s [-p]
 Usage: %s [-v] [-v] ...
 Usage: %s [<exit_code>]
 Usage: %s [<jobno>]
 Usage: %s [OPTS] command args...
 Usage: %s [OPTS] file
 Usage: %s [OPTS] files...
 Usage: %s [OPTS] mode file...
 Usage: %s [options] <dirs>
 Usage: %s cmd [args...]
 Usage: %s command args...
 Usage: %s local-dir
 Usage: %s module [args...]
 Usage: cd remote-dir
 Usage: find [OPTS] [directory]
Print contents of specified directory or current directory recursively.
Directories in the list are marked with trailing slash.
You can redirect output of this command.
 -d, --maxdepth=LEVELS  Descend at most LEVELS of directories.
 Usage: mv <file1> <file2>
 Usage: reget [OPTS] <rfile> [-o <lfile>]
Same as `get -c'
 Usage: rels [<args>]
Same as `ls', but don't look in cache
 Usage: renlist [<args>]
Same as `nlist', but don't look in cache
 Usage: reput <lfile> [-o <rfile>]
Same as `put -c'
 Usage: sleep <time>[unit]
Sleep for given amount of time. The time argument can be optionally
followed by unit specifier: d - days, h - hours, m - minutes, s - seconds.
By default time is assumed to be seconds.
 Usage: slot [<label>]
List assigned slots.
If <label> is specified, switch to the slot named <label>.
 Use specified info for remote login. If you specify URL, the password
will be cached for future usage.
 Valid arguments are: Validation: %u/%u (%u%%) %s%s Verify command failed without a message Verifying... Wait for specified job to terminate. If jobno is omitted, wait
for last backgrounded job.
 Waiting for TLS shutdown... Waiting for data connection... Waiting for other copy peer... Waiting for response... Waiting for transfer to complete Warning: chdir(%s) failed: %s
 Warning: discarding incomplete command
 [%d] Done (%s) [%u] Attached to terminal %s. %s
 [%u] Attached to terminal.
 [%u] Detached from terminal. %s
 [%u] Detaching from the terminal to complete transfers...
 [%u] Exiting and detaching from the terminal.
 [%u] Finished. %s
 [%u] Moving to background to complete transfers...
 [%u] Started.  %s
 [%u] Terminated by signal %d. %s
 [re]cls [opts] [path/][pattern] [re]nlist [<args>] ` `%s' at %lld %s%s%s%s `%s', got %lld of %lld (%d%%) %s%s `lftp' is the first command executed by lftp after rc files
 -f <file>           execute commands from the file and exit
 -c <cmd>            execute the commands and exit
 --help              print this help and exit
 --version           print lftp version and exit
Other options are the same as in `open' command
 -e <cmd>            execute the command just after selecting
 -u <user>[,<pass>]  use the user/password for authentication
 -p <port>           use the port for connection
 <site>              host name, URL or bookmark name
 alias [<name> [<value>]] ambiguous argument %s for %s ambiguous variable name anon - login anonymously (by default)
 assuming failed host name lookup bookmark [SUBCMD] bookmark command controls bookmarks

The following subcommands are recognized:
  add <name> [<loc>] - add current place or given location to bookmarks
                       and bind to given name
  del <name>         - remove bookmark with the name
  edit               - start editor on bookmarks file
  import <type>      - import foreign bookmarks
  list               - list bookmarks (default)
 cache [SUBCMD] cache command controls local memory cache

The following subcommands are recognized:
  stat        - print cache status (default)
  on|off      - turn on/off caching
  flush       - flush cache
  size <lim>  - set memory limit
  expire <Nx> - set cache expiration time to N seconds (x=s)
                minutes (x=m) hours (x=h) or days (x=d)
 cannot create socket of address family %d cannot get current directory cannot parse EPSV response cannot seek on data source cat - output remote files to stdout (can be redirected)
 -b  use binary mode (ascii is the default)
 cat [-b] <files> cd <rdir> cd ok, cwd=%s
 chdir(%s) failed: %s
 chmod [OPTS] mode file... chunked format violated copy: all data received, but get rolled back
 copy: destination file is already complete
 copy: get rolled back to %lld, seeking put accordingly
 copy: put is broken
 copy: put rolled back to %lld, seeking get accordingly
 copy: received redirection to `%s'
 debug is off
 debug level is %d, output goes to %s
 depend module `%s': %s
 du [options] <dirs> eta: execl(/bin/sh) failed: %s
 execlp(%s) failed: %s
 execvp(%s) failed: %s
 exit - exit from lftp or move to background if jobs are active

If no jobs active, the code is passed to operating system as lftp
termination status. If omitted, exit code of last command is used.
`bg' forces moving to background if cmd:move-background is false.
 exit [<code>|bg] extra server response file name missed in URL file size decreased during transfer ftp over http cannot work without proxy, set hftp:proxy. ftp:fxp-force is set but FXP is not available ftp:proxy password:  ftp:skey-force is set and server does not support OPIE nor S/KEY ftp:ssl-force is set and server does not support or allow SSL get [OPTS] <rfile> [-o <lfile>] glob [OPTS] <cmd> <args> help [<cmd>] history -w file|-r file|-c|-l [cnt] host name resolve timeout integer overflow invalid argument %s for %s invalid argument for `--sort' invalid block size invalid boolean value invalid boolean/auto value invalid floating point number invalid mode string: %s
 invalid number invalid peer response format invalid server response format invalid unsigned number kill all|<job_no> lcd <ldir> lcd ok, local cwd=%s
 lftp [OPTS] <site> ln [-s] <file1> <file2> ls [<args>] max-retries exceeded memory exhausted mget [OPTS] <files> mirror [OPTS] [remote [local]] mirror: protocol `%s' is not suitable for mirror
 module name [args] modules are not supported on this system more <files> mput [OPTS] <files> mrm <files> mv <file1> <file2> next request in %s no closure defined for this setting no such %s service no such variable non-option arguments found only PUT and POST values allowed open [OPTS] <site> parse: missing filter command
 parse: missing redirection filename
 peer closed connection peer closed connection (before handshake) peer closed just accepted connection peer handshake timeout peer short handshake peer unexpectedly closed connection after %s pget [OPTS] <rfile> [-o <lfile>] pget: falling back to plain get pipe() failed:  pseudo-tty allocation failed:  put [OPTS] <lfile> [-o <rfile>] queue [OPTS] [<cmd>] quote <cmd> recls [<args>]
Same as `cls', but don't look in cache
 rename successful
 repeat [OPTS] [delay] [command] rm [-r] [-f] <files> rmdir [-f] <dirs> saw file size in response scache [<session_no>] seek failed set [OPT] [<var> [<val>]] site <site-cmd> source <file> the source file size is unknown the target file is remote this encoding is not supported total unknown address family `%s' unsupported network protocol user <user|URL> [<pass>] wait [<jobno>] zcat <files> zmore <files> Project-Id-Version: lftp 1.0
Report-Msgid-Bugs-To: lftp-bugs@lftp.yar.ru
POT-Creation-Date: 2015-06-17 17:10+0300
PO-Revision-Date: 2016-02-22 16:01+0000
Last-Translator: Arnaldo Carvalho de Melo <Unknown>
Language-Team: pt_BR <pt@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:54+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
 	Executando comando embutido `%s'[%s]
 	Contagem de repetições: %d
 	Executando
 	Aguardando pelo comando
 	Esperanto término do serviço [%d]
 	Esperando o fim das tarefas:  
       queue [-n número] <comando>

Adiciona o comando na fila para o site atual. Cada site tem seu próprio comando
de fila. `-n' adiciona o comando antes do item especificado na fala. É
possível enfileirar um processo corrente usando `queue wait <número_do_processo>'.


       queue --delete|-d [indice ou expressões curingas]

Deleta um ou mais items de uma fila. Se nenhum argumento é passado, a última entrada é deletada.

       queue --move|-m <indice ou expressões curingas> [indice]

Move os items dados para a posição da fila especificada, ou o final da filna se nenhuma posição é dada.

Opções:
 -q Calado.
 -v Seja rigido.
 -Q Mostre em uma maneira que possa ser re-enfilado.
                       Usado com --delete.
 
Espelhar diretório remoto no diretório local

 -c --continue -continua o espelhamento se possivel
 -e --delete -deleta arquivos não presentes no site remoto
     --delete-first -deleta arquivos antigos antes de transferir os novos
 -s -allow-suid seta os suid/sgid bits de acordo com o site remoto
     --allow-chown -tenta setar o dono e grupo nos arquivos
     --ignore-time -ignora o tempo enquanto decide o que baixar
 -n --only-newer -baixa apenas arquivos novos (-c não funciona)
 -r -no-recursion -não vai para os sub-diretórios
 -p -no-perms -não seta as permissões de arquivos
      --no-umask -não aplica a mascara nos modos dos arquivos
 -R -reverse -reverte espelhamento (coloca os arquivos)
 -L, --dereference -baixa links simbolicos como arquivos
 -N, --newer-than=<SPEC> -baixa apenas arquivos mais novos do que o tempo <SPEC>
 -P, --parallel[=N]  -baixa N arquivos em paralelo
 -i RX, --include-RX -inclui arquivos com RX no nome
 -x RX, --exclude-RX -exclui arquivos com RX no nome
                        RX é uma expressão regular
 -v, --verbose[=N] operação obrigatória
     --log=ARQUIVO escreve comandos lftp no ARQUIVO, mas não executa-os
     --just-print, --dry-run mesmo que --script=-

Quando usar -R,o primeiro diretório é olocal e o segundo é o remoto.
Se o segundo diretório é omitido, o nome base do primeiro é usado.
Se ambos os diretórios forem omitidos, atual local e atual remoto serão usados.
 
lftp agora engana o shell e o move para o grupo de processos sendo executados em background.
lftp continua sendo executado em segundo plano apesar da mensagem `Parado'.
lftp irá automaticamente terminar de executar quando todos os trabalhos estiverem concluídos.
Use o comando `fg' no shell para retornar ao lftp se este ainda estiver sendo executado.
  - protocolo não suportado  -w <arquivo> Escreve histórico no arquivo.
 -r <arquivo> Lê histórico do arquivo; acrescenta no histórico atual.
 -c Limpa o histórico.
 -l Lista o histórico (padrão).
O argumento opcional cnt especifica o número de linhas do histórico para listar,
ou "all" para listar todas as entradas.
  [no cache] !<shell-command> %.0f B/s %.0fb/s %.1f KiB/s %.1fK/s %.2f MiB/s %.2f/M/s %d endereço$|s$ encontrados %ld $#l#byte|bytes$ em cache %lld $#ll#byte|bytes$ transferido$|s$ %lld $#ll#byte|bytes$ transferidos em %ld $#l#segundo|segundos$ %s (filtro) %s transferência de %d de %d diretório$|s$ falhou
 %s falhou para %d de %d arquivo$|s$
 %s é um alias interno para %s
 %s é um apelido para `%s'
 %s ok, %d diretório$|s$ criado$|s$
 %s ok, %d diretório$|s$ removido$|s$
 %s ok, %d arquivo$|s$ removido$|s$
 %s ok, `%s' criado
 %s ok, `%s' removido
 %s%d erro$|s$ detectado
 %s: %d - tarefa não encontrada
 %s: %s - nenhuma sessão logada. Use 'scache' para olhar a lista de sessões.
 %s: %s - não é um número
 %s: %s. Use `set -a' para olhar todas as variáveis.
 %s: %s: arquivo já existente e xfcer:clobber não está ativado
 %s: %s: nenhum arquivo encontrado
 %s: -m: Número esperado como o segundo argumento.  %s: -n: Número esperado.  %s: -n: esperado número positivo.  %s: BUG - detectado um deadlock
 %s: GetPass() falhou - assumido login anônimo
 %s: Nenhuma fila está ativa.
 %s: Faltando operador para 'expire'
 %s: Faltando operador para tamanho
 %s: Especifique o arquivo de meta-informação ou URL.
 %s: argumento requerido.  %s: é preciso nome do marcador
 %s: não é possível criar sessão local
 %s: o suporte para o comando `%s' não foi compilado.
 %s: erro de análise data-hora
 %s: especificação de data-hora ausente
 %s: é preciso o tipo de importação (netscape, ncftp)
 %s: tamanho de bloco inválido `%s'
 %s: opção -- '%c' inválida
 %s: nenhuma tarefa ativa
 %s: nenhum diretório antigo para este site
 %s: marcador `%s' não encontrado
 %s: opção '%c%s' não permite um argumento
 %s: opção '%s' é ambígua
 %s: opção '%s' é ambígua; possibilidades: %s: opção '--%s' não permite um argumento
 %s: opção '--%s' requer um argumento
 %s: opção '-W %s' não permite um argumento
 %s: opção '-W %s' é ambígua
 %s: opção '-W %s' requer um argumento
 %s: opção necessita de argumentos -- '%c'
 %s: recebeu redirecionamento para `%s'
 %s: expressão regular `%s': %s
 %s: alguma outra tarefa espera pela tarefa %d
 %s: espaços no nome do marcador não são permitidos
 %s: resumir conflita com --max-depth=%i
 %s: opção não reconhecida '%c%s'
 %s: opção não reconhecida '--%s'
 %s: loop de espera detectado
 %s: atenção: resumir é o mesmo que usar --max-depth=0
 %sModificado: %d arquivo$|s$, %d link$|s$ simbólico$|s$
 %sNovo: %d arquivo$|s$, %d ligaç$ão|ões$ simbólica$|s$
 %sRemovido: %d diretório$|s$, %d arquivo$|s$, %d link$|s$ simbólico$|s$
 %sTotal: %d diretório$|s$, %d arquivo$|s$, %d link$|s$ simbólico$|s$
 ' (comandos) *** FXP: desistindo, mudando para cópia simples
 **** FXP: tentando inverter ftp:fxp-passive-source
 **** FXP: tentando inverter ftp:fxp-passive-sscn
 **** FXP: tentando inverter ftp:ssl-protect-fxp
 , tamanho máximo %ld
 , sem limite de tamanho =1=0|>1 Conexão aceita de [%s]:%d Conexão de dados aceita de (%s) porta %u Falha no acesso: %s  A conta é requerida, configurar ftp:acct variável Job adicionado$|s$ Comando `%s' ambíguo.
 Comando indefinido `%s'. Use `ajuda' para visualizar a lista de comandos disponíveis.
 Comando ambíguo.  Anexar o terminal ao processo lftp especificado em segundo plano.
 Não é possível vincular um soquete para torrent:port-range Muda o diretório corrente local para <diretório_local>. O diretório local
anterior é armazenado como `-'. Você pode executar `lcd -' para voltar ao
diretório anterior.
 muda o diretório remoto corrente para <diretório_remoto>. O diretório remoto
anterior fica guardado como `-'. Você pode simplesmente executar `cd -' para
voltar ao diretório anterior. O diretório anterior para cada servidor acessado
também é armazenado em disco, desta forma você pode executar `open servidor;
cd -' mesmo após ter saido e voltado ao lftp.
 Muda o modo de cada arquivo para o novo MODO.

 -c, --changes - modo verbose mas só reporta quando alterações são feitas
 -f, --quiet - suprimi a maioria das mensagens de erro
 -v, --verbose - mostra um diagnóstico para cada arquivo processado
 -R, --recursive - muda arquivos e diretórios recursivamente

MODO pode ser um número octal ou modo simbólico (veja chmod(1))
 Alterando o diretório remoto... Feche conexões sem atividade. Como padrão somente no servidor sendo acessado no
momento.
 -a  feche conexões sem atividade em todos os servidores
 Fechando a conexão HTTP Fechando o socket de dados abortado Fechando o socket de controle Fechando socket de dados Encerrando conexão ociosa Comandos enfileirados: Conectado Conectando o socket de dados a (%s) port %u Conectando o socket de dados ao proxy %s (%s) porta %u Conectando à %s%s (%s) porta %u Conectando ao par %s porta %u Conectando... Conexão ociosa Limite de conexões alcançado Não pôde fazer o parse da linha de status do HTTP Criada uma fila parada.
 Resolução DNS não confiável. Conexão de dados estabelecida A conexão de data remota tem endereço inválido A conexão remota tem número de porta errada Define ou elimine alias <nome>. Se <valor> for omitido,
o alias é eliminado, de outra forma ele assume o valor <valor>.
Se nenhum argumento for dado os aliases correntemente definidos serão listados.
 Esperando antes de reconectar Esperando antes de tentar novamente Termina serviço especificado em <número_do_serviço> ou todos os serviços
 Job deletado$|s$ Concluído Executar comandos gravados no arquivo <arquivo>
 Execute um comando remoto <comando> e mostra o resultado
Você redirecionar sua saída
 Expande coringas (wildcards) e executa o comando especificado.
Opções podem ser usadas para expandir os coringas para arquivos, diretórios,
ou para ambos. O tipo de seleção não é muito confiável e depende do servidor.
Se o tipo de entrada não pode ser determinada, vai ser incluída na lista.
 -f arquivos (padrão)
 -d diretórios
 -a todos os tipos
 Negociação FEAT... Falhou ao alterar o modo de `%s' porque o modo anterior não está disponível.
 Falhou ao alterar o modo de `%s' para %04o (%s).
 Erro fatal Buscando cabeçalhos... Arquivo não pode ser acessado Arquivo movido Arquvo movido para ` Faltando o nome do arquivo.  Obtém o arquivo especificado usando várias conexóes. Isto pode aumentar a velocidade de transferência.
mas a carga na rede pode impactar seriamente outros usuários. Use somente se você realmente
necessita trasnferir o arquivo ASAP.

Opções:
 -c continua a transferência. Requer arquivo <lfile>.lftp-pget-status .
 -n <maxconn> atribue o número máximo de conexões (o padrão é obtido da
     configuração de pget:default-n)
 -O <base> especifica o diretório base onde os arquivos devem ser colocoados
 Obtendo conteúdo do diretório Baixando lista de arquivo (%lld) [%s] Obtendo informações dos arquivos Obtendo metadados: %s Agrupe comandos para serem executados com um comando
Você pode executar um grupo em segundo plano
 Efetuando Handshake... Encontrou EOF EOF encontrado enquanto buscava por cabeçalhos Busca por nome do host falhou Interromper Endereço numérico IPv4 inválido Endereço numérico IPv6 inválido Comando inválido.  Formato de faixa inválido. O formato é min-max, ex.: 10-20. Formato de tempo inválido. O formato é <tempo><unidade>, exemplo: 2h30m. Letra da unidade de tempo inválida, somente [smhd] são permitidos. LFTP é software livre: você pode redistribui-lo e ou modificá-lo
sob os termos da GNU General Public License como publicada
pela Free Software Foundation, tanto na versão 3 da Licensa, ou
(na sua opnião) alguma versão posterior.

Este programa é distribuído na esperança de que seja útil, mas
SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
COMERCIABILIDADE ou ADEQUAÇÃO A UM DETERMINADO PROPÓSITO.
Veja a GNU General Public License para mais detalhes.

Você deve ter recebido uma cópia da GNU General Public License
juntamente com o LFTP. Se não, veja <http://www.gnu.org/licenses/>.
 LFTP | Versão %s | Direitos Autorais (c) 1996-%d Alexander V. Lukyanov
 Localize o shell ou comando shell
 Bibliotecas usadas:  Link <arquivo1> para <arquivo2>
 Liste sessões armazenadas ou mude para a sessão especificado por número
 Lista os nomes de arquivos remotos.
Por padrão, os dados gerados por nlist são armazenados no cache, para ver uma nova lista use `renlist' ou
`cache flush'.
 Lista arquivos remotos. Você pode redirecionar a saída deste comando
para um arquivo ou via pipe para um comando externo.
Como padrão a saída do ls é armazenada no cache, para ver uma nova
listagem execute `rels' ou `cache flush'.
 Carregar módulo (objeto compartilhado). O módulo deve conter a função
   void module_init(int argc,const char *const *argv)
Se o nome contem um traço, então o módulo é procurado no diretório
atual, se não nos diretórios especificados por setting module:caminho.
 Efetuando login... Falha no login MLSD é desabilitado pelo ftp:use-mlsd MLST e MLSD não são suportados por este site Realizando conexão de dados... Criando diretório `%s' Criando link simbólico de `%s' para `%s' Espelhando diretório remoto `%s' O modo de `%s' foi alterado para %04o (%s).
 O módulo para o comando `%s' não pode registrá-lo.
 Job movido$|s$ Nenhum endereço encontrado Job não enfileirado #%i.
 Nenhum job enfileirado iguala-se a "%s".
 Jobs não enfileirados.
 Nenhum comando `%s'. Use `ajuda' para visualizar a lista de comandos disponíveis.
 Não conectado Executando agora: Objeto não se encontra no cache e http:cache-control está definida como only-if-cached Diretório antigo `%s' não foi removido O arquivo antigo `%s' não foi removido Operação não suportada Método POST falhou Senha:  O cliente fechou a conexão Pesista e tente novamente Imprimir URL remota atual.
 -p mostrar senha
 Mostra ajuda para o comando <comando> ou lista comandos disponíveis
 Protocolo de proxy não suportado A lista de espera está parada. Tudo recebido Tudo recebido (total) Último bloco recebido Os dados recebidos não são suficientes, tentando novamente Recebida informação válida sobre %d par IPv6 $|s$ Recebida informação válida sobre %d par $|s$ Recebendo corpo... Recebendo dados Recebendo dados/TLS Remover os diretórios remotos
 Remove os arquivos remotos
 -r remoção recursiva de diretórios, cuidado
 -f trabalha quieto
 Remove os arquivos especificados com a expansão do curinga
 Removendo antigo diretório `%s' Removendo arquivo antigo `%s' Removendo arquivo do local antigo `%s' Renomear <arquivo1> para <arquivo2>
 Repita o comando especificado com uma pausa entre as interações. 
A pausa padrão é um segundo, o comando padrão é em branco.
 -c <contagem> número máximo de interações
 -d <pausa> pausa entre interações
 --while-ok pare quando o comando terminar com código diferente de zero
 --until-ok pare quando o comando terminar com código zero 
 --weak pare quando lftp mover-se para o fundo.
 Resolvendo endereço do host... Tentando novamento o espelho...
 Executando o programa de conexão SITE CHMOD é desabilitado pelo ftp:use-site-chmod SITE CHMOD não é suportado por este site Mesmo que `cat <files> | more'. Se PAGER está ligado, é usado como um filtro
 Mesmo que "cat", mas filtra cada arquivo usando "bzcat"
 Mesmo que cat, mas filtra cada arquivo usando zcat
 Mesmo que "more", mas filtra cada arquivo usando "bzcat"
 Mesmo que "more", mas filtra cada arquivo usando zcat
 Semeando em segundo plano...
 Selecionar servidor, URL ou favorito
 -e <cmd> executa o comando somente depois de selecionado
 -u <user> [,<pass>] use o usuário/senha para autenticação
 -p <port> use a porta para conexão
 -s <slot> atribui uma conexão a este slot
 <site> nome do host, nome da URL ou favorito
 Envie relatório de bugs e questões para a lista de e-mail <%s>.
 Envia o comando sem interpretá-lo. Use com cautela - pode levar
um estado remoto não conhecido e poderá reconectar. Você não
pode ter certeza de qualquer mudança no estado remoto por causa do comando
entre aspas é sólido - pode ser resetado apenas conectando a qualquer hora.
 Enviando comandos... Enviando dados Enviando dados/TLS Enviando requisição... Servidor responde igual ftp:retry-530, tentando novamente Servidor responde igual ftp:retry-530-anonymous, tentando novamente Seta a variável para o valor dado, se o valor é omitido, não seta a variável.
Nome da variável tem formato "nome/closure", quando closure pode ser especificado
a aplicação exata desta configuração. Veja lftp(1) para a lista de configurações
Pode ser mudado pela opção:
 -a lista todas as configurações, incluindo os valores padrões
 -d lista apenas os valores padrões, não necessariamente os atuais.
 Mostra a versão do lftp
 Desligando:  Pulando diretório `%s' (only-existing) Ignorando o arquivo `%s' (only-existing) Ignorando link simbólico `%s' (only-existing) Tempo de sono restante:  Dormindo para sempre Erro no socket (%s) - reconectando Desculpe, sem ajuda para %s
 Falha no armazenamento - você terá que reenviar Lista o uso do disco.
 -a, --all mostra o uso para todos os arquivos e não só para os diretórios
     --block-size=SIZ use SIZ-byte blocos
 -b, --bytes imprimi o tamanho em bytes
 -c, --total produz uma saída completa
 -d, --max-depth=N imprimi o tamanho de um diretório (ou arquivo se usado com --all)
                       somente se N for igual o menor que os números de níveis do diretório
                       argumento de linha;         --max-depth=0 é o mesmo que
                       --resumir
 -F, --files imprimi o número de arquivos ao invés do tamanho
 -h, --human-readable imprime o tamanho em um formato mais fácil de ler (exemplo: 1K 234M 2G)
 -H, --si como o de cima, mas use base de 1000 ao invés de 1024
 -k, --kilobytes como --block-size=1024
 -m, --megabytes como --block-size=1048576
 -S, --separate-dirs não inclui o tamanho de subdiretórios
 -s, --summarize mostra somente o total para cada argumento
     --exclude=PAT ignora os arquivos no caminho indicado por PAT
 Mudando o modo passivo para desligado Mudando modo passivo para ligado Mudando para modo NOREST Negociação TLS... Existem operações rodando e 'cmd:move-background' não está setada.
Use 'exit bg' para forçar para segundo plano ou 'kill all' para terminar todas as operações.
 Tempo esgotado - reconectando Excesso de redirecionamentos Total %d $arquivo|arquivos$ transferido$|s$
 Transferência de %d de %d arquivos falhou
 Transferindo arquivo `%s' Tente `%s --help' para conseguir mais informações
 Tente `help %s' para mais informações.
 Ativando o sync-mode Comando `%s' desconhecido.
 Erro de sistema desconhecido Upload <lfile> com nome remoto <rfile>.
 -o <rfile> especifique nome do arquivo remoto (default - basename of lfile)
 -c  continue, reput
     isto requer permissão de sobrescrita nos arquivos
 -E  deletar arquivo local depois de transferir (perigoso)
 -a  use modo ascii (é binário por padrão)
 -O <base> especifica o diretório base ou URL onde os arquivo pode ser colodado
 Busca os arquivos selecionados com coringas (wildcards) expandidos
 -c  continue, reget
 -d  crie diretórios com o mesmo nome dos arquivos e os coloque os
     os arquivos neles ao invés de no diretório corrente
 -e  remova os arquivos remotos após recepção bem-sucedida
 Uso: %s
 Uso: %s %s[-f] arquivos...
 Uso: %s <comando>
 Uso: %s <número da tarefa> ... | all
 Uso: %s <usuário|URL> [<senha>]
 Uso: %s [-d #] diretório
 Uso: %s [-e comando] [-p porta] [-u usário[,senha]] <host|ur>
 Uso: %s [-e] <arquivo|comando>
 Uso: %s [-p]
 Uso: %s [-v] [-v] ...
 Uso: %s [<código de saída>]
 Uso: %s [<número da tarefa>]
 Uso: %s [OPÇÕES] comando argumentos...
 Uso: %s [OPÇÕES] arquivo
 Uso: %s [OPÇÕES] arquivos...
 Uso: %s [OPÇÕES] modalidade arquivo...
 Uso: %s [opções] <diretórios>
 Uso: %s cmd [args...]
 Uso: %s comando argumentos...
 Uso: %s diretório-local
 Uso: %s módulo [argumentos...]
 Uso: cd diretório-remoto
 Uso: find [OPÇÕES] [diretório]
Mostra o contéudo do diretório dado ou o atual diretório recursivamente.
Diretórios na lista são marcados com uma barra.
Você pode redirecionar a saída deste comando.
 -d, --maxdepth=NIVEIS Descendente na maioria dos níveis de diretórios.
 Uso: mv <arquivo1> <arquivo2>
 Uso: reget [OPÇÕES] <arquivo_remoto> [-o <arquivo_local>]
Mesmo que `get -c'
 Uso: rels [<argumentos>]
Mesmo que `ls', mas não usa o cache
 Uso: renlist [<argumentos>]
Mesmo que `nlist', mas não usa o cache
 Uso: reput <arquivo_local> [-o <arquivo_remoto>]
Mesmo que `put -c'
 Uso: sleep <tempo>[unidade]
"Dorme" por um período de tempo dado. O argumento tempo pode ser
seguido ou não por uma unidade específica: d - dias, h - horas, s - segundos.
Por padrão o tempo é considerado como segundos.
 Uso: slot [<label>]
Lista os slots utilizados.
Se <label> for especificada, altera para o slot chamado <label>.
 Usa informações específica para autenticação remove. Se você especificar uma URL, a senha será será gravado para uso no futuro.
 Argumentos válidos são: Validação: %u/%u (%u%%) %s%s O comando verify falhou sem um mensagem Verificando... Espera por uma tarefa específica terminar. Se o número da tarefa for omitida,
espera pela última tarefa em background.
 Esperando pelo TLS desligar... Esperando pela conexão de dados... Esperando por outro cliente de cópia... Esperando por resposta... Esperando a conclusão da transferência Aviso: chdir(%s) falhou: %s
 Atenção: descartando comando incompleto
 [%d] Concluído (%s) [%u] Anexado ao terminal %s. %s
 [%u] Anexado ao terminal.
 [%u] Desanexado do terminal. %s
 [%u] Desanexar do terminal para completar transferências...
 [%u] Saindo e desanexando do terminal.
 [%u] Finalizado. %s
 [%u] Movendo para segundo plano para completar transferências...
 [%u] Iniciado.  %s
 [%u] Terminado pelo sinal %d. %s
 [re]cls [opções] [caminho/][modelo] [re]nlist [<argumentos>] ` `%s' em %lld %s%s%s%s `%s', recebeu %lld de %lld (%d%%) %s%s `lftp' é o primeiro comando executado por lftp depois dos arquivos de comandos
 -f <arquivo> executa comandos do arquivo e termina
 -c <comando> executa os comandos e termina
 --help mostra esse help e termina
Outras opções são a mesma do comando 'open'
 -e <comando> executa o comando apenas depois da seleção
 -u <usuario>[,<senha>] usa o usuário/senha para autenticação
 -p <porta> usa a porta para conexão
 <site> nome do HOST, URL ou nome favorito
 alias [<nome> [<valor>]] argumento ambíguo %s para `%s' nome da variável é ambíguo anon - conecta anonimamente (como padrão)
 assumindo que a procura pelo nome do host falhou favorito [SUBCOMANDO] O comando favorito controla os favoritos
Os seguintes subcomandos são reconhecidos:
  add <nome> [<local>] - adicione local corrente ou local informado
                         no bookmark e o associe ao nome informado
  del <nome>           - remova o bookmark com este nome
  list                 - lista os bookmarks (padrão)
 cache [SUBCOMANDO] O comando cache controla o cache local de memória

Os subcomandos seguintes são possíveis:
  stat          - mostre o status do cache (padrão)
  on|off        - liga/desliga cache
  flush         - limpa o cache
  size <limite> - configura limite de memória, -1 significa ilimitado
  expire <Nx>   - configure tempo de expiração do cache para N segundos (x=s),
                  minutos (x=m), horas (x=h) ou dias (x=d)
 não pôde criar socket para a família de endereços %d não foi possível obter o diretório atual não consegue interpretar a resposta do EPSV não é possível procurar na fonte de dados cat - mostra conteúdo de arquivos remotos (pode ser redirecionado
 -b usar modalidade binária (ascii é o padrão)
 cat [-b] <arquivos> cd <diretório_remoto> cd ok, cwd=%s
 chdir(%s) falhou: %s
 chmod [OPTS] modalidade do arquivo formato blocado violado copy: todos os dados foram recebidos, mas foram desfeitos
 copy: arquivo de destino já está completo
 copy: get desfeito para %lld, procurando pelo put correto
 copy: put está quebrado
 copy: put desfeito para %lld, procurando pelo get correto
 copy: recebido o redirecionamento para `%s'
 Depurar está desligado
 Nível de depuração é %d, saída vai para %s
 dependência do módulo `%s': %s
 du [opções] <diretórios> Tempo restante estimado: execl(/bin/sh) falhou: %s
 execlp(%s) falhou: %s
 execvp(%s) falhou: %s
 exit - sai do lftp ou o coloca em segundo plano se serviços estiverem
       ativos
Se nenhum serviço estiver ativo o código é passado para o sistema
operacional como o status de término. Se omitido o código de saida do
último comando é usado.
 sair [<código>|bg] resposta extra do servidor nome do arquivo está faltando no URL tamanho do arquivo reduziu durante a trasnferência ftp sobre http não pode trabalhar sem proxy, configure hftp:proxy. ftp:fxp-force está configurado mas o FXP não está disponível ftp:proxy senha:  ftp:skey-force está configurado e o servidor não suporta OPIE e nem S/KEY ftp:ssl-force está configurada e o servidor não suporta ou permite SSL get [OPÇÕES] <arquivo_remoto> [-o <arquivo_local>] glob [OPÇÕES] <comando> <argumentos> help [<comando>] history -w arquivo|-r arquivo|-c|-l [contagem] tempo esgotado para resolução do endereço do host estouro de inteiro argumento inválido %s para '%s' argumento inválido para `--sort' tamanho de bloco inválido valor booleano inválido valor booleano/automático inválido número de ponto flutuante inválido modalidade de string inválida: %s
 número inválido formato inválido de resposta do par formato inválido da resposta do servidor Número sem sinal inválido kill all|<número_do_serviço> lcd <diretório_local> lcd ok, local cwd=%s
 lftp [OPÇÕES] <site> ln [-s] <arquivo1> <arquivo2> ls [<argumentos>] excedido o número máximo de tentativas memória insuficiente mget [OPÇÕES] <arquivos> mirror [OPÇÕES] [remoto [local]] mirror: o protocolo `%s' não pode ser espelhado
 nome do módulo [argumentos] módulos não são suportados neste sistema more <arquivos> mput [OPÇÕES] <arquivos> mrm <arquivos> mv <arquivo1> <arquivo2> próxima requisição em %s nenhum fechamento definido para este ajuste serviço %s inexistente variável desconhecida foram encontrados argumentos non-option somente os valores PUT e POST são permitidos abrir [OPÇÕES] <site> Atenção: perdendo filtro do comando
 parse: falta informar o nome do arquivo de redirecionamento
 o par fechou a conexão o par fechou a conexão (antes do handshake) par fechado apenas aceitou a conexão tempo esgotado para handshake com o par handshake curto de par o par fechou inesperadamente a conexão após %s pget [OPÇÕES] <arquivo_remoto> [-o <arquivo_local>] pget: voltando ao modo plain get pipe() falhou:  alocação do pseudo-tty falhou:  put [OPÇÕES] <arquivo_local> [-o <arquivo_remoto>] queue [OPÇÕES] [<comando>] quote <comando> recls [<argumentos>]
Mesmo que `cls', mas não olha no cache
 sucesso ao renomear
 repetir [OPCS] [atraso] [comando] rm [-r] [-f] <arquivos> rmdir [-f] <arquivos> encontrado o tamanho do arquivo na resposta scache [<session_no>] busca falhou set [OPÇÕES] [<variável> [<valor>]] site <site-cmd> source <arquivo> o tamanho do arquivo de origem é desconhecido o arquivo destino é remoto esta codificação não é suportada total família de endereços desconhecida `%s' protocolo de rede não suportado user <usuário|URL> [<senha>] wait [<número_da_tarefa>] zcat <arquivos> zmore <arquivos> 