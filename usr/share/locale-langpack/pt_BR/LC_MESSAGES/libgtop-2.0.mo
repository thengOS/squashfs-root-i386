��    )      d  ;   �      �     �     �     �     �     �     �  	             !     :     C  	   ^     h     y     �     �     �     �     �     �  	                  ,     1     G  F   L     �     �     �  
   �     �     �     �          !     4  ,   O     |     �  �  �     �     �     �  !   �  )   �     	     	     -	     D	  	   ]	  "   g	     �	     �	  )   �	  %   �	     �	     
     
     3
     F
     c
     o
     �
     �
     �
     �
  _   �
          4     :     G     a          �     �      �     �  /        >  !   V         %           '         )               "       
                                                              !                                                     $      	   (                 #   &    Abort Alarm clock Background read from tty Background write to tty Bad argument to system call Broken pipe Bus error CPU limit exceeded Child status has changed Continue Don't fork into background EMT error Enable debugging Enable verbose output File size limit exceeded Floating-point exception Hangup I/O now possible Illegal instruction Information request Interrupt Invoked from inetd Keyboard stop Kill Profiling alarm clock Quit Run '%s --help' to see a full list of available command line options.
 Segmentation violation Stop Termination Trace trap Urgent condition on socket User defined signal 1 User defined signal 2 Virtual alarm clock Window size change read %d byte read %d bytes read %lu byte of data read %lu bytes of data read data size wrote %d byte wrote %d bytes Project-Id-Version: libgtop
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-03 23:17+0000
PO-Revision-Date: 2015-12-04 20:36+0000
Last-Translator: Vladimir Melo <vladimirmelo.psi@gmail.com>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 17:00+0000
X-Generator: Launchpad (build 18115)
 Interromper Despertador Leitura de TTY em segundo plano Escrita para TTY em segundo plano Argumento inválido na chamada de sistema Conexão interrompida Erro do barramento Limite de CPU excedido Status do filho alterado Continuar Não bifurcar para o segundo plano Erro EMT Habilitar depuração Habilitar saída com mensagens detalhadas Limite de tamenho de arquivo excedido Exceção de ponto flutuante Desligar E/S possível agora Instrução ilegal Requisição de informação Interromper Acionado pelo inetd Teclado parado Matar Personalizando despertador Sair Execute "%s --help" para ver uma lista completa das opções de linha de comando disponíveis.
 Violação de segmentação Parar Terminação Rastrear interceptações Condição urgente no soquete Sinal 1 definido pelo usuário Sinal 2 definido pelo usuário Despertador virtual Alteração do tamanho da janela %d byte lido %d bytes lidos %lu byte de dados lido %lu bytes de dados lidos tamanho dos dados lidos %d byte gravado %d bytes gravados 