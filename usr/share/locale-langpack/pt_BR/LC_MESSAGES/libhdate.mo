��    �        3        �  E   �  �   /  ,   �  W     :   q  
   �     �     �     �     �     �     �     �     �     �                 
             ,     2     C     K  	   T     ^     a     n     w     �     �     �     �     �     �     �     �     �     �     �  
   �     �     �     �     �                 %   )     O     a     s     |     �     �     �     �     �     �     �     �     �  
   �     �     �     �     �     �  
   �  
                  #     '     -     3     9     E  8   I     �     �     �  	   �     �     �     �     �     �     �     �     �     �     �     �                              %  	   ,  
   6     A     M     U     [     n     t     �     �     �     �  	   �     �     �     �  
   �     �     �     �     �     �                 	        (     ,     3     B     I     R     Y     h     q     y     �  �   �     -     1     :     F     N     U  
   ]     h     l     t     y     �  �   �  ?  �     �  
   �     �     �     �  	                       '     0  
   8     C     R  	   V     `     f     �     �     �  
   �     �     �     �     �     �            -        I     O  (   W     �     �     �     �  "   �  3   �            #        @     `     v     �  )   �     �     �     �     �  
   �     �  	   �                       !      (      .      :      M   3   Z   4   �   
   �   >   �   4   !  4   B!     w!  0   |!     �!     �!  �  �!  ^   �#  �   �#  -   �$  \   �$  J   <%  
   �%     �%     �%     �%     �%     �%     �%     �%     �%     �%     �%     �%     �%  
   �%     �%     &     &     &     &&  	   /&     9&     <&     I&     R&     ^&     g&     n&     {&     &     �&     �&     �&     �&     �&     �&     �&     �&  	   �&     �&     �&     �&     �&     '  +   '     >'     P'     b'     k'     x'     ~'     �'     �'     �'     �'     �'     �'     �'  
   �'     �'     �'     �'     �'     �'  
   �'  
   �'     (     
(     (     (     !(     '(     -(     9(  F   >(     �(     �(     �(  	   �(     �(     �(     �(     �(     �(     �(     �(     �(     )     )     )     )     )     &)     /)     5)     =)  	   D)  
   N)     Y)     e)     m)     s)     �)     �)     �)     �)     �)     �)     �)     �)     �)     �)  
   �)     �)     �)     �)     *     *     *     +*     1*  	   8*     B*     F*     N*     ]*     d*     m*     t*     �*     �*     �*     �*  �   �*     f+     j+     w+     �+     �+     �+  
   �+     �+     �+     �+     �+     �+    �+  Y  �,     5.  
   ;.     F.     O.     `.  	   h.     r.     y.     �.     �.     �.  
   �.     �.     �.     �.     �.     �.     �.     �.     /  
   /     /     0/     H/     [/      q/     �/     �/  1   �/     �/     �/  /   �/     0     "0     .0     :0  %   C0  >   i0     �0     �0  #   �0  +   �0     1     $1     11  2   Q1     �1     �1     �1     �1     �1     �1  
   �1     �1     �1     �1     �1     �1     �1     2     %2  D   12  3   v2     �2  ;   �2  B   �2  B   23     u3  P   y3     �3     �3         6   ?   �   >          =       �       �   �   	   @   �   �   �   Z   #           �       �   �               �   5   K   &   �           8               |   (   �   �           m   )   �   <   _   �   q       �   a          �          �   1       ^   �   �   n       �       �   f   �   �       �       �   �   x   X   A   z          �   W   "   �   �   F   �   �   �       4   �   �           �   P   �   T         U   ,   �   �   �       �   %   �       �               l          B      `   p       �   �   �   �   M       �   �   �   �               \       �   2              [       $               u       r   I      b          �       �   N             �   Y   s   G   �   �   C   
   .   �   �               �                  R                    �       o   '   �   -   O           �       �   t   �       �   j   �   �       �   �   0   i   v               J   D   {   �   S   �      �   �   :   y             V   /      �                     �          *      �   �       �   �   �   ]   g   7   �   �   �   3         c   �   �   ~           �      ;   �   �   9   �   H       h   !   }   �                  �   �   w   +   k   d       �       Q   E   e       L   �       �    ALERT: -m (--menu) option specified, but no menu items in config file ALERT: The information displayed is for today's Hebrew date.
              Because it is now after sunset, that means the data is
              for the Gregorian day beginning at midnight. ALERT: guessing... will use co-ordinates for ALERT: options --parasha, --shabbat, --footnote are not supported in 'three-month' mode ALERT: time zone not entered, using system local time zone Achrei Mot Achrei Mot-Kedoshim Adar Adar I Adar II Apr April Asara B'Tevet Aug August Av Balak Bamidbar Bechukotai Beha'alotcha Behar Behar-Bechukotai Beijing Bereshit Beshalach Bo Buenos Aires Chanukah Chayei Sara Cheshvan Chukat Chukat-Balak Dec December Devarim Eikev Elul Emor Erev Shavuot Erev Yom Kippur Family Day Feb February Fri Friday Gregorian date Ha'Azinu Hebrew Date Hmmm, ... hate to do this, really ... Hol hamoed Pesach Hol hamoed Sukkot Honolulu Hoshana raba Iyyar Jan January Jul July Jun June Kedoshim Ki Tavo Ki Teitzei Ki Tisa Kislev Korach L (Longitude) L (Longitue) Lag B'Omer Lech-Lecha London Los Angeles Mar March Masei Matot Matot-Masei May Memorial day for fallen whose place of burial is unknown Metzora Mexico City Miketz Mishpatim Mon Monday Moscow Nasso New York City Nisan Nitzavim Nitzavim-Vayeilech Noach Nov November Oct October Parashat Paris Pekudei Pesach Pesach II Pesach VII Pesach VIII Pinchas Purim Rabin memorial day Re'eh Rosh Hashana I Rosh Hashana II Sat Saturday Sep September Sh'lach Sh'vat Shavuot Shavuot II Shemot Shmini Shmini Atzeret Shoftim Shushan Purim Simchat Torah Sivan Sukkot Sukkot II Sun Sunday Ta'anit Esther Tammuz Tashkent Tazria Tazria-Metzora Tel-Aviv Terumah Tetzaveh Tevet This seems to be to be your first time using this version.
Please read the new documentation in the man page and config
file. Attempting to create a config file ... Thu Thursday Tish'a B'Av Tishrei Toldot Tu B'Av Tu B'Shvat Tue Tuesday Tzav Tzom Gedaliah Tzom Tammuz Usage: hcal [options] [coordinates timezone] ] [[month] year]
       coordinates: -l [NS]yy[.xxx] -L [EW]xx[.xxx]
                    -l [NS]yy[:mm[:ss]] -L [EW]xx[:mm[:ss]]
       timezone:    -z nn[( .nn | :mm )]
Try 'hcal --help' for more information Usage: hdate [options] [coordinates timezone] [[[day] month] year]
       hdate [options] [coordinates timezone] [julian_day]

       coordinates: -l [NS]yy[.yyy] -L [EW]xx[.xxx]
                    -l [NS]yy[:mm[:ss]] -L [EW]xx[:mm[:ss]]
       timezone:    -z nn[( .nn | :mm )]
Try 'hdate --help' for more information Vaera Vaetchanan Vayakhel Vayakhel-Pekudei Vayechi Vayeilech Vayera Vayeshev Vayetzei Vayigash Vayikra Vayishlach Vezot Habracha Wed Wednesday Yitro Yitzhak Rabin memorial day Yom HaAtzma'ut Yom HaShoah Yom HaZikaron Yom Kippur Yom Yerushalayim Zeev Zhabotinsky day Zhabotinsky day candle-lighting config file created day degrees enter your selection, or <return> to continue error eve of  failure attempting to create config file failure closing first_light first_stars havdalah hcal - Hebrew calendar
version 1.6 hdate - display Hebrew date information
version 1.6 holiday in the Omer is incompatible with a longitude of is non-numeric or out of bounds is not a valid option l (latitude) memory allocation failure menu selection received was out of bounds midday missing parameter month none omer count option parameter parashat sun_hour sunrise sunset talit three_stars time zone value of today is day too many arguments; after options max is dd mm yyyy too many parameters received. expected  [[mm] [yyyy] translator using co-ordinates for the equator, at the center of time zone valid latitude parameter missing for given longitude valid longitude parameter missing for given latitude year your custom menu options (from your config file) z (time zone) z (timezone) Project-Id-Version: libhdate
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2012-01-08 19:48-0500
PO-Revision-Date: 2013-09-17 14:20+0000
Last-Translator: Fábio Nogueira <fnogueira@protonmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:52+0000
X-Generator: Launchpad (build 18115)
 ALERTA: opção -m (--menu) especificada mas nenhum item de menu no arquivo de configuração. ALERTA: A informação exibida é para a data hebraica de hoje.
              Porque é agora depois do pôr do sol, que significa que o dado é
              para o dia gregoriano iniciando à meia-noite. ALERTA: supondo... irá usar coordenadas para ALERTA: opções --parasha, --shabbat, --footnote não são suportadas no modo 'three-month' ALERTA: fuso horário não digitado, usando fuso horário local do sistema Achrei Mot Achrei Mot-Kedoshim Adar Adar I Adar II abr Abril Asara B'Tevet ago Agosto Av Balak Bamidbar Bechukotai Beha'alotcha Behar Behar-Bechukotai Beijing Bereshit Beshalach Bo Buenos Aires Chanukah Chayei Sara Cheshvan Chukat Chukat-Balak dez Dezembro Devarim Eikev Elul Emor Erev Shavuot Erev Yom Kippur Dia da família fev Fevereiro Sex Sexta-feira Data Gregoriana Ha'Azinu Data hebraica Hmmm, ... detesto fazer isto, realmente ... Hol hamoed Pesach Hol hamoed Sukkot Honolulu Hoshana raba Iyyar jan Janeiro jul Julho jun Junho Kedoshim Ki Tavo Ki Teitzei Ki Tisa Kislev Korach L (Longitude) L (Longitude) Lag B'Omer Lech-Lecha Londres Los Angeles mar Março Masei Matot Matot-Masei maio Dia do memorial para mortes cujo local de sepultamento é desconhecido Metzora Cidade do México Miketz Mishpatim Seg Segunda-feira Moscou Nasso Cidade de Nova Iorque Nisan Nitzavim Nitzavim-Vayeilech Noach nov Novembro out Outubro Parashat Paris Pekudei Pesach Pesach II Pesach VII Pesach VIII Pinchas Purim Dia do memorial Rabin Re'eh Rosh Hashana I Rosh Hashana II Sáb Sábado set Setembro Sh'lach Sh'vat Shavuot Shavuot II Shemot Shmini Shmini Atzeret Shoftim Shushan Purim Simchat Torah Sivan Sukkot Sukkot II Dom Domingo Ta'anit Esther Tammuz Tashkent Tazria Tazria-Metzora Tel-Aviv Terumah Tetzaveh Tevet Parece que esta é a primeira vez que você usa esta versão.
Por favor, leia a nova documentação na página man e no arquivo
de configuração. Tentando criar um arquivo de configuração ... Qui Quinta-feira Tish'a B'Av Tishrei Toldot Tu B'Av Tu B'Shvat Ter Terça-feira Tzav Tzom Gedaliah Tzom Tammuz Uso: hcal [opções] [coordenadas fuso horário] ] [[mês] ano]
       coordenadas: -l [NS]aa[.xxx] -L [EW]xx[.xxx]
                    -l [NS]yy[:mm[:ss]] -L [EW]xx[:mm[:ss]]
       fuso horário: -z nn[( .nn | :mm )]
Tente 'hcal --help' para mais informação Uso: hdate [opções] [coordenadas de fuso horário] [[[dia] mês] ano]
       hdate [opções] [coordenadas de fuso horário] [dia juliano]

       coordenadas: -l [NS]yy[.yyy] -L [LO]xx[.xxx]
                    -l [NS]yy[:mm[:ss]] -L [LO]xx[:mm[:ss]]
       fuso horário: -z nn[( .nn | :mm )]
Tentar 'hdate --help' para maiores informações Vaera Vaetchanan Vayakhel Vayakhel-Pekudei Vayechi Vayeilech Vayera Vayeshev Vayetzei Vayigash Vayikra Vayishlach Vezot Habracha Qua Quarta-feira Yitro Dia do memorial Yitzhak Rabin Yom HaAtzma'ut Yom HaShoah Yom HaZikaron Yom Kippur Yom Yerushalayim Dia do Zeev Zhabotinsky Dia do Zhabotinsky Iluminação de velas arquivo de configuração criado dia graus informe sua seleção, ou <return> para continuar erro véspera de  falha ao tentar criar arquivo de configuração falha ao encerrar first_light first_stars havdalá hcal - Calendário Hebreu
versão 1.6 hdate - exibe informações sobre datas hebráicas
versão 1.6 feriado no Omer é incompatível com a longitude de não é numérico ou está fora dos limites não é uma opção válida l (latitude) falha na alocação de memória seleção do menu recebida estava fora dos limites meio-dia parâmetro ausente mês nenhum contagem omer opção parâmetro parashat sun_hour nascer do sol pôr do sol talit three_stars valor do fuso horário de hoje é dia argumentos em excesso; opções máximas posteriores são dd mm yyyy muitos parâmetros recebidos. esperado [[mm] [aaaa] tradutor usando coordenadas para o equador, no meio do fuso horário parâmetro de latitude válido faltando para a longitude informada parâmetro de longitude válido faltando para a latitude informada ano suas opções de menu personalizadas (a partir do seu arquivo de configuração) z (fuso horário) z (fuso horário) 