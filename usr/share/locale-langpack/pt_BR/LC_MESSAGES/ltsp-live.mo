��          �   %   �      P  &   Q     x     �     �  (   �  (   �                0  %   H     n     ~     �     �     �     �     �     �     �  &   
     1  R   8  #   �     �  K   �  �    -   �     �            *   7  (   b     �  "   �     �  *   �     �                     @     Z     l     ~     �  /   �     �  W   �  4   ?	  ,   t	  W   �	                                                           
              	                                                    Adding LTSP network to Network Manager Configuring DNSmasq Configuring LTSP Creating the guest users Enabling LTSP network in Network Manager Extracting thin client kernel and initrd Failed Installing the required packages LTSP-Live configuration LTSP-Live should now be ready to use! Network devices None Ready Restarting Network Manager Restarting openbsd-inetd Start LTSP-Live Starting DNSmasq Starting NBD server Starting OpenSSH server Starts an LTSP server from the live CD Status The selected network interface is already in use.
Are you sure you want to use it? Unable to configure Network Manager Unable to parse config Welcome to LTSP Live.
Please choose a network interface below and click OK. Project-Id-Version: ltsp
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-07-18 13:50-0400
PO-Revision-Date: 2015-07-20 15:23+0000
Last-Translator: Tiago Hillebrandt <tiagohillebrandt@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:19+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
 Adicionando rede LTSP ao Gerenciador de redes Configurando DNSmasq Configurando LTSP Criando usuários convidados Ativando rede LTSP no Gerenciador de redes Extraindo kernel e initrd do thin client Falhou Instalando os pacotes necessários Configuração do LTSP-Live LTSP-Live já deve estar pronto para usar! Dispositivos de rede Nenhum Pronto Reiniciando Gerenciador de redes Reiniciando openbsd-inetd Iniciar LTSP-Live Iniciando DNSmasq Iniciando servidor NBD Iniciando servidor OpenSSH Iniciar um servidor LTSP a partir de um live CD Estado A interface de rede selecionada já está em uso.
Você tem certeza que deseja usá-la? Não foi possível configurar o Gerenciador de redes Não foi possível analisar a configuração Bem-vindo ao live LTSP.
Por favor, escolha uma interface de rede abaixo e clique em OK. 