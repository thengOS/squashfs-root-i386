��            )         �  "   �     �  &   �          #     A     Y     f     k      ~     �     �  
   �     �  
   �     �       %        D     T     n     �  e   �     �          5  	   D  !   N  /   p     �  1  �  ,   �       ,     %   F  (   l  "   �     �     �      �  %   �     	  &   -	  
   T	     _	     o	  '   ~	     �	  )   �	     �	  $   �	     
     ,
  t   :
  $   �
  &   �
     �
       2     6   K     �                                                   
                     	                                                                - GNOME mouse accessibility daemon Button Style Button style of the click-type window. Click-type window geometry Click-type window orientation Click-type window style Double Click Drag Enable dwell click Enable simulated secondary click Failed to Display Help Hide the click-type window Horizontal Hover Click Icons only Ignore small pointer movements Orientation Orientation of the click-type window. Secondary Click Set the active dwell mode Shut down mousetweaks Single Click Size and position of the click-type window. The format is a standard X Window System geometry string. Start mousetweaks as a daemon Start mousetweaks in login mode Text and Icons Text only Time to wait before a dwell click Time to wait before a simulated secondary click Vertical Project-Id-Version: mousetweaks
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=mousetweaks&component=general
POT-Creation-Date: 2015-12-03 23:45+0000
PO-Revision-Date: 2012-02-22 23:09+0000
Last-Translator: Renato Krupa <renatokrupa@gmail.com>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:24+0000
X-Generator: Launchpad (build 18115)
X-Poedit-Country: BRAZIL
X-Poedit-Language: Portuguese
 - Daemon de acessibilidade de mouse do GNOME Estilo do botão Botão de estilo da janela de tipo de clique Geometria da janela de tipo de clique Orientação da janela de tipo de clique Estilo da janela de tipo de clique Clique duplo Arrastar Habilitar clique de permanência Habilitar clique secundário simulado Falha ao exibir a ajuda Ocultar a janela com um tipo de clique Horizontal Focar no clique Apenas ícones Ignorar pequenos movimentos do ponteiro Orientação Orientação da janela de tipo de clique. Clique secundário Definir o modo de permanência ativo Desligar o mousetweaks Clique único Tamanho e posição da janela de tipo de clique. O formato é um expressão geométrica padrão do sistema X Window. Iniciar o mousetweaks como um daemon Iniciar o mousetweaks em modo de login Texto e Ícones Apenas texto Tempo de espera antes de um clique de permanência Tempo de espera antes de simular um clique secundário Vertical 