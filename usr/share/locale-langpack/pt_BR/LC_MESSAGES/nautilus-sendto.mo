��    	      d      �       �      �   )   �   2        F  ,   T  #   �  "   �  0   �    �       @     E   \     �  8   �  $   �  1     5   L                                          	    Archive Could not parse command-line options: %s
 Expects URIs or filenames to be passed as options
 Files to send No mail client installed, not sending files
 Output version information and exit Run from build directory (ignored) Use XID as parent to the send dialogue (ignored) Project-Id-Version: nautilus-sendto
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=nautilus-sendto&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 23:48+0000
PO-Revision-Date: 2015-12-04 22:00+0000
Last-Translator: Rafael Ferreira <Unknown>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:35+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
 Pacote Não foi possível analisar as opções da linha de comando: %s
 Esperar que os URIs ou nomes de arquivo sejam passados como opções
 Arquivos a serem enviados Nenhum cliente instalado, não serão enviados arquivos
 Mostra informação da versão e sai Executar do diretório de construção (ignorado) Usar XID como pai para o diálogo de envio (ignorado) 