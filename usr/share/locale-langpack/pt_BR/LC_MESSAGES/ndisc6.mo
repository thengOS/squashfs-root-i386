��   f   0   `  �   �     �     �  �      �  �  �  �  �
  �  �  �  �     D  	   W     a  9   |     �     �  &   �          4     H     U     f     x     �     �     �     �  	         
     %     7     J     Z     h     p     �     �  $   �  (   �          !     5     K  ,   _     �     �     �  6   �  (        ;     P     p     |          �     �     �     �     �     �     
  &   '  %   N     t     �  )   �  )   �     �  �     
   �     �  "   �  �  �  �  �  u   �  T     �   p  `   '   =   �   0   �      �      !  	   !     %!     -!     ?!  
   S!  	   ^!     h!     m!     u!     �!     �!     �!     �!  =   �!     �!     	"     "     '"  ;   3"     o"  +   ~"     �"  4   �"  	   �"      #  �  #  �  �$  !  �&  ]  )  �  f-     J1  	   ]1     g1  F   �1     �1     �1     �1     
2     2     22     ?2     P2     b2  
   2     �2  (   �2     �2     �2      �2     3      3     33     D3     R3     Z3      z3  '   �3  *   �3  ;   �3  *   *4     U4  '   q4     �4  2   �4     �4     5     !5  6   @5  (   w5     �5     �5     �5     �5     �5     �5     6     6     *6     36     K6     f6     �6  ,   �6     �6     �6  "   7     &7  *   B7  �   m7     8     &8  '   28  �  Z8  �  ?:  �   <  V   �<  �   �<  j   �=  F   .>  /   u>      �>     �>  
   �>  	   �>     �>     �>  
   ?     ?     ?  	   ?     '?     E?     K?     R?     e?  G   �?     �?     �?     �?     �?  G   @     V@  5   g@     �@  F   �@  
   �@     A     [   A                              ?         C   ]   /   '   G   W           <   2               Z   `                 P   8      9      Y   d   B      %       a   V               T   R   )   H   :      >   N          b      &   U       .   S           D      K   
                  4   Q           I                         @       ^          \       +       E   ,   3   e       	       L       $   6   -      O   "   !       ;       =   #       5       F       M   X   7   0      c      J   f   (   1   _   *         A  �  �  A             ����8A            ���� 
  -1, --single   display first response and exit
  -h, --help     display this help and exit
  -m, --multiple wait and display all responses
  -n, --numeric  don't resolve host names
  -q, --quiet    only print the %s (mainly for scripts)
  -r, --retry    maximum number of attempts (default: 3)
  -V, --version  display program version and exit
  -v, --verbose  verbose display (this is the default)
  -w, --wait     how long to wait for a response [ms] (default: 1000)

 
  -4  force usage of the IPv4 protocols family
  -6  force usage of the IPv6 protocols family
  -b  specify the block bytes size (default: 1024)
  -d  wait for given delay (usec) between each block (default: 0)
  -e  perform a duplex test (TCP Echo instead of TCP Discard)
  -f  fill sent data blocks with the specified file content
  -h  display this help and exit
  -n  specify the number of blocks to send (default: 100)
  -V  display program version and exit
  -v  enable verbose output
 
  -A  send TCP ACK probes
  -d  enable socket debugging
  -E  set TCP Explicit Congestion Notification bits in TCP packets
  -f  specify the initial hop limit (default: 1)
  -g  insert a route segment within a "Type 0" routing header
  -h  display this help and exit
  -I  use ICMPv6 Echo Request packets as probes
  -i  force outgoing network interface
  -l  display incoming packets hop limit
  -m  set the maximum hop limit (default: 30)
  -N  perform reverse name lookups on the addresses of every hop
  -n  don't perform reverse name lookup on addresses
  -p  override destination port
  -q  override the number of probes per hop (default: 3)
  -r  do not route packets
  -S  send TCP SYN probes
  -s  specify the source IPv6 address of probe packets
  -t  set traffic class of probe packets
  -U  send UDP probes (default)
  -V  display program version and exit
  -w  override the timeout for response in seconds (default: 5)
  -z  specify a time to wait (in ms) between each probes (default: 0)
 
  -A  send TCP ACK probes
  -d  enable socket debugging
  -E  set TCP Explicit Congestion Notification bits in probe packets
  -f  specify the initial hop limit (default: 1)
  -g  insert a route segment within a "Type 0" routing header
  -h  display this help and exit
  -i  force outgoing network interface
  -l  set probes byte size
  -m  set the maximum hop limit (default: 30)
  -N  perform reverse name lookups on the addresses of every hop
  -n  don't perform reverse name lookup on addresses
  -p  override source TCP port
  -q  override the number of probes per hop (default: 3)
  -r  do not route packets
  -S  send TCP SYN probes (default)
  -s  specify the source IPv6 address of probe packets
  -t  set traffic class of probe packets
  -V, --version  display program version and exit
  -w  override the timeout for response in seconds (default: 5)
  -z  specify a time to wait (in ms) between each probes (default: 0)
                          %3u     infinite (0xffffffff)
   DNS server lifetime     :    DNS servers lifetime    :    Pref. time              :    Route lifetime          :    Route preference        :       %6s
   Valid time              :   %3u%% completed...  %u.%03u ms   (      0x%02x)
  (%0.3f kbytes/s)  MTU                      :   Prefix                   :   Recursive DNS server     : %s
  Source link-layer address:   built %s on %s
  from %s
  unspecified (0x00000000)
 %12u (0x%08x) %s
 %s %lu %s in %f %s %s port %s: %s
 %s%s%s%s: %s
 %s: %s
 %s: invalid hop limit
 %s: invalid packet length
 %u hop max,  %u hops max,  %zu byte packets
 %zu bytes packets
 Cannot create %s (%m) - already running? Cannot find user "%s" Cannot run "%s": %m Cannot send data: %s
 Cannot write %s: %m Child process hung up unexpectedly, aborting Child process returned an error Configured with: %s
 Connection closed by peer Copyright (C) %u-%u Pierre Ynard, Remi Denis-Courmont
 Copyright (C) %u-%u Remi Denis-Courmont
 Fatal error (%s): %m Hop limit                 :     Input error No No response. Raw IPv6 socket Reachable time            :  Receive error: %s
 Received Receiving ICMPv6 packet Retransmit time           :  Router lifetime           :  Router preference         :       %6s
 Sending %ju %s with blocksize %zu %s
 Sending ICMPv6 packet Soliciting %s (%s) on %s...
 Stateful address conf.    :          %3s
 Stateful other conf.      :          %3s
 Target link-layer address:  This is free software; see the source for copying conditions.
There is NO warranty; not even for MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.
 Timed out. Transmitted Try "%s -h" for more information.
 Usage: %s [-4|-6] [hostnames]
Converts names to addresses.

  -4, --ipv4     only lookup IPv4 addresses
  -6, --ipv6     only lookup IPv6 addresses
  -c, --config   only return addresses for locally configured protocols
  -h, --help     display this help and exit
  -m, --multiple print multiple results separated by spaces
  -n, --numeric  do not perform forward hostname lookup
  -r, --reverse  perform reverse address to hostname lookup
  -V, --version  display program version and exit
 Usage: %s [OPTIONS]
Starts the IPv6 Recursive DNS Server discovery Daemon.

  -f, --foreground  run in the foreground
  -H, --merge-hook  execute this hook whenever resolv.conf is updated
  -h, --help        display this help and exit
  -p, --pidfile     override the location of the PID file
  -r, --resolv-file set the path to the generated resolv.conf file
  -u, --user        override the user to set UID to
  -V, --version     display program version and exit
 Usage: %s [options] <IPv6 address> <interface>
Looks up an on-link IPv6 node link-layer address (Neighbor Discovery)
 Usage: %s [options] <IPv6 hostname/address> [%s]
Print IPv6 network route to a host
 Usage: %s [options] <hostname/address> [service/port number]
Use the discard TCP service at the specified host
(the default host is the local system, the default service is discard)
 Usage: %s [options] [IPv6 address] <interface>
Solicits on-link IPv6 routers (Router Discovery)
 Warning: "%s" is too small (%zu %s) to fill block of %zu %s.
 Written by Pierre Ynard and Remi Denis-Courmont
 Written by Remi Denis-Courmont
 Yes [closed]  [open]  addrinfo %s (%s)
 advertised prefixes byte bytes from %s,  high invalid link-layer address low medium medium (invalid) millisecond milliseconds ndisc6: IPv6 Neighbor/Router Discovery userland tool %s (%s)
 packet length port  port %u, from port %u,  port number rdnssd: IPv6 Recursive DNS Server discovery Daemon %s (%s)
 second seconds tcpspray6: TCP/IP bandwidth tester %s (%s)
 traceroute to %s (%s)  traceroute6: TCP & UDP IPv6 traceroute tool %s (%s)
 undefined valid Project-Id-Version: ndisc6
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2011-02-20 10:14+0200
PO-Revision-Date: 2011-09-19 23:56+0000
Last-Translator: Edvaldo de Souza Cruz <edvaldoscruz@hotmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 18:10+0000
X-Generator: Launchpad (build 18115)
 
  -1, --single   mostra apenas a primeira resposta e sai
  -h, --help     mostra essa ajuda e sai
  -m, --multiple espera e mostra todas as respostas
  -n, --numeric  não resolve nomes de host
  -q, --quiet    apenas mostra o %s (principalmente para scripts)
  -r, --retry    número máximo de tentativas (padrão: 3)
  -V, --version  mostra a versão do programa e sai
  -v, --verbose  modo verbose (esse é o padrão)
  -w, --wait     por quanto tempo espera por uma resposta [ms] (padrão: 1000)

 
  -4  força do uso da família de protocolos IPv4
  -6  força do uso da família de protocolos IPv6
  -b  especifica o tamanho em bytes do bloco (padrão: 1024)
  -d  espera o atraso especificado (usec) entre cada bloco (padrão: 0)
  -e  faz um teste duplex (Eco do TCP ao invés de Descarte do TCP)
  -f  enche os blocos enviados com o conteúdo do arquivo especificado
  -h  mostra essa ajuda e sai
  -n  especifica o número de blocos a serem enviados (padrão: 100)
  -V  mostra a versão do programa e sai
  -v  ativar a saída verbose
 
  -A enviar sondas TCP ACK
   -d tomada de ativar a depuração
   E-set TCP pedaços do Explicit Congestion Notification em pacotes TCP
   -f especifica o limite hop inicial (default: 1)
   -g inserir um segmento de rota dentro de um cabeçalho "Tipo 0" de roteamento
   -h mostrar esta ajuda e sai
   -Eu uso pacotes ICMPv6 Echo Request como sondas
   -i força de interface de rede de saída
   -l mostrar hop limite de entrada de pacotes
   -m definir o limite máximo hop (padrão: 30)
   -N realizar pesquisas de nome reversa nos endereços de cada hop
   -n não realizam pesquisa de nome inversa em endereços
   -p porta de destino substituir
   -q substituir o número de sondas por hop (padrão: 3)
   -r não rotear pacotes
   -S envia TCP SYN sondas
   -s especifica o endereço de origem de pacotes IPv6 sonda
   classe de tráfego-t conjunto de pacotes de sondagens
   -U enviar sondas UDP (padrão)
   -V versão do programa de exibição e de saída
   -w substituir o tempo limite para a resposta (padrão: 5) em segundos
   -z especificar um tempo de espera (em ms) entre cada sondas (default: 0)
 
  -A enviar sondas TCP ACK
-d tomada de ativar a depuração
-E set TCP pedaços do Notificação de Congestionamento Explícita em pacotes de sondagens
-f especifica o limite hop inicial (default: 1)
-g inserir um segmento de rota dentro de um cabeçalho "Tipo 0" de roteamento
-h mostrar esta ajuda e sai
-i força de interface de rede de saída
-l definir o tamanho do byte sondas
-m definir o limite máximo hop (padrão: 30)
-N realizar pesquisas de nome reversa nos endereços de cada hop
-n não realizam pesquisa de nome inversa em endereços
-p fonte substituir a porta TCP
-q substituir o número de sondas por hop (padrão: 3)
-r não rotear pacotes
-S envia TCP SYN sondas (padrão)
-s especifica o endereço de origem de pacotes IPv6 sonda
classe de tráfego-t conjunto de pacotes de sondagens
-V, -- mostrar a versão do programa e sair
-w substituir o tempo limite para a resposta (padrão: 5) em segundos
-z especificar um tempo de espera (em ms) entre cada sondas (default: 0)
                          %3u     infinito (0xffffffff)
   Tempo de vida do servidor DNS:    Tempo de vida dos servidores DNS:    Pref. tempo:    Vida útil rota:    Preferência de rota: %6s
   Tempo válido :   %3u%% concluído...  %u.%03u ms   (      0x%02x)
  (%0.3f kbytes/s)  MTU                      :   Prefixo:   Servidor DNS recursivo : %s
  Endereço de camada de rede de origem:   compilar %s em %s
  de %s
  não especificado (0x00000000)
 %12u (0x%08x) %s
 %s %lu %s em %f %s %s porta %s: %s
 %s%s%s%s: %s
 %s: %s
 %s: limite de saltos inválido
 %s: tamanho de pacote inválido
 %u salto máximo,  %u saltos máximos,  pacotes de %zu byte
 pacotes de %zu bytes
 Não foi possível criar %s (%m) - já está em execução? Não foi possível encontrar usuário "%s" Não pode executar "%s": %m Não foi possível enviar os dados: %s
 Não é possível gravar %s: %m Processo filho desligou inesperadamente, abortando Processo filho retornou um erro Configurado com: %s
 Conexão fechada pelo parceiro Copyright (C) %u-%u Pierre Ynard, Remi Denis-Courmont
 Copyright (C) %u-%u Remi Denis-Courmont
 Erro fatal (%s): %m Limite de saltos:     Erro de entrada Não Nenhuma resposta. Soquete IPv6 primas Tempo acessível:  Receber erro: %s
 Recebido Recebendo pacote ICMPv6 Tempo de retransmissão :  Tempo de vida do roteador:  Preferência de roteador: %6s
 Enviando %ju %s com tamanho de bloco %zu %s
 Enviando pacote ICMPv6 Solicitando %s (%s) em %s...
 Endereço de conf. de estado: %3s
 Outra conf. de estado: %3s
 Endereço de destino da camada de enlace:  Este é um software livre; veja a fonte para condições de cópia.
NÃO há garantia; nem mesmo para COMERCIALIZAÇÃO ou
ADEQUAÇÃO PARA UMA FINALIDADE ESPECÍFICA.
 Tempo esgotado. Transmitido Tente "%s -h" para mais informações.
 Uso: %s [-4|-6] [nomes das máquinas]
Converte nomes para endereços.

  -4, --ipv4 procura somente endereços IPv4
  -6, --ipv6 procura somente endereços IPv6
  -c, --config retorna somente endereços para protocolos configurados localmente
  -h, --help exibe esta ajuda e sai
  -m, --multiple imprime múltiplos resultados separados por espaços
  -n, --numeric não executa endereçamento reverso para procura de nome de máquina
  -V, --version exibe a versão do programa e sai
 Uso: %s [OPÇÕES]
Inicia o Daemon de descoberta de servidor de DNS recursivo do IPv6.

  -f, --foreground executa em primeiro plano
  -H, --merge-hook executa este gancho sempre que o resolv.conf é atualizado
  -h, --help exibe esta ajuda e sai
  -p, --pidfile sobrescreve a localização do arquivo PID
  -r, --resolv-file define o caminho do arquivo resolv.conf gerado
  -u, --user sobrescreve o usuário para definir UID
  -V, --version exibe a versão do programa e sai
 Uso:% s [opções] <endereço IPv6> <interface>
Olha uma ligação on-IPv6 endereço do nó da camada de enlace (descoberta do vizinho)
 Uso: %s [opções] <nome do host IPv6/endereço> [%s]
Mostra a rota IPv6 até um host
 Uso: %s [opções] <nomedohost/endereço> [serviço/número da porta]
Usa o serviço de descarte do TCP no host especificado
(o host padrão é o sistema local, o serviço padrão é o de descarte)
 Uso: %s [opções] [endereço IPv6] <interface>
Solicita roteadores IPv6 no link (Descoberta de roteador)
 Aviso: "%s" é muito pequeno (%zu %s) para preencher bloco de %zu %s.
 Escrito por Pierre Ynard e Remi Denis-Courmont
 Escrito por Remi Denis-Courmont
 Sim [fechado]  [aberto]  addrinfo %s (%s)
 prefixos anunciados byte bytes de %s,  alto inválido endereço da camada de enlace baixo médio médio (inválido) milissegundo milissegundos ndisc6: Ferramenta para descoberta de Vizinhos/Rotas para IPv6 %s (%s)
 comprimento do pacote porta  porta %u, da porta %u,  número da porta rdnssd: Daemon de descoberta de servidor DNS recursivo do IPv6 %s (%s)
 segundo segundos tcpspray6: Teste de largura de banda TCP/IP  %s (%s)
 traceroute para %s (%s)  traceroute6: ferramenta de rastrear rota do IPv6 de TCP & UDP %s (%s)
 indefinido válido PRIu8  Route                    : %s/%
  Rota: %s/%
 