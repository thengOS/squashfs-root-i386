��    G     T    �$      �0  P   �0  9   *1  J   d1  A   �1  H   �1  +   :2     f2     �2  $   �2  %   �2  %   �2  "   
3  $   -3  6   R3  G   �3  B   �3     4     .4     H4  :   d4  :   �4  :   �4  9   5  G   O5  A   �5  O   �5  B   )6  8   l6  P   �6  J   �6  5   A7  :   w7  @   �7  Q   �7  E   E8  G   �8  9   �8  B   9  5   P9  0   �9  F   �9  7   �9  5   6:  K   l:  ,   �:  $   �:  (   
;  $   3;  $   X;  L   };  B   �;  ?   <  D   M<  2   �<  B   �<  1   =  L   :=  K   �=     �=  @   �=      2>  *   S>  4   ~>  N   �>  !   ?  G   $?  F   l?  K   �?  A   �?  K   A@  O   �@  U   �@      3A  K   TA  &   �A  *   �A  0   �A  5   #B  +   YB  *   �B  ,   �B  @   �B  P  C  #   oD  9   �D  .   �D     �D  C   E  N   WE  +   �E     �E      �E  =   F  %   MF  7   sF  #   �F     �F     �F  ;   	G     EG  )   UG      G      �G  1   �G  ?   �G     3H  ?   PH  "   �H     �H     �H  P   �H  Q   +I  P   }I  P   �I  
   J     *J  	   =J  
   GJ     RJ     YJ     aJ     hJ     nJ     �J     �J  	   �J     �J     �J     �J     �J  
   �J     K  ,   $K  ,   QK  ;   ~K  "   �K     �K     �K  -   L  !   :L     \L     uL     �L  /   �L  -   �L  -   �L  +   -M     YM     lM  '   �M     �M     �M     �M  1   �M  7   -N     eN     }N  !   �N     �N  /   �N  /   �N  /   +O     [O  4   zO     �O     �O  *   �O  %   
P  =   0P  1   nP     �P  9   �P  -   �P  %   &Q     LQ  ,   gQ  *   �Q  +   �Q  8   �Q     $R  #   9R     ]R     {R     �R     �R     �R  "   �R     �R     �R     S     6S     TS     rS  *   �S  +   �S     �S     �S     T     7T     TT  >   mT  /   �T  K   �T      (U  7   IU  $   �U  E   �U     �U     V  B   V     ]V  $   mV     �V  5   �V     �V     W     W     =W  1   RW     �W     �W     �W     �W     �W     �W     X  +   "X     NX  &   nX     �X  :   �X  4   �X  :   Y  0   ZY  -   �Y     �Y     �Y     �Y     Z     Z  G   +Z  }   sZ     �Z     �Z     [     [     .[     <[     Q[     l[     �[     �[  	   �[  
   �[     �[  	   �[     �[  %   �[     �[  b   \     r\     �\     �\     �\     �\  Q   �\  /   B]  #   r]     �]     �]  
   �]     �]     �]  ,   �]  
   ^     ^  
   ^     ^  	   %^  	   /^  
   9^     D^  '   V^     ~^     �^     �^     �^     �^  	   �^     �^     �^     �^     �^  M   �^  C   D_  r   �_  @   �_  O   <`  N   �`  c   �`     ?a  1   Za  '   �a  '   �a  7   �a  -   b     Bb  /   Sb     �b     �b     �b  "   �b     �b  	   �b  	   �b     �b     �b      �b  ,   c  -   Kc     yc     �c     �c     �c     �c     �c     �c  "   �c      d     d     d  +   2d  ,   ^d     �d     �d  #   �d     �d     �d     e     e  O   e  N   me     �e     �e     �e     f     f     f     9f     Rf     kf     �f     �f     �f     �f     �f  (   g     7g     <g     Eg  	   Lg  	   Vg     `g     eg     tg  
   |g     �g     �g     �g  -   �g  '   �g  "   h     /h     6h  W   Bh  y   �h     i  $   )i  &   Ni  $   ui  %   �i  "   �i     �i  
   j     j     j  2   $j     Wj     oj  %   �j     �j     �j     �j  $   �j     �j     �j  7   k  >   >k  <   }k  7   �k     �k     l      l     8l  	   Ml  	   Wl     al     hl     ol     xl     �l     �l     �l     �l     �l     �l  O   �l  N   Gm  l   �m  �   n     �n  	   �n  @   �n  <   �n  #   -o  #   Qo     uo     �o     �o     �o     �o     �o     �o     �o  W   �o  @   ,p  F   mp  $   �p  W   �p  2   1q  7   dq  +   �q  D   �q  H   r  N   Vr     �r     �r  *   �r  .   �r  /   )s  -   Ys  /   �s  1   �s  S   �s  .   =t     lt  &   �t  (   �t  
   �t     �t  
   �t  	   �t     �t     u     1u  ,   Ku  &   xu  !   �u      �u     �u      �u  3   v  '   Sv  )   {v  /   �v  #   �v     �v     w     4w     Iw  0   gw     �w     �w     �w     �w  @   �w     )x     :x     Kx     ]x     ex  
   �x     �x     �x     �x     �x  <   �x  7   y  ,   My  &   zy  '   �y  #   �y     �y  %   z  "   2z     Uz  #   sz  !   �z     �z  %   �z  "   �z     {  '   ={     e{     �{     �{     �{     �{     �{     |     3|     Q|     ]|  )   k|     �|     �|     �|     �|     �|     �|  U   	}  >   _}  !   �}     �}     �}  "   �}     ~     7~     D~  ,   R~     ~     �~     �~     �~  7   �~  +     (   J  "   s      �  *   �     �     �      �     @�  *   ^�  2   ��  .   ��  '   �  )   �  )   =�     g�     y�     ��     ��     ��     ́     ��     ��     �  )   %�     O�     ^�     u�  N   }�  !   ̂  "   �     �     /�     M�     k�  &   ��     ��  �  ƃ  S   X�  >   ��  X   �  A   D�  H   ��  )   φ     ��     �  (   .�  *   W�  *   ��  '   ��  )   Շ  6   ��  E   6�  F   |�     È     ܈      ��  3   �  6   P�  8   ��  E   ��  M   �  A   T�  R   ��  =   �  4   '�  Q   \�  ,   ��  4   ۋ  ?   �  B   P�  Q   ��  3   �  K   �  <   e�  C   ��  .   �  6   �  D   L�  <   ��  =   Ύ  B   �  3   O�  %   ��  B   ��  %   �  $   �  �   7�  P   Ȑ  I   �  C   c�  3   ��  C   ۑ  2   �  N   R�  M   ��     �  @   �      N�  %   o�  &   ��  g   ��  !   $�  M   F�  J   ��  �   ߔ  M   h�  �   ��  �   K�  �   ֖  (   x�  N   ��  2   �  /   #�  8   S�  1   ��  -   ��  1   �  2   �  @   Q�  d  ��  +   ��  9   #�  .   ]�     ��  M   ��  N   ��  1   C�  -   u�  (   ��  J   ̜  /   �  =   G�  *   ��     ��      ˝  ?   �     ,�  )   <�  $   f�  $   ��  3   ��  ?   �     $�  E   A�  "   ��     ��     ��  ;   џ  F   �  I   T�  R   ��  
   �     ��  	   �  
   �  	   &�     0�     9�  	   G�     Q�     f�     ��     ��  $   ��     ��     ڡ     ��     �     �  .   2�  /   a�  B   ��  +   Ԣ  $    �  "   %�  0   H�  %   y�      ��     ��     ݣ  6   �  6   *�  1   a�  ,   ��     ��  '   Ԥ  +   ��     (�     C�     _�  <   z�  9   ��     �     
�  (   "�      K�  G   l�  C   ��     ��     �  5   3�     i�  !   ��  0   ��  1   ٧  G   �  8   S�  $   ��  e   ��  4   �  )   L�     v�  5   ��  7   ǩ  4   ��  O   4�     ��  (   ��  '   ɪ     �  $   �     0�     ?�  '   U�     }�  !   ��     ��  !   ԫ  +   ��  #   "�  ,   F�  .   s�  #   ��      Ƭ  #   �     �  !   +�  K   M�  :   ��  Y   ԭ  #   .�  I   R�  &   ��  U   î  !   �     ;�  G   P�     ��  0   ��     ݯ  9   ��     5�     Q�  )   m�     ��  ;   ��     ��  #   �     (�  &   D�     k�     ��     ��  <   ��  1   �  :   #�  *   ^�  ?   ��  <   ɲ  J   �  8   Q�  8   ��     ó     �     ��     �     :�  X   G�  �   ��     =�     D�     Q�     f�     ��     ��     ��     ȵ     Ե     �  	   �  
   ��     �  	   �     �  ,   &�     S�  d   i�     ζ     �      �     �     1�  2   A�  5   t�  ,   ��     ׷     �  
   �     ��     
�  :   !�  
   \�     g�     n�     �     ��  	   ��  
   ��     ��  :   ��  !   ��     �     #�     2�     9�     ?�     L�     Z�     h�     q�  =   ��  A   ��  v   �  A   z�  R   ��  R   �  g   b�     ʻ  2   �  ,   �  ,   E�  C   r�  6   ��     ��  8   �     F�     L�     Y�  +   `�     ��     ��     ��     ��  (   ��     ܽ  7   �  :   (�     c�  *   w�     ��     ��     ľ     ˾     Ѿ  +   �     �     �     7�  (   Q�  (   z�     ��     ��  ,   ɿ  "   ��     �     1�     6�  H   F�  R   ��     ��  %    �     &�     .�     A�  %   G�  "   m�  "   ��  #   ��  !   ��  $   ��     �  '   >�  $   f�  ,   ��     ��     ��     ��     ��  	   ��     ��     ��     ��  
   ��     �  	   "�     ,�  9   >�  (   x�  "   ��     ��     ��  [   ��  {   3�     ��  '   ��  (   ��  &   �  '   D�  C   l�  &   ��  
   ��     ��     ��  @   ��     9�     Q�  %   n�     ��     ��     ��  %   ��     ��     ��  6   ��  A   %�  <   g�  >   ��     ��     ��     �     /�     G�  	   T�     ^�     e�     l�     y�     ��  (   ��  (   ��  #   ��      �     �  R   �  R   o�  p   ��  p   3�     ��  	   ��  D   ��  =   �  %   Q�  /   w�     ��     ��     ��     ��     ��     ��  *   ��     �  T   2�  @   ��  a   ��  #   *�  W   N�  /   ��  5   ��  '   �  G   4�  �   |�  M   �     ^�     m�  (   u�  3   ��  2   ��  '   �  4   -�  7   b�  i   ��  >   �     C�  +   Y�  1   ��     ��     ��     ��     ��  $   ��  '   �     D�  -   c�  '   ��  .   ��  (   ��     �  6   0�  G   g�  E   ��  ;   ��  :   1�  )   l�  %   ��  &   ��  !   ��      �  @   &�  '   g�     ��     ��     ��  E   ��     �     #�     9�  
   R�     ]�     w�     ��     ��     ��     ��  Z   ��  V   ,�  2   ��  &   ��  +   ��  $   	�  !   .�  (   P�  #   y�      ��  '   ��  "   ��     	�  (   )�  "   R�     u�  +   ��      ��     ��  &    �  )   '�  $   Q�     v�     ��     ��     ��     ��  3   ��     �     3�     K�     R�     h�     ��  U   ��  >   ��  )   3�      ]�  0   ~�  7   ��     ��     �     �  :   2�     m�     ��     ��     ��  @   ��  3   �  4   O�  !   ��  4   ��  *   ��  "   �     )�      I�     j�  8   ��  M   ��  @   �  :   P�  <   ��  >   ��     �     �     /�     B�     a�     |�     ��     ��     ��  *   ��     �     '�     B�  L   O�  +   ��  ,   ��  '   ��  '   �  '   E�  (   m�  .   ��     ��           Q  7     ~   �         �       r                 |        H       3      �  �     �          �   D      �  [    �      �   C    �      e     �   "  o  �   (      �  '  �  6   �  J      p   �     /  �   �     ?      �  �  h     X  C       �       �   �   8           �  �   +       0      �  �  �  �  �  k  �      >   �              E  1       �   �   �   <   z  �     �             �  N   y   �       c  �  �      #     t  O       �   c   T   �   �   �    h   B      �   |       �   -          �   �   �      :   �    u     *  G   �           �   
  2  �   �      2  )            �       �  A   +  �            X                 �     0   u  _  �  �  
  n       ;  �   *   �  L       b   n  �  \  O              �  M        ?  ,   .     )  �           �  �  9  �          �  �                              �   b  E   �   G  �      "  w   $      �      ?      w  �   �   �       �   =      �  -           �      0  C  �           R   G      {       �  �   �   �  +  �     �   4  �       �     @   �      �  �       �  ~      �      �      Z  �   �   �  �  j     �  �  <  �     �   g  �  *  �   ;   �   �                       H  �   �     i   #  T  �   �  l  �  �   x       �  �          `       �   �   '   �  K  �   �  �  �  �  &  6  �   y  v       �       9   �           �    8  E  �  :          �  �  S  �                 �        t           3  �   �  �  f   F       �              U       '                      W       �   ^           @  �            m   �  5  %  
   �                  3       �     �     A  6  D  �   [   d        �   Y  �      F  �   �   �           �          �   	   Z   �   �       �  �  d             �          �   4   Q   �  �  �       g           p      �  R  �       B  �   �  �   �           �   �  U  �  �  W  �   �  �       �   I      2   �           �   .     r                �  P       �     f          �   N      =     5   "   v  $       :  �  _       �  B     �           !      �      �  �            (                    �      �   �   (     �  �  �            {   �   %              �    �         %   L  ]     D   �       F  �  �       �  k       V   $          5         .  !   �      �       >  /  ;  V  K   �   �  �         e   �  Y       �  1      �       �  �  �      �  �  �                 �  �  !  l   �      -  j           &   @  I     �   �   1  J       x  �  �   �  �   7  �             �   o   P  m  �   �  �                 }  a  �   4      /   s   �    �  �   ,      ]  �       7  >      �  s            �     �       \       8      �   S   �   �  �  q   �   <             �      �           �   a   �   `      �     i          &  	  	     z   ^  �   �    q  #   �  A      =  �   9      �   M   �   }   �      ,  )       �     �  �    �                  �       
Proto Recv-Q Send-Q Local Address           Foreign Address         State       
Proto RefCnt Flags       Type       State         I-Node 
Unless you are using bind or NIS for host lookups you can change the DNS
                               [mod] [dyn] [reinstate] [[dev] If]
                               [netmask N] [mss Mss] [window W] [irtt I]
                   [nibble NN] [trigger NN]
              compressed:%lu
           %s addr:%s            EtherTalk Phase 2 addr:%s
           IPX/Ethernet 802.2 addr:%s
           IPX/Ethernet 802.3 addr:%s
           IPX/Ethernet II addr:%s
           IPX/Ethernet SNAP addr:%s
           [ [i|o]seq ] [ [i|o]key KEY ] [ [i|o]csum ]
           [ mode { ipip | gre | sit } ] [ remote ADDR ] [ local ADDR ]
           [ ttl TTL ] [ tos TOS ] [ nopmtudisc ] [ dev PHYS_DEV ]
           collisions:%lu            econet addr:%s
           inet6 addr: %s/%d         --numeric-hosts          don't resolve host names
         --numeric-ports          don't resolve port names
         --numeric-users          don't resolve user names
         -A, -p, --protocol       specify protocol family
         -C, --cache              display routing cache instead of FIB

         -D, --use-device         read <hwaddr> from given device
         -F, --fib                display Forwarding Information Base (default)
         -M, --masquerade         display masqueraded connections

         -N, --symbolic           resolve hardware names
         -a                       display (all) hosts in alternative (BSD) style
         -a, --all, --listening   display all sockets (default: connected)
         -c, --continuous         continuous listing

         -d, --delete             delete a specified entry
         -e, --extend             display other/more information
         -f, --file               read new entries from file or from /etc/ethers

         -g, --groups             display multicast group memberships
         -i, --device             specify network interface (e.g. eth0)
         -i, --interfaces         display interface table
         -l, --listening          display listening server sockets
         -n, --numeric            don't resolve names
         -o, --timers             display timers
         -p, --programs           display PID/Program name for sockets
         -r, --route              display routing table
         -s, --set                set a new ARP entry
         -s, --statistics         display networking statistics (like SNMP)
         -v, --verbose            be verbose
        ADDR := { IP_ADDRESS | any }
        KEY  := { DOTTED_QUAD | NUMBER }
        TOS  := { NUMBER | inherit }
        TTL  := { 1..255 | inherit }
        domainname [-v] {nisdomain|-F file}   set NIS domainname (from file)
        hostname -V|--version|-h|--help       print info and exit

        hostname [-v]                         display hostname

        hostname [-v] [-d|-f|-s|-a|-i|-y|-n]  display formatted name
        inet6_route [-FC] flush      NOT supported
        inet6_route [-vF] add Target [gw Gw] [metric M] [[dev] If]
        inet_route [-FC] flush      NOT supported
        inet_route [-vF] add {-host|-net} Target[/prefix] [gw Gw] [metric M]
        inet_route [-vF] add {-host|-net} Target[/prefix] [metric M] reject
        ipmaddr -V | -version
        ipmaddr show [ dev STRING ] [ ipv4 | ipv6 | link | all ]
        iptunnel -V | --version

        netstat [-vnNcaeol] [<Socket> ...]
        netstat { [-veenNac] -i | [-cnNe] -M | -s }

        nodename [-v] {nodename|-F file}      set DECnet node name (from file)
        plipconfig -V | --version
        rarp -V                               display program version.

        rarp -d <hostname>                    delete entry from cache.
        rarp -f                               add entries from /etc/ethers.
        rarp [<HW>] -s <hostname> <hwaddr>    add entry to cache.
        route [-v] [-FC] {add|del|flush} ...  Modify routing table for AF.

        route {-V|--version}                  Display version/author and exit.

        route {-h|--help} [<AF>]              Detailed usage syntax for specified AF.
      - no statistics available -     -F, --file            read hostname or NIS domainname from given file

     -a, --alias           alias names
     -d, --domain          DNS domain name
     -f, --fqdn, --long    long host name (FQDN)
     -i, --ip-address      addresses for the hostname
     -n, --node            DECnet node name
     -s, --short           short host name
     -y, --yp, --nis       NIS/YP domainname
     dnsdomainname=hostname -d, {yp,nis,}domainname=hostname -y

    This command can read or set the hostname or the NIS domainname. You can
   also read the DNS domain or the FQDN (fully qualified domain name).
   Unless you are using bind or NIS for host lookups you can change the
   FQDN (Fully Qualified Domain Name) and the DNS domain name (which is
   part of the FQDN) in the /etc/hosts file.
   <AF>=Address family. Default: %s
   <AF>=Use '-6|-4' or '-A <af>' or '--<af>'; default: %s
   <AF>=Use '-A <af>' or '--<af>'; default: %s
   <HW>=Hardware Type.
   <HW>=Use '-H <hw>' to specify hardware address type. Default: %s
   <Socket>={-t|--tcp} {-u|--udp} {-w|--raw} {-x|--unix} --ax25 --ipx --netrom
   Checksum in received packet is required.
   Checksum output packets.
   Drop packets out of sequence.
   List of possible address families (which support routing):
   List of possible address families:
   List of possible hardware types (which support ARP):
   List of possible hardware types:
   Outfill:%d  Keepalive:%d   Sequence packets on output.
   [[-]broadcast [<address>]]  [[-]pointopoint [<address>]]
   [[-]dynamic]
   [[-]trailers]  [[-]arp]  [[-]allmulti]
   [add <address>[/<prefixlen>]]
   [del <address>[/<prefixlen>]]
   [hw <HW> <address>]  [metric <NN>]  [mtu <NN>]
   [mem_start <NN>]  [io_addr <NN>]  [irq <NN>]  [media <type>]
   [multicast]  [[-]promisc]
   [netmask <address>]  [dstaddr <address>]  [tunnel <address>]
   [outfill <NN>] [keepalive <NN>]
   [txqueuelen <NN>]
   [up|down] ...

   arp [-v]          [-i <if>] -d  <host> [pub]               <-Delete ARP entry
   arp [-v]   [<HW>] [-i <if>] -Ds <host> <if> [netmask <nm>] pub          <-''-

   arp [-v]   [<HW>] [-i <if>] -s  <host> <hwaddr> [temp]            <-Add entry
   arp [-vnD] [<HW>] [-i <if>] -f  [<filename>]            <-Add entry from file
  Bcast:%s   MTU:%d  Metric:%d  Mask:%s
  P-t-P:%s   Path
  Scope:  Timer  User  User       Inode       interface %s
  on %s  users %d %-9.9s Link encap:%s   %s	nibble %lu  trigger %lu
 %s (%s) -- no entry
 %s (%s) at  %s started %s: %s/ip  remote %s  local %s  %s: ERROR while getting interface flags: %s
 %s: ERROR while testing interface flags: %s
 %s: You can't change the DNS domain name with this command
 %s: address family not supported!
 %s: bad hardware address
 %s: can't open `%s'
 %s: error fetching interface information: %s
 %s: hardware type not supported!
 %s: illegal option mix.
 %s: invalid %s address.
 %s: name too long
 %s: you must be root to change the domain name
 %s: you must be root to change the host name
 %s: you must be root to change the node name
 %u DSACKs for out of order packets received %u DSACKs received %u DSACKs sent for old packets %u DSACKs sent for out of order packets %u ICMP messages failed %u ICMP messages received %u ICMP messages sent %u ICMP packets dropped because socket was locked %u ICMP packets dropped because they were out-of-window %u SYN cookies received %u SYN cookies sent %u SYNs to LISTEN sockets ignored %u TCP data loss events %u TCP sockets finished time wait in fast timer %u TCP sockets finished time wait in slow timer %u acknowledgments not containing data received %u active connections openings %u active connections rejected because of time stamp %u bad SACKs received %u bad segments received. %u congestion window recovered using DSACK %u congestion windows fully recovered %u congestion windows partially recovered using Hoe heuristic %u congestion windows recovered after partial ack %u connection resets received %u connections aborted after user close in linger timeout %u connections aborted due to memory pressure %u connections aborted due to timeout %u connections established %u connections reset due to early user close %u connections reset due to unexpected SYN %u connections reset due to unexpected data %u delayed acks further delayed because of locked socket %u delayed acks sent %u dropped because of missing route %u failed connection attempts %u fast retransmits %u forward retransmits %u forwarded %u fragments created %u fragments dropped after timeout %u fragments failed %u fragments received ok %u incoming packets delivered %u incoming packets discarded %u input ICMP message failed. %u invalid SYN cookies received %u of bytes directly received from backlog %u of bytes directly received from prequeue %u other TCP timeouts %u outgoing packets dropped %u packet headers predicted %u packet reassembles failed %u packet receive errors %u packets collapsed in receive queue due to low socket buffer %u packets directly queued to recvmsg prequeue. %u packets dropped from out-of-order queue because of socket buffer overrun %u packets dropped from prequeue %u packets header predicted and directly queued to user %u packets pruned from receive queue %u packets pruned from receive queue because of socket buffer overrun %u packets reassembled ok %u packets received %u packets rejects in established connections because of timestamp %u packets sent %u packets to unknown port received. %u passive connection openings %u passive connections rejected because of time stamp %u predicted acknowledgments %u reassemblies required %u reno fast retransmits failed %u requests sent out %u resets received for embryonic SYN_RECV sockets %u resets sent %u retransmits in slow start %u retransmits lost %u sack retransmits failed %u segments received %u segments retransmited %u segments send out %u time wait sockets recycled by time stamp %u timeouts after SACK recovery %u timeouts after reno fast retransmit %u timeouts in loss state %u times receiver scheduled too late for direct processing %u times recovered from packet loss due to SACK data %u times recovered from packet loss due to fast retransmit %u times the listen queue of a socket overflowed %u times unabled to send RST due to no memory %u total packets received %u with invalid addresses %u with invalid headers %u with unknown protocol (Cisco)-HDLC (No info could be read for "-p": geteuid()=%d but you should be root.)
 (Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
 (auto) (incomplete) (only servers) (servers and established) (w/o servers) 16/4 Mbps Token Ring 16/4 Mbps Token Ring (New) 6-bit Serial Line IP <from_interface> <incomplete>  ALLMULTI  AMPR AX.25 AMPR NET/ROM AMPR ROSE ARCnet AX.25 not configured in this system.
 Active AX.25 sockets
 Active IPX sockets
Proto Recv-Q Send-Q Local Address              Foreign Address            State Active Internet connections  Active NET/ROM sockets
 Active UNIX domain sockets  Active X.25 sockets
 Adaptive Serial Line IP Address                  HWtype  HWaddress           Flags Mask            Iface
 Address deletion not supported on this system.
 Address family `%s' not supported.
 Appletalk DDP Ash BROADCAST  Bad address.
 Base address:0x%x  Broadcast tunnel requires a source address.
 CCITT X.25 CLOSE CLOSE_WAIT CLOSING CONN SENT CONNECTED CONNECTING Callsign too long Cannot change line discipline to `%s'.
 Cannot create socket Compat DARPA Internet DEBUG  DGRAM DISC SENT DISCONNECTING DMA chan:%x  DYNAMIC  Default TTL is %u Dest         Source          Device  LCI  State        Vr/Vs  Send-Q  Recv-Q
 Dest       Source     Device  State        Vr/Vs    Send-Q  Recv-Q
 Destination                                 Next Hop                                Flags Metric Ref    Use Iface
 Destination               Router Net                Router Node
 Destination     Gateway         Genmask         Flags   MSS Window  irtt Iface
 Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
 Destination     Gateway         Genmask         Flags Metric Ref    Use Iface    MSS   Window irtt
 Destination  Iface    Use
 Destination  Mnemonic  Quality  Neighbour  Iface
 Detected reordering %u times using FACK Detected reordering %u times using SACK Detected reordering %u times using reno fast retransmit Detected reordering %u times using time stamp Device not found Don't know how to set addresses for family %d.
 ESTAB ESTABLISHED Econet Entries: %d	Skipped: %d	Found: %d
 Ethernet FIN_WAIT1 FIN_WAIT2 FREE Failed to get type of [%s]
 Fiber Distributed Data Interface Flushing `inet' routing table not supported
 Flushing `inet6' routing table not supported
 Forwarding is %s Frame Relay Access Device Frame Relay DLCI Generic EUI-64 Global HIPPI HWaddr %s   Hardware type `%s' not supported.
 Host ICMP input histogram: ICMP output histogram: INET (IPv4) not configured in this system.
 INET6 (IPv6) not configured in this system.
 IP masquerading entries
 IPIP Tunnel IPX not configured in this system.
 IPX: this needs to be written
 IPv4 Group Memberships
 IPv6 IPv6-in-IPv4 Iface   MTU Met   RX-OK RX-ERR RX-DRP RX-OVR    TX-OK TX-ERR TX-DRP TX-OVR Flg
 Iface   MTU Met   RX-OK RX-ERR RX-DRP RX-OVR   TX-OK TX-ERR TX-DRP TX-OVR Flg
 Interface       RefCnt Group
 Interface %s not initialized
 Interrupt:%d  Invalid callsign IrLAP Kernel AX.25 routing table
 Kernel IP routing cache
 Kernel IP routing table
 Kernel IPX routing table
 Kernel IPv6 Neighbour Cache
 Kernel IPv6 routing table
 Kernel Interface table
 Kernel NET/ROM routing table
 Kernel ROSE routing table
 Keys are not allowed with ipip and sit.
 LAPB LAST_ACK LISTEN LISTENING LOOPBACK  Link Local Loopback MASTER  MULTICAST  Malformed Ash address Media:%s Memory:%lx-%lx  Modifying `inet' routing cache not supported
 NET/ROM not configured in this system.
 NET/ROM: this needs to be written
 NOARP  NOTRAILERS  Neighbour                                   HW Address        Iface    Flags Ref State
 Neighbour                                   HW Address        Iface    Flags Ref State            Stale(sec) Delete(sec)
 No ARP entry for %s
 No routing for address family `%s'.
 No support for ECONET on this system.
 No support for INET on this system.
 No support for INET6 on this system.
 No usable address families found.
 Node address must be ten digits Novell IPX POINTOPOINT  PROMISC  Please don't supply more than one address family.
 Point-to-Point Protocol Problem reading data from %s
 Quick ack mode was activated %u times RAW RDM RECOVERY ROSE not configured in this system.
 RTO algorithm is %s RUNNING  RX bytes:%llu (%lu.%lu %s)  TX bytes:%llu (%lu.%lu %s)
 RX packets:%llu errors:%lu dropped:%lu overruns:%lu frame:%lu
 RX: Packets    Bytes        Errors CsumErrs OutOfSeq Mcasts
 Ran %u times out of system memory during packet sending Resolving `%s' ...
 Result: h_addr_list=`%s'
 Result: h_aliases=`%s'
 Result: h_name=`%s'
 SABM SENT SEQPACKET SLAVE  STREAM SYN_RECV SYN_SENT Serial Line IP Setting domainname to `%s'
 Setting hostname to `%s'
 Setting nodename to `%s'
 Site Sorry, use pppd!
 Source          Destination     Gateway         Flags   MSS Window  irtt Iface
 Source          Destination     Gateway         Flags Metric Ref    Use Iface
 Source          Destination     Gateway         Flags Metric Ref    Use Iface    MSS   Window irtt  HH  Arp
 Source          Destination     Gateway         Flags Metric Ref    Use Iface    MSS   Window irtt  TOS HHRef HHUptod     SpecDst
 TCP ran low on memory %u times TIME_WAIT TX packets:%llu errors:%lu dropped:%lu overruns:%lu carrier:%lu
 TX: Packets    Bytes        Errors DeadLoop NoRoute  NoBufs
 This kernel does not support RARP.
 Too much address family arguments.
 UNIX Domain UNK. UNKNOWN UNSPEC UP  Unknown Unknown address family `%s'.
 Unknown media type.
 Usage:
  arp [-vn]  [<HW>] [-i <if>] [-a] [<hostname>]             <-Display ARP cache
 Usage:
  ifconfig [-a] [-v] [-s] <interface> [[<AF>] <address>]
 Usage: hostname [-v] {hostname|-F file}      set hostname (from file)
 Usage: inet6_route [-vF] del Target
 Usage: inet_route [-vF] del {-host|-net} Target[/prefix] [gw Gw] [metric M] [[dev] If]
 Usage: ipmaddr [ add | del ] MULTIADDR dev STRING
 Usage: iptunnel { add | change | del | show } [ NAME ]
 Usage: plipconfig [-a] [-i] [-v] interface
 Usage: rarp -a                               list entries in cache.
 Usage: route [-nNvee] [-FC] [<AF>]           List kernel routing tables
 User       Dest       Source     Device  State        Vr/Vs    Send-Q  Recv-Q
 VJ 6-bit Serial Line IP VJ Serial Line IP WARNING: at least one error occured. (%d)
 Warning: Interface %s still in ALLMULTI mode.
 Warning: Interface %s still in BROADCAST mode.
 Warning: Interface %s still in DYNAMIC mode.
 Warning: Interface %s still in MULTICAST mode.
 Warning: Interface %s still in POINTOPOINT mode.
 Warning: Interface %s still in promisc mode... maybe other application is running?
 Warning: cannot open %s (%s). Limited output.
 Where: NAME := STRING
 Wrong format of /proc/net/dev. Sorry.
 You cannot start PPP with this program.
 [NO FLAGS] [NO FLAGS]  [NONE SET] [UNKNOWN] address mask replies: %u address mask request: %u address mask requests: %u arp: %s: hardware type without ARP support.
 arp: %s: kernel only supports 'inet'.
 arp: %s: unknown address family.
 arp: %s: unknown hardware type.
 arp: -N not yet supported.
 arp: cannot open etherfile %s !
 arp: cannot set entry on line %u of etherfile %s !
 arp: cant get HW-Address for `%s': %s.
 arp: device `%s' has HW address %s `%s'.
 arp: format error on line %u of etherfile %s !
 arp: in %d entries no match found.
 arp: invalid hardware address
 arp: need hardware address
 arp: need host name
 arp: protocol type mismatch.
 cannot determine tunnel mode (ipip, gre or sit)
 cannot open /proc/net/snmp compressed:%lu  destination unreachable: %u disabled domain name (which is part of the FQDN) in the /etc/hosts file.
 echo replies: %u echo request: %u echo requests: %u enabled error parsing /proc/net/snmp family %d  generic X.25 getdomainname()=`%s'
 gethostname()=`%s'
 getnodename()=`%s'
 hw address type `%s' has no handler to set address. failed.
 ifconfig: Cannot set address for this protocol family.
 ifconfig: `--help' gives usage information.
 ifconfig: option `%s' not recognised.
 in_arcnet(%s): invalid arcnet address!
 in_arcnet(%s): trailing : ignored!
 in_arcnet(%s): trailing junk!
 in_ether(%s): invalid ether address!
 in_ether(%s): trailing : ignored!
 in_ether(%s): trailing junk!
 in_fddi(%s): invalid fddi address!
 in_fddi(%s): trailing : ignored!
 in_fddi(%s): trailing junk!
 in_hippi(%s): invalid hippi address!
 in_hippi(%s): trailing : ignored!
 in_hippi(%s): trailing junk!
 in_tr(%s): invalid token ring address!
 in_tr(%s): trailing : ignored!
 in_tr(%s): trailing junk!
 ip: %s is invalid IPv4 address
 ip: %s is invalid inet address
 ip: %s is invalid inet prefix
 ip: argument is wrong: %s
 keepalive (%2.2f/%ld/%d) missing interface information netmask %s  netrom usage
 netstat: unsupported address family %d !
 no RARP entry for %s.
 off (0.00/%ld/%d) on %s
 on (%2.2f/%ld/%d) on%d (%2.2f/%ld/%d) problem reading data from %s
 prot   expire    initseq delta prevd source               destination          ports
 prot   expire source               destination          ports
 rarp: %s: unknown hardware type.
 rarp: %s: unknown host
 rarp: cannot open file %s:%s.
 rarp: cannot set entry from %s:%u
 rarp: format error at %s:%u
 redirect: %u redirects: %u route: %s: cannot use a NETWORK as gateway!
 route: Invalid MSS/MTU.
 route: Invalid initial rtt.
 route: Invalid window.
 route: bogus netmask %s
 route: netmask %.8x doesn't make sense with host route
 route: netmask doesn't match route address
 rresolve: unsupport address family %d !
 slattach: /dev/%s already locked!
 slattach: cannot write PID file
 slattach: setvbuf(stdout,0,_IOLBF,0) : %s
 slattach: tty name too long
 slattach: tty_hangup(DROP): %s
 slattach: tty_hangup(RAISE): %s
 slattach: tty_lock: (%s): %s
 slattach: tty_lock: UUCP user %s unknown!
 slattach: tty_open: cannot get current line disc!
 slattach: tty_open: cannot get current state!
 slattach: tty_open: cannot set %s bps!
 slattach: tty_open: cannot set 8N1 mode!
 slattach: tty_open: cannot set RAW mode!
 source quench: %u source quenches: %u time exceeded: %u timeout in transit: %u timestamp replies: %u timestamp reply: %u timestamp request: %u timestamp requests: %u timewait (%2.2f/%ld/%d) ttl != 0 and noptmudisc are incompatible
 txqueuelen:%d  unkn-%d (%2.2f/%ld/%d) unknown usage: netstat [-veenNcCF] [<Af>] -r         netstat {-V|--version|-h|--help}
 warning, got bogus igmp line %d.
 warning, got bogus igmp6 line %d.
 warning, got bogus raw line.
 warning, got bogus tcp line.
 warning, got bogus udp line.
 warning, got bogus unix line.
 warning: no inet socket available: %s
 wrong parameters: %u Project-Id-Version: net-tools 1.54
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-06-30 12:28+0900
PO-Revision-Date: 2015-12-04 23:55+0000
Last-Translator: Arnaldo Carvalho de Melo <Unknown>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:13+0000
X-Generator: Launchpad (build 18115)
 
Proto Recv-Q Send-Q Endereço Local          Endereço Remoto         Estado       
Proto CntRef Flags       Tipo       Estado        I-Node Rota 
A menos que esteja usando bind ou NIS para resolução de nomes você pode mudar o DNS
                               [mod] [dyn] [reinstate] [[dev] If]
                               [netmask N] [mss Mss] [window W] [irtt I]
                   [[família] endereço]
              compactados:%lu
           %s end.: %s            Endereço EtherTalk fase 2:%s
           Endereço IPX/Ethernet 802.2:%s
           Endereço IPX/Ethernet 802.3:%s
           Endereço IPX/Ethernet II:%s
           Endereço IPX/Ethernet SNAP:%s
           [ [i|o]seq ] [ [i|o]key KEY ] [ [i|o]csum ]
           [ mode { ipip | gre | sit } ] [ remote END ] [ local END ]
           [ ttl TTL ] [ tos TOS ] [ nopmtudisc ] [ dev DISP_FÍSICO ]
           colisões:%lu            Endereço econet:%s
           endereço inet6: %s/%d         --numeric-hosts não resolve nome de hosts
         --numeric-ports não resolve nomes das portas
         --numeric-users não resolve names de usuários
         -A, -p, --protocol       especifica a família de protocolos
         -C, --cache              mostra cache de roteamento no lugar da FIB

         -D, --use-device         leia <hwaddr> de um dispositivo
         -F, --fib             mostra a Base de Informações de Repasse (default)
         -M, --masquerade        mostra conexões mascaradas

         -N, --symbolic     resolve nome de hardware
         -a               mostra (todas as) máquinas no estilo alternativo (BSD)
         -a, --all, --listening  mostra tudo
         -c, --continuous        listagem contínua

         -d, --delete             remove a entrada especificada
         -e, --extend             mostra outras/mais informações
         -f, --file             leia novas entradas de arquivo ou de /etc/ethers

         -g, --groups    mostra os grupos multicast
         -i, --device             especifica a interface de rede (ex: eth0)
         -i, --interfaces        mostra tabela de interfaces
         -L, --netlink           mostra mensagens netlink do kernel
         -n, --numérico não resolva os nomes
         -o, --timers            mostra temporizadores
         -p, --programs     mostra PID/Nome do programa para sockets
         -r, --route             mostra tabela de roteamento
         -s, --set                define uma nova entrada ARP
         -s, --statistics mostra estatísticas de rede (como SNMP)
         -v, --verbose           listagem detalhada
        END := { ENDEREÇO_IP | any }
        KEY  := { QUATRO_NÚMEROS_SEPARADOS_POR_PONTOS | NÚMERO }
        TOS  := { NÚMERO | inherit }
        TTL  := { 1..255 | inherit }
        domainname [-v] {domínio_nis|-F file} configura nome do domínio NIS
                                             (a partir de arquivo)
        hostname -V|--version|-h|--help          mostra informações e termina

        hostname [-v]                            mostra nome da máquina

        hostname [-v] [-d|-f|-s|-a|-i|-y|-n]  mostra nome formatado
        inet6_route [-FC] flush      NÃO suportado
        inet6_route [-vF] add Destino [gw Gw] [metric M] [[dev] If]
        inet_route [-FC] flush      NÃO suportado
        inet_route [-vF] add {-host|-net} Destino[/prefixo] [gw Gw] [metric M]
        inet_route [-vF] add {-host|-net} Destino[/prefixo] [metric M] reject
        ipmaddr -V | -version
        ipmaddr show [ dev STRING ] [ ipv4 | ipv6 | link | all ]
        iptunnel -V | --version

        netstat [-vnNcaeo] [<Socket>]
        netstat [-vnNcaeo] [<Socket>]

        Uso: hostname [-v] {máquina|-F arquivo}   configura o nome do nó DECnet (a partir de arquivo)
        plipconfig -V | --version
        rarp -V                                   mostra versão do programa

        rarp -d máquina                           remove entrada do cache
        rarp -f                               adiciona entradas a partir do
                                             arquivo ethers.
        rarp [-t tipo-hw] -s máquina endereço-hw  adiciona entrada ao cache
        route [-v] [-FC] {add|del|flush} ...        Modifica tabela de rotea-
                                                   mento da família.

        route {-V|--version}                        Mostra a versão do comando
                                                   e sai.

        route {-h|--help} [família_de_endereços]    Sintaxe para a AF (Família
                                                   de endereços) espeficicada.
      - estatísticas não disponíveis -     -F, --file            leia o nome da máquina ou domínio NIS do arquivo

     -a, --alias           aliases para a máquina
     -d, --domain          nome do domínio DNS
     -f, --fqdn, --long    nome longo da máquina (FQDN)
     -i, --ip-address      endereços da máquina
     -n, --node            nome do nó DECnet
     -s, --short           nome curto da máquina
     -y, --yp, --nis       nome do domínio NIS/YP
     dnsdomainname=máquina -d, {yp,nis,}domainname=hostname -y

    Este comando pode ler ou configurar o nome da máquina ou o domínio NIS.
   Você também pode ler o domínio DNS ou o FQDN (nome de domínio completa-
   mente qualificado). A menos que você esteja usando bind ou NIS para as
   resoluções de nome é possível mudar o FQDN e o nome do domínio DNS (que 
   é parte do FQDN) no arquivo /etc/hosts.
   <AF>=Família de endereços. Default: %s
   <AF>=Use '-6|-4' ou '-A <af>' ou '--<af>'; padrão: %s
   <AF>=Use '-A <af>' ou '--<af>'; padrão: %s
   <HW>=Tipo de Hardware.
   <HW>=Use '-H <hw>' para especificar o tipo de endereço de hw. Default: %s
   <Socket>={-t|--tcp} {-u|--udp} {-w|--raw} {-x|--unix} --ax25 --ipx --netrom
   É necessário checksum nos pacotes recebidos.
   Calcule o checksum para pacotes de saída.
   Descarte pacotes fora de seqüência.
   Lista das famílias de endereços possíveis (que suportam roteamento):
   Lista de famílias de endereços possíveis:
   Lista dos tipos de hardware possíveis (que suportam ARP):
   Lista dos tipos possíveis de hardware:
   Outfill:%d  Keepalive:%d   Seqüencie pacotes na saída.
   [[-]broadcast [<endereço>]]  [[-]pointopoint [<endereço>]]
   [[-]dynamic]
   [[-]trailers]  [[-]arp]  [[-]allmulti]
   [add <endereço>[/<tam_prefixo>]]
   [del <endereço>[/<tam_prefixo>]]
   [hw <HW> <endereço>]  [metric <NN>]  [mtu <NN>]
   [mem_start <NN>]  [io_addr <NN>]  [irq <NN>]  [media <tipo>]
   [multicast]  [[-]promisc]
   [netmask <endereço>]  [dstaddr <endereço>]  [tunnel <endereço>]
   [outfill <NN>] [keepalive <NN>]
   [txqueuelen <NN>]
   [up|down] ...

   arp [-v] [-i <if>] -d <host> [pub] <-Apaga a entrada ARP
   arp [-v] [<HW>] [-i <if>] -Ds <host> <if> [netmask <nm>] pub <-''-

   arp [-v] [<HW>] [-i <if>] -s <host> <hwaddr> [temp] <-Adiciona entrada
   arp [-vnD] [<HW>] [-i <if>] -f [<arquivo>] <-Inclui entrada a partir do arquivo
  Bcast:%s   MTU:%d  Métrica:%d  Masc:%s
  P-a-P:%s   Caminho
  Escopo:  Temporizador  Usuário  Usuário Inode       %s: interface desconhecida.
  em %s  usuários %d %-9.9s  Encapsulamento do Link: %s   %s	nibble %lu  trigger %lu
 %s (%s) -- nenhuma entrada
 %s (%s) em  %s inicializado %s: %s/ip  remoto %s  local %s  %s: ERRO ao obter marcadores da interface: %s
 %s: ERRO ao testar marcadores da interface: %s
 %s: Você não pode mudar o nome do domínio DNS com este comando
 %s: família de endereços não suportada!
 %s: endereço de hardware inválido
 %s: não foi possível abrir `%s'
 %s: erro obtendo informações da interface: %s
 %s: tipo de hardware não suportado!
 %s: mistura ilegal de opções.
 %s: endereço %s inválido.
 %s: nome muito longo
 %s: você deve ser root para mudar o nome do domínio
 %s: você deve ser root para mudar o nome da máquina
 %s: você deve ser root para mudar o nome do nó
 %u DSACKS para pacotes danificados recebidos %u DSACKs recebidos %u DSACKs enviados para pacotes antigos %u DSACKs enviados para pacotes danificados %u mensagens ICMP falharam %u mensagens ICMP recebidas %u mensagens ICMP enviadas %u pacotes ICMP descartados porque o socket estava bloqueado %u pacotes ICMP descartados porque estavam fora da janela %u cookies SYN recebidos %u cookies SYN enviados %u SYNs para soquete de escuta ignorados %u eventos TCP de perda de dados %u soquetes TCP concluíram o tempo de espera mais rápido que o normal %u soquetes TCP concluíram o tempo de espera em temporizador lento %u pacotes sem dados recebidos %u conexões ativas abertas %u conexões ativas rejeitadas por causa do timestamp %u segmentos ruins recebidos %u segmentos inválidos recebidos %u janela sobrecarregada recuperada usando DSACK %u janelas sobrecarregadas totalmente recuperadas %u janelas congestionadas parcialmente restauradas usando Hoe heuristic %u janelas sobrecarregadas recuperadas após ack parcial %u reinícios de conexões recebidos %u conexões foram abortadas depois que o usuário fechou durante tempo de expiração ainda em vigor %u conexões abortados devido a pressão de memória %u conexões abortadas por tempo expirado %u conexões estabelecidas %u conexões restauradas por cancelamento do usuário %u conexões resetadas por receber um SYN não esperado %u conexões resetadas devido a dados não esperados %u confirmações adiadas foram novamente adiadas devido a um soquete bloqueado %u acks retardados enviados %u descartado por causa de falta de rota %u tentantivas de conexão que falharam %u retramissões rápidas %u retransmissões de encaminhamento %u encaminhado %u fragmentos criados %u fragmentos descartados após timeout %u pedaços falharam %u pedaços recebidos com sucesso %u pacotes de entrada entregues %u pacotes de entrada descartados %u mensagens ICMP de entrada com problemas. %u cookies SYN inválidos recebidos %u de bytes recebidos diretamente do backlog %u de bytes recebidos diretamento da pré-fila %u outras expirações de tempo TCP %u pacotes de saída descartados %u cabeçalhos de pacotes previstos %u pacotes remontados com falha %u erros na recepção de pacotes %u pacotes caíram em fila de recepção devido a um buffer baixo do socket %u pacotes diretamente enfileirados em recebmsg pré-fila. %u pacotes cortados da fila de danificados por causa de sobrecarga na memória do soquete %u pacotes descartados do pré-fila %u cabeçalhos de pacote previstos e diretamente enfileirados ao usuário %u pacotes cortados da fila de entrada %u pacotes cortados da fila de entrada por causa de sobrecarga na memória do soquete %u pacotes remontados com sucesso %u pacotes recebidos %u pacotes rejeitados em conexões estabelecidas por causa do timestamp %u pacotes enviados %u pacotes recebidos para uma porta desconhecida %u conexões passivas abertas %u conexões passivas rejeitadas devido à marca de tempo %u reconhecimentos preditos %u remontagens necessárias %u retransmissões rápidas Reno falharam %u requisições enviadas %u reinícios recebidos para soquetes SYN_RECV embriônicos %u reinícios enviados %u retransmissões em início lento %u retransmições perdidas %u retransmissões de pacotes falharam %u segmentos recebidos %u segmentos retransmitidos %u segmentos enviados %u soquetes de tempo de espera reciclados por timbre de hora %u expirações de tempo após recuperação SACK %u expirações de tempo após retransmissão rápida Reno %u expirações de tempo em estado perdido receptor programado atrazado %u vezes para processamento direto %u vezes recuperados devido a perda de pacotes de dados SACK recuperado %u vezes de pacotes perdidos devido a retransmissões rápidas. fila de escuta de um soquete foi sobrecarregada %u vezes %u vezes foi incapaz de enviar RST por falta de memória %u total de pacotes recebidos %u com endereços inválidos %u com cabeçalhos inválidos %u com protocolo desconhecido (Cisco)-HDLC (Não foi possível ler informações para "-p": geteuid()=%d mas você deve ser root.)
 (Nem todos os processos puderam ser identificados, informações sobre processos
 de outrem não serão mostrados, você deve ser root para vê-los todos.)
 (auto) (incompleto) (somente servidores) (servidores e estabelecidas) (sem os servidores) 16/4 Mbps Token Ring 16/4 Mbps Token Ring (Novo) SLIP 6 bits <from_interface> <incompleto>  ALLMULTI  AX.25 AMPR NET/ROM AMPR AMPR ROSE ARCnet O AX.25 não foi configurado neste sistema.
 Sockets AX.25 ativos
 Sockets IPX ativos
Proto Recv-Q Send-Q Endereço Local             Endereço Remoto           Estado Conexões Internet Ativas  Ative sockets NET/ROM
 Domain sockets UNIX ativos  Sockets X.25 ativos
 SLIP Adaptativo Endereço TipoHW EndereçoHW Flags Máscara Iface
 Remoção de endereço não suportada neste sistema.
 Família de endereços `%s' não suportada.
 Appletalk DDP Ash BROADCAST  Endereço inválido.
 Endereço de E/S:0x%x  Um túnel de broadcast precisa de um endereço de origem.
 CCITT X.25 FECHAR ESPERANDO_FECHAR FECHANDO CONN ENVIADO CONECTADO CONECTANDO Callsign muito longo Não foi possível mudar a disciplina da linha para `%s'.
 Não foi possível criar o socket Compat DARPA Internet DEBUG  DGRAM DISC ENVIADO DESCONECTANDO Canal DMA:%x  DYNAMIC  TTL padrão é %u Dest Orig Dispositivo LCI Estado Vr/Vs Fila-Envio Fila-Recep
 Destino    Origem Dispositivo Estado       Vr/Vs  Send-Q  Recv-Q
 Destino                                     Próximo "Hop"                          Opções Métrica Ref   Uso Iface
 Destino                   Rede Roteadora            Nó Roteador
 Destino         Roteador        MáscaraGen.    Opções   MSS Janela  irtt Iface
 Destino         Roteador        MáscaraGen.    Opções Métrica Ref   Uso Iface
 Destino         Roteador        MáscaraGen     Opções Métrica Ref   Uso Iface    MSS   Janela irtt
 Destino      Iface    Uso
 Destino      Mnemônico Qualidade Vizinho   Iface
 Reordenação detectada %u vezes usando FACK Reordenação detectada %u vezes usando SACK Reordenação detectada %u vezes usando retransmissão rápida reno Reordenação detectada %u vezes usando timbre de hora %s: dispositivo não encontrado Não sei como configurar endereços para a família %d.
 ESTAB ESTABELECIDA Econet Entradas: %d	Ignoradas: %d	Encontradas: %d
 Ethernet ESPERA_FIN1 ESPERA_FIN2 LIVRE Não foi possível obter o tipo de [%s]
 FDDI - Fibra Ótica Não é suportado limpar a tabela de roteamento `inet'
 Limpeza da tabela de roteamento `inet6' não é suportada
 Repassagem está %s FRAD - Dispositivo de Acesso a Frame Relay Frame Relay DLCI EUI-64 Genérico Global HIPPI Endereço de HW %s   O tipo de hardware `%s' não é suportado.
 Máquina Histograma de entrada ICMP: Histograma de saída ICMP NET/ROM não configurado neste sistema.
 NET/ROM não configurado neste sistema.
 Entradas de IP mascarado
 Túnel IPIP O AX.25 não foi configurado neste sistema.
 NET/ROM: isto precisa ser escrito
 IPv4 Group Memberships
 IPv6 IPv6 sobre IPv4 Iface MTU Met RX-OK RX-ERR RX-DRP RX-OVR TX-OK TX-ERR TX-DRP TX-OVR Flg
 Iface     MTU Met  RX-OK RX-ERR RX-DRP RX-OVR  TX-OK TX-ERR TX-DRP TX-OV Opções
 Interface       CntRef Grupo
 Interface %s não está inicializada
 IRQ:%d  Callsign inválido IrLAP Tabela de roteamento AX.25 do kernel
 Tabela de Roteamento IP do Kernel
 Tabela de Roteamento IP do Kernel
 Tabela de roteamento IPX do kernel
 Cache de Vizinhos IPv6 do Kernel
 Tabela de Roteamento IPv6 do Kernel
 Tabela de Interfaces do Kernel
 Tabela de roteamento NET/ROM do kernel
 Tabela de roteamento ROSE do kernel
 Chaves não são permitidas com ipip e sit.
 LAPB ÚLTIMO_ACK OUÇA OUVINDO LOOPBACK  Link Loopback Local MASTER  MULTICAST  Endereço Ash mal formado Mídia:%s Memória:%lx-%lx  Não é suportado modificar o cache de roteamento `inet'
 NET/ROM não configurado neste sistema.
 NET/ROM: isto precisa ser escrito
 NOARP  NOTRAILERS  Vizinho                                     Endereço HW       Iface   Opções Estado Ref
 Vizinho                                     Endereço HW       Iface   Opções Estado Ref
       Parado(seg) Remover(seg)
 Nenhuma entrada ARP para %s
 Nenhum roteamento para a família `%s'
 Este sistema não tem suporte a ECONET.
 Este sistema não tem suporte a INET.
 Este sistema não tem suporte a INET6.
 Nenhuma família de endereços que possa ser usada foi encontrada.
 Endereço do nó deve ter dez dígitos Novell IPX POINTOPOINT  PROMISC  Por favor não especifique mais que uma família de endereços.
 Protocolo Ponto-a-Ponto Problemas lendo dados de %s
 Modo ack rápido foi ativado %u vezes RAW RDM RECUPERAÇÃO ROSE não configurada neste sistema.
 Algorítmo RTO é %s RUNNING  RX bytes:%llu (%lu.%lu %s) TX bytes:%llu (%lu.%lu %s)
 pacotes RX:%llu erros:%lu descartados:%lu excesso:%lu quadro:%lu
 RX: Pacotes    Bytes        Erros  CsunErrs ForaSeq  Mcasts
 Sistema ficou %u vezes sem memória durante o envio de pacotes Resolvendo `%s'...
 Resultado: h_addr_list=`%s'
 Resultado: h_aliases=`%s'
 Resultado: h_name=`%s'
 SABM ENVIADO SEQPACKET SLAVE  STREAM SYN_RECEBIDO SYN_ENVIADO SLIP Configurando nome do domínio para `%s'
 Configurando nome da máquina para `%s'
 Configurando nome do nó como `%s'
 Site Desculpe, use o pppd!
 Destino         Roteador        MáscaraGen.    Opções   MSS Janela  irtt Iface
 Destino         Roteador        MáscaraGen.    Opções Métrica Ref   Uso Iface
 Destino         Roteador        Origem         Opções Métrica Ref   Uso Iface    MSS   Janela irtt   HH  Arp
 Destino         Roteador        Origem         Opções Métrica Ref   Uso Iface    MSS   Janela irtt   HH  Arp
 TCP ficou sem memória %u vezes TIME_WAIT Pacotes TX:%llu erros:%lu descartados:%lu excesso:%lu portadora:%lu
 TX: Pacotes    Bytes        Erros  DeadLoop SemRota  SemBufs
 Este kernel não tem suporte a RARP.
 Excesso no número de famílias de endereços.
 UNIX Domain DESC. DESCONHECIDA Não Especificado UP  Desconhecido Família de endereços `%s' desconhecida.
 Tipo desconhecido de mídia.
 Uso:
  arp [-vn]  [<HW>] [-i <if>] [-a] [<máquina>]             <-Mostra cache ARP
 Uso:
  ifconfig [-a] [-v] [-s] <interface> [[<AF>] <endereço>]
 Uso: hostname [-v] {máquina|-F arquivo}       configura nome da máquina ( a partir de arquivo)
 Uso: inet6_route [-vF] del Destino
 Uso: inet_route [-vF] del {-host|-net} Destino[/prefixo] [gw Gw] [metric M] [[dev] If]
 Uso: ipmaddr [ add | del ] ENDMULTI dev STRING
 Uso: iptunnel { add | change | del | show } [ NOME ]
 Uso: ifconfig [-a] [-i] [-v] interface
 Uso: rarp -a                                   lista entradas no cache
 Uso: route [-nNvee] [-FC] [famílias_de_endereços]  Lista as tabelas de rotea-
                                                   mento do kernel
 Usuário    Destino    Origem  Dispositivo Estado      Vr/Vs  Send-Q  Recv-Q
 SLIP VJ 6 bits SLIP VJ AVISO: no mínimo um erro ocorreu. (%d)
 Aviso: A Interface %s ainda está no modo ALLMULTI
 Aviso: A Interface %s está em modo TRANSMISSAO .
 A Interface %s está em modo DINAMICO.
 Aviso: A Interface %s ainda está no modo MULTICAST
 Aviso: A interface %s ainda está no modo POINTOPOINT.
 Aviso: A Interface %s ainda está em modo promíscuo... talvez outra aplicação esteja sendo executada?
 Atenção: não foi possível abrir %s (%s). Saída limitada.
 Onde: NAME := STRING
 Formato errado de /proc/net/dev. Desculpe.
 Você não pode iniciar o PPP com este programa.
 [SEM FLAGS] [NENHUMA FLAG]  [Nenhum configurado] [DESCONHECIDO] respostas de máscara de subrede: %u requisição de máscara de subrede: %u requisições de máscaras: %u arp: %s: tipo de hardware sem suporte a ARP.
 arp: %s: kernel somente suporta inet'.
 arp: %s: família de endereços desconhecida.
 arp: %s: tipo de hardware desconhecido.
 arp: -N ainda não suportada.
 arp: não foi possível abrir o arquivo etherfile %s!
 arp: não foi possível configurar a linha %u do arquivo etherfile %s!
 arp: não foi possível obter o endereço de hardware para `%s': %s.
 arp: o dispositivo `%s' tem endereço de hardware %s `%s'.
 arp: erro de formato na linha %u do arquivo etherfile %s!
 arp: em %d entradas não foi encontrado.
 arp: endereço inválido de hardware
 arp: preciso do endereço de hardware
 arp: preciso do nome da máquina
 arp: erro no tipo do protocolo.
 não foi possível determinar o modo do túnel (ip, gre ou sit)
 não foi possível abrir /proc/net/snmp compactados:%lu  destino inalcançável: %u desabilitado o nome do domínio DNS (que é parte do FQDN) no arquivo /etc/hosts.
 respostas de eco: %u requisição echo: %u requisições de eco: %u habilitado erro lendo /proc/net/snmp família %d  X.25 genérico getdomainname()=`%s'
 gethostname()=`%s'
 getnodename()=`%s'
 tipo de endereço de hw `%s' não tem um controlador para configurar o endereço. falhou.
 ifconfig: Não foi possível configurar o endereço para esta família de protocolos.
 ifconfig: `--help' apresenta informações de uso
 ifconfig: opção `%s' não conhecida
 in_arcnet(%s): endereço arcnet inválido!
 in_arcnet(%s): trailing : ignorado!
 in_arcnet(%s): lixo no trailing!
 in_ether(%s): endereco ether inválido!
 in_ether(%s): trailing : ignorado!
 in_ether(%s): lixo no trailing!
 in_fddi(%s): endereço fddi inválido!
 in_fddi(%s): trailing : ignorado!
 in_fddi(%s): lixo no trailing!
 in_hippi(%s): endereço fddi inválido!
 in_fddi(%s): trailing : ignorado!
 in_fddi(%s): lixo no trailing!
 in_tr(%s): endereço token ring inválido!
 in_tr(%s): trailing : ignorado!
 in_tr(%s): lixo no trailing!
 ip: %s é um endereço IPv4 inválido
 ip: %s é um endereço de rede inválido
 ip: %s é um prefixo inválido inet
 ip: argumento errado: %s
 keepalive (%2.2f/%ld/%d) falta informação da interface mascara %s  uso de netrom
 netstat: família de protocolos %d não suportada!
 Sem entrada RARP para %s.
 desligado (0.00/%ld/%d) em %s
 ligado (%2.2f/%ld/%d) ligado %d (%2.2f/%ld/%d) problemas lendo dados de %s
 prot   expira    initseq delta prevd origem               destino             portas
 prot   expira origem               destino             portas
 rarp: %s: tipo desconhecido de hardware.
 rarp: %s: máquina desconhecida
 rarp: não foi possível abrir o arquivo %s:%s.
 rarp: não é possível incluir uma entrada para %s:%u
 rarp: erro de formato em %s:%u
 redirecionamento: %u redirecionamentos: %u route: %s: não é possível usar uma REDE como roteador!
 route: MSS inválido.
 route: rtt inicial inválido.
 route: janela inválida.
 route: netmask %s inválida
 route: a máscara %.8x não faz sentido em rotas para máquinas
 route: a netmask não casa com o endereço de rede
 rresolve: família de endereços %d não suportada!
 slattach: /dev/%s já bloqueado!
 slattach: não foi possível escrever o arquivo PID
 slattach: setvbuf(stdout,0,_IOLBF,0) : %s
 slattach: nome do tty muito longo
 slattach: tty_hangup(DROP): %s
 slattach: tty_hangup(RAISE): %s
 slattach: tty_lock: (%s): %s
 slattach: tty_lock: o usuário UUCP %s é desconhecido!
 slattach: tty_open: não foi possível obter a disciplina de linha corrente!
 slattach: tty_open: não foi possível obter o estado corrente!
 slattach: tty_open: não foi possível configurar %s bps!
 slattach: tty_open: não foi possível configurar modo 8N1!
 slattach: tty_open: não foi possível configurar o modo RAW!
 quebra na fonte: %u fontes extintas: %u tempo excedido: %u tempo expirou em trânsito: %u respostas de data/hora: %u respostas de timestamp: %u requisições de timestamp: %u requisições de timestamp: %u timewait (%2.2f/%ld/%d) ttl != 0 e noptmudisc são incompatíveis
 txqueuelen:%d  desconh.-%d (%2.2f/%ld/%d) Desconhecido uso: netstat [-veenNcCF] [<Af>] -r         netstat {-V|--version|-h|--help}
 atenção, recebi linha igmp inválida %d.
 atenção, recebi linha igmp6 inválida %d.
 atenção, recebi linha raw inválida.
 atenção, recebi linha tcp inválida.
 atenção, recebi linha udp inválida.
 atenção, recebi linha unix inválida.
 atenção: nenhum socket inet disponível: %s
 parâmetros incorretos: %u 