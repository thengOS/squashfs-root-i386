��          �      L      �     �  �   �  -   �  -   �  >   �     >  0   C  ;   t     �     �  "   �     �           :     H     d          �  �  �     b  �   {  2   W  3   �  X   �       A     >   ^     �     �  1   �  0   �  2   /	     b	     r	  !   �	  %   �	     �	                                
                          	                                          Convert key to lower case Copyright (C) %s Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 Create simple DB database from textual input. Do not print messages while building database INPUT-FILE OUTPUT-FILE
-o OUTPUT-FILE INPUT-FILE
-u INPUT-FILE NAME Print content of database file, one entry a line Report bugs using the `glibcbug' script to <bugs@gnu.org>.
 Write output to file NAME Written by %s.
 cannot open database file `%s': %s cannot open input file `%s' cannot open output file `%s': %s duplicate key problems while reading `%s' while reading database: %s while writing database file: %s wrong number of arguments Project-Id-Version: libnss-db
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2000-09-10 16:37+0200
PO-Revision-Date: 2007-11-08 05:13+0000
Last-Translator: André Gondim <Unknown>
Language-Team: Portuguese (Brazil) <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:03+0000
X-Generator: Launchpad (build 18115)
 Converte para minúsculo Copyright (C) %s Free Software Foundation, Inc.
Este é um programa livre; veja o código fonte para condições de cópia. NÃO há
garantia; nem mesmo para COMERCIALIZAÇÃO ou ADEQUAÇÃO A UM PROPÓSITO PARTICULAR.
 Cria um simples DB database de uma entrada textual Não mostra mensagem quando constrói banco de dado ENTRADA -ARQUIVO SAÍDA -ARQUIVO
-o SAÍDA -ARQUIVO ENTRADA -ARQUIVO
-u ENTRADA -ARQUIVO NOME Imprime o índice do arquivo banco de dado, uma entrada uma linha Reportar erro usando o script `glibcbug' para <bugs@gnu.org>.
 Escreve saida no arquivo NAME Escrito por %s.
 não é possível abrir arquivo database `%s': %s não foi possível abrir arquivo de entrada `%s' não é possível abrir arquivo de saída `%s': %s tecla duplicada problema durante leitura `%s' enquando está lendo database: %s enquanto escreve arquivo database: %s número de argumentos errado 