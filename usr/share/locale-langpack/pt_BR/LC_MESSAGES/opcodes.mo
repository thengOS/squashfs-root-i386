��    �      �  K        8     9  L   ;  K   �  �   �  u   �  �     �   �  k   ;  }   �  [   %  �   �  [     �   k  �   �  (   �  (   �  (   �  1   
  B   <  %     %   �  3   �  (   �  (   (  2   Q  E   �  ?   �  (   
  1   3     e     z     �  "   �  %   �  /   �  .     $   ?     d     �     �     �     �  	   �     �  	   �                .     D     X  (   v     �     �     �     �           &      ?      ]      z      �   =   �   &   �   *   !     ;!  
   S!  D   ^!  C   �!  +   �!  &   "     :"     P"  ,   k"  %   �"  )   �"  %   �"  !   #  !   0#  $   R#     w#  :   �#  1   �#  9    $  6   :$     q$     �$  "   �$     �$     �$     �$     %     %     -%  !   C%     e%  '   �%  '   �%     �%  7   �%      &  "   ;&  #   ^&     �&  )   �&  /   �&     �&  .   '     F'     \'  %   n'  #   �'  +   �'  +   �'  1   (  1   B(  %   t(  +   �(  1   �(  1   �(     *)  !   F)  %   h)  "   �)  *   �)     �)     �)  "   *     <*      S*  /   t*  #   �*     �*     �*     �*     +      )+  %   J+     p+     �+  +   �+     �+     �+      ,  %   ,  $   A,     f,  0   �,  0   �,  #   �,  7   -  !   ?-  !   a-  5   �-  "   �-  +   �-      .      ).      J.     k.  +   �.  2   �.  2   �.  )   /  #   G/     k/     �/     �/     �/     �/     �/     0     +0  "   I0     l0     �0     �0     �0     �0     �0     1     1     31     K1  2   ^1  %   �1     �1     �1  &   �1     2     %2     @2     ]2     q2     �2     �2     �2  !   �2  ;   �2     3  %   *3  +   P3  /   |3     �3     �3  &   �3  2   4  2   >4  2   q4  4   �4  ,   �4     5  )    5     J5  (   i5  %   �5     �5  !   �5  +   �5     #6  5   =6     s6     �6  -   �6  ;   �6     7     7  /   77  9   g7  	   �7     �7     �7     �7     �7     �7     8      8     88  &   Q8  �  x8     -:  P   /:  O   �:  �   �:  z   ~;  �   �;  �   �<  p   /=  l   �=  j   >  �   x>  g   ?  �   {?  �   @  0   �@  0   �@  0   A  1   GA  Y   yA  +   �A  +   �A  -   +B  "   YB  #   |B  4   �B  \   �B  B   2C  $   uC  .   �C     �C     �C     �C  '    D  *   (D  5   SD  4   �D  *   �D  &   �D  
   E     E     'E     CE     _E  %   nE     �E     �E     �E     �E     �E     F  :   !F     \F     zF  !   �F  *   �F     �F     G  %   G  %   EG     kG  !   �G  H   �G  3   �G  5   %H     [H  
   oH  K   zH  H   �H  -   I  '   =I  *   eI  %   �I  0   �I  ,   �I  0   J  ,   EJ  *   rJ  &   �J  +   �J     �J  ;   K  2   HK  :   {K  :   �K  "   �K     L  *   0L     [L     oL     �L     �L     �L     �L  *   �L  "   M  .   .M  .   ]M     �M  8   �M  &   �M  $   	N  %   .N      TN  /   uN  H   �N     �N  3   O      @O     aO  (   wO  2   �O  8   �O  ;   P  1   HP  1   zP  (   �P  6   �P  3   Q  3   @Q  )   tQ  +   �Q  2   �Q  "   �Q  .    R  2   OR  %   �R  /   �R     �R  6   �R  3   *S  $   ^S     �S     �S     �S     �S  $   �S  3   T     FT  "   eT  8   �T     �T  )   �T      	U  8   *U  7   cU  "   �U  F   �U  F   V  +   LV  C   xV  +   �V  )   �V  ?   W  )   RW  6   |W  #   �W  #   �W  #   �W  "   X  /   BX  6   rX  6   �X  -   �X  1   Y  !   @Y     bY     zY  "   �Y  #   �Y     �Y     �Y     Z  +   ,Z  "   XZ     {Z     �Z  /   �Z     �Z     �Z     [     ,[     I[     c[  :   [  *   �[     �[  %    \  4   &\     [\  #   r\  )   �\     �\     �\     �\     ]     ]  (   !]  :   J]  "   �]  /   �]  3   �]  7   ^     D^  %   `^  -   �^  7   �^  7   �^  7   $_  5   \_  2   �_     �_  .   �_  "   `  )   7`  (   a`      �`  "   �`  0   �`     �`  B   a     aa      ua  5   �a  C   �a     b     )b  5   Cb  ?   yb  
   �b     �b     �b     �b     �b  *   c     Dc  %   _c     �c  ,   �c                 �   u   M   �   �          �                         �          q   �   O   �   H          -           �   �   �   	       �   �       �   !   �   )       �   �   _   B                  �           �   N   l   �      �   Z       �      �      h   �          y   i           �   /   P   }   �   Y   �   �   %   �   t   �       *       s   �   �   �                 g       f   p   �   �   �   G       "          |       6       �   �       v   �   F   �   �   �   D   ?   n   �           :   �   �   �   d   �   �   b   �      �   X   �   �      �               �   �       �   4   �   �   ,       I       �   �   �               ^   ;   �   R                �   �   Q   �   '   K   C   a       j   �   �      �   k   �   r   <              o   [   �       �       �   �       T           m       A   �   2   U       1       �   �               �      @   �   9       �       e   #   $   3   V              �   +   W   �       x                   w   7       z   �   .   ]   �   �       L   (   >   �          c              �   �       �   
   S   \       J   �   �   �   =   `           �       �           �   �          �       ~   �          �       �   0   �   5      �   �          �   &   �   �   �   �               8       �   �   �   E      {   �   �       �            
 
  For the options above, The following values are supported for "ARCH":
    
  For the options above, the following values are supported for "ABI":
    
  cp0-names=ARCH           Print CP0 register names according to
                           specified architecture.
                           Default: based on binary being disassembled.
 
  fpr-names=ABI            Print FPR names according to specified ABI.
                           Default: numeric.
 
  gpr-names=ABI            Print GPR names according to  specified ABI.
                           Default: based on binary being disassembled.
 
  hwr-names=ARCH           Print HWR names according to specified 
			   architecture.
                           Default: based on binary being disassembled.
 
  reg-names=ABI            Print GPR and FPR names according to
                           specified ABI.
 
  reg-names=ARCH           Print CP0 register and HWR names according to
                           specified architecture.
 
The following ARM specific disassembler options are supported for use with
the -M switch:
 
The following MIPS specific disassembler options are supported for use
with the -M switch (multiple options should be separated by commas):
 
The following PPC specific disassembler options are supported for use with
the -M switch:
 
The following S/390 specific disassembler options are supported for use
with the -M switch (multiple options should be separated by commas):
 
The following i386/x86-64 specific disassembler options are supported for use
with the -M switch (multiple options should be separated by commas):
   addr16      Assume 16bit address size
   addr32      Assume 32bit address size
   addr64      Assume 64bit address size
   att         Display instruction in AT&T syntax
   att-mnemonic
              Display instruction in AT&T mnemonic
   data16      Assume 16bit data size
   data32      Assume 32bit data size
   esa         Disassemble in ESA architecture mode
   i386        Disassemble in 32bit mode
   i8086       Disassemble in 16bit mode
   intel       Display instruction in Intel syntax
   intel-mnemonic
              Display instruction in Intel mnemonic
   suffix      Always display instruction suffix in AT&T syntax
   x86-64      Disassemble in 64bit mode
   zarch       Disassemble in z/Architecture mode
 # <dis error: %08lx> $<undefined> %02x		*unknown* %d unused bits in i386_cpu_flags.
 %d unused bits in i386_operand_type.
 %dsp16() takes a symbolic address, not a number %dsp8() takes a symbolic address, not a number %s: %d: Missing `)' in bitfield: %s
 %s: %d: Unknown bitfield: %s
 %s: Error:  %s: Warning:  (DP) offset out of range. (SP) offset out of range. (unknown) *unknown operands type: %d* *unknown* 21-bit offset out of range <function code %d> <illegal instruction> <illegal precision> <internal disassembler error> <internal error in opcode table: %s %s>
 <unknown register %d> ABORT: unknown operand Address 0x%s is out of bounds.
 Attempt to find bit index of 0 Bad case %d (%s) in %s:%d
 Bad immediate expression Bad register in postincrement Bad register in preincrement Bad register name Biiiig Trouble in parse_imm16! Bit number for indexing general register is out of range 0-15 Byte address required. - must be even. Don't know how to specify # dependency %s
 Don't understand 0x%x 
 Hmmmm 0x%x IC note %d for opcode %s (IC:%s) conflicts with resource %s note %d
 IC note %d in opcode %s (IC:%s) conflicts with resource %s note %d
 IC:%s [%s] has no terminals or sub-classes
 IC:%s has no terminals or sub-classes
 Illegal as 2-op instr Illegal as emulation instr Illegal limm reference in last instruction!
 Immediate is out of range -128 to 127 Immediate is out of range -32768 to 32767 Immediate is out of range -512 to 511 Immediate is out of range -7 to 8 Immediate is out of range -8 to 7 Immediate is out of range 0 to 65535 Internal disassembler error Internal error:  bad sparc-opcode.h: "%s", %#.8lx, %#.8lx
 Internal error: bad sparc-opcode.h: "%s" == "%s"
 Internal error: bad sparc-opcode.h: "%s", %#.8lx, %#.8lx
 Internal: Non-debugged code (test-case missing): %s:%d Invalid size specifier Label conflicts with `Rx' Label conflicts with register name Missing '#' prefix Missing '.' prefix Missing 'pag:' prefix Missing 'pof:' prefix Missing 'seg:' prefix Missing 'sof:' prefix No relocation for small immediate Not a pc-relative address. Only $sp or $15 allowed for this opcode Only $tp or $13 allowed for this opcode Operand is not a symbol Operand out of range. Must be between -32768 and 32767. Register list is not valid Register must be between r0 and r7 Register must be between r8 and r15 Register number is not valid Small operand was not an immediate number Special purpose register number is out of range Syntax error: No trailing ')' The percent-operator's operand is not a symbol Unknown bitfield: %s
 Unknown error %d
 Unrecognised disassembler option: %s
 Unrecognised register name set: %s
 Unrecognized field %d while building insn.
 Unrecognized field %d while decoding insn.
 Unrecognized field %d while getting int operand.
 Unrecognized field %d while getting vma operand.
 Unrecognized field %d while parsing.
 Unrecognized field %d while printing insn.
 Unrecognized field %d while setting int operand.
 Unrecognized field %d while setting vma operand.
 Value is not aligned enough Value of A operand must be 0 or 1 W keyword invalid in FR operand slot. Warning: rsrc %s (%s) has no chks
 Warning: rsrc %s (%s) has no chks or regs
 address register in load range address writeback not allowed attempt to read writeonly register attempt to set HR bits attempt to set readonly register attempt to set y bit when using + or - modifier auxiliary register not allowed here bad instruction `%.50s' bad instruction `%.50s...' bad jump flags value bit,base is out of range bit,base out of range for symbol branch address not on 4 byte boundary branch operand unaligned branch to odd offset branch value not in range and to odd offset branch value out of range byte relocation unsupported can't cope with insert %d
 can't create i386-init.h, errno = %s
 can't create i386-tbl.h, errno = %s
 can't find %s for reading
 can't find i386-opc.tbl for reading, errno = %s
 can't find i386-reg.tbl for reading, errno = %s
 can't find ia64-ic.tbl for reading
 cgen_parse_address returned a symbol. Literal required. class %s is defined but not used
 displacement value is not aligned displacement value is not in range and is not aligned displacement value is out of range don't know how to specify %% dependency %s
 dsp:16 immediate is out of range dsp:20 immediate is out of range dsp:24 immediate is out of range dsp:8 immediate is out of range expecting got relative address: got(symbol) expecting got relative address: gotoffhi16(symbol) expecting got relative address: gotofflo16(symbol) expecting gp relative address: gp(symbol) flag bits of jump address limm lost ignoring invalid mfcr mask illegal bitmask illegal use of parentheses imm:6 immediate is out of range immediate is out of range 0-7 immediate is out of range 1-2 immediate is out of range 1-8 immediate is out of range 2-9 immediate value cannot be register immediate value is out of range immediate value out of range impossible store index register in load range invalid %function() here invalid conditional option invalid constant invalid counter access invalid load/shimm insn invalid mask field invalid operand.  type may have values 0,1,2 only. invalid register for stack adjustment invalid register name invalid register number `%d' invalid register operand when updating invalid sprg number jump flags, but no .f seen jump flags, but no limm addr jump hint unaligned junk at end of line ld operand error missing `)' missing `]' missing mnemonic in syntax string most recent format '%s'
appears more restrictive than '%s'
 multiple note %s not handled
 must specify .jd or no nullify suffix no insns mapped directly to terminal IC %s
 no insns mapped directly to terminal IC %s [%s] not a valid r0l/r0h pair offset(IP) is not a valid form opcode %s has no class (ops %d %d %d)
 operand out of range (%ld not between %ld and %ld) operand out of range (%ld not between %ld and %lu) operand out of range (%lu not between %lu and %lu) operand out of range (0x%lx not between 0 and 0x%lx) operand out of range (not between 1 and 255) overlapping field %s->%s
 overwriting note %d with note %d (IC:%s)
 parse_addr16: invalid opindex. percent-operator operand is not a symbol register name used as immediate value register number must be even register source in immediate move register unavailable for short instructions rsrc %s (%s) has no regs
 source and target register operands must be different st operand error store value must be zero syntax error (expected char `%c', found `%c') syntax error (expected char `%c', found end of instruction) too many long constants too many shimms in load unable to change directory to "%s", errno = %s
 unable to fit different valued constants into instruction undefined unknown unknown	0x%02lx unknown	0x%04lx unknown constraint `%c' unknown operand shift: %x
 unknown reg: %d
 unrecognized form of instruction unrecognized instruction warning: ignoring unknown -M%s option
 Project-Id-Version: opcodes 2.12.91
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-02-10 09:42+1030
PO-Revision-Date: 2016-05-30 16:27+0000
Last-Translator: Alexandre Folle de Menezes <Unknown>
Language-Team: Brazilian Portuguese <ldp-br@bazar.conectiva.com.br>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:04+0000
X-Generator: Launchpad (build 18115)
 
 
  Para as opções acima, os seguintes valores são suportados para "ARCH":
    
  Para as opções acima, os seguintes valores são suportados para "ABI":
    
  nomes-cp0=nomes de registrador de Impressão ARCH CP0 de acordo com a arquitetura especificada.
                           Padrão: baseado no binário sendo desmontado.
 
  nomes-fpr=nomes de Impressão ABI FPR de acordo com o ABI especificado.
                           Padrão: numérico.
 
  nomes-gpr=nomes de Impressão ABI GPR de acordo com o ABI especificado.
                           Padrão: baseado no binário sendo desmontado.
 
  nomes-hwr=nomes de Impressão ARCH HWR, de acordo com a arquitetura especificada.
                           Padrão: baseado no binário sendo desmontado.
 
  nomes-de-reg=nomes de Impressão ABI GPR e FPR de 
                           acordo com o ABI especificado.
 
  nomes-de-reg=nomes de registrador de Impressão ARCH CP0 e HWR
de acordo com a arquitetura especificada.
 
As opções do desmontador espcíficas para ARM a seguir não são suportadas para
uso com a opção -M:
 
As seguintes opções específicas do desmontador MIPS são suportadas para uso com
a opção -M (múltiplas opções devem ser separadas com vírgula):
 
As seguintes opções disassembler específicas para PPC são suportadas para uso com
o comutador -M:
 
As seguintes opções do desmontador de tipo S/390 podem ser usadas
com o parâmetro -M (múltiplas opções devem ser separadas por vírgulas):
 
As seguintes opções específicas de desmontagem i386/x86-64 são suportadas para usar
com o comutador -M (opções múltiplas devem ser separadas por vírgulas):
   addr16 Assume o tamanho de endereço de 16bit
   addr32 Assume o tamanho de endereço de 32bit
   addr64 Assume o tamanho de endereço de 64bit
   att Mostra a instrução com a sintaxe em AT&T
   att-mnemonic
              Mostra a instrução usando o formato de mnemônico da AT&T
   data16 Assume o tamanho de dado de 16bit
   data32 Assume o tamanho de dado de 32bit
   Desmontador esa no modo de arquitetura ESA
   i386 Desmontar em modo de 32bit
   i8086 Desmontar em modo de 16bit
   intel Mostra a instrução com a sintaxe em Intel
   intel-mnemonic
              Mostra a instrução usando o formato de mnemônico da Intel
   suffix Sempre mostra o sufixo da instrução com a sintaxe AT&T
   x86-64 Desmontar em modo de 64bit
   Desmontador zarch no modo de z/Architecture
 # <dis erro: %08lx> $<indefinido> %02x		*desconhecido* %d bits não usados em i386_cpu_flags.
 %d bits não usados em i386_operand_type.
 %dsp16() usa um endereço simbólico, não um número %dsp8() usa um endereço simbólico, não um número %s: %d: ')' Ausente no campo binário: %s
 %s: %d: Campo de bit desconhecido: %s
 %s: Erro:  %s: Aviso:  Desvio (DP) fora de limite. Desvio (SP) fora de limite. (desconhecido) *tipo de operandos desconhecidos: %d* *desconecida* desvio de 21-bit fora do limite <código de função %d> <instrução ilegal> <precisão ilegal> <erro interno do desmontador> <erro interno na tabela de códigos de operação: %s %s>
 <registrador %d desconhecido> ABORTAR: operando desconhecido Endereço 0x%s fora dos limites.
 Tentativa de achar o índice binário de 0 Case %d errado (%s) em %s:%d
 Expressão imediata errada Registrador errado no pós-incremento Registrador errado no pré-incremento Nome de registrador errado Graaande Problema em parse_imm16! Número do bit para indexar o registrador geral está fora da faixa 0-15 Endereço do "byte" necessário. - precisa ser par. Não se sabe como especificar a # de dependência %s
 Não entendo 0x%x 
 Hmmmm 0x%x ota IC %d para o mnemônico %s (IC:%s) conflitua com o recurso %s, nota %d
 Nota IC %d no mnemônico %s (IC:%s) conflitua com o recurso %s, nota %d
 IC:%s [%s] não tem terminais ou subclasses.
 IC:%snão tem terminais ou subclasses.
 2 operadores são ilegais como instrução Ilegal como instrução de emulação Referência limm ilegal na última instrução!
 Immediate está fora do intervalo -128 a 127 Immediate está fora do intervalo -32768 a 32767 Immediate está fora do intervalo -512 a 511 Immediate está fora de alcançe -7 até 8 Imediato está fora da faixa de -8 a 7 Immediate está fora do intervalo 0 a 65535 Erro interno do desmontador Erro interno:  sparc-opcode.h errado: "%s", %#.8lx, %#.8lx
 Erro interno: sparc-opcode.h errado: "%s" == "%s"
 Erro interno: sparc-opcode.h errado: "%s", %#.8lx, %#.8lx
 Interno: Código não depurado (test-case faltando): %s:%d Especificador de tamanho inválido O rótulo conflita com `Rx' O rótulo conflita com nome de registrador Prefixo '#' perdido Prefixo '.'  perdido Prefixo 'pag:' perdido Prefixo 'pof:' perdido Prefixo 'seg:' perdido Prefixo 'sof:' perdido Não há relocação para pequeno imediato Não é um endereço relativo a pc Somente $sp ou $15 permitidos para este opcode Somente $tp ou $13 permitidos para este opcode Operando não é um símbolo Operando fora da faixa. Deve estar entre -32768 e 32767. Lista de Registradores não é válida Registrador deve estar entre r0 e r7 Registrador deve estar entre r8 e r15 Número do registrador inválido O operando pequeno não era um número imediato Número do registrador de propósito especial está fora da faixa limite Erro de sintaxe: ')' faltando O operador porcento do operando não é um símbolo Campo binário desconhecido: %s
 Erro %d desconhecido
 Opção do desmontador desconhecida: %s
 Conjunto de nomes de registrador desconhecido: %s
 Campo %d não reconhecido durante construção de insn.
 Campo %d não reconhecido durante decodificação de insn.
 Campo %d não reconhecido ao obter operando int.
 Campo %d não reconhecido ao obter operando vma.
 Campo %d desconhecido durante análise.
 Campo %d não reconhecido durante impressão de insn.
 Campo %d não reconhecido ao definir operando int.
 Campo %d não reconhecido ao definir operando vma.
 Valor não está suficientemente alinhado Valor do operando A deve estar entre 0 ou 1 Palavra-chave W inválida no lugar do operando FR. Aviso: rsrc %s (%s) não tem chks
 Atenção: rsrc %s (%s) não tem chks ou regs
 registro de endereço no intervalo de carregamento endereço de writeback não permitido tentativa de ler um registrador somente-escrita tentativa de setar bits HR tentativa de configurar um registrador somente-leitura tentativa de setar bit y ao usar modificador + ou - registo auxiliar não permitido aqui instrução `%.50s' errada instrução `%.50s...' errada valor marcas de salto ruim bit,base for da faixa bit, fora do intervalo para símbolo endereço de desvio não está no limite de 4 bytes operando de desvio desalinhado desvio para um deslocamento ímpar valor do desvio fora da faixa e para deslocamento ímpar valor do desvio fora da faixa não há suporte para relocação de byte impossível lidar com insert %d
 não foi possível criar i386-init.h, erro número = %s
 não foi possível criar i386-tbl.h, erro número = %s
 impossível achar %s para leitura
 não foi possível achar i386-opc.tbl para leitura, erro número = %s
 não foi possível achar i386-reg.tbl para leitura, erro número = %s
 impossível achar ia64-ic.tbl para leitura
 cgen_parse_address retornou um símbolo. Um literal é necessário. classe %s está definida mas não é usada
 valor do deslocamento não está alinhado valor do deslocamento está fora da faixa e não está alinhado valor do deslocamento está fora da faixa Não se sabe como especificar a %% de dependência %s
 dsp:imediato 16 está fora da faixa dsp:imediato 20 está fora da faixa dsp:imediato 24 está fora da faixa dsp:imediato 8 está fora da faixa esperando endereço relativo got: got(símbolo) esperando endereço relativo got: gotoffhi16(símbolo) esperando endereço relativo got: gotofflo16(símbolo) esperando endereço relativo gp: gp(símbolo) bits de marca de endereço de salto limm perdidos ignorando máscara mfcr inválida máscara de bits ilegal uso ilegal de parênteses imm:imediato 6 está fora da faixa immediate está fora de alcance 0-7 imediato fora da faixa 1-2 imediato fora da faixa 1-8 imediato fora da faixa 2-9 valor imediato não pode ser um registrador valor imediato está fora da faixa valor imediato fora do limite impossível armazenar registrador de índice na faixa de carregamento inválida %function() aqui opção condicional inválida constante inválida acesso inválido do contador load/shimm insn inválido campo de máscara inválido operando inválido. type pode ter apenas os valores 0,1,2. registrador inválido para ajuste da pilha nome de registro inválido número inválido do registrador `%d' operando de registro inválido durante atualização número sprg inválido marca de salto, mas nenhum .f visto marca de salto, mas nenhum endereço limm dica de salto desalinhada lixo no final do arquivo erro de operador ld ')' faltando faltando `]' mnemônico faltando na string de sintaxe formato mais recente '%s'
parece mais restritivo que '%s'
 nota múltipla %s não manipulada
 deve especificar .jd ou não deixar sufixo nulo nenhum insns mapeado diretamente ao terminal IC %s
 nenhum insns mapeado diretamente ao terminal IC %s [%s] par r0l/r0h não é válido desvio(IP) não é um formato válido mnemônico %s não tem classe (ops %d %d %d)
 operando fora de faixa (%ld não está entre %ld e %ld) operando fora de faixa (%ld não está entre %ld e %lu) operando fora de faixa (%lu não está entre %lu e %lu) operando fora de alcance (0x%lx não entre 0 e 0x%lx) operando fora do limite (não está entre 1 e 255) campo em sobreposição %s->%s
 Sobrescrevendo nota %d com a nota %d  (IC:%s)
 parse_addr16: "opindex" inválido. O operando porcentual não é um simbolo. registrar nome usado como valor imediato número de registro deve ser par registrar origem no mover imediato indisponível para registrar instruções curtas rsrc %s (%s) não possui regs
 Operandos de registradores de fonte e destino devem ser diferentes erro no operando st o valor armazenado deve ser zero erro de sintaxe (esperado char `%c', encontrado `%c') erro de sintaxe (esperado char `%c', encontrado fim de instrução) muitas constantes longas Muitos shimms ao carregar impossível mudar diretório para "%s", numerro = %s
 incapaz de fixar constantes avaliadas diferentes na instrução indefinido desconhecido desconhecido	0x%02lx desconhecido	0x%04lx restrição `%c' desconhecida deslocamento de operando desconhecido: %x
 registro desconhecido: %d
 forma de instrução não reconhecida instrução não reconhecida alerta: ignorando opção desconhecida -M%s
 