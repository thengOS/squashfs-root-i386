��    r      �  �   <      �	  g   �	     
  *   2
  2   ]
  &   �
  5   �
  /   �
          6  1   N  B   �     �  	   �  >   �       !   +     M  )   i  *   �  =   �  2   �  )   /  D   Y  !   �     �  '   �                >  $   X  !   }     �     �  /   �     	  9        I  %   c  7   �  6   �  $   �          0     H     X     m     r  ?   �     �     �     �  3     A   J  =   �  ,   �  2   �  �   *  �  �  R   �     D     Y  8   k     �     �  <   �  ?        A  /   Q     �  (   �  *   �  1   �  *   #  -   N  "   |     �  ,   �  =   �     !     2  $   M     r     �     �  $   �     �  '   �  +     $   H     m  ?   �     �     �     �     �          '  !   8     Z     h     �     �      �     �  #   �     
  
   '     2  %   E     k     {     �     �  �  �  h   b     �  .   �  6     /   E  /   u  .   �  /   �  2     9   7  S   q     �     �  I   �     -   +   ?   +   k   ?   �   B   �   ]   !  >   x!  8   �!  f   �!  4   W"  &   �"  <   �"  2   �"  2   ##  "   V#  >   y#  :   �#  )   �#  (   $  6   F$     }$  A   �$     �$  2   �$  B   %  7   U%  (   �%     �%  )   �%     �%  &   &     4&     A&  R   S&  !   �&  $   �&     �&  B   
'  O   M'  O   �'  <   �'  9   *(  �   d(  ,  .)  \   [+     �+     �+  F   �+     &,     2,  5   D,  =   z,     �,  /   �,     �,  6   -  1   K-  7   }-  *   �-  0   �-  6   .     H.  5   f.  B   �.     �.  !   �.  +   /     F/     \/     n/  '   �/     �/  /   �/  4   �/  %   .0  "   T0  T   w0     �0     �0     �0      1  #   1     *1     >1     ^1  %   j1     �1     �1  *   �1  !   �1  '   2     -2     G2     X2  #   k2     �2     �2     �2     �2     j         L   o   $           `   l       #      Z       6   .   4                     a       n   >      Q   X   c       )   ^   Y   1             ]           k   B   :   9   O              R   *   C   \   T   N   0   F   8      S       [   5          ?                    ;   A       -                  =   3             '               r   !          K   E   (       G           "   P      &   V       I   e   2      q           i   
   +   H          b       h          7   p      m   	   d              <   f       D                     M   W      ,       U   @   %   _      /       g   J      PID   | Name                 | Port    | Flags
  ------+----------------------+---------+-----------
 %-14s - eject the CDROM
 %-14s - prints a list of clients attached
 %-14s - queries the server for certain parameters
 %-14s - reconfigure server parameters
 %-14s - reinitialising the keyboard and the trackpad
 %-14s - save the current configuration to disk
 %-14s - suspend to Disk
 %-14s - suspend to RAM
 %s (version %s) - control client for pbbuttonsd.
 %s - daemon to support special features of laptops and notebooks.
 %s, version %s <unknown> ADB keyboard can't disable fnmode, force mode to 'fkeyslast'.
 Buffer overflow Can't access i2c device %s - %s.
 Can't attach card '%s': %s
 Can't create IBaM object, out of memory.
 Can't create message port for server: %s.
 Can't find pmud on port 879. Trigger PMU directly for sleep.
 Can't get devmask from mixer [%s]; using default.
 Can't get volume of master channel [%s].
 Can't install PMU input handler. Some functionality may be missing.
 Can't install any signal handler
 Can't load card '%s': %s
 Can't open %s. Eject CDROM won't work.
 Can't open ADB device %s: %s
 Can't open PMU device %s: %s
 Can't open card '%s': %s
 Can't open framebuffer device '%s'.
 Can't open mixer device [%s]. %s
 Can't register mixer: %s
 Card '%s' has no '%s' element.
 Current battery cycle: %d, active logfile: %s.
 ERROR ERROR: Have problems reading configuration file [%s]: %s
 ERROR: Missing arguments
 ERROR: Not enough memory for buffer.
 ERROR: Problems with IPC, maybe server is not running.
 ERROR: Unexpected answer from server, actioncode %ld.
 ERROR: tag/data pairs not complete.
 File doesn't exist File not a block device File not a file Help or version info INFO Initialized: %s
 Insufficient permissions for object %s, at least '0%o' needed.
 Memory allocation failed.
 Memory allocation failed: %s
 Messageport not available Mixer element '%s' has no playback volume control.
 No backlight driver available - check your Kernel configuration.
 No event devices available. Please check your configuration.
 Not all signal handlers could be installed.
 Option 'ALSA_Elements' contains no valid elements
 Options:
    -%c, --help               display this help and exit
    -%c, --version            display version information and exit
    -%c, --ignore             ignore config return and error values
 Options:
   -%c, --help               display this help text and exit
   -%c, --version            display version information and exit
   -%c, --quiet              suppress welcome message
   -%c, --detach[=PIDFILE]   start %s as background process and
                            optional use an alternative pid-file
                            (default: %s)
   -%c, --configfile=CONFIG  use alternative configuration file
                            (default: %s)
see configuration file for more options.
 Orphaned server port found and removed. All running clients have to be restarted.
 PMU Backlight Driver Permission denied Please check your CDROM settings in configuration file.
 Private Tag Registration failed SECURITY: %s must be owned by the same owner as pbbuttonsd.
 SECURITY: %s must only be writable by the owner of pbbuttonsd.
 Script '%s' %s
 Script '%s' can't be launched - fork() failed.
 Script '%s' doesn't exist.
 Script '%s' lauched but exitcode is %d.
 Script '%s' lauched but exited by signal.
 Script '%s' lauched but killed after %d seconds.
 Script '%s' launched and exited normally.
 Script '%s' skipped because it's not secure.
 Script must be write-only by owner Server already running Server didn't send an answer and timed out.
 Server is already running. Sorry, only one instance allowed.
 Server not found Setting of %s failed: %s.
 Sorry, Couldn't get data for %s: %s. Supported commands:
 Supported tags:
 SysFS Backlight Driver The leading 'TAG_' could be omited.
 The object '%s' doesn't exist.
 The object '%s' is not a block device.
 The object '%s' is not a character device.
 The object '%s' is not a directory.
 The object '%s' is not a file.
 Too many formatsigns. Max three %%s allowed in TAG_SCRIPTPMCS.
 Unknown PowerBook Usage:
 Usage: %s [OPTION]... 
 WARNING WARNING: tag %s not supported.
 argument invalid can't be launched - fork() failed doesn't exist failed - unknown error code format error function not supported lauched but exitcode is not null lauched but exited by signal lauched but killed after %d seconds launched and exited normally open error pbbcmd, version %s pmud support compiled in and active.
 read-only value skipped because it's not secure unknown tag write-only value Project-Id-Version: pbbuttonsd
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2007-07-07 19:36+0200
PO-Revision-Date: 2008-04-16 21:30+0000
Last-Translator: André Gondim <Unknown>
Language-Team: Portuguese (Brazil) <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:35+0000
X-Generator: Launchpad (build 18115)
   PID   | Nome                 | Porta    | Flags
  ------+----------------------+---------+-----------
 %-14s - ejeta o CD
 %-14s - imprime uma lista de clientes ligados
 %-14s - questiona o servidor sobre certos parâmetros
 %-14s - reconfigura os parâmetros do servidor
 %-14s - reinicializando o teclado e o touchpad
 %-14s - salva a configuração atual no disco
 %-14s - entra em modo de hibernação no disco
 %-14s - entra em modo de hibernação na memória
 %s (versão %s) - cliente de controle para o pbbuttonsd.
 %s - serviço que habilita o suporte a funções especiais presentes em notebooks.
 %s, versão %s <desconhecido> Teclado ADB não pode desabilitar fnmode, forçar modo para 'fkeyslast'.
 Estouro de buffer Não pode acessar dispositivo i2c %s - %s.
 Não foi possível anexar cartão '%s': %s
 Não foi possível criar o objeto IBaM, memória insuficiente.
 Não foi possível criar a porta de mensagem para o servidor: %s.
 Não é possível localizar pmud na porta 879. Ativar PMU diretamente para estado em espera.
 Não é possível obter devmask do mixer [%s]; usando padrão
 Não foi possível obter o volume do canal mestre [%s].
 Não foi possível instalar o gerenciador de dados do PMU. Algumas funções podem não estar ativas.
 Não foi possível instalar nenhum gerador de sinal
 Impossível carregar cartão '%s': %s
 Não foi possível abrir %s. Retirar o CD não funcionará.
 Não foi possível abrir o dispositivo ADB %s: %s
 Não foi possível abrir o dispositivo PMU %s: %s
 Impossível abrir cartão'%s': %s
 Não foi possível acessar o framebuffer do dispositivo '%s'.
 Não foi possível abrir o mixer do dispositivo  [%s]. %s
 Não foi possível registrar o mixer: %s
 A placa '%s' não possui '%s' elemento.
 Ciclo da bateria atual: %d, arquivo de log ativo: %s.
 ERRO ERRO: Problemas na leitura do arquivo de configuração [%s]: %s
 ERRO: Argumentos faltando
 ERRO: Não há memória suficiente para o buffer.
 ERRO: Problemas com o IPC, talvez o servidor não esteja rodando.
 ERRO: Resposta inesperada do servidor, actioncode %ld.
 ERRO: paridade de tag/dados incompleta.
 Arquivo não existe O arquivo não é um dispositivo de bloco O arquivo não é valido Ajuda ou informações sobre a versão INFORMAÇÃO Inicializado: %s
 Permissões insuficientes para o objeto %s, é necessário ter no mínimo '0%o' .
 Falha na alocação de memória.
 Falha na alocação de memória: %s
 Messageport não disponível O mixer do elemento '%s' não tem controle de volume de playback.
 Nenhum driver de backlight disponível- cheque a configuração do seu Kernel.
 Nenhum dispositivo de evento disponível. Por favor cheque sua configuração.
 Alguns gerenciadores de sinais não puderam ser instalados.
 A opção 'ALSA_Elements' não contem elementos válidos
 Opções:
    -%c, --ajuda               mostra esta ajuda e sai
    -%c, --versão             mostra informaçòes sobre a versão e sai
    -%c, --ignorar             ignora erros de configuração
 Opções:
   -%c, --help               exibe este texto de ajuda e sai
   -%c, --version            exibe as informações de versão e sai
   -%c, --quiet              suprime a mensagem de boas vindas
   -%c, --detach[=PIDFILE]   inicia %s como processo em segundo plano e
                            opcionalmente usa um pid-file alternativo
                            (padrão: %s)
   -%c, --configfile=CONFIG  usa um arquivo de configuração alternativo
                            (padrão: %s)
veja o arquivo de configuração para mais opções.
 Porta órfã encontrada e removida do servidor. Todos os clientes precisam ser reiniciados.
 Driver PMU Backlight Permissão negada Por favor cheque suas opções de CDROM no arquivo de configuração.
 Tag privada Falha no registro SEGURANÇA: %s deve ser do mesmo dono de pbbuttonsd.
 SEGURANÇA: %s só pode ser gravado pelo dono de pbbuttonsd.
 Script '%s' %s
 O script '%s' não pode rodar - fork() falhou.
 O script '%s' não existe.
 O script '%s' rodou mas foi fechado com o código %d.
 O script '%s' rodou mas foi fechado por demanda.
 O script '%s' rodou mas foi fechado após %d segundos.
 O script '%s' rodou e fechou normalmente.
 O script '%s' foi ignorado pois não é seguro.
 O script deve ser de apenas escrita pelo proprietário O servidor já está rodando. O servidor não enviou uma resposta em tempo hábil.
 O Servidor já está rodando. Apenas uma instância é permitida.
 Servidor não encontrado Configuração de %s falhou: %s.
 Não foi possível obter dados para %s: %s. Comandos suportados:
 Tags suportadas:
 Driver SysFS Backlight A 'TAG_' principal pode estar omitida.
 O objeto '%s' não existe.
 O objeto '%s' não é um dispositivo de bloco.
 O objeto '%s' não é um dispositivo de caracteres.
 O objeto '%s' não é um diretório.
 O objeto '%s' não é um arquivo.
 Sinais de formatação em excesso. árvore máxima %%s permitido em TAG_SCRIPTPMCS.
 PowerBook desconhecido Uso:
 Uso: %s [OPÇÃO]... 
 AVISO AVISO: a tag %s não é suportada.
 argumento inválido não pode rodar - fork() falhou não existe falhou - código de erro desconhecido erro no formato função não suportada rodou mas o código de saída não é nulo rodou mas foi fechado por demanda rodou mas foi fechado após %d segundos rodou e saiu normalmente. erro na abertura pbbcmd, versão %s suporte ao pmud compilado e ativo.
 valor somente leitura ignorado pois não é seguro tag desconhecida valor escrita apenas 