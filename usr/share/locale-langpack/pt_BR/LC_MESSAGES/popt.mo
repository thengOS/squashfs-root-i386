��             +         �     �     �     �  "   �               '     +     0     9  (   >     g     m     t     �     �     �     �     �     �     �               7  /   H     x     �  )   �     �     �     �  �       �     �      �  &   �     �     �     �     �     �       3        C     I     P     o     �     �     �  )   �  ;   �     �          3     S  6   f  %   �  %   �  /   �     	     6	     H	                                                                 	                 
                                                              ARG DOUBLE Display brief usage message Display option defaults in message FLOAT Help options: INT LONG LONGLONG NONE Options implemented via popt alias/exec: SHORT STRING Show this help message Terminate options Usage: VAL [OPTION...] aliases nested too deeply config file failed sanity test error in parameter quoting invalid numeric value memory allocation failed missing argument mutually exclusive logical operations requested number too large or too small opt->arg should not be NULL option type (%u) not implemented in popt
 unknown errno unknown error unknown option Project-Id-Version: popt 1.6.3
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-02-17 13:35-0500
PO-Revision-Date: 2010-08-04 03:43+0000
Last-Translator: André Gondim <Unknown>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:04+0000
X-Generator: Launchpad (build 18115)
 ARG DOUBLE Mostra uma breve mensagem de uso Exibir as opções padrão na mensagem FLOAT Opções de ajuda: INT LONG LONGLONG NENHUM Opções implementadas através de popt alias/exec: SHORT STRING Mostrar esta mensagem de ajuda Rescindir opções Uso: VAL [OPÇÕES...] apelidos (aliases) aninhados muitas vezes falha no arquivo de configuração para o teste de sanidade erro ao citar parâmetros valor numérico inválido falha na alocação de memória faltando argumento operações lógicas mutualmente exclusivas requeridas número muito grande ou muito pequeno opt->arg não deveria ser nulo (NULL) tipo de opção (%u) não implementada em popt
 número de erro desconhecido erro desconhecido opção desconhecida 