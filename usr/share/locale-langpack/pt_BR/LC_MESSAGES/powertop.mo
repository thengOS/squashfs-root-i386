��    t      �  �   \      �	  $   �	     �	     �	  	   
  9   
     J
     R
  ,   W
     �
     �
     �
     �
  "   �
  -   �
       !     	   >  
   H     S     [     t     �     �  %   �  *   �     	     #     <     R     f     o     {  	   �  #   �  ?   �     �     �       /        N     ^  	   l     v     �  
   �     �     �     �     �  K   �  !   @     b     }  $   �  -   �     �      �     	          0  
   J  $   U  	   z  =   �  /   �     �          #     ?     \     l     z     �  .   �     �  *   �  >   �  ,   >  5   k     �     �     �     �      �     �     �          &      ;  	   \  &   f  
   �  .   �  
   �     �  3   �               -  "   >     a     s          �     �  !   �  0   �          1     K     d  !   x     �  .   �     �  �  �  2   �       	   
       >        \     d  J   i     �     �     �     �  ,     <   0     m  ,   r     �     �  
   �     �     �     �  #     %   +  4   Q     �  ,   �  %   �  '   �  	        $     0     M  6   [  E   �  	   �     �     �  5        7     T  	   i     s     �     �     �     �  %   �     �  b     (   {     �     �  2   �  P   �     P     d     �  "   �  /   �     �  /   �  	     @   (  9   i  !   �     �  H   �  I   $     n          �  %   �  3   �     �  .   �  D   .   5   s   4   �      �      �      �      !  4   $!     Y!     ]!     v!  6   �!  (   �!     �!  -   "  
   /"  5   :"     p"     |"  4   �"     �"     �"     �"  &   �"     #  
   -#  +   8#     d#     |#  $   �#  7   �#     �#  !   $     .$     F$  &   b$     �$  3   �$     �$                E              C   c   _   K   =   I   M   b   P   D                        J   d   "       )          6   i   h   g          R   !   ]      &   f   -       2   e           N       3   %                                V       Z   9   
   1   7       j       H       B          $           k   	   Q             '              #                 8   [      Y   a   >   r       0   q      \      p   X       @   <      L   :   4               t      O   (   .       S   m      ;      5          n       s   ^       W   o      U      ?   *       /   `      A   T   ,       F   l   G   +                  Usage     Device name
     0 mW   Core   Package  <ESC> Exit | <Enter> Toggle tunable | <r> Window refresh  CPU %i %7sW %s device %s has no runtime power management .... device %s 
 Actual Audio codec %s: %s Audio codec %s: %s (%s) Autosuspend for USB device %s [%s] Autosuspend for unknown USB device %s (%s:%s) Bad Bluetooth device interface status C0 active C0 polling CPU use Calibrating USB devices
 Calibrating backlight
 Calibrating idle
 Calibrating radio devices
 Calibrating: CPU usage on %i threads
 Calibrating: CPU wakeup power consumption
 Calibrating: disk usage 
 Cannot create temp file
 Cannot load from file Cannot save to file Category Description Device stats Disk IO/s Enable Audio codec power management Estimated power: %5.1f    Measured power: %5.1f    Sum: %5.1f

 Events/s Exit Failed to mount debugfs!
 Finishing PowerTOP power estimate calibration 
 Frequency stats GFX Wakeups/s GPU ops/s GPU ops/seconds Good Idle stats Intel built in USB hub Leaving PowerTOP Loaded %i prior measurements
 Measuring workload %s.
 Model-specific registers (MSR)			 not found (try enabling CONFIG_X86_MSR).
 NMI watchdog should be turned off Network interface: %s (%s) Overview Overview of Software Power Consumers PCI Device %s has no runtime power management PCI Device: %s PS/2 Touchpad / Keyboard / Mouse Package Parameters after calibration:
 Power Aware CPU scheduler Power est. Power est.    Usage     Device name
 PowerTOP  PowerTOP %s needs the kernel to support the 'perf' subsystem
 PowerTOP is out of memory. PowerTOP is Aborting Preparing to take measurements
 Radio device: %s Runtime PM for %s device %s Runtime PM for PCI Device %s SATA controller SATA disk: %s SATA link: %s Set refresh time out Starting PowerTOP power estimate calibration 
 Summary System baseline power is estimated at %sW
 Taking %d measurement(s) for a duration of %d second(s) each.
 The battery reports a discharge rate of %sW
 The estimated remaining time is %i hours, %i minutes
 Tunables USB device: %s USB device: %s (%s) Unknown Unknown issue running workload!
 Usage Usage: powertop [OPTIONS] VFS ops/sec and VM writeback timeout Wake-on-lan status for device %s Wakeups/s Wireless Power Saving for interface %s [=devnode] [=iterations] number of times to run each test [=seconds] [=workload] as well as support for trace points in the kernel:
 cpu package cpu package %i cpu rapl package cpu_idle event returned no state?
 dram rapl package exiting...
 file to execute for workload generate a csv report generate a html report generate a report for 'x' seconds power or cpu_frequency event returned no state?
 print this help menu print version information read_msr cpu%d 0x%llx :  run in "debug" mode runs powertop in calibration mode suppress stderr output uses an Extech Power Analyzer for measurements wakeups/second Project-Id-Version: PowerTOP 1.11
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-11-05 16:49-0800
PO-Revision-Date: 2013-09-17 14:21+0000
Last-Translator: Vinicius Almeida <vinicius.algo@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:21+0000
X-Generator: Launchpad (build 18115)
X-Poedit-Country: BRAZIL
X-Poedit-Language: Portuguese
               Nome do dispositivo de utilização
     0 mW   Núcleo   Pacote  <ESC> Sair | <Enter> Alternar sintonia | <r> Atualizar janela  CPU %i %7sW %s dispositivo %s não tem tempo de execução de gerenciamento de energia .... dispositivo %s 
 Atual Codec de áudio %s: %s Codec de áudio %s: %s (%s) Auto-suspensão para dispositivo USB %s (%s) Auto-suspensão para dispositivo USB desconhecido %s (%s:%s) Ruim Status da interface de dispositivo bluetooth C0 ativo C0 votação Uso da CPU Calibrando dispositivos USB
 Calibrar luz de fundo
 Calibragem ociosa
 Calibrar os dispositivos de rádio
 Calibrando: uso da CPU em %i threads
 Calibrando: consumo de energia de ativação de CPU
 Calibrar: uso do disco 
 Não é possível criar arquivo temporário
 Não é possível carregar do arquivo Não é possível salvar para o arquivo Categoria Descrição Estatísticas de dispositivo IO de Disco/s Permitir o gerenciamento de energia do codec de áudio Potência estimada: %5.1f    Potência medida: %5.1f    Soma: %5.1f

 Eventos/s Sair Falha ao montar debugfs!
 Finalizando calibragem de energia estimada PowerTOP 
 Estatísticas de frequência Recuperações GFX/s ops GPU/s operações GPU/segundos Bom Estatísticas inativas Hub USB interno da Intel Saindo do PowerTOP Carregadas %i  medições anteriores
 Medindo carga de trabalho %s.
 Registradores de modelos específicos (MSR)			 não encontrados (tentar ativando CONFIG_X86_MSR).
 Cão de guarda NMI deveria ser desligado Interface de rede: %s (%s) Visão geral Visão global de programas consumidores de energia O dispositivo PCI %s não possui gerenciamento de energia em tempo de execução Dispositivo PCI: %s PS/2 Touchpad / Teclado / Mouse Pacote Parâmetros após a calibração:
 Agendador da CPU para melhor consumo de energia Est. de energia Nome do dispositivo de uso estimado de energia
 PowerTOP  PowerTOP %s precisa do kernel para suportar o subsistema 'perf'
 O PowerTOP está sem memória. O PowerTOP está abortando Preparando para fazer medições
 Dispositivo Radio: %s Gerenciamento de energia em tempo de execução para o %s dispositivo %s Gerenciamento de energia em tempo de execução para o dispositivo PCI %s Controlador SATA Disco SATA: %s Link SATA: %s Definir tempo limite de atualização Iniciando calibragem de energia estimada PowerTOP 
 Sumário Energia de base do sistema é estimada em %sW
 Tomando %d medição(ões) por uma duração de %d segundo(s) cada.
 A bateria reporta uma taxa de descarregamento de %sW
 O tempo restante estimado é  %i horas e %i minutos
 Ajustáveis Dispositivo USB: %s Dispositivo USB: %s (%s) Desconhecido Problema desconhecido executando carga de trabalho!
 Uso Uso: powertop [OPÇÕES] operações VFS/segundo e Tempo espirado da escrita atrasada da máquina virtual Estado wake-on-lan para o dispositivo %s Recuperações/s Economia de energia para interface sem fio %s [=devnode] [=iterações] número de vezes a executar cada teste [=segundos] [=carga_de_trabalho] bem como suporte para pontos de rastreio no kernel:
 pacote de cpu pacote de cpu %i pacote cpu rapl evento cpu_idle não retornou estado?
 pacote dram rapl saindo...
 arquivo a executar para a carga de trabalho gerar um relatório csv gerar um relatório html gerar um relatório por 'x' segundos energia ou evento cpu_frequency retornaram sem estado?
 exibir este menu de ajuda imprimir informações da versão read_msr cpu%d 0x%llx:  rodar no modo "depuração" executar o powertop no modo calibragem suprimir saída stderr usa um analisador de energia Extech para medições recuperações/segundo 