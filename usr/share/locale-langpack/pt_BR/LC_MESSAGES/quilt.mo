��    �      �  �   �	        �     *    r   9    �  Z   �    '  �  �  _  �  }  �  �   f  �   Z  @  �  �   )   m   �   r   B!  \   �!  �  "  j  �#  �   I%  [   C&  `   �&  N    '  \   O'  �  �'    �,  �   �1  p  V2    �4  @   �5  '  6    87     T8     k8  %   ~8  *   �8     �8     �8  B   9  E   D9  L   �9     �9     �9  $   �9  !   :     @:     X:  .   n:  =   �:  0   �:     ;  -   (;     V;     r;  ,   �;  !   �;  (   �;     <     "<     A<     ]<     s<      �<     �<     �<     �<     �<     =  #   <=  -   `=      �=  %   �=  #   �=  0   �=  H   *>  .   s>     �>     �>     �>     �>     �>  =   ?  C   @?  (   �?  '   �?  +   �?  .   @  B   0@  9   s@  3   �@     �@  #   �@     A     =A     SA     sA      �A     �A     �A     �A  '   �A  &   #B  #   JB     nB     �B     �B     �B     �B  0   �B  1   C     =C  #   TC     xC  %   �C  #   �C  <   �C  &   D  O   DD  �   �D  3   E  X   �E  /   F     <F  @   SF  (   �F     �F     �F  "   �F  �   G     �G  =   �G  $   *H     OH  U   nH  *   �H  d   �H  <   TI  g   �I     �I     J     4J  '   RJ     zJ  9   �J  �   �J  +   ]K  )   �K     �K  @   �K     L     ,L      ?L     `L  .   wL  /   �L  �  �L  /  {N  �  �O  �   gS  [  �S  b   HV  �  �V  �  JY  �  �]  �  u_  $  Ma  �   rb  |  c  �   �d  {   Se  �   �e  c   Xf  �  �f  �  �h  -  �j  g   �k  h   l  K   �l  p   �l  �  =m  �  s  �   �x  �  �y  $  �|  P   �}  �   �}  V  �~     J�     a�  )   u�  :   ��     ڀ  7   �  f   )�  Q   ��  [   �     >�     P�  *   f�  )   ��     ��     ւ  6   �  [   &�  B   ��  3   Ń  6   ��     0�      N�  4   o�  &   ��  .   ˄     ��  !   �  )   :�     d�     ��  $   ��  (   ��  '   �  "   �  +   4�  )   `�  /   ��  ;   ��  -   ��  1   $�  ,   V�  ;   ��  W   ��  6   �     N�     n�     ��     ��     ��  F   Ɉ  L   �  *   ]�  )   ��  '   ��  0   ډ  B   �  =   N�  :   ��     Ǌ  *   ߊ     
�     *�  &   D�     k�  *   ��  !   ��     ׋  "   ��  /   �  *   I�  .   t�  $   ��     Ȍ     �     ��     �  /   ,�  3   \�     ��  (   ��  %   ԍ  *   ��  "   %�  F   H�  (   ��  M   ��    �  ;   �  f   I�  -   ��     ސ  @   ��  )   =�     g�     ��      ��  �   ő     y�  >   ��  "   ֒     ��  Z   �  )   r�  b   ��  >   ��  h   >�      ��     Ȕ     �  +   �     -�  =   M�  �   ��  )   �  '   <�     d�  >   }�     ��     ז     �     �  -   �  0   J�     v           �          \   =              �           Q   I   c          �   �   y   G          ~   w      �      p           h   �      #   a          �   L           F       �   /   n   m             �   0              d      l   ?      4   9   ^       o          E   3   �   [   i   �   _   D   �          H   Y   b       r   J   e             >   �       W   8          .   f       2   !           }   6       ;   Z   �       O   j   `   (       )   
   U   s       �   x   �           �   A          K   5       +          �   &   �   	   <   7           :   @                      $      %   g           1   k       �   "   �   �   X          �   P   u   t   -   N       ,   �      R          T           �   �      �   ]   z   C   |   M   *       '               S   �   {      q       B   V              
Add one or more files to the topmost or named patch.  Files must be
added to the patch before being modified.  Files that are modified by
patches already applied on top of the specified patch cannot be added.

-p patch
	Patch to add files to.
 
Apply patch(es) from the series file.  Without options, the next patch
in the series file is applied.  When a number is specified, apply the
specified number of patches.  When a patch name is specified, apply
all patches up to and including the specified patch.  Patch names may
include the patches/ prefix, which means that filename completion can
be used.

-a	Apply all patches in the series file.

-f	Force apply, even if the patch has rejects.

-q	Quiet operation.

-v	Verbose operation.

--leave-rejects
	Leave around the reject files patch produced, even if the patch
	is not actually applied.

--interactive
	Allow the patch utility to ask how to deal with conflicts. If
	this option is not given, the -f option will be passed to the 
	patch program.

--color[=always|auto|never]
	Use syntax coloring.
 
Create a new patch with the specified file name, and insert it after the
topmost patch in the patch series file.
 
Create mail messages from all patches in the series file, and either store
them in a mailbox file, or send them immediately. The editor is opened
with a template for the introductory message. Please see the file
%s for details.

--mbox file
	Store all messages in the specified file in mbox format. The mbox
	can later be sent using formail, for example.

--send
	Send the messages directly using %s.

--from, --subject
	The values for the From and Subject headers to use.

--to, --cc, --bcc
	Append a recipient to the To, Cc, or Bcc header.
 
Edit the specified file(s) in \$EDITOR (%s) after adding it (them) to
the topmost patch.
 
Fork the topmost patch.  Forking a patch means creating a verbatim copy
of it under a new name, and use that new name instead of the original
one in the current series.  This is useful when a patch has to be
modified, but the original version of it should be preserved, e.g.
because it is used in another series, or for the history.  A typical
sequence of commands would be: fork, edit, refresh.

If new_name is missing, the name of the forked patch will be the current
patch name, followed by \"-2\".  If the patch name already ends in a
dash-and-number, the number is further incremented (e.g., patch.diff,
patch-2.diff, patch-3.diff).
 
Generate a dot(1) directed graph showing the dependencies between
applied patches. A patch depends on another patch if both touch the same
file or, with the --lines option, if their modifications overlap. Unless
otherwise specified, the graph includes all patches that the topmost
patch depends on.
When a patch name is specified, instead of the topmost patch, create a
graph for the specified patch. The graph will include all other patches
that this patch depends on, as well as all patches that depend on this
patch.

--all	Generate a graph including all applied patches and their
	dependencies. (Unapplied patches are not included.)

--reduce
	Eliminate transitive edges from the graph.

--lines[=num]
	Compute dependencies by looking at the lines the patches modify.
	Unless a different num is specified, two lines of context are
	included.

--edge-labels=files
	Label graph edges with the file names that the adjacent patches
	modify.

-T ps	Directly produce a PostScript output file.
 
Global options:

--trace
	Runs the command in bash trace mode (-x). For internal debugging.

--quiltrc file
	Use the specified configuration file instead of ~/.quiltrc (or
	/etc/quilt.quiltrc if ~/.quiltrc does not exist).  See the pdf
	documentation for details about its possible contents.

--version
	Print the version number and exit immediately. 
Grep through the source files, recursively, skipping patches and quilt
meta-information. If no filename argument is given, the whole source
tree is searched. Please see the grep(1) manual page for options.

-h	Print this help. The grep -h option can be passed after a
	double-dash (--). Search expressions that start with a dash
	can be passed after a second double-dash (-- --).
 
Import external patches.

-p num
	Number of directory levels to strip when applying (default=1)

-n patch
	Patch filename to use inside quilt. This option can only be
	used when importing a single patch.

-f	Overwite/update existing patches.
 
Initializes a source tree from an rpm spec file or a quilt series file.

-d	optional path prefix (sub-directory).

-v	verbose debug output.
 
Integrate the patch read from standard input into the topmost patch:
After making sure that all files modified are part of the topmost
patch, the patch is applied with the specified strip level (which
defaults to 1).

-p strip-level
	The number of pathname components to strip from file names
	when applying patchfile.
 
Please remove all patches using \`quilt pop -a' from the quilt version used to create this working tree, or remove the %s directory and apply the patches from scratch.\n 
Print a list of applied patches, or all patches up to and including the
specified patch in the file series.
 
Print a list of patches that are not applied, or all patches that follow
the specified patch in the series file.
 
Print an annotated listing of the specified file showing which
patches modify which lines.
 
Print or change the header of the topmost or specified patch.

-a, -r, -e
	Append to (-a) or replace (-r) the exiting patch header, or
	edit (-e) the header in \$EDITOR (%s). If none of these options is
	given, print the patch header.
	
--strip-diffstat
	Strip diffstat output from the header.

--strip-trailing-whitespace
	Strip trailing whitespace at the end of lines of the header.

--backup
	Create a backup copy of the old version of a patch as patch~.
 
Print the list of files that the topmost or specified patch changes.

-a	List all files in all applied patches.

-l	Add patch name to output.

-v	Verbose, more user friendly output.

--combine patch
	Create a listing for all patches between this patch and
	the topmost applied patch. A patch name of \"-\" is equivalent
	to specifying the first applied patch.

 
Print the list of patches that modify the specified file. (Uses a
heuristic to determine which files are modified by unapplied patches.
Note that this heuristic is much slower than scanning applied patches.)

-v	Verbose, more user friendly output.
 
Print the name of the next patch after the specified or topmost patch in
the series file.
 
Print the name of the previous patch before the specified or topmost
patch in the series file.
 
Print the name of the topmost patch on the current stack of applied
patches.
 
Print the names of all patches in the series file.

-v	Verbose, more user friendly output.
 
Produces a diff of the specified file(s) in the topmost or specified
patch.  If no files are specified, all files that are modified are
included.

-p n	Create a -p n style patch (-p0 or -p1 are supported).

-u, -U num, -c, -C num
	Create a unified diff (-u, -U) with num lines of context. Create
	a context diff (-c, -C) with num lines of context. The number of
	context lines defaults to 3.

--no-timestamps
	Do not include file timestamps in patch headers.

--no-index
	Do not output Index: lines.

-z	Write to standard output the changes that have been made
	relative to the topmost or specified patch.

-R	Create a reverse diff.

-P patch
	Create a diff for the specified patch.  (Defaults to the topmost
	patch.)

--combine patch
	Create a combined diff for all patches between this patch and
	the patch specified with -P. A patch name of \"-\" is equivalent
	to specifying the first applied patch.

--snapshot
	Diff against snapshot (see \`quilt snapshot -h').

--diff=utility
	Use the specified utility for generating the diff. The utility
	is invoked with the original and new file name as arguments.

--color[=always|auto|never]
	Use syntax coloring.

--sort	Sort files by their name instead of preserving the original order.
 
Refreshes the specified patch, or the topmost patch by default.
Documentation that comes before the actual patch in the patch file is
retained.

It is possible to refresh patches that are not on top.  If any patches
on top of the patch to refresh modify the same files, the script aborts
by default.  Patches can still be refreshed with -f.  In that case this
script will print a warning for each shadowed file, changes by more
recent patches will be ignored, and only changes in files that have not
been modified by any more recent patches will end up in the specified
patch.

-p n	Create a -p n style patch (-p0 or -p1 supported).

-u, -U num, -c, -C num
	Create a unified diff (-u, -U) with num lines of context. Create
	a context diff (-c, -C) with num lines of context. The number of
	context lines defaults to 3.

--no-timestamps
	Do not include file timestamps in patch headers.
	
--no-index
	Do not output Index: lines.

--diffstat
	Add a diffstat section to the patch header, or replace the
	existing diffstat section.

-f	Enforce refreshing of a patch that is not on top.

--backup
	Create a backup copy of the old version of a patch as patch~.

--sort	Sort files by their name instead of preserving the original order.

--strip-trailing-whitespace
	Strip trailing whitespace at the end of lines.
 
Remove one or more files from the topmost or named patch.  Files that
are modified by patches on top of the specified patch cannot be removed.

-p patch
	Patch to remove files from.
 
Remove patch(es) from the stack of applied patches.  Without options,
the topmost patch is removed.  When a number is specified, remove the
specified number of patches.  When a patch name is specified, remove
patches until the specified patch end up on top of the stack.  Patch
names may include the patches/ prefix, which means that filename
completion can be used.

-a	Remove all applied patches.

-f	Force remove. The state before the patch(es) were applied will
	be restored from backup files.

-R	Always verify if the patch removes cleanly; don't rely on
	timestamp checks.

-q	Quiet operation.

-v	Verbose operation.
 
Remove the specified or topmost patch from the series file.  If the
patch is applied, quilt will attempt to remove it first. (Only the
topmost patch can be removed right now.)

-n	Delete the next patch after topmost, rather than the specified
	or topmost patch.
 
Rename the topmost or named patch.

-p patch
	Patch to rename.
 
Take a snapshot of the current working state.  After taking the snapshot,
the tree can be modified in the usual ways, including pushing and
popping patches.  A diff against the tree at the moment of the
snapshot can be generated with \`quilt diff --snapshot'.

-d	Only remove current snapshot.
 
Upgrade the meta-data in a working tree from an old version of quilt to the
current version. This command is only needed when the quilt meta-data format
has changed, and the working tree still contains old-format meta-data. In that
case, quilt will request to run \`quilt upgrade'.
        quilt --version %s: I'm confused.
 Appended text to header of patch %s\n Applied patch %s (forced; needs refresh)\n Applying patch %s\n Cannot add symbolic link %s\n Cannot diff patches with -p%s, please specify -p0 or -p1 instead\n Cannot refresh patches with -p%s, please specify -p0 or -p1 instead\n Cannot use --strip-trailing-whitespace on a patch that has shadowed files.\n Commands are: Conversion failed\n Converting meta-data to version %s\n Delivery address '%s' is invalid
 Diff failed, aborting\n Directory %s exists\n Display name '%s' contains invalid characters
 Display name '%s' contains non-printable or 8-bit characters
 Display name '%s' contains unpaired parentheses
 Failed to back up file %s\n Failed to copy files to temporary directory\n Failed to create patch %s\n Failed to import patch %s\n Failed to insert patch %s into file series\n Failed to patch temporary files\n Failed to remove file %s from patch %s\n Failed to remove patch %s\n Failed to rename %s to %s: %s
 File %s added to patch %s\n File %s disappeared!
 File %s exists\n File %s is already in patch %s\n File %s is not being modified\n File %s is not in patch %s\n File %s may be corrupted\n File %s modified by patch %s\n File %s removed from patch %s\n File \`%s' is located below \`%s'\n File series fully applied, ends at patch %s\n Fork of patch %s created as %s\n Fork of patch %s to patch %s failed\n Importing patch %s (stored as %s)\n Interrupted by user; patch %s was not applied.\n More recent patches modify files in patch %s. Enforce refresh with -f.\n More recent patches modify files in patch %s\n No next patch\n No patch removed\n No patches applied\n Nothing in patch %s\n Now at patch %s\n Option \`-n' can only be used when importing a single patch\n Options \`-c patch', \`--snapshot', and \`-z' cannot be combined.\n Patch %s appears to be empty, removing\n Patch %s appears to be empty; applied\n Patch %s does not apply (enforce with -f)\n Patch %s does not exist; applied empty patch\n Patch %s does not remove cleanly (refresh it or enforce with -f)\n Patch %s exists already, please choose a different name\n Patch %s exists already, please choose a new name\n Patch %s exists already\n Patch %s exists. Replace with -f.\n Patch %s is already applied\n Patch %s is applied\n Patch %s is currently applied\n Patch %s is not applied\n Patch %s is not in series file\n Patch %s is not in series\n Patch %s is now on top\n Patch %s is unchanged\n Patch %s needs to be refreshed first.\n Patch %s not applied before patch %s\n Patch %s not found in file series\n Patch %s renamed to %s\n Patch is not applied\n Refreshed patch %s\n Removed patch %s\n Removing patch %s\n Removing trailing whitespace from line %s of %s
 Removing trailing whitespace from lines %s of %s
 Renaming %s to %s: %s
 Renaming of patch %s to %s failed\n Replaced header of patch %s\n Replacing patch %s with new version\n SYNOPSIS: %s [-p num] [-n] [patch]
 The %%prep section of %s failed; results may be incomplete\n The -v option will show rpm's output\n The quilt meta-data in %s are already in the version %s format; nothing to do\n The quilt meta-data in this tree has version %s, but this version of quilt can only handle meta-data formats up to and including version %s. Please pop all the patches using the version of quilt used to push them before downgrading.\n The topmost patch %s needs to be refreshed first.\n The working tree was created by an older version of quilt. Please run 'quilt upgrade'.\n USAGE: %s {-s|-u} section file [< replacement]
 Unpacking archive %s\n Usage: quilt [--trace[=verbose]] [--quiltrc=XX] command [-h] ... Usage: quilt add [-p patch] {file} ...\n Usage: quilt annotate {file}\n Usage: quilt applied [patch]\n Usage: quilt delete [patch | -n]\n Usage: quilt diff [-p n] [-u|-U num|-c|-C num] [--combine patch|-z] [-R] [-P patch] [--snapshot] [--diff=utility] [--no-timestamps] [--no-index] [--sort] [--color] [file ...]\n Usage: quilt edit file ...\n Usage: quilt files [-v] [-a] [-l] [--combine patch] [patch]\n Usage: quilt fold [-p strip-level]\n Usage: quilt fork [new_name]\n Usage: quilt graph [--all] [--reduce] [--lines[=num]] [--edge-labels=files] [patch]\n Usage: quilt grep [-h|options] {pattern}\n Usage: quilt header [-a|-r|-e] [--backup] [--strip-diffstat] [--strip-trailing-whitespace] [patch]\n Usage: quilt import [-f] [-p num] [-n patch] patchfile ...\n Usage: quilt mail {--mbox file|--send} [--from ...] [--to ...] [--cc ...] [--bcc ...] [--subject ...]\n Usage: quilt new {patchname}\n Usage: quilt next [patch]\n Usage: quilt patches {file}\n Usage: quilt pop [-afRqv] [num|patch]\n Usage: quilt previous [patch]\n Usage: quilt push [-afqv] [--leave-rejects] [num|patch]\n Usage: quilt refresh [-p n] [-f] [--no-timestamps] [--no-index] [--diffstat] [--sort] [--backup] [--strip-trailing-whitespace] [patch]\n Usage: quilt remove [-p patch] {file} ...\n Usage: quilt rename [-p patch] new_name\n Usage: quilt series [-v]\n Usage: quilt setup [-d path-prefix] [-v] {specfile|seriesfile}\n Usage: quilt snapshot [-d]\n Usage: quilt top\n Usage: quilt unapplied [patch]\n Usage: quilt upgrade\n Warning: trailing whitespace in line %s of %s
 Warning: trailing whitespace in lines %s of %s
 Project-Id-Version: quilt
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-06-27 17:14+0000
PO-Revision-Date: 2011-04-08 16:24+0000
Last-Translator: Kenzo Okamura <Unknown>
Language-Team: Portuguese (Brazil) <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:14+0000
X-Generator: Launchpad (build 18115)
 
Adiciona um ou mais arquivos para o nível superior ou patch nomeado. Os arquivos devem ser
adicionados ao patch antes de ser modificado. Os arquivos que são modificados por
patches já aplicados em cima do patch especificado não pode ser adicionado.

-p patch
        Patch para adicionar arquivos.
 
Aplicar caminho(s) do arquivo de séries. Sem opções, o próximo caminho
no arquivo de séries é aplicado. Quando um número é especificado, aplica o
número especificado de caminhos. Quando um nome de caminho é especificado, aplica
todos os caminhos até ele e incluindo o caminho especificado. Nomes de caminhos podem
incluir os caminhos/prefixos, os que significa que a finalização do nome do arquivo pode
ser usada.

-a	Aplica todos os caminhos no arquivo de séries.

-f	Força a aplicação, mesmo se o caminho tem rejeições.

-q	Operação silenciosa.

-v	Operação detalhada.

--leave-rejects
	Deixa os caminhos dos arquivos de rejeição produzidos, mesmo se o caminho
	não estiver aplicado atualmente.

--interactive
	Permite o utilitário de caminho perguntar como lidar com conflitos. Se
	esta opção não for dada, a opção -f será passada para o
	programa de caminho.

--color[=always|auto|never]
	Usa coloração de sintaxe.
 
Cria um novo caminho com o nome de arquivo especificado, e insere ele após o
caminho principal no arquivo de séries de caminhos.
 
Cria mensagens de e-mail de todos os caminhos no arquivo de séries, e também as
armazena em um arquivo mailbox, ou as envia imediatamente. O editor é aberto
com um template para a mensagem introdutória. Por favor, veja o arquivo
%s para mais detalhes.

--mbox arquivo
	Armazena todas as mensagens no arquivo especificado no formato mbox. O mbox
	pode ser enviado depois usando formail, por exemplo.

--send
	Envia a mensagem diretamente usando %s.

--from, --subject
	Os valores dos cabeçalhos De e Assunto que serão usados.

--to, --cc, --bcc
	Anexa um recipiente ao cabeçalho Para, CC ou CCo.
 
Edita o(s) arquivo(s) especificado(s) em \$EDITOR (%s) após adicioná-lo(s) ao patch mais alto.
 
Bifurcar o caminho principal. Bifurcar um caminho significa criar uma cópia textual
dele sob um novo nome, e usar este novo nome ao invés do original
nas séries atuais. Isto é útil quando um caminho tem que ser
modificado, mas a versão original dele deverá ser preservada, por exemplo
porque ele será usado em outras séries, ou para o histórico. Uma sequência
típica dos comandos seria: fork, edit, refresh.

Se novo_nome estiver faltando, o nome do caminho bifurcado será o nome
atual do caminho, seguido por \"-2\".  Se o nome do caminho já terminar com um
traço e número, este número será incrementado (ex, patch.diff,
patch-2.diff, patch-3.diff).
 
Gera um gráfico direcionado ponto(1)  mostrando as dependências entre
os caminhos aplicados. Um caminho depende de outro caminho se ambos levam ao mesmo
arquivo ou, com a opção --lines, se suas modificações coincidirem. Salvo quando
o contrário for especificado, quando o gráfico inclui todos os caminhos que o caminho
principal depende.
Quando um nome de caminho é especificado, a não ser que seja o caminho principal, é criado um
gráfico para o caminho especificado. O gráfico incluirá todos os outros caminhos
que este caminho depende, assim como todos os caminhos dependentes deste
caminho.

--all	Gera um gráfico incluindo todos os caminhos aplicados e suas
	dependências. (Caminhos não aplicados não são incluídos.)

--reduce
	Elimina bordas transitivas do gráfico.

--lines[=número]
	Computa dependências ao olhar as linhas que os caminhos modificam.
	A não ser que um númerom diferente seja especificado, duas linhas do contexto são
	incluídas.

--edge-labels=arquivos
	Bordas das etiquetas dos gráficos com os nomes de arquivo que os caminhos adjacentes
	modificam.

-T ps	Produz diretamente um arquivo de saída PostScript.
 
Opções globais:

--traço
        Executar o comando no bash em modo de rastreamento (-x). Para debugging interno.

--arquivo quiltrc
        Use a configuração do arquivo especificado em vez de ~/.quiltrc (ou
        etc/quilt.quiltrc se ~/.quiltrc não existe). Veja o pdf
        documentação para detalhes sobre possiveis contéudos

--versão
        Imprimir o número da versão e sair imediatamente. 
Faz um grep através dos arquivos de fonte, recursivamente, ignorando
patches e meta-informação do quilt. Se nenhum nome de arquivo for
fornecido como argumento, toda a árvore de fontes será utilizada na
busca. Por favor, veja a página de manual do grep(1) para ver as opções.

-h	Imprime esta ajuda. A opção grep -h pode ser passada após um
	duplo traço (--). Expressões de busca que inicial com um traço
	podem ser passados após um segundo duplo traço.
 
Importa caminhos externos.

-p número
	Número de níveis de diretórios para retirar ao aplicar (padrão=1)

-n caminho
	Nome do arquivo do caminho para usar dentro do quilt. Esta opção pode ser usada
	somente ao importar um único caminho.

-f	Sobrescreve/atualiza caminhos existentes.
 
Inicializa uma árvore fonte de um arquivo rpm spec ou um arquivo de séries quilt.

-d	prefixo de caminho opcional (sub-diretório).

-v	saída de depuração detalhada.
 
Integra o caminho lido da entrada padrão dentro do caminho principal:
Após ter certeza que todos os arquivos modificados são parte do caminho
principal, o caminho é aplicado com o nível de retirada especificado (para o qual
o padrão é 1).

-p strip-level
	O número de componentes de nome do caminho para retirar dos nomes de arquivo
	quando aplicar o arquivo do caminho.
 
Por favor, remova todos os caminhos usando \`quilt pop -a' da versão quilt usada para criar esta árvore de trabalho, ou remova o diretório %s e aplique os caminhos do rascunho.\n 
Imprimir um lista de patches aplicados, ou todos os patches até e inclusive o
patche espicificado na série de arquivos.
 
Imprime uma lista de caminhos que não estão aplicados, ou todos os caminhos que seguem
o caminho especificado no arquivo de séries.
 
Imprimir uma lista anotada de arquivo especificado mostrando quais
patches modificam quais linhas
 
Imprime ou altera o cabeçalho do caminho principal ou o especificado.

-a, -r, -e
	Anexa (-a) ou substitui (-r) o cabeçalho de caminho existente, ou
	edita (-e) o cabeçalho em \$EDITOR (%s). Se nenhuma dessas opções foram
	dadas, imprime o cabeçalho do caminho.
	
--strip-diffstat
	Retira a saída diffstat do cabeçalho.

--strip-trailing-whitespace
	Retira os espaços em branco do final das linhas do cabeçalho.

--backup
	Cria uma cópia de segurança da última versão do caminho como caminho~.
 
Imprimir a lista dos arquivos de nível superior ou específico ao pacote de mudanças.

- a 	 Listar todos os arquivos e aplicativos  do pacote.

- I 	 Adicionar nome para saída do pacote.

- v 	 Detalhar, saídas mais amigáveis ao usuário.

-- coligar pacotes
	 criar uma lista para todos os pacotes entre este pacote e
	 o  pacote de aplicativos superior. O nome do pacote \"-\" é equivalente
	 para especificar o primeiro pacote de aplicativos

 
Imprime a lista de caminhos que modificam o arquivo especificado. (Usa uma
heurística para determinar quais arquivos são modificados por caminhos não aplicados.
Note que esta heurística é muito mais lenta que procurar pelos caminhos aplicados.)

-v	Detalhado, saída mais amigável ao usuário.
 
Imprime o nome do próximo caminho após o caminho especificado ou o principal
no arquivo de séries.
 
Imprime o nome do caminho anterior antes do caminho especificado
ou o principal no arquivo de séries.
 
Imprime o nome do caminho principal na pilha atual de caminhos
aplicados.
 
Imprime os nomes de todos os caminhos no arquivo de séries.

-v	Detalhado, saída mais amigável ao usuário.
 
Produz um diff do arquivo(s) especificado no nível superior ou patch
especificado. Se nenhum arquivo for especificado, todos os arquivos que são modificados estão
incluído.

-p n        Criar um -p n estilo de patche (-p0 ou p1 são suportados).

-u, -U num, -c, -C num
        Criar um diff unificado(-u, -U) com número de linhas de contexto. Criar
        um contexto de diff (-c, -C) com número de linhas de contexto. O número de
        linhas de contexto padrão para 3.

--no-timestamps
        Não inclue arquivos de selo de tempo no cabeçalho do patch.

--no-index
        Não gera índice: linhas.

-z        Escreve para a saída padrão as alterações que foram feitas
        em relação ao nível superior ou patch especificado.

-R        Criar um diff reverso.

-P patch
        Criar um diff para o patch especificado. (Padrão para o patch
mais elevado.)

--combine patch
        Criar um diff combinado para todos patches entre este patche e
        o patch selecionado com -P. Um nome de patch de \"-\" é equivalente
        ao especificar o patch aplicado pela primeira vez.
--snapshot
        Diff contra instantâneo (veja \'quilt instantâneo -h').
--diff=utility
        Use o utilitário especificado para gerar o diff. O utilitário
        é invocado com o original e o novo nome de arquivo como argumentos.

--color[=sempre|auto|nunca]
        Use a sintaxe colorir.

--short        Organizar arquivos pelo seu nome, em vez de preservar a ordem original.
 
Atualiza o caminho especificado, ou o caminho principal por padrão.
Documentação que vem antes do caminho atual no arquivo de caminho é
retido.

É possível atualizar os caminhos que não estão no topo. Se alguns caminhos
no topo do caminho a atualizar modifica os mesmos arquivos, o script aborta
por padrão. Caminhos podem ainda ser atualizados com -f. Nesse caso, este
script imprimirá um aviso para cada arquivo sombreado, as alterações no caminho
mais recente serão ignoradas, e apenas alterações nos arquivos que não
foram modificados por nenhum outro caminho recente terminará no caminho
especificado.

-p n	Cria um caminho de estilo -p n (-p0 ou -p1 suportados).

-u, -U num, -c, -C num
	Cria um diff unificado (-u, -U) com num linhas de contexto. Cria
	um diff de contexto (-c, -C) com num linhas de contexto. O padrão de
	número de linhas de contexto é 3.

--no-timestamps
	Não inclui data/hora de arquivo nos cabeçalhos do caminho.
	
--no-index
	Não retorna o índice: linhas.

--diffstat
	Adiciona uma seção diffstat para o cabeçalho do caminho, ou substitui a
	seção diffstat existente.

-f	Força a atualização de um caminho que não está no topo.

--backup
	Cria uma cópia de segurança da versão antiga de um caminho como caminho~.

--sort	Ordena arquivos pelos seus nomes ao invés de preservar a ordem original.

--strip-trailing-whitespace
	Retira espaços em branco do final das linhas.
 
Remove um ou mais arquivos do caminho principal ou nomeado. Arquivos que
são modificados pelos caminhos no topo do caminho especificado não podem ser removidos.

-p caminho
	Caminho de onde os arquivos serão removidos.
 
Remover caminho(s) da pilha de caminhos aplicados. Sem opções,
o caminho principal é removido. Quando um número for especificado, remove o
número especificado de caminhos. Quando um nome de caminho é especificado, remove
caminhos até o caminho especificado terminar no topo da pilha. Nomes de
caminhos podem incluir prefixos/caminhos, os quais significam que a finalização do
nome do arquivo pode ser usado.

-a	Remove todos os caminhos aplicados.

-f	Força remoção. O estado antes do(s) caminho(s) ser(em) aplicado(s)
	será(ão) restaurado(s) a partir dos arquivos de backup.

-R	Sempre verificar se o caminho remove corretamente; não contar com
	a verificação da data/hora.

-q	Operação silenciosa.

-v	Operação detalhada.
 
Remova o patch especificado ou superior a partir do arquivo da série. Se o
patch é aplicado, quilt vai tentar removê-lo primeiro. (Apenas
patch superior pode ser removida agora.)

-n        Apagar o próximo patch depois do superior, ao invés do patch especificado
         ou superior.
 
Renomeia o caminho principal ou o nomeado.

-p caminho
	Caminho para renomear.
 
Tira um snapshot do estado de trabalho atual. Depois de tirar o snapshot,
a árvore pode ser modificada nos modos usuais, incluindo mover e
destruir caminhos. Um diff na árvore no momento do
snapshot pode ser gerado com \` quilt diff --snapshot'.
 
Atualizar os meta-dados na árvore de trabalho de uma versão antiga do quilt para
a versão atual. Este comando somente é necessário quando o formato dos meta-dados do quilt
tiverem sido alterados, e a árvore de trabalho ainda contiver formatos de meta-dados antigos. Nesse
caso, o quilt solicitará executar o comando \`quilt upgrade'.
        quilt --version %s: Estou confuso.
 texto anexado ao cabeçalho do patch %s\n Caminho %s aplicado (forçado; necessita ser atualizado)\n Aplicando caminho %s\n Não foi possível adicionar o vínculo simbólico %s\n Você não pode fazer o diff dos patches com -p%s, por favor especifique -p0 ou -p1 como alternativa\n Não foi possível atualizar patches com -p%s, por favor especifique -p0 ou -p1\n Não é possível utilizar --strip-trailing-whitespace em um patch que sombreou arquivos.\n Os comandos são: A conversão falhou\n Convertendo meta-dados para a versão %s\n O endereço de entrega '%s' é inválido
 Falha no diff, abortando\n O diretório %s existe\n O nome apresentado '%s' contém caracteres inválidos
 O nome exibido '%s' contém caracteres que não podem ser impressos ou caracteres de 8-bit
 O nome apresentado '%s' contém parênteses não fechados/abertos
 Falha em fazer cópia de segurança do arquivo %s\n Falha ao copiar arquivos para diretório temporário\n Falha ao criar o caminho %s\n Falha ao importar o caminho %s\n Falha ao inserir o caminho %s no arquivo de série\n Falha no patch arquivos temporários\n Falhou ao remover o arquivo %s do caminho %s\n Falha ao remover o patch %s\n Falha ao renomear %s para %s: %s
 O arquivo %s foi adicionado ao patch %s\n Arquivo %s não encontrado!
 O arquivo %s já existe\n O arquivo %s já está no patch %s\n Arquivo %s não está sendo modificado\n O arquivo %s não está no caminho %s\n Arquivo %s pode estar corrompido\n O arquivo %s foi modificado pelo patch %s\n O arquivo %s foi removido do caminho %s\n O arquivo \`%s' está localizado abaixo \`%s'\n Arquivo series totalmente aplciado, termina no caminho %s\n Bifurcação do patch %s criada conforme %s\n Bifurcação de patch % s para patch % s falhou\n Importando caminho %s (armazenado como %s)\n Interrompido pelo usuário; Caminho %s não foi aplicado.\n Patches mais recentes modificam arquivos no patch %2. Force uma atualização com -f./n Patches mais recentes modificam arquivos do patch %s\n Não existe mais nenhum patch\n Caminho não removido\n Nenhum patch aplicado\n Nada no caminho %s\n Agora no caminho %s\n Opção \`-n' só pode ser usada quando for importar apenas um patch\n As opções \`-c patch', \`--snapshot', e \`-z' não podem ser combinadas.\n Caminho %s parece estar vazio, removendo\n Caminho %s parece estar vazio; aplicado\n Caminho %s não aplica (force com -f)\n Caminho %s não existe; caminho vazio aplicado\n Caminho %s não remove corretamente (atualize-o ou force com -f)\n O caminho %s já existe, Por favor, tente um nome diferente\n Caminho %s já existe, por favor escolha outro nome novo\n Caminho %s já existe\n Caminho %s já existe. Substitua com -f.\n Caminho %s já está aplicado\n Caminho %s foi aplicado\n O patch %s está atualmente aplicado\n O patch %s não foi aplicado\n Patch %s não está no arquivo da série\n O patch %s não está na série\n Patch %s agora está no topo\n O caminho %s não foi modificado\n Caminho %s necessita ser atualizado primeiro.\n Patch %s não aplicado antes do patch %s\n Caminho %s não encontrado no arquivo series\n O caminho %s foi renomeado para %s\n O patch não foi aplicado\n Patch atualizado %s\n O patch %s foi removido\n Removendo caminho %s\n. Removendo espaços em branco na linha %s de %s
 Removendo espaços em branco nas linhas de %s a %s
 Renomeando %s para %s: %s
 Falha ao renomear o caminho %s para %s\n Substituído cabeçalho do patch %s\n Substituindo caminho %s com nova versão\n SINOPSE: %s [-p num] [-n] [patch]
 A %%prep section of %s falhou; os resultados podem estar incompletos\n A opção -v mostrará a saída do rpm\n Os meta-dados quilt em %s já estão no formato da versão %s; nada a fazer\n Os meta-dados quilt nesta árvore tem a versão %s, mas esta versão do quilt somente pode manipular formatos de meta-dados até a versão incluída %s. Por favor, destrua todos caminho usando a versão do quilt usada para movê-los antes de fazer o downgrade.\n O caminho principal %s necessita ser atualizado primeiro.\n A árvore de trabalho foi criada por uma versão antiga do quilt. Por favor execute 'quilt upgrade'.\n USO: %s {-s|-u} section file [< replacement]
 Descompactando o arquivo %s\n Uso: quilt [--trace[=detalhado]] [--quiltrc=XX] comando [-h] ... Uso: quilt add [-p patch] {arquivo} ...\n Uso: quilt annotate {arquivo}\n Uso: quilt applied [patch]\n Uso: quilt delete [patch | -n]\n Uso: quilt diff [-p n] [-u|-U num|-c|-C num] [--combine patch|-z] [-R] [-P patch] [--snapshot] [--diff=utilidade] [--no-timestamps] [--no-index] [--sort] [--color] [arquivo ...]\n Uso: quilt edit arquivo ...\n Uso: quilt arquivos [-v] [-a] [-l] [--combine patch] [patch]\n Uso: quilt fold [-p strip-level]\n Uso: quilt fork [novo_nome]\n Uso: quilt graph [--all] [--reduce] [--lines[=número]] [--edge-labels=arquivos] [patch]\n Uso: quilt grep [-h|opções] {padrão}\n Uso: quilt header [-a|-r|-e] [--backup] [--strip-diffstat] [--strip-trailing-whitespace] [patch]\n Uso: quilt import [-f] [-p num] [-n patch] arquivo patch ...\n Uso: quilt mail {--mbox arquivo|--send} [--from ...] [--to ...] [--cc ...] [--bcc ...] [--subject ...]\n Uso: quilt new {nome do patch}\n Uso: quilt next [patch]\n Uso: quilt patches {arquivo}\n Uso: quilt pop [-afRqv] [número|caminho]\n Uso: quilt previous [caminho]\n Uso: quilt push [-afqv] [--leave-rejects] [número|caminho]\n Uso: quilt refresh [-p n] [-f] [--no-timestamps] [--no-index] [--diffstat] [--sort] [--backup] [--strip-trailing-whitespace] [patch]\n Uso: quilt remove [-p patch] {file} ...\n Uso: quilt rename [-p patch] new_name\n Uso: quilt series [-v]\n Uso: quilt setup [-d path-prefix] [-v] {specfile|seriesfile}\n Uso: quilt snapshot [-d]\n Uso: quilt top\n Uso: quilt unapplied [patch]\n Uso: quilt upgrade\n Avisvo: espaços em branco na linha %s de %s
 Aviso: espaços em branco nas linhas de %s a %s
 