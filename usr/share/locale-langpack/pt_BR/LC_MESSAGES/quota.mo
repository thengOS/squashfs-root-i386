��    �       �  �       (  3   (  D   5(  C   z(  6   �(  @   �(  \   6)     �)  �  �)  �  q+  =   -     @-  K   \-  Y   �-  C   .     F.     ].     t.     �.     �.     �.     �.     �.     �.     /  B   /  	   N/  	   X/     b/     p/     w/     z/  	   �/     �/  0   �/  )   �/     0     -0     L0     j0      �0     �0     �0     �0     �0     �0     1  0   81     i1     �1     �1  	   �1  	   �1  &   �1  &   �1  7  �1  �   /4  �   �4  �   55     6  2   #6      V6  &   w6  9   �6  8   �6  "   7  -   47  G   b7     �7     �7  A   �7  7   8  3   N8     �8  !   �8     �8  
   �8  +   �8     �8     9     9  -   )9  5   W9  !   �9  -   �9  *   �9  #   :  #   ,:  ?   P:  "   �:  %   �:  "   �:  .   �:  4   +;  #   `;     �;  &   �;     �;  !   �;  A   �;  Q   A<  6   �<  0   �<  4   �<  -   0=      ^=     =     �=  *   �=  5   �=  '   	>  .   1>  M   `>  %   �>  9   �>  7   ?  &   F?     m?  4   ~?     �?  <   �?     @  8   )@      b@  "   �@  (   �@  1   �@  )   A  *   +A  !   VA     xA  O   �A  *   �A  /   B  #   ?B  "   cB  &   �B     �B  *   �B  (   �B     C  /   .C  "   ^C  *   �C  %   �C     �C     �C  /   D  3   =D     qD  2   �D  .   �D  )   �D  %   E  .   =E  (   lE  -   �E  *   �E     �E     F  2   +F  G   ^F  �   �F  V   PG  .   �G  )   �G      H     H      ,H  &   MH     tH  &   �H  2   �H  *   �H  8   I  1   NI     �I     �I  /   �I     �I     J     +J  3   JJ  2   ~J  $   �J      �J     �J  C   K  $   SK  T   xK  S   �K  #   !L     EL     aL  "   ~L  -   �L     �L  $   �L     M  $   2M     WM     vM     �M  5   �M     �M  %   �M     N  !   1N  .   SN     �N     �N     �N  H   �N  $   O  '   2O  !   ZO  $   |O  $   �O     �O     �O     �O  ,   P  $   EP  =   jP  !   �P     �P     �P     �P  )   Q     9Q  !   YQ     {Q  9   �Q  5   �Q  *   R  !   3R  0   UR  %   �R     �R  
   �R     �R  6   �R  6   S  "   QS  2   tS  7   �S  3   �S  6   T     JT     [T  
   qT  L   |T     �T     �T     U     U     3U  _   PU  m   �U     V  #   ;V  3   _V     �V  /   �V     �V  2   �V  �   W  }   �W     X  n   ,X  (   �X     �X     �X     ]Y     vY  G   �Y     �Y  '   �Y     Z  2   Z     CZ     ZZ     zZ  i   �Z     �Z     [     [  ,   =[  8   j[  1   �[  N   �[  <   $\  #   a\  =   �\     �\  (   �\  )   ]  !   1]     S]  $   m]  5   �]  /   �]     �]  1   ^  F   >^  F   �^      �^      �^     _     *_     H_     d_     �_  1   �_     �_     �_     �_  %   �_     `  4   1`  4   f`     �`     �`  9   �`  7   �`  G   4a  2   |a  G   �a  6   �a  0   .b  0   _b  W   �b  x   �b  *   ac  :   �c  O   �c  �   d  ,   �d  &   �d  '   �d  S   e  D   ke     �e     �e     �e  3   f     Af  $   _f     �f     �f  *   �f  ,   �f     g  1   %g  -   Wg  X   �g     �g  P   �g     Bh     Th  2   dh     �h  ;   �h  /   �h  J   	i     Ti  .   li  (   �i  4   �i  0   �i     *j  I   Jj  H   �j  1   �j  2   k  2   Bk  (   uk     �k  (   �k  $   �k  N   l     Tl  �   jl  !   5m    Wm  �  vn  (   rr  �  �r  q  �t  j  �u  ;   ew     �w    �w  �  �{  N   �}  5   �}  5   (~  .   ^~  N   �~  )   �~  M     8   T     �  O   �  )   �  6   �  $   F�  1   k�  #   ��  *   ��  $   �  '   �  <   9�  �   v�  X   �     u�     ��     ��     ��     ��     ܂     �     ��     �  I   .�  A   x�  A   ��     ��      �     �     �  $   (�  %   M�  5   s�     ��     ��     Є     �  7   �     +�     <�     M�     c�     y�     ��     ��     ��     ��  5   Ʌ  1   ��     1�     6�     <�     B�     I�     Q�     V�     Z�     ]�     |�     ��     ��     ��     ��     ҆     ن     �     ��     �     �  6   .�  2   e�  	   ��     ��     ��     ��     Ç     ܇  �  ��  ;   ��  M   �  M   =�  C   ��  =   ϊ  O   �      ]�    ~�  �  ��  :   a�     ��  :   ��  H   �  R   6�     ��     ��     ��     ɐ     ݐ     �     �     ,�     B�     H�  M   O�  	   ��  	   ��     ��     ��     Ǒ     ʑ  	   ؑ     �  1   �  7   4�     l�  +   ��     ��     ؒ  (   �      �     =�  )   E�  &   o�  -   ��  *   ē  8   �     (�     G�     O�  
   X�     c�  .   o�  .   ��  j  ͔  v   8�  {   ��  �   +�  
   �  4   �     C�  0   c�  R   ��  F   �  '   .�  @   V�  j   ��     �  !   �  O   ;�  C   ��  F   ϛ     �  (   �     E�  
   _�  N   j�     ��     ؜     �  4   ��  E   *�  7   p�  I   ��  F   �  1   9�  2   k�  U   ��  3   ��  1   (�  0   Z�  ?   ��  K   ˟  5   �  $   M�  9   r�  &   ��  1   Ӡ  k   �  _   q�  L   ѡ  ?   �  <   ^�  :   ��  ,   ֢     �  %   #�  :   I�  V   ��  @   ۣ  I   �     f�  >   �  c   %�  R   ��  <   ܥ     �  S   8�  .   ��  \   ��  ,   �  U   E�  1   ��  7   ͧ  ?   �  E   E�  A   ��  B   ͨ  :   �  .   K�     z�  >   ��  9   9�  ?   s�  2   ��  <   �      #�  E   D�  <   ��  *   ǫ  @   �  6   3�  D   j�  ;   ��  2   �  +   �  @   J�  E   ��  &   ѭ  I   ��  E   B�  ?   ��  ?   Ȯ  H   �  C   Q�  F   ��  M   ܯ  6   *�  &   a�  P   ��  }   ٰ  <  W�  �   ��  G   �  B   `�  ,   ��     г  8   �  <   $�  $   a�  <   ��  Y   ô  H   �  c   f�  Y   ʵ  6   $�  5   [�  a   ��  1   �  8   %�  @   ^�  M   ��  S   ��  @   A�  >   ��  +   ��  d   ��  >   R�  �   ��  �   �  ;   ��  7   ٺ  -   �  B   ?�  K   ��  .   λ  5   ��  1   3�  *   e�  -   ��  -   ��     �  B   ��     A�  1   V�  *   ��  .   ��  3   �     �     ,�     F�  e   e�  4   ˾  .    �  &   /�  (   V�  -   �  )   ��     ׿     �  =   
�  '   H�  R   p�  *   ��  '   ��     �  *   �  8   F�  "   �  &   ��     ��  <   ��  9   �  3   W�  #   ��  ;   ��  8   ��     $�     2�     ;�  =   X�  =   ��  8   ��  8   �  ?   F�  =   ��  B   ��     �     %�     E�  \   Y�  !   ��  .   ��     �  !   $�      F�  h   g�  u   ��  '   F�  :   n�  F   ��     ��  ;   ��     2�  J   I�  �   ��  �   7�     ��  �   ��  1   X�     ��  �   ��     L�     g�  e   ��     ��  .   ��     '�  5   6�     l�  %   ��  #   ��  p   ��     A�     Q�  %   h�  @   ��  8   ��  9   �  T   B�  P   ��  3   ��  Y   �  %   v�  -   ��  6   ��  *   �  (   ,�  /   U�  <   ��  C   ��     �  8   �  l   W�  k   ��      0�  '   Q�     y�     ��     ��     ��     ��  7   �  .   L�  	   {�     ��  2   ��      ��  D   ��  D   %�     j�     ��  O   ��  N   ��  P   9�  C   ��  S   ��  I   "�  9   l�  @   ��  v   ��  �   ^�  G   ��  K   D�  g   ��  �   ��  7   ��  -   ��  .   !�  p   P�  R   ��  "   �  #   7�  &   [�  A   ��  "   ��  8   ��       �  ,   A�  6   n�  4   ��     ��  L   ��  7   :�  m   r�     ��  e   ��     V�     i�  ,   y�     ��  F   ��  >   ��  ]   4�     ��  9   ��  2   ��  >   �  =   V�  "   ��  Y   ��  O   �  X   a�  V   ��  O   �  5   a�     ��  4   ��  1   ��  d   �     ��  �   ��  +   ��  W  ��  �  �  .   ��  �  �  s  �  w  v�  9   ��     (�  
  1�  �  <�  m    �  E   n�  D   ��  ;   ��  w   5�  <   ��  x   ��  A   c�     ��  v   ��  >   "�  D   a�  3   ��  8   ��  /   �  7   C�  $   {�  4   ��  F   ��  �   �  q   ��     a�     u�     ��     ��     ��     ��      ��  +   ��      *�  Y   K�  C   ��  E   ��     /�     3�     8�  !   D�  2   f�  <   ��  4   ��     �     '�     A�     [�  N   d�     ��     ��      ��     ��  "   �      =�     ^�     d�     j�  =   ��  :   ��     ��     ��                      	   "     ,  "   3     V     c     i     �     �     �     �  "   �     �     �     �  8    4   T 
   �    �    �    �    �    �    $       �     �   �  X  �  B      @   x        h  �   �   �  �  #  7   �   �  �  �  M         �    ,      X       �  F      k  c  `                   �     �   �      4       �   �   0  K   .       8   �            z   (   �          �   S       �  3  �           �   U             �   �               �  �   S  0   j   �   d  �   �  �  +  �       l      c   q   �  �      �     �   �       �      �  �   �   �  Q  �     �  7  $         =      L  �      z  �   �   N      �  T  h       �       �   >  �   {       �   
     I   �  6   	   b           �        F           n   �   P  .  �  �   v  ]  �  ?  �   ]   �  ^       o          �  �       C   �  �  e   p      �   �  H      D       _     ~  �   �       �   �   /   O           �   �                  �   �   o  �     �       �   3   I  �                 p   A   �         �  9  v   �           �   �  �      �       �   �   �   �   s           W         }  �     �      �   |  �      C  �  �       �  �  E   )         s  �   T   �       `            �   Z              !           u  �   *  �   {        :   1   �       �        �  Y   �   #       �         �  q  H  >       L   J      A  �  f  �       =   D  �    M   �  �   �       �   �          [           �  ;   V  �   �   �   �   �  \  �     �          �       �   �      �  2   y     �   �   �  "      l       P   m  �           N   (  �   	  �  @  ^  &      �   +   �     �  2  �      �  '  �   |           �            �  8  %  G       -  �         �  n  U  t   �  d   �         <     �   �   /      �             r   �   a   }               9   �   �                 
        k   5  i       *   Z          5   x  '       �  "   �   �  �   �  4      �   f       a  y  �      �   �              [   !      �   O  �      �  t  :      �   �   �   �      �  �     ?   �  6      \   r  b     V       �  �   �   �  �      �  �           �      �           W   �  B   _                        R      �  �  K  �   �       �   �  �   �       ,   �           �       �                    �          w      &       �       �  g       R   m   ;  �       �  1  �  �      �     Q   �  �   <         �     w   �   �   �       Y      g  -   �   �   �    J   �   %   �       �  �   i        )   �   �      �     G  u   �           e      �   ~       E  �  j      �       �        	Adding %s size %lld ino %d links %d uid %u gid %u
 	quota [-qvswim] [-l | [-Q | -A]] [-F quotaformat] -g groupname ...
 	quota [-qvswim] [-l | [-Q | -A]] [-F quotaformat] -u username ...
 	quota [-qvswugQm] [-F quotaformat] -f filesystem ...
 
                        Block limits               File limits
 
-u, --user                    edit user data
-g, --group                   edit group data
 
Can open directory %s: %s
 
Please cleanup the group data before the grace period expires.

Basically, this means that the system thinks group is using more disk space
on the above partition(s) than it is allowed.  If you do not delete files
and get below group quota before the grace period expires, the system will
prevent you and other members of the group from creating new files owned by
the group.

For additional assistance, please contact us at %s
or via phone at %s.
 
We hope that you will cleanup before your grace period expires.

Basically, this means that the system thinks you are using more disk space
on the above partition(s) than you are allowed.  If you do not delete files
and get below your quota before the grace period expires, the system will
prevent you from creating new files.

For additional assistance, please contact us at %s
or via phone at %s.
                         %s limits                File limits
     %8llu    %8llu    %8llu   Filesystem                         block grace               inode grace
   Filesystem                   blocks       soft       hard     inodes     soft     hard
   Filesystem             Block grace period     Inode grace period
   cache hits:      %u
   cache misses:    %u
   dquot dups:      %u
   dquot wants:     %u
   inact reclaims:  %u
   missed reclaims: %u
   reclaims:        %u
   shake reclaims:  %u
 #%-7d %-8.8s %-9s       used    soft    hard  grace    used  soft  hard  grace
 %02d:%02d %8llu     %d	%llu	%llu
 %ddays %s %s (%s) %ss:
 %s (%s):
 %s (gid %d): Permission denied
 %s (gid %d): error while trying getgroups(): %s
 %s (gid %d): gid set allocation (%d): %s
 %s (uid %d): Permission denied
 %s [%s]: %s quotas turned off
 %s [%s]: %s quotas turned on
 %s quota on %s (%s) is %s
 %s quota sync failed for %s: %s
 %s quota sync failed: %s
 %s: %s
 %s: %s quotas turned off
 %s: %s quotas turned on
 %s: %s root_squash turned off
 %s: %s root_squash turned on
 %s: Quota cannot be turned on on NFS filesystem
 %s: deleted %s quota blocks
 %udays %uhours %uminutes %useconds *** Report for %s quotas on device %s
 *** Status for %s quotas on device %s
 -F, --format=formatname       edit quotas of a specific format
-p, --prototype=name          copy data from a prototype user/group
    --always-resolve          always try to resolve name, even if it is
                              composed only of digits
-f, --filesystem=filesystem   edit data only on a specific filesystem
-t, --edit-period             edit grace period
-T, --edit-times              edit grace time of a user/group
-h, --help                    display this help text and exit
-V, --version                 display version information and exit

 -r, --remote                  edit remote quota (via RPC)
-m, --no-mixed-pathnames      trim leading slashes from NFSv4 mountpoints
 -r, --remote               set remote quota (via RPC)
-m, --no-mixed-pathnames      trim leading slashes from NFSv4 mountpoints
 -t, --edit-period          edit grace period
-T, --edit-times           edit grace times for user/group
-h, --help                 display this help text and exit
-V, --version              display version information and exit

 0seconds Accounting [ondisk]: %s; Enforcement [ondisk]: %s
 Accounting: %s; Enforcement: %s
 Adding dquot structure type %s for %d
 Administrator for a group %s not found. Cancelling mail.
 Allocated %d bytes memory
Free'd %d bytes
Lost %d bytes
 Already accounting %s quota on %s
 As you wish... Canceling check of this file.
 Bad file magic or version (probably not quotafile with bad endianity).
 Bad format:
%s
 Bad number of arguments.
 Bad time units. Units are 'second', 'minute', 'hour', and 'day'.
 Batch mode and prototype user cannot be used together.
 Batch mode cannot be used for setting grace times.
 Block Block %u in tree referenced twice Block %u is truncated.
 Block %u:  Block grace time: %s; Inode grace time: %s
 Block limit reached on Bugs to %s
 Bugs to: %s
 Cannot access the specified xtab file %s: %s
 Cannot allocate new quota block (out of disk space).
 Cannot bind to given address: %s
 Cannot change grace times over RPC protocol.
 Cannot change owner of temporary file: %s
 Cannot change permission of %s: %s
 Cannot change state of GFS2 quota.
 Cannot change state of XFS quota. It's not compiled in kernel.
 Cannot commit dquot for id %u: %s
 Cannot connect to netlink socket: %s
 Cannot connect to system DBUS: %s
 Cannot create DBUS message: No enough memory.
 Cannot create file for %ss for new format on %s: %s
 Cannot create new quotafile %s: %s
 Cannot create pipe: %s
 Cannot create set for sigaction(): %s
 Cannot create socket: %s
 Cannot create temporary file: %s
 Cannot delete %s quota on %s - switch quota accounting off first
 Cannot delete %s quota on %s - switch quota enforcement and accounting off first
 Cannot detect quota format for journalled quota on %s
 Cannot duplicate descriptor of file to edit: %s
 Cannot duplicate descriptor of file to write to: %s
 Cannot duplicate descriptor of temp file: %s
 Cannot duplicate descriptor: %s
 Cannot exec %s
 Cannot execute '%s': %s
 Cannot find a device with %s.
Skipping...
 Cannot find a filesystem mountpoint for directory %s
 Cannot find any quota file to work on.
 Cannot find checked quota file for %ss on %s!
 Cannot find filesystem to check or filesystem not mounted with quota option.
 Cannot find mountpoint for device %s
 Cannot find quota file on %s [%s] to turn quotas on/off.
 Cannot find quota option on filesystem %s with quotas!
 Cannot finish IO on new quotafile: %s
 Cannot fork: %s
 Cannot gather quota data. Tree root node corrupted.
 Cannot get device name for %s
 Cannot get exact used space... Results might be inaccurate.
 Cannot get host name: %s
 Cannot get info for %s quota file from kernel on %s: %s
 Cannot get name for uid/gid %u.
 Cannot get name of new quotafile.
 Cannot get name of old quotafile on %s.
 Cannot get quota for %s %d from kernel on %s: %s
 Cannot get quota information for user %s
 Cannot get quota information for user %s.
 Cannot get quotafile name for %s
 Cannot get system info: %s
 Cannot guess format from filename on %s. Please specify format on commandline.
 Cannot initialize IO on new quotafile: %s
 Cannot initialize IO on xfs/gfs2 quotafile: %s
 Cannot initialize mountpoint scan.
 Cannot initialize quota on %s: %s
 Cannot join quota multicast group: %s
 Cannot open %s: %s
 Cannot open %s: %s
Will use device names.
 Cannot open any file with mount points.
 Cannot open file %s: %s
 Cannot open file with group administrators: %s
 Cannot open new quota file %s: %s
 Cannot open old format file for %ss on %s
 Cannot open old quota file on %s: %s
 Cannot open quotafile %s: %s
 Cannot parse input line %d.
 Cannot parse line %d in quotatab (missing ':')
 Cannot parse time at CC_BEFORE variable (line %d).
 Cannot read block %u: %s
 Cannot read entry for id %u from quotafile %s: %s
 Cannot read first entry from quotafile %s: %s
 Cannot read header from quotafile %s: %s
 Cannot read header of old quotafile.
 Cannot read individual grace times from file.
 Cannot read info from quota file %s: %s
 Cannot read information about old quotafile.
 Cannot read quota structure for id %u: %s
 Cannot read quotas from file.
 Cannot read stat file %s: %s
 Cannot register callback for netlink messages: %s
 Cannot remount filesystem %s read-write. cannot write new quota files.
 Cannot remount filesystem mounted on %s read-only so counted values might not be right.
Please stop all programs writing to filesystem or use -m flag to force checking.
 Cannot remount filesystem mounted on %s read-only. Counted values might not be right.
 Cannot rename new quotafile %s to name %s: %s
 Cannot rename old quotafile %s to %s: %s
 Cannot reopen temp file: %s
 Cannot reopen! Cannot reset signal handler: %s
 Cannot resolve mountpoint path %s: %s
 Cannot resolve path %s: %s
 Cannot resolve quota netlink name: %s
 Cannot set both individual and global grace time.
 Cannot set grace times over RPC protocol.
 Cannot set info for %s quota file from kernel on %s: %s
 Cannot set quota for %s %d from kernel on %s: %s
 Cannot set signal handler: %s
 Cannot set socket options: %s
 Cannot stat device %s (maybe typo in quotatab)
 Cannot stat directory %s: %s
 Cannot stat mountpoint %s: %s
 Cannot stat quota file %s: %s
 Cannot stat() a mountpoint with %s: %s
Skipping...
 Cannot stat() given mountpoint %s: %s
Skipping...
 Cannot stat() mounted device %s: %s
 Cannot stat() mountpoint %s: %s
 Cannot statfs() %s: %s
 Cannot switch off %s quota accounting on %s when enforcement is on
 Cannot sync quotas on device %s: %s
 Cannot turn %s quotas off on %s: %s
Kernel won't know about changes quotacheck did.
 Cannot turn %s quotas on on %s: %s
Kernel won't know about changes quotacheck did.
 Cannot turn on/off quotas via RPC.
 Cannot wait for mailer: %s
 Cannot write block (%u): %s
 Cannot write grace times to file.
 Cannot write individual grace times to file.
 Cannot write quota (id %u): %s
 Cannot write quota for %u on %s: %s
 Cannot write quotas to file.
 Checked %d directories and %d files
 Checking quotafile headers...
 Checking quotafile info...
 Compiled with:%s
 Continue checking assuming version from command line? Corrupted blocks:  Corrupted number of used entries (%u) Could not get values for %s.
 Could not open PID file '%s': %s
 Creation of %s quota format is not supported.
 Data dumped.
 Denied access to host %s
 Detected quota format %s
 Device (%s) filesystem is mounted on unsupported device type. Skipping.
 Disabling %s quota accounting on %s
 Disabling %s quota enforcement %son %s
 Disk quotas for %s %s (%cid %d):
 Disk quotas for %s %s (%cid %u): %s
 Do not know how to buffer format %d
 Dumping gathered data for %ss.
 Duplicated entries. EXT2_IOC_GETFLAGS failed: %s
 Enable XFS %s quota accounting during mount
 Enabling %s quota enforcement on %s
 Enabling %s quota on root filesystem (reboot to take effect)
 Enforcing %s quota already on %s
 Entry for id %u is truncated.
 Error Error checking device name: %s
 Error in config file (line %d), ignoring
 Error parsing netlink message.
 Error while editing grace times.
 Error while editing quotas.
 Error while getting old quota statistics from kernel: %s
 Error while getting quota statistics from kernel: %s
 Error while opening old quota file %s: %s
 Error while releasing file on %s
 Error while searching for old quota file %s: %s
 Error while syncing quotas on %s: %s
 Error with %s.
 Exitting.
 Failed to delete quota: %s
 Failed to find tty of user %llu to report warning to.
 Failed to open tty %s of user %llu to report warning.
 Failed to parse grace times file.
 Failed to read or parse quota netlink message: %s
 Failed to remove IMMUTABLE flag from quota file %s: %s
 Failed to write message to dbus: No enough memory.
 Failed to write quota message for user %llu to %s: %s
 File info done.
 File limit reached on Filesystem Filesystem           used    soft    hard  grace    used  soft  hard  grace
 Filesystem remounted RW.
 Filesystem remounted read-only
 First entry loaded.
 Found an invalid UUID: %s
 Found i_num %ld, blocks %ld
 Found more structures for ID %u. Using values: BHARD: %lld BSOFT: %lld IHARD: %lld ISOFT: %lld
 Found more structures for ID %u. Values: BHARD: %lld/%lld BSOFT: %lld/%lld IHARD: %lld/%lld ISOFT: %lld/%lld
 Gid set allocation (%d): %s
 Going to check %s quota file of %s
 Grace period before enforcing soft limits for %ss:
 Group Group and user quotas cannot be used together.
 Headers checked.
 Headers of file %s checked. Going to load data...
 Hi,

We noticed that the group %s you are member of violates the quotasystem
used on this system. We have found the following violations:

 Hi,

We noticed that you are in violation with the quotasystem
used on this system. We have found the following violations:

 High uid detected.
 ID %u has more structures. User intervention needed (use -i for interactive mode or -n for automatic answer).
 Illegal free block reference to block %u Illegal port number: %s
 Illegal reference (%u >= %u) in %s quota file on %s. Quota file is probably corrupted.
Please run quotacheck(8) and try again.
 In block grace period on In file grace period on Incorrect format string for variable %s.
Unrecognized expression %%%c.
 Info Inode: #%llu (%llu blocks, %u extents)
 Inode: none
 Inserting already present quota entry (block %u).
 Invalid argument "%s"
 Kernel quota version: %u.%u.%u
 Kernel quota version: old
 LDAP library version >= 2.3 detected. Please use LDAP_URI instead of hostname and port.
Generated URI %s
 Leaving %s
 Line %d too long.
 Line %d too long. Truncating.
 Loading first quota entry with grace times.
 Maximum %u dquots (currently %u incore, %u on freelist)
 Maybe create new quota files with quotacheck(8)?
 Metadata init_io called when kernel does not support generic quota interface!
 Metadata init_io called when kernel support is not enabled.
 Mountpoint %s is not a directory?!
 Mountpoint (or device) %s not found or has no quota enabled.
 Mountpoint not specified.
 Name must be quotaon or quotaoff not %s
 Name of quota file too long. Contact %s.
 No correct mountpoint specified.
 No filesystem specified.
 No filesystems with quota detected.
 No possible destination for messages. Nothing to do.
 Not all specified mountpoints are using quota.
 Not enough memory.
 Not found any corrupted blocks. Congratulations.
 Not setting block grace time on %s because softlimit is not exceeded.
 Not setting inode grace time on %s because softlimit is not exceeded.
 Number of allocated dquots: %ld
 Number of dquot cache hits: %ld
 Number of dquot drops: %ld
 Number of dquot lookups: %ld
 Number of dquot reads: %ld
 Number of dquot writes: %ld
 Number of free dquots: %ld
 Number of in use dquot entries (user/group): %ld
 Number of quotafile syncs: %ld
 OFF ON Old file found removed during check!
 Old file not found.
 Only RPC quota format is allowed on NFS filesystem.
 Only XFS quota format is allowed on XFS filesystem.
 Over block quota on Over file quota on Parse error at line %d. Cannot find administrators name.
 Parse error at line %d. Cannot find end of group name.
 Parse error at line %d. Trailing characters after administrators name.
 Possible error in config file (line %d), ignoring
 Prototype name does not make sense when editing grace period or times.
 Prototype user has no sense when editing grace times.
 Quota enforcement already disabled for %s on %s
 Quota file %s has IMMUTABLE flag set. Clearing.
 Quota file %s has corrupted headers. You have to specify quota format on command line.
 Quota file format version %d does not match the one specified on command line (%d). Quota file header may be corrupted.
 Quota file not found or has wrong format.
 Quota file on %s [%s] does not exist or has wrong format.
 Quota for %ss is enabled on mountpoint %s so quotacheck might damage the file.
 Quota for %ss is enabled on mountpoint %s so quotacheck might damage the file.
Please turn quotas off or use -f to force checking.
 Quota for id %u referenced but not present.
 Quota format not supported in kernel.
 Quota not supported by the filesystem.
 Quota structure for %s owning quota file not present! Something is really wrong...
 Quota structure has offset to other block (%u) than it should (%u).
 Quota utilities version %s.
 Quota write failed (id %u): %s
 RPC quota format not compiled.
 RPC quota format specified for non-NFS filesystem.
 Reference to illegal block %u Renaming new files to proper names.
 Renaming new quotafile
 Renaming old quotafile to %s~
 Repquota cannot report through RPC calls.
 Required format %s not supported by kernel.
 Scanning %s [%s]  Scanning stored directories from directory stack
 Setting grace period on %s is not supported.
 Setting grace times and other flags to default values.
Assuming number of blocks is %u.
 Should I continue? Size of file: %lu
Blocks: %u Free block: %u Block with free entry: %u Flags: %x
 Skipping %s [%s]
 Skipping line.
 Something weird happened while scanning. Error %d
 Space Specified both -n and -t but only one of them can be used.
 Specified path %s is not directory nor device.
 Statistics:
Total blocks: %u
Data blocks: %u
Entries: %u
Used average: %f
 Substracted %lu bytes.
 Substracting space used by old %s quota file.
 The running kernel does not support XFS
 Time units may be: days, hours, minutes, or seconds
 Times to enforce softlimit for %s %s (%cid %d):
 Too many parameters to editor.
 Trying to set quota limits out of range supported by quota format on %s.
 Trying to set quota usage out of range supported by quota format on %s.
 Trying to write info to readonly quotafile on %s
 Trying to write info to readonly quotafile on %s.
 Trying to write quota to readonly quotafile on %s
 Unable to resolve name '%s' on line %d.
 Undefined program name.
 Unexpected XFS quota state sought on %s
 Unknown action should be performed.
 Unknown format of kernel netlink message!
Maybe your quota tools are too old?
 Unknown option '%c'.
 Unknown quota format: %s
Supported formats are:
  vfsold - original quota format
  vfsv0 - standard quota format
  vfsv1 - quota format with 64-bit limits
  rpc - use RPC calls
  xfs - XFS quota format
 Unterminated last line, ignoring
 Usage:
	edquota %1$s[-u] [-F formatname] [-p username] [-f filesystem] username ...
	edquota %1$s-g [-F formatname] [-p groupname] [-f filesystem] groupname ...
	edquota [-u|g] [-F formatname] [-f filesystem] -t
	edquota [-u|g] [-F formatname] [-f filesystem] -T username|groupname ...
 Usage:
  setquota [-u|-g] %1$s[-F quotaformat] <user|group>
	<block-softlimit> <block-hardlimit> <inode-softlimit> <inode-hardlimit> -a|<filesystem>...
  setquota [-u|-g] %1$s[-F quotaformat] <-p protouser|protogroup> <user|group> -a|<filesystem>...
  setquota [-u|-g] %1$s[-F quotaformat] -b [-c] -a|<filesystem>...
  setquota [-u|-g] [-F quotaformat] -t <blockgrace> <inodegrace> -a|<filesystem>...
  setquota [-u|-g] [-F quotaformat] <user|group> -T <blockgrace> <inodegrace> -a|<filesystem>...

-u, --user                 set limits for user
-g, --group                set limits for group
-a, --all                  set limits for all filesystems
    --always-resolve       always try to resolve name, even if is
                           composed only of digits
-F, --format=formatname    operate on specific quota format
-p, --prototype=protoname  copy limits from user/group
-b, --batch                read limits from standard input
-c, --continue-batch       continue in input processing in case of an error
 Usage: %s [-acfugvViTq] [filesystem...]
 Usage: %s [options]
Options are:
 -h --help             shows this text
 -V --version          shows version information
 -F --foreground       starts the quota service in foreground
 -I --autofs           do not ignore mountpoints mounted by automounter
 -p --port <port>      listen on given port
 -s --no-setquota      disables remote calls to setquota (default)
 -S --setquota         enables remote calls to setquota
 -x --xtab <path>      set an alternative file with NFSD export table
 Usage: %s [options]
Options are:
 -h --help             shows this text
 -V --version          shows version information
 -F --foreground       starts the quota service in foreground
 -I --autofs           do not ignore mountpoints mounted by automounter
 -p --port <port>      listen on given port
 -x --xtab <path>      set an alternative file with NFSD export table
 Usage: %s [options]
Options are:
 -h --help         shows this text
 -V --version      shows version information
 -C --no-console   do not try to write messages to console
 -b --print-below  write to console also information about getting below hard/soft limits
 -D --no-dbus      do not try to write messages to DBUS
 -F --foreground   run daemon in foreground
 Usage: quota [-guqvswim] [-l | [-Q | -A]] [-F quotaformat]
 User Utility for checking and repairing quota files.
%s [-gucbfinvdmMR] [-F <quota-format>] filesystem|-a

-u, --user                check user files
-g, --group               check group files
-c, --create-files        create new quota files
-b, --backup              create backups of old quota files
-f, --force               force check even if quotas are enabled
-i, --interactive         interactive mode
-n, --use-first-dquot     use the first copy of duplicated structure
-v, --verbose             print more information
-d, --debug               print even more messages
-m, --no-remount          do not remount filesystem read-only
-M, --try-remount         try remounting filesystem read-only,
                          continue even if it fails
-R, --exclude-root        exclude root when checking all filesystems
-F, --format=formatname   check quota files of specific format
-a, --all                 check all filesystems
-h, --help                display this message and exit
-V, --version             display version information and exit

 Utility for converting quota files.
Usage:
	%s [options] mountpoint

-u, --user                          convert user quota file
-g, --group                         convert group quota file
-e, --convert-endian                convert quota file to correct endianity
-f, --convert-format oldfmt,newfmt  convert from old to VFSv0 quota format
-h, --help                          show this help text and exit
-V, --version                       output version information and exit

 WARNING -  Quotafile %s was probably truncated. Cannot save quota settings...
 WARNING - %s: cannot change current block allocation
 WARNING - %s: cannot change current inode allocation
 WARNING - Quota file %s has corrupted headers
 WARNING - Quota file %s was probably truncated. Cannot save quota settings...
 WARNING - Quota file info was corrupted.
 WARNING - Quotafile %s was probably truncated. Cannot save quota settings...
 WARNING - Some data might be changed due to corruption.
 Warning Warning: Cannot open export table %s: %s
Using '/' as a pseudofilesystem root.
 Warning: Cannot set EXT2 flags on %s: %s
 Warning: Ignoring -%c when filesystem list specified.
 Warning: Mailer exitted abnormally.
 Warning: No quota format detected in the kernel.
 XFS Quota Manager dquot statistics
 XFS quota allowed only on XFS filesystem.
 XFS_IOC_FSBULKSTAT ioctl failed: %s
 You have to specify action to perform.
 You have to specify source and target format of conversion.
 Your kernel probably supports journaled quota but you are not using it. Consider switching to journaled quota to avoid running quotacheck after an unclean shutdown.
 Your quota file is stored in wrong endianity. Please use convertquota(8) to convert it.
 and accounting  bad format:
%s
 block limit reached block quota exceeded block quota exceeded too long blocks cannot create udp service.
 cannot find %s on %s [%s]
 cannot open %s: %s
 cannot write times for %s. Maybe kernel does not support such operation?
 copy_user_quota_limits: Failed to get userquota for uid %ld : %s
 copy_user_quota_limits: Failed to set userquota for uid %ld : %s
 day days done
 error (%d) while opening %s
 error (%d) while opening inode scan
 error (%d) while starting inode scan
 error while getting quota from %s for %s (id %u): %s
 file limit reached file quota exceeded file quota exceeded too long files find_free_dqentry(): Data block full but it shouldn't.
 fsname mismatch
 getgroups(): %s
 got below block limit got below block quota got below file limit got below file quota grace group group %s does not exist.
 host %s attempted to call setquota from port >= 1024
 host %s attempted to call setquota when disabled
 hour hours limit minute minutes none off on popd %s
Entering directory %s
 pushd %s/%s
 quota quotactl on %s [%s]: %s
 quotactl on %s: %s
 quotactl() on %s: %s
 second seconds set root_squash on %s: %s
 space svc_run returned
 unable to free arguments
 unable to register (RQUOTAPROG, EXT_RQUOTAVERS, TCP).
 unable to register (RQUOTAPROG, RQUOTAVERS, TCP).
 undefined unknown quota warning unset user user %s does not exist.
 using %s on %s [%s]: %s
 Project-Id-Version: quota
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-16 18:19+0100
PO-Revision-Date: 2015-01-08 16:25+0000
Last-Translator: Juliano Fischer Naves <julianofischer@gmail.com>
Language-Team: Portuguese (Brazil) <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:15+0000
X-Generator: Launchpad (build 18115)
 	Adicionando %s tamanho %lld ino %d links %d uid %u gid %u
 	quota [-qvswim] [-l | [-Q | -A]] [-F Formato_da_quota] -g Nome_do_grupo ...
 	quota [-qvswim] [-l | [-Q | -A]] [-F formato_quota] -u nome_do_usuário ...
 	quota [-qvswugQm] [-F formato da quota] -f sistema de arquivo ...
 
                        Limites de bloco Limites de arquivo
 
-u, --user  editar os dados do usuário
-g, --group  editar os dados do grupo
 
Pode abrir o diretório %s: %s
 
Por favor, limpe os dados do grupo antes que o período de tolerância acabe.

Basicamente, isto significa que o sistema acha que este grupo está usando mais espaço em disco 
na(s) partição(ões) acima do que o permitido. Se você não apagar arquivos 
e ficar abaixo do limite de cota do grupo antes do período de tolerância acabar, o sistema irá 
impedir você e outros membros do grupo de criar novos arquivos pertencentes ao
grupo.

Para maiores informações, por favor contacte-nos em %s
ou por telefone no %s.
 
Esperamos que você limpe sua conta antes que seu período de tolerância acabe.

Basicamente, isto significa que o sistema acha que você está usando mais espaço em disco 
na(s) partição(ões) acima do que o permitido. Se você não apagar arquivos 
e ficar abaixo do limite de cota antes do seu período de tolerância acabar, o sistema irá 
impedi-lo(a) de criar novos arquivos.

Para maiores informações, por favor contate-nos em %s
ou por telefone no %s.
                         %s delimita os limites do arquivo
     %8llu %8llu %8llu   Sistema de arquivos bloco tolerância inode tolerância
   Sistema de arquivos blocos permitido físico inodes permitido físico
   Sistema de arquivos Bloco período de tolerância Inode período de tolerância
   cache em cliques: %u
   falha no cache: %u
   dquot dups: %u
   requer dquot: %u
   recuperar inat: %u
   recuperação perdida: %u
   recuperação:        %u
   reclamar agito: %u
 #%-7d %-8.8s %-9s usado permitido físico tolerância usado permitido físico tolerância
 %02d:%02d %8llu     %d	%llu	%llu
 %d dias %s %s (%s) %ss:
 %s (%s):
 %s (gid %d): Permissão negada
 %s (gid %d): erro quando tentava getgroups(): %s
 %s (gid %d): alocação da configuração gid (%d): %s
 %s (uid %d): Permissão negada
 %s [%s]: %s voltando quotas para desligado
 voltando quotas em %s [%s]: %s
 %s quota em %s (%s) é %s
 %s sincronia de cota falhou para %s: %s
 %s sincronia de cota falhou: %s
 %s: %s
 %s: %s voltando os quotas para desligado
 %s: %s voltando os quotas para ligado
 %s: %s voltando o root_squash para desligado
 %s: %s voltando o root_squash para ligado
 %s: Cota não pode ser ligado em sistema de arquivo NFS
 %s: apagando o bloco quota %s
 %u dias %u horas %u minutos %u segundos *** Informar sobre cotas %s no dispositivo %s
 *** Status de %s para o dispositivo quotas %s
 -F, --format=formatname       edita o formato especifico do quotas
-p, --prototype=name          copia os dados para um protótipo usuário/grupo
    --always-resolve          sempre tenta resolver o nome, se ele for
                              composto somente com dígitos
-f, --filesystem=filesystem   editar somente os dados de sistema de arquivo especifico
-t, --edit-period             edita o período grace
-T, --edit-times              edita o tempo do usuário/grupo do grace
-h, --help                    mostra o texto de ajuda e saí
-V, --version                 mostra informação da versão e saí

 -r, --remote edita quota remota (via RPC)
-m, --no-mixed-pathnames remove barras iniciais de pontos de montagem NFSv4
 -r, --remote selecionar cota remota (via RPC)
-m, --no-mixed-pathnames remover barras iniciais de pontos de montabem NFSv4
 -t, --edit-period editar período de tolerância
-T, --edit-times editar tempos de tolerância para usuário/grupo
-h, --help mostrar este texto de ajuda e sair
-V, --version mostrar informação de versão e sair

 0 segundos Contabilizado [no disco]: %s; Filtro [no disco]: %s
 Contabilizado: %s; Filtros: %s
 Adicionado a estrutura dquot do tipo %s para %d
 Não foi possível encontrar administrador para um grupo %s. Cancelando mensagem.
 %d bytes de memória alocado(s)
%d bytes livre(s)
%d bytes perdido(s)
 Já está contabilizando cota %s em %s
 Como você deseja... Cancelando a verificação deste arquivos.
 Arquivo mágico ou versão péssimo (provavelmente nenhum arquivo quota com endianity, estão péssimos).
 Formato inválido:
%s
 Números ou argumentos péssimo.
 Unidade de tempo incorreta. Unidades são 'segundo', 'minuto', 'hora' e 'dia'.
 Modo batch e usuário protótipo não podem ser utilizados juntos.
 Modo batch não pode ser usado para selecionar tempos de tolerância.
 Bloco Bloco %u na árvore com duas referência Bloco %u está truncado.
 Bloco %u:  Período de tolerância de blocos: %s, Período de tolerância para Inode: %s
 Limite do bloco alcançando em Bugs para %s
 Bugs para: %s
 Impossível acessar o arquivo xtab informado %s: %s
 Não foi possível alocar o novo bloco quota (sem espaço no disco).
 Não foi possível associar ao endereço fornecido: %s
 Não foi possível alterar os tempos de carência sobre o protocolo RPC.
 Não foi possível alterar o proprietário do arquivo temporário: %s
 Não é possível alterar a permissão de %s: %s
 Não foi possível alterar o estado da cota GFS2.
 Não foi possível alterar o estado do quota XFS. Este não foi compilado no kernel.
 Não foi possível enviar o dquot para a id %u: %s
 Não é possível conetar ao soquete netlink: %s
 Não foi possível conectar ao sistema DBUS: %s
 Não foi possível criar mensagem DBUS: Memória insuficiente.
 Não foi possível criar um arquivo para %ss para o novo formato em %s: %s
 Não foi possível criar o novo arquivo quota %s: %s
 Não foi possível criar o Pipe: %s
 Não é possível criar definição para sigaction(): %s
 Não foi possível criar o socket: %s
 Não foi possível criar arquivo temporário: %s
 Não foi possível deletar o quota %s em %s - alterar a contabilização do quota ao primeiro desligamento
 Não foi possível apagar cota %s em %s - mude aplicação e contabilização de cota primeiro
 Não foi possível detectar o quota com formato para quota journalled em %s
 Impossível duplicar descritor do arquivo para ser editado: %s
 Impossível duplicar descritor do arquivo pra gravar em: %s
 Impossível duplicar descritor do arquivo temporário: %s
 Não foi possível duplicar o descritor: %s
 Não foi possível executar %s
 Não foi possível executar '%s': %s
 Impossível localizar um dispositivo com %s.
Ignorando...
 Impossível localizar um ponto de montagem do sistema de arquivo para o diretório %s
 Não foi possível encontrar um arquivo de cota para processar.
 Não foi possível localizar o arquivo quota selecionado para %ss em %s!
 Não foi possível localizar o arquivo de sistema para verificar ou nenhum arquivo de sistema foi montado com a opção quota.
 Impossível localizar ponto de montagem para o dispositivo %s
 Não foi possível localizar o arquivo quota ligado %s [%s] para voltar o quotas ligado/desligado.
 Não foi possível localizar o quota opção no sistema de arquivo %s com quotas!
 Não foi possível finalizar o IO no novo arquivo quota: %s
 Não é possível remover: %s
 Não foi possível reunir os dados. Ligação com a raiz do diretório corrompido.
 Impossível obter nome do dispositivo para %s
 Não foi possível obter o uso de espaço exato... Resultados pode ser que esteja com erro.
 Não foi possível obter o nome do host: %s
 Não foi possível obter a informação do arquivo quota %s sobre este kernel %s: %s
 Não foi possível obter o nome para uid/gid %u.
 Não foi possível obter o nome do novo arquivo quota.
 Não foi possível obter o nome do antigo arquivo quota em %s.
 Não foi possível obter o quota para %s %d sobre este kernel %s: %s
 Não foi possível obter informação de cota para o usuário %s
 Não foi possível obter informação de cota para o usuário %s.
 Não foi possível obter no nome do arquivo quota para %s
 Impossível pegar informação do sistema: %s
 Não foi possível informar o formato do nome de arquivo em %s. Por favor, especifique o formato através da linha de comando.
 Não foi possível inicializar o IO no novo arquivo quota: %s
 Não é possível inicializar ES no arquivo xfs/gfs2: %s
 Não foi possível inicializar a analise do ponto de montagem.
 Não foi possível inicializar o quota em  %s: %s
 Não é possível entrar no grupo de multicast do quota: %s
 Não foi possível abrir %s: %s
 Não foi possível abrir %s: %s
Nomes de dispositivos serão usados.
 Não foi possível abrir um arquivo com pontos de montagem.
 Não foi possível abrir o arquivo %s: %s
 Não foi possível abrir arquivo com administrador de grupo: %s
 Não foi possível abrir um novo arquivo quota %s: %s
 Não foi possível abrir o formato do arquivo antigo para %ss em %s
 Não foi possível abrir arquivo antigo de quota em %s: %s
 Não é possível abrir o arquivo de quota %s: %s
 Impossível processar linha de entrada %d.
 Não foi possível analisar linha %d em quotatab (':' faltando)
 Não foi possível analisar tempo na variável CC_BEFORE (linha %d).
 Não foi possível ler o bloco %u: %s
 Não foi possível ler a entrada para o id %u deste arquivo quota %s: %s
 Não foi possível ler a primeira entrada deste arquivo quota %s: %s
 Não foi possível ler o cabeçalho deste arquivo quota %s: %s
 Não foi possível ler o cabeçalho do arquivo de cota antigo.
 Não foi possível ler o período de tolerância individual do arquivo.
 Não foi possível ler as informações deste arquivo quota %s: %s
 Não foi possível ler as informações sobre o arquivo quota antigo.
 Não foi possível ler a estrutura do quota para a identificação id %u: %s
 Não foi possível escrever o quotas do arquivo file.
 Impossível abrir arquivo stat %s: %s
 Não foi possível registrar o retorno no chamado para mensagens do netlink: %s
 Não foi possível remontar o sistema de arquivo %s leitura e escrita. Não foi possível escrever nos novos arquivos quota.
 Não foi possível remontar o sistema de arquivo montado em %s somente leitura portanto, os valores contados podem não estar certo.
Por favor, pare todos os programas que estão sendo gravados para o sistema de arquivo ou pwriting para o sistema de arquivo ou use a sinalização -m para forçar uma verificação.
 Não foi possível remontar o sistema de arquivos montado em %s somente-leitura. Valores contabilizados podem não estar corretos.
 Não foi possível renomear o novo arquivo quota %s para o nome %s: %s
 Não é possível renomear antigo arquivo de quota %s para %s: %s
 Impossível reabrir arquivo temporário: %s
 Não é possível reabrir! Não foi possível redefinir o manipulador de sinal: %s
 Impossível resolver o caminho do ponto de montagem: %s: %s
 Impossível resolver caminho %s: %s
 Não foi possível determinar o nome do netlink do cota: %s
 Não é possível selecionar ao mesmo tempo tempos globais e individuais de tolerância.
 Não é possível ajustar os tempos de carência sobre o protocolo RPC.
 Não foi possível as informações da configuração do arquivo quota %s sobre este kernel %s: %s
 Não foi possível obter as configurações do quota para %s %d sobre este kernel %s: %s
 Não foi possível definir o manipulador de sinal: %s
 Não foi possível selecionar opções de socket: %s
 Não foi possível analisar estado do dispositivo %s (talvez um erro de digitação em quotatab)
 Não foi possível o status do diretório %s: %s
 Não foi possível o status do ponto de montagem %s: %s
 Não foi possível verificar o estado do arquivo de cota %s: %s
 Não foi possível aplicar stat() ao ponto de montagem com %s: %s
Pulando...
 Não foi possível aplicar stat() ao ponto de montagem fornecido %s: %s
Pulando...
 Não foi possível aplicar stat() ao dispositivo montado %s: %s
 Não foi possível aplicar stat() ao ponto de montagem %s: %s
 Não foi possível aplicar statfs() %s: %s
 Não é possível desligar contabilização de cota %s em %s enquanto a cota estiver sendo aplicada
 Não foi possível sincronizar o dispositivo quotas em %s: %s
 Não foi possível  desativar %s quotas desligado em %s: %s
Kernel não sabe sobre as alterações feitas pela verificação do quota.
 Não foi possível desativar %s quotas ligado em %s: %s
Kernel não sabe sobre as alterações feitas pela verificação do quota.
 Não foi possível voltar ligar/desligar o quotas via RPC.
 Não foi possível esperar por programa de correio: %s
 Não foi possível gravar os blocos (%u): %s
 Não foi possível escrever o período de tolerância no arquivo.
 Não foi possível escrever período de tolerância individual no arquivo.
 Não foi possível gravar a quota (id %u): %s
 Não foi possível gravar no quota para %u em %s: %s
 Não foi possível escrever as cotas no arquivo.
 Verificar diretórios %d e os arquivos %d
 Verificar os cabeçalhos do arquivo quota...
 Verificando informação do arquivo quota...
 Compilado com:%s
 Continuar a verificar de acordo com a versão da linha de comando? Blocos corrompidos:  Corrompido o número de entradas do usuário (%u) Não foi possível obter valores para %s.
 Não foi possível abrir arquivo PID '%s': %s
 Criando o formato quota %s este não é suportado.
 Despejando os dados.
 Acesso negado ao host %s
 Formato %s de quota detectado
 Dispositivo (%s) sistema de arquivo está montado num tipo de dispositivo não suportado. Ignorando.
 Desabilitar a contabilização do quota %s sobre %s
 Desabilitando aplicação de %s cota %s em %s
 Quotas de disco para %s %s (%cid %d):
 Cotas de disco para %s %s (%cid %u): %s
 Não sei como colocar no buffer o formato %d
 Despejando o conjunto de dados para %ss.
 Entradas duplicadas. EXT2_IOC_GETFLAGS falhou: %s
 Habilitar contabilização de cota XFS %s durante a montagem
 Habilitar aplicação de cota %s em %s
 Habilitando %s quota no sistema de arquivo raíz (reinicie para que tenha efeito)
 Encerramento do quota %s agora sobre o %s
 Entrada para o id %u está incompleto.
 Erro Erro ao verificar nome do dispositivo: %s
 Erro no arquivo de configuração (linha %d), ignorando
 Erro analisando mensagem netlink.
 Erro ao editar tempos de tolerância.
 Erro ao editar cotas.
 Erro ao obter estatísticas de quotas antigas do kernel: %s
 Erro quando obtinha estatísticas da quota no kernel: %s
 Erro na abertura do arquivo antigo de quota %s: %s
 Erro quando liberava arquivo em %s
 Erro no momento da pesquisa do antigo arquivo quota %s: %s
 Erro no momento da sincronização dos quotas em %s: %s
 Erro com %s.
 Saindo.
 Falha ao apagar o quota: %s
 Falha ao procurar tty do usuário %llu para informar alerta.
 Falha ao abrir tty %s do usuário %llu para informar alerta.
 Falha ao analisar o período de tolerância do arquivo.
 Falha ao ler ou parsear mensagem do netlink de cota: %s
 Falha ao remover indicador IMMUTABLE do arquivo de cota %s: %s
 Falha ao escrever mensagem para dbus: Memória insuficiente.
 Falha ao escrever mensagem de cota para o usuário %llu em %s: %s
 Arquivo informado terminado.
 Limite do arquivo alcançado em Sistema de arquivos Sistema de arquivos usado permitido físico tolerância usado permitido físico tolerância
 Arquivo de sistema remontado RW.
 Sistema de arquivos remontado somente leitura
 Primeira entrada carregada.
 Encontrado um UUID inválido: %s
 Encontrar i_num %ld, blocos %ld
 Mais estruturas encontradas para ID %u. Usando valores: BHARD: %lld BSOFT: %lld IHARD: %lld ISOFT: %lld
 Mais estruturas encontradas para ID %u. Valores: BHARD: %lld/%lld BSOFT: %lld/%lld IHARD: %lld/%lld ISOFT: %lld/%lld
 Alocação do conjunto de gid (%d): %s
 Começando a verificação para %s do arquivo quota de %s
 Período de tolerância antes de aplicar limites permitidos para %ss:
 Grupo Cotas de grupo e de usuário não podem ser usadas juntas.
 Verificar cabeçalho.
 Cabeçalho do arquivo %s verificado. Iniciando o carregamento de dados...
 Olá,

Foi determinado que o grupo %s do qual você é membro está violando o sistema de cotas
usado neste sistema. As seguintes violações foram encontradas:

 Olá,

Foi determinado que você está violando o sistema de cota
usado neste sistema. As seguintes violações foram encontradas:

 Detectado uid alta
 ID %u existe mais estruturas. Necessário a intervenção do usuário (use -i para o modo interativo ou -n para resposta automática).
 referência de bloco livre ilegal para o bloco %u Número de porta ilegal: %s
 Referência ilegal  (%u >= %u) no arquivo de cota %s em %s. O arquivo de cota provavelmente está corrompido.
Por favor, execute o quotacheck(8) e tente novamente.
 No bloco grace período em No arquivo grace período em Sequência de caracteres de formato inválida para a variável %s.
Expressão não reconhecida %%%c.
 Informações Inode: #%llu (%llu bloqueados, %u extensões)
 Inode: nenhum
 Inserir uma entrada atual agora no quota (block %u).
 Argumento inválido "%s"
 Versão da quota no Kernel: %u.%u.%u
 Versão da quota no Kernel: antigo
 Versão de biblioteca LDAP >= 2.3 detectada. Por favor use LDAP_URI no lugar de hostname e porta.
URI gerada %s
 Abandonando %s
 Linha %d muito longa.
 Linha %d muito extensa. Truncando-a.
 Carregando primeira entrada de cotas com tempos de tolerância.
 Máximo %u dquots (atualmente %u INCORE %u em freelist)
 Considerar criar novo arquivo de cota com quotacheck(8)?
 Metadata init_io chamada quando o kernel não suportar interface genérica de cota!
 Metadata init_io chamada quando suporte para o kernel não estiver disponível.
 Ponto de montagem %s não está em um diretório?!
 Ponto de montagem (ou dispositivo) %s não foi encontrado ou não tem cotas habilitadas.
 Ponto de montagem não especificado.
 O nome deve ser quotaon ou quotaoff, não %s
 Nome do arquivo de quota muito comprido. Contacte %s.
 Ponto de montagem especificado inválido.
 Nenhum sistema de arquivo especificado.
 Nenhum sistema de arquivo com quota detectado.
 Não há destino possível para as mensagens. Nada a fazer.
 Nem todos os pontos de montagem especificados estão usando quota.
 Memória insuficiente.
 Não foi encontrado nenhum bloco corrompido. Parabéns.
 Não selecionar tempo de tolerância de blocos em %s pois o limite permitido (softlimit) não foi excedido.
 Não selecionar tempo de tolerância de inode em %s pois o limite permitido (softlimit) não foi excedido.
 Número de dquots alocados: %ld
 Número de acessos ao cache dquot: %ld
 Número de quedas dquot: %ld
 Número de buscas dquot: %ld
 Número de leituras dquot: %ld
 Número de escritas dquot: %ld
 Número de dquots livres: %ld
 Número de entradas dquot em uso (usuário/grupo): %ld
 Número de sincronizações de quotafile: %ld
 DESLIGADO LIGADO Remover o arquivo antigo durante a verificação!
 Arquivo antigo não encontrado.
 Somente é permitido o formato quota RPC no sistema de arquivo NFS.
 Somente é permitido o formato quota XFS no sistema de arquivo XFS.
 Sobre o bloco quota em Sobre o arquivo quota em Erro ao analisar linha %d. Não foi possível encontrar nome do administrador.
 Erro ao analisar linha %d. Não foi possível encontrar fim do nome de grupo.
 Erro ao analisar linha %d. Caracteres sobrando ao fim do nome do administrador.
 Possível erro no arquivo de configuração (linha %d), ignorando.
 O nome do protótipo não faz sentido ao editar período ou tempos de tolerância.
 Protótipo do usuário não faz sentido ao editar tempos de tolerância.
 Aplicação de cota já está desabilitada para %s em %s
 Arquivo de cota %s possui indicador IMMUTABLE ligado. Limpando.
 O arquivo de cota %s possui cabeçalhos corrompidos. Você tem que especificar o formato de cota na linha de comando.
 A versão do formato do arquivo de cota %d não corresponde à especificada na linha de comando (%d). O cabeçalho do arquivo de Cota pode estar corrompido.
 Arquivo quota não foi encontrado ou o formato do arquivo esta errado.
 O arquivo de cota em %s [%s] não existe ou está em um formato incorreto.
 Para o quota %ss está habilitado no ponto de montagem %s assim o quotacheck pode danificar o arquivo.
 Para o quota for %ss está habilitado no ponto de montagem %s assim a verificação do quota pode danificar o arquivo.
Por favor, desativa o quotas off ou use -f para forçar uma verificação.
 A identificação quota referente ao %u não é atual.
 Formato da quota não suportado pelo kernel.
 Cota não suportada pelo sistema de arquivos.
 Estrutura de cota para arquivo de cota pertencente a %s não está presente! Alguma coisa está muito errada...
 Estrutura de cota foi deslocada para outro bloco (%u) diferente do esperado (%u).
 Utilitários de quota versão:%s.
 Erro ao gravar o quota (id %u): %s
 formato quota RPC não foi compilado.
 O formato de cota RPC foi especificado para um sistema não-NFS.
 Referência ilegal para o bloco %u Renomeando os novos arquivos para os nomes apropriados.
 Renomeando o novo arquivo quota
 Renomeando antigo arquivo de quota para %s~
 Repquota não pode reportar através de chamadas RPC.
 Formato requerido %s não é suportado pelo kernel.
 Scaneando %s [%s]  Analisando o conteúdo armazenado nos diretórios desta pilha de diretório
 Definir período de carência em %s não é suportado.
 Configurar a hora grace e outras sinalizações para os valores padrão.
O número do bloco assumindo é %u.
 Devo continuar? Tamanho do arquivo: %lu
Blocos: %u Blocos livres: %u Blocos com entrada livre: %u Sinalizações: %x
 Ignorando %s [%s]
 Pulando linha.
 Problema detectado durante analize. Erro %d
 Espaço Ambos -n e -t foram especificados mas apenas um deles pode ser usado.
 Caminho especificado %s não é um diretório ou dispositivo.
 Estatísticas:
Total de blocos: %u
Dados dos blocos: %u
Entradas: %u
Freqüência de uso: %f
 Subtraídos %lu bytes.
 Subtraindo espaço usado pelo arquivo de quota antigo %s
 O kernel em execução não possui suporte a XFS.
 Unidades de tempo podem ser: dias, horas, minutos ou segundos
 Tempos para aplicar limites permitidos para %s %s (%cid %d):
 Excesso de parâmetros no editor.
 Tentando definir limites de cota fora do intervalo suportado pelo formato de cota em %s.
 Tentando definir o uso de quota além do que é suportado, pelo formato em %s.
 Tentativa de gravar as informações para um arquivo quota que é somente leitura em %s
 Tentativa para escrever as informações somente para leitura do arquivo quota em %s.
 Tentativa de escrever o quota em um arquivo quota que é somente leitura em %s
 Não foi possível determinar nome '%s' na linha %d.
 Nome de programa indefinido.
 Estado de cota XFS inesperado sendo procurado em %s
 A ação que deve ser executada é desconhecida.
 Formato de mensagem do kernel netlink desconhecido!
Talvez suas ferramentas de quota sejam antigas?
 Opção desconhecida '%c'.
 Formato de cota desconhecido: %s
Os formatos suportados são:
  vfsold - formato de cota original
  vfsv0 - formato de cota padrão
  vfsv1 - formato de cota com limites de 64-bits
  rpc - usar chamadas RPC
  xfs - formato de cota XFS
 Última linha não determinada, ignorando.
 Usage:
	edquota %1$s[-u] [-F NomeDoFormato] [-p NomeDoUsuario] [-f SistemaDeArquivo] NomeDoUsuario ...
	edquota %1$s-g [-F NomeDoFormato] [-p NomeDoGrupo] [-f SistemaDeArquivo] NomeDoGrupo ...
	edquota [-u|g] [-F NomeDoFormato] [-f SistemaDeArquivo] -t
	edquota [-u|g] [-F NomeDoFormato] [-f SistemaDeArquivo] -T NomeDoUsuario|NomeDoGrupo ...
 Uso:
  setquota [-u|-g] %1$s[-F quotaformat] <usuário|grupo>
	<block-softlimit> <block-hardlimit> <inode-softlimit> <inode-hardlimit> -a|<filesystem>...
  setquota [-u|-g] %1$s[-F quotaformat] <-p protouser|protogroup> <user|group> -a|<filesystem>...
  setquota [-u|-g] %1$s[-F quotaformat] -b [-c] -a|<filesystem>...
  setquota [-u|-g] [-F quotaformat] -t <blockgrace> <inodegrace> -a|<filesystem>...
  setquota [-u|-g] [-F quotaformat] <user|group> -T <blockgrace> <inodegrace> -a|<filesystem>...

-u, --user seleciona limites para usuário
-g, --group seleciona limites para grupo
-a, --all seleciona limites para todos os sistemas de arquivo
    --always-resolve sempre tentar determinar nome, mesmo se for
                           composed only of digits
-F, --format=formatname operar num formato específico de cota
-p, --prototype=protoname copiar limites do usuário/grupo
-b, --batch ler limites do input padrão
-c, --continue-batch continuar processando input em caso de erro
 Uso: %s [-acfugvViTq] [sistema de arquivo...]
 Uso: %s [opções]
Opções:
 -h --help mostrar este texto
 -V --version mostrar informação de versão
 -F --foreground iniciar o serviço de cota no primeiro plano
 -I --autofs não ignorar pontos de montagem montados por automounter
 -p --port <port> escutar na porta fornecida
 -s --no-setquota desabilita chamadas remotas a setquota (padrão)
 -S --setquota habilita chamadas remotas a setquota
 -x --xtab <path> selecionar um arquivo alternativo com tabela de exportação NFSD
 Uso: %s [opções]
Opções:
 -h --help mostrar este texto
 -V --version mostrar informação de versão
 -F --foreground iniciar o serviço de cota no primeiro plano
 -I --autofs não ignorar pontos de montagem montados pelo automounter
 -p --port <porta> escutar na porta fornecida
 -x --xtab <caminho> selecionar um arquivo alternativo com tabela de exportação NFSD
 Uso: %s [opções]
Opções:
 -h --help mostrar este texto
 -V --version mostrar informação de versão
 -C --no-console não tentar escrever mensagens no console
 -b --print-below escrever no console informações sobre como ficar abaixo de limites físicos/permitidos
 -D --no-dbus não tentar escrever mensagens no DBUS
 -F --foreground executar daemon no primeiro plano
 Use: quota [-guqvswim] [-l | [-Q | -A]] [-F quotaformat]
 Usuário Utilitário para checagem e reparo dos arquivos de cota.
%s [-gucbfinvdmMR] [-F <formato-da-cota>] sistema de arquivos|-a
-u, --user checar arquivos do usuário
-g, --group checar arquivos do grupo
-c, --create-files cria novos arquivos de cota
-b, --backup cria backup dos arquivos de cota antigos
-f, --force força a checagem mesmo se o sistema de cotas estiver ativado
-i, --interactive modo interativo
-n, --use-first-dquot use the first copy of duplicated structure
-v, --verbose mostra mais informações
-d, --debug mostra informações de debug
-m, --no-remount não remounta o sistema de arquivos como somente-leitura
-M, --try-remount try tenta remontar o sistema de arquivos como somente-leitura,
                          continua mesmo se falhar
-R, --exclude-root exclui a raiz quando checa todos os sistemas de arquivos
-F, --format=formatname checa os arquivos de cota de um formato específico
-a, --all checa todos os sistemas de arquivos
-h, --help mostra essa mensagem e sai
-V, --version mostra a versão e sai

 Utilitário para converter arquivos de cota.
Uso:
	%s [opções] pontodemontagem

-u, --user converte um arquivo cota de usuário
-g, --group converte um arquivo de cota de grupo
-e, --convert-endian converte um arquivo de cota para corrigir a "endianidade"
-f, --convert-format oldfmt,newfmt converte do formato antigo para o formato de cota VFSv0
-h, --help mostra este texto de ajuda e sai
-V, --version mostra informações sobre a versão e sai

 Aviso -  Arquivo quota %s possivelmente incompleto. Não foi possível salvar as configurações do quota...
 AVISO - %s: não foi possível a alteração atual no bloco alocado.
 AVISO - %s: não foi possível a alteração atual no inode alocado
 AVISO - Arquivo quota %s está com o cabeçalho corrompido
 AVISO - Arquivo quota %s provavelmente está incompleto.Não foi possível salvar as configurações feita no quota...
 AVISO - As informações do arquivo quota está corrompido.
 AVISO - Arquivo Quota %s provavelmente está incompleto. Não foi possível salvar as configurações feita no quota...
 AVISO - Alguns dados talvez foram alterado por está corrompido.
 Aviso Alerta: Não foi possível abrir tabela de exportação %s: %s
Usando '/' como raiz de um pseudo-sistema de arquivos.
 Aviso: não foi possível sinalizar o conjunto EXT2 em %s: %s
 Aviso: Ignorar -%c quando listar um arquivo de sistema específico.
 Aviso: Serviço de e-mail finalizado anormalmente.
 Aviso: Nenhum formato quota foi detectado neste kernel.
 Estatísticas do Gerenciador de Cota XFS dquot
 quota XFS permitido somente no sistema de arquivo XFS.
 XFS_IOC_FSBULKSTAT ioctl falhou: %s
 Você tem que especificar a ação a ser executada.
 Você tem que especificar o formato de origem e o formato de destino.
 O seu kernel provavelmente suporta cotas "journaled" mas você não está usando esta opção. Considere mudar para cotas "journaled" para evitar a execução de quotacheck depois de desligar de forma abrupta.
 O seu arquivo quota está armazenado erradamente no endianity. Por favor, use convertquota(8) para convertê-lo.
 e contabilização  formato inválido:
%s
 limite de blocos atingido cota de blocos excedida cota de blocos excedida blocos impossível criar serviço udp.
 não foi possível localizar %s em %s [%s]
 não foi possível abrir %s: %s
 não foi possível escrever tempos para %s. Talvez o kernel não suporte tal operação?
 copy_user_quota_limits: Falha ao obter userquota para uid %ld : %s
 copy_user_quota_limits: Falha ao aplicar userquota para uid %ld : %s
 dia dias concluído
 erro (%d) durante abertura de %s
 erro (%d) no momento de abrir a pesquisa do inode
 erro (%d) no momento da inicialização da procura do inode
 erro quando obtinha quota de %s para %s (id %u): %s
 limite de arquivos atingido cota de arquivos excedida cota de arquivos excedida arquivos find_free_dqentry(): Dados do bloco estão completos, mas não deveria estar.
 fsname incompatível
 getgroups(): %s
 ficou abaixo do limite de blocos ficou abaixo da cota de blocos ficou abaixo do limite de arquivos ficou abaixo da cota de arquivos grace grupo grupo %s não existe.
 host %s tentou chamar setquota a partir de uma porta >= 1024
 host %s tentou chamar setquota quando estava desabilidato
 hora horas limite minuto minutos nenhum desligado ligado popd %s
Entrando no diretório %s
 pushd %s/%s
 quota quotactl em %s [%s]: %s
 quotactl em %s: %s
 quotactl() em %s: %s
 segundo segundos configura o root_squash em %s: %s
 espaço svc_run retornado
 impossível liberar argumentos
 incapaz de registrar (RQUOTAPROG, EXT_RQUOTAVERS, TCP).
 incapaz de registrar (RQUOTAPROG, RQUOTAVERS, TCP).
 indefinido aviso desconhecido de cota sem definição usuário usuário %s não existe.
 usando %s em %s [%s]: %s
 