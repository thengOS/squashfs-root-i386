��    P      �  k         �  K   �  �    �   �  �   b	  8  *
  �  c  �     |  �  0   +  9   \  6   �  C   �               6     U     i  9   r     �     �     �     �  -   �  $     5   @  '   v  $   �  $   �  $   �          +  &   E     l     �  J   �  $   �          "  H   0     y     �     �     �     �     �  "   �     "  1   6     h  (   w     �     �     �  &   �          &     ?  "   N     q  0   �     �  (   �     �  �     �  �  "   D     g  �   �     
  &   )     P     e     ~  �   �  7   m     �  
   �     �     �  �  �  J   w  #  �  �   �   �   x!  
  \"  �  g%  �   J'  �  �'  7   �)  @   �)  9   '*  K   a*     �*     �*     �*     �*  
   +  >   +     Q+     k+     y+     �+  G   �+  2   �+  J   ,  3   `,  9   �,  /   �,  7   �,     6-     T-  6   r-     �-     �-  S   �-  7   7.     o.     �.  H   �.     �.      /  .   (/  (   W/     �/     �/  )   �/     �/  1   �/     0  3   -0     a0     �0     �0  +   �0     �0  !   �0     1  3   /1  #   c1  E   �1     �1  $   �1     2  �   12  �  �2  -   �5     �5  }   �5  )   p6  +   �6     �6     �6     �6  �   7  8   �7     #8     (8     48  	   98        G       B   :   A   1      (   	          >              M   P       0             ;          F   !       '   .       $   3                     7   -         9   ,   *       #          5       E      )   2   4      @   ?       
       <           H                        +   =      C   D                  N          O       I   K   8          6                 &   J       "   L   %   /       
Copyright (C) 1990, 92, 93, 94, 96, 97, 99 Free Software Foundation, Inc.
 
Fine tuning:
  -s, --strict           use strict mappings, even loose characters
  -d, --diacritics       convert only diacritics or alike for HTML/LaTeX
  -S, --source[=LN]      limit recoding to strings and comments as for LN
  -c, --colons           use colons instead of double quotes for diaeresis
  -g, --graphics         approximate IBMPC rulers by ASCII graphics
  -x, --ignore=CHARSET   ignore CHARSET while choosing a recoding path
 
If a long option shows an argument as mandatory, then it is mandatory
for the equivalent short option also.  Similarly for optional arguments.
 
If none of -i and -p are given, presume -p if no FILE, else -i.
Each FILE is recoded over itself, destroying the original.  If no
FILE is specified, then act as a filter and recode stdin to stdout.
 
Listings:
  -l, --list[=FORMAT]        list one or all known charsets and aliases
  -k, --known=PAIRS          restrict charsets according to known PAIRS list
  -h, --header[=[LN/]NAME]   write table NAME on stdout using LN, then exit
  -F, --freeze-tables        write out a C module holding all tables
  -T, --find-subsets         report all charsets being subset of others
  -C, --copyright            display Copyright and copying conditions
      --help                 display this help and exit
      --version              output version information and exit
 
Operation modes:
  -v, --verbose           explain sequence of steps and report progress
  -q, --quiet, --silent   inhibit messages about irreversible recodings
  -f, --force             force recodings even when not reversible
  -t, --touch             touch the recoded files after replacement
  -i, --sequence=files    use intermediate files for sequencing passes
      --sequence=memory   use memory buffers for sequencing passes
 
Option -l with no FORMAT nor CHARSET list available charsets and surfaces.
FORMAT is `decimal', `octal', `hexadecimal' or `full' (or one of `dohf').
 
REQUEST is SUBREQUEST[,SUBREQUEST]...; SUBREQUEST is ENCODING[..ENCODING]...
ENCODING is [CHARSET][/[SURFACE]]...; REQUEST often looks like BEFORE..AFTER,
with BEFORE and AFTER being charsets.  An omitted CHARSET implies the usual
charset; an omitted [/SURFACE]... means the implied surfaces for CHARSET; a /
with an empty surface name means no surfaces at all.  See the manual.
 
Report bugs to <recode-bugs@iro.umontreal.ca>.
 
Usage: %s [OPTION]... [ [CHARSET] | REQUEST [FILE]... ]
   -p, --sequence=pipe     same as -i (on this system)
   -p, --sequence=pipe     use pipe machinery for sequencing passes
  done
  failed: %s in step `%s..%s'
 %s failed: %s in step `%s..%s' %s in step `%s..%s' %s to %s %sConversion table generated mechanically by Free `%s' %s %sfor sequence %s.%s *Unachievable* *mere copy* Ambiguous output Cannot complete table from set of known pairs Cannot invert given one-to-one table Cannot list `%s', no names available for this charset Charset %s already exists and is not %s Charset `%s' is unknown or ambiguous Child process wait status is 0x%0.2x Codes %3d and %3d both recode to %3d Dec  Oct Hex   UCS2  Mne  %s
 Expecting `..' in request Following diagnostics for `%s' to `%s' Format `%s' is ambiguous Format `%s' is unknown Free `recode' converts files between various character sets and surfaces.
 Identity recoding, not worth a table Internal recoding bug Invalid input LN is some language, it may be `c', `perl' or `po'; `c' is the default.
 Language `%s' is ambiguous Language `%s' is unknown Misuse of recoding library No character recodes to %3d No error No table to print No way to recode from `%s' to `%s' Non canonical input Pair no. %d: <%3d, %3d> conflicts with <%3d, %3d> Recoding %s... Recoding is too complex for a mere table Request `%s' is erroneous Request: %s
 Required argument is missing Resurfacer set more than once for `%s' Sequence `%s' is ambiguous Sequence `%s' is unknown Shrunk to: %s
 Sorry, no names available for `%s' Step initialisation failed Step initialisation failed (unprocessed options) Symbol `%s' is unknown Syntax is deprecated, please prefer `%s' System detected problem This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 Try `%s %s' for more information.
 UCS2   Mne   Description

 Unless DEFAULT_CHARSET is set in environment, CHARSET defaults to the locale
dependent encoding, determined by LC_ALL, LC_CTYPE, LANG.
 Unrecognised surface name `%s' Unsurfacer set more than once for `%s' Untranslatable input Virtual memory exhausted Virtual memory exhausted! With -k, possible before charsets are listed for the given after CHARSET,
both being tabular charsets, with PAIRS of the form `BEF1:AFT1,BEF2:AFT2,...'
and BEFs and AFTs being codes are given as decimal numbers.
 Written by Franc,ois Pinard <pinard@iro.umontreal.ca>.
 byte reversible ucs2 variable Project-Id-Version: GNU recode 3.6
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2001-01-02 22:35+0100
PO-Revision-Date: 2005-11-08 11:17+0000
Last-Translator: Alexandre Folle de Menezes <Unknown>
Language-Team: Brazilian Portuguese <ldp-br@bazar.conectiva.com.br>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:04+0000
X-Generator: Launchpad (build 18115)
 
Copyright © 1990, 92, 93, 94, 96, 97, 99 Free Software Foundation, Inc.
 
Ajuste fino:
  -s, --strict           usa mapeamentos estritos, mesmo perdendo caracteres
  -d, --diacritics       converte apenas os diacríticos ou semelhantes
                         para HTML/LaTeX
  -S, --source[=LN]      limita a recodificação à strings e comentários, como
                         para o LN
  -c, --colons           usa dois pontos em vez de aspas para o trema
  -g, --graphics         aproxima os gráficos do IBMPC com gráficos ASCII
  -x, --ignore=CHARSET   ignora o CHARSET na escolha da rota de recodificação
 
Os argumentos obrigatórios para as opções longas são também obrigatórios
para as opções curtas.  O mesmo vale para argumentos opcionais
 
Na ausência de -i e -p, assumir -p se não houver ARQUIVO, ou então -i.
Cada ARQUIVO é recodificado para ele nesmo, destruindo o original. Se não for
indicado ARQUIVO, atua como um filtro convertendo do stdin para stdout.
 
Listagens:
  -l, --list[=FORMATO]       lista um ou todos os conjuntos de caracteres e
                             apelidos conhecidos
  -k, --known=PARES          restringe os conjuntos de caracteres com a lista
                             de PARES conhecidos
  -h, --header[=[LN/]NOME]   escreve a tabela NOME na saída padrão usando LN,
                             depois sai
  -F, --freeze-tables        escreve um módulo C contendo todas as tabelas
  -T, --find-subsets         reporta todos os conjuntos de carcteres que são
                             subconjuntos de outros
  -C, --copyright            exibe Copyright e condições de cópia
      --help                 exibe esta ajuda e sai
      --version              mostra informações de versão e sai
 
Modos de operação:
  -v, --verbose           Explica a sequência de passos e relata o progresso
  -q, --quiet, --silent   inibe mensagens sobre recodificações irreversíveis
  -f, --force             força recodificações mesmo quando irreversíveis
  -t, --touch             atualiza horário dos arquivos processados
  -i, --sequence=files    usa arquivos intermediários na seqüência de passos
      --sequence=memory   usa buffers na memória na seqüência de passos
 
A opção -l sem FORMATO ou CHARSET lista conjuntos de carcteres disponíveis.
FORMATO é `decimal', `octal', `hexadecimal' ou `full' (ou uma letra de `dohf').
 
REQUEST é SUBREQUEST[,SUBREQUEST]...; SUBREQUEST é ENCODING[..ENCODING]...
ENCODING é [CHARSET][/[SURFACE]]...; REQUEST geralmente é BEFORE..AFTER,
sendo BEFORE e AFTER conjuntos de caracters.  Se CHARSET for omitido, é
assumido o conjunto de caracteres usual; se [/SURFACE]... for omitida, são
assumidas as superfícies implicadas para o CHARSET; uma / com um nome vazio de
superfície significa nenhuma superfície.  Veja a documentação.
 
Informe os erros para <recode-bugs@iro.umontreal.ca>.
 
Uso: %s [OPÇÃO]... [ [CHARSET] | REQUISIÇÃO [ARQUIVO]... ]
   -p, --sequence=pipe     o mesmo que -i (neste sistema)
   -p, --sequence=pipe     usa redirecionamentos para seqüenciar os passos
  feito
  falhou: %s no passo `%s..%s'
 %s falhou: %s no passo `%s..%s' %s no passo %s..%s %s para %s %sTabela de conversão gerada automaticamente por Free `%s' %s %spara a sequência %s.%s *Impossível* *simples cópia* Saída ambígua Impossível completar a tabela a partir do conjunto de pares conhecidos Impossível inverter a tabela um-para-um fornecida Impossível listar `%s', não existem nomes disponíveis para este charset O conjunto de caracteres %s já existe e não é %s O conjunto de caracteres `%s' é desconhecido ou ambíguo O estado de espera do processo filho é 0x%0.2x Ambos os códigos %3d e %3d são recodificados para %3d Dec  Oct Hex   UCS2  Mne  %s
 Esperado `..' na requisição Os seguintes diagnósticos aplica-se de `%s' para `%s' O formato `%s' é ambíguo O formato `%s' é desconhecido O `recode' converte arquivos entre vários conjuntos de caracteres e superfícies.
 A recodificação identidade não precisa de uma tabela Erro interno de recodificação Entrada inválida LN é alguma linguagem, pode ser `c', `perl' ou `po'; `c' é o padrão.
 A linguagem `%s' é ambígua A linguagem `%s' é desconhecida Uso incorreto da biblioteca de recodificação Nenhum caracter é recodificado para %3d Sem erro Nenhuma tabela para imprimir Impossível recodificar de `%s' para `%s' Entrada não-canônica Par nº %d: <%3d, %3d> em conflito com <%3d, %3d> Recodificando %s... Recodificação muito complexa para uma mera tabela Requisição `%s' está errada Requisição: %s
 Falta argumento obrigatório Resurfacer setado mais de uma vez para `%s' A seqüencia `%s' é ambígua A seqüencia `%s' é desconhecida Encolhido para: %s
 Desculpe, não existem nomes disponíveis para `%s' A inicialização dos passos falhou A inicialização dos passos falhou (opções não foram processadas) O símbolo `%s' é desconhecido Sintaxe obsoleta, por favor use `%s' O sistema detectou um problema Este programa é software livre; veja o código fonte para condições de cópia.
NÃO HÁ GARANTIA; nem mesmo de COMERCIABILIDADE ou ADEQUAÇÃO A QUALQUER
FIM PARTICULAR.
 Este programa é software livre; a sua redistribuição e/ou modificação
nos termos da Licença Geral Pública GNU como publicada pela
Free Software Foundation, é permitida; de acordo com a versão 2 ou
(opcionalmente) qualquer outra versão posterior.

Este programa é distribuído na esperança de que possa ser útil, mas
SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIABILIDADE ou
ADEQUAÇÃO À QUALQUER FINALIDADE PARTICULAR.  Veja a Licença Geral Pública GNU
para mais detalhes.

Deve ter sido recebida uma cópia da Licença Geral Pública GNU
junto com este programa; senão, escreva para a Free Software Foundation,
Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 Tente `%s %s' para obter mais informações.
 UCS2   Mne   Descrição

 A menos que DEFAULT_CHARSET esteja setado no ambiente, o CHARSET padrão é o
local, determinado por LC_ALL, LC_CTYPE, LANG.
 Nome de superfície `%s' não reconhecido Unsurfacer setado mais de uma vez para `%s' Entrada intraduzível Memória virtual esgotada Memória virtual esgotada! Com -k, são listados os conjuntos de caracteres de entrada possíveis para o
CHARSET charset fornecido, com PARES no formato `INI1:FIN1,INI2:BEF2,...'
sendo INIs e FINs os respectivos códigos em notação decimal.
 Escrito por François Pinard <pinard@iro.umontreal.ca>.
 byte reversível ucs2 variável 