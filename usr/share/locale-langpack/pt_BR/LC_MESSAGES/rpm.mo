��   �  0   �  �  �*     <9     D9  L9      �9     �9     �9     �9  
   �9  	   �9     �9  "   :  &   ':  2   N:     �:  +   �:      �:     �:      
;     +;     ;;     J;     V;     Y;  2   w;     �;     �;     �;     �;  (   �;  &   #<  5   J<  2   �<  3   �<     �<     �<     =  $   $=     I=  +   i=     �=     �=     �=     �=     �=  B   �=  !   0>     R>     r>     �>     �>     �>     �>  &   �>      ?     .?  -   N?  1   |?     �?  $   �?     �?     @  "   #@     F@     ^@     v@  !   �@     �@      �@  !   �@  "   A     8A  #   SA  M   wA     �A     �A     �A     �A     �A     
B  
   B     (B     7B     >B     KB     ZB     iB     �B     �B  	   �B     �B     �B     �B     �B     �B  <   �B  9   9C  ?   sC  I   �C  >   �C  <   <D  >   yD  ?   �D  F   �D  U   ?E  G   �E  6   �E  J   F  ?   _F  D   �F  $   �F  
   	G     G  %   G     @G  
   LG     WG     fG     mG  
   ~G  	   �G  ,   �G     �G  )   �G  *   H     .H  !   LH     nH     �H     �H     �H     �H     �H  	   I     I     "I     7I     VI     jI     ~I  2   �I  A   �I     
J     "J     AJ     EJ  $   bJ     �J  1   �J  )   �J  (   �J  $   'K     LK  6   cK  #   �K     �K     �K      �K  $   L  *   7L     bL  +   xL     �L     �L  	   �L     �L  ;   �L     (M      8M     YM     rM  $   �M  "   �M     �M     �M  "   �M     N     9N     ON  "   lN     �N     �N     �N     �N     �N     O     $O  &   :O     aO  !   yO     �O  #   �O     �O      �O  4   P  !   FP     hP      �P  *   �P     �P  !   �P     Q  %   "Q     HQ     _Q     }Q     �Q     �Q     �Q     �Q  0   �Q     &R     .R     BR     VR     qR  !   �R     �R  ,   �R     �R     S     S     :S     PS     hS     xS     �S     �S     �S     �S     �S  '   �S  )   T  !   BT  !   dT     �T     �T     �T      �T      �T  !   	U     +U     /U  "   6U  ,   YU     �U     �U     �U  !   �U     �U     V     V     V     4V     PV  !   lV  ,   �V     �V     �V     �V  5   �V  A   0W     rW     �W     �W     �W  #   �W  '   �W     X     X  .   %X     TX     cX     vX  &   �X  H   �X  $   �X  '   Y     AY  e   aY  $   �Y  *   �Y  #   Z     ;Z  %   RZ     xZ      �Z     �Z     �Z     �Z  #   
[  '   .[  %   V[     |[     �[  6   �[     �[     �[     \     %\      B\     c\  !   w\  "   �\  $   �\     �\  '   �\     $]  *   9]     d]     ~]  %   �]     �]  !   �]  
   �]     �]  .   �]     -^     4^  <   O^     �^  )   �^  ,   �^  /   _     3_     P_  *   j_  )   �_  (   �_  0   �_  /   `  )   I`  (   s`  :   �`  9   �`  J   a  D   \a  C   �a  F   �a  E   ,b  )   rb      �b  #   �b     �b     �b     c     1c  $   Ec     jc  4   �c  %   �c     �c  &   �c     #d      <d     ]d     md     �d  3   �d  	   �d  	   �d  +   �d     e  &   1e  )   Xe  (   �e  *   �e  '   �e  )   �e  (   (f  (   Qf  +   zf  /   �f  .   �f  +   g  9   1g  &   kg  #   �g     �g  "   �g     �g  -   h  ;   :h      vh  "   �h  (   �h     �h  .   i  $   1i  /   Vi  "   �i  .   �i     �i  '   �i  #   j     Aj     _j  %   {j     �j  '   �j     �j  !    k  !   "k     Dk  %   ck  !   �k     �k  "   �k     �k     l     l     %l     +l  %   El     kl  "   �l  #   �l  *   �l  1   �l  %   +m  '   Qm  &   ym  %   �m     �m  !   �m  	   �m     �m     n     'n     .n  "   In  ?   ln  "   �n  2   �n     o  #   o     =o     Ko  9   Po  >   �o  $   �o     �o  1   �o  F   -p  7   tp  D   �p     �p     q  '   &q  %   Nq  /   tq  1   �q  -   �q  &   r     +r     Gr     ^r     {r     �r     �r     �r  G   �r     s     )s  !   <s     ^s     qs     �s     �s     �s  !   �s  $   t     (t     5t     Tt  #   et     �t      �t  *   �t      �t     u     0u     Iu  (   eu     �u  4   �u  )   �u  #   v  &   )v     Pv  "   ov     �v  &   �v     �v  3   �v  $   'w  ,   Lw     yw  )   �w     �w     �w     �w  /   x  ;   6x  0   rx     �x     �x  #   �x  1   �x  )   %y  	   Oy     Yy     vy     �y     �y     �y     �y     �y     z     $z  '   6z     ^z     uz     }z  !   �z  !   �z  $   �z  %   �z  #   {  !   @{     b{     {     �{     �{  
   �{     �{     �{     �{     |     (|     F|     ]|     u|     �|     �|     �|     �|     �|     }     1}     8}     G}     Z}     n}     |}  3   �}  -   �}  $   �}  <   ~     O~  2   f~  '   �~     �~     �~  8   �~     '  ,   G  0   t     �     �  0   �  *   �  +   =�  '   i�  8   ��  .   ʀ  -   ��  +   '�  )   S�  3   }�  @   ��     �     �  (   *�     S�  !   s�  3   ��     ɂ  %   �  #   �     1�     N�  )   k�     ��  -   ��     փ     ��     �  0   +�      \�  #   }�  .   ��  /   Є  6    �  6   7�  *   n�     ��     ��  >   ΅     �  +   *�  '   V�  )   ~�  -   ��     ֆ     ��  )   �  s   =�     ��     ͇     �  1    �     2�     ;�     J�     c�     u�     ��     ��  1   ��     ؈  V   �     K�     ^�  +   e�     ��  (   ��     ʉ  )   ݉  /   �     7�     ?�  &   ^�  &   ��  &   ��     ӊ     �     �     -�  8   L�     ��  W   ��     �     ��     �     (�     5�     L�     d�     |�  :   ��     Č     Ќ     �  $   ��  &   �     <�  5   \�     ��  '   ��  V   ͍     $�     D�     ^�  $   }�  %   ��  $   Ȏ     �     �     �  	   :�     D�     P�      _�      ��     ��     ��  �  ڏ      ɑ     �     ��  
   �     #�     0�  $   G�  -   l�  9   ��     Ԓ  .   �  (   "�      K�  "   l�     ��     ��     ��     ��     ��  2   ۓ     �     *�     ?�     R�  -   a�  .   ��  @   ��  C   ��  9   C�     }�  "   ��     ��  +   ˕      ��  -   �     F�     N�     Z�     q�     ��  N   ��  >   �  0   ,�  $   ]�     ��  %   ��  )   ��  '   �  ,   �     ?�  )   N�  6   x�  :   ��     �  ,   �  #   /�  &   S�  (   z�     ��     ��     י  !   ��     �  "   3�  !   V�  "   x�  !   ��  *   ��  [   �     D�     K�     X�     q�     ~�     ��     ��     ��     ��     ț     ڛ     �      �     )�     ;�     O�     ^�     `�  &   c�  !   ��     ��  J   Ü  I   �  K   X�  R   ��  L   ��  J   D�  L   ��  K   ܞ  U   (�  a   ~�  Q   ��  @   2�  T   s�  M   Ƞ  Q   �  "   h�     ��     ��  (   ��     ȡ  	   ء     �  	   ��     �  
   �     �  +   )�     U�  /   n�  6   ��  "   բ  '   ��  *    �     K�  0   a�     ��  &   ��     ٣     ��     �  "   !�  (   D�     m�     ��  '   ��  0   ɤ  I   ��     D�  '   c�     ��  )   ��  +   ��     �  7   ��  .   6�  (   e�  4   ��  #   æ  J   �  0   2�  (   c�      ��  -   ��  2   ۧ  <   �     K�  ;   i�  #   ��  #   ɨ  
   ��     ��  B   �     W�  )   l�     ��     ��  #   ɩ  *   ��     �     8�  "   I�     l�     ��     ��  "   ª  #   �     	�     �     0�  %   N�  "   t�      ��  -   ��     �  .   ��     ,�  (   D�  +   m�  &   ��  1   ��  "   �  !   �  $   7�  *   \�  (   ��  #   ��     ԭ  /   �     �  (   4�  %   ]�  ,   ��     ��  "   ͮ     �  8   �     =�     I�  "   d�     ��  #   ��  "   ǯ  1   �  B   �     _�     n�      {�     ��     ��     Ѱ  '   �     �     !�     '�      3�      T�  .   u�  0   ��  %   ձ  &   ��     "�     8�     O�  &   f�  %   ��  !   ��     ղ     ۲  ,   �  ?   �  0   V�     ��     ��  $   ��     �     ��     �     �     ;�  '   T�  $   |�  ?   ��  
   �     �  !   �  >   %�  I   d�     ��     ĵ     ҵ      �  )   �  7   7�     o�     t�  6   ��     ��     Ƕ     ߶  5   ��  J   0�  1   {�  0   ��     ޷  z   ��  1   y�  >   ��  .   �      �  0   :�  )   k�  /   ��  #   Ź  3   �  '   �  :   E�  3   ��  3   ��  '   �  3   �  U   D�     ��     ��      ̻  "   ��  .   �     ?�  (   Y�  %   ��  2   ��     ۼ  -   ��     (�  5   ;�     q�     ��  /   ��     ۽  )   �     �     %�  ;   )�     e�     n�  Y   ��  "   �  1   �  7   9�  @   q�  "   ��      տ  4   ��  8   +�  7   d�  >   ��  =   ��  5   �  4   O�  K   ��  L   ��  Y   �  U   w�  T   ��  c   "�  b   ��  )   ��  A   �  ;   U�      ��  0   ��  )   ��      �  ;   .�  -   j�  ?   ��  /   ��  !   �  0   *�  &   [�  6   ��     ��     ��      ��  7   	�  	   A�  
   K�  /   V�  '   ��  %   ��  &   ��  -   ��  /   )�  ,   Y�  .   ��  (   ��  )   ��  *   �  .   3�  -   b�  *   ��  8   ��  -   ��  '   "�  !   J�  (   l�     ��  @   ��  J   ��  ,   >�  )   k�  3   ��  (   ��  4   ��  2   '�  >   Z�  )   ��  A   ��  $   �  /   *�  6   Z�  $   ��  #   ��  5   ��  "   �  3   3�  +   g�  &   ��  )   ��  *   ��  .   �  +   >�  %   j�  =   ��  -   ��     ��     �     -�     4�  +   Q�     }�  #   ��  3   ��  .   ��  2   �  1   Q�  4   ��  -   ��  ,   ��     �  .   �  	   I�     S�  #   h�     ��     ��  #   ��  T   ��  %   (�  6   N�     ��  '   ��     ��     ��  B   ��  F   �  +   e�     ��  .   ��  `   ��  J   1�  ?   |�      ��  $   ��  &   �  '   )�  >   Q�  L   ��  .   ��  +   �     8�     S�  $   p�  "   ��     ��     ��     ��  J   ��     >�     X�  !   k�     ��     ��  &   ��      ��  %   �  $   -�  '   R�     z�     ��     ��  .   ��  #   ��  +   �  2   7�  /   j�  #   ��      ��  #   ��  -   �     1�  8   O�  -   ��  '   ��  ,   ��  "   �  *   .�  %   Y�  *   �  #   ��  8   ��  #   �  6   +�     b�  .   x�     ��     ��  $   ��  5   	�  K   ?�  -   ��     ��     ��  +   ��  A   �  ,   W�  
   ��  *   ��  *   ��     ��     ��     �     :�     S�     j�     ��  3   ��     ��     ��     ��  &   ��  &   %�  *   L�  &   w�  &   ��  $   ��     ��     �     '�     :�     N�     f�     ��  (   ��  *   ��  /   ��     #�     >�  #   Z�  %   ~�     ��     ��     ��  (   ��  +    �     L�     S�     b�     x�     ��     ��  @   ��  5   ��  /   %�  D   U�     ��  >   ��  .   ��      �      @�  >   a�     ��  .   ��  6   ��  !   &�      H�  4   i�  -   ��  6   ��  2   �  I   6�  =   ��  <   ��  <   ��  <   8�  B   u�  P   ��     	�  $   )�  -   N�     |�  (   ��  <   ��  #   �  A   &�  -   h�  "   ��  !   ��  A   ��  '   �  .   E�  0   t�  (   ��  $   ��  <   ��  )   0�  2   Z�  A   ��  =   ��  ;   �  <   I�  4   ��     ��  !   ��  b   ��  "   [�  +   ~�  3   ��  .   ��  0   �  &   >�     e�  *   ��  x   ��  1   )�     [�     w�  ?   ��     ��     ��     ��     �     (�     @�     ]�  :   q�     ��  s   ��     =�     T�  (   b�     ��  *   ��     ��  6   ��  ;   �     T�  '   ]�  -   ��  =   ��  2   ��     $�     C�     c�     ��  @   ��     ��  v   ��     g�  $   ��     ��     ��  %   ��  !   ��     �     2�  8   B�     {�     ��     ��  .   ��  ,   ��  $   "�  D   G�     ��  3   ��  e   ��  3   :�      n�  '   ��  *   ��  &   ��  %   	�  $   /�  &   T�  0   {�     ��  
   ��     ��      ��      ��     �     /�     �      7   �  U      �  �   =  C   ,  �   0             y     1   �   �   J           �     �   �  Q  �         ;  y  �  _  �   �  F  W   �       x      *  �  r  �    �  �   �   +  �  e  �   *  �   �     �         >   �  �      �  V      �   �   �   �       g   o          �              �  )     a  	        �  �      D  	   l  #   �  �  
  �      �      �       g  �   �  p       -   �      �  q   ~  �      �                �   �               L   �  �   �   �           �  �   �               �  �  �   �  �   0  �       P  �   X  �   v                T   �   ]  �          �  �           �      Y  �           p  <  �      �              )  \   g      �      i  �      @      _   &           "  (             8  j           �       �   V       �   G  4       �       �   �        2  �   F      (       �  �      v   w           �  �   &  u       �  H          �      F   �   
   {   -  �  �   �   ]  �      4  �  :     �  �   �  :                       �      f  Z  b  r   H  
  �   �   �      �  5  B  �   W      _        �   1  t   �     �          �  �  �       m  T  �  �      j  A      ~   k  �  `   �   S  �      Y  A   �        �  R  �         %    s    �  o               �   *   ^  �      �      3  �  �             %       �  �      {  f  G   �  O       e                  �  �      �      �   B  m  �  \  �       L  4      �  /         %  y       x   �      �  �  B   h  �      M  n  R  5   �      �  &    t  [    x  �   :      P   �   D      k       �  �  }  ;  a          �  �   6  N     �  �   D      9   <  �   �           "   =      v  +      �  �  �       �  \      w      �   #  '   z   �   �  �  J  �    �  $    �  �      �               �  ,       �      �  �  (  e      �  [   6           9               u  �  �     �   �      I      q  �   ?   I   ~  !   h  $       |   �   �  9      �     "  �   �   �  ;       l   �       Z   �           H   !        5  |      /  E  �  �                 �      j  �       �  b              �         �     =   P  �    b  w        �  �  �  �  V  �  K   �  a   ,  �   C      �   |    �         J    U   )  �      �      p  �   <   �  �  K  �  �           M   +   2   �  �           �     c       6          >  �          �  s   X            �  -      �  @  �  �       �  O  '  n  �           �  ?  �   �          .  Z  �         �              h   ^      �   8   i   �   �  0  m               	          X   �  s     !  �  �           �                  �     n   �       W      �  z    N          .   �  �   �  �   �       �  S   �  Q   f   l        A     O  M  7      �   z  q  �       �          �  �  L        S  i     �           �  C             �                         ?      �       I  �      �   �  �   �     k  .  #  �          �   �  Q  d     G  t  {  �  c      }  �  K  �   �       �          T      �  /  E   �       �    �   �   �     �  �      �   �   `  U  �  �  7        8  >      �  �      �  �   �                  �        1      �   2      '      Y   �  �   �  �  �   o          �      �   ^  $      �  [        �     �   R   �                 3   r  3    c     u  @       d  }   E          �          �  d  �  �  �      �  �  �    �  �   �  ]   �      `     N             H�  T9  h9  |9  �9  O�            ������            ������  (          �����  (       "   ���� 

RPM build errors:
  (MISSING KEYS:  (UNTRUSTED KEYS:  failed -   on file  ! only on numbers
 "%s" specifies multiple packages:
 %%changelog entries must start with *
 %%changelog not in descending chronological order
 %%dev glob not permitted: %s
 %%patch without corresponding "Patch:" tag
 %%semodule requires a file path
 %%{buildroot} can not be "/"
 %%{buildroot} couldn't be empty
 %3d<%*s(empty)
 %3d>%*s(empty) %a %b %d %Y %c %s %d defined multiple times
 %s already contains identical signature, skipping
 %s cannot be installed
 %s conflicts with %s%s %s created as %s
 %s failed: %x
 %s field must be present in package: %s
 %s has invalid numeric value, skipped
 %s has too large or too small integer value, skipped
 %s has too large or too small long value, skipped
 %s is a Delta RPM and cannot be directly installed
 %s is needed by %s%s %s is obsoleted by %s%s %s saved as %s
 %s scriptlet failed, exit status %d
 %s scriptlet failed, signal %d
 %s scriptlet failed, waitpid(%d) rc %d: %s
 %s: %s
 %s: %s: %s
 %s: Fflush failed: %s
 %s: Fread failed: %s
 %s: Fwrite failed: %s
 %s: Immutable header region could not be read. Corrupted package?
 %s: can't load unknown tag (%d).
 %s: cannot read header at 0x%x
 %s: chroot directory not set
 %s: failed to encode
 %s: headerRead failed: %s
 %s: import read failed(%d).
 %s: key %d import failed.
 %s: key %d not an armored public key.
 %s: line: %s
 %s: not an armored public key.
 %s: not an rpm package (or package manifest)
 %s: not an rpm package (or package manifest): %s
 %s: open failed: %s
 %s: option table misconfigured (%d)
 %s: public key read failed.
 %s: read manifest failed: %s
 %s: reading of public key failed.
 %s: regcomp failed: %s
 %s: regexec failed: %s
 %s: rpmReadSignature failed: %s %s: rpmWriteSignature failed: %s
 %s: writeLead failed: %s
 %s:%d: Argument expected for %s
 %s:%d: Got a %%else with no %%if
 %s:%d: Got a %%endif with no %%if
 %s:%d: bad %%if condition
 && and || not suported for strings
 '%s' type given with other types in %%semodule %s. Compacting types to '%s'.
 'EXPR' 'MACRO EXPR' (contains no files)
 (installed)  (invalid type) (invalid xml type) (no error) (no state)     (none) (not a blob) (not a number) (not a string) (not an OpenPGP signature) (not base64) (unknown %3d)  (unknown) ) )  * / not suported for strings
 - not suported for strings
 - only on numbers
 --allfiles may only be specified during package installation --allmatches may only be specified during package erasure --excludedocs may only be specified during package installation --hash (-h) may only be specified during package installation and erasure --ignorearch may only be specified during package installation --ignoreos may only be specified during package installation --ignoresize may only be specified during package installation --includedocs may only be specified during package installation --justdb may only be specified during package installation and erasure --nodeps may only be specified during package installation, erasure, and verification --percent may only be specified during package installation and erasure --prefix may only be used when installing new packages --relocate and --excludepath may only be used when installing new packages --replacepkgs may only be specified during package installation --test may only be specified during package installation and erasure : expected following ? subexpression <FILE:...> <dir> <lua> scriptlet support not built in
 <old>=<new> <package>+ <packagefile>+ <path> <source package> <specfile> <tarball> ======================== active %d empty %d
 ? expected in expression A %% is followed by an unparseable macro
 Arch dependent binaries in noarch package
 Architecture is excluded: %s
 Architecture is not included: %s
 Archive file not in header Bad CSA data
 Bad arch/os number: %s (%s:%d)
 Bad dirmode spec: %s(%s)
 Bad exit status from %s (%s)
 Bad file: %s: %s
 Bad magic Bad mode spec: %s(%s)
 Bad owner/group: %s
 Bad package specification: %s
 Bad source: %s: %s
 Bad syntax: %s(%s)
 Bad/unreadable  header Base modules '%s' and '%s' have overlapping types
 Build options with [ <specfile> | <tarball> | <source package> ]: Building for target %s
 Building target platforms: %s
 CMD Cannot sign RPM v3 packages
 Checking for unpackaged file(s): %s
 Cleaning up / removing...
 Common options for all rpm modes and executables: Conversion of %s to long integer failed.
 Copyright (C) 1998-2002 - Red Hat, Inc.
 Could not canonicalize hostname: %s
 Could not exec %s: %s
 Could not generate output filename for package %s: %s
 Could not open %%files file %s: %m
 Could not open %s file: %s
 Could not open %s: %s
 Couldn't create pipe for %s: %m
 Couldn't create pipe for signing: %m Couldn't create temporary file for %s: %s
 Couldn't download %s
 Couldn't duplicate file descriptor: %s: %s
 Couldn't exec %s: %s
 Couldn't fork %s: %s
 DIRECTORY Database options: Dependency tokens must begin with alpha-numeric, '_' or '/' Digest mismatch Directory not found by glob: %s
 Directory not found: %s
 Downloading %s to %s
 Duplicate %s entries in package: %s
 Duplicate locale %s in %%lang(%s)
 Empty file classifier
 Enter pass phrase:  Error executing scriptlet %s (%s)
 Error parsing %%setup: %s
 Error parsing %s: %s
 Error parsing tag field: %s
 Error reading %%files file %s: %m
 Exec of %s failed (%s): %s
 Executing "%s":
 Executing(%s): %s
 Execution of "%s" failed.
 Expecting %%semodule tag: %s
 Failed build dependencies:
 Failed dependencies:
 Failed to determine a policy name: %s
 Failed to dlopen %s %s
 Failed to encode policy file: %s
 Failed to find %s:
 Failed to get policies from header
 Failed to open tar pipe: %m
 Failed to read  policy file: %s
 Failed to read auxiliary vector, /proc not mounted?
 Failed to read spec file from %s
 Failed to rename %s to %s: %m
 Failed to resolve symbol %s: %s
 File %s does not appear to be a specfile.
 File %s is not a regular file.
 File %s is smaller than %u bytes
 File %s: %s
 File capability support not built in
 File listed twice: %s
 File must begin with "/": %s
 File needs leading "/": %s
 File not found by glob: %s
 File not found: %s
 File too large for archive Finding  %s: %s
 Generating %d missing index(es), please wait...
 Header  Header SHA1 digest: Header size too big Ignoring invalid regex %s
 Incomplete data line at %s:%d
 Incomplete default line at %s:%d
 Install/Upgrade/Erase options: Installed (but unpackaged) file(s) found:
%s Installing %s
 Internal error Internal error: Bogus tag %d
 Invalid %s token: %s
 Invalid capability: %s
 Invalid date %u Invalid patch number %s: %s
 Keyring options: MACRO MD5 digest: Macro %%%s failed to expand
 Macro %%%s has empty body
 Macro %%%s has illegal name (%%define)
 Macro %%%s has illegal name (%%undefine)
 Macro %%%s has unterminated body
 Macro %%%s has unterminated opts
 Missing %s in %s %s
 Missing '(' in %s %s
 Missing ')' in %s(%s
 Missing module path in line: %s
 Missing rpmlib features for %s:
 More than one file on a line: %s
 NO  NOT OK No "Source:" tag in the spec file
 No compatible architectures found for build
 No file attributes configured
 No patch number %u
 No source number %u
 Non-white space follows %s(): %s
 Not a directory: %s
 OK OS is excluded: %s
 OS is not included: %s
 Package already exists: %s
 Package check "%s" failed.
 Package has no %%description: %s
 Pass phrase check failed or gpg key expired
 Pass phrase is good.
 Please contact %s
 Plugin %s not loaded
 Policy module '%s' duplicated with overlapping types
 PreReq:, Provides:, and Obsoletes: dependencies support versions. Preparing packages... Preparing... Processing files: %s
 Processing policies: %s
 Query options (with -q or --query): Query/Verify package selection options: ROOT RPM version %s
 Recognition of file "%s" failed: mode %06o %s
 Retrieving %s
 Signature options: Spec options: Symlink points to BuildRoot: %s -> %s
 This program may be freely redistributed under the terms of the GNU GPL
 Too many args in data line at %s:%d
 Too many args in default line at %s:%d
 Too many arguments in line: %s
 Too many levels of recursion in macro expansion. It is likely caused by recursive macro declaration.
 Unable to change root directory: %m
 Unable to create immutable header region.
 Unable to open %s for reading: %m.
 Unable to open %s: %s
 Unable to open current directory: %m
 Unable to open icon %s: %s
 Unable to open spec file %s: %s
 Unable to open stream: %s
 Unable to open temp file: %s
 Unable to read icon %s: %s
 Unable to reload signature header.
 Unable to restore current directory: %m Unable to restore root directory: %m
 Unable to write package: %s
 Unable to write temp header
 Unknown file digest algorithm %u, falling back to MD5
 Unknown file type Unknown format Unknown icon type: %s
 Unknown option %c in %s(%s)
 Unknown payload compression: %s
 Unknown system: %s
 Unsatisfied dependencies for %s:
 Unsupported PGP hash algorithm %u
 Unsupported PGP pubkey algorithm %u
 Unsupported PGP signature
 Unsupported payload (%s) in package %s
 Unterminated %c: %s
 Unusual locale length: "%s" in %%lang(%s)
 Updating / installing...
 V%d %s/%s %s, key ID %s Verify options (with -V or --verify): Version required Versioned file name not permitted Wrote: %s
 YES You must set "%%_gpg_name" in your macro file
 [none] ] expected at end of array a hardlink file set may be installed without being complete. argument is not an RPM package
 arguments to --prefix must begin with a / arguments to --root (-r) must begin with a / array iterator used with different sized arrays bad date in %%changelog: %s
 bad option '%s' at %s:%d
 build binary package from <source package> build binary package only from <specfile> build binary package only from <tarball> build source and binary packages from <specfile> build source and binary packages from <tarball> build source package only from <specfile> build source package only from <tarball> build through %build (%prep, then compile) from <specfile> build through %build (%prep, then compile) from <tarball> build through %install (%prep, %build, then install) from <source package> build through %install (%prep, %build, then install) from <specfile> build through %install (%prep, %build, then install) from <tarball> build through %prep (unpack sources and apply patches) from <specfile> build through %prep (unpack sources and apply patches) from <tarball> buildroot already specified, ignoring %s
 can't create %s lock on %s (%s)
 cannot add record originally at %u
 cannot create %s: %s
 cannot get %s lock on %s/%s
 cannot open %s at %s:%d: %m
 cannot open %s: %s
 cannot open Packages database in %s
 cannot re-open payload: %s
 cannot use --prefix with --relocate or --excludepath create archive failed on file %s: %s
 create archive failed: %s
 creating a pipe for --pipe failed: %m
 debug file state machine debug payload file state machine debug rpmio I/O define MACRO with value EXPR delete package signatures dependency comparison supports versions with tilde. different directory display final rpmrc and macro configuration display known query tags display the states of the listed files do not accept i18N msgstr's from specfile do not execute %%post scriptlet (if any) do not execute %%postun scriptlet (if any) do not execute %%pre scriptlet (if any) do not execute %%preun scriptlet (if any) do not execute %check stage of the build do not execute %clean stage of the build do not execute any %%triggerin scriptlet(s) do not execute any %%triggerpostun scriptlet(s) do not execute any %%triggerprein scriptlet(s) do not execute any %%triggerun scriptlet(s) do not execute any scriptlet(s) triggered by this package do not execute any stages of the build do not execute package scriptlet(s) do not glob arguments do not install configuration files do not install documentation do not process non-package files as manifests do not reorder package installation to satisfy dependencies do not verify build dependencies do not verify package dependencies don't check disk space before installing don't execute verify script(s) don't import, but tell if it would work or not don't install file security contexts don't install, but tell if it would work or not don't verify capabilities of files don't verify database header(s) when retrieved don't verify digest of files don't verify digest of files (obsolete) don't verify file security contexts don't verify files in package don't verify group of files don't verify header+payload signature don't verify mode of files don't verify modification time of files don't verify owner of files don't verify package architecture don't verify package dependencies don't verify package digest(s) don't verify package operating system don't verify package signature(s) don't verify size of files don't verify symlink path of files dump basic file information empty tag format empty tag name erase erase (uninstall) package error creating temporary file %s: %m
 error reading from file %s
 error reading header from package
 error(%d) adding header #%d record
 error(%d) allocating new package instance
 error(%d) getting "%s" records from %s index: %s
 error(%d) removing header #%d record
 error(%d) removing record "%s" from %s
 error(%d) storing record "%s" into %s
 error(%d) storing record #%d into %s
 error:  exclude paths must begin with a / exclusive exec failed
 extra '(' in package label: %s
 failed failed to create directory failed to create directory %s: %s
 failed to rebuild database: original database remains in place
 failed to remove directory %s: %s
 failed to replace old database with new database!
 failed to stat %s: %m
 failed to write all data to %s: %s
 fatal error:  file file %s conflicts between attempted installs of %s and %s file %s from install of %s conflicts with file from package %s file %s is not owned by any package
 file %s: %s
 file digest algorithm is per package configurable file name(s) stored as (dirName,baseName,dirIndex) tuple, not as path. files may only be relocated during package installation generate package header(s) compatible with (legacy) rpm v3 packaging gpg exec failed (%d)
 gpg failed to write signature
 group %s does not contain any packages
 group %s does not exist - using root
 header #%u in the database is bad -- skipping.
 header tags are always sorted after being loaded. ignore ExcludeArch: directives from spec file ignore file conflicts between packages illegal _docdir_fmt %s: %s
 illegal signature type import an armored public key incomplete %%changelog entry
 incorrect format: %s
 initialize database install install all files, even configurations which might otherwise be skipped install documentation install package(s) internal support for lua scripts. invalid dependency invalid field width invalid index type %x on %s/%s
 invalid package number: %s
 invalid syntax in lua file: %s
 invalid syntax in lua script: %s
 invalid syntax in lua scriptlet: %s
 line %d: %s
 line %d: %s is deprecated: %s
 line %d: %s: %s
 line %d: Bad %%setup option %s: %s
 line %d: Bad %s number: %s
 line %d: Bad %s: qualifiers: %s
 line %d: Bad BuildArchitecture format: %s
 line %d: Bad arg to %%setup: %s
 line %d: Bad no%s number: %u
 line %d: Bad number: %s
 line %d: Bad option %s: %s
 line %d: Docdir must begin with '/': %s
 line %d: Empty tag: %s
 line %d: Epoch field must be an unsigned number: %s
 line %d: Error parsing %%description: %s
 line %d: Error parsing %%files: %s
 line %d: Error parsing %%policies: %s
 line %d: Error parsing %s: %s
 line %d: Illegal char '%c' in: %s
 line %d: Illegal char in: %s
 line %d: Illegal sequence ".." in: %s
 line %d: Malformed tag: %s
 line %d: Only noarch subpackages are supported: %s
 line %d: Package does not exist: %s
 line %d: Prefixes must not end with "/": %s
 line %d: Second %s
 line %d: Tag takes single token only: %s
 line %d: Too many names: %s
 line %d: Unclosed %%if
 line %d: Unknown tag: %s
 line %d: internal script must end with '>': %s
 line %d: interpreter arguments not allowed in triggers: %s
 line %d: script program must begin with '/': %s
 line %d: second %%prep
 line %d: second %s
 line %d: triggers must have --: %s
 line %d: unclosed macro or bad line continuation
 line %d: unsupported internal script: %s
 line: %s
 list all configuration files list all documentation files list files in package list keys from RPM keyring lua hook failed: %s
 lua script failed: %s
 magic_load failed: %s
 magic_open(0x%x) failed: %s
 malformed %s: %s
 memory alloc (%u bytes) returned NULL.
 miFreeHeader: skipping missing missing   %c %s missing '(' in package label: %s
 missing ')' in package label: %s
 missing ':' (found 0x%02x) at %s:%d
 missing architecture for %s at %s:%d
 missing architecture name at %s:%d
 missing argument for %s at %s:%d
 missing name in %%changelog
 missing second ':' at %s:%d
 missing { after % missing } after %{ net shared net shared     no arguments given no arguments given for parse no arguments given for query no arguments given for verify no dbpath has been set no dbpath has been set
 no description in %%changelog
 no package matches %s: %s
 no package provides %s
 no package requires %s
 no package triggers %s
 no packages given for erase no packages given for install normal normal         not an rpm package not an rpm package
 not installed not installed  one type of query/verify may be performed at a time only installation and upgrading may be forced only one major mode may be specified only one of --excludedocs and --includedocs may be specified open of %s failed: %s
 operate on binary rpms generated by spec (default) operate on source rpm generated by spec override build root override target platform package %s (which is newer than %s) is already installed package %s is already installed package %s is intended for a %s architecture package %s is intended for a %s operating system package %s is not installed
 package %s is not relocatable
 package %s was already added, replacing with %s
 package %s was already added, skipping %s
 package has neither file owner or id lists
 package has not file owner/group lists
 package name-version-release is not implicitly provided. package payload can be compressed using bzip2. package payload can be compressed using lzma. package payload can be compressed using xz. package payload file(s) have "./" prefix. package scriptlets can be expanded at install time. package scriptlets may access the rpm database while installing. parse error in expression
 parse spec file(s) to stdout path %s in package %s is not relocatable predefine MACRO with value EXPR print dependency loops as warning print hash marks as package installs (good with -v) print macro expansion of EXPR print percentages as package installs print the version of rpm being used provide less detailed output provide more detailed output query of specfile %s failed, can't parse
 query spec file(s) query the package(s) triggered by the package query/verify a header instance query/verify a package file query/verify all packages query/verify package(s) from install transaction query/verify package(s) in group query/verify package(s) owning file query/verify package(s) with header identifier query/verify package(s) with package identifier query/verify the package(s) which provide a dependency query/verify the package(s) which require a dependency read <FILE:...> instead of default file(s) read failed: %s (%d)
 reading symlink %s failed: %s
 rebuild database inverted lists from installed package headers record %u could not be read
 reinstall if the package is already present relocate files from path <old> to <new> relocate files in non-relocatable package relocate the package to <dir>, if relocatable relocations must begin with a / relocations must contain a = relocations must have a / following the = remove all packages which match <package> (normally an error is generated if <package> specified multiple packages) remove build tree when done remove sources when done remove specfile when done replace files in %s with files from %s to recover replaced replaced       replacing %s failed: %s
 rpm checksig mode rpm query mode rpm verify mode rpmMkTemp failed
 rpmdb: damaged header #%u retrieved -- skipping.
 rpmdbNextIterator: skipping script disabling options may only be specified during package installation and erasure send stdout to CMD shared short hand for --replacepkgs --replacefiles sign package(s) sign package(s) (identical to --addsign) skip %%ghost files skip files with leading component <path>  skip straight to specified stage (only for c,i) skipped skipping %s - transfer failed
 source package contains no .spec file
 source package expected, binary found
 support for POSIX.1e file capabilities syntax error in expression
 syntax error while parsing &&
 syntax error while parsing ==
 syntax error while parsing ||
 the scriptlet interpreter can use arguments from header. transaction trigger disabling options may only be specified during package installation and erasure types must match
 unable to read the signature
 undefine MACRO unexpected ] unexpected query flags unexpected query format unexpected query source unexpected } unknown error %d encountered while manipulating package %s unknown tag unknown tag: "%s"
 unmatched (
 unpacking of archive failed%s%s: %s
 unrecognized db option: "%s" ignored.
 unsupported RPM package version update the database, but do not modify the filesystem upgrade package(s) upgrade package(s) if already installed upgrade to an old version of the package (--force on upgrades does this automatically) use ROOT as top level directory use database in DIRECTORY use the following query format user %s does not exist - using root
 verify %files section from <specfile> verify %files section from <tarball> verify database files verify package signature(s) waiting for %s lock on %s
 warning:  wrong color wrong color    { expected after : in expression { expected after ? in expression | expected at end of expression } expected in expression Project-Id-Version: RPM
Report-Msgid-Bugs-To: rpm-maint@lists.rpm.org
POT-Creation-Date: 2014-09-18 14:01+0300
PO-Revision-Date: 2015-01-08 16:30+0000
Last-Translator: Vinicius Almeida <vinicius.algo@gmail.com>
Language-Team: Portuguese (Brazil) (http://www.transifex.com/projects/p/rpm/language/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:05+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
 

Erros na construção do RPM:
  (CHAVES FALTANDO:  (CHAVES NÃO CONFIÁVEIS:  falhou -   no arquivo  ! somente em números
 "%s" especifica múltiplos pacotes:
 entradas do %%changelog devem começar com *
 %%changelog não está na ordem cronológica decrescente
 %%dev glob não permitida: %s
 %%patch não corresponde à etiqueta "Patch:"
 %%semodulo requer um caminho de arquivo
 %%{buildroot} não pode ser "/"
 %%{buildroot} não pode ser vazio
 %3d<%*s(vazio)
 %3d>%*s(vazio) %a %b %d %Y %c %s %d definido várias vezes
 %s já contém uma assinatura idêntica, omitindo
 %s não pode ser instalado
 %s conflita com %s%s %s criado como %s
 %s falhou: %x
 o campo %s deve estar presente no pacote: %s
 %s tem um valor numérico inválido, ignorado
 %s tem um valor inteiro muito grande ou muito pequeno, ignorado
 %s tem valor inteiro longo muito grande ou muito pequeno, ignorado
 %s é um Delta RPM e não pode ser instalado diretamente
 %s é requerido por %s%s %s tornou-se obsoleto pelo(a) %s%s %s salvo como %s
 o scriptlet %s falhou, status de saída %d
 o scriptlet %s falhou, sinal %d
 o scriptlet %s falhou, waitpid(%d) rc %d: %s
 %s: %s
 %s: %s: %s
 %s: Fflush falhou: %s
 %s: Fread falhou: %s
 %s: Fwrite falhou: %s
 %s: A região de cabeçalho imutável não pôde ser lida. Pacote corrompido?
 %s: não foi possível carregar a etiqueta desconhecida (%d).
 %s: não foi possível ler o cabeçalho em 0x%x
 %s: diretório chroot não definido
 %s: falha ao codificar
 %s: leitura de cabeçalho falhou: %s
 %s: leitura de importação falhou (%d).
 %s: a importação da chave %d falhou.
 %s: %d não é uma chave pública blindada.
 %s: linha: %s
 %s: não é uma chave pública blindada.
 %s: não é um pacote rpm (ou um manifesto de pacote)
 %s: não é um pacote rpm (ou um manifesto de pacote): %s
 %s: falha ao abrir: %s
 %s: tabela de opções mal configurada (%d)
 %s: falha ao ler a chave pública.
 %s: falha na leitura do manifesto: %s
 %s: falha na leitura da chave pública.
 %s: o regcomp falhou: %s
 %s: o regexec falhou: %s
 %s: rpmReadSignature falhou: %s %s: rpmWriteSignature falhou: %s
 %s: writeLead falhou: %s
 %s:%d: Esperado argumento para %s
 %s:%d: Há um %%else sem um %%if
 %s:%d: Há um %%endif sem um %%if
 %s:%d: condição %%if inválida
 && e || não são suportados para strings
 '%s' determinado tipo com outros tipos em %%semodulo %s. Tipos de compactação para '%s'.
 "EXPR" "MACRO EXPR" (não contém arquivos)
 (instalado)  (tipo inválido) (tipo xml inválido) (sem erros) (sem estado)     (nada) (não é um blob) (não é um número) (não é uma sequência) (não é uma assinatura OpenPGP) (não é base 64) (%3d desconhecido)  (desconhecido) ) )  * / não são suportados para strings
 - não é suportado para strings
 - somente em números
 --allfiles somente pode ser especificado durante a instalação de pacotes --allmatches somente pode ser especificado durante a remoção de pacotes --excludedocs somente pode ser especificado durante instalação de pacotes --hash (-h) só pode ser especificado durante a instalação e remoção do pacote --ignorearch somente pode ser especificado durante a instalação de pacotes --ignoreos somente pode ser especificado durante a instalação de pacotes --ignoresize somente pode ser especificado durante a instalação de pacotes --includedocs somente pode ser especificado durante instalação de pacotes --justdb somente pode ser especificado durante a instalação ou remoção de pacotes --nodeps só pode ser especificado durante a instalação do pacote, o apagamento e verificação --percent  só pode ser especificado durante a instalação e remoção do pacote --prefix somente pode ser usado na instalação de novos pacotes --relocate e --excludepath somente podem ser usados na instalação de novos pacotes --replacepkgs somente pode ser especificado durante a instalação de pacotes --test só pode ser especificado durante a instalação do pacote e de apagamento : esperado após a subexpressão ? <ARQUIVO:...> <dir> suporte a scriptlet <lua> não embutido
 <antigo>=<novo> <pacote>+ <arquivo_do_pacote>+ <caminho> <pacote fonte> <specfile> <arquivo tar> ======================== %d ativo %d vazio
 ? esperado na expressão Um %% é seguido por um macro não analisável
 Binários dependentes de arquitetura no pacote noarch
 A arquitetura está excluída: %s
 A arquitetura não está excluída: %s
 Arquivo de pacote não está no cabeçalho Dados CSA inválidos
 Número de arquitetura/SO inválido: %s (%s:%d)
 Dirmode spec inválido: %s(%s)
 Status de saída de %s inválido (%s)
 Arquivo inválido: %s: %s
 Magic inválido Modo spec inválido: %s(%s)
 Proprietário/grupo inválido: %s
 Especificação do pacote inválida: %s
 Fonte inválida: %s: %s
 Sintaxe inválida: %s(%s)
 Cabeçalho inválido/impossível de ler Base de modulos '%s' e '%s' se sobrepõem tipos
 opções de construção com [ <specfile> | <tarball> | <pacote fonte> ]: Construindo para o destino %s
 Construindo plataformas de destino: %s
 CMD Não é possível assinar pacotes RPM v3
 Procurando por arquivos desempacotados: %s
 Limpando / removendo...
 Opções comuns para todos os executáveis e modos rpm: A conversão de %s para inteiro longo falhou.
 Copyright (C) 1998-2002 - Red Hat, Inc.
 Não foi possível canonizar o nome de máquina: %s
 Não foi possível executar %s: %s
 Não foi possível gerar o nome de arquivo de saída para o pacote %s: %s
 Não foi possível abrir %%files arquivo %s: %m
 Não foi possível abrir arquivo %s: %s
 Não foi possível abrir %s: %s
 Não foi possível criar um pipe para %s: %m
 Não foi possível criar um canal para assinar: %m Não foi possível criar um arquivo temporário para %s: %s
 Não foi possível baixar %s
 Não foi possível duplicar o descritor do arquivo: %s: %s
 Não foi possível executar %s: %s
 Não foi possível bifurcar %s: %s
 DIRETÓRIO Opções de banco de dados: Tokens de dependência deve começar com alfanumérico, '_' or '/' Digest incompatível Diretório não encontrado para glob: %s
 Diretório não encontrado: %s
 Baixando %s em %s
 Entrada %s duplicada no pacote: %s
 Localização duplicada: %s em %%lang(%s)
 Classificador de arquivo vazio
 Digite a senha:  Erro executando scriptlet %s (%s)
 Erro ao analisar %%setup: %s
 Erro de análise %s: %s
 Erro ao analisar campo tag:%s
 Erro lendo arquivo %%files %s: %m
 A execução de %s falhou (%s): %s
 Executando "%s":
 Executando (%s): %s
 A execução de "%s" falhou.
 Esperando %%semodulo de etiqueta: %s
 Falha ao construir dependências:
 Dependências não satisfeitas:
 Falhou ao determinar o nome da política: %s
 Falhou o dlopen %s %s
 Falha para codificar arquivo de política: %s
 Falha ao localizar %s:
 Falha ao obter políticas do cabeçalho
 Não foi possível abrir o pipe do tar: %m
 Falha ao ler arquivo de política: %s
 Falha ao ler vetor auxiliar, /proc não montado?
 Falha ao ler o arquivo spec de %s
 Falha ao renomear %s para %s: %m
 Falha ao resolver o símbolo %s: %s
 O arquivo %s não parece ser um specfile.
 O arquivo %s não é um arquivo normal.
 O arquivo %s é menor que %u bytes
 Arquivo %s: %s
 Suporte à capacidade de arquivo não embutida
 Arquivo listado duas vezes: %s
 O arquivo deve começar com uma "/": %s
 O arquivo precisa da "/" inicial: %s
 O arquivo não foi encontrado pelo glob: %s
 Arquivo não encontrado: %s
 Arquivo muito grande para arquivar Localizando %s: %s
 Gerando %d índice(s) ausente(s), por favor, aguarde...
 Cabeçalho  Digest do cabeçalho SHA1: Tamanho do cabeçalho muito grande Ignorar regex inválida %s
 Linha de dados incompleta em %s:%d
 Linha padrão incompleta em %s:%d
 Opções de Instalação/Atualização/Remoção: Arquivo(s) instalado(s) (mas não empacotado(s)) encontrado(s):
%s Instalando %s
 Erro interno Erro interno: etiqueta %d falsa
 Token de %s inválido: %s
 Capacidade inválida: %s
 Data %u inválida número da correção %s inválido: %s
 Opções do chaveiro: MACRO Digest MD5: O macro %%%s falhou ao expandir
 O macro %%%s tem um corpo vazio
 O macro %%%s tem um nome inválido (%%define)
 O macro %%%s tem um nome inválido (%%undefine)
 O macro %%%s tem um corpo incompleto
 O macro %%%s tem opções incompletas
 %s faltando em %s %s
 "(" faltando em %s %s
 "(" faltando em %s(%s
 Falta caminho do módulo na linha: %s
 Faltando recursos do rpmlib para %s:
 Mais de um arquivo por linha: %s
 NÃO  Não está OK Nenhuma etiqueta "Source:" no arquivo .spec
 Nenhuma arquitetura compatível encontrada para a construção
 Os atributos do arquivo não foram configurados
 Nenhum número de patch %u
 Nenhum número de fonte %u
 caractere de espaço após %s(): %s
 Não é um diretório: %s
 OK O SO está excluído: %s
 O SO não está incluído: %s
 O pacote já existe: %s
 Falha na verificação "%s" do pacote.
 O pacote não tem %%description: %s
 Falha na verificação de frase secreta ou a chave gpg expirou
 Senha ok.
 Por favor, contate %s
 O plugin %s não está carregado
 Politica de modulo '%s' duplicado com tipos de sobreposição
 PreReq:, Capacidades: e Obsoletos: as dependências suportam as versões. Preparando pacotes... Preparando... Processando arquivos: %s
 Políticas de processamento: %s
 Opções de consulta (com -q ou --query): Consultar/Verificar as opções de seleção do pacote: ROOT RPM versão %s
 Falha no reconhecimento do arquivo "%s": modo %06o %s
 Obtendo %s
 Opções de assinatura: Opções especificações: Ligação simbólica aponta para BuildRoot: %s -> %s
 Este programa pode ser livremente redistribuído sob os termos da GNU GPL
 Argumentos em excesso na linha de dados em %s:%d
 Argumentos em excesso na linha padrão em %s:%d
 Muitos argumentos na linha: %s
 Muitos níveis de recursão na expansão de macro. Isso provavelmente é causado por uma declaração de macro recursiva.
 Não foi possível alterar o diretório raiz: %m
 Não foi possível criar uma região de cabeçalho imutável.
 Não foi possível abrir %s para leitura: %m.
 Não foi possível abrir %s: %s
 Não foi possível abrir o diretório atual: %m
 Não foi possível abrir o ícone %s: %s
 Não foi possível abrir o arquivo spec %s: %s
 Não é possível abrir stream: %s
 Não foi possível abrir o arquivo temporário: %s
 Não foi possível ler o ícone %s: %s
 Não foi possível recarregar o cabeçalho da assinatura.
 Não foi possível restaurar o diretório atual: %m Não foi possível restaurar o diretório raiz: %m
 Não foi possível gravar o pacote: %s
 Não foi possível gravar o cabeçalho temporário
 Algoritmo de digest %u do arquivo é desconhecido, utilizando o MD5 como alternativa
 Tipo de arquivo desconhecido Formato desconhecido Tipo de ícone desconhecido: %s
 Opção desconhecida %c em %s(%s)
 Compactação de carga útil desconhecida: %s
 Sistema desconhecido: %s
 Dependências não satisfeitas para %s:
 Algoritmo hash PGP %u não suportado
 Algoritmo PGP de chave pública %u não suportado
 Assinatura PGP não suportada
 Carga útil (%s) não suportada no pacote %s
 %c incompleto: %s
 Localização de tamanho incomum: "%s" em %%lang(%s)
 Atualizando / instalando...
 V%d %s/%s %s, ID da chave %s Opções de verificação (com -V ou --verify): Versão necessária Nome do arquivo versionado não permitida Gravou: %s
 SIM Você deve definir o "%%_gpg_name" no seu arquivo de macro
 [nenhum] ] esperado no fim da matriz um conjunto de arquivos de ligação absoluta podem ser instalados sem estarem completos. o argumento não é um pacote RPM
 argumentos para --prefix devem começar com uma / os argumentos para --root (-r) devem começar com uma / iterador da matriz utilizado com diferentes tamanhos de matrizes data inválida no %%changelog: %s
 opção inválida "%s" em %s:%d
 construir pacote binário a partir do <pacote fonte> construir pacote binário somente a partir do <specfile> construir pacote binário somente a partir do <tarball> construir os pacotes fontes e binários a partir do <specfile> construir os pacotes fontes e binários a partir do <tarball> construir pacote fonte somente a partir do <specfile> construir pacote fonte somente a partir do <tarball> construir através de %build (%prep, então compile) a partir do <specfile> construindo através de %build (%prep, então compile) a partir do <tarball> construir através de %install (%prep, %build, então instale) a partir do <pacote fonte> construir através de %install (%prep, %build, então instale) a partir do <specfile> construir através de %install (%prep, %build, então instale) a partir do <tarball> construir através de %prep (desempacote os fontes e aplique as correções) a partir do <specfile> construir através de %prep (desempacote os fontes e aplique as correções) a partir do <tarball> buildroot já especificado, ignorando %s
 não foi possível criar o bloqueio de transação %s em %s (%s)
 não é possível adicionar o registro originalmente em %u
 Não foi possível criar %s: %s
 não foi possível obter o bloqueio %s em %s/%s
 não foi possível abrir %s em %s:%d: %m
 Não foi possível abrir %s: %s
 não foi possível abrir o banco de dados de pacotes em %s
 Não foi possível reabrir a carga útil: %s
 não é possível usar --prefix com --relocate ou --excludepath a criação do pacote falhou no arquivo %s: %s
 a criação do pacote falhou: %s
 a criação de um pipe para o --pipe falhou: %m
 depurar máquina de estados do arquivo depurar máquina de estados do arquivo de carga últil depurar E/S rpmio definir MACRO com valor EXPR remover a assinatura dos pacotes comparação de dependências suporta versões com til. diferente diretório exibir configuração final do rpmrc e do macro exibir etiquetas de consulta conhecidas exibir o estado dos arquivos listados não aceitar msgstr's i18N do specfile não executar o scriptlet %%post (se existir) não executar o scriptlet %%postun (se existir) não executar o scriptlet %%pre (se existir) não executar o scriptlet %%preun (se existir) não excutar fase %check da compilação não executar fase %clean da compilação não executar nenhum scriptlet %%triggerin não executar nenhum scriptlet %%triggerpostun não executar nenhum scriptlet %%triggerprein não executar nenhum scriptlet %%triggerun não executar nenhum scriptlet disparado por este pacote não executar nenhum estágio da construção não executar os scriptlet(s) do pacote não fazer glob com os argumentos não instalar arquivos de configuração não instalar documentação não processar arquivos que não são de pacotes como manifestos não reordenar a instalação dos pacotes para satisfazer as dependências não verificar dependências de construção não verificar as dependências do pacote não verificar o espaço em disco antes de instalar não executar script(s) de verificação não importe, mas diga se ele iria funcionar ou não não instalar contextos de segurança dos arquivos não instalar, mas dizer se a instalação funcionaria ou não não verifica as capacidades dos arquivos não verificar cabeçalho(s) do banco de dados ao recuperá-lo(s) não verificar o digest dos arquivos não verificar o digest dos arquivos (obsoleto) não verificar os contextos de segurança dos arquivos não verificar os arquivos do pacote não verificar o grupo dos arquivos não verificar a assinatura do cabeçalho+carga útil não verificar o modo dos arquivos não verificar a hora de modificação dos arquivos não verificar o proprietário dos arquivos não verificar a arquitetura do pacote não verificar as dependências do pacote não verificar o(s) assunto(s) dos pacotes não verificar o sistema operacional do pacote não verificar a(s) assinatura(s) do pacote não verificar o tamanho dos arquivos não verificar o caminho da ligação simbólica dos arquivos descarregar informações básicas do arquivo formato da etiqueta vazio nome da etiqueta vazio apagar remover (desinstalar) pacote erro ao criar o arquivo temporário %s: %m
 erro ao ler o arquivo %s
 erro ao ler o cabeçalho do pacote
 erro(%d) ao adicionar o registro de cabeçalho #%d
 erro (%d) ao alocar nova instância do pacote
 erro(%d) obtendo "%s" registros do índice %s: %s
 erro(%d) ao remover o registro de cabeçalho #%d
 erro (%d) ao remover o registro "%s" a partir de %s
 erro (%d) ao armazenar o registro "%s" em %s
 erro (%d) ao armazenar o registro #%d em %s
 erro:  caminhos de exclusão devem começar com uma / exclusivo a execução falhou
 "(" extra no rótulo do pacote: %s
 falhou falha ao criar o diretório falha ao criar o diretório %s: %s
 falha ao reconstruir o banco de dados: o banco de dados original permanece no lugar
 falha ao remover o diretório %s: %s
 falha ao substituir o banco de dados velho pela novo!
 falha ao iniciar %s: %m
 falhou ao gravar todo dado para %s: %s
 erro fatal:  arquivo o arquivo %s conflita entre a tentativa de instalação de %s e %s o arquivo %s da instalação de %s conflita com o arquivo do pacote %s o arquivo %s não pertence a nenhum pacote
 arquivo %s: %s
 o algoritmo digest é configurável por pacote Nome(s) de arquivo(s) armazenados como tuplas (dirName,baseName,dirIndex), não como um caminho. os arquivos somente podem ser realocados durante a instalação de pacotes gerar cabeçalho(s) de pacote com (legado) empacotamento rpm v3 a execução do gpg falhou (%d)
 o gpg falhou ao gravar a assinatura
 o grupo %s não contém nenhum pacote
 o grupo %s não existe - usando o root
 o cabeçalho #%u do banco de dados é inválido -- ignorando.
 As etiquetas de cabeçalho sempre são classificadas após serem carregadas. ignorar ExcludeArch: diretivas do arquivo spec ignorar conflitos de arquivos entre pacotes ilegal _docdir_fmt %s: %s
 tipo inválido de assinatura importar uma chave pública blindada entrada do %%changelog incompleta
 formato incorreto: %s
 Inicializar banco de dados instalar instala todos os arquivos, até configurações que poderiam ser ignoradas instalar a documentação instalar pacote(s) suporte interno para scripts lua. dependência inválida campo de largura inválido tipo de índice inválido %x em %s/%s
 número de pacote inválido: %s
 sintaxe inválida no arquivo lua: %s
 sintaxe inválida no script lua: %s
 sintaxe inválida no scriptlet lua: %s
 linha %d: %s
 linha %d: %s é obsoleto: %s
 linha %d: %s: %s
 linha %d: Opção inválida %s de %%setup: %s
 linha %d: Número %s inválido: %s
 linha %d: %s inválido: qualificadores: %s
 linha %d: formato BuildArchitecture inválido: %s
 linha %d: Argumento inválido para %%setup: %s
 linha %d: Número %s inválido: %u
 linha %d: Número inválido: %s
 linha %d: Opção inválida %s: %s
 linha %d: O docdir deve começar com "/": %s
 linha %d: Etiqueta vazia: %s
 linha %d: campo Epoch deve ser um número sem sinal: %s
 linha %d: Erro ao analisar %%description: %s
 linha %d: Erro ao analisar %%files: %s
 linha %d: Erro ao analisar %%políticas: %s
 linha %d: Erro ao analisar %s: %s
 linha %d: caractere inválido "%c" em: %s
 linha %d: caractere inválido em: %s
 linha %d: caractere inválido ".." em: %s
 linha %d: Etiqueta mal formada: %s
 linha %d: Somente subpacotes noarch são suportados: %s
 linha %d: O pacote não existe: %s
 linha %d: Os prefixos não podem terminar com "/": %s
 linha %d: Segundo %s
 linha %d: A etiqueta toma apenas um token: %s
 linha %d: Nomes em excesso: %s
 linha %d: %%if não fechado
 linha %d: Etiqueta desconhecida: %s
 linha %d: o script interno deve terminar com ">": %s
 linha %d: argumentos do interpretador não são permitidos em gatilhos: %s
 linha %d: o script deve começar com "/": %s
 linha %d: segundo %%prep
 linha %d: segundo %s
 linha %d: os disparadores devem ter --: %s
 linha %d: macro não fechada ou continuação de linha inválida
 linha %d: script interno não suportado: %s
 linha: %s
 listar todos os arquivos de configuração listar todos os arquivos de documentação listar arquivos do pacote lista de chaves no chaveiro RPM falha na conexão lua: %s
 falha no script lua: %s
 magic_load falhou: %s
 magic_open(0x%x) falhou: %s
 %s malformado: %s
 a alocação de memória (%u bytes) retornou NULL.
 miFreeHeader: ignorando faltando %c %s faltando "(" faltando no rótulo do pacote: %s
 ")" faltando no rótulo do pacote: %s
 ":" faltando (0x%02x encontrado) em %s:%d
 arquitetura faltando para %s em %s:%d
 nome da arquitetura faltando em %s:%d
 argumento faltando para %s em %s:%d
 Nome faltando no %%changelog
 segundo ":" faltando em %s:%d
 { faltando após % } faltando após %{ compartilhado pela rede compartilhado pela rede     nenhum argumento foi passado nenhum argumento fornecido para analisar nenhum argumento foi passado para consulta nenhum argumento foi passado para verificação nenhum dbpath foi definido nenhum dbpath foi definido
 nenhuma descrição no %%changelog
 nenhum pacote corresponde com %s: %s
 nenhum pacote fornece %s
 nenhum pacote requer %s
 nenhum disparador de pacote %s
 nenhum pacote foi passado para remoção nenhum pacote foi passado para instalação normal normal         não é um pacote rpm não é um pacote rpm
 não instalado não instalado  somente um tipo de consulta/verificação pode ser feita por vez apenas instalação e atualização pode ser forçada somente um modo principal pode ser especificado somente um entre --excludedocs e --includedocs pode ser especificado falha ao abrir %s: %s
 operar em rpms binários gerados por especificação (padrão) operar em rpm fonte gerado por especificação substituir raíz da construção substituir plataforma de destino o pacote %s (que é mais recente que o %s) já está instalado o pacote %s já está instalado o pacote %s é destinado para a arquitetura %s o pacote %s é destinado para o sistema operacional %s o pacote %s não está instalado
 o pacote %s não é realocável
 o pacote %s já foi adicionado, substituindo por %s
 o pacote %s já foi adicionado, ignorando %s
 o pacote não tem listas de proprietários nem de ids
 o pacote não tem listas de proprietários/grupos
 o nome-versão-lançamento do pacote não está fornecido implicitamente. a carga útil do pacote pode ser compactada utilizando bzip2. a carga útil do pacote pode ser compactada utilizando lzma. a carga útil do pacote pode ser compactada utilizando o xz. o(s) arquivo(s) da carga útil do pacote tem o prefixo "./". scriptlets de pacotes podem ser expandidos durante a instalação. scriptlets de pacotes podem acessar o banco de dados rpm durante a instalação. erro de análise na expressão
 analisar arquivo(s) spec para stdout o caminho %s no pacote %s não é realocável predefinir MACRO com valor EXPR exibir loops de dependências como aviso exibir cerquilhas a medida que o pacote instala (bom com -v) imprimir expansão do macro da EXPR exibir porcentagens na medida em que o pacote vai sendo instalado mostra a versão do rpm que está sendo usada fornece uma saída menos detalhada fornece uma saída mais detalhada a consulta ao specfile %s falhou, não foi possível analisá-lo
 consultar arquivo(s) de especificação consultar o(s) pacote(s) disparado pelo pacote consultar/verificar uma instância do cabeçalho consultar/verificar um arquivo de pacote consultar/verificar todos os pacotes consultar/verificar pacote(s) da transação de instalação consultar/verificar pacote(s) em um grupo consultar/verificar pacote(s) que detém o arquivo consultar/verificar pacote(s) com um identificador de cabeçalhos consultar/verificar pacote(s) com um identificador de pacotes consultar/verificar pacote(s) que fornecem uma dependência consultar/verificar pacotes que precisam de uma dependência ler <ARQUIVO:...> ao invés do(s) arquivo(s) padrão falha na leitura: %s (%d)
 leitura do symlink %s falhou: %s
 reconstruir as listas invertidas do banco de dados a partir dos cabeçalhos dos pacotes instalados o registro %u não pôde ser lido
 reinstalar se o pacote já estiver presente realocar arquivos do caminho <antigo> para o <novo> realocar arquivos em pacotes não realocáveis realocar o pacote para <dir>, se for realocável realocações devem começar com uma / realocações devem conter um = realocações devem conter uma / após o = remover todos os pacotes iguais ao <pacote> (normalmente um erro é gerado se o <pacote> especificou múltiplos pacotes) remover a árvore de construção quando terminar remover fontes ao finalizar remover specfile ao finalizar substituir arquivos em %s com arquivos de %s para recuperação substituído substituído       substituição %s falhou: %s
 modo checksig do rpm modo de consulta do rpm modo de verificação do rpm o rpmMkTemp falhou
 rpmdb: cabeçalho danificado #%u recuperado -- ignorando.
 rpmdbNextIterator: ignorando opções de desativação de scripts somente podem ser especificadas durante a instalação ou remoção de pacotes enviar stdout para CMD compartilhado atalho para --replacepkgs --replacefiles assinar pacote(s) assinar pacote(s) (idêntico ao --addsign) ignorar arquivos %%ghost ignorar arquivos com componentes principais <caminho>  pule direto para o estágio especificado (somente para c,i) ignorado ignorando %s - a transferência falhou
 o pacote fonte não contém um arquivo .spec
 um pacote fonte era esperado, mas um binário foi encontrado
 suporte para as capacidades de arquivo do POSIX.1e erro de sintaxe na expressão
 erro de sintaxe ao analisar &&
 erro de sintaxe ao analisar ==
 erro de sintaxe ao analisar ||
 o interpretador do scriptlet pode usar argumentos do cabeçalho. transação opções de desativação de disparador somente podem ser especificadas durante a instalação ou remoção de pacotes os tipos devem corresponder
 não foi possível ler a assinatura
 MACRO indefinida ] não esperado sinalizador de consulta não esperado formato de consulta não esperado fonte de pesquisa não esperada } não esperado erro desconhecido %d encontrado ao manipular o pacote %s etiqueta desconhecida etiqueta desconhecida: "%s"
 ( sem correspondência
 a descompactação do arquivo falhou %s%s: %s
 opção db não reconhecida: "%s" ignorado.
 versão do pacote RPM não suportada atualizar o banco de dados, mas não modificar o sistema de arquivos atualizar pacote(s) atualizar pacote(s) se já estiver(em) instalado(s) atualizar para uma versão mais antiga do pacote (--force em atualizações faz isso automaticamente) utilizar ROOT como o diretório de nível mais alto usar banco de dados no DIRECTORY utilizar o seguinte formato de consulta o usuário %s não existe - usando o root
 verificar seção %files do <specfile> verificar seção %files do <tarball> verificar arquivos do banco de dados verificar a(s) assinatura(s) do pacote esperando pelo bloqueio de transação %s em %s
 aviso:  cor errada cor errada    { esperado após : na expressão { esperado após ? na expressão | esperado no fim da expressão } esperado na expressão PRIu64 installing package %s needs %%cB on the %s filesystem installing package %s needs % inodes on the %s filesystem a instalação do pacote %s precisa de %%cB no sistema de arquivos %s a instalação do pacote %s precisa de % inodes no sistema de arquivos %s 