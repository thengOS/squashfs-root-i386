��          |      �          &   !  '   H  A   p  >   �  >   �  +   0  9   \     �  6   �  �   �  v   �  �  D  -     %   =  C   c  8   �  Z   �  /   ;  :   k  )   �  P   �    !  �   .	                   
                                    	    Change Screen Resolution Configuration Change the effect of Ctrl+Alt+Backspace Changing the Screen Resolution configuration requires privileges. Changing the effect of Ctrl+Alt+Backspace requires privileges. Could not connect to Monitor Resolution Settings DBUS service. Enable or disable the NVIDIA GPU with PRIME Enabling or disabling the NVIDIA GPU requires privileges. Monitor Resolution Settings Monitor Resolution Settings can't apply your settings. Monitor Resolution Settings has detected that the virtual resolution must be set in your configuration file in order to apply your settings.

Would you like Screen Resolution to set the virtual resolution for you? (Recommended) Please log out and log back in again.  You will then be able to use Monitor Resolution Settings to setup your monitors Project-Id-Version: screen-resolution-extra
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-02-18 12:57+0000
PO-Revision-Date: 2013-12-14 23:34+0000
Last-Translator: Fábio Nogueira <fnogueira@protonmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:34+0000
X-Generator: Launchpad (build 18115)
 Altera configurações de resolução de tela Altera o efeito do Ctrl+Alt+Backspace Mudar a configuração da resolução de tela  requer privilégios. Mudar o efeito do Ctrl+Alt+Backspace requer privilegios. Não foi possível conectar ao serviço DBUS de Configurações de Resolução do Monitor. Habilitar ou desabilitar a GPU NVIDIA com PRIME Habilitar ou desabilitar a GPU NVIDIA requer privilégios. Configurações de resolução do monitor Configurações de Resolução do Monitor não pode aplicar as suas definições As Configurações de Resolução do Monitor detectaram que a resolução virtual deve ser definida em seu arquivo de configuração, para aplicar as suas definições.

Você gostaria que a Resolução de Tela ajustasse a resolução virtual para você? (Recomendado) Por favor, encerre e inicie a sessão novamente.  Você terá então a possibilidade de utilizar as Configurações de Resolução do Monitor para ajustar os monitores 