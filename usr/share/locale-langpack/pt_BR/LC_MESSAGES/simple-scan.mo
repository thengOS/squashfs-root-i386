��    �      �  �   �      �     �     �               )     .     2     6     :     @  	   R     \  	   s     }     �     �     �     �     �     �  	   �     �     �               )     D     T     ]      f     �     �  :   �     �     �  !   �       4   !     V     b     t     �     �     �     �     �     �  	   �  
   �     �     �  1   �               3  
   B     M     _  5   e     �     �     �  )   �  *   �          '     ,     @     N     i     �     �     �     �     �  /   �  (   �  E     #   ^     �  
   �     �     �     �     �     �                    &  
   4  #   ?     c     x     �     �     �     �     �     �     �  :   �  8   4  M   m  O   �       <   "  :   _  N   �  c  �     M  h   l     �     �  (         4     U  -   j      �     �  
   �     �     �  	   �     �       	   
               !     )     /  
   5     @     S     `  
   f     q     �     �  �  �     m     t     �     �     �     �     �     �     �     �     �  !   �          &     ,     J     R     i     z     �  
   �     �     �     �     �          /  
   F  	   Q  )   [     �     �  N   �          
  )        C  F   I     �     �     �     �     �  
   �     �     �     �     �          !     &  F   5     |  '   �     �     �  "   �        C         T   !   b      �   '   �   (   �   
   �      �      �      !  )   !  *   >!     i!     q!     �!     �!     �!  3   �!  -   �!  ]   "  -   y"     �"     �"     �"  !   �"     �"     #     )#     C#     P#     \#     s#     �#  (   �#     �#     �#     $  .   $     K$     [$     u$     {$     �$  :   �$  7   �$  l   %  X   %  %   �%  N   �%  M   M&  Q   �&  �  �&  $   w)  ~   �)  /   *  +   K*  A   w*  3   �*  *   �*  ;   +  *   T+  1   +  
   �+  	   �+     �+  
   �+  	   �+     �+  
   �+     �+     ,     ,     ,     ,     #,     +,     A,     X,     `,     w,  !   �,    �,     %   T   E       4   $   w   Q   &   )   d   �          5   X   i   \   l   ]          0   8   A          @   y   [           }   1   M       �   b              a   �         >      G       e   C   P   D   m       U      .          g             Z             	       I   p   �                  �   f           ,      *   �       L      "   R   W       �   c      �       /   �      B                  q   t   h   �   =       �       V   '   z   +   H   
   3       K   |   Y   �         N       �               F   2      <   !   7           6   (   _   #          :                 �           `      J                   u   j   ?   n   {   O          x      k       r               ~       v   o   9   -       s      S   ;   ^    %d dpi %d dpi (default) %d dpi (draft) %d dpi (high resolution) 4×6 A_4 A_5 A_6 About About Simple Scan All Files All Pages From _Feeder Automatic Back Brightness of scan Brightness: Change _Scanner Combine sides Combine sides (reverse) Contrast of scan Contrast: Crop Crop the selected page Darker Device to scan from Directory to save files to Discard Changes Document Email... Error communicating with scanner Failed to save file Failed to scan Fix PDF files generated with older versions of Simple Scan Front Front and Back Height of paper in tenths of a mm Help If you don't save, changes will be permanently lost. Image Files JPEG (compressed) Keep unchanged Le_gal Less Lighter Maximum Minimum More Move Left Move Right New New Document No scanners available.  Please connect a scanner. No scanners detected PDF (multi-page document) PNG (lossless) Page Size: Page side to scan Photo Please check your scanner is connected and powered on Preferences Print debugging messages Print... Quality value to use for JPEG compression Quality value to use for JPEG compression. Quality: Quit Quit without Saving Reorder Pages Resolution for photo scans Resolution for text scans Reverse Rotate Left Rotate Right Rotate _Left Rotate _Right Rotate the page to the left (counter-clockwise) Rotate the page to the right (clockwise) Run '%s --help' to see a full list of available command line options. SANE device to acquire images from. Save Save As... Save current document? Save document before quitting? Save document to a file Saving document... Saving page %d out of %d Sc_an Scan Scan Documents Scan S_ource: Scan Side: Scan a single page from the scanner Scanned Document.pdf Show release version Simple Scan Simple document scanning tool Single _Page Start a new document Stop Stop the current scan Text The brightness adjustment from -100 to 100 (0 being none). The contrast adjustment from -100 to 100 (0 being none). The directory to save files to. Defaults to the documents directory if unset. The height of the paper in tenths of a mm (or 0 for automatic paper detection). The page side to scan. The resolution in dots-per-inch to use when scanning photos. The resolution in dots-per-inch to use when scanning text. The width of the paper in tenths of a mm (or 0 for automatic paper detection). This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. Type of document being scanned Type of document being scanned. This setting decides on the scan resolution, colors and post-processing. Unable to connect to scanner Unable to open help file Unable to open image preview application Unable to save image for preview Unable to start scan Username and password required to access '%s' Width of paper in tenths of a mm [DEVICE...] - Scanning utility _Authorize _Cancel _Close _Contents _Crop _Custom _Document _Email _Help _Letter _None _Page _Password: _Photo Resolution: _Rotate Crop _Save _Stop Scan _Text Resolution: _Username for resource: translator-credits Project-Id-Version: simple-scan
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-03-20 21:13+0000
PO-Revision-Date: 2014-12-30 19:08+0000
Last-Translator: Fábio Nogueira <fnogueira@protonmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:48+0000
X-Generator: Launchpad (build 18115)
 %d ppp %d dpi (padrão) %d dpi (rascunho) %d dpi (alta resolução) 4×6 A_4 A_5 A_6 Sobre Sobre o Digitalizador simples Todos os arquivos Todas as páginas do ali_mentador Automático Verso Luminosidade do digitalizador Brilho: Alterar _digitalizador Combine os lados Combine os lados (reverso) Contraste do digitalizador Contraste: Recortar Recortar a página selecionada Mais escuro Dispositivo de onde digitalizar Diretório para salvar arquivos Descartar alterações Documentos E-mail... Erro na comunicação com o digitalizador Falha ao salvar o arquivo Falha ao digitalizar Corrigir arquivos PDF gerados com versões anteriores do Digitalizador simples Frente Frente e verso Altura do papel em décimos de milímetro Ajuda Se você não salvar, as alterações serão permanentemente perdidas. Arquivos de imagem JPEG (comprimido) Manter inalterado _Ofício Menos Mais claro Máxima Mínima Mais Mover para a esquerda Mover para a direita Novo Novo documento Nenhum digitalizador disponível. Por favor, conecte um digitalizador. Nenhum digitalizador detectado PDF (documento com múltiplas páginas) PNG (sem perdas) Tamanho da página: Lado da página a ser digitalizado Foto Por favor verifique se o seu digitalizador está conectado e ligado Preferências Imprimir mensagens de depuração Imprimir... Qualidade para usar na compressão JPEG Qualidade para usar na compressão JPEG. Qualidade: Sair Sair sem salvar Reordenar páginas Resolução para digitalização de fotos Resolução para digitalização de textos Reverso Girar à esquerda Girar à direita Girar à _esquerda Girar à di_reita Girar a página à esquerda (sentido anti-horário) Girar a página à direita (sentido horário) Execute '%s --help' para ver a lista completa de opções disponíveis para linha de comando. Dispositivo de onde o SANE adquirire imagens. Salvar Salvar como... Salvar documento atual? Salvar o documento antes de sair? Salvar documento em arquivo Salvando documento... Salvando página %d de %d Digit_alizar Digitalizar Digitalizar documentos Origem da digitalizaçã_o: Lado a digitalizar: Digitalizar uma página no digitalizador Documento digitalizado.pdf Mostrar versão de lançamento Digitalizador simples Ferramenta simples para digitalizar documentos _Página única Iniciar um novo documento Parar Parar a digitalização atual Texto O ajuste de luminosidade de -100 para 100 (0 sendo nenhum) O ajuste do contraste de -100 para 100 (0 sendo nenhum) O diretório no qual salvar os arquivos. Se não configurado, o diretório Documentos é usado como padrão. A altura do papel em décimos de milímetro (ou 0 para detecção automática de papel). O lado da página a ser digitalizado. A resolução em pontos por polegadas a ser usada na digitalização de fotos. A resolução em pontos por polegada a ser usada na digitalização e textos. A largura do papel em décimos de mm (ou 0 para detecção automática de papel). Este programa é uma software livre: você pode redistribuí-lo e/ou modificá-lo
sob os termos da Licença Pública Geral GNU conforme publicada pela
Free Software Foundation, seja a versão 3 da licença, ou (se preferir)
alguma versão posterior.

Este programa é distribuído na expectativa de que seja útil, mas
SEM NENHUMA GARANTIA; nem mesmo a garantia implícita de COMERCIALIZAÇÃO
ou ADEQUAÇÃO PARA UMA FINALIDADE EM PARTICULAR. Veja a Licença Pública
Geral GNU para mais detalhes.

Você deve ter recebido uma cópia da Licença Pública Geral GNU juntamente a
este programa. Caso contrário, acesse <http://www.gnu.org/licenses/>. Tipo do documento a ser digitalizado Tipo de documento a ser digitalizado. Esta configuração define a resolução da digitalização, cores e pós-processamento. Não foi possível conectar-se ao digitalizador Não foi possível abrir o arquivo de ajuda Não foi possível abrir o aplicativo de visualização de imagem Não é possível salvar imagem para visualização Não foi possível iniciar o digitalizador É necessário o nome de usuário e senha para acessar '%s' Largura do papel em décimos de milímetro [DISPOSITIVO...] - Utilitário de digitalização _Autorizar _Cancelar Fe_char _Conteúdo Re_cortar _Personalizar _Documento _E-mail A_juda _Carta _Nenhum _Página _Senha: Resolução da _foto: Gira_r área recortada _Salvar Par_ar digitalização Resolução do _texto: Nome de _usuário para o recurso: Launchpad Contributions:
  Almufadado https://launchpad.net/~almufadado
  André Gondim https://launchpad.net/~andregondim
  Beatriz Vital https://launchpad.net/~vitalb
  Benjamim Gois https://launchpad.net/~benjamim-gois
  Celio Alves https://launchpad.net/~celio.alves
  Celio Ricardo Quaio Goetten https://launchpad.net/~celioric
  Celso H. L. S. Junior https://launchpad.net/~celsojunior
  Douglas Santos https://launchpad.net/~douglasrpg
  Fábio Nogueira https://launchpad.net/~fnogueira
  Matheus Pacheco de Andrade https://launchpad.net/~matheusp-andrade-deactivatedaccount
  Neliton Pereira Jr. https://launchpad.net/~nelitonpjr
  Pablo Diego Moço https://launchpad.net/~pablodm89
  Paulo José https://launchpad.net/~pauloup
  Paulo Pernomian https://launchpad.net/~paulopernomian
  Rafael Neri https://launchpad.net/~rafepel
  Rafael Zenni https://launchpad.net/~rafaeldz
  Salomão Carneiro de Brito https://launchpad.net/~salomaocar
  Tiago Hillebrandt https://launchpad.net/~tiagohillebrandt
  Vinicius Almeida https://launchpad.net/~vinicius-algo
  Vitor da Silva Gonçalves https://launchpad.net/~vitorsgoncalves
  Vítor Avelino https://launchpad.net/~vitoravelino
  andbelo https://launchpad.net/~andbelo
  kahue https://launchpad.net/~kahuemm
  millemiglia https://launchpad.net/~dnieper650 