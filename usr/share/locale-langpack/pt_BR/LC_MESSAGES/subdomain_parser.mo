��    U      �  q   l      0  "   1     T  8   t  <   �  5   �  >      $   _  2   �  B   �  2   �  >   -	  G   l	  =   �	  ?   �	  :   2
  0   m
  %   �
     �
     �
  $   �
  Z        r     �  )   �     �  w   �     n     �  *   �     �  #   �  !     "   3      V     w     �  )   �  &   �  E   �  H   @  $   �  '   �  .   �  -     !   3     U  "   k  <   �  B   �  C     *   R  C   }  *   �  C   �  *   0      [  ,   |  D   �     �  F     /   N  -   ~     �  ;   �     �     
  !   "  $   D     i     �     �      �     �     �               8     U  .   s  -   �     �  9   �     *     F  �  f     B  $   a  Q   �  N   �  S   '  V   {  1   �  ?     Q   D  >   �  B   �  s     S   �  c   �  H   D  :   �  +   �  !   �       /   1  q   a     �  "   �  0     !   G  �   i  &        )  /   C     s  -   �  #   �  $   �  "   
     -  "   M  ;   p  4   �  U   �  ]   7   %   �   5   �   3   �   G   %!  +   m!  #   �!  $   �!  F   �!  K   )"  S   u"  6   �"  M    #  .   N#  M   }#  .   �#  %   �#  3    $  O   T$     �$  K   �$  6   %  8   G%     �%  K   �%     �%     �%  &   &  (   2&     [&  !   q&      �&  %   �&     �&     �&     '  %   '  *   A'  +   l'  5   �'  1   �'  #    (  :   $(  $   _(  !   �(             >   P          D   %            K      1      8   O      ,       U   /   M   $      )   -       L   J   
              +   Q             '               *               R   	         !   :   9   5       0   4   #       ?          =       2          <               6   3           E       B      N         H   &       A      T   C   (              G       .          I   "   S      F              7   ;      @    !
Ensure that it has been loaded.
 %s: ASSERT: Invalid option: %d
 %s: Could not allocate memory for subdomain mount point
 %s: Could not allocate memory for subdomainbase mount point
 %s: Errors found during regex postprocess. Aborting.
 %s: Errors found in combining rules postprocessing. Aborting.
 %s: Errors found in file. Aborting.
 %s: Failed to compile regex '%s' [original: '%s']
 %s: Failed to compile regex '%s' [original: '%s'] - malloc failed
 %s: Illegal open {, nesting groupings not allowed
 %s: Internal buffer overflow detected, %d characters exceeded
 %s: Regex grouping error: Invalid close }, no matching open { detected
 %s: Regex grouping error: Invalid number of items between {}
 %s: Regex grouping error: Unclosed grouping, expecting close }
 %s: Sorry. You need root privileges to run this program.

 %s: Subdomain '%s' defined, but no parent '%s'.
 %s: Two SubDomains defined for '%s'.
 %s: Unable to add "%s".   %s: Unable to find  %s: Unable to parse input line '%s'
 %s: Unable to query modules - '%s'
Either modules are disabled or your kernel is too old.
 %s: Unable to remove "%s".   %s: Unable to replace "%s".   %s: Unable to write entire profile entry
 %s: Unable to write to stdout
 %s: Warning! You've set this program setuid root.
Anybody who can run this program can update your AppArmor profiles.

 %s: error near                %s: error reason: '%s'
 (ip_mode) Found unexpected character: '%s' Addition succeeded for "%s".
 AppArmor parser error, line %d: %s
 Assert: 'hat rule' returned NULL. Assert: `addresses' returned NULL. Assert: `netrule' returned NULL. Assert: `rule' returned NULL. Bad write position
 Couldn't copy profile Bad memory address
 Couldn't merge entries. Out of Memory
 Default allow subdomains are no longer supported, sorry. (domain: %s) Default allow subdomains are no longer supported, sorry. (domain: %s^%s) ERROR in profile %s, failed to load
 Error couldn't allocate temporary file
 Error: #include %s%c not found. line %d in %s
 Error: Can't add directory %s to search path
 Error: Could not allocate memory
 Error: Out of Memory
 Error: bad include. line %d in %s
 Error: could not allocate buffer for include. line %d in %s
 Error: exceeded %d levels of includes.  NOT processing %s include
 Exec qualifier 'i' invalid, conflicting qualifier already specified Exec qualifier 'i' must be followed by 'x' Exec qualifier 'p' invalid, conflicting qualifier already specified Exec qualifier 'p' must be followed by 'x' Exec qualifier 'u' invalid, conflicting qualifier already specified Exec qualifier 'u' must be followed by 'x' Found unexpected character: '%s' Internal: unexpected mode character in input Invalid mode, 'x' must be preceded by exec qualifier 'i', 'u' or 'p' Memory allocation error. Negative subdomain entries are no longer supported, sorry. (entry: %s) Network entries can only have one FROM address. Network entries can only have one TO address. Out of memory
 PANIC bad increment buffer %p pos %p ext %p size %d res %p
 Permission denied
 Profile already exists
 Profile does not match signature
 Profile doesn't conform to protocol
 Profile doesn't exist
 Profile version not supported
 Removal succeeded for "%s".
 Replacement succeeded for "%s".
 Unable to open %s - %s
 Unknown error
 Warning (line %d):  `%s' is not a valid ip address. `%s' is not a valid netmask. `/%d' is not a valid netmask. md5 signature given without execute privilege. missing an end of line character? (entry: %s) ports must be between %d and %d profile %s: has merged rule %s with multiple x modifiers
 unable to create work area
 unable to serialize profile %s
 Project-Id-Version: apparmor-parser
Report-Msgid-Bugs-To: <apparmor@lists.ubuntu.com>
POT-Creation-Date: 2005-03-31 13:39-0800
PO-Revision-Date: 2016-04-13 10:50+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Novell Language <language@novell.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:13+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
 !
Verifique se foi carregado.
 %s: DECLARAR: Opção inválida: %d
 %s: Não foi possível reservar memória para o ponto de montagem do subdomínio
 %s: Impossível alocar memória para ponto de montagem de base de subdomínio
 %s: Erros encontrados durante o processamento de expressões regulares. Abortando.
 %s: Erros encontrados no pós-processamento de regras de combinação. Interrompendo.
 %s: Erros encontrados no arquivo. Interrompendo.
 %s: Falha ao compilar expressão regular '%s' [original: '%s']
 %s: Falha ao compilar expressão regular '%s' [original: '%s'] - falha de malloc
 %s: Abertura ilegal {, agrupamentos aninhados não permitidos
 %s: Overflow de buffer interno detectado; %d caracteres excedidos
 %s: Erro de agrupamento de expressão regular: Fechamento inválido }; nenhuma abertura correspondente { detectada
 %s: Erro de agrupamento de expressão regular: Número inválido de itens entre {}
 %s: Erro de agrupamento de Expressão Regular: Agrupamento não fechado, esperado fechamento com }
 %s: Você precisa de privilégios de root para executar este programa.

 %s: Subdomínio '%s' definido, mas nenhum ancestral '%s'.
 %s: Dois SubDomínios definidos para '%s'.
 %s: Impossível adicionar "%s".   %s: Impossível localizar  %s: Impossível analisar linha de entrada '%s'
 %s: Impossível consultar módulos - '%s'
Os módulos podem estar desabilitados ou o seu kernel é muito antigo.
 %s: Impossível remover "%s".   %s: Impossível substituir "%s".   %s: Impossível gravar toda a entrada do perfil
 %s: Impossível gravar em stdout
 %s: Aviso! Você definiu o setuid deste programa como root.
Qualquer pessoa capaz de executar esse programa poderá atualizar seus perfis do AppArmor.

 %s: proximidade do erro                %s: motivo do erro: '%s'
 (ip_mode) Caractere inesperado encontrado: '%s' Adição bem-sucedida de "%s".
 Erro do analisador do AppArmor, linha %d: %s
 Declarar: 'hat rule' retornou NULL. Declarar: `addresses' retornou NULL. Declarar: `netrule' retornou NULL. Declarar: `rule' retornou NULL. Posição de gravação incorreta
 Impossível copiar perfil. Endereço de memória incorreto
 Impossível mesclar entradas. Memória Insuficiente
 Desculpe, a permissão de subdomínios padrão não é mais suportada. (Domínio: %s) Desculpe, as permissões de subdomínios padrão não são mais suportadas. (Domínio: %s^%s) ERRO no perfil %s. Falha ao carregar
 Erro não foi possível alocar o arquivo temporário
 Erro #include %s%c não encontrado. linha %d em %s
 Erro: Não foi posśivel adicionar o diretório %s ao caminho de busca
 Erro: Não foi possível alocar a memória
 Erro: Não há memória suficiente
 Erro: má inclusão. linha %d em %s
 Erro: não foi possível alocar o buffer para incluir. linha %d em %s
 Erro: %d níveis de inclusões excedidos. NÃO irá processar inclusão %s
 Qualificador de execução 'i' inválido. Qualificador em conflito já especificado Qualificador de execução 'i' deve ser seguido de 'x' Qualificador Exec 'p' inválido, já especificado um qualificador em conflito Qualificador Exec 'p' deve ser seguido por 'x' Qualificador Exec 'u' inválido, já especificado um qualificador em conflito Qualificador Exec 'u' deve ser seguido por 'x' Caractere inesperado encontrado: '%s' Interno: modo de caractere não esperado na entrada modo inválido, 'x' deve ser precedido por um qualificador exec 'i', 'u' ou 'p' Erro de alocação de memória. Entradas negativas de subdomínios não são mais suportadas. (Entrada: %s) As entradas de rede podem ter somente um endereço DE. As entradas de rede podem ter somente um endereço PARA. Memória insuficiente
 PÂNICO: buffer de incremento incorreto %p pos %p ext %p tamanho %d res %p
 Permissão negada
 O perfil já existe
 Perfil não corresponde à assinatura
 Perfil não compatível com o protocolo
 O perfil não existe
 Versão do perfil não suportada
 Remoção bem-sucedida de "%s".
 Substituição bem-sucedida de "%s".
 Impossível abrir %s - %s
 Erro desconhecido
 Aviso (linha %d):  `%s' não é um endereço IP válido. `%s' não é uma máscara de rede válida. `/%d' não é uma máscara de rede válida. assinatura md5 enviada sem privilégio de execução. caractere de fim de linha ausente?  (entrada: %s) portas precisam estar entre %d e %d perfil %s: mesclou a regra %s com vários modificadores x
 impossível criar área de trabalho
 impossível serializar perfil %s
 