��            )   �      �     �  <   �  9   �  3   (     \  /   y     �     �     �  2   �  '   #     K     h  $   w     �  0   �  0   �          3     P     k     }  7  �  �  �  M   y     �     �     �  �  �  -   �  _   �  F   ?  B   �     �  8   �        0   ,  $   ]  ;   �  0   �  $   �       7   %  %   ]  =   �  E   �  -     /   5  (   e     �      �  k  �  �  3  d         r      �      �          
                      	                                                                                                           -- Press any key to continue -- An Ispell program was not given in the configuration file %s Are you sure you want to throw away your changes? (y/n):  Conversion of '%s' to character set '%s' failed: %s Error initialising libvoikko Error initializing character set conversion: %s File: %s Incomplete spell checker entry Missing argument for option %s Parse error in file "%s" on line %d, column %d: %s Parse error in file "%s" on line %d: %s Parse error in file "%s": %s Replace with:  Unable to open configuration file %s Unable to open file %s Unable to open file %s for reading a dictionary. Unable to open file %s for writing a dictionary. Unable to open temporary file Unable to set encoding to %s Unable to write to file %s Unknown option %s Unterminated quoted string Usage: %s [options] [file]...
Options: [FMNLVlfsaAtnhgbxBCPmSdpwWTv]

 -F <file>  Use given file as the configuration file.

The following flags are same for ispell:
 -v[v]      Print version number and exit.
 -M         One-line mini menu at the bottom of the screen.
 -N         No mini menu at the bottom of the screen.
 -L <num>   Number of context lines.
 -V         Use "cat -v" style for characters not in the 7-bit ANSI
            character set.
 -l         Only output a list of misspelled words.
 -f <file>  Specify the output file.
 -s         Issue SIGTSTP at every end of line.
 -a         Read commands.
 -A         Read commands and enable a command to include a file.
 -e[e1234]  Expand affixes.
 -c         Compress affixes.
 -D         Dump affix tables.
 -t         The input is in TeX format.
 -n         The input is in [nt]roff format.
 -h         The input is in sgml format.
 -b         Create backup files.
 -x         Do not create backup files.
 -B         Do not allow run-together words.
 -C         Allow run-together words.
 -P         Do not generate extra root/affix combinations.
 -m         Allow root/affix combinations that are not in dictionary.
 -S         Sort the list of guesses by probable correctness.
 -d <dict>  Specify an alternate dictionary file.
 -p <file>  Specify an alternate personal dictionary.
 -w <chars> Specify additional characters that can be part of a word.
 -W <len>   Consider words shorter than this always correct.
 -T <fmt>   Assume a given formatter type for all files.
 -r <cset>  Specify the character set of the input.
 Whenever an unrecognized word is found, it is printed on
a line on the screen. If there are suggested corrections
they are listed with a number next to each one. You have
the option of replacing the word completely, or choosing
one of the suggested words. Alternatively, you can ignore
this word, ignore all its occurrences or add it in the
personal dictionary.

Commands are:
 r       Replace the misspelled word completely.
 space   Accept the word this time only.
 a       Accept the word for the rest of this session.
 i       Accept the word, and put it in your personal dictionary.
 u       Accept and add lowercase version to personal dictionary.
 0-9     Replace with one of the suggested words.
 x       Write the rest of this file, ignoring misspellings,
         and start next file.
 q       Quit immediately.  Asks for confirmation.
         Leaves file unchanged.
 ^Z      Suspend program.
 ?       Show this help screen.
 [SP] <number> R)epl A)ccept I)nsert L)ookup U)ncap Q)uit e(X)it or ? for help \ at the end of a string aiuqxr yn Project-Id-Version: tmispell-voikko
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-02-07 18:46+0200
PO-Revision-Date: 2010-03-24 11:09+0000
Last-Translator: Tiago Hillebrandt <tiagohillebrandt@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:10+0000
X-Generator: Launchpad (build 18115)
 -- Pressione qualquer tecla para continuar -- Nenhum programa de verificação ortográfica não foi definido no arquivo de configuração %s Tem certeza que deseja descartar suas alterações? (y: sim/n: não):  A conversão de '%s' para o conjunto de caracteres '%s' falhou: %s Erro ao inicializar libvoikko Erro ao iniciar conversão do conjunto de caracteres: %s Arquivo: %s Entrada de verificação ortográfica incompleta Faltando argumento para a opção %s Erro de análise no arquivo "%s" na linha %d, coluna %d: %s Erro de análise no arquivo "%s" na linha %d: %s Erro de análise no arquivo "%s": %s Substituir por:  Não foi possível abrir o arquivo de configuração %s Não foi possível abrir o arquivo %s Não é possível abrir o arquivo %s para ler um dicionário. Não é possível abrir o arquivo %s para escrever em um dicionário. Não é possível abrir o arquivo temporário Não é possível definir codificação para %s Não é possível escrever no arquivo %s Opção desconhecida %s Expressão citada não terminada Uso: %s [opções] [arquivo]...
Opções: [FMNLVlfsaAtnhgbxBCPmSdpwWTv]

 -F <arquivo> Usa o arquivo especificado como arquivo de configuração.

Os comandos a seguir são iguais para o ispell:
 -v[v] Exibe o número da versão e sai.
 -M Mini-menu de uma linha na parte inferior da tela.
 -N Sem mini-menu na parte inferior da tela.
 -L <num> Número de linhas de contexto.
 -V Usa o estilo "cat -v" para caracteres fora do conjunto
            ANSI 7-bit.
 -l Exibe apenas uma lista de palavras errôneas.
 -f <arquivo> Especifica o arquivo de saída.
 -s Emite um SIGTSTP ao final de cada linha.
 -a Lê comandos.
 -A Lê comandos e habilita um comando a incluir um arquivo.
 -e[e1234] Expande os afixos.
 -c Comprime os afixos.
 -D Esvazia as tabelas de afixos.
 -t A entrada está no formato TeX.
 -n A entrada está no formato [nt]roff.
 -h A entrada está no formato sgml.
 -b Cria arquivos de backup.
 -x Não cria arquivos de backup.
 -B Não permite palavras justapostas.
 -C Permite palavras justapostas.
 -P Não gera combinações entre raízes e afixos adicionais.
 -m Permite combinações entre raízes e afixos que não estão no dicionário.
 -S Organiza a lista de sugestões pela probabilidade de exatidão.
 -d <dict> Especifica um arquivo de dicionário alternativo.
 -p <file> Especifica um dicionário pessoal alternativo.
 -w <chars> Especifica caracteres adicionais que podem ser parte de uma palavra.
 -W <len> Considera sempre corretas as palavras mais curtas que o valor especificado.
 -T <fmt> Assume um tipo formatador definido para todos os arquivos.
 -r <cset> Especifica o conjunto de caracteres da entrada.
 Toda vez que uma palavra desconhecida é encontrada,
ela é exibida em uma linha na tela. Se existirem sugestões
de correção, elas serão listadas junto de um número para
cada uma. Você tem a opção de substituir a palavra
completamente ou escolher uma das palavras sugeridas.
Por outro lado, você pode ignorar esta palavra, ignorar todas
as suas ocorrências ou adicionar ela ao dicionário pessoal.

Os comandos são:
 r Substitui completamente a palavra errônea.
 espaço Aceita a palavra apenas desta vez.
 a Aceita a palavra pelo resto desta sessão.
 i Aceita a palavra e a adiciona ao seu dicionário pessoal.
 u Aceita e adiciona uma versão em minúsculas ao dicionário pessoal.
 0-9 Substitui com uma das palavras sugeridas.
 x Ignora os erros de ortografia no resto deste arquivo.
         e começa o próximo arquivo.
 q Sai diretamente. Pergunta por confirmação.
         Deixa o arquivo sem alterações.
 ^Z Suspende o programa.
 ? Exibe esta tela de ajuda.
 [SP] <número> R) Repetir A) Aceitar I) Inserir L) Buscar U) Uncap Q) Fechar X) Sair ou ? para ajuda \ no fim da string aiuqxr sn 