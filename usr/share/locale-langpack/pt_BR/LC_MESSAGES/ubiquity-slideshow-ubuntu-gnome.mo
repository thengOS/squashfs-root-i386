��          �      <      �  X   �     
  �     0     !   K     m     �     �  f   �       �   6     �  :   �       X   !     z  �   �  �  t  q   B     �  %  �  9   �	  #   &
     J
     b
     �
  �   �
       �   5     �  E   �       g   .     �    �                                       
      	                                                        <strong>The Activities Overview is an easy way to access all your basic tasks. </strong> Access for everyone At the heart of the Ubuntu GNOME philosophy is the belief that computing is for everyone. With advanced accessibility tools and options to change language, color scheme and text size, Ubuntu GNOME makes computing easy – whoever and wherever you are. Check <strong>your notifications</strong> easily Easily access your personal cloud Enjoy and have fun! Enjoy your music and videos Find even more software GNOME Videos allows you to easily watch videos from your computer, DVD, or streamed over the Internet. Have fun with your photos Having everything in one place is convenient and means that you don't have to learn your way around a maze of different technologies. Help and support Launch and manage <strong>your apps</strong> from the Dash Make the most of the web Rhythmbox lets you organize your music and listen to Internet radio, podcasts, and more. Simple and easy to use Ubuntu GNOME will seamlessly integrate with your online accounts, so that all your data can be accessed from the same place. This takes the work out of using online accounts and makes it easy to find things when you need them. Project-Id-Version: ubiquity-slideshow-ubuntu
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-03-24 23:23+0200
PO-Revision-Date: 2013-09-23 23:29+0000
Last-Translator: gabriell nascimento <gabriellhrn@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:43+0000
X-Generator: Launchpad (build 18115)
 <strong>A Visualização de atividades é uma maneira fácil de acessar todas as suas tarefas básicas. </strong> Acesso para todos No coração da filosofia do Ubuntu GNOME está a crença de que a computação é para todos. Com ferramentas avançadas de acessibilidade e opções para mudar o idioma, esquema de cores e o tamanho do texto, o Ubuntu GNOME torna a computação fácil - onde quer e quem quer que você seja. Verifique <strong>suas notificações</strong> facilmente Acesse facilmente sua nuvem pessoal Aproveite e divirta-se! Curta suas músicas e vídeos Encontre mais programas O GNOME Vídeos permite que você assista facilmente vídeos a partir do seu computador, DVD ou transmitidos através da Internet. Divirta-se com suas fotos Ter tudo em um só lugar é conveniente e significa que você não tem que aprender a lidar com um labirinto de diferentes tecnologias. Ajuda e suporte Lance e gerencie <strong>seus aplicativos</strong> a partir do Painel Aproveite o máximo da web O Rhythmbox permite que você organize suas músicas e ouça rádio na Internet, podcasts e muito mais. Simples e fácil de utilizar O Ubuntu GNOME irá integrar perfeitamente com suas contas online, de modo que todos os seus dados poderão ser acessados a partir do mesmo lugar. Isso tira o trabalho de usar contas online e torna mais fácil encontrar as coisas quando você precisa delas. 