��          �      l      �     �     �  
   �                          ,  	   =     G     N     S     a     o     ~  �   �  �   w     M     Y     e  �  l     ]     l  
   s     ~     �     �     �     �     �     �  
   �     �     �            	    �   %     	  
   	     %	                                       
                                                       	    %d min %d mins Cast Dimensions Director Directors Format Free Genre Genres More suggestions My videos Online Play Remote videos Search videos Show in Folder Size This is an Ubuntu search plugin that enables information from various video providers to be searched and displayed in the Dash underneath the Video header. If you do not wish to search these content sources, you can disable this search plugin. This is an Ubuntu search plugin that enables local videos to be searched and displayed in the Dash underneath the Video header. If you do not wish to search this content source, you can disable this search plugin. Uploaded by Uploaded on Videos Project-Id-Version: unity-lens-video
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-12 20:02+0000
PO-Revision-Date: 2013-12-15 18:59+0000
Last-Translator: Lucas Cunha de Alencar <lucas85cunha@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 19:08+0000
X-Generator: Launchpad (build 18115)
 %d min %d mins Elenco Dimensões Diretor Diretores Formato Livre Gênero Gêneros Mais sugestões Meus vídeos Disponível Reproduzir Vídeos remotos Buscar vídeos Mostrar na pasta Tamanho Este é um plugin de busca do Ubuntu que permite que informações de vários provedores de vídeo sejam buscadas e exibidas no Painel abaixo do título Vídeo. Se você não deseja realizar buscas usando essa fonte de conteúdo, você pode desabilitar esse plugin. Este é um plugin de busca do Ubuntu que permite que seus vídeos sejam buscados e exibidos no Painel abaixo do título Vídeo. Se você não deseja realizar buscas usando essa fonte de conteúdo, você pode desabilitar esse plugin. Enviado por Enviado em Vídeos 