��          \      �       �      �      �      �   ;   �      !  �   5       �  ,     �     �     �  H        L  �   a     D                                       Devhelp Search Devhelp Show Sorry, there are no Devhelp results that match your search. Technical Documents This is an Ubuntu search plugin that enables information from Devhelp to be searched and displayed in the Dash underneath the Code header. If you do not wish to search this content source, you can disable this search plugin. devhelp;dev;help;doc; Project-Id-Version: unity-scope-devhelp
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-28 09:22+0000
PO-Revision-Date: 2014-04-24 21:29+0000
Last-Translator: Marcelo Odir <Unknown>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:23+0000
X-Generator: Launchpad (build 18115)
 Devhelp Pesquisa do Devhelp Mostrar Desculpe,não há nenhum resultado do Devhelp que combine com sua busca. Documentos técnicos Este é um plugin de pesquisa do Ubuntu que permite que informações do Devhelp possam ser pesquisadas e exibidas pelo Dash. Se você não deseja pesquisar esse tipo de conteúdo, você pode desativar esse plugin de pesquisa. devhelp;dev;ajuda;doc; 