��    
      l      �       �   #   �   #        9      R     s  &   �     �      �  "   �  �    3   �  .   �     #      @     a  *   ~     �  +   �  /   �                               
          	    Dump all parameters for all objects Enumerate objects paths for devices Exit after a small delay Exit after the engine has loaded Get the wakeup data Monitor activity from the power daemon Monitor with detail Show extra debugging information Show information about object path Project-Id-Version: upower
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-06-15 11:24+0000
PO-Revision-Date: 2013-04-01 01:53+0000
Last-Translator: Neliton Pereira Jr. <nelitonpjr@gmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:49+0000
X-Generator: Launchpad (build 18115)
 Esvaziar todos os parâmetros para todos os objetos Enumerar os caminhos objetos para dispositivos Sair após um pequeno atraso Sair após o motor ser carregado Obter os dados de ativação Monitorar a atividade do daemon de energia Monitorar com detalhes Mostrar informações extras de depuração Mostrar informações sobre o caminho do objeto 