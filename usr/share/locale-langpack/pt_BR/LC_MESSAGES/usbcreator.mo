��    -      �  =   �      �     �     �  $     	   4     >     M     T  .   ]     �     �     �  0   �  
   �     �     �               0  n   E  
   �     �     �     �  
   �     �     �                     %     1     @     U  "   u  :   �  	   �  2   �  $     P   5  s   �  X   �     S  :   i  (   �  �  �     s
  *   �
  '   �
     �
     �
     �
  
     D        R     ^     o  2   �     �     �     �     �     �       ~   ,  
   �     �     �     �     �               1     C     P     X     f  $   s  &   �  /   �  U   �     E  E   R  #   �  V   �  �     s   �       [   7  5   �     (          $      -          ,   %                              *              )                   "      
   	            !      '              #                                    &                     +        %d%% complete All existing data will be lost. An uncaught exception was raised:
%s CD Images CD-Drive/Image Cancel Capacity Create a startup disk using a CD or disc image Device Disk Images Disk to use: Do you really want to quit the installation now? Erase Disk Finishing... Image the device Installation Complete Installation Failed Installation failed. Installation is complete.  You may now run Ubuntu on other computers by booting them with this drive inserted. Installing Label Make Startup Disk Mount a device OS Version Other... Quit the installation? Retry? Run KVM Size Starting up Starting up... Startup Disk Creator System policy prevents mounting System policy prevents running KVM System policy prevents writing a disk image to this device Test Disk The device is not large enough to hold this image. The extension "%s" is not supported. The installation failed.  Please see ~/.cache/usb-creator.log for more details.
 The installation is complete.  You may now reboot your computer with this device inserted to try or install Ubuntu. To try or install Ubuntu from a removable disk, it needs to be set up as a startup disk. Writing disk image... You must select both source image and target device first. allow writing to system-internal devices Project-Id-Version: usb-creator
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-01-22 19:49+0000
PO-Revision-Date: 2016-03-22 20:57+0000
Last-Translator: Fábio Nogueira <fnogueira@protonmail.com>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:36+0000
X-Generator: Launchpad (build 18115)
 %d%% concluído Todos os dados existentes serão perdidos. Um erro não previsto foi levantado:
%s Imagens de CD Unidade de CD/Imagem Cancelar Capacidade Cria um disco de inicialização usando um CD ou uma imagem de disco Dispositivo Imagens de disco Disco a ser usado: Você realmente deseja sair da instalação agora? Apagar disco Finalizando... Imagem do dispositivo Instalação concluída Falha na instalação Falha na instalação. Instalação concluída. Você agora pode usar o Ubuntu em outros computadores inicializando-os com esse dispositivo inserido. Instalando Rótulo Criar disco de inicialização Montar um dispositivo Versão do sistema operacional Outro... Sair da instalação? Tentar novamente? Executar KVM Tamanho Inicializando Iniciando... Criador de discos de inicialização Política do sistema impede a montagem A política do sistema impede execução do KVM Política do sistema impede a gravação de uma imagem de disco para este dispositivo Testar disco O dispositivo não é grande o suficiente para armazenar esta imagem. A extensão "%s" não é suportada. A instalação falhou. Por favor veja ~/.cache/usb-creator.log para maiores detalhes.
 A instalação está concluída. Você deve agora reiniciar seu computador, com este dispositivo conectado, para experimentar ou instalar o Ubuntu. Para testar ou instalar o Ubuntu a partir de um disco removível, é necessário configurá-lo como inicializável. Gravando imagem de disco... Você deve primeiramente selecionar tanto a imagem de origem como o dispositivo de destino. permitir escrever em dispositivos internos do sistema 