��    �      D  �   l      8     9     >     X  *   j  -   �     �     �  A   �  =   4  O   r     �  4   �  '   �     '     F     ^  (   u  $   �  5   �  )   �  0   #  >   T  7   �  	   �     �  ,   �  5        Q     a     }  %   �  
   �     �     �  =   �  �   $  C   �  #        1     K     a     ~  /   �     �  %   �  !   �          /     L  �   O  �     �   �  �   �  �   @  �   �  Z   �  @   �  L   .  �   {  �       �  5  �  )   +     U     u     �     �  /   �  E   �  +   3  5   _  *   �  .   �     �       8   ,     e     x  M   �  M   �     /     6     ?     [  #   x     �     �     �     �  )   �        +   <      h   :   �      �     �   �   �!  8   e"  =   �"  -   �"  )   
#  &   4#  '   [#    �#  �   �$  &   $%     K%     j%  *   {%  $   �%  x   �%  >   D&  B   �&  8   �&  \   �&  6   \'     �'     �'     �'  *   �'     �'     �'     �'     (     "(  )   *(  -   T(     �(     �(  !   �(  ,   �(     �(  B  )     P+  $   U+     z+  9   �+  E   �+     ,     ",  S   >,  M   �,  a   �,     B-  C   J-  :   �-  %   �-     �-     .  9   .  0   W.  7   �.  1   �.  ;   �.  H   ./  _   w/  	   �/     �/  -   �/  L   +0  %   x0  6   �0  8   �0  2   1     A1     M1     \1  R   n1  �   �1  N   z2  ,   �2      �2     3     .3     N3  /   m3     �3  +   �3  #   �3  .   �3  :   $4     _4  �   b4  �   @5  !  �5  �   7  �   �7  �   o8  o   Q9  Q   �9  Q   :  �   e:     �:      �=  S  >  8   j?     �?  ,   �?     �?     	@  7   @  Z   W@  <   �@  G   �@  *   7A  4   bA     �A  5   �A  :   �A     "B     6B  c   VB  c   �B     C  
   #C  %   .C  4   TC  6   �C     �C  )   �C     �C  .   D  .   DD  $   sD  8   �D  +   �D  O   �D     ME    ZE  �   aF  =   �F  @   <G  *   }G  %   �G  %   �G  $   �G    H  �   /I  .   �I  *   *J     UJ     iJ  +   �J  �   �J  J   JK  i   �K  1   �K  z   1L  :   �L  9   �L     !M  	   (M  >   2M     qM     xM  )   M     �M     �M  )   �M  2   �M  o  N     �R     �R  +   �R  0   �R     ?      �   {       X   �                  H   Y   3       @   r      Z       ;   p           q       d   +   w   P   t   x           ,         8             W   :               �   C               z              i       O   >                  0      -       G   
       |   E   ~      &       J   o   u   "   `   v   (   g           A      }   a          U   9   6   _   n           s   4              )             F         h   N   b   l      c   =   Q   f   R       B      �       7   #   y   M       *           K   D             j   I   �   V       5   ]   !          $   S   	           <   k   �   .      /      2   \   1   ^       '   e   L   %   m       [            T        or  %s's remote desktop on %s '%s' disconnected '%s' is remotely controlling your desktop. '%s' rejected the desktop sharing invitation. - Updates Vino password - VNC Server for GNOME A user on the computer '%s' is remotely controlling your desktop. A user on the computer '%s' is remotely viewing your desktop. A user on the computer '%s' is trying to remotely view or control your desktop. Al_ways All remote users will be disconnected. Are you sure? Allow other users to _view your desktop Allowed authentication methods Alternative port number An error has occurred: Another user is controlling your desktop Another user is viewing your desktop Application does not accept documents on command line Are you sure you want to disconnect '%s'? Are you sure you want to disconnect all clients? Automatically _configure UPnP router to open and forward ports Can't pass document URIs to a 'Type=Link' desktop entry Cancelled Changing Vino password.
 Checking the connectivity of this machine... Choose how other users can remotely view your desktop Desktop Sharing Desktop Sharing Preferences Desktop sharing is enabled Disable connection to session manager Disconnect Disconnect %s Disconnect all E-mail address to which the remote desktop URL should be sent ERROR: Maximum length of password is %d character. Please, re-enter the password. ERROR: Maximum length of password is %d characters. Please, re-enter the password. ERROR: You do not have enough permissions to change Vino password.
 Enable remote access to the desktop Enter new Vino password:  Error displaying help Error displaying preferences Error initializing libnotify
 Error while displaying notification bubble: %s
 FILE Failed to open connection to bus: %s
 File is not a valid .desktop file GNOME Desktop Sharing GNOME Desktop Sharing Server ID If not set, the server will listen on all network interfaces.

Set this if you want to accept connections only from some specific network interface. For example, eth0, wifi0, lo and so on. If true, allows remote access to the desktop via the RFB protocol. Users on remote machines may then connect to the desktop using a VNC viewer. If true, do not use the XDamage extension of X.org. This extension does not work properly on some video drivers when using 3D effects. Disabling it will make Vino work in these environments, with slower rendering as a side effect. If true, remote users accessing the desktop are not allowed access until the user on the host machine approves the connection. Recommended especially when access is not password protected. If true, remote users accessing the desktop are only allowed to view the desktop. Remote users will not be able to use the mouse or keyboard. If true, remote users accessing the desktop are required to support encryption. It is highly recommended that you use a client which supports encryption unless the intervening network is trusted. If true, request that a UPnP-capable router should forward and open the port used by Vino. If true, show a notification when a user connects to the system. If true, the screen will be locked after the last remote client disconnects. If true, the server will listen on another port, instead of the default (5900). The port must be specified in the 'alternative-port' key. Licensed under the GNU General Public License Version 2

Vino is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

Vino is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.
 Listen on an alternative port Lists the authentication methods with which remote users may access the desktop.

There are two possible authentication methods; "vnc" causes the remote user to be prompted for a password (the password is specified by the vnc-password key) before connecting and "none" which allows any remote user to connect. Lock the screen when last user disconnect Network interface for listening Nobody can access your desktop. Not a launchable item Notify on connect One person is connected %d people are connected One person is viewing your desktop %d people are viewing your desktop Only allow remote users to view the desktop Others can access your computer using the address %s. Password required for "vnc" authentication Prompt the user before completing a connection Received signal %d, exiting. Remote desktop sharing password Remote users are able to control your mouse and keyboard Require encryption Retype new Vino password:  Run 'vino-passwd --help' to see a full list of available command line options Run 'vino-server --help' to see a full list of available command line options Screen Security Session management options: Share my desktop information Share your desktop with other users Sharing Show Notification Area Icon Show Vino version Show session management options Some of these preferences are locked down Sorry, passwords do not match.
 Specify file containing saved configuration Specify session management ID Start in tube mode, for the ‘Share my Desktop’ feature Starting %s The password which the remote user will be prompted for if the "vnc" authentication method is used. The password specified by the key is base64 encoded.

The special value of 'keyring' (which is not valid base64) means that the password is stored in the GNOME keyring. The port which the server will listen to if the 'use-alternative-port' key is set to true. Valid values are in the range of 5000 to 50000. The remote user '%s' will be disconnected. Are you sure? The remote user from '%s' will be disconnected. Are you sure? The router must have the UPnP feature enabled The screen on which to display the prompt There was an error displaying help:
%s There was an error showing the URL "%s" This key controls the behavior of the status icon. There are three options: "always" - the icon will always be present; "client" - the icon will only be present when someone is connected (this is the default behavior); "never" - the icon will not be present. This key specifies the e-mail address to which the remote desktop URL should be sent if the user clicks on the URL in the Desktop Sharing preferences dialog. Unrecognized desktop file Version '%s' Unrecognized launch option: %d VINO Version %s
 Waiting for '%s' to connect to the screen. When the status icon should be shown When true, disable the desktop background and replace it with a single block of color when a user successfully connects. Whether a UPnP router should be used to forward and open ports Whether to disable the desktop background when a user is connected Whether we should disable the XDamage extension of X.org Your XServer does not support the XTest extension - remote desktop access will be view-only
 Your desktop is only reachable over the local network. Your desktop will be shared _About _Allow _Allow other users to control your desktop _Help _Never _Only when someone is connected _Preferences _Refuse _Require the user to enter this password: _You must confirm each access to this machine translator-credits vino-mdns:showusername vino-passwd: password unchanged.
 vino-passwd: password updated successfully.
 vnc;share;remote; Project-Id-Version: vino
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=vino&keywords=I18N+L10N&component=Preferences Dialog
POT-Creation-Date: 2016-02-18 07:50+0000
PO-Revision-Date: 2016-02-22 14:42+0000
Last-Translator: Enrico Nicoletto <liverig@gmail.com>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 17:21+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
  ou  Área de trabalho remota de %s em %s "%s" desconectou "%s" está controlando sua área de trabalho remotamente. "%s" rejeitou seu convite para compartilhamento da área de trabalho. - Muda a senha do Vino - Servidor VNC para o GNOME Um usuário no computador "%s" está controlando sua área de trabalho remotamente. Um usuário no computador "%s" está vendo sua área de trabalho remotamente. Um usuário no computador "%s" está tentando ver ou controlar sua área de trabalho remotamente. Se_mpre Todos os usuários remotos serão desconectados. Você tem certeza? Permitir que outros usuários _vejam sua área de trabalho Métodos de autenticação permitidos Número da porta alternativa Ocorreu um erro: Um outro usuário está controlando sua área de trabalho Outro usuário está vendo sua área de trabalho O aplicativo não aceita documentos na linha de comando Você tem certeza de que deseja desconectar "%s"? Você tem certeza que deseja desconectar todos os clientes? Automaticamente _configurar roteador UPnP para abrir e encaminhar portas Impossível passar URIs de documento para uma entrada do tipo "Type=Link" em um arquivo desktop Cancelado Alterando a senha do Vino.
 Verificando a conectividade desta máquina... Defina como outros usuários podem acessar sua área de trabalho remotamente Compartilhamento da área de trabalho Preferências do Compartilhamento da área de trabalho O compartilhamento de área de trabalho está habilitado Desabilita a conexão com o gerenciador de sessão Desconectar Desconectar %s Desconectar todos Endereço de e-mail para o qual o URL da área de trabalho remota deve ser enviada ERRO: Comprimento máximo da senha é de %d caractere. Por favor, digite novamente a senha. ERRO: Comprimento máximo da senha é de %d caracteres. Por favor, digite novamente a senha. ERRO: Você não possui permissões suficientes para alterar a senha do Vino.
 Habilitar acesso remoto à área de trabalho Entre com a nova senha do Vino:  Erro ao exibir a ajuda Erro ao exibir as preferências Erro ao inicializar libnotify
 Erro ao exibir a mensagem de notificação: %s
 ARQUIVO Falha ao abrir conexão com o serviço: %s
 Não é um arquivo .desktop válido Compartilhamento da área de trabalho do GNOME Servidor de compartilhamento da área de trabalho do GNOME ID Se não definido, o servidor irá escutar em todas as interfaces de rede.

Defina isto apenas se você deseja apenas aceitar conexões vindas de interfaces de rede específicas. Por exemplo: eth0, wifi0, lo e muitas mais. Permitir acesso remoto à área de trabalho via o protocolo RFB. Usuários em máquinas remotas poderão então se conectar à área de trabalho usando um visualizador VNC. Se verdadeiro, não será usado a extensão XDamage do X.org. Esta extensão não funciona corretamente em alguns drivers de vídeo quando são usados efeitos 3D. Desativar isto fará com que o Vino funcione nestes ambientes, porém com uma renderização mais lenta como efeito colateral. Usuários remotos acessando a área de trabalho não poderão acessar até que o usuário na máquina acessada aprove a conexão. Recomendado especialmente quando o acesso não é protegido por senha. Se verdadeiro, a área de trabalho será a única coisa que usuários remotos poderão ver. Usuários remotos não poderão usar o mouse ou o teclado. Se verdadeiro, usuários remotos acessando a área de trabalho precisarão ter suporte a criptografia. É altamente recomendado que você use um cliente com suporte a criptografia, a não ser que a rede envolvida seja segura. Se verdadeiro, solicita que um roteador compatível com UPnP deva encaminhar e usar as portas usadas pelo Vino. Se verdadeiro, mostra uma notificação quando um usuário conecta-se ao sistema. Se verdadeiro, a tela será bloqueada após o último cliente remoto desconectar. Se verdadeiro, o servidor irá escutar em outra porta, ao invés da padrão (5900). A porta deve ser especificada na chave "alternative-port". Licenciado sob a Licença Pública Geral GNU Versão 2

O Vino é software livre; você pode redistribuí-lo e/ou modificá-lo sob os 
termos da Licença Pública Geral GNU, conforme publicada pela Free Software 
Foundation; tanto a versão 2 da Licença como (a seu critério) qualquer versão
mais nova.

O Vino é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA;
sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER
PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais
detalhes.

Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este
programa; caso contrario, escreva para a Free SoftwareFoundation, Inc., 51 Franklin
St, Fifth Floor, Boston, MA02110-1301 USA.
 Escutar em uma porta alternativa Lista os métodos de autenticação com os quais usuários remotos podem acessar a área de trabalho.

Existem dois métodos de autenticação possíveis; "vnc" faz com que seja pedida uma senha ao usuário remoto (a senha é especificada pela chave vnc-password) antes de conectar e "none" que permite a qualquer usuário remoto conectar. Bloquear a tela quando o último usuário se desconectar Interface de rede para escuta Ninguém pode acessar sua área de trabalho. Não é um item executáel Notificar ao conectar Uma pessoa está conectada %d pessoas estão conectadas Uma pessoa está vendo sua área de trabalho %d pessoas estão vendo sua área de trabalho Permitir somente usuários remotos a ver a área de trabalho Outros usuários podem acessar seu computador através do endereço %s. Senha exigida para autenticação do "vnc" Notificar o usuário antes de completar uma conexão Recebeu sinal %d, saindo. Senha de compartilhamento da área de trabalho remota Usuários remotos poderão controlar o seu mouse e teclado Exigir criptografia Redigite a nova senha do Vino:  Execute "vino-passwd --help" para ver a lista completa de opções disponíveis da linha de comando Execute "vino-server --help" para ver a lista completa de opções disponíveis da linha de comando Tela Segurança Opções do gerenciamento de sessão: Compartilhar minha informação da área de trabalho Compartilhe sua área de trabalho com outros usuários Compartilhamento Mostrar ícone da área de notificações Mostrar a versão do Vino Mostra as opções do gerenciamento de sessão Algumas destas preferências estão bloqueadas Desculpe, as senhas não coincidem.
 Especifique o arquivo que contém a configuração salva Especifica o ID do gerenciamento de sessão Iniciar no modo "tube", para o recurso 'Compartilhando minha área de trabalho' Iniciando %s A senha que será pedida ao usuário remoto se o método de autenticação "vnc" for usado. A senha especificada pela chave é codificada em base64.

O valor especial do 'chaveiro' (a qual não é base64) significa que a senha está armazenada no chaveiro GNOME. A porta que o servidor irá escutar se a chave "use-alternative-port" estiver definida como verdadeira. Os valores válidos estão na faixa de 5000 a 50000. O usuário remoto "%s" será desconectado. Você tem certeza? O usuário remoto em "%s" será desconectado. Você tem certeza? O roteador deve ter a opção UPnP ativada A tela na qual exibir a notificação Ocorreu um erro ao exibir a ajuda:
%s Ocorreu um erro ao exibir a URL "%s" Esta chave controla o comportamento do ícone de status. Existem três opções: "sempre" - o ícone sempre estará presente; "cliente" - o ícone só estará presente quando alguém estiver conectado (este é o comportamento padrão); "nunca" - o ícone não estará presente. Esta chave especifica o endereço de e-mail para o qual a URL da área de trabalho remota deve ser enviada se o usuário clicar nela no diálogo de preferências de compartilhamento da área de trabalho. Arquivo desktop não reconhecido, Versão "%s" Opção de execução não reconhecida: %d Versão do Vino %s
 Esperando "%s" conectar-se. Quando o ícone de status deve ser mostrado Quando verdadeiro, desabilita o plano de fundo da área de trabalho substituindo-o por um bloco único de cor quando um usuário conecta-se com sucesso. Se um roteador UPnP deve ou não ser usado para encaminhar e abrir portas. Se o plano de fundo da área de trabalho deve ou não ser desabilitado quando um usuário está conectado Se devemos desativar a extensão XDamage do X.Org Seu servidor X não tem suporte a extensão XTest - o acesso à área de trabalho remota será apenas para visualização
 Sua máquina é acessível somente através da rede local. Se selecionado, sua área de trabalho será compartilhada _Sobre _Permitir Permitir que outros usuários _controlem sua área de trabalho Aj_uda _Nunca _Somente quando alguém estiver conectado _Preferências _Recusar _Exigir que o usuário digite esta senha: Você _deve confirmar cada acesso à esta máquina Raphael Higino <In memoriam>
Igor Pires Soares <igor@projetofedora.org>
Leonardo Ferreira Fontenelle <leo.fontenelle@gmail.com>
Jonh Wendell <wendell@bani.com.br>
Og Maciel <ogmaciel@gnome.org>
Vladimir Melo <vladimirmelo.psi@gmail.com>
Fábio Nogueira <deb-user-ba@ubuntu.com>
Enrico Nicoletto <liverig@gmail.com>

Launchpad Contributions:
  André Gondim https://launchpad.net/~andregondim
  Celio Alves https://launchpad.net/~celio.alves
  Enrico Nicoletto https://launchpad.net/~liverig
  Erick Major https://launchpad.net/~erick-major
  Fábio Nogueira https://launchpad.net/~fnogueira
  Jonh Wendell https://launchpad.net/~wendell
  Leonardo Ferreira Fontenelle https://launchpad.net/~leonardof
  Og Maciel https://launchpad.net/~ogmaciel
  Rafael Fontenelle https://launchpad.net/~rffontenelle
  Rafael Neri https://launchpad.net/~rafepel
  Raphael Higino https://launchpad.net/~raphaelh
  Raphael Valencio https://launchpad.net/~valencio
  Rodrigo Padula de Oliveira https://launchpad.net/~rodrigopadula
  Tiago Hillebrandt https://launchpad.net/~tiagohillebrandt
  Vladimir Melo https://launchpad.net/~vmelo-deactivatedaccount vino-mdns:showusername vino-passwd: senha inalterada.
 vino-passwd: senha atualizada com sucesso.
 vnc;compartilhamento;compratilhar;remoto;remota; 