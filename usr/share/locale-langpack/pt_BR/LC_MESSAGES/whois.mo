��            )   �      �     �  <   �  >   �  �  9     �     �       S   !     u     �     �     �     �  "   �  1     
   C     N  F   c     �     �  &   �  =   �  T   6     �      �  3   �  r   �  \   e	  "   �	  �  �	  #   �  B   �  H     �  N          &  #   <  X   `     �     �     �          &  %   ?  <   e     �     �  X   �  )        >  +   Y  G   �  U   �     #  #   <  9   `  �   �  �   1  !   �                               	              
                                                                                            

Found a referral to %s.

 
Querying for the IPv4 endpoint %s of a 6to4 IPv6 address.

 
Querying for the IPv4 endpoint %s of a Teredo IPv6 address.

       -m, --method=TYPE     select method TYPE
      -5                    like --method=md5
      -S, --salt=SALT       use the specified SALT
      -R, --rounds=NUMBER   use the specified NUMBER of rounds
      -P, --password-fd=NUM read the password from file descriptor NUM
                            instead of /dev/tty
      -s, --stdin           like --password-fd=0
      -h, --help            display this help and exit
      -V, --version         output version information and exit

If PASSWORD is missing then it is asked interactively.
If no SALT is specified, a random one is generated.
If TYPE is 'help', available methods are printed.

Report bugs to %s.
 %s/tcp: unknown service Available methods:
 Cannot parse this line: %s Catastrophic error: disclaimer text has been changed.
Please upgrade this program.
 Host %s not found. Illegal salt character '%c'.
 Interrupted by signal %d... Invalid method '%s'.
 Invalid number '%s'.
 Method not supported by crypt(3).
 No whois server is known for this kind of object. Password:  Query string: "%s"

 This TLD has no whois server, but you can access the whois database at This TLD has no whois server. Timeout. Try '%s --help' for more information.
 Unknown AS number or IP network. Please upgrade this program. Usage: mkpasswd [OPTIONS]... [PASSWORD [SALT]]
Crypts the PASSWORD using crypt(3).

 Using server %s.
 Version %s.

Report bugs to %s.
 Warning: RIPE flags used with a traditional server. Wrong salt length: %d byte when %d <= n <= %d expected.
 Wrong salt length: %d bytes when %d <= n <= %d expected.
 Wrong salt length: %d byte when %d expected.
 Wrong salt length: %d bytes when %d expected.
 standard 56 bit DES-based crypt(3) Project-Id-Version: whois 4.7.5
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-08 17:14+0000
PO-Revision-Date: 2011-12-13 23:11+0000
Last-Translator: Douglas Moura <Unknown>
Language-Team: Portuguese/Brazil
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 17:22+0000
X-Generator: Launchpad (build 18115)
Language: pt_BR
 

Achado uma referência para %s.

 
Procurando pela extremidade IPv4 %s de um endereço IPv6. 6to4.

 
Pesquisando pelos pontos finais IPv4  %s de um endereço Teredo IPv6.

       -m, --method=TIPO        selecione o TIPO de método
      -5   o mesmo que --method=md5
      -S, --salt=SALT              usa o SALT especificado
      -R, --rounds=NUMBER    usa o NUMBER especificado de vezes
      -P, --password-fd=NUM   lê a senha no arquivo NUM descrito
                            ao invés de /dev/tty
      -s, --stdin     o mesmo que --password-fd=0
      -h, --help      imprime essa ajuda e sai
      -V, --version  imprime a versão e sai

Se o PASSWORD estiver faltando será requisitado de modo interativo
Se SALT não for especificado, será gerado um aleatório
Se TYPE for 'help', todos os métodos disponíveis serão imprimidos.

Relatar erros para %s.
 %s/tcp: serviço desconhecido Método disponível:
 Não pôde processar esta linha: %s Erro catastrófico: o texto de aviso foi modificado.
Por favor, atualize este programa.
 Host %s não encontrado. Caractere salt ilegal '%c'.
 Interrompido pelo sinal %d... Método inválido '%s'.
 Número inválido '%s'.
 Método não suportado por crypt(3).
 Nenhum servidor whois é conhecido para este tipo de objeto. Senha:  Consulta: "%s"

 Este TLD não tem servidor de whois, mas você pode acessar o banco de dados de whois em Não existe servidor whois para este TLD. Limite de Tempo Estourado. Tente '%s --help' para mais informações.
 Número AS ou rede IP desconhecidos. Por favor, atualize este programa. Uso: mkpassword [OPÇÕES]... [SENHA [SALTO]]
Encripta a SENHA utilizando crypt(3).

 Utilizando servidor %s.
 Versão %s.

Reporte bugs para %s.
 Aviso: RIPE flags utilizados com um servidor tradicional. Tamanho do salt incorreto: %d byte enquanto %d <= n <= %d eram esperados.
 Tamanho do salt incorreto: %d bytes enquanto %d <= n <= %d eram esperados.
 Tamanho do salt incorreto: %d byte enquanto %d eram esperados.
 Tamanho do salt incorreto: %d bytes enquanto %d eram esperados.
 padrão 56 bit DES-based crypt(3) 