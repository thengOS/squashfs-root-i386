��    �     �  K  �      �$  ?   �$     �$  4   %     N%  3   e%     �%  &   �%     �%     �%  x   &  H   �&  m   �&  =   ;'  {   y'  K   �'  p   A(  -   �(  #   �(  2   )     7)  '   U)  2   })     �)     �)     �)     �)  $   *     >*     G*  0   S*     �*     �*     �*     �*     �*  
   �*  
   �*     �*     +  "   &+     I+     R+     g+     s+  #   +     �+      �+     �+  %   �+     ,     $,  *   5,     `,     z,  <   �,  *   �,     �,     -     (-     8-     M-  %   f-      �-     �-     �-  (   �-     .  %   .  &   @.     g.      �.     �.     �.     �.     �.     �.     �.     /  &   ./  $   U/     z/     �/     �/     �/     �/  !   �/     0  	    0  
   *0     50  +   L0  /   x0  (   �0  $   �0     �0  3   1  &   E1     l1  9   �1  9   �1  4   �1  &   +2  )   R2     |2  '   �2     �2     �2     �2     3     3     &3     83     P3  #   g3  $   �3     �3     �3     �3     �3     4     4  "   '4     J4     W4     g4     �4     �4     �4  $   �4     �4     �4  	   �4     5  O   5  /   W5     �5  %   �5     �5     �5     �5     �5  A   6     D6  )   c6  3   �6  ,   �6  2   �6  3   !7      U7  )   v7     �7     �7  	   �7  /   �7  @   �7     ?8     P8     m8  "   t8  4   �8     �8     �8  7   �8  1   /9  <   a9  >   �9  7   �9  $   :  >   ::  '   y:  :   �:  M   �:     *;  A   0;     r;  5   �;  B   �;  R   �;  !   Q<  1   s<     �<  $   �<  $   �<  W   =  /   g=  0   �=  $   �=  .   �=     >     0>  %   A>     g>     y>     �>  %   �>     �>     �>     �>     �>     ?     *?     ??     X?  5   u?  E   �?  W   �?  '   I@     q@  3   �@  #   �@  *   �@     A  #   $A  6   HA     A  $   �A     �A     �A     �A     �A  	   �A     �A     �A     B  k   ,B     �B     �B  '   �B     �B  
   C      C     -C  '   <C     dC     yC  2   �C  8   �C  (   �C  *   (D     SD  &   pD     �D  "   �D      �D  *   �D  2   E  /   ME     }E     �E     �E     �E     �E     �E     F     .F     GF  1   UF     �F     �F     �F     �F     �F     �F  M   	G  M   WG     �G     �G  B   �G     H  /   	H     9H  !   TH     vH     �H     �H     �H  !   �H  0   �H  2   I     NI     \I     sI     �I  m   �I  m   J     rJ  
   �J     �J  #   �J     �J  	   �J  #   �J  	   �J  
   K  !   K  =   2K  ;   pK  )   �K     �K     �K     L  6   /L     fL     oL  (   vL  7   �L     �L     �L     �L      �L     M      'M  5   HM     ~M  0   �M  (   �M  -   �M  >   N  ,   UN  +   �N  .   �N  .   �N     O  '   O  3   EO  *   yO     �O  *   �O  [   �O  &   @P  )   gP  &   �P  @   �P  ?   �P  5   9Q  "   oQ  K   �Q     �Q  	   �Q     �Q     �Q  &   R  (   ,R     UR     [R     `R     R     �R  +   �R     �R     �R     �R     S     ,S     FS     VS     gS     sS     �S     �S     �S     �S     �S     T     &T     +T  !   1T  ,   ST  S   �T     �T     �T     U  /   U  0   NU     U  $   �U     �U  
   �U  &   �U  (   V  `   4V     �V     �V     �V  /   �V     �V     �V  <    W  .   =W  /   lW  !   �W  .   �W     �W  0   X     6X  5   OX  ?   �X  *   �X  !   �X  1   Y  1   DY     vY  )   �Y  <   �Y     �Y     Z     *Z  <   GZ  <   �Z     �Z  &   �Z     [  4    [  6   U[     �[     �[  *   �[  /   �[  P   \  K   o\  K   �\  <   ]  |   D]  >   �]  ?    ^     @^  <   G^  ,   �^  G   �^     �^     _     +_     3_     ?_  +   Z_  =   �_     �_     �_  �  �_  ]   �a     b  C   9b     }b  @   �b     �b  .   �b     'c  '   >c  �   fc  a   �c  �   [d  Z   �d  �   Be  d   �e  �   Af  3   �f  '   g  ;   0g     lg  4   �g  E   �g  %   h     *h     Eh     ch  )   �h     �h     �h  8   �h     �h     i     4i     Bi     Vi     mi  
   vi     �i     �i  $   �i  	   �i     �i     �i  
   
j  *   j     @j  '   Zj     �j  *   �j     �j     �j  *   �j     k     2k  H   Mk  5   �k     �k  *   �k     l     l     4l  /   Sl  '   �l     �l     �l  2   �l     m  2   *m  -   ]m     �m  '   �m     �m  '   �m      n     n     !n     8n  .   Mn  0   |n  5   �n     �n  "   �n  $   o     4o     Jo  6   io      �o  	   �o  
   �o     �o  4   �o  ?   )p  ,   ip  6   �p      �p  C   �p  8   2q     kq  >   �q  C   �q  5   r  &   ;r  >   br  #   �r  9   �r     �r     s  !   1s     Ss  	   fs     ps  *   �s     �s  -   �s  2   �s     &t     /t     Lt  .   ht     �t     �t  )   �t     �t     u     u     5u     =u  '   Ou  >   wu     �u     �u  	   �u     �u  T   �u  4   ?v     tv  *   �v     �v     �v     �v     �v  U   �v  )   Iw  )   sw  3   �w  ,   �w  2   �w  3   1x      ex  )   �x     �x     �x     �x  :   �x  D   y     cy  "   vy     �y  0   �y  U   �y     )z  #   >z  G   bz  <   �z  F   �z  I   .{  @   x{  )   �{  C   �{  *   '|  F   R|  \   �|     �|  W   �|     U}  6   o}  [   �}  e   ~  9   h~  M   �~  1   �~  ;   "  -   ^  v   �  9   �  B   =�  H   ��  8   ɀ     �     �  6   0�     g�     ��     ��  %   ��     Ձ  	   �     ��  -   �  !   6�  !   X�      z�  &   ��  P     Y   �  q   m�  6   ߃      �  D   7�  )   |�  C   ��      �  2   �  J   >�     ��  (   ��     ��  (   ��     �     ��  	   �     �     *�  %   ?�  h   e�  "   Ά  %   �  -   �     E�     e�     w�     ��  1   ��     ʇ     �  8   �  ?   ;�  2   {�  /   ��  "   ވ  0   �     2�  "   Q�      t�  8   ��  B   Ή  <   �     N�  %   [�  +   ��  3   ��  ,   �  /   �  5   >�  #   t�     ��  <   ��     �     ��     �     5�     J�     d�  a   u�  c   ׌     ;�     L�  S   Z�     ��  4   ��      �  7   	�     A�     [�     t�      ��  4   ��  H   َ  F   "�     i�     {�     ��     ��  �   Ə  �   S�     ڐ     �     ��  -   �     K�  	   Q�  .   [�     ��     ��  *   ��  N   ޑ  Y   -�  /   ��  '   ��  %   ߒ  !   �  <   '�  	   d�     n�  6   u�  E   ��     �     ��     	�  "   �     @�  )   G�  D   q�     ��  7   ˔  :   �  H   >�  ]   ��  0   �  .   �  2   E�  2   x�     ��  .   ��  L   �  <   9�     v�  )   ��  z   ��  4   .�  :   c�  2   ��  P   ј  E   "�  F   h�  *   ��  f   ڙ     A�     M�     [�  
   c�  0   n�  1   ��     њ     ך  "   ܚ     ��     �  6   �  )   P�     z�  $   ��     ��  %   ޛ     �     �     .�  %   >�  +   d�  2   ��  +   Ü  .   �  4   �  
   S�     ^�     d�  0   l�  0   ��  ]   Ν     ,�     E�  $   c�  1   ��  J   ��  !   �  3   '�  %   [�  	   ��  +   ��  2   ��     �     j�     z�     ��  2   ��     Ԡ     �  J   �  0   5�  =   f�  &   ��  4   ˡ  ,    �  1   -�      _�  6   ��  @   ��  8   ��  1   1�  K   c�  L   ��  ,   ��  C   )�  U   m�     ä      ݤ  )   ��  X   (�  Y   ��  "   ۥ  A   ��  7   @�  A   x�  8   ��  0   �     $�  ?   =�  +   }�  P   ��  O   ��  M   J�  >   ��  ~   ר  @   V�  J   ��     �  M   �  B   <�  Y   �     ٪  $   �     �     !�  "   -�  0   P�  =   ��  !   ��  !   �     C       {      m  	   �           u    �   �         (  F                    �   �           �   �       2      �  g    L          f  �   �       �       n   �      !      �   �  +   �  B      �           �      �        �   N       .         U          Z  �   �   u   R   �      )   h   J        $   X  �       �       �            
   �   �           n  w   �     =       |  }       �       c        �   �   �   �   s   �      Z           �   �  �   '  �       _  G      �   �   *         H       t  i   �  ~  W  �         �   1  5       #   �   <          �      [   �       d       6           V  P   4   �          ,   �       �  K   v       T   �      �   �   �   �   (   �   �   ,     �  \  O  �   }  q  �     �  �         �   �   7   I       �   �  �         $      �   [  �  ^           &   �  S  �   1   >   �   �     d         �  �  *  	  �   Y       �   �   5      �      �  �               �   �         �  �  �  G       V         �   "   �     f   �   �           P  4      �       �   �    �   �   o   ;       &  H  �       M   /   W   r  �  �   �   8           �   �   e           g   �          9       E   �   �   D      y       �  �       o  �       I  S       R    Q      C  �       p  �   r       `                �      3   �   ^  -   \   �       �  �       J              �            6  �      b      i          �  �  k   Q      U   {   x   a   N  #  �  7     l   �   )  �  A   �          :  �   �   �   �  >      �   �   =         @   �       �           q      ?  L     '   :      a  M  
  �   ~   j   x      �      A      �   �           p   l        t   �       k  �   `   j  z           �           �           �      �  2               _   z  9  B   �           %   �   T          �       �   �  �              �          �          �   0     �   �   3  �   -          �         Y      ]      y     h  e  w      �       K      �   �   b   v  �      �   �   +  �   �  �  c            D       �       �   O       �       �  �   E  s  �   �  �   �  "  %  �  |   !     �  �  �   �        .  <   �   �     �   8  �   m   X   ;  ?   �          F   �   ]   �   �   /  @  0   �   	here may be unidentified media objects not yet fully restored
 

--------- fstab ------------
 
        may be additional unidentified media files
 
        media file %u 
    may be additional unidentified media objects

 
    media object %u:

 
    media objects not yet identified
 
media stream %u:
 
session interrupt in progress
 
there are additional unidentified media objects containing media files not yet tried for directory hierarchy restoral:
 
there are additional unidentified media objects not yet fully restored
 
there are unidentified media objects containing media files not yet tried for directory hierarchy restoral:
 
there are unidentified media objects not yet fully restored
 
there may be additional unidentified media objects containing media files not yet tried for directory hierarchy restoral:
 
there may be additional unidentified media objects not yet fully restored
 
there may be unidentified media objects containing media files not yet tried for directory hierarchy restoral:
         contains no selected non-directories
         contains session inventory
         first extent contained: ino %llu off %lld
         is stream terminator
         media files not yet identified
         next extent to restore: ino %llu off %lld
         non-directories done
         now reading
         rollback mark %lld
         size: %lld bytes
         used for directory restoral
     id:      id: %s
     index within object of first media file: %u
     label is blank
     label not identified
     label:      now in drive
     now in drive %u
  (current)  (default)  (timeout in %u sec)  (timeout in %u sec)
  - type %s for status and control
  resumed %d second intervals
 %d seconds
 %s created
 %s does not identify a file system
 %s is not a directory
 %s must be mounted to be dumped
 %s not found
 %s quota information written to '%s'
 %s%s%s%s: drive %d:  %s: %s Summary:
 %s: -%c argument does not require a value
 %s: -%c argument invalid
 %s: -%c argument missing
 %s: -%c subsystem subargument %s requires a verbosity value
 %s: unknown file type: mode 0x%x ino %llu
 %s: usage: %s  (allow files to be excluded) (contents only) (cumulative restore) (display dump inventory) (don't dump extended file attributes) (don't overwrite existing files) (don't overwrite if changed) (don't prompt) (don't restore extended file attributes) (don't timeout dialogs) (dump DMF dualstate files as offline) (force interrupted session completion) (force usage of minimal rmt) (generate tape record checksums) (help) (inhibit inventory update) (interactive) (overwrite tape) (pin down I/O buffers) (pre-erase media) (restore DMAPI event settings) (restore owner/group even if not root) (restore root dir owner/permissions) (resume) (show subsystem in messages) (show verbosity in messages) (timeout in %d sec) (timestamp messages) (unload media when change needed) (use small tree window) - (stdin) - (stdout) -%c allowed only once
 -%c and -%c option cannot be used together
 -%c and -%c valid only when initiating restore
 -%c argument %s (%s) is not a directory
 -%c argument %s too long: max is %d
 -%c argument (%s) invalid
 -%c argument (subtree) must be a relative pathname
 -%c argument is not a valid file size
 -%c argument missing
 -%c argument must be a positive number (MB): ignoring %u
 -%c argument must be a positive number (Mb): ignoring %u
 -%c argument must be between %u and %u: ignoring %u
 -%c argument must be between 0 and %d
 -%c argument not a valid dump session id
 -%c argument not a valid uuid
 -%c valid only when initiating restore
 <I/O buffer ring length> <alt. workspace dir> ... <base dump session id> <blocksize> <destination> <destination> ... <dump media file size>  <excluded subtree> ... <file> (restore only if newer than) <file> (use file mtime for dump time <level> <maximum file size> <maximum thread stack size> <media change alert program>  <media label> ... <options file> <seconds between progress reports> <session id> <session label> <source (mntpnt|device)> <source> ... <subtree> ... <use QIC tape settings> Can't open %s for mount information
 Dev	%s
 FSF tape command failed
 FSid	%s

 I/O I/O metrics: %u by %s%s %sring; %lld/%lld (%.0lf%%) records streamed; %.0lfB/s
 INV : Unknown version %d - Expected version %d
 INV: Error in fstab
 INV: inv_put_session: unknown cookie
 INV: put_fstab_entry failed.
 INV: put_starttime failed.
 KB MB Minimal rmt cannot be used without specifying blocksize. Use -%c
 Overwrite command line option
 RMTCLOSE( %d ) returns %d: errno=%d (%s)
 RMTIOCTL( %d, %d, 0x%x ) returns %d: errno=%d (%s)
 RMTOPEN( %s, %d ) returns %d: errno=%d (%s)
 RMTREAD( %d, 0x%x, %u ) returns %d: errno=%d (%s)
 RMTWRITE( %d, 0x%x, %u ) returns %d: errno=%d (%s)
 SGI_FS_BULKSTAT failed: %s (%d)
 This tape was erased earlier by xfsdump.
 UNIX domain socket Unmark and quit
 WARNING:  WARNING: unable to open directory ino %llu: %s
 WARNING: unable to read dirents (%d) for directory ino %llu: %s
 XENIX named pipe abnormal dialog termination
 abort
 advancing tape to next media file
 all map windows in use. Check virtual memory limits
 all of the above all subsystems selected

 assuming media is corrupt or contains non-xfsdump data
 attempt to access/open device %s failed: %d (%s)
 attempt to access/open remote tape drive %s failed: %d (%s)
 attempt to get status of remote tape drive %s failed: %d (%s)
 attempt to get status of tape drive %s failed: %d (%s)
 attempt to read %u bytes failed: %s
 attempt to reserve %lld bytes for %s using %s failed: %s (%d)
 attempt to truncate %s failed: %d (%s)
 attempt to write %u bytes to %s at offset %lld failed: %s
 attempt to write %u bytes to %s at offset %lld failed: only %d bytes written
 audio bad media file header at BOT indicates foreign or corrupted tape
 block special file both /var/lib/xfsdump and /var/xfsdump exist - fatal
 cannot calculate incremental dump: online inventory not available
 cannot create %s thread for stream %u: too many child threads (max allowed is %d)
 cannot determine tape block size
 cannot determine tape block size after two tries
 cannot dump to %s file type %x
 cannot open option file %s: %s (%d)
 cannot restore from %s file type %x
 cannot select dump session %d as base for incremental dump: level must be less than %d
 cannot specify source files and stdin together
 cannot specify source files and stdout together
 cannot stat option file %s: %s (%d)
 change interval of or disable progress reports change media dialog change verbosity changing progress report interval to  char special file chdir %s failed: %s
 chmod %s failed: %s
 chown (uid=%d, gid=%d) %s failed: %s
 confirm media change continue continuing
 corrupt directory entry header
 corrupt extent header
 corrupt file header
 could not create %s: %s
 could not erase %s: %s (%d)
 could not find specified base dump (%s) in inventory
 could not forward space %d tape blocks: rval == %d, errno == %d (%s)
 could not forward space one tape block beyond read error: rval == %d, errno == %d (%s)
 could not fstat stdin (fd %d): %s (%d)
 could not open %s: %s
 could not open/create persistent state file %s: %s
 could not read from drive: %s (%d)
 could not rewind %s in prep for erase: %s
 could not rewind %s: %s
 could not save first mark: %d (%s)
 could not set access and modification times of %s: %s
 debug destination directory not specified
 dir disabling progress reports
 display metrics drive %u drive %u  dump label dialog dumping core effective user ID must be root
 either try using a smaller block size with the -b option, or increase max_sg_segs for the scsi tape driver
 enable progress reports enabling progress reports at  encountered EOD : assuming blank media
 encountered EOD : end of data
 end dialog end of media erasing media
 error removing temp DMF attr on %s: %s
 examining new media
 existing version is newer failed to get bulkstat information for inode %llu
 failed to get valid bulkstat information for inode %llu
 failed to map in node (node handle: %u)
 failed to save %s information, continuing
 file mark missing from tape
 file mark missing from tape (hit EOD)
 file system id: %s
 fssetdm_by_handle of %s failed %s
 get_headers() - malloc(seshdrs)
 given option file %s is not ordinary file
 giving up attempt to determining tape record size
 giving up waiting for drive to indicate online
 hangup
 hide log message levels hide log message subsystems hide log message timestamps hiding log message levels
 hiding log message subsystems
 hiding log message timestamps
 hostname length is zero
 hostname: %s
 initiating session interrupt (timeout in %d sec)
 interactively restore
 interrupt request accepted
 interrupt this session invidx (%d)	%d - %d
 keyboard interrupt
 keyboard quit
 level %u incremental non-subtree dump will be based on subtree level %u dump
 level %u incremental subtree dump will be based on non-subtree level %u dump
 level changed
 level: %s%s
 likely problem is that the block size, %d, is too large for Linux
 link malloc of stream context failed (%d bytes): %s
 may be an EFS dump at BOT
 may not specify both -%c and -%c
 media change aborted
 media change declined media changed media error or no media media file header checksum error
 media file header magic number mismatch: %s, %s
 media file header version (%d) invalid: advancing
 media id: %s
 media inventory status media label:  mkdir %s failed: %s
 most recent base for incremental dump was interrupted (level %u): must resume or redump at or below level %d
 most recent level %d dump was interrupted, but not resuming that dump since resume (-R) option not specified
 mount point: %s
 named pipe needed media objects nh 0x%x np 0x%x hash link not null
 nitty nitty + 1 no additional media objects needed
 no change no change
 no destination file(s) specified
 no media strategy available for selected dump destination(s)
 no media strategy available for selected restore source(s)
 no more data can be written to this tape
 no session label specified
 no source file(s) specified
 node %x %s %llu %u parent NULL
 node %x %s %llu %u parent mismatch: nodepar %x par %x
 non-root nondir not allowed to pin down I/O buffer ring
 not enough physical memory to pin down I/O buffer ring
 orphan other controls overwriting: %s
 parent of %s is not a directory
 pinned  please change media in drive %u
 please change media: type %s to confirm media change
 please confirm
 please enter a value between 1 and %d inclusive  please enter label for this dump session please enter seconds between progress reports please enter seconds between progress reports, or 0 to disable please select one of the following controls
 please select one of the following metrics
 please select one of the following operations
 please select one of the following subsystems
 preparing drive
 read of option file %s failed: %s (%d)
 read_record encountered EOD : assuming blank media
 read_record encountered EOD : end of data
 reading directories
 received signal %d (%s): cleanup and exit
 recommended media file size of %llu Mb less than estimated file header size %llu Mb for %s
 record %lld corrupt: bad magic number
 record %lld corrupt: bad record checksum
 record %lld corrupt: dump id mismatch
 record %lld corrupt: incorrect record offset in header (0x%llx)
 record %lld corrupt: incorrect record padding offset in header
 record %lld corrupt: incorrect record size in header
 record %lld corrupt: null dump id
 record %lld corrupt: record offset in header not a multiple of record size
 rename from rename to restore restore
 restore complete: %ld seconds elapsed
 resynchronized at record %lld offset %u
 rmdir root saving %s information for: %s
 secure session id: %s
 session interrupt in progress: please wait
 session interrupt timeout
 session inventory display
 session inventory unknown
 session label entered: " session label left blank
 session label:  session time: %s set dirattr show log message levels show log message subsystems show log message timestamps showing log message levels
 showing log message subsystems
 showing log message timestamps
 silent skip slave source file system not specified
 specified destination %s is not a directory
 specified minimum stack size is larger than maximum: min is 0x%llx,  max is 0x%llx
 stat of %s failed: %s
 status and control dialog subtree selection dialog syssgi( SGI_FS_BULKSTAT ) on fsroot failed: %s
 tape drive %s is not ready (0x%x): retrying ...
 tape is write protected
 tape media error on write operation
 tape record checksum error
 terminate
 the following commands are available:
 the following media objects are needed:
 the following media objects contain media files not yet tried for directory hierarchy restoral:
 timeout
 tm (%d)	%d
 too many -%c arguments
 too many -%c arguments: "-%c %s" already given
 too old trace tree_extattr_recurse: Could not convert node to path for %s
 unable to allocate memory for I/O buffer ring
 unable to backspace tape: assuming media error
 unable to backspace/rewind media
 unable to bind %s ino %llu %s: %s: discarding
 unable to chown %s: %s
 unable to create %s ino %llu %s: %s: discarding
 unable to create %s: %s
 unable to create symlink ino %llu %s: %s: discarding
 unable to create symlink ino %llu %s: src too long: discarding
 unable to determine current directory: %s
 unable to determine hostname: %s
 unable to determine uuid of fs containing %s: %s
 unable to determine uuid of fs mounted at %s: %s
 unable to get status of %s: %s
 unable to locate next mark in media file
 unable to lower stack size soft limit from 0x%llx to 0x%llx
 unable to map %s: %s
 unable to open %s: %s
 unable to open directory %s
 unable to raise stack size hard limit from 0x%llx to 0x%llx
 unable to raise stack size soft limit from 0x%llx to 0x%llx
 unable to remove %s: %s
 unable to rename dir %s to dir %s: %s
 unable to rmdir %s: not empty
 unable to set %s extended attribute for %s: %s (%d)
 unable to set access and modification times of %s: %s
 unable to set block size to %d
 unable to stat %s: %s
 unable to write file mark at eod: %s (%d)
 unexpected EIO error attempting to read record
 unexpected error attempting to read record %lld: read returns %d, errno %d (%s)
 unexpected error attempting to read record: read returns %d, errno %d (%s)
 unexpected error attempting to read record: read returns %d, errno %s (%s)
 unexpected tape error: errno %d nread %d blksz %d recsz %d 
 unexpected tape error: errno %d nread %d blksz %d recsz %d isvar %d wasatbot %d eod %d fmk %d eot %d onl %d wprot %d ew %d 
 unexpectedly encountered EOD at BOT: assuming corrupted media
 unexpectedly encountered a file mark: assuming corrupted media
 unlink unrecognized drive strategy ID (media says %d, expected %d)
 unrecognized media file header version (%d)
 use of QIC drives via variable blocksize device nodes is not supported
 using %s strategy
 valid record %lld but no mark
 verbose volume: %s
 will rewind and try again
 win_map(): try to select a different win_t
 win_map(): unable to map a node segment of size %d at %d: %s
 write to %s failed: %d (%s)
 writing file mark at EOD
 Project-Id-Version: xfsdump
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-01-08 05:09+0000
PO-Revision-Date: 2015-01-08 16:09+0000
Last-Translator: Juliano Fischer Naves <julianofischer@gmail.com>
Language-Team: Portuguese (Brazil) <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:22+0000
X-Generator: Launchpad (build 18115)
 	Aqui podem ter objetos não identificados. A mídia, ainda não foi, totalmente restaurada.
 

--------- fstab ------------
 
        podem ser arquivo de mídia adicionais não identificados
 
        arquivo de mídia %u 
    podem ser arquivos de mídia adcionais não identificados

 
    objeto de mídia %u:

 
    objeto de mídia ainda não identificado
 
stream de mídia %u:
 
interrupção de sessão em progresso
 
Há outros objetos de mídia não identificados contendo arquivos de mídia que ainda não foram colocados na hierarquia do diretório restoral:
 
Existem outros objetos de mídia não-identificados que ainda não foram totalmente restaurados
 
Há objetos de mídia não identificados contendo arquivos de mídia que ainda não foram colocados na hierarquia do diretório restoral:
 
Existem objetos de mídia não-identificados que ainda não foram totalmente restaurados
 
Pode haver outros objetos de mídia não identificados contendo arquivos de mídia que ainda não foram colocados na hierarquia do diretório restoral:
 
Pode haver outros objetos de mídia não identificados que ainda não foram totalmente restaurados
 
Pode haver objetos de mídia não identificados contendo arquivos de mídia que ainda não foram colocados na hierarquia do diretório restoral:
         não contém não-diretórios selecionados
         contém sessão de inventário
         primeira extensão contendo: ino %llu fora de %lld
         é flixo terminator
         arquivos de mídia ainda não identificados
         próxima extensão para restauração: ino %llu fora de %lld
         não-diretórios concluídos
         agora está lendo
         marca reversão %lld
         tamanho: %lld bytes
         usado para o diretório restante
     id:      id: %s
     índice no objeto do primeiro arquivo de mídia: %u
     rótulo está em branco
     rótulo não identificado
     rótulo:      agora no drive
     agora no drive %u
  (atual)  (padrão)  (intervalo em %u seg)  (intervaldo em %u seg)
  - digite %s para status e controle
  resumido intervalos de %d segundo(s)
 %d segundos
 %s criado
 %s não identifica um sistema de arquivos
 %s não é um diretório
 %s deve ser montado para ser despejado
 %s não encontrado
 %s parte da informação escrita por '%s'
 %s%s%s%s: unidade %d:  %s: %s Sumário:
 %s: -%c o argumento não requer um valor.
 %s: -%c argumento inválido
 %s: -%c faltando argument
 %s: -%c sub-argumento do subsistema  %s requer um valor de detalhamento
 %s: tipo de arquivo desconhecido: modo 0x%x ino %llu
 %s: uso: %s  (permitir que os arquivos sejam excluidos) (somente conteúdos) (restauração cumulativa) (exibe inventário de despejo) (não despeje arquivos extendidos de atributos) (não sobrescrever arquivos existentes) (não substituir se alterado) (não imediato) (não restaurar os atributos de arquivo extendido) (não são diálogos timeout) (despejo DMF dualstate arquivos como desconectado) (força a conclusão da sessão interrompida) (forçar uso mínimo de rmt) (gerar checksums de gravação em fita) (ajuda) (inibem a atualização do inventário) (interativo) (substituir a fita) (pin down I/O buffers) (pré-apagar mídia) (restaurar as configurações do evento DMAPI) (restaurar dono/grupo, mesmo que não seja root) (restaurar diretório raiz permissões/proprietário) <resumo> (mostrar subsistema nas mensagens) (mostrar detalhamento nas mensagens) (intervalo em %d seg) (mensagens de carimbo de hora) (descarregar mídia quando a mudança for necessária) <usar pequena janela de árvore) - (stdin) - (stdout) -%c permitida apenas uma vez
 opções -%c e -%c não podem ser utilizadas juntas
 -%c e -%c válido somente quando inicializando a restauração
 -%c argumento %s (%s) não é um diretório
 -%c argumento %s muito longo: o tamanho máximo é %d
 argumento -%c (%s) é inválido
 -%c o argumento (sub-árvore) deve ser um nome de caminho relativo
 argumento -%c não possui um tamanho de arquivo válido
 argumento -%c ausente
 -%c argumento deve ser um número positivo (MB): ignorando %u
 o argumento -%c precisa ser um número positivo (Mb): ignorando %u
 -%c argumento deve estar entre %u e %u: ignorando %u
 argumento -%c deve estar entre 0 e %d
 -%c o argumento não é um ID válido para sessão de despejo
 argumento -%c não é uuid válida
 -%c válido somente quando inicializando a restauração
 <I/O buffer ring length> <alt. workspace dir> ... <id da sessão da base de desejo> <tamanho do bloco> <destino> <destinação> ... <tamanho do arquivo de mídia da lixeira>  <subárvore excluída> <arquivo> (restaurar apenas se mais novo que) <arquivo> (use arquivo mtime para despejo de tempo <nível> <tamanho máximo de arquivo> <maximum thread stack size> <alerta de programa de alteração de mídia>  <identificação da mídia> ... <arquivo de opções> <segundos entre relatórios de progresso> <id da sessão> <rótulo da sessão> <source (mntpnt|device)> <fonte> <sub-árvore> ... <use as configurações de da fita QIC> Não foi possível abrir %s para informações sobre montagem
 Dev	%s
 falha no comando de fita FSF
 FSid	%s

 I/O E/S métricas: %u por %s%s %sring; %lld/%lld (%.0lf%%) registros fluídos; %.0lfB/s
 INV : Versão desconhecida %d - Versão esperada %d
 INV: Erro no fstab
 INV: inv_put_session: cookie desconhecido
 INV: put_fstab_entry falhou.
 INV: put_starttime falhou.
 KB MB rmt mínimo não pode ser utilizado sem especificar o tamanho do bloco. Utilizar -%c
 Sobrescrever opção de linha de comando
 RMTCLOSE( %d ) retorna %d: errno=%d (%s)
 RMTIOCTL( %d, %d, 0x%x ) retorna %d: errno=%d (%s)
 RMTOPEN( %s, %d ) retorna %d: errno=%d (%s)
 RMTREAD( %d, 0x%x, %u ) retorna %d: errno=%d (%s)
 RMTWRITE( %d, 0x%x, %u ) retorna %d: errno=%d (%s)
 SGI_FS_BULKSTAT falhou: %s (%d)
 Esta fita foi apagada antes por xfsdump.
 soquete UNIX de domínio Desmarcar e sair
 AVISO:  AVISO: não foi possível abrir o diretório ino %llu: %s
 AVISO: não foi possível dirents (%d) para diretório ino %llu: %s
 pipe XENIX nomeado terminação de  diálogo anormal
 abortar
 avançar fita para o próximo arquivo de mídia
 todas as janelas de mapeamento estão em uso. Verifique o limite de memória virtual
 todos os itens acima todos os subsistemas selecionados

 presumindo que a mídia está corrompida ou contém dados não-xfsdump
 tentativa de acessar/abrir o dispositivo %s falhou: %d (%s)
 falha na tentativa de acessar/abrir drive de fita remota %s : %d (%s)
 tentativa de atualizar status do drive de fita remoto %s falhou: %d (%s)
 tentativa de obter o status do drive de fita %s falhou: %d (%s)
 tentativa de leitura %u bytes falhou: %s
 tentativa de reservar %lld bytes para %s usando %s falhou: %s (%d)
 A tentativa de truncar %s falhou: %d (%s)
 tentativa de escrita %u bytes para %s no deslocamento %lld falhou: %s
 tantativa de escrita %u bytes para %s no deslocamento %lld falhou: apenas %d bytes escritos
 áudio cabeçalho de arquivo de mídia inválido em BOT indica fita estrangeira ou corrompida
 arquivo especial de bloco ambos /var/lib/xfsdump e /var/xfsdump existem - fatal
 não foi possível calcular despejo incremental: inventário online não está disponível
 não foi possível criar thread %s para stream %u: threads filhas demais (o máximo permitido é %d)
 não foi possível determinar o tamanho do bloco de fita
 não foi possível determinar tamanho de bloco de fita após duas tentativas
 não é possível despejar %s tipo de arquivo %x
 não é possível abrir as opções do arquivo %s: %s (%d)
 não pode restaurar de %s tipo de arquivo %x
 não é possível selecionar a sessão de despejo %d como base para despejo incremental: nível deve ser menor que %d
 não pode especificar arquivos fonte e stdin em conjunto
 não é possível especificar arquivos fonte e stdout em conjunto
 Não foi possível obter estatística de arquivo de opção %s: %s (%d)
 desativa ou muda o intervalo de relatórios de progresso alterar meios de diálogo muda o detalhamento alterando o intervalo de relatório de progresso para  arquivo de caractere especial chdir %s falhou: %s
 chmod %s falhou: %s
 chown (uid=%d, gid=%d) %s falhou: %s
 confirme mudança de mídia continuar continuando
 diretório de cabeçario de entrada corrupto
 extensão de cabeçario corrupto
 arquivo de cabeçalho corrompido
 não foi possível criar %s: %s
 não foi possível apagar %s: %s (%d)
 não foi possível encontrar a base especificada (%s) no inventário de despejo
 não foi possível encaminhar blocos de fita do espaço %d: rval == %d, errno == %d (%s)
 não foi possível encaminhar bloco de fita do espaço um além do erro de leitura: rval == %d, errno == %d (%s)
 não foi possível fazer fstat stdin (fd %d): %s (%d)
 não foi possível abrir %s: %s
 não foi possível abrir/criar arquivo de estado persistente %s: %s
 não foi possível ler do drive: %s (%d)
 não foi possível rebobinar %s em preparação para exclusão: %s
 não podia voltar atrás %s: %s
 não foi possível salvar primeira marca: %d (%s)
 não foi possível estabelecer tempos de acesso e modificação de %s: %s
 depurar diretório de destino não especificado
 dir desabilitar os relatórios de progresso
 exibir métricas drive %u drive %u  diálogo de rótulo de despejo descartando o nucleo ID de usuário efetivo deve ser root
 tente usar um tamanho de bloco menor com a opção -b, ou aumente max_sg_segs para o drive de fita scsi
 habilitar relatórios de progresso ativando relatórios de progresso em  encontrado EOD : presumindo mídia em branco
 encountered EOD : fim de dados
 encerrar diálogo final da mídia apagando mídia
 erro ao remover temperatura DMF attr sobre %s:%s
 examinando nova mídia
 versão existem é mais recente Falha ao obter informações bulkstat para o inode %llu
 falha ao obter informações bulkstat válidas para inode %llu
 falhou para mapear no nó (nó identificador: %u)
 falha ao salvar a informação %s, continuando
 marca de arquivo faltando na fita
 marca de arquivo faltando na fita (atingir EOD)
 id do sistema de arquivos: %s
 fssetdm_by_handle de %s falhou %s
 get_headers() - malloc(seshdrs)
 opção dada do arquivo %s não é um arquivo ordinario
 desistindo da tentativa de determinar tamanho de registro da fita
 desistindo de esperar o drive indicar que está disponível
 desconectar
 ocultar os níveis de log de mensagem ocultar os logs de mensagens de subsistemas ocultar marcas temporais dos registros de mensagens ocultando níveis de mensagens de registros
 ocultando subsistemas de mensagens de registro
 ocultando marcas temporais das mensagens de registro
 O tamanho do nome do host é zero.
 hostname:%s
 início da sessão de interrupção (finalizando em %d seg)
 restauração interativa
 pedido de interrupção aceito
 interromper esta sessão invidx (%d)	%d - %d
 interrupção do teclado
 parar o teclado
 despejo incremental de não-subárvore nível %userá baseado no despejo de subárvore nível %u
 despejo incremental de sub-árvore nível %u será baseado no despejo de não-subárvore nível %u
 nível alterado
 nível: %s%s
 O provável problema é que o tamanho do bloco, %d, é grande demais para o Linux.
 link falha em malloc de contexto de fluxo (%d bytes): %s
 pode ser um dump de EFS  em BOT
 -%c e -%c não podem ser especificados simultaneamente
 troca de mídia abortada
 troca de mídia recusada mídia trocada erro na mídia ou nenhuma mídia erro de checksum do cabeçalho do arquivo de mídia
 número mágico do cabeçalho de arquivo de media incompatível: %s, %s
 versão de cabeçalho de arquivo de mídia (%d) inválida: avançando
 id da mídia: %s
 status do inventário de mídia rótulo da mídia:  falha em mkdir %s: %s
 a base mais recente para despejo incremental foi interrompida (nível %u): deve-se continuar ou redespejar no nível igual ou inferior a %d
 o mais recente nível de despejo foi interrompido (%d), mas não retomando o despejo uma vez que a opção (-R) não foi especificada
 ponto de montagem: %s
 pipe nomeado objetos de mídia necessários nh 0x%x np 0x%x link do erro grave não nulo
 nitty nitty + 1 Nenhum objeto de mídia adicional necessário
 nenhuma alteração nenhuma alteração
 nenhum arquivo(s) de destino especificado
 não há estratégia de mídia disponível para destinação(ões) do despejo
 não há estratégia de mídia disponível para fonte(s) de restauração selecionada(s)
 não é possível gravar mais dados nesta fita
 nenhum rótulo de sessão especificado
 nenhum arquivo(s) fonte especificado
 nó principal %x %s %llu %u nulo
 nó principal %x %s %llu %u incompatível:nodepar %x par %x
 não-root nondir sem permissão para identificar anel de buffer de E/S
 memória física insuficiente para identificar anel de buffer de E/S
 orphan outros comandos sobrescrevendo: %s
 o pai de %s não é um diretório
 preso  por favor, troque a mídia na unidade %u
 favor mudar a mídia: digite %s para confirmar a mudança de mídia
 por favor, confirme
 por favor, entre com um valor entre 1 e %d (inclusive)  por favor, indique um rótulo para esta sessão de despejo por favor entre o tempo (em segundos) entre os relatórios de progresso. por favor entre o tempo (em segundos) entre os relatórios de progresso, ou 0 para desativar. por favor, selecione um dos seguintes controles
 por favor, selecione um dos seguintes medidas
 por favor selecione uma das seguintes operações
 por favor, selecione um dos seguintes subsistemas
 preparando drive
 ao ler a opção do arquivo %s falhou: %s (%d
 A leitura da gravação encontrou um EOD : Assumindo que é uma midia vazia
 A leitura da gravação encontrou um EOD : Fim dos arquivos
 lendo diretórios
 sinal recebido %d (%s): limpeza e saída
 tamanho de arquivo de mídia recomendado %llu Mb é inferior ao tamanho estimado de cabeçalho de arquivo %llu Mb para %s
 registro %lld corrompido: número mágico inválido
 registro %lld corrompidot: checksum inválido de registro
 registro %lld corrompido: id de dump não confere
 registro %lld corrompido: gravação do offset incorreta no cabeçalho (0x%llx)
 registro %lld corrupto: gravação incorreta do offset no cabeçalho
 registro %lld corrompido: tamanho de registro incorreto no cabeçalho
 registro %lld corrompido: id de dump nula
 registro %lld corrompido: registro do offset no cabeçalho não é um multiplo do tamanho do registro
 renomear de renomear para restora restaurar
 restauração completa: %ld segundos decorridos
 ressincronizado no registro %lld deslocamento %u
 rmdir root salvando %s informação para: %s
 seguro id da sessão: %s
 interromper a sessão em andamento: por favor aguarde
 tempo limite da sessão de interrupção
 exibir sessão de inventário
 sessão de inventário desconhecida
 rótulo de sessão informado: " rótulo da sessão deixado em branco
 rótulo da seção:  tempo de seção: %s definir dirattr mostrar os níveis de log de mensagem mostrar os logs de mensagens de subsistemas exibir marcas temporais dos registros de mensagens exibindo níveis de mensagens de registros
 exibindo subsistemas de mensagens de registro
 exibindo marcas temporais das mensagens de registro
 silencioso pular escravo sistema de arquivos de origem não especificado
 o destino especificado %s não é um diretório
 o tamanho mínimo especificado da pilha é maior que o máximo: min é 0x%llx, max é 0x%llx
 estado de %s falhou: %s
 status e diálogo de controle diálogo de seleção de sub-árvore falha em syssgi( SGI_FS_BULKSTAT ) em fsroot: %s
 o dispositivo de fita %s não está pronto (0x%x): tentando novamente ...
 fita protegida contra gravação
 erro de mídia de fita em operação de gravação
 erro de checksum em registro da fita
 terminar
 os seguintes comandos estão disponíveis:
 Os seguintes objetos de mídia são necessários:
 os seguintes objetos de mídia contém arquivos de mídia que ainda não foram colocados na hierarquia do diretório restoral:
 tempo esgotado
 tm (%d)	%d
 excesso de argumentos -%c
 excesso de argumentos -%c: "-%c %s" já informado
 muito antigo rastrear tree_extattr_recurse: Não foi possível converter nó em caminho para %s
 incapaz de alocar memória para I/O buffer ring
 não é possivel retroceder a fita: o erro pode ser da midia
 impossível voltar/rebobinar a mídia
 incapaz de vincular %s ino %llu %s: %s: descartando
 não foi possível de executar chown %s: %s
 incapza de criar %s ino %llu %s: %s: descartando
 não foi possível criar %s: %s
 incapaz de criar symlink ino %llu %s: %s: descartando
 incapaz de criar symlink ino %llu %s: src too long: descartando
 não foi possível determinar o diretório corrente: %s
 Não foi possível determinar o nome do host: %s
 não foi possível determinar o uuid de sistema de arquivo contendo %s: %s
 não foi possível determinar uuid do sistema de arquivos montado em %s: %s
 não foi possível obter o status de %s: %s
 não foi possível localizar a próxima marca no arquivo de mídia
 não foi possível baixar o limite flexível do tamanho da pilha de 0x%llx to 0x%llx
 incapaz de mapear %s: %s
 não foi possível abrir %s: %s
 não foi possível abrir o diretório %s
 não foi possível aumentar o  limite rígido do tamanho da pilha de 0x%llx para 0x%llx
 não foi possível aumentar o limite flexível do tamanho da pilha de 0x%llx para 0x%llx
 não foi possível remover %s: %s
 não foi possível renomear diretório %s para diretório %s: %s
 Diretório %s não está vazio, impossível removê-lo
 não é possível definir %s atributo extendido para %s: %s (%d)
 incapaz de definir acesso e vezes modificadas de %s: %s
 Impossível de mudar o tamanho do bloco para %d
 incapaz de  stat %s: %s
 impossível gravar as informações do arquivo na EOD: %s (%d)
 EIO erro inesperado ao tentar ler registro
 erro inesperado ao tentar ler o registo %lld: leitura retorna %d, errno %d (%s)
 erro inesperado ao tentar ler registro: a leitura retornamarc %d, erro %d (%s)
 erro inesperando tentando acessar registro: leia o retorno %d, errno %s (%s)
 erro de fita inesperado: errno %d nread %d blksz %d recsz %d 
 erro de fita inesperado: errno %d nread %d blksz %d recsz %d isvar %d wasatbot %d eod %d fmk %d eot %d onl %d wprot %d ew %d 
 inesperadamente encontrou EOD no BOT: supondo mídia corrompida
 inesperadamente encontrou uma marca de arquivo: supondo mídia corrompida
 desvincular ID de estratégia de drive não reconhecido (mídia informa %d, esperado %d)
 não reconhecida a versão do cabeçalho de arquivo de media (%d)
 o uso de drives QIC através de nós de dispositivo de bloco variável não é suportado
 usando %s estratégia
 registro válido %lld mas sem marca
 informativo volume: %s
 irá rebobinar e tentar novamente
 win_map(): tente selecionar uma win_t diferente
 win_map(): incapaz de mapear um nó de segmente %d em %d: %s
 gravação em %s falhou: %d (%s)
 marcar escrita de arquivo no EOD
 