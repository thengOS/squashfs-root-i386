��    �     l  �  �*      �8  6   �8     09     L9     d9  )   v9     �9     �9  �   �9  .  �:  �  �;  d  �?  �  �A  )	  �C  \  �L  �  Q    �S  G  �W  �  7Y  %  �\  /   _  +   @_     l_     }_     �_  -   �_  !   �_  >   �_  -   (`  0   V`  7   �`  /   �`  6   �`     &a  4   =a  3   ra  '   �a     �a     �a     �a  *   b  !   :b  "   \b     b     �b  '   �b     �b     �b     �b  
   �b     �b     c  "   c     <c  	   Kc     Uc     cc     jc  	   rc  	   |c     �c  4   �c     �c     �c     �c     �c     d  4   d     Jd  	   Wd  7   ad  )   �d     �d  I   �d     -e     @e     `e     ue     �e     �e  "   �e  >   �e  e   f     |f  &   �f     �f     �f      g  $   $g     Ig  !   hg  2   �g     �g  2   �g  7   h  =   Eh  %   �h  5   �h  .   �h  *   i  '   9i  .   ai      �i     �i  ,   �i  $   �i  .   "j  *   Qj  4   |j  )   �j  +   �j  *   k  )   2k     \k  $   vk  "   �k  #   �k  #   �k  #   l  >   *l  $   il     �l  .   �l     �l     �l     m  %   ,m     Rm  "   jm     �m  +   �m     �m  !   �m     n     -n  '   Jn  )   rn  -   �n     �n     �n  #   �n     o     :o  2   Vo      �o      �o     �o  (   �o  )   p  ,   ;p     hp     �p     �p  !   �p     �p  $   �p  $   q     8q     Tq  )   qq     �q     �q     �q  !   �q     r  %   2r  #   Xr     |r     �r     �r     �r     �r  0   �r     's     Bs  6   `s     �s  -   �s     �s     �s     t     #t      9t  4   Zt     �t     �t     �t     �t  .   �t  3   u  J   <u     �u     �u     �u     �u     �u     �u  
   �u  "   v     +v     Kv  .   fv  .   �v     �v     �v  >   �v  	   %w  4   /w  4   dw  v   �w  %   x     6x  1   Qx     �x     �x  )   �x     �x     �x  '   �x     y  $   'y  !   Ly  !   ny     �y  (   �y  )   �y     �y  '   z  "   >z     az     sz  '   �z  �  �z     B|     F|  #   V|      z|  $   �|     �|     �|  %    }     &}     A}     N}     c}  5   t}     �}      �}  C   �}     ~     -~  %   4~  +   Z~  ;   �~  %   �~     �~  ;         <     @  #   C     g     �  ,   �      �  d   �  Q   R�     ��     ��     ��  #   ؀  (   ��     %�  (   B�     k�  B   q�  M   ��  0   �  s   3�  /   ��  
   ׂ  !   �     �  *   	�  (   4�  �   ]�  �   �  �   ۄ  �   ~�  �   #�  �   ��  �   S�  �   �  �   ��  �   9�  -   ��  v   $�  ~   ��  8   �  A   S�  z   ��  �   �  7   ��  D   ˌ  t   �  |   ��     �  
   "�     -�  
   6�  	   A�     K�     _�     p�     }�     ��     ��     ��  "   ̎     �  ,   �     <�     [�     r�  +   y�     ��     ��     ʏ     Ϗ     �     ��  ;   �     C�     L�     e�     t�     y�     }�     ��  	   ��  
   ��  	   ��  &   ��  %   �  $   	�  3   .�  "   b�  &   ��  1   ��  )   ޑ  5   �  %   >�  1   d�  1   ��  #   Ȓ     �  !   �  1   �  &   E�  %   l�  %   ��     ��  3   ד  <   �  B   H�     ��     ��     ��     Ք  9   �     (�     8�     M�  :   ]�  $   ��  %   ��     �      ��  "   �      8�     Y�     v�  !   ��     ��  '   Ö     �     ��     �     4�  ,   S�      ��  ?   ��     �  &   �     �     �     6�     P�  (   n�     ��  %   ��  !   ݘ      ��  $    �     E�     c�  -   ��     ��  .   ̙  %   ��  #   !�     E�  &   Z�     ��  4   ��     ֚  )   �     �     �     ,�     C�     _�     w�     ��     ��     ��      ƛ      �     �  %   $�  $   J�      o�  .   ��     ��     ڜ  2   �  !   &�  $   H�  A   m�     ��     ��  :   ��  &   ��  *   �  +   I�     u�  3   ��  B   Ȟ  B   �     N�     R�     W�  	   ^�  O   h�     ��     ϟ     �     ��     �     �  '   !�  +   I�     u�     ��     ��  =   ��  !   �      �  $   '�     L�  -   ]�  .   ��     ��     ¡     ˡ     ܡ  "   �  %   �     <�  '   [�  $   ��     ��     ��  #   ��  +   �     �     �     �     %�     *�  (   G�  3   p�  *   ��  !   ϣ     �     ��     �     �     .�     ?�     W�     m�  #   ��  #   ��  "   ̤     �     �     �     !�     5�     R�     W�  /   n�      ��     ��  .   ֥     �     !�  (   9�  A   b�  ,   ��  "   Ѧ     ��     ��  "   �  (   @�  /   i�  B   ��  G   ܧ  "   $�  4   G�     |�     ��     ��     ��  "   ��     ɨ  +   Ѩ  8   ��  $   6�  6   [�  !   ��     ��     ϩ  "   ��  .   �     ?�     M�  -   `�     ��     ��  >   ��  /   ��  %   *�  $   P�     u�  &   ��     ��  #   ��     ׫  !   �  #   �  
   +�     6�  #   C�     g�     �     ��  7   ��     Ŭ     ʬ     ά     ֬     ٬     ��     �     "�     ;�     Y�  5   l�  5   ��     ح  '   ܭ     �     �     !�  !   )�     K�  	   c�  
   m�  -   x�  6   ��     ݮ     �  #   �  *   %�  .   P�  /   �     ��  $   ��     �     �     ��  &   �     -�  #   >�     b�     ��  -   ��     ư     ߰     ��  '   �     8�     T�     m�     o�     ��     ��  	   ʱ     Ա  +   ر     �  /   �     I�  <   _�  J   ��     �     ��     ��     �     #�     3�     D�     T�     f�     v�     ��     ��     ��     Գ     �  %   �  7   )�     a�     t�     {�     ��     ��     ��     ´     ޴     ��  +   ��     (�     G�  *   _�  #   ��     ��     ŵ  
   ʵ      յ  
   ��  	   �  B   �  +   N�  A   z�     ��     ϶     �     ��     �     �     /�     N�     m�     ��     ��     ��  *   ��  !   ܷ     ��  )   �  �  G�  K   �  '   ;�     c�     {�  �   ��     ��     ��     ��  ?  Ѽ  N  �  �  `�  �  �  �
  �  �  ��  /  @�  �  p�  ~  �  y  ��  s  �  /   ��  0   ��     ��     ��     �  <   �  J   T�  a   ��  ;   �  3   =�  ?   q�  6   ��  V   ��     ?�  (   V�  (   �  -   ��  "   ��     ��     �  *    �  !   K�  "   m�     ��     ��  #   ��     ��  
   ��  %   ��     �     (�     9�  "   B�     e�  	   t�     ~�     ��     ��  	   ��  
   ��  	   ��  7   ��     ��     �     �     6�     N�  ?   Q�     ��  	   ��  @   ��  -   ��  *   �  X   B�     ��     ��     ��     ��     �     !�  (   0�  K   Y�  �   ��  '   (�  7   P�  (   ��  (   ��  '   ��  '   �  %   *�  &   P�  3   w�     ��  4   ��  <   ��  4   9�  .   n�  ?   ��  5   ��  /   �  1   C�  <   u�  *   ��  $   ��  3   �  0   6�  M   g�  /   ��  8   ��  )   �  +   H�  *   t�  )   ��  #   ��  9   ��  1   '�  5   Y�  0   ��  6   ��  L   ��  :   D�  *   �  N   ��  .   ��  -   (�  1   V�  <   ��  $   ��  1   ��      �  :   =�  +   x�  3   ��  +   ��  +   �  D   0�  C   u�  Q   ��  &   �  4   2�  *   g�  $   ��  &   ��  8   ��  3   �  %   K�     q�  2   ��  *   ��  9   ��     &�     B�     ^�  3   r�  #   ��  .   ��  *   ��     $�     @�  ,   ]�      ��  "   ��  #   ��  ,   ��     �  /   ?�  1   o�  *   ��     ��     ��             6   1     h      �  H   �     �  A       J "   g    �    � #   � 4   �     )   /    Y    a B   j <   � 0   �            # #   @    d        � &   �    �    � :   	 :   D         � [   � 	   � P   	 P   Z �   � '   @     h =   �    �    � -       /    B ,   I     v .   � '   � $   �     +   & -   R !   � 6   � 8   �    	     &	 4   G	 �  |	    G    K )   ] !   � $   �    �    � +       9    U    k    � K   �    � !   � S       a    x ,    *   � U   � 1   -    _ D   { 	   �    � %   �    �     5   1 (   g g   � g   �    `    h !   } (   � 3   �    � 1       L T   R ]   � D    �   J 8   � 
    -       @ :   I .   � �   � �   � �   ^ �    �   � �   \ �     �   � �   N �    >   � �    �   � P   . ]    �   � �   j =   � J   : �   � �       � 
   �    �    � 	   �    �    	          '     5     F  !   W  %   y  "   �  ,   �     �     !    %! 5   ,!    b!    u!    �!    �!    �!    �! B   �! 
   "    "    1"    @"    E"    I"    `" 	   i"    s" 	   " )   �" (   �" '   �" 1   # %   6# )   \# 7   �# .   �# ;   �# '   )$ @   Q$ @   �$     �$    �$ &   �$ 2    % -   S% ,   �% ,   �% $   �% C    & N   D& N   �& #   �& #   ' *   *'    U' K   r'    �'    �'    �' c   ( 4   p( 5   �(    �( #   �( %   ) #   ?) !   c) #   �) +   �)    �) :   �) !   +* '   M* -   u* ,   �* 4   �* +   + Q   1+    �+ 0   �+    �+ #   �+ $   �+ (   , 0   B, 2   s, 3   �, 1   �, (   - /   5- 0   e- &   �- 2   �- 7   �- 2   (. :   [. 0   �. #   �. /   �. 2   / 1   N/    �/ ;   �/    �/    �/    �/ "   0    40    R0    n0    z0 %   �0 8   �0 5   �0 %    1 7   F1 -   ~1 :   �1 7   �1 !   2 '   A2 B   i2 <   �2 ?   �2 [   )3    �3    �3 H   �3 ,   �3 7   4 ?   >4 &   ~4 7   �4 M   �4 Q   +5    }5    �5    �5 
   �5 8   �5    �5 #   �5    6     6    =6    J6 6   R6 H   �6    �6 
   �6    �6 @   7 $   O7 &   t7 -   �7    �7 3   �7 2   8 	   A8    K8    S8    j8 #   �8 0   �8 "   �8 ?   �8 1   :9    l9    |9 2   �9 @   �9    �9 	   :    :    : "   ": .   E: 9   t: 1   �: -   �:    ;    ;    ";    6;    I;    Z;    r;    �; 1   �; +   �; )   �;     '<    H<    N<    \< "   {<    �<    �< =   �< )   �<    )= ;   E= "   �=    �= .   �= _   �= >   O> :   �>    �> #   �> )   �> 1    ? 8   R? L   �? I   �? "   "@ =   E@    �@    �@ 	   �@    �@ *   �@    �@ A   �@ T   (A 2   }A 2   �A &   �A    
B #   &B 5   JB 3   �B    �B    �B 9   �B    C    -C [   KC 3   �C 1   �C 0   D    >D ;   WD    �D .   �D    �D 1   �D +   E    EE    QE '   `E    �E    �E    �E >   �E    �E 	   �E    	F    F )   F    >F    SF    lF    �F    �F ?   �F D   �F    AG ,   EG    rG 	   �G    �G %   �G    �G    �G    �G 3   H 8   6H    oH    �H :   �H 1   �H ;   I E   GI    �I <   �I    �I    �I    �I 3   J    :J 8   NJ $   �J    �J 3   �J #   �J #   K "   BK 2   eK D   �K 3   �K    L     L    4L    SL    qL    }L 5   �L    �L 2   �L    
M M   )M \   wM    �M    �M    �M    N    N    &N    7N    GN    YN    iN    �N    �N    �N    �N    �N *   �N F   !O    hO    {O -   �O    �O    �O    �O ,   �O &   P    5P 2   :P )   mP !   �P +   �P 0   �P    Q    3Q    <Q (   MQ    vQ 	   �Q N   �Q =   �Q Q    R    rR    �R    �R    �R    �R    �R %   �R %   S $   -S    RS    bS    xS )   |S     �S    �S 4   �S    0  �  �  :   t      @  q               �             �                �              �   r          �      �   R  �          6      �       P    �  �  �   �   V   I      �   �  �  �  b  �  �  p  �  J  �  c  <      �  �   3   o  �   �  �              �                  �  4  l   f   �       �     :      �   U  3             �   �  �   H   �  �          �   �   �   �       u   �   �   �       ,  5  @  �          �  3  [     -   L        �  �  w   "  F    H     �      T   �   �  m           �  t      �   \  T  F  �  `  �   �              <  Y   .       M  �  �       y          �  �                x  �  r  �      �  s  �   �      o    �  S  �  u  #   �               !   #  l  �   �   w  E  "  p  �  �   '  �       �       z       N   �  y      P          �  �   �   f      �      '  �  �  g          a  �       x  z      �       �  ?     �  +  �  S  �      e      =   �  G  g  �  �         n  �  d      A       >      �  o   _   W  �   �  X        c          �       Q  �       m  �  �      _  ,  �      �  �      �    �          �       z  g       �    E      j   0  �  �   2  �   �   *  �  ]  "   �   �   �  �  �  x   �  �   �       �   �   �  A      �   R  �       �       %   6  h  ~      /  ]  �  k          �   �      �         ?      8   �   �  '   |  7      @   �   �   �       5      �  �               4  �  T  /   d       ~  V          �  $      �      �   �       2  !  e    �   t   V  �      X   D       M   �  �       j  `   Y         �               %  9   .        9  j    	  �           �        h   ^       �     �   �  i  ~       �   `  v  �       C      �  �  �   �    �   
   �  �        K                q  �   R   �   �  <          h  ;      �   �    �   0       �    1   �  �       Z  �   w  �  �  (       �   &  �           b  �  W   C  (     U   ;   �                 >   �  �                    #  \      �         7   9  �      �   �              �  �   J      �   !  �    �   a  �   �  v   M  �  �        �   Q     -  i   I       Y  I  �  +         u  �  ]   Z   4   8    *  �   d  �      2   �     )          �  �  �      �  �   �       �      n   B                        �     L   �     �  	          S   k                    G  �  	      r   N  �      �  �      $   .  �  �    &   A    [      �  =  $  s      :  B   b         �     ;      �      X  l  �       {    �  F   �         6  �                �      v    �    G   �   7  W  k       ^      �   �         �  �       |  O   i      C   U  �   �          �  s   �       J   �        ?   �  �   �  �       �   �  [           =      {      �       �   �  �             �  �    L  >  �          B      �              �      �  }     N  �  8      �  �  �   
  m    1  *   �  �          D  &  K     �  }        }      �   �   5   �  +      O  �  K   �   �               �  {   �   �  �  c      1  e          �  �   �  
  �      �  \       /  )           �           �       �          �      �  %    �  �   �  �  )      f          �       �    q  �          �  �  �  �      H  ^  Z      �  �  �  p         P  �      a     E   �  �   �  y       �           �   (      _  D  �           O  �       �  ,       |   �       n  Q  -      �     		unmounted or mounted read-only.  Copy proceeding...
 	Is target "%s" too small?
 	will clear entry "%s"
 	will junk block
 	with the external log using %llu blocks  	would clear entry "%s"
 	would junk block
 
 create a backup file which contains quota limits information
 -g -- dump out group quota limits
 -p -- dump out project quota limits
 -u -- dump out user quota limits (default)
 -f -- write the dump out to the specified file

 
 disable quota enforcement on a filesystem

 If a filesystem is mounted and is currently enforcing quota, this
 provides a mechanism to switch off the enforcement, but continue to
 perform used space (and used inodes) accounting.
 The affected quota type is -g (groups), -p (projects) or -u (users).

 
 display a summary of filesystem ownership

 -a -- summarise for all local XFS filesystem mount points
 -c -- display three columns giving file size in kilobytes, number of files
       of that size, and cumulative total of kilobytes in that size or
       smaller file.  The last row is used as an overflow bucket and is the
       total of all files greater than 500 kilobytes.
 -v -- display three columns containing the number of kilobytes not
       accessed in the last 30, 60, and 90 days.
 -g -- display group summary
 -p -- display project summary
 -u -- display user summary
 -b -- display number of blocks used
 -i -- display number of inodes used
 -r -- display number of realtime blocks used
 -n -- skip identifier-to-name translations, just report IDs
 -N -- suppress the initial header
 -f -- send output to a file
 The (optional) user/group/project can be specified either by name or by
 number (i.e. uid/gid/projid).

 
 display usage and quota information

 -g -- display group quota information
 -p -- display project quota information
 -u -- display user quota information
 -b -- display number of blocks used
 -i -- display number of inodes used
 -r -- display number of realtime blocks used
 -h -- report in a human-readable format
 -n -- skip identifier-to-name translations, just report IDs
 -N -- suppress the initial header
 -v -- increase verbosity in reporting (also dumps zero values)
 -f -- send output to a file
 The (optional) user/group/project can be specified either by name or by
 number (i.e. uid/gid/projid).

 
 enable quota enforcement on a filesystem

 If a filesystem is mounted and has quota accounting enabled, but not
 quota enforcement, enforcement can be enabled with this command.
 With the -v (verbose) option, the status of the filesystem will be
 reported after the operation is complete.
 The affected quota type is -g (groups), -p (projects) or -u (users)
 and defaults to user quota (multiple types can be specified).

 
 list projects or setup a project tree for tree quota management

 Example:
 'project -c logfiles'
 (match project 'logfiles' to a directory, and setup the directory tree)

 Without arguments, report all projects found in the /etc/projects file.
 The project quota mechanism in XFS can be used to implement a form of
 directory tree quota, where a specified directory and all of the files
 and subdirectories below it (i.e. a tree) can be restricted to using a
 subset of the available space in the filesystem.

 A managed tree must be setup initially using the -c option with a project.
 The specified project name or identifier is matched to one or more trees
 defined in /etc/projects, and these trees are then recursively descended
 to mark the affected inodes as being part of that tree - which sets inode
 flags and the project identifier on every file.
 Once this has been done, new files created in the tree will automatically
 be accounted to the tree based on their project identifier.  An attempt to
 create a hard link to a file in the tree will only succeed if the project
 identifier matches the project identifier for the tree.  The xfs_io utility
 can be used to set the project ID for an arbitrary file, but this can only
 be done by a privileged user.

 A previously setup tree can be cleared from project quota control through
 use of the -C option, which will recursively descend the tree, clearing
 the affected inodes from project quota control.

 The -c option can be used to check whether a tree is setup, it reports
 nothing if the tree is correct, otherwise it reports the paths of inodes
 which do not have the project ID of the rest of the tree, or if the inode
 flag is not set.

 The -p <path> option can be used to manually specify project path without
 need to create /etc/projects file. This option can be used multiple times
 to specify multiple paths. When using this option only one projid/name can
 be specified at command line. Note that /etc/projects is also used if exists.

 The -d <depth> option allows to descend at most <depth> levels of directories
 below the command line arguments. -d 0 means only apply the actions
 to the top level of the projects. -d -1 means no recursion limit (default).

 The /etc/projid and /etc/projects file formats are simple, and described
 on the xfs_quota man page.

 
 modify quota enforcement timeout for the current filesystem

 Example:
 'timer -i 3days'
 (soft inode limit timer is changed to 3 days)

 Changes the timeout value associated with the block limits, inode limits
 and/or realtime block limits for all users, groups, or projects on the
 current filesystem.
 As soon as a user consumes the amount of space or number of inodes set as
 the soft limit, a timer is started.  If the timer expires and the user is
 still over the soft limit, the soft limit is enforced as the hard limit.
 The default timeout is 7 days.
 -d -- set the default values, used the first time a file is created
 -g -- modify group quota timer
 -p -- modify project quota timer
 -u -- modify user quota timer
 -b -- modify the blocks-used timer
 -i -- modify the inodes-used timer
 -r -- modify the blocks-used timer for the (optional) realtime subvolume
 The timeout value is specified as a number of seconds, by default.
 However, a suffix may be used to alternatively specify minutes (m),
 hours (h), days (d), or weeks (w) - either the full word or the first
 letter of the word can be used.

 
 modify quota limits for the specified user

 Example:
 'limit bsoft=100m bhard=110m tanya

 Changes the soft and/or hard block limits, inode limits and/or realtime
 block limits that are currently being used for the specified user, group,
 or project.  The filesystem identified by the current path is modified.
 -d -- set the default values, used the first time a file is created
 -g -- modify group quota limits
 -p -- modify project quota limits
 -u -- modify user quota limits
 The block limit values can be specified with a units suffix - accepted
 units are: k (kilobytes), m (megabytes), g (gigabytes), and t (terabytes).
 The user/group/project can be specified either by name or by number.

 
 modify the number of quota warnings sent to the specified user

 Example:
 'warn 2 jimmy'
 (tell the quota system that two warnings have been sent to user jimmy)

 Changes the warning count associated with the block limits, inode limits
 and/or realtime block limits for the specified user, group, or project.
 When a user has been warned the maximum number of times allowed, the soft
 limit is enforced as the hard limit.  It is intended as an alternative to
 the timeout system, where the system administrator updates a count of the
 number of warnings issued to people, and they are penalised if the warnings
 are ignored.
 -d -- set maximum warning count, which triggers soft limit enforcement
 -g -- set group quota warning count
 -p -- set project quota warning count
 -u -- set user quota warning count
 -b -- set the blocks-used warning count
 -i -- set the inodes-used warning count
 -r -- set the blocks-used warn count for the (optional) realtime subvolume
 The user/group/project can be specified either by name or by number.

 
 remove any space being used by the quota subsystem

 Once quota has been switched 'off' on a filesystem, the space that
 was allocated to holding quota metadata can be freed via this command.
 The affected quota type is -g (groups), -p (projects) or -u (users)
 and defaults to user quota (multiple types can be specified).

 
 report used space and inodes, and quota limits, for a filesystem
 Example:
 'report -igh'
 (reports inode usage for all groups, in an easy-to-read format)
 This command is the equivalent of the traditional repquota command, which
 prints a summary of the disk usage and quotas for the current filesystem,
 or all filesystems.
 -a -- report for all mounted filesystems with quota enabled
 -h -- report in a human-readable format
 -n -- skip identifier-to-name translations, just report IDs
 -N -- suppress the header from the output
 -t -- terse output format, hides rows which are all zero
 -L -- lower ID bound to report on
 -U -- upper ID bound to report on
 -g -- report group usage and quota information
 -p -- report project usage and quota information
 -u -- report user usage and quota information
 -b -- report blocks-used information only
 -i -- report inodes-used information only
 -r -- report realtime-blocks-used information only

 
 reports the number of free disk blocks and inodes

 This command reports the number of total, used, and available disk blocks.
 It can optionally report the same set of numbers for inodes and realtime
 disk blocks, and will report on all known XFS filesystem mount points and
 project quota paths by default (see 'print' command for a list).
 -b -- report the block count values
 -i -- report the inode count values
 -r -- report the realtime block count values
 -h -- report in a human-readable format
 -N -- suppress the header from the output

 
LOG REC AT LSN cycle %d block %d (0x%x, 0x%x)
 
Use 'help commandname' for extended help.
 
fatal error --                 - agno = %d
         - block cache size set to %d entries
         - found root inode chunk
         - process known inodes and perform inode discovery...
         - process newly discovered inodes...
         - reporting progress in intervals of %s
         - scan (but don't clear) agi unlinked lists...
         - scan and clear agi unlinked lists...
         - scan filesystem freespace and inode maps...
         - zero log...
       Used       Soft       Hard    Warn/ Grace            Used       Soft       Hard    Warn/Grace          Inodes      IUsed      IFree IUse%%    Size   Used  Avail Use%%   Accounting: %s
   Enforcement: %s
   Inode: #%llu (%llu blocks, %lu extents)
   Used   Soft   Hard Warn/Grace     Used   Soft   Hard Warn/Grace     %lld blocks
  (project %u  1K-blocks       Used  Available  Use%%  FLAG Values:
  FLAGS  Inodes   Used   Free Use%%  Pathname
  at offset %lld
 %-19s %s %-39s %5llu %8llu %10.1fMB %10llu
 %02d:%02d:%02d %c%03d%c  %d	%llu	%llu
 %d day %d hour %d minute %d second %d week %lld allocation groups is too many, maximum is %lld
 %llu directories
 %llu regular files
 %llu special files
 %llu symbolic links
 %s %s %s filesystem failed to initialize
%s: Aborting.
 %s (%s) %s:
 %s (%s):
 %s - project identifier is not set (inode=%u, tree=%u)
 %s - project inheritance flag is not set
 %s already fully defragmented.
 %s freespace btree block claimed (state %d), agno %d, bno %d, suspect %d
 %s grace time: %s
 %s quota are not enabled on %s
 %s quota on %s (%s)
 %s quota state on %s (%s)
 %s start inode=%llu
 %s version %s
 %s will take about %.1f megabytes
 %s:  Warning -- a filesystem is mounted on the source device.
 %s:  a filesystem is mounted on target device "%s".
%s cannot copy to mounted filesystems.  Aborting
 %s:  cannot grow data section.
 %s:  could not write to logfile "%s".
 %s:  couldn't open source "%s"
 %s:  couldn't open target "%s"
 %s:  failed to write last block
 %s:  lseek64 failure at offset %lld
 %s:  offset was probably %lld
 %s:  read failure at offset %lld
 %s:  write error on target %d "%s" at offset %lld
 %s:  xfsctl on "%s" failed.
 %s: %s appears to contain a partition table (%s).
 %s: %s appears to contain an existing filesystem (%s).
 %s: %s appears to contain something weird according to blkid
 %s: %s contains a mounted filesystem
 %s: %s filesystem failed to initialize
%s: Aborting.
 %s: %s has a real-time section.
%s: Aborting.
 %s: %s has an external log.
%s: Aborting.
 %s: %s is not a mounted XFS filesystem
 %s: %s possibly contains a mounted filesystem
 %s: %s: device %lld is not open
 %s: %s: too many open devices
 %s: Directory defragmentation not supported
 %s: Stats not yet supported for XFS
 %s: Unknown child died (should never happen!)
 %s: Use the -f option to force overwrite.
 %s: Warning: `%s' in quota blocks is 0 (unlimited).
 %s: XFS_IOC_FSGEOMETRY xfsctl failed: %s
 %s: XFS_IOC_FSGROWFSDATA xfsctl failed: %s
 %s: XFS_IOC_FSGROWFSLOG xfsctl failed: %s
 %s: XFS_IOC_FSGROWFSRT xfsctl failed: %s
 %s: bad format string %s
 %s: can't determine device size: %s
 %s: can't get geometry ["%s"]: %s
 %s: cannot allocate space for file
 %s: cannot clear project on %s: %s
 %s: cannot defragment: %s: Not XFS
 %s: cannot determine geometry of filesystem mounted at %s: %s
 %s: cannot find any valid arguments
 %s: cannot find group %s
 %s: cannot find mount point for path `%s': %s
 %s: cannot find project %s
 %s: cannot find user %s
 %s: cannot get flags on %s: %s
 %s: cannot initialise path table: %s
 %s: cannot open %s: %s
 %s: cannot read attrs on "%s": %s
 %s: cannot realloc %d bytes
 %s: cannot repair this filesystem.  Sorry.
 %s: cannot set limits: %s
 %s: cannot set project on %s: %s
 %s: cannot set timer: %s
 %s: cannot set warnings: %s
 %s: cannot setup path for mount %s: %s
 %s: cannot setup path for project %s: %s
 %s: cannot setup path for project dir %s: %s
 %s: cannot stat %s: %s
 %s: cannot stat file %s
 %s: char special not supported: %s
 %s: could not open %s: %s
 %s: could not stat: %s: %s
 %s: couldn't initialize XFS library
%s: Aborting.
 %s: couldn't open log file "%s"
 %s: device %lld is already open
 %s: failed stat64 on %s: %s
 %s: failed to access data device for %s
 %s: failed to access external log for %s
 %s: failed to access realtime device for %s
 %s: failed to open %s: %s
 %s: fdopen on %s failed: %s
 %s: file busy
 %s: file modified defrag aborted
 %s: file type not supported
 %s: filesystem failed to initialize
 %s: final argument is not directory
 %s: fopen on %s failed: %s
 %s: getmntinfo() failed: %s
 %s: growfs operation in progress already
 %s: invalid group name: %s
 %s: invalid project name: %s
 %s: invalid user name: %s
 %s: log growth not supported yet
 %s: malloc of %d bytes failed.
 %s: marked as don't defrag, ignoring
 %s: must specify a project name/ID
 %s: must specify files to copy
 %s: open failed
 %s: open of %s failed: %s
 %s: open on %s failed: %s
 %s: out of memory
 %s: project ID %u (%s) doesn't match ID %u (%s)
 %s: read failed on %s: %s
 %s: skipping special file %s
 %s: specified file ["%s"] is not on an XFS filesystem
 %s: stat64 of %s failed
 %s: unable to extract mount options for "%s"
 %s: unknown flag
 %s: unrecognised argument %s
 %s: write error: %s
 %s: xfs_bulkstat: %s
 %s: xfsctl on file "%s" failed.
 %s: xfsctl(XFS_IOC_GETBMAPX) iflags=0x%x ["%s"]: %s
 %s: zero size, ignoring
 %sFilesystem          Pathname
 (empty) (or %s)  * ERROR: bad magic number in log header: 0x%x
 * ERROR: log format incompatible (log=%d, ours=%d)
 * ERROR: mismatched uuid in log
*            SB : %s
*            log: %s
 ,  , %s , bogus flags will be cleared
 , bogus flags would be cleared
 , junking %d entries
 , would junk %d entries
 ,real-time -%c %s option cannot have a value
 -%c %s option requires a value
 -i infile | -f N [off len] -m option cannot be used with -o bhash option
 -o bhash option cannot be used with -m option
 0x%lx  %lu pages (%llu : %lu)
 AG AG superblock geometry info conflicts with filesystem geometry AG-OFFSET AGF geometry info conflicts with filesystem geometry AGI geometry info conflicts with filesystem geometry ALERT: The filesystem has valuable metadata changes in a log which is being
destroyed because the -L option was used.
 Aborting XFS copy - no more targets.
 Aborting XFS copy - reason Aborting XFS copy -- logfile error -- reason: %s
 Aborting target %d - reason All copies completed.
 Allocation of the realtime summary failed Bad log Blocks Can't use %s: mode=0%o own=%d nlink=%d
 Cannot find %d
 Check logfile "%s" for more details
 Checking project %s (path %s)...
 Clearing project %s (path %s)...
 Command [fpdir] :  Completion of the realtime bitmap failed Completion of the realtime summary failed Couldn't allocate target array
 Couldn't initialize global thread mask
 Couldn't rewind on temporary file
 Creating file %s
 Directory creation failed Disk quotas for %s %s (%u)
Filesystem%s ERROR: The filesystem has valuable metadata changes in a log which needs to
be replayed.  Mount the filesystem to replay the log, and unmount it before
re-running xfs_repair.  If you are unable to mount the filesystem, then use
the -L option to destroy the log and attempt a repair.
Note that destroying the log may cause corruption -- please attempt a mount
of the filesystem before doing this.
 EXT End of range ?  Error completing the realtime space Error creating first semaphore.
 Error creating thread for target %d
 Error creating thread mutex %d
 Error initializing btree buf 1
 Error initializing the realtime space Error initializing wbuf 0
 Filesystem   Filesystem           Found something
 Found unsupported filesystem features.  Exiting now.
 Group Illegal value %s for -%s option
 Inode allocation btrees are too corrupted, skipping phases 6 and 7
 Inode allocation failed Inodes Must fit within an allocation group.
 No improvement will be made (skipping): %s
 No modify flag set, skipping filesystem flush and exiting.
 No modify flag set, skipping phase 5
 No paths are available
 Note - quota info will be regenerated on next quota mount.
 OFF ON Phase 2 - using external log on %s
 Phase 2 - using internal log
 Phase 3 - for each AG...
 Phase 7 - verify and correct link counts...
 Phase 7 - verify link counts...
 Primary superblock would have been modified.
Cannot proceed further in no_modify mode.
Exiting now.
 Processed %d (%s and cmdline) paths for project %s with recursion depth %s (%d).
 Project Realtime Blocks See "%s" for more details.
 Setting up project %s (path %s)...
 Skipping %s: could not get XFS geometry
 Skipping %s: not mounted rw
 THE FOLLOWING COPIES FAILED TO COMPLETE
 TOTAL This filesystem contains features not understood by this program.
 This filesystem has an external log.  Specify log device with the -l option.
 This filesystem has uninitialized extent flags.
 This filesystem uses feature(s) not yet supported in this release.
Please run a more recent version of xfs_repair.
 Transfer data directly between file descriptors UUID = %s
 Unable to get geom on fs for: %s
 User WARNING:  source filesystem inconsistent.
 WARNING:  unknown superblock version %d
 WARNING:  you have disallowed aligned inodes but this filesystem
	has aligned inodes.  The filesystem will be downgraded.
	This will permanently degrade the performance of this filesystem.
 WARNING:  you have disallowed aligned inodes but this filesystem
	has aligned inodes.  The filesystem would be downgraded.
	This would permanently degrade the performance of this filesystem.
 WARNING:  you have disallowed attr2 attributes but this filesystem
	has attributes.  The filesystem will be downgraded and
	all attr2 attributes will be removed.
 WARNING:  you have disallowed attr2 attributes but this filesystem
	has attributes.  The filesystem would be downgraded and
	all attr2 attributes would be removed.
 WARNING:  you have disallowed attributes but this filesystem
	has attributes.  The filesystem will be downgraded and
	all attributes will be removed.
 WARNING:  you have disallowed attributes but this filesystem
	has attributes.  The filesystem would be downgraded and
	all attributes would be removed.
 WARNING:  you have disallowed quotas but this filesystem
	has quotas.  The filesystem will be downgraded and
	all quota information will be removed.
 WARNING:  you have disallowed quotas but this filesystem
	has quotas.  The filesystem would be downgraded and
	all quota information would be removed.
 WARNING:  you have disallowed superblock-feature-bits-allowed
	but this superblock has feature bits.  The superblock
	will be downgraded.  This may cause loss of filesystem meta-data
 WARNING:  you have disallowed superblock-feature-bits-allowed
	but this superblock has feature bits.  The superblock
	would be downgraded.  This might cause loss of filesystem
	meta-data.
 WARNING: this may be a newer XFS filesystem.
 Warning:  group quota information was cleared.
Group quotas can not be enforced until limit information is recreated.
 Warning:  group quota information would be cleared.
Group quotas could not be enforced until limit information was recreated.
 Warning:  no quota inodes were found.  Quotas disabled.
 Warning:  no quota inodes were found.  Quotas would be disabled.
 Warning:  project quota information was cleared.
Project quotas can not be enforced until limit information is recreated.
 Warning:  project quota information would be cleared.
Project quotas could not be enforced until limit information was recreated.
 Warning:  quota inodes were cleared.  Quotas disabled.
 Warning:  quota inodes would be cleared.  Quotas would be disabled.
 Warning:  user quota information was cleared.
User quotas can not be enforced until limit information is recreated.
 Warning:  user quota information would be cleared.
User quotas could not be enforced until limit information was recreated.
 XFS_IOC_SWAPEXT failed: %s: %s
 [--------] [------] [--none--] [-D | -R] [-D | -R] [extsize] [-D | -R] projid [-R|-D] [+/- [-R|-D|-a|-v] [-adlpv] [-n nx] [-ais] [off len] [-bir] [-gpu] [-acv] [-f file] [-bir] [-gpu] [-ahntLNU] [-f file] [-bir] [-gpu] [-ahnt] [-f file] [-bir] [-gpu] [-hnNv] [-f file] [id|name]... [-bir] [-gpu] value -d|id|name [-bir] [-hn] [-f file] [-cpv] [-c|-s|-C|-d <depth>|-p <path>] project ... [-dnrsw] [off len] [-drsw] [off len] [-f] [-gpu] [-a] [-v] [-f file] [-gpu] [-f file] [-gpu] [-v] [-gpu] bsoft|bhard|isoft|ihard|rtbsoft|rtbhard=N -d|id|name [-none-] [-r] [-S seed] [off len] [-r] [off len] [-v] [N] [N] | [-rwx] [off len] [blocks] [command] [nentries] [off len] agf_btreeblks %u, counted %u in ag %u
 agf_freeblks %u, counted %u in ag %u
 agf_longest %u, counted %u in ag %u
 agi unlinked bucket %d is %u in ag %u (inode=%lld)
 agi_count %u, counted %u in ag %u
 agi_freecount %u, counted %u in ag %u
 agsize (%lld) not a multiple of fs blk size (%d)
 allocates zeroed space for part of a file allocation of the realtime bitmap failed, error = %d
 already have block usage information
 already have external log noted, can't have both
 already have internal log noted, can't have both
 attempted to perform I/O beyond EOF attr available reserved blocks = %llu
 avl_insert: Warning! duplicate range [%llu,%llu]
 bad agbno %u for btbcnt root, agno %d
 bad agbno %u for btbno root, agno %d
 bad agbno %u for inobt root, agno %d
 bad agbno %u in agfl, agno %d
 bad argument count %d to %s, expected %d arguments
 bad argument count %d to %s, expected at least %d arguments
 bad argument count %d to %s, expected between %d and %d arguments
 bad block number for bmap %s
 bad blocksize field bad blocksize log field bad blocktrash count %s
 bad btree nrecs (%u, min=%u, max=%u) in bt%s block %u/%u
 bad copy to %s
 bad error code - %d
 bad fsblock %s
 bad inode size or inconsistent with number of inodes/block bad magic # %#x in bt%s block %d/%d
 bad magic # %#x in inobt block %d/%d
 bad magic number bad option for blockget command
 bad option for blocktrash command
 bad option for blockuse command
 bad option for bmap command
 bad or unsupported version bad read of %d bytes from %s: %s
 bad sector size bad shared version number in superblock bad stale count bad state in block map %d
 bad stripe unit in superblock bad stripe width in superblock bad uncorrected agheader %d, skipping ag...
 bad write of %d bytes to %s: %s
 block (%d,%d-%d) multiply claimed by %s space tree, state - %d
 block device block usage information not allocated
 blocks blocksize %llu too large
 blocksize %llu too small
 blocksize not available yet.
 bogus quota flags 0x%x set in superblock calloc failed in dir_hash_init
 can't allocate memory for superblock
 can't get size of data subvolume
 can't read agfl block for ag %d
 can't read block %u/%u for trashing
 can't read btree block %d/%d
 cannot allocate buffer (%d)
 cannot allocate worker item, error = [%d] %s
 cannot create tmpdir: %s: %s
 cannot create worker threads, error = [%d] %s
 cannot get realtime geometry for: %s
 cannot open: %s: Permission denied
 cannot reserve space cannot specify both %s and -d name=%s
 cannot strdup command '%s': %s
 change project identifier on the currently open file char device check, setup or clear project quota trees checklen 0/1 ?  clearing entry
 clearing inode number
 close the current open file command "%s" not found
 command %s not found
 correcting
 correcting imap
 could not allocate buf: %s
 could not create tmpdir: %s: %s
 could not open tmp file: %s: %s
 could not open: inode %llu
 could not pre-allocate tmp space: %s
 could not read %s, starting with %s
 could not remove tmpdir: %s: %s
 couldn't allocate realtime bitmap, error = %d
 couldn't fork sub process: couldn't get superblock
 couldn't iget realtime bitmap inode -- error - %d
 couldn't malloc dir2 buffer list
 couldn't malloc dir2 shortform copy
 creation of .. entry failed (%d), filesystem may be out of space
 current data data and log sector sizes must be equal for internal logs
 data blocks changed from %lld to %lld
 data size %lld too large, maximum is %lld
 data size %lld too small, old size is %lld
 data size unchanged, skipping
 data su must be a multiple of the sector size (%d)
 data su/sw must not be used in conjunction with data sunit/swidth
 data sunit/swidth must not be used in conjunction with data su/sw
 day days direct directory directory                               bsize   blocks    megabytes    logsize
 directory create error directory createname error dirsize=%llu
 disable quota enforcement disabled done
 dubious inode btree block header %d/%d
 dump quota information for backup utilities enable quota enforcement enabled end of range ?  entry contains illegal value in attribute named SGI_CAP_FILE
 error allocating space for a file error reserving space for a file existing superblock read failed: %s
 exit the program expected level %d got %d in bt%s block %d/%d
 expected level %d got %d in inobt block %d/%d
 extents external extra arguments
 failed reading extents failed reading extents: inode %llu failed to create prefetch thread: %s
 failed to get inode attrs: %s
 failed to initialize prefetch cond var
 failed to initialize prefetch mutex
 fd.path = "%s"
 fifo filesystem mkfs-in-progress bit set find mapping pages that are memory resident flags:  flipped foreign free free block usage information freeblk count %d != flcount %d in ag %d
 frees reserved space associated with part of a file frees space associated with part of a file freeze filesystem of current file from fullblocks=%llu
 geom.agblocks = %u
 geom.agcount = %u
 geom.bsize = %u
 geom.datablocks = %llu
 geom.rtblocks = %llu
 geom.rtextents = %llu
 get overall quota state information get/set enforcement warning counter get/set quota enforcement timeouts give advice about use of memory group group quota hash value mismatch help for one or all commands hole illegal block size %d
 illegal data length %lld, not a multiple of %d
 illegal directory block size %d
 illegal inode size %d
 illegal log length %lld, not a multiple of %d
 illegal log sector size %d
 illegal sector size %d
 illegal sector size %d; hw sector is %d
 inconsistent filesystem geometry in realtime filesystem component inconsistent filesystem geometry information inconsistent inode alignment value infinite inject errors into a filesystem ino %10llu count %2d mask %016llx
 inode block %d/%d bad state, (state %d)
 inode block %d/%d multiply claimed, (state %d)
 inode btree block claimed (state %d), agno %d, bno %d, suspect %d
 inode chunk claims used block, inobt block - agno %d, bno %d, inopb %d
 inode max pct unchanged, skipping
 insufficient freespace for: %s: size=%lld: ignoring
 internal internal log invalid isize=%llu
 length argument too large -- %lld
 limited list current open files and memory mappings list extended inode flags set on the currently open file list known mount points and projects list project identifier set on the currently open file log blocks changed from %d to %d
 log changed from %s to %s
 log size unchanged, skipping
 log stripe unit adjusted to 32KiB
 low on realtime free space: %s: ignoring file
 lseek64 error malloc failed: %s
 maximum indicated percentage of inodes > 100% modify quota limits must run blockget first
 name create failed in %s (%d), filesystem may be out of space
 name or value attribute lengths are too large,
 need at least %lld allocation groups
 need at most %lld allocation groups
 no data entry no device name given in argument list
 no error no files are open, try 'help open'
 no leaf entry no log subvolume or internal log
 no mapped regions, try 'help mmap'
 non-direct non-existent non-numeric extsize argument -- %s
 non-numeric mode -- %s
 non-sync none not enough secondary superblocks with matching geometry null off off len ok open the file specified by path open(%s) failed: %s
 option respecified
 or about %.1f megabytes
 out of memory on realloc: %s
 out of memory: %s
 out-of-order bno btree record %d (%u %u) block %u/%u
 out-of-order cnt btree record %d (%u %u) block %u/%u
 pct permanently switch quota off for a path prefetch corruption
 primary project projects file "%s" doesn't exist
 ran out of disk space!
 read-only read-write reads a number of bytes at a specified offset reads data from a region in the current memory mapping realloc failed: %s
 realtime bitmap realtime bitmap inode marked free,  realtime blocks changed from %lld to %lld
 realtime size %lld too large, maximum is %lld
 realtime size %lld too small, old size is %lld
 realtime summary realtime summary inode marked free,  rebuilding
 regular regular file remove quota extents from a filesystem repairing table
 report filesystem quota information report process resource usage reserved blocks = %llu
 reserves space associated with part of a file reset bad agf for ag %d
 reset bad agi for ag %d
 reset bad sb for ag %d
 restore quota limits from a backup file root inode chunk not found
 root inode marked free,  s sb_fdblocks %lld, counted %lld
 sb_icount %lld, counted %lld
 sb_ifree %lld, counted %lld
 secondary set set current path, or show the list of paths set the current file show free and used counts for blocks and inodes show usage and limits size %lld of data subvolume is too small, minimum %d blocks
 size %s specified for data subvolume is too large, maximum is %lld blocks
 size of range ?  socket stat.atime = %s stat.blocks = %lld
 stat.ctime = %s stat.ino = %lld
 stat.mtime = %s stat.size = %lld
 stat.type = %s
 statfs.f_bavail = %lld
 statfs.f_blocks = %lld
 statfs.f_bsize = %lld
 statfs.f_ffree = %lld
 statfs.f_files = %lld
 statfs.f_frsize = %lld
 statistics on the currently open file statistics on the filesystem of the currently open file strdup(%s) failed
 string summarize filesystem ownership symbolic link sync sync failed: %s: %s
 tmp file name too long: %s
 tmpdir already exists: %s
 to too many allocation groups for size = %lld
 unable to get bstat on %s: %s
 unable to open: %s: %s
 uncertain inode block %d/%d already known
 unfreeze filesystem of current file unknown option -%c %s
 user user quota value %d is out of range (0-%d)
 volume log volume rt warning: data length %lld not a multiple of %d, truncated to %lld
 warning: device is not properly aligned %s
 warning: log length %lld not a multiple of %d, truncated to %lld
 would clear entry
 would clear inode number
 would correct
 would correct imap
 would rebuild
 would repair table
 would reset bad agf for ag %d
 would reset bad agi for ag %d
 would reset bad sb for ag %d
 write error write(%s) failed: %s
 xfs xfs_bmap_last_offset failed -- error - %d
 xfs_bunmapi failed -- error - %d
 xfs_trans_reserve returned %d
 zero length name entry in attribute fork, Project-Id-Version: xfsprogs
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-11-27 10:38+1100
PO-Revision-Date: 2013-09-12 23:15+0000
Last-Translator: Leandro Vital <Unknown>
Language-Team: Portuguese (Brazil) <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:23+0000
X-Generator: Launchpad (build 18115)
 		não montado ou montado como somente leitura. Prosseguindo com cópia...
 	O destino "%s" não é muito pequeno?
 	limpará entrada "%s"
 	descartará bloco
 	com o log externo usando %llu blocos 
	 representa uma tabela de caracteres.  Por favor, escreva exatamente da mesma forma. 	 na sua tradução. representa o caracter de espaço. Pressione espaço numa posição equivalente à da tradução.  	limparia entrada "%s"
 	descartaria bloco
 
 cria um arquivo de cópia de segurança que contém informações sobre limites de cota
 -g -- saída dos limites de cota de grupo
 -p -- saída dos limites de cota de projetos
 -u -- saída de limite de cotas por usuário (padrão)
 -f -- escreve a saída para o arquivo especificado

 
 desabilitar imposição de cotas num sistema de arquivos

 Se um sistema de arquivos está montado e impondo cota, isto
 fornece um mecanismo para desativar a imposição, mas continuar a
 fazer contabilidade de espaço usado (e nós usados).
 O tipo de cota afetado é -g (grupos), -p (projetos) ou -u (usuários).

 
 mostra um resumo das propriedades do sistema de arquivos

 -a -- sumariza os pontos de montagem de todos os sistemas de arquivos XFS locais
 -c -- mostra três colunas disponibilizando tamanho do arquivo em kilobytes, números dos arquivos
       daquele tamanho, e total acumulado de kilobytes naquele tamanho ou
       arquivos menores. A última linha é usada como um espaço de armazenamento e é o
       total de todos os arquivos maiores que 500 kilobytes.
 -v -- mostra três colunas contendo o número de kilobytes nao
       acessados nos últimos 30, 60, e 90 dias.
 -g -- mostra o sumário de grupos
 -p -- mostra o sumário de projetos
 -u -- mostra o sumário de usuários
 -b -- mostra o número de blocos usados
 -i -- mostra o número de estrutura de dados usados
 -r -- mostra o número de blocos usados em tempo real
 -n -- retira traduções com nome do identificador, apenas descreve IDs
 -N -- retira o cabeçalho inciial
 -f -- envia saída para o arquivo
 O usuário/grupo/projeto (opcional) pode ser especificado tanto pelo nome quanto pelo
 número (i.e. uid/gid/projid).

 
 exibe utilização e informação de quota

 -g -- exibe informações da quota do grupo
 -p -- exibe informações da quota do projeto
 -u -- exibe informações da quota do usuário
 -b -- exibe número de blocos utilizados
 -i -- exibe número de inodes utilizados
 -r -- exibe número de blocos de tempo real utilizados
 -h -- reportar em formato legível
 -n -- pular identificador para traduções de nomes, apenas reportar IDs
 -N -- suprimir o cabeçalho inicial
 -v -- aumentar modo verbose no reporte(também faz dump de valores zero)
 -f -- enviar saída para um arquivo
 O (opcional) user/group/project podem ser especificados tanto por nome como por número (i.e. uid/gid/projid).

 
 habilitar imposição de cota num sistema de arquivos

 Se um sistema de arquivos estiver montado e tiver contabilidade de cota habilitada,
 mas não imposição de cota, a imposição de cota pode ser habilitada com este comando.
 Com a opção -v (verbose), o estado do sistema de arquivos será relatado
 depois que a operação for completada.
 O tipo de cota afetado é -g (grupos), -p (projetos) ou -u (usuários)
 e o padrão é cota de usuário (tipos múltiplos podem ser especificados).

 
 listar projetos ou estabelecer uma árvore de projeto para gerenciamento de cota de árvore

 Exemplo:
 'project -c logfiles'
 (parear o projeto 'logfiles' com um diretório, e preparar a árvore de diretório)

 Sem argumentos, relatar todos projetos encontrados no arquivo /etc/projects.
 O mecanismo de cota de projeto no XFS pode ser usado para implementar uma forma de
 cota de árvore de diretório, onde um diretório especificado e todos os arquivos
 e subdiretórios abaixo dele (ou seja, uma árvore) podem ser restringidos a usar um
 subconjunto do espaço disponível no sistema de arquivos.

 Uma árvore gerenciada deve ser estabelecida inicialmente usando a opção -c com um projeto.
 O nome ou identificador de projeto especificado é pareado com uma ou mais árvores
 definidas em /etc/projects, e então essas árvores são descidas recursivamente
 para marcar os nós afetados como parte daquela árvore - o que define os flags
 dos nós e o identificador de projeto em todos arquivos.
 Uma vez que isso é feito, novos arquivos criados na árvore serão automaticamente
 designados à árvore com base no identificador de projeto.  Uma tentativa de
 criar um link absoluto na árvore só terá sucesso se o identificador
 de projeto conferir com o identificador de projeto da árvore.  O utilitário xfs_io
 pode ser usado para definir o identificador de projeto para uma árvore arbitrária, mas isso
 só pode ser feito por um usuário privilegiado.

 Uma árvore estabelecida anteriormente pode ser liberada do controle de cota de projeto através
 do uso da opção -C, que descerá recursivamente a árvore, liberando os nós afetados do
 controle de cota de projeto.

 A opção -c pode ser usada para verificar se uma árvore está estabelecida. Ela não
 relata nada  se a árvore estiver correta; do contrário, relata os caminhos dos nós que
 não têm o identificador de projeto do resto da árvore, ou se o flag do nó não está
 definido.

 A opção -p <caminho> pode ser usada para especificar manualmente o caminho do projeto
 sem necessidade de criar o arquivo /etc/projects. Esta opção pode ser usada múltiplas vezes
 para especificar caminhos múltiplos. Ao usar essa opção, só um identificador/nome de projeto
 pode ser especificado na linha de comando. Note que /etc/projects também é usado se existir.

 A opção -d <profundidade> permite descer a no máximo <profundidade> níveis
 de diretórios abaixo dos argumentos de linha de comando. -d 0 significa apenas aplicar as
 ações ao nível mais alto dos projetos. -d -1 significa sem limite de recursão (padrão).

 Os formatos de arquivo /etc/projid e /etc/projects são simples, e descritos 
 na página man do xfs_quota.

 
 modificar limite de tempo de imposição de cota para o sistema de arquivos atual

 Exemplo:
 'timer -i 3days'
 (contador de tempo limite para nó soft é alterado para 3 dias)

 Alterar o valor do limite de tempo associado aos limites de bloco, limites de nó
 e/ou limites de bloco em tempo real para todos usuários, grupos ou projetos no
 sistema de arquivos atual.
 Assim que um usuário consome a quantidade de espaço ou ou o número de nós
 definido como o limite soft, um contador de tempo é iniciado.  Se o contador expirar e
 o usuário ainda estiver acima do limite soft, o limite soft é imposto como o limite hard.
 O limite de tempo padrão é 7 dias.
 -d -- definir os valores padrão, usados na primeira vez que um arquivo é criado
 -g -- modificar contador de tempo de cota de grupo
 -p -- modificar contador de tempo de cota de projeto
 -u -- modificar contador de tempo de cota de usuário
 -b -- modificar contador de tempo de blocos usados
 -i -- modificar contador de tempo de nós usados
 -r -- modificar contador de tempo de blocos usados para o subvolume de tempo real (opcional)
 O valor de limite de tempo é especificado como um número de segundos, por padrão.
 Entretanto, um sufixo pode ser usado para especificar minutos (minutes), horas (hours),
 dias (days), ou semanas (weeks) alternativamente - ou a palavra inteira ou a primeira letra
 da palavra pode ser usada.

 
 modificar limites de cota para o usuário especificado

 Exemplo:
 'limit bsoft=100m bhard=110m tanya

 Alterar os limites de bloco soft e/ou hard, limites de nó e/ou limites de bloco em
 tempo real que estejam sendo usados atualmente pelo usuário, grupo ou projeto
 especificado. O sistema de arquivos identificado pelo caminho atual é modificado.
 -d -- definir os valores padrão, usados na primeira vez que um arquivo é criado
 -g -- modificar limites de cota de grupo
 -p -- modificar limites de cota de projeto
 -u -- modificar limites de cota de usuário
 Os valores dos limites de bloco podem ser especificados com um sufixo de unidade -
 unidades aceitas são: k (kilobytes), m (megabytes), g (gigabytes), e t (terabytes).
 O usuário/grupo/projeto pode ser especificado ou por nome ou por número.

 
 modificar o número de avisos de cota enviados ao usuário especificado

 Exemplo:
 'warn 2 jimmy'
 (dizer ao sistema de cota que dois avisos foram enviados ao usuário jimmy)

 Altera a contagem de avisos associada aos limites de bloco, limites de nó
 e/ou limites de bloco de tempo real para o usuário, grupo ou projeto especificado.
 Quando um usuário foi avisado o número máximo de vezes permitido, o limite
 soft é imposto como o limite hard.  Isso foi idealizado como uma alternativa ao
 sistema de limite de tempo, onde o administrador do sistema atualiza uma contagem
 do número de avisos emitidos a pessoas, e elas são penalizadas se os avisos forem
 ignorados.
 -d -- definir contagem máxima de avisos, que desencadeia a imposição do limite soft
 -g -- definir contagem de avisos de cota de grupo
 -p -- definir contagem de avisos de cota de projeto
 -u -- definir contagem de avisos de cota de usuário
 -b -- definir contagem de avisos de blocos usados
 -i -- definir contagem de avisos de nós usados
 -r -- definir contagem de avisos de blocos usados para o subvolume de tempo real (opcional)
 O usuário/grupo/projeto pode ser especificado ou por nome ou por número.

 
 remover qualquer espaço que esteja sendo usado pelo subsistema de cotas

 Uma vez que a cota foi desabilitada num sistema de arquivos, o espaço que
 foi alocado para guardar metadados de cota pode ser liberado via este comando.
 O tipo de cota afetado é -g (grupos), -p (projetos) ou -u (usuários)
 e o padrão é cota de usuário (tipos múltiplos podem ser especificados).

 
 informa estrutura de dados e espaço utilizado, e limites de cotas, para um sistema de arquivos
 Exemplo:
 'informe -igh'
 (informa utilização da estrutura de dados para todos os grupos, em um formato de fácil leitura)
 Este comando equivale ao tradicional comando repquota, que
 imprime um resumo de utilização das cotas de disco para o sistema de arquivos atual,
 ou todos os sistemas de arquivos.
 -a -- informa todos os sistemas de arquivos montados com cota habilitada
 -h -- informa em um formato de leitura humano
 -n -- tira o nome do identificador das traduções, apenas informa IDs
 -N -- retira o cabeçalho da saída
 -t -- formato de saída resumido, esconde filas zeradas
 -L -- salto inferior de ID para informar ligado
 -U -- salto superior de ID para informar ligado
 -g -- descreve a utilização de grupos e cotas
 -p -- descreve a utilização de projetos e cotas
 -u -- descreve a utilização de usuários e cotas
 -b -- descreve apenas informações de blocos utilizados
 -i -- descreve apenas informações de estrutura de dados utilizadas
 -r -- descreve apenas informações de blocos utilizados em tempo real

 
 informa o número de blocos e estrutura de dados livres no disco

 Este comando informa o número total, usado e disponível de blocos de disco.
 Ele pode opcionalmente informar o mesmo número de estrutura de dados e
 blocos de discos em tempo real, e informará em todos os sistemas de arquivos XFS montados e
 caminho de cotas de projetos por padrão (veja a lista do comando 'imprimir').
 -b -- informa o valor de contagem do bloco
 -i -- informa o valor da  estrutura de dados
-r -- informa o valor de contagem dos blocos em tempo real
-h -- informa em um formato de leitura humana
-N --- retira o cabeçalho de saída

 
LOG REC AT LSN ciclo %d bloco %d (0x%x, 0x%x)
 
Uso 'help nomedocomando' para ajuda extendida.
 
erro fatal --                 - agno =%d
         - tamanho de cache do bloco setado para %d entradas
         - encontrada maior parte da estrutura de dados do super usuário.
         - processa a estrutura de dados conhecidos e apresenta estrutura de dados descobertas...
         - processa novas estrutura de dados descobertas...
         - informando progresso em intervalos de %s
         - scaneia (mas não limpa) lista de agi deslincados...
         - scaneia e limpa lista de agi deslincados...
         - scaneia espaço livre no sistema de arquivos e mapeia estrutura de dados...
         - log zero...
       Usado Leve Pesado Aviso/Tempo            Usado Leve Pesado Aviso/Tempo          Estrutura de dados |Usados |Livres |Uso%%    Tamanho usado uso disponível%%   Contabilidade: %s
   Execução: %s
   Inode: #%llu (%llu blocos, %lu extents)
   Usado Leve Pesado Aviso/Tempo     Usado Leve Pesado Aviso/Tempo     %lld blocos
  (projeto %u  Blocos-1K Usados Uso Disponível%%  Valores do marcador
  Bandeiras  Estutura de Dados Usados Uso Livre%%  Nome do caminho
  no offset %lld
 %-19s %s %-39s %5llu %8llu %10.1fMB %10llu
 %02d:%02d:%02d %c%03d%c  %d	%llu	%llu
 %d dia %d hora %d minuto %d segundo %d semana %lld grupos de alocação demais, o máximo é de %lld
 %llu diretórios
 %llu arquivos regulares
 %llu arquivos especiais
 %llu links simbólicos
 %s %s: falhou ao inicializar sistema de arquivo %s
%s: Abortando.
 %s (%s) %s:
 %s (%s):
 %s - identificador do projeto não definido (inode=%u, tree=%u)
 %s - flag de herança do projeto não setada
 %s já esta completamente desfragmentado.
 %s bloco de árvore B do espaço livre tomado (estado %d), agno %d, bno %d, suspeito %d
 %s tempo de carência: %s
 %s cota não habilitada em %s
 %s cota em %s (%s)
 %s estado da cota em %s (%s)
 %s inode de início=%llu
 %s versão %s
 %s levará mais ou menos %.1f megabytes
 %s: Aviso -- um sitema de arquivos está montado no dispositivo de origem.
 %s: um sistema de arquivo está montado no dispositivo "%s".
%s não pode copiar para os sistemas de arquivos montados. Abortando
 %s: seção de dados não pode crescer
 %s: não é possível escrever no arquivo de log "%s".
 %s: não foi possível abrir fonte "%s"
 %s:  não foi possível abrir alvo "%s"
 %s: falha ao escrever no último bloco
 %s: falha em lseek64 no intervalo %lld
 %s: intervalo foi provavelmente %lld
 %s: erro de leitura no intervalo %lld
 %s: erro de escrita no alvo %d "%s" no offset %lld
 %s: xfsctl falhou em "%s".
 %s: %s parece conter uma tabela de partição (%s).
 %s: %s parece conter um sistema de arquivos existente (%s).
 %s: %s parece ter algo estranho de acordo com blkid
 %s: %s contém um sistema de arquivos montado
 %s: falhou ao inicializar sistema de arquivo %s
%s: Abortando.
 %s: %s tem uma seção em tempo real.
%s: Abortando.
 %s: %s tem um registro externo.
%s: Abortando.
 %s: %s não é um sistema de arquivo XFS montado
 %s: %s possivelmente contém um sistema de arquivos montado
 %s: %s: dispositivo %lld não esta aberto
 %s: %s: muitos dispositivos abertos
 %s: Desfragmentação de diretório não suportada
 %s: Estatísticas ainda não suportada pelo XFS
 %s: Término de processo filho desconhecido (jamais deveria ter acontecido!)
 %s: Use a opção -f para forçar sobrescrita.
 %s: Alerta: '%s' in blocos na cota é zero (ilimitado).
 %s: XFS_IOC_FSGEOMETRY xfsctl falhou: %s
 %s: XFS_IOC_FSGROWFSDATA xfsctl falhou: %s
 %s: XFS_IOC_FSGROWFSLOG xfsctl falhou: %s
 %s: XFS_IOC_FSGROWFSRT xfsctl falhou: %s
 %s: string de formato inválido %s
 %s: não pôde determinar o o tamanho do dispositivo: %s
 %s: não foi possível obter geometria ["%s]: %s
 %s: não foi possível alocar espaço para o arquivo
 %s: não foi possível limpar projeto em %s: %s
 %s: não foi possível desfragmentar: %s: Não é XFS
 %s não pôde determinar geometria do sistema de arquivos montado em %s: %s
 %s: não foi possível encontrar nenhum argumento válido
 %s: não foi possível encontrar grupo %s
 %s: não foi possível encontrar um ponto de montagem para o caminho `%s': %s
 %s: não foi possível encontrar o projeto %s
 %s: não foi possível encontrar usuário %s
 %s: não foi possível recuperar flags em %s: %s
 %s: não foi possível inicializar a tabela de caminhos: %s
 %s: não foi possível abrir %s: %s
 %s: não foi possível ler atributos em "%s": %s
 %s: não pode realocar %d bytes
 %s: não pode reparar este sistema de arquivos. Desculpe.
 %s: não foi possível definir limites: %s
 %s: não foi possível definir o projeto em %s: %s
 %s: não foi possível definir o timer: %s
 %s: não foi possível definir alertas: %s
 %s: não foi possível configurar um caminho para a montagem %s: %s
 %s: não foi possível configurar um caminho para o projeto %s: %s
 %s: não foi possível configurar um caminho para o diretório de projeto %s: %s
 %s: não foi possível acessar %s: %s
 %s: não foi possível obter o estado do arquivo %s
 %s: caractere especial não suportado: %s
 %s: não foi possível abrir %s: %s
 %s: Não foi possível checar: %s: %s
 %s: não pode inicializar biblioteca XFS
%s: Abortando.
 %s: não foi possível abrir o arquivo de log "%s"
 %s: dispositivo %lld já esta aberto
 %s: stat64 falhou em %s: %s
 %s: falha ao acessar dispositivo de dados para %s
 %s: falhou ao acessar log externo para %s
 %s: falha ao acessar em tempo real o dispositivo para %s
 %s: falhou ao abrir %s: %s
 %s: fdopen falhou em%s: %s
 %s: arquivo em uso
 %s: arquivo modificado, desfragmentação abortada
 %s: tipo de arquivo não suportado
 %s: falhou ao inicializar sistema de arquivos
 %s: argumento final não é um diretório
 %s: fopen em %s falhou: %s
 %s: getmntinfo() falhou: %s
 %s: operação growfs já esta em progresso
 %s: nome do grupo inválido: %s
 %s: nome do projeto inválido: %s
 %s: nome do usuário inválido: %s
 %s: crescimento de log ainda não suportado
 %s: malloc de %d bytes falhou.
 %s: marcado como não desfragmentar, ignorando
 %s: necessário especificar o ID/nome do projeto
 %s: deve especificar arquivos para copiar
 %s: abertura falhou
 %s: abertura do %s falhou: %s
 %s: falhou ao abrir em %s: %s
 %s: sem memória
 %s: ID do projeto %u (%s) ID não corresponde %u (%s)
 %s leitura falhou em %s: %s
 %s: pulando arquivo especial %s
 %s: arquivo especificado ["%s"] não esta em um sistema de arquivos XFS
 %s: stat64 de %s falhou
 %s: não foi possível extrair as opções de montagem para "%s"
 %s: marcação desconhecida
 %s: argumento não reconhecido %s
 %s: erro de escrita: %s
 %s: xfs_bulkstat: %s
 %s: xfsctl no arquivo "%s" falhou.
 %s: xfsctl(XFS_IOC_GETBMAPX) iflags=0x%x ["%s"]: %s
 %s: tamanho zero, ignorando
 %sNome de caminho do sistema de arquivos
 (vazio) (ou %s)  * ERRO: número mágico incorreto no cabeçalho de registro: 0x%x
 * Erro: formato de registro incompatível (log=%d, ours=%d)
 * Erro: perdido uuid ao logar
* SB:%s
* log: %s
 ,  , %s , opção falsa será limpa
 , flags inválidas seriam limpadas
 , descartando %d entradas
 , descartaria %d entradas
 , tempo real -%c %s opção não pode ter um valor
 Opção -%c %s requer um valor
 -i infile | -f N [off len] opção -m não pode ser utilizada com a opção bhash -o
 opção bhash -o não pode ser utilizada com a opção -m
 0x%lx %lu páginas (%llu : %lu)
 AG informação da geometria do super bloco AG conflita com a geometria do sistema de arquivos AG-OFFSET Informação da geometria do ADF conflita com a geometria do sistema de arquivos Informação da geometria do AGI conflita com a geometria do sistema de arquivos Alerta: O sistema de arquivos possui alterações valiosas de metadados em um log cujo início foi destruído por causa da utilização opção -L.
 Abortando cópia XFS - sem mais alvos.
 Abortando cópia em XFS - razão Abortando cópia em XFS -- erro no arquivo log -- razão: %s
 Abortando alvo %d - razão Todas as cópias completadas.
 Falha na alocação do sumário de tempo real Registro incorreto Blocos Não pode usar %s: mode=0%o own=%d nlink=%d
 Não foi possível encontrar %d
 Veja o arquivo de log "%s" para mais detalhes
 Verificando projeto %s (caminho %s)...
 Limpando projeto %s (caminho %s)...
 Comando [ifpdir]:  Falha na conclusão do bitmap de tempo real Falha na conclusão do sumário de tempo real Impossível alocar a matriz alvo
 Não foi possível inicializar máscara thread global
 Não foi possível voltar atrás em arquivo temporário
 Criando arquivo %s
 Falha na criação do diretório Quotas de disco para %s %s (%u)
Sistema de arquivo%s Erro: O sistema de arquivos possui alterações valiosas de metadados em um log que precisa ser retocado. Monte o sistema de arquivos para retocar o log, e desmonte-o antes de re-rodar o xfs_repair. Se você for incapaz de montar o sistema de arquivos, então use a opçao -L para destruir o log e buscar a reparação.
Saiba que destruindo o log pode causar corrupção -- por favor realizar a montagem
do sistema de arquivos antes de executar esta tarefa.
 EXT Fim do Alcance ?  Erro ao completar o espaço de tempo real Erro ao criar primeiro semáforo
 Erro ao criar thread para o alvo %d
 Erro ao criar thread mutex %d
 Erro ao inicializar bufbtree 1
 Erro ao inicializar o espaço de tempo real Erro ao inicializar wbuf 0
 Sistema de arquivos   Sistema de arquivo           Algo encontrado
 Encontradas opções não suportadas no sistema de arquivos. Saindo agora.
 Grupo Valor ilegal %s para opção -%s
 Alocação de estrutura de dados btrees estão corrompidas, saindo das fases 6 e 7
 Falhou ao alocar Inode Inodes Deve caber dentro de um grupo de alocação
 Nenhuma melhora será feita (pulando): %s
 Nenhuma opção foi modificada, abortando e saindo dos dados do sistema de arquivos.
 Nenhuma opção foi modificada, saindo da fase 5
 Nenhum caminho disponível
 Nota - Informação de cotas serão regeradas na próxima montagem.
 DESLIGADO LIGADO Fase 2 - usando um log externo no %s
 Fase 2 - usando um log interno
 Fase 3 - para cada AG...
 Fase 7 - verificar and corrigir contagem de links...
 Fase 7 - verificar contagem de links...
 Super bloco primário deveria ser modificado.
Não pode mais proceder no modo no_modify.
Saindo agora.
 Processou %d (%s e linha de comando) caminhos para o projeto %s com profundidade de recursão %s (%d).
 Projeto Blocos em tempo real Veja "%s" para maiores detalhes.
 Configurando projeto %s (caminho %s)...
 Pulando %s: não foi possível obter geometria XFS
 Saltando %s: rw não montado
 AS SEGUINTES CÓPIAS FALHARAM ANTES DE COMPLETAR
 TORAL Esse sistema de arquivos contém funcionalidades não entendidas por este programa.
 O sistema de arquivos possui um log externo. Especifique o aparelho de log com a opção -l.
 Este sistema de arquivos tem flags de extensão não-inicializadas.
 Este sistema de arquivos usa propriedades ainda sem suporte nesta versão.
Por favor, rode uma versão mais recente do xfs_repair.
 Transfere dados diretamente entre descritores de arquivo UUID = %s
 Não foi possível obter geom em fs para: %s
 Usuário AVISO: sistema de arquivos de origem está inconsistente.
 AVISO:  versão de superbloco desconhecida %d
 AVISO:  você desabilitou nós desalinhados mas este sistema de arquivos
	tem nós alinhados.  O sistema de arquivos sofrerá um downgrade.
	Isto degradará permanentemente a performance deste sistema de arquivos.
 AVISO:  você desabilitou nós alinhados mas este sistema de arquivos
	tem nós alinhados.  O sistema de arquivos sofreria um downgrade.
	Isto degradaria permanentemente a performance deste sistema de arquivos.
 AVISO:  você desabilitou atributos attr2 mas este sistema de arquivos
	tem atributos.  O sistema de arquivos sofrerá um downgrade e
	todos atributos attr2 serão removidos.
 AVISO:  você desabilitou atributos attr2 mas este sistema de arquivos
	tem atributos.  O sistema de arquivos sofreria downgrade e
	todos atributos attr2 seriam removidos.
 AVISO: você desabilitou atributos mas este sistema de arquivos
	tem atributos. O sistema de arquivos sofrerá um downgradee
	todos atributos serão removidos.
 AVISO:  você desabilitou atributos mas este sistema de arquivos
	tem atributos.  O sistema de arquivos sofreria um downgrade e
	todos atributos seriam removidos.
 AVISO:  você desabilitou cotas mas este sistema de arquivos
	tem cotas.  O sistema de arquivos sofrerá um downgrade e
	todas informações de cota serão removidas.
 AVISO:  você desabilitou cotas mas este sistema de arquivos
	tem  cotas.  O sistema de arquivos sofreria downgrade e
	todas informações de cota seriam removidas.
 AVISO: você desabilitou superblock-feature-bits-allowed
	mas este superbloco tem feature bits. O superbloco
	sofrerá um downgrade.  Isto pode causar perda de metadados do sistema de arquivos
 AVISO:  você desabilitou superblock-feature-bits-allowed
	mas este suberbloco tem feature bits.  O superbloco
	sofreria um downgrade.  Isto pode causar perda de metadados
	do sistema de arquivos.
 AVISO: este pode ser um sistema de arquivos XFS mais recentes
 Aviso: informação de cota do grupo foi clara.
Cotas de grupos não podem ser impostas até que a informação de limite seja recriada.
 Aviso: informação de cota de grupo deve ser clara.
Cotas de grupos não podem ser impostas até que a informação de limite seja recriada.
 Aviso: não foram encontradas cotas na estrutura de dados. cotas desabilitadas.
 Aviso: não foram encontradas cotas na estrutura de dados. Cotas deverão ser desabilitadas.
 Aviso: informação de cota de projeto foi clara.
Cotas de projeto não podem ser impostas até que a informação de limite seja recriada.
 Aviso: informação de cota de projeto deve ser clara.
Cotas de projeto não podem ser impostas até que a informação de limite seja recriada.
 Aviso: estrutura de cotas foram limpas. Cotas desabilitadas.
 Aviso: Estrutura de dados devem ser claras. Cota devem ser desabilitadas.
 Aviso: informação de cota de usuário foi clara.
Cotas de usuário não podem ser impostas até que a informação de limites ser recriada.
 Aviso: informação da cota de usuários devem ser clara.
Cota de usuários não podem ser impostas até que a informação de limite seja recriada.
 XFS_IOC_SWAPEXT falhou: %s: %s
 [--------] [------] [--nenhum--] [-D | -R] [-D | -R] [extsize] [-D | -R] projid [-R|-D] [+/- [-R|-D|-a|-v] [-adlpv] [-n nx] [-ais] [off len] [-bir] [-gpu] [-acv] [-f arquivo] [-bir] [-gpu] [-ahntLNU] [-f arquivo] [-bir] [-gpu] [-ahnt] [-f arquivo] [-bir] [-gpu] [-hnNv] [-f file] [id|name]... [-bir] [-gpu] valor -d|id|nome [-bir] [-hn] [-f file] [-cpv] [-c|-s|-C|-d <profundidade>|-p <caminho>] projeto ... [-dnrsw] [off len] [-drsw] [off len] [-f] [-gpu] [-a] [-v] [-f arquivo] [-gpu] [-f arquivo] [-gpu] [-v] [-gpu] bsoft |bforte |ileve |iforte |rtbleve |rtbforte=N-d|id|nome [-nenhum-] [-r] [-S seed] [off len] [-r] [off len] [-v] [N] [N] | [-rwx] [off len] [blocos] [comando] [nentradas] [off len] agf_btreeblks %u, contado(s) %u em ag %u
 agf_freeblks %u, contado(s) %u em ag %u
 agf_longest %u, contado(s) %u em ag %u
 bucket agi sem link %d é %u em ag %u (nó=%lld)
 agi_count %u, contado(s) %u em ag %u
 agi_freecount %u, contado(s) %u em ag %u
 agsize (%lld) não é múltiplo do tamanho fs blk (%d)
 aloca espaço zeoed para a parte de um arquivo alocação de mapa de bits em tempo real falhou, erro = %d
 Já tenho informação de uso de bloco
 já existe um log externo impresso, não é possível ter ambos
 já existe um log interno impresso, não é possível ter ambos
 tentou criar E/S através do EOF attr blocos reservados disponíveis = %llu
 alv_insere: Alerta! Alcance duplicado [%llu,%llu]
 agbno inválido %u para raiz btbcnt, agno %d
 agbno inválido %u para raiz btbno, agno %d
 agbno inválido %u para raiz inobt, agno %d
 agbno inválido %u em agfl, agno %d
 erro na contagem de argumentos, %d para %s, esperado %d argumentos
 erro na contagem de argumentos, %d para %s, esperado pelo menos %d argumentos
 erro na contagem de argumentos, %d para %s, esperado entre %d e %d argumentos
 número de bloco ruim para bmap %s
 campo de tamanho do bloco incorreto campo de log do tamanho do bloco incorreto contagem ruim blocktrash %s
 registros de árvore B inválidos (%u, min=%u, max=%u) em bt%s bloco %u/%u
 cópia mal sucedida para %s
 erro de código incorreto - %d
 fsblock ruim %s
 tamanho da estrutura de dados incorreta ou inconsistente com o número de estrutura de dados/blocos número mágico inválido # %#x em bt%s bloco %d/%d
 número mágico inválido # %#x em bloco inobt %d/%d
 número mágico incorreto opção ruim para comando blockget
 opção ruim para comando blocktrash
 opção ruim para comando blockuse
 opção ruim para o comando bmap
 versão não suportada ou incorreta leitura mal sucedida de %d bytes de %s: %s
 tamanho do setor incorreto número de versão compartilhadas incorreta no super bloco contagem de inatividade inválida bloco de mapas com estado incorreto %d
 divisão de unidades incorreta no super bloco divisão de largura incorreta no super bloco agheader inválido não-corrigido %d, pulando ag...
 escrita mal sucedida de %d bytes em %s: %s
 bloco (%d,%d-%d) tomado múltiplas vezes por árvore de espaço %s , estado - %d
 Dispositivo de bloco informações sobre o uso de bloco não alocado
 blocos tamanho do bloco %llu muito grande
 tamanho do bloco %llu muito pequeno
 Tamanho do bloco ainda não disponível
 opção de cota falsa 0X%x setada no super bloco não foi possível alocar matriz em dir_hash_init
 não foi possível alocar memória para superbloco
 não pôde obter o tamanho de dados do subvolume
 não pode ler o bloco agfl para o ag %d
 não é possível ler bloco %u/%u para lixeira
 não foi possível ler bloco %d/%d de árvore-B
 não foi possível alocar buffer (%d)
 não pode alocar item de trabalho, erro = [%d] %s
 não é possível criar diretório temporário: %s: %s
 não pode criar linha de trabalho, erro = [%d] %s
 não foi possível obter geometria de tempo real para: %s
 não foi possível abrir: %s: Permissão negada
 não foi possível reservar espaço Não é possível especificar  %s e -d nome=%s
 não foi possível usar o comando strdup '%s': %s
 muda o identificador do projeto no arquivo aberto dispositivo de caractere verificar, configurar ou limpar árvores de cota de projeto tamanho 0/1 ?  limpando entrada
 limpando número do inode
 fechar o arquivo atualmente aberto comando "%s" não encontrado
 comando %s não encontrado
 corrigindo
 corrigindo imap
 não foi possível alocar buffer: %s
 não foi possível criar diretório temporário: %s: %s
 não foi possível abrir arquivo temporário: %s: %s
 não foi possível abrir: inode %llu
 não foi possível pré-alocar espaço temporário: %s
 não foi possível ler %s, começando com %s
 não foi possível remover diretório temporário: %s: %s
 não pode alocar mapa de bits em tempo real, erro = %d
 não poderia criar sub processos: Não foi possível pegar o super bloco
 não foi possível obter nó de bitmap em tempo real -- erro - %d
 não foi possível alocar lista de buffers dir2 na memória
 não foi possível alocar formato reduzido de dir2 na memória
 criação da entrada .. falhou (%d), sistema de arquivo pode estar sem espaço disponível
 atual dado tamanho do setor de log e de dados deve ser igual para os logs internos
 blocos de dados alterados de %lld para %lld
 tamanho dos dados %lld muito grande, o máximo é %lld
 tamanho dos dados %lld muito pequeno, tamanho anterior é %lld
 tamanho dos dados inalterado, pulando
 dado su deve ser um múltiplo do tamanho do setor (%d)
 Dados su/sw não devem ser utilizados em combinação com dados Sunit/swidth
 Dados Sunit/swidth não devem ser usados ​​em conjunto com os dados de su/sw
 dia dias direto diretório diretório bsize bloqueou o tamanho do log em megabytes
 erro ao criar diretório erro ao criar de nome de diretório dirsize=%llu
 desabilitar cota de aplicações desabilitado pronto
 cabeçalho dúbio de bloco de árvore B de nós %d/%d
 Informação de cota de saída para utilitários de cópia de segurança habilitar cota de aplicações habilitado fim de alcance ?  entrada contém valor ilegal para atributo chamado SGI_CAP_FILE
 erro alocando espaço para o arquivo erro reservando espaço para o arquivo falha na leitura de superbloco existente: %s
 sair do programa esperado nível %d recebido %d em bt%s bloco %d/%d
 esperado nível %d obtido %d em bloco inobt %d/%d
 extensão externo argumentos adicionais
 falha ao ler extensões falha ao ler extensões: inode %llu Não foi possível criar thread de prefetch: %s
 falha ao obter attrs do inode: %s
 não foi possível iniciar variável de condição de prefetch
 não foi possível inicializar mutex de prefetch
 fd.path = "%s"
 fifo setado bit do sistema de arquivos mkfs-in-progress encontra paginas de mapeamento que estão residentes na memória sinalizações:  invertido estrangeiro livre informação de uso livre de bloco contagem de freeblk %d != flcount %d em ag %d
 reserva espaço livre associado com a parte de um arquivo espaço livre associado com a parte de um arquivo Congela o arquivo de sistema do arquivo atual de fullblocks=%llu
 geom.agblocks = %u
 geom.agcount = %u
 geom.bsize = %u
 geom.datablocks = %llu
 geom.rtblocks = %llu
 geom.rtextents = %llu
 adquirir todas as informações do estado da cota usar/setar contador de avisos da execução usar/setar cota de timeouts de execução da dicas sobre o uso de memória grupo cota de grupo disparidade de valores de hash ajuda para um ou todos os comandos buraco tamanho ilegal do bloco %d
 comprimento de dados ilegal %lld, não é um múltiplo de %d
 tamanho ilegal do bloco de diretório %d
 tamanho ilegal do inode %d
 comprimento de log ilegal %lld, não é um múltiplo de %d
 tamanho ilegal do setor de log %d
 tamanho ilegal do setor %d
 Tamanho de setor inválido %d; setor hw é %d
 geometria do sistema de arquivo inconsistente no componente de sistema de arquivo em tempo real informação de geometria do sistema de arquivos inconsistente valor de alimentação da estrutura de dados inconsistente infinito Insere erros no sistema de arquivos ino %10llu contagem %2d máscara %016llx
 bloco de nó %d/%d estado inválido, (estado %d)
 bloco de nó %d/%d tomado múltiplas vezes, (estado %d)
 bloco de árvore B de nós tomado (estado %d), agno %d, bno %d, suspeito %d
 pedaço de nó toma bloco usado, bloco inobt - agno %d, bno %d, inopb %d
 inode max pct inalterado, pulando
 espaço livre insuficiente para: %s: tamanho=%lld: ignorando
 interno Registro interno inválido isize=%llu
 tamanho do argumento muito grande -- %lld
 limitado lista os arquivos abertos atualmente e os mapeamentos de memória lista marcações extendidas da configuração do inode do arquivo atualmente aberto lista dos projetos e pontos de montagem conhecidos lista o identificador do projeto no arquivo aberto blocos de log alterados de %d para %d
 log alterado de %s para %s
 tamanho do log inalterado, pulando
 log de unidade da distribuição ajustado para 32KiB
 pouco espaço de tempo real: %s: ignorando arquivo
 erro no lseek64 malloc falhou: %s
 percentagem máxima indicada de estrutura de dados > 100% modifica os limites de cota deve rodar blockget primeiro
 criação de nome falhou em %s (%d), sistema de arquivo pode estar sem espaço disponível
 tamanho do nome ou valor do atributo muito grande,
 necessário pelo menos %lld grupos de alocação
 precisa de no máximo %lld grupos de alocação
 nenhuma entrada de dados nenhum nome de dispositivo informado na lista de argumento
 nenhum erro nenhum arquivo esta aberto, tente 'help open'
 nenhuma entrada de folha nenhum subvolume de registro ou registro interno
 nenhuma região mapeada, tente 'help mmap'
 não direto não existente argumento extsize não numérico -- %s
 modo não numérico -- %s
 não-sincronizar nenhum não há mais super blocos secundários com geometria acertada nulo desligado off len ok abrir o arquivo especificado pelo caminho open(%s) falhou: %s
 opção re-especificada
 ou cerca de %.1f megabytes
 sem memória em realloc: %s
 memória esgotada: %s
 gravação %d(%u%u) do bloco %u/%u bno árvore-B fora de ordem
 registro contador de árvore B fora de ordem %d (%u %u) bloco %u/%u
 pct desliga cota permanentemente para um caminho corrupção de prefetch
 primária projeto arquivo de projetos "%s" não existe
 espaço em disco esgotado!
 somente leitura leitura/gravação lê o número de bytes do deslocamento especificado lê dados de uma região no mapeamento de memória atual realloc falhou: %s
 mapa de bits em tempo real estrutura de dados livre marcada no bitmap em tempo real,  blocos em tempo real alterados de %lld para %lld
 tamanho em tempo real %lld muito grande, o máximo é %lld
 tamanho em tempo real %lld muito pequeno, o tamanho anterior é %lld
 resumo em tempo real estrutura de dados livre marcada no sumário de tempo real,  reconstruindo
 regular arquivo regular remove extensões de cota de um sistema de arquivos reparando a tabela
 informação de relatório de cota do sistema de arquivo relatar o uso de recurso do processo blocos reservados = %llu
 reserva espaço associado com a parte de um arquivo redefinir agf inválido para ag %d
 redefinir agi inválido para ag %d
 redefinir sb inválido para ag %d
 restaura o limite de cotas de um arquivo de backup maior parte da estrutura de dados do super usuário não encontrada
 estrutura de dados livre do super usário marcada,  s sb_fdblocks %lld, contados %lld
 sb_icount %lld, contados %lld
 sb_ifree %lld, contados %lld
 secundário definir definir caminho atual, ou mostrar a lista de caminhos definir como arquivo atual. Mostra estrutura de dados e blocos livres e usados mostrar limites e utilização tamanho %lld dos dados do subvolume é muito pequeno, o mínimo é %d blocos
 tamanho %s especificado para o subvolume de dados é muito grande, o máximo é %lld blocos
 tamanho do alcance ?  soquete stat.atime = %s stat.blocks = %lld
 stat.ctime = %s stat.ino = %lld
 stat.mtime = %s stat.size = %lld
 stat.type = %s
 statfs.f_bavail = %lld
 statfs.f_blocks = %lld
 statfs.f_bsize = %lld
 statfs.f_ffree = %lld
 statfs.f_files = %lld
 statfs.f_frsize = %lld
 estatísticas do arquivo aberto atualmente estatísticas sobre o sistema de arquivos do arquivo aberto atualmente strdup(%s) falhou
 string propriedade do sistema de arquivos sumarizada link simbólico sincronizar sync falhou: %s: %s
 nome de arquivo temporário muito longo: %s
 diretório temporário já existe: %s
 para muitos grupos de alocação para o tamanho = %lld
 não foi possível obter bstat em %s: %s
 não foi possível abrir: %s: %s
 bloco de inode incerto %d/%d já conhecido
 descongela o arquivo de sistema do arquivo atual opção desconhecida -%c %s
 usuário cota de usuário valor %d está fora do intervalo (0-%d)
 registro de volume volume rt alerta: comprimento de dados %lld não é múltiplo de %d, truncado para %lld
 alerta: o dispositivo não está apropriadamente alinhado %s
 alerta: o comprimento de log %lld não é um múltiplo de %d, truncado para %lld
 limparia a entrada
 limparia número do inode
 corrigiria
 corrigiria imap
 reconstruiria
 repararia a tabela
 redefiniria agf inválido para ag %d
 redefiniria agi inválido para ag %d
 redefiniria sb inválido para ag %d
 erro de escrita write(%s) falhou: %s
 xfs xfs_bmap_last_offset falhou -- erro - %d
 xfs_bunmapi falhou -- erro - %d
 xfs_trans_reserve retornou %d
 informado nome com tamanho zero no atributo do fork, 