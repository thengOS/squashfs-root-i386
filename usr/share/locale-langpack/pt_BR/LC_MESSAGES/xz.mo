��   �   0   �  �   �     �     �  �      L  �   M  7    �  W  -      F   N     �     �  7   �  �   �  �   �  �   *  �     �  �  H   j     �  E   3  �   y      >   &  9   e  �   �  �   5  �   �  �   <  �   
  �   �  l   \     �     �     �          5     U     o     �     �  z   �     8      R      l   .   ~   6   �      �      �      !     !  !   &!  !   H!  '   j!     �!     �!     �!  *   �!  /   "  %   L"     r"  /   �"  ,   �"     �"  4   �"     ,#     H#     f#     ~#     �#      �#      �#  h   �#  <   _$     �$  :   �$  $   �$     %  2   *%     ]%  $   z%  /   �%  I   �%     &  3   -&  =   a&  d   �&      '  O   %'  .   u'  /   �'     �'  A   �'  )   1(     [(     d(  8   }(     �(     �(  (   �(  I   )  !   Y)  '   {)  '   �)  9   �)     *      *  0   #*     T*  <   Y*  -   �*  @   �*  /   +  7   5+  D   m+  &   �+  '   �+     ,  %   	,     /,     G,  
   U,  
   `,  
   k,  
   v,  
   �,  	   �,  	   �,  	   �,  	   �,  	   �,  	   �,  	   �,  "   �,  *   �,      -  A   4-  Q   v-  *   �-  @   �-  !   4.     V.  �  Z.  �   )0  n  1  �  t4  :   h6  X   �6     �6     7  =   07  �   n7  u   -8    �8  �   �9  �  o:  ^   G<  �   �<  H   ==  �   �=  0  2>  6   c?  ;   �?  �   �?  �   �@  �   A  �   �A  �   aB  �   �B  V   �C     	D     D     (D     BD  -   bD     �D     �D     �D     �D  d   �D     _E     mE     �E  2   �E  ?   �E     F     ,F     >F     CF  9   ]F  9   �F  A   �F  !   G  ,   5G  "   bG  3   �G  ;   �G  0   �G     &H  7   >H  =   vH     �H  D   �H     I  %   5I     [I     yI     �I  (   �I  (   �I  n   	J  G   xJ     �J  B   �J  1   K     MK  A   kK  +   �K  4   �K  3   L  \   BL     �L  8   �L  G   �L  x   ;M  1   �M  l   �M  8   SN  =   �N  '   �N  D   �N  5   7O     mO  '   zO  A   �O  #   �O     P  +   P  S   IP  #   �P  /   �P  0   �P  X   "Q     {Q     �Q  I   �Q     �Q  E   �Q  6   6R  @   mR  .   �R  7   �R  J   S  %   `S  *   �S     �S  )   �S     �S     �S     T     T     /T     ?T     OT     _T     nT     }T     �T     �T     �T     �T  *   �T  2   �T     &U  \   ?U  V   �U  D   �U  F   8V  &   V     �V     ^   =       K   I       |   .           �          �   1              U   u      +          _              5   8       q       #   d       9   `   (       3   c          ]       -   [   �   y   a          J   "   ~   �   j            Z       %           s      0                          x          $   e       A          E      k           ,   }       R       O   B   �       )   ?       @   
       �       L   t   f   G             �       <              !   {   n   l   >   V   Q   2      Y   \       m   M   '           F          D   o      r   ;      p      X   w   v   7   z       	      N         �   i   4           b   *   H   W       6   g      �   �       T   &       h               P   /   :       �      C   S         �V     �V  �       0  �V  2          �����V  0               ���� W  ?          ����`W  2               ���� 
  --delta[=OPTS]      Delta filter; valid OPTS (valid values; default):
                        dist=NUM   distance between bytes being subtracted
                                   from each other (1-256; 1) 
  --lzma1[=OPTS]      LZMA1 or LZMA2; OPTS is a comma-separated list of zero or
  --lzma2[=OPTS]      more of the following options (valid values; default):
                        preset=PRE reset options to a preset (0-9[e])
                        dict=NUM   dictionary size (4KiB - 1536MiB; 8MiB)
                        lc=NUM     number of literal context bits (0-4; 3)
                        lp=NUM     number of literal position bits (0-4; 0)
                        pb=NUM     number of position bits (0-4; 2)
                        mode=MODE  compression mode (fast, normal; normal)
                        nice=NUM   nice length of a match (2-273; 64)
                        mf=NAME    match finder (hc3, hc4, bt2, bt3, bt4; bt4)
                        depth=NUM  maximum search depth; 0=automatic (default) 
  --x86[=OPTS]        x86 BCJ filter (32-bit and 64-bit)
  --powerpc[=OPTS]    PowerPC BCJ filter (big endian only)
  --ia64[=OPTS]       IA-64 (Itanium) BCJ filter
  --arm[=OPTS]        ARM BCJ filter (little endian only)
  --armthumb[=OPTS]   ARM-Thumb BCJ filter (little endian only)
  --sparc[=OPTS]      SPARC BCJ filter
                      Valid OPTS for all BCJ filters:
                        start=NUM  start offset for conversions (default=0) 
 Basic file format and compression options:
 
 Custom filter chain for compression (alternative for using presets): 
 Operation modifiers:
 
 Other options:
 
With no FILE, or when FILE is -, read standard input.
       --block-size=SIZE
                      when compressing to the .xz format, start a new block
                      after every SIZE bytes of input; 0=disabled (default)       --info-memory   display the total amount of RAM and the currently active
                      memory usage limits, and exit       --memlimit-compress=LIMIT
      --memlimit-decompress=LIMIT
  -M, --memlimit=LIMIT
                      set memory usage limit for compression, decompression,
                      or both; LIMIT is in bytes, % of RAM, or 0 for defaults       --no-adjust     if compression settings exceed the memory usage limit,
                      give an error instead of adjusting the settings downwards       --no-sparse     do not create sparse files when decompressing
  -S, --suffix=.SUF   use the suffix `.SUF' on compressed files
      --files[=FILE]  read filenames to process from FILE; if FILE is
                      omitted, filenames are read from the standard input;
                      filenames must be terminated with the newline character
      --files0[=FILE] like --files but use the null character as terminator       --robot         use machine-parsable messages (useful for scripts)       --single-stream decompress only the first stream, and silently
                      ignore possible remaining input data       CheckVal %*s Header  Flags        CompSize    MemUsage  Filters   -0 ... -9           compression preset; default is 6; take compressor *and*
                      decompressor memory usage into account before using 7-9!   -F, --format=FMT    file format to encode or decode; possible values are
                      `auto' (default), `xz', `lzma', and `raw'
  -C, --check=CHECK   integrity check type: `none' (use with caution),
                      `crc32', `crc64' (default), or `sha256'   -Q, --no-warn       make warnings not affect the exit status   -V, --version       display the version number and exit   -e, --extreme       try to improve compression ratio by using more CPU time;
                      does not affect decompressor memory requirements   -h, --help          display the short help (lists only the basic options)
  -H, --long-help     display this long help and exit   -h, --help          display this short help and exit
  -H, --long-help     display the long help (lists also the advanced options)   -k, --keep          keep (don't delete) input files
  -f, --force         force overwrite of output file and (de)compress links
  -c, --stdout        write to standard output and don't delete input files   -q, --quiet         suppress warnings; specify twice to suppress errors too
  -v, --verbose       be verbose; specify twice for even more verbose   -z, --compress      force compression
  -d, --decompress    force decompression
  -t, --test          test compressed file integrity
  -l, --list          list information about .xz files   Blocks:
    Stream     Block      CompOffset    UncompOffset       TotalSize      UncompSize  Ratio  Check   Blocks:             %s
   Check:              %s
   Compressed size:    %s
   Memory needed:      %s MiB
   Minimum XZ Utils version: %s
   Number of files:    %s
   Ratio:              %s
   Sizes in headers:   %s
   Stream padding:     %s
   Streams:
    Stream    Blocks      CompOffset    UncompOffset        CompSize      UncompSize  Ratio  Check      Padding   Streams:            %s
   Uncompressed size:  %s
  Operation mode:
 %s MiB of memory is required. The limit is %s. %s MiB of memory is required. The limiter is disabled. %s file
 %s files
 %s home page: <%s>
 %s:  %s: Cannot remove: %s %s: Cannot set the file group: %s %s: Cannot set the file owner: %s %s: Cannot set the file permissions: %s %s: Closing the file failed: %s %s: Error reading filenames: %s %s: Error seeking the file: %s %s: File already has `%s' suffix, skipping %s: File has setuid or setgid bit set, skipping %s: File has sticky bit set, skipping %s: File is empty %s: File seems to have been moved, not removing %s: Filename has an unknown suffix, skipping %s: Filter chain: %s
 %s: Input file has more than one hard link, skipping %s: Invalid filename suffix %s: Invalid multiplier suffix %s: Invalid option name %s: Invalid option value %s: Is a directory, skipping %s: Is a symbolic link, skipping %s: Not a regular file, skipping %s: Null character found when reading filenames; maybe you meant to use `--files0' instead of `--files'? %s: Options must be `name=value' pairs separated with commas %s: Read error: %s %s: Seeking failed when trying to create a sparse file: %s %s: Too small to be a valid .xz file %s: Unexpected end of file %s: Unexpected end of input when reading filenames %s: Unknown file format type %s: Unsupported integrity check type %s: Value is not a non-negative decimal integer %s: With --format=raw, --suffix=.SUF is required unless writing to stdout %s: Write error: %s --list does not support reading from standard input --list works only on .xz files (--format=xz or --format=auto) Adjusted LZMA%c dictionary size from %s MiB to %s MiB to not exceed the memory usage limit of %s MiB Cannot establish signal handlers Cannot read data from standard input when reading filenames from standard input Compressed data cannot be read from a terminal Compressed data cannot be written to a terminal Compressed data is corrupt Compression and decompression with --robot are not supported yet. Decompression will need %s MiB of memory. Disabled Empty filename, skipping Error restoring the O_APPEND flag to standard output: %s File format not recognized Internal error (bug) LZMA1 cannot be used with the .xz format Mandatory arguments to long options are mandatory for short options too.
 Maximum number of filters is four Memory usage limit for compression:     Memory usage limit for decompression:   Memory usage limit is too low for the given filter setup. Memory usage limit reached No No integrity check; not verifying file integrity None Only one file can be specified with `--files' or `--files0'. Report bugs to <%s> (in English or Finnish).
 Strms  Blocks   Compressed Uncompressed  Ratio  Check   Filename The .lzma format supports only the LZMA1 filter The environment variable %s contains too many arguments The exact options of the presets may vary between software versions. The sum of lc and lp must not exceed 4 Total amount of physical memory (RAM):  Totals: Try `%s --help' for more information. Unexpected end of input Unknown error Unknown-11 Unknown-12 Unknown-13 Unknown-14 Unknown-15 Unknown-2 Unknown-3 Unknown-5 Unknown-6 Unknown-7 Unknown-8 Unknown-9 Unsupported LZMA1/LZMA2 preset: %s Unsupported filter chain or filter options Unsupported options Unsupported type of integrity check; not verifying file integrity Usage: %s [OPTION]... [FILE]...
Compress or decompress FILEs in the .xz format.

 Using a preset in raw mode is discouraged. Valid suffixes are `KiB' (2^10), `MiB' (2^20), and `GiB' (2^30). Writing to standard output failed Yes Project-Id-Version: xz-utils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-04 03:00+0000
PO-Revision-Date: 2013-01-04 14:55+0000
Last-Translator: Rafael Neri <Unknown>
Language-Team: Brazilian Portuguese <pt_BR@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Launchpad-Export-Date: 2016-06-27 18:40+0000
X-Generator: Launchpad (build 18115)
 
  --delta[=OPTS]      Filtro de diferença; OPTS válido (valores válidos; padrão):
                        dist=NUM   diferença entre bytes sendo subtraídos
                                   de cada um (1-256; 1) 
  --lzma1[=OPÇÕES] LZMA1 ou LZMA2; OPÇÕES é uma lista, separada por vírgula, de zeros ou
  --lzma2[=OPÇÕES] mais das opções a seguir (valores válidos; padrão):
                      preset=PRE recompõe opções de uma pré-definição (0-9[e])
                      dict=NUM tamanho do dicionário (4KiB - 1536MiB; 8MiB)
                      lc=NUM número de bits no contexto literal (0-4; 3)
                      lp=NUM número de bits na posição literal (0-4; 0)
                      pb=NUM número de bits posicionados (0-4; 2)
                      mode=MODO modo de compressão (fast, normal; normal)
                      nice=NUM comprimento de uma boa combinação (2-273; 64);
                      mf=NOME buscador de combinação (hc3, hc4, bt2, bt3, bt4; bt4)
                      depth=NUM profundidade máxima de busca; 0=automático (padrão) 
  --x86[=OPÇÕES] filtro BCJ para x86 (32-bit e 64-bit)
  --powerpc[=OPÇÕES] filtro BCJ para PowerPC (somente maior ordem de bits)
  --ia64[=OPÇÕES] filtro BCJ para IA-64 (Itanium)
  --arm[=OPÇÕES] filtro BCJ para ARM (somente menor ordem de bits)
  --armthumb[=OPÇÕES] filtro BCJ para AMR-Thumb
  --sparc[=OPÇÕES] filtro BCJ para SPARC
                      OPÇÕES válidas para todos os filtros BCJ:
                        start=NUM inicial deslocamento para conversões (padrão=0) 
 Opções de formato de arquivo básico e compactação:
 
 Cadeia de filtros personalizados para compressão (alternativa para o uso de presets): 
 Modificadores de operação:
 
 Outras opções:
 
Sem ARQUIVO, ou quando ARQUIVO é -, lê a entrada padrão.
       --block-size=TAMANHO
                      ao compactar para o formato .xz, inicia um novo bloco
                      depois de cada TAMANHO bytes de entrada; 0=desabilitado (padrão)       --info-memory mostra o tamanho total de RAM e os limites atuais de
                      uso de memória, e sai       --memlimit-compress=LIMITE
      --memlimit-decompress=LIMITE
  -M, --memlimit=LIMITE
                      define o limite de memória utilizado para compressão, decompressão,
                      ou ambos; LIMITE é definido em bytes, % de RAM, ou 0 para padrão       --no-adjust se as configurações de compressão excederem o limite de uso de memória,
                      retorna um erro ao invés de ajudar as configurações para menor uso       --no-sparse não cria arquivos espaçados ao decomprimir
  -S, --suffix=.SUF usar o sufixo `.SUF' nos arquivos comprimidos
      --files[=ARQUIVO] lê nomes de arquivos à partir de ARQUIVO; se ARQUIVO é
                      omitido, os nomes dos arquivos serão lidos à partir da entrada padrão;
                      nomes dos arquivos devem terminar com o caractere de nova linha
      --files0[=ARQUIVO] como --files mas usa o caractere nulo como terminador       --robot         use mensagens que podem ser analizadas por computador (útil em scripts)       --single-stream descompacta somente a primeira transmissão e silenciosamente
                      ignora possíveis dados de entrada restantes       VerifValor %*s Cabeçalho Sinalizadores TamComp UsoMemoria Filtros   -0 ... -9 pré-definição de compressão; padrão é 6; leve em conta o
                      uso de memória pelo compactador *e* descompactador antes de utilizar 7-9!   -F, --format=FORMATO formato do arquivo a codificar ou decodificar; valores possíveis são:
                      `auto' (padrão), `xz', `lzma', e `raw'
  -C, --check=VERIFICAR tipo de integridade para verificar: `nome' (use com cuidado),
                      `crc32', `crc64' (padrão), ou `sha256'   -Q, --no-warn       avisos não afetam o exit status   -V, --version       mostra o número da versão e encerra   -e, --extreme tenta melhorar a taxa de compressão utilizando mais tempo de processamento;
                      não afeta os requisitos de memória do descompactador   -h, --help          mostra a ajuda curta (lista somente as opções básicas)
  -H, --long-help     mostra esta ajuda longa e encerra   -h, --help          mostra esta ajuda curta e encerra
  -H, --long-help     mostra a ajuda longa (lista as opções avançadas)   -k, --keep manter (não apagar) arquivos de entrada
  -f, --force força a sobrescrição do arquivo de saída e (des)comprime links
  -c --stdout escreve para a saída padrão e não apaga os arquivos de entrada   -q, --quiet suprimir avisos; especifique duas vezes para suprimir erros também
 -v, --verbose detalhado; especifique duas vezes para mais detalhes ainda   -z, --compress força compressão
  -d, --decompress força decompressão
  -t, --test testa integridade do arquivo comprimido
  -l, --list lista informações sobre arquivos .xz   Blocos:
    Fluxo Bloco DeslocComp DeslocDescomp TamTotal TamDescomp Taxa Verificado   Blocos: %s
   Verificar: %s
   Tamanho comprimido: %s
   Memória necessária: %s MiB
   Versão mínima dos utilitários do XZ: %s
   Número de arquivos: %s
   Proporção: %s
   Tamanho no cabeçário: %s
   Fluxo de preenchimento: %s
   Fluxos:
    Fluxo Blocos DeslocComp DeslocDescomp TamComp TamDescomp Taxa Verificado Preenchimento   Fluxos: %s
   Tamanho descompactado: %s
  Modo de operação:
 %s MiB de memória é necessário. O limite é %s. %s MB de memória é necessária. O limitador está desativado. %s arquivo
 %s arquivos
 %s página: <%s>
 %s:  %s: Não pode remover: %s %s: Não foi possível estabelecer o grupo de arquivo: %s %s: Não pode indentificar o proprietário do arquivo: %s %s: Não foi possível estabelecer as premissões do arquivos: %s %s: Falha ao fechar o arquivo: %s %s: Erro na leitura do nome dos arquivos: %s %s: Erro ao procurar o arquivo: %s %s: O arquivo já possui o sufixo `%s' , avançando %s: Arquivo possui o bit setuid ou setgid definido, pulando %s:  Arquivo possui o bit fixo definido, pulando %s: Arquivo está vazio %s: Arquivos parecem ter sido movidos, e não removidos %s: O nome d arquio possui um sufixo desconhecido, avançando %s: Sequência de filtros: %s
 %s: O arquivo de entrada tem mais de uma conexão quebrada, omitindo %s: Sufixo do nome inválido %s: Sufixo de multiplicador inválido %s: Opção de nome inválida %s: Opção de valor inválida %s: É um diretório, omitindo %s: É uma conexão simbólica, omitindo %s: Não é um arquivo regular, omitindo %s: Foi encontrado um caractere nulo ao ler o arquivo: talvez se você usasse `--files0' instead of `--files'? %s: As opções devem ser pares de 'name=value' separados com vírgulas %s: Erro de leitura: %s %s: Pesquisa falhou quando tentava criar um arquivo aleatório: %s %s: Muito pequeno para ser um arquivo .xz válido %s: Inesperado fim de arquivo %s: Final inesperado de entrada na leitura dos nomes dos arquivos %s: Tipo de formato de arquivo desconhecido %s: Tipo de verificação de integridade sem suporte %s: Valor não é um inteiro decimal não-negativo. %s: Com --format=raw, --suffix=.SUF é necessário a menos que esteja escrevendo para stdout %s: Erro de gravação: %s --list não suporta leitura à partir da entrada padrão --list funciona somente com arquivos .xz (--format=xz ou --format=auto) Ajustado LZMA%c tamanho do dicionário de %s MiB para %s MiB a fim de não exceder o limite de uso de memória de %s MiB Não possível estabelecer manipuladores de sinal Não foi possível ler dados na entrada padrão enquanto se estava lendo nomes de arquivo na entrada padrão Os dados compactados não podem ser lidos de um terminal Os dados compactados não podem ser escritos para um terminal Os dados compactados estão corrompidos Compactação e descompactação com --robot ainda não tem suporte. Descompressão irá necessitar de %s MiB de memória. Desabilitado Nome do arquivo está vazio, avançando Erro ao restaurar o marcador O_APPEND para sua saída padrão: %s Formato de arquivo não reconhecido Erro interno (falha) LZMA1 não pode ser usado com o formato .xz Argumentos obrigatórios para opções longas também o são para opções curtas.
 Número maximo de filtros é quatro Limite de uso de memória para compressão:     Limite de uso de memória para descompressão:   O limite do uso de memória é muito baixo para a configuração de filtro especificada. Limite de memória alcançado Não Sem checagem de integridade: não foi verificada a integridade do arquivo Nenhuma Somente um arquivo pode ser especificado com `--files' ou `--files0'. Reportar falhas para <%s> (em Inglês ou Finlandês).
 Fluxos Blocos Comprimido Não-comprimido Taxa Verificado Arquivo O formato .lzma suporta somente o filtro LZMA1 A variável de ambiente, %s , contém muitos argumentos As opções exatas de pré-ajuste podem variar entre versões de software. A soma de lc e lp não deve exceder 4 Montante total de memória física (RAM):  Totais: Tente '%s --help' para mais informações Fim da entrada inesperado Erro desconhecido Desconhecido-11 Desconhecido-12 Desconhecido-13 Desconhecido-14 Desconhecido-15 Desconhecido-2 Desconhecido-3 Desconhecido-5 Desconhecido-6 Desconhecido-7 Desconhecido-8 Desconhecido-9 Configuração LZMA1/LZMA2 sem suporte: %s Sequência ou opções de filtros não suportadas. Opções não suportadas Tipo de checagem de integridade não suportado: não foi verificada a integridade do arquivo Use: %s [OPÇÃO]... [ARQUIVO]...
Compactar ou descompactar ARQUIVOS no formato .xz.

 A utilização de um pré-ajuste no modo coluna é desaconselhável. Os sufixos válidos são `KiB' (2^10), `MiB' (2^20), and `GiB' (2^30). Falha na escrita para a saída padrão Sim PRIu32 PRIu64 The selected match finder requires at least nice=% Value of the option `%s' must be in the range [%, %] O localizador de igualdade selecionado requer pelo menos nice=% O valor da opção `%s' está fora do intervalo [%, %] 