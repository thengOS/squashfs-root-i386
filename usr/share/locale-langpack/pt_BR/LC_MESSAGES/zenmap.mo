��    q     �  �  ,      �     �     
       
   -  S   8  c   �  J   �     ;   M   D   H   �      �      �      �      	!  	   !     !  "   &!     I!     R!     [!     d!     t!  	   }!     �!     �!  	   �!     �!     �!     �!     �!  8   �!  �   "     �"  �   �"     "#     A#     ^#     n#  :   �#     �#     �#     �#     �#     �#  	   $  %   $     8$     D$     J$     Q$     ^$     k$     �$     �$     �$     �$     �$     �$  "   �$     �$     �$      %     %  3   %     G%     O%     T%     a%     q%     �%  �   �%     R&     Y&     a&     m&     �&     �&     �&     �&     �&  h   '  &   l'  �  �'  z  +  3   �1     �1     �1     �1     2  $   "2     G2     [2     q2     �2     �2     �2     �2     �2     �2     �2  	   �2     �2     3     3     3     *3     23     F3     M3     U3     b3     v3     |3  	   �3     �3     �3     �3     �3     �3     �3  	   �3  	   �3     �3     �3     �3     4  	   4     !4     .4  	   B4     L4     c4     f4     l4     {4     �4     �4     �4     �4     �4     �4  W   �4     ,5     35     85  
   F5     Q5  ~   X5     �5     �5     �5     �5     �5     6     6     !6     (6     96     >6     K6  
   Q6     \6     t6     �6     �6     �6     �6     �6     �6     �6     7  +   $7     P7     a7     n7     �7  $   �7     �7     �7     �7     �7     8  	   8     8  
   8  	   )8     38     A8     X8  #   d8     �8     �8     �8     �8     �8     �8     �8     �8  
   �8     9     9  
   !9     ,9     C9     K9     Z9     n9     {9     �9  	   �9      �9     �9     �9     �9     �9     �9     �9     �9     �9     :     $:     ,:     @:     I:     f:     s:  	   �:     �:     �:     �:     �:     �:  Y   �:  	   7;     A;     M;     ];  0   j;  
   �;     �;     �;     �;     �;     �;     �;     �;     <      <     9<     U<     ]<     o<     �<  	   �<     �<     �<  	   �<     �<     �<     �<     =  ?   =     S=     l=     �=     �=     �=  v   �=  �   %>     �>     �>     �>     �>  	   �>     �>     �>     �>     �>     ?  
   ?     ?     "?     )?     8?     @?     U?  O   Z?  �   �?  ;   =@  1   y@  G   �@  U   �@  D   IA  {   �A  &  
B  M   1C  ^   C  �   �C  /   �D  8   �D  J   �D  O   FE  L   �E     �E     �E  o   F     sF     �F  
   �F  ?   �F     �F     �F     	G  
   G     G     "G     *G     7G     RG     bG     rG     zG     �G  >   �G     �G     �G     �G     �G     �G     �G     H     H     %H     7H     <H     EH  
   QH     \H     kH  ]   rH  )   �H     �H     �H     I     I     .I     4I  
   @I     KI     eI     nI  
   |I     �I     �I     �I     �I     �I     �I     �I     �I     �I  	   �I     �I     J     !J  
   4J     ?J     GJ  �  \J      L     L     .L     @L  e   ML  v   �L  O   *M     zM  P   �M  I   �M  !   N     @N     IN     ZN     gN     vN  )   �N     �N     �N     �N     �N  	   �N  
   �N     �N  
   �N  
   O     O     O  	   *O     4O  B   EO  �   �O     P  �   'P  :   �P  1   �P  #   ,Q  )   PQ  R   zQ     �Q     �Q  	   �Q  (   �Q  -   R  
   IR  /   TR     �R     �R     �R     �R     �R     �R     �R     �R      S     S     "S     /S  ,   CS     pS  	   ~S     �S     �S  7   �S     �S     �S     �S      T     T     &T  �   2T     U     U     U  $   "U     GU      eU  '   �U     �U     �U  r   �U  +   PV  5  |V  �  �Z  9   ?b     yb     �b     �b  "   �b  ,   �b     c     -c     Lc     ic     ~c  '   �c     �c     �c     �c     �c     �c     �c     d     d     )d     7d     Ed     [d     cd     id     ~d     �d     �d     �d     �d     �d     �d     �d     �d     �d     e      e     4e     :e     Te     je     �e     �e     �e  	   �e  #   �e     �e     �e     �e     	f     #f     2f     8f     Gf     Mf     ff  c   {f     �f     �f     �f     g     g  �   $g  "   �g     �g     �g     �g     �g     �g     h     h     "h     3h     8h     Eh     Kh     Wh     oh     ~h     �h     �h     �h     �h     �h     i     /i  2   Ki     ~i     �i  $   �i  #   �i  /   �i     %j     ;j     Nj     \j  !   jj     �j     �j     �j     �j     �j  "   �j     �j  2    k     3k  #   Gk     kk     �k     �k     �k     �k     �k     �k     �k     �k      l     l     )l     0l     Al     Xl     gl  	   ol  
   yl  '   �l     �l     �l     �l     �l     �l     �l     �l     �l  +   m  
   @m     Km     im  /   rm     �m     �m     �m     �m  +   �m  #   n     8n  	   @n  f   Jn     �n     �n     �n     �n  2   o     >o     Qo  
   do     oo     �o     �o     �o  $   �o     �o  &   p  %   (p  
   Np     Yp  #   vp     �p     �p     �p  	   �p  
   �p  %   �p     q  $   'q     Lq  K   gq  $   �q     �q     �q     �q     r  �   "r  �   �r     ds     qs     xs     �s  
   �s     �s     �s     �s     �s     �s     �s     �s     �s     �s     t     t     !t  Y   't  �   �t  N   -u  6   |u  R   �u  n   v  Q   uv  �   �v  7  Iw  U   �x  b   �x  �   :y  -   �y  8   *z  I   cz  [   �z  [   	{     e{  	   �{  v   �{     |     |  
   ;|  P   F|  
   �|     �|     �|  
   �|     �|     �|     �|  !   �|     }     *}  
   :}     E}     ^}  J   l}     �}     �}     �}  
   �}     �}     �}     ~     ~     $~     :~  	   >~     H~     Y~     e~     y~  �   �~  ,        <     A     S     Z     u     {     �     �     �     �     �     �     �     �     �     �     �     -�     =�     N�     Q�     ^�     y�     ��     ��     ��     ŀ     �           f   �      �   p        d   3          X  b      h  C  �   �       �              �   "   l   �   �       _                  �   j           T        �   n  �   �   �   n   N      �   I      M  �   �       �               B   �      �   �   �           `       \   �   ^                 <  @  g      �   U  8   |   )  	     ,      7    �   �   G   t              6  i  V   a  0              
  k                 �   O         j      �   J   �                   L  O  �   �   g  Q   �   I      5           �   �   �   &          [      1  �   E               �   �           #   D       B  �   �   ?   �     *           k  �      F  %  �   ;      �   c  }       �     \  `  �   5  
   h             )         �   �   �   �   P  A       [       �      W   d  �   ;           �     �          X   /   z   (          4   �   L   �   R  _  !  �   '   �   W  �   +   4  �   �       >  �     �       �   �   r   ]          �   �   l  ?      :   Q  o       �   �          M   �          0      �   %      y   (   "      �     ]  �   b   9      e   �   m       f  $  �           o      v   Y   s       1          �   �   e      i   �   �           E  !       N   �   #  @       �       H  �   �   K  �   �   q      ~   �      Z  �      �   �     S         �   �       >   &   �                   �   3      p         6           F   ^   T   D  $   �   �         Y     �       .      H         x   �       /     �   �   c   8  '  :  q   �       �       R   -   �   �          �       <   �   �   �       �   +          �       �           S  �         C   *      �   �       V  u   	       K   �   =   A      m  �       �   {   9               Z              �   �   P           �   2   �   U       �   .     a   �   �   2      =      ,   7          �          G  �   -          w           �               �   J  �    # scanned ports: %d/%d hosts shown %s (%s seconds). %s credits %s is a free and open source utility for network exploration and security auditing. %s is a multi-platform graphical %s frontend and results viewer. It was originally derived from %s. %s is an %s GUI created as part of the Nmap/Google Summer of Code program. %s on %s <b>*</b> TCP sequence <i>index</i> equal to %d and <i>difficulty</i> is "%s". <b>Copy and email to <a href="mailto:dev@nmap.org">dev@nmap.org</a>:</b> <b>Fisheye</b> on ring <none> <special field> <undefined> <unknown> A Scan A required module was not found.

 ACK ping ACK scan About %s About %s and %s Accuracy Accuracy: Action Address: Addresses Alert All files (%s) Append Append Scan Append a saved scan to the list of scans in this window. Arguments

A list of arguments that affect the selected script. Enter a value by clicking in the value field beside the argument name. B Scan Begin with the specified profile selected. If combined with the -t (--target) option, automatically run the profile against the specified target. Can't find documentation files Can't open file to write.
%s Can't save file Can't save to database Can't store unsaved scans to the recent scans database:
%s Cancel Cancel Scan Canceled Cannot merge scan Cannot open selected file Cartesian Choose a directory to save scans into Choose file Class Class: Close Window Close anyway Close this scan window Closed ports: Command Command Info Command: Comments Compare Results Compare Scan Results using Diffies Contributors Controls Count Crash Report Create a new scan profile using the current command DB Line Date Debug level: Debugging level Deleting Profile Description Description

This box shows the categories a script belongs to. In addition, it gives a detailed description of the script which is present in script. A URL points to online NSEDoc documentation. Design Details Difficulty: Disable reverse DNS resolution Disallowed profile name Don't ping before scanning Edit selected scan profile Empty Nmap Command Enable IPv6 scanning. Enable OS detection (-O), version detection (-sV), script scanning (-sC), and traceroute (--traceroute). Enable all advanced/aggressive options Entering the text into the search performs a <b>keyword search</b> - the search string is matched against every aspect of the host.

To refine the search, you can use <b>operators</b> to search only specific fields within a host. Most operators have a short form, listed. 
<b>target: (t:)</b> - User-supplied target, or a rDNS result.
<b>os:</b> - All OS-related fields.
<b>open: (op:)</b> - Open ports discovered in a scan.
<b>closed: (cp:)</b> - Closed ports discovered in a scan.
<b>filtered: (fp:)</b> - Filtered ports discovered in scan.
<b>unfiltered: (ufp:)</b> - Unfiltered ports found in a scan (using, for example, an ACK scan).
<b>open|filtered: (ofp:)</b> - Ports in the "open|filtered" state.
<b>closed|filtered: (cfp:)</b> - Ports in the "closed|filtered" state.
<b>service: (s:)</b> - All service-related fields.
<b>inroute: (ir:)</b> - Matches a router in the scan's traceroute output.
 Entering the text into the search performs a <b>keyword search</b> - the search string is matched against the entire output of each scan.

To refine the search, you can use <b>operators</b> to search only within a specific part of a scan. Operators can be added to the search interactively if you click on the <b>Expressions</b> button, or you can enter them manually into the search field. Most operators have a short form, listed.

<b>profile: (pr:)</b> - Profile used.
<b>target: (t:)</b> - User-supplied target, or a rDNS result.
<b>option: (o:)</b> - Scan options.
<b>date: (d:)</b> - The date when scan was performed. Fuzzy matching is possible using the "~" suffix. Each "~" broadens the search by one day on "each side" of the date. In addition, it is possible to use the "date:-n" notation which means "n days ago".
<b>after: (a:)</b> - Matches scans made after the supplied date (<i>YYYY-MM-DD</i> or <i>-n</i>).
<b>before (b:)</b> - Matches scans made before the supplied date(<i>YYYY-MM-DD</i> or <i>-n</i>).
<b>os:</b> - All OS-related fields.
<b>scanned: (sp:)</b> - Matches a port if it was among those scanned.
<b>open: (op:)</b> - Open ports discovered in a scan.
<b>closed: (cp:)</b> - Closed ports discovered in a scan.
<b>filtered: (fp:)</b> - Filtered ports discovered in scan.
<b>unfiltered: (ufp:)</b> - Unfiltered ports found in a scan (using, for example, an ACK scan).
<b>open|filtered: (ofp:)</b> - Ports in the "open|filtered" state.
<b>closed|filtered: (cfp:)</b> - Ports in the "closed|filtered" state.
<b>service: (s:)</b> - All service-related fields.
<b>inroute: (ir:)</b> - Matches a router in the scan's traceroute output.
 Error creating the per-user configuration directory Error executing command Error loading file Error parsing file Error parsing ndiff output Error parsing the configuration file Error running ndiff Error saving snapshot Excluded hosts/networks Exclusion file Expressions  Extra options defined by user Extraports (%s) FIN scan Failed Family Fast scan Filter Hosts Filtered ports: Fingerprint Finished on: Fisheye Fragment IP packets Frames General General Info General information Green Help Highlight Highlight definitions Host Host Details Host Filter: Host Status Hostname Hostname: Hostnames Hosts Hosts Viewer Hosts down: Hosts scanned: Hosts up: Hosts viewer How to Report a Bug ICMP ping ICMP timestamp request IP IP ID IP ID Sequence IP protocol scan IPProto probes IPv4: IPv6 support IPv6: Idle Scan (Zombie) Import error Increase verbosity of the output. May be used more than once to get even more verbosity Index: Info Interpolation Last boot: Layout List of scripts

A list of all installed scripts. Activate or deactivate a script by clicking the box next to the script name. Lower ring gap MAC: Maemo Maimon scan Match Max Retries Max probe timeout Method NSE scripts (%s) Name Name - Type: Name: Navigation New _Profile or Command Nmap Output Nmap Output Properties Nmap Version: Nmap XML files (%s) Nmap XML format (.xml) Nmap text format (.nmap) No OS information. No filename extension No host selected. No module named dbapi2.pysqlite2 or sqlite3 No node selected No port scan No search method selected! No sequence information. No traceroute information available. Non-TCP scans:  Non-root user Not Available Not available Nothing to save Null scan OS OS Classes OS Family OS Generation Open a new scan window Open ports: Open the results of a previous scan Operating System Operating system detection Packet trace Parse error Ping options Polar Port Port-Protocol-State: Ports (%s) Ports / Hosts Ports to scan Ports used Print the current scan Profile Profile Editor Profile Information Profile name Profile: Protocol Protocol: Python 2.4 or later is required. Quit Quit the application RTT Reasons Red Remove Scan Report a bug Ring gap Run %s with the specified args. Running SCTP INIT port scan SYN ping Save All Scans to _Directory Save Changes Save Graphic Save Scan Save Topology Save all scans into a directory Save current scan results Sc_an Scan Scan IP protocols (TCP, ICMP, IGMP, etc.) to find which are supported by target machines. Scan Info Scan Output Scan is running Scan options Scan ports in order instead of randomizing them. Scan type: Scanned ports: Scans Script arguments Script scan Scripting options (NSE) Scripts to run Search Scan Results Search Scans Search for a scan result Search for host by criteria Search: Select File Type: Select Scan Result Select script files Sequences Service Services Services: Set IPv4 time to live (ttl) Set network interface Set source IP address Set source port Show all information sent and received by the scripting engine. Show the topology anyway Shows the application help SoC 2007 Source options Special fields Specify a scan result file in Nmap XML output format. Can be used more than once to specify several scan result files. Specify a target to be used along with other options. If specified alone, open with the target field filled with the specified target Started on: State State: Status Symmetric TCP * TCP SYN scan TCP Sequence TCP TS Sequence TCP Timestamp TCP scan:  TTL Target Target options Target: Targets (optional):  Text The file is not an Nmap XML output file. The parsing error that occurred was
%s The filename "%s" does not have an extension, and no specific file type was chosen.
Enter a known extension or select the file type from the list. The given scan has unsaved changes.
What do you want to do? The ndiff process terminated with status code %d. There are %u scans still running. Wait until they finish and then save. There are no scans with results to be saved. Run a scan with the "Scan" button first. There is a scan still running. Wait until it finishes and then save. There is no file type known for the filename extension "%s".
Enter a known extension or select the file type from the list. There was an error creating the directory %s or one of the files in it. The directory is created by copying the contents of %s. The specific error was

%s

%s needs to create this directory to store information such as the list of scan profiles. Check for access to the directory and try again. There was an error getting the list of scripts from Nmap. Try upgrading Nmap. There was an error loading the documentation file %s (%s). See the online documentation at %s. There was an error parsing the configuration file %s. The specific error was

%s

%s can continue without this file but any information in it will be ignored until it is repaired. There was an error running the ndiff program.

 There was an error while merging the new scan's XML:

%s There was an error while parsing the XML file generated from the scan:

%s This means that the nmap executable was not found in your system PATH, which is This scan has not been run yet. Start the scan with the "Scan" button first. Timing and performance Topology Topology is disabled because too many hosts can cause it
to run slowly. The limit is %d hosts and there are %d. Trace routes to targets Trace script execution Traceroute Traceroute on port <b>%s/%s</b> totalized <b>%d</b> known hops. Translation Trying to close Type UDP probes UDP scan Unknown Unknown Host Unknown filename extension Unknown version Unnamed profile Unsaved Unsaved changes Up time: Use DIR as the user configuration directory. Default: %default Used ports: Values Values: Vendor Vendor: Verbosity level Verbosity level: Version Version detection View Weighted Window scan Written by Xmas Tree scan Yellow You are trying to run %s with a non-root user!

Some %s options need root privileges to work. You must provide a name for this profile. Zoom [%d] service: %s _About _Edit Selected Profile _Help _New Window _Open Scan _Open Scan in This Window _Profile _Report a bug _Save Scan _Tools and spread factor closed port date details filtered port highlight color hostname ip open port plus the extra directories plus the extra directory port listing title text color unknown with interest factor Project-Id-Version: Zenmap
Report-Msgid-Bugs-To: dev@nmap.org
POT-Creation-Date: 2014-10-23 15:14+0000
PO-Revision-Date: 2014-01-31 12:45+0000
Last-Translator: Juliano Fischer Naves <julianofischer@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:14+0000
X-Generator: Launchpad (build 18115)
 # portas analisadas: %d/%d máquina mostrando %s (%s segundos). %s créditos %s é um utilitário livre e de código aberto para exploração de redes e auditorias de segurança. %s é uma interface gráfica multi-plataforma  para %s e visualizador de resultados. Foi originalmente derivado de %s. %s é uma GUI para %s criada como parte do programa Nmap/Google Summer of Code. %s em %s <b>*</b> sequência TCP<i>índice</i> igual a %d e a <i>dificuldade</i> é "%s". <b>Copie e envie para <a href="mailto:dev@nmap.org">dev@nmap.org</a>:</b> <b>Olho de peixe</b> quando tocar <nenhum> <campo especial> <indefinido> <desconhecido> Varredura A Um módulo exigido não foi encontrado.

 Ping ACK Varredura ACK Sobre %s Sobre %s e %s Precisão Precisão: Ação Endereço: Endereços Alerta Todos os arquivos (%s) Adicionar Anexar varredura Anexa uma varredura salva para a lista de varreduras nesta janela. Argumentos

Uma lista de argumentos que afetam o script selecionado. Entre com um valor clicando no campo de valores ao lado do nome do argumento. Varredura B Começa com o perfil especificado selecionado. Se combinado com a opção -t (--target), automaticamente executa o perfil contra o destino selecionado. Não foi possível localizar os arquivos de documentação Não é possível abrir o arquivo para gravar.
%s Não foi possível salvar o arquivo Não é possível salvar na base de dados Não pode salvar varreduras não salvas para o banco de dados de scans recente:
%s Cancelar Cancelar pesquisa Cancelado Não é possível substituir a varredura Não é possível abrir o arquivo selecionado Cartesiano Escolha um diretório para salvar as varreduras Escolha um arquivo Classe Classe: Fechar janela Fechar mesmo assim Fechar esta janela de varredura Portas Fechadas: Comando Informações do Comando Comando: Comentários Comparar Resultados Compara resultados de varreduras usando Diff Colaboradores Controles Contador Relatório de falhas Cria um novo perfil de varredura usando o comando atual Linha do BD Data Nível de Debug: Nível de depuração Apagando Perfil Descrição Descrição

Esta caixa mostra as categorias as quais um script pertence. Além disso, ela dá uma descrição detalhada do script que está presente em script. Uma URL aponta para documentação NSEDoc online. Design Detalhes Dificuldade: Desativar resolução de DNS reverso Nome de perfil não permitido Não usa ping antes da varredura Edita o perfil de varredura selecionado Comando do Nmap vazio Habilitar varredura IPv6 Ativar detecção de S.O.  (-O), detecção de versão (-sV), varredura script (-sC), e traceroute (--traceroute). Ativar todas opções avançadas/agressivas Digitando o texto na pesquisa é realizada uma <b>pesquisa por palavra-chave<b> - a string da pesquisa é comparada a cada aspecto da máquina.

Para refinar a pesquisa, você pode usar <b>operadores<b> para buscar apenas campos específicos dentro de uma máquina. A maioria dos operadores têm uma forma curta, conforme listado. 
<b>target: (t:)</b> - Destino fornecido pelo usuário, ou um resultado de rDNS.
<b>os:</b> - Todos os campos relacionados a SO.
<b>open: (op:)</b> - Portas abertas descobertas em uma varredura.
<b>closed: (cp:)</b> - Portas fechadas descobertas em uma varredura.
<b>filtered: (fp:)</b> - Portas filtradas descobertas em uma varredura.
<b>unfiltered: (ufp:)</b> - Portas não filtradas encontradas em uma varredura (usando, por exemplo, uma varredura ACK).
<b>open|filtered: (ofp:)</b> - Portas em estado "aberto|filtrado".
<b>closed|filtered: (cfp:)</b> - Portas em estado "fechado|filtrado".
<b>service: (s:)</b> - Todos os campos relacionados a serviço.
<b>inroute: (ir:)</b> - Corresponde a um roteador na saída de traceroute da varredura.
 Digitando o texto na pesquisa é realizada uma <b>pesquisa por palavra-chave<b> - a string da pesquisa é comparada a saída inteira de cada varredura.

Para refinar a pesquisa, você pode usar <b>operadores</b> para buscar apenas dentro de uma parte específica de uma varredura. Os operadores podem ser adicionados a busca interativamente se você presssionar o botão <b>Expressões</b>, ou você pode digitá-los manualmente no campo de pesquisa. A maioria dos operadores têm uma forma curta, conforme listado.

<b>profile: (pr:)</b> - Perfil usado.
<b>target: (t:)</b> - Destino fornecido pelo usuário, ou um resultado de rDNS.
<b>option: (o:)</b> - Opções de varredura.
<b>date: (d:)</b> - A data em que a varredura foi realizada. Uma correspondência aproximada é possível usando o sufixo "~" . Cada "~" amplia a busca por um dia "de cada lado" da data. Além disso, é possível usar a notação "date:-n" que significa "n dias atrás". 
<b>after: (a:)</b> - Corresponde a varreduras feitas depois da data especificada (<i>YYYY-MM-DD</i> ou <i>-n</i>).
<b>before (b:)</b> - Corresponde a varreduras feitas antes da data especificada (<i>YYYY-MM-DD</i> ou <i>-n</i>).
<b>os:</b> - Todos os campos relacionados a SO.
<b>scanned: (sp:)</b> - Corresponde a uma porta se ela estiver entre as da varredura.
<b>open: (op:)</b> - Portas abertas descobertas em uma varredura.
<b>closed: (cp:)</b> - Portas fechadas descobertas em uma varredura.
<b>filtered: (fp:)</b> - Portas filtradas descobertas em uma varredura.
<b>unfiltered: (ufp:)</b> - Portas não filtradas encontradas em uma varredura (usando, por exemplo, uma varredura ACK).
<b>open|filtered: (ofp:)</b> - Portas em estado "aberto|filtrado".
<b>closed|filtered: (cfp:)</b> - Portas em estado "fechado|filtrado".
<b>service: (s:)</b> - Todos os campos relacionados a serviço.
<b>inroute: (ir:)</b> - Corresponde a um roteador na saída de traceroute da varredura.
 Erro ao criar o diretório de configuração por usuário Erro ao executar comando Erro ao carregar arquivo Erro ao analisar arquivo Erro ao analisar a saída do ndiff Erro ao analisar o arquivo de configuração Erro ao executar o ndiff Erro ao salvar captura de tela Hospedeiros/redes excluídas Arquivo de exclusão Expressões  Opções extras definidas pelo usuário Portas extras (%s) Varredura FIN Falhou Família Varredura rápida Filtrar hosts Portas Filtradas: Impressão digital Terminado em: Olho de peixe Fragmentar pacotes IP Quadros Geral Informações Gerais Informação geral Verde Ajuda Realçar Destacar definições Host Detalhes da Máquina Filtro de máquina: Status da Máquina Nome da máquina Nome da máquina: Nomes das máquinas Hosts Visualizador de máquinas Máquinas desligadas: Máquinas analisadas: Máquinas ligadas: Visualizador de máquinas Como relatar um bug ping ICMP Requisição de marca de tempo ICMP IP IP ID Sequência IP ID Varredura do protocolo IP Sondas IPProto IPv4: Suporte a IPV6 IPv6: Varredura ociosa (Zumbi) Erro de importação Aumenta o detalhamento da saída. Pode ser usado mais de uma vez para obter ainda mais detalhamento Índice: Informação Interpolação Última inicialização: Layout Lista de scripts

Uma lista de todos scripts instalados. Ative ou desative um script clicando a caixa ao lado do nome do script. Diminuir intervalo entre os toques MAC: Maemo Varredura maimon Coincide Máximo de tentativas Tempo máximo de sonda Método Scripts NSE (%s) Nome Nome - Tipo: Nome: Navegação Novo p_erfil ou comando Saída do Nmap Propriedades de saída do Nmap Versão do Nmap: Arquivos XML do Nmap (%s) Formato XML Nmap (.xml) Formato de texto Nmap (.nmap) Sem informação de SO. Sem extensão de arquivo Nenhum maquina selecionado. Nenhum módulo chamado dbapi2.pysqlite2 ou sqlite3 Nenhum nó selecionado Sem varredura de porta Nenhum método de busca selecionado! Nenhuma informação de sequência. Nenhuma informação do traceroute disponível. Varreduras não-TCP:  Usuário não root Indisponível Indisponível Nenhuma modificação a ser salva Varredura nula SO Classes de S.O. Família do SO Geração do SO Abrir uma nova janela de varredura Portas abertas: Abre resultados de uma varredura previamente salva Sistema Operacional Detecção de sistemas operacionais Rastreamento de pacotes Erro de processamento Opções de ping Polar Porta Porta-Protocolo-Estado: Portas (%s) Portas / Máquinas Portas para varrer Portas usadas Imprimir a varredura atual Perfil Editor de Perfil Informação do Perfil Nome do perfil Perfil: Protocolo Protocolo: É necessário Python 2.4 ou posterior. Sair Sair do aplicativo RTT Razões Vermelho Remover varredura Relatar um bug Intervalo entre os toques Executou %s com os argumentos especificados Executando Varredura de portas SCTP INIT Ping SYN Salvar todas as verificações para _diretório Salvar alterações Salvar gráfico Salvar Varredura Salvar topologia Salvar todas as varreduras em um diretório Salva resultados da varredura atual _Varrer Verificar Varre protocolos IP (TCP, ICMP, IGMP, etc.) para encontrar quais são suportados pelas máquinas alvo. Informações da Varredura Saída da varredura Executando varredura Opções de varredura Varrer portas em ordem ao invés de aleatoriedade. Tipo de Varredura: Portas analisadas: Varreduras Argumentos do script Varredura script Opções de scripting (NSE) Scripts para executar Pesquisar os resultados da varredura Pesquisar varreduras Procurar por um resultado de varredura Pesquisar por máquina pelo critério Pesquisar: Selecione o tipo de arquivo: Selecione um resultado de varredura Selecione arquivos de script Sequências Serviço Serviços Serviços: Configura tempo de vida do IPv4 (ttl) Configurar a interface da rede. Configura o endereço de IP da fonte Configura a porta da fonte Mostra todas as informações enviadas e recebidas pelo mecanismo de script Mostre a topologia de qualquer forma Mostra a ajuda da aplicação SoC 2007 Opções de fonte Campos especiais Especificar um arquivo de resultado da verificação no formato de saída Nmap XML. Poderá ser usado mais de uma vez para especificar vários arquivos de verificação. Especifica um destino para ser usado junto com outras opções. Se especificado sozinho, abre com o campo destino preenchido com o destino especificado Iniciado em: Estado Estado: Status Simétrico TCP * Varredura TCP SYN Sequência TCP Sequência TCP TS Marca de tempo TCP Varredura TCP:  TTL Alvo Opções de alvo Alvo: Alvos (opcional):  Texto O arquivo não é um arquivo XML de saída do Nmap. O erro de análise que ocorreu foi
%s O nome do arquivo "% s" não tem uma extensão, e nenhum tipo de arquivo específico foi escolhido.
Digite uma extensão conhecida ou selecione o tipo de arquivo da lista. A varredura informada possui alterações não salvas.
O que você quer fazer? O processo ndiff finalizou com o código de status %d. Ainda há %u varreduras sendo executadas. Aguarde até que termine e então salve. Não há varreduras com resultados que podem ser salvos. Execute uma varredura com o botão "Varrer" primeiro. Ainda há uma varredura sendo executada. Aguarde até que termine e então salve. Não há tipo de arquivo conhecido com a extensão "%s".
Digite uma extensão conhecida ou selecione um tipo de arquivo da lista. Houve um erro ao criar o diretório %s ou um dos arquivos dentro dele. O diretório é criado copiando-se o conteúdo de %s. O erro específico foi

%s

%s precisa criar este diretório para armazenar informações como a lista de perfis de varredura. Verifique se possui acesso ao diretório e tente novamente. Houve um erro ao conseguir a lista de scripts do Nmap. Tente fazer o upgrade do Nmap. Houve um erro ao carregar o arquivo de documentação %s (%s). Veja a documentação online em %s. Houve um erro ao analisar o arquivo de configuração %s. O erro específico foi

%s

%s pode continuar sem este arquivo porém qualquer informação nele será ignorada até que seja reparado. Houve um erro ao executar o programa ndiff.

 Houve um erro ao substituir o novo XML da varredura:

%s Houve um erro ao analisar o arquivo XML gerado a partir da varredura:

%s Isto significa que o executável do nmap não foi encontrado no PATH de seu sistema, que é Esta varredura não foi executada ainda. Inicie a varredura com o botão "Varrer" primeiro. Temporização e desempenho Topologia A topologia está desativada porque hosts em excesso podem fazê-la
rodar devagar. O limite é de %d hosts and há %d. Traça a rota para alvos Rastreia a execução do script Traceroute Traceroute na porta <b>%s/%s</b> totalizou <b>%d</b> transferências conhecidas. Tradução Tentando fechar Tipo Sondas UDP Varredura UDP Desconhecido Máquina desconhecida Extensão de arquivo desconhecida Versão desconhecida Perfil sem nome Não salvo Alterações não salvas Tempo ligado: Use DIR como o diretório de configuração do usuário. Default: %default Portas usadas: Valores Valores: Fabricante Fabricante: Nível de detalhamento Nível de Verbosidade: Versão Detecção de versão Ver Ponderado Varredura janela Escrito por Varredura Xmas Tree Amarelo Você está tentando executar %s com um usuário diferente do root!

Algumas opções de %s precisam dos privilégios do root para funcionar. Você precisa atribuir um nome a este perfil Zoom [%d] serviço: %s _Sobre _Editar Perfil Selecionado Ajuda _Nova janela _Abrir varredura _Abrir varredura nesta janela _Perfil _Relate um bug _Salvar Varredura Ferramen_tas e fator de expansão porta fechada data detalhes porta filtrada cor do destaque nome da máquina ip porta aberta mais os diretórios extras mais o diretório extra listando título da porta cor do texto desconhecido com fator de interesse 