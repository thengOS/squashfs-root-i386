��    I      d  a   �      0     1     G     \     r     ~     �     �     �     �     �  n   �     ]     y     �  +   �  J   �  E   +  �   q  r   �  k   n	     �	     �	  5   �	  "   &
  <   I
     �
  #   �
     �
  )   �
     �
  <      3   =      q     �     �     �      �      �     �  
        "     @     P  �   g            ;   3  -   o  	   �  (   �  u   �  h   F     �  `   �     '     5  %   Q     w  
   �  D   �  	   �  
   �  6   �  6   ,  -   c     �     �  �   �  6   E  ;   |  &   �     �  �  �  ,   �  +     #   B     f     t     �  #   �  +   �     �  7   	  
  A  >   L  B   �  %   �  P   �  q   E  l   �  �   $  �     �   �      <     ]  a   b  S   �  �     D   �  d   �     M  D   e     �  �   �  p   3  =   �     �     �  $   �  @     Y   X  B   �     �  3        8  /   Q  1  �     �   4   �   [   �   @   H!     �!  (   �!  �   �!  �   f"  )   �"  �   %#     �#  ?   $  3   B$  Z   v$      �$  �   �$  
   t%     %  \   �%  L   �%  f   @&      �&  J   �&  �   '  Q   �'  V   (  O   e(  M   �(     *          %                          E      '   4   +       0          :   ;   1          &      I                         F      $   B   (   9      7   @   G       8   C   
      ?   2       3                   ,   )   5   H             .       =       #       A   /             "                  6              D   <   !   >         	   -               128-bit (most secure) 40-bit (less secure) <b>Authentication</b> <b>Echo</b> <b>General</b> <b>Misc</b> <b>Optional</b> <b>Security and Compression</b> Ad_vanced... All Available (Default) Allow MPPE to use stateful mode. Stateless mode is still attempted first.
config: mppe-stateful (when checked) Allow _BSD data compression Allow _Deflate data compression Allow st_ateful encryption Allow the following authentication methods: Allow/disable BSD-Compress compression.
config: nobsdcomp (when unchecked) Allow/disable Deflate compression.
config: nodeflate (when unchecked) Allow/disable Van Jacobson style TCP/IP header compression in both the transmit and the receive directions.
config: novj (when unchecked) Allow/disable authentication methods.
config: refuse-pap, refuse-chap, refuse-mschap, refuse-mschap-v2, refuse-eap Append the domain name <domain> to the local host name for authentication purposes.
config: domain <domain> Authenticate VPN CHAP Compatible with Microsoft and other PPTP VPN servers. Could not find pptp client binary. Could not find secrets (connection invalid, no vpn setting). Could not find the pppd binary. D-Bus name to use for this instance Default Don't quit when VPN connection terminates EAP Enable custom index for ppp<n> device name.
config: unit <n> Enable verbose debug logging (may expose passwords) Invalid or missing PPTP gateway. MSCHAP MSCHAPv2 Missing VPN gateway. Missing or invalid VPN password. Missing or invalid VPN username. Missing required option '%s'. NT Domain: No VPN configuration options. No VPN secrets! No cached credentials. Note: MPPE encryption is only available with MSCHAP authentication methods. To enable this checkbox, select one or more of the MSCHAP authentication methods: MSCHAP or MSCHAPv2. PAP PPTP Advanced Options PPTP server IP or name.
config: the first parameter of pptp Password passed to PPTP when prompted for it. Password: Point-to-Point Tunneling Protocol (PPTP) Require the use of MPPE, with 40/128-bit encryption or all.
config: require-mppe, require-mppe-128 or require-mppe-40 Send LCP echo-requests to find out whether peer is alive.
config: lcp-echo-failure and lcp-echo-interval Send PPP _echo packets Set the name used for authenticating the local system to the peer to <name>.
config: user <name> Show password Use TCP _header compression Use _Point-to-Point encryption (MPPE) Use custom _unit number: User name: You need to authenticate to access the Virtual Private Network '%s'. _Gateway: _Security: couldn't convert PPTP VPN gateway IP address '%s' (%d) couldn't look up PPTP VPN gateway IP address '%s' (%d) invalid boolean property '%s' (not yes or no) invalid gateway '%s' invalid integer property '%s' nm-pptp-service provides integrated PPTP VPN capability (compatible with Microsoft and other implementations) to NetworkManager. no usable addresses returned for PPTP VPN gateway '%s' no usable addresses returned for PPTP VPN gateway '%s' (%d) property '%s' invalid or not supported unhandled property '%s' type %s Project-Id-Version: ru
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=NetworkManager&keywords=I18N+L10N&component=VPN: pptp
POT-Creation-Date: 2016-04-14 07:51+0000
PO-Revision-Date: 2016-05-21 12:45+0000
Last-Translator: Eugene Marshal <Unknown>
Language-Team: Russian <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:03+0000
X-Generator: Launchpad (build 18115)
 128 бит (самое защищённое) 40 бит (менее защищённое) <b>Аутентификация</b> <b>Эхо</b> <b>Общие</b> <b>Прочее</b> <b>Дополнительные</b> <b>Шифрование и сжатие</b> _Дополнительно… Любое доступное (по умолчанию) Разрешить MPPE использовать контекстный (stateful) режим. Сначала производится попытка использовать простой (stateless) режим.
config: mppe-stateful (если не выбрано) Использовать для данных сжатие _BSD Использовать для данных сжатие _Deflate Включить _Stateful Encryption Разрешить следующие методы аутентификации: Разрешить/запретить сжатие BSD-Compress.
config: nobsdcomp (если не выбрано) Разрешить/запретить сжатие Deflate.
config: nodeflate (если не выбрано) Разрешить/запретить сжатие заголовков TCP/IP по методу Ван Якобсона в направлениях передачи и приёма.
config: novj (если не выбрано) Разрешить/запретить методы аутентификации.
config: refuse-pap, refuse-chap, refuse-mschap, refuse-mschap-v2, refuse-eap Добавить имя домена <domain> к имени локального узла для аутентификации.
config: domain <domain> Аутентификация VPN CHAP Совместим с серверами Microsoft и другими серверами PPTP VPN. Не удалось найти исполняемый файл клиента pptp. Не удалось найти секретные ключи (некорректное подключение, не настроен vpn). Не удалось найти исполняемый файл pppd. Наименование D-Bus используемой этой исполняемой копией По умолчанию Не завершать работу при отключении VPN EAP Включить пользовательский индекс для ppp<n> имени устройства.
config: номер <n> Включить подробный режим отладки (могут отображаться пароли) Шлюз PPTP отсутствует или неверный. MSCHAP MSCHAPv2 Отсутствует шлюз VPN. Пароль VPN отсутствует или неверный. Имя пользователя VPN отсутствует или недопустимо. Отсутствует требуемый параметр «%s». NT-домен: Нет параметров настройки VPN. Нет VPN-ключей! Нет учётных данных в кэше. Замечание: Шифрование MPPE доступно только с методами аутентификации MSCHAP. Чтобы включить данный пункт, выберите один или более методов аутентификации MSCHAP: MSCHAP или MSCHAPv2. PAP Дополнительные параметры PPTP IP-адрес или имя PPTP-сервера.
config: первый параметр pptp Пароль, передаваемый PPTP по запросу. Пароль: Point-to-Point Tunneling Protocol (PPTP) Требуется использовать MPPE с 40- или 128-битным шифрованием.
config: require-mppe, require-mppe-128 или require-mppe-40 Посылать эхо-запросы LCP, чтобы проверить состояние узла.
config: lcp-echo-failure и lcp-echo-interval Посылать _эхо-пакеты PPP Установить имя, используемое для аутентификации локальной системы к узлу <name>.
config: имя пользователя <name> Показать пароль Использовать сжатие _заголовков TCP Использовать шифрование _MPPE Использовать пользовательский номер устройства: Имя пользователя: Необходима аутентификация для доступа к виртуальной частной сети «%s». _Шлюз: _Шифрование: не удалось преобразовать IP-адрес шлюза PPTP VPN «%s» (%d) не удалось найти IP-адрес шлюза PPTP VPN «%s» (%d) «%s» неверно для логического свойства (не «да» или «нет») неверный шлюз «%s» «%s» неверно для целочисленного свойства nm-pptp-service позволяет использовать PPTP VPN (совместимые с Microsoft и другими реализациями) в NetworkManager. не получен приемлемый IP-адрес шлюза PPTP VPN «%s» не получен приемлемый IP-адрес шлюза PPTP VPN «%s» (%d) свойство «%s» неверно или не поддерживается свойство «%s» имеет необрабатываемый тип %s 