��    U     �"  �  lE      �\  !   �\  '   �\  -   �\     	]     &]     9]     G]  !   f]     �]  #   �]     �]     �]     �]  
   ^      ^  7   3^     k^     r^  	   z^     �^  <   �^     �^  &   �^      #_     D_  *   a_  (   �_     �_  .   �_  /   �_     )`     >`  -   Z`     �`     �`     �`      �`  '   �`     "a  0   Ba  "   sa     �a     �a     �a     �a     �a     b     7b  '   Sb      {b  !   �b  "   �b  2   �b  ,   c  4   Ac  "   vc  *   �c     �c  7   �c  !   d  '   0d  #   Xd      |d  0   �d  .   �d  4   �d     2e  5   Fe     |e  %   �e  *   �e     �e  #   �e  ,   #f     Pf      of  %   �f  -   �f     �f     �f  #   g  &   1g  	   Xg     bg     ig  	   yg     �g  �  �g  2  j  
   �l     �l     �l     �l     �l     �l     �l  &   �l     m     0m  /   Im     ym  -   �m  	   �m  %   �m  *   �m  5   
n  4   @n     un     |n     �n     �n     �n     �n     �n     �n     �n     �n     �n     �n     �n     �n     o     o     7o     Mo     Zo     co     yo     �o     �o     �o     �o     �o  &   �o     p     p     p  	   8p  
   Bp  $   Mp  %   rp  5   �p  (   �p  4   �p      ,q     Mq     \q  +   vq     �q     �q     �q  	   �q     �q     r     "r     <r     Tr     kr     yr     �r  	   �r     �r     �r     �r     �r     �r  	   �r     �r     �r     �r     �r     �r     s     ,s     Ks     cs     zs     �s  "   �s     �s     �s     �s     t  !   t     :t  	   Vt     `t     mt     ut     �t     �t     �t     �t     �t     �t     �t     �t  
   �t     �t     �t     �t     �t     �t     	u     u     &u     :u     Mu     du     ~u  	   �u  
   �u     �u  )   �u  +   �u  ,   v  )   9v  A   cv  "   �v     �v     �v     �v  /   w  +   @w  :   lw  V   �w     �w  ?   x  %   Px  +   vx     �x  !   �x  9   �x  4   y  #   @y  #   dy     �y     �y     �y     �y     �y     z  %   0z      Vz      wz     �z     �z     �z  '   �z  '    {  &   H{      o{      �{     �{     �{     �{  	   �{     �{     |     |     !|     (|  !   0|     R|     d|     w|     �|     �|     �|     �|     �|     �|     �|     �|     �|     �|     �|     �|  
   }     }     }     $}     >}      E}     f}  %   u}     �}     �}     �}     �}  '   �}  -   ~  #   >~  )   b~     �~  (   �~     �~     �~     �~     �~               $     6  &   >     e     w     �     �  #   �     �  0   �  *   /�  #   Z�     ~�  J   ��  �  ܀  k   ��  6  �  �   R�  I  �     g�  b   �  0   �  .   �  !   B�  '   d�  "   ��     ��  
   ��  '        �  0   	�  
   :�     E�      T�  #   u�  +   ��  >   ň  3   �  )   8�  .   b�  2   ��  '   ĉ  0   �  #   �  ,   A�  B   n�     ��  >   ̊  )   �     5�  :   O�     ��  6   ��  8   ۋ  5   �  0   J�     {�  '   ��     ��  7   ڌ  <   �     O�     h�  /   ��     ��  7   ̍  -   �  ;   2�     n�      ��  +   ��     ؎  8   �     '�  %   @�  7   f�  8   ��  0   ׏     �  5   %�  4   [�     ��  &   ��      ʐ  !   �  ;   �  .   I�     x�  8   ��  5   ˑ  4   �  +   6�  5   b�  6   ��  6   ϒ  )   �  @   0�  @   q�     ��     ͓     ߓ     �  #   �  5   )�     _�     ~�  0   ��  .   Δ  5   ��  A   3�  '   u�  &   ��  $   ĕ  %   �  '   �  (   7�  %   `�  ,   ��  )   ��     ݖ  #   ��  ;   �  %   [�     ��  -   ��     ͗     �     �  '   *�  0   R�  1   ��  >   ��  6   ��  >   +�  5   j�  0   ��  !   љ     �     �     2�     R�  6   o�  /   ��      ֚  3   ��  #   +�  *   O�  (   z�  "   ��  /   ƛ  *   ��  '   !�  +   I�  0   u�  )   ��  #   М  #   ��  #   �  $   <�  /   a�  !   ��     ��  ;   Н  5   �      B�  (   c�  )   ��  �   ��  %   :�  )   `�  +   ��  (   ��  9   ߟ  (   �  )   B�  )   l�     ��  -   ��     �     ��  )   �  8   ?�     x�     ��     ��  +   ˡ  J   ��     B�     K�  ?   b�     ��     ��     Ģ     �  $   �     
�  "   (�  &   K�  *   r�  <   ��  =   ڣ  #   �     <�  5   S�  ,   ��  )   ��  )   �  '   
�  +   2�  0   ^�  ,   ��  0   ��  ,   ��  !   �  -   <�  "   j�      ��      ��  +   Ϧ  +   ��     '�     5�     J�     R�     Z�     `�     }�  !   ��     ��     ҧ     �     �     �     ��  
   �     �       �     A�     F�     O�  
   R�  L   ]�     ��     ��     ��     ��     Ψ  #   �  -   �     3�  �   <�     ѩ  <   ة  A   �  
   W�  A   b�     ��     ��  1   Ӫ     �     �     %�     D�     Y�  (   e�  )   ��  0   ��  B   �     ,�     G�     Z�  %   ^�     ��     ��     ��     ��     ��     ��  
   ͬ      ج     ��  5   �     F�     ]�     j�     ��     ��     ��     ��     ��     í     ̭     Э     ݭ     �  4   ��  0   1�  9   b�  0   ��  /   ͮ  4   ��     2�     9�     B�     J�     Y�     s�     z�     ��     ��  !   į     �     �  #   �     �     3�     Q�  (   m�  !   ��  #   ��  =   ܰ     �  )   /�     Y�     ]�  
   b�  
   m�     x�  0   |�     ��     ��     ֱ     ��     �  "   '�     J�     i�  �   ��  )   ]�     ��     ��     ��  
   ճ  (   �     	�  &   �     ?�     H�     h�     ��     ��     ��     ��     ִ  	   ٴ     �     ��  !   �     $�     0�  F   G�  /   ��     ��     ͵     ѵ  $   յ  &   ��  !   !�  
   C�     N�     R�  2   c�     ��     ��  
   ��     Ŷ     ޶     ��      �     �     "�     *�     9�     @�     W�     q�     z�     ��  
   ��  N   ��  	   ��     �     �  *   &�     Q�  %   Y�     �  $   ��     ��     ¸     ϸ  Z   ߸     :�     ?�     D�     M�     T�     f�  	   l�     v�     {�     ��     ��     ��  +   ��  +   �     �     �      �     )�     0�     F�     M�     Y�     _�  (   d�     ��     ��     ��     ��     ��     ��  �   Һ     ^�  '   m�     ��  1   ��  4   л  	   �     �     �     (�     5�     J�  /   ^�     ��     ��  
   ��      ��  )   м     ��     ��     �  "   �     7�     K�     ]�     e�  5   l�  9   ��  K   ܽ  E   (�  >   n�  E   ��  @   �  E   4�  F   z�  G   ��  C   	�     M�  	   R�  	   \�     f�     u�     z�     ��     ��     ��     ��      ��     ��     ��  ,   ��  '   �  $   ;�  q   `�  `   ��  0   3�  1   d�  &   ��     ��  *   ��  ,   ��     ,�     H�     c�  )   ��  "   ��  5   ��     �  6   �     L�     i�  S   ��     ��     ��     ��  E   �  *   R�     }�     ��     ��     ��      ��     ��  %   ��     ��     �  +   3�     _�     g�     ��     ��     ��     ��     ��     ��  _   ��  �   X�  �   "�  M   �  2  `�  �   ��    H�  V  \�  �   ��  4  ��  Q   ��  X  :�  �  ��  �   u�  �   f�  �  �    ��  n  ��  b   $�  �   ��  �   '�  �   ��  >   ��  <   ��  }   1�  ~   ��  }   .�  �   ��     5�     >�     P�     V�     ]�     e�     j�     z�     ��     ��  '   ��     ��     ��     ��  )   ��  $   $�     I�     b�     q�  &   ��     ��  >   ��     ��  
   �  	   �     %�     +�     G�     ^�     w�     {�  !   ��  	   ��     ��     ��     ��     ��  !   �     *�     0�     5�     =�     M�     S�     \�     m�     r�     ��     ��     ��  	   ��     ��     ��     ��     ��     ��  J   ��  F   /�  J   v�  F   ��  <   �  i   E�  c   ��     �     �     )�     6�  
   J�     U�     h�     x�     ��     ��     ��     ��     ��     ��     ��     �  *   �  (   3�     \�     y�     ~�     ��     ��  �   ��  	   ��  
   ��  �   ��     ��     ��     ��     ��     ��     ��  !   ��  &   �  	   :�     D�     I�     P�     ]�  O   c�  	   ��     ��     ��  
   ��  %   ��     �  %   4�      Z�     {�  +   ��  
   ��     ��     ��     ��     �     �  C   �  �   [�  �   ��  /   ��     ��     ��     ��  (   ��     	�     �  	   !�     +�     0�  #   9�     ]�      x�     ��     ��     ��     ��  �   ��  &   ��  0   ��  4   ,�  !   a�  (   ��     ��     ��     ��     ��     �     .�     B�  &   ]�  7   ��     ��     ��  %   ��  #   
�     .�     H�  -   Z�     ��     ��     ��     ��      ��     ��  &   ��  $   "�     G�     X�     v�     ��  &   ��  +   ��     ��     ��  V  ��  B   S�  1   ��     ��     ��     ��  #   ��     �  #   3�     W�     i�     �     ��     ��     ��     ��     ��     ��  $   ��     ��     �  	   �  �   &�  �   ��  $   p�  !   ��     ��     ��     ��     ��     �  0   !�     R�  .   c�  �   ��  
  �     %�  3   C�     w�     ��     ��  �   ��  U   *�  5   ��     ��     ��     ��     ��  
   ��     ��  %   ��       .   ?  $   n  +   �      �     �     �  O       i )   � (   �    �    �    �    � 	    ?    :   X "   � 	   �    �    � o  � 1   B K   t [   � #       @    _ A   w C   �    � L    $   d K   � #   �    � #    n   ) 	   � 	   � 
   � +   � a   � D   E	 H   �	 7   �	 ,   
 E   8
 [   ~
 *   �
 N    d   T #   � <   � M    H   h )   � >   � 5    X   P =   � @   � 7   ( 7   ` 7   � =   � -    F   < F   � +   � i   � U   ` L   � D    ^   H s   � m    ;   � E   � 0    �   < W   � |     V   � [   � 3   P g   � q   �    ^ q   } %   � O    `   e :   � @    K   B L   � A   � 0    K   N    � 8   � /   � 2   "    U    o %   x    �    � q  � 5  )     _#    v# 
   x#    �# 
   �#    �#    �# ;   �# *   �# "   $ w   ;$    �$ P   �$    % 4   % H   N% N   �% Z   �%    A&    P& 
   j&    u&    {&    �&    �&     �&    �&    �&    	'    '    '    ,' 1   K' 0   }' )   �'    �'    �'    ( 8   ( #   U( $   y(    �( !   �( F   �( j   )    y)    �) =   �)    �)    �) I   * 1   O* W   �* 7   �* V   + 7   h+    �+ .   �+ j   �+    W,     t, :   �,    �, .   �, 3   - (   N- *   w-    �-    �-    �-    �- 	   �- 
   �-    �-    .    .     . 	   &. +   0.    \.    q. 8   v.    �.    �.     �.    
/    "/ 3   9/ ?   m/ H   �/    �/    0 *   0 -   @0 4   n0 /   �0    �0    �0    �0    
1    $1    )1    C1 
   \1    g1    l1    q1    �1    �1    �1 
   �1    �1    �1 +   �1 *   (2 
   S2 )   ^2 )   �2 ?   �2 5   �2 <   (3    e3    z3    �3 5   �3 >   �3 3   #4 5   W4 a   �4 6   �4    &5 *   @5 6   k5 f   �5 f   	6 m   p6 �   �6    f7 r   �7 :   �7 @   28    s8 2   �8 r   �8 u   .9 B   �9 V   �9 C   >: ?   �: G   �: .   
; ;   9; @   u; G   �; A   �; A   @< A   �< E   �< E   
= O   P= O   �= N   �= ;   ?> ;   {>    �>     �> /   �> 	   ? 6   !?    X?    p?    �?    �? /   �?     �?    @ 8   "@    [@    a@    g@    k@ 
   ~@    �@    �@    �@    �@     �@    �@    A    A    +A    :A &   OA    vA 2   �A *   �A Z   �A /   DB 4   tB    �B #   �B !   �B ;   C 1   >C E   pC >   �C S   �C "   ID %   lD    �D    �D *   �D %   �D %   �D    E >   *E #   iE J   �E !   �E    �E -   F D   @F c   �F z   �F R   dG (   �G f   �G y  GH �   �J �  �K   LM �  QN .   -P �   \P f   �P d   bQ 8   �Q O    R ?   PR    �R    �R V   �R 9   S `   BS    �S    �S <   �S 8   T J   AT �   �T m   U =   {U L   �U `   V 9   gV f   �V #   W ]   ,W R   �W A   �W d   X ;   �X @   �X m   Y !   oY c   �Y m   �Y h   cZ r   �Z 3   ?[ >   s[ @   �[ c   �[ s   W\     �\ #   �\ F   ] $   W] j   |] o   �] n   W^ $   �^ 0   �^ J   _    g_ ^   �_ ?   �_ =   #` �   a` �   �` Z   wa C   �a [   b g   rb    �b B   �b '   7c (   _c Z   �c M   �c @   1d b   rd Q   �d ]   'e I   �e _   �e p   /f \   �f H   �f i   Fg i   �g A   h    \h    uh    �h R   �h h   �h X   gi E   �i [   j L   bj S   �j r   k T   vk B   �k ?   l Q   Nl B   �l C   �l @   'm K   hm G   �m >   �m M   ;n �   �n 1   !o =   So L   �o ;   �o =   p 9   Xp :   �p c   �p v   1q ^   �q F   r ^   Nr E   �r N   �r 2   Bs E   us C   �s B   �s 9   Bt X   |t `   �t U   6u d   �u H   �u `   :v Z   �v =   �v Q   4w O   �w S   �w Q   *x w   |x ;   �x H   0y G   yy ]   �y :   z C   Zz C   �z D   �z `   '{ ]   �{ O   �{ P   6| D   �| �   �| =   �} M   �} O   >~ O   �~ n   �~ /   M 5   } G   � 2   � T   .� !   �� #   �� ^   ɀ h   (�    �� :   �� :   � P   %� �   v�    � *   � }   <� #   ��    ރ 3   ��    0� J   ?� B   �� C   ̈́ G   � O   Y� �   �� �   A� @   Æ /   � \   4� O   �� D   � N   &� e   u� i   ۈ d   E� ]   �� d   � [   m� D   Ɋ [   � 9   j� =   �� =   � Z    � _   {� +   ی    �    � 
   %�    0� @   =� @   ~� 7   �� 0   ��    (�    A�    H�    Q� ,   q� !   ��    �� ?   ю    �    �    .� 
   1� �   <�    ��    �    �    �    ;� I   Y� ^   ��    � �   �    � ~   � x   �� 
   � [   �    k� $   �� ]   �� &   �    5� H   R� !   ��    �� C   Ҕ D   � h   [� �   ĕ 9   O�    ��    �� *   ��    ۖ    ��    � 4   �� )   4� 8   ^�    �� 9   �� *   � �   � B   Ƙ    	� 4   �    M�    b�     n� 
   ��    ��    ��    ��    ��    �� K   ƙ ^   � _   q� _   њ S   1� V   �� h   ܛ    E�    T� !   c�    �� :   ��    Ϝ 8   ޜ    � *   *� (   U� 
   ~�    �� L   �� ?   � 6   $� !   [� ^   }� J   ܞ Q   '� �   y� %   �� }   "�    ��    ��    ��     ��    נ �   ۠    f� 2   v� J   ��    �� .   � 2   6� %   i� .   �� �  �� 7   m� (   �� !   Τ 8   �    )� V   2�    �� l   �� !   � 8   8� 7   q�    �� "   �� #   ަ -   �    0� 	   3�    =�    X� �   i�    �� ?   � t   \� I   Ѩ    �    4�    8� L   <� F   �� S   Щ    $�    9� -   =� i   k�    ժ    �    	� #   � 7   :�    r�    x�     ��    ��    ë    ԫ :   �� 6   (�    _�    l� +   ��    �� �   ��    I�    e�    �� n   ��    � :   �    O� A   b� ,   ��    Ѯ    � �   � 
   ɯ    ԯ     �    � +   �    A�    P�    _�    d�    {�    �� 8   �� ;   ް ;   �    V�    m�    ��    �� *   ��    ٱ    �    �    � R   �    [�    o�    x�    �    ��     �� �   ��    �� Z   ճ    0� F   =� _   ��    �    ��    �     � ,   <�     i� I   ��    Ե 
   �    �� 9   � W   K�    ��    ��    Ҷ 6   ߶ 6   � %   M�    s�    �� g   �� y   �� �   s� �   � �   Ĺ �   T� |   ں �   W� �   � �   y� �   �    �� "   Ͻ    � '   �    4� #   9�    ]�    d�    k� #   r� c   �� 8   �� ;   3� u   o� 3   � 7   � �   Q� �   �� G   �� R   �� Y   0� '   �� S   �� Q   � U   X� I   �� D   �� ^   =� ,   �� [   ��    %� n   =� 3   �� #   �� a   � #   f�    �� !   �� �   �� ^   L�    ��    ��    ��    �� H   �� ;   !� J   ]� ?   �� <   �� U   %�    {� ?   �� .   �� #   �� E   #� I   i� ,   ��    �� �   �� S  �� �  �� p   �� �  ?�    5� �  6� �  �� /  �� �  � {   �� g  ?� �  �� o  �� �   � �   � i  �� �  �� �   �� �   s� �   \� %  D� `   j� ]   �� �   )� �   �� �   z� �   4�    � #   +�    O�    `�    {�    ��    ��    �� "   ��    �� W   ��    I�    M�    d� K   � L   �� 1   �    J�    c� 9   � #   �� o   ��    M�    b�    o�    �� 7   �� ,   �� /   ��    �    � 4   =�    r�    ��    ��    ��    �� .   ��    ��    ��    ��    ��    ��    �    �    #� :   (�    c�    g�    ~�    ��    ��    ��    ��    ��    �� I   �� �   #� �   �� �   V� d   �� �   Q� �        �     �     �     �     �      	 +   *    V    ]    c    u M   � '   � *   � >   )    h K   l R   � Y       e 5   n &   �    � �  �    �    � �  �    y ,   �    �    �    �    � <       B 	   � #   �    � 5   �    /	 �   8	    �	 %   �	 "   
    ;
 C   R
 )   �
 G   �
 H    -   Q W       �    �    
        !    8 �   P   � )  � U       n        � O   � '   �        .    A    F ?   O >   � @   �        + S   G    � �  � R   4 J   � J   � P    P   n -   � V   �    D %   \ %   � #   � 4   � C    ]   E $   � 0   � V   � K   P 7   � #   � J   � '   C    k    �    � I   �    � I   � A   ' (   i `   � !   � /    S   E m   �         *  1 c   \ \   � &       D    K M   c F   � K   �    D 3   ` (   �    � <   �         	   5    ? f   G '   �    �    � �   � ~  �  2   w" :   �" !   �" 6   #    ># !   \# *   ~# 7   �# *   �# \   $ �   i$ }  
% :   �& S   �& =   '    U'    l' �   t' �   Z( �   )    �)    �)    �) #   �) E    *    F* >   b* 7   �* Q   �* C   ++ O   o+ ?   �+ #   �+ :   #, }   ^, 0   �, L   - U   Z-    �-    �- 1   �- %   .    2. m   L. h   �. m   #/    �/    �/    �/    8      �          �        �      f   l  _       �        k   3  B  �          �       �  >  ,  �      �  \     1   4  �  o  O   |        X   �   �  �  e   $  \   �  Q  s      >    0  �   �  �  �  �  �  L  �      `  <   d   7   )     �  �  �  �  '  �   �  &        �   �            �      �      �  U  n      �          <      /   �    �  (  g  7  D  H  Q   �   z   �   �      �   �            �   �  O         ,  T  �  �  �        �          v  _  e  �   c  q            �  V   4                        �  �  �      �  D   �  /      �          |  m  �   
  �   [  �   �   �           �  *  ]  �   1  $  "      u   �  >   e  �  Q    :  �  �         �  �      �   �   6    �       u  (       �     ^           �         �   }   �  �          R  �  �        P  �  5  �  �  �   �        )   �  �      �        �    ,   �  �  �  �  G      U      m      L  #   #  �  x   �  \  �      �  �  �  �  h   �  F      F  I      q          �      �     ?  �  �  f  �   �         �  �  �   �   	     x  ?  Z   �              a      �  L           /  ^          @  p      �       A  y  �      o  :      �   �      �  H             �      �        �  �   =      �  g   �  �  �          �   �   �          h  �   �  8   �  �  9  t  h      &      �  �  l  t  �  �                C    |  E  �   {              R      �  $   l    f      /                L  �      �  �  m        0      E  �  �  �                   �    6       0       +  �  �   �  .      �  *  �      �  >      1  �              �       ;  5  G  d      �  �  M  �   M  �  �  �     �        t   �   v  N         3  �      �  �   �  M   "       �  �  �  R  �  �   �  H  -   b   e  �      �  �  �   �    ,  �      \  ?  S                  1    z  W  6  W  �   9  p  1      �      �  )  �  �    �     �                x  4   �          
              �  b  '          �      Y  u      E      -      2       �    �      &  �  o   h      %  M  �  B  �   i     C   �   ~  E           �  K      `   ]          �   �   �   �   7    !  H  �  �  <  �      ~       �      �  �   �   9    D      =          �  N      �    V              �  �   �  �  �  �  �      �  �  �          �        G      �  (  b  w        �  5  �      �      I   �  �  T      S         z  k            �      �  L                  �    V  }      �  3          !  �       N  j  �   _  �      �  �   �  �  �   �  J  �   �  q   �  �    �      -      �  �  �   F  (    �  �  9  �      +       |      j  �  �    4             a    ]      �  H       �    =    �   c   �   P  O  �  �     �  �        �      U   V  �  %  �  �       m   z  #  �  $    =  �  5   J  W               �      �   `    �  J            '      �      �  �   �  n       *  j  D  w  [  o  �  �  �   J      �  �    �   �  �      �  �  �   b              �  P      -  )  l           ~  �   �      ;  �   �  �  �  �      S                X  �  �   	  �  �  s      K   F  �      �  i      �       	  �  A  �   �  �  �      .  c      �   2        N  
          �      �  �  �  �     �  i  �  T   �  �  J                  �   �  �  �  0  �            �      %  �  K           �  �   i  S  �         �      2  Y   �      R  /  B  C      y  �  �      �  #  O  �  <  �     �      Z  O      !      �  �          =           �      �  �  +              G   �  &     �   6      �    �        �                  �      �  D  �        �  ^      �  �                  �               �  �  �  �   �  �  .  �  �   �      �       �   �    �             s  �   �   x  �          �               �                    �   �  �          �  ;  0  
  3   w   �  �  �        �             C      	          �           M      Z  6  �  U  �  �        8  g  k  :       �  �          p       �      )  �  �  �   3      �  {  �  �   �  �  �  �         �     �  F     �  v   �    �      �      �          �   @      �     �  Y  Y  }  �       �  �      �    >      �       �    �     S  I  B          �         �   �          Z  �       !   �  E  j   �       �  {      �  
   �      �           r   �  *   �  n  �   I  [   �  �      Q  4  �  �  ~  �    �           *  8      �    �       �  �      T  �  �       -  `  @  _  w  �  �  �      s     #        ^      X  �  �   R   t  �  �  N  �       (  5              g  $  G  A  �                     �    ;  K    �  �  W  �   "  �  �  �  ;   "        Q  �       �  �          �   �  �  X          B   �    �   @      %  �  r     :      �  �   �  d      T          �  �  �  �   �         �   �  P       f     �        ,    �  ]   �   �  �          �       k      �   A   �        n  �     .  �      2  p  �   �  �         �   u  �  �   �   &       '   �      A  �  �      "          �   r  �  �       8  <  �  :  �            K  7  a       �    U      �  ?  9   �                �  a      �     {     �       ?   [  2      I  �  @   '  �      C  �      �  +  y        �  q          %   r  P  7  �  c  .   �  !  y   	    �   �         �  d  �              v            �                  +   �  }          �              �    �     Address successfully added: %s
   Warning: address already present: %s
   Warning: ignoring garbage at the end: '%s'
 # Created by NetworkManager
 # Merged from %s

 %d (disabled) %d (enabled, prefer public IP) %d (enabled, prefer temporary IP) %d (unknown) %d. IPv4 address has invalid prefix %d. IPv4 address is invalid %d. route has invalid prefix %d. route is invalid %s Network %s is already running (pid %ld)
 %s.  Please use --help to see a list of valid options.
 %u MHz %u Mb/s %u Mbit/s '%d' is not a valid channel '%d' is not a valid value for the property (should be <= %d) '%d' is not valid; use <%d-%d> '%d' is out of valid range <128-16384> '%d' value is out of range <0-3> '%ld' is not a valid channel '%s' and '%s' cannot have different values '%s' can only be used with '%s=%s' (WEP) '%s' cannot be empty '%s' connections require '%s' in this property '%s' contains invalid char(s) (use [A-Za-z._-]) '%s' has to be alone '%s' is ambiguous (%s x %s) '%s' is neither an UUID nor an interface name '%s' is not a DCB app priority '%s' is not a number '%s' is not a valid DCB flag '%s' is not a valid Ethernet MAC '%s' is not a valid Ethernet port value '%s' is not a valid IBoIP P_Key '%s' is not a valid IPv4 address for '%s' option '%s' is not a valid InfiniBand MAC '%s' is not a valid MAC '%s' is not a valid MAC address '%s' is not a valid PSK '%s' is not a valid UUID '%s' is not a valid Wi-Fi mode '%s' is not a valid band '%s' is not a valid channel '%s' is not a valid channel; use <1-13> '%s' is not a valid duplex value '%s' is not a valid hex character '%s' is not a valid interface name '%s' is not a valid interface name for '%s' option '%s' is not a valid number (or out of range) '%s' is not a valid team configuration or file name. '%s' is not a valid value for '%s' '%s' is not a valid value for the property '%s' is not valid '%s' is not valid master; use ifname or connection UUID '%s' is not valid; use 0, 1, or 2 '%s' is not valid; use <option>=<value> '%s' is not valid; use [%s] or [%s] '%s' is not valid; use [e, o, n] '%s' length is invalid (should be 5 or 6 digits) '%s' not a number between 0 and %u (inclusive) '%s' not a number between 0 and %u (inclusive) or %u '%s' not among [%s] '%s' not among [0 (unknown), 1 (key), 2 (passphrase)] '%s' option is empty '%s' option is only valid for '%s=%s' '%s' option requires '%s' option to be set '%s' option should be string '%s' requires setting '%s' property '%s' security requires '%s' setting presence '%s' security requires '%s=%s' '%s' value doesn't match '%s=%s' '%s=%s' is incompatible with '%s > 0' '%s=%s' is not a valid configuration for '%s' (%s): Invalid IID %s
 (No custom routes) (No support for dynamic-wep yet...) (No support for wpa-enterprise yet...) (default) (none) (unknown error) (unknown) * ---[ Main menu ]---
goto     [<setting> | <prop>]        :: go to a setting or property
remove   <setting>[.<prop>] | <prop> :: remove setting or reset property value
set      [<setting>.<prop> <value>]  :: set property value
describe [<setting>.<prop>]          :: describe property
print    [all | <setting>[.<prop>]]  :: print the connection
verify   [all | fix]                 :: verify the connection
save     [persistent|temporary]      :: save the connection
activate [<ifname>] [/<ap>|<nsp>]    :: activate the connection
back                                 :: go one level up (back)
help/?   [<command>]                 :: print this help
nmcli    <conf-option> <value>       :: nmcli configuration
quit                                 :: exit nmcli
 ---[ Property menu ]---
set      [<value>]               :: set new value
add      [<value>]               :: add new option to the property
change                           :: change current value
remove   [<index> | <option>]    :: delete the value
describe                         :: describe property
print    [setting | connection]  :: print property (setting/connection) value(s)
back                             :: go to upper level
help/?   [<command>]             :: print this help or command description
quit                             :: exit nmcli
 -1 (unset) 0 0 (NONE) 0 (disabled) 0 (none) 1024 802.1X 802.1X supplicant configuration failed 802.1X supplicant disconnected 802.1X supplicant failed 802.1X supplicant took too long to authenticate 802.3ad ===| nmcli interactive connection editor |=== A (5 GHz) A dependency of the connection failed A password is required to connect to '%s'. A problem with the RFC 2684 Ethernet over ADSL bridge A secondary connection of the base connection failed ACTIVE ACTIVE-PATH ADDRESS ADHOC ADSL ADSL connection ADSL encapsulation ADSL encapsulation %s ADSL protocol AP APN:  ARP ARP targets AUTOCONNECT AUTOCONNECT-PRIORITY AVAILABLE-CONNECTION-PATHS AVAILABLE-CONNECTIONS Access Point Activate Activate a connection Activate connection details Activation failed Active Backup Ad-Hoc Ad-Hoc Network Adaptive Load Balancing (alb) Adaptive Transmit Load Balancing (tlb) Add Add... Adding a new '%s' connection Addresses Aging time Allow control of network connections Allowed values for '%s' property: %s
 An http(s) address for checking internet connectivity An interface name and UUID are required
 Are you sure you want to delete the connection '%s'? Ask for this password every time Authentication Authentication error: %s
 Authentication required by wireless network AutoIP service error AutoIP service failed AutoIP service failed to start Automatic Automatic (DHCP-only) Automatically connect Available properties: %s
 Available settings: %s
 Available to all users B/G (2.4 GHz) BANNER BARS BLUETOOTH BOND BRIDGE BRIDGE PORT BSID BSSID Bluetooth Bluetooth device address:  Bluetooth type %s Bond Bond connection Bond connection %d Bonding arp-interval [0]:  Bonding arp-ip-target [none]:  Bonding downdelay [0]:  Bonding miimon [100]:  Bonding mode [balance-rr]:  Bonding monitoring mode %s Bonding primary interface [none]:  Bonding updelay [0]:  Bridge Bridge connection Bridge connection %d Bridge port STP path cost [100]:  Bridge port priority [32]:  Broadcast CAPABILITIES CARRIER CARRIER-DETECT CCMP CDMA connection CFG CHAN CHAP CINR CON-PATH CON-UUID CONNECTION CONNECTIONS CONNECTIVITY CTR-FREQ Cancel Cannot create '%s': %s Carrier/link changed Channel Cloned MAC [none]:  Cloned MAC address Closing %s failed: %s
 Config directory location Config file location Connected Connecting Connecting... Connection '%s' (%s) successfully added.
 Connection '%s' (%s) successfully deleted.
 Connection '%s' (%s) successfully modified.
 Connection '%s' (%s) successfully saved.
 Connection '%s' successfully deactivated (D-Bus active path: %s)
 Connection (name, UUID, or path):  Connection UUID Connection is already active Connection profile details Connection sharing via a protected WiFi network Connection sharing via an open WiFi network Connection successfully activated (D-Bus active path: %s)
 Connection successfully activated (master waiting for slaves) (D-Bus active path: %s)
 Connection type:  Connection with UUID '%s' created and activated on device '%s'
 Connection(s) (name, UUID, or path):  Connection(s) (name, UUID, path or apath):  Connectivity Could not activate connection: %s Could not create editor for connection '%s' of type '%s'. Could not create editor for invalid connection '%s'. Could not create temporary file: %s Could not daemonize: %s [error %u]
 Could not decode private key. Could not find "%s" binary Could not generate random data. Could not load file '%s'
 Could not parse arguments Could not re-read file: %s Couldn't convert password to UCS2: %d Couldn't decode PKCS#12 file: %d Couldn't decode PKCS#12 file: %s Couldn't decode PKCS#8 file: %s Couldn't decode certificate: %d Couldn't decode certificate: %s Couldn't initialize PKCS#12 decoder: %d Couldn't initialize PKCS#12 decoder: %s Couldn't initialize PKCS#8 decoder: %s Couldn't verify PKCS#12 file: %d Couldn't verify PKCS#12 file: %s Create Current DHCPv4 address Current nmcli configuration:
 DBUS-PATH DCB or FCoE setup failed DEFAULT DEFAULT6 DEVICE DEVICES DHCP anycast MAC address [none]:  DHCP client error DHCP client failed DHCP client failed to start DHCP4 DHCP6 DNS DNS servers DOMAIN DOMAINS DRIVER DRIVER-VERSION DSL DSL authentication DSL connection %d Datagram Deactivate Delete Destination Destination port [8472]:  Device Device '%s' has been connected.
 Device details Device disconnected by user or client Device is now managed Device is now unmanaged Disabled Disconnected by D-Bus Do you also want to clear '%s'? [yes]:  Do you also want to set '%s' to '%s'? [yes]:  Do you want to add IP addresses? %s Doesn't look like a PEM private key file. Don't become a daemon Don't become a daemon, and log to stderr Don't print anything Dynamic WEP (802.1x) EAP ETHERNET Edit '%s' value:  Edit Connection Edit a connection Edit... Editing existing '%s' connection: '%s' Editor failed: %s Egress priority maps [none]:  Enable IGMP snooping %s Enable STP %s Enable STP (Spanning Tree Protocol) Enable or disable WiFi devices Enable or disable WiMAX mobile broadband devices Enable or disable mobile broadband devices Enable or disable system networking Enter '%s' value:  Enter a list of IPv4 addresses of DNS servers.

Example: 8.8.8.8, 8.8.4.4
 Enter a list of IPv6 addresses of DNS servers.  If the IPv6 configuration method is 'auto' these DNS servers are appended to those (if any) returned by automatic configuration.  DNS servers cannot be used with the 'shared' or 'link-local' IPv6 configuration methods, as there is no upstream network. In all other IPv6 configuration methods, these DNS servers are used as the only DNS servers for this connection.

Example: 2607:f0d0:1002:51::4, 2607:f0d0:1002:51::1
 Enter a list of S/390 options formatted as:
  option = <value>, option = <value>,...
Valid options are: %s
 Enter a list of bonding options formatted as:
  option = <value>, option = <value>,... 
Valid options are: %s
'mode' can be provided as a name or a number:
balance-rr    = 0
active-backup = 1
balance-xor   = 2
broadcast     = 3
802.3ad       = 4
balance-tlb   = 5
balance-alb   = 6

Example: mode=2,miimon=120
 Enter a list of user permissions. This is a list of user names formatted as:
  [user:]<user name 1>, [user:]<user name 2>,...
The items can be separated by commas or spaces.

Example: alice bob charlie
 Enter bytes as a list of hexadecimal values.
Two formats are accepted:
(a) a string of hexadecimal digits, where each two digits represent one byte
(b) space-separated list of bytes written as hexadecimal digits (with optional 0x/0X prefix, and optional leading 0).

Examples: ab0455a6ea3a74C2
          ab 4 55 0xa6 ea 3a 74 C2
 Enter connection type:  Enter the type of WEP keys. The accepted values are: 0 or unknown, 1 or key, and 2 or passphrase.
 Error converting IP4 address '0x%X' to text form Error converting IP6 address '%s' to text form Error in configuration file: %s.
 Error initializing certificate data: %s Error updating secrets for %s: %s
 Error:  Error: %s
 Error: %s - no such connection profile. Error: %s argument is missing. Error: %s properties, nor it is a setting name.
 Error: %s. Error: %s: %s. Error: '%s' argument is missing. Error: '%s' is not a valid UID/GID. Error: '%s' is not a valid connection type. Error: '%s' is not a valid monitoring mode; use '%s' or '%s'.
 Error: '%s' is not a valid timeout for '%s' option. Error: '%s' is not an active connection.
 Error: '%s' is not valid 'connection' command. Error: '%s' is not valid argument for '%s' option. Error: '%s': '%s' is not a valid %s %s. Error: '%s': '%s' is not a valid %s MAC address. Error: '%s': '%s' is not valid; %s  Error: '%s': '%s' is not valid; use <%u-%u>. Error: '--fields' value '%s' is not valid here (allowed field: %s) Error: 'addr' is required. Error: 'ageing-time': '%s' is not a valid number <0-1000000>.
 Error: 'agent' command '%s' is not valid. Error: 'apn' is required. Error: 'arp-interval': '%s' is not a valid number <0-%u>.
 Error: 'autoconnect': %s. Error: 'bt-type': '%s' is not a valid bluetooth type.
 Error: 'bt-type': '%s' not valid; use [%s, %s (%s), %s]. Error: 'channel': '%s' is not a valid number <1-13>.
 Error: 'channel': '%s' is not valid; use <1-13>. Error: 'connection show': %s Error: 'dev' command '%s' is not valid. Error: 'dev' is required. Error: 'dev': '%s' is neither UUID nor interface name.
 Error: 'dev': '%s' is neither UUID, interface name, nor MAC. Error: 'device show': %s Error: 'device status': %s Error: 'device wifi' command '%s' is not valid. Error: 'device wifi': %s Error: 'downdelay': '%s' is not a valid number <0-%u>.
 Error: 'flags': '%s' is not valid; use <0-7>. Error: 'forward-delay': '%s' is not a valid number <2-30>.
 Error: 'general logging': %s Error: 'general permissions': %s Error: 'general' command '%s' is not valid. Error: 'hairpin': %s. Error: 'hello-time': '%s' is not a valid number <1-10>.
 Error: 'id' is required. Error: 'ifname' argument is required. Error: 'ifname': '%s' is not a valid interface nor '*'. Error: 'lacp_rate': '%s' is invalid ('slow' or 'fast').
 Error: 'local': '%s' is not a valid IP address.
 Error: 'master' is required. Error: 'max-age': '%s' is not a valid number <6-40>.
 Error: 'miimon': '%s' is not a valid number <0-%u>.
 Error: 'mode': %s. Error: 'mtu': '%s' is not a valid MTU. Error: 'multicast-snooping': %s. Error: 'multicast-snooping': %s.
 Error: 'networking connectivity' command '%s' is not valid. Error: 'networking' command '%s' is not valid. Error: 'nsp' is required. Error: 'p-key' is mandatory when 'parent' is specified.
 Error: 'p-key': '%s' is not a valid InfiniBand P_KEY. Error: 'parent': '%s' is not a valid interface name. Error: 'parent': not valid without 'p-key'. Error: 'primary': '%s' is not a valid interface name. Error: 'primary': '%s' is not a valid interface name.
 Error: 'priority': '%s' is not a valid number <0-%d>.
 Error: 'radio' command '%s' is not valid. Error: 'source-port-max': '%s' is not a valid number <0-65535>.
 Error: 'source-port-min': '%s' is not a valid number <0-65535>.
 Error: 'ssid' is required. Error: 'stp': %s. Error: 'stp': %s.
 Error: 'tap': %s.
 Error: 'type' argument is required. Error: 'updelay': '%s' is not a valid number <0-%u>.
 Error: 'username' is required. Error: 'vpn-type' is required. Error: <setting>.<property> argument is missing. Error: Access point with bssid '%s' not found. Error: Argument '%s' was expected, but '%s' provided. Error: BSSID to connect to (%s) differs from bssid argument (%s). Error: Cannot activate connection: %s.
 Error: Connection '%s' does not exist. Error: Connection activation failed. Error: Connection activation failed.
 Error: Connection activation failed: %s Error: Connection activation failed: %s. Error: Connection deletion failed: %s Error: Could not create NMClient object: %s. Error: Device '%s' is not a Wi-Fi device. Error: Device '%s' not found. Error: Device activation failed: %s Error: Failed to add/activate new connection: Unknown error Error: NetworkManager is not running. Error: No Wi-Fi device found. Error: No access point with BSSID '%s' found. Error: No arguments provided. Error: No connection specified. Error: No interface specified. Error: No network with SSID '%s' found. Error: Object '%s' is unknown, try 'nmcli help'. Error: Option '%s' is unknown, try 'nmcli -help'. Error: Option '--pretty' is mutually exclusive with '--terse'. Error: Option '--pretty' is specified the second time. Error: Option '--terse' is mutually exclusive with '--pretty'. Error: Option '--terse' is specified the second time. Error: Parameter '%s' is neither SSID nor BSSID. Error: SSID or BSSID are missing. Error: Timeout %d sec expired. Error: Unexpected argument '%s' Error: Unknown connection '%s'. Error: Unknown parameter %s. Error: bssid argument value '%s' is not a valid BSSID. Error: cannot delete unknown connection(s): %s. Error: connection ID is missing. Error: connection is not saved. Type 'save' first.
 Error: connection is not valid: %s
 Error: connection verification failed: %s
 Error: extra argument not allowed: '%s'. Error: failed to modify %s.%s: %s. Error: failed to remove a value from %s.%s: %s. Error: failed to remove value of '%s': %s
 Error: failed to set '%s' property: %s
 Error: fields for '%s' options are missing. Error: invalid '%s' argument: '%s' (use on/off). Error: invalid <setting>.<property> '%s'. Error: invalid connection type; %s
 Error: invalid connection type; %s. Error: invalid extra argument '%s'. Error: invalid gateway address '%s'
 Error: invalid or not allowed setting '%s': %s. Error: invalid property '%s': %s. Error: invalid property: %s
 Error: invalid property: %s, neither a valid setting name.
 Error: invalid setting argument '%s'; valid are [%s]
 Error: invalid setting name; %s
 Error: missing argument for '%s' option. Error: missing setting for '%s' property
 Error: nmcli (%s) and NetworkManager (%s) versions don't match. Force execution using --nocheck, but the results are unpredictable. Error: no active connection provided. Error: no argument given; valid are [%s]
 Error: no setting selected; valid are [%s]
 Error: not all active connections found. Error: only one of 'id', uuid, or 'path' can be provided. Error: only these fields are allowed: %s Error: openconnect failed with signal %d
 Error: openconnect failed with status %d
 Error: openconnect failed: %s
 Error: polkit agent initialization failed: %s Error: property %s
 Error: save-confirmation: %s
 Error: secret agent initialization failed Error: setting '%s' is mandatory and cannot be removed.
 Error: status-line: %s
 Error: unknown setting '%s'
 Error: unknown setting: '%s'
 Error: value for '%s' argument is required. Error: wep-key-type argument value '%s' is invalid, use 'key' or 'phrase'. Ethernet Ethernet connection %d Exit immediately if NetworkManager is not running or connecting FIRMWARE-MISSING FIRMWARE-VERSION FQDN to send to DHCP server FREQ Failed to decode PKCS#8 private key. Failed to decode certificate. Failed to decrypt the private key. Failed to decrypt the private key: %d. Failed to decrypt the private key: %s (%s) Failed to decrypt the private key: decrypted data too large. Failed to decrypt the private key: unexpected padding length. Failed to encrypt the data: %s (%s) Failed to encrypt: %d. Failed to finalize decryption of the private key: %d. Failed to find expected PKCS#8 end tag '%s'. Failed to find expected PKCS#8 start tag. Failed to initialize the MD5 context: %d. Failed to initialize the crypto engine. Failed to initialize the crypto engine: %d. Failed to initialize the decryption cipher slot. Failed to initialize the decryption context. Failed to initialize the encryption cipher slot. Failed to initialize the encryption context. Failed to read configuration: %s
 Failed to register with the requested network Failed to select the specified APN Failed to set IV for decryption. Failed to set IV for encryption. Failed to set symmetric key for decryption. Failed to set symmetric key for encryption. Forward delay Forward delay [15]:  GATEWAY GENERAL GROUP GSM Modem's SIM PIN required GSM Modem's SIM PUK required GSM Modem's SIM card not inserted GSM Modem's SIM wrong GSM connection GVRP,  Gateway HWADDR Hairpin mode Hello time Hello time [2]:  Hex-encoded Interface Identifier Hide Hostname ID INFINIBAND IP configuration could not be reserved (no available address, timeout, etc.) IP-IFACE IP4 IP6 IPv4 CONFIGURATION IPv6 CONFIGURATION IV contains non-hexadecimal digits. IV must be an even number of bytes in length. Identity If you are creating a VPN, and the VPN connection you wish to create does not appear in the list, you may not have the correct VPN plugin installed. Ignore Ignoring unrecognized log domain(s) '%s' from config files.
 Ignoring unrecognized log domain(s) '%s' passed on command line.
 InfiniBand InfiniBand P_Key connection did not specify parent interface name InfiniBand connection InfiniBand connection %d InfiniBand device does not support connected mode InfiniBand transport mode Infra Ingress priority maps [none]:  Interface name [*]:  Interface:  Invalid IV length (must be at least %d). Invalid IV length (must be at least %zd). Invalid configuration option '%s'; allowed [%s]
 Invalid option.  Please use --help to see a list of valid options. Invalid verify option: %s
 JSON configuration Key LACP rate ('slow' or 'fast') [slow]:  LEAP LEVEL LOOSE_BINDING,  Link down delay Link monitoring Link up delay Link-Local List of plugins separated by ',' Local address [none]:  Log domains separated by ',': any combination of [%s] Log level: one of [%s] MAC [none]:  MAC address ageing time [300]:  MACVLAN mode:  MASTER-PATH MII (recommended) MODE MSCHAP MSCHAPv2 MTU MTU [auto]:  MVRP,  Make all warnings fatal Malformed PEM file: DEK-Info was not the second tag. Malformed PEM file: Proc-Type was not first tag. Malformed PEM file: invalid format of IV in DEK-Info tag. Malformed PEM file: no IV found in DEK-Info tag. Malformed PEM file: unknown Proc-Type tag '%s'. Malformed PEM file: unknown private key cipher '%s'. Manual Master:  Max age Max age [20]:  Maximum source port [0]:  Metric Minimum source port [0]:  Mobile Broadband Mobile broadband connection %d Mobile broadband network password Mode Mode %s Modem failed or no longer available Modem initialization failed Modem now ready and available ModemManager is unavailable Modify network connections for all users Modify persistent system hostname Modify personal network connections Monitoring connection activation (press any key to continue)
 Monitoring frequency Must specify a P_Key if specifying parent N/A NAME NETWORKING NM-MANAGED NSP Necessary firmware for the device may be missing Network name Network registration denied Network registration timed out NetworkManager TUI NetworkManager active profiles NetworkManager connection profiles NetworkManager is not running. NetworkManager logging NetworkManager monitors all network connections and automatically
chooses the best connection to use.  It also allows the user to
specify wireless access points which wireless cards in the computer
should associate with. NetworkManager needs to turn off networks NetworkManager permissions NetworkManager status NetworkManager went to sleep Networking Never use this network for default route New Connection New connection activation was enqueued Next Hop No carrier could be established No custom routes are defined. No dial tone No reason given No such connection '%s' Not searching for networks OK OLPC Mesh OLPC Mesh channel [1]:  OPTION One custom route %d custom routes Open System Opening %s failed: %s
 Option '--terse' requires specific '--fields' option values , not '%s' Option '--terse' requires specifying '--fields' PAN connection PAP PCI PEM certificate had no end tag '%s'. PEM certificate had no start tag '%s'. PEM key file had no end tag '%s'. PERMISSION PIN PIN check failed PIN code is needed for the mobile broadband device PIN code required PPP CONFIGURATION PPP failed PPP service disconnected PPP service failed to start PPPoE PPPoE connection PPPoE username:  PRODUCT P_KEY [none]:  Parent Parent device [none]:  Parent interface [none]:  Password Password [none]:  Password must be UTF-8 Password:  Passwords or encryption keys are required to access the wireless network '%s'. Path cost Please select an option Prefix Press <Enter> to finish adding addresses.
 Primary Print NetworkManager version and exit Priority Private key cipher '%s' was unknown. Private key password Profile name Property name?  Put NetworkManager to sleep or wake it up (should only be used by system power management) Quit RATE READONLY REASON REORDER_HEADERS,  ROUTE RSN-FLAGS RSSI RUNNING Radio switches Remove Require 128-bit encryption Require IPv4 addressing for this connection Require IPv6 addressing for this connection Round-robin Routing SECURITY SIGNAL SIM PIN was incorrect SLAVES SPEC-OBJECT SPEED SSID SSID length is out of range <1-32> bytes SSID or BSSID:  SSID-HEX SSID:  STARTUP STATE STP priority [32768]:  Saving the connection with 'autoconnect=yes'. That might result in an immediate activation of the connection.
Do you still want to save? %s Search domains Secrets were required, but not provided Security Select the type of connection you wish to create. Select the type of slave connection you wish to add. Select... Service Service [none]:  Set Hostname Set hostname to '%s' Set system hostname Setting '%s' is not present in the connection.
 Setting name?  Shared Shared Key Shared connection service failed Shared connection service failed to start Show Show password Slaves Specify the location of a PID file State file location Status of devices Success System System policy prevents control of network connections System policy prevents enabling or disabling WiFi devices System policy prevents enabling or disabling WiMAX mobile broadband devices System policy prevents enabling or disabling mobile broadband devices System policy prevents enabling or disabling system networking System policy prevents modification of network settings for all users System policy prevents modification of personal network settings System policy prevents modification of the persistent system hostname System policy prevents putting NetworkManager to sleep or waking it up System policy prevents sharing connections via a protected WiFi network System policy prevents sharing connections via an open WiFi network TEAM TEAM PORT TIMESTAMP TIMESTAMP-REAL TKIP TUN device mode TX-POW TYPE Tap %s Team Team JSON configuration [none]:  Team connection Team connection %d The Bluetooth connection failed or timed out The IP configuration is no longer valid The Wi-Fi network could not be found The connection profile has been removed from another client. You may type 'save' in the main menu to restore it.
 The connection profile has been removed from another client. You may type 'save' to restore it.
 The connection was not an InfiniBand connection. The device could not be readied for configuration The device parent's management changed The device was removed The device's active connection disappeared The device's existing connection was assumed The device's parent changed The dialing attempt failed The dialing request timed out The error cannot be fixed automatically.
 The expected start of the response The interval between connectivity checks (in seconds) The line is busy The mode of the device and the connection didn't match The modem could not be found The supplicant is now available Time to wait for a connection, in seconds (without the option, default value is 30) Transport mode Transport mode %s Tunnel mode:  Type 'describe [<setting>.<prop>]' for detailed property description. Type 'help' or '?' for available commands. UDI USB USERNAME UUID Unable to add new connection: %s Unable to delete connection: %s Unable to determine private key type. Unable to save connection: %s Unable to set hostname: %s Unexpected amount of data after encrypting. Unknown Unknown command argument: '%s'
 Unknown command: '%s'
 Unknown error Unknown log domain '%s' Unknown log level '%s' Unknown parameter: %s
 Usage Usage: nmcli agent all { help }

Runs nmcli as both NetworkManager secret and a polkit agent.

 Usage: nmcli agent polkit { help }

Registers nmcli as a polkit action for the user session.
When a polkit daemon requires an authorization, nmcli asks the user and gives
the response back to polkit.

 Usage: nmcli agent secret { help }

Runs nmcli as NetworkManager secret agent. When NetworkManager requires
a password it asks registered agents for it. This command keeps nmcli running
and if a password is required asks the user for it.

 Usage: nmcli agent { COMMAND | help }

COMMAND := { secret | polkit | all }

 Usage: nmcli connection clone { ARGUMENTS | help }

ARGUMENTS := [--temporary] [id | uuid | path] <ID> <new name>

Clone an existing connection profile. The newly created connection will be
the exact copy of the <ID>, except the uuid property (will be generated) and
id (provided as <new name> argument).

 Usage: nmcli connection delete { ARGUMENTS | help }

ARGUMENTS := [id | uuid | path] <ID>

Delete a connection profile.
The profile is identified by its name, UUID or D-Bus path.

 Usage: nmcli connection down { ARGUMENTS | help }

ARGUMENTS := [id | uuid | path | apath] <ID> ...

Deactivate a connection from a device (without preventing the device from
further auto-activation). The profile to deactivate is identified by its name,
UUID or D-Bus path.

 Usage: nmcli connection edit { ARGUMENTS | help }

ARGUMENTS := [id | uuid | path] <ID>

Edit an existing connection profile in an interactive editor.
The profile is identified by its name, UUID or D-Bus path

ARGUMENTS := [type <new connection type>] [con-name <new connection name>]

Add a new connection profile in an interactive editor.

 Usage: nmcli connection load { ARGUMENTS | help }

ARGUMENTS := <filename> [<filename>...]

Load/reload one or more connection files from disk. Use this after manually
editing a connection file to ensure that NetworkManager is aware of its latest
state.

 Usage: nmcli connection modify { ARGUMENTS | help }

ARGUMENTS := [id | uuid | path] <ID> ([+|-]<setting>.<property> <value>)+

Modify one or more properties of the connection profile.
The profile is identified by its name, UUID or D-Bus path. For multi-valued
properties you can use optional '+' or '-' prefix to the property name.
The '+' sign allows appending items instead of overwriting the whole value.
The '-' sign allows removing selected items instead of the whole value.

Examples:
nmcli con mod home-wifi wifi.ssid rakosnicek
nmcli con mod em1-1 ipv4.method manual ipv4.addr "192.168.1.2/24, 10.10.1.5/8"
nmcli con mod em1-1 +ipv4.dns 8.8.4.4
nmcli con mod em1-1 -ipv4.dns 1
nmcli con mod em1-1 -ipv6.addr "abbe::cafe/56"
nmcli con mod bond0 +bond.options mii=500
nmcli con mod bond0 -bond.options downdelay

 Usage: nmcli connection reload { help }

Reload all connection files from disk.

 Usage: nmcli connection show { ARGUMENTS | help }

ARGUMENTS := [--active] [--order <order spec>]

List in-memory and on-disk connection profiles, some of which may also be
active if a device is using that connection profile. Without a parameter, all
profiles are listed. When --active option is specified, only the active
profiles are shown. --order allows custom connection ordering (see manual page).

ARGUMENTS := [--active] [id | uuid | path | apath] <ID> ...

Show details for specified connections. By default, both static configuration
and active connection data are displayed. It is possible to filter the output
using global '--fields' option. Refer to the manual page for more information.
When --active option is specified, only the active profiles are taken into
account. Use global --show-secrets option to reveal associated secrets as well.
 Usage: nmcli connection up { ARGUMENTS | help }

ARGUMENTS := [id | uuid | path] <ID> [ifname <ifname>] [ap <BSSID>] [nsp <name>] [passwd-file <file with passwords>]

Activate a connection on a device. The profile to activate is identified by its
name, UUID or D-Bus path.

ARGUMENTS := ifname <ifname> [ap <BSSID>] [nsp <name>] [passwd-file <file with passwords>]

Activate a device with a connection. The connection profile is selected
automatically by NetworkManager.

ifname      - specifies the device to active the connection on
ap          - specifies AP to connect to (only valid for Wi-Fi)
nsp         - specifies NSP to connect to (only valid for WiMAX)
passwd-file - file with password(s) required to activate the connection

 Usage: nmcli device connect { ARGUMENTS | help }

ARGUMENTS := <ifname>

Connect the device.
NetworkManager will try to find a suitable connection that will be activated.
It will also consider connections that are not set to auto-connect.

 Usage: nmcli device show { ARGUMENTS | help }

ARGUMENTS := [<ifname>]

Show details of device(s).
The command lists details for all devices, or for a given device.

 Usage: nmcli device status { help }

Show status for all devices.
By default, the following columns are shown:
 DEVICE     - interface name
 TYPE       - device type
 STATE      - device state
 CONNECTION - connection activated on device (if any)
Displayed columns can be changed using '--fields' global option. 'status' is
the default command, which means 'nmcli device' calls 'nmcli device status'.

 Usage: nmcli general hostname { ARGUMENTS | help }

ARGUMENTS := [<hostname>]

Get or change persistent system hostname.
With no arguments, this prints currently configured hostname. When you pass
a hostname, NetworkManager will set it as the new persistent system hostname.

 Usage: nmcli general logging { ARGUMENTS | help }

ARGUMENTS := [level <log level>] [domains <log domains>]

Get or change NetworkManager logging level and domains.
Without any argument current logging level and domains are shown. In order to
change logging state, provide level and/or domain. Please refer to the man page
for the list of possible logging domains.

 Usage: nmcli general permissions { help }

Show caller permissions for authenticated operations.

 Usage: nmcli general status { help }

Show overall status of NetworkManager.
'status' is the default action, which means 'nmcli gen' calls 'nmcli gen status'

 Usage: nmcli general { COMMAND | help }

COMMAND := { status | hostname | permissions | logging }

  status

  hostname [<hostname>]

  permissions

  logging [level <log level>] [domains <log domains>]

 Usage: nmcli networking connectivity { ARGUMENTS | help }

ARGUMENTS := [check]

Get network connectivity state.
The optional 'check' argument makes NetworkManager re-check the connectivity.

 Usage: nmcli networking off { help }

Switch networking off.

 Usage: nmcli networking on { help }

Switch networking on.

 Usage: nmcli networking { COMMAND | help }

COMMAND := { [ on | off | connectivity ] }

  on

  off

  connectivity [check]

 Usage: nmcli radio all { ARGUMENTS | help }

ARGUMENTS := [on | off]

Get status of all radio switches, or turn them on/off.

 Usage: nmcli radio wifi { ARGUMENTS | help }

ARGUMENTS := [on | off]

Get status of Wi-Fi radio switch, or turn it on/off.

 Usage: nmcli radio wwan { ARGUMENTS | help }

ARGUMENTS := [on | off]

Get status of mobile broadband radio switch, or turn it on/off.

 Username Username [none]:  VALUE VENDOR VERSION VLAN VLAN connection VLAN connection %d VLAN flags (<0-7>) [none]:  VLAN id VLAN parent device or connection UUID:  VPN VPN connected VPN connecting VPN connecting (getting IP configuration) VPN connecting (need authentication) VPN connecting (prepare) VPN connection VPN connection %d VPN connection (name, UUID, or path):  VPN connection failed VPN connection successfully activated (D-Bus active path: %s)
 VPN disconnected VPN type:  VPN-STATE VXLAN Valid connection types: %s
 Verify connection: %s
 Verify setting '%s': %s
 WEP WEP 128-bit Passphrase WEP 40/128-bit Key (Hex or ASCII) WEP index WEP key index1 (Default) WEP key index2 WEP key index3 WEP key index4 WEP key is guessed to be of '%s'
 WI-FI WIFI WIFI-HW WIFI-PROPERTIES WIMAX WIMAX-HW WIMAX-PROPERTIES WINS WIRED-PROPERTIES WPA WPA & WPA2 Enterprise WPA & WPA2 Personal WPA-FLAGS WPA1 WPA2 WWAN WWAN radio switch WWAN-HW Waits for NetworkManager to finish activating startup network connections. Warning: changes will have no effect until '%s' includes 1 (enabled)

 Warning: editing existing connection '%s'; 'con-name' argument is ignored
 Warning: editing existing connection '%s'; 'type' argument is ignored
 Warning: master='%s' doesn't refer to any existing profile.
 Warning: nmcli (%s) and NetworkManager (%s) versions don't match. Use --nocheck to suppress the warning.
 Warning: password for '%s' not given in 'passwd-file' and nmcli cannot ask without '--ask' option.
 Wi-Fi Wi-FiAutomatic Wi-FiClient Wi-Fi connection %d Wi-Fi mode Wi-Fi radio switch Wi-Fi scan list Wi-Fi securityNone WiMAX WiMAX NSP name:  Wired Wired 802.1X authentication Wired connection Wired connection %d Writing to %s failed: %s
 XOR You may edit the following properties: %s
 You may edit the following settings: %s
 You must be root to run %s!
 ZONE ['%s' setting values]
 [NM property description] [nmcli specific description] activate [<ifname>] [/<ap>|<nsp>]  :: activate the connection

Activates the connection.

Available options:
<ifname>    - device the connection will be activated on
/<ap>|<nsp> - AP (Wi-Fi) or NSP (WiMAX) (prepend with / when <ifname> is not specified)
 activated activating add [<value>]  :: append new value to the property

This command adds provided <value> to this property, if the property is of a container type. For single-valued properties the property value is replaced (same as 'set').
 advertise,  agent-owned,  always asleep auth auto back  :: go to upper menu level

 bandwidth percentages must total 100%% bluetooth bond bridge bridge-slave bytes change  :: change current value

Displays current value and allows editing it.
 connected connected (local only) connected (site only) connecting connecting (checking IP connectivity) connecting (configuring) connecting (getting IP configuration) connecting (need authentication) connecting (prepare) connecting (starting secondary connections) connection connection failed connection id fallback%s %d deactivated deactivating default default route cannot be added (NetworkManager handles it by itself) describe  :: describe property

Shows property description. You can consult nm-settings(5) manual page to see all NM settings and properties.
 describe [<setting>.<prop>]  :: describe property

Shows property description. You can consult nm-settings(5) manual page to see all NM settings and properties.
 device '%s' not compatible with connection '%s' disabled disconnected disconnecting don't know how to get the property value element invalid enabled enabled,  eth0 ethernet failed to read passwd-file '%s': %s field '%s' has to be alone filename has invalid format (%s) flags are invalid flags invalid flags invalid - disabled full goto <setting>[.<prop>] | <prop>  :: enter setting/property for editing

This command enters into a setting or property for editing it.

Examples: nmcli> goto connection
          nmcli connection> goto secondaries
          nmcli> goto ipv4.addresses
 has to match '%s' property for PKCS#12 help/? [<command>]  :: help for nmcli commands

 help/? [<command>]  :: help for the nmcli commands

 index '%d' is not in range <0-%d> index '%d' is not in the range of <0-%d> index '%s' is not valid invalid '%s' or its value '%s' invalid IP address: %s invalid IPv4 address '%s' invalid IPv6 address '%s' invalid VPN secrets invalid certificate format invalid field '%s'; allowed fields: %s invalid field '%s'; allowed fields: %s and %s, or %s,%s invalid metric '%s' invalid option '%s' invalid option '%s' or its value '%s' invalid prefix '%s'; <1-%d> allowed invalid priority map '%s' invalid route: %s invalid setting name in 'password' entry '%s' is not a valid MAC address limited long device name%s %s macvlan mandatory option '%s' is missing millisecondsms missing colon in 'password' entry '%s' missing dot in 'password' entry '%s' missing filename missing name, try one of [%s] missing option mobile broadband must contain 8 comma-separated numbers neither a valid connection nor device given never new hostname nmcli can accepts both direct JSON configuration data and a file name containing the configuration. In the latter case the file is read and the contents is put into this property.

Examples: set team.config { "device": "team0", "runner": {"name": "roundrobin"}, "ports": {"eth1": {}, "eth2": {}} }
          set team.config /etc/my-team.conf
 nmcli successfully registered as a NetworkManager's secret agent.
 nmcli successfully registered as a polkit agent.
 nmcli tool, version %s
 no no (guessed) no active connection on device '%s' no active connection or device no device found for connection '%s' no item to remove no priority to remove no valid VPN secrets none not a valid interface name not required,  not saved,  off on only one of '%s' and '%s' can be set path is not absolute (%s) portal preparing print [all]  :: print setting or connection values

Shows current property or the whole connection.

Example: nmcli ipv4> print all
 print [property|setting|connection]  :: print property (setting, connection) value(s)

Shows property value. Providing an argument you can also display values for the whole setting or connection.
 priority '%s' is not valid (<0-%ld>) private key password not provided property invalid property invalid (not enabled) property is empty property is invalid property is missing property is not specified and neither is '%s:%s' property missing property value '%s' is empty or too long (>64) quit  :: exit nmcli

This command exits nmcli. When the connection being edited is not saved, the user is asked to confirm the action.
 remove <setting>[.<prop>]  :: remove setting or reset property value

This command removes an entire setting from the connection, or if a property
is given, resets that property to the default value.

Examples: nmcli> remove wifi-sec
          nmcli> remove eth.mtu
 requires '%s' or '%s' setting requires presence of '%s' setting in the connection requires setting '%s' property running seconds set [<setting>.<prop> <value>]  :: set property value

This command sets property value.

Example: nmcli> set con.id My connection
 set [<value>]  :: set new value

This command sets provided <value> to this property
 setting this property requires non-zero '%s' property started starting sum not 100% team team-slave teamd control failed the VPN service did not start in time the VPN service failed to start the VPN service returned invalid configuration the VPN service stopped unexpectedly the base network connection was interrupted the connection attempt timed out the connection was removed the property can't be changed the second component of route ('%s') is neither a next hop address nor a metric the user was disconnected this property cannot be empty for '%s=%s' this property is not allowed for '%s=%s' unavailable unknown unknown device '%s'. unknown reason unmanaged use 'goto <setting>' first, or 'describe <setting>.<property>'
 use 'goto <setting>' first, or 'set <setting>.<property>'
 value '%d' is out of range <%d-%d> willing,  yes yes (guessed) Project-Id-Version: ru
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=NetworkManager&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-05-12 09:07+0000
PO-Revision-Date: 2016-06-09 12:42+0000
Last-Translator: Aleksey Kabanov <Unknown>
Language-Team: русский <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 17:45+0000
X-Generator: Launchpad (build 18115)
Language: ru
   Адрес успешно добавлен: %s
   Предупреждение: адрес уже существует: %s
   Предупреждение. Игнорируется мусор в конце: «%s».
 # Создано в NetworkManager
 # Объединено с %s

 %d (отключено) %d (включено, предпочитая открытый IP) %d (включено, предпочитая временный IP) %d (неизвестно) %d. Адрес IPv4 содержит недопустимый префикс %d. Неверный адрес IPv4 %d. Маршрут содержит недопустимый префикс %d. Неверный маршрут Сеть %s %s уже запущен (pid %ld)
 %s. Используйте --help для вывода списка допустимых параметров.
 %u МГц %u Мб/c %u МБ/с Недопустимый канал «%d». Недопустимое значение свойства: «%d» (должно быть <= %d). «%d» выходит за пределы диапазона <%d-%d> «%d» выходит за пределы диапазона <128-16384> Значение «%d» вне диапазона <0-3> Недопустимый канал «%ld». '%s' и '%s' не могут иметь разные значения «%s» может использоваться только вместе с «%s=%s» (WEP) '%s' не может быть пустым Соединения «%s» требуют «%s» в этот свойстве «%s» содержит недопустимые символы. Используйте [A-Za-z._-]. '%s' должен быть один Неоднозначное значение «%s» (%s x %s) «%s» может содержать UUID или имя интерфейса. «%s» не содержит приоритет приложения DCB «%s» не является числом «%s» не содержит допустимый флаг DCB Недействительный MAC Ethernet: «%s» «%s» не является допустимым значением порта Ethernet Недопустимое значение IBoIP P_Key: «%s» Недопустимый адрес IPv4 («%s») для «%s». Недействительный MAC InfiniBand: «%s» Недействительный MAC-адрес: «%s» Недействительный MAC-адрес: «%s» «%s» не является действительным PSK Недействительный UUID: «%s» «%s» не является допустимым режимом Wi-Fi «%s» не является допустимым диапазоном Недопустимый канал «%s». Недопустимое значение «%s». Используйте значения от 1 до 13. «%s» не является допустимым значением дуплекса Неверное шестнадцатеричное значение: «%s» Недействительное имя интерфейса: «%s» Недопустимое имя интерфейса («%s») для параметра «%s» Число выходит за пределы диапазона или определено неверно: «%s» '%s' не является верной конфигурацией группы или имени файла. «%s» — недопустимое значение «%s» «%s» — недопустимое значение свойства Недопустимое значение «%s» Недействительный мастер: «%s». Используйте имя интерфейса или UUID соединения. Недопустимое значение «%s». Используйте 0, 1 или 2. Недопустимое значение «%s». Используйте формат: <параметр>=<значение> Недопустимое значение «%s». Ожидается [%s] или  [%s] Неверное значение «%s». Допустимые значения: [e, o, n]. «%s» должно содержать 5-6 цифр «%s» не является числом в диапазоне от 0 до %u включительно «%s» не является числом в диапазоне от 0 до %u включительно или %u «%s» не входит в %s «%s» может быть равно [0 (неизвестно), 1 (ключ), 2 (парольная фраза)] Пустой параметр: «%s» Параметр «%s» действителен только для «%s=%s». Для «%s» необходимо, чтобы был определен параметр «%s» параметр '%s' должен быть строкой «%s» требует установки свойства «%s» Для защиты «%s» необходимо определить «%s» Для защиты «%s» необходимо выражение «%s=%s» Значение «%s» не соответствует «%s=%s» «%s=%s» несовместимо с «%s > 0» Недопустимая конфигурация («%s=%s») для «%s» (%s): неверный IID %s
 (нет дополнительных маршрутов) (dynamic-wep не поддерживается) (wpa-enterprise не поддерживается) (по умолчанию) (нет) (неизвестная ошибка) (неизвестно) * ---[ Главное меню]---
goto     [<параметр> | <свойство>]             :: перейти к параметру или свойству
remove   <параметр>[.<свойство>] | <свойство>  :: удалить параметр или сбросить значение свойства
set      [<параметр>.<свойство> <значение>]    :: установить новое значение свойства
describe [<параметр>.<свойство>]               :: показать описание отдельного свойства
print    [all | <параметр>[.<свойство>]]       :: показать список параметров и свойств
verify   [all | fix]                           :: проверить свойства
save     [persistent|temporary]                :: сохранить соединение
activate [<интерфейс>] [/<ap>|<nsp>]           :: включить соединение
back                                           :: вернуться к предыдущему меню
help/?   [<команда>]                           :: показать это справочное сообщение
nmcli    <параметр> <значение>                 :: конфигурация nmcli
quit                                           :: выход
 ---[ меню свойств ]---
set      [<значение>]               :: определить новое значение
add      [<значение>]               :: добавить параметр
change                           :: изменить текущее значение
remove   [<индекс> | <параметр>]    :: удалить значение
describe                         :: добавить описание
print    [параметр | соединение]  :: вывести значения параметров
back                             :: вернуться к предыдущему шагу
help/?   [<команда>]             :: показать справку или описание команды
quit                             :: выход из nmcli
 -1 (не задано) 0 0 (НЕТ) 0 (отключено) 0 (нет) 1024 802.1X Сбой конфигурации заявителя 802.1X Заявитель 802.1X отключён Сбой заявителя 802.1X Превышено время ожидания при проверке подлинности заявителя 802.1X 802.3ad ===| интерактивный редактор соединений nmcli |=== A (5 ГГц) Сбой зависимости соединения Для подключения к «%s» требуется пароль. Ошибка RFC 2684 Ethernet с использованием моста ADSL Сбой вторичного подключения основого соединения АКТИВЕН АКТИВНЫЙ ПУТЬ АДРЕС ADHOC ADSL Соединение ADSL ADSL инкапсуляция ADSL инкапсуляция %s Протокол ADSL ТОЧКА ДОСТУПА APN:  ARP Получатели ARP АВТОПОДКЛЮЧЕНИЕ ПРИОРИТЕТ АВТОПОДКЛЮЧЕНИЯ ДОСТУПНЫЕ ПУТИ СОЕДИНЕНИЯ ДОСТУПНЫЕ-ПОДКЛЮЧЕНИЯ Точка доступа Включить Подключиться Активные параметры соединения Ошибка подключения Активный и запасные Ad-Hoc Динамическая сеть Адаптивное распределение нагрузки (ALB) Адаптивное распределение нагрузки исходящего трафика (TLB) Добавить Добавить... Добавление нового соединения «%s» Адреса Время старения Разрешить контроль сетевых подключений Допустимые значения «%s»: %s
 http(s) адрес для проверки соединения с Интернетом Требуются имя интерфейса и UUID
 Вы действительно хотите удалить соединение '%s'? Запрашивать пароль каждый раз Аутентификация Ошибка аутентификации: %s
 Для доступа к беспроводной сети требуется аутентификация Сбой службы AutoIP Ошибка службы AutoIP Не удалось запустить службу AutoIP Автоматически Автоматически (только DHCP) Подключаться автоматически Доступные свойства: %s
 Доступные параметры: %s
 Доступно всем B/G (2.4 ГГц) БАННЕР СТОЛБЦЫ BLUETOOTH СВЯЗЬ МОСТ ПОРТ МОСТА BSID BSSID Bluetooth Адрес устройства Bluetooth:  Тип Bluetooth: %s Bond Агрегированное (Bond) соединение Подключение Bond %d Bonding arp-interval [0]:  Bonding arp-ip-target [нет]:  Bonding downdelay [0]:  Bonding miimon [100]:  Режим агрегации (Bond) [balance-rr]:  Режим мониторинга агрегации (Bond): %s Основной интерфейс агрегации (Bond) [нет]:  Bonding updelay [0]:  Мост Соединение типа «мост» Соединение типа «мост» %d Стоимость STP порта моста [100]:  Приоритет порта моста [32]:  Рассылка ВОЗМОЖНОСТИ НЕСУЩАЯ ОПРЕД.НЕСУЩЕЙ CCMP Соединение CDMA КОНФИГУРАЦИЯ КАНАЛ CHAP CINR ПУТЬ ПОДКЛЮЧЕНИЯ UUID СОЕДИНЕНИЯ СОЕДИНЕНИЕ ПОДКЛЮЧЕНИЯ СВЯЗЬ CTR-FREQ Отменить Не удаётся создать '%s': %s Изменена несущая/связь Канал Клонированный MAC [нет]:  Клонированный MAC-адрес При закрытии %s произошла ошибка: %s
 Путь к каталогу конфигурации Расположение файла конфигурации Подключено Подключение... Подключение... Соединение «%s» (%s) добавлено.
 Соединение '%s' (%s) успешно удалено.
 Соединение «%s» (%s) изменено.
 Соединение «%s» (%s) сохранено.
 Соединение '%s' успешно выключено (Активный путь DBus: %s)
 Соединение (имя, UUID, или путь):  UUID соединения Соединение уже активно Параметры соединения профиля Совместное использование соединений в закрытой сети WiFi Совместное использование соединений в открытой сети WiFi Соединение успешно активировано (адрес действующего D-Bus: %s)
 Подключение успешно активировано (мастер ждет рабов) (DBus активный путь: %s)
 Тип соединения:  Соединение с UUID «%s» создано и задействовано на устройстве «%s»
 Соединение(я) (имя, UUID, или путь):  Соединение(я) (имя, UUID, путь или apath):  Соединение Не удалось подключиться к %s Не удалось открыть окно редактора для соединения «%s» типа «%s». Не удалось открыть окно редактора для неверного соединения «%s». Не удалось создать временный файл: %s Невозможно преобразовать в службу: %s [ошибка %u]
 Не удалось расшифровать личный ключ. Не удалось найти бинарный файл «%s» Ошибка при генерации случайных данных. Не удалось загрузить «%s»
 Не удалось обработать аргументы Не удалось заново прочитать файл: %s Не удалось преобразовать пароль в UCS2: %d Не удалось расшифровать файл PKCS#12: %d Не удалось расшифровать файл PKCS#12: %s Не удалось расшифровать файл PKCS#12: %s Не удалось расшифровать сертификат: %d Не удалось расшифровать сертификат: %s Не удалось инициализировать декодер PKCS#12: %d Не удалось инициализировать декодер PKCS#12: %s Не удалось инициализировать декодер PKCS#8: %s Не удалось проверить файл PKCS#12: %d Не удалось проверить файл PKCS#12: %s Создать Текущий адрес DHCPv4 Текущая конфигурация nmcli:
 DBUS-PATH Не удалось настроить DCB или FCoE ПО УМОЛЧАНИЮ ПО УМОЛЧАНИЮ 6 УСТРОЙСТВО УСТРОЙСТВА MAC-адрес рассылки DHCP [нет]:  Ошибка клиента DHCP Сбой клиента DHCP Не удалось запустить клиент DHCP DHCP4 DHCP6 DNS Серверы DNS ДОМЕН ДОМЕНЫ ДРАЙВЕР ВЕРСИЯ ДРАЙВЕРА DSL Аутентификация DSL Cоединение DSL %d Датаграмма Отключить Удалить Назначение Порт назначения [8472]:  Устройство Устройство «%s» подключено.
 Сведения об устройстве Устройство отключено пользователем или клиентом Устройство обслуживается Устройство не обслуживается Отключено Отключено через D-Bus Очистить «%s»? [да]:  Присвоить «%s» значение «%s»? [да]:  Хотите добавить IP-адреса? %s Возможно, это не файл личного ключа PEM. Не использовать в качестве службы Не работать в режиме службы и не входить в stderr Ничего не выводить Динамический WEP (802.1x) EAP ETHERNET Измените значение «%s»:  Изменить соединение Изменить соединение Изменить... Редактируется соединение «%s»: «%s» Ошибка редактора: %s Соответствие приоритета на выходе [нет]:  Включить IGMP snooping %s Включить STP %s Включить STP (Spanning Tree Protocol) Включить или выключить устройства WiFi Включить или выключить устройства мобильных сетей WiMax Включить или выключить устройства широкополосных мобильных сетей Включить или выключить системные соединения Введите значение «%s»:  Введите список адресов IPv4 серверов DNS.

Пример: 8.8.8.8, 8.8.4.4
 Введите список адресов IPv6 серверов DNS. Если IPv6 настроен в режиме «auto», серверы будут добавлены в список, полученный в результате автоматической конфигурации. Серверы DNS не могут использоваться в режимах «shared» и «link-local». В остальных режимах они рассматриваются как единственные серверы DNS для заданного соединения.

Пример: 2607:f0d0:1002:51::4, 2607:f0d0:1002:51::1
 Введите список параметров S/390 в формате:
  параметр = <значение>, параметр = <значение>,...
Допустимые параметры: %s
 Введите список параметров агрегации:
  параметр = <значение>, параметр = <значение>,... 
Доступные параметры: %s
«режим»  можно определить с помощью имени или кода:
balance-rr    = 0
active-backup = 1
balance-xor   = 2
broadcast     = 3
802.3ad       = 4
balance-tlb   = 5
balance-alb   = 6

Пример: mode=2,miimon=120
 Введите список разрешений в виде:
 [user:]<пользователь 1>, [user:]<пользователь 2>,...
Записи могут разделяться пробелом или запятой.

Пример: alice bob charlie
 Введите шестнадцатеричные значения.
Допустимый формат:
а)  шестнадцатеричные числа, где одна цифра представляет один байт;
б) список шестнадцатеричных чисел через запятую с дополнительным префиксом 0x/0X или ведущим нулем.

Примеры: ab0455a6ea3a74C2
          ab 4 55 0xa6 ea 3a 74 C2
 Выберите тип соединения:  Определите тип ключей WEP. Возможные значения: 0 (неизвестно), 1 (ключ), 2 (парольная фраза).
 Ошибка преобразования адреса IP4 «0x%X» в текстовый формат Ошибка преобразования адреса IP6 «%s» в текстовый формат Ошибка в файле конфигурации: %s
 Ошибка инициализации данных сертификата: %s Ошибка обновления секрета для %s: %s
 Ошибка:  Ошибка: %s
 Ошибка: %s - несуществующий профиль подключения. Ошибка: отсутствует аргумент %s. Ошибка. %s не является именем параметра или свойства.
 Ошибка: %s. Ошибка: %s: %s. Ошибка: отсутствует аргумент «%s» Ошибка: '%s' — некорректный UID/GID. Ошибка. Недопустимый тип соединения: «%s» Ошибка. Недопустимый режим мониторинга: «%s». Используйте «%s» или  «%s».
 Ошибка. Недопустимое время ожидания («%s») для параметра «%s». Ошибка: соединение '%s' не активно.
 Ошибка. Недопустимая команда «connection»: «%s» Ошибка: недопустимый аргумент «%s» для параметра «%s». Ошибка: '%s': '%s' некорректный  %s %s. Ошибка. «%s»: «%s» не является действительным %s MAC-адресом. Error: '%s': '%s' is not valid; %s  Ошибка «%s». Недействительный «%s»: используйте <%u-%u>. Недопустимое поле «--fields»: «%s». Допускается: %s Ошибка. Необходимо определить «addr». Недопустимое значение «ageing-time»: «%s». Ожидается: <0-1000000>.
 Ошибка. Неверная команда «agent %s». Ошибка. Необходимо определить «apn». Ошибка «arp-interval». Недопустимое значение «%s». Ожидается: <0-%u>.
 Ошибка «autoconnect»: %s. Ошибка: «bt-type»: «%s» не является допустимым типом bluetooth.
 Ошибка «bt-type». Неверное значение «%s». Используйте [%s, %s (%s), %s]. Ошибка «channel». Недопустимое значение «%s». Ожидается: <1-13>.
 Ошибка «channel». Неверное значение «%s». Допустимые значения: <1-13>. Ошибка: 'Показ соединения': %s Ошибка. Неверная команда «dev»: «%s». Ошибка. Необходимо определить «dev». Ошибка: 'dev': '%s' не является ни UUID, ни именем интерфейса.
 Ошибка «dev». «%s» может содержать UUID, имя интерфейса или MAC-адрес. Ошибка «device show»: %s Ошибка: «device status»: %s Ошибка. Неверная команда «device wifi»: «%s». Ошибка «device wifi»: «%s» Ошибка «downdelay». Недопустимое значение «%s». Ожидается: <0-%u>.
 Ошибка «flags». Неверное значение «%s». Допустимые значения: <0-7>. Ошибка «forward-delay». Недопустимое значение «%s». Ожидается: <2-30>.
 Ошибка «general logging»: %s Ошибка общих разрешений: %s Ошибка. Недопустимая команда «general»: «%s». Ошибка «hairpin»: %s Недопустимое значение «hello-time»: «%s». Ожидается: <1-10>.
 Ошибка. Необходимо определить «id». Ошибка. Требуется аргумент «ifname». Ошибка «ifname». Недействительный интерфейс: «%s». Укажите другой интерфейс или «*». Ошибка: 'lacp_rate': '%s' является недействительным ("медленным" или "быстрым").
 Ошибка: 'local': '%s' не является допустимым IP-адресом.
 Ошибка. Необходимо определить «master». Недопустимое значение «max-age»: «%s». Ожидается: <6-40>.
 Ошибка «miimon». Недопустимое значение «%s». Ожидается: <0-%u>.
 Ошибка «mode»: %s Ошибка «mtu». Недействительный MTU: «%s» Ошибка «multicast-snooping»: %s Ошибка «multicast-snooping»: %s
 Ошибка. Недопустимая команда «networking connectivity»: «%s». Ошибка. Недопустимая команда «networking»: «%s». Ошибка. Необходимо определить «nsp». Ошибка. Если задан «parent», необходимо определить «p-key»
 Ошибка «p-key». Недействительный InfiniBand P_KEY: «%s» Ошибка «parent». Недействительное имя интерфейса: «%s» Ошибка «parent». Недействительно без «p-key». Ошибка «primary». Недействительное имя интерфейса: «%s». Ошибка: «primary»: «%s» не является допустимым именем интерфейса.
 Недопустимое значение «priority»: «%s». Ожидается: <0-%d>.
 Ошибка. Недопустимая команда «radio»: «%s». Ошибка: 'source-port-max': '%s' не является допустимым числом <0-65535>.
 Ошибка: 'source-port-min': '%s' не является допустимым числом <0-65535>.
 Ошибка. Необходимо определить «ssid». Ошибка «stp»: %s Ошибка: «stp»: %s.
 Ошибка: 'tap': %s.
 Ошибка. Аргумент «type» является обязательным. Ошибка «updelay». Недопустимое значение «%s». Ожидается: <0-%u>.
 Ошибка. Необходимо определить имя пользователя. Ошибка. Необходимо определить «vpn-type». Ошибка. Отсутствует аргумент <параметр>.<свойство> Ошибка: точка доступа с bssid «%s» не найдена. Ошибка. Ожидается аргумент «%s», а получен «%s». Ошибка: BSSID для подключения к (%s) отличается от bssid аргумента (%s). Ошибка. Не удалось активировать соединение: %s
 Ошибка: соединение '%s' не существует. Ошибка: сбой активации соединения. Ошибка. Не удалось активировать соединение.
 Ошибка: сбой активации соединения: %s Ошибка: сбой активации соединения: %s. Ошибка: сбой удаления соединения: %s Ошибка: не удалось создать объект NMClient: %s. Ошибка: «%s» не является устройством WiFi. Ошибка: не найдено устройство «%s». Ошибка. Не удалось отключить устройство: %s Ошибка: не удалось добавить или активировать новое соединение: неизвестная ошибка Ошибка: NetworkManager не запущен. Ошибка: устройства Wi-Fi не найдены. Ошибка: точка доступа с BSSID «%s» не найдена. Ошибка. Аргументы не определены. Ошибка. Соединение не определено. Ошибка. Интерфейс не определен. Ошибка: не найдена сеть с SSID «%s». Неизвестный объект «%s». Попробуйте выполнить «nmcli help». Ошибка: неизвестный параметр «%s». Попробуйте выполнить «nmcli -help». Ошибка: параметры «--pretty» и «--terse» взаимоисключаемы. Ошибка: параметр «--pretty» указан дважды. Ошибка: параметры «--terse» и «--pretty» взаимоисключаемы. Ошибка. Параметр «--terse» указан дважды. Ошибка: параметр «%s» не является SSID или BSSID. Ошибка: не указан SSID или BSSID. Ошибка: превышено время ожидания (%d с). Ошибка. Непредвиденный аргумент: «%s» Ошибка. Неизвестное соединение: «%s». Ошибка: неизвестный параметр %s. Ошибка: недопустимое значение аргумента bssid «%s». Ошибка. Не удалось удалить неизвестные соединения: %s Ошибка. Идентификатор соединения отсутствует. Ошибка. Соединение не сохранено. Сначала введите «type».
 Ошибка. Недействительное соединение: %s
 Ошибка. Проверка соединения завершилась неудачей: %s
 Ошибка. Дополнительный аргумент не разрешен: «%s». Ошибка. Не удалось изменить %s.%s: %s. Ошибка: не удалось удалить значение из %s.%s: %s. Ошибка. Не удалось изменить значение «%s»: %s
 Ошибка. Не удалось установить свойство «%s»: %s
 Ошибка: отсутствуют поля для параметров «%s». Недопустимый аргумент «%s»: «%s». Может принимать значения on или off. Ошибка <параметр>.<свойство>: «%s». Ошибка. Недопустимый тип соединения: %s.
 Ошибка. Недопустимый тип соединения: %s. Ошибка. Недопустимый дополнительный аргумент: «%s». Ошибка: неверный адрес шлюза '%s'
 Ошибка. Недопустимый параметр «%s»: %s. Ошибка. Недопустимое свойство «%s»: %s. Ошибка. Недействительное свойство: %s
 Недопустимое свойство %s или неверное имя параметра.
 Ошибка. Недопустимый аргумент «%s». Допускается: [%s]
 Ошибка. Недопустимое значение параметра: %s
 Ошибка: отсутствует аргумент параметра «%s». Ошибка. Нет определения свойства «%s»
 Ошибка: версии nmcli (%s) и NetworkManager (%s) не совпадают. Для принудительного запуска используйте --nocheck, но результаты не предсказуемы. Ошибка: нет активного соединения. Ошибка. Аргумент не задан. Допускается: [%s]
 Ошибка. Параметр не выбран. Допускается: [%s]
 Ошибка: Найдены не все активные соединения. Ошибка. Допускается только одно значение: «id», «uuid» или «path». Ошибка. Допустимые поля: %s Ошибка: openconnect сбой сигнала %d
 Ошибка: не удалось openconnect со статусом %d
 Ошибка: openconnect не удалось: %s
 Ошибка: инициализация агента polkit не удалась: %s Ошибка свойства %s
 Ошибка save-confirmation: %s
 Ошибка: инициализация секретного агента не удалось Ошибка. Обязательное значение «%s» не может быть удалено.
 Ошибка status-line: %s
 Ошибка: неизвестный параметр: %s
 Ошибка. Неизвестный параметр: %s
 Ошибка. Значение «%s» является обязательным. Ошибка: недопустимое значение аргумента wep-key-type «%s»; используйте «key» или «phrase». Ethernet Проводное соединение %d Выйти немедленно, если NetworkManager не запущен или выполняет подключение ТРЕБУЕТСЯ ПРОШИВКА ВЕРСИЯ ПРОШИВКИ FQDN для отправки на сервер DHCP ЧАСТОТА Не удалось расшифровать личный ключ PKCS#8. Не удалось расшифровать сертификат. Не удалось расшифровать личный ключ. Не удалось расшифровать личный ключ: %d. Не удалось расшифровать закрытый ключ: %s (%s) Не удалось расшифровать личный ключ: слишком большой объем расшифрованных данных. Не удалось расшифровать личный ключ: непредвиденная длина заполнения. Не удалось зашифровать данные: %s (%s) Не удалось зашифровать: %d. Не удалось завершить расшифровку личного ключа: %d. Не удалось найти закрывающий тег PKCS#8 — «%s». Не удалось найти открывающий тег PKCS#8. Не удалось инициализировать контекст MD5: %d. Не удалось инициализировать криптографический модуль. Не удалось инициализировать криптографический модуль: %d. Не удалось инициализировать слот шифра декодирования. Не удалось инициализировать контекст расшифровки. Не удалось инициализировать слот шифра декодирования. Не удалось инициализировать контекст шифрования. Не удалось прочитать конфигурацию: %s
 Не удалось зарегистрироваться в запрошенной сети Не удалось выбрать указанный APN Не удалось задать ВИ расшифровки. Не удалось задать ВИ расшифровки. Не удалось задать симметричный ключ расшифровки. Не удалось задать симметричный ключ для шифрования. Задержка переадресации Forward delay [15]:  ШЛЮЗ ОБЩИЕ ГРУППА Требуется PIN-код к SIM-карте GSM-модема Требуется PUK-код к SIM-карте GSM-модема Не вставлена SIM-карта GSM-модема Неверная SIM-карта GSM модема Соединение GSM GVRP,  Шлюз АППАРАТНЫЙ АДРЕС Режим разворота пакетов Время приветствия Hello time [2]:  16-ричный идентификатор интерфейса Скрыть Имя узла ID INFINIBAND Конфигурация IP не может быть зарезервирована (отсутствует доступный адрес, истекло время ожидания и т.д.) IP_ИНТЕРФЕЙСА IP4 IP6 КОНФИГУРАЦИЯ IPv4 КОНФИГУРАЦИЯ IPv6 ВИ включает не шестнадцатеричные цифры. Длина ВИ должна быть равна чётному количеству байт. Идентификация Отсутствие необходимого типа VPN в списке может говорить о том, что в системе не установлен соответствующий модуль VPN. Игнорировать Неизвестный домен журналов «%s» в файле конфигурации. Пропускается...
 Неизвестные домены журналов «%s» в строке команды. Пропускаются...
 InfiniBand InfiniBand P_Key не содержит имя родительского интерфеса Соединение InfiniBand Подключение InfiniBand %d Устройство InfiniBand не поддерживает режим соединения Режим InfiniBand передачи Инфраструктура Соответствие приоритета на входе [нет]:  Имя интерфейса [*]:  Интерфейс:  Недопустимая длина ВИ (минимально %d). Недопустимая длина ВИ (минимально %zd). Недопустимый параметр конфигурации «%s». Допускается: [%s]
 Неверный параметр. Используйте --help для вывода списка возможных параметров. Недопустимый параметр verify: «%s»
 Конфигурация JSON Ключ Скорость LACP (slow/fast) [slow]:  LEAP УРОВЕНЬ LOOSE_BINDING,  Задержка разрыва соединения Мониторинг соединения Задержка установки соединения Локальная связь Список модулей, разделённых «,» Локальный адрес [нету]:  Домены журналов должны быть перечислены через запятую и могут содержать любую комбинацию [%s]. Уровень журнала: одно значение из [%s] MAC [нет]:  Время действия MAC-адреса [300]:  Режим MACVLAN:  MASTER-PATH MII (рекомендуется) РЕЖИМ MSCHAP MSCHAPv2 MTU MTU [авто]:  MVRP,  Сделать все предупреждения критическими Некорректный файл PEM: DEK-Info не является вторым тегом. Некорректный файл PEM: Proc-Type не является первым тегом. Некорректный файл PEM: неверный формат ВИ в теге DEK-Info. Некорректный файл PEM: в теге DEK-Info не найден ВИ. Некорректный файл PEM: неизвестный тег Proc-Type «%s». Некорректный файл PEM: неизвестный шифр личного ключа «%s». Вручную Мастер:  Максимальный срок Max age [20]:  Максимальный порт источника [0]:  Метрика Минимальный порт источника [0]:  Мобильное Мобильное соединение %d Пароль мобильной сети Режим Режим %s Модем отсоединился или больше недоступен Не удалось инициализировать модем Сейчас модем готов и доступен ModemManager недоступен Изменить подключения доступные всем пользователям Изменить постоянное имя узла компьютера Редактировать личные настройки подключения Контроль активации соединения (нажмите любую клавишу для продолжения)
 Частота мониторинга При определении родительского интерфейса необходимо определить P_Key Н/Д ИМЯ СЕТЬ ПОД УПРАВЛЕНИЕМ NM NSP Возможно для устройства требуется дополнительное программное обеспечение Имя сети Регистрация сети запрещена Истекло время ожидания регистрации сети TUI NetworkManager Активные профили NetworkManager Профили соединения NetworkManager NetworkManager не работает. Ведение журналов NetworkManager NetworkManager отслеживает все сетевые соединения и автоматически
выбирает для использования наиболее подходящее соединение. А также позволяет
пользователю задать беспроводным картам точки доступа, с которыми они 
должны ассоциироваться. NetworkManager должен отключить сети Права доступа NetworkManager Состояние NetworkManager NetworkManager перешёл в спящий режим Сеть Не использовать эту сеть для текущего маршрута Новое соединение Новая соединение было активано и было поставлено в очередь Следующий переход Невозможно установить несущую Нет дополнительных маршрутов. Нет гудка Причина не указана Нет соединения «%s». Не выполнять поиск сетей OK OLPC Mesh Канал OLPC mesh [1]:  ПАРАМЕТР %d дополнительный маршрут %d дополнительных маршрута %d дополнительных маршрутов Открытая система При открытии %s произошла ошибка: %s
 Параметр «--terse» требует определённых значений «--fields», но не «%s» Параметр «--terse» требует указания «--fields» Соединение PAN PAP PCI Сертификат PEM не имеет тега окончания «%s». Сертификат PEM не имеет тега начала «%s». Файл ключа PEM не содержит завершающий тег «%s». РАЗРЕШЕНИЕ PIN PIN-код не прошёл проверку Для использования мобильного устройства требуется PIN-код Требуется PIN КОНФИГУРАЦИЯ PPP Сбой PPP Служба PPP отключена Не удалось запустить службу PPP PPPoE Соединение PPPoE Пользователь PPPoE:  ПРОДУКТ P_KEY [нет]:  Родительский Родительское устройство [нету]:  Родительский интерфейс [нет]:  Пароль Пароль [нет]:  Пароль должен быть в UTF-8 Пароль:  Для доступа к беспроводной сети «%s» требуются пароли или ключи шифрования. Стоимость пути Выберите вариант Префикс Нажмите <Enter>, чтобы завершить процедуру добавления адресов.
 Основной Показать версию NetworkManager и выйти Приоритет Неизвестный шифр личного ключа «%s». Пароль секретного ключа Имя профиля Имя свойства?  Перевести NetworkManager в режим сна или пробудить его (должно использоваться только системой управления питанием) Выйти СКОРОСТЬ ТОЛЬКО ДЛЯ ЧТЕНИЯ ПРИЧИНА УПОРЯДОЧИТЬ_ЗАГОЛОВКИ,  МАРШРУТ ФЛАГИ RSN RSSI ВЫПОЛНЯЕТСЯ Переключатели Удалить Требовать 128-битное шифрование Соединение требует адресацию IPv4 Соединение требует адресацию IPv6 Циклический Маршрутизация БЕЗОПАСНОСТЬ СИГНАЛ Неверный PIN-код SIM-карты СЛЕЙВЫ SPEC-OBJECT СКОРОСТЬ SSID Длина SSID должна быть в пределах от 1 до 32 байт. SSID или BSSID:  SSID-HEX SSID:  ЗАПУСК СОСТОЯНИЕ Приоритет STP [32768]:  Вы собираетесь сохранить соединение с установленным параметром «autoconnect=yes», что может привести к его немедленной активации.
Сохранить? %s Поиск доменов Не предоставлена требуемая секретная информация Защита Выберите тип создаваемого соединения. Выберите тип добавляемого подчиненного соединения. Выбрать... Служба Служба [нет]:  Измените имя узла Изменить имя узла на «%s» Измените имя узла Параметр «%s» не определен в соединении.
 Установить имя?  Общее Общий ключ Сбой службы общего подключения Не удалось запустить службу общего подключения Показать Показать пароль Слейвы Укажите расположение PID файла Расположение файла состояния Состояние устройств Успешно Система Системная политика запрещает управление подключениями. Системная политика запрещает включение и выключение WiFi устройств Системная политика запрещает включение или выключение устройств мобильной связи WiMAX Системная политика запрещает включение или выключение устройств широкополосной мобильной связи Системная политика препятствует включению или отключению сетевой подсистемы Системная политика запрещает изменение настроек для всех пользователей Системная политика запрещает изменение персональных настроек сети Системная политика запрещает изменение постоянного имени узла компьютера Системная политика не разрешает переводить NetworkManager в режим сна или пробуждать его Системная политика запрещает совместное использование соединений в закрытой сети WiFi Системная политика запрещает совместное использование соединений в открытой сети WiFi АГРЕГАЦИЯ (TEAM) ПОРТ АГРЕГАЦИИ (TEAM) МЕТКА ВРЕМЕНИ ДЕЙСТВ. МЕТКА ВРЕМЕНИ TKIP Режим устройства TUN TX-POW ТИП Tap %s Агрегированное (Team) Конфигурация JSON агрегированного соединения (team) [нет]:  Агрегированное (Team) соединение Агрегированное (Team) соединение %d Время ожидания соединения по Bluetooth истекло или произошла ошибка Конфигурация IP не актуальна Не удалось обнаружить сеть Wi-Fi Профиль соединения был удален из другого клиента. Чтобы его восстановить, введите «save».
 Профиль соединения был удален из другого клиента. Чтобы его восстановить, введите «save».
 Это не соединение не относится к InfiniBand. Устройство не позволяет чтение конфигурации Управление родительского устройства изменилось Устройство извлечено Устройство потеряло используемое соединение Существующее соединение устройства принято Родительское (корневое) устройство изменилось Во время набора номера произошла ошибка Истекло время ожидания набора номера Эта ошибка не может быть исправлена автоматически.
 Ожидается начало ответа Интервал между проверками соединения (в секундах) Линия занята Режим работы устройства не соответствует режиму соединения Не удалось обнаружить модем Доступен заявитель Время ожидания соединения в секундах (по умолчанию 30) Транспортный режим Режим передачи %s Туннельный режим:  Для просмотра описания свойства введите «describe [<параметр>.<свойство>]». Для просмотра доступных команд введите «help» или «?» UDI USB ИМЯ_ПОЛЬЗОВАТЕЛЯ UUID Не удалось добавить новое соединение: %s Не удалось удалить соединение: %s Не удалось определить тип личного ключа. Не удалось сохранить соединение: %s Не удалось установить имя узла: %s Непредвиденный объём данных после шифрования. Неизвестно Неизвестный аргумент команды: «%s»
 Неизвестная команда: «%s»
 Неизвестная ошибка Неизвестный домен журналирования «%s» Неизвестный уровень журналирования «%s» Неизвестный параметр: %s
 Использование Использование: nmcli agent all { help }

Запускается nmcli в качестве как NetworkManager секрет и polkit агент

 Использование: nmcli agent polkit { help }

Регистрирует nmcli как действие polkit для сеанса пользователя.
Когда демон polkit требуется разрешение, nmcli запрашивает у пользователя и дает
ответ обратно polkit.

 Использование: nmcli agent secret { help }

Работает nmcli в качестве секретного агента NetworkManager. Когда NetworkManager требуется
пароль, он запрашивает зарегистрированных агентов для него. Эта команда сохраняет nmcli работающим
и если требуется пароль запрашивает его у пользователя

 Использование: nmcli agent { КОМАНДА | help }

КОМАНДА := { secret | polkit | all }

 Использование: nmcli connection clone { АРГУМЕНТЫ | help }

АРГУМЕНТЫ := [--temporary] [id | uuid | path] <ID> <новое имя>

Клонировать существующий профиль подключения. Вновь созданное соединение будет
точной копией <ID>, за исключением UUID (который будет сгенерирован) и
id (при условии аргумента <новое имя>).

 Формат: nmcli connection delete { АРГУМЕНТЫ | help }

АРГУМЕНТЫ := [id | uuid | path] <ID>

Удаление профиля соединения.
Профиль определяется по имени, UUID или пути D-Bus.

 Использование: nmcli connection dows { АРГУМЕНТЫ | help }

АРГУМЕНТЫ := [id | uuid | path | apath] <ID> ...

Отключение соединения с устройства (без предупреждения устройства от 
автоматической активации). Профиль деактивировируется идентифицируя своё имя,
UUID или D-Bus путь.

 Формат: nmcli connection edit { АРГУМЕНТЫ | help }

АРГУМЕНТЫ := [id | uuid | путь] <ID>

Изменение существующего профиля соединения.
Профиль определяется по имени, UUID или пути D-Bus.

АРГУМЕНТЫ := [type <новый_тип>] [con-name <новое_имя>]

Добавление нового профиля соединения в интерактивном редакторе.

 Формат: nmcli connection load { АРГУМЕНТЫ | help }

АРГУМЕНТЫ := <filename> [<файл>...]

Загружает файлы соединений с диска. 
Обычно используется после изменения файлов соединений вручную.

 Использование: nmcli connection modify { АРГУМЕНТЫ | help }

АРГУМЕНТЫ := [id | uuid | path] <ID> ([+|-]<настройка>.<свойство> <значение>)+

Изменение одного или нескольких свойств соединения в профиле.
Профиль идентифицируется своим именем, UUID или путь D-Bus. Для многозначных
свойств, которые могут использовать дополнительный '+' или '-' префикс к имени свойства.
Знак '+' позволяет добавлять элементы, а не перезаписывать целое значение.
Знак '-' позволяет удалять выбранные элементы вместо целого значения.

Examples:
nmcli con mod home-wifi wifi.ssid rakosnicek
nmcli con mod em1-1 ipv4.method manual ipv4.addr "192.168.1.2/24, 10.10.1.5/8"
nmcli con mod em1-1 +ipv4.dns 8.8.4.4
nmcli con mod em1-1 -ipv4.dns 1
nmcli con mod em1-1 -ipv6.addr "abbe::cafe/56"
nmcli con mod bond0 +bond.options mii=500
nmcli con mod bond0 -bond.options downdelay

 Формат: nmcli connection reload { help }

Перезагрузка файлов соединений с диска.

 Использование: nmcli connection show { АРГУМЕНТЫ | help }

АРГУМЕНТЫ: = [--active] [--order <порядок спецификации>]

Список в памяти и на диске профили соединений, некоторые из которых также может быть
активным, если устройство использует этот профиль подключения. Без параметров, все
профили перечислены. При указании опции --active, только активный
профили показаны. --order позволяет сортировать пользовательские соединения (см страницу руководства).

АРГУМЕНТЫ: = [--active] [ID | UUID | path | apath] <ID> ...

Показать детали для указанных соединений. По умолчанию отображаются, как статические конфигурации,
так и активные подключения данных. Можно фильтровать вывод
используя глобальную опцию "--fields". Обратитесь к странице руководства для получения дополнительной информации.
При указании параметра "--active", только активные профили принимаются в
cчет. Используйте глобальный параметр "--show-secrets" чтобы раскрыть тайны связанные с ним, а также.
 Использование: nmcli connection up { АРГУМЕНТЫ | help }

АРГУМЕНТЫ := [id | uuid | path] <ID> [ifname <имя_интерфейса>] [ap <BSSID>] [nsp <имя>] [passwd-file <файл с паролем>]

Активируйте соединение на устройстве. Профиль для активации идентифицируется своим
именем, UUID или путь D-Bus.

ARGUMENTS := ifname <имя_интерфейса> [ap <BSSID>] [nsp <name>] [passwd-file <файл с паролем>]

Активация устройства с подключением. Профиль подключения выбран
автоматически NetworkManager.

ifname (имя_интерфейса) - определяет устройство для активного соединения на что-либо
ар - задает точку доступа для подключения к чему-либо (действительны только для Wi-Fi)
NSP - указывает NSP для подключения к чему-либо (действителен только для WiMAX)
PASSWD-файл - файл с паролем(ями), необходимый для активации соединения

 Формат: nmcli device connect { АРГУМЕНТЫ | help }

АРГУМЕНТЫ := <ifname>

Подключение устройства.
NetworkManager выберет подходящее соединение,
принимая во внимание соединения без включенных функций
автоматического запуска.

 Формат: nmcli device show { АРГУМЕНТЫ | help }

АРГУМЕНТЫ := [<интерфейс>]

Показывает информацию обо всех или
отдельно выбранном устройстве.

 Формат: nmcli device status { help }

Возвращает статус всех устройств.
По умолчанию будут показаны столбцы:
 УСТРОЙСТВО     -  имя интерфейса
 ТИП       -  тип устройства
 СТАТУС      -  статус устройства
 СОЕДИНЕНИЕ -  соединение устройства
Параметр «--fields» позволяет изменить показанные столбцы.
«status» используется по умолчанию, то есть  «nmcli gen» эквивалентно «nmcli gen status».

 Формат: nmcli general hostname { АРГУМЕНТЫ | help }

АРГУМЕНТЫ := [<имя_узла>]

Просмотр и изменение имени узла.
Если аргументы не указаны, команда вернет текущее имя узла.
В противном случае имя узла будет изменено.

 Формат: nmcli general logging { АРГУМЕНТЫ | help }

АРГУМЕНТЫ := [level <уровень_ведения_журналов>] [domains <домены>]

Просмотр и изменение доменов и уровня ведения журналов.
Если аргументы не указаны, команда вернет текущие настройки.
Полный список доменов можно найти на справочной странице команды.

 Формат: nmcli general permissions { help }

Возвращает разрешения для закрытых операций.

 Формат: nmcli general status { help }

Возвращает статус NetworkManager.
«status» используется по умолчанию, то есть «nmcli gen» эквивалентно «nmcli gen status».

 Формат: nmcli general { КОМАНДА | help }

КОМАНДА := { status | hostname | permissions | logging }

  status

  hostname [<узел>]

  permissions

  logging [level <уровень>] [domains <домены>]

 Формат: nmcli networking connectivity { АРГУМЕНТЫ | help }

АРГУМЕНТЫ := [check]

Возвращает статус подключения к сети.
Дополнительный аргумент «check» повторно проверяет подключение.

 Формат: nmcli networking off { help }

Отключает сетевые функции.

 Формат: nmcli networking on { help }

Включает сетевые функции.

 Формат: nmcli networking { КОМАНДА | help }

КОМАНДА := { [ on | off | connectivity ] }

  on

  off

  connectivity [check]

 Формат: nmcli radio all { АРГУМЕНТЫ | help }

АРГУМЕНТЫ := [on | off]

Просмотр и изменение статуса всех переключателей.

 Формат: nmcli radio wifi { АРГУМЕНТЫ | help }

АРГУМЕНТЫ := [on | off]

Просмотр и изменение статуса переключателя Wi-Fi.

 Формат: nmcli radio wwan { АРГУМЕНТЫ | help }

АРГУМЕНТЫ := [on | off]

Просмотр и изменение статуса переключателя мобильного соединения.

 Пользователь Пользователь [нет]:  ЗНАЧЕНИЕ ПРОИЗВОДИТЕЛЬ ВЕРСИЯ VLAN Соединение VLAN Подключение VLAN %d Флаги VLAN (<0-7>) [нет]:  VLAN ID Родительское устройство VLAN или подключение UUID:  VPN VPN подключён Подключение VPN Подключение VPN (получение конфигурации IP) Подключение VPN (требуется аутентификация) Подключение VPN (подготовка) VPN-соединение Соединение VPN %d VPN-соединение (имя, UUID или путь):  Сбой подключения VPN VPN-соединение успешно установлено (адрес действующего D-Bus: %s)
 VPN отключён Тип VPN:  СТАТУС VPN VXLAN Доступные типы подключений: %s
 Проверьте соединение: %s
 Проверьте параметр «%s»: %s
 WEP 128-битный ключ WEP 40/128-битный ключ WEP (Hex или ASCII)) Индекс WEP 1 (по умолчанию) 2 3 4 Ключ WEP определен как «%s»
 WI-FI WIFI WIFI-HW СВОЙСТВА WIFI WIMAX WIMAX-HW СВОЙСТВА WIMAX WINS СВОЙСТВА ПРОВОДНОГО СОЕДИНЕНИЯ WPA WPA и WPA2 Enterprise WPA и WPA2 Personal ФЛАГИ WPA WPA1 WPA2 WWAN Переключатель WWAN WWAN-HW Ожидать успешного соединения в NetworkManager. Предупреждение. Изменения вступят в силу, только когда «%s» будет равно 1 (включено)

 Предупреждение. При редактировании соединения «%s» аргумент «con-name» будет пропущен.
 Предупреждение. При редактировании соединения «%s» аргумент «type» будет пропущен.
 Внимание: master='%s' не относится к существующему профилю.
 Предупреждение: версии nmcli (%s) и NetworkManager (%s) не совпадают. Используйте --nocheck для подавления этого предупреждения.
 Внимание: пароль для '%s' не дан в 'passwd-file' и nmcli не может спросить без опции' --ask'.
 Wi-Fi Автоматически Клиент Соединение Wi-Fi %d Режим Wi-Fi Переключатель Wi-Fi Список сканирования Wi-Fi Нет WiMAX Имя WiMAX NSP  Проводное Аутентификация проводного соединения 802.1X Проводное соединение Проводное соединение %d При записи в %s произошла ошибка: %s
 XOR Вы можете изменить следующие свойства: %s
 Разрешается изменить следующие параметры: %s
 Вы должны быть суперпользователем для запуска %s
 ЗОНА [«%s» устанавливает значения]
 [описание свойства NM] [описание в nmcli] activate [<интерфейс>] [/<ap>|<nsp>]  :: активация соединения

Эта команда аткивирует выбранное соединение.

Параметры:
<интерфейс>    - имя интерфейса для активации соединения
/<ap>|<nsp> - AP (Wi-Fi) или NSP (WiMAX) (если интерфейс не задан, в начале строки надо добавить /)
 включено активация add [<значение>]  :: добавление нового значения свойства

Эта команда добавляет новое значение свойства к списку существующих значений. Если же свойство имеет единственное значение, оно будет переопределено (что равносильно команде «set»).
 объявление,  под управлением агента,  всегда спящий подлинный автоматически back  :: возврат к предыдущему меню

 сумма процентных частей пропускной способности должна быть равна 100%% bluetooth агрегированное (Bond) мост Слейв соединения типа «мост» байт change  :: изменение текущего значения

Показывает значение и позволяет его отредактировать.
 подключено подключен (локально) подключен (на узле) подключение подключение (проверка IP-подключения) соединение (настройка) подключение (получение конфигурации IP) подключение (требуется аутентификация) подключение (подготовка) подключение (запуск второстепенных соединений) соединение сбой соединения %s %d выключен деактивация по умолчанию не получается добавить маршрут по умолчанию (NetworkManager обработает это самостоятельно) describe  :: описание свойства

Возвращает описание свойства. Полный список параметров и их значений можно найти на справочной странице nm-settings(5).
 describe [<параметр>.<свойство>]  :: описание свойства

Возвращает описание свойства. Полный список параметров и их значений можно найти на справочной странице nm-settings(5).
 устройство «%s» несовместимо с соединением «%s» отключен отключено отключение неизвестно, как получить значение свойства недопустимый элемент включен включено,  eth0 ethernet не удалось прочитать PASSWD-файл '%s': %s поле «%s» должно быть единственным недопустимый формат имени файла (%s) неверные флаги неверные флаги флаги не установлены или установлены неверно полностью goto <параметр>[.<свойство>] | <свойство>  :: выбор свойства параметр для редактирования

Команда открывает параметр и его свойство для редактирования.

Примеры: nmcli> goto connection
          nmcli connection> goto secondaries
          nmcli> goto ipv4.addresses
 должно соответствовать свойству «%s» для PKCS#12 help/? [<команда>]  :: справка по командам nmcli

 help/? [<команда>]  :: справка по командам nmcli

 Индекс «%d» выходит за пределы диапазона <0-%d> Индекс «%d» выходит за пределы диапазона <0-%d> Недопустимый индекс «%s». недопустимый параметр «%s» или его значение «%s» неверный IP: %s неверный IPv4 адрес '%s' неверный IPv6 адрес '%s' неверные секреты VPN неверный формат сертификата неверное поле «%s»; допустимые поля: %s Неправильное поле '%s'; Допустимые поля: %s и %s или %s, %s неверная метрика '%s' Недопустимый параметр «%s» недопустимый параметр «%s» или его значение «%s» некорректный префикс '%s'; разрешается <1-%d> неверная карта приоритетов '%s' неверный маршрут: %s неверное имя параметра в 'password' записи '%s' недопустимый MAC-адрес ограничено %s %s macvlan Обязательный параметр «%s» не определен. мс отсутствует двоеточие в 'password' записи '%s' отсутствует точка в 'password' записи '%s' отсутствует имя файла имя не определено, попробуйте выбрать значение из [%s] пропущен параметр мобильное широкополосное должен содержать 8 чисел, разделенных запятой недействительное подключение или устройство не определено никогда новое имя узла nmcli принимает имя файла конфигурации или данные настройки JSON напрямую.

Примеры: set team.config { "device": "team0", "runner": {"name": "roundrobin"}, "ports": {"eth1": {}, "eth2": {}} }
          set team.config /etc/my-team.conf
 nmcli успешно зарегистрирован как тайный агент NetworkManager.
 nmcli успешно зарегистрирован в качестве агента polkit
 утилита nmcli, версия %s
 нет нет (угадали) на устройстве «%s» нет активных соединений на устройстве нет активных соединений не найдено устройство для соединения «%s» нечего удалять нет приоритета для удаления нет верных секретов VPN нет недействительное имя интерфейса не требуется,  не сохранено,  выкл. вкл. Может быть определено только одно из двух — «%s» или «%s». не абсолютный путь (%s) портал подготовка print [all]  :: вывод информации о параметрах и соединениях

Возвращает отдельное свойство или данные для целого соединения.

Пример: nmcli ipv4> print all
 print [свойство|параметр|соединение]  :: вывод свойства (параметра, соединения)

Если аргумент не указан, возвращает значение свойства. Укажите параметр или соединение, чтобы получить соответствующую информацию.
 Приоритет '%s' неверный (<0-%ld>) Не указан пароль частного ключа неверное свойство неверное свойство (отключено) пустое свойство неверное свойство свойство не определено Не определено свойство и «%s:%s» свойство не определено пустое значение «%s» или свойство не определено (>64) quit  :: выход из nmcli

Если изменения соединения не сохранены, появится окно подтверждения.
 remove <параметр>[.<свойство>]  :: удаление или сброс параметра

Эта команда удаляет параметр соединения, а если задано свойство — 
будет восстановлено его исходное значение.

Примеры: nmcli> remove wifi-sec
          nmcli> remove eth.mtu
 требует определения «%s» или «%s» требует наличия «%s» в определении соединения требует определения свойства «%s» выполняется сек. set [<параметр>.<свойство> <значение>]  :: установка свойства

Эта команда определяет значение свойства.

Пример: nmcli> set con.id My connection
 set [<значение>]  :: установка нового значения

Эта команда определяет новое значение свойства.
 для определения этого свойства необходимо, чтобы «%s» имело ненулевое значение запущено запускается сумма не равна 100% агрегированное (team) Слейв агрегированного соединения (team) teamd не отвечает служба VPN не была запущена вовремя не удалось запустить службу VPN служба VPN вернула недопустимую конфигурацию служба VPN внезапно прекратила работу основное соединение с сетью было разорвано время ожидания соединения истекло соединение удалено свойство не может быть изменено второй компонент маршрута ('%s') не является адресом узла или метрикой пользователь был отключён это свойство не может быть пустым для '%s=%s' это свойство не может использоваться для «%s=%s» недоступен неизвестно неизвестное устройство '%s'. неизвестная причина не настроенно используйте «goto <параметр>» или «describe <параметр>.<свойство>»
 используйте «goto <параметр>» или «set <параметр>.<свойство>»
 значение «%d» выходит за пределы допустимого диапазона: <%d-%d> разрешение,  да да (угадали) 