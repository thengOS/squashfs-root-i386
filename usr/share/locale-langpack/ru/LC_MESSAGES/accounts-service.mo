��          t      �         C     .   U  7   �  %   �     �     �       #   '  Q   K     �  �  �  �   �  w   )  �   �  M   &  K   t  ,   �  Q   �  U   ?  �   �  <   E               	          
                                   Authentication is required to change the login screen configuration Authentication is required to change user data Authentication is required to change your own user data Change the login screen configuration Change your own user data Enable debugging code Manage user accounts Output version information and exit Provides D-Bus interfaces for querying and manipulating
user account information. Replace existing instance Project-Id-Version: accounts service
Report-Msgid-Bugs-To: http://bugs.freedesktop.org/
POT-Creation-Date: 2013-01-17 09:48-0500
PO-Revision-Date: 2011-07-27 08:49+0000
Last-Translator: Yuri Kozlov <yuray@komyakino.ru>
Language-Team: Russian (http://www.transifex.com/projects/p/freedesktop/language/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:01+0000
X-Generator: Launchpad (build 18115)
Language: ru
 Для изменения настроек экрана входа в систему требуется аутентификация Для изменения пользовательских данных требуется аутентификация Для изменения личных пользовательских данных требуется аутентификация Изменить настройки экрана входа в систему Изменить личные пользовательские данные Включить отладочный код Управление учётными записями пользователей Вывод информации о версии и выход из программы Предоставляет интерфейс D-Bus для опроса и изменения
информации об учётных данных пользователей. Заменить существующий экземпляр 