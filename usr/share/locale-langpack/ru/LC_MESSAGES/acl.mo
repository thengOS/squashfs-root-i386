��    5      �  G   l      �     �     �     �     �     �  )   �  )   %     O  �   d  7  0  �  h  B   J
  l  �
  �   �  Z     '   �  '        *  $   H     m     �  &   �  2   �  3   �  /   2  /   b  =   �     �  %   �  2        D  $   \  &   �  +   �  '   �  ,   �  &   )  '   P  *   x  +   �     �     �     �          #     :  &   X          �     �     �     �  �  �     �     �     �     �     �  7   �  8   3     l    �  �  �  �  v  f   p    �  �   �  9   �  X   �  \   (   (   �   >   �      �   <   !  O   I!  J   �!  S   �!  W   8"  [   �"  w   �"  +   d#  4   �#  Q   �#  (   $  8   @$  M   y$  X   �$  V    %  a   w%  Q   �%  Z   +&  :   �&  O   �&  %   '  &   7'  '   ^'  $   �'  ?   �'  7   �'  [   #(     (  "   �(  ,   �(  3   �(  4    )                 	              
   5                                 -           !          1      &             *      3   +      $                    )       "              %   #         ,                         '   /   .         4          0   2   (    	%s -B pathname...
 	%s -D pathname...
 	%s -R pathname...
 	%s -b acl dacl pathname...
 	%s -d dacl pathname...
 	%s -l pathname...	[not IRIX compatible]
 	%s -r pathname...	[not IRIX compatible]
 	%s acl pathname...
       --set=acl           set the ACL of file(s), replacing the current ACL
      --set-file=file     read ACL entries to set from file
      --mask              do recalculate the effective rights mask
   -R, --recursive         recurse into subdirectories
  -L, --logical           logical walk, follow symbolic links
  -P, --physical          physical walk, do not follow symbolic links
      --restore=file      restore ACLs (inverse of `getfacl -R')
      --test              test mode (ACLs are not modified)
   -a,  --access           display the file access control list only
  -d, --default           display the default access control list only
  -c, --omit-header       do not display the comment header
  -e, --all-effective     print all effective rights
  -E, --no-effective      print no effective rights
  -s, --skip-base         skip files that only have the base entries
  -R, --recursive         recurse into subdirectories
  -L, --logical           logical walk, follow symbolic links
  -P, --physical          physical walk, do not follow symbolic links
  -t, --tabular           use tabular output format
  -n, --numeric           print numeric user/group identifiers
  -p, --absolute-names    don't strip leading '/' in pathnames
   -d, --default           display the default access control list
   -m, --modify=acl        modify the current ACL(s) of file(s)
  -M, --modify-file=file  read ACL entries to modify from file
  -x, --remove=acl        remove entries from the ACL(s) of file(s)
  -X, --remove-file=file  read ACL entries to remove from file
  -b, --remove-all        remove all extended ACL entries
  -k, --remove-default    remove the default ACL
   -n, --no-mask           don't recalculate the effective rights mask
  -d, --default           operations apply to the default ACL
   -v, --version           print version and exit
  -h, --help              this help text
 %s %s -- get file access control lists
 %s %s -- set file access control lists
 %s: %s in line %d of file %s
 %s: %s in line %d of standard input
 %s: %s: %s in line %d
 %s: %s: Cannot change mode: %s
 %s: %s: Cannot change owner/group: %s
 %s: %s: Malformed access ACL `%s': %s at entry %d
 %s: %s: Malformed default ACL `%s': %s at entry %d
 %s: %s: No filename found in line %d, aborting
 %s: %s: Only directories can have default ACLs
 %s: No filename found in line %d of standard input, aborting
 %s: Option -%c incomplete
 %s: Option -%c: %s near character %d
 %s: Removing leading '/' from absolute path names
 %s: Standard input: %s
 %s: access ACL '%s': %s at entry %d
 %s: cannot get access ACL on '%s': %s
 %s: cannot get access ACL text on '%s': %s
 %s: cannot get default ACL on '%s': %s
 %s: cannot get default ACL text on '%s': %s
 %s: cannot set access acl on "%s": %s
 %s: cannot set default acl on "%s": %s
 %s: error removing access acl on "%s": %s
 %s: error removing default acl on "%s": %s
 %s: malloc failed: %s
 %s: opendir failed: %s
 Duplicate entries Invalid entry type Missing or wrong entry Multiple entries of same type Try `%s --help' for more information.
 Usage:
 Usage: %s %s
 Usage: %s [-%s] file ...
 preserving permissions for %s setting permissions for %s Project-Id-Version: acl
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-07 11:10+0000
PO-Revision-Date: 2010-04-16 19:07+0000
Last-Translator: Andrey "ХолоD" Bobylev <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:54+0000
X-Generator: Launchpad (build 18115)
 	%s -B путь...
 	%s -D путь...
 	%s -R путь...
 	%s -b acl dacl путь...
 	%s -d dacl путь...
 	%s -l путь...	[не совместимо с IRIX]
 	%s -r путь... 	[не совместимо с IRIX]
 	%s acl путь...
       --set=acl установить СКД файлов, заместив текущий СКД
      --set-file=file прочитать записи СКД для установки из файла
      --mask пересчитать маску эффективных прав
   -R, --recursive рекурсивно в подкаталоги
  -L, --logical логический проход, следовать символическим ссылкам
  -P, --physical физический проход, не следовать символическим ссылкам
      --restore=file восстановить СКД (обратное действие к `getfacl -R')
      --test тестовый режим (СКД не изменяются)
   -a,  --access           показать только список контроля доступа файла
  -d, --default           показать только список контроля доступа по-умолчанию
  -c, --omit-header       не показывать заголовок с комментариями
  -e, --all-effective     вывести все действующие права
  -E, --no-effective      вывести недействующие права
  -s, --skip-base         пропускать файлы, имеющие только базовые записи
  -R, --recursive         включать подкаталоги
  -L, --logical           логическое прохождение, следовать по символьным ссылкам
  -P, --physical          физическое прохождение, не следовать по символьным ссылкам
  -t, --tabular           использовать формат вывода с табуляцией
  -n, --numeric           выводить цифровые идентификаторы пользователя/группы
  -p, --absolute-names    не убирать ведущий '/' в путях каталогов
   -d, --default показать список контроля доступа по умолчанию
   -m, --modify=acl изменить текущие СКД файлов
  -M, --modify-file=file прочитать записи СКД для внесения изменения в файл
  -x, --remove=acl удалить записи из СКД файлов
  -X, --remove-file=file прочитать записи СКД для удаления из файла
  -b, --remove-all удалить все расширенные записи СКД
  -k, --remove-default удалить СКД по умолчанию
   -n, --no-mask не пересчитывать маску эффективных прав
  -d, --default действия применяются к СКД по умолчанию
   -v, --version версия
  -h, --help справка
 %s %s -- получить файловые списки контроля доступа
 %s %s -- установите файловые списки контроля доступа
 %s: %s в строке %d файла %s
 %s: %s в строке %d стандартного ввода
 %s: %s: %s в строке %d
 %s: %s: Не удается изменить режим: %s
 %s: %s: Не удается сменить владельца/группу: %s
 %s: %s: Ошибка в СКД доступа `%s': %s в записи %d
 %s: %s: Ошибка в СКД по умолчанию `%s': %s в записи %d
 %s: %s: Имя файла не найдено в строке %d, прерывание
 %s: %s: Только каталоги могут иметь СКД по умолчанию
 %s: Имя файла не найдено в строке %d стандартного ввода, прерывание
 %s: Опция -%c не завершена
 %s: Опция -%c: %s возле символа %d
 %s: Удаление начальных '/' из абсолютных путей
 %s: Стандартный ввод: %s
 %s: СКД доступа '%s': %s на записи %d
 %s: не удается получить СКД доступа на '%s': %s
 %s: не удается получить текст СКД доступа на '%s': %s
 %s: не удается получить СКД по умолчанию на '%s': %s
 %s: не удается получить текст СКД по умолчанию на '%s': %s
 %s: не удается установить СКД доступа на "%s": %s
 %s: не удается установить СКД по умолчанию на "%s": %s
 %s: невозможно удалить acl на "%s": %s
 %s: ошибка удаления СКД по умолчанию на "%s": %s
 %s: malloc не сработал: %s
 %s: opendir не сработал: %s
 Повторяющиеся записи Неверный тип записи Отсутствующая или неверная запись Множество записей одного типа Попробуйте `%s --help' для дополнительной информации.
 Использование:
 Использование: %s %s
 Применение: %s [-%s] файл ...
 сохранение ограничений на %s установка прав доступа для %s 