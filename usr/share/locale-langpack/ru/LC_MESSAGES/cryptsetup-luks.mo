��    A      $  Y   ,      �     �  �   �     f     �     �     �     �     �     �     �  7   �     6     Q     V     m     |     �     �     �  )   �       ?     4   S     �  %   �  &   �     �  -   	     9	     A	  "   X	  6   {	  >   �	     �	     
  &   '
  M   N
  +   �
  6   �
  #   �
     #  .   3  '   b     �     �     �     �     �       G     ,   _     �     �  "   �     �     �     �                  (   3     \     v  1   �  �  �  )   V  �  �  /     ;   J     �  2   �  *   �  9   �     5  )   H  h   r  ;   �       =     0   \  2   �  V   �  4     3   L  W   �  "   �  n   �  V   j  H   �  I   
  9   T  *   �  I   �       *     N   ?  ^   �  j   �  !   X  ,   z  H   �  �   �  t   �  ^     ^   z  (   �  `     J   c  6   �  #   �  J   	  0   T  ?   �     �  �   �  Z   �  6   �       M   $  3   r  (   �  #   �  E   �     9  8   @  d   y  @   �  '      b   G      +   %   $   /   1      &                   =   <      @      ?   *   ;              (   
      A       >             #      0       ,                                   .         "                                        '                   9   -   )   	   :   4   !           5   8   7   2                     3          6    
<action> is one of:
 
<name> is the device to create under %s
<device> is the encrypted device
<key slot> is the LUKS key slot number to modify
<key file> optional key file for the new key for luksAddKey action
 %s is not a LUKS partition
 %s: requires %s as arguments <device> <device> <key slot> <device> <name>  <device> [<new key file>] <name> <name> <device> Align payload at <n> sector boundaries - for luksFormat Argument <action> missing. BITS Can't open device: %s
 Command failed Command successful.
 Create a readonly mapping Display brief usage Do not ask for confirmation Failed to obtain device mapper directory. Help options: How many sectors of the encrypted data to skip at the beginning How often the input of the passphrase can be retried Key %d not active. Can't wipe.
 Key size must be a multiple of 8 bits PBKDF2 iteration time for LUKS (in ms) Print package version Read the key from a file (can be /dev/random) SECTORS Show this help message Shows more detailed error messages The cipher used to encrypt the disk (see /proc/crypto) The hash used to create the encryption key from the passphrase The size of the device The size of the encryption key The start offset in the backend device This is the last keyslot. Device will become unusable after purging this key. This will overwrite data on %s irrevocably. Timeout for interactive passphrase prompt (in seconds) Unable to obtain sector size for %s Unknown action. Verifies the passphrase by asking for it twice [OPTION...] <action> <action-specific>] add key to LUKS device create device dump LUKS partition information formats a LUKS device key %d active, purge first.
 key %d is disabled.
 key material section %d includes too few stripes. Header manipulation?
 memory allocation error in action_luksFormat modify active device msecs open LUKS device as mapping <name> print UUID of LUKS device remove LUKS mapping remove device resize active device secs show device status tests <device> for LUKS partition header unknown hash spec in phdr unknown version %d
 wipes key with number <key slot> from LUKS device Project-Id-Version: cryptsetup
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2007-01-02 16:49+0100
PO-Revision-Date: 2013-03-12 04:46+0000
Last-Translator: Anton Patsev <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:25+0000
X-Generator: Launchpad (build 18115)
 
<действие> может быть:
 
<название> — название устройства для создания под %s
<устройство> — зашифрованное устройство
<ключевой слот> — номер изменяемого ключевого слота LUKS
<файл-ключ> — необязательный файл-ключ для нового ключа для действия luksAddKey
 %s не является разделом LUKS
 %s требует %s в качестве аргумента <устройство> <устройство> <ключевой слот> <устройство> <название>  <устройство> [<новый файл ключа>] <название> <название> <устройство> Выравнивать нагрузку по границам <n> секторов — для luksFormat Аргумент <действие> отсутствует. БИТ Невозможно открыть устройство: %s
 Ошибка выполнения команды Команда выполнена успешно.
 Создать назначение в режиме "Только для чтения" Показать краткие инструкции Не cпрашивать подтверждения Невозможно получить каталог перечня устройств. Параметры справки: Сколько секторов зашифрованных данных пропустить от начала Как часто можно повторять попытку ввода пароля Ключ %d не активен. Невозможно очистить.
 Размер ключа должен быть кратным 8 битам Время итерации PBKDF2 для LUKS (мсек) Показать версию пакета Считать ключ из файла (может быть /dev/random) СЕКТОРОВ Показать это сообщение Показывает подробные сообщения об ошибках Шифр, используемый для шифрования диска (см. /proc/crypto) Хэш, используемый для создания ключа шифрования из пароля Размер устройства Размер ключа шифрования Начальное смещение в бэкенд-устройстве Это последний ключевой слот. Устройство будет невозможно использовать после удаления этого ключа. Данные на %s будут перезаписаны без возможности восстановления. Тайм-аут для интерактивного запроса пароля (секунд) Невозможно получить значение размера сектора для %s Неизвестное действие. Проверяет правильность пароля, спрашивая его дважды [ОПЦИЯ...] <действие> <параметры действия> ] добавить ключ к устройству LUKS создать устройство выгрузить в дамп информацию о разделе LUKS форматирует устройство LUKS ключ %d активен. Деактивируйте его.
 ключ %d отключен.
 Секция ключевого материала %d включает слишком мало полос. Были манипуляции с заголовком?
 Ошибка выделения памяти при выполнении action_luksFormat изменить активное устройство мсек открыть устройство LUKS как назначенное <name> напечатать UUID устройства LUKS удалить назначение LUKS удалить устройство изменить размер активного устройства сек показать состояние устройства проверить <устройство> на наличие заголовка раздела LUKS неизвестная спецификация хеша в phdr неизвестная версия %d
 стирает ключ с номером <ключевой слот> с устройства LUKS 