��    G      T  a   �        o     ?   �  �   �  .   X  #   �     �  '   �     �     �          +  (   :     c  K   z     �     �  -   �     	     	  R   	     k	     y	  8   �	  M   �	  k   
  8   �
  (   �
     �
     �
  u   	          �  X   �  @   �     #     9  ;   V  6   �  7   �  �     /   �  4   �  =   �  Y   -  �  �  )   K  7   u     �  1   �  '   �  .   &  C   U    �     �  �   �     R     X  n   x     �  @         A  &   ^     �     �  '   �     �  !   �     �  a        {  �    �   .  j   �  �   a  ;   ]  A   �  Q   �  O   -     }  *   �  '   �     �  H     c   P  �   �  
   F     Q  ^   b     �     �  �   �     �  %   �  \   �  m   G  �   �  [   �  A   �     7   O   F   �   �      �!  
   �!  �   �!  m   T"     �"  E   �"  k   (#  �   �#  D   $  �   _$  \   U%  Q   �%  U   &  �   Z&  �  �&  K   �*  O   �*  =   +  \   ]+  \   �+  I   ,  e   a,  Z  �,  -   "/    P/     f0  9   v0  ~   �0  Y   /1  |   �1  8   2  j   ?2     �2  4   �2  Y   �2  .   @3  K   o3  9   �3  �   �3     �4        4      '       B                    >                       0      A                    ?       *                  (       3   =   ,   ;                 8   /   9   G      <         -      E   .   C          6       1                            +   2   #          D             :      %      7          !       $   "   )   
      F      	   5   @   &    
        --outdated		Merge in even outdated translations.
	--drop-old-templates	Drop entire outdated templates. 
  -o,  --owner=package		Set the package that owns the command.   -f,  --frontend		Specify debconf frontend to use.
  -p,  --priority		Specify minimum priority question to show.
       --terse			Enable terse mode.
 %s failed to preconfigure, with exit status %s %s is broken or not fully installed %s is fuzzy at byte %s: %s %s is fuzzy at byte %s: %s; dropping it %s is missing %s is missing; dropping %s %s is not installed %s is outdated %s is outdated; dropping whole template! %s must be run as root (Enter zero or more items separated by a comma followed by a space (', ').) Back Choices Config database not specified in config file. Configuring %s Debconf Debconf is not confident this error message was displayed, so it mailed it to you. Debconf on %s Debconf, running at %s Dialog frontend is incompatible with emacs shell buffers Dialog frontend requires a screen at least 13 lines tall and 31 columns wide. Dialog frontend will not work on a dumb terminal, an emacs shell buffer, or without a controlling terminal. Enter the items you want to select, separated by spaces. Extracting templates from packages: %d%% Help Ignoring invalid priority "%s" Input value, "%s" not found in C choices! This should never happen. Perhaps the templates were incorrectly localized. More Next No usable dialog-like program is installed, so the dialog based frontend cannot be used. Note: Debconf is running in web mode. Go to http://localhost:%i/ Package configuration Preconfiguring packages ...
 Problem setting up the database defined by stanza %s of %s. TERM is not set, so the dialog frontend is not usable. Template #%s in %s does not contain a 'Template:' line
 Template #%s in %s has a duplicate field "%s" with new value "%s". Probably two templates are not properly separated by a lone newline.
 Template database not specified in config file. Template parse error near `%s', in stanza #%s of %s
 Term::ReadLine::GNU is incompatable with emacs shell buffers. The Sigils and Smileys options in the config file are no longer used. Please remove them. The editor-based debconf frontend presents you with one or more text files to edit. This is one such text file. If you are familiar with standard unix configuration files, this file will look familiar to you -- it contains comments interspersed with configuration items. Edit the file, changing any items as necessary, and then save it and exit. At that point, debconf will read the edited file, and use the values you entered to configure the system. This frontend requires a controlling tty. Unable to load Debconf::Element::%s. Failed because: %s Unable to start a frontend: %s Unknown template field '%s', in stanza #%s of %s
 Usage: debconf [options] command [args] Usage: debconf-communicate [options] [package] Usage: debconf-mergetemplate [options] [templates.ll ...] templates Usage: dpkg-reconfigure [options] packages
  -u,  --unseen-only		Show only not yet seen questions.
       --default-priority	Use default priority instead of low.
       --force			Force reconfiguration of broken packages.
       --no-reload		Do not reload templates. (Use with caution.) Valid priorities are: %s You are using the editor-based debconf frontend to configure your system. See the end of this document for detailed instructions. _Help apt-extracttemplates failed: %s debconf-mergetemplate: This utility is deprecated. You should switch to using po-debconf's po2debconf program. debconf: can't chmod: %s delaying package configuration, since apt-utils is not installed falling back to frontend: %s must specify some debs to preconfigure no none of the above please specify a package to reconfigure template parse error: %s unable to initialize frontend: %s unable to re-open stdin: %s warning: possible database corruption. Will attempt to repair by adding back missing question %s. yes Project-Id-Version: 1.5.53
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-11-08 02:00+0000
PO-Revision-Date: 2015-01-15 23:47+0000
Last-Translator: Yuri Kozlov <yuray@komyakino.ru>
Language-Team: Russian <debian-l10n-russian@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:09+0000
X-Generator: Launchpad (build 18115)
Language: ru
 
        --outdated		Слияние с использованием устаревших трансляций.
	--drop-old-templates	Не использовать устаревшие шаблоны. 
  -o, --owner=package		Указать пакет, которому принадлежит команда.   -f, --frontend		Задать желаемый debconf интерфейс.
  -p, --priority		Указать желаемый приоритет задаваемых вопросов.
       --terse			Включить лаконичный режим.
 %s не могу настроить, код ошибки %s пакет %s не установлен или поврежден %s имеет нечёткий перевод начиная с %s байта: %s %s является нечетким около байта %s: %s; бросаю %s отсутствует отсутствует %s; бросаю %s пакет %s не установлен пакет %s устарел %s устарел; игнорирование всего шаблона! %s должен быть запущен с правами суперпользователя (root) (Укажите необходимое количество элементов, разделяя их запятой с пробелом (', ').) Назад Варианты База данных настройки не указана в файле настройки. Настраиваем %s Debconf По данным debconf непонятно, выводилось ли это сообщение об ошибке на экран, поэтому оно было вам отправлено по почте. Debconf на %s Debconf, работающий на %s Внешний диалог не совместим с буфером оболочки emacs Внешние диалоги требуют экран минимум в 13 линий и 31 столбец. Интерфейс dialog не будет работать на неуправляемом (dumb) терминале, из буфера emacs'а, или в отсутствие контролирующего терминала. Укажите желаемые элементы, разделяя их пробелами. Распаковываю шаблоны из пакетов: %d%% Справка Игнорирование неправильного приоритета "%s" Входное значение "%s" на найдено среди вариантов локали С! Это не должно было случиться. Возможно, шаблоны были неправильно локализованы. Еще Далее Ни одна из dialog-подобных программ не установлена, поэтому вы не можете использовать dialog-интерфейс. Замечание: используется веб-интерфейс. Откройте http://localhost:%i/ Настройка пакета Предварительная настройка пакетов ...
 Проблемы при настройке базы данных, заданной строфой %s в %s. УСЛОВИЯ не установлены, потому внешний диалог не может быть использован. Шаблон #%s в %s не содержит линии 'Template:'
 Шаблон #%s в %s повторяет поле "%s" с новым значением "%s". Возможно, два шаблона не отделены друг от друга должным образом одинарной линией.
 База данных шаблонов не указана в файле настройки. Ошибка синтаксиса рядом с '%s' в строфе #%s из %s
 Term::ReadLine::GNU несовместимо с буфером оболочки emacs Параметры Sigils и Smileys в файле настройки устарели и больше не используются. Удалите их. Интерфейс к debconf, использующий текстовый редактор, предлагает вам редактировать один или несколько текстовых файлов. Перед вами один из таких файлов. Его формат схож с форматом стандартных файлов настройки Unix: параметры и их значения идут вперемешку с описывающими их комментариями. Вы должны изменить этот файл в соответствии с вашими потребностями, сохранить его и выйти из редактора. Затем программа debconf прочитает изменённый файл и использует введённые вами параметры для настройки системы. Внешний интерфейс требует управляемый tty Не удалось загрузить Debconf::Element::%s. Причина: %s Не удалось запустить интерфейс: %s Неизвестное поле шаблона '%s', в строфе номер #%s из %s
 Использование: debconf [параметры] команда [аргументы] Использование: debconf-communicate [опции] [пакет] Использование: debconf-mergetemplate [опции] [шаблоны.ll ...] шаблоны Использование: dpkg-reconfigure [параметры] пакеты
  -u,  --unseen-only		Показывать только ещё не просмотренные вопросы.
       --default-priority	Использовать приоритет по умолчанию
                         	вместо низкого.
       --force			Принудительная перенастройка сломанных
              			пакетов.
       --no-reload		Не перезагружать шаблоны.
                         	(используйте осторожно.) Допустимые приоритеты: %s Для конфигурирования вашей системы вы используете внешний интерфейс на основе редактора. Просмотрите конец документа для более детальной инструкции. _Справка ошибка при работе apt-extracttemplates: %s debconf-mergetemplate: Эта утилита не одобрена. Вам следует использовать po2debconf. debconf: не удалось изменить режим доступа к файлу: %s отсрочка настройки пакета до тех пор, пока не будет установлен apt-utils будет использован интерфейс: %s должны быть указаны некоторые deb-пакеты для перенастройки нет ни один из предложенных выше пожалуйста укажите имя пакета для перенастройки ошибка разбора шаблона: %s не удалось инициализировать интерфейс: %s не удалось заново открыть stdin: %s Предупреждение: возможно искажение базы данных. Будет произведена попытка исправления путем добавления недостающих вопросов %s. да 