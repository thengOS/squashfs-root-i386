��    �      �  �         �     �  9   �  8      7   9  6   q  5   �  3   �  E     I   X  G   �  @   �  G   +  8   s     �  0   �     �  0        B     _     q  ,   �  ,   �  ,   �  '     -   <      j  (   �  (   �     �     �       *        J  (   N  H   w  E   �  (     C   /  @   s  ;   �  J   �  3   ;  >   o  ;   �  K   �  H   6  0     H   �  J   �  F   D  �   �  H     H   [  6   �  J   �  ;   &  L   b  5   �  @   �  7   &  =   ^  <   �  @   �  ?     >   Z  <   �  6   �  3        A  !   `     �  !   �     �     �     �  F   �  =   C  &   �     �     �  ?   �          6     S     o  $   �     �     �  I   �     #      4      =      W      v      �      �   #   �      �      �      !  �   !     �!      �!  q   �!     _"     k"     }"  %   �"     �"     �"     �"     �"     #  "   #  4   1#     f#  .   �#     �#  ;   �#  3   $  /   9$  +   i$  '   �$  #   �$     �$     %     %     -%     /%  )   B%  !   l%  )   �%     �%     �%      �%      &     &  	   6&      @&     a&     f&     {&  1   �&  2   �&  0   �&     #'     >'  )   Y'  1   �'  -   �'  $   �'     (     (     '(  "   B(  %   e(     �(     �(     �(     �(  	   �(     �(     �(     �(     �(     
)     )     )     )     ,)     H)     \)  
   k)     v)  �  �)  O   4+  d   �+  c   �+  Q   M,  P   �,  O   �,  M   @-  w   �-  w   .  q   ~.  \   �.  y   M/  W   �/  ;   0  T   [0     �0  6   �0  8   1  4   :1  5   o1  ^   �1  M   2  ^   R2  K   �2  ]   �2  8   [3  L   �3  M   �3  ;   /4  ;   k4     �4  T   �4     �4  I   5  �   L5  �   �5  I   Y6  �   �6  �   -7  c   �7  a   8  K   z8  b   �8  _   )9  t   �9  n   �9  M   m:  �   �:  U   @;  p   �;  �   <  v   �<     4=  N   �=  m   >  h   q>  �   �>  N   _?  ]   �?  V   @  e   c@  c   �@  `   -A  _   �A  ^   �A  \   MB  N   �B  A   �B  <   ;C  ,   xC  3   �C  :   �C  :   D  #   OD  -   sD  �   �D  �   SE  ;   �E  '   (F  +   PF  u   |F     �F  3   G  A   DG  *   �G  U   �G  *   H  8   2H  �   kH  6   �H     #I  7   ?I  O   wI     �I     �I     �I  S   J  C   fJ  R   �J  0   �J    .K  
   1L  F   <L  �   �L  !   LM  !   nM  D   �M  m   �M  6   CN     zN     �N     �N     �N  A   �N  O   'O  8   wO  c   �O  -   P  W   BP  I   �P  E   �P  A   *Q  =   lQ  9   �Q  5   �Q  1   R  %   LR     rR  F   tR  >   �R  ?   �R  \   :S  L   �S  !   �S  N   T  V   UT  C   �T     �T  I   �T     IU  %   XU  ,   ~U  j   �U  h   V  d   V  3   �V  6   W  ^   OW  m   �W  h   X  G   �X     �X  !   �X  /   Y  ,   ?Y  i   lY     �Y     �Y  $   Z     ;Z     SZ  #   bZ  
   �Z  #   �Z  !   �Z     �Z     �Z     �Z  !   �Z  O   [  6   ][  !   �[     �[     �[     i   r   �   c           �          {   /   o      :   w   �          2       N   �   3   ;      )   m   M   t   Q   �       l   �   �   f       �       (   e              #   F   5   �       9   b          P         �           %       y           �      '   !   �       �       ,       	       R   d   A      [          �   �   �   �   ?   X   ~   Y   h   n   �   �          \             G       �   k       �   �   �   �   �       �   }           0   �   T          7      |   �   u   W       `   �      -   S         �       6   �   .   �   V   s       �       =   �       �       g           v              J   �   j       1           �   &       "   U   ]       8   _   �      �          B       $   L      �         q   4       �   ^   a       O   �       D       �   E   @   *   �   <   �   �   
         H   K   z   C   +   I               �              Z          x           �   >   p    
Report bugs to: %s
     --diff-program=PROGRAM   use PROGRAM to compare files     --diff-program=PROGRAM  use PROGRAM to compare files     --help                   display this help and exit     --help                  display this help and exit     --help                 display this help and exit     --help               display this help and exit     --ignore-file-name-case     ignore case when comparing file names     --left-column             output only the left column of common lines     --no-ignore-file-name-case  consider case when comparing file names     --normal                  output a normal diff (the default)     --strip-trailing-cr         strip trailing carriage return on input     --suppress-common-lines   do not output common lines %s %s differ: byte %s, line %s
 %s %s differ: byte %s, line %s is %3o %s %3o %s
 %s home page: <%s>
 %s home page: <http://www.gnu.org/software/%s/>
 %s%s argument '%s' too large %s: diff failed:  %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 ' '-' specified for more than one input file (C) --from-file and --to-file both specified -B, --ignore-blank-lines        ignore changes where lines are all blank -B, --ignore-blank-lines     ignore changes whose lines are all blank -D option not supported with directories -E, --ignore-tab-expansion      ignore changes due to tab expansion -E, --ignore-tab-expansion   ignore changes due to tab expansion -N, --new-file                  treat absent files as empty -S, --starting-file=FILE        start with FILE when comparing directories -W, --ignore-all-space       ignore all white space -Z, --ignore-trailing-space     ignore white space at line end -Z, --ignore-trailing-space  ignore white space at line end -b, --ignore-space-change       ignore changes in the amount of white space -b, --ignore-space-change    ignore changes in the amount of white space -b, --print-bytes          print differing bytes -i, --ignore-case               ignore case differences in file contents -i, --ignore-case            consider upper- and lower-case to be the same -i, --ignore-initial=SKIP         skip first SKIP bytes of both inputs -i, --ignore-initial=SKIP1:SKIP2  skip first SKIP1 bytes of FILE1 and
                                      first SKIP2 bytes of FILE2 -l, --left-column            output only the left column of common lines -l, --verbose              output byte numbers and differing byte values -n, --bytes=LIMIT          compare at most LIMIT bytes -o, --output=FILE            operate interactively, sending output to FILE -q, --brief                   report only when files differ -r, --recursive                 recursively compare any subdirectories found -s, --quiet, --silent      suppress all normal output -s, --report-identical-files  report when two files are the same -s, --suppress-common-lines  do not output common lines -t, --expand-tabs             expand tabs to spaces in output -t, --expand-tabs            expand tabs to spaces in output -v, --version                output version information and exit -v, --version               output version information and exit -v, --version              output version information and exit -v, --version            output version information and exit -w, --ignore-all-space          ignore all white space -y, --side-by-side            output in two columns Binary files %s and %s differ
 Common subdirectories: %s and %s
 Compare FILES line by line. Compare three files line by line. Compare two files byte by byte. David Hayes David MacKenzie Exit status is 0 if inputs are the same, 1 if different, 2 if trouble. Exit status is 0 if successful, 1 if conflicts, 2 if trouble. File %s is a %s while file %s is a %s
 Files %s and %s are identical
 Files %s and %s differ
 General help using GNU software: <http://www.gnu.org/gethelp/>
 Invalid back reference Invalid character class name Invalid collation character Invalid content of \{\} Invalid preceding regular expression Invalid range end Invalid regular expression Mandatory arguments to long options are mandatory for short options too.
 Memory exhausted No match No newline at end of file No previous regular expression Only in %s: %s
 Packaged by %s
 Packaged by %s (%s)
 Premature end of regular expression Regular expression too big Report %s bugs to: %s
 Richard Stallman SKIP values may be followed by the following multiplicative suffixes:
kB 1000, K 1024, MB 1,000,000, M 1,048,576,
GB 1,000,000,000, G 1,073,741,824, and so on for T, P, E, Z, Y. Success Symbolic links %s and %s differ
 The optional SKIP1 and SKIP2 specify the number of bytes to skip
at the beginning of each file (zero by default). Thomas Lord Torbjorn Granlund Trailing backslash Try '%s --help' for more information. Unknown system error Unmatched ( or \( Unmatched ) or \) Unmatched [ or [^ Unmatched \{ Usage: %s [OPTION]... FILE1 FILE2
 Usage: %s [OPTION]... FILE1 [FILE2 [SKIP1 [SKIP2]]]
 Usage: %s [OPTION]... FILES
 Usage: %s [OPTION]... MYFILE OLDFILE YOURFILE
 Written by %s and %s.
 Written by %s, %s, %s,
%s, %s, %s, %s,
%s, %s, and others.
 Written by %s, %s, %s,
%s, %s, %s, %s,
%s, and %s.
 Written by %s, %s, %s,
%s, %s, %s, %s,
and %s.
 Written by %s, %s, %s,
%s, %s, %s, and %s.
 Written by %s, %s, %s,
%s, %s, and %s.
 Written by %s, %s, %s,
%s, and %s.
 Written by %s, %s, %s,
and %s.
 Written by %s, %s, and %s.
 Written by %s.
 ` block special file both files to be compared are directories cannot compare '-' to a directory cannot interactively merge standard input character special file cmp: EOF on %s
 conflicting output style options conflicting tabsize options conflicting width options directory failed to reopen %s with mode %s fifo incompatible options input file shrank internal error: invalid diff type in process_diff internal error: invalid diff type passed to output internal error: screwup in format of diff blocks invalid %s%s argument '%s' invalid --bytes value '%s' invalid diff format; incomplete last line invalid diff format; incorrect leading line chars invalid diff format; invalid change separator invalid suffix in %s%s argument '%s' memory exhausted message queue missing operand after '%s' options -l and -s are incompatible pagination not supported on this host program error read failed regular empty file regular file semaphore shared memory object socket stack overflow standard output stderr stdin stdout symbolic link too many file label options typed memory object unknown stream weird file write failed Project-Id-Version: diffutils 2.8.7
Report-Msgid-Bugs-To: bug-diffutils@gnu.org
POT-Creation-Date: 2013-03-24 11:05-0700
PO-Revision-Date: 2016-06-02 15:47+0000
Last-Translator: AsstZD <eskaer_spamsink@ngs.ru>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:43+0000
X-Generator: Launchpad (build 18115)
Language: ru
 
Отчёты об ошибках отправляйте по адресу: %s
     --diff-program=PROGRAM   использовать PROGRAM для сравнения файлов     --diff-program=PROGRAM  использовать PROGRAM для сравнения файлов     --help                   показать эту справку и выйти     --help                  показать эту справку и выйти     --help                 показать эту справку и выйти     --help               показать эту справку и выйти     --ignore-file-name-case     игнорировать регистр при сравнении имён файлов     --left-column             выводить только левый столбец совпадающих строк     --no-ignore-file-name-case  учитывать регистр при сравнении имён файлов     --normal                  выводить обычный diff (по умолчанию)     --strip-trailing-cr         убирать возврат каретки в конце строки при вводе     --suppress-common-lines   не выводить совпадающие строки %s %s различаются: байт %s, строка %s
 %s %s различаются: байт %s, строка %s равен %3o %s %3o %s
 %s веб-сайт: <%s>
 %s веб-сайт: <http://www.gnu.org/software/%s/>
 слишком большой аргумент %s%s '%s' %s: поиск различий неуспешен:  %s: недопустимый параметр — %c
 %s: для параметра «%c%s» нельзя использовать аргумент
 %s: неоднозначный параметр '%s'; возможности: %s: для параметра «--%s» нельзя использовать аргумент
 %s: для параметра «--%s» требуется аргумент
 %s: с параметром «-W %s» нельзя использовать аргумент
 %s: параметр «-W %s» неоднозначен
 %s: для параметра «-W %s» требуется аргумент
 %s: для параметра требуется аргумент — «%c»
 %s: нераспознанный параметр «%c%s»
 %s: нераспознанный параметр «--%s»
 ' '-' указано для более чем одного входного файла (C) необходимо задать как --from-file, так и --to-file -B, --ignore-blank-lines        игнорировать изменения там, где строки полностью пустые -B, --ignore-blank-lines     игнорировать изменения там, где строки полностью пустые Опция -D не поддерживается для каталогов -E, --ignore-tab-expansion      игнорировать изменения из-за замены табуляции на пробелы -E, --ignore-tab-expansion   игнорировать изменения из-за замены табуляции на пробелы -N, --new-file                  считать отсутствующие файлы пустыми -S, --starting-file=FILE        начать с FILE при сравнении каталогов -W, --ignore-all-space       игнорировать все пробелы -Z, --ignore-trailing-space     игнорировать пробелы в концах строк -Z, --ignore-trailing-space  игнорировать пробелы в конце строки -b, --ignore-space-change       игнорировать изменения в количестве пробелов -b, --ignore-space-change    игнорировать изменение количества пробелов -b, --print-bytes          вывести отличающиеся байты -i, --ignore-case               не учитывать регистр при сравнении содержимого файлов -i, --ignore-case            игнорировать регистр символов -i, --ignore-initial=SKIP         пропустить первые SKIP байтов с обоих входов -i, --ignore-initial=SKIP1:SKIP2  пропустить первые SKIP1 байтов из FILE1 и
                                      первые SKIP2 байтов из FILE2 -l, --left-column            выводить только левый столбец совпадающих строк -l, --verbose              выводить номера байтов и отличающиеся значения байтов -n, --bytes=LIMIT          сравнить не более LIMIT байтов -o, --output=FILE            работать интерактивно, отправляя вывод в FILE -q, --brief                   сообщить только когда файлы различаются -r, --recursive                 рекурсивное сравнение всех обнаруженных подкаталогов -s, --quiet, --silent      подавить весь обычный вывод -s, --report-identical-files  сообщать, если два файла одинаковы -s, --suppress-common-lines  не выводить совпадающие строки -t, --expand-tabs             заменять в выводе табуляцию на пробелы -t, --expand-tabs            заменять в выводе табуляцию пробелами -v, --version                показать информацию о версии и выйти -v, --version               показать информацию о версии и выйти -v, --version              показать информацию о версии и выйти -v, --version            показать информацию о версии и выйти -w, --ignore-all-space          игнорировать все пробелы -y, --side-by-side            вывод в два столбца Двоичные файлы %s и %s различаются
 Общие подкаталоги: %s и %s
 Сравнивает ФАЙЛЫ построчно. Построчно сравнивает три файла. Побайтово сравнивает два файла. Дэвид Хейес (David Hayes) Дэвид Маккензи (David MacKenzie) Выходной статус равен 0, если входные файлы идентичны, 1 -- если
различаются, и 2 в случае неполадок. Выходной статус равен 0 в случае успеха, 1 в случае конфликтов и 2 в
случае неполадок. Файл %s это %s, тогда как файл %s -- %s
 Файлы %s и %s идентичны
 Файлы %s и %s различаются
 Общая  информация по использованию программ GNU: <http://www.gnu.org/gethelp/>
 Неверная ссылка Неверное имя класса символа Недопустимый для сортировки символ Неверное содержимое \{\} Неверное предшествующее регулярное выражение Неверный нижний предел Неверное регулярное выражение Обязательные аргументы для длинных опций обязательны и для коротких.
 Оперативная память исчерпана Совпадений нет В конце файла нет новой строки Регулярное выражение не было задано раньше Только в %s: %s
 Упаковано %s
 Упаковано %s (%s)
 Преждевременный конец регулярного выражения Регулярное выражение слишком велико Отчёты об ошибках %s отправляйте по адресу <%s>
 Ричард Столлман (Richard Stallman) К значениям Н можно приписывать один из следующих множительных суффиксов:
kB 1000, K 1024, MB 1,000,000, M 1,048,576,
GB 1,000,000,000, G 1,073,741,824, и так далее для T, P, E, Z, Y. Успех Символические ссылки %s и %s отличаются
 Необязательные SKIP1 и SKIP2 указывают количество байтов,
пропускаемое в начале каждого файла (ноль по умолчанию). Томас Лорд (Thomas Lord) Торбьёрн Гранлунд Обратная косая черта стоит последней Попробуйте '%s --help' для получения дополнительной информации. Неизвестная системная ошибка Непарная ( или \( Непарная ) или \) Непарная [ или [^ Непарная \{ Использование: %s [КЛЮЧ]... ФАЙЛ1 ФАЙЛ2
 Использование: %s [КЛЮЧ]... ФАЙЛ1 [ФАЙЛ2 [Н1 [Н2]]]
 Использование: %s [КЛЮЧ]... ФАЙЛЫ
 Использование: %s [КЛЮЧ]... МОЙ-ФАЙЛ СТАРЫЙ-ФАЙЛ ВАШ-ФАЙЛ
 Авторы программы -- %s и %s.
 Авторы программы -- %s, %s, %s,
%s, %s, %s, %s,
%s, %s и другие.
 Авторы программы -- %s, %s, %s,
%s, %s, %s, %s,
%s и %s.
 Авторы программы -- %s, %s, %s,
%s, %s, %s, %s
и %s.
 Авторы программы -- %s, %s, %s,
%s, %s, %s и %s.
 Авторы программы -- %s, %s, %s,
%s, %s и %s.
 Авторы программы -- %s, %s, %s,
%s и %s.
 Авторы программы -- %s, %s, %s
и %s.
 Авторы программы -- %s, %s и %s.
 Автор программы -- %s.
 ` специальный файл с поблочным доступом оба сравниваемых файла -- каталоги невозможно сравнить '-' с каталогом невозможно интерактивно сливать стандартный ввод специальный файл с посимвольным доступом cmp: конец файла в %s
 противоречивые ключи задания стиля вывода противоречивые ключи задания ширины табуляции противоречивые ключи задания ширины каталог не удалось повторно открыть %s в режиме %s очередь несовместимые ключи входной файл уменьшился внутренняя ошибка: неверный тип заплаты в функции `process_diff' внутренняя ошибка: на выход передан неверный тип заплаты внутренняя ошибка: поврежденный формат блоков заплаты недопустимый аргумент %s%s '%s' недопустимое значение --bytes '%s' неверный формат заплаты; неполная последняя строка неверный формат заплаты; неправльные первые символы строки неверный формат заплаты; неверный разделитель изменения недопустимый суффикс в аргументе %s%s '%s' память исчерпана очередь сообщений пропущен операнд после '%s' ключи -l и -s несовместимы разбиение на страницы не поддерживается на данной машине ошибка программы чтение неуспешно обычный пустой файл обычный файл семафор разделяемый объект сокет переполнение стека стандартный вывод stderr stdin stdout символьная ссылка слишком много ключей задающих метки файлов объект типизированной памяти неизвестный поток странный файл запись неуспешна 