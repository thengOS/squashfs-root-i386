��          D      l       �      �   !   �   &   �   R  �   �  >  4   �  L     P   a  �  �                          Access Your Private Data Record your encryption passphrase Setup Your Encrypted Private Directory To encrypt your home directory or "Private" folder, a strong passphrase has been automatically generated. Usually your directory is unlocked with your user password, but if you ever need to manually recover this directory, you will need this passphrase. Please print or write it down and store it in a safe location. If you click "Run this action now", enter your login password at the "Passphrase" prompt and you can display your randomly generated passphrase. Otherwise, you will need to run "ecryptfs-unwrap-passphrase" from the command line to retrieve and record your generated passphrase. Project-Id-Version: ecryptfs-utils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-27 00:09+0000
PO-Revision-Date: 2013-01-30 06:55+0000
Last-Translator: Aleksey Kabanov <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:21+0000
X-Generator: Launchpad (build 18115)
 Доступ к вашим личным данным Запишите свою парольную фразу шифрования Настройте ваш личный зашифрованный каталог Для шифрования вашего домашнего каталога или папки "Private" была автоматически создана надёжная парольная фраза. Обычно ваш каталог можно разблокировать вашим паролем пользователя, но при необходимости восстановить этот каталог вручную вам понадобится эта парольная фраза. Распечатайте или запишите её и храните в надёжном месте. Если вы нажмёте кнопку «Выполнить это действие сейчас» и введёте свой пароль для входа в систему в ответ на приглашение "Passphrase", то сможете увидеть свою случайно сгенерированную парольную фразу. В противном случае для показа и записи парольной фразы вам понадобится запустить "ecryptfs-unwrap-passphrase" в командной строке. 