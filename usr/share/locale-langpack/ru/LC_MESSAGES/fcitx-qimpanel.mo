��    *      l  ;   �      �     �     �     �     �     �  	   �  
   �     �     �     �            S   #     w     |  
   �  
   �     �     �  
   �     �  	   �     �  	   �     �       
     )   !     K     S     e     m     r  -   ~     �  +   �     �     �                  �       �     �     �     	               1  
   G     R     r     �     �  �   �  
   T	     _	     y	     �	  2   �	     �	     �	     
     )
  ,   C
     p
     �
     �
     �
  /   �
                <     W     `  f   x  -   �  S        a     n  +   �     �     �     $                             &                             )                        %   "         	       *      
              (              #      !          '                                          &Apply &Cancel &Cannel &Ok &Refresh All Skins Appearance Author Character Map ConfigureFcitx ConfigureIM ConfigureIMPanel Description: Click to select and preview, double-click local to edit, save locally. Exit FontSize Horizontal IndexColor Keyboard Layout Chart MarginBottom MarginLeft MarginRight MarginTop Mozc Edit mode Mozc Tool No input window Online &Help! OtherColor Please install fcitx-qimpanel-configtool! Preview Qimpanel Settings Restart Skin Skin Design Sougo Skin does not support preview and edit! Text Entry Settings... The default configuration has been restored Version Vertical Virtual Keyboard Warning tips Project-Id-Version: fcitx-qimpanel
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-10 02:31+0000
PO-Revision-Date: 2016-06-02 14:27+0000
Last-Translator: Данил Тютюник <den.tyutyunik@ya.ru>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:29+0000
X-Generator: Launchpad (build 18115)
 &Применить &Отмена &Отменить &OK О&бновить Все скины Внешний вид Автор Таблица символов Настроить Fcitx Настроить IM Настроить IMPanel Описание: Нажмите для выбора и просмотра, двойное нажатие редактирует и сохраняет. Выйти Размер шрифта Горизонтально Индекс цвета Схема раскладки клавиатуры Отступ снизу Отступ слева Отступ справа Отступ сверху Режим редактирования Mozc Инструмент Mozc Нет окна ввода Онлайн &Помощь! Другой цвет Установите fcitx-qimpanel-configtool! Предпросмотр Настройки Qimpanel Перезапустить Скин Дизайн скина Скин Sougo не поддерживает предпросмотр и редактирование! Параметры ввода текста... Стандартная конфигурация была восстановлена Версия Вертикально Виртуальная клавиатура Предупреждение советы 