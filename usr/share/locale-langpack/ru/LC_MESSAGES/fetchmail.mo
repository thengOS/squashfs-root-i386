��    ~       U  �'      P5     Q5  L  q5  D   �6  *   7  1   .7  6   `7  4   �7  @   �7  .   8  4   <8  >   q8  K   �8  6   �8  @   39  E   t9  E   �9  5    :  +   6:  8   b:  7   �:  Q   �:  1   %;  +   W;  E   �;  H   �;  N   <  )   a<  8   �<  M   �<  D   =  2   W=  *   �=  *   �=  +   �=  =   >     J>  e   \>  e   �>  -   (?  ;   V?  4   �?  ,   �?  4   �?  5   )@  8   _@  )   �@  3   �@  0   �@  H   'A  8   pA  -   �A  7   �A  )   B  1   9B  2   kB  9   �B  8   �B  7   C  7   IC  7   �C  0   �C  6   �C  9   !D  2   [D  3   �D  >   �D  (   E  /   *E  "   ZE  /   }E  4   �E  5   �E  ?   F     XF  @   oF  6   �F  -   �F  5   G  3   KG  7   G  5   �G  +   �G  2   H  1   LH     ~H  B   �H  :   �H  6   I  F   PI  <   �I  -   �I  !   J  '   $J  '   LJ  8   tJ  ;   �J  @   �J  (   *K  )   SK  *   }K  (   �K  J   �K  J   L  -   gL  -   �L  C   �L     M  .   M  ,   GM  %   tM      �M      �M  0   �M  ?   N  &   MN  +   tN  (   �N  /   �N  .   �N     (O  &   <O  0   cO      �O  4   �O  $   �O  &   P  (   6P  %   _P  "   �P     �P      �P     �P     Q     &Q  .   =Q  :   lQ  6   �Q  4   �Q  %   R  F   9R  K   �R  3   �R      S  Q   S  V   mS      �S     �S  *   �S  !   $T  j   FT  ?   �T      �T  !   U  )   4U     ^U  2   oU     �U  2   �U  /   �U  B   V  B   [V  "   �V  "   �V  ;   �V      W     5W  +   RW  (   ~W  #   �W     �W  J   �W  D   0X  2   uX  2   �X  ?   �X  *   Y     FY  .   [Y     �Y  7   �Y  ;   �Y     Z     6Z     HZ     \Z     iZ  
   xZ     �Z     �Z     �Z     �Z     �Z     �Z     �Z     [     
[  	   [     [  
   ([     3[     O[  3   `[  :   �[  6   �[  4   \  $   ;\  D   `\  J   �\  D   �\     5]     D]     a]     j]  ;   ]     �]  #   �]  B   �]     =^     \^     t^  3   �^  1   �^  )   �^  *   _  )   G_    q_  �   w`     [a     {a  <   �a  =   �a     b     1b  1   Pb  #   �b     �b     �b     �b  (   �b     
c  &   !c     Hc     bc  !   }c     �c     �c  .   �c  ?   	d  9   Id  2   �d  -   �d  )   �d  *   e  :   9e     te  -   �e     �e     �e  #   �e  $   f     ;f     Yf  !   sf  
   �f     �f     �f     �f  ,   �f  !   g     3g     Rg      qg     �g      �g     �g     �g  &   �g     h     2h     Ph     mh  ;   �h  �   �h  )   �i  M   �i  &   j  4   ;j  0   pj  >   �j  9   �j  4   k  D   Ok  D   �k      �k     �k  >   l  $   Xl  ?   }l     �l  3   �l     m     m     3m      ?m     `m     nm     �m  =   �m  '   �m  !   n     %n     Bn     Yn  8   rn  8   �n      �n      o     &o     Bo     \o     vo  !   �o     �o     �o     �o     �o     �o     p     -p  ;   Fp     �p     �p     �p     �p  0   �p     q  #   0q     Tq      mq     �q  &   �q  *   �q  *   �q  (   !r  *   Jr  *   ur  +   �r  +   �r  +   �r  )   $s  )   Ns  (   xs  #   �s     �s     �s      �s      t      't  3   Ht     |t     �t  ,   �t  )   �t  #   �t  #    u     Du     Uu     ou     �u     �u     �u     �u     �u     v      v     ;v     Tv     lv     �v     �v     �v  0   �v  !   w     $w  )   9w  '   cw     �w  "   �w      �w     �w      x     x  !   ,x     Nx  !   hx     �x  *   �x     �x     �x  .   �x  2   ,y  5   _y  4   �y  .   �y  *   �y     $z     6z  2   Vz  0   �z     �z     �z     �z      {      {     4{     M{     c{     x{  %   �{     �{  *   �{  3   �{  *   .|  "   Y|  )   ||  -   �|     �|      �|     }     ,}     B}  H   \}  *   �}  -   �}  -   �}  1   ,~  )   ^~     �~  D   �~  %   �~  /   �~    +    7�  @   ��  F   ��    ?�     X�  #   u�  (   ��       9   փ     �  �   .�     ߄  ?   �  &   2�  #   Y�  %   }�  (   ��  ,   ̅  	   ��     �     �     5�     K�  /   f�     ��  '   ��     ӆ     ��     �  $   �  )   ?�  :   i�  A   ��  G   �  5   .�  =   d�     ��     ��     و     �     �     %�     C�     a�  $   }�  ?   ��     �     �     �  
   �     $�     9�  ,   Q�     ~�     ��     ��     Ê     �     �  (   �  "   *�  '   M�  *   u�  (   ��  $   ɋ  5   �  "   $�  0   G�  "   x�  )   ��     Ō     �     ��     	�     �     #�     ?�     R�  �   b�  E   �  V   )�  '   ��  4   ��  >   ݎ  ;   �  1   X�  I   ��  M   ԏ  ,   "�  M   O�  ;   ��  +   ِ  +   �     1�  U   I�     ��  !   ��     ّ  S   �  /   G�  )   w�  #   ��     Œ  '   �  H   �  
   T�  5   _�     ��     ��     ē  +   ޓ  "   
�  !   -�  "   O�     r�  2   ��  &        �     �     "�  %   5�     [�  0   b�  4   ��     ȕ     ѕ  '   �  1   �     ?�     [�     u�     ��     ��  H   ��     �     �     ;�  @   X�  =   ��     ח  &   �  '   �  #   A�  3   e�  4   ��     Θ  0   �     �  @   =�  .   ~�  2   ��  -   ��  1   �  ,   @�     m�  $   }�     ��     ��     њ     �  0   ��  "   *�      M�  "   n�  	   ��  '   ��  "   Û      �     �     �  %   4�  ,   Z�     ��  !   ��     ǜ     �     �     �     �     1�     E�  )   `�  :   ��  0   ŝ  :   ��     1�  	   L�     V�     ^�     k�  )   ��  @   ��  4   ��     +�     A�  -  `�  =   ��  u  ̡  T   B�  E   ��  M   ݤ  X   +�  u   ��  e   ��  ^   `�  V   ��  t   �  q   ��  K   ��  r   I�  w   ��  r   4�  Y   ��  E   �  y   G�  l   ��  �   .�  T   ǫ  <   �  u   Y�  �   Ϭ  �   S�  A   �  t   #�  �   ��  �   0�  N   ��  X   �  ]   Z�  c   ��  w   �  "   ��  �   ��  �   ��  7   ��  }   ۳  m   Y�  O   Ǵ  W   �  \   o�  a   ̵  ?   .�  Z   n�  L   ɶ  {   �  m   ��  R    �  j   S�  D   ��  w   �  U   {�  o   ѹ  N   A�  V   ��  h   �  _   P�  F   ��  e   ��  w   ]�  i   ռ  V   ?�  V   ��  A   ��  V   /�  J   ��  C   Ѿ  _   �  r   u�  �   �     l�  p   ��  k   ��  D   f�  ]   ��  Z   	�  U   d�  R   ��  Q   �  X   _�  V   ��  B   �  y   R�  I   ��  N   �  u   e�  w   ��  b   S�  F   ��  H   ��  X   F�  i   ��  g   	�  b   q�  h   ��  B   =�  F   ��  C   ��     �  ~   ��  m   
�  m   x�  t   ��  "   [�  G   ~�  D   ��  e   �  7   q�  7   ��  `   ��  �   B�  M   ��  J   �  >   c�  a   ��  e   �  +   j�  f   ��  Z   ��  C   X�  j   ��  F   �  A   N�  [   ��  R   ��  9   ?�  5   y�  1   ��  Q   ��  K   3�  +   �  _   ��  ]   �  b   i�  b   ��  e   /�  }   ��  �   �  V   ��  '   ��  �   �  �   ��  2   U�     ��  s   ��  0   �  I  E�  ~   ��  ?   �  S   N�  M   ��     ��  G   �     P�  ^   c�  k   ��  m   .�  l   ��  G   	�  A   Q�  \   ��     ��  9   �  O   G�  G   ��  4   ��  1   �  �   F�  |   ��  V   E�  R   ��  �   ��  B   u�  )   ��  b   ��  5   E�  X   {�  ]   ��  /   2�      b�  *   ��     ��     ��     ��     ��  ;   �     N�  (   _�  &   ��  <   ��  +   ��     �     &�     +�     <�     R�  ,   g�     ��  X   ��  o   �  �   u�  w   �  _   ��  �   ��  �   ��  �   ��      ��  8   ��  
   ��     ��  �   �  (   ��  1   ��  [   ��  /   R�  *   ��  %   ��  X   ��  R   ,�  <   �  =   ��  <   ��  �  7�  �  ��  9   ��  >   ��  �   �  �   ��  ,   G�  E   t�  i   ��  N   $�  0   s�     ��     ��  W   ��  $   4�  X   Y�  1   ��  7   ��  3   �  6   P�  6   ��  s   ��  �   2�  �   ��  S   S�  Q   ��  O   ��  _   I�  d   ��  (   �  f   7�  '   ��  H   ��  O   �  f   _�  P   ��  C   �  @   [�     ��  %   ��  &   ��  F   ��  Y   A�  /   ��  ,   ��  ,   ��  1   %  '   W  ?        �  *   �  S   �  %   S N   y N   � %    i   = �  � @   s    � .   4 S   c S   � w    W   � y   � �   U �   � ?   i ?   � _   � =   I	 X   �	    �	 o   �	     n
 ;   �
    �
 1   �
     0   + =   \ �   � C     5   d a   � $   � 6   ! t   X t   � 4   B 4   w ,   � /   � B   	    L 7   b    � ,   � '   � :   � ?   . +   n 2   � t   � $   B )   g 7   �    � i   � 1   P 5   � !   � 1   � &    9   3 =   m =   � ;   � =   % =   c >   � >   � >    <   ^ <   � ;   � 9       N !   n 1   � 1   � ?   � X   4 0   �    � d   � M   @ N   � N   �    , (   L '   u &   � %   � '   � *    '   = )   e )   � '   � &   � &    (   / )   X C   � Q   � 8    /   Q n   � 8   � .   ) =   X =   �    �    � $   	 <   . $   k <   �     � :   � G   ) @   q d   � W     W   o  V   �  l   ! Z   �!    �! ;    " ]   <" _   �"    �" 5   # 0   J#    {# S   �# 5   �# )   $ B   G$ I   �$ X   �$ <   -% H   j% q   �% E   %& /   k& h   �& `   ' "   e' 3   �' 2   �' .   �' ,   ( �   K( V   �( j   0) J   �) J   �) S   1*    �* �   �* D   %+ 5   j+ �  �+ !  e- �   �0 �   /1 N  �1     +4 Z   L4 A   �4 '   �4 X   5 1   j5 J  �5 1   �6 |   7 G   �7 I   �7 `   (8 J   �8 ?   �8    9 ,   #9 7   P9 .   �9 F   �9 `   �9 6   _: R   �: >   �: '   (; 2   P; 4   �; ]   �; �   < �   �< �   7= v   �= y   S> ,   �> .   �> !   )? %   K? :   q? :   �? -   �? 4   @ *   J@ t   u@    �@    A (   A    GA ;   aA :   �A N   �A 3   'B (   [B *   �B 4   �B -   �B .   C 6   AC K   xC a   �C ^   &D \   �D L   �D q   /E @   �E Y   �E Q   <F g   �F ,   �F !   #G 7   EG    }G    �G :   �G    �G #   �G �  H �   �I �    J 0   �J v   �J l   UK =   �K <    L q   =L �   �L F   ;M �   �M X   N R   eN N   �N N   O l   VO    �O J   �O    +P j   GP I   �P N   �P I   KQ    �Q T   �Q �   
R #   �R g   �R -   &S &   TS    {S 1   �S $   �S (   �S )   T !   AT \   cT M   �T 4   U 4   CU    xU 5   �U    �U b   �U m   8V    �V ;   �V ]   �V q   QW E   �W =   	X    GX K   XX X   �X �   �X :   �Y :   �Y H   �Y r   EZ n   �Z &   '[ b   N[ d   �[ ]   \ �   t\ a   �\ ;   Z] [   �] ?   �] �   2^ b   �^ p   _ \   �_ j   �_ I   O`     �` @   �` 2   �` :   .a    ia 7   {a t   �a =   (b 0   fb H   �b D   �b g   %c R   �c H   �c 3   )d 4   ]d H   �d J   �d <   &e <   ce 8   �e    �e 8   �e 
   f '   *f *   Rf (   }f ?   �f a   �f N   Hg h   �g +    h    ,h    Fh    Rh @   ch G   �h �   �h V   �i >   �i :   4j    �                         [  �  �   T  
           I      h  �      �  8  �  �  �          ,          �  \   �          �   3   :  Q       E  d   >  �   u  �   !   �           �       h   �   "   �          K    G  �   �   $  d     �       �   v  �  -  �  #    �   �   �       �  �  �           +                    ^   Y   �       �  �   �      �   {  6     B  y  )  �   �  g   �   �         ?   c  +      �  '  �   }       5                o  N           3    {   �   k       $   ?     ;  M  �  �   D  	      �      �           �  <  }  �   �         �  l       �    �  r   g       �  �   G  R      5  �   g    9  �   �      p    �    �  2   7              _  �   1  �       �   �   J      �  9   �  m  )  I      u  �  k  N  �      �      H  �  %      <   W     2    �      �       $  �   e   W           T      �   7       �   �   C      L       �   �      @  Y             �  y   S   �      �   1      �   �       �  �   t   �  �   !  :   t  i  �  V  �       �   (      x  �        4  �   I       �   |   U   w  b      �  0   q  �  O  �  .         J      )   0  �   "  \      �  �   �   �  ,    �      �    �   �      f  �   S  R       �              v       �  �   +      *         �   �   �  L  ~   >  >         �  �       e    �   %         j   �           �  �      �   �   \      A  s   �   #              �  :  a  &  `  B    E   R  D      .               -      �  �       X  `    �  �   �   D     l  �   �  M   �  2  C       �                               �   Q         �       O   �                q      q  n          �   k  F  "  J      �   C  
  e  �       {  m  W  �   x   Q   ^      4      �   �   r  �  b  �  �  6    �     �   3      7  5   A   K       @              Z      O  X          �  �              H  �   �  �   0  �  w     �   �      �   B           P  �   �        1   �   z        	   U  j  (  c  �  n     �   �   �  �       *      �         _      V      L  a   <  �                   �  l          �   �   E          c   G   �   �      �  �   r      ]   �  �          u             [  �   -   /  �  �  �  �  ]      �  �  j      �  /   @    �        Z  ;       �   �       �      �                   �        �  �   s              �      V   �  �   �  ?  �       �  A  �   S  i  n   ~  _                       t  �    U  z  �       f  �   	  v      s  (   m   d      �  =    =  �          �  ^  �  �   ;        �      P   ~      �       =   �   �  �      �  '         �  T       �   y  p   �     �          �  �  8  �  �  �                  �  �   �   �              o   �  �  �      �     [   X   �   b   �           P    �   4  �  N  �     F   �   
   �  �      �       �       F      ]      w   *               o  Z       �  !  p  �                           �   �   i                   8      H   �   �     �   �  �  �   %         &   .  �       z   h  �       �  �      '  &  9  �       #    6      �   �   �      �      ,   |      Y        |  f       }  M  �   x  �   a      /      K            `    
Caught SIGINT... bailing out.
 
However, if you HAVE changed your account details since starting the
fetchmail daemon, you need to stop the daemon, change your configuration
of fetchmail, and then restart the daemon.

The fetchmail daemon will continue running and attempt to connect
at each cycle.  No future notifications will be sent until service
is restored.       --auth        authentication type (password/kerberos/ssh/otp)
       --bsmtp       set output BSMTP file
       --fastuidl    do a binary search for UIDLs
       --fetchdomains fetch mail for specified domains
       --fetchsizelimit set fetch message size limit
       --invisible   don't write Received & enable host spoofing
       --limitflush  delete oversized messages
       --lmtp        use LMTP (RFC2033) for delivery
       --nobounce    redirect bounces from user to postmaster.
       --nosoftbounce fetchmail deletes permanently undeliverable messages.
       --pidfile     specify alternate PID (lock) file
       --plugin      specify external command to open connection
       --plugout     specify external command to open smtp connection
       --port        TCP port to connect to (obsolete, use --service)
       --postmaster  specify recipient of last resort
       --principal   mail service principal
       --showdots    show progress dots even in logfiles
       --smtpname    set SMTP full name username@domain
       --softbounce  keep permanently undeliverable messages on server (default).
       --ssl         enable ssl encrypted session
       --sslcert     ssl client certificate
       --sslcertck   do strict server certificate check (recommended)
       --sslcommonname  expect this CommonName from server (discouraged)
       --sslfingerprint fingerprint that must match that of the server's cert.
       --sslkey      ssl private key file
       --sslproto    force ssl protocol (SSL2/SSL3/TLS1)
       --syslog      use syslog(3) for most messages when running as a daemon
       --tracepolls  add poll-tracing information to Received header
     attacks by a dishonest service are possible.)
     cannot be sure you are talking to the
     prevent you logging in, but means you
     service that you think you are (replay
    Do linear search of UIDs during each poll (--fastuidl 0).
   %d UIDs saved.
   %d message  %d octets long deleted by fetchmail.   %d messages %d octets long deleted by fetchmail.   %d message  %d octets long skipped by fetchmail.   %d messages %d octets long skipped by fetchmail.   -?, --help        display this option help
   -B, --fetchlimit  set fetch limit for server connections
   -D, --smtpaddress set SMTP delivery domain to use
   -E, --envelope    envelope address header
   -F, --flush       delete old messages from server
   -I, --interface   interface required specification
   -K, --nokeep      delete new messages after retrieval
   -L, --logfile     specify logfile name
   -M, --monitor     monitor interface for activity
   -N, --nodetach    don't detach daemon process
   -P, --service     TCP service to connect to (can be numeric TCP port)
   -Q, --qvirtual    prefix to remove from local user id
   -S, --smtphost    set SMTP forwarding host
   -U, --uidl        force the use of UIDLs (pop3 only)
   -V, --version     display version info
   -Z, --antispam,   set antispam response values
   -a, --[fetch]all  retrieve old and new messages
   -b, --batchlimit  set batch limit for SMTP connections
   -c, --check       check for messages without fetching
   -d, --daemon      run as a daemon once per n seconds
   -e, --expunge     set max deletions between expunges
   -f, --fetchmailrc specify alternate run control file
   -i, --idfile      specify alternate UIDs file
   -k, --keep        save new messages after retrieval
   -l, --limit       don't fetch messages over given size
   -m, --mda         set MDA to use for forwarding
   -n, --norewrite   don't rewrite header addresses
   -p, --protocol    specify retrieval protocol (see man page)
   -q, --quit        kill daemon process
   -r, --folder      specify remote folder name
   -s, --silent      work silently
   -t, --timeout     server nonresponse timeout
   -u, --username    specify users's login on server
   -v, --verbose     work noisily (diagnostic output)
   -w, --warnings    interval between warning mail notification
   APOP secret = "%s".
   Address to be put in RCPT TO lines shipped to SMTP will be %s
   All available authentication methods will be tried.
   All messages will be retrieved (--all on).
   Carriage-return forcing is disabled (forcecr off).
   Carriage-return forcing is enabled (forcecr on).
   Carriage-return stripping is disabled (stripcr off).
   Carriage-return stripping is enabled (stripcr on).
   Connection must be through interface %s.
   DNS lookup for multidrop addresses is disabled.
   DNS lookup for multidrop addresses is enabled.
   Default mailbox selected.
   Deletion interval between expunges forced to %d (--expunge %d).
   Delivered-To lines will be discarded (dropdelivered on)
   Delivered-To lines will be kept (dropdelivered off)
   Do binary search of UIDs during %d out of %d polls (--fastuidl %d).
   Do binary search of UIDs during each poll (--fastuidl 1).
   Domains for which mail will be fetched are:   End-to-end encryption assumed.
   Envelope header is assumed to be: %s
   Envelope-address routing is disabled
   Fetch message size limit is %d (--fetchsizelimit %d).
   Fetched messages will be kept on the server (--keep on).
   Fetched messages will not be kept on the server (--keep off).
   GSSAPI authentication will be forced.
   Host part of MAIL FROM line will be %s
   Idle after poll is disabled (idle off).
   Idle after poll is enabled (idle on).
   Interpretation of Content-Transfer-Encoding is disabled (pass8bits on).
   Interpretation of Content-Transfer-Encoding is enabled (pass8bits off).
   Kerberos V4 authentication will be forced.
   Kerberos V5 authentication will be forced.
   Listener connections will be made via plugout %s (--plugout %s).
   Local domains:   MIME decoding is disabled (mimedecode off).
   MIME decoding is enabled (mimedecode on).
   MSN authentication will be forced.
   Mail service principal is: %s
   Mail will be retrieved via %s
   Message size limit is %d octets (--limit %d).
   Message size warning interval is %d seconds (--warnings %d).
   Messages will be %cMTP-forwarded to:   Messages will be appended to %s as BSMTP
   Messages will be delivered with "%s".
   Messages with bad headers will be passed on.
   Messages with bad headers will be rejected.
   Multi-drop mode:    NTLM authentication will be forced.
   No SMTP message batch limit (--batchlimit 0).
   No UIDs saved from this host.
   No fetch message size limit (--fetchsizelimit 0).
   No forced expunges (--expunge 0).
   No interface requirement specified.
   No localnames declared for this host.
   No message size limit (--limit 0).
   No monitor interface specified.
   No plugin command specified.
   No plugout command specified.
   No post-connection command.
   No pre-connection command.
   No prefix stripping
   No received-message limit (--fetchlimit 0).
   Nonempty Status lines will be discarded (dropstatus on)
   Nonempty Status lines will be kept (dropstatus off)
   Number of envelope headers to be skipped over: %d
   OTP authentication will be forced.
   Old messages will be flushed before message retrieval (--flush on).
   Old messages will not be flushed before message retrieval (--flush off).
   Only new messages will be retrieved (--all off).
   Options are as follows:
   Oversized messages will be flushed before message retrieval (--limitflush on).
   Oversized messages will not be flushed before message retrieval (--limitflush off).
   Pass-through properties "%s".
   Password = "%s".
   Password authentication will be forced.
   Password will be prompted for.
   Poll of this server will occur every %d interval.
   Poll of this server will occur every %d intervals.
   Poll trace information will be added to the Received header.
   Polling loop will monitor %s.
   Predeclared mailserver aliases:   Prefix %s will be removed from user id
   Protocol is %s   Protocol is KPOP with Kerberos %s authentication   RPOP id = "%s".
   Received-message limit is %d (--fetchlimit %d).
   Recognized listener spam block responses are:   Rewrite of server-local addresses is disabled (--norewrite on).
   Rewrite of server-local addresses is enabled (--norewrite off).
   SMTP message batch limit is %d.
   SSL encrypted sessions enabled.
   SSL key fingerprint (checked against the server key): %s
   SSL protocol: %s.
   SSL server CommonName: %s
   SSL server certificate checking enabled.
   SSL trusted certificate directory: %s
   SSL trusted certificate file: %s
   Selected mailboxes are:   Server aliases will be compared with multidrop addresses by IP address.
   Server aliases will be compared with multidrop addresses by name.
   Server connection will be brought up with "%s".
   Server connection will be taken down with "%s".
   Server connections will be made via plugin %s (--plugin %s).
   Server nonresponse timeout is %d seconds   Single-drop mode:    Size warnings on every poll (--warnings 0).
   Spam-blocking disabled
   This host will be queried when no host is specified.
   This host will not be queried when no host is specified.
   True name of server is %s.
  (%d body octets)  (%d header octets)  (%d octets)  (%d octets).
  (default)  (default).
  (forcing UIDL use)  (length -1)  (oversized)  (previously authorized)  (using default port)  (using service %s)  <empty>  and   flushed
  not flushed
  retained
 %cMTP connect to %s failed
 %cMTP error: %s
 %cMTP listener doesn't like recipient address `%s'
 %cMTP listener doesn't really like recipient address `%s'
 %d local name recognized.
 %d local names recognized.
 %d message (%d %s) for %s %d messages (%d %s) for %s %d message for %s %d messages for %s %d message waiting after expunge
 %d messages waiting after expunge
 %d message waiting after first poll
 %d messages waiting after first poll
 %d message waiting after re-poll
 %d messages waiting after re-poll
 %lu is unseen
 %s (log message incomplete)
 %s at %s %s at %s (folder %s) %s configuration invalid, LMTP can't use default SMTP port
 %s connection to %s failed %s error while fetching from %s@%s
 %s error while fetching from %s@%s and delivering to SMTP host %s
 %s fingerprints do not match!
 %s fingerprints match.
 %s key fingerprint: %s
 %s querying %s (protocol %s) at %s: poll completed
 %s querying %s (protocol %s) at %s: poll started
 %s's SMTP listener does not support ATRN
 %s's SMTP listener does not support ESMTP
 %s's SMTP listener does not support ETRN
 %s: The NULLMAILER_FLAGS environment variable is set.
This is dangerous as it can make nullmailer-inject or nullmailer's
sendmail wrapper tamper with your From:, Message-ID: or Return-Path: headers.
Try "env NULLMAILER_FLAGS= %s YOUR ARGUMENTS HERE"
%s: Abort.
 %s: The QMAILINJECT environment variable is set.
This is dangerous as it can make qmail-inject or qmail's sendmail wrapper
tamper with your From: or Message-ID: headers.
Try "env QMAILINJECT= %s YOUR ARGUMENTS HERE"
%s: Abort.
 %s: You don't exist.  Go away.
 %s: can't determine your host! %s: opportunistic upgrade to TLS failed, trying to continue
 %s: opportunistic upgrade to TLS failed, trying to continue.
 %s: upgrade to TLS failed.
 %s: upgrade to TLS succeeded.
 %s:%d: warning: found "%s" before any host names
 %s:%d: warning: unknown token "%s"
 %u is first unseen
 %u is unseen
 -- 
The Fetchmail Daemon --check mode enabled, not fetching mail
 ATRN request refused.
 All connections are wedged.  Exiting.
 Authentication required.
 Authorization OK on %s@%s
 Authorization failure on %s@%s%s
 BSMTP file open failed: %s
 Bad base64 reply from server.
 Bad certificate: Subject CommonName too long!
 Both fetchall and keep on in daemon or idle mode is a mistake!
 Cannot find my own host in hosts database to qualify it!
 Cannot handle UIDL response from upstream server.
 Cannot open fetchids file %s for writing: %s
 Cannot rename fetchids file %s to %s: %s
 Cannot resolve service %s to port number.
 Certificate/fingerprint verification was somehow skipped!
 Challenge decoded: %s
 Checking if %s is really the same node as %s
 Command not implemented
 Could not decode OTP challenge
 Couldn't get service name for [%s]
 Couldn't unwrap security level data
 Credential exchange complete
 Cygwin socket read retry
 Cygwin socket read retry failed!
 DNS lookup Deity error Deleting fetchids file.
 Digest text buffer too small!
 ERROR: no support for getpassword() routine
 ESMTP CRAM-MD5 Authentication...
 ESMTP LOGIN Authentication...
 ESMTP PLAIN Authentication...
 ETRN support is not configured.
 ETRN syntax error
 ETRN syntax error in parameters
 EVP_md5() failed!
 Enter password for %s@%s:  Error creating security level request
 Error deleting %s: %s
 Error exchanging credentials
 Error releasing credentials
 Error writing to MDA: %s
 Error writing to fetchids file %s, old file left in place.
 Fetchmail comes with ABSOLUTELY NO WARRANTY. This is free software, and you
are welcome to redistribute it under certain conditions. For details,
please see the file COPYING in the source or documentation directory.
 Fetchmail could not get mail from %s@%s.
 Fetchmail saw more than %d timeouts while attempting to get mail from %s@%s.
 Fetchmail was able to log into %s@%s.
 Fetchmail will direct error mail to the postmaster.
 Fetchmail will direct error mail to the sender.
 Fetchmail will forward misaddressed multidrop messages to %s.
 Fetchmail will masquerade and will not generate Received
 Fetchmail will show progress dots even in logfiles.
 Fetchmail will treat permanent errors as permanent (drop messages).
 Fetchmail will treat permanent errors as temporary (keep messages).
 File %s must be a regular file.
 File %s must be owned by you.
 File %s must have no more than -rwx------ (0700) permissions.
 File descriptor out of range for SSL For help, see http://www.fetchmail.info/fetchmail-FAQ.html#R15
 GSSAPI error %s: %.*s
 GSSAPI support is configured, but not compiled in.
 Get response
 Get response return %d [%s]
 Hdr not 60
 IMAP support is not configured.
 Idfile is %s
 Inbound binary data:
 Invalid APOP timestamp.
 Invalid SSL protocol '%s' specified, using default (SSLv23).
 Invalid authentication `%s' specified.
 Invalid protocol `%s' specified.
 Invalid userid or passphrase Issuer CommonName: %s
 Issuer Organization: %s
 KERBEROS v4 support is configured, but not compiled in.
 KERBEROS v5 support is configured, but not compiled in.
 Kerberos V4 support not linked.
 Kerberos V5 support not linked.
 LMTP delivery error on EOM
 Lead server has no name.
 Lock-busy error on %s@%s
 Logfile is %s
 MD5 being applied to data block:
 MDA MDA died of signal %d
 MDA open failed
 MDA returned nonzero status %d
 Maximum GSS token size is %ld
 Mechanism field incorrect
 Merged UID list from %s: Messages inserted into list on server. Cannot handle this.
 New UID list from %s: No IP address found for %s No interface found with name %s No mail for %s
 No mailservers set up -- perhaps %s is missing?
 No messages waiting for %s
 No, their IP addresses don't match
 Node %s not allowed: %s
 ODMR support is not configured.
 Old UID list from %s: Option --all is not supported with %s
 Option --check is not supported with ETRN
 Option --check is not supported with ODMR
 Option --flush is not supported with %s
 Option --flush is not supported with ETRN
 Option --flush is not supported with ODMR
 Option --folder is not supported with ETRN
 Option --folder is not supported with ODMR
 Option --folder is not supported with POP3
 Option --keep is not supported with ETRN
 Option --keep is not supported with ODMR
 Option --limit is not supported with %s
 Options for retrieving from %s@%s:
 Out of memory!
 Outbound data:
 POP2 support is not configured.
 POP3 support is not configured.
 Pending messages for %s started
 Please specify the service as decimal port number.
 Poll interval is %d seconds
 Polling %s
 Progress messages will be logged via syslog
 Protocol identified as IMAP2 or IMAP2BIS
 Protocol identified as IMAP4 rev 0
 Protocol identified as IMAP4 rev 1
 Query status=%d
 Query status=0 (SUCCESS)
 Query status=1 (NOMAIL)
 Query status=10 (SMTP)
 Query status=11 (DNS)
 Query status=12 (BSMTP)
 Query status=13 (MAXFETCH)
 Query status=2 (SOCKET)
 Query status=3 (AUTHFAIL)
 Query status=4 (PROTOCOL)
 Query status=5 (SYNTAX)
 Query status=6 (IOERR)
 Query status=7 (ERROR)
 Query status=8 (EXCLUDE)
 Query status=9 (LOCKBUSY)
 Queuing for %s started
 RPA Failed open of /dev/urandom. This shouldn't
 RPA Session key length error: %d
 RPA String too long
 RPA User Authentication length error: %d
 RPA _service_ auth fail. Spoof server?
 RPA authorisation complete
 RPA error in service@realm string
 RPA rejects you, reason unknown
 RPA rejects you: %s
 RPA status: %02X
 RPA token 2 length error
 RPA token 2: Base64 decode error
 RPA token 4 length error
 RPA token 4: Base64 decode error
 Realm list: %s
 Received BYE response from IMAP server: %s Releasing GSS credentials
 Repoll immediately on %s@%s
 Required APOP timestamp not found in greeting
 Required LOGIN capability not supported by server
 Required NTLM capability not compiled into fetchmail
 Required OTP capability not compiled into fetchmail
 Restricted user (something wrong with account) Routing message version %d not understood. SDPS not enabled. SMTP listener refused delivery
 SMTP listener rejected local recipient addresses:  SMTP server requires STARTTLS, keeping message.
 SMTP transaction SMTP: (bounce-message body)
 SSL connection failed.
 SSL is not enabled SSL support is not compiled in.
 Saved error is still %d
 Scratch list of UIDs: Secret pass phrase:  Sending credentials
 Server CommonName mismatch: %s != %s
 Server busy error on %s@%s
 Server certificate verification error: %s
 Server name not set, could not verify certificate!
 Server name not specified in certificate!
 Server rejected the AUTH command.
 Server requires integrity and/or privacy
 Server responded with UID for wrong message.
 Service challenge (l=%d):
 Service chose RPA version %d.%d
 Service has been restored.
 Service timestamp %s
 Session key established:
 Strange: MDA pclose returned %d and errno %d/%s, cannot handle at %s:%d
 String '%s' is not a valid number string.
 Subject: Fetchmail oversized-messages warning Subject: fetchmail authentication OK on %s@%s Subject: fetchmail authentication failed on %s@%s Subject: fetchmail sees repeated timeouts Success TLS is mandatory for this session, but server refused CAPA command.
 Taking options from command line%s%s
 The CAPA command is however necessary for TLS.
 The attempt to get authorization failed.
Since we have already succeeded in getting authorization for this
connection, this is probably another failure mode (such as busy server)
that fetchmail cannot distinguish because the server didn't send a useful
error message. The attempt to get authorization failed.
This probably means your password is invalid, but some servers have
other failure modes that fetchmail cannot distinguish from this
because they don't send useful error messages on login failure.

The fetchmail daemon will continue running and attempt to connect
at each cycle.  No future notifications will be sent until service
is restored. The following oversized messages remain on server %s account %s: The following oversized messages were deleted on server %s account %s: This could mean that your mailserver is stuck, or that your SMTP
server is wedged, or that your mailbox file on the server has been
corrupted by a server error.  You can run `fetchmail -v -v' to
diagnose the problem.

Fetchmail won't poll this mailbox again until you restart it.
 This is fetchmail release %s Timestamp syntax error in greeting
 Token Length %d disagrees with rxlen %d
 Token length error
 Try adding the --service option (see also FAQ item R12).
 Trying to connect to %s/%s... Trying to continue with unqualified hostname.
DO NOT report broken Received: headers, HELO/EHLO lines or similar problems!
DO repair your /etc/hosts, DNS, NIS or LDAP instead.
 Turnaround now...
 Unable to open kvm interface. Make sure fetchmail is SGID kmem. Unable to parse interface name from %s Unable to process ATRN request now
 Unable to queue messages for node %s
 Undefined protocol request in POP3_auth
 Unexpected non-503 response to LMTP EOM: %s
 Unicode:
 Unknown ETRN error %d
 Unknown Issuer CommonName
 Unknown Organization
 Unknown Server CommonName
 Unknown login or authentication error on %s@%s
 Unknown system error Unwrapped security level flags: %s%s%s
 User authentication (l=%d):
 User challenge:
 Using service name [%s]
 Value of string '%s' is %s than %d.
 WARNING: Running as root is discouraged.
 Warning: Issuer CommonName too long (possibly truncated).
 Warning: Issuer Organization Name too long (possibly truncated).
 Warning: ignoring bogus data for message sizes returned by the server.
 Warning: multiple mentions of host %s in config file
 We've run out of allowed authenticators and cannot continue.
 Writing fetchids file.
 Yes, their IP addresses match
 You have no mail.
 about to deliver with: %s
 activity on %s -noted- as %d
 activity on %s checked as %d
 activity on %s was %d, is %d
 analyzing Received line:
%s attempt to re-exec fetchmail failed
 attempt to re-exec may fail as directory has not been restored
 awakened at %s
 awakened by %s
 awakened by signal %d
 background bogus message count! can't even send to %s!
 can't raise the listener; falling back to %s cannot create socket: %s
 challenge mismatch
 client/server protocol client/server synchronization connected.
 connection failed.
 connection to %s:%s [%s/%s] failed: %s.
 could not decode BASE64 challenge
 could not decode BASE64 ready response
 could not decode initial BASE64 challenge
 could not get current working directory
 could not open %s to append logs to
 couldn't fetch headers, message %s@%s:%d (%d octets)
 couldn't find HESIOD pobox for %s
 couldn't find canonical DNS name of %s (%s): %s
 couldn't time-check %s (error %d)
 couldn't time-check the run-control file
 dec64 error at char %d: %x
 decoded as %s
 discarding new UID list
 dup2 failed
 end of input error writing message text
 execvp(%s) failed
 expunge failed
 fetchlimit %d reached; %d message left on server %s account %s
 fetchlimit %d reached; %d messages left on server %s account %s
 fetchmail: %s configuration invalid, RPOP requires a privileged port
 fetchmail: %s configuration invalid, specify positive port number for service or port
 fetchmail: %s fetchmail at %ld killed.
 fetchmail: Cannot detach into background. Aborting.
 fetchmail: Error: multiple "defaults" records in config file.
 fetchmail: another foreground fetchmail is running at %ld.
 fetchmail: background fetchmail at %ld awakened.
 fetchmail: can't accept options while a background fetchmail is running.
 fetchmail: can't check mail while another fetchmail to same host is running.
 fetchmail: can't find a password for %s@%s.
 fetchmail: can't poll specified hosts with another fetchmail running at %ld.
 fetchmail: error killing %s fetchmail at %ld; bailing out.
 fetchmail: error opening lockfile "%s": %s
 fetchmail: error reading lockfile "%s": %s
 fetchmail: fork failed
 fetchmail: interface option is only supported under Linux (without IPv6) and FreeBSD
 fetchmail: invoked with fetchmail: lock creation failed.
 fetchmail: malloc failed
 fetchmail: monitor option is only supported under Linux (without IPv6) and FreeBSD
 fetchmail: no mailservers have been specified.
 fetchmail: no other fetchmail is running
 fetchmail: removing stale lockfile
 fetchmail: socketpair failed
 fetchmail: thread sleeping for %d sec.
 fetchmail: warning: no DNS available to check multidrop fetches from %s
 foreground forwarding and deletion suppressed due to DNS errors
 forwarding to %s
 found Received address `%s'
 get_ifinfo: malloc failed get_ifinfo: sysctl (iflist estimate) failed get_ifinfo: sysctl (iflist) failed getaddrinfo("%s","%s") error: %s
 getaddrinfo(NULL, "%s") error: %s
 gethostbyname failed for %s
 id=%s (num=%d) was deleted, but is still present!
 interval not reached, not querying %s
 invalid IP interface address
 invalid IP interface mask
 kerberos error %s
 krb5_sendauth: %s [server says '%s']
 larger line accepted, %s is an alias of the mailserver
 line rejected, %s is not an alias of the mailserver
 line: %s lock busy on server lock busy!  Is another session active?
 mail expunge mismatch (%d actual != %d expected)
 mail from %s bounced to %s
 mailbox selection failed
 malloc failed
 mapped %s to local %s
 mapped address %s to local %s
 message %s@%s:%d was not the expected length (%d actual != %d expected)
 message has embedded NULs missing IP interface address
 missing or bad RFC822 header nameserver failure while looking for '%s' during poll of %s: %s
 nameserver failure while looking for `%s' during poll of %s.
 no Received address found
 no address matches; forwarding to %s.
 no address matches; no postmaster set.
 no local matches, forwarding to %s
 no recipient addresses matched declared local names non-null instance (%s) might cause strange behavior
 normal termination, status %d
 not swapping UID lists, no UIDs seen this query
 passed through %s matching %s
 poll of %s skipped (failed authentication or too many timeouts)
 post-connection command failed with status %d
 post-connection command terminated with signal %d
 pre-connection command failed with status %d
 pre-connection command terminated with signal %d
 principal %s in ticket does not match -u %s
 protocol error
 protocol error while fetching UIDLs
 re-poll failed
 reading message %s@%s:%d of %d realloc failed
 receiving message data
 recipient address %s didn't match any local name restarting fetchmail (%s changed)
 running %s (host %s service %s)
 search for unseen messages failed
 seen seen selecting or re-polling default folder
 selecting or re-polling folder %s
 server option after user options server recv fatal
 skipping message %s@%s:%d skipping message %s@%s:%d (%d octets) skipping poll of %s, %s IP address excluded
 skipping poll of %s, %s down
 skipping poll of %s, %s inactive
 sleeping at %s for %d seconds
 smaller smtp listener protocol error
 socket starting fetchmail %s daemon
 swapping UID lists
 terminated with signal %d
 timeout after %d seconds waiting for %s.
 timeout after %d seconds waiting for listener to respond.
 timeout after %d seconds waiting for server %s.
 timeout after %d seconds waiting to connect to server %s.
 timeout after %d seconds.
 undefined unknown unknown (%s) unsupported protocol selected.
 usage:  fetchmail [options] [server ...]
 warning: Do not ask for support if all mail goes to postmaster!
 warning: multidrop for %s requires envelope option!
 will idle after poll
 writing RFC822 msgblk.headers
 Project-Id-Version: fetchmail 6.3.10-pre1
Report-Msgid-Bugs-To: fetchmail-devel@lists.berlios.de
POT-Creation-Date: 2013-04-23 23:24+0200
PO-Revision-Date: 2015-12-04 23:10+0000
Last-Translator: Pavel Maryanov <Unknown>
Language-Team: Russian <gnu@mx.ru>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 16:21+0000
X-Generator: Launchpad (build 18115)
Language: ru
 
Получен SIGINT... завершение работы.
 
Однако, если изменили данные своей учётной записи ПОСЛЕ запуска
демона fetchmail, то вам понадобится остановить демон, изменить
настройки fetchmail, а затем перезапустить демон.

Демон fetchmail продолжит свою работу и будет периодически пытаться
подключиться. В дальнейшем уведомления отправляться не будут до тех
пор, пока служба не будет восстановлена.       --auth        тип аутентификации (password/kerberos/ssh/otp)
       --bsmtp       установка файла вывода BSMTP
       --fastuidl    выполнить двоичный поиск UIDL'ов
       --fetchdomains выборка почты для указанных доменов
       --fetchsizelimit установка ограничения на размер выборки сообщений
       --invisible   не записывать Received и включить спуфинг хоста
       --limitflush  удаление сообщений завышенного размера
       --lmtp        использование для доставки LMTP (RFC2033)
       --nobounce    перенаправлять рикошеты пользователей постмастеру
       --nosoftbounce fetchmail удаляет постоянно недоставляемые сообщения
       --pidfile     указание другого файла с PID'ом
       --plugin      указание внешней команды для установки соединения
       --plugout     указание внешней команды для установки соединения smtp
       --port        TCP-порт для подключения (устарел, используйте --service)
       --postmaster  указать получателя на крайний случай
       --principal   принципал почтовой службы
       --showdots    отображение точечной строки прогресса даже в log-файлах
       --smtpname    установка полного (пользователь@домен) имени SMTP
       --softbounce  сохранять на сервере постоянно недоставляемые сообщения (по умолчанию)
       --ssl         включение сессии, зашифрованной ssl
       --sslcert     сертификат клиента ssl
       --sslcertck   строгая проверка сертификата севера (рекомендуется)
       --sslcommonname  ожидать это общепринятое имя от сервера (не рекомендуется)
       --sslfingerprint отпечаток, который должен соответствовать сертификату сервера.
       --sslkey      файл секретного ключа ssl
       --sslproto    принудительное использование протокола ssl (SSL2/SSL3/TLS1)
       --syslog      использование syslog(3) для большинства сообщений, когда запущен как демон
       --tracepolls  добавление информации трассировки опроса в заголовок Received
     воспроизведения атак подложной службы.)
     не можете быть уверены в том, что общаетесь со
     помешать вам войти в систему, но означает, что вы
     службой, в которой, как вы думаете, вы есть (возможны
    Выполнять линейный поиск UID'ов во время каждого опроса (--fastuidl 0).
   Сохранено %d UID'ов.
   fetchmail удалил %d сообщение длиной %d октетов.   fetchmail удалил %d сообщения длиной %d октетов.   fetchmail удалил %d сообщений длиной %d октетов.   fetchmail пропустил %d сообщение длиной %d октетов.   fetchmail пропустил %d сообщения длиной %d октетов.   fetchmail пропустил %d сообщение длиной %d октетов.   -?, --help        вывод этой справки
   -B, --fetchlimit  установка ограничения на выборку для соединений сервера
   -D, --smtpaddress установка домена, используемого для доставки SMTP
   -E, --envelope    заголовок охватывающего адреса
   -F, --flush       удаление старых сообщений с сервера
   -I, --interface   требуемая для интерфейса спецификация
   -K, --nokeep      удаление новых сообщений после получения
   -L, --logfile     указание имени log-файла
   -M, --monitor     наблюдение за активностью интерфейса
   -N, --nodetach    не отсоединять процесс демона
   -P, --service     служба TCP для подключения (может быть числовым TCP-портом)
   -Q, --qvirtual    префикс, удаляемый из id локального пользователя
   -S, --smtphost    установка хоста переадресации SMTP
   -U, --uidl        принудительное использование UIDL'ов (только pop3)
   -V, --version     вывод информации о версии
   -Z, --antispam,   установка значений антиспамерских ответных действий
   -a, --[fetch]all  получение старых и новых сообщений
   -b, --batchlimit  установка ограничения на пакет для SMTP-соединений
   -c, --check       проверка сообщений без выборки
   -d, --daemon      запуск в виде демона каждые n секунд
   -e, --expunge     установка максимального количества удалений
   -f, --fetchmailrc указание другого файла контроля запуска
   -i, --idfile      указание других UID'ов файла
   -k, --keep        сохранение новых сообщений после получения
   -l, --limit       не делать выборку сообщений больше заданного размера
   -m, --mda         установка MDA, используемого для переадресации
   -n, --norewrite   не перезаписывать адреса заголовка
   -p, --protocol    указание протокола поиска (см. man page)
   -q, --quit        останов процесса демона
   -r, --folder      указание имени удаленного каталога
   -s, --silent      работа без лишней информации
   -t, --timeout     таймаут молчания сервера
   -u, --username    указание логина пользователя на сервере
   -v, --verbose     работа с подробным выводом диагностических данных
   -w, --warnings    промежутки между предупреждающими почтовыми уведомлениями
   Секрет APOP = "%s".
   Адрес, вставляемый в строки RCPT TO, отправляемые по SMTP, будет %s
   Будут испробованы все доступные способы аутентификации.
   Будут получены все сообщения (--all on).
   Принудительный возврат каретки выключен (forcecr off).
   Принудительный возврат каретки включен (forcecr on).
   Удаление возврата каретки выключено (stripcr off).
   Удаление возврата каретки включено (stripcr on).
   Подключение должно быть через интерфейс %s.
   DNS-запрос для многоканальных адресов выключен.
   DNS-запрос для многоканальных адресов включен.
   Выбран почтовый ящик по умолчанию.
   Интервал между удалениями принудительно установлен в %d (--expunge %d).
   Строки Delivered-To будут удалены (dropdelivered on)
   Строки Delivered-To будут сохранены (dropdelivered off)
   Выполнять двоичный поиск UID'ов во время %d из %d опросов (--fastuidl %d).
   Выполнять двоичный поиск UID'ов во время каждого опроса (--fastuidl -1).
   Домены, для которых будет выполняться выборка почты:   Допускается оперативное шифрование.
   Охватывающий заголовок должен быть: %s
   Маршрутизация охватывающего адреса отключена
   Ограничение на размер выборки сообщений - %d (--fetchsizelimit %d).
   Выбранные сообщения будут оставлены на сервере (--keep on).
   Выбранные сообщения будут удалены с сервера (--keep off).
   Будет использована принудительная аутентификация GSSAPI.
   Часть хоста из строки MAIL FROM будет %s
   Простой после опроса выключен (idle off).
   Простой после опроса включен (idle on).
   Интерпретация кодировки передаваемого контента выключена (pass8bits on).
   Интерпретация кодировки передаваемого контента включена (pass8bits off).
   Будет использована принудительная аутентификация Kerberos V4.
   Будет использована принудительная аутентификация Kerberos V5.
   Подключения слушателя будут выполняться через plugout %s (--plugout %s).
   Локальные домены:   MIME-декодирование выключено (mimedecode off).
   MIME-декодирование включено (mimedecode on).
   Будет использована принудительная аутентификация MSN.
   Принципал почтовой службы: %s
   Почта будет получена через %s
   Ограничение на размер сообщения - %d октетов (--limit %d).
   Интервал между предупреждениями о размере сообщения - %d секунд (--warnings %d).
   Сообщения будут переадресованы по %cMTP на:   Сообщения будут прикреплены к %s как BSMTP
   Сообщения будут доставлены с "%s".
   Сообщения с плохими заголовками будут предаваться.
   Сообщения с плохими заголовками будут отбрасываться.
   Многоканальный режим:    Будет использована принудительная аутентификация NTLM.
   Нет ограничения на пакет SMTP-сообщений (--batchlimit 0).
   С этого хоста нет сохраненных UID'ов.
   Нет ограничения на размер выборки сообщений (--fetchsizelimit 0).
   Нет принудительных удалений (--expunge 0).
   Не указаны требования интерфейса.
   Нет локальных имен, объявленных для этого хоста.
   Нет ограничения на размер сообщения (--limit 0).
   Не указан интерфейс монитора.
   Не указана команда плагина.
   Не указана команда plugout'а.
   Нет команды, выполняемой после соединения.
   Нет команды, выполняемой до соединения.
   Нет разборки префикса
   Нет ограничения на полученные сообщения (--fetchlimit 0).
   Непустые строки состояния будут удалены (dropstatus on)
   Непустые строки состояния будут сохранены (dropstatus off)
   Количество пропускаемых охватывающих заголовков: %d
   Будет использована принудительная аутентификация OTP.
   Старые сообщения будут удалены перед получением сообщения (--flush on).
   Старые сообщения не будут удалены перед получением сообщения (--flush off).
   Будут получены только новые сообщения (--all off).
   Используемые опции:
   Сообщения завышенного размера будут удалены перед получением сообщения (--limitflush on).
   Сообщения завышенного размера не будут удалены перед получением сообщения (--limitflush  off).
   Свойства ретрансляции "%s".
   Пароль = "%s".
   Будет использована принудительная аутентификация с паролем.
   Будет затребован пароль.
   Опрос этого сервера будет выполняться каждый %d интервал.
   Опрос этого сервера будет выполняться каждые %d интервала.
   Опрос этого сервера будет выполняться каждые %d интервалов.
   Информация с трассировкой опроса будет добавлена в заголовок Received.
   Цикл опроса будет наблюдать за %s.
   Ранее объявленные алиасы почтового сервера:   Префикс %s будет удален из id пользователя
   Протокол - %s   Протокол - KPOP с аутентификацией Kerberos %s   RPOP id = "%s".
   Ограничение на полученные сообщения - %d (--fetchlimit %d).
   Ответные действия на распознанные слушателем спам-блоки:   Перезапись локальных адресов сервера выключена (--norewrite on).
   Перезапись локальных адресов сервера включена (--norewrite off).
   Ограничение на пакет SMTP-сообщений - %d.
   Включены сессии, зашифрованные SSL.
   Отпечаток ключа SSL (сверенный с ключом сервера): %s
   Протокол SSL: %s.
   Общепринятое имя SSL-сервера: %s
   Включена проверка сертификата сервера SSL.
   Каталог доверенных сертификатов SSL: %s
   доверенный сертификат SSL: %s
   Выбранные почтовые ящики:   Алисы сервера будут сравнены с многоканальными адресами по IP-адресу.
   Алисы сервера будут сравнены с многоканальными адресами по имени.
   Подключение к серверу будет установлено с "%s".
   Подключение к серверу будет разорвано с "%s".
   Подключения к серверу будут выполняться посредством плагина %s (--plugin %s).
   Таймаут молчания сервера - %d секунд   Одноканальный режим:    Предупреждения о размере при каждом опросе (--warnings 0).
   Блокировка спама отключена
   Когда хост не указан, будет запрошен этот хост.
   Когда хост не указан, этот хост не будет запрошен.
   Настоящее имя севера - %s.
  (%d октетов в теле)  (%d октетов в заголовке)  (%d октетов)  (%d октетов).
  (по умолчанию)  (по умолчанию).
  (принудительно используется UIDL)  (длина -1)  (завышенного размера)  (ранее авторизовано)  (используется порт по умолчанию)  (используется служба %s)  <пусто>  и   очищено
  не очищено
  задержано
 Сбой %cMTP-подключения к %s
 Ошибка %cMTP: %s
 Слушателю %cMTP не понравился адрес получателя `%s'
 Слушателю %cMTP в самом деле не понравился адрес получателя `%s'
 распознано %d локальное имя.
 распознано %d локальных имени.
 распознано %d локальных имен.
 %d сообщение (%d %s) для %s %d сообщения (%d %s) для %s %d сообщений (%d %s) для %s %d сообщение для %s %d сообщения для %s %d сообщений для %s %d сообщение ожидает после удаления
 %d сообщения ожидают после удаления
 %d сообщений ожидают после удаления
 %d сообщение ожидает после первого опроса
 %d сообщения ожидают после первого опроса
 %d сообщений ожидают после первого опроса
 %d сообщение ожидает после повторного опроса
 %d сообщения ожидают после повторного опроса
 %d сообщений ожидают после повторного опроса
 %lu не просмотрено
 %s (неполное сообщение журнала)
 %s на %s %s на %s (каталог %s) конфигурация %s не верна, LMTP не может использовать порт SMTP по умолчанию
 сбой %s-подключения к %s Ошибка %s при выборке из %s@%s
 Ошибка %s при выборке из %s@%s и доставке на хост SMTP %s
 Отпечатки %s не совпадают!
 Отпечатки %s совпадают.
 Отпечаток ключа %s: %s
 %s запрашивает %s (протокол %s) на %s: опрос завершен
 %s запрашивает %s (протокол %s) на %s: опрос начат
 SMTP-приемник %s не поддерживает ATRN
 SMTP-приемник %s не поддерживает ESMTP
 SMTP-приемник %s не поддерживает ETRN
 %s: Установлена переменная окружения NULLMAILER_FLAGS.
Она представляет собой опасность, т.к. с ней можно сделать
nullmailer-inject или подменить nullmailer'овский упаковщик sendmail
своими заголовками From:, Message-ID: или Return-Path:.
Попробуйте "env NULLMAILER_FLAGS= %s СВОИ АРГУМЕНТЫ"
%s: Сброс.
 %s: Установлена переменная окружения QMAILINJECT.
Она представляет собой опасность, т.к. с ней можно сделать
qmail-inject или подменить qmail'овский упаковщик sendmail
своими заголовками From: или Message-ID:.
Попробуйте "env QMAILINJECT= %s СВОИ АРГУМЕНТЫ"
%s: Сброс.
 %s: Ты существуешь.  Вали отсюда.
 %s: невозможно определить ваш хост! %s: сбой принудительного обновления до TLS; выполняется попытка продолжить работу
 %s: сбой принудительного обновления до TLS; выполняется попытка продолжить работу.
 %s: сбой обновления до TLS.
 %s: обновление до TLS выполнено успешно.
 %s:%d: предупреждение: перед всеми именами хостов найден "%s"
 %s:%d: предупреждение: неизвестный маркер "%s"
 %u первое не просмотренное
 %u не просмотрено
 --
Демон Fetchmail влючен режим --check, выборка почты не выполняется
 Запрос ATRN отклонен.
 Все соединения перегружены.  Завершение работы.
 Требуется аутентификация.
 Авторизация выполнена для %s@%s
 Ошибка авторизации для %s@%s%s
 Не удалось открыть файл BSMTP: %s
 Неверный ответ base64 с сервера.
 Неправильный сертификат: общее название Темы слишком длинное!
 Запускать fetchall, оставляя его при этом в режиме демона или простоя, нельзя!
 Невозможно найти свой собственный хост в базе данных хостов, чтобы определить его!
 Невозможно обработать UIDL-ответ upstream-сервера.
 Невозможно открыть fetchids-файл %s для записи: %s
 Невозможно переименовать fetchids-файл %s в %s: %s
 Невозможно определить тип службы %s по номеру порта.
 Проверка сертификата/отпечатка была как-то пропущена!
 Запрос декодирован: %s
 Проверяется, если %s действительно такой же узел, как и %s
 Команда не выполнена
 Невозможно было декодировать запрос OTP
 Невозможно было получить имя службы для [%s]
 Невозможно было развернуть данные уровня безопасности
 Обмен идентификационными данными завершен
 Повторная попытка чтения сокета Cygwin
 Сбой повторного чтения сокета Cygwin!
 DNS-запрос Божественная ошибка Удаляется файл fetchids.
 Буфер текстового журнала слишком мал!
 ОШИБКА: отсутствует поддержка процедуры getpassword()
 Аутентификация ESMTP CRAM-MD5...
 Аутентификация ESMTP LOGIN...
 Аутентификация ESMTP PLAIN...
 Поддержка ETRN не настроена.
 Ошибка синтаксиса ETRN
 Ошибка синтаксиса ETRN в параметрах
 сбой EVP_md5()!
 Введите пароль для %s@%s:  Ошибка создания запроса уровня безопасности
 Ошибка удаления %s: %s
 Ошибка обмена идентификационными данными
 Ошибка создания идентификационных данных
 Ошибка записи в MDA: %s
 Ошибка записи в fetchids-файл %s, старый файл остался на месте.
 Fetchmail поставляется АБСОЛЮТНО БЕЗО ВСЯКОЙ ГАРАНТИИ. Это свободное
программное обеспечение и вы можете распространять его далее при
соблюдении определённых условий. Подробности смотрите в файле COPYING
в каталоге с документацией или исходными текстами.
 Fetchmail не смог получить почту из %s@%s.
 Fetchmail обнаружил более %d тайм-аутов при попытке получения почты из %s@%s.
 Fetchmail вошел в систему %s@%s.
 Fetchmail направит ошибочное письмо постмастеру.
 Fetchmail направит ошибочное письмо отправителю.
 Fetchmail будет пересылать многоканальные сообщения без адреса на %s.
 Fetchmail включит маскировку и не создаст поле Received
 Fetchmail будет отображать точечную строку прогресса даже в log-файлах.
 Fetchmail рассматривает долговременные ошибки как постоянные (удалять сообщения).
 Fetchmail будет считать постоянные ошибки временными (сохранит сообщения).
 Файл %s должен быть обычным файлом.
 Владельцем файла %s должны быть вы.
 Файл %s должен иметь прав доступа не более -rwx------ (0700).
 Дескриптор файла вне диапазона SSL Для справки смотрите http://www.fetchmail.info/fetchmail-FAQ.html#R15
 Ошибка GSSAPI %s: %.*s
 Поддержка GSSAPI настроена, но не была включена при компиляции.
 Получение ответа
 Получение обратного ответа %d [%s]
 Заголовок не 60
 Поддержка IMAP не настроена.
 ID-файл - %s
 Входящие двоичные данные:
 Недопустимая временная метка APOP.
 Указан неверный протокол SSL '%s', используется протокол по умолчанию (SSLv23).
 Указана неверная аутентификация `%s'.
 Указан неверный протокол `%s'.
 Неверный id пользователя или идентификационная фраза Общепринятое имя: %s
 Запрашивающая организация: %s
 Поддержка KERBEROS v4 настроена, но не была включена при компиляции.
 Поддержка KERBEROS v5 настроена, но не была включена при компиляции.
 Поддержка Kerberos V4 не связана.
 Поддержка Kerberos V5 не связана.
 Ошибка доставки LMTP на EOM
 Ведущий сервер без имени.
 Ошибка блокировки занятости для %s@%s
 Лог-файл - %s
 MD5 применяется к блоку данных:
 MDA MDA завершен по сигналу %d
 Не удалось открыть MDA
 MDA возвратил ненулевой статус %d
 Максимальный размер маркера GSS - %ld
 Поле механизма неверно
 Объединенный список UID из %s: Сообщения добавлены в список на сервере. Обработка невозможна.
 Новый список UID из %s: Не найден IP-адрес для %s Не найден интерфейс с именем %s Для %s почты нет
 Нет настроенных почтовых серверов -- может отсутствует %s?
 Нет сообщений, ожидающих %s
 Нет, их IP-адреса не совпадают
 Узел %s запрещен: %s
 Поддержка ODMR не настроена.
 Старый список UID из %s: Опция --all не поддерживается с %s
 Опция --check не поддерживается с ETRN
 Опция --check не поддерживается с ODMR
 Опция --flush не поддерживается с %s
 Опция --flush не поддерживается с ETRN
 Опция --flush не поддерживается с ODMR
 Опция --folder не поддерживается с ETRN
 Опция --folder не поддерживается с ODMR
 Опция --folder не поддерживается с POP3
 Опция --keep не поддерживается с ETRN
 Опция --keep не поддерживается с ODMR
 Опция --limit не поддерживается с %s
 Параметры для получения из %s@%s:
 Нехватка памяти!
 Исходящие данные:
 Поддержка POP2 не настроена.
 Поддержка POP3 не настроена.
 Запущено ожидание сообщений для %s
 Укажите службу в виде десятичного номера порта.
 Интервал опроса - %d секунд
 Опрашивается %s
 Обработка сообщений будет регистрироваться через syslog
 Протокол идентифицирован как IMAP2 или IMAP2BIS
 Протокол идентифицирован как IMAP4 ревизия 0
 Протокол идентифицирован как IMAP4 ревизия 1
 Статус запроса=%d
 Статус запроса=0 (SUCCESS)
 Статус запроса=1 (NOMAIL)
 Статус запроса=10 (SMTP)
 Статус запроса=11 (DNS)
 Статус запроса=12 (BSMTP)
 Статус запроса=13 (MAXFETCH)
 Статус запроса=2 (SOCKET)
 Статус запроса=3 (AUTHFAIL)
 Статус запроса=4 (PROTOCOL)
 Статус запроса=5 (SYNTAX)
 Статус запроса=6 (IOERR)
 Статус запроса=7 (ERROR)
 Статус запроса=8 (EXCLUDE)
 Статус запроса=9 (LOCKBUSY)
 Запущено формирование очереди для %s
 RPA не смог открыть /dev/urandom. Это не должно было
 Ошибка длины ключа сеанса RPA: %d
 Строка RPA слишком длинная
 Ошибка продолжительности аутентификации пользователя RPA: %d
 Сбой RPA _service_ auth. Ложный сервер?
 Авторизация RPA завершена
 Ошибка RPA в строке служба@область
 RPA отверг вас, причина неизвестна
 RPA отверг вас: %s
 Статус RPA: %02X
 Ошибка длины RPA token 2
 RPA token 2: ошибка декодирования Base64
 Ошибка длины RPA token 4
 RPA token 4: ошибка декодирования Base64
 Список области: %s
 Получен ответ BYE от сервера IMAP: %s Создаются идентификационные данные GSS
 Немедленно повторить опрос для %s@%s
 В приветствии не найдена требуемая временная метка APOP
 Требуемая опция LOGIN не поддерживается сервером
 Fetchmail скомпилирован без требуемой поддержки NTLM
 Fetchmail скомпилирован без требуемой поддержки OTP
 Ограниченный пользователь (что-то не так с учетной записью) Версия маршрутизируемого сообщения %d не понятна. SDPS не включен. Слушатель SMTP отказал в доставке
 Слушатель SMTP отверг адреса локальных получателей:  Для SMTP-сервера требуется STARTTLS; сообщение сохранено.
 SMTP-транзакция SMTP: (тело сообщения-рикошета)
 Сбой подключения через SSL.
 SSL не включен Поддержка SSL не была включена при компиляции.
 Сохраненная ошибка все еще %d
 Временный список UID'ов: Секретная идентификационная фраза:  Отправляются идентификационные данные
 Несовпадение общепринятого имени сервера: %s != %s
 Ошибка занятости сервера для %s@%s
 Ошибка проверки сертификата сервера: %s
 Имя сервера не установлено, невозможно проверить сертификат!
 Имя сервера не указано в сертификате!
 Сервер отверг команду AUTH.
 Серверу требуются целостность и/или конфиденциальность
 Сервер возвратил ответ с UID для неверного сообщения.
 Запрос службы (l=%d):
 Служба выбрала RPA версии %d.%d
 Служба была восстановлена.
 Временная метка службы %s
 Сеансовый ключ признан:
 Странно: MDA pclose возвратил %d и ошибку с номером %d/%s, невозможно обработать в %s:%d
 Строка '%s' не является верной числовой строкой.
 Тема: Предупреждение Fetchmail о превышении размера сообщений Тема: Выполнена авторизация fetchmail для %s@%s Тема: Ошибка аутентификации fetchmail для %s@%s Тема: fetchmail наблюдает повторяющиеся тайм-ауты Успешно Для этого сеанса требуется использовать TLS, однако сервер отклонил команду CAPA.
 Опции берутся из командной строки %s%s
 Команда CAPA необходима для TLS.
 Не удалось пройти авторизацию.
Так как, для этого подключения авторизация уже была успешно выполнена,
возможно, возникла другая ошибка (например, сервер занят), которую
fetchmail не смог распознать из-за того, что сервер не отправил
сообщение об ошибке. Не удалось пройти  авторизацию.
Возможно, это означает, что ваш пароль неверен, но
некоторые серверы имеют другие режимы отказов, которые
fetchmail не может выделить из этой ошибки, потому что они
не отправляют нужных сообщений об ошибке при неудачном
входе в систему.

Демон fetchmail продолжит свою работу и будет пытаться
подключиться в каждом цикле. В дальнейшем уведомления
отправляться не будут до тех пор, пока служба не будет
восстановлена. Следующие сообщения завышенного размера находятся на почтовом сервере %s, учётная запись %s: Следующие сообщения завышенного размера были удалены с почтового сервера %s, учётная запись %s: Это может означать, что возникла пробка на вашем почтовом
сервере или на вашем SMTP-сервере, или ваш файл почтового
ящика на сервере был поврежден из-за ошибки на сервере.
Вы можете запустить `fetchmail -v -v', чтобы провести
диагностику проблемы.

Fetchmail не будет опрашивать этот почтовый ящик до тех
пор, пока вы его не перезапустите.
 Это fetchmail версии %s В приветствии ошибка синтаксиса временной метки
 Длина маркера %d не совпадает с rxlen %d
 Ошибка длины маркера
 Попробуйте добавить опцию --service (см. FAQ, пункт R12).
 Попытка подключения к %s/%s... Пытаемся продолжить с неопределённым именем хоста.
НЕ СООБЩАЙТЕ о нарушенных заголовках Received:, строках HELO/EHLO или подобных проблемах!
Вместо этого ИСПРАВЬТЕ свой /etc/hosts, DNS, NIS или LDAP.
 Реверсирование передачи...
 Невозможно открыть интерфейс kvm. Убедитесь, что fetchmail является SGID kmem. Невозможно извлечь имя интерфейса из %s Невозможно сейчас обработать запрос ATRN
 Невозможно поставить в очередь сообщения для узла %s
 Неопределённый запрос протокола в POP3_auth
 Неожиданный не-503 ответ для LMTP EOM: %s
 Уникод:
 Неизвестная ошибка ETRN %d
 Неизвестное общепринятое имя
 Неизвестная организация
 Неизвестное общепринятое имя сервера
 Неизвестный логин или ошибка аутентификации для %s@%s
 Неизвестная системная ошибка Флаги развернутого уровня безопасности: %s%s%s
 Аутентификация пользователя (l=%d):
 Запрос пользователя:
 Используется имя службы [%s]
 Значение строки '%s' - %s, чем %d.
 ПРЕДУПРЕЖДЕНИЕ. Запуск под root'ом не рекомендуется.
 Предупреждение: общепринятое имя слишком длинное (возможно укорочено).
 Предупреждение: имя запрашивающей организации слишком длинное (возможно укорочено).
 Предупреждение: игнориованы фиктивные данные о размере сообщений, которые вернул сервер
 Предупреждение: многократные ссылки на хост %s в файле настройки
 Закончились допустимые аутентификаторы, продолжение невозможно.
 Записывается файл fetchids.
 Да, их IP-адреса совпадают
 Для вас почты нет.
 будет отправлен с: %s
 активность на %s -отмечена- как %d
 активность на %s проверена как %d
 активность на %s была %d, %d
 анализируется строка Received:
%s сбой перезапуска fetchmail
 возможен сбой при перезапуске, т.к. каталог не был восстановлен
 разбужен на %s
 разбужен %s'ом
 разбужен по сигналу %d
 фоновый режим фиктивное количество сообщений! даже невозможно отправить на %s!
 невозможно запустить слушателя; откат на %s невозможно создать сокет: %s
 несовпадение запроса
 протокол клиент/сервер синхронизация клиент/сервер соединение установлено.
 не удалось подключиться.
 сбой подключения к %s:%s [%s/%s]: %s.
 невозможно было декодировать запрос BASE64
 невозможно было декодировать ответ BASE64 о готовности
 невозможно было декодировать начальный запрос BASE64
 невозможно было получить текущий рабочий каталог
 не могу открыть %s для добавления в журнал
 невозможно было извлечь заголовки, сообщение %s@%s:%d (%d октетов)
 невозможно было найти HESIOD pobox для %s
 невозможно было найти каноническое имя DNS %s (%s): %s
 невозможно было проверить время %s (ошибка %d)
 невозможно было проверить время файла контроля запуска
 Ошибка dec64 в символе %d: %x
 декодирован как %s
 отбрасывается новый список UID
 сбой dup2
 конец ввода ошибка записи текста сообщения
 сбой execvp(%s)
 не удалось удалить
 Достигнут предел получения %d; %d сообщение осталось на сервере %s ящик %s
 Достигнут предел получения %d; %d сообщения осталось на сервере %s ящик %s
 Достигнут предел получения %d; %d сообщений осталось на сервере %s ящик %s
 fetchmail: конфигурация %s не верна, для RPOP требуется привилегированный порт
 fetchmail: конфигурация %s не верна, укажите положительный номер порта для службы
 fetchmail: %s fetchmail %ld уничтожен.
 fetchmail: невозможно выполнить переход в фоновый режим. Прерывание.
 fetchmail: Ошибка: многократные записи "defaults" в файле настройки.
 fetchmail: другой fetchmail запущен как %ld.
 fetchmail: фоновый fetchmail %ld пробуждён.
 fetchmail: невозможно применить опции, пока запущен фоновый fetchmail.
 fetchmail: невозможно проверить почту, пока на том же хосте запущен другой fetchmail.
 fetchmail: невозможно найти пароль для %s@%s.
 fetchmail: не могу опросить указанные хосты с другой запущенной копией fetchmail %ld.
 fetchmail: ошибка при уничтожении %s fetchmail %ld; выходим.
 fetchmail: ошибка открытия файла блокировки "%s": %s
 fetchmail: ошибка чтения файла блокировки "%s": %s
 fetchmail: не удалось создать дочерний процесс
 fetchmail: опция interface поддерживается только в Linux (без IPv6) и FreeBSD
 fetchmail: запущен с fetchmail: не удалось установить блокировку.
 fetchmail: сбой malloc
 fetchmail: опция monitor поддерживается только в Linux (без IPv6) и FreeBSD
 fetchmail: не были указаны почтовые серверы.
 fetchmail: другие запущенные fetchmail отсутствуют
 fetchmail: удаляется старый файл блокировки
 fetchmail: сбой socketpair
 fetchmail: поток приостановлен в течение %d секунд.
 fetchmail: предупреждение: не доступен DNS для проверки многоканальной выборки из %s
 приоритетный режим переадресация и удаление приостановлены из-за ошибок DNS
 переадресовывается на %s
 найден адрес Received `%s'
 get_ifinfo: сбой malloc get_ifinfo: сбой sysctl (оценка iflist) get_ifinfo: сбой sysctl (iflist) ошибка getaddrinfo("%s","%s"): %s
 ошибка getaddrinfo(NULL, "%s"): %s
 сбой gethostbyname для %s
 id=%s (номер=%d) было удалено, но все еще присутствует!
 интервал не достигнут, %s не запрашивается
 неверный адрес IP-интерфейса
 неверная маска IP-интерфейса
 ошибка kerberos %s
 krb5_sendauth: %s [сервер ответил '%s']
 больше строка принята, %s является алиасом почтового сервера
 строка отвергнута, %s не является алиасом почтового сервера
 строка: %s блокировка занятости на сервере блокировка занятости!  Может активен другой сеанс?
 разногласие при удалении писем (%d на самом деле != %d ожидалось)
 письмо от %s отправлено рикошетом на %s
 не удалось выбрать почтовый ящик
 сбой malloc
 установлено соответствие %s локальному %s
 установлено соответствие адреса %s локальному %s
 длина сообщения %s@%s:%d не совпала с ожидаемой (%d на самом деле != %d ожидалось)
 сообщение содержит вложенные NUL отсутствует адрес IP-интерфейса
 отсутствует или неверный заголовок RFC822 Выполняя поиск `%s', nameserver возвратил ошибку во время опроса %s: %s.
 Выполняя поиск `%s', nameserver возвратил ошибку во время опроса %s.
 адрес Received не найден
 нет сопоставленных адресов; переадресовывается на %s.
 нет сопоставленных адресов; постмастер не установлен.
 нет локальных совпадений, переадресовывается на %s
 нет адресов получателей, соответствующих объявленным локальным именам ненулевая копия (%s) может вызвать странное поведение
 нормальное завершение, статус %d
 списки UID не обмениваются, в этом запросе нет UID'ов
 пройден через %s соответствующий %s
 опрос %s пропущен (ошибка аутентификации или слишком много тайм-аутов)
 сбой команды, выполняемой после соединения; статус: %d
 выполняемая после соединения команда завершена с сигналом %d
 сбой команды, выполняемой до соединения; статус: %d
 выполняемая до соединения команда завершена с сигналом %d
 принципал %s в мандате не совпадает с -u %s
 ошибка протокола
 ошибка протокола при выборке UIDL'ов
 не удалось повторить опрос
 считывается сообщение %s@%s:%d из %d сбой realloc
 принимаются данные сообщения
 адрес получателя %s не соответствует ни одному локальному имени fetchmail перезапускается (%s изменен)
 запущен %s (хост %s служба %s)
 сбой поиска непросмотренных сообщений
 просмотрено просмотрено просмотрено выбирается или повторяется опрос каталога по умолчанию
 выбирается или повторяется опрос каталога %s
 опция сервера после опций пользователя сервер прекратил получение
 пропускается сообщение %s@%s:%d пропускается сообщение %s@%s:%d (%d октетов) пропускается опрос %s, %s IP-адрес исключен
 пропускается опрос %s, %s отключен
 пропускается опрос %s, %s отключен
 в спящем режиме в %s на %d секунд
 меньше ошибка протокола слушателя smtp
 сокет Запуск fetchmail %s демона
 обмениваются списки UID
 завершен по сигналу %d
 таймаут после %d секунд ожидания %s.
 таймаут после %d секунд ожидания ответа от слушателя.
 таймаут после %d секунд ожидания сервера %s.
 таймаут после %d секунд ожидания подключения к серверу %s.
 таймаут после %d секунд.
 не определено неизв. неизв. (%s) выбран неподдерживаемый протокол.
 использование:  fetchmail [опции] [сервер ...]
 предупреждение: Не просите технической поддержки, если все сообщения отправляются постмастеру!
 предупреждение: multidrop для %s требует опцию envelope!
 будет приостановлен после опроса
 записываются заголовки RFC822 msgblk
 