��    �      L  �   |
      �  7   �     1     ?     X     u  /   �  '   �     �  .   �     .     C  "   O  #   r     �      �      �  *   �  3        K     j  !   ~  C   �  $   �  )   	     3  %   M  "   s  #   �     �     �     �  F   �     ?     ^  `   q  "   �  &   �  0     +   M     y      �     �     �  $   �  "   �  )        I  4   i  5   �  E   �  /     .   J  &   y  (   �  +   �     �       6   .  !   e  "   �     �     �     �     �               <     W     q     �     �     �     �     �  #         ;     \     {     �  (   �     �     �  2   �  #     +   B     n  &   �     �     �     �     �       *   &  C   Q  3   �  0   �  %   �        %   ?     e  +        �  "   �     �     �          #     =  ,   V  3   �  /   �     �     �  (        :  (   N  +   w     �     �  '   �          "     ?     \     {     �     �     �     �  '        +  $   F  2   k      �     �  ,   �  -         5   	   C      M      i   !   |   &   �      �   !   �      !     !  0   &!  !   W!     y!     �!     �!     �!  %   �!  F   �!     /"     F"     b"     |"  "   �"     �"     �"     �"  <   �"  -   :#  �  h#  u   !%     �%  0   �%  4   �%     &  h   /&  ]   �&  4   �&  \   +'  )   �'     �'  O   �'  J   (  '   `(  B   �(  E   �(  >   )  Q   P)  0   �)  "   �)  3   �)  �   **  C   �*  J   �*  *   F+  K   q+  D   �+  j   ,  ?   m,  7   �,  &   �,  �   -  I   �-     �-  �   �-  8   �.  \   �.  N   G/  J   �/     �/  D   �/  =   -0      k0  .   �0  S   �0  K   1  9   [1  d   �1  �   �1  �   {2  y   3  \   �3  =   �3  ?   (4  V   h4  L   �4  K   5  d   X5  U   �5  i   6  ,   }6  ,   �6  *   �6  ,   7  /   /7  2   _7  0   �7  /   �7  /   �7  0   #8  4   T8  /   �8  3   �8  @   �8  R   .9  2   �9  0   �9     �9  :   �9  W   7:  >   �:  /   �:  S   �:  S   R;  c   �;  1   
<  o   <<  )   �<     �<  5   �<  B   #=  7   f=  d   �=  �   >  �   �>  �   F?  N   �?  2   !@  Q   T@  8   �@  A   �@  *   !A  C   LA  -   �A  !   �A  2   �A  :   B  ?   NB  H   �B  W   �B  I   /C  &   yC  <   �C  P   �C  &   .D  n   UD  c   �D  E   (E  4   nE  \   �E  +    F  I   ,F  >   vF  A   �F  F   �F  >   >G  E   }G  9   �G  .   �G  H   ,H  2   uH  6   �H  ]   �H  R   =I  .   �I  @   �I  H    J  %   IJ     oJ  )   �J  (   �J  L   �J  \   (K  +   �K  Z   �K     L  6   'L  i   ^L  @   �L     	M  ,   #M  1   PM     �M  Z   �M  �   �M  .   �N  K   �N  8   O  8   XO  N   �O  )   �O  5   
P  +   @P  �   lP  h   �P         %   Y       ~   C            �       9   F   "   1           \          c   B   !   )   �   T   l   :   &           `   S      �                -   �      +          @   H   �   n   �      h   �                     y   Z   8   �      D   �   �   |           e       m   2       ;   X           O   k       �   �           b      <   �   N   Q   =   �          �      (   ^      �   q          ]          
           �      �           *         _   J          ,       0   6   M       L   �               �   w   �   �           z   '              �   ?   a   I       d   }   U       /       A   	      �   G               v   V   i   �   P   o   3           �   s   7   �   x   �       4   R              u   >   K   �   p   [       �   {      �   �   r   f   �       5   g       �   W           E       �   t   �       .   �   �   j   #   �          $    

********** beginning dump of nfa with start state %d
 

DFA Dump:

 

Equivalence Classes:

 

Meta-Equivalence Classes:
 
 jam-transitions: EOF    %d (%d saved) hash collisions, %d DFAs equal
   %d backing-up (non-accepting) states
   %d empty table entries
   %d epsilon states, %d double epsilon states
   %d protos created
   %d rules
   %d sets of reallocations needed
   %d state/nextstate pairs created
   %d table entries
   %d templates created, %d uses
   %d total table entries needed
   %d/%d (peak %d) nxt-chk entries created
   %d/%d (peak %d) template nxt-chk entries created
   %d/%d DFA states (%d words)
   %d/%d NFA states
   %d/%d base-def entries created
   %d/%d character classes needed %d/%d words of storage, %d reused
   %d/%d equivalence classes created
   %d/%d meta-equivalence classes created
   %d/%d start conditions
   %d/%d unique/duplicate transitions
   Beginning-of-line patterns used
   Compressed tables always back-up
   No backing up
   no character classes
   scanner options: -  and may be the actual source of other reported performance penalties
  associated rule line numbers:  out-transitions:  %%option yylineno entails a performance penalty ONLY on rules that can match newline characters
 %array incompatible with -+ option %d backing up (non-accepting) states.
 %option yyclass only meaningful for C++ scanners %option yylineno cannot be used with REJECT %s %s
 %s version %s usage statistics:
 %s: fatal internal error, %s
 ********** end of dump
 *Something Weird* - tok: %d val: %d
 -Cf and -CF are mutually exclusive -Cf/-CF and -Cm don't make sense together -Cf/-CF and -I are incompatible -Cf/-CF are incompatible with lex-compatibility mode -I (interactive) entails a minor performance penalty
 -l AT&T lex compatibility option entails a large performance penalty
 -s option given but default rule can be matched Allocation of buffer for line directive failed Allocation of buffer for m4 def failed Allocation of buffer for m4 undef failed Allocation of buffer to print string failed Can't use -+ with -CF option Can't use -+ with -l option Can't use --reentrant or --bison-bridge with -l option Can't use -f or -F with -l option Compressed tables always back up.
 Could not write ecstbl Could not write eoltbl Could not write ftbl Could not write ssltbl Could not write yyacc_tbl Could not write yyacclist_tbl Could not write yybase_tbl Could not write yychk_tbl Could not write yydef_tbl Could not write yymeta_tbl Could not write yynultrans_tbl Could not write yynxt_tbl Could not write yynxt_tbl[][] Definition name too long
 Definition value for {%s} too long
 EOF encountered inside an action EOF encountered inside pattern End Marker
 Input line too long
 Internal error. flexopts are malformed.
 No backing up.
 Option line too long
 Options -+ and --reentrant are mutually exclusive. REJECT cannot be used with -f or -F REJECT entails a large performance penalty
 State #%d is non-accepting -
 Try `%s --help' for more information.
 Unknown error=(%d)
 Unmatched '{' Unrecognized option `%s'
 Usage: %s [OPTIONS] [FILE]...
 Usage: %s [OPTIONS]...
 Variable trailing context rule at line %d
 Variable trailing context rules entail a large performance penalty
 [:^lower:] is ambiguous in case insensitive scanner [:^upper:] ambiguous in case insensitive scanner allocation of macro definition failed allocation of sko_stack failed attempt to increase array size failed bad <start condition>: %s bad character '%s' detected in check_char() bad character class bad character class expression: %s bad character inside {}'s bad character: %s bad iteration values bad line in skeleton file bad start condition list bad state type in mark_beginning_as_normal() bad transition character detected in sympartition() bison bridge not supported for the C++ scanner. can't open %s can't open skeleton file %s consistency check failed in epsclosure() could not create %s could not create backing-up info file %s could not create unique end-of-buffer state could not write tables header dangerous trailing context dynamic memory failure in copy_string() empty machine in dupmachine() error closing backup file %s error closing output file %s error closing skeleton file %s error creating header file %s error deleting output file %s error writing backup file %s error writing output file %s fatal parse error found too many transitions in mkxtion() incomplete name definition input error reading skeleton file %s input rules are too complicated (>= %d NFA states) iteration value must be positive malformed '%top' directive memory allocation failed in allocate_array() memory allocation failed in yy_flex_xmalloc() missing quote missing } name "%s" ridiculously long name defined twice negative range in character class option `%s' doesn't allow an argument
 option `%s' is ambiguous
 option `%s' requires an argument
 premature EOF rule cannot be matched scanner requires -8 flag to use the character %s start condition %s declared twice state # %4d	 state # %d accepts:  state # %d accepts: [%d]
 state # %d:
 symbol table memory allocation failed the character range [%c-%c] is ambiguous in a case-insensitive scanner too many rules (> %d)! trailing context used twice undefined definition {%s} unknown -C option '%c' unknown error processing section 1 unrecognized %%option: %s unrecognized '%' directive unrecognized rule variable trailing context rules cannot be used with -f or -F yymore() entails a minor performance penalty
 Project-Id-Version: flex 2.5.38
Report-Msgid-Bugs-To: flex-devel@lists.sourceforge.net
POT-Creation-Date: 2015-11-17 11:17-0500
PO-Revision-Date: 2016-02-29 05:45+0000
Last-Translator: Yuri Kozlov <yuray@komyakino.ru>
Language-Team: Russian <gnu@mx.ru>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:21+0000
X-Generator: Launchpad (build 18115)
Language: ru
 

******** начало дампа конечного автомата с начальным состоянием %d
 

Дамп ДКА:

 

Классы эквивалентности:

 

Мета-эквивалентные Классы:
 
 jam-переходы: EOF    %d (%d записано) коллизий хэш-таблицы, %d эквивалентных ДКА
   резервное копирование %d (недопустимых) состояний
   %d пустых элементов таблицы
   %d эпсилон-состояний, %d двойных эпсилон-состояний
   создано %d прототипов
   %d правил
   требуется %d наборов повторных размещений
   создано %d пар состояние/след_состояние
   %d элементов таблицы
   создано %d шаблонов, %d используются
   всего требуется %d элементов таблицы
   создано %d/%d (пик %d) nxt-chk элементов
   создано %d/%d (пик %d) шаблонных nxt-chk элементов
   %d/%d состояний ДКА (%d слов)
   %d/%d состояний НКА
   создано %d/%d base-def элементов
   %d/%d классов символов требуют %d/%d слов для хранения, %d повторно использовано
   созданы классы эквивалентности %d/%d
   %d/%d мета-эквивалентных классов создано
   %d/%d начальных условий
   %d/%d уникальных/повторяющихся переходов
   Используются шаблоны начала строки
   Резервное копирование сжатых таблиц выполняется всегда
   Резервное копирование отключено
   отсутствуют классы символов
   параметры сканера: -  и также может быть истинным источником проблем с производительностью
  номера строк ассоциированного правила:  out-переходы:  %%option yylineno вызывает снижение производительности ТОЛЬКО на правилах, проверяющих символы перевода строк
 %array несовместим с параметром -+ резервное копирование %d (недопустимых) состояний.
 %option yyclass имеет смысл только для сканеров C++ %option yylineno не может быть использован с REJECT %s %s
 статистика использования %s версии %s:
 %s: фатальная внутренняя ошибка, %s
 ********* конец дампа
 *Что-то не так* — tok: %d val: %d
 Параметры -Cf и -CF являются взаимоисключающими Параметры -Cf/-CF и -Cm вместе не имеют смысла Параметры -Cf/-CF и -I несовместимы Параметры -Cf/-CF несовместимы с режимом lex-совместимости -I (интерактивный) влечёт незначительное снижение производительности
 Параметр -l совместимости с AT&T lex влечёт значительное снижение производительности
 указан параметр -s, но правило по умолчанию не может быть применено Не удалось выделить буфер для строковой директивы Не удалось выделить буфер для m4 def Не удалось выделить буфер для m4 undef Не удалось выделить буфер для выводимой строки Невозможно использовать -+ с параметром -CF Невозможно использовать -+ с параметром -l Использование --reentrant или --bison-bridge с ключем -l невозможно Невозможно использовать -f или -F с параметром -l Резервное копирование сжатых таблиц выполняется всегда.
 Невозможно записать ecstbl Невозможно записать eoltbl Невозможно записать ftbl Невозможно записать ssltbl Невозможно записать yyacc_tbl Не удается записать yyacclist_tbl Невозможно записать yybase_tbl Невозможно записать yychk_tbl Невозможно записать yydef_tbl Невозможно записать yymeta_tbl Невозможно записать yynultrans_tbl Невозможно записать yynxt_tbl Невозможно записать yynxt_tbl[][] Слишком длинное определение имени
 Слишком длинное определение значения для {%s}
 встречен EOF внутри действия встречен EOF внутри шаблона Метка конца
 Слишком длинная входная строка
 Внутренняя ошибка. Неправильное значение flexopts.
 Резервное копирование отключено.
 Слишком длинный параметр
 Ключи -+ и --reentrant являются взаимоисключающими. REJECT не может быть использован вместе с -f или -F REJECT влечёт значительное снижение производительности
 Состояние #%d не допускает -
 Попробуйте «%s --help» для получения более подробного описания.
 Неизвестная ошибка=(%d)
 Непарная «{» Нераспознанный параметр «%s»
 Использование: %s [ПАРАМЕТРЫ] [ФАЙЛ]…
 Использование: %s [ПАРАМЕТРЫ]…
 Правило с переменным замыкающим контекстом в строке %d
 Правила с переменным замыкающим контекстом приводят к значительному снижению производительности
 использование [:^lower:] сомнительно для сканера, не чувствительного к регистру использование [:^upper:] сомнительно для сканера, не чувствительного к регистру Не удалось разместить определение макроса не удалось разместить sko_stack ошибка при попытке увеличить размер массива неверное <начальное условие>: %s найден неверный символ «%s» в check_char() неверный класс символа неверное выражение класса символа: %s неверный символ внутри {} неверный символ: %s неверные значения итераций неверная строка в файле-каркасе неверный список начальных условий неверный тип состояния в mark_beginning_as_normal() обнаружен неверный переходный символ в sympartition() bison bridge не поддерживается для сканера C++. невозможно открыть %s Невозможно открыть файл-каркас %s ошибка при проверке на целостность в epsclosure() невозможно создать %s невозможно создать резервную копию информационного файла %s невозможно создать уникальное состояние конца буфера Невозможно записать заголовок таблиц опасный замыкающий контекст ошибка при работе с динамической памятью в copy_string() пустой автомат в dupmachine() ошибка закрытия резервной копии файла %s ошибка закрытия выходного файла %s ошибка при закрытии файла-каркаса %s ошибка создания заголовочного файла %s ошибка удаления выходного файла %s ошибка записи резервной копии файла %s ошибка записи в выходной файл %s фатальная ошибка разбора найдено слишком много переходов в mkxtion() неполное определение имени ошибка чтения файла-каркаса %s входные правила слишком сложные (>= %d состояний НКА) значение итераций должно быть положительным искаженная директива '%top' ошибка выделения памяти в allocate_array() ошибка при выделении памяти в yy_flex_xmalloc() отсутствуют кавычки отсутствует } имя «%s» нелепо длинное имя определено дважды отрицательный диапазон в классе символов параметр «%s» должен использоваться без аргумента
 неоднозначный ключ «%s»
 параметр «%s» должен использоваться с аргументом
 неожиданный EOF невозможно применить правило для использования символа %s сканеру требуется параметр -8 начальное условие %s описано дважды состояние # %4d	 состояние # %d допускает:  состояние # %d допускает: [%d]
 состояние # %d:
 ошибка при выделении памяти для таблицы символов использование символьного диапазона [%c-%c] сомнительно в сканере, не чувствительном к регистру слишком много правил (> %d)! замыкающий контекст используется дважды неопределенное определение {%s} неизвестное значение «%c» для -C неизвестная ошибка при обработке раздела 1 нераспознанный %%option: %s нераспознанная директива «%» нераспознанное правило правила с переменным замыкающим контекстом не могут быть использованы с -f или -F yymore() приводит к небольшому снижению производительности
 