��    F      L  a   |                      	                     '  ,   4     a  &   s     �     �  $   �     �                    '     3  
   :     E     T     \  	   m     w  
   |     �     �     �     �     �  �   �    �     �
     �
     �
     �
     �
             )   7     a  	   g  
   q  �   |  �  -  E  �  �  %  �   �  �  �  ]     
   t       
   �  �   �  +   /     [  �   d     �          $     C     `  ,   y     �     �     �  D  �  �    �  �     @     B     X     i  
   ~     �     �  J   �  )   �  M   '  7   u  ,   �  Q   �  5   ,     b     }  $   �     �     �  '   �  #   �       #   #  %   G     m  
   ~  '   �  /   �     �     �       �   /         #     $#  ,   :#  <   g#     �#  :   �#  2   �#  [   2$     �$  )   �$     �$  `  �$  }  C&  m  �(    /+  b  >.    �/  �   �2     @3     `3     w3     �3  i   �4     5    65  &   F6  "   m6  1   �6  1   �6  '   �6  K   7     h7     �7     �7  >  �7  �  �9                  @          :   <   7       4                  ?          %       ;   9           >         *            
   )   .   0      1          E   /       "   F      2                             D          A   -       5                       +          '   $       #         C   6              (   =          ,          B   &   3          	   !   8      About Add Application Browse Config Configure... Could not construct a property list for (%s) Could not load %s Could not write property list for (%s) Could not write to %s Could not write to (%s) Couldn't create pixmap from file: %s Couldn't find pixmap file: %s Credits DSN Database System Description Driver Driver Lib Driver Manager Drivers Enter a DSN name FileUsage Name ODBCConfig ODBCConfig - Credits ODBCConfig - Database Systems ODBCConfig - Drivers ODBCConfig - odbc.ini ODBCConfig - odbcinst.ini Open DataBase Connectivity (ODBC) was developed to be an Open and portable standard for accessing data. unixODBC implements this standard for Linux/UNIX. Perhaps the most common type of Database System today is an SQL Server

SQL Servers with Heavy Functionality
  ADABAS-D
  Empress
  Sybase - www.sybase.com
  Oracle - www.oracle.com

SQL Servers with Lite Functionality
  MiniSQL
  MySQL
  Solid

The Database System may be running on the local machine or on a remote machine. It may also store its information in a variety of ways. This does not matter to an ODBC application because the Driver Manager and the Driver provides a consistent interface to the Database System. Remove Select File Select a DSN to Remove Select a DSN to configure Select a driver to Use Select a driver to configure Select a driver to remove Select the DRIVER to use or Add a new one Setup Setup Lib System DSN System data sources are shared among all users of this machine.These data sources may also be used by system services. Only the administrator can configure system data sources. The Application communicates with the Driver Manager using the standard ODBC calls.

The application does not care; where the data is stored, how it is stored, or even how the system is configured to access the data.

The Application only needs to know the data source name (DSN)

The Application is not hard wired to a particular database system. This allows the user to select a different database system using the ODBCConfig Tool. The Driver Manager carries out a number of functions, such as:
1. Resolve data source names via odbcinst lib)
2. Loads any required drivers
3. Calls the drivers exposed functions to communicate with the database. Some functionality, such as listing all Data Source, is only present in the Driver Manager or via odbcinst lib). The ODBC Drivers contain code specific to a Database System and provides a set of callable functions to the Driver Manager.
Drivers may implement some database functionality when it is required by ODBC and is not present in the Database System.
Drivers may also translate data types.

ODBC Drivers can be obtained from the Internet or directly from the Database vendor.

Check http://www.unixodbc.org for drivers These drivers facilitate communication between the Driver Manager and the data server. Many ODBC drivers  for Linux can be downloaded from the Internet while others are obtained from your database vendor. This is the main configuration file for ODBC.
It contains Data Source configuration.

It is used by the Driver Manager to determine, from a given Data Source Name, such things as the name of the Driver.

It is a simple text file but is configured using the ODBCConfig tool.
The User data sources are typically stored in ~/.odbc.ini while the System data sources are stored in /etc/odbc.ini
 This is the program you are using now. This program allows the user to easily configure ODBC. Trace File Tracing Tracing On Tracing allows you to create logs of the calls to ODBC drivers. Great for support people, or to aid you in debugging applications.
You must be 'root' to set Unable to find a Driver line for this entry User DSN User data source configuration is stored in your home directory. This allows you configure data access without having to be system administrator gODBCConfig - Add DSN gODBCConfig - Appication gODBCConfig - Configure Driver gODBCConfig - Driver Manager gODBCConfig - New Driver gODBCConfig - ODBC Data Source Administrator http://www.unixodbc.org odbc.ini odbcinst.ini odbcinst.ini contains a list of all installed ODBC Drivers. Each entry also contains some information about the driver such as the file name(s) of the driver.

An entry should be made when an ODBC driver is installed and removed when the driver is uninstalled. This can be done using ODBCConfig or the odbcinst command tool. unixODBC consists of the following components

- Driver Manager
- GUI Data Manager
- GUI Config
- Several Drivers and Driver Config libs
- Driver Code Template
- Driver Config Code Template
- ODBCINST lib
- odbcinst (command line tool for install scripts)
- INI lib
- LOG lib
- LST lib
- TRE lib
- SQI lib
- isql (command line tool for SQL)

All code is released under GPL and the LGPL license.
 Project-Id-Version: unixodbc
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2003-12-02 14:45+0000
PO-Revision-Date: 2013-01-14 17:55+0000
Last-Translator: Sergey Loshakov <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:46+0000
X-Generator: Launchpad (build 18115)
   О программе Добавить Приложение Обзор Настройка Настроить... Не удалось создать список свойств для (%s) Не удалось загрузить %s Невозможно записать список свойств для (%s) Не удалось записать данные в %s Не удалось записать в (%s) Не удалось загрузить изображение из файла: %s Не найден файл изображения: %s Благодарности DSN Система базы данных Описание Драйвер Библиотека драйверов Менеджер драйверов Драйверы Введите название DSN Использование файла Название ODBCConfig ODBCConfig - Благодарности ODBCConfig - Системы баз данных ODBCConfig - Драйверы ODBCConfig - odbc.ini ODBCConfig - odbcinst.ini Open DataBase Connectivity (ODBC) был разработан как открытый и переносимый стандарт для доступа к данным. unixODBC реализует его для Linux/UNIX. Пожалуй, в настоящее время, самыми распространенными системами управления базами данных являются серверы SQL

Серверы SQL, обладающие "тяжелой" функциональностью
  ADABAS-D
  Empress
  Sybase - www.sybase.com
 Oracle - www.oracle.com

Серверы SQL с "легкой" функциональностью
  MiniSQL
  MySQL
  Solid

Система управления базами данных может быть запущена локально на компьютере или на удаленной машине. Она также может хранить данные, используя множество способов. Это никак не повлияет на приложения ODBC, так как Driver Manager и Driver предоставляют единообразный интерфейс доступа к системе управления базами данных. Удалить Выбор файла Выберите DSN для удаления Выберите DSN для конфигурирования Выберите драйвер Выберите настраиваемый драйвер Выберите удаляемый драйвер Выберите используемый драйвер или добавьте новый Настройка Установить библиотеку Системный DSN Системные источники данных общие для всех пользователей этого компьютера. Также их могут использовать системные службы. Только администраторы могут управлять системными источниками данных. Приложение связывается с Менеджером драйверов путем стандартных вызовов ODBC.

Приложению всё равно где хранятся данные, как они хранятся, и как система настроена для доступа к ним.

Приложение просто использует имя источника данных (DSN)

Приложение не связано с какой-либо базой данных. Пользователь может выбрать другую базу данных, используя ODBCConfig. Менеджер Драйверов carries выполняет множество фукций, таких как:
1. Определяет имена источников данных с помощью odbcinst lib)
2. Загружает любые необходимые драйверы
3. Вызывает функции драйвера для связи с базой данных. Некоторая функциональность, например вывод всех Источников Данных, присутствует толко в Менеджере Драйверов или через odbcinst lib). Драйверы ODBC содержат код, зависящий от базы данных, и предоставляют набор функций Менеджеру драйверов.
Драйверы могут выполнять некоторые функции базы данных, если они требуются стандартом ODBC, но не поддерживаются соответствующей базой данных.
Также драйверы могут преобразовывать типы данных.

Драйверы ODBC можно найти в Интернете или получить от производителя базы данных.

Проверьте наличие драйверов на сайте http://www.unixodbc.org Эти драйверы обеспечивают связь между Менеджером драйверов и сервером данных. Многие драйверы ODBC для Linux можно загрузить из интернета, а некоторые доступны от вашего производителя базы данных. Это основной файл настроек ODBC.
Он содержит настройки источников данных (Data Source).

Он используеься Driver Manager, чтобы определять по заданному имени источника данных (Data Source Name) такие параметры, как название драйвера (Driver).

Это обычный текстовый файл, управляемый с помощью утилиты ODBCConfig.
Настройки пользовательских (User) источников данных обычно храняться в файле ~/.odbc.ini в то время как системные (System) источники данных хранятся в  /etc/odbc.ini
 Эту программу вы используется в данный момент. Она позволяет настраивать ODBC. Файл трассировки Трассировка Трассировка Трассировка создает журналы вызовов к драйверам ODBC. Используется службами поддержки, а также при отладке приложений.
Вы должны иметь права root`a для установки Не удалось обнаружить строку драйвера для этого элемента DSN пользователя Сведения о ваших источниках данных хранятся в вашем домашнем каталоге. Таким образом, вы сможете настраивать их, не будучи администратором системы. gODBCConfig - Добавление DSN gODBCConfig - Приложение gODBCConfig - Настройка драйвера gODBCConfig - Менеджер драйверов gODBCConfig - Новый драйвер gODBCConfig - Управление источниками данных ODBC http://www.unixodbc.org odbc.ini odbcinst.ini odbcinst.ini содержит перечень всех установленных ODBC драйверов. Каждая запись также содержит некоторую информацию о драйвере такую, как имя файла драйвера.

Запись необходимо добавлять при установке ODBC драйвера и удалять после его деинсталляции. Это можно сделать воспользовавшись ODBCConfig или консольной утилитой odbcinst. unixODBC состоит из следующих компонентов

- Менеджера драйверов (Driver Manager)
- Графической оболочки менеджера данных (Data Manager)
- Графической оболочки настройки
- Нескольких драйверов (Drivers) и библиотек для их настройки
- Шаблона исходного кода драйвера
- Шаблона исходного кода настройки драйвера
- библиотеки ODBCINST
- odbcinst (консольная утилита для скриптов установки)
- библиотеки INI
- библиотеки LOG
- библиотеки LST
- библиотеки TRE
- библиотеки SQI
- isql (консольная утилита для SQL)

Все исходные коды распространяются по лицензии GPL и LGPL.
 