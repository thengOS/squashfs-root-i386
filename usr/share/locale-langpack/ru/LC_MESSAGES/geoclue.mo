��          �      �       H  /   I     y  !   �     �     �     �     �     �  7        U     X     d     h  �  u  j     (   }  Q   �  @   �     9     A  ;   ^  @   �  h   �     D  
   K     V     [                                   	      
                        Allow '%s' to access your location information? Display version number Enable submission of network data Find your current location GeoClue Geolocation Geolocation service in use
 Geolocation service not in use
 Nickname to submit network data under (2-32 characters) No Where am I? Yes geolocation; Project-Id-Version: geoclue-2.0
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-01-12 09:11+0000
PO-Revision-Date: 2016-05-21 13:36+0000
Last-Translator: Eugene Marshal <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:33+0000
X-Generator: Launchpad (build 18115)
 Разрешить '%s' доступ к информации о вашем местонахождении? Показать номер версии Задействовать представление сетевых данных Определение вашего местоположения GeoClue Местоположение Служба геолокации используется
 Служба геолокации не используется
 Псевдоним для отправки сетевых данных (от 2 до 32 символов) Нет Где я? Да местоположение; 