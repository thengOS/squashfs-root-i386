��    	     d  �  �       �+  ~   �+  X    ,  H   y,     �,     �,     �,  &   �,  &   -  %   D-  $   j-  =   �-  '   �-     �-     .     .  �   +.  :   �.     �.     �.     �.     /     /  
   %/     0/     9/     F/  	   M/     W/  $   p/     �/     �/     �/     �/     �/  &   0  <   90     v0     �0     �0      �0  	   �0  2   �0  2   1     A1      I1  #   j1     �1     �1     �1  O   �1     2     12     =2     E2     N2     U2     g2     {2     �2     �2     �2     �2  "   �2     3  D   3  �   Z3  7   4  C   T4  3   �4     �4     �4     �4  f   5  (   �5      �5     �5  6   �5      6     <6     O6     f6     u6     |6     �6     �6     �6     �6     �6  	   �6     �6     �6     7     7     7     $7     47     :7     C7     c7     }7  #   �7  #   �7  +   �7     8     8     8     %8  
   28     =8     S8     e8     �8     �8     �8     �8     �8     �8     �8     �8      9     9  #   79     [9     y9     �9     �9     �9     �9     �9     �9  	   �9      :     :     1:     A:     E:     Y:     l:     t:     �:     �:     �:     �:     �:     �:     �:  	   ;     ;     2;     I;     b;     t;  
   �;      �;  !   �;     �;     �;  
   <     !<  :   5<  $   p<     �<     �<     �<     �<  $   �<     �<     �<     =     !=     7=  +   L=  "   x=     �=  1   �=     �=     >     &>     D>  �   [>     =?     \?      {?      �?     �?     �?     �?     �?  
   @     @     -@     J@     Z@  8   r@     �@     �@     �@     �@     �@     A     A     *A     0A     =A  	   IA  5   SA  %   �A  %   �A      �A     �A     B  $    B     EB     TB     bB     oB     }B  (   �B     �B     �B     �B     �B  �   C     �C  4   �C     �C     �C     �C     �C     D     D     1D  %   OD     uD     �D     �D     �D     �D     �D     E  $   'E     LE     PE     XE  �   pE  �   XF  �   EG     6H     FH     TH  )   jH     �H  (   �H     �H     �H     �H     �H  	   I  	   $I     .I     II     aI     gI     }I     �I     �I  /   �I  "   �I     J  ,   J     LJ     YJ      nJ     �J     �J     �J     �J     �J  
   �J  	   �J     �J     �J     �J     �J     K  �   $K  M   �K     �K     L     M     (M     @M     VM     kM     �M  �   �M     'N     AN     ^N     vN     �N     �N  v   �N     O     .O     3O     LO     dO     lO  
   tO     O     �O     �O  "   �O     �O  !   �O      �O  -   P     =P  �   ZP  "   ,Q     OQ     lQ  �   �Q     R     R  $   $R  
   IR  )   TR     ~R     �R     �R     �R     �R     �R     �R     �R     �R     �R     �R     S     S  H   +S  (   tS     �S     �S     �S     �S     �S     �S     �S     �S     �S     T  	   T     "T     )T  	   7T  
   AT     LT     XT     rT     yT     T  �   �T     U  7   U     QU     bU     qU     �U  !   �U     �U     �U     �U     �U     V     V     /V     MV     ]V  4   lV  $   �V     �V  "   �V     �V     W     W     W     ?W  
   FW     QW  ,   kW  +   �W     �W     �W     �W     X     X     $X     AX     JX     ZX     kX  (   �X     �X     �X     �X     �X     Y     Y     4Y     KY     [Y     xY     �Y     �Y  !   �Y  '   �Y  *   Z  -   -Z     [Z     zZ     �Z     �Z     �Z     �Z     �Z     �Z     �Z      [  -   [  9   @[  @   z[  r   �[     .\  A   E\  �   �\     =]     W]     d]     �]     �]     �]     �]  <   �]     �]  "   ^     $^     (^     =^  *   W^     �^     �^     �^     �^     �^     
_  !   _  N   1_     �_     �_  
   `     `     3`     G`     ``     y`     �`     �`     �`     �`     �`     	a     !a  `   )a  %   �a  ,   �a     �a     �a     �a  ,   b  	   >b     Hb  /   Xb  	   �b     �b     �b  -   �b  1   �b  "   &c  (   Ic     rc  �   �c  �   Ld  }   e  4   �e     �e     �e     �e     �e     f     f     !f  5   :f     pf     yf     f     �f  -   �f     �f     �f     �f     �f     �f     g     g     'g  ,   /g     \g  �  og  �   i  �   �i  \   hj     �j      �j  
   k  :   k  W   Kk  Z   �k  H   �k  w   Gl  S   �l  "   m     6m  %   Hm  �   nm  m   Bn     �n     �n     �n  6   �n  :   o  F   Jo  @   �o  9   �o     p      p  B   ;p  K   ~p  8   �p  H   q  g   Lq  D   �q  &   �q  *    r  �   Kr  2   �r  ,   s  &   Gs  1   ns     �s  a   �s  P   t     ft  G   rt  H   �t  6   u  
   :u  ,   Eu  �   ru  )   v     7v     Sv  
   _v     jv  )   {v  &   �v  )   �v  ;   �v  
   2w  U   =w  U   �w  ]   �w     Gx  �   Tx  ;  �x  o   6z  �   �z  q   *{  B   �{     �{  @   �{  �   ?|  k   /}  H   �}  @   �}  �   %~  p   �~  3   $  3   X  )   �     �     �  .   �     �  9   �     P�     o�     ��     ��  H   ��     ��  -   
�     8�      T�     u�     ��  ?   ��  0   ԁ  -   �  o   3�  o   ��  w   �  :   ��  *   ƃ     �     �     �  *   )�      T�  &   u�  =   ��     ڄ  /   �     �     4�     P�  1   m�  '   ��  8   ǅ  +    �  B   ,�  "   o�     ��     ��     Ά     ݆  8   ��  $   2�  0   W�     ��  (   ��  *   Ç     �     	�  *   �  ,   E�     r�  J   ��     Ո     �  F    �     G�  ,   d�  5   ��  P   ǉ     �  %   *�  +   P�  9   |�  6   ��  0   �     �  r   .�  r   ��  )   �  3   >�     r�  +   ��  U   ��  >   �     R�     e�  ,   ��     ��  |   ��  1   =�  )   o�  0   ��  C   ʎ  2   �  4   A�  M   v�  W   ď  a   �  =   ~�  1   ��  =   �  4   ,�  N  a�  a   ��  c   �  E   v�  G   ��  *   �     /�     I�  B   c�     ��     ��  -   ܔ     
�  *    �  �   K�  *   �     �  D   2�  ?   w�  :   ��  	   �     ��     �     !�  
   ;�     F�  ~   `�  d   ߗ  m   D�  i   ��  #   �     @�  Z   \�  #   ��  
   ۙ     �     ��     �  d   .�     ��     ��  /   ��  %   �  �   �  5   �  o   !�     ��  	   ��     ��  ,   Ĝ     �  <   �  o   M�  \   ��     �  %   8�  ?   ^�  7   ��  O   ֞  @   &�  !   g�  n   ��     ��     ��  /   �  �  B�  �  !�  �  �  *   �     �  &   "�  Z   I�  ,   ��  g   Ѧ     9�     X�  $   v�  *   ��     Ƨ     ڧ  6   ��  S   1�     ��  )   ��     ��  N   Ө  1   "�  r   T�  ?   ǩ  D   �  L   L�     ��     ��  [   Ǫ  )   #�     M�  /   d�     ��  4   ��     ۫     ��     �     $�     1�  *   8�  F   c�    ��  �   ��  B   z�  �  ��     Z�  )   u�  !   ��  4   ��  >   ��  A   5�    w�  ,   ��  0   ²  <   �  <   0�  &   m�     ��  �   ��  .   l�     ��  >   ��  8   �     "�     4�     G�     ]�     x�     ��  O   ��     �  >   ��  +   7�  W   c�  +   ��  �  �  P   ��  H   ո  D   �  �   c�     Z�     m�  W   |�     Ժ  F   �     (�  5   ;�     q�     ��     ��  )   ��  
   �     �  /   ��     -�  4   @�     u�  '   ��  [   ��  p   �     x�     ��  O   ��  %   �  =   �     P�     `�  F   o�  !   ��  0   ؾ     	�     �  '   6�     ^�     |�     ��  P   ��     ��  
   �     �  �   3�     �  v   �  C   ��  #   ��  #   ��  5   �  <   T�  %   ��  "   ��  F   ��     !�  >   .�  ?   m�  K   ��     ��  4   �  m   N�  ?   ��  1   ��  >   .�     m�     ��      ��  /   ��     ��     ��     �  m   /�  e   ��  O   �  :   S�     ��     ��     ��  9   ��     &�     E�  $   a�  6   ��  e   ��  ?   #�  @   c�     ��  R   ��  =   �  ?   S�  ?   ��  2   ��  8   �  4   ?�  L   t�  0   ��  L   ��  Y   ?�  d   ��  q   ��  0   p�     ��  J   ��     �  0   �  .   M�  ?   |�     ��  
   ��  $   ��  :   �  L   ?�  �   ��  �   �  <   ��  �   #�    ��  Q   ��  @   �  <   Y�  8   ��  3   ��  /   �     3�  m   O�  #   ��  A   ��     #�  !   0�  5   R�  @   ��  !   ��  +   ��  <   �  S   T�  =   ��     ��  k   ��  �   c�  )   �  8  6�     o�  Y   ��  0   ��  "   �  =   3�  =   q�  5   ��  7   ��  =   �  ]   [�  >   ��  /   ��     (�  �   ;�  C   ��  A   �  *   ^�  A   ��  &   ��  d   ��     W�  (   w�  C   ��     ��  -   ��  5   #�  H   Y�  L   ��  9   ��  J   )�  :   t�  )  ��  Y  ��    3�  c   O�  *   ��  )   ��     �     '�  H   4�     }�  O   ��  _   ��     :�     P�     ]�  3   {�  \   ��  ,   �  -   9�     g�     v�     z�      ��  G   ��     ��  X   �  G   o�         �  �        i  -  �   q       �   �          [  �  �     [       )   �   �  7   �   (   �  �      Q   �   �      �  �           �  +   `   n      �  �      �  �     k   �  m   0      l       �  �  �  �   �          �  <   �  4             �   �   �     �  s  �          p          /               �  p  n   �       j  +      �   �     �  a   C   $   H  �   �           �   x      T      �   �          �   �   =   �  b  �          e  �   �  �  �                  i   �          |           }   V           w  �    �  �               �  r  �   �  �   �  W      �   �  w   |  l  �   �  �  1        d       �   #       �  g     �   �               �  �  �  g            �   �      9       �   �   �   �      t         8   0   �                 X      �   �             �       �               �           )  �  ^   �   "    3   �  �  �   �          %  �  A   M  �  ~  �  e   B  ;   �  �   �  �  4               	      �        �  o   *   �    �           �                    _      m  �     �  ~   b       L  �          �  �  �   �      R            T   �   �   �   \      %   �  �   N       �  A  �      �  @      ]   v     (  Z      O  �   h  �   J   O      �  �      �     �   �      �   U   �   =  r   E  �      {  �     &  .     D   {   z   �   �       >  �   �  G   �     �   �   �      �     E   �  ^  _   �          7  H   �  f       �   �         G  c  u      �   �   �   Z       	  	       ,           f   �   �      Q  �   �      ?   �   q  #  �  C            �       $  3  �   �  ]      �   �   �              �   y   �   '     �  �  �   �           
  �              B          x           �   `                 �  "   �  �  �  �  F    F   �  9  �              *  �      @       �    8  ?  �   M   �  \      �        �     s   �  �  <  D      P  �       V  d    �       &       �  S    z  �      �      �       �     �      �   t   W              o  P   �  �   �   ;      }    �  �   �       K  �   J  �   u   �       �   I  �  �         �       :   ,  -           �      L   �              �  5  �       R   �   /  �   �  y  �      �  �          Y  1   �       Y   �      �       �   �  �       �  �   �   k  S   �  �    �   �  �               �       �     K       �  �  �  �   �       !      �   �  �   �       �  �   v      6   >       �      h           
   �  �  �  �  �   5               U                    �   6  2     N  a  .         �   �    �  '       I   2        X  �       �   �                 !      �   :      �       �  j       c    

A good replacement for %s
is placing values for the user.name and
user.email settings into your personal
~/.gitconfig file.
 
* Untracked file clipped here by %s.
* To see the entire file, use an external editor.
 
This is due to a known issue with the
Tcl binary distributed by Cygwin. %s ... %*i of %*i %s (%3i%%) %s Repository %s of %s '%s' is not an acceptable branch name. '%s' is not an acceptable remote name. (Blue denotes repository-local tools) * Binary file (not showing content). * Untracked file is %d bytes.
* Showing only first %d bytes.
 A branch is required for 'Merged Into'. Abort Merge... Abort completed.  Ready. Abort failed. Abort merge?

Aborting the current merge will cause *ALL* uncommitted changes to be lost.

Continue with aborting the current merge? Aborted checkout of '%s' (file level merging is required). Aborting About %s Add Add New Remote Add New Tool Command Add Remote Add Tool Add globally Add... Adding %s Adding resolution for %s Always (Do not perform merge checks) Amend Last Commit Amended Commit Message: Amended Initial Commit Message: Amended Merge Commit Message: Annotation complete. Annotation process is already running. Any unstaged changes will be permanently lost by the revert. Apply/Reverse Hunk Apply/Reverse Line Arbitrary Location: Are you sure you want to run %s? Arguments Ask the user for additional arguments (sets $ARGS) Ask the user to select a revision (sets $REVISION) Author: Blame Copy Only On Changed Files Blame History Context Radius (days) Blame Parent Commit Branch Branch '%s' already exists. Branch '%s' already exists.

It cannot fast-forward to %s.
A merge is required. Branch '%s' does not exist. Branch Name Branch: Branches Browse Browse %s's Files Browse Branch Files Browse Branch Files... Browse Current Branch's Files Busy Calling commit-msg hook... Calling pre-commit hook... Calling prepare-commit-msg hook... Cancel Cannot abort while amending.

You must finish amending this commit.
 Cannot amend while merging.

You are currently in the middle of a merge that has not been fully completed.  You cannot amend the prior commit unless you first abort the current merge activity.
 Cannot determine HEAD.  See console output for details. Cannot fetch branches and objects.  See console output for details. Cannot fetch tags.  See console output for details. Cannot find HEAD commit: Cannot find git in PATH. Cannot find parent commit: Cannot merge while amending.

You must finish amending this commit before starting any type of merge.
 Cannot move to top of working directory: Cannot parse Git version string: Cannot resolve %s as a commit. Cannot resolve deletion or link conflicts using a tool Cannot use bare repository: Cannot write icon: Cannot write shortcut: Case-Sensitive Change Change Font Checked out '%s'. Checkout Checkout After Creation Checkout Branch Checkout... Choose %s Clone Clone Existing Repository Clone Type: Clone failed. Clone... Cloning from %s Close Command: Commit %s appears to be corrupt Commit Message Text Width Commit Message: Commit declined by commit-msg hook. Commit declined by pre-commit hook. Commit declined by prepare-commit-msg hook. Commit failed. Commit: Commit@@noun Commit@@verb Committer: Committing changes... Compress Database Compressing the object database Conflict file does not exist Continue Copied Or Moved Here By: Copy Copy All Copy Commit Copy To Clipboard Copying objects Could not add tool:
%s Could not start ssh-keygen:

%s Could not start the merge tool:

%s Couldn't find git gui in PATH Couldn't find gitk in PATH Counting objects Create Create Branch Create Desktop Icon Create New Branch Create New Repository Create... Created commit %s: %s Creating working directory Current Branch: Cut Database Statistics Decrease Font Size Default Default File Contents Encoding Delete Delete Branch Delete Branch Remotely Delete Branch... Delete Local Branch Delete Only If Delete Only If Merged Into Delete... Deleting branches from %s Destination Repository Detach From Local Branch Diff/Console Font Directory %s already exists. Directory: Disk space used by loose objects Disk space used by packed objects Displaying only %s of %s files. Do Full Copy Detection Do Nothing Do Nothing Else Now Do not know how to initialize repository at location '%s'. Don't show the command output window Done Edit Email Address Encoding Error loading commit data for amend: Error loading diff: Error loading file: Error retrieving versions:
%s Error: Command Failed Explore Working Copy Failed to add remote '%s' of location '%s'. Failed to completely save options: Failed to configure origin Failed to configure simplified git-pull for '%s'. Failed to create repository %s: Failed to delete branches:
%s Failed to open repository %s: Failed to rename '%s'. Failed to set current branch.

This working directory is only partially switched.  We successfully updated your files, but failed to update an internal Git file.

This should not have occurred.  %s will now close and give up. Failed to stage selected hunk. Failed to stage selected line. Failed to unstage selected hunk. Failed to unstage selected line. Failed to update '%s'. Fast Forward Only Fetch Immediately Fetch Tracking Branch Fetch from Fetching %s from %s Fetching new changes from %s Fetching the %s File %s already exists. File %s seems to have unresolved conflicts, still stage? File Browser File Viewer File level merge required. File type changed, not staged File type changed, staged File: Find Text... Find: Font Example Font Family Font Size Force overwrite existing branch (may discard changes) Force resolution to the base version? Force resolution to the other branch? Force resolution to this branch? Found a public key in: %s From Repository Full Copy (Slower, Redundant Backup) Further Action Garbage files Generate Key Generating... Generation failed. Generation succeeded, but no keys found. Git Gui Git Repository Git Repository (subproject) Git directory not found: Git version cannot be determined.

%s claims it is version '%s'.

%s requires at least Git 1.5.0 or later.

Assume '%s' is version 1.5.0?
 Global (All Repositories) Hardlinks are unavailable.  Falling back to copying. Help In File: Include tags Increase Font Size Index Error Initial Commit Message: Initial file checkout failed. Initialize Remote Repository and Push Initializing... Invalid GIT_COMMITTER_IDENT: Invalid date from Git: %s Invalid font specified in %s: Invalid global encoding '%s' Invalid repo encoding '%s' Invalid revision: %s Invalid spell checking configuration KiB LOCAL:
 LOCAL: deleted
REMOTE:
 Last scanned state does not match repository state.

Another Git program has modified this repository since the last scan.  A rescan must be performed before a merge can be performed.

The rescan will be automatically started now.
 Last scanned state does not match repository state.

Another Git program has modified this repository since the last scan.  A rescan must be performed before another commit can be created.

The rescan will be automatically started now.
 Last scanned state does not match repository state.

Another Git program has modified this repository since the last scan.  A rescan must be performed before the current branch can be changed.

The rescan will be automatically started now.
 Linking objects Loading %s... Loading annotation... Loading copy/move tracking annotations... Loading diff of %s... Loading original location annotations... Local Branch Local Branches Local Merge... Location %s already exists. Location: Main Font Match Tracking Branch Name Match Tracking Branches Merge Merge Commit Message: Merge Into %s Merge Verbosity Merge completed successfully. Merge failed.  Conflict resolution is required. Merge strategy '%s' not supported. Merge tool failed. Merge tool is already running, terminate it? Merged Into: Merging %s and %s... Minimum Letters To Blame Copy On Mirroring to %s Missing Modified, not staged Name: New Branch Name Template New Commit New Name: New... Next No No Suggestions No changes to commit. No changes to commit.

No files were modified by this commit and it was not a merge commit.

A rescan will be automatically started now.
 No changes to commit.

You must stage at least 1 file before you can commit.
 No default branch obtained. No differences detected.

%s has no changes.

The modification date of this file was updated by another application, but the content within the file was not changed.

A rescan will be automatically started to find other files which may have the same state. No keys found. No repository selected. No revision selected. No working directory Not a GUI merge tool: '%s' Not a Git repository: %s Note that the diff shows only conflicting changes.

%s will be overwritten.

This operation can be undone only by restarting the merge. Nothing to clone from %s. Number of Diff Context Lines Number of loose objects Number of packed objects Number of packs OK One or more of the merge tests failed because you have not fetched the necessary commits.  Try fetching from %s first. Online Documentation Open Open Existing Repository Open Recent Repository: Open... Options Options... Original File: Originally By: Other Packed objects waiting for pruning Paste Please select a branch to rename. Please select a tracking branch. Please select one or more branches to delete. Please supply a branch name. Please supply a commit message.

A good commit message has the following format:

- First line: Describe in one sentence what you did.
- Second line: Blank
- Remaining lines: Describe why this change is good.
 Please supply a name for the tool. Please supply a remote name. Portions staged for commit Possible environment issues exist.

The following environment variables are probably
going to be ignored by any Git subprocess run
by %s:

 Preferences Prev Prune Tracking Branches During Fetch Prune from Pruning tracking branches deleted from %s Push Push Branches Push to Push... Pushing %s %s to %s Pushing changes to %s Quit REMOTE:
 REMOTE: deleted
LOCAL:
 Reading %s... Ready to commit. Ready. Recent Repositories Recovering deleted branches is difficult.

Delete the selected branches? Recovering lost commits may not be easy. Redo Refresh Refreshing file status... Remote Remote Details Remote: Remove Remove Remote Remove Tool Remove Tool Commands Remove... Rename Rename Branch Rename... Repository Repository: Requires merge resolution Rescan Reset Reset '%s'? Reset changes?

Resetting the changes will cause *ALL* uncommitted changes to be lost.

Continue with resetting the current changes? Reset... Resetting '%s' to '%s' will lose the following commits: Restore Defaults Revert Changes Revert To Base Revert changes in file %s? Revert changes in these %i files? Reverting %s Reverting dictionary to %s. Reverting selected files Revision Revision Expression: Revision To Merge Revision expression is empty. Run Command: %s Run Merge Tool Run only if a diff is selected ($FILENAME not empty) Running %s requires a selected file. Running merge tool... Running thorough copy detection... Running: %s Save Scanning %s... Scanning for modified files ... Select Select All Setting up the %s (at %s) Shared (Fastest, Not Recommended, No Backup) Shared only available for local repository. Show Diffstat After Merge Show History Context Show Less Context Show More Context Show SSH Key Show a dialog before running Sign Off Source Branches Source Location: Spell Checker Failed Spell checker silently failed on startup Spell checking is unavailable Spelling Dictionary: Stage Changed Stage Changed Files To Commit Stage Hunk For Commit Stage Line For Commit Stage Lines For Commit Stage To Commit Staged Changes (Will Commit) Staged for commit Staged for commit, missing Staged for removal Staged for removal, still present Staging area (index) is already locked. Standard (Fast, Semi-Redundant, Hardlinks) Standard only available for local repository. Start git gui In The Submodule Starting Revision Starting gitk... please wait... Starting... Staying on branch '%s'. Success Summarize Merge Commits System (%s) Tag Target Directory: The 'master' branch has not been initialized. The following branches are not completely merged into %s: The following branches are not completely merged into %s:

 - %s There is nothing to amend.

You are about to create the initial commit.  There is no commit before this to amend.
 This Detached Checkout This is example text.
If you like this text, it can be your font. This repository currently has approximately %i loose objects.

To maintain optimal performance it is strongly recommended that you compress the database.

Compress the database now? Tool '%s' already exists. Tool Details Tool completed successfully: %s Tool failed: %s Tool: %s Tools Tracking Branch Tracking branch %s is not a branch in the remote repository. Transfer Options Trust File Modification Timestamps URL Unable to cleanup %s Unable to copy object: %s Unable to copy objects/info/alternates: %s Unable to display %s Unable to display parent Unable to hardlink object: %s Unable to obtain your identity: Unable to unlock the index. Undo Unexpected EOF from spell checker Unknown file state %s detected.

File %s cannot be committed by this program.
 Unlock Index Unmerged files cannot be committed.

File %s has merge conflicts.  You must resolve them and stage the file before committing.
 Unmodified Unrecognized spell checker Unstage From Commit Unstage Hunk From Commit Unstage Line From Commit Unstage Lines From Commit Unstaged Changes Unstaging %s from commit Unsupported merge tool '%s' Unsupported spell checker Untracked, not staged Update Existing Branch: Updated Updating the Git index failed.  A rescan will be automatically started to resynchronize git-gui. Updating working directory to '%s'... Use '/' separators to create a submenu tree: Use Local Version Use Merge Tool Use Remote Version Use thin pack (for slow network connections) User Name Verify Database Verifying the object database with fsck-objects Visualize Visualize %s's History Visualize All Branch History Visualize All Branch History In The Submodule Visualize Current Branch History In The Submodule Visualize Current Branch's History Visualize These Changes In The Submodule Working... please wait... You are in the middle of a change.

File %s is modified.

You should complete the current commit before starting a merge.  Doing so will help you abort a failed merge, should the need arise.
 You are in the middle of a conflicted merge.

File %s has merge conflicts.

You must resolve them, stage the file, and commit to complete the current merge.  Only then can you begin another merge.
 You are no longer on a local branch.

If you wanted to be on a branch, create one now starting from 'This Detached Checkout'. You must correct the above errors before committing. Your OpenSSH Public Key Your key is in: %s [Up To Parent] buckets commit-tree failed: error fatal: Cannot resolve %s fatal: cannot stat path %s: No such file or directory fetch %s files files checked out files reset git-gui - a graphical user interface for Git. git-gui: fatal error lines annotated objects pt. push %s remote prune %s update-ref failed: warning warning: Tcl does not support encoding '%s'. write-tree failed: Project-Id-Version: git-gui
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-01-26 15:47-0800
PO-Revision-Date: 2014-08-21 08:22+0000
Last-Translator: ☠Jay ZDLin☠ <Unknown>
Language-Team: Russian Translation <git@vger.kernel.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:28+0000
X-Generator: Launchpad (build 18115)
 

Вместо использования %s можно
сохранить значения user.name и
user.email в Вашем персональном
файле ~/.gitconfig.
 
* Неподготовленный файл обрезан: %s.
* Чтобы увидеть весь файл, используйте программу-редактор.
 
Это известная проблема с Tcl,
распространяемым Cygwin. %s ... %*i из %*i %s (%3i%%) Для репозитория %s %s из %s Недопустимое название ветви '%s'. Недопустимое название внешнего репозитория '%s'. (Синим выделены программы локальные репозиторию) * Двоичный файл (содержимое не показано) * Размер неподготовленного файла %d байт.
* Показано первых %d байт.
 Для опции 'Слияние с' требуется указать ветвь. Прервать слияние... Прервано. Прервать не удалось. Прервать операцию слияния?

Прерывание этой операции приведет к потере *ВСЕХ* несохраненных изменений.

Продолжить? Прерван переход на '%s' (требуется слияние содержания файлов) Прерываю О %s Добавить Добавить внешний репозиторий Новая вспомогательная операция Зарегистрировать внешний репозиторий Добавить вспомогательную операцию Добавить для всех репозиториев Добавить... Добавление %s... Добавляю результат разрешения для %s Всегда (не выполнять проверку на слияние) Исправить последнее состояние Комментарий к исправленному состоянию: Комментарий к исправленному первоначальному состоянию: Комментарий к исправленному слиянию: Аннотация завершена. Аннотация уже запущена Любые изменения, не подготовленные к сохранению, будут потеряны при данной операции. Применить/Убрать изменение Применить/Убрать строку Указанное положение: Действительно запустить %s? Аргументы Запрос дополнительных аргументов (устанавливает $ARGS) Запрос на выбор версии (устанавливает $REVISION) Автор: Поиск копий только в изменённых файлах Радиус исторического контекста (в днях) Рассмотреть состояние предка Ветвь Ветвь '%s' уже существует. Ветвь '%s' уже существует.

Она не может быть прокручена(fast-forward) к %s.
Требуется слияние. Ветвь '%s' не существует Название ветви Ветвь: Ветви Показать Показать файлы ветви %s Показать файлы ветви Показать файлы ветви... Просмотреть файлы текущей ветви Занят Вызов программы поддержки репозитория commit-msg... Вызов программы поддержки репозитория pre-commit... Вызов программы поддержки репозитория prepare-commit-msg... Отмена Невозможно прервать исправление.

Завершите текущее исправление сохраненного состояния.
 Невозможно исправить состояние во время операции слияния.

Текущее слияние не завершено. Невозможно исправить предыдущее сохраненное состояние, не прерывая эту операцию.
 Не могу определить HEAD. Дополнительная информация на консоли. Не могу получить ветви и объекты. Дополнительная информация на консоли. Не могу получить метки. Дополнительная информация на консоли. Невозможно найти текущее состояние: git не найден в PATH. Невозможно найти состояние предка: Невозможно выполнить слияние во время исправления.

Завершите исправление данного состояния перед выполнением операции слияния.
 Невозможно перейти к корню рабочего каталога репозитория: Невозможно распознать строку версии Git: Не могу распознать %s как состояние. Программа слияния не обрабатывает конфликты с удалением или участием ссылок Невозможно использование репозитория без рабочего каталога: Невозможно записать значок: Невозможно записать ссылку: Игн. большие/маленькие Изменить Изменить Ветвь '%s' сделана текущей. Перейти После создания сделать текущей Перейти на ветвь Перейти... Выберите %s Склонировать Склонировать существующий репозиторий Тип клона: Клонирование не удалось. Склонировать... Клонирование из %s Закрыть Команда: Состояние %s выглядит поврежденным Ширина текста комментария Комментарий к состоянию: Сохранение прервано программой поддержки репозитория commit-msg Сохранение прервано программой поддержки репозитория pre-commit Сохранение прервано программой поддержки репозитория prepare-commit-msg Сохранить состояние не удалось. Сохраненное состояние: Состояние Сохранить Сохранил: Сохранение изменений... Сжать базу данных Сжатие базы объектов Конфликтующий файл не существует Продолжить Скопировано/перемещено в: Копировать Копировать все Скопировать SHA-1 Скопировать в буфер обмена Копирование объектов Ошибка добавления программы:
%s Ошибка запуска ssh-keygen:

%s Ошибка запуска программы слияния:

%s git gui не найден в PATH. gitk не найден в PATH. Считаю объекты Создать Создание ветви Создать ярлык на рабочем столе Создать новую ветвь Создать новый репозиторий Создать... Создано состояние %s: %s Создаю рабочий каталог Текущая ветвь: Вырезать Статистика базы данных Уменьшить размер шрифта По умолчанию Кодировка содержания файла по умолчанию Удалить Удаление ветви Удаление ветви во внешнем репозитории Удалить ветвь... Удалить локальную ветвь Удалить только в случае, если Удалить только в случае, если было слияние с Удалить... Удаление ветвей из %s Репозиторий назначения Отсоединить от локальной ветви Шрифт консоли и изменений (diff) Каталог '%s' уже существует. Каталог: Объем дискового пространства, занятый несвязанными объектами Объем дискового пространства, занятый упакованными объектами Показано %s из %s файлов. Провести полный поиск копий Ничего не делать Больше ничего не делать Невозможно инициализировать репозиторий в '%s'. Не показывать окно вывода команды Завершено Редактировать Адрес электронной почты Кодировка Ошибка при загрузке данных для исправления сохраненного состояния: Ошибка загрузки изменений: Ошибка загрузки файла: Ошибка получения версий:
%s Ошибка: не удалось выполнить команду Просмотр рабочего каталога Не удалось добавить '%s' из '%s'. Не удалось полностью сохранить настройки: Не могу сконфигурировать исходный репозиторий. Ошибка создания упрощённой конфигурации git pull для '%s'. Не удалось создать репозиторий %s: Не удалось удалить ветви:
%s Не удалось открыть репозиторий %s: Не удалось переименовать '%s'. Не удалось установить текущую ветвь.

Ваш рабочий каталог обновлен только частично. Были обновлены все файлы кроме служебных файлов Git. 

Этого не должно было произойти. %s завершается. Не удалось подготовить к сохранению выбранную часть. Не удалось подготовить к сохранению выбранную строку. Не удалось исключить выбранную часть. Не удалось исключить выбранную строку. Не удалось обновить '%s'. Только Fast Forward Скачать сразу Получить изменения из внешней ветви Получение из Получение %s из %s Получение изменений из %s Получение %s Файл '%s' уже существует. Файл %s, похоже, содержит необработанные конфликты. Продолжить подготовку к сохранению? Просмотр списка файлов Просмотр файла Требуется слияние содержания файлов. Тип файла изменён, не подготовлено Тип файла изменён, подготовлено Файл: Найти текст... Поиск: Пример текста Шрифт Размер шрифта Намеренно переписать существующую ветвь (возможна потеря изменений) Использовать базовую версию для разрешения конфликта? Использовать версию другой ветви для разрешения конфликта? Использовать версию этой ветви для разрешения конфликта? Публичный ключ из %s Из репозитория Полная копия (Медленный, создает резервную копию) Следующая операция Мусор Создать ключ Создание... Ключ не создан. Создание ключа завершилось, но результат не был найден Git Gui Репозиторий Репозиторий Git (подпроект) Каталог Git не найден: Невозможно определить версию Git

%s указывает на версию '%s'.

для %s требуется версия Git, начиная с 1.5.0

Принять '%s' как версию 1.5.0?
 Общие (для всех репозиториев) "Жесткие ссылки" недоступны. Будет использовано копирование. Помощь Файл: Передать метки Увеличить размер шрифта Ошибка в индексе Комментарий к первому состоянию: Не удалось получить начальное состояние файлов репозитория. Инициализировать внешний репозиторий и отправить Инициализация... Неверный GIT_COMMITTER_IDENT: Неправильная дата в репозитории: %s В %s установлен неверный шрифт: Ошибка в глобальной установке кодировки '%s' Неверная кодировка репозитория: '%s' Неверная версия: %s Неправильная конфигурация программы проверки правописания КиБ ЛОКАЛЬНО:
 ЛОКАЛЬНО: удалён
ВНЕШНИЙ:
 Последнее прочитанное состояние репозитория не соответствует текущему.

С момента последней проверки репозиторий был изменен другой программой Git. Необходимо перечитать репозиторий, прежде чем изменять текущую ветвь.

Это будет сделано сейчас автоматически.
 Последнее прочитанное состояние репозитория не соответствует текущему.

С момента последней проверки репозиторий был изменен другой программой Git. Необходимо перечитать репозиторий, прежде чем изменять текущую ветвь. 

Это будет сделано сейчас автоматически.
 Последнее прочитанное состояние репозитория не соответствует текущему.

С момента последней проверки репозиторий был изменен другой программой Git. Необходимо перечитать репозиторий, прежде чем изменять текущую ветвь.

Это будет сделано сейчас автоматически.
 Создание ссылок на objects Загрузка %s... Загрузка аннотации... Загрузка аннотации копирований/переименований... Загрузка изменений в %s... Загрузка аннотаций первоначального положения объекта... Локальная ветвь: Локальные ветви Локальное слияние... Путь '%s' уже существует. Положение: Шрифт интерфейса Взять из имен ветвей слежения Имя новой ветви взять из имен ветвей слежения Слияние Комментарий к слиянию: Слияние с %s Уровень детальности сообщений при слиянии Слияние успешно завершено. Не удалось завершить слияние. Требуется разрешение конфликта. Неизвестная стратегия слияния: '%s'. Ошибка выполнения программы слияния. Программа слияния уже работает. Прервать? Слияние с: Слияние %s и %s... Минимальное количество символов для поиска копий Точное копирование в %s Отсутствует Изменено, не подготовлено Название: Шаблон для имени новой ветви Новое состояние Новое название: Новый... Дальше Нет Исправлений не найдено Отсутствуют изменения для сохранения. Отсутствуют изменения для сохранения.

Ни один файл не был изменен и не было слияния.

Сейчас автоматически запустится перечитывание репозитория.
 Отсутствуют изменения для сохранения.

Подготовьте хотя бы один файл до создания сохраненного состояния.
 Не было получено ветви по умолчанию. Изменений не обнаружено.

в %s отсутствуют изменения.

Дата изменения файла была обновлена другой программой, но содержимое файла осталось прежним.

Сейчас будет запущено перечитывание репозитория, чтобы найти подобные файлы. Ключ не найден Не указан репозиторий. Версия не указана. Отсутствует рабочий каталог '%s' не является программой слияния Каталог не является репозиторием: %s Внимание! Список изменений показывает только конфликтующие отличия.

%s будет переписан.

Это действие можно отменить только перезапуском операции слияния. Нечего клонировать из %s. Число строк в контексте diff Количество несвязанных объектов Количество упакованных объектов Количество pack-файлов OK Некоторые тесты на слияние не прошли, потому что Вы не получили необходимые состояния. Попытайтесь получить их из %s. Документация в интернете Открыть Выбрать существующий репозиторий Открыть последний репозиторий Открыть... Настройки Настройки... Исходный файл: Источник: Другая Несвязанные объекты, которые можно удалить Вставить Укажите ветвь для переименования. Укажите ветвь слежения. Укажите одну или несколько ветвей для удаления. Укажите название ветви. Напишите комментарий к сохраненному состоянию.

Рекомендуется следующий формат комментария:

- первая строка: краткое описание сделанных изменений.
- вторая строка пустая
- оставшиеся строки: опишите, что дают ваши изменения.
 Укажите название вспомогательной операции. Укажите название внешнего репозитория. Части, подготовленные для сохранения Возможны ошибки в переменных окружения.

Переменные окружения, которые возможно
будут проигнорированы командами Git,
запущенными из %s

 Настройки Обратно Чистка ветвей слежения при получении изменений Чистка Чистка ветвей слежения, удаленных из %s Отправить Отправить изменения в ветвях Отправить Отправить... Отправка %s %s в %s Отправка изменений в %s Выход ВНЕШНИЙ:
 ВНЕШНИЙ: удалён
ЛОКАЛЬНО:
 Чтение %s... Подготовлено для сохранения Готово. Недавние репозитории Восстановить удаленные ветви сложно.

Продолжить? Восстановить потерянные сохраненные состояния будет сложно. Повторить Обновить Обновление информации о состоянии файлов... Внешние репозитории Информация о внешнем репозитории внешний: Удалить Удалить ссылку на внешний репозиторий Удалить программу Удалить команды программы Удалить... Переименовать Переименование ветви Переименовать... Репозиторий Репозиторий: Требуется разрешение конфликта при слиянии Перечитать Сброс Сбросить '%s'? Прервать операцию слияния?

Прерывание этой операции приведет к потере *ВСЕХ* несохраненных изменений.

Продолжить? Сбросить... Сброс '%s' в '%s' приведет к потере следующих сохраненных состояний: Восстановить настройки по умолчанию Отменить изменения Отменить изменения Отменить изменения в файле %s? Отменить изменения в %i файле(-ах)? Отмена изменений в %s Словарь вернут к %s. Удаление изменений в выбранных файлах Версия Выражение для определения версии: Версия, с которой провести слияние Пустое выражение для определения версии. Запуск команды: %s Запустить программу слияния Запуск только если показан список изменений ($FILENAME не пусто) Запуск %s требует выбранного файла. Запуск программы слияния... Выполнение полного поиска копий... Выполнение: %s Сохранить Перечитывание %s... Поиск измененных файлов... Выбрать Выделить все Настройка %s (в %s) Общий (Самый быстрый, не рекомендуется, без резервной копии) Общий клон возможен только для локального репозитория. Показать отчет об изменениях после слияния Показать исторический контекст Меньше контекста Больше контекста Показать ключ SSH Показать диалог перед запуском Вставить Signed-off-by Исходные ветви Исходное положение: Ошибка проверки правописания Программа проверки правописания не смогла запуститься Проверка правописания не доступна Словарь для проверки правописания: Подготовить все Подготовить измененные файлы для сохранения Подготовить часть для сохранения Подготовить строку для сохранения Подготовить строки для сохранения Подготовить для сохранения Подготовлено (будет сохранено) Подготовлено для сохранения Подготовлено для сохранения, отсутствует Подготовлено для удаления Подготовлено для удаления, еще не удалено Рабочая область заблокирована другим процессом. Стандартный (Быстрый, полуизбыточный, "жесткие" ссылки) Стандартный клон возможен только для локального репозитория. Запустить git gui в субмодуле Начальная версия Запускается gitk... Подождите, пожалуйста... Запуск... Ветвь '%s' остается текущей. Процесс успешно завершен Суммарный комментарий при слиянии Системная (%s) Метка Каталог назначения: Не инициализирована ветвь 'master'. Ветви, которые не полностью сливаются с %s: Следующие ветви могут быть объединены с %s при помощи операции слияния:

 - %s Отсутствует состояние для исправления.

Вы создаете первое состояние в репозитории, здесь еще нечего исправлять.
 Текущее отсоединенное состояние Это пример текста.
Если Вам нравится этот текст, это может быть Ваш шрифт. Этот репозиторий сейчас содержит примерно %i свободных объектов

Для лучшей производительности рекомендуется сжать базу данных.

Сжать базу данных сейчас? Вспомогательная операция '%s' уже существует. Описание вспомогательной операции Программа %s завершилась успешно. Ошибка выполнения программы: %s Вспомогательная операция: %s Вспомогательные операции Ветвь слежения Ветвь слежения %s не является ветвью во внешнем репозитории. Настройки отправки Доверять времени модификации файла Ссылка Не могу очистить %s Не могу скопировать объект: %s Не могу скопировать objects/info/alternates: %s Не могу показать %s Не могу показать предка Не могу "жестко связать" объект: %s Невозможно получить информацию об авторстве: Не удалось разблокировать индекс Отменить Программа проверки правописания прервала передачу данных Обнаружено неизвестное состояние файла %s.

Файл %s не может быть сохранен данной программой.
 Разблокировать индекс Нельзя сохранить файлы с незавершённой операцией слияния.

Для файла %s возник конфликт слияния. Разрешите конфликт и добавьте к подготовленным файлам перед сохранением.
 Не изменено Нераспознанная программа проверки правописания Убрать из подготовленного Не сохранять часть Убрать строку из подготовленного Убрать строки из подготовленного Изменено (не будет сохранено) Удаление %s из подготовленного Неизвестная программа слияния '%s' Неподдерживаемая программа проверки правописания Не отслеживается, не подготовлено Обновить имеющуюся ветвь: Обновлено Не удалось обновить индекс Git. Состояние репозитория будет перечитано автоматически. Обновление рабочего каталога из '%s'... Используйте '/' для создания подменю Взять локальную версию Использовать для слияния программу Взять внешнюю версию Использовать thin pack (для медленных сетевых подключений) Имя пользователя Проверить базу данных Проверка базы объектов при помощи fsck Наглядно Показать историю ветви %s Показать историю всех ветвей Показать историю всех ветвей подмодуля Показать историю текущей ветви подмодуля Показать историю текущей ветви Визуализация этих изменений в субмодуле В процессе... пожалуйста, ждите... Изменения не сохранены.

Файл %s изменен.

Подготовьте и сохраните изменения перед началом слияния. В случае необходимости это позволит прервать операцию слияния.
 Предыдущее слияние не завершено из-за конфликта.

Для файла %s возник конфликт слияния.

Разрешите конфликт, подготовьте файл и сохраните. Только после этого можно начать следующее слияние.
 Вы находитесь не в локальной ветви.

Если вы хотите снова вернуться к какой-нибудь ветви, создайте ее сейчас, начиная с 'Текущего отсоединенного состояния'. Прежде чем сохранить, исправьте вышеуказанные ошибки. Ваш публичный ключ OpenSSH Ваш ключ находится в: %s [На уровень выше] наборы Программа commit-tree завершилась с ошибкой: ошибка критическая ошибка: невозможно разрешить %s критическая ошибка: %s: нет такого файла или каталога получение %s файлов файлы извлечены изменения в файлах отменены git-gui - графический пользовательский интерфейс к Git. git-gui: критическая ошибка строк прокомментировано объекты pt. отправить %s чистка внешнего %s Программа update-ref завершилась с ошибкой: предупреждение предупреждение: Tcl не поддерживает кодировку '%s'. Программа write-tree завершилась с ошибкой: 