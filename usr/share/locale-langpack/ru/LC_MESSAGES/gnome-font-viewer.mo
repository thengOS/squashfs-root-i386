��            )         �     �  	   �     �  	   �     �     �     �     �  �     �   �     �     �     �  	   �     �     �  E   �     �          !     '     ,  !   L     n     �     �     �  	   �     �     �  =  �          6  
   J     U     s  +   �  %   �  +   �  j  	  w  m
     �     �  "        2     I     Z  o   m     �  2   �  
     
   (  D   3  9   x  >   �     �     �  E        K     Y  �   p                                                                                  	      
                                                About All Fonts Back Copyright Description FONT-FILE OUTPUT-FILE Font Viewer GNOME Font Viewer GNOME Font Viewer also supports installing new font files downloaded in the .ttf and other formats. Fonts may be installed only for your use or made available to all users on the computer. GNOME Font Viewer shows you the fonts installed on your computer for your use as thumbnails. Selecting any thumbnails shows the full view of how the font would look under various sizes. Info Install Install Failed Installed Name Quit Run '%s --help' to see a full list of available command line options. SIZE Show the application's version Style TEXT Text to thumbnail (default: Aa) This font could not be displayed. Thumbnail size (default: 128) Type Version View fonts on your system [FILE...] fonts;fontface; translator-credits Project-Id-Version: gnome-utils trunk
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-font-viewer&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 22:21+0000
PO-Revision-Date: 2015-07-10 10:32+0000
Last-Translator: Stas Solovey <whats_up@tut.by>
Language-Team: Русский <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:15+0000
X-Generator: Launchpad (build 18115)
Language: ru
10<=4 && (n%100<10 || n%100>=20) ? 1: 2);
 О приложении Все шрифты Назад Авторское право Описание ФАЙЛ-ШРИФТА ФАЙЛ-ВЫВОДА Просмотрщик шрифтов Просмотрщик шрифтов GNOME Просмотрщик шрифтов GNOME также поддерживает установку новых шрифтов из файлов формата .ttf и прочих форматов. Шрифты можно установить для текущего пользователя или для всех пользователей компьютера. Просмотрщик шрифтов GNOME показывает в виде миниатюр установленные на вашем компьютере шрифты. Выбор миниатюры открывает подробный режим просмотра шрифта, как будет выглядеть шрифт при различных размерах. Информация Установить Сбой при установке Установлено Название Закончить Введите '%s --help' чтобы увидеть полный список доступных команд. РАЗМЕР Показать версию приложения Стиль ТЕКСТ Текст для миниатюры (по умолчанию: Аа) Не удалось показать этот шрифт. Размер миниатюры (по умолчанию: 128) Тип Версия Просмотр шрифтов, доступных в системе [ФАЙЛ…] шрифты;fontface; Юрий Мясоедов <ymyasoedov@yandex.ru>, 2012, 2013, 2014.
Станислав Соловей <whats_up@tut.by>, 2015.

Launchpad Contributions:
  Stas Solovey https://launchpad.net/~whats-up
  Yuri Myasoedov https://launchpad.net/~ymyasoedov 