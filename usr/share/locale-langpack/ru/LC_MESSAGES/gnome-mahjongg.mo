��    @        Y         �  *   �  �  �     H  6   M  .   �  �   �     9  �   H     �  ;   
	     F	     N	  
   W	     b	     h	     �	     �	     �	     �	     �	     �	     �	  !   �	     
     
     0
     7
     <
     P
     a
     n
  O   �
     �
     �
     �
  	   �
                 	   (  	   2  	   <     F     S     Y     b     p     x     �     �     �     �  #   �     �               9  !   T     v     �     �     �     �      G     �  O     �  c   �  @   >  !       �  �  �  )   P  e   z     �     �            P   0     �     �     �  #   �  
   �     �  6   �  H   .  (   w     �     �  
   �  *   �          "  )   =  �   g     �               &     <     [     k     |     �     �     �     �     �     �          *  
   @     K  H   ]     �     �     �     �     �               2     N     _  ~  }  W   �        /   4                       ?   $   :                      ,                 2         #   ;              <   6   1         %   -   9   @           	      (         +                      8       3      )          5      "                                     *   &   >      .           
       !   0   7       '   =       A matching game played with Mahjongg tiles A solitaire version of the classic Eastern tile game. Tiles are stacked on the board at the start of the game. The goal is to remove all the tiles in as little time as possible. Select two matching tiles and they will disappear from the board, but you can only select a tile if there is an empty space to its left or right at the same level. Be careful: tiles that look alike may actually be slightly different. Date Disassemble a pile of tiles by removing matching pairs Do you want to start a new game with this map? Each puzzle has at least one solution.  You can undo your moves and try and find the solution, restart this game, or start a new one. GNOME Mahjongg GNOME Mahjongg features a variety of starting layouts, some easy and some difficult. If you get stuck, you can ask for a hint, but this adds a large time penalty. Height of the window in pixels If you continue playing the next game will use the new map. Layout: Mahjongg Main game: Maps: Match tiles and clear the board Moves Left: New Game OK Pause the game Paused Preferences Print release version and exit Receive a hint for your next move Redo your last move There are no more moves. Tiles: Time Undo your last move Unpause the game Use _new map Width of the window in pixels You can also try to reshuffle the game, but this does not guarantee a solution. _About _Background color: _Close _Contents _Continue playing _Help _Layout: _Mahjongg _New Game _New game _Preferences _Quit _Restart _Restart Game _Scores _Shuffle _Theme: _Undo game;strategy;puzzle;board; mahjongg map nameCloud mahjongg map nameConfounding Cross mahjongg map nameDifficult mahjongg map nameEasy mahjongg map nameFour Bridges mahjongg map nameOverpass mahjongg map namePyramid's Walls mahjongg map nameRed Dragon mahjongg map nameThe Ziggurat mahjongg map nameTic-Tac-Toe translator-credits true if the window is maximized Project-Id-Version: gnome-games trunk
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-mahjongg&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 22:17+0000
PO-Revision-Date: 2015-12-04 15:00+0000
Last-Translator: vantu5z <Unknown>
Language-Team: русский <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:21+0000
X-Generator: Launchpad (build 18115)
Language: ru
 Игра на поиск одинаковых фишек Маджонг Солитер классической азиатской игры. В начале игры фишки расположены поверх друг друга на игровом столе. Цель игры — убрать все фишки, затратив на это как можно меньше времени. Чтобы убрать фишки, найдите и выделите две одинаковые фишки, и они исчезнут с игрового стола. Можно выбирать только те фишки, которые расположены на одном и том же уровне и у которых свободна левая или правая грань. Будьте внимательны: некоторые фишки немного похожи друг на друга, но на самом деле являются разными. Дата Разберите большую кучу фишек, убирая совпадающие пары Начать новую игру с этим раскладом? Каждая головоломка имеет по крайней мере одно решение. Вы можете отменить сделанные ходы и попытаться найти решение, перезапустить эту игру или начать новую. Маджонг GNOME В маджонге GNOME используются различные раскладки фишек, некоторые раскладки проще, некоторые сложнее. Если вы затрудняетесь сделать ход, вы можете воспользоваться подсказкой, но в этом случае будет начислено штрафное время. Высота окна в пикселах Если продолжить, у следующей игры будет другой расклад. Раскладка: Маджонг Основная игра: Расклады: Очистите доску от фишек, выбирая одинаковые Осталось ходов: Новая игра OK Приостановить игру Пауза Параметры Показать номер версии и выйти Получить подсказку для следующего хода Вернуть последний ход Ходов больше нет. Фишки: Время Отменить последний ход Возобновить игру В_ыбрать карту Ширина окна в пикселах Вы можете перемешать фишки, но это не гарантирует решения головоломки. _О приложении _Цвет фона: _Закрыть _Содержание _Продолжить игру _Справка _Расклад: _Маджонг _Новая игра _Новая игра _Параметры _Закончить _Начать заново Н_ачать заново _Результаты _Перемешать _Тема: _Отменить игра;стратегия;головоломка;настольная; Облако Смешанный крест Сложная Простая Четыре моста Переход Стены пирамиды Красный дракон Зиккурат Крестики-нолики Валек Филиппов aka frob
Дмитрий Мастрюков
Вячеслав Диконов
Михаил Яхонтов
Леонид Кантер

Launchpad Contributions:
  Vassili Platonov https://launchpad.net/~vassilip
  Yuri Myasoedov https://launchpad.net/~ymyasoedov
  vantu5z https://launchpad.net/~vantu5z
  ☠Jay ZDLin☠ https://launchpad.net/~black-buddha666 если выбрано, то окно имеет максимальный размер 