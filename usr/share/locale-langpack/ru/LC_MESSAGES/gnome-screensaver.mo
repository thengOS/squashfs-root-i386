��    7      �  I   �      �     �     �     �     �  )   
     4  (   @     i          �  "   �  E   �       %   +     Q     Z     b  )   �     �  *   �     �  /   �     #  	   6  8   @  "   y     �     �     �     �     �     �       D   9  `   ~     �     �  )   	     A	  *   S	  #   ~	  	   �	     �	  D   �	  D   
     R
  !   q
  ,   �
  B   �
  
          '         >  +   _  �  �  #     &   9  3   `  U   �  n   �     Y  T   m  *   �  *   �  ,     8   E  �   ~       ]         ~     �  H   �  >   �     8  =   W     �  g   �  7        S  �   a  K   �  0   ?  3   p     �  6   �  C   �  S   ?  $   �  �   �  �   J  /   E  1   u  ?   �     �  L     =   O      �  !   �  �   �  �   X  $   �  G   �  `   F  �   �     I     X  D   u  6   �  N   �         -              0                	      1              4      %       '      *      6                 !   &          #         7   
   "       (      5   /   +              )   2                           ,   $         .            3                         (current) Unix password: Authentication failed. Can't set PAM_TTY=%s Cannot get username Causes the screensaver to exit gracefully Checking… Command to invoke from the logout button Don't become a daemon Enable debugging code Enter new Unix password: Error while changing NIS password. If the screensaver is active then deactivate it (un-blank the screen) Incorrect password. Launch screensaver and locker program Log _Out MESSAGE Message to show in the dialog No longer permitted to access the system. No password supplied Not permitted to gain access at this time. Not used Password has been already used. Choose another. Password unchanged Password: Query the length of time the screensaver has been active Query the state of the screensaver Retype new Unix password: S_witch User… Screensaver Show debugging output Show the logout button Show the switch user button Sorry, passwords do not match Tells the running screensaver process to lock the screen immediately The screensaver has been active for %d second.
 The screensaver has been active for %d seconds.
 The screensaver is active
 The screensaver is inactive
 The screensaver is not currently active.
 Time has expired. Turn the screensaver on (blank the screen) Unable to establish service %s: %s
 Username: Version of this application You are required to change your password immediately (password aged) You are required to change your password immediately (root enforced) You have the Caps Lock key on. You must choose a longer password You must wait longer to change your password Your account has expired; please contact your system administrator _Password: _Unlock failed to register with the message bus not connected to the message bus screensaver already running in this session Project-Id-Version: gnome-screensaver trunk
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-screensaver&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-01-04 14:27+0000
PO-Revision-Date: 2016-01-05 00:35+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Russian <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 17:43+0000
X-Generator: Launchpad (build 18115)
 (текущий) пароль UNIX: Сбой аутентификации. Не удалось установить PAM_TTY=%s Не удалось получить получить имя пользователя Приводит к корректному завершению работы экранной заставки Проверка… Команда, вызываемая при нажатии кнопки выхода Не становиться демоном Включить режим отладки Введите новый пароль UNIX: Ошибка при изменении пароля NIS. Если хранитель экрана активен, деактивировать его (восстановить экран) Неверный пароль. Запустить хранитель экрана и программу блокировки Завершить _сеанс СООБЩЕНИЕ Показывать сообщение в диалоговом окне Больше нет доступа к этой системе. Пароль не введён Нет доступа к системе в это время. Не использовано Этот пароль уже был использован. Выберите другой пароль. Пароль оставлен без изменения Пароль: Запрашивает промежуток времени, в течение которого работала экранная заставка Запрашивает состояние экранной заставки Повторите новый пароль UNIX: Пе_реключить пользователя… Хранитель экрана Показывать сообщения отладки Показывать кнопку завершения сеанса Показывать кнопку переключения пользователя Пароли не совпадают Сообщает запущенному процессу заставки экрана немедленно заблокировать экран Заставка экрана работала в течение %d секунды.
 Заставка экрана работала в течение %d секунд.
 Заставка экрана работала в течение %d секунд.
 Заставка экрана включёна
 Заставка экрана выключёна
 Заставка экрана сейчас неактивна.
 Время истекло. Включить экранную заставку (чёрный экран) Не удалось установить службу %s: %s
 Имя пользователя: Версия приложения Вам необходимо сменить ваш пароль немедленно (срок действия пароля истёк) Вам необходимо сменить пароль немедленно (требование администратора) Включён режим Caps Lock. Вы должны выбрать более длинный пароль Вы должны подождать дольше, чтобы сменить ваш пароль Срок действия вашей учетной записи истёк, свяжитесь со своим системным администратором _Пароль: Разблокировать ошибка регистрации на шине сообщений не подключён к шине сообщений хранитель экрана уже запущен в этом сеансе 