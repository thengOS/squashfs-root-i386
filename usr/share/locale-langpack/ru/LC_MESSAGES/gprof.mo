��    a      $  �   ,      8     9  )   I     s     �     �     �     �     �     	  '   	  (   F	     o	     �	  0   �	     �	     �	     �	     
     5
     L
  8   b
  6   �
  $   �
  %   �
  %        C  	   Z  +   d  !   �  6   �     �  (   	  J   2  ,   }     �  #   �     �     �  (     )   ;  W   e  O   �  /     7   =  #   u     �  )   �  %   �  8     -   @  '   n  &   �  "   �  5   �  -     4   D  3   y  %   �  ,   �  "         #  A   ?  !   �     �     �  
   �     �  	   �  L   �  !   *     L     [     i     �  I   �  1   �          1     H     _  *   x     �     �     �  
   �     �     �  3   �     
                    #     (     G     M  �  T     �  B     >   V  @   �  +   �  -     (   0  *   Y  "   �  F   �  L   �  8   ;  &   t  T   �  -   �          -  '   K  1   s  1   �  K   �  I   #  S   m  5   �  @   �  %   8     ^  ]   r  6   �  W     5   _  H   �  `   �  Z   ?  .   �  <   �  *     +   1  R   ]  P   �  �     ~   �  P   	   [   Z   s   �   ;   *!  W   f!  T   �!  �   "  N   �"  9   �"  ?   !#  G   a#  k   �#  \   $  C   r$  S   �$  Q   
%  B   \%  H   �%  6   �%  �   &  +   �&     �&     �&     	'  #   '     ;'  U   R'  4   �'     �'     �'  -   (  >   9(  �   x(  @   D)     �)     �)     �)     �)  =   �)     **     9*     F*     U*     h*     y*  R   �*     �*     �*     �*  
   �*  
   +  6   +  
   I+     T+     U   :       Z              R          -                 )               +   %   C   D   @         B   H       J       T   [      6   >   7   4   	   `   ;   O   <       .   /   5               9           3       &             E         A   L   I   _      Y   a      2       \           V              '   ^   1       W   M       P          $   "   #   F       ,   
             =   Q   X   G                 *   8       ]       S           0          N       (   ?      K         !           			Call graph

 		     Call graph (explanation follows)

 	%d basic-block count record
 	%d basic-block count records
 	%d call-graph record
 	%d call-graph records
 	%d histogram record
 	%d histogram records
 


flat profile:
 

Top %d Lines:

     Line      Count

 
%9lu   Total number of line executions
 
Each sample counts as %g %s.
 
Execution Summary:

 
granularity: each sample hit covers %ld byte(s)  <cycle %d as a whole> [%d]
  <cycle %d>  for %.2f%% of %.2f %s

  for %.2f%% of %.2f seconds

  no time accumulated

  no time propagated

 %6.6s %5.5s %7.7s %11.11s %7.7s %7.7s     <spontaneous>
 %6.6s %5.5s %7.7s %7.7s %7.7s %7.7s     <spontaneous>
 %9.2f   Average executions per line
 %9.2f   Percent of the file executed
 %9ld   Executable lines in this file
 %9ld   Lines executed
 %c%c/call %s: %s: found bad tag %d (file corrupted?)
 %s: %s: not in executable format
 %s: %s: unexpected EOF after reading %u of %u samples
 %s: %s: unexpected end of file
 %s: -c not supported on architecture %s
 %s: Only one of --function-ordering and --file-ordering may be specified.
 %s: address size has unexpected value of %u
 %s: can't do -c
 %s: can't find .text section in %s
 %s: could not locate `%s'
 %s: could not open %s.
 %s: debugging not supported; -d ignored
 %s: different scales in histogram records %s: dimension abbreviation changed between histogram records
%s: from '%c'
%s: to '%c'
 %s: dimension unit changed between histogram records
%s: from '%s'
%s: to '%s'
 %s: don't know how to deal with file format %d
 %s: file '%s' does not appear to be in gmon.out format
 %s: file `%s' has bad magic cookie
 %s: file `%s' has no symbols
 %s: file `%s' has unsupported version %d
 %s: file too short to be a gmon file
 %s: found a symbol that covers several histogram records %s: gmon.out file is missing call-graph data
 %s: gmon.out file is missing histogram
 %s: incompatible with first gmon file
 %s: overlapping histogram records
 %s: profiling rate incompatible with first gmon file
 %s: ran out room for %lu bytes of text space
 %s: somebody miscounted: ltab.len=%d instead of %ld
 %s: sorry, file format `prof' is not yet supported
 %s: unable to parse mapping file %s.
 %s: unexpected EOF after reading %d/%d bins
 %s: unknown demangling style `%s'
 %s: unknown file format %s
 %s: warning: ignoring basic-block exec counts (use -l or --line)
 %s:%d: (%s:0x%lx) %lu executions
 %time *** File %s:
 <cycle %d> <indirect child> <unknown> Based on BSD gprof, copyright 1983 Regents of the University of California.
 File `%s' (version %d) contains:
 Flat profile:
 GNU gprof %s
 Index by function name

 Report bugs to %s
 This program is free software.  This program has absolutely no warranty.
 [cg_tally] arc from %s to %s traversed %lu times
 [find_call] %s: 0x%lx to 0x%lx
 [find_call] 0x%lx: bsr [find_call] 0x%lx: jal [find_call] 0x%lx: jalr
 [find_call] 0x%lx: jsr%s <indirect_child>
 called calls children cumulative descendants index index %% time    self  children    called     name
 name parents self self   time time is in ticks, not seconds
 total total  Project-Id-Version: gprof 2.20.90
Report-Msgid-Bugs-To: bug-binutils@gnu.org
POT-Creation-Date: 2014-02-10 09:42+1030
PO-Revision-Date: 2010-11-21 18:23+0000
Last-Translator: Yuri Kozlov <yuray@komyakino.ru>
Language-Team: Russian <gnu@mx.ru>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:03+0000
X-Generator: Launchpad (build 18115)
 			Граф вызовов

 		     Граф вызовов (объяснения далее)

 	%d запись счётчика простых блоков
 	%d записей счётчика простых блоков
 	%d запись графа вызовов
 	%d записей графа вызовов
 	%d запись гистограммы
 	%d записей гистограммы
 


плоский профиль:
 

Первые %d строк:

     Строка      Счётчик

 
%9lu   Полное количество выполненных строк
 
Все образцы считаются как %g %s.
 
Итог по выполнению:

 
грануляция: каждый образец охватывает %ld байт  <охватывающий цикл %d> [%d]
  <цикл %d>  для %.2f%% из %.2f %s

  для %.2f%% за %.2f секунд

  нет накопленного времени

  нет накопленного времени

 %6.6s %5.5s %7.7s %11.11s %7.7s %7.7s     <самопроизвольно>
 %6.6s %5.5s %7.7s %7.7s %7.7s %7.7s     <самопроизвольно>
 %9.2f   Среднее количество выполнений на строку
 %9.2f   процент файла выполнено
 %9ld   исполняемых строк в этом файле
 %9ld   строк выполнено
 %c%c/вызовов %s: %s: найдена неправильная метка %d (файл повреждён?)
 %s: %s: не в исполняемым формате
 %s: %s: неожиданный EOF после чтения %u из %u образцов
 %s: %s: неожиданный конец файла
 %s: -c не поддерживается на архитектуре %s
 %s: можно указать что-то одно: --function-ordering или --file-ordering.
 %s: размер адреса содержит неожиданное значение %u
 %s: не удалось выполнить -c
 %s: не удалось найти раздел .text в %s
 %s: не удалось найти «%s»
 %s: не удалось открыть %s.
 %s: отладка не поддерживается; -d игнорируется
 %s: различные масштабы в записях гистограммы %s: аббревиатура измерений изменена в записях гистограммы
%s: с «%c»
%s: на «%c»
 %s: единицы измерения изменены в записях гистограммы
%s: с «%s»
%s: на «%s»
 %s: непонятно что делать с файлом в формате %d
 %s: кажется, содержимое файла «%s» не в формате gmon.out
 %s: файл «%s» содержит неправильный идентификатор формата файла
 %s: файл «%s» не содержит символов
 %s: файл «%s» в формате неподдерживаемой версии %d
 %s: файл слишком короткий, чтобы быть файлом gmon
 %s: найден символ, который присутствует в нескольких записях гистограммы %s: в файле gmon.out нет данных по графу вызовов
 %s: в файле gmon.out нет гистограммы
 %s: несовместимо с первым файлом gmon
 %s: перекрывающиеся записи гистограммы
 %s: степень профилирования несовместима с первым файлом gmon
 %s: не хватает место для %lu байт в пространстве кода
 %s: кто-то обсчитался: ltab.len=%d вместо %ld
 %s: файл в формате «prof» пока не поддерживается
 %s: не удалось разобрать отображённый файл %s.
 %s: неожиданный EOF после чтения %d/%d bins
 %s: неизвестный стиль декодирования «%s»
 %s: неизвестный формат файла %s
 %s: предупреждение: игнорируется счётчик выполнений простого блока (используйте -l или --line)
 %s:%d: (%s:0x%lx) %lu выполнений
 %time *** Файл %s:
 <цикл %d> <косвенный потомок> <неизвестно> На основе BSD gprof, copyright 1983 Regents of the University of California.
 Файл «%s» (версия %d) содержит:
 Плоский профиль:
 GNU gprof %s
 Индекс по имени функции

 Об ошибках сообщайте по адресу <%s>
 Эта программа является открытым программным обеспечением. Эта программа не имеет абсолютно никаких гарантий.
 [cg_tally] ребро из %s в %s проходит %lu раз
 [find_call] %s: 0x%lx к 0x%lx
 [find_call] 0x%lx: bsr [find_call] 0x%lx: jal [find_call] 0x%lx: jalr
 [find_call] 0x%lx: jsr%s <косвенный_потомок>
 вызвана вызовы потомок суммарное потомков индекс индекс %% время    сама  потомок    вызван     имя
 имя родителей сама сама   время время в тиках, а не в секундах
 всего всего  