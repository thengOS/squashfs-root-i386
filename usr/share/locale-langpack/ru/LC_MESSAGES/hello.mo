��            )         �  l   �  n        �  0   �     �  ,   �  ,     ,   H  '   u  -   �      �  (   �  (        >     ^     ~  �   �  ?   x     �  )   �     �               0     G  '   I     q          �     �  �  �  �   `	  �   
  +   �
  H   
  )   S  Z   }  \   �  Z   5  X   �  [   �  2   E  Y   x  Z   �  -   -  -   [     �  e  �  ^   �     Q  `   o  C   �  =     6   R  -   �     �  Y   �  +        @     ]     }                  	                                      
                                                                                  -h, --help          display this help and exit
  -v, --version       display version information and exit
   -t, --traditional       use traditional greeting
  -g, --greeting=TEXT     use TEXT as the greeting message
 %s home page: <%s>
 %s home page: <http://www.gnu.org/software/%s/>
 %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 ' Copyright (C) %d Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
 General help using GNU software: <http://www.gnu.org/gethelp/>
 Hello, world! Print a friendly, customizable greeting.
 Report %s bugs to: %s
 Report bugs to: %s
 Unknown system error Usage: %s [OPTION]...
 ` conversion to a multibyte string failed extra operand hello, world memory exhausted write error Project-Id-Version: hello 2.9
Report-Msgid-Bugs-To: bug-hello@gnu.org
POT-Creation-Date: 2014-11-16 11:53+0000
PO-Revision-Date: 2016-06-09 11:04+0000
Last-Translator: Данил Тютюник <den.tyutyunik@ya.ru>
Language-Team: Russian <gnu@mx.ru>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:33+0000
X-Generator: Launchpad (build 18115)
Language: ru
   -h, --help          показать эту справку и выйти
  -v, --version       показать информацию о версии и выйти
   -t, --traditional использовать традиционное приветствие
  -g, --greeting=TEXT использовать TEXT в качестве приветственного сообщения
 Домашняя страница %s: <%s>
 Домашняя страница %s: <http://www.gnu.org/software/%s/>
 %s: неверный ключ — «%c»
 %s: ключ «%c%s» должен использоваться без аргумента
 %s: двусмысленный параметр «%s»; возможные варианты: %s: ключ «--%s» должен использоваться без аргумента
 %s: ключ «--%s» должен использоваться с аргументом
 %s: ключ «-W %s» должен использоваться без аргумента
 %s: неоднозначный ключ «-W %s»
 %s: ключ «-W %s» должен использоваться с аргументом
 %s: ключ должен использоваться с аргументом — «%c»
 %s: неизвестный ключ «%c%s»
 %s: неизвестный ключ «--%s»
 » Copyright (C) %d Free Software Foundation, Inc.
Лицензия GPLv3+: GNU GPL версии 3 или новее <http://gnu.org/licenses/gpl.html>
Это свободное ПО: вы можете продавать и распространять его.
Нет НИКАКИХ ГАРАНТИЙ до степени, разрешённой законом.
 Справка по работе с программами GNU: <http://www.gnu.org/gethelp/>
 Здравствуй, мир! Печатает дружественное, настраиваемое приветствие.
 Об ошибках в %s сообщайте по адресу: %s
 Об ошибках сообщайте по адресу: %s
 Неизвестная системная ошибка Использование: %s [КЛЮЧ]…
 « преобразование в многобайтную строку не удалось дополнительный операнд здравствуй, мир память исчерпана ошибка записи 