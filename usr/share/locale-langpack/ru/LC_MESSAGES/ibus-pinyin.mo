��    *      l  ;   �      �     �     �  	   �      �  1   �     (     ,     2     >     F  
   O     Z     h     |     �     �     �     �     �     �     �     �  	   �     �                1     6     J     g     s     �  
   �     �     �     �     �     �     �     �       |  *  ,   �     �     �  (     1   +     ]     a  >   w     �     �     �     �  F   
	     Q	     f	     s	  .   �	  N   �	  
   
     
  /   -
     ]
  	   }
     �
     �
     �
     �
  $   �
  0   �
     '      :  @   [     �     �  D   �            ^     ^   w  I   �                   &      	                                   !                                #          (            $                      "   %                  '   *           
         )                        <b>Initial state</b> <b>Other</b> <b>UI</b> <big><b>IBus Pinyin %s</b></big> <small>Copyright (c) 2009-2010 Peng Huang</small> ABC About Auto commit Chinese Chinese: Dictionary Double pinyin Edit custom phrases English Full Full pinyin Full/Half width Full/Half width punctuation General Half Half/full width: Incomplete pinyin Language: MSPY Number of candidates: Orientation of candidates: PYJJ Pinyin input method Pinyin input method for IBus Preferences Punctuations: Simplfied/Traditional Chinese Simplified Traditional Use custom phrases ZGPY ZRM [,] [.] flip page [-] [=] flip page [Shift] select candidate http://ibus.googlecode.com Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-09-20 13:06+0000
PO-Revision-Date: 2014-11-11 11:50+0000
Last-Translator: ☠Jay ZDLin☠ <Unknown>
Language-Team: Russian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:36+0000
X-Generator: Launchpad (build 18115)
 <b>Начальное состояние</b> <b>Прочее</b> <b>Интерфейс</b> <big><b>IBus Пиньинь %s</b></big> <small>Copyright (c) 2009-2010 Peng Huang</small> ABC О программе Автоматически подтверждать фразу Китайский Иероглифы: Словарь Двойной пиньинь Редактировать пользовательские фразы Английский Полная Полный пиньинь Полная/половинная ширина Знаки препинания полной/половинной ширины Общие Половинная Полная/половинная ширина: Неполный пиньинь Язык: MSPY Число кандидатов Число кандидатов PYJJ Метод ввода пиньинь Метод ввода пиньинь для IBus Параметры Знаки препинания: Упрощенные/традиционные иероглифы Упрощенные Традиционные Использовать пользовательские фразы ZGPY ZRM Использовать клавиши [,] [.] для переключения страниц Использовать клавиши [-] [=] для переключения страниц Использовать [Shift] для выбора кандидатов http://ibus.googlecode.com 