��    8      �  O   �      �  +   �  %     /   +     [  /   t     �     �     �     �  2     2   ;     n     �     �     �     �     �  1   �     0  G   K     �     �     �     �     �  (   �  '        8     I     ]  5   |     �     �     �     �     	     .	     6	     J	  '   W	  !   	     �	  )   �	     �	     �	  "   
     .
  <   G
  :   �
  0   �
  *   �
  :        V  &   b  ]   �  �  �  7   n  E   �  S   �  1   @  )   r  ,   �  ?   �  N   	  =   X  s   �  e   
  P   p  P   �  9        L  <   Y  8   �  f   �  9   6  �   p       8     !   O  #   q     �  Q   �  G   �     A  )   a  :   �  X   �  ,     ;   L     �  B   �  @   �     #  %   @     f  7   �  E   �       W     /   o  )   �  D   �  /     y   >  i   �  o   "  Q   �  w   �     \  9   r  �   �     -                       *         .   7       
                       &       (   )                         8   ,      2         /              	      +   '   0   $       #   1       !                        4           %                               6   3                "      5      stack[%d] =>   stack: %d   ind_level: %d
 %s: missing argument to parameter %s
 %s: option ``%s'' requires a numeric parameter
 %s: unknown option "%s"
 (Lines with comments)/(Lines with code): %6.3f
 CANNOT FIND '@' FILE! Can't close output file %s Can't open backup file %s Can't open input file %s Can't preserve modification time on backup file %s Can't preserve modification time on output file %s Can't stat input file %s Can't write to backup file %s EOF encountered in comment Error Error closing input file %s Error reading input file %s File %s contains NULL-characters: cannot proceed
 File %s is too big to read File named by environment variable %s does not exist or is not readable GNU indent %s
 Internal buffering error Line broken Line broken 2 ParseStack [%d]:
 Profile contains an unterminated comment Profile contains unpalatable characters Read profile %s
 Stmt nesting error. System problem reading file %s There were %d non-blank output lines and %d comments
 Unexpected end of file Unknown code to parser Unmatched 'else' Unterminated character constant Unterminated string constant Warning Zero-length file %s command line indent:  Strange version-control value
 indent:  Using numbered-existing
 indent: %s:%d: %s: indent: Can't make backup filename of %s
 indent: Fatal Error:  indent: System Error:  indent: Virtual memory exhausted.
 indent: can't create %s
 indent: can't have filenames when specifying standard input
 indent: only one input file when output file is specified
 indent: only one input file when stdout is used
 indent: only one output file (2nd was %s)
 old style assignment ambiguity in "=%c".  Assuming "= %c"
 option: %s
 set_option: internal error: p_type %d
 usage: indent file [-o outfile ] [ options ]
       indent file1 file2 ... fileN [ options ]
 Project-Id-Version: indent 2.2.10
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-01-31 17:27+0100
PO-Revision-Date: 2014-02-05 12:11+0000
Last-Translator: ☠Jay ZDLin☠ <Unknown>
Language-Team: Russian <gnu@mx.ru>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:47+0000
X-Generator: Launchpad (build 18115)
   стек[%d] =>   стек: %d    уровень: %d
 %s: отсутствует аргумент у параметра %s
 %s: параметру «%s» необходим числовой аргумент
 %s: неизвестный параметр "%s"
 (комментарии)/(код): %6.3f
 НЕ УДАЛОСЬ НАЙТИ ФАЙЛ '@'! Не удалось закрыть выходной файл %s Не удалось открыть резервную копию файла %s Не удалось открыть входной файл %s Не удалось сохранить дату изменения на файле резервной копии %s Не удалось сохранить время изменения выходного файла %s Не удалось получить статус входного файла %s Не удалось записать резервную копию файла %s Конец файла внутри комментария Ошибка Ошибка закрытия входного файла %s Ошибка чтения входного файла %s Файл %s содержит нулевые символы: невозможно продолжить
 Файл %s слишком велик для чтения Файл, указанный в переменной окружения %s, не существует или недоступен для чтения GNU indent: %s
 Внутренняя ошибка буферизации Строка повреждена Строка повреждена 2 ParseStack [%d]:
 Профиль содержит незавершённый комментарий Профиль содержит недопустимые символы Чтение профиля %s
 Ошибка вложенности stmt. Системная ошибка чтения файла %s Было %d непустых выходных строк и %d комментариев
 Неожиданный конец файла Неизвестный для анализатора код 'else' без пары Незавершённая символьная константа Незавершённая строковая константа Предупреждение Файл нулевой длины %s командная строка indent: Странное значение версии
 indent: Используются существующие числа
 indent: %s:%d: %s: indent: Не удалось создать резервную копию файла %s
 indent: Неисправимая ошибка:  indent: Системная ошибка:  indent: Закончилась виртуальная память.
 indent: невозможно создать %s
 indent: нельзя указывать имена файлов, когда указан стандартный ввод
 indent: только один входной файл, когда указан выходной файл
 indent: только один входной файл, когда указан стандартный ввод
 indent: только один выходной файл (второй был %s)
 двусмысленное присваивание в старом стиле в "=%c". Принимается "= %c"
 параметр: %s
 set_option: внутренняя ошибка: p_type %d
 использование: indent файл [-o выходной­­_файл ] [ параметры ]
               indent файл1 файл2 ... файлN [ параметры ]
 