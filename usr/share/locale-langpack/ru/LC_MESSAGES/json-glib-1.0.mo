��          �      \      �     �     �          ,     L  &   f     �     �  !   �     �     �     �          %  )   E  M   o  N   �  %     '   2  �  Z  1   �  <   (  >   e  2   �  -   �  `        f  /   o  :   �     �  0   �  7     (   R  C   {  @   �  ^    	  ^   _	  o   �	  [   .
                               	                                             
                       %s: %s: error closing: %s
 %s: %s: error opening file: %s
 %s: %s: error parsing file: %s
 %s: %s: error writing to stdout %s:%d:%d: Parse error: %s Error parsing commandline options: %s
 FILE Format JSON files. GVariant class '%c' not supported Indentation spaces Invalid GVariant signature Invalid first character '%c' JSON data is empty JSON data must be UTF-8 encoded No node available at the current position The index '%d' is greater than the size of the array at the current position. The index '%d' is greater than the size of the object at the current position. Try "%s --help" for more information. Unexpected extra elements in JSON array Project-Id-Version: json-glib
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-03-01 22:13+0000
PO-Revision-Date: 2014-07-10 10:59+0000
Last-Translator: ☠Jay ZDLin☠ <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:32+0000
X-Generator: Launchpad (build 18115)
 %s: %s: ошибка при закрытии: %s
 %s: %s: ошибка при открытии файла: %s
 %s: %s: ошибка при обработке файла: %s
 %s: %s: ошбика при записи в stdout %s:%d:%d: ошибка обработки: %s Ошибка при обработке параметров командной строки: %s
 ФАЙЛ Форматирование файлов JSON. Класс GVariant '%c' не поддерживается Отступы Недопустимая подпись GVariant Недопустимый первый символ '%c' Данные JSON отсутствуют Данные JSON должны быть в кодировке UTF-8 Узел отсутствует в текущей позиции Индекс '%d' больше размера массива в текущей позиции. Индекс '%d' больше размера объекта в текущей позиции. Для получения дополнительной информации используйте "%s --help". Неожиданные дополнительные элементы в массиве JSON 