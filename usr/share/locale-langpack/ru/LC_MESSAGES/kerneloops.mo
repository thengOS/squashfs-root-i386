��          |      �             !  $   (  �   M  &   6     ]     c     o  �   r     G      K     l  �  ~       P   (  ]  y  e   �     =     L     h    o     �  E   �     �     
                                            	          Always Connecting to system bus failed: %s
 Diagnostic information from your Linux kernel has been sent to <a href="http://oops.kernel.org">oops.kernel.org</a> for the Linux kernel developers to work on. 
Thank you for contributing to improve the quality of the Linux kernel.
 Kernel bug diagnostic information sent Never Never again No There is diagnostic information available for this failure. Do you want to submit this information to the <a href="http://oops.kernel.org/">www.oops.kernel.org</a> website for use by the Linux kernel developers?
 Yes Your system had a kernel failure kerneloops client Project-Id-Version: kerneloops
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-01-01 06:36-0800
PO-Revision-Date: 2016-03-11 05:55+0000
Last-Translator: Aleksey Kabanov <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:32+0000
X-Generator: Launchpad (build 18115)
 Всегда Не удалось подключиться к системной шине: %s
 Диагностическая информация из вашего ядра Linux отправлена на <a href="http://oops.kernel.org">oops.kernel.org</a> для обработки разработчиками ядра Linux. 
Спасибо за сотрудничество в деле улучшения качества ядра Linux!
 Диагностическая информация об ошибках ядра отправлена Никогда Никогда больше Нет Для этого сбоя доступна диагностическая информация. Хотите отправить её на сайт <a href="http://oops.kernel.org/">www.oops.kernel.org</a> для использования разработчиками ядра Linux?
 Да В вашей системе произошла ошибка ядра kerneloops клиент 