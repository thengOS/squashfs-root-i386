��    (      \  5   �      p  *   q  
   �  )   �     �     �  z   �     l     {  ,   �     �     �  3   �     %     6  7   O  (   �  a   �          1     P     m     �  -   �     �  (   �       &        :  -   O  '   }     �     �     �     �     �     �  	     
        )  Q  B  ]   �
     �
  Z     '   a  '   �  �   �  !   �  '   �  _   �  B   I  B   �  z   �  )   J  1   t  k   �  a     �   t  >   K  >   �  3   �  3   �     1  @   C  !   �  P   �     �  H     &   Z  U   �  R   �     *  +   ?  '   k  !   �  M   �          !     ;  K   W                 
      	           '                    $         %                             !                                             #       "             &            (       Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: ru
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&component=general
POT-Creation-Date: 2016-01-28 14:10+0000
PO-Revision-Date: 2011-05-18 05:09+0000
Last-Translator: Yuri Myasoedov <ymyasoedov@yandex.ru>
Language-Team: Russian <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 18:12+0000
X-Generator: Launchpad (build 18115)
 Группа по умолчанию, назначаемая при создании окна Индикатор: Сохранять и поддерживать разные группы, пооконно Раскладка клавиатуры Раскладка клавиатуры Клавиатурная раскладка «%s»
Авторские права © X.Org Foundation и авторы XKeyboardConfig
Сведения о лицензировании находятся в метаданных пакета Модель клавиатуры Параметры клавиатуры Загружать редко используемые раскладки и параметры Загружать дополнительные параметры Предварительный просмотр раскладок Сохранять/восстанавливать индикаторы вместе с группами раскладок Второстепенные группы Показывать флаги в апплете Показывать флаги в апплете для указания текущей раскладки Показывать названия раскладок вместо названий групп Показывать названия раскладок вместо названий групп (только для версий XFree, поддерживающих множественные раскладки) Просмотр клавиатуры, смещение по X Просмотр клавиатуры, смещение по Y Просмотр клавиатуры, высота Просмотр клавиатуры, ширина Цвет фона Цвет фона для раскладки клавиатуры Семейство шрифтов Семейство шрифтов для индикатора раскраски Размер шрифта Размер шрифта для индикатора раскладки Цвет переднего плана Цвет переднего плана для индикатора раскладки Произошла ошибка при загрузке изображения: %s Неизвестно Ошибка инициализации XKB раскладка клавиатуры модуль клавиатуры раскладка «%s» раскладки «%s» раскладки «%s» модель «%s», %s и %s нет раскладки нет параметров параметр «%s» параметры «%s» параметры «%s» 