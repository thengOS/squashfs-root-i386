��    �     �$  #  �I      (b  #   )b     Mb     gb  h   yb     �b  4   �b     .c     Cc     Rc  F   jc     �c     �c     �c     �c     �c  b   �c  ;   4d     pd  4   �d  �   �d  O  �e  	   �f     �f     �f     �f     �f     �f     �f     �f     g     	g     g     g     g     g     #g     'g     ,g     1g     8g     ?g     Gg     Ng     Tg     \g     cg     ig     ng     tg     |g     �g     �g     �g     �g     �g     �g     �g     �g     �g     �g     �g     �g     �g     �g     �g     �g     �g     �g     �g     �g     h     	h     h     h     h     h     'h     .h     3h     7h     ?h     Fh     Lh     Qh  
   Yh     dh  
   jh     uh     zh  
   ~h     �h  
   �h     �h     �h     �h  
   �h     �h  
   �h     �h     �h     �h  	   �h     �h     �h     �h     �h  
   �h     �h  	   �h     i     i     
i     i     i     i  
   i  
   $i     /i     3i     9i     <i     ?i  	   Ai     Ki     Qi     Ti     Wi  	   ^i  	   hi     ri     vi  	   |i     �i     �i     �i     �i     �i  	   �i  	   �i     �i     �i     �i     �i     �i     �i  	   �i     �i     �i     �i     �i     �i  
   �i     �i     j     0j     8j     =j     Bj     Gj  �   Jj  �   �j     �k    �k     �l  '   �l     �l  
   �l     �l  -   �l     %m     1m  9   :m     tm     }m     �m     �m     �m     �m     �m     �m     �m     �m     n     !n     :n     Mn     bn     zn     �n  
   �n     �n  (   �n     �n     �n     	o     o     $o     (o  +   +o  1   Wo  '   �o     �o     �o    �o     �p     q     q     'q  o   5q     �q     �q     �q  *   �q     	r  #   r     9r     ?r     Dr     Vr     _r     gr     lr     �r     �r     �r     �r     �r     �r     �r     �r  
   �r     �r     �r     	s     s     )s     5s     Hs     Ms     Qs     Xs     ms     �s  O   �s     �s     �s     �s  
   t     t     $t     4t     Nt  k   nt  #   �t     �t  A   u  +   Tu     �u  (   �u  @   �u     �u     v     4v  #   Iv     mv     �v     �v     �v     �v  '   �v     w     %w  =   <w  )   zw  $   �w  >   �w  �   x  #   �x  "   �x      y     (y     9y     Wy     \y     ky     �y     �y     �y     �y  (   �y     �y     z     *z     :z     Jz     bz  	   |z     �z     �z     �z     �z     �z     �z     �z     �z  
   �z     �z     {     {     {     ={     R{     h{     ~{     �{     �{     �{     �{     �{     �{  "   |     '|     ;|     Q|     f|  	   o|  	   y|     �|  	   �|     �|     �|     �|     �|  :   �|  <   '}     d}     �}     �}     �}  &   �}     �}     ~     0~     N~     m~  0   �~     �~     �~  !   �~  '        =     \  >   x     �     �  /   �  <   �  <   W�  "   ��     ��  (   р     ��     �      2�  *   S�  2   ~�  -   ��  ;   ߁  0   �     L�     a�     ��     ��  5   ��  =   �  s   )�     ��     ��  &   ΃  k   ��  E   a�     ��       �   ߄     e�  
   l�     w�     ��     ��     ��     ��     ͅ     �  �   �     �     ��     �     �     �     �  �  *�     �  !   �     2�     E�     ^�     o�  .   {�     ��     ��  E   ĉ     
�     �     /�     E�  L   \�  �   ��  H  _�  �   ��  ,  S�     ��     ��  $   ��     ǎ     ێ     �     	�     �     1�     G�     \�     q�     ��     ��     ��  	   ��     ��     я  <   �     #�     @�     W�     o�     ��     ��     Đ  
   ʐ     Ր     ݐ     �     ��     �  (   $�     M�  '   a�  5   ��  
   ��  
   ʑ  1   Ց  -   �     5�     >�     Y�     o�     ��     ��     ��     ǒ     ؒ     �     ��     �     0�     K�     f�     ��     ��     ��     ��     ѓ     ߓ     �     �     �     ��     ��     ��      �     �     #�  ,   (�     U�     r�     ��     ��     ��     ��  U   ��  	   �     �     2�     G�     X�  	   ]�     g�  N   ~�     ͕     �     ��     �     $�     ,�     2�  
   8�  	   C�     M�     [�  
   j�     u�     ��     ��     ��  	   ��     ʖ  
   Ӗ     ޖ  $   �     �     &�  "   6�     Y�     p�     y�     ��     ��     ��     ��     ї     �     �     �     �     !�     -�  
   <�     G�     Z�     i�  L   y�  �  Ƙ  �  ��     �     �     8�     M�     ^�     v�     ��     ��  
   ��     ��     ��     ��     ϟ     �  "   ��     �  .   ,�  
   [�  	   f�     p�     v�     ��     ��     ��  �   ��     ��  Q   ��  U   ��  2   K�     ~�     ��     ��  	   ��     ��     ɢ     Ϣ     Ԣ     �     ��     �     �     &�     4�     C�  
   R�     ]�     q�     ��     ��     ��     ��  /   ѣ     �     �     9�     J�     Q�     ^�     e�     n�     ��  .  ��  )   ��     �     ��  1   �     8�     L�  *   `�     ��     ��     ��     æ  %   ܦ     �     �     8�     U�     s�     ��     ��  �   ��     '�     :�  	   ?�     I�     U�     ^�     v�  �   ��  
  ��     ��  �   �  ^   �  ?   A�     ��  �   ��  g   :�     ��     ��     ��     ɭ     ح     �  	   �     ��  R   ��     P�     `�  	   r�  	   |�  	   ��     ��     ��     ��     ��     Ю     ߮     ��     �  #   �     C�     I�     V�  E   r�     ��      ϯ     �     ��     ��     �  %    �  
   F�     Q�     W�     ]�     d�     q�     ~�  B   ��  _   Ӱ     3�  S   :�     ��     ��     ��     Ʊ     ͱ     �     ��     
�     �     4�     @�     ]�     p�  "   ~�     ��     ��     ղ     �  p   �  K   `�     ��  
   ��  t   ��  ,   4�  �   a�     �      �     �     �     (�     4�     B�     T�  �   `�     �     �  5   �  G   Q�  $   ��     ��     ˶     ж     ߶     �     ��     �     �  �  (�  �   ��     ��     ��     ��     ˹     �     �     �     �  $   -�     R�  #   m�     ��     ��     ��     ʺ     ۺ     �     �  F   �     e�     |�     ��     ��     ��     ��     ��  .   ��     �  	   ��     �  9   �     J�     `�     w�  +   ��     ��     ��     ¼     ż     ɼ     м     Ӽ     �  M   ��     F�     U�     i�     y�     ��     ��     ��     ��     ��     ν      �     �  
   �     "�     5�     9�     <�  �   X�  Y   �     _�     k�     w�     ��     ��     ��     ��     ��     ֿ     �     �     �     $�  	   )�  
   3�     >�  	   G�     Q�     W�     d�     v�     }�     ��     ��     ��     ��     ��     ��  !   ��      �     3�      H�     i�  *   ��     ��     ��     ��     ��     ��     �     �     �     (�     >�     V�     m�     ��     ��     ��     ��     ��  	   ��  
   ��  C   ��     ;�  3   A�  *   u�     ��     ��     ��     ��     ��     �     �     �     &�     8�     ?�     [�  Z   i�     ��  
   ��     ��     ��  �   �  �   ��     /�  $   >�     c�  N   i�  �   ��  �   G�  
   D�  
   O�     Z�     y�     ��  
   ��     ��     ��     ��     ��     ��  
   ��     ��     �     �  ,   )�     V�  
   \�  	   g�     q�     �     ��     ��     ��     ��     ��  ^   ��     P�     W�  j   d�     ��     ��     ��  	   ��  	   �  z   �     ��     ��     ��     ��    ��  .  ��  Q   ��  �   F�  �   ��  7   ��     �  3   �     ;�     C�  H   ^�     ��     ��     ��     ��     ��  Z   ��  N   2�  T   ��  L   ��     #�     1�     M�     Y�  (   n�  '   ��     ��     ��  
   ��     ��     ��     ��     ��  G   �  �   ]�  �   7�  �    �  L   �  b   Z�  &   ��  ,   ��  %   �  �   7�  -   ��  9   !�  )   [�  M   ��  0   ��  7   �  ,   <�  :   i�  7   ��     ��  =   ��  F   9�  K   ��  J  ��  �   �  j   ��  S   �  R   l�  :   ��  �  ��  �  ��    w�     ��  	   ��     ��     ��     ��     ��     �     �     �     +�  
   7�     B�  D   K�  /   ��     ��     ��     ��  
   ��     
�     �  	   (�     2�     :�  	   B�     L�  
   X�     c�     k�     ��     ��     ��     ��     ��     ��      �     �     !�     /�     B�     U�     j�     ��     ��     ��  A   ��  D   ��     B�     T�     g�     |�     ��     ��     ��     ��     ��  	   ��     ��  	   ��     �     �     "�  
   .�     9�     F�     T�  
   n�  6   y�     ��     ��     ��     ��     ��     ��     ��     �     �      �  3   0�     d�  
   p�     {�     ��  l   ��  -   ��  <   )�  h   f�  �   ��  n   S�  6   ��  J   ��  H   D�  +   ��  ,   ��  0   ��  `   �     x�     }�     ��     ��     ��     ��     ��     ��     ��     ��  r    �     s�     x�     ��     ��  -   ��  ?   ��  @   �  Q   X�     ��     ��     ��     ��  
   ��     ��     �  �  
�  �   ��     ��     ��  �   ��  7   ��     ��     ��  
   ��  =   ��  ?   �     V�     _�     m�     �     ��     ��     ��     ��     ��     ��     �     +�     0�     D�     I�     P�     b�     f�     i�  
   ��  F   ��  0   ��  6   �  '   <�  )   d�     ��     ��  	   ��  	   ��  	   ��     ��     ��     ��     ��     ��     ��     �     (�     B�     Y�     r�     w�     ��  ;   ��  	   ��     ��     ��     ��     �  N   ,�  +  {�  ;   ��  <   ��      �  �   :�  .   ��  I   �  "   Q�     t�  2   ��  p   ��  	   ,     6     ?     G     M  �   P  c   �  6   Y h   � k  � �  e        -    2    7    <    A    E    L    P    W 	   Y    c    r    v    }    �    �    �    � 	   �    �    � 	   �    �    �    �    � 	   �    �    �    � 	            	       '    0    8    ?    D 	   L    V    _    f 	   j    t    }    � 	   �    �    �    �    �    � 	   �    �    �    � 	   �    �    �    � 
   �                ,    1 
   7    B 
   J    U    [ 
   ^    i    w    }    �    �    �    �    �    �    �    �    �    �    �    �    �    �    �    �    �     	    	    	    $	    ,	    1	    4	    6	    G	    O	    T	 	   W	    a	    n	    }	    �	    �	    �	    �	    �	    �	    �	    �	 	   �	    �	    �	    �	    �	    �	    �	    �	    
    
    	
    
    
    1
 2   I
 7   |
 !   �
 C   �
 *    <   E *   � �   � 9  � :   � �      � '   �    �    � %    g   D $   �    � H   �    . %   A %   g #   � 
   � 1   �    �    � F    \   Y '   � @   � 8    %   X 8   ~    � +   �     +    �   ? +   � *   � !       =    T    Z r   ` k   � ]   ? J   � Z   � _  C =   � 2   �     '   + �   S    @    Z Q   p R   � 7    M   M    �    � ;   � .   � ,       I F   V    �    �    �    �    �        ' 
   4    ?    N    ^ )   n    �    � L   �        *    . 1   E #   w     � f   �    # %   ;    a    � )   �    � 3   � E     �   _  @   @! 0   �! �   �! f   ?"    �" *   �" �   �" c   �# 7   �# X   $ M   v$ @   �$ +   % %   1% 3   W% F   �% c   �% A   6& =   x& v   �& s   -' Z   �' c   �' �   `( D   ) B   X)    �)    �) 8   �) 
   *     * Q   -* ;   * +   �* $   �* :   + W   G+ 8   �+ =   �+ )   ,     @, /   a, <   �, 
   �,    �,    �, .   �,    )- 8   2-    k-    �-    �-    �-    �- )   �- '   �- 9   '. )   a.    �.    �.    �. ;   �.    / %   -/    S/    `/    �/ 5   �/ )   �/ ;   �/ 9   50    o0    �0    �0 !   �0 	   �0 &   �0    �0 7   1 !   M1 a   o1 ]   �1 D   /2 9   t2 3   �2 Q   �2 Q   43 >   �3 F   �3 H   4 <   U4 D   �4 M   �4 S   %5 ]   y5 9   �5 A   6 6   S6 3   �6 b   �6 -   !7 .   O7 \   ~7    �7 m   [8 ,   �8 .   �8 ?   %9 L   e9 ;   �9 ]   �9 D   L: L   �: {   �: v   Z; Z   �; )   ,< 6   V< 3   �< S   �< `   = y   v= �   �= 3   �> "   ? [   )? o   �? I   �? 3   ?@ E   s@   �@     �A    �A "   �A    B    5B /   KB    {B    �B    �B �  �B    �D    �D    �D    �D    �D      E Z  !E    |H -   �H -   �H #   �H    I !   1I .   SI %   �I %   �I \   �I #   +J     OJ .   pJ '   �J �   �J I  aK   �L �   �N �  �O    EQ    ^Q 3   uQ    �Q )   �Q *   �Q $   R     @R /   aR .   �R .   �R *   �R    S    4S #   CS 2   gS 7   �S *   �S �   �S >   �T 3   �T E   �T 7   <U 8   tU 8   �U 
   �U    �U    V    V 2   (V 0   [V 1   �V P   �V (   W [   8W o   �W    X 
   X ]   "X g   �X    �X    �X '   Y A   >Y    �Y    �Y !   �Y '   �Y :   �Y    1Z +   MZ <   yZ 0   �Z 0   �Z m   [    �[ 2   �[ (   �[ *    \ )   +\    U\    b\    e\    j\    o\    r\    w\    z\ %   �\    �\ *   �\ +   �\    ]    5]    S]    n]    �] �   �]    2^ .   D^    s^    �^    �^    �^ "   �^ �   �^ %   �_    �_ '   �_ $   �_    ` 	   '`    1`    @`    Z` !   p` !   �` +   �` (   �` %   	a 9   /a .   ia !   �a    �a    �a    �a ?   b    Nb    ib ?   �b *   �b    �b '   �b )   c )   Hc )   rc )   �c )   �c    �c >   d <   Gd 
   �d    �d %   �d !   �d 0   �d !   (e $   Je x   oe {  �e x  dl ,   �q L   
r ,   Wr "   �r (   �r    �r    �r    s #   s    5s .   Fs "   us /   �s     �s 9   �s (   #t t   Lt    �t    �t    �t    u    &u    2u 0   Nu ~  u _   �v �   ^w �   x @   �x ?   y    Zy 1   ]y    �y A   �y    �y    �y *   z    /z A   Fz "   �z #   �z '   �z %   �z )   { #   G{ /   k{ 2   �{ (   �{ ;   �{ (   3| (   \| e   �| B   �| R   .} (   �}    �} !   �}    �}    �} .   ~    C~ �  X~ G   � 3   T�    �� Z   �� &   �� &   &� Q   M�    �� \   �� 1   	� 4   ;� W   p� G   ȃ O   � Q   `� =   �� K   �� 0   <�    m� �   ��    ?�    ]� $   b�    ��    �� :   �� +   �� �  �   ш �   ݊ &  Ƌ �   � G   ��    ύ �   � �   ��    _�    r� @   w�     ��    ُ    ��    	�    � w   � ,   �� "   Đ    � #   �    )� )   C�    m� -   |� "   �� 2   ͑ ;    � 5   <�    r� =   ��    Ȓ    � >   �� M   7� &   �� F   ��    � 	   �    � 2   ,� H   _�    �� v   �� 
   2�    =� #   J� #   n�    �� h   �� �   �    � ^    � /   _� 	   �� I   ��    �    � 6   � ,   B� "   o� (   ��    �� .   Ә *   �    -� <   E� 2   ��    �� -   ՙ    � �   � n   �    X�    g� �   y� K   A� �   ��    ��    ��    ��    ϝ #   �    � !   *�    L� �   `�    ]�    w� y   |� �   �� [   y�    ՠ    �    �� ,   �    I�    c�    �    �� u  �� �  )�    �� >   �� (   � #   �    =� *   S� 4   ~� '   �� 5   ۦ 2   � ;   D� _   ��    � @   ��    @�    `� #   � 1   �� �   ը 1   [�    ��    �� 	   ��    ��    Ʃ 8   ש B   � )   S�    }�    �� �   �� 0   <� /   m� 0   �� Q   Ϋ     �    )�    .�    5�    >�    G� #   N� 6   r� �   ��    C� !   [� '   }�    ��    ��    ǭ 3   �    � %   0� N   V� O   ��    ��    � *   "�    M�    Q� 6   V� �   �� a   w�    ٰ    �    ��    � !   %� '   G�     o� #   �� +   �� )   �    
� -   !�    O� )   c�    ��    ��    ��    Ӳ    � '   �    8� '   E� '   m� '   �� '   �� '   �    � >   "� P   a� E   �� (   �� K   !� E   m� n   ��    "�    <�    [�    q�    ��    �� *   �� "   ζ 2   � (   $� 2   M� (   �� .   �� $   ط    ��    � @   �    Y�    s� �   �� 
   � l   � L   �� C   ֹ 0   � &   K�    r�    �    ��    �� ,   �� ;   �    '� /   <�    l� �   ��     #�    D� -   Y� *   �� �   �� �   Y�    /� =   I�    ��    �� �   � \  �    D�    ]� 8   p�    �� 5   ��    �� %   �     8�     Y�    z� 
   �� &   ��    ��     �� *   � y   .� 
   �� %   ��    ��    ��    �     � &   8� 8   _� 5   �� 0   �� s   ��    s�    �� r   �� !   �    6� ,   N� 1   {� 1   �� �   �� (   {�    ��    �� 
   �� -  �� Z  � �   `�   �   0� \   H�    �� ;   ��    �� 6   �� �   4� 
   ��    �� (   �� #   �    (� �   ?� �   �� �   �� �   E�    �� #   ��    � -   3� P   a� V   ��     	�    *� 
   6�    A� (   E� #   n� 1   �� �   �� �  W� [  �� 9  G� �   �� �   � 4   �� V   �� I   @�   �� Y   �� a   � D   d� �   �� Z   2� s   �� ]   � �   _� o   �� ;   `� l   �� g   	� {   q� c  ��   Q� �   W� ^   �� ]   U� [   ��   � �  � �  �� .   ��    �� $   � $   2� $   W�    |�    ��    ��    ��    �� ,   �� -   � L   L� Z   �� F   �� D   ;� #   ��    ��    ��    ��    ��    �    	�    �    &�    :�    L� 5   a� M   �� %   �� #   � $   /� ,   T� 5   �� #   �� (   �� '   � ,   ,� ,   Y� .   �� @   �� '   �� %   � 0   D� }   u� �   ��    |  (   �  *   �     �  B   �  1   B 1   t 1   � (   �            " %   7 !   ]        �    �    � (   �    � b   �    `    m    �    �    �    �    � 5       9 "   V �   y            +    F �   K �   � S   p �   �   �   � m   �	 q   
 �   �
 b   ( E   � ]   � �   /    � 2   � <   * "   g "   � "   � "   � "   � %       < �   T    � 7   �    1    H m   ^ X   � ^   % s   �    �     ?   ! 4   a    �    �    � �  � n  R    �    � L  � ?   F    � $   �    � W   � Y       o    �    �    � 3   � )   � 3    )   M 1   w '   �    �    � 0   �    )    0    E    ]    f 6   m    � a   � C    J   a E   � G   �    :    Y    m     �    � 
   �    � ,   �     3    )   A 3   k )   � 1   � '   �    #    (    > J   T    � !   � -   � 7    0   : �   k        �  �      3          /  �       A   Q  L        x            I  Q         �  �      F       �          b  n   9  �  �  �  �         :  K  J           �    �      ]   0  {             �           O               _      �      
  �  D   -  �  `        �  �  X  �  8  .  �   �  �   a  �   �      4      ]  T           �  �   V        d      f       �       �  2   �  �    ^  ?   2  {      	  �      �   I  �      u   �  8  %  _  �  H          �        }  v  �   �   z  �   Y           �  �          [  �  Y  5  �      �      r          �  L      �   �  �   �  �  d  �      �    Q  c   m  �            _                    \  �    �  �  9      �      }            k       +      �  �   o  W  �  k  X   �            u  �  �  �  �  �  �   6   7  �           �   �  �  d     �       �      �       L      �                  �  @  7  �  !      >  f  �      t  N              �  �       n  �   �  �        �      �  �        �  �      �  9   ,   (  )  *     �  �  �   �  �   �  �  �      �   2  D  c  !  �  �  �  �   w  �      A      �     d  Z  �  U  �  %  3   x          k        9  
  �        /   �      c  �      �  �   �   �   �     �  �   7      O      �   [  T  j  +   �  �  b  �  J  "  �    1     #  �      �  	   �      �      l  �  /      �      �  �  �   I  	      7  w    4  X  =  �  �              U  �   �   �  
     o    �  �      �   �  �  �           �  M      5      v  ~  �  �          *      '    j      i    E      o      ,  I          �   �      Z  �   �  �  (  �  �  c  �  �   �  �  G   �   d  5  O  &  �           �   �   �      �      �      e  R  �       a  �  �  P  �       �  �  a  �      c      �  �   �             �      N    �     S   �        K               R         �      �   |  *                     �       �  @        �   S     �  �       8   N  g  1      6  <      -  &  �  v   �       �   n      �      �  X     �       �  '  �      �  v  �           .  /              �  �        �    �  >  $      �       B          O      t    �  �      �   �  �   j      �      ?  E      i  �  �  N  }   q  �      �   y  ;       1  �             z      )        3  0      �   �       �                p    �  �      e  �   �  �  q  (  U  �   �  �  k  �      �    R  J  �       }      !     *   <      �   �  �             `  �   �  -      o   M   �  �  �  ?  Q   D  �  �  �  V      �        �  Z  �          �  �   `  6  �  �  �  {  �   U      -   �  g  �            �  �       f      �  �  j    �              �   Y      �      �  ~       j     R   �  �  g   �  T  �       �  W      W  �  �  X  S  K  #   p  �  �  �  %   �        m  �      �  �  x   �          4       B               P           (  �  >       �      �      �  �  �      �          l      %          �  �      k    b  �  �                  A  �  q    J  l   �       i  �  �  �  �          @  �   _   ]  �      .    W  �      �  8         �  �      %  '      G          Z   	  e  �    t   �        �  �   5   �  ^  5      �          �  �  �  �      �      l      �  �  �  �   9      �  1  �      �  C               �   e           ^       P  �   M  �  �   h  r       �   �      s   �  \  +      �  #  �  =  F  �  y   �  [      Z  z       �  b   C  .   �   L            ~   �  *  4  �     <    �  �      &  A  �      �   �             �               �      �      H  8  �                 �       <  2      �  +    W   	  �  �  �      �      �   �   >  �  O  ,  �  �  �      �   �   :         �  h   [     �   �  s     H            �  �  V  �    �       /  �      �  p              �  �  !  "   E   �         �       �  G  �  H  s    s      K  �  �   :  a   $          �      ,         �  r  �          t  �  
   F  '      h  \  �      �   Y  �  �  :      6      �  �      C        �          �   �   �      �  �   �  �  m   �  U   x                �         �     4          v    |      =     �  �          D    �              �    f  �                  B  �              �  �         �  �  �       0      �   �  �  #      z  �  �    i  s      &          3  �   m  �   +          �  �  �  p   �      @   6  �  �  �      �          �          b  �        �  @    [  �  o  S      �  �   :  ;  g      �   �   Q  D  ;    �      G  �          �   �  F  w  E          _  �      �        h      �      p  |   e      �  u        r  �  a      3  �                 �  �  q      y        1  �       �      ;  ~  B  �        �      �  �      �  ?      �  I   7   n  ]  �  �  �   �      �  g  �       �  $      Y      �  �            H  $   �      \     y    �  �  �    �  �     �  �        �      �  $            �   �  �  �  P  V   C  �  �  �  �  �      2      �  �  �       �  w     �   &   )  h  �  m  '   �   �  �          0      �  w  J  �  �          t  �          ~  �  �  x  �  �  n  M  ,  �  C              ?  �      P  �  �  �  �  V  �  A  f    �  |  �  �  �  N           }             `  �   |  �      F  M  �   �     {    "  �   �  �         E  (   �  �       �    �  T  T  �   �   �   �          �      .  R  )        �     �  �  #  -  �  ^          0         
  �  �  \      �       �      ^  i   ]  )             �  u             �  �   =   S  �  �  �      �   �  z          "  �         �  �  �    �      ;  l  �      !        q    {        B      G  �  �  "  r      �  L      �  y  u  K   =  >  <     �       `    	Free Space (Bytes): %llu (%lu MB)
 	Free Space (Images): %d
 	VolumeLabel: %s
 
Camera identification:
  Model: %s
  Owner: %s

Power status: %s

Flash disk information:
%s

Time: %s
 
Device Capabilities:
   Drive %s
  %11s bytes total
  %11s bytes available   Serial Number: %s
   Version: %s
  (battery is %d%% full)  Total memory is %8d bytes.
 Free memory is  %8d bytes.
 Filecount: %d %.0f mm %d %d/%d %d mm %d/%d %f %i bytes of an unknown image format have been received. Please write to %s and ask for assistance. %i pictures could not be deleted because they are protected %s (host time %s%i seconds) %s is a file type for which no thumbnail is provided (Traveler) SX330z Library (And other Aldi-cams).
Even other Vendors like Jenoptik, Skanhex, Maginon should work.
Please send bugreports and comments.
Dominik Kuhlen <kinimod@users.sourceforge.net>
 * Image glitches or problems communicating are
  often caused by a low battery.
* Images captured remotely on this camera are stored
  in temporary RAM and not in the flash memory card.
* Exposure control when capturing images can be
  configured manually or set to automatic mode.
* Image quality is currently lower than it could be.
 *UNKNOWN* -0.5 -1.0 -1.5 -2.0 0.0 0.1 m 0.5 0.5 m 1 1 min 1 minute 1.0 1.3s 1.5 1.6s 1.6x 1/1.3s 1/1.6s 1/1000s 1/100s 1/10s 1/1250s 1/125s 1/13s 1/15 1/15s 1/1600s 1/160s 1/2 1/2.5s 1/2000s 1/200s 1/20s 1/2500s 1/250s 1/25s 1/2s 1/30 1/30s 1/3200s 1/320s 1/3s 1/4 1/4000s 1/400s 1/40s 1/4s 1/5000s 1/500s 1/50s 1/5s 1/60 1/60s 1/6400s 1/640s 1/6s 1/8 1/8000s 1/800s 1/80s 1/8s 10 mins 10 minutes 10 mm 10 seconds 100% 10s 1152 x 864 12 mm 1280 x 960 13s 15 15 mins 15 minutes 15s 16 seconds 1s 1x 2 2 seconds 2.0 2.0 m 2.5s 2.5x 20 seconds 20s 21 points 25% 25s 29 mm 2s 2x 30 30 minutes 30 seconds 30s 34 mm 3s 3x 4 4 seconds 41 mm 4s 4x 5 mins 5 minutes 5 seconds 50% 51 mm 51 points 51 points (3D) 58 mm 5s 5x 6 mm 6 seconds 640 x 480 6s 6x 75% 7x 8 8 mm 8 seconds 8s 8x 9 points 9x AC AC adapter AC adapter is connected.
 AC adapter is not connected.
 AE-lock AF-A AF-C AF-S AV AX203 USB picture frame driver
Hans de Goede <hdegoede@redhat.com>
This driver allows you to download, upload and delete pictures
from the picture frame. AX203 based picture frames come with a variety of resolutions.
The gphoto driver for these devices allows you to download,
upload and delete pictures from the picture frame. A_DEP About Konica Q-M150:
This camera does not allow any changes
from the outside. So in the configuration, you can
only see what it is configured on the camera
but you can not change anything.

If you have some issues with this driver, please e-mail its authors.
 Access mode Adc65
Benjamin Moos <benjamin@psnw.com> AdobeRGB Album name Alkalium manganese An unknown error occurred. Please contact %s. Anti-redeye Anywhere Aox generic driver
Theodore Kilgore <kilgota@auburn.edu>
 Aperture Aperture Setting Aperture Settings Aperture changed Artist Assist Light Auto Auto Aperture Auto Off (field) (in seconds) Auto Off (host) (in seconds) Auto Off Time Auto Power Off (minutes) Auto exposure lock Auto focus: one-shot Auto, red-eye reduction Autofocus Mode Autofocus error. Automatic
 Automatic Flash Automatic exposure adjustment on preview Automatic flash Automatic flash on capture Available memory:  Average B/W BW Bad checksum - got 0x%02x, expected 0x%02x. Bad data - got 0x%02x, expected 0x%02x or 0x%02x. Bad data - got 0x%02x, expected 0x%02x. Bad value for card status %d Bad value for lens status %d
 Barbie/HotWheels/WWF
Scott Fritzinger <scottf@unr.edu>
Andreas Meyer <ahm@spies.com>
Pete Zaitcev <zaitcev@metabyte.com>

Reverse engineering of image data by:
Jeff Laing <jeffl@SPATIALinfo.com>

Implemented using documents found on
the web. Permission given by Vision. Batteries are low. Batteries are ok. Battery Battery Level Battery Level:		%s
Number of Images:	%d
Minimum Capacity Left:	%d
Busy:			%s
Flash Charging:		%s
Lens Status:		 Battery Mode Battery Type Battery exhausted, camera off. Battery level: %.1f Volts. Revision: %08x. Battery low Battery status: %s, AC Adapter: %s
 Beach Beep Beep mode changed Beep off Beep on Best Bit rate %ld is not supported. Black & White Black & white Black Board Black and White Black board Black/White Blink Blue Brightness Brightness+ Brightness- Brightness/Contrast Builtin RAM Builtin ROM Bulb Exposure Time Busy CRW Camera Camera Configuration Camera Date and Time Camera ID: %s
 Camera Library for the Kodak DC215 Zoom camera.
Michael Koltan <koltan@gmx.de>
 Camera Mode Camera Model Camera Model: %s
 Camera OK. Camera Owner Camera Settings Camera Status Information Camera and Driver Configuration Camera appears to not be using CompactFlash storage
Unfortunately we do not support that at the moment :-(
 Camera could not complete operation Camera has memory.
 Camera has taken %d pictures, and is using CompactFlash storage.
 Camera is in wrong mode. Please contact %s. Camera not ready Camera not ready, get_battery failed: %s Camera not ready, multiple 'Identify camera' requests failed: %s Camera power save (seconds) Camera rejected the command. Camera reset itself. Camera sent unexpected byte 0x%02x. Camera supports videoformats:  Camera unavailable Camera unavailable: %s Camera was already active Camera was woken up Can't capture new images. Unknown error Can't delete all images. Can't delete image %s. Can't upload this image to the camera. An error has occurred. Cannot retrieve the available memory left Cannot retrieve the battery capacity Cannot retrieve the name of the folder containing the pictures Canon PowerShot series driver by
 Wolfgang G. Reissnegger,
 Werner Almesberger,
 Edouard Lafargue,
 Philippe Marzouk,
A5 additions by Ole W. Saastad
Additional enhancements by
 Holger Klemm
 Stephen H. Westin Canon disable viewfinder failed: %d Canon enable viewfinder failed: %d Capture Capture Settings Capture type is not supported Card Card Status:		 Card can not be written. Card is not formatted Card is not open Card is open Card is write protected. Card name: %s
Free space on card: %d kB
 Card not supported. Card removed during access. Center-Weighted Center-weighted Center-weighted average Changing speed... wait... Character Character & Sound Charging Checksum error Children Choose Color Temperature Closeup Cloudy Color Color Mode Color Settings Color Space Color Temperature Command can not be cancelled. Command order error. Communication error 1 Communication error 2 Communication error 3 Compact Flash Card detected
 CompactFlash Card Compatibility Mode Compression Compression Setting Configuration Configuration for your FUJI camera Connected to camera Continuous high speed Continuous low speed Contrast Contrast+ Contrast- Copy object Copyright Copyright (max. 20 characters) Copyright Info Copyright Information Corrupted data Could not allocate %i byte(s) for downloading the picture. Could not allocate %i byte(s) for downloading the thumbnail. Could not apply USB settings Could not change ISO speed Could not change aperture Could not change beep mode Could not change exposure compensation Could not change flash mode Could not change focus mode Could not change image format Could not change shooting mode Could not change shutter speed Could not change time of file '%s' in '%s' (%m). Could not change zoom level Could not contact camera. Could not create \DCIM directory. Could not create destination directory. Could not create directory %s. Could not create directory. Could not delete file '%s' in folder '%s' (error code %i: %m). Could not delete file. Could not detect any camera Could not establish initial contact with camera Could not extract JPEG thumbnail from data: Data is not JFIF Could not extract JPEG thumbnail from data: No beginning/end Could not find any driver for '%s' Could not find file '%s'. Could not find localization data at '%s' Could not get disk info: %s Could not get disk name: %s Could not get flash drive letter Could not get information about '%s' (%m). Could not get information about '%s' in '%s' (%m). Could not get register %i. Please contact %s. Could not initialize upload (camera responded with 0x%02x). Could not load required camera driver '%s' (%s). Could not open '%s'. Could not remove directory %s. Could not remove directory. Could not reset camera.
 Could not set speed to %i (camera responded with %i). Could not transmit packet (error code %i). Please contact %s. Could not upload, no free folder name available!
999CANON folder name exists and has an AUT_9999.JPG picture in it. Couldn't read from file "%s" Create Wifi profile Creation date: %s, Last usage date: %s Creative PC-CAM 300
 Authors: Till Adam
<till@adam-lilienthal.de>
and: Miah Gregory
 <mace@darksilence.net> Creative PC-CAM600
Author: Peter Kajberg <pbk@odense.kollegienet.dk>
 Current Mode: Camera Mode
 Current Mode: Playback Mode
 Current camera time:  %04d-%02d-%02d  %02d:%02d
Free card memory: %d
Images on card: %d
Free space (Images): %d
Battery level: %d %%. Custom DD/MM/YYYY Data has been corrupted. Date & Time Date Format Date Time Stamp Format Date and Time Date and time (GMT) Date display Date: %i/%02i/%02i %02i:%02i:%02i
Pictures taken: %i
Free pictures: %i
Software version: %s
Baudrate: %s
Memory: %i megabytes
Camera mode: %s
Image quality: %s
Flash setting: %s
Information: %s
Timer: %s
LCD: %s
Auto power off: %i minutes
Power source: %s Date: %s Day/Month/Year Daylight Debug Deep Default gateway Default sierra driver:

    This is the default sierra driver, it
    should be capable of supporting the download
    and browsing of pictures on your camera.

    Camera configuration (or preferences)
    settings are based on the Olympus 3040,
    and are likely incomplete. If you verify
    that the configuration settings are
    complete for your camera, or can contribute
    code to support complete configuration,
    please contact the developer mailing list.
 Delete object Deleting '%s' from folder '%s'... Deleting image %s. Detected a "%s" aka "%s" Detected a '%s'. Device Icon Digita
Johannes Erdfelt <johannes@erdfelt.com> Digital Zoom Digital zoom Directory Browse Mode - written by Scott Fritzinger <scottf@unr.edu>. Directory exists Directory not found Disconnecting camera. Do not display TV menu Don't know how to handle camera->port->type value %i aka 0x%x in %s line %i. Download program for Digital Dream Enigma 1.3. by <olivier@aixmarseille.com>, and adapted from spca50x driver.Thanks you, spca50x team, it was easy to port your driver on this cam!  Download program for GrandTek 98x based cameras. Originally written by Chris Byrne <adapt@ihug.co.nz>, and adapted for gphoto2 by Lutz Mueller <lutz@users.sf.net>.Protocol enhancements and postprocessing  for Jenoptik JD350e by Michael Trawny <trawny99@users.sourceforge.net>.Bugfixes by Marcus Meissner <marcus@jet.franken.de>. Download program for Polaroid DC700 camera. Originally written by Ryan Lantzer <rlantzer@umr.edu> for gphoto-4.x. Adapted for gphoto2 by Lutz Mueller <lutz@users.sf.net>. Download program for several Polaroid cameras. Originally written by Peter Desnoyers <pjd@fred.cambridge.ma.us>, and adapted for gphoto2 by Nathan Stenzel <nathanstenzel@users.sourceforge.net> and Lutz Mueller <lutz@users.sf.net>.
Polaroid 640SE testing was done by Michael Golden <naugrim@juno.com>. Downloading %s. Downloading %s... Downloading '%s' from folder '%s'... Downloading '%s'... Downloading EXIF data... Downloading audio... Downloading data... Downloading file... Downloading image %s. Downloading image... Downloading movie... Downloading thumbnail... Downloading... Driver Driver Settings Dual Iris EEPROM checksum error. ERROR: %d is too big ERROR: a fatal error condition was detected, can't continue  ERROR: message format error. ERROR: message overrun ERROR: out of sequence. ERROR: unexpected message ERROR: unexpected message2. ERROR: unexpected packet type. Empty Encryption English Error Error capturing image Error changing speed Error changing speed. Error deleting associated thumbnail file Error deleting file Error waiting ACK during initialization Error waiting for ACK during initialization, retrying Evaluative Everywhere Execute predefined command
with parameter values. Expected 32 bytes, got %i. Please contact %s. Exposure Exposure Bias Compensation Exposure Compensation Exposure Index (film speed ISO) Exposure Meter Exposure Metering Exposure Metering Mode Exposure Program Exposure Program Mode Exposure Time Exposure compensation Exposure compensation changed Exposure compensation: %d
 Exposure compensation: %s
 Exposure level on preview External Flash External Flash Attached External Flash Mode External Flash Status External sync F-Number F2 F2.3 F2.8 F4 F5.6 F8 FLASH:
 Files: %d
 Factory Default File File '%s' could not be found in folder '%s'. File '%s/%s' does not exist. File compression File exists File not found File protected. File resolution File size is %ld bytes. The size of the largest file possible to upload is: %i bytes. File type File upload failed. Filetype: FlashPix ( Filetype: JPEG ( Fine Fireworks Firmware Revision: %8s Firmware Revision: %8s
Pictures:     %i
Memory Total: %ikB
Memory Free:  %ikB
 Firmware Revision: %d.%d
 Firmware Version Firmware version: %d.%02d
 Firmware: %d.%d
 Fisheye Fixed Flash Flash Mode Flash Off Flash Setting Flash Settings Flash auto Flash mode changed Flash mode: auto,  Flash mode: force,  Flash mode: off
 Flash off Flash on Flash only FlashLight : Auto FlashLight : Auto (RedEye Reduction) FlashLight : Off FlashLight : On FlashLight : On (RedEye Reduction) FlashLight : undefined FlashPix Fluorescent Fluorescent Lamp 1 Fluorescent Lamp 2 Fluorescent Lamp 3 Fluorescent Lamp 4 Fluorescent Lamp 5 Focal Length Focal Length Maximum Focal Length Minimum Focus Focus Areas Focus Distance Focus Mode Focus mode changed Focusing Point Focusing error. Folder '%s' only contains %i files, but you requested a file with number %i. For cameras with S&Q Technologies chip.
Should work with gtkam. Photos will be saved in PPM format.

All known S&Q cameras have two resolution settings. What
those are, will depend on your particular camera.
A few of these cameras allow deletion of all photos. Most do not.
Uploading of data to the camera is not supported.
The photo compression mode found on many of the S&Q
cameras is supported, to some extent.
If present on the camera, video clips are seen as subfolders.
Gtkam will download these separately. When clips are present
on the camera, there is a little triangle before the name of
the camera. If no folders are listed, click on the little 
triangle to make them appear. Click on a folder to enter it
and see the frames in it, or to download them. The frames will
be downloaded as separate photos, with special names which
specify from which clip they came. Thus, you may freely 
choose to save clip frames in separate directories. or not.
 For cameras with insides from S&Q Technologies, which have the 
USB Vendor ID 0x2770 and Product ID 0x905C, 0x9050, 0x9051,
0x9052, or 0x913D.  Photos are saved in PPM format.

Some of these cameras allow software deletion of all photos.
Others do not. No supported camera can do capture-image. All
can do capture-preview (image captured and sent to computer).
If delete-all does work for your camera, then capture-preview will
have the side-effect that it also deletes what is on the camera.

File uploading is not supported for these cameras. Also, none of the
supported cameras allow deletion of individual photos by use of a
software command.
 Force Flash
 Format card and set album name. Format compact flash Frames Left: %i
 Frames Taken     : %4d
 Frames Taken: %i
 French Full Full Image German Getting configuration... Getting data... Getting file list... Getting file... Getting information on %i files... Getting thumbnail... Got unexpected result 0x%x. Please contact %s. Gray scale Grayscale Green Hardware Error High High (1152 x 872) High (good quality) Host Mode:		%s
Exposure Correction:	%s
Exposure Data:		%d
Date Valid:		%s
Date:			%d/%02d/%02d %02d:%02d:%02d
Self Timer Set:		%s
Quality Setting:	%s
Play/Record Mode:	%s
Card ID Valid:		%s
Card ID:		%d
Flash Mode:		 Host power save (seconds) How long will it take until the camera powers off when connected to the computer? How long will it take until the camera powers off when not connected to the computer? How long will it take until the camera powers off? I/O in progress ID IP address (empty for DHCP) ISO Speed ISO speed changed Icons Idle Illegal parameter. Image Image %s is delete protected. Image & Sound Image Format Image Quality Image Rotation Image Settings Image Size Image Stabilization Image format changed Image height: %d
 Image number not valid. Image protected. Image size: %d
 Image type %d is not supported by this camera ! Image type %d not supported Image type is not supported Image width: %d
 In use Incandescent Indoor Infinity Infinity/Fish-eye Information Information regarding cameras with ID 0x2770:0x9153.

We do not recommend the use of a GUI program to access
this camera, unless you are just having fun or trying to
see if you can blow a fuse.
For production use, try
gphoto2 -P
from the command line.
Note: it is not possible to download video clips.
 Initial camera response '%c' unrecognized Initializing Camera Internal RAM Internal error #1 in get_file_func() (%s line %i) Internal error (1). Internal error (2). Internal memory: %d MB total, %d MB free.
 Invalid Invalid ISO speed setting Invalid Value ( %d )
 Invalid aperture setting Invalid exposure compensation setting Invalid flash mode setting Invalid focus mode setting Invalid image format setting Invalid shooting mode setting Invalid shutter speed setting Iris error. Italian JD11
Marcus Meissner <marcus@jet.franken.de>
Driver for the Jenoptik JD11 camera.
Protocol reverse engineered using WINE and IDA. JD11 Configuration JPEG JPEG Fine JPEG Normal Japanese Keep filename on upload Kids and pets Known problems:

1. If the Kodak DC3200 does not receive a command at least every 10 seconds, it will time out, and will have to be re-initialized. If you notice the camera does not respond, simply re-select the camera. This will cause it to reinitialize. Known problems:

If communications problems occur, reset the camera and restart the application.  The driver is not robust enough yet to recover from these situations, especially if a problem occurs and the camera is not properly shutdown at speeds faster than 9600. Kodak DC120 Camera Library
Scott Fritzinger <scottf@gphoto.net>
Camera Library for the Kodak DC120 camera.
(by popular demand). Kodak DC240 Camera Library
Scott Fritzinger <scottf@gphoto.net> and Hubert Figuiere <hfiguiere@teaser.fr>
Camera Library for the Kodak DC240, DC280, DC3400 and DC5000 cameras.
Rewritten and updated for gPhoto2. Kodak DC3200 Driver
Donn Morrison <dmorriso@gulf.uvic.ca>

Questions and comments appreciated. Kodak EZ200 driver
Bucas Jean-Francois <jfbucas@tuxfamily.org>
 Konica Configuration Konica Q-M150 Library
Marcus Meissner <marcus@jet.franken.de>
Aurelien Croc (AP2C) <programming@ap2c.com>
http://www.ap2c.com
Support for the french Konica Q-M150. Konica library
Lutz Mueller <lutz@users.sourceforge.net>
Support for all Konica and several HP cameras. Korean LCD LCD Auto Shut Off (seconds) LCD Brightness LCD Mode LED Mode Landscape Language Largan driver
Hubert Figuiere <hfiguiere@teaser.fr>

Handles Largan Lmini camera.
 Large Fine JPEG Large Normal JPEG Lens Mode Lens Name Lens Type Lens is not connected
 Light Light too dark. List Wifi profiles List all files Listing files in '%s'... Listing folders in '%s'... Lithium Ion Loading camera drivers from '%s'... Local Localization Localization file too long! Logitech Clicksmart 310 driver
Theodore Kilgore <kilgota@auburn.edu>
 Looking for camera ... Looks like a modem, not a camera Lossless Low Low (576 x 436) Low (best quality) Lower case letters in %s not allowed. MM/DD/YYYY M_DEP Macro Manual Manual Focus Manual focus Manufacturer: %s
 Mars MR97310 camera library
Theodore Kilgore <kilgota@auburn.edu>
 Mars MR97310 camera.
There is %i photo in it.
 Mars MR97310 camera.
There are %i photos in it.
 Matrix Matthew G. Martin
Based on fujiplay by Thierry Bousch <bousch@topo.math.u-psud.fr>
 Max. Focal Length Maximal Maximum number of Images: %d
 Medium Medium (1152 x 872) Medium (better quality) Medium Fine JPEG Medium Normal JPEG Memory Left: %i bytes
 Memory card Memory card status (%d): %s
 Menus and Playback Metering Mode Microsoft Excel Spreadsheet (.xls) Microsoft Powerpoint (.ppt) Microsoft Word Document Min. Focal Length Minimal Minolta Dimage V Camera Library
%s
Gus Hartmann <gphoto@gus-the-cat.org>
Special thanks to Minolta for the spec. Model:			Minolta Dimage V (%s)
Hardware Revision:	%s
Firmware Revision:	%s
 Model:  Model: %s
 Model: %s
Capacity: %i Mb
Power: %s
Auto Off Time: %i min
Mode: %s
Images: %i/%i
Date display: %s
Date and Time: %s
 Model: %s
Memory: %d byte(s) of %d available Model: %s
Serial Number: %s,
Hardware Version: %i.%i
Software Version: %i.%i
Testing Software Version: %i.%i
Name: %s,
Manufacturer: %s
 Model: %x, %x, %x, %x Model: Kodak %s
 Monitor Month/Day/Year Move object Movie Quality Movie Screen Size Movie Sound Mustek MDC-800 gPhoto2 Library
Henning Zabel <henning@uni-paderborn.de>
Ported to gphoto2 by Marcus Meissner <marcus@jet.franken.de>
Supports Serial and USB Protocol. NOT RECOGNIZED NTSC Name "%s" from camera does not match any known camera Name '%s' (%li characters) too long, maximum 30 characters are allowed. Name to set on card when formatting. Network mask Next Nickel cadmium Nickel hydride Night Landscape Night Portrait Night scene Night snapshot Nikon Coolpix 880:
    Camera configuration (or preferences):

        The optical zoom does not properly
        function.

        Not all configuration settings
        can be properly read or written, for
        example, the fine tuned setting of
        white balance, and the language settings.

        Put the camera in 'M' mode in order to
        to set the shutter speed.
 Nikon Coolpix 995:
    Camera configuration (preferences) for this
    camera are incomplete, contact the gphoto
    developer mailing list
    if you would like to contribute to this
    driver.

    The download should function correctly.
 No No Compact Flash Card detected
 No Dual Iris No EXIF data available for %s. No Flash No Power Light No Resolution Switch No SD Card inserted.
 No additional information available. No answer from the camera. No audio file could be found for %s No camera manual available.
 No card No card in camera.
 No card present. No memory card present No reason available No response from camera No space available to capture new images. You must delete some images. No space left on card. None None selected Normal Normal
 Not Full Not enough free space Not enough memory available on the memory card Not in fill mode Not ready Not used Note: no memory card present, some values may be invalid
 Number of Images: %d
 Number of pictures: %d Number of pictures: %d
 Number of pictures: %i
Firmware Version: %s OFF OK ON Off Office On On screen tips On, red-eye reduction Only root folder is supported - you requested a file listing for folder '%s'. Operation Mode Operation cancelled Optimal quality Orientation Other Other Settings Other command executing. Out of Focus Out of memory Out of memory: %d bytes needed. Out of memory: %ld bytes needed. Outdoor Owner Name Owner name changed PAL PC PTP Parameter Not Supported Panasonic DC1000 gPhoto library
Mariusz Zynel <mariusz@mizar.org>

Based on dc1000 program written by
Fredrik Roubert <roubert@df.lth.se> and
Galen Brooks <galen@nine.com>. Panasonic PV-L859-K/PV-L779-K Palmcorder Driver
Andrew Selkirk <aselkirk@mailandnews.com> Parameter 1 Parameter 2 Parameter 3 Partial Path not absolute Persistent Settings Photo in movie Photos on camera: %d
 Picture Frame Configuration Picture Settings Pictures Pictures in camera: %d
 Play Play mode Port speed Portrait Portugese Power Power Saving Power down device Preset Preset Custom 1 Preset Custom 2 Preset Custom 3 Preset Custom 4 Preset Custom 5 Previous Problem downloading image Problem getting image information Problem getting number of images Problem opening port Problem reading image from flash Problem resetting camera Problem setting camera communication speed Problem taking live image Product ID: %02x%02x
 Profile name Prohibit Flash
 Quality RAW RAW + JPEG Fine RAW + JPEG Normal RAW + Large Fine JPEG RAW + Large Normal JPEG RAW + Medium Fine JPEG RAW + Medium Normal JPEG RAW + Small Fine JPEG RAW + Small Normal JPEG RAW 2 RGB Gain Read Only with Object deletion Read-Only Read-Write Read-only file attributes like width and height can not be changed. Ready Received unexpected answer (%i). Please contact %s. Received unexpected data (0x%02x, 0x%02x). Received unexpected header (%i) Received unexpected response Receiving data... Record Record Mode Record mode Red Red Eye Suppression Red-eye Reduction Remote Removable RAM (memory card) Removable ROM Requested information about picture %i (= 0x%x), but got information about picture %i back Resetting protocol... Resolution Resolution Switch Resolution plus Size Ricoh / Philips driver by
Lutz Mueller <lutz@users.sourceforge.net>,
Martin Fischer <martin.fischer@inka.de>,
based on Bob Paauwe's driver
 Ricoh Caplio G3.
Marcus Meissner <marcus@jet.franken.de>
Reverse engineered using USB Snoopy, looking
at the firmware update image and wild guessing.
 Rotation Angle SD memory: %d MB total, %d MB free.
 SDRAM SDRAM:
 Files: %d
  Images: %4d
  Movies: %4d
Space used: %8d
Space free: %8d
 STV0674
Vincent Sanders <vince@kyllikki.org>
Driver for cameras using the STV0674 processor ASIC.
Protocol reverse engineered using SnoopyPro
 STV0680
Adam Harrison <adam@antispin.org>
Driver for cameras using the STV0680 processor ASIC.
Protocol reverse engineered using CommLite Beta 5
Carsten Weinholz <c.weinholz@netcologne.de>
Extended for Aiptek PenCam and other STM USB Dual-mode cameras. Saturation Self Timer Self Timer (next picture only) Self Timer Time Self test device Self-timer Selftimer Delay Sending data... Sensitivity Sensor ID: %d.%d
 Sepia Sequence 9 Serial Number Serial Number: %s
 Set clock in camera Shall the camera beep when taking a picture? Sharp Sharpening Sharpness Shooting Mode Shooting Speed Shooting mode Shooting mode changed Shutter Speed Shutter Speed (in seconds) Shutter speed changed SiPix Web2
Marcus Meissner <marcus@jet.franken.de>
Driver for accessing the SiPix Web2 camera. Single Single frame Sipix StyleCam Blink Driver
Vincent Sanders <vince@kyllikki.org>
Marcus Meissner <marcus@jet.franken.de>.
 Size Priority Skylight Slide Show Interval Slow Sync Slow-sync Smal Ultrapocket
Lee Benfield <lee@benf.org>
Driver for accessing the Smal Ultrapocket camera, and OEM versions (slimshot) Small Fine JPEG Small Normal JPEG Snow Soft Some notes about Epson cameras:
- Some parameters are not controllable remotely:
  * zoom
  * focus
  * custom white balance setup
- Configuration has been reverse-engineered with
  a PhotoPC 3000z, if your camera acts differently
  please send a mail to %s (in English)
 Some notes about Epson cameras:
- Some parameters are not controllable remotely:
  * zoom
  * focus
  * custom white balance setup
- Configuration has been reverse-engineered with
  a PhotoPC 3000z, if your camera acts differently
  please send a mail to the gphoto developer mailing list (in English)
 Sonix camera.
There is %i photo in it.
 Sonix camera.
There are %i photos in it.
 Sony DSC-F1 Digital Camera Support
M. Adam Kendall <joker@penguinpub.com>
Based on the chotplay CLI interface from
Ken-ichi Hayashi
Gphoto2 port by Bart van Leeuwen <bart@netage.nl> Sony DSC-F55/505 gPhoto library
Supports Sony MSAC-SR1 and Memory Stick used by DCR-PC100
Originally written by Mark Davies <mdavies@dial.pipex.com>
gPhoto2 port by Raymond Penners <raymond@dotsphinx.com> Sorry, your Canon camera does not support Canon capture Sound Soundvision Driver
Vince Weaver <vince@deater.net>
 Spanish Speed %i is not supported! Speeds greater than 57600 are not supported for uploading to this camera Sports Spot Spot Metering Mode Spot-AF Standard Step #2 of initialization failed! (returned %i bytes, expected %i). Camera not operational Step #2 of initialization failed: ("%s" on read of %i). Camera not operational Step #3 of initialization failed! (returned %i, expected %i). Camera not operational Step #3 of initialization failed: "%s" on read of %i. Camera not operational Strobe error. Summary for Mustek MDC800:
 Super macro Switching Camera Off Synchronize camera date and time with PC Synchronize frame data and time with PC System error. TFT + PC TIFF (RGB) TTL TV TV Output Format Testing different speeds... The Directory Browse "camera" lets you index photos on your hard drive. The JD11 camera works rather well with this driver.
An RS232 interface @ 115 kbit is required for image transfer.
The driver allows you to get

   - thumbnails (64x48 PGM format)
   - full images (640x480 PPM format)
 The Kodak DC120 camera uses the KDC file format for storing images. If you want to view the images you download from your camera, you will need to download the "kdc2tiff" program. It is available from http://kdc2tiff.sourceforge.net The Samsung digimax 800k driver has been written by James McKenzie <james@fishsoup.dhs.org> for gphoto. Lutz Mueller <lutz@users.sourceforge.net> ported it to gphoto2. Marcus Meissner <marcus@jet.franken.de> fixed and enhanced the port. The battery level of the camera is too low (%d%%). The operation is aborted. The camera could not be contacted. Please make sure it is connected to the computer and turned on. The camera did not accept the command. The camera does not accept '%s' as filename. The camera does not support speed %i. The camera has just sent an error that has not yet been discovered. Please report the following to %s with additional information how you got this error: (0x%x,0x%x). Thank you very much! The camera sent more bytes than expected (%i) The camera sent only %i byte(s), but we need at least %i. The file to be uploaded has a null length The filename's length must not exceed 12 characters ('%s' has %i characters). The filesystem does not support upload of files. The filesystem doesn't support getting file information The filesystem doesn't support getting files The filesystem doesn't support getting storage information The filesystem doesn't support setting file information The path '%s' is not absolute. The requested port type (%i) is not supported by this driver. There are still files in folder '%s/%s' that you are trying to remove. There are still subfolders in folder '%s/%s' that you are trying to remove. There are two resolution settings, 352x288 and 176x144. Photo data 
is in JPEG format when downloaded and thus has no predetermined
size. Therefore, the advertised maximum number of photos the
camera can hold must be understood as an approximation.
All gphoto2 options will work, except for the following which
the hardware will not support:
	Deletion of individual or selected photos (gphoto2 -d)
	Capture (gphoto2 --capture or --capture-image)
However, capture is possible using the webcam interface,
supported by the spca50x kernel module.
GUI access using gtkam has been tested, and works. However,
the camera does not produce separate thumbnails. Since the images
are in any event already small and of low resolution, the driver
merely downloads the actual images to use as thumbnails.
The camera can shoot in 'video clip' mode. The resulting frames
are saved here as a succession of still photos. The user can 
animate them using (for example) ImageMagick's 'animate' function.
For more details on the camera's functions, please consult
libgphoto2/camlibs/clicksmart310/README.clicksmart310.
 There is currently an operation in progress. This camera only supports one operation at a time. Please wait until the current operation has finished. There is space for another
   %d low compressed
   %d medium compressed or
   %d high compressed pictures
 This camera contains a Jeilin JL2005%c chipset.
The number of photos in it is %i. 
 This camera contains a Jeilin JL2005A chipset.
The number of photos in it is %i. 
 This camera does not provide information about the driver. This driver supports cameras with Jeilin jl2005a chip 
These cameras do not support deletion of photos, nor uploading
of data. 
Decoding of compressed photos may or may not work well
and does not work equally well for all supported cameras.
If present on the camera, video clip frames are downloaded 
as consecutive still photos.
For further details please consult libgphoto2/camlibs/README.jl2005a
 This is the driver for Canon PowerShot, Digital IXUS, IXY Digital,
 and EOS Digital cameras in their native (sometimes called "normal")
 mode. It also supports a small number of Canon digital camcorders
 with still image capability.
It includes code for communicating over a serial port or USB connection,
 but not (yet) over IEEE 1394 (Firewire).
It is designed to work with over 70 models as old as the PowerShot A5
 and Pro70 of 1998 and as new as the PowerShot A510 and EOS 350D of
 2005.
It has not been verified against the EOS 1D or EOS 1Ds.
For the A50, using 115200 bps may effectively be slower than using 57600
If you experience a lot of serial transmission errors, try to have your
 computer as idle as possible (i.e. no disk activity)
 This library has been tested with a Kodak DC 215 Zoom camera. It might work also with DC 200 and DC 210 cameras. If you happen to have such a camera, please send a message to koltan@gmx.de to let me know, if you have any troubles with this driver library or if everything is okay. This preview doesn't exist. Thumbnail Thumbnail height: %d
 Thumbnail size: %d
 Thumbnail width: %d
 Time format Time: %a, %d %b %Y %T
 Timer Timer 10 sec Timer 2 sec Too bright Too dark Toshiba
David Hogue <david@jawa.gotdns.org>
Toshiba pdr-m11 driver.
 Total pictures captured: %d, Flashes fired: %d
 Total pictures taken: %d
 Trying to contact camera... Tungsten Turbo mode Type of Slideshow UI Language UNIX Time USB 1.1 USB 2.0 USB Speed Unavailable Underwater Unknown Unknown (some kind of error) Unknown camera library error Unknown command Unknown error Unknown error. Unknown file type %i. Unknown flash mode %d,  Unknown model Unknown model "%s" Unknown value Unknown value %04d Unknown value %04x Unknown value 0x%04x Unknown zoom mode %d
 Unrecognized response Unsuitable card Unsupported command. Unsupported port type %i = 0x%x given. Initialization impossible. Unsupported port type: %d. This driver only works with USB cameras.
 Uploading file... Uploading image... Uploading image: %s. Uploading... User canceled download User defined 1 User defined 2 User defined 3 Vendor ID: %02x%02x
 Version:  Version: %s
 Video OUT Volatile Settings Volume Level WEP 128-bit WEP 64-bit WIFI channel WIFI profiles Waiting for completion... Waiting... We expected 0x%x but received 0x%x. Please contact %s. Weak White Balance White Board White balance White board White level WhiteBalance Whitebalance Reset Error Wide Write-protected Wrong escape sequence: expected 0x%02x, got 0x%02x. XMLDocument YYYY/MM/DD Year/Month/Day Yes You have been trying to delete '%s' from folder '%s', but the filesystem does not support deletion of files. You must be in record mode to capture images. You need to specify a folder starting with /store_xxxxxxxxx/ Your Logitech Clicksmart 310 has %i picture in it.
 Your Logitech Clicksmart 310 has %i pictures in it.
 Your USB camera has a S&Q chipset.
The total number of pictures taken is %i
Some of these could be clips containing
several frames
 Your USB camera has an Aox chipset.
Number of lo-res PICs = %i
Number of hi-res PICs = %i
Number of PICs = %i
 Your USB camera is a Kodak EZ200.
Number of PICs = %i
 Your USB camera is an iClick 5X.
The total number of pictures taken is %i
 Your USB camera seems to be a LG GSM.
Firmware: %s
Firmware Version: %s
 Your USB picture frame has a AX203 chipset
 Your USB picture frame has a ST2205 chipset
 Your camera does not support changing filenames. Your camera only supports deleting the last file on the camera. In this case, this is file '%s'. Zoom Zoom (in millimeters) Zoom level changed Zoom: 29 mm
 Zoom: 34 mm
 Zoom: 41 mm
 Zoom: 51 mm
 Zoom: 58 mm
 Zoom: macro
 a/c adaptor agfa_cl20
The Agfa CL20 Linux Driver People!
     Email us at cl20@poeml.de    
 Visit us at http://cl20.poeml.de  auto auto + red eye suppression battery black & white camera inactive for > 9 seconds, re-initing.
 canon_serial_get_dirents: Could not allocate %i bytes of memory canon_serial_get_dirents: Failed to read another directory entry canon_serial_get_dirents: canon_serial_dialogue failed to fetch directory entries color coolshot library v could not change owner name could not set time externally fill in fine gPhoto2 Mustek VDC-3500/Relisys Dimera 3500
This software was created with the
help of proprietary information belonging
to StarDot Technologies.

Author:
  Brian Beattie  <URL:http://www.beattie-home.net>
Contributors:
  Chuck Homic <chuck@vvisions.com>
     Converting raw camera images to RGB
  Dan Fandrich <dan@coneharvesters.com>
     Information on protocol, raw image format,
     gphoto2 port
 gsmart300 library 
Till Adam <till@adam-lilienthal.de>
Jerome Lodewyck <jerome.lodewyck@ens.fr>
Support for Mustek gSmart 300 digital cameras
based on several other gphoto2 camlib modules and the specifications kindly provided by Mustek.

 high (640x480) high compression,  hp215
Marcus Meissner <marcus@jet.franken.de>
Driver to access the HP Photosmart 215 camera.
Merged from the standalone hp215 program.
This driver allows download of images and previews, and deletion of images.
 iClick 5X driver
Theodore Kilgore <kilgota@auburn.edu>
 iTTL in fill mode internally jl2005a camera library
Theodore Kilgore <kilgota@auburn.edu>
 jl2005bcd camera library
Theodore Kilgore <kilgota@auburn.edu>
 lossless low (320x240) low compression,  mRAW mRAW + Large Fine JPEG mRAW + Large Normal JPEG mRAW + Medium Fine JPEG mRAW + Medium Normal JPEG mRAW + Small Fine JPEG mRAW + Small Normal JPEG medium compression,  menu no status reported. none normal not available: %s off on on + red eye suppression on battery pccam600 init:Unexpected error: gp_port_read returned %d instead of %d pccam600_close:return value was %d instead of %d pccam600_get_file:got index %d but expected index > %d pccam600_init:Expected %d blocks got %d pccam600_init:Expected > %d blocks got %d play power OK power bad read only readwrite ready record red eye suppression sRAW sRAW + Large Fine JPEG sRAW + Large Normal JPEG sRAW + Medium Fine JPEG sRAW + Medium Normal JPEG sRAW + Small Fine JPEG sRAW + Small Normal JPEG sRGB sRGB (nature) sRGB (portrait) sq905 generic driver
Theodore Kilgore <kilgota@auburn.edu>
 superfine time set unexpected datatype %i unknown compression %d,  unknown resolution %d)
 warning: CRC not checked (add len %d, value 0x%04x) #########################
 Project-Id-Version: libgphoto2 2.4.13
Report-Msgid-Bugs-To: gphoto-devel@lists.sourceforge.net
POT-Creation-Date: 2012-04-15 17:43+0200
PO-Revision-Date: 2016-05-22 13:02+0000
Last-Translator: Eugene Marshal <Unknown>
Language-Team: Russian <gnu@mx.ru>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 16:59+0000
X-Generator: Launchpad (build 18115)
Language: ru
 	Свободное место (байт): %llu (%lu Мб)
 	Свободное место (изображений): %d
 	Метка тома: %s
 
Определение фотоаппарата:
  Модель: %s
  Владелец: %s

Питание: %s

Карта памяти:
%s

Время: %s
 
Возможности устройства:
   Диск %s
  %11s байт всего
  %11s байт свободно   Серийный номер: %s
   Версия: %s
  (аккумулятор заряжен на %d%%)  Всего памяти %8d байт.
 Свободно памяти %8d байт.
 Число файлов: %d %.0f мм %d %d/%d %d мм %d/%d %f %i байт неизвестного формата изображения были приняты. Напишите %s и спросите о содействии. Не удалось удалить %i изображений, так как они защищены %s (время компьютера %s%i секунд) %s относится к одному из типов файлов, не имеющих миниатюр Библиотека для (Traveler) SX330z (И других фотоаппаратов Aldi).
Также должны работать аппараты таких производителей, как Jenoptik, Skanhex, Maginon.
Сообщения об ошибках и отзывы шлите по адресу:
Dominik Kuhlen <kinimod@users.sourceforge.net>
 * Часто артефакты на изображениях или сбои передачи данных
  возникают из-за недостаточного заряда аккумулятора.
* Сделанные эти аппаратом по удалённой команде снимки 
  хранятся во временном ОЗУ, и не записываются на карту памяти.
* Выдержка при съёмке фотографий может быть задана вручную
  или определяться автоматически.
* Пока что качество изображений уступает возможному.
 *НЕИЗВЕСТНО* -0,5 -1,0 -1,5 -2,0 0,0 0.1 м 0,5 0.5 м 1 1 мин. 1 минута 1,0 1,3 с 1,5 1,6 с 1,6x 1/1,3 с 1/1,6 с 1/1000 с 1/100 с 1/10 с 1/1250 с 1/125 с 1/13 с 1/15 1/15 с 1/1600 с 1/160 с 1/2 1/2,5 с 1/2000 с 1/200 с 1/20 с 1/2500 с 1/250 с 1/25 с 1/2 с 1/30 1/30 с 1/3200 с 1/320 с 1/3 с 1/4 1/4000 с 1/400 с 1/40 с 1/4 с 1/5000 с 1/500 с 1/50 с 1/5 с 1/60 1/60 с 1/6400 с 1/640 с 1/6 с 1/8 1/8000 с 1/800 с 1/80 с 1/8 с 10 мин. 10 минут 10 мм 10 секунд 100% 10 с 1152 x 864 12 мм 1280 x 960 13 с 15 15 мин. 15 минут 15 с 16 секунд 1 с 1x 2 2 секунды 2,0 2.0 м 2,5 с 2,5x 20 секунд 20 с 21 точка 25% 25 с 29 мм 2 с 2x 30 30 минут 30 секунд 30 с 34 мм 3 с 3x 4 4 секунды 41 мм 4 с 4x 5 мин. 5 минут 5 секунд 50% 51 мм 51 точка 51 точка (3D) 58 мм 5 с 5x 6 мм 6 секунд 640 x 480 6 с 6x 75% 7x 8 8 мм 8 секунд 8 с 8x 9 точек 9x Блок питания блок питания Адаптер питания подключен.
 Адаптер питания не подключен.
 Фиксация выдержки AF-A (автоматический режим автофокуса) AF-C (следящий автофокус) AF-S (автофокус для статичных сцен) AV (приоритет диафрагмы) Драйвер AX203 USB для фото-рамок
Hans de Goede <hdegoede@redhat.com>
Драйвер gphoto позволяет вам загружать,
сохранять и удалять изображения в фото-рамке. Фото-рамки, использующие набор микросхем AX203 выпускаются с разными разрешениями экрана.
Драйвер gphoto позволяет вам загружать,
сохранять и удалять изображения в фото-рамке. A_DEP (приоритет глубины резкости) О камере Konica Q-M150:
Эта камера не позволяет вносить изменения извне.
Поэтому можно увидеть настройки, сделанные на
самой камере, но нельзя внести в них
никаких изменений.

Если возникли проблемы с этим драйвером, сообщите авторам.
 Режим доступа Adc65
Benjamin Moos <benjamin@psnw.com> AdobeRGB Название альбома Щёлочно-марганцевый Произошла неизвестная ошибка. Пожалуйста, свяжитесь с %s. Против красных глаз Где угодно Базовый драйвер Aox
Theodore Kilgore <kilgota@auburn.edu>
 Диафрагма Параметры диафрагмы Параметры диафрагмы Диафрагма изменена Автор Вспомогательное освещение Авто Автодиафрагма Автовыключение (автономн.) (в секундах) Автоматическое отключение (компьютер) (в секундах) Время автовыключения Автоматическое отключение (минуты) Запоминание настроек выдержки Автофокус: один кадр Авто, уменьшение красноты глаз Режим автофокуса Ошибка автофокусировки Автомат
 Автоматическая вспышка Автоматическая настройка выдержки во время предварительного просмотра Автоматическая вспышка Автовспышка при съёмке Доступная память:  Усреднённый Ч/Б Ч/Б Неправильная контрольная сумма — получено 0x%02x, ожидалось 0x%02x. Неправильные данные — получено 0x%02x, ожидалось 0x%02x или 0x%02x. Неправильные данные — получено 0x%02x, ожидалось 0x%02x. Недопустимое значение состояния карты %d Недопустимое значение для состояния объектива %d
 Barbie/HotWheels/WWF
Scott Fritzinger <scottf@unr.edu>
Andreas Meyer <ahm@spies.com>
Pete Zaitcev <zaitcev@metabyte.com>

Формат изображений исследовал:
Jeff Laing <jeffl@SPATIALinfo.com>

Реализовано благодаря документации доступной в
Интернете с согласия Vision. Заряд аккумулятора недостаточен. Заряд аккумулятора в норме. Аккумулятор Зарядка аккумулятора Зарядка аккумулятора:		%s
Число изображений:	%d
Минимальный оставшийся объём:	%d
Занято:			%s
Зарядка вспышки:		%s
Состояние объектива:		 Режим батареи Тип батареи Акуумулятор разряжен, фотоаппарат выключен. Уровень заряда батареи: %.1f Вольт. Ревизия: %08x. Низкий уровень заряда батареи Состояние аккумулятора: %s, Блок питания: %s
 Пляж Сигнал Режим звукового сигнала изменён Звуковой сигнал выключен Звуковой сигнал включен Лучшее Битовая скорость %ld не поддерживается. Чёрно-белый Чёрно-белый Чёрная доска Чёрно-белый Чёрная доска Чёрный/белый Мигать Синий Яркость Яркость+ Яркость- Яркость/Контрастность Встроенное ОЗУ Встроенное ПЗУ Продолжительность сверхдлинной выдержки Занято CRW Фотоаппарат Конфигурация фотоаппарата Дата и время камеры ID фотоаппарата: %s
 Библиотека для фотоаппарата Kodak DC215 Zoom.
Michael Koltan <koltan@gmx.de>
 Режим камеры Модель фотоаппарата Модель камеры: %s
 Камера готова. Владелец фотоаппарата Настройки камеры Сведения о состоянии камеры Конфигурация фотоаппарата и драйвера Похоже, что камера не использует в качестве носителя карту CompactFlash.
К сожалению, мы пока не поддерживаем такие устройства :(
 Камера не может завершить операцию Фотоаппарат имеет память.
 Камерой снято %d фотографий, в качестве носителя используется память CompactFlash.
 Камера в неправильном режиме. Пожалуйста, свяжитесь с %s. Камера не готова Фотоаппарат не готов, %s Фотоаппарат не готов, несколько запросов идентификации завершились ошибкой: %s Время до перехода фотоаппарата в спящий режим (секунд) Фотоаппарат отклонил команду. Самопроизвольный сброс состояния фотоаппарата. Камера отправила непредвиденный байт 0x%02x. Камера поддерживает видеоформаты:  Фотоаппарат недоступен Камера недоступна: %s Фотоаппарат уже был активен Фотоаппарат выведен из спящего режима Не удаётся выполнить новые снимки. Неизвестная ошибка Не удалось удалить все изображения. Не удалось удалить изображение %s. Не удаётся загрузить это изображение на камеру. Возникла ошибка. Не удалось получить информацию об оставшейся свободной памяти Не удалось получить информацию о ёмкости батареи Не удалось получить имя папки, содержащей изображения Драйвер Canon PowerShot создан
Wolfgang G. Reissnegger,
Werner Almesberger,
Edouard Lafargue,
Philippe Marzouk,
добавление A5 - Ole W. Saastad
Holger Klemm Ошибка выключения видоискателя Canon: %d Ошибка включения видоискателя Canon: %d Съёмка Параметры съёмки Неподдерживаемый метод съёмки Карта Состояние карты:		 Не удаётся выполнить запись на карту памяти. Карта памяти не отформатирована Карта памяти не открыта Карта памяти вынута Карта памяти защищена от записи Название карты: %s
Свободное место на карте: %d Кб
 Неподдерживаемая карта памяти Карта извлечена во время доступа. Центрально-взвешенный Центровзвешенный Центровзвешенный средний Изменение скорости... подождите... Текст Character & Sound Зарядка Ошибка контрольной суммы Дети Выберите цветовую температуру Крупный план Облачность Цвет Режим цвета Настройки цвета Цветовое пространство Цветовая температура Команда не может быть отменена. Ошибка порядка команд. Ошибка связи 1 Ошибка связи 2 Ошибка связи 3 Обнаружена карта памяти Compact Flash
 Карта CompactFlash Режим совместимости Сжатие Параметры сжатия Конфигурация Конфигурация вашей камеры FUJI Фотоаппарат подключен Непрерывная на высокой скорости Непрерывная на низкой скорости Контраст Контраст+ Контраст- Копировать объект Copyright Copyright (макс.. 20 знаков) Сведения о Copyright Информация об авторском праве Данные повреждены Невозможно выделить %i байт для загрузки изображения. Невозможно выделить %i байт для загрузки миниатюры. Невозможно применить конфигурацию USB Не удалось изменить скорость ISO Не удалось сменить апертуру Не удалось изменить режим звукового сигнала Невозможно изменить компенсацию экспозиции Не удалось изменить режим вспышки Не удалось изменить режим фокусировки Не удалось изменить формат изображения Не удалось изменить режим съёмки Не удалось изменить скорость затвора Невозможно изменить время файла '%s' в '%s' (%m) Невозможно изменить уровень масштабирования Невозможно установить соединение с фотоаппаратом. Невозможно создать каталог \DCIM. Невозможно создать целевой каталог Невозможно создать каталог %s. Невозможно создать каталог. Невозможно удалить файл '%s' в папке '%s' (код ошибки %i: %m). Невозможно удалить файл. Фотоаппарат не обнаружен Невозможно установить соединение с фотоаппаратом Невозможно получить миниатюру JPEG: Данные не соответствуют формату JFIF Невозможно получить миниатюру JPEG: Отсутствует начало/конец Не найден драйвер для '%s' Невозможно найти файл '%s'. Не найдены данные локализации в '%s' Невозможно получить информацию о диске: %s Невозможно получить имя диска: %s Невозможно получить букву устройства карты памяти Невозможно получить сведения о '%s' (%m). Невозможно получить сведения о '%s' в '%s' (%m). Невозможно получить значение регистра %i. Пожалуйста, свяжитесь с %s. Не удалось начать процесс передачи данных (камера ответила: 0x%02x). Невозможно загрузить драйвер фотоаппарата '%s' (%s). Невозможно открыть '%s'. Невозможно удалить каталог %s. Невозможно удалить каталог. Невозможно сбросить состояние фотоаппарата.
 Невозможно назначить скорость %i (камера ответила: %i). Не удалось передать пакет (код ошибки %i). Пожалуйста, свяжитесь с %s. Невозможно загрузить, нет доступного свободного имени папки!
Папка 999CANON уже существует и в ней имеется изображение AUT_9999.JPG. Невозможно прочесть файл "%s" Создать профиль Wifi Дата создания: %s, дата последнего использования: %s Creative PC-CAM 300
 Авторы: Till Adam
<till@adam-lilienthal.de>
и: Miah Gregory
 <mace@darksilence.net> Creative PC-CAM600
Автор: Peter Kajberg <pbk@odense.kollegienet.dk>
 Текущий режим: режим камеры
 Текущий режим: режим воспроизведения
 Текущее время камеры:  %04d-%02d-%02d  %02d:%02d
Свободное место на карте памяти: %d
Изображений на карте: %d
Свободное место (изображений): %d
Зарядка батареи: %d %%. Пользовательский ДД/ММ/ГГГГ Данные повреждены. Дата и время Формат даты Формат метки даты/времени Дата и время Дата и время (GMT) Отображение даты Дата: %i/%02i/%02i %02i:%02i:%02i
Сделано снимков: %i
Осталось снимков: %i
Версия программы: %s
Скорость: %s
Память: %i мегабайт
Режим фотоаппарата: %s
Уровень качества: %s
Параметры вспышки: %s
Информация: %s
Таймер: %s
ЖК: %s
Автоматическое выключение: %i минут
Источник питания: %s Дата: %s День/Месяц/Год Дневной свет Отладка Глубокий Шлюз по умолчанию Стандартный драйвер sierra:

    Этот драйвер sierra используется по умолчанию.
    Он должен поддерживать копирование изображений
    и просмотр файлов в памяти фотоаппарата.

    Настройки фотоаппарата (или предпочтения)
    основаны на Olympus 3040 и скорее всего
    неполны. Если вы можете подтвердить, что
    список настроек вашего аппарата полон или
    сможете добавить свой код для поддержки
    всех имеющихся параметров, напишите, пожа-
    луйста в список рассылки для разработчиков.
 Удалить объект Удаление '%s' из папки '%s'... Удаляется изображение %s. Обнаружен "%s" или "%s" Обнаружен '%s'. Значок устройства Digita
Johannes Erdfelt <johannes@erdfelt.com> Цифровое увеличение Цифровое увеличение Режим обзора каталога - написан Scott Fritzinger <scottf@unr.edu>. Каталог существует Каталог не найден Отключение фотоаппарата. Не показывать ТВ меню Неизвестно как взаимодействовать с камерой->порт->тип значение %i как 0x%x в %s строка %i. Программа копирования снимков для Digital Dream Enigma 1.3. от <olivier@aixmarseille.com>, сделана на основе драйвера spca50x.Спасибо авторам spca50x! Адаптировать ваш драйвер для этого аппарата было нетрудно!  Программа копирования изображений для фотоаппаратов основанных на  GrandTek 98x.Первоначальный автор - Chris Byrne <adapt@ihug.co.nz>, адаптация для gphoto2 - Lutz Mueller <lutz@users.sf.net>. Усовершенствование протокола и доработка дляJenoptik JD350e выполнил Michael Trawny <trawny99@users.sourceforge.net>.Исправление ошибок - Marcus Meissner <marcus@jet.franken.de>. Программа копирования изображений для Polaroid DC700. Первоначально написана - Ryan Lantzer <rlantzer@umr.edu> для gphoto-4.x. Адаптация для gphoto2 - Lutz Mueller <lutz@users.sf.net>. Программа копирования снимков для нескольких моделей PolaroidПервоначальный автор - Peter Desnoyers <pjd@fred.cambridge.ma.us>, адаптация для gphoto2 - Nathan Stenzel <nathanstenzel@users.sourceforge.net>и Lutz Mueller <lutz@users.sf.net>.
Тестирование Polaroid 640SE - Michael Golden <naugrim@juno.com>. Копируется %s. Загрузка %s... Копирование '%s' из папки '%s'... Копируется '%s'... Копируется данные EXIF... Копируется звукопись... Копируются данные... Копируется файл... Копируется изображение %s. Копируется изображение... Копируется видеозапись... Копируется миниатюра... Копирование... Драйвер Параметры драйвера Двойная ирисовая диафрагма Ошибка контрольной суммы ППЗУ ОШИБКА: %d слишком велик ОШИБКА: обнаружены признаки неисправимой ошибки. Невозможно продолжать  ОШИБКА: неверный формат сообщения ОШИБКА: наслоение сообщений ОШИБКА: нарушение последовательности ОШИБКА: неожиданное сообщение ОШИБКА: неожиданное сообщение2 ОШИБКА: неожиданный тип пакета Пусто Шифрование Английский Ошибка Ошибка захвата изображения Ошибка изменения скорости Ошибка изменения скорости. Ошибка удаления связанного файла миниатюры Ошибка удаления файла Ошибка ожидания сигнала ACK во время инициализации Ошибка ожидания ACK во время инициализации, повторная попытка Оценочный Везде Выполнить предопределённую команду
с параметрами. Ожидалсь 32 байта, а получено %i. Пожалуйста, свяжитесь с %s. Выдержка Экспокоррекция Компенсация выдержки Индекс выдержки (ISO скорость плёнки) Экспонометр Экспозамер Режим экспозамера Программа экспозиции Режим программируемой выдержки Время выдержки Компенсация экспозиции Компенсация экспозиции изменена Компенсация экспозиции: %d
 Компенсация экспозиции: %s
 Длительность выдержки во время предварительного просмотра Внешняя вспышка Подключена внешняя вспышка Режим внешней вспышки Статус внешней вспышки Внешняя синхронизация Число F F2 F2,3 F2,8 F4 F5,6 F8 ППЗУ:
 Файлов: %d
 Заводские настройки Файл Файла '%s' нет в папке '%s'. Файл '%s/%s' не существует. Сжатие файла Файл существует Файл не найден Файл защищен. Разрешение Размер файла составляет %ld байт. Максимально возможный размер для закачки: %i байт. Тип файла Закачка файла не удалась. Тип файла: FlashPix ( Тип файла: JPEG ( Улучшенный Фейерверки Версия прошивки: %8s Версия прошивки: %8s
Изображения:     %i
Всего памяти: %iкб
Свободно памяти:  %iкб
 Версия прошивки: %d.%d
 Версия прошивки Версия прошивки: %d.%02d
 Микропрограмма: %d.%d
 Рыбий глаз Фикс. Вспышка Режим вспышки Без вспышки Параметры вспышки Параметры вспышки Автоматическая вспышка Режим вспышки изменён Режим вспышки: авто,  Режим вспышки: принудительная,  Режим вспышки: отключена
 Вспышка отключена Вспышка вкючена Только вспышка Вспышка : Авто Вспышка: Авто (Против красных глаз) Вспышка : Выкл. Вспышка : Вкл. Вспышка : Вкл. (Против красных глаз) Вспышка : не определено FlashPix Флуоресцентная лампа Флуоресцентная лампа 1 Флуоресцентная лампа 2 Флуоресцентная лампа 3 Флуоресцентная лампа 4 Флуоресцентная лампа 5 Длина фокуса Максимальное фокусное расстояние Минимальное фокусное расстояние Фокус Зоны фокусировки Фокусное расстояние Режим фокусировки Режим фокусировки изменён Точка фокусировки Ошибка фокусировки. Папка '%s' содержит только %i файлов, а вы запросили файл с номером %i. Для фотоаппаратов с микросхемой S&Q Technologies.
Работает с gtkam. Снимки сохраняются в формате PPM.

Все известные аппараты S&Q имеют два разрешения.
Каковы они, зависит от конкретной модели. Некоторые из
этих·фотоаппаратов позволяют удалять любые снимки, но
большинство·-·нет. Закачка данных в память не поддер-
живается.·Режим·сжатия, имеющийся у многих аппаратов
S&Q·отчасти·поддерживается.
Если фотоаппарат позволяет, видеозаписи представляются.
в виде подкаталогов. Gtkam копирует их раздельно.При
наличии видеозаписей перед названием аппарата появляется
маленький треугольник. Если в списке нет папок, щёлкните на
треугольнике, чтобы отобразить их. Щёлкните папку, чтобы
войти в неё и увидеть или копировать кадры. Кадры извлекаются
как отдельные изображения, имена которых сообщают, к какой
видеопоследовательности они относятся. Таким образом можно 
выбрать, сохранять ли кадры в отдельных каталогах или нет.
 Для камер с внутренними компонентами S&Q Technologies и
идентификатором поставщика USB ID 0x2770 и идентификатором 
продукта 0x905C, 0x9050, 0x9051, 0x9052 или 0x913D.  
Фотографии сохраняются в формате PPM.

Некоторые камеры допускают программное удаление всех
фотографий. Другие нет. Неподдерживаемая камера может
производить захват изображения (capture-all). Все камеры 
могут производить захват предпросмотровых изображений 
'capture-preview' (изображение записывается и отправляется 
на компьютер). Если функция удаления всех фотографий 
'delete-all' работает с вашей камерой, то функция 
'capture-preview' имеет побочный эффект - удалит также 
изображение с камеры.

Выгрузка файлов неподдерживается этими камерами. 
Ни одна из поддерживаемых камер не позволяет
удаление конкретных фотографий программной командой.
 Принудительная вспышка
 Форматировать карту и задать имя альбома. Отформатировать Compact Flash Осталось кадров: %i
 Сделано снимков     : %4d
 Снято кадров: %i
 Французский Полная Полное изображение Немецкий Получение конфигурации... Получение данных... Получение списка файлов... Получение файла... Получение сведений о %i файлах... Получение миниатюры... Получен непредвиденный результат 0x%x. Пожалуйста, свяжитесь с %s. Оттенки серого Оттенки серого Зелёный Аппаратный сбой Высок. высокое (1152 x 872) высокое (хорошее качество) Режим работы:		%s
Коррекция выдержки:	%s
Данные выдержки:		%d
Дата указана:		%s
Дата:			%d/%02d/%02d %02d:%02d:%02d
Задержка спуска:		%s
Уровень качества:	%s
Режим воспроизведения/записи:	%s
ID карты указан:		%s
ID карты:		%d
Режим вспышки:		 Время до перехода компьютера в спящий режим (секунд) Сколько времени пройдёт до автоматического выключения фотоаппарата,когда он подключён к компьютеру? Сколько времени пройдёт до автоматического выключения фотоаппарата,когда он не подключён к компьютеру? Длительность до отключения камеры? Выполняется операция ввода-вывода ID IP-адрес (не заполнен для DHCP) Скорость ISO Светочувствительность (ISO) изменена Значки Свободно Недопустимый параметр: Изображение Изображение %s защищено от удаления. Изображение и звук Формат изображения Качество изображения Поворот изображения Параметры изображения Размер изображения Стабилизация изображения Формат изображения изменён Высота изображения: %d
 Недопустимое число изображений. Изображение защищено. Размер изображения: %d
 Формат изображения %d не поддерживается фотоаппаратом ! Тип изображений %d не поддерживается Данный формат изображения не поддерживается Ширина изображения: %d
 Подсоединён Лампа накаливания В помещении Бесконечность Бесконечность/Рыбий глаз Информация Сведения относящиеся к камерам с идентификатором 
0x2770:0x9153.

Мы не рекомендуем использовать программу с
графическим интерфейсом пользователя для доступа
к этой камере, если вы только не просто хотите повеселиться
или лицезреть сгорит ли предохранитель.
Для производственного использования, попробуйте:
gphoto2 -P из командной строки.
Примечание: Невозможно загружать видео клипы.
 не распознан начальный ответ камеры '%c' Инициализация фотоаппарата Внутреннее ОЗУ Внутренняя ошибка 1 в функции get_file_func() (%s строка %i) Внутренняя ошибка (1). Внутренняя ошибка (2). Внутренняя память: %d Мб всего, %d Мб свободно.
 Ошибка Недопустимая настройка светочувствительности (ISO) Недопустимое значение ( %d )
 Неверные настройки апертуры Недопустимая настройка компенсации экспозиции Недопустимая настройка режима вспышки Недопустимая настройка режима фокусировки Недопустимые настройки формата изображения Неверные настройки режима съёмки Недопустимая настройка выдержки затвора Ошибка ирисовой диафрагмы Итальянский JD11
Marcus Meissner <marcus@jet.franken.de>
Драйвер фотоаппарата Jenoptik JD11.
Для исследования протокола применялись WINE и IDA. Конфигурация JD11 JPEG JPEG Fine (качественный) JPEG Normal (обычный) Японский Сохранить имя файла при закачке Дети и домашние питомцы Известные проблемы:

1. Если Kodak DC3200 не получает команд в течение 10 секунд, Произойдёт тайм-аут и потребуется повторная инициализация. Если аппарат перестаёт реагировать, просто заново выберите его. При этом произойдёт новая инициализация. Известные проблемы:

При возникновении ошибок передачи данных выполните сброс фотоаппарата и перезапустите программу. Драйвер пока не может самостоятельно выходить из таких положений, особенно, если проблема возникает при скоростях больших чем 9600 и аппарат не выключается полностью. Библиотека поддержки Kodak DC120
Scott Fritzinger <scottf@gphoto.net>
Библиотека для работы с фотоаппаратом Kodak DC120.
(по многочисленным требованиям). Библиотека для фотоаппарата Kodak DC240
Scott Fritzinger <scottf@gphoto.net> и Hubert Figuiere <hfiguiere@teaser.fr>
Библиотека для фотоаппаратов Kodak DC240, DC280, DC3400 и DC5000.
Обновлено и переписано для gPhoto2. Драйвер Kodak DC3200
Donn Morrison <dmorriso@gulf.uvic.ca>

Вопросы и отзывы принимаются с благодарностью. Драйвер Kodak EZ200
Bucas Jean-Francois <jfbucas@tuxfamily.org>
 Настройка Konica Библиотека Konica Q-M150
Marcus Meissner <marcus@jet.franken.de>
Aurelien Croc (AP2C) <programming@ap2c.com>
http://www.ap2c.com
Поддержка французской версии Konica Q-M150. Библиотека Konica
Lutz Mueller <lutz@users.sourceforge.net>
Поддержка всех аппаратов Konica и некоторых моделей HP. Корейский ЖК Автоматическое отключение ЖК (сек.) Яркость ЖК-экрана Режим ЖК экрана Светодиод Пейзаж Язык Драйвер Largan
Hubert Figuiere <hfiguiere@teaser.fr>

Поддержка фотоаппарата Largan Lmini.
 Большой качественный JPEG Большой обычный JPEG Режим объектива Название объектива Тип объектива Объектив не подключен
 Светлый Недостаточное освещение Список профилей Wifi Показатьсписок всех файлов Составление списка файлов в '%s'... Получение списка папок в '%s'... Литий-ионный Загрузка драйверов камеры с «%s»... Подключённый Локализация Слишком длинный файл локализации! Драйвер Logitech Clicksmart 310
Theodore Kilgore <kilgota@auburn.edu>
 Поиск фотоаппарата... Похоже, что это модем, а не фотоаппарат Без потерь Низк. высокое (576 x 436) слабое (наилучшее качество) Нельзя использовать строчные буквы в %s. ММ/ДД/ГГГГ M_DEP (максимальная глубина резкости для элементов в видоискателе) Макро Ручной Ручная фокусировка Ручная фокусировка Производитель: %s
 Библиотека для фотоаппарата Mars MR97310
Theodore Kilgore <kilgota@auburn.edu>
 Камера Mars MR97310.
Содержит %i фотографию.
 Камера Mars MR97310.
Содержит %i фотографии.
 Камера Mars MR97310.
Содержит %i фотографий.
 Матричный Matthew G. Martin
На основе fujiplay от Thierry Bousch <bousch@topo.math.u-psud.fr>
 Макс. фокусное расстояние Макс. Максимальное количество изображений: %d
 Средн. среднее (1152 x 872) среднее (повышенное качество) Средний качественный JPEG Средний обычный JPEG Осталось памяти: %i bytes
 Карта памяти Статус карты памяти (%d): %s
 Меню и воспроизведение Режим замера Электронная таблица Microsoft Excel (.xls) Презентация Microsoft Powerpoint (.ppt) Документ Microsoft Word Мин. фокусное расстояние Мин. Библиотека для фотоаппарата Minolta Dimage V
%s
Gus Hartmann <gphoto@gus-the-cat.org>
Особо благодарю фирму Minolta за предоставленные спецификации. Модель:			Minolta Dimage V (%s)
Версия устройства:	%s
Версия прошивки:	%s
 Модель:  Модель: %s
 Модель: %s
Ёмкость: %i МБ
Питание: %s
Автоотключение: %i мин.
Режим: %s
Изображений: %i/%i
Формат даты: %s
Дата и время: %s
 Модель: %s
Память: %d байт(ов) из %d доступных Модель: %s
Серийный номер: %s,
Версия устройства: %i.%i
Версия программы: %i.%i
Версия диагностической программы: %i.%i
Название: %s,
Производитель: %s
 Модель: %x, %x, %x, %x Модель: Kodak %s
 Монитор Месяц/День/Год Переместить объект Качество видео Размер видеокадра Звук видео Библиотека gPhoto2 для Mustek MDC-800
Henning Zabel <henning@uni-paderborn.de>
Адаптация для gphoto2 - Marcus Meissner <marcus@jet.franken.de>
Поддерживает последовательный и USB протоколы. НЕ РАСПОЗНАНО NTSC Сообщаемого фооаппаратом названия "%s" нет среди известных моделей Слишком длинное имя «%s» (%li символов), максимально допустимо 30 символов. Название карты присваиваемое при форматировании. Маска сети Следующий Никель-кадмиевый Никель-металл-гидридный Ночной пейзаж Ночной портрет Ночная съёмка Ночная съёмка Nikon Coolpix 880:
    Настройка фотоаппарата (предпочтения):

        Оптическое приближение работает
        неправильно.

        Не все параметры настройки
        можно как следует читать и изменять
        например, это относится к тонкой настройке
        баланса белого и настройкам языка.

        Переведите аппарат в режим 'M', если хотите
        задать скорость затвора.
 Nikon Coolpix 995:
    Набор параметров настройки этого аппарата
    неполон, напишите в список рассылки
    разработчиков gphoto, если хотите внести 
    поправки·в·этот·драйвер.

    Копирование снимков должно работать верно.
 Нет Не обнаружено карт памяти Compact Flash
 Без двойной диафрагмы Нет данных EXIF для %s. Без вспышки Без индикатора питания Без переключения разрешения Не вставлена SD-карта.
 Нет дополнительных сведений. Нет ответа от фотоаппарата. Не найден файл звукозаписи для %s Руководство пользователя фотоаппарата недоступно.
 Нет карты памяти В камере отсутствует карта памяти.
 Нет карты памяти. Нет карты памяти Причина неизвестна Нет ответа от фотоаппарата Не хватает места для новых снимков. Нужно удалить некоторые изображения. Не осталось места на карте. Нет Не выбран Норм. Нормальный
 Неполная Недостаточно свободного места Недостаточно памяти на карте памяти Не в режиме заполнения Не готово Отсоединён Замечание: карта памяти отсутствует, некоторые значения могут быть неправильными
 Количество изображений: %d
 Количество изображений: %d Количество изображений: %d
 Количество изображений: %i
Версия прошивки: %s ВЫКЛ ОК ВКЛ Выкл Офис Вкл Экранные подсказки Вкл, уменьшение красноты глаз Поддерживается обращения только в корневую папку - запрошен список файлов папки '%s'. Режим работы Действие отменено Оптимальное качество Ориентация Прочее Прочие параметры Выполняется другая команда. Не в фокусе Недостаточно памяти Недостаточно памяти: необходимо %d байт(ов). Недостаточно памяти: необходимо %ld байт(ов). Солнечный Имя владельца Имя владельца изменено PAL ПК PTP Параметр не поддерживается Библиотека gPhoto для Panasonic DC1000
Mariusz Zynel <mariusz@mizar.org>

Основана на программе для dc1000, написанной
Fredrik Roubert <roubert@df.lth.se> и
Galen Brooks <galen@nine.com>. Драйвер Panasonic PV-L859-K/PV-L779-K Palmcorder
Andrew Selkirk <aselkirk@mailandnews.com> Параметр 1 Параметр 2 Параметр 3 Частичный Неабсолютный путь Постоянные настройки Снимок в движении Снимков в камере: %d
 Конфигурация фоторамки Настройки изображения Изображения Изображений на камере: %d
 Воспроизв. Режим воспроизведения Скорость порта Портрет Португальский Электропитание Экономия энергии Выключить устройство Шаблон Собственный профиль 1 Собственный профиль 2 Собственный профиль 3 Собственный профиль 4 Собственный профиль 5 Предыдущий Проблема копирования изображения Проблема получения сведений об изображении Проблема получения числа изображений Ошибка открытия порта Ошибка чтения изображения с карты памяти Ошибка сброса состояния фотоаппарата Проблема установки скорости обмена данными с фотоаппаратом Ошибка съёмки ID продукта: %02x%02x
 Имя профиля Запрет вспышки
 Качество RAW RAW + JPEG Fine (качественный) RAW + JPEG Normal (обычный) RAW + крупный качественный JPEG RAW + крупный обычный JPEG RAW + средний качественный JPEG RAW + средний обычный JPEG RAW + малый качественный JPEG RAW + малый обычный JPEG RAW 2 Усиление RGB Только чтение с удалением объектов Только чтение Чтение и запись Нельзя изменять атрибуты только для чтения, такие как ширина и высота. Готов Получен непредвиденный ответ (%i). Пожалуйста, свяжитесь с %s. Получены непредвиденные данные (0x%02x, 0x%02x). Получен непредвиденный заголовок (%i) Получен неожиданный ответ Принимаются данные... Запись Режим записи Режим записи Красный Подавление красных глаз Уменьшение эффекта красных глаз Автономный Съёмное ОЗУ (карта памяти) Съёмное ПЗУ Запрашивалась информация об изображении %i (= 0x%x), а получена информация об изображении %i Сброс протокола... Разрешение Переключение разрешения Разрешение плюс размер Драйвер Ricoh / Philips
Lutz Mueller <lutz@users.sourceforge.net>,
Martin Fischer <martin.fischer@inka.de>,
на основе драйвера от Bob Paauwe
 Ricoh Caplio G3.
Marcus Meissner <marcus@jet.franken.de>
Исследование протокола проведено с помощью USB Snoopy,
декомпиляции прошивки и интуиции.
 Угол поворота SD-карта: %d Мб всего, %d Мб свободно.
 SDRAM SDRAM:
 Файлов: %d
  Изображений: %4d
  Видеозаписей: %4d
Занято: %8d
Свободно: %8d
 STV0674
Vincent Sanders <vince@kyllikki.org>
Драйвер для фотоаппаратов с процессором STV0674.
Для исследование протокола использовался SnoopyPro
 STV0680
Adam Harrison <adam@antispin.org>
Драйвер с процессором STV0680.
Исследование протокола выполнено с помощью CommLite Beta 5
Carsten Weinholz <c.weinholz@netcologne.de>
Расширено для  Aiptek PenCam и других двурежимных USB фотоаппаратов STM. Насыщенность Автоспуск Автоспуск (след. снимок только) Время автоспуска Самотестирование устройства Автоспуск Задержка автоспуска Передача данных... Чувствительность ID сенсора: %d.%d
 Сепия Последовательность 9 Серийный номер Серийный номер: %s
 Установить часы камеры Должна ли камера подавать звуковой сигнал при съёмке изображения? Резко Увеличение резкости Чёткость Режим съёмки Скорость съёмки Режим съёмки Режим съёмки изменён Скорость срабатывания затвора Скорость затвора (в секундах) Скорость затвора изменена SiPix Web2
Marcus Meissner <marcus@jet.franken.de>
Драйвер доступа к веб-камере SiPix Web2. Одиночный кадр Один кадр Драйвер Sipix StyleCam Blink
Vincent Sanders <vince@kyllikki.org>
Marcus Meissner <marcus@jet.franken.de>.
 Приоритет размера Дневной свет Интервал между слайдами Замедленная синхронизация Замедленная синхронизация Smal Ultrapocket
Lee Benfield <lee@benf.org>
Драйвер доступа к аппарату Smal Ultrapocket и его OEM вариантам (slimshot) Малый качественный JPEG Малый обычный JPEG Снег Мягко Несколько замечаний о фотоаппаратах Epson:
- Отдельными параметрами нельзя управлять удалённо:
  * приближение
  * фокус
  * пользовательские настройки баланса белого
- Декомпиляция файлов конфигурации производилась с
  PhotoPC 3000z. Если ваш аппарат работает иначе, напишите,
  пожалуйста по адресу %s (по английски)
 Несколько замечаний о фотоаппаратах Epson:
- Отдельными параметрами нельзя управлять удалённо:
  * приближение
  * фокус
  * пользовательские настройки баланса белого
- Декомпиляция файлов конфигурации производилась с
  PhotoPC 3000z, если ваш аппарат работает иначе, напишите
  об этом в список рассылки для разработчиков gphoto (по английски)
 Камера Sonix.
Содержит %i фотографию.
 Камера Sonix.
Содержит %i фотографии.
 Камера Sonix.
Содержит %i фотографий.
 Поддержка фотоаппарата Sony DSC-F1
M. Adam Kendall <joker@penguinpub.com>
На основе консольной версии chotplay от
Кен-Ичи Хайяши
Портирование в Gphoto2 выполнил Bart van Leeuwen <bart@netage.nl> Библиотека gPhoto для Sony DSC-F55/505
Поддерживает Sony MSAC-SR1 и карты Memory Stick от DCR-PC100
Изначально написана Mark Davies <mdavies@dial.pipex.com>
Адаптация к gPhoto2 - Raymond Penners <raymond@dotsphinx.com> Камера Canon не поддерживает функцию захвата Canon capture Звук Драйвер Soundvision
Vince Weaver <vince@deater.net>
 Испанский Скорость %i не поддерживается! Превышающие 57600 скорости не поддерживаются при загрузке в этот фотоаппарат Спорт Точечный Точечный режим замера Точечный автофокус Стандартный Шаг №2. Ошибка инициализации! (возвращено %i байт, а ожидалось %i). Камера отказывается функционировать. Шаг №2. Ошибка инициализации: ("%s" при чтении %i). Камера отказывается функционировать. Шаг №3. Ошибка инициализации! (возвращено %i, а ожидалось %i). Камера отказывается функционировать. Шаг №3. Ошибка инициализации: "%s" при чтении %i. Камера отказывается функционировать. Ошибка строба. Сводка для Mustek MDC800:
 Супер-макро Фотоаппарат выключается Синхронизировать время и данные камеры с ПК Синхронизировать данные и время фоторамки с ПК Системная ошибка. ЖК + ПК TIFF (RGB) TTL TV (приоритет выдержки) Формат вывода на ТВ Тест различных скоростей... Вы можете проиндексировать все ваши фотографии на жёстком диске в каталог "camera". Фотоаппарат JD11 достаточно хорошо работает с этим драйвером.
Для получения изображений необходим интерфейс RS232 со скоростью 115 Кбит.
Драйвер позволяет копировать

   - миниатюры (64x48, формат PGM )
   - фотографии (640x480, формат PPM)
 Фотоаппарат Kodak DC120 использует формат KDC для записи изображений. Для просмотра полученных из аппарата изображений понадобится программа "kdc2tiff". Её можно загрузить по адресу http://kdc2tiff.sourceforge.net Драйвер Samsung digimax 800k написан James McKenzie <james@fishsoup.dhs.org> для gphoto. Lutz Mueller <lutz@users.sourceforge.net> портировал его в gphoto2. Marcus Meissner <marcus@jet.franken.de> исправлял ошибки и вносил усовершенствования. Слишком слабый заряд аккумулятора фотоаппарата (%d%%). Действие прервано. Не удаётся установить связь с камерой. Убедитесь, что она подсоединена к компьютеру и включена. Камера не принимает команды. Камера не принимает «%s» в качестве имени файла. Фотоаппарат не поддерживает скорость %i. Фотоаппарат только что сообщил об нераспознанной ошибке. Пожалуйста, сообщите об этом %s и опишите, как и когда произошла эта ошибка: (0x%x,0x%x). Большое спасибо! Камера отправила больше байтов, чем ожидалось (%i) Камера отправила только %i байт, а нужно как минимум %i. Загружаемый файл имеет нулевую длину Длина имени файла не должна превышать 12 символов («%s» содержит %i символов). Файловая система не поддерживает закачку файлов. Файловая система не поддерживает получение информации о файле Файловая система не поддерживает получение файлов Файловая система не поддерживает получение информации об устройстве хранения Файловая система не поддерживает изменение сведений о файле Путь «%s» не является абсолютным. Запрошенный тип порта (%i) не поддерживается этим драйвером. Папка '%s/%s', которую вы пытаетесь удалить, содержит файлы. В папке '%s/%s', которую вы пытаетесь удалить, имеются вложенные папки. В наличии две настройки разрешения 352 x 288 и 176 x 144. 
Данные фотографии в формате JPEG на момент загрузки, т.о. 
не предопределён размер. Следовательно, рекламируемое
максимальное количество фотографий, которое может
удерживать камера должно приниматься за приблизительное.
Все параметры gphoto2 будут работоспособны, за исключением
тех, которые не поддерживаются оборудованием:
	Удаление конкретных или выделенных фотографий (gphoto2 -d)
	Захват (gphoto2 --capture или --capture-image)
Тем не менее, функция захвата доступна при использовании
интерфейса веб-камеры поддерживаемого модулем ядра spca50x.
Доступ через графический интерфейс пользователя gtkam был
протестирован и он работоспособен. Однако, камера не производит
создание отдельных миниатюр. Так как изображения уже уменьшены
и имеют низкое разрешение, драйвер просто загружает
актуальные изображения и выдаёт их за миниатюры.
Камера может снимать в режиме 'видео клипа'.  Готовые кадры
сохраняются здесь как череда неподвижных фотографий. 
Пользователь может придать им движение используя функцию
'анимирования' к примеру в ImageMagick.
Для получения дополнительных сведений о возможностях камеры,
ознакомьтесь с:
libgphoto2/camlibs/clicksmart310/README.clicksmart310.
 Выполняется действие. Данный фотоаппарат не поддерживает одновременное выполнение нескольких команд. Дождитесь окончания текущей операции.. Есть место ещё для
   %d слабо сжатых
   %d средне сжатых или
   %d сильно сжатых изображений
 Это камера с чипсетом Jeilin JL2005%c.
Содержит %i снимков. 
 Это камера с чипсетом Jeilin JL2005A.
Содержит %i снимков. 
 Эта камера не предоставила информации о драйвере. Драйвер поддерживает камеры с чипом Jeilin jl2005a.  
Эти камеры не поддерживают удаление фотографий
и выгрузку данных. 
Декодирование сжатых фотографий может работать,
а может не работать надлежащим образом и не функционирует
одинаково для всех поддерживаемых камер.
Если поддерживается камерой, кадры видео клипа загружаются
как последовательность неподвижных изображений.
Дополнительные сведения см. в libgphoto2/camlibs/README.jl2005a
 Это драйвер камер Canon PowerShot, Digital IXUS, IXY Digital,
 и EOS Digital, поддерживающий их обычную функциональность.
Он также поддерживает небольшое количество цифровых видеокамер Canon
с возможностью создания неподвижного изображения.
Включает в себя код для создания соединения с использованием 
последовательного порта или USB, но (в разработке) без поддержки IEEE 1394 (Firewire).
Разработан для работы с более чем 70 моделями снятыми с производства PowerShot A5
и Pro70 1998 года выпуска и новыми PowerShot A510, EOS 350D 2005 года выпуска.
Проверка работоспособности с EOS 1D или EOS 1Ds не была проведена.
Для A50 115200 бит/с может замедлять эффективную работу, чем используя
57600. Если у вас возникает большое количество ошибок при последовательной передаче, 
попробуйте не пользоваться компьютером, а сохранять режим простоя (например: избегать
активного пользования диском)
 Работоспособность драйвера проверена с аппаратом Kodak DC 215 Zoom Возможно он подойдёт для DC 200 и DC 210. Если у вас есть один из перечисленных фотоаппаратов, напишите, пожалуйста по адресу koltan@gmx.de и сообщите, все ли функции доступны с этим драйвером, и нет ли с ним проблем. Предпросмотр недоступен. Миниатюра Высота миниатюры: %d
 Размер миниатюры: %d
 Ширина миниатюры: %d
 Формат времени Время: %a, %d %b %Y %T
 Таймер Таймер 10 сек. Таймер 2 сек. Слишком яркое освещение Недостаточное освещение Toshiba
David Hogue <david@jawa.gotdns.org>
Драйвер Toshiba pdr-m11.
 Всего сделано снимков: %d, срабатываний вспышки: %d
 Общее количество сделанных снимков: %d
 Установление связи с фотоаппаратом... Вольфрамовая лампа Турбо-режим Тип слайд-шоу Язык интерфейса Время UNIX USB 1.1 USB 2.0 Скорость USB нет данных Под водой Неизвестно Неизвестно. Похоже на ошибку. Неизвестная ошибка драйвера фотоаппарата Неизвестная команда Неизвестная ошибка Неизвестная ошибка. Неизвестный тип файла %i. Неизвестный режим вспышки %d,  Неизвестная модель Неизвестная модель "%s" Неизвестное значение Неизвестное значение %04d Неизвестное значение %04x Неизвестное значение 0x%04x Неизвестный режим трансфокатора %d
 Нераспознанный ответ Несовместимая карта Неподдерживаемая команда. Получен неподдерживаемый тип порта %i = 0x%x. Инициализация невозможна. Не поддерживаемый тип порта: %d. Этот драйвер работает только с USB-камерами.
 Закачка файла... Закачка изображения... Закачка изображения: %s. Закачка... Копирование прервано пользователем Определено пользователем 1 Определено пользователем 2 Определено пользователем 3 ID производителя: %02x%02x
 Версия:  Версия: %s
 Видеовыход Временные настройки Уровень громкости WEP 128-бит WEP 64-бит Канал WIFI Профили WIFI Ожидание завершения... Ожидание... Ожидалось 0x%x, а получено 0x%x. Пожалуйста, свяжитесь с %s. Слабая Баланс белого Белая доска Баланс белого Белая доска Уровень белого Баланс белого Ошибка сброса баланса белого Широкоугольный Защищена от записи Неправильная управляющая последовательность: ожидалось 0x%02x, получено 0x%02x. Документ XML ГГГГ/ММ/ДД Год/Месяц/День Да Вы пытаетесь удалить '%s' из папки '%s', но файловая система не поддерживает удаление файлов. Для захвата изображений, вам необходимо переключиться в режим записи. Нужно указать папку, начинающуюся с /store_xxxxxxxxx/ Logitech Clicksmart 310 содержит %i изображение.
 Logitech Clicksmart 310 содержит %i изображения.
 Logitech Clicksmart 310 содержит %i изображений.
 Ваш USB фотоаппарат имеет м/с набор S&Q.
Общее число сделанных снимков - %i
Некоторые из них могут быть видеозаписями,
состоящими из нескольких кадров.
 Ваш USB-фотоаппарат основан на м/с наборе Aox.
Число изображений низкого разрешения = %i
Число изображений высокого разрешения = %i
Число изображений = %i
 Модель вашего USB-фотоаппарата - Kodak EZ200.
Число изображений = %i
 Ваша USB-камера — iClick 5X.
Общее количество снятых изображений: %i
 Ваша USB-камера идентифицируется как LG GSM.
Микропрограмма: %s
Версия микропрограммы: %s
 В вашей USB фоторамке используется набор микросхем AX203
 Ваша USB-фоторамка содержит чипсет ST2205
 Ваша камера не поддерживает изменение имён файлов. Камера поддерживает удаление только последнего файла из её памяти. В данном случае это файл «%s». Приближение Приближение (в миллиметрах) Уровень масштабирования изменён Трансфокатор: 29 мм
 Трансфокатор: 34 мм
 Трансфокатор: 41 мм
 Трансфокатор: 51 мм
 Трансфокатор: 58 мм
 Трансфокатор: макро
 блок питания agfa_cl20
Разработчики драйвера Agfa CL20 для Linux!
     Э-почта - cl20@poeml.de    
 Веб-адрес http://cl20.poeml.de  авто авто + подавление красных глаз аккумулятор чёрно-белый камера неактивна > 9 секунд, выполняется переинициализация.
 canon_serial_get_dirents: Невозможно выделить %i байт памяти canon_serial_get_dirents: Ошибка чтения другой записи каталога canon_serial_get_dirents: canon_serial_dialogue не может получить записи из каталога цвет Библиотека coolshot v невозможно изменить имя владельца невозможно установить время снаружи заполнение высокое gPhoto2 Mustek VDC-3500/Relisys Dimera 3500
При создании этой программы использовалась
закрытая информация, принадлежащая фирме
StarDot Technologies.

Автор:
  Brian Beattie  <URL:http://www.beattie-home.net>
Помощь оказывали:
  Chuck Homic <chuck@vvisions.com>
     Преобразование исходных данных снимков в формат RGB
  Dan Fandrich <dan@coneharvesters.com>
     Информация о протоколе обмена, формате исходных данных,
     портирование для gphoto2
 Библиотека gsmart300 
Till Adam <till@adam-lilienthal.de>
Jerome Lodewyck <jerome.lodewyck@ens.fr>
Поддержка фотоаппаратов Mustek gSmart 300
основывается на нескольких модулях gphoto2 и спецификациях, которые были любезно предоставлены фирмой Mustek.

 высокое  (640x480) сильное сжатие,  hp215
Marcus Meissner <marcus@jet.franken.de>
Драйвер доступа к камере HP Photosmart 215.
Выделено из отдельной программы hp215.
Драйвер позволяет загружать изображения 
и миниатюры, а так же удалять изображения.
 Драйвер iClick 5X
Theodore Kilgore <kilgota@auburn.edu>
 iTTL в режиме заполнения внутри Библиотека для камеры jl2005a
Theodore Kilgore <kilgota@auburn.edu>
 Библиотека для камеры jl2005bcd
Theodore Kilgore <kilgota@auburn.edu>
 без потерь низкое (320x240) слабое сжатие,  mRAW mRAW + крупный качественный JPEG mRAW + крупный обычный JPEG mRAW + средний качественный JPEG mRAW + средний обычный JPEG mRAW + мелкий качественный JPEG mRAW + мелкий обычный JPEG среднее сжатие,  меню нет сообщения о состоянии. нет нормальное нет данных: %s выкл вкл вкл. + подавление красных глаз аккумулятор pccam600 init:Неожиданная ошибка: gp_port_read вернула %d вместо %d pccam600_close:получено значение %d вместо %d pccam600_get_file:получен индекс %d, а ожидался > %d pccam600_init:Ожидалось %d блоков, получено %d pccam600_init:Ожидалось > %d блоков, получено %d воспроизведение питание ОК сбой питания только для чтения чтение и запись готов запись подавление красных глаз sRAW sRAW + крупный качественный JPEG sRAW + крупный обычный JPEG sRAW + средний качественный JPEG sRAW + средний обычный JPEG sRAW + мелкий качественный JPEG sRAW + мелкий обычный JPEG sRGB sRGB (природа) sRGB (портрет) Базовый драйвер sq905
Theodore Kilgore <kilgota@auburn.edu>
 наилучшее время установлено неожиданный тип данных %i неизвестный уровень сжатия %d,  неизвестное разрешение %d)
 внимание: контрольная сумма не проверена (+ длина %d, значение 0x%04x) #########################
 