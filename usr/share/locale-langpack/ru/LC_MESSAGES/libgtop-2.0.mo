��    )      d  ;   �      �     �     �     �     �     �     �  	             !     :     C  	   ^     h     y     �     �     �     �     �     �  	                  ,     1     G  F   L     �     �     �  
   �     �     �     �          !     4  ,   O     |     �    �     �     �  $   �  "   	  D   %	     j	     �	  $   �	  ;   �	     
  8   
     O
     `
  .   �
  7   �
  2   �
       1   -  -   _  !   �     �     �  #   �  
   �  +     
   3  �   >  )   �          !  #   6  ?   Z  '   �  '   �  #   �  *     `   9  �   �  0   !  V   R         %           '         )               "       
                                                              !                                                     $      	   (                 #   &    Abort Alarm clock Background read from tty Background write to tty Bad argument to system call Broken pipe Bus error CPU limit exceeded Child status has changed Continue Don't fork into background EMT error Enable debugging Enable verbose output File size limit exceeded Floating-point exception Hangup I/O now possible Illegal instruction Information request Interrupt Invoked from inetd Keyboard stop Kill Profiling alarm clock Quit Run '%s --help' to see a full list of available command line options.
 Segmentation violation Stop Termination Trace trap Urgent condition on socket User defined signal 1 User defined signal 2 Virtual alarm clock Window size change read %d byte read %d bytes read %lu byte of data read %lu bytes of data read data size wrote %d byte wrote %d bytes Project-Id-Version: libgtop trunk
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-03 23:17+0000
PO-Revision-Date: 2009-06-30 04:01+0000
Last-Translator: Nickolay V. Shmyrev <nshmyrev@yandex.ru>
Language-Team: Russian <gnome-cyr@gnome.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 17:00+0000
X-Generator: Launchpad (build 18115)
 Прекратить Таймер Фоновое чтение из tty Фоновая запись в tty Неверный аргумент в системном вызове Нарушенный канал Ошибка шины Превышен предел ЦПУ Состояние потомка было изменено Продолжить Не разветвлять в фоновый режим Ошибка EMT Включить отладку Включить подробный вывод Превышен предел размера файла Исключение плавающей точки Разорвать Сейчас возможен ввод/вывод Недопустимая инструкция Запрос информации Прервать Вызван из inetd Останов клавиатуры Убить Профилированный таймер Выйти Используйте "%s --help", чтобы увидеть полный список допустимых параметров командной строки.
 Нарушение сегментации Остановить Завершение Захват трассировки Требующие внимания условия сокета Сигнал пользователя 1 Сигнал пользователя 2 Виртуальный таймер Изменение размера окна прочитан %d байт прочитано %d байта прочитано %d байтов прочитан %lu байт данных прочитано %lu байта данных прочитано %lu байт данных размер прочитанных данных записан %d байт записано %d байта записано %d байт 