��          �   %   �      P  &   Q     x     �     �  (   �  (   �                0  %   H     n     ~     �     �     �     �     �     �     �  &   
     1  R   8  #   �     �  K   �  �    A   �     �       ?   (  K   h  E   �     �  8     "   @  7   c  #   �     �     �  2   �  "   	     )	     F	     [	  #   {	  ^   �	     �	  �   
  C   �
  K   �
  v   5                                                           
              	                                                    Adding LTSP network to Network Manager Configuring DNSmasq Configuring LTSP Creating the guest users Enabling LTSP network in Network Manager Extracting thin client kernel and initrd Failed Installing the required packages LTSP-Live configuration LTSP-Live should now be ready to use! Network devices None Ready Restarting Network Manager Restarting openbsd-inetd Start LTSP-Live Starting DNSmasq Starting NBD server Starting OpenSSH server Starts an LTSP server from the live CD Status The selected network interface is already in use.
Are you sure you want to use it? Unable to configure Network Manager Unable to parse config Welcome to LTSP Live.
Please choose a network interface below and click OK. Project-Id-Version: ltsp
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-07-18 13:50-0400
PO-Revision-Date: 2015-07-20 15:23+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:19+0000
X-Generator: Launchpad (build 18115)
Language: ru
 Добавление LTSP сети в диспетчер сети Настройка DNSmasq Настройка LTSP Создание гостевых учётных записей Задействование LTSP сети в диспетчере сети Извлечение ядра тонкого клиента и initrd Ошибка Установка необходимых пакетов Конфигурация LTSP-Live LTSP-Live готов для использования! Сетевые устройства Нет Готово Перезапуск диспетчера сети Перезапуск openbsd-inetd Запустить LTSP-Live Запуск DNSmasq Запуск сервера NBD Запуск сервера OpenSSH Запустить LTSP-сервер с интерактивного компакт-диска Состояние Выбранный сетевой интерфейс уже используется.
Вы точно хотите использовать его? Невозможно настроить диспетчер сети Невозможно обработать файл конфигурации Добро пожаловать в LTSP Live.
Выберите сетевой интерфейс и нажмите OK. 