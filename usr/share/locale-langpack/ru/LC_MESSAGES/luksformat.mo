��          t      �              +   1  #   ]  6   �     �     �  0   �  /   $  )   T  x   ~  �  �  ?   �  o   �  a   D  n   �  <     K   R  L   �  A   �  l   -  �   �                                 
      	                     Could not create LUKS device %s Could not format device with file system %s Creating encrypted device on %s...
 Error: could not generate temporary mapped device name Error: device mounted: %s
 Error: invalid file system: %s
 Please enter your passphrase again to verify it
 The passphrases you entered were not identical
 This program needs to be started as root
 luksformat - Create and format an encrypted LUKS device
Usage: luksformat [-t <file system>] <device> [ mkfs options ]

 Project-Id-Version: cryptsetup
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-03 20:26+0100
PO-Revision-Date: 2012-12-29 20:07+0000
Last-Translator: dr&mx <dronmax@gmail.com>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:25+0000
X-Generator: Launchpad (build 18115)
 Не удалось создать устройство LUKS %s Не удалось отформатировать устройство с файловой системой %s Создание устройства использующего шифрование на %s...
 Ошибка: не удалось сгенерировать имя временного  устройства Ошибка: устройство подключено: %s
 Ошибка: недопустимая файловая система: %s
 Введите пароль повторно для его проверки
 Введённые вами пароли не совпадают
 Эту программу нужно запускать от имени администратора (root)
 luksformat - создание и форматирование зашифрованного LUKS устройства
Использование: luksformat [-t <файловая система>] <устройство> [ параметры mkfs ]

 