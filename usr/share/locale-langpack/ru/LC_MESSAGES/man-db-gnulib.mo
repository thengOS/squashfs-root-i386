��    7      �  I   �      �     �     �  .   �  .   �  %   #     I     a  ,   }  ,   �  ,   �  '     -   ,      Z  (   {  (   �     �     �  "     4   0  3   e     �     �     �     �       $        C     U  s   p     �     �     �     	  #   "	     F	     a	     u	     z	     �	  6   �	     �	     �	     �	     
     
     $
  -   +
     Y
     t
  $   �
     �
     �
     �
  *   �
  �    
   �     �  h   �  D   V  ;   �  3   �  8     N   D  \   �  N   �  K   ?  P   �  :   �  L     L   d  ;   �  ;   �  E   )  d   o  D   �       .   9  5   h  0   �  5   �  ]     .   c  8   �  �   �  #   �     �     �  S     ]   X  E   �  9   �     6     C  7   R  k   �  6   �     -     J     g     �     �  d   �  P     &   j  e   �  #   �  ;     &   W  S   ~               "   /       2                                  &   6           -   1          $          )   	              %      #      
   3      '         ,   (                  0      +         !          5   .   7      4          *                                or:   [OPTION...] %.*s: ARGP_HELP_FMT parameter must be positive %.*s: ARGP_HELP_FMT parameter requires a value %.*s: Unknown ARGP_HELP_FMT parameter %s: Too many arguments
 %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '%s' is ambiguous; possibilities: %s: option '--%s' doesn't allow an argument
 %s: option '--%s' requires an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option '-W %s' requires an argument
 %s: option requires an argument -- '%c'
 %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 (PROGRAM ERROR) No version known!? (PROGRAM ERROR) Option should have been recognized!? ARGP_HELP_FMT: %s value is less than or equal to %s Garbage in ARGP_HELP_FMT: %s Invalid back reference Invalid character class name Invalid collation character Invalid content of \{\} Invalid preceding regular expression Invalid range end Invalid regular expression Mandatory or optional arguments to long options are also mandatory or optional for any corresponding short options. Memory exhausted NAME No match No previous regular expression Premature end of regular expression Regular expression too big Report bugs to %s.
 SECS Success Trailing backslash Try '%s --help' or '%s --usage' for more information.
 Unknown system error Unmatched ( or \( Unmatched ) or \) Unmatched [ or [^ Unmatched \{ Usage: failed to return to initial working directory give a short usage message give this help list hang for SECS seconds (default 3600) memory exhausted print program version set the program name unable to record current working directory Project-Id-Version: gnulib 3.0.0.6062.a6b16
Report-Msgid-Bugs-To: bug-gnulib@gnu.org
POT-Creation-Date: 2015-11-06 15:42+0000
PO-Revision-Date: 2015-12-03 18:30+0000
Last-Translator: Yuri Kozlov <yuray@komyakino.ru>
Language-Team: Russian <gnu@mx.ru>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:11+0000
X-Generator: Launchpad (build 18115)
Language: ru
   или:   [ПАРАМЕТР...] %.*s: значение параметра ARGP_HELP_FMT должно быть положительным %.*s: параметр ARGP_HELP_FMT требует значения %.*s: неизвестный параметр ARGP_HELP_FMT %s: слишком много аргументов
 %s: неправильный параметр -- «%c»
 %s: для параметра «%c%s» аргумент не разрешён
 %s: двусмысленный параметр «%s»; возможные варианты: %s: для параметра «--%s» аргумент не разрешён
 %s: для параметра «--%s» требуется аргумент
 %s: у параметра «-W %s» не может быть аргумента
 %s: двусмысленный параметр «-W %s»
 %s: для параметра «-W %s» требуется аргумент
 %s: для параметра требуется аргумент -- «%c»
 %s: нераспознанный параметр «%c%s»
 %s: нераспознанный параметр «--%s»
 (ОШИБКА ПРОГРАММЫ) Версия неизвестна!? (ОШИБКА ПРОГРАММЫ) Параметр должен был быть распознан!? ARGP_HELP_FMT: значение %s меньше или равно %s Мусор в ARGP_HELP_FMT: %s Неверная обратная ссылка Неверное имя класса символов Неверный символ сравнения Недопустимое содержимое в \{\} Недопустимое предшествующее регулярное выражение Неверный конец диапазона Неверное регулярное выражение Обязательные или необязательные аргументы к длинным именам параметров остаются таковыми и к соответствующим коротким параметрам. Закончилась память ИМЯ Нет совпадений Отсутствует предыдущее регулярное выражение Преждевременное завершение регулярного выражения Слишком большое регулярное выражение Об ошибках сообщай по адресу %s.
 СЕКУНД Успешно Конечная обратная косая черта Наберите '%s --help' или '%s --usage' для дополнительной информации.
 Неизвестная системная ошибка Непарная ( или \( Непарная ) или \) Непарная [ или [^ Непарная \{ Использование: не удалось вернуться в первоначальный рабочий каталог показать короткую справку по использованию показать эту справку остановиться на заданное число СЕКУНД (по умолчанию 3600) закончилась память показать номер версии программы задать имя программы не удалось запомнить текущий рабочий каталог 