��    (      \  5   �      p  /   q  &   �  %   �  #   �  %     
   8  	   C     M     V     g  C   n     �     �  
   �     �  
   �     �     �     	          3     <     P     \     n     t     z     �     �     �  
   �     �     �     �                    +     1  �  ?  x   �  /   N  /   ~  )   �  1   �     
	  	    	  
   *	  )   5	     _	  �   n	     
  
   6
     A
     R
     p
     �
  0   �
     �
  5   �
       *   '     R  &   k  
   �     �  1   �  8   �            $   6  @   [  =   �  $   �     �           =  
   X     c        "         '                     #          	                                    (            $   &                     !                       
                   %              * You should restart nabi to apply above option <span weight="bold">Connected</span>:  <span weight="bold">Encoding</span>:  <span weight="bold">Locale</span>:  <span weight="bold">XIM name</span>:  About Nabi BackSpace Choseong English keyboard Hangul Hangul input method: Nabi - You can input hangul using this program Hangul keyboard Hanja Hanja Font Hanja Options Hanja keys Keyboard Keypress Statistics Nabi Preferences Nabi keypress statistics Nabi: %s Nabi: error message Orientation Select hanja font Shift Space Start in hangul mode The orientation of the tray. Total Tray Tray icons Use simplified chinese Use tray icon XIM Server is not running _Hide palette _Show palette hangul(hanja) hanja hanja(hangul) Project-Id-Version: nabi
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2013-01-26 17:31+0900
PO-Revision-Date: 2012-09-24 07:40+0000
Last-Translator: Eugene Marshal <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:12+0000
X-Generator: Launchpad (build 18115)
 * Нужно перезапустить nabi для применения вышеуказанных параметров <span weight="bold">Соединено</span>:  <span weight="bold">Кодировка</span>:  <span weight="bold">Локаль</span>:  <span weight="bold">XIM название</span>:  О программе BackSpace Чосон Английская клавиатура Хангыль Метод ввода хангыль: Nabi - с помощью этой программы можно вводить символы письменности хангыль Клавиатура Hangul Ханча Шрифт Hanja Параметры ханча Клавиши ханча Клавиатура Статисктка нажатий клавиш Параметры Nabi Статистика нажатия клавиш Nabi Nabi: %s Nabi: сообщение об ошибке Расположение Выберите шрифт ханча Сдвиг Пробел Запустить в режиме хангыль Положение области уведомления Итог Панель задач Значки панели задач Использовать упрощённый китайский Использовать значок панели задач Сервер XIM не запущен _Скрыть палитру _Показать палитру хангыль(ханча) ханча ханча(хангыль) 