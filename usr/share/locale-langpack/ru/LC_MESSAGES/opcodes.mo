��    *      l  ;   �      �     �     �     �     �          "     4     S     o  "   �     �     �     �     �     �       7   *     b     }     �  "   �     �      �                ;  !   P     r     �  2   �     �     �     �     
     #  -   4  ;   b  	   �     �     �     �  �  �  6   �	     �	  '   �	  >   
  *   F
  (   q
  3   �
  :   �
  )   	  B   3  '   v  )   �  ,   �  ,   �  ,   "  ,   O  }   |  0   �  L   +  '   x  �   �  /   (  T   X  -   �  0   �  ?     C   L  *   �  8   �  b   �  1   W      �     �  !   �      �  T     o   W     �     �     �  1        &   (      )                     '              $                                              
          *                                          "              #   %   !                 	              *unknown operands type: %d* <function code %d> <illegal instruction> <internal disassembler error> <unknown register %d> Bad register name Biiiig Trouble in parse_imm16! Internal disassembler error Label conflicts with `Rx' Label conflicts with register name Missing '#' prefix Missing '.' prefix Missing 'pag:' prefix Missing 'pof:' prefix Missing 'seg:' prefix Missing 'sof:' prefix Operand out of range. Must be between -32768 and 32767. Register list is not valid Syntax error: No trailing ')' Unknown error %d
 attempt to read writeonly register attempt to set HR bits attempt to set readonly register bad instruction `%.50s' bad instruction `%.50s...' bad jump flags value class %s is defined but not used
 illegal bitmask illegal use of parentheses invalid operand.  type may have values 0,1,2 only. invalid register number `%d' ld operand error missing `]' not a valid r0l/r0h pair st operand error syntax error (expected char `%c', found `%c') syntax error (expected char `%c', found end of instruction) undefined unknown	0x%02lx unknown	0x%04lx unrecognized instruction Project-Id-Version: binutils
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-02-10 09:42+1030
PO-Revision-Date: 2012-09-18 07:15+0000
Last-Translator: Игорь DesT Осколков <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:04+0000
X-Generator: Launchpad (build 18115)
 *неизвестный тип операндов: %d* <код функции %d> <неверная инструкция> <внутренняя ошибка дизассемблера> <неизвестный регистр %d> Неверное имя регистра Бооольшие проблемы в parse_imm16! Внутрення ошибка дизассемблера Метка конфликтует с `Rx' Метка конфликтует с именем регистра Префикс '#' отсутсвует Префикс '.' отсутствует Префикс 'pag:' отсутствует Префикс 'pof:' отсутствует Префикс 'seg:' отсутствует Префикс 'sof:' отсутствует Операнд вышел за границы диапазона. Допустимы значения от -32768 до 32767. Неверный список регистров Синтаксическая ошибка: нет закрывающей ')' Неизвестная ошибка %d
 Попытка прочитать данные из регистра, предназначенного только для записи попытка установить HR биты Попытка записи в защищённый от записи регистр неверная инструкция `%.50s' неверная инструкция `%.50s...' неверное значение флагов перехода класс %s определён, но не использован
 неверная битовая маска неверное использование скобок неверный операнд. тип может иметь значения только 0,1,2. Неверный номер регистра `%d' ошибка операнда ld пропущен `]' неверная пара r0l/r0h ошибка операнда st синтаксическая ошибка (ожидался `%c', найден `%c') синтаксическая ошибка (ожидался `%c', найден конец инструкции) неопределено неизвестное	0x%02lx неизвестное	0x%04lx неопределённая инструкция 