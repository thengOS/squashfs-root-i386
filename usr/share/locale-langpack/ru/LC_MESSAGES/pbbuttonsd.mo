��    7      �  I   �      �  g   �       *   2  &   ]  /   �  B   �     �       !     =   8  )   v     �  '   �     �             !   8     Z     t  9   z  %   �     �     �       8        S     g     w  1   �  -   �     �  ,   
	  =   7	     u	     �	     �	     �	  $   �	     �	  ?   
     Q
     Y
     q
     y
     �
     �
     �
     �
     �
  #   �
  
        '     :     Z  �  f  |        �  R   �  N   �  K   @  �   �       %   2  ;   X  �   �  c   &  <   �  f   �  D   .  D   s  8   �  [   �  G   M     �  i   �  I     "   V  5   y     �     �  #   M     q  1   �  \   �  b     "   }  ]   �  q   �     p  -   �  )   �  -   �  <     6   R  �   �       /   2     b  C   s  )   �     �  9   �     5  0   Q  Z   �     �     �  J     !   ^                  4             
             !   *                             $   #       6                    '   3             5       %       .   	      /       1             +   &   0   "              -          )                       ,   2   7   (             PID   | Name                 | Port    | Flags
  ------+----------------------+---------+-----------
 %-14s - eject the CDROM
 %-14s - prints a list of clients attached
 %-14s - reconfigure server parameters
 %-14s - save the current configuration to disk
 %s - daemon to support special features of laptops and notebooks.
 %s, version %s Buffer overflow Can't access i2c device %s - %s.
 Can't find pmud on port 879. Trigger PMU directly for sleep.
 Can't get volume of master channel [%s].
 Can't load card '%s': %s
 Can't open %s. Eject CDROM won't work.
 Can't open ADB device %s: %s
 Can't open PMU device %s: %s
 Can't open card '%s': %s
 Can't open mixer device [%s]. %s
 Can't register mixer: %s
 ERROR ERROR: Have problems reading configuration file [%s]: %s
 ERROR: Not enough memory for buffer.
 File doesn't exist Memory allocation failed.
 Permission denied Please check your CDROM settings in configuration file.
 Registration failed Script '%s' %s
 Script '%s' doesn't exist.
 Script '%s' lauched but killed after %d seconds.
 Script '%s' skipped because it's not secure.
 Server already running Server didn't send an answer and timed out.
 Server is already running. Sorry, only one instance allowed.
 Server not found Supported commands:
 Supported tags:
 The object '%s' doesn't exist.
 The object '%s' is not a directory.
 The object '%s' is not a file.
 Too many formatsigns. Max three %%s allowed in TAG_SCRIPTPMCS.
 Usage:
 Usage: %s [OPTION]... 
 WARNING WARNING: tag %s not supported.
 argument invalid doesn't exist failed - unknown error code format error function not supported lauched but killed after %d seconds open error pbbcmd, version %s skipped because it's not secure unknown tag Project-Id-Version: pbbuttonsd
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2007-07-07 19:36+0200
PO-Revision-Date: 2013-03-28 05:37+0000
Last-Translator: Aleksey Kabanov <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:35+0000
X-Generator: Launchpad (build 18115)
   PID   | Название                 | Порт    | Флаги
  ------+----------------------+---------+-----------
 %-14s - извлечь CDROM
 %-14s - выводит список присоединённых клиентов
 %-14s - настроить параметры сервера повторно
 %-14s - сохранить текущие настройки на диск
 %s - демон для поддержки специфических особенностей портативных ПК и ноутбуков.
 %s, версия %s Переполнение буфера Нет доступа к устройству i2c %s - %s.
 Не обнаружен демон pmud на порту 879. Переключаю PMU непосредственно в спящий режим.
 Невозможно установить громкость основного канала [%s].
 Невозможно загрузить карту '%s': %s
 Не удалось открыть %s. Выброс лотка CDROM работать не будет
 Невозможно открыть устройство ADB %s: %s
 Невозможно открыть устройство PMU %s: %s
 Невозможно открыть карту '%s': %s
 Невозможно открыть устройство микширования [%s]. %s
 Невозможно зарегистрировать микшер: %s
 ОШИБКА ОШИБКА: Проблемы с чтением конфигурационного файла  [%s]: %s
 ОШИБКА: Недостаточно памяти для буфера.
 Файл не существует Выделение памяти не удалось.
 Доступ запрещён Пожалуйста, проверьте настройки Вашего CDROM в конфигурационном файле.
 Ошибка регистрации Сценарий '%s' %s
 Сценарий '%s' не существует.
 Сценарий '%s' был запущен и завершён через %d секунд.
 Сценарий '%s' пропущен, так как не является безопасным.
 Сервер уже запущен Сервер не отправил ответ и время ожидания истекло.
 Сервер уже запущен. Только один экземпляр разрешён, извините.
 Сервер не найден Поддерживаемые команды:
 Поддерживаемые метки:
 Объект '%s' не существует.
 Объект '%s' не является каталогом.
 Объект '%s' не является файлом.
 Слишком много символов форматирования. Разрешено максимум три %%s в TAG_SCRIPTPMCS.
 Использование:
 Использование: %s [ОПЦИИ].. 
 ВНИМАНИЕ ВНИМАНИЕ: метка %s не поддерживается.
 недопустимый аргумент не существует ошибка - неизвестный код ошибки ошибка формата функция не поддерживается запущен, но в последствии завершён через %d секунд ошибка открытия pbbcmd, версия %s пропущен, так как не является безопасным неизвестная метка 