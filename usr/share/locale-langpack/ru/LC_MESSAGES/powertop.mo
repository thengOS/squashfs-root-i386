��    r      �  �   <      �	  $   �	     �	     �	  	   �	     �	     �	     	
     
     
     +
  "   C
  -   f
  !   �
     �
     �
     �
     �
     �
     �
            %   4     Z     t     �     �     �     �     �     �     �     �  #   �  ?        X     a     f  U   �  2   �     	       	         *     :  
   ?     J     a     w     �     �     �     �     �     �          .  $   7     \      k     �     �     �  	   �  =   �       /   &     V     v     ~     �     �     �     �     �     �     �  #        2     :  *   M     x     �  ,   �  )   �  5   �  I        `     w     �     �     �     �      �  	   �  &   �  
     .     
   N  3   Y     �     �     �     �     �     �  !   �           5     O  !   c     �  .   �     �  z  �  <   U     �  
   �     �  
   �     �     �     �           !  E   B  a   �  F   �     1  	   8  ,   B     o  ,   x  (   �  ,   �  2   �  L   .  =   {  @   �  7   �  3   2     f     y     �     �     �  )   �  V   �  y   H     �  
   �  6   �  �     q   �  '   3     [     c     r     �  +   �  <   �  /   �  0         Q     g  B   �  7   �  *         +  @   J  
   �  N   �     �  F     
   J  4   U  m   �  	   �  \         _   _   u   @   �      !  1   %!  P   W!  "   �!     �!     �!     �!     "  E   &"  Q   l"     �"  &   �"  X   �"     K#  	   \#  8   f#  D   �#  g   �#  �   L$  ;   �$      %  !   =%     _%     v%  1   �%  7   �%     �%  `   &  
   w&  R   �&     �&  e   �&     M'     _'     t'  X   �'      �'  0   �'  1   .(  3   `(  5   �(  3   �(  >   �(  %   =)  _   c)     �)                Z               D   G   c   l   ?   K       *   V   H              4   Q       M               )      	   9   i   2   g   k   0   Y   !   J   ^   %   F           5              E   -   X   $                                \          ;          :              <       C          #           L       B             &          '   "   U             e   _          d       p       3          P   
   n   N   7   @   >      O   8   S               r      R          f   1   [      =      6          m   o   `   b      ]   a      W      T   h       /          A   (   ,   .   q   j   I   +                  Usage     Device name
     0 mW   Core   Package  CPU %i .... device %s 
 Actual Adapter Audio codec %s: %s Audio codec %s: %s (%s) Autosuspend for USB device %s [%s] Autosuspend for unknown USB device %s (%s:%s) Bluetooth device interface status CPU CPU %d CPU Information CPU:  Calibrating USB devices
 Calibrating backlight
 Calibrating idle
 Calibrating radio devices
 Calibrating: CPU usage on %i threads
 Calibrating: disk usage 
 Cannot create temp file
 Cannot load from file Cannot save to file Category Core %d Description Device Device Name Device stats Enable Audio codec power management Estimated power: %5.1f    Measured power: %5.1f    Sum: %5.1f

 Events/s Exit Failed to mount debugfs!
 File will be loaded after taking minimum number of measurement(s) with battery only 
 For more help please refer to the 'man 8 powertop' Frequency stats GPU %d GPU ops/s GPU ops/seconds GPU: Idle stats Intel built in USB hub Invalid CSV filename
 Invalid HTML filename
 Kernel Version Leaving PowerTOP Loaded %i prior measurements
 Measuring workload %s.
 Network interface: %s (%s) OS Information Optimal Tuned Software Settings Overview Overview of Software Power Consumers PCI Device: %s PS/2 Touchpad / Keyboard / Mouse Package Parameters after calibration:
 Power Aware CPU scheduler PowerTOP  PowerTOP %s needs the kernel to support the 'perf' subsystem
 PowerTOP Version PowerTOP is out of memory. PowerTOP is Aborting Preparing to take measurements
 Process Processor Frequency Report Processor Idle State Report Radio device: %s SATA controller SATA disk: %s SATA link: %s Script Set refresh time out Software Settings in Need of Tuning Summary System Information System baseline power is estimated at %sW
 System:  Target: The battery reports a discharge rate of %sW
 The battery reports a discharge rate of:  The estimated remaining time is %i hours, %i minutes
 Too many open files, please increase the limit of open file descriptors.
 Top 10 Power Consumers USB device: %s USB device: %s (%s) Unknown Usage Usage: powertop [OPTIONS] Wake-on-lan status for device %s Wakeups/s Wireless Power Saving for interface %s [=devnode] [=iterations] number of times to run each test [=seconds] as well as support for trace points in the kernel:
 cpu package cpu package %i exiting...
 file to execute for workload generate a csv report generate a html report generate a report for 'x' seconds print this help menu print version information run in "debug" mode runs powertop in calibration mode suppress stderr output uses an Extech Power Analyzer for measurements wakeups/second Project-Id-Version: ru
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-11-05 16:49-0800
PO-Revision-Date: 2016-06-09 11:36+0000
Last-Translator: Eugene Marshal <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:21+0000
X-Generator: Launchpad (build 18115)
               Используемое устройство
     0 мВт   Ядро   Пакет  ЦПУ %i .... устройство %s 
 Фактически Адаптер Аудиокодек %s: %s Аудиокодек %s: %s (%s) Автоприостановление устройств USB %s [%s] Автоприостановление неопознанных устройств USB %s (%s:%s) Состояние инетрфейса Bluetooth устройства ЦПУ ЦПУ %d Информация о процессоре ЦПУ:  Калибровка устройств USB
 Калибровка подсветки
 Бездействие калибровки
 Калибровка радиоустройств
 Калибровка: использование CPU на %i потоках
 Калибровка: использование диска 
 Невозможно создать временный файл
 Невозможно загрузить из файла Невозможно сохранить в файл Категория Ядро %d Описание Устройство Имя устройства Статистика устройства Включить управление питанием звукового кодека Расчетная мощность: %5.1f    Измеренная мощность: %5.1f    Суммарная: %5.1f

 Событий/с Выйти Не удалось смонтировать debugfs!
 Файл будет загружен после выполнения минимального количества измерений (только с батареей) 
 Для получения дополнительных сведений, обратитесь к 'man 8 powertop' Частотная статистика ГП %d ГП оп./с. ГП оп./с. ГП: Статистика бездействия Встроенный USB-концентратор от Intel Неправильное имя файла CSV
 Неправильное имя файла HTML
 Версия ядра Завершение PowerTOP Загружены %i предварительные замеры
 Измерение рабочей нагрузки %s.
 Сетевой интерфейс: %s (%s) Информация об ОС Оптимальные программные настройки Обзор Сводка о программных потребителях энергии Устройство PCI : %s PS/2 сенсорная панель / клавиатура / мышь Пакет Параметры после калибровки:
 Планировщик энергопредупреждений Центрального процессора PowerTOP  PowerTOP %s нуждается в поддержке ядром подсистемы 'perf'
 Версия PowerTOP PowerTOP не хватает памяти. Прерывается выполнение PowerTOP Подготовка к проведению измерений
 Процесс Отчет о частоте процессора Отчет состояния времени простоя процессора Радиоустройство: %s Контроллер SATA Диск SATA: %s Соединение SATA: %s Сценарий Установить время ожидания обновления Программные настройки требующие подстройки Сводка Информация о системе Основноей системной энергии осталось около %sВт
 Система:  Цель: Разряд батареи составляет %sВт
 Батарея сообщает о скорости разряда:  Предполагаемое оставшееся время работы %i часов, %i минут
 Открыто слишком много файлов. Увеличьте ограничение дескрипторов открытия файла.
 10 основных потребителей энергии USB устройство: %s USB устройство: %s (%s) Неизвестное Использование Использование: powertop [ОПЦИИ] Статус wake-on-lan для устройства %s Пробуждений/с. Экономия электроэнергии беспроводного интерфейса %s [=devnode] [=итерации] сколько раз запускать каждый тест [=секунды] как и поддержка отметок отслеживания для ядра системы:
 пакет ЦПУ пакет ЦПУ %i выход...
 файл для исполнения в качестве рабочей нагрузки создать отчёт в csv создать отчёт в формате HTML создать отчёт для 'x' секунд вывести это справочное меню показать информацию о версии запустить в режиме "отладки" запутить powertop в режиме калибровки подавить вывод в stderr использовать для измерений анализатор можности Extech пробуждений/с. 