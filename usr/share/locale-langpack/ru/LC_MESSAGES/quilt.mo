��    �      �  �   �	        �     *    r   9    �  Z   �    '  �  �  _  �  }  �  �   f  �   Z  @  �  �   )   m   �   r   B!  \   �!  �  "  j  �#  �   I%  [   C&  `   �&  N    '  \   O'  �  �'    �,  �   �1  p  V2    �4  @   �5  '  6    87     T8     k8  %   ~8  *   �8     �8     �8  B   9  E   D9  L   �9     �9     �9  $   �9  !   :     @:     X:  .   n:  =   �:  0   �:     ;  -   (;     V;     r;  ,   �;  !   �;  (   �;     <     "<     A<     ]<     s<      �<     �<     �<     �<     �<     =  #   <=  -   `=      �=  %   �=  #   �=  0   �=  H   *>  .   s>     �>     �>     �>     �>     �>  =   ?  C   @?  (   �?  '   �?  +   �?  .   @  B   0@  9   s@  3   �@     �@  #   �@     A     =A     SA     sA      �A     �A     �A     �A  '   �A  &   #B  #   JB     nB     �B     �B     �B     �B  0   �B  1   C     =C  #   TC     xC  %   �C  #   �C  <   �C  &   D  O   DD  �   �D  3   E  X   �E  /   F     <F  @   SF  (   �F     �F     �F  "   �F  �   G     �G  =   �G  $   *H     OH  U   nH  *   �H  d   �H  <   TI  g   �I     �I     J     4J  '   RJ     zJ  9   �J  �   �J  +   ]K  )   �K     �K  @   �K     L     ,L      ?L     `L  .   wL  /   �L  �  �L  �  �N  *  hP  �   �U  h  ?V  �   �Y    LZ  �  g^  >  dd  �  �f  �  mi    bk    el  5  �n  �   �o  �   cp  �   q  �  �q  |  �t  �  w  �   �x  �   ky  {   �y  �   mz  �  �z  �  ��  _  ��  P  U�  �  ��  �   G�     �  �  �     �  &   ��  A   �  b   a�  "   Ę  K   �  c   3�  `   ��  }   ��     v�  2   ��  F   ��  >    �  C   ?�  .   ��  ^   ��  p   �  T   ��  P   ל  ^   (�  0   ��  <   ��  E   ��  T   ;�  C   ��  2   Ԟ  <   �  /   D�     t�  "   ��  %   ��  '   ֟  5   ��  4   4�  ,   i�  -   ��  4   Ġ  n   ��  ?   h�  X   ��  9   �  V   ;�  �   ��  O   P�  4   ��  2   գ  ,   �  &   5�     \�  q   z�  q   �  6   ^�  4   ��  D   ʥ  M   �  �   ]�  O   �  M   :�  )   ��  k   ��  %   �     D�     c�  #   ��  2   ��      ٨  )   ��  !   $�  9   F�  E   ��  4   Ʃ  ,   ��      (�     I�     h�      ��  N   ��  L   �  *   @�  B   k�  1   ��  6   �  7   �  �   O�  -   Ҭ  f    �  �  g�  ]   G�  �   ��  F   C�  &   ��  T   ��  <   �  /   C�  .   s�  2   ��  �   ձ  -   ��  P   �  $   1�  4   V�  q   ��  G   ��  t   E�  P   ��  t   �  3   ��  +   ��  .   �  >   �  /   N�  P   ~�  �   ϶  ?   h�  B   ��  '   �  c   �  )   w�     ��  0   ��  #   �  W   �  U   n�     v           �          \   =              �           Q   I   c          �   �   y   G          ~   w      �      p           h   �      #   a          �   L           F       �   /   n   m             �   0              d      l   ?      4   9   ^       o          E   3   �   [   i   �   _   D   �          H   Y   b       r   J   e             >   �       W   8          .   f       2   !           }   6       ;   Z   �       O   j   `   (       )   
   U   s       �   x   �           �   A          K   5       +          �   &   �   	   <   7           :   @                      $      %   g           1   k       �   "   �   �   X          �   P   u   t   -   N       ,   �      R          T           �   �      �   ]   z   C   |   M   *       '               S   �   {      q       B   V              
Add one or more files to the topmost or named patch.  Files must be
added to the patch before being modified.  Files that are modified by
patches already applied on top of the specified patch cannot be added.

-p patch
	Patch to add files to.
 
Apply patch(es) from the series file.  Without options, the next patch
in the series file is applied.  When a number is specified, apply the
specified number of patches.  When a patch name is specified, apply
all patches up to and including the specified patch.  Patch names may
include the patches/ prefix, which means that filename completion can
be used.

-a	Apply all patches in the series file.

-f	Force apply, even if the patch has rejects.

-q	Quiet operation.

-v	Verbose operation.

--leave-rejects
	Leave around the reject files patch produced, even if the patch
	is not actually applied.

--interactive
	Allow the patch utility to ask how to deal with conflicts. If
	this option is not given, the -f option will be passed to the 
	patch program.

--color[=always|auto|never]
	Use syntax coloring.
 
Create a new patch with the specified file name, and insert it after the
topmost patch in the patch series file.
 
Create mail messages from all patches in the series file, and either store
them in a mailbox file, or send them immediately. The editor is opened
with a template for the introductory message. Please see the file
%s for details.

--mbox file
	Store all messages in the specified file in mbox format. The mbox
	can later be sent using formail, for example.

--send
	Send the messages directly using %s.

--from, --subject
	The values for the From and Subject headers to use.

--to, --cc, --bcc
	Append a recipient to the To, Cc, or Bcc header.
 
Edit the specified file(s) in \$EDITOR (%s) after adding it (them) to
the topmost patch.
 
Fork the topmost patch.  Forking a patch means creating a verbatim copy
of it under a new name, and use that new name instead of the original
one in the current series.  This is useful when a patch has to be
modified, but the original version of it should be preserved, e.g.
because it is used in another series, or for the history.  A typical
sequence of commands would be: fork, edit, refresh.

If new_name is missing, the name of the forked patch will be the current
patch name, followed by \"-2\".  If the patch name already ends in a
dash-and-number, the number is further incremented (e.g., patch.diff,
patch-2.diff, patch-3.diff).
 
Generate a dot(1) directed graph showing the dependencies between
applied patches. A patch depends on another patch if both touch the same
file or, with the --lines option, if their modifications overlap. Unless
otherwise specified, the graph includes all patches that the topmost
patch depends on.
When a patch name is specified, instead of the topmost patch, create a
graph for the specified patch. The graph will include all other patches
that this patch depends on, as well as all patches that depend on this
patch.

--all	Generate a graph including all applied patches and their
	dependencies. (Unapplied patches are not included.)

--reduce
	Eliminate transitive edges from the graph.

--lines[=num]
	Compute dependencies by looking at the lines the patches modify.
	Unless a different num is specified, two lines of context are
	included.

--edge-labels=files
	Label graph edges with the file names that the adjacent patches
	modify.

-T ps	Directly produce a PostScript output file.
 
Global options:

--trace
	Runs the command in bash trace mode (-x). For internal debugging.

--quiltrc file
	Use the specified configuration file instead of ~/.quiltrc (or
	/etc/quilt.quiltrc if ~/.quiltrc does not exist).  See the pdf
	documentation for details about its possible contents.

--version
	Print the version number and exit immediately. 
Grep through the source files, recursively, skipping patches and quilt
meta-information. If no filename argument is given, the whole source
tree is searched. Please see the grep(1) manual page for options.

-h	Print this help. The grep -h option can be passed after a
	double-dash (--). Search expressions that start with a dash
	can be passed after a second double-dash (-- --).
 
Import external patches.

-p num
	Number of directory levels to strip when applying (default=1)

-n patch
	Patch filename to use inside quilt. This option can only be
	used when importing a single patch.

-f	Overwite/update existing patches.
 
Initializes a source tree from an rpm spec file or a quilt series file.

-d	optional path prefix (sub-directory).

-v	verbose debug output.
 
Integrate the patch read from standard input into the topmost patch:
After making sure that all files modified are part of the topmost
patch, the patch is applied with the specified strip level (which
defaults to 1).

-p strip-level
	The number of pathname components to strip from file names
	when applying patchfile.
 
Please remove all patches using \`quilt pop -a' from the quilt version used to create this working tree, or remove the %s directory and apply the patches from scratch.\n 
Print a list of applied patches, or all patches up to and including the
specified patch in the file series.
 
Print a list of patches that are not applied, or all patches that follow
the specified patch in the series file.
 
Print an annotated listing of the specified file showing which
patches modify which lines.
 
Print or change the header of the topmost or specified patch.

-a, -r, -e
	Append to (-a) or replace (-r) the exiting patch header, or
	edit (-e) the header in \$EDITOR (%s). If none of these options is
	given, print the patch header.
	
--strip-diffstat
	Strip diffstat output from the header.

--strip-trailing-whitespace
	Strip trailing whitespace at the end of lines of the header.

--backup
	Create a backup copy of the old version of a patch as patch~.
 
Print the list of files that the topmost or specified patch changes.

-a	List all files in all applied patches.

-l	Add patch name to output.

-v	Verbose, more user friendly output.

--combine patch
	Create a listing for all patches between this patch and
	the topmost applied patch. A patch name of \"-\" is equivalent
	to specifying the first applied patch.

 
Print the list of patches that modify the specified file. (Uses a
heuristic to determine which files are modified by unapplied patches.
Note that this heuristic is much slower than scanning applied patches.)

-v	Verbose, more user friendly output.
 
Print the name of the next patch after the specified or topmost patch in
the series file.
 
Print the name of the previous patch before the specified or topmost
patch in the series file.
 
Print the name of the topmost patch on the current stack of applied
patches.
 
Print the names of all patches in the series file.

-v	Verbose, more user friendly output.
 
Produces a diff of the specified file(s) in the topmost or specified
patch.  If no files are specified, all files that are modified are
included.

-p n	Create a -p n style patch (-p0 or -p1 are supported).

-u, -U num, -c, -C num
	Create a unified diff (-u, -U) with num lines of context. Create
	a context diff (-c, -C) with num lines of context. The number of
	context lines defaults to 3.

--no-timestamps
	Do not include file timestamps in patch headers.

--no-index
	Do not output Index: lines.

-z	Write to standard output the changes that have been made
	relative to the topmost or specified patch.

-R	Create a reverse diff.

-P patch
	Create a diff for the specified patch.  (Defaults to the topmost
	patch.)

--combine patch
	Create a combined diff for all patches between this patch and
	the patch specified with -P. A patch name of \"-\" is equivalent
	to specifying the first applied patch.

--snapshot
	Diff against snapshot (see \`quilt snapshot -h').

--diff=utility
	Use the specified utility for generating the diff. The utility
	is invoked with the original and new file name as arguments.

--color[=always|auto|never]
	Use syntax coloring.

--sort	Sort files by their name instead of preserving the original order.
 
Refreshes the specified patch, or the topmost patch by default.
Documentation that comes before the actual patch in the patch file is
retained.

It is possible to refresh patches that are not on top.  If any patches
on top of the patch to refresh modify the same files, the script aborts
by default.  Patches can still be refreshed with -f.  In that case this
script will print a warning for each shadowed file, changes by more
recent patches will be ignored, and only changes in files that have not
been modified by any more recent patches will end up in the specified
patch.

-p n	Create a -p n style patch (-p0 or -p1 supported).

-u, -U num, -c, -C num
	Create a unified diff (-u, -U) with num lines of context. Create
	a context diff (-c, -C) with num lines of context. The number of
	context lines defaults to 3.

--no-timestamps
	Do not include file timestamps in patch headers.
	
--no-index
	Do not output Index: lines.

--diffstat
	Add a diffstat section to the patch header, or replace the
	existing diffstat section.

-f	Enforce refreshing of a patch that is not on top.

--backup
	Create a backup copy of the old version of a patch as patch~.

--sort	Sort files by their name instead of preserving the original order.

--strip-trailing-whitespace
	Strip trailing whitespace at the end of lines.
 
Remove one or more files from the topmost or named patch.  Files that
are modified by patches on top of the specified patch cannot be removed.

-p patch
	Patch to remove files from.
 
Remove patch(es) from the stack of applied patches.  Without options,
the topmost patch is removed.  When a number is specified, remove the
specified number of patches.  When a patch name is specified, remove
patches until the specified patch end up on top of the stack.  Patch
names may include the patches/ prefix, which means that filename
completion can be used.

-a	Remove all applied patches.

-f	Force remove. The state before the patch(es) were applied will
	be restored from backup files.

-R	Always verify if the patch removes cleanly; don't rely on
	timestamp checks.

-q	Quiet operation.

-v	Verbose operation.
 
Remove the specified or topmost patch from the series file.  If the
patch is applied, quilt will attempt to remove it first. (Only the
topmost patch can be removed right now.)

-n	Delete the next patch after topmost, rather than the specified
	or topmost patch.
 
Rename the topmost or named patch.

-p patch
	Patch to rename.
 
Take a snapshot of the current working state.  After taking the snapshot,
the tree can be modified in the usual ways, including pushing and
popping patches.  A diff against the tree at the moment of the
snapshot can be generated with \`quilt diff --snapshot'.

-d	Only remove current snapshot.
 
Upgrade the meta-data in a working tree from an old version of quilt to the
current version. This command is only needed when the quilt meta-data format
has changed, and the working tree still contains old-format meta-data. In that
case, quilt will request to run \`quilt upgrade'.
        quilt --version %s: I'm confused.
 Appended text to header of patch %s\n Applied patch %s (forced; needs refresh)\n Applying patch %s\n Cannot add symbolic link %s\n Cannot diff patches with -p%s, please specify -p0 or -p1 instead\n Cannot refresh patches with -p%s, please specify -p0 or -p1 instead\n Cannot use --strip-trailing-whitespace on a patch that has shadowed files.\n Commands are: Conversion failed\n Converting meta-data to version %s\n Delivery address '%s' is invalid
 Diff failed, aborting\n Directory %s exists\n Display name '%s' contains invalid characters
 Display name '%s' contains non-printable or 8-bit characters
 Display name '%s' contains unpaired parentheses
 Failed to back up file %s\n Failed to copy files to temporary directory\n Failed to create patch %s\n Failed to import patch %s\n Failed to insert patch %s into file series\n Failed to patch temporary files\n Failed to remove file %s from patch %s\n Failed to remove patch %s\n Failed to rename %s to %s: %s
 File %s added to patch %s\n File %s disappeared!
 File %s exists\n File %s is already in patch %s\n File %s is not being modified\n File %s is not in patch %s\n File %s may be corrupted\n File %s modified by patch %s\n File %s removed from patch %s\n File \`%s' is located below \`%s'\n File series fully applied, ends at patch %s\n Fork of patch %s created as %s\n Fork of patch %s to patch %s failed\n Importing patch %s (stored as %s)\n Interrupted by user; patch %s was not applied.\n More recent patches modify files in patch %s. Enforce refresh with -f.\n More recent patches modify files in patch %s\n No next patch\n No patch removed\n No patches applied\n Nothing in patch %s\n Now at patch %s\n Option \`-n' can only be used when importing a single patch\n Options \`-c patch', \`--snapshot', and \`-z' cannot be combined.\n Patch %s appears to be empty, removing\n Patch %s appears to be empty; applied\n Patch %s does not apply (enforce with -f)\n Patch %s does not exist; applied empty patch\n Patch %s does not remove cleanly (refresh it or enforce with -f)\n Patch %s exists already, please choose a different name\n Patch %s exists already, please choose a new name\n Patch %s exists already\n Patch %s exists. Replace with -f.\n Patch %s is already applied\n Patch %s is applied\n Patch %s is currently applied\n Patch %s is not applied\n Patch %s is not in series file\n Patch %s is not in series\n Patch %s is now on top\n Patch %s is unchanged\n Patch %s needs to be refreshed first.\n Patch %s not applied before patch %s\n Patch %s not found in file series\n Patch %s renamed to %s\n Patch is not applied\n Refreshed patch %s\n Removed patch %s\n Removing patch %s\n Removing trailing whitespace from line %s of %s
 Removing trailing whitespace from lines %s of %s
 Renaming %s to %s: %s
 Renaming of patch %s to %s failed\n Replaced header of patch %s\n Replacing patch %s with new version\n SYNOPSIS: %s [-p num] [-n] [patch]
 The %%prep section of %s failed; results may be incomplete\n The -v option will show rpm's output\n The quilt meta-data in %s are already in the version %s format; nothing to do\n The quilt meta-data in this tree has version %s, but this version of quilt can only handle meta-data formats up to and including version %s. Please pop all the patches using the version of quilt used to push them before downgrading.\n The topmost patch %s needs to be refreshed first.\n The working tree was created by an older version of quilt. Please run 'quilt upgrade'.\n USAGE: %s {-s|-u} section file [< replacement]
 Unpacking archive %s\n Usage: quilt [--trace[=verbose]] [--quiltrc=XX] command [-h] ... Usage: quilt add [-p patch] {file} ...\n Usage: quilt annotate {file}\n Usage: quilt applied [patch]\n Usage: quilt delete [patch | -n]\n Usage: quilt diff [-p n] [-u|-U num|-c|-C num] [--combine patch|-z] [-R] [-P patch] [--snapshot] [--diff=utility] [--no-timestamps] [--no-index] [--sort] [--color] [file ...]\n Usage: quilt edit file ...\n Usage: quilt files [-v] [-a] [-l] [--combine patch] [patch]\n Usage: quilt fold [-p strip-level]\n Usage: quilt fork [new_name]\n Usage: quilt graph [--all] [--reduce] [--lines[=num]] [--edge-labels=files] [patch]\n Usage: quilt grep [-h|options] {pattern}\n Usage: quilt header [-a|-r|-e] [--backup] [--strip-diffstat] [--strip-trailing-whitespace] [patch]\n Usage: quilt import [-f] [-p num] [-n patch] patchfile ...\n Usage: quilt mail {--mbox file|--send} [--from ...] [--to ...] [--cc ...] [--bcc ...] [--subject ...]\n Usage: quilt new {patchname}\n Usage: quilt next [patch]\n Usage: quilt patches {file}\n Usage: quilt pop [-afRqv] [num|patch]\n Usage: quilt previous [patch]\n Usage: quilt push [-afqv] [--leave-rejects] [num|patch]\n Usage: quilt refresh [-p n] [-f] [--no-timestamps] [--no-index] [--diffstat] [--sort] [--backup] [--strip-trailing-whitespace] [patch]\n Usage: quilt remove [-p patch] {file} ...\n Usage: quilt rename [-p patch] new_name\n Usage: quilt series [-v]\n Usage: quilt setup [-d path-prefix] [-v] {specfile|seriesfile}\n Usage: quilt snapshot [-d]\n Usage: quilt top\n Usage: quilt unapplied [patch]\n Usage: quilt upgrade\n Warning: trailing whitespace in line %s of %s
 Warning: trailing whitespace in lines %s of %s
 Project-Id-Version: quilt
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-06-27 17:14+0000
PO-Revision-Date: 2011-09-19 18:20+0000
Last-Translator: Sergey Basalaev <SBasalaev@gmail.com>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:14+0000
X-Generator: Launchpad (build 18115)
Language: ru
 
Добавить один или более файлов к верхнему или к названному патчу.
Файлы необходимо добавлять к патчу перед тем, как изменять их.
Файлы, которые изменены патчами, уже наложенными поверх выбранного,
не могут быть добавлены.

-p патч
	Патч, к которому добавлять файлы.
 
Применить патчи из файла series.  Без опций накладывается следующий
патч из файла series.  Если указано число, накладывается указанное
число патчей. Если указано имя файла, накладываются все патчи
до и включая указанный патч. Имена патчей могут содержать префикс
patches/, чтобы можно было использовать автодополнение имён.

-a	Применить все патчи из файла series.

-f	Принудительно применить, даже если наложение вызывает ошибки.

-q	Работать тихо.

-v	Подробный вывод.

--leave-rejects
	Оставлять файлы reject, созданные программой patch, даже если патч
	не был применён.

--interactive
	Позволить утилите patch спрашивать о разрешении конфликтов. Если
	эта опция не указана, опция -f будет передана программе patch.

--color[=always|auto|never]
	Использовать подсветку синтаксиса.
 
Создать новый патч с указанным именем файла, и вставить его после
верхнего патча в файле series.
 
Создать почтовые сообщения из всех патчей в файле series, и либо
сохранить их в файле mailbox, либо немедленно отослать. Открывается
редактор с шаблоном для вводного сообщения. Подробности смотрите в
файле %s.

--mbox файл
	Сохранить все сообщения в указанном файле в формате mbox.
	mbox затем можно отправить, используя, например, formail.

--send
	Отослать сообщения немедленно, используя %s.

--from, --subject
	Значения для заголовочных полей From и Subject.

--to, --cc, --bcc
	Добавить получателя к заголовку To, Cc, или Bcc.
 
Редактировать указанные файлы в редакторе \$EDITOR (%s) после
добавления их к верхнему патчу.
 
Создать ответвление верхнего патча. Создаётся точная копия патча с новым
именем, и это имя используется вместо оригинального патча в файле series.
Это полезно, если необходимо изменить патч, но исходная версия должна
быть сохранаена, например, потому что она используется в другой серии
патчей или для истории. Типичной последовательностью команд в этом случае
может быть: fork, edit, refresh.

Если новое_имя отсутствует, используется имя исходного патча, к которому
добавляется «-2».  Если патч уже заканчивается дефисом и числом, то это
число увеличивается (т.е. patch.diff, patch-2.diff, patch-3.diff).
 
Создать ориентированный граф dot(1), показывающий зависимости между
применёнными патчами. Патч зависит от другого патча, если оба изменяют
один и тот же файл или, с опцией --lines, если их изменения пересекаются.
Если не указано иное, граф включает все патчи, от которых зависит самый
верхний патч.
Когда указано имя патча, создаётся граф зависимостей для него. Граф
включает все патчи, от которых зависит этот патч, а также все патчи,
которые зависят от него.

--all	Создать граф, включающий все применённые патчи и их
	зависимости. (Неприменённые патчи не включаются.)

--reduce
	Удалить транзитивные рёбра из графа.

--lines[=число]
	Вычислять зависимости по строкам, которые патчи изменяют.
	Если не указано другое число, включает две строки контекста.

--edge-labels=files
	Помечать рёбра именами файлов, которые смежные патчи изменяют.

-T ps	Сразу создать файл PostScript.
 
Общие опции:

--trace
	Запускает команду в режиме трассировки bash (-x). Для внутренней отладки.

--quiltrc файл
	Использовать указанный файл конфигурации вместо ~/.quiltrc (или
	/etc/quilt.quiltrc, если ~/.quiltrc не существует).  Смотрите
	pdf-документацию, чтобы узнать о возможном содержимом.

--version
	Напечатать номер версии и выйти из программы. 
Произвести поиск с помощью grep(1) по файлам исходного кода рекурсивно,
пропуская патчи и метаинформацию quilt. Если имя файла не указано,
поиск выполняется по всему дереву. Опции смотрите на странице справки
grep(1).

-h	Вывести эту справку. Опция grep -h может быть передана после
	двойного дефиса (--). Выражения для поиска, начинающиеся с дефиса,
	могут быть переданы после второго двойного дефиса (-- --).
 
Импортировать внешние патчи.

-p число
	Число уровней директорий, которые нужно убрать перед наложением
	(по умочанию=1)

-n патч
	Имя патча для использования в quilt. Эту опцию можно использовать
	только прии импорте одного патча.

-f	Перезаписать или обновить существующие патчи.
 
Инициализирует исходное дерево из файла rpm spec или файла quilt series.

-d	опциональный префикс пути (поддиректория).

-v	вывод отладочной информации.
 
Интегрировать патч, считанный со стандартного ввода, в верхний:
После проверки того, что все изменённые файлы часть самого верхнего
патча, патч применяется с указанным уровнем очистки (по умолчанию 1).

-p strip-level
	Число компонент пути, которые необходимо удалить из имён файлов
	при наложении патча.
 
Пожалуйста, удалите все патчи, используя \`quilt pop -a' версией quilt, использованной при создании этого рабочего дерева, или удалите директорию %s и примените все патчи с нуля.\n 
Вывести список применённых патчей или всех патчей, до и включая
указанный патч в файле series.
 
Вывести список неприменённых патчей или всех патчей, следующих
за указанным патчем в файле series.
 
Вывести список, показывающий, какие строки указанного файла
какими патчами изменены.
 
Вывести или изменить заголовок верхнего или указанного патча.

-a, -r, -e
	Добавить к (-a) или заменить (-r) текущий заголовок патча, или
	редактировать (-e) заголовок в редакторе \$EDITOR (%s). Если
	не дано ни одной из этих опций, вывести заголовок патча.

--strip-diffstat
	Удалить вывод diffstat из заголовка.

--strip-trailing-whitespace
	Удалить пробелы из концов строк заголовка.

--backup
	Создать резервную копию старой версии патча в файле патч~.
 
Вывести список файлов, которые указанный или верхний патч изменяет.

-a	Список всех файлов во всех применённых патчах.

-l	Добавить имя патча к выводу.

-v	Подробный, более дружественный вывод.

--combine патч
	Создать список всех патчей между этим патчем и верхним
	применённым патчем. Указание имени «-» эквивалентно
	указанию первого применённого патча.

 
Вывести список всех патчей, изменяющих указанный файл. (Использует
эвристику, чтобы определить, какие файлы изменяют неналоженные патчи.
Учтите, что эвристика медленнее, чем сканирование наложенных патчей.)

-v	Подробный, более дружественный вывод.
 
Вывести имя патча, следующего за указанным или верхним патчем в
файле series.
 
Вывести имя патча, предшествующего указанному или верхнему
в файле series.
 
Напечатать имя верхнего патча в текущем стеке применённых патчей.
 
Вывести имена всех патчей в файле series.

-v	Подробный, более дружественный вывод.
 
Создаёт diff указанных файлов в верхнем или указанном патче.  Если не
указано файлов, то в diff включаются все изменённые файлы.

-p n	Создать патч в стиле -p n (поддерживаются -p0 и -p1).

-u, -U число, -c, -C число
	Создать универсальный diff (-u, -U) с заданным числом строк
	контекста. Создать контекстный diff (-c, -C) с заданным числом
	строк контекста. По умолчанию число строк контекста равно 3.

--no-timestamps
	Не включать отметки времени в заголовки патчей.

--no-index
	Не выводить строки Index:

-z	Вывести на стандартный вывод изменения, сделанные
	относительно самого верхнего или указанного патча.

-R	Создать обратный diff.

-P патч
	Создать diff для указанного патча.  (По умолчанию для верхнего.)

--combine патч
	Создать объединённый diff для всех патчей между этим патчем и
	указанным в опции -P. Имя патча «-» эквивалентно первому
	применённому патчу.

--snapshot
	diff относительно снимка (см. \`quilt snapshot -h').

--diff=утилита
	Использовать указанную утилиту для создания diff. Утилита вызывается
	с именами оригинального файла и нового файла в качестве аргументов.

--color[=always|auto|never]
	Использовать подсветку синтаксиса.

--sort	Сортировать файлы по имени, не сохраняя их исходного порядка.
 
Обновляет указанный патч, или по умолчанию верхний патч. Документация,
которая содержится в файле патча перед данными, сохраняется.

Есть возможность обновлять патчи, которые располагаются не сверху.
Если какой-нибудь патч поверх обновляемого патча изменяет те же файлы,
то по умолчанию программа прекращает выполнение.  Такие патчи всё же
можно обновить с опцией -f.  В таком случае для каждого такого файла
программа выведет предупреждение, изменения более новыми патчами будут
игнорированы, и только изменения в файлах, которые не были изменены
более новыми патчами, попадут в указанный патч.

-p n	Создать патч в стиле -p n (поддерживаются -p0 и -p1).

-u, -U число, -c, -C число
	Создать универсальный diff (-u, -U) с заданным числом строк
	контекста. Создать контекстный diff (-c, -C) с заданным числом
	строк контекста. По умолчанию число строк контекста равно 3.

--no-timestamps
	Не включать отметки времени в заголовки патчей.
	
--no-index
	Не выводить строки Index:

--diffstat
	Добавить секцию diffstat в заголовок патча, или заменить
	существующую секцию diffstat.

-f	Принудительно обновить патч, находящийся не вверху стека.

--backup
	Создать резервную копию старой версии патча с именем патч~.

--sort	Сортировать файлы по имени, не соблюдая исходный порядок.

--strip-trailing-whitespace
	Удалять пробелы на концах строк.
 
Удалить один или более файлов из самого верхнего или названного
патча.  Файлы, изменённые патчами поверх указанного патча, не
могут быть удалены.

-p патч
	Патч, из которого нужно удалить файлы.
 
Убрать патчи из стека наложенных патчей. Если не указаны опции,
отменяется верхний патч.  Если указано число, отменяется указанное
число патчей.  Когда указано имя патча, патчи убираются до тех пор,
пока указанный патч не окажется на вершине стека. Имена патчей могут
содержать префикс patches/ для использования автодополнения.

-a	Отменить все наложенные патчи.

-f	Принудительно убрать патч. Состояние перед тем, как патчи были
	наложены, может быть восстановлено из резервных копий.

-R	Всегда проверять, чисто ли удаляются патчи; не надеяться
	на проверки отметок времени.

-q	Работать тихо.

-v	Подробный вывод.
 
Удалить указанный или верхний патч из файла series. Если патч применён,
то quilt попытается его отменить. (В настоящий момент можно удалить
лишь верхний патч).

-n	Удалить патч, предшествующий верхнему, вместо указанного
	или верхнего.
 
Переименовать верхний или указанный патч.

-p патч
	Патч, который нужно переименовать.
 
Сделать снимок текущего состояния рабочего дерева.  После снятия снимка,
дерево можно модифицировать обычными спосоами, включая применение и
отмену патчей. Список изменений diff относительно дерева в момент взятия
снимка может быть создан с помощью \`quilt diff --snapshot'.

-d	Просто удалить текущий снимок.
 
Обновить метаданные в рабочем дереве со старой версии quilt до текущей
версии. Эта команда необходима только, если формат метаданных quilt
изменился, а рабочее дерево по-прежнему содержит метаданные старого
формата. В этом случае quilt попросит запустить \`quilt upgrade'.
        quilt --version %s: я в растерянности.
 Добавлен текст к заголовку патча %s\n Применён патч %s (принудительно; следует сделать refresh)\n Наложение патча %s\n Не удалось добавить символьную ссылку %s\n Невозможно сравнить патчи с -p%s, используйте -p0 либо -p1\n Не удалось обновить патчи с -p%s, используйте -p0 или -p1\n Нельзя использовать --strip-trailing-whitespace на патче с затенёнными файлами.\n Команды: Преобразование не удалось\n Преобразование метаданных к версии %s\n Некорректный адрес назначения '%s'
 Не удалось выполнить diff, завершение\n Директория %s существует\n Отображаемое имя '%s' содержит некорректные символы
 Отображаемое имя '%s' содержит непечатные или 8-битные символы
 Отображаемое имя '%s' содержит непарные скобки
 Не удалось создать резервную копию файла %s\n Не удалось скопировать файлы во временный каталог\n Не удалось создать патч %s\n Не удалось импортировать патч %s\n Не удалось вставить патч %s в файл series\n Не удалось применить патч к временным файлам\n Не удалось удалить файл %s из патча %s\n Не удалось отменить патч %s\n Не удалось переименовать %s в %s: %s
 Файл %s добавлен к патчу %s\n Файл %s исчез!
 Файл %s существует\n Файл %s уже в патче %s\n Файл %s не изменяется\n Файл %s отсутствует в патче %s\n Файл %s может быть повреждён\n Файл %s изменён патчем %s\n Файл %s удалён из патча %s\n Файл \`%s' расположен ниже \`%s'\n Серия файлов полностью применена, заканчивается на патче %s\n Ответвление патча %s создано как %s\n Не удалось создать ответвление патча %s в патч %s\n Импорт патча %s (сохранён как %s)\n Прервано пользователем; патч %s не был наложен.\n Более новые патчи изменяют файлы в патче %s. Принудительное обновление можно сделать с помощью опции -f.\n Более новые патчи изменяют файлы в патче %s\n Отсутствует следующий патч\n Не удалено ни одного патча\n Нет применённых патчей\n Ничего нет в патче %s\n Текущий патч: %s\n Опцию \`-n' можно использовать только при импорте одного патча\n Опции \`-c patch', \`--snapshot' и \`-z' не могут использоваться совместно.\n Похоже, патч %s пуст, удаление\n Похоже, патч %s пуст; наложен\n Патч %s не применяется (принудить с -f)\n Патч %s не существует; наложен пустой патч\n Не удалось чисто удалить патч %s (обновите его или удалите принудительно с -f)\n Патч %s уже существует, выберите другое имя\n Патч %s уже существует, выберите новое имя\n Патч %s уже существует\n Патч %s существует. Принудительно заменить можно опцией -f.\n Патч %s уже применён\n Патч %s применён\n Патч %s применён\n Патч %s не применён\n Патч %s не входит в файл series\n Патч %s не в серии\n Патч %s теперь наверху\n Патч %s не изменён\n Сначала нужно обновить патч %s.\n Патч %s не был применён перед патчем %s\n Патч %s не найден в файле series\n Патч %s переименован в %s\n Патч не применён\n Патч %s обновлён\n Удалён патч %s\n Удаление патча %s\n Удаление пробела из конца строки %s файла %s
 Удаление пробела из конца строк %s файла %s
 Переименование %s в %s: %s
 Не удалось переименовать патч %s в %s\n Заменён заголовок патча %s\n Замена патча %s новой версией\n СИНТАКСИС: %s [-p число] [-n] [патч]
 Секция %%prep файла %s содержит ошибки; результаты могут быть незавершены\n Опция -v покажет вывод rpm\n Метаданные quilt в %s уже в формате версии %s; нечего делать\n Метаданные quilt в этом дереве имеют версию %s, но эта версия quilt может обрабатывать форматы метаданных толь до версии %s включительно. Пожалуйста, отмените все патчи, используя версию quilt, которая использовалась для их наложения, прежде чем использовать эту версию.\n Сначала необходимо обновить самый верхний патч %s.\n Рабочее дерево было создано более старой версией quilt. Пожалуйста, выполните 'quilt upgrade'.\n СИНТАКСИС: %s {-s|-u} секция файл [< замена]
 Распаковка архива %s\n Синтаксис: quilt [--trace[=verbose]] [--quiltrc=XX] команда [-h] ... Синтаксис: quilt add [-p патч] {файл} ...\n Синтаксис: quilt annotate {файл}\n Синтаксис: quilt applied [патч]\n Синтаксис: quilt delete [патч | -n]\n Синтаксис: quilt diff [-p n] [-u|-U число|-c|-C число] [--combine патч|-z] [-R] [-P патч] [--snapshot] [--diff=утилита] [--no-timestamps] [--no-index] [--sort] [--color] [файл ...]\n Синтаксис: quilt edit файл ...\n Синтаксис: quilt files [-v] [-a] [-l] [--combine патч] [патч]\n Usage: quilt fold [-p strip-level]\n Синтаксис: quilt fork [новое_имя]\n Синтаксис: quilt graph [--all] [--reduce] [--lines[=число]] [--edge-labels=файлы] [патч]\n Синтаксис: quilt grep [-h|параметры] {шаблон}\n Синтаксис: quilt header [-a|-r|-e] [--backup] [--strip-diffstat] [--strip-trailing-whitespace] [патч]\n Usage: quilt import [-f] [-p число] [-n патч] файл-патча ...\n Синтаксис: quilt mail {--mbox file|--send} [--from ...] [--to ...] [--cc ...] [--bcc ...] [--subject ...]\n Синтаксис: quilt new {имя-патча}\n Синтаксис: quilt next [патч]\n Синтаксис: quilt patches {файл}\n Синтаксис: quilt pop [-afRqv] [число|патч]\n Синтаксис: quilt previous [патч]\n Синтаксис: quilt push [-afqv] [--leave-rejects] [число|патч]\n Синтаксис: quilt refresh [-p n] [-f] [--no-timestamps] [--no-index] [--diffstat] [--sort] [--backup] [--strip-trailing-whitespace] [патч]\n Синтаксис: quilt remove [-p патч] {файл} ...\n Синтаксис: quilt rename [-p патч] новое_имя\n Синтаксис: quilt series [-v]\n Синтаксис: quilt setup [-d префикс-пути] [-v] {файл-spec|файл-series}\n Синтаксис: quilt snapshot [-d]\n Синтаксис: quilt top\n Синтаксис: quilt unapplied [патч]\n Синтаксис: quilt upgrade\n Предупреждение: пробел в конце строки %s файла %s
 Предупреждение: пробел в конце строк %s файла %s
 