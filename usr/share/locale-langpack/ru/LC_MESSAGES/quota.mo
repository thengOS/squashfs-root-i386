��    �      D  �   l      8  D   9  C   ~  6   �  \   �     V     r     �     �     �     �     �     �  	   �            	     7    �   T  -   �  G        O     i     �     �  -   �  *   �  #   �  "     %   9  "   _  .   �  4   �  !   �  *     5   3  %   i     �  "   �  )   �  *   �  !   !     C     _  "   x  *   �  %   �     �     
  %   $  .   J  (   y  -   �     �  G   �  .   7  )   f     �  3   �  2   �  "     -   )     W     u     �     �  $   �     �             )   &     P  !   p     �  *   �  0   �  %     6   1  6   h  "   �  7   �  3   �  6   .     e     {     �     �  i   �     %  #   D  =   h  !   �     �  $   �  5        =  %   Q     w  9   �  7   �  G   �  2   F  G   y  0   �     �          '     F     X     k  2   }  /   �  (   �  $   	  N   .     }    �  (   �  ;   �  �        �!  )   �!  6   )"  '   `"  <   �"     �"     �"     �"     	#     #     :#     K#     \#  �  i#  Z   %  f   \%  W   �%  �   &  A   �&     �&  (   '  $   5'  6   Z'  ,   �'     �'     �'  	   �'     �'     �'  	   �'  f  �'  �   W+  R   6,     �,  <   	-  (   F-     o-  =   �-  c   �-  ]   &.  ;   �.  ?   �.  J    /  I   K/  d   �/  V   �/  D   Q0  T   �0  ~   �0  b   j1  <   �1  G   
2  e   R2  f   �2  K   3  R   k3  4   �3  H   �3  Z   <4  C   �4  =   �4  8   5  ^   R5  l   �5  ]   6  b   |6  D   �6  �   $7  n   �7  \   R8  ,   �8  m   �8  [   J9  O   �9  f   �9  >   ]:  =   �:  A   �:  ,   ;  7   I;     �;     �;  I   �;  \   �;  @   S<  H   �<  ;   �<  H   =  D   b=  B   �=  y   �=  �   d>  D   �>  Q   *?  e   |?  k   �?  (   N@  i   w@  '   �@     	A  �   A  A   �A  V   5B  �   �B  N   C  5   ^C  K   �C  m   �C  '   ND  E   vD  )   �D  r   �D  q   YE  �   �E  m   OF  �   �F  H   EG  %   �G  >   �G  I   �G  !   =H     _H     uH  a   �H  g   �H  G   YI  Q   �I  �   �I  (   �J    �J  =   �L  \   *M  �  �M     �P  b   �P  �   Q  <   �Q  ^   �Q  *   $R  -   OR  *   }R  .   �R  Q   �R      )S     JS     [S     s   	   G       Q                    p   u   9   T       f               >             i   (   U   k                 "   ]   
   o   m      X   �      *   .                  4   I   2   t      b   J   {   A       v          r   -   B           n       +       @   a       �   y   q   F   :      7               5      1   �   H   3   W   R       6   &       M       =       e   '   /              Z           ?   �   ,      #   |   S              )   w   !   �       N          Y   E   }       C   `   \   $   8   ~                 x   0   j      h          L           �   d   g      O   ^              D              [   z       <   P       V   K      ;         _      l   %   c    	quota [-qvswim] [-l | [-Q | -A]] [-F quotaformat] -g groupname ...
 	quota [-qvswim] [-l | [-Q | -A]] [-F quotaformat] -u username ...
 	quota [-qvswugQm] [-F quotaformat] -f filesystem ...
 
-u, --user                    edit user data
-g, --group                   edit group data
 
Can open directory %s: %s
     %8llu    %8llu    %8llu   cache hits:      %u
   dquot dups:      %u
   missed reclaims: %u
   reclaims:        %u
 #%-7d %-8.8s %8llu     %d	%llu	%llu
 %s %s (%s):
 -F, --format=formatname       edit quotas of a specific format
-p, --prototype=name          copy data from a prototype user/group
    --always-resolve          always try to resolve name, even if it is
                              composed only of digits
-f, --filesystem=filesystem   edit data only on a specific filesystem
-t, --edit-period             edit grace period
-T, --edit-times              edit grace time of a user/group
-h, --help                    display this help text and exit
-V, --version                 display version information and exit

 -r, --remote                  edit remote quota (via RPC)
-m, --no-mixed-pathnames      trim leading slashes from NFSv4 mountpoints
 As you wish... Canceling check of this file.
 Bad file magic or version (probably not quotafile with bad endianity).
 Bad number of arguments.
 Block limit reached on Bugs to %s
 Bugs to: %s
 Cannot change grace times over RPC protocol.
 Cannot change owner of temporary file: %s
 Cannot change permission of %s: %s
 Cannot commit dquot for id %u: %s
 Cannot connect to netlink socket: %s
 Cannot connect to system DBUS: %s
 Cannot create DBUS message: No enough memory.
 Cannot create file for %ss for new format on %s: %s
 Cannot create temporary file: %s
 Cannot find a device with %s.
Skipping...
 Cannot find a filesystem mountpoint for directory %s
 Cannot find mountpoint for device %s
 Cannot get host name: %s
 Cannot get name of new quotafile.
 Cannot get quota information for user %s
 Cannot get quota information for user %s.
 Cannot get quotafile name for %s
 Cannot get system info: %s
 Cannot open file %s: %s
 Cannot open new quota file %s: %s
 Cannot open old format file for %ss on %s
 Cannot open old quota file on %s: %s
 Cannot open quotafile %s: %s
 Cannot read block %u: %s
 Cannot read header of old quotafile.
 Cannot read individual grace times from file.
 Cannot read info from quota file %s: %s
 Cannot read information about old quotafile.
 Cannot read quotas from file.
 Cannot remount filesystem %s read-write. cannot write new quota files.
 Cannot rename new quotafile %s to name %s: %s
 Cannot rename old quotafile %s to %s: %s
 Cannot reopen! Cannot stat() a mountpoint with %s: %s
Skipping...
 Cannot stat() given mountpoint %s: %s
Skipping...
 Cannot write grace times to file.
 Cannot write individual grace times to file.
 Cannot write quotas to file.
 Checking quotafile info...
 Compiled with:%s
 Detected quota format %s
 Disk quotas for %s %s (%cid %u): %s
 EXT2_IOC_GETFLAGS failed: %s
 Error Error checking device name: %s
 Error in config file (line %d), ignoring
 Error parsing netlink message.
 Error while editing grace times.
 Error while editing quotas.
 Error while opening old quota file %s: %s
 Error while searching for old quota file %s: %s
 Error while syncing quotas on %s: %s
 Failed to find tty of user %llu to report warning to.
 Failed to open tty %s of user %llu to report warning.
 Failed to parse grace times file.
 Failed to remove IMMUTABLE flag from quota file %s: %s
 Failed to write message to dbus: No enough memory.
 Failed to write quota message for user %llu to %s: %s
 File limit reached on Filesystem remounted read-only
 Found an invalid UUID: %s
 Info LDAP library version >= 2.3 detected. Please use LDAP_URI instead of hostname and port.
Generated URI %s
 Line %d too long. Truncating.
 Mountpoint %s is not a directory?!
 Mountpoint (or device) %s not found or has no quota enabled.
 No correct mountpoint specified.
 No filesystem specified.
 No filesystems with quota detected.
 No possible destination for messages. Nothing to do.
 Not enough memory.
 Old file found removed during check!
 Old file not found.
 Parse error at line %d. Cannot find administrators name.
 Parse error at line %d. Cannot find end of group name.
 Parse error at line %d. Trailing characters after administrators name.
 Possible error in config file (line %d), ignoring
 Prototype name does not make sense when editing grace period or times.
 Quota file %s has IMMUTABLE flag set. Clearing.
 Quota utilities version %s.
 Renaming new quotafile
 Renaming old quotafile to %s~
 Scanning %s [%s]  Should I continue? Skipping %s [%s]
 Something weird happened while scanning. Error %d
 Specified path %s is not directory nor device.
 The running kernel does not support XFS
 Unknown action should be performed.
 Unknown format of kernel netlink message!
Maybe your quota tools are too old?
 Unknown option '%c'.
 Usage:
	edquota %1$s[-u] [-F formatname] [-p username] [-f filesystem] username ...
	edquota %1$s-g [-F formatname] [-p groupname] [-f filesystem] groupname ...
	edquota [-u|g] [-F formatname] [-f filesystem] -t
	edquota [-u|g] [-F formatname] [-f filesystem] -T username|groupname ...
 Usage: %s [-acfugvViTq] [filesystem...]
 Usage: quota [-guqvswim] [-l | [-Q | -A]] [-F quotaformat]
 Utility for converting quota files.
Usage:
	%s [options] mountpoint

-u, --user                          convert user quota file
-g, --group                         convert group quota file
-e, --convert-endian                convert quota file to correct endianity
-f, --convert-format oldfmt,newfmt  convert from old to VFSv0 quota format
-h, --help                          show this help text and exit
-V, --version                       output version information and exit

 Warning Warning: Cannot set EXT2 flags on %s: %s
 Warning: Ignoring -%c when filesystem list specified.
 You have to specify action to perform.
 You have to specify source and target format of conversion.
 cannot open %s: %s
 error (%d) while opening %s
 file limit reached file quota exceeded file quota exceeded too long fsname mismatch
 getgroups(): %s
 pushd %s/%s
 Project-Id-Version: quota
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-12-16 18:19+0100
PO-Revision-Date: 2015-02-17 10:25+0000
Last-Translator: Aleksey Kabanov <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:15+0000
X-Generator: Launchpad (build 18115)
 	quota [-qvswim] [-l | [-Q | -A]] [-F формат квоты] -g имя группы ...
 	quota [-qvswim] [-l | [-Q | -A]] [-F формат квоты] -u имя пользователя ...
 	quota [-qvswugQm] [-F формат квоты] -f файловая система ...
 
-u, --user редактирование данных пользователя
-g, --group редактирование данных группы
 
Невозможно открыть директорию %s: %s
     %8llu    %8llu    %8llu   попаданий в кеш:      %u
   dquot дубликаты:      %u
   пропущено восстановление: %u
   восстанавливаем:        %u
 #%-7d %-8.8s %8llu     %d	%llu	%llu
 %s %s (%s):
 -F, --format=formatname       изменить квоты используя специальный формат
-p, --prototype=name          копировать данные из исходных пользователя/группы
    --always-resolve          всегда пытаться определить имя, даже если оно
                              состоит из цифр
-f, --filesystem=filesystem   изменять данные только в определенной файловой системе
-t, --edit-period             изменить период отстрочки
-T, --edit-times              изменить период отстрочки пользователя/группы
-h, --help                    отобразить справку
-V, --version                 отобразить версию

 -r, --remote                  редактировать удалённые квоты (через RPC)
-m, --no-mixed-pathnames      удалить ведущие слэши из NFSv4 точек монтирования
 Как пожелаете... Отмена проверки этого файла.
 Некорректный файл (возможно не является файлом квот верного формата)
 Неверное количество аргументов.
 Лимит блоков исчерпан Ошибка в: %s
 Об ошибках сообщайте по адресу: %s
 Невозможно изменить время отсрочки через протокол RPC.
 Невозможно изменить владельца временного файла: %s
 Невозможно изменить права к %s: %s
 Невозможно применить dquot для id %u: %s
 Невозможно соединиться с netlink-сокетом: %s
 Невозможно соединиться с системой DBUS: %s
 Невозможно создать DBUS-сообщение: Недостаточно памяти.
 Не удается создать файл %ss нового формата в %s: %s
 Невозможно создать временный файл: %s
 Не удается найти устройство %s.
Пропускается...
 Не удается найти точку монтирования файловой системы для каталога %s
 Не удается найти точку монтирования для устройства %s
 Невозможно получить имя хоста: %s
 Не могу получить имя нового файла квот
 Невозможно получить информацию о квоте пользователя %s
 Невозможно получить информацию о квоте пользователя %s.
 Невозможно получить имя файла квот для %s
 Невозможно получить системную информацию: %s
 Невозможно открыть файл %s: %s
 Невозможно открыть новый файл квот %s: %s
 Невозможно открыть старый формат файла для %ss в %s
 Не могу открыть старый файл квот %s: %s
 Невозможно открыть файл квот %s: %s
 Невозможно прочитать блок %u: %s
 Не могу прочитать заголовок для старого файла квот
 Невозможно прочитать отдельные периоды отсрочки из файла.
 Невозможно прочитать информацию из файла квот %s: %s
 Невозможно прочитать информацию о старом файле квот.
 Невозможно прочитать квоты из файла.
 Невозможно перемонтировать файловую систему %s в режим "чтения-записи". Ошибка записи новых файлов квот.
 не могу переименовать новый файл квот %s на файл с именем %s: %s
 Невозможно переименовать старый файл квот %s в %s: %s
 Невозможно переоткрыть! Невозможно получить точки монтирования %s: %s
Пропускается...
 Не указаны точки монтирования %s: %s
Пропускается...
 Невозможно записать время отсрочки в файл.
 Невозможно записать в файл периоды отдельных отсрочек.
 Невозможно записать квоты в файл.
 Проверка информации файла квот...
 Скомпилировано с использованием: %s
 Определён формат квот %s
 Дисковые квоты для %s %s (%cid %u): %s
 EXT2_IOC_GETFLAGS failed: %s
 Ошибка Ошибка при проверке имени устройства: %s
 Ошибка в файле конфигурации (строка %d), пропустить
 Ошибка расшифровки netlink-сообщения.
 Ошибка при изменении времени отсрочки.
 Ошибка при редактировании квот.
 Ошибка открытия старого файла квот %s: %s
 Ошибка поиска старого файла квот %s: %s
 Ошибка при синхронизации квот в %s: %s
 Ошибка поиска tty пользователя %llu для отправки ему предупреждения.
 Ошибка открытия tty %s пользователя %llu для отправки ему предупреждения.
 Ошибка при обработке файла отсрочки.
 Ошибка удаления флага IMMUTABLE с файла квот %s: %s
 Не удалось написать сообщение dbus: Недостаточно памяти.
 Не удалось записать сообщение квоты пользователю %llu в %s: %s
 Лимит файлов исчерпан Файловая система перемонтирована в режим "только чтение"
 Найден неверный UUID: %s
 Информация Обнаружена библиотека LDAP версии 2.3 или выше. Пожалуйста, используйте LDAP_URI вместо хоста и порта.
Сгенерированный URI %s
 Строка %d слишком длинная. Усечение.
 Точка монтирования %s не является директорией?!
 Точка монтирования (или устройство) %s не найдена или не включены квоты.
 Указана не правильная точка монтирования.
 Не указана файловая система.
 Не обнаружено файловых систем с квотами.
 Нет возможного назначения для сообщений. Нечего выполнять.
 Недостаточно памяти.
 Старый файл удален во время проверки!
 Старый файл не найден.
 Ошибка анализа строки %d. Невозможно найти имя администратора.
 Ошибка анализа строки %d. Невозможно найти конец имени группы.
 Ошибка анализа строки %d. Замыкающий символ после имени администратора.
 Возможна ошибка в файле конфигурации (строка %d), пропустить
 Исходное имя не имеет смысла, когда изменяете период или время отстрочки.
 Файл квот %s имел флаг IMMUTABLE. Исправлено.
 Версия утилиты Quota %s
 Переименование нового файла квот
 Переименования старого файла квот на %s~
 Сканирование %s [%s]  Продолжить? Пропущен %s [%s]
 Произошло что-то странное при сканировании. Ошибка %d
 Указанный путь %s не является каталогом или устройством.
 Текущая версия ядра не поддерживает XFS
 Неизвестное действие должно быть выполнено
 Неизвестый формат netlink-сообщения ядра!
Возможно ваши программы управления квотами слишком устарели?
 Неизвестная опция '%c'.
 Использование:
	edquota %1$s[-u] [-F имя_формата] [-p имя_пользователя] [-f файловая_система] имя_пользователя ...
	edquota %1$s-g [-F имя_формата] [-p имя_группы] [-f файловая_система] имя_группы ...
	edquota [-u|g] [-F имя_формата] [-f файловая_система] -t
	edquota [-u|g] [-F имя_формата] [-f файловая_система] -T имя_пользователя|имя_группы...
 Использование: %s [-acfugvViTq] [filesystem...]
 Использование: quota [-guqvswim] [-l | [-Q | -A]] [-F формат квоты]
 Утилита для конвертирования файлов квот.
Использование:
	%s [опции] точка монтирования

-u, --user конвертировать файл квот пользователя
-g, --group конвертировать файл квот группы
-e, --convert-endian конвертировать файл квот для исправления порядка следования байтов
-f, --convert-format oldfmt,newfmt конвертировать из старого формата в формат квот VFSv0
-h, --help показать текст этой справки и выйти
-V, --version вывести на экран информацию о версии и выйти

 Предупреждение Предупреждение: Невозможно установить флаг EXT2 на %s: %s
 Предупреждение: Игнорировуется -%c когда указан список файловых систем.
 Укажите действие для выполнения
 Укажите исходный и конечный формат преобразования
 не удалось открыть %s: %s
 ошибка (%d) при открытии %s
 достигнут лимит файлов файловая квота превышена файловая квота слишком долго была превышена несовпадение fsname
 getgroups(): %s
 pushd %s/%s
 