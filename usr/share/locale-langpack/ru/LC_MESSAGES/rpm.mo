��   K  0   �    �$     ,1     41  81      d1     e1     z1     �1  
   �1  	   �1     �1  &   �1  2   �1  +   2     J2      h2     �2     �2     �2     �2     �2     �2     �2     3     3  (   %3  &   N3  5   u3  2   �3  3   �3     4     '4     ?4  $   O4     t4     �4     �4     �4     �4  B   �4  !   5     :5     Z5     p5     �5     �5  1   �5     �5  $   6     '6     D6  "   b6     �6     �6  !   �6     �6  !   �6  "   7  #   >7     b7     i7     v7     �7     �7     �7  
   �7     �7     �7     �7     �7     �7     8     !8     .8  	   =8     G8     I8     L8     j8     �8  <   �8  9   �8  ?   9  I   P9  >   �9  <   �9  >   :  ?   U:  F   �:  U   �:  G   2;  6   z;  J   �;  ?   �;  D   <<  $   �<  
   �<     �<  %   �<     �<  
   �<     �<     =     
=  
   =  	   &=  ,   0=     ]=  )   v=  *   �=     �=  !   �=     >     &>     4>     T>     n>     �>  	   �>     �>     �>     �>     �>     ?     ?  A   2?     t?     �?     �?     �?  $   �?     �?  1   @  )   >@  (   h@  $   �@     �@  6   �@  #   A     (A     DA      [A  *   |A     �A  +   �A     �A     �A  	   B     B  ;   1B     mB     }B     �B  $   �B     �B     �B      C     C     2C     CC     VC     qC     �C     �C     �C  !   �C     �C  *   D     @D  !   `D     �D     �D     �D     �D     �D     �D     E  0   !E     RE     ZE     nE     �E     �E  !   �E     �E  ,   �E     *F     9F     HF     fF     |F     �F     �F     �F     �F     �F  '   G  )   -G  !   WG  !   yG     �G     �G     �G     �G     �G  "   �G  ,   
H     7H  !   KH     mH     �H     �H     �H     �H     �H  !   �H  ,   I     8I     NI     aI  A   wI     �I     �I     �I  #   �I  '   J     >J     CJ  .   SJ     �J     �J  &   �J  H   �J  $   K  '   9K     aK  $   �K  *   �K  #   �K     �K  %   L     2L      NL     oL     �L     �L  #   �L  '   �L     M     -M  6   JM     �M     �M     �M     �M     �M  !   �M  "   N     /N     JN     _N  %   yN     �N  
   �N     �N  .   �N     �N     �N  <   O     MO  )   mO  ,   �O     �O     �O  *   �O  )   &P  (   PP  0   yP  /   �P  )   �P  (   Q  :   -Q  9   hQ  J   �Q  D   �Q  C   2R  F   vR  E   �R  )   S  #   -S     QS     gS     �S     �S  $   �S     �S  4   �S  %   +T     QT  &   lT     �T      �T     �T     �T     �T  	   U  	   U  +   (U     TU  &   mU  )   �U  (   �U  *   �U  '   V  )   :V  +   dV  /   �V  .   �V  +   �V  9   W  &   UW  #   |W  "   �W     �W  ;   �W      X  "   =X  (   `X     �X  /   �X  .   �X     Y  '   $Y     LY     jY  %   �Y     �Y  '   �Y     �Y  !   Z  !   -Z     OZ  %   nZ  !   �Z     �Z  "   �Z     �Z     [     ![     0[     6[  %   P[     v[  "   �[  *   �[  '   �[  &   \  %   /\     U\  !   ]\  	   \     �\     �\     �\     �\  "   �\  ?   �\  "   ;]  2   ^]     �]  #   �]     �]     �]  9   �]  >   ^  $   X^     }^  1   �^  F   �^  7   _     ;_     Q_  '   p_  %   �_  /   �_  1   �_  -    `  &   N`     u`     �`     �`     �`     �`     �`  G   �`     Aa     Wa  !   ja     �a     �a     �a     �a  !   �a  $   b     6b     Cb  #   Tb     xb      �b  *   �b      �b     c     c  (   6c     _c  )   wc  #   �c     �c  "   �c     d  &   %d     Ld  3   hd  $   �d  ,   �d     �d  )   e     ,e     Ie  /   ce  0   �e     �e     �e  #   �e  )   f  	   >f     Hf     ef     �f     �f     �f     �f     �f  '   �f     g     4g  !   <g  !   ^g  $   �g  %   �g  #   �g  !   �g     h     .h     Kh     ]h     ph     h     �h     �h     �h     �h     �h     i     6i     Ni     fi     ~i     �i     �i     �i     �i     �i     �i     j  3   j  -   Fj  $   tj  <   �j     �j     �j     k  8   k     Sk  ,   sk  0   �k     �k  0   �k  *   l  +   Jl  '   vl  8   �l  .   �l  -   m  +   4m  )   `m  @   �m     �m     �m  !   n  3   (n     \n  %   zn  #   �n     �n     �n  )   �n  -   (o     Vo     uo     �o  0   �o      �o  #   �o  .   !p  /   Pp  6   �p  6   �p  *   �p     q  >   /q     nq  +   �q  '   �q  -   �q     r     -r  )   Jr  s   tr     �r     s     s  1   7s     is     rs     �s     �s     �s  1   �s     �s  V    t     Wt     jt  +   qt     �t  (   �t     �t  )   �t  /   u     Cu  &   Ku  &   ru     �u     �u     �u     �u  8   v     Kv  W   Wv     �v     �v     �v     �v     w     w     3w  :   @w     {w     �w     �w  $   �w  &   �w     �w  5   x     Ix  '   \x  V   �x     �x     �x     y  $   4y  %   Yy  $   y     �y     �y  	   �y     �y      �y      z     1z     Qz  �  jz  ,   6|  $   c|  (   �|     �|     �|  !   �|  @   �|  W   >}  B   �}  +   �}  4   ~     :~     O~     c~     o~  5   r~  1   �~  !   �~     �~       L   3  J   �  z   �  w   F�  `   ��     �     9�     W�  f   v�  Q   ݁     /�     7�     C�     _�  �   |�  P   �  E   Y�  "   ��  P        �  +   )�  D   U�  &   ��  W   ��  H   �  <   b�  H   ��  ?   �  %   (�  '   N�     v�  '   ��  (   ��  =   �     %�     ,�  %   9�     _�  !   w�     ��     ��     ͇     �     ��     �     !�  ,   5�     b�     p�     ��     ��     ��  8   ��  6   ڈ  !   �  _   3�  _   ��  b   �     V�  a   ֊  _   8�  a   ��  b   ��  u   ]�  �   ӌ  }   Y�  |   ׍  �   T�  b   �  q   M�  ;   ��  
   ��     �  F   �     ^�     j�     x�  
   ��     ��  #   ��     ސ  ;   �  ,   +�  0   X�  Q   ��  .   ۑ  1   
�  J   <�  "   ��  0   ��  7   ے  6   �  !   J�     l�  $   ��  <   ��  ;   �  *   !�  ,   L�  8   y�  ~   ��  *   1�  +   \�     ��  <   ��  F   ɕ  %   �  [   6�  C   ��  ;   ֖  R   �  +   e�  Q   ��  @   �  4   $�  +   Y�  J   ��  N   И  +   �  U   K�  /   ��  #   љ     ��  (   �  q   -�  :   ��  %   ښ      �  ?   �  ,   [�  (   ��  #   ��  3   ՛     	�     %�  /   E�  H   u�  ;   ��  $   ��  3   �  F   S�  =   ��  D   ؝  "   �  9   @�     z�  '   ��  6   ��  6   �     !�     A�     a�  a   ~�     ��  :   ��  ,   /�  c   \�  4   ��  ?   ��  L   5�  m   ��  "   �  !   �  H   5�  #   ~�  4   ��     ע  .   ��  &   #�  8   J�     ��  ?   ��  A   ߣ  '   !�  ?   I�  #   ��  $   ��  $   Ҥ     ��     ��  <   �  W   D�  &   ��  5   å  -   ��     '�     +�     H�  +   h�  1   ��  -   Ʀ  n   ��  ,   c�  .   ��  &   ��  _   �  &   F�     m�  $   ��  9   ��  E   �     *�     /�  L   E�     ��  "   ��  T   Щ  q   %�  U   ��  `   ��  C   N�  N   ��  r   �  @   T�  +   ��  E   ��  B   �  I   J�  2   ��  C   ǭ  F   �  S   R�  O   ��  5   ��  L   ,�  J   y�  (   į  #   ��  9   �  5   K�  *   ��  E   ��  P   �  4   C�     x�  -   ��  <   ñ      �      �     4�  X   9�     ��  3   ��  y   ߲  K   Y�  I   ��  L   �  -   <�  0   j�  M   ��  S   �  @   =�  i   ~�  V   �  b   ?�  @   ��  }   �  j   a�  �   ̷  �   M�  r   Ӹ  �   F�  �   �  ;   u�  V   ��  +   �  I   4�  3   ~�  +   ��  B   ޻  A   !�  Q   c�  B   ��  /   ��  D   (�  ;   m�  X   ��  =   �  7   @�  *   x�     ��     ��  J   ž  A   �  K   R�  S   ��  E   �  G   8�  E   ��  F   ��  6   �  :   D�  9   �  6   ��  g   ��  @   X�  L   ��  K   ��  8   2�     k�  U   ��  ;   A�  a   }�  =   ��  i   �  ]   ��  F   ��  Y   ,�  /   ��  1   ��  U   ��  K   >�  F   ��  3   ��  ;   �  ;   A�  ;   }�  L   ��  @   �  1   G�  H   y�  D   ��  "   �     *�     G�  <   V�  E   ��  -   ��  ?   �  i   G�  :   ��  >   ��  >   +�     j�  ;   y�     ��     ��  0   ��     #�  1   4�  :   f�  �   ��  6   *�  Z   a�  E   ��  C   �  !   F�     h�  R   q�  y   ��  N   >�     ��  �   ��  [   5�  g   ��  0   ��  4   *�  D   _�  J   ��  h   ��  b   X�  L   ��  X   �  &   a�  4   ��  *   ��  #   ��  6   �     C�  �   X�  -   ��  #   �  V   >�  /   ��  .   ��  -   ��  B   "�  F   e�  L   ��     ��     �  B   '�  4   j�  J   ��  E   ��  E   0�  1   v�  :   ��  E   ��  )   )�  ?   S�  9   ��  4   ��  ;   �  >   >�  S   }�  -   ��  Y   ��  :   Y�  X   ��  !   ��  V   �  8   f�  3   ��  l   ��  g   @�  %   ��  !   ��  I   ��  \   :�     ��  ;   ��  ;   ��  7   !�  9   Y�  1   ��  7   ��  .   ��  '   ,�     T�     q�  :   ��  :   ��  :   ��  A   9�  H   {�  ;   ��  )    �  1   *�  )   \�  *   ��     ��  $   ��  3   ��  <   �  1   Z�  2   ��  '   ��  9   ��  E   !�  9   g�  H   ��  8   ��  8   #�     \�     q�     ��     ��     ��     ��  ~   ��  y   k�  [   ��  }   A�  +   ��  '   ��  >   �  P   R�  )   ��  G   ��  X   �  (   n�  F   ��  F   ��  ]   %�  r   ��  m   ��  g   d�  f   ��  d   3�  g   ��  �    �  /   ��  =   ��  G   ��  W   B�  3   ��  b   ��  B   1�  2   t�  A   ��  x   ��  V   b�  8   ��  ;   ��  9   .�  B   h�  B   ��  b   ��  b   Q�  c   ��  W   �  K   p�  Y   ��  #   �  �   :�  8   ��  M   �  >   _�  d   ��  =   �  8   A�  =   z�  �   ��  P   ��  A   ��  P   "�  `   s�     ��     ��  *   �     -�     K�  c   k�  !   ��  �   ��  7   ��     ��  8   ��  !   �  E   ?�  '   ��  7   ��  j   ��     P�  V   c�  d   ��  @   �  C   `�  C   ��  C   ��  }   ,�     ��  �   ��  )   g  5   �     �  0   �  2    6   F    } O   �    � $       + >   C c   � >   � n   %    � D   � �   � D   � B   � E    V   ^ O   � <    3   B 2   v    �    � ;   � ;   ! 5   ] ,   �    �     �       6  �      �       ~         /      �   $   �      �  �   ,      �   �      �  �   h          �     �   �  �           
  �  "   H   L   !            �  o  8       �      H  �  ]  @         �  �   +       �  �             O                   c          \  D      �  M  {         ;  h   n       (      �   d   �  �   �            �   �   �         �  �   �         �   �               �         ;       �   A   �   i   .   �   <  -      �   �  �     �                          +          �  �   �  �       �   �  �        8  Z         �   x        �  �    �   r  X   �        �   D  �  p          �   �   �  4  �       "        �            �   �   �   �  �  '          �  y    2  s  �  �    7    =  '    �   D       �      
  L  �   b  	  l        I  Y      �        <              m      `   K  ~           �   2       �              �      t   �  �   �        J      �     �      #   �    0  �  u          �       �       �   q  W  6  F      �      '       p             v   �   �  �      �     �  �     �      F  �   �           �   �  /  �  e   �       >            C   ^     �   %   �   %  �   �       :  X                |  �     �   S       5      8        	   2      �      f  s   9      }   �      =      �      a   �        �   w  �   �       $  �  �          }  �    �       �   A  �      /   �   @   :   .  �  �   �   J  �   (   "      u           _      5   �  �  �   G  �  R  �      �  �  �   �   |       �      �       o   ]   �   E  3  #  �   )   �   *       �       v  &       �   �  6   �  �  >     �  �   %  &  
       )    N   C  !   �       x      -  ^      �  �   �   �         m   �        �     �  t  �   ?  l   g  B  �   �   �             [       �  H          [      �   r   c       �           K   w   9  +  �  y       �       �  �     &      7        #  �   �  �  �                 a    �   :  R   �   P  �  	  �  �   �      �   {      �  S  .     j  �       Y   z       �               �  �  �  b       G  �   N    9   ,   n  �       k   P   �      k  *  �   �   �  B  �  �  3  q   ;         Q                       0   -       �   �  1           �        T  �  Q    �       �  �   �      5  _   �        �       �   \   �        �  ,  �     >   �  �   �  �         E  ?      �   �  �       `  7   0  =           $    �       �       F          �  G   !      *  M   C      Z  d  �      �      1   �  3   V      1  j        �  g   �   �  I   I  4                     �   �   �    �   U   @  �  �          �      �              �   �   f       <      �   �  J   �           �  �   )  �          �           �              (  �     �  �  i  A      V   B   �      4   W   �       �   T     K      z  �  �   �           �  O  ?  �  U  e         �   �                   �  E   �   �         � <1  P1  �           ����� @       +   ���� 

RPM build errors:
  (MISSING KEYS:  (UNTRUSTED KEYS:  failed -   on file  ! only on numbers
 %%changelog entries must start with *
 %%changelog not in descending chronological order
 %%patch without corresponding "Patch:" tag
 %%{buildroot} can not be "/"
 %%{buildroot} couldn't be empty
 %3d<%*s(empty)
 %3d>%*s(empty) %a %b %d %Y %c %s %d defined multiple times
 %s cannot be installed
 %s conflicts with %s%s %s created as %s
 %s failed: %x
 %s field must be present in package: %s
 %s has invalid numeric value, skipped
 %s has too large or too small integer value, skipped
 %s has too large or too small long value, skipped
 %s is a Delta RPM and cannot be directly installed
 %s is needed by %s%s %s is obsoleted by %s%s %s saved as %s
 %s scriptlet failed, exit status %d
 %s scriptlet failed, signal %d
 %s: %s
 %s: %s: %s
 %s: Fread failed: %s
 %s: Fwrite failed: %s
 %s: Immutable header region could not be read. Corrupted package?
 %s: can't load unknown tag (%d).
 %s: cannot read header at 0x%x
 %s: failed to encode
 %s: import read failed(%d).
 %s: line: %s
 %s: not an armored public key.
 %s: not an rpm package (or package manifest): %s
 %s: open failed: %s
 %s: option table misconfigured (%d)
 %s: public key read failed.
 %s: read manifest failed: %s
 %s: reading of public key failed.
 %s: regexec failed: %s
 %s: rpmReadSignature failed: %s %s: rpmWriteSignature failed: %s
 %s: writeLead failed: %s
 %s:%d: Got a %%else with no %%if
 %s:%d: Got a %%endif with no %%if
 && and || not suported for strings
 'EXPR' 'MACRO EXPR' (contains no files)
 (installed)  (invalid type) (invalid xml type) (no error) (no state)     (none) (not a blob) (not a number) (not a string) (not an OpenPGP signature) (not base64) (unknown %3d)  (unknown) ) )  * / not suported for strings
 - not suported for strings
 - only on numbers
 --allfiles may only be specified during package installation --allmatches may only be specified during package erasure --excludedocs may only be specified during package installation --hash (-h) may only be specified during package installation and erasure --ignorearch may only be specified during package installation --ignoreos may only be specified during package installation --ignoresize may only be specified during package installation --includedocs may only be specified during package installation --justdb may only be specified during package installation and erasure --nodeps may only be specified during package installation, erasure, and verification --percent may only be specified during package installation and erasure --prefix may only be used when installing new packages --relocate and --excludepath may only be used when installing new packages --replacepkgs may only be specified during package installation --test may only be specified during package installation and erasure : expected following ? subexpression <FILE:...> <dir> <lua> scriptlet support not built in
 <old>=<new> <package>+ <packagefile>+ <path> <source package> <specfile> <tarball> ======================== active %d empty %d
 ? expected in expression A %% is followed by an unparseable macro
 Arch dependent binaries in noarch package
 Architecture is excluded: %s
 Architecture is not included: %s
 Archive file not in header Bad CSA data
 Bad arch/os number: %s (%s:%d)
 Bad dirmode spec: %s(%s)
 Bad exit status from %s (%s)
 Bad file: %s: %s
 Bad magic Bad mode spec: %s(%s)
 Bad owner/group: %s
 Bad package specification: %s
 Bad source: %s: %s
 Bad syntax: %s(%s)
 Bad/unreadable  header Build options with [ <specfile> | <tarball> | <source package> ]: Building for target %s
 Building target platforms: %s
 CMD Cannot sign RPM v3 packages
 Checking for unpackaged file(s): %s
 Cleaning up / removing...
 Common options for all rpm modes and executables: Conversion of %s to long integer failed.
 Copyright (C) 1998-2002 - Red Hat, Inc.
 Could not canonicalize hostname: %s
 Could not exec %s: %s
 Could not generate output filename for package %s: %s
 Could not open %%files file %s: %m
 Could not open %s file: %s
 Could not open %s: %s
 Couldn't create pipe for %s: %m
 Couldn't create temporary file for %s: %s
 Couldn't download %s
 Couldn't duplicate file descriptor: %s: %s
 Couldn't exec %s: %s
 Couldn't fork %s: %s
 DIRECTORY Database options: Dependency tokens must begin with alpha-numeric, '_' or '/' Digest mismatch Directory not found: %s
 Downloading %s to %s
 Duplicate %s entries in package: %s
 Enter pass phrase:  Error parsing %%setup: %s
 Error parsing %s: %s
 Exec of %s failed (%s): %s
 Executing "%s":
 Executing(%s): %s
 Execution of "%s" failed.
 Failed build dependencies:
 Failed dependencies:
 Failed to find %s:
 Failed to open tar pipe: %m
 Failed to read spec file from %s
 Failed to rename %s to %s: %m
 File %s does not appear to be a specfile.
 File %s is not a regular file.
 File %s is smaller than %u bytes
 File %s: %s
 File listed twice: %s
 File must begin with "/": %s
 File needs leading "/": %s
 File not found by glob: %s
 File not found: %s
 Finding  %s: %s
 Generating %d missing index(es), please wait...
 Header  Header SHA1 digest: Header size too big Ignoring invalid regex %s
 Incomplete data line at %s:%d
 Incomplete default line at %s:%d
 Install/Upgrade/Erase options: Installed (but unpackaged) file(s) found:
%s Installing %s
 Internal error Internal error: Bogus tag %d
 Invalid %s token: %s
 Invalid capability: %s
 Invalid date %u Invalid patch number %s: %s
 MD5 digest: Macro %%%s failed to expand
 Macro %%%s has empty body
 Macro %%%s has illegal name (%%define)
 Macro %%%s has illegal name (%%undefine)
 Macro %%%s has unterminated body
 Macro %%%s has unterminated opts
 Missing %s in %s %s
 Missing '(' in %s %s
 Missing ')' in %s(%s
 NO  NOT OK No "Source:" tag in the spec file
 No compatible architectures found for build
 No patch number %u
 Non-white space follows %s(): %s
 Not a directory: %s
 OK OS is excluded: %s
 OS is not included: %s
 Package already exists: %s
 Package check "%s" failed.
 Package has no %%description: %s
 Pass phrase check failed or gpg key expired
 Pass phrase is good.
 Please contact %s
 Plugin %s not loaded
 PreReq:, Provides:, and Obsoletes: dependencies support versions. Preparing packages... Preparing... Processing files: %s
 Query options (with -q or --query): Query/Verify package selection options: ROOT RPM version %s
 Recognition of file "%s" failed: mode %06o %s
 Retrieving %s
 Signature options: Symlink points to BuildRoot: %s -> %s
 This program may be freely redistributed under the terms of the GNU GPL
 Too many args in data line at %s:%d
 Too many args in default line at %s:%d
 Too many arguments in line: %s
 Unable to change root directory: %m
 Unable to create immutable header region.
 Unable to open %s for reading: %m.
 Unable to open %s: %s
 Unable to open current directory: %m
 Unable to open icon %s: %s
 Unable to open spec file %s: %s
 Unable to open stream: %s
 Unable to open temp file: %s
 Unable to read icon %s: %s
 Unable to reload signature header.
 Unable to restore current directory: %m Unable to write package: %s
 Unable to write temp header
 Unknown file digest algorithm %u, falling back to MD5
 Unknown file type Unknown format Unknown icon type: %s
 Unknown option %c in %s(%s)
 Unknown system: %s
 Unsatisfied dependencies for %s:
 Unsupported PGP hash algorithm %u
 Unsupported PGP signature
 Unterminated %c: %s
 Updating / installing...
 Verify options (with -V or --verify): Version required Wrote: %s
 YES You must set "%%_gpg_name" in your macro file
 [none] ] expected at end of array a hardlink file set may be installed without being complete. argument is not an RPM package
 arguments to --prefix must begin with a / arguments to --root (-r) must begin with a / bad date in %%changelog: %s
 bad option '%s' at %s:%d
 build binary package from <source package> build binary package only from <specfile> build binary package only from <tarball> build source and binary packages from <specfile> build source and binary packages from <tarball> build source package only from <specfile> build source package only from <tarball> build through %build (%prep, then compile) from <specfile> build through %build (%prep, then compile) from <tarball> build through %install (%prep, %build, then install) from <source package> build through %install (%prep, %build, then install) from <specfile> build through %install (%prep, %build, then install) from <tarball> build through %prep (unpack sources and apply patches) from <specfile> build through %prep (unpack sources and apply patches) from <tarball> buildroot already specified, ignoring %s
 cannot add record originally at %u
 cannot create %s: %s
 cannot get %s lock on %s/%s
 cannot open %s at %s:%d: %m
 cannot open %s: %s
 cannot open Packages database in %s
 cannot re-open payload: %s
 cannot use --prefix with --relocate or --excludepath create archive failed on file %s: %s
 create archive failed: %s
 creating a pipe for --pipe failed: %m
 debug file state machine debug payload file state machine debug rpmio I/O define MACRO with value EXPR delete package signatures different directory display final rpmrc and macro configuration display known query tags display the states of the listed files do not accept i18N msgstr's from specfile do not execute %%post scriptlet (if any) do not execute %%postun scriptlet (if any) do not execute %%pre scriptlet (if any) do not execute %%preun scriptlet (if any) do not execute any %%triggerin scriptlet(s) do not execute any %%triggerpostun scriptlet(s) do not execute any %%triggerprein scriptlet(s) do not execute any %%triggerun scriptlet(s) do not execute any scriptlet(s) triggered by this package do not execute any stages of the build do not execute package scriptlet(s) do not install configuration files do not install documentation do not reorder package installation to satisfy dependencies do not verify build dependencies do not verify package dependencies don't check disk space before installing don't execute verify script(s) don't install, but tell if it would work or not don't verify database header(s) when retrieved don't verify digest of files don't verify digest of files (obsolete) don't verify files in package don't verify group of files don't verify header+payload signature don't verify mode of files don't verify modification time of files don't verify owner of files don't verify package architecture don't verify package dependencies don't verify package digest(s) don't verify package operating system don't verify package signature(s) don't verify size of files don't verify symlink path of files dump basic file information empty tag format empty tag name erase erase (uninstall) package error creating temporary file %s: %m
 error reading from file %s
 error reading header from package
 error(%d) allocating new package instance
 error(%d) removing record "%s" from %s
 error(%d) storing record "%s" into %s
 error(%d) storing record #%d into %s
 error:  exclude paths must begin with a / exclusive exec failed
 extra '(' in package label: %s
 failed failed to create directory failed to create directory %s: %s
 failed to rebuild database: original database remains in place
 failed to remove directory %s: %s
 failed to replace old database with new database!
 failed to stat %s: %m
 failed to write all data to %s: %s
 fatal error:  file file %s conflicts between attempted installs of %s and %s file %s from install of %s conflicts with file from package %s file %s is not owned by any package
 file %s: %s
 file digest algorithm is per package configurable file name(s) stored as (dirName,baseName,dirIndex) tuple, not as path. files may only be relocated during package installation gpg exec failed (%d)
 gpg failed to write signature
 group %s does not contain any packages
 group %s does not exist - using root
 header #%u in the database is bad -- skipping.
 header tags are always sorted after being loaded. ignore ExcludeArch: directives from spec file ignore file conflicts between packages illegal signature type import an armored public key incomplete %%changelog entry
 incorrect format: %s
 initialize database install install all files, even configurations which might otherwise be skipped install documentation install package(s) internal support for lua scripts. invalid dependency invalid field width invalid package number: %s
 invalid syntax in lua file: %s
 invalid syntax in lua script: %s
 invalid syntax in lua scriptlet: %s
 line %d: %s
 line %d: %s: %s
 line %d: Bad %%setup option %s: %s
 line %d: Bad %s number: %s
 line %d: Bad %s: qualifiers: %s
 line %d: Bad BuildArchitecture format: %s
 line %d: Bad arg to %%setup: %s
 line %d: Bad number: %s
 line %d: Bad option %s: %s
 line %d: Docdir must begin with '/': %s
 line %d: Empty tag: %s
 line %d: Error parsing %%description: %s
 line %d: Error parsing %%files: %s
 line %d: Error parsing %s: %s
 line %d: Illegal char '%c' in: %s
 line %d: Illegal char in: %s
 line %d: Illegal sequence ".." in: %s
 line %d: Malformed tag: %s
 line %d: Only noarch subpackages are supported: %s
 line %d: Package does not exist: %s
 line %d: Prefixes must not end with "/": %s
 line %d: Second %s
 line %d: Tag takes single token only: %s
 line %d: Too many names: %s
 line %d: Unknown tag: %s
 line %d: internal script must end with '>': %s
 line %d: script program must begin with '/': %s
 line %d: second %%prep
 line %d: second %s
 line %d: triggers must have --: %s
 line %d: unsupported internal script: %s
 line: %s
 list all configuration files list all documentation files list files in package lua script failed: %s
 magic_load failed: %s
 magic_open(0x%x) failed: %s
 malformed %s: %s
 memory alloc (%u bytes) returned NULL.
 miFreeHeader: skipping missing missing '(' in package label: %s
 missing ')' in package label: %s
 missing ':' (found 0x%02x) at %s:%d
 missing architecture for %s at %s:%d
 missing architecture name at %s:%d
 missing argument for %s at %s:%d
 missing name in %%changelog
 missing second ':' at %s:%d
 missing { after % missing } after %{ net shared     no arguments given no arguments given for query no arguments given for verify no dbpath has been set no dbpath has been set
 no description in %%changelog
 no package matches %s: %s
 no package provides %s
 no package requires %s
 no package triggers %s
 no packages given for erase no packages given for install normal normal         not an rpm package not an rpm package
 not installed not installed  one type of query/verify may be performed at a time only installation and upgrading may be forced only one major mode may be specified only one of --excludedocs and --includedocs may be specified open of %s failed: %s
 override build root override target platform package %s (which is newer than %s) is already installed package %s is already installed package %s is intended for a %s architecture package %s is intended for a %s operating system package %s is not installed
 package %s was already added, replacing with %s
 package %s was already added, skipping %s
 package has neither file owner or id lists
 package has not file owner/group lists
 package name-version-release is not implicitly provided. package payload can be compressed using bzip2. package payload can be compressed using lzma. package payload can be compressed using xz. package payload file(s) have "./" prefix. package scriptlets may access the rpm database while installing. parse error in expression
 predefine MACRO with value EXPR print dependency loops as warning print hash marks as package installs (good with -v) print macro expansion of EXPR print percentages as package installs print the version of rpm being used provide less detailed output provide more detailed output query of specfile %s failed, can't parse
 query the package(s) triggered by the package query/verify a header instance query/verify a package file query/verify all packages query/verify package(s) from install transaction query/verify package(s) in group query/verify package(s) owning file query/verify package(s) with header identifier query/verify package(s) with package identifier query/verify the package(s) which provide a dependency query/verify the package(s) which require a dependency read <FILE:...> instead of default file(s) read failed: %s (%d)
 rebuild database inverted lists from installed package headers record %u could not be read
 reinstall if the package is already present relocate files from path <old> to <new> relocate the package to <dir>, if relocatable relocations must begin with a / relocations must contain a = relocations must have a / following the = remove all packages which match <package> (normally an error is generated if <package> specified multiple packages) remove build tree when done remove sources when done remove specfile when done replace files in %s with files from %s to recover replaced replaced       rpm checksig mode rpm query mode rpm verify mode rpmdb: damaged header #%u retrieved -- skipping.
 rpmdbNextIterator: skipping script disabling options may only be specified during package installation and erasure send stdout to CMD shared short hand for --replacepkgs --replacefiles sign package(s) sign package(s) (identical to --addsign) skip %%ghost files skip files with leading component <path>  skip straight to specified stage (only for c,i) skipped source package contains no .spec file
 source package expected, binary found
 syntax error in expression
 syntax error while parsing &&
 syntax error while parsing ==
 syntax error while parsing ||
 the scriptlet interpreter can use arguments from header. transaction trigger disabling options may only be specified during package installation and erasure types must match
 unable to read the signature
 unexpected ] unexpected query flags unexpected query format unexpected query source unexpected } unknown error %d encountered while manipulating package %s unknown tag unknown tag: "%s"
 unmatched (
 unpacking of archive failed%s%s: %s
 unrecognized db option: "%s" ignored.
 unsupported RPM package version update the database, but do not modify the filesystem upgrade package(s) upgrade package(s) if already installed upgrade to an old version of the package (--force on upgrades does this automatically) use ROOT as top level directory use database in DIRECTORY use the following query format user %s does not exist - using root
 verify %files section from <specfile> verify %files section from <tarball> verify database files verify package signature(s) warning:  wrong color    { expected after : in expression { expected after ? in expression | expected at end of expression } expected in expression Project-Id-Version: RPM
Report-Msgid-Bugs-To: rpm-maint@lists.rpm.org
POT-Creation-Date: 2014-09-18 14:01+0300
PO-Revision-Date: 2014-03-12 12:43+0000
Last-Translator: ☠Jay ZDLin☠ <Unknown>
Language-Team: Russian (http://www.transifex.com/projects/p/rpm/language/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:05+0000
X-Generator: Launchpad (build 18115)
Language: ru
 

Ошибки сборки пакетов:
  (ОТСУТСТВУЮТ КЛЮЧИ:  (НЕТ ДОВЕРИЯ К КЛЮЧАМ:  не удалось -   на файле  ! только для чисел
 записи %%changelog должны начинаться с *
 %%changelog не в нисходящем хронологическом порядке
 %%patch без соответствующего тэга "Patch:"
 %%{buildroot} не может быть "/"
 %%{buildroot} не может быть пустым
 %3d<%*s(пусто)
 %3d>%*s(пусто) %a %b %d %Y %c %s %d определено несколько раз
 %s не может быть установлен
 %s конфликтует с %s%s %s создан как %s
 %s не удалось: %x
 Поле %s обязано присутствовать в пакете: %s
 неверное числовое значение %s, пропущено
 %s имеет слишком малую или слишком большую величину integer, пропущено
 %s имеет слишком малую или слишком большую величину long, пропущено
 %s является Delta RPM и не может быть установлен напрямую
 %s нужен для %s%s %s заменено на %s%s %s сохранен как %s
 неудачное выполнение скриптлета %s, статус завершения %d
 неудачное выполнение скриптлета %s, сигнал %d
 %s: %s
 %s: %s: %s
 %s: ошибка Fread: %s
 %s: ошибка Fwrite: %s
 %s: Неизменная область заголовка не может быть прочитана. Испорченный пакет?
 %s: невозможно загрузить неизвестный тэг (%d).
 %s: невозможно прочесть заголовок в 0x%x
 %s: сбой шифрования
 %s: ошибка чтения во время импортирования(%d).
 %s: строка: %s
 %s: это не открытый ключ.
 %s: не пакет (или манифест пакета) rpm : %s
 %s: ошибка открытия: %s
 %s: таблица параметров неправильно настроена (%d)
 %s: не удалось прочитать публичный ключ.
 %s: ошибка чтения списка файлов: %s
 %s: не удалось прочитать публичный ключ.
 %s: ошибка регулярного выражения: %s
 %s: ошибка rpmReadSignature: %s %s: ошибка rpmWriteSignature: %s
 %s: ошибка writeLead: %s
 %s:%d: Найден %%else без %%if
 %s:%d: Найден %%endif без %%if
 && и || не поддерживаются для строк
 'EXPR' 'MACRO EXPR' (не содержит файлов)
 (установлен)  (неправильный тип) (неверный тип xml) (нет ошибки) (сост. нет)     (отсутствует) (не число) (не число) (не строка) (не подпись формата OpenPGP) (не base64) (неизв. %3d)  (неизвестный) ) )  * / не поддерживается для строк
 - не поддерживается для строк
 - только для чисел
 --allfiles может быть указан только при установке пакета --allmatches может быть указан только при удалении пакета --excludedocs может быть указан только при установке пакета --hash (-h) может быть указан только во время установки и удаления пакетов --ignorearch может быть указан только при установке пакета --ignoreos может быть указан только при установке пакета --ignoresize может быть указан только при установке пакета --includedocs может быть указан только при установке пакета --justdb может быть указан только при установке или удалении пакета --nodeps может быть указана только при установке, удалении и проверке пакета --percent может быть указан только во время установки и удаления пакетов вариант --prefix можно использовать только при установке новых пакетов опции  --relocate и --excludepath могут быть использованы только при установке новых пакетов --replacepkgs может быть указан только при установке пакета --test может быть указана только при установке и удалении пакета в выражении после "?" ожидалось ":" <FILE:...> <каталог> поддержка скриптлетов <lua> не встроена
 <old>=<new> <пакет>+ <файл пакета>+ <путь> <исходный пакет> <файл спецификации> <архив tar> ====================== активных %d пустых %d
 в выражении ожидалось "?" непонятный макрос после %%
 Двоичные данные с архитекутрой в пакете noarch
 Архитектура исключена: %s
 Архитектура не включена: %s
 Файл архива не найден в заголовке пакета Неверные данные CSA
 Неверный номер arch/os: %s (%s:%d)
 Неверные права на каталог %s(%s)
 Неверный код возврата из %s (%s)
 Неверный файл %s: %s
 Неверный magic Неверные права: %s(%s)
 Неверная пара владелец/группа: %s
 Неверная спецификация пакета: %s
 Неверный исходник: %s: %s
 Неверный синтаксис: %s(%s)
 Неверный/нечитаемый заголовок Параметры сборки с [ <файл спецификации> | <tar архив> | <исходный пакет> ]: Сборка для платформы %s
 Платформы для сборки: %s
 CMD Невозможно подписать пакеты RPM v3
 Проверка на неупакованный(е) файл(ы): %s
 Очистка / удаление...
 Общие параметры для всех режимов и компонентов rpm: Не удалось преобразовать %s в long integer.
 Авторские права © 1998-2002 - Red Hat, Inc.
 Невозможно канонизировать имя компьютера: %s
 Невозможно вылнить %s: %s
 Невозможно создать имя файла для пакета %s: %s
 Не удалось открыть файл %s из %%files: %m
 Невозможно открыть %s файл: %s
 Невозможно открыть %s: %s
 Невозможно создать канал данных для %s: %m
 Невозможно создать временный файл для %s: %s
 Невозможно загрузить %s
 Не удалось сдублировать дескриптор файла: %s: %s
 Невозможно выполнить %s: %s
 Сбой ветвления %s: %s
 КАТАЛОГ Параметры базы данных Токены зависимостей должны начинаться с буквы, цифры, '_' или '/' Несовпадение контрольной суммы Каталог не найден: %s
 Загрузка %s в %s
 Повторяющиеся записи %s в пакете: %s
 Введите ключевую фразу:  Ошибка анализа %%setup: %s
 Ошибка разбора %s: %s
 Выполнить %s не удалось (%s): %s
 Выполнение "%s":
 Выполняется(%s): %s
 Выполнение "%s" не удалось.
 Неудовлетворенные зависимости сборки:
 Неудовлетворенные зависимости:
 Невозможно найти %s:
 Ошибка открытия канала tar: %m
 Ошибка чтения файла спецификации из %s
 Невозможно переименовать %s в %s: %m
 Файл %s не похож на файл спецификации.
 Не обычный файл: %s.
 Длина файла %s меньше чем %u байт
 Файл %s: %s
 Файл указан дважды: %s
 Файл должен начинаться с "/": %s
 Файл должен начинаться с "/": %s
 Файл не найден: %s
 Файл не найден: %s
 Идет поиск  %s: %s
 Создание %d отсутствующего(их) индекса(ов), ожидайте...
 Заголовок  Контрольная сумма заголовка SHA1: Заголовок слишком велик Игнорирование недопустимого регулярного выражения %s
 Неполная строка данных в %s:%d
 Неполная строка по умолчанию в %s:%d
 Параметры Установки/Обновления/Удаления: Обнаружен(ы) установленный(е) (но не упакованный(е)) файл(ы):
%s Устанавливается %s
 Внутренняя ошибка Внутренняя ошибка: Неизвестный ярлык %d
 Неверный токен %s: %s
 Неправильная возможность: %s
 Неверная дата %u Неверный номер патча %s: %s
 Контрольная сумма MD5: Невозможно раскрыть макрос %%%s
 Макрос %%%s пуст
 Недопустимое имя (%%define) макроса %%%s
 Недопустимое имя (%%undefine) макроса %%%s
 Незакрытый макрос %%%s
 Незакрытые параметры в макросе %%%s
 Отсутствует %s в %s %s
 Отсутствует '(' в %s %s
 отсутствует ')' в %s(%s
 НЕT  НЕ OК Не определен тэг "Source:" в spec файле
 Не найдены совместимые архитектуры для сборки.
 Отсутствует патч №%u
 Не пробел следует после %s(): %s
 Не является каталогом: %s
 OК ОС исключена: %s
 ОС не включена: %s
 Пакет уже существует: %s
 Ошибка проверки пакета "%s".
 Пакет не имеет %%description: %s
 Неправильная кодовая фраза или истёк срок действия ключа gpg
 Ключевая фраза принята.
 Пожалуйста, обратитесь %s
 Модуль %s не загружен
 PreReq:, Provides:, и Obsoletes: зависимости поддерживают версии. Подготовка пакетов... Подготовка... Обработка файлов: %s
 Параметры запроса (с -q или --query): Опции выбора запроса/проверки пакета: ROOT Версия RPM  %s
 Не удалось распознать файл "%s" : режим %06o %s
 Загружается %s
 Параметры подписи: Символическая ссылка указывает на BuildRoot: %s -> %s
 Эту программу можно свободно распространять на условиях GNU GPL
 Слишком много аргументов в строке данных в %s:%d
 Слишком много аргументов в строке по умолчанию в %s:%d
 Слишком много аргументов в строке: %s
 Невозможно сменить корневую директорию: %m
 Невозможно поместить заголовок в непрерывную область памяти.
 Невозможно открыть %s для чтения: %m.
 Невозможно открыть %s: %s
 Не удалось открыть текущий каталог: %m
 Невозможно открыть пиктограмму %s: %s
 Ошибка открытия файла спецификации %s: %s
 Не удаётся открыть поток: %s
 Не удаётся открыть временный файл: %s
 Невозможно прочитать пиктограмму %s: %s
 Невозможно перезагрузить заголовок подписи.
 Невозможно восстановить текущий каталог: %m Невозможно записать пакет: %s
 Невозможно записать временный заголовок
 Не удается проверить файл %u, возврат к MD5
 Неизвестный тип файла Неизвестный формат Неизвестный тип пиктограммы: %s
 Неизвестный параметр %c в %s(%s)
 Неизвестная система: %s
 Неудовлетворённые зависимости для %s:
 Неподдерживаемый алгоритм хэширования PGP %u
 Неподдерживаемая подпись PGP
 Незакрытая %c: %s
 Обновление / установка...
 Параметры проверки (с -V или --verify): Требуется версия Записан: %s
 ДА Вы должны установить "%%_gpg_name" в вашем макрофайле
 [отсутствует] в конце массива ожидалась "]" поддерживается частичная установка набора жестких ссылок пакета. заданный аргумент не является пакетом RPM
 аргументы для --prefix должны начинаться с / аргументы для --root (-r) должны начинаться с / неверная дата в %%changelog: %s
 неверный параметр '%s' в %s:%d
 собрать двоичный пакет из <исходный пакет> собрать бинарный пакет по <файл спецификации> собрать двоичный пакет из <архив tar> собрать исходный и бинарный пакеты по <файл спецификации> собрать исходный и двоичный пакеты из <архив tar> собрать только исходный пакет по <файлу спецификации> собрать исходный пакет из <архив tar> выполнить по стадию %build (%prep, затем компиляция) из <файл спецификации> выполнить по стадию %build (%prep, затем компиляция) из <архив tar> выполнить по стадию %install (%prep, %build, затем установка)  из <исходный пакет> выполнить по стадию %install (%prep, %build, затем установка) из <файл спецификации> выполнить по стадию %install (%prep, %build, затем установка) из <архив tar> выполнить по стадию %prep (развернуть исходники и наложить заплаты) из <файл спецификации> выполнить по стадию %prep (развернуть исходники и наложить заплаты) из <архив tar> buildroot уже указан, %s игнорируется
 невозможно добавить запись (первоначально в %u)
 невозможно создать %s: %s
 невозможно получить блокировку %s на %s/%s
 не удалось открыть %s at %s:%d: %m
 невозможно открыть %s: %s
 не могу открыть базу данных Packages в %s
 невозможно повторно открыть payload: %s
 --prefix нельзя использовать с --relocate или --excludepath ошибка создания архива на файле %s: %s
 ошибка создания архива: %s
 создание канала для --pipe не удалось: %m
 отладка машины состояния файлов отладка машины состояния присоединенных файлов отладка процесса ввода/вывода rpmio определить MACRO со значением EXPR удалить подписи пакета различный каталог показать текущее значение rpmrc и макросов отобразить известные ключи запроса показать состояние перечисленных файлов игнорировать строки i18N из файла спецификации не исполнять %%post сценариев (если есть) не исполнять %%postun сценариев (если есть) не исполнять  %%pre сценариев (если есть) не исполнять %%preun сценариев (если есть) не исполнять %%triggerin сценариев не исполнять %%triggerpostun сценариев не исполнять %%triggerprein сценариев не исполнять %%triggerun сценариев не исполнять триггер-сценариев, взведенных этим пакетом не выполнять никаких этапов сборки не исполнять никаких сценариев пакета(ов) не устанавливать конфигурационные файлы не устанавливать документацию не менять порядок установки пакетов для удовлетворения зависимостей не проверять зависимости пакета перед сборкой не проверять зависимости пакета не проверять дисковое пространство перед установкой не исполнять сценарий(и) проверки не устанавливать, а только сообщить, удастся ли установка не проверять заголовки, извлекаемые из базы данных не проверять контрольные суммы файлов не проверять контрольные суммы файлов (устарело) не проверять файлы пакета не проверять группу файлов не проверять подпись заголовока и содержимого не проверять права доступа файлов пакета не проверять время модификации файлов не проверять хозяина файлов не проверять архитектуру пакета не проверять зависимости пакета не проверять дайджест пакета(ов) не проверять операционную систему пакета не проверять подпись(и) в пакете(ах) не проверять размер файлов не проверять путь символических ссылок показать основную информацию о файле пустой формат тэга пустое имя тэга стереть удалить (деинсталлировать) пакет ошибка создания временного файла %s: %m
 ошибка чтения из файла %s
 ошибка чтения заголовка из пакета
 ошибка(%d) резервирования памяти для образа нового пакета
 ошибка(%d) удаления записи %s из %s
 ошибка(%d) сохранения записи "%s" в %s
 ошибка (%d) сохранения записи #%d в %s
 ошибка:  исключения должны начинаться с / исключительный запуск не удался
 лишняя '(' в метке пакета: %s
 неудачно не удалось создать каталог невозможно создать каталог %s: %s
 перестроение базы данных не удалось, старая база данных остается на месте
 ошибка удаления каталога %s: %s
 невозможно заменить старую базу данных на новую!
 невозможно получить информацию о %s: %m
 не удалось записать все данные в %s: %s
 фатальная ошибка:  файл конфликт файла %s при попытках установки %s и %s файл %s из установленного пакета %s конфликтует с файлом из пакета %s файл %s не принадлежит ни одному из пакетов
 файл %s: %s
 алгоритм подсчёта контрольной суммы файла может быть настроен для каждого пакета имена файла(ов) хранятся в формате (dirName,baseName,dirIndex). файлы могут быть перемещены только при установке пакета не удалось выполнить gpg (%d)
 ошибка gpg при записи подписи
 группа %s не содержит никаких пакетов
 группа %s не существует - используется root
 заголовок номер %u в базе данных неверный -- пропускается.
 ярлыки заголовков всегда сортируются после загрузки. игнорировать ExcludeArch: в файле конфигурации игнорировать файловые конфликты между пакетами неверный тип подписи импортировать открытый ключ неполная запись %%changelog
 ошибка в формате: %s
 инициализировать базу данных установить устанавливать все файлы, даже конфигурационные, которые могли бы быть пропущены установить документацию установить пакет(ы) внутренняя поддержка для сценариев на языке lua. недопустимая зависимость недопустимая ширина поля неверный номер пакета: %s
 некорректный синтаксис в файле lua: %s
 некорректный синтаксис в скрипте lua: %s
 некорректный синтаксис в скриптлете lua: %s
 строка %d: %s
 строка %d: %s: %s
 строка %d: Неверный параметр %%setup %s: %s
 строка %d: Неверное число %s: %s
 строка %d: Неверное число %s: определяет: %s
 строка %d: Неверный формат BuildArchitecture: %s
 строка %d: Неверный аргумент для %%setup %s
 строка %d: Неверное число: %s
 строка %d: Неверный параметр %s: %s
 строка %d: Docdir должен начинаться с '/': %s
 строка %d: Пустой тэг: %s
 строка %d: Ошибка анализа %%description: %s
 строка %d: Ошибка разбора %%files: %s
 строка %d: Ошибка анализа %s: %s
 строка %d: неверный символ '%c' в: %s
 строка %d: недопустимый символ в: %s
 строка %d: неверная последовательность ".." в: %s
 строка %d: Неверный тэг: %s
 строка %d: Поддерживаются только noarch подпакеты: %s
 строка %d: Пакет не существует: %s
 строка %d: Префикс не может заканчиваться на "/": %s
 строка %d: Второе %s
 строка %d: Ярлык требует только один аргумент: %s
 строка %d: Слишком много имен: %s
 строка %d: Неизвестный тэг: %s
 строка %d: внутренний сценарий должен заканчиваться на '>': %s
 строка %d: Программы в сценариях должны начинаться с '/': %s
 строка %d: второй %%prep
 строка %d: второе %s
 строка %d: триггеры должны содержать --: %s
 строка %d: неподдерживаемый внутренний сценарий: %s
 строка: %s
 показать все файлы конфигурации показать все файлы документации показать список файлов пакета ошибка выполнения скрипта lua: %s
 Ошибка выполнения magic_load: %s
 Ошибка выполнения magic_open(0x%x): %s
 Невозможно прочесть %s: %s.
 memory alloc (%u bytes) returned NULL.
 miFreeHeader: пропуск отсутствует отсутствует '(' в метке пакета: %s
 отсутствует ')' в метке пакета: %s
 отсутствует ':' (найден 0x%02x) в %s:%d
 отсутствует архитектура для %s в %s:%d
 отсутствует название архитектуры в %s:%d
 отсутствует аргумент для %s в %s:%d
 пропущено имя в %%changelog
 отсутствует второе ':' в %s:%d
 отсутствует "{" после "%" отсутствует "}" после "%{" сетевой     не заданы аргументы не заданы аргументы запроса не заданы аргументы для проверки параметр dbpath не установлен параметр dbpath не установлен
 нет описания в %%changelog
 ни один пакет не подходит к %s: %s
 ни один из пакетов не предоставляет %s
 ни один из пакетов не требует %s
 ни один из пакетов не взводит триггер %s
 не заданы пакеты для установки не заданы пакеты для установки нормальный нормальный         не rpm-пакет не rpm-пакет
 не установлен не установлен  одновременно может выполняться только один тип проверки или запроса только установка и обновление могут быть выполнены принудительно может быть указан только один из основных режимов может быть использован только один из параметров --excludedocs или --includedocs невозможно открыть %s: %s
 переопределить build root переопределить целевую платформу пакет %s (который новее, чем %s) уже установлен пакет %s уже установлен пакет %s предназначен для архитектуры %s пакет %s предназначен для операционной системы %s пакет %s не установлен
 пакет %s уже был добавлен, заменяется %s
 пакет %s был уже добавлен, пропускаем %s
 пакет не содержит списков ни хозяев файлов, ни их ID
 пакет не содержит списков владельцев/групп-владельцев файлов
 имя-версия-выпуск пакета не предоставляется автоматически. содержимое пакета может быть сжато с использованием bzip2. содержимое пакета может быть сжато с использованием lzma. содержимое пакета может быть сжато с использованием xz. префикс "./" используется для фала(ов) содержимого пакета. скриптлеты пакета могут получать доступ к базе данных rpm во время установки. ошибка анализа выражения
 преопределить MACRO со значением EXPR предупреждать о взаимных зависимостях выводить "#" по мере установки пакета (хорошо с -v) вывести значение макроса EXPR выводить процент готовности по мере установки пакета вывести номер версии этой программы выводить минимум сообщений выводить более детальные сообщения запрос файла спецификации %s не удался, невозможно разобрать файл
 запросить пакеты с триггер-сценариями на пакет запросить/проверить заголовок запросить/проверить файл пакета запросить/проверить все пакеты запросить/проверить пакеты в группе запросить/проверить пакеты в группе запросить/проверить пакет, которому принадлежит файл запросить/проверить пакет, которому принадлежит файл запросить/проверить пакет(ы) по идентификатору пакета найти/проверить пакеты, предоставляющие сервис найти/проверить пакеты, требующие сервис использовать <FILE:...> вместо файла(ов) по умолчанию ошибка чтения: %s (%d)
 переиндексировать базу инвертированных списков из установленных заголовков пакетов невозможно прочитать запись %u
 переустановить, если пакет уже установлен переместить файлы из пути <old> в <new> переместить пакет в <каталог>, если пакет это позволяет перемещения должны начинаться с / перемещения должны содержать = перемещения должны иметь / после = удалить все пакеты, совпадающие с <пакет> (обычно, если <пакет> соответствует нескольким пакетам, генерируется ошибка) после завершения удалить дерево исходников после завершения удалить исходники после завершения удалить файл спецификации файлы в %s заменяются файлами из %s для восстановления перемещен замененный       режим проверки подписи режим запроса rpm режим проверки rpm rpmdb: получен поврежденный заголовок #%u -- пропускается.
 rpmdbNextIterator: пропуск параметры запрета сценариев могут быть указаны только при установке или удалении пакета послать стандартный вывод в CMD разделяемый сокращение для --replacepkgs --replacefiles подписать пакет(ы) подписать пакет (то же самое что --addsign) пропустить файлы %%ghost пропустить файлы в пути <путь>  перейти непосредственно к указанному этапу (только для c,i) пропущено исходный пакет не содержит файла спецификации
 обнаружен двоичный пакет вместо ожидаемого исходного
 синтаксическая ошибка в выражении
 синтаксическая ошибка при анализе &&
 синтаксическая ошибка при анализе ==
 синтаксическая ошибка при анализе ||
 интерпретатор сценариев может использовать аргументы из заголовка. транзакция параметры запрета триггеров могут быть указан только при установке или удалении пакета(ов) типы должны совпадать
 невозможно прочесть подпись
 неожиданная "]" неожиданные флаги запроса неожиданный формат запроса неожиданный источник запроса неожиданная "}" неизвестная ошибка %d при работе с пакетом %s неизвестный тэг неизвестный тэг: "%s"
 незакрытая (
 распаковка архива не удалась%s%s: %s
 неопознанный параметр базы данных: "%s" проигнорирован
 неподдерживаемая версия пакета RPM обновить базу данных, но не модифицировать файловую систему обновить пакет(ы) обновить пакет(ы) если уже установлен откат на более старую версию пакета (--force при обновлении делает это автоматически) использовать ROOT как корневой каталог использовать базу данных в КАТАЛОГЕ используйте следующий формат запроса пользователь %s не существует - используется root
 проверить раздел %files из <файл спецификации> проверить секцию %files из <архив tar> проверить файлы базы данных проверить подпись(и) пакета предупреждение:  неверный цвет    в выражении после ":" ожидалось "{" в выражении после "?" ожидалось "{" в конце выражения ожидался "|" в выражении ожидалось "}" PRIu64 installing package %s needs %%cB on the %s filesystem для установки пакета %s необходимо %%cБ в файловой системе %s 