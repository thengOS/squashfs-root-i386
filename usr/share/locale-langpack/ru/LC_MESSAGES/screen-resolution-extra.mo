��          |      �          &   !  '   H  A   p  >   �  >   �  +   0  9   \     �  6   �  �   �  v   �  �  D  K   �  F   9  �   �  �     n   �       �   �  4   *	  C   _	  �  �	    �                   
                                    	    Change Screen Resolution Configuration Change the effect of Ctrl+Alt+Backspace Changing the Screen Resolution configuration requires privileges. Changing the effect of Ctrl+Alt+Backspace requires privileges. Could not connect to Monitor Resolution Settings DBUS service. Enable or disable the NVIDIA GPU with PRIME Enabling or disabling the NVIDIA GPU requires privileges. Monitor Resolution Settings Monitor Resolution Settings can't apply your settings. Monitor Resolution Settings has detected that the virtual resolution must be set in your configuration file in order to apply your settings.

Would you like Screen Resolution to set the virtual resolution for you? (Recommended) Please log out and log back in again.  You will then be able to use Monitor Resolution Settings to setup your monitors Project-Id-Version: screen-resolution-extra
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-02-18 12:57+0000
PO-Revision-Date: 2013-12-18 07:01+0000
Last-Translator: Eugene Marshal <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:34+0000
X-Generator: Launchpad (build 18115)
 Изменить конфигурацию разрешения экрана Изменить реакцию на нажатие Ctrl+Alt+Backspace Изменение настроек разрешения требует определённых привилегий пользователя. Изменение реакции на нажатие Ctrl+Alt+Backspace требует определённых привилегий пользователя. Не могу подключится к службе Управления разрешением экрана. Задействовать или отключить графический процессор NVIDIA, используя PRIME Для включения или отключения NVIDIA GPU необходимы соответствующие привилегии. Настройка разрешения экрана Невозможно применить Ваши настройки Программа управления разрешением экрана обнаружила, что для применения Ваших настроек необходимо установить виртуальное разрешение в конфигурационном файле.

Вы хотите чтобы программа управления разрешением экрана установила виртуальное разрешение для Вас? (Рекомендуется) Пожалуйста, завершите текущий сеанс и авторизуйтесь заново. После этого программа управления разрешением экрана сможет настраивать Ваш монитор. 