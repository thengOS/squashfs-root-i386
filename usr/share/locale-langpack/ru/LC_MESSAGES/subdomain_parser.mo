��    =        S   �      8  "   9     \  <   |  >   �  $   �  2     B   P  2   �  >   �  G     =   M  :   �     �     �  $   �  Z        t     �  )   �     �  w   �     p	     �	  *   �	     �	  #   �	  !   
  "   5
      X
     y
     �
  )   �
  &   �
  $   �
     !  C   7  *   {      �     �  /   �  -        >  ;   M     �     �  !   �  $   �     �           /     P     h     w     �     �  -   �        9         Z     v  �  �  E   o  P   �  �     �   �  I   9  Y   �  o   �  o   M  t   �  �   2  j   �  �   I  1   �  "     K   &  �   r  /     1   5  U   g  4   �  "  �  *     &   @  <   g  D   �  B   �  C   ,  >   p  <   �  ?   �  1   ,  k   ^  Y   �  J   $  4   o  k   �  G     8   X  -   �  X   �  \     &   u  m   �     
  +   )  >   U  B   �  )   �  1      -   3   ,   a   $   �   ,   �   8   �   9   !  K   S!  4   �!  �   �!  B   W"  F   �"     4   "   3   5                    2   	               ;              %                            #   )   .                9   '   (      <   $   1         =   *   &         7   /                               8   
            :                          !           0   -   +          ,           6    !
Ensure that it has been loaded.
 %s: ASSERT: Invalid option: %d
 %s: Could not allocate memory for subdomainbase mount point
 %s: Errors found in combining rules postprocessing. Aborting.
 %s: Errors found in file. Aborting.
 %s: Failed to compile regex '%s' [original: '%s']
 %s: Failed to compile regex '%s' [original: '%s'] - malloc failed
 %s: Illegal open {, nesting groupings not allowed
 %s: Internal buffer overflow detected, %d characters exceeded
 %s: Regex grouping error: Invalid close }, no matching open { detected
 %s: Regex grouping error: Invalid number of items between {}
 %s: Sorry. You need root privileges to run this program.

 %s: Unable to add "%s".   %s: Unable to find  %s: Unable to parse input line '%s'
 %s: Unable to query modules - '%s'
Either modules are disabled or your kernel is too old.
 %s: Unable to remove "%s".   %s: Unable to replace "%s".   %s: Unable to write entire profile entry
 %s: Unable to write to stdout
 %s: Warning! You've set this program setuid root.
Anybody who can run this program can update your AppArmor profiles.

 %s: error near                %s: error reason: '%s'
 (ip_mode) Found unexpected character: '%s' Addition succeeded for "%s".
 AppArmor parser error, line %d: %s
 Assert: 'hat rule' returned NULL. Assert: `addresses' returned NULL. Assert: `netrule' returned NULL. Assert: `rule' returned NULL. Bad write position
 Couldn't copy profile Bad memory address
 Couldn't merge entries. Out of Memory
 ERROR in profile %s, failed to load
 Error: Out of Memory
 Exec qualifier 'i' invalid, conflicting qualifier already specified Exec qualifier 'i' must be followed by 'x' Found unexpected character: '%s' Memory allocation error. Network entries can only have one FROM address. Network entries can only have one TO address. Out of memory
 PANIC bad increment buffer %p pos %p ext %p size %d res %p
 Permission denied
 Profile already exists
 Profile does not match signature
 Profile doesn't conform to protocol
 Profile doesn't exist
 Removal succeeded for "%s".
 Replacement succeeded for "%s".
 Unable to open %s - %s
 Unknown error
 `%s' is not a valid ip address. `%s' is not a valid netmask. `/%d' is not a valid netmask. missing an end of line character? (entry: %s) ports must be between %d and %d profile %s: has merged rule %s with multiple x modifiers
 unable to create work area
 unable to serialize profile %s
 Project-Id-Version: apparmor-parser
Report-Msgid-Bugs-To: <apparmor@lists.ubuntu.com>
POT-Creation-Date: 2005-03-31 13:39-0800
PO-Revision-Date: 2016-04-13 10:49+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Novell Language <language@novell.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:13+0000
X-Generator: Launchpad (build 18115)
Language: ru
 !
Удостоверьтесь, что он был загружен.
 %s: ПРЕДУПРЕЖДЕНИЕ: Недопустимый параметр: %d
 %s: невозможно выделить память для точки монтирования базового поддомена
 %s: обнаружены ошибки при заключительной обработке объединения правил. Операция прерывается.
 %s: Обнаружены ошибки в файле. Прерываем.
 %s: Не удалось скомпилировать regex '%s' [прототип: '%s']
 %s: не удалось скомпилировать regex "%s" [прототип: "%s"] - ошибка malloc
 %s: Неверное открытие {, вложенная группировки не допускаются
 %s: Обнаружено переполнение внутреннего буфера, лишние %d знаков
 %s: Ошибка группировки Regex: Неверное закрытие }, обнаружено что нет соответствующего открытия {
 %s: Ошибка группировки Regex: Неверное число объектов между {}
 %s: Извините. Для запуска этой программы необходимы права администратора.

 %s: Невозможно добавить "%s".   %s: Неудалось найти  %s: невозможно обработать строку ввода "%s"
 %s: Невозможно опросить модули - '%s'
Или модули отключены или ядро слишком старое.
 %s: Невозможно удалить "%s".   %s: Невозможно заменить "%s".   %s: Невозможно записать запись профиля целиком
 %s: Невозможно записать в stdout
 %s: Предупреждение! Для этой программы задан идентификатор пользователя root.
Любой пользователь, запустивший эту программу, сможет обновить ваши профили AppArmor.

 %s: ошибка возле                %s: причина ошибки: "%s"
 (ip_mode) Найден неожиданный знак: '%s' Дополнение успешно выполнено для "%s".
 Ошибка анализатора AppArmor, строка %d: %s
 Предупреждение: "hat rule" возвращает NULL. Предупреждение: `addresses' вернуло NULL. Предупреждение: `netrule' вернуло NULL. Предупреждение: "rule" возвращает NULL. Неверное положение записи
 Невозможно скопировать профиль с неверным адресом памяти
 Невозможно объединить записи. Не хватает памяти
 ОШИБКА в профиле %s, не удалось загрузить
 Ошибка: Недостаточно памяти
 Недопустимый ключ запуска "i", уже задан конфликтующий ключ Ключ запуска 'i' должен следовать за by 'x' Найден непредвиденный знак: "%s" Ошибка выделения памяти. Сетевые записи могут иметь только один ОТ адрес. Сетевые записи могут иметь только один КОМУ адрес. Недостаточно памяти
 ТРЕВОГА! неправильный инкрементный буфер %p pos %p ext %p size %d res %p
 Доступ запрещен
 Профиль уже существует
 Профиль не соответствует подписи
 Профиль не соответствует протоколу
 Профиль не существует
 Удаление для "%s" выполнено.
 Замена для "%s" выполнена.
 Невозможно открыть %s - %s
 Неизвестная ошибка
 `%s' это не верный ip адрес. `%s' это не верная сетевая маска. `/%d' это не верная сетевая маска. отсутствует знак конца строки? (запись: %s) порты должны быть между %d и %d профиль %s: имеет объединенные правило %s с несколькими модификаторами x
 невозможно создать рабочую область
 невозможно сеарилизировать профиль %s
 