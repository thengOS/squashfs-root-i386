��    w     �  �  �      h     i     k     }     �      �     �     �     �  *          0      Q   #   e      �   "   �   ;   �   0   !  .   6!     e!  +   �!     �!  *   �!     �!  %   "     ,"     L"     e"     �"  $   �"     �"     �"  '   �"  &   #  !   C#  )   e#     �#  -   �#     �#     �#     $     $     )$  +   9$  $   e$     �$  	   �$  
   �$  9   �$  7   �$  .   %  )   L%  5   v%  )   �%  5   �%  3   &  /   @&  -   p&  :   �&  A   �&  <   '      X'  *   y'     �'  >   �'  )    (     *(     C(     V(  $   i(  $   �(     �(  +   �(  )   �(  '   )     C)     Y)  )   n)     �)     �)     �)  	   �)  
   �)  
   �)     �)     *  &   *     F*     \*  '   o*     �*  *   �*     �*     �*  .   +      E+     f+     �+     �+     �+     �+     �+     �+     	,     ,     0,     A,     [,     p,     �,     �,     �,     �,     �,     �,     -     +-     A-  =   Y-  -   �-     �-      �-     .  !    .  ;   B.  e   ~.     �.     �.  2   /  -   @/     n/     �/     �/     �/     �/     �/  *   0     00      P0  #   q0     �0     �0     �0  $   �0  �    1  (   �1     �1     �1     �1  S   2  U   c2     �2  &   �2     �2     3     23     J3     g3     �3     �3  -   �3  $   �3     4  0    4  "   Q4  &   t4  0   �4  /   �4     �4     5  =   .5  A   l5  =   �5  '   �5  +   6  &   @6     g6     ~6  "   �6  %   �6     �6     7     7     27  %   I7     o7     �7     �7  !   �7  *   �7     8      8     08     O8     j8     }8  3   �8  '   �8  I   �8  %   49  -   Z9  !   �9     �9     �9  "   �9  #   :     ,:  2   9:     l:     ~:     �:     �:  -   �:  !   �:     ;     ;     ,;  *   C;      n;  �  �;  0   G>  ,   x>  0   �>     �>     �>     �>  �    ?     �?     �?     �?     @     @  L   :@  ,   �@  -   �@  (   �@  	   A     A     2A     LA  8   aA     �A     �A     �A     �A     �A     B     B      =B     ^B  '   pB  %   �B  +   �B  �   �B     �C  !   �C     �C     �C  0   �C  &   D     ED  (   ^D  '   �D  ;   �D     �D     E  $   "E  :   GE  #   �E  $   �E     �E  )   �E  	   F  ,   F     LF     `F     sF     �F     �F      �F     �F     �F  &   �F  5   G  ,   QG     ~G  .   �G  (   �G     �G     H  "   H     ?H     NH     ]H  &   lH     �H     �H     �H     �H     �H     �H     �H      I     3I     AI     _I  G   ~I  /   �I  I   �I  h   @J     �J  1   �J     �J     K  .   K  =   MK  (   �K     �K  9   �K  $   L  *   1L  (   \L     �L  $   �L     �L     �L  !   �L      M     9M  *   PM     {M     �M     �M     �M     �M     �M     N     )N     FN  /   \N     �N     �N     �N     �N     �N     O     *O  
   DO     OO  @   oO     �O     �O     �O  >   P  !   BP     dP  "   pP     �P     �P     �P  "   �P  *   Q     7Q     PQ     lQ      �Q     �Q  0   �Q     �Q      R     /R     FR  !   ]R     R     �R  �   �R  #  �S     �U  "   �U  *   �U  "   V  @   ;V  "   |V  .   �V      �V  ?   �V  N   /W     ~W  4   �W  /   �W  8   X  }   ;X  L   �X  |   Y  8   �Y  J   �Y  -   Z  Y   5Z  '   �Z  O   �Z  ,   [  '   4[  1   \[  '   �[  6   �[  4   �[  "   "\  H   E\  1   �\  k   �\  ?   ,]  ?   l]  T   �]     ^  9   ^     R^     r^      �^  g   �^  C   _     T_     j_     |_  N   �_  U   �_  L   7`  >   �`  S   �`  >   a  S   Va  H   �a  D   �a  L   8b  X   �b  _   �b  Z   >c  >   �c  ?   �c  >   d  f   Wd  c   �d  %   "e     He  )   fe  I   �e  I   �e     $f  d   >f  i   �f  h   g  C   vg  #   �g  O   �g  0   .h  6   _h     �h     �h     �h     �h  "    i  @   #i  D   di  0   �i  -   �i  N   j  7   Wj  S   �j  6   �j  O   k  W   jk  D   �k  D   l  -   Ll  6   zl  6   �l  7   �l  '    m  6   Hm  0   m  *   �m  (   �m  I   n  H   Nn  E   �n  '   �n  6   o  0   <o  F   mo  :   �o  &   �o  D   p  A   [p  5   �p  �   �p  n   _q  K   �q  G   r  S   br  L   �r  s   s  �   ws     t  ?   >t  L   ~t  F   �t  '   u  '   :u  F   bu  &   �u  B   �u  Z   v  Q   nv  C   �v  I   w  I   Nw  ;   �w  2   �w     x  A   &x    hx  K   yy  5   �y  9   �y     5z  �   Lz  �   {  "   �{  H   �{  *   |  ;   @|  5   ||  3   �|  =   �|  ;   $}  7   `}  �   �}  B   -~  '   p~  L   �~  7   �~  h     f   �     �  )   m�     ��  n   ��  t   #�  m   ��  =   �  D   D�  h   ��  @   �  E   3�  H   y�  W     G   �  5   b�  2   ��  3   ˄  Q   ��  /   Q�  7   ��      ��  M   څ  P   (�  <   y�     ��  =   Ԇ  4   �  $   G�     l�  k   ��  [   �  �   N�  K   و  V   %�  Z   |�  B   ׉  L   �  J   g�  O   ��  -   �  ]   0�  &   ��  '   ��  %   ݋  +   �  N   /�  M   ~�  7   ̌  0   �  >   5�  i   t�  @   ލ  w  �  \   ��  _   ��  L   T�  #   ��  %   Ŕ  *   �  9  �  <   P�  ;   ��  G   ɖ  5   �  J   G�  �   ��  a   *�  [   ��  O   �     8�  4   D�  1   y�  $   ��  {   Й      L�     m�     ��  (   ��  <   ʚ  ;   �  >   C�  D   ��  -   Ǜ  H   ��  ;   >�  V   z�    ќ  +   �  :   �     N�  )   i�  J   ��  =   ޞ  '   �  ?   D�  =   ��  u     6   8�  .   o�  /   ��  W   Π  5   &�  ^   \�  E   ��  `   �     b�  W   r�  ,   ʢ  $   ��  ;   �  )   X�     ��  *   ��     ��  (   ף  C    �  \   D�  W   ��      ��  \   �  M   w�  0   ť  +   ��  B   "�      e�     ��     ��  U   ��     �     "�     B�  !   `�     ��  A   ��     �  @   ��     @�  =   Y�  >   ��  o   ֨  F   F�  �   ��  �   ;�  >   ��  j   3�  2   ��  )   ѫ  J   ��  a   F�  4   ��  *   ݬ  �   �  ;   ��  D   ԭ  >   �  =   X�  ]   ��  %   ��     �  Y   7�  -   ��  "   ��  ;   �     �  5   '�  >   ]�  <   ��  4   ٰ  9   �  B   H�  8   ��  $   ı  t   �  @   ^�  6   ��  J   ֲ  8   !�  G   Z�  )   ��  .   ̳     ��  <   �  �   S�  5   ۴  .   �  *   @�  �   k�  J   �     ]�  X   w�  8   ж  @   	�  o   J�  I   ��  S   �  6   X�  @   ��  Q   и  =   "�  @   `�  Z   ��  =   ��  H   :�  )   ��  <   ��  X   �  8   C�  D   |�  W  ��     �   V  �       }   J       ,   u           �       �   _                   _      D  F                        �   K       4   ^  �   8          �   �       �           w   �                           B   �   P      �       h  �   (  Q  .   ;  e  ]   �   1       4  a     �   '   J          c   �   �   r  3      �   e   #  L  ;   t        L   l   �   "       g   �     :   ]  d       d     �   +      �   �         >  �   �       <   [       �     �   =  �   �   Q           T   �   6                  	     -           @  �       �   p   5  k      �   N         P   �       u       �           b   �                  �   �       R  )  q  F   n      k   �   
   &  M  7   �   `   W      "      a  �   �   f   �   r   �           #   \   �   Z    �   �   �   �   N   i   2   �   �      j      .  q      b  p  �   �   E       n  �   <  �   �   �       >   m          2        s  W       $          h   O  0     v   K  H   �           �   �       8  o      M   [  g  �   B      !  o  �   6               +   y   %      �   �   �   ?       ^   �         t       �   �   f         �   1  
             I       9   5   /       �   \  �       �   E                    �               �      |   Y  *   =   )   S   A   �   �   �   �      Y   U           �   @   �          �   C         �   :  �   �   �   �   �                  �   �             s   �   �          �   i  ~         G  !       �   G   �   (   �               �         �   D   �   �         �   �       /           �   �      �   �                   �       R         �       �   �       X      c  %   �   �   &        -      O   I  z   m   �   `  �           	   C   9        �       v  S  �   �   *  �   �     �           �   {   �   �   Z   '  �   �   �   7      0  X          j           A      l  T  �       H  x   3   �   w          ,  ?      V   $       U     �    
 
Global options:
 
Valid options:
 
Warning: %s
  - The certificate has expired.
  removing '\r' from %s ... "%s": unknown command.

 '%s' already exists '%s' already exists and is not a directory '%s' does not appear to be a URL '%s' does not exist '%s' does not exist in revision %ld '%s' exists and is non-empty '%s' exists and is not a directory '%s' has local modifications -- commit or revert them first '%s' is a URL, but URLs cannot be commit targets '%s' is a reserved name and cannot be imported '%s' is an existing repository '%s' is neither a file nor a directory name '%s' is not a directory '%s' is not a directory in filesystem '%s' '%s' is not a file '%s' is not a file in filesystem '%s' '%s' is not a file or directory '%s' is not a local path '%s' is not a relative path '%s' is not a working copy '%s' is not a working copy directory '%s' is not a working copy root '%s' is not locked '%s' is not locked in this working copy '%s' is not the root of the repository '%s' is not under version control '%s' isn't in the same repository as '%s' '%s' locked by user '%s'.
 '%s' must be from the same repository as '%s' '%s' not found '%s' not found in filesystem '%s' path not found '%s' returned %d '%s' unlocked.
 'incremental' option only valid in XML mode 'verbose' option invalid in XML mode (no author) (no date) (no error) --allow-mixed-revisions cannot be used with --reintegrate --auto-props and --no-auto-props are mutually exclusive --depth and --set-depth are mutually exclusive --depth cannot be used with --reintegrate --diff-cmd and --internal-diff are mutually exclusive --force cannot be used with --reintegrate --message (-m) and --file (-F) are mutually exclusive --reintegrate cannot be used with --ignore-ancestry --reintegrate cannot be used with --record-only --relocate and --depth are mutually exclusive --relocate and --non-recursive (-N) are mutually exclusive --with-all-revprops and --with-no-revprops are mutually exclusive --with-revprop and --with-no-revprops are mutually exclusive -c and -r are mutually exclusive -r and -c can't be used with --reintegrate A checksum mismatch occurred A conflict in the working copy obstructs the current operation Aborting commit: '%s' remains in conflict Access to '%s' forbidden Adding         %s
 Adding  (bin)  %s
 Argument to --limit must be positive Argument to --strip must be positive At revision %ld.
 Attempt to change immutable filesystem node Attempt to remove or recreate fs root dir Attempted to lock an already-locked dir Authentication failed Authorization failed Bad parent pool passed to svn_make_pool() Bad property name Bad property name: '%s' Berkeley DB error Bogus URL Bogus UUID Bogus date Bogus mime-type Bogus server specification Can't change working directory to '%s' Can't close file '%s' Can't close stream Can't convert string from '%s' to '%s': Can't copy '%s' to '%s' Can't create an entry for a forbidden name Can't create directory '%s' Can't create symbolic link '%s' Can't create temporary file from template '%s' Can't find a temporary directory Can't find a working copy path Can't find an entry Can't get file name Can't make directory '%s' Can't move '%s' to '%s' Can't open '%s' Can't open directory '%s' Can't open file '%s' Can't open stderr Can't open stdin Can't read directory '%s' Can't read file '%s' Can't read stream Can't remove '%s' Can't remove directory '%s' Can't remove file '%s' Can't set permissions on '%s' Can't start process '%s' Can't write to '%s' Can't write to file '%s' Can't write to stream Cannot change node kind Cannot copy between two different filesystems ('%s' and '%s') Cannot copy path '%s' into its own child '%s' Cannot delete a file external Cannot move URL '%s' into itself Cannot move a file external Cannot move path '%s' into itself Cannot specify revisions (except HEAD) with move operations Certificate information:
 - Hostname: %s
 - Valid: from %s until %s
 - Issuer: %s
 - Fingerprint: %s
 Changed paths:
 Changelist doesn't match Changing directory '%s' is forbidden by the server Changing file '%s' is forbidden by the server Checked out revision %ld.
 Checkout complete.
 Checksum mismatch for '%s' Checksum: %s
 Client/server version mismatch Commit failed (details follow): Commit succeeded, but other errors follow: Conflict Current Base File: %s
 Conflict Previous Base File: %s
 Conflict Previous Working File: %s
 Conflict resolution failed Copied From Rev: %ld
 Copied From URL: %s
 Could not convert '%s' into a number Could not use external editor to fetch log message; consider setting the $SVN_EDITOR environment variable or using the --message (-m) or --file (-F) options Couldn't determine absolute path of '%s' Couldn't find a repository Couldn't open a repository Deleting       %s
 Describe the usage of this program or its subcommands.
usage: help [SUBCOMMAND...]
 Destination directory exists; please remove the directory or use --force to overwrite Directory '%s' is out of date Directory needs to be empty but is not Entry already exists Error calling external program Error closing directory Error closing directory '%s' Error closing filesystem Error getting UID of process Error parsing arguments Error unlocking locked dirs (details follow): Error writing stream: unexpected EOF Error writing to stream Expected '%s' to be a directory but found a file Failed to execute WebDAV PROPPATCH Failed to load module for FS type '%s' Failed to locate 'copyfrom' path in working copy Failed to revert '%s' -- try updating instead.
 File '%s' already exists File '%s' is out of date File already exists: filesystem '%s', revision %ld, path '%s' File already exists: filesystem '%s', transaction '%s', path '%s' File is not mutable: filesystem '%s', revision %ld, path '%s' File not found: revision %ld, path '%s' File not found: transaction '%s', path '%s' Filesystem directory has no such entry Filesystem has no item Filesystem has no such copy Filesystem has no such node-rev-id Filesystem has no such representation Filesystem has no such string Filesystem is already open Filesystem is corrupt Filesystem is not open First line of '%s' contains non-digit Found a working copy path General filesystem error HTTP Path Not Found Illegal parent directory URL '%s' Illegal target for the requested operation Incompatible library version Incomplete data Inconsistent line ending style Incorrect parameters given Input/output error Invalid URL '%s' Invalid URL: illegal character in proxy port number Invalid URL: negative proxy port number Invalid URL: proxy port number greater than maximum TCP port number 65535 Invalid argument '%s' in diff options Invalid character '%c' found in revision list Invalid character in hex checksum Invalid configuration value Invalid filesystem path syntax Invalid filesystem revision number Invalid filesystem transaction name Invalid lock Invalid operation on the current working directory Invalid option.

 Invalid relocation Invalid schedule Invalid switch Invalid syntax of argument of --config-option Item already exists in filesystem Last Changed Author: %s
 Last Changed Date Last Changed Rev: %ld
 Left locally modified or unversioned files Line endings other than expected List directory entries in the repository.
usage: list [TARGET[@REV]...]

  List each TARGET file and the contents of each TARGET directory as
  they exist in the repository.  If TARGET is a working copy path, the
  corresponding repository URL will be used. If specified, REV determines
  in which revision the target is first looked up.

  The default TARGET is '.', meaning the repository URL of the current
  working directory.

  With --verbose, the following fields will be shown for each item:

    Revision number of the last commit
    Author of the last commit
    If locked, the letter 'O'.  (Use 'svn info URL' to see details)
    Size (in bytes)
    Date and time of the last commit
 Local URL '%s' contains only a hostname, no path Local URL '%s' contains unsupported hostname Local URL '%s' does not contain 'file://' prefix Lock Created Lock Expires Lock Owner: %s
 Lock working copy paths or URLs in the repository, so that
no other user can commit changes to them.
usage: lock TARGET...

  Use --force to steal the lock from another user or working copy.
 MD5 checksum is missing Malformed network data Malformed skeleton data Malformed stream data Merge conflict during commit Mismatched FS module version for '%s': found %d.%d.%d%s, expected %d.%d.%d%s Moving a path from one changelist to another Name does not refer to a filesystem directory Name does not refer to a filesystem file Name: %s
 No external editor available No such XML tag attribute No such revision %ld No username is currently associated with filesystem '%s' Node Kind: directory
 Node Kind: file
 Node Kind: none
 Node Kind: unknown
 Not enough arguments given Not enough arguments provided Object is not a revision root Object is not a transaction root Obstructed update Operation does not apply to binary file Operation does not apply to directory Operation does not support multiple sources Output the content of specified files or URLs.
usage: cat TARGET[@REV]...

  If specified, REV determines in which revision the target is first
  looked up.
 Passphrase for '%s':  Password for '%s' GNOME keyring:  Password for '%s':  Path '%s' already exists Path '%s' already exists, but is not a directory Path '%s' contains '.' or '..' element Path '%s' does not exist Path '%s' does not exist in revision %ld Path '%s' doesn't exist in revision %ld Path '%s' is already locked by user '%s' in filesystem '%s' Path '%s' is not a directory Path '%s' is not in UTF-8 Path '%s' is not in the working copy Path '%s' must be an immediate child of the directory '%s' Path '%s' not found in revision %ld Path is not a working copy directory Path is not a working copy file Path syntax not supported in this context Path: %s
 Problem on first log entry in a working copy Problem running log Property not found Ran out of unique names Read error in pipe Reading '%s' Reading from stdin is disallowed Replacing      %s
 Repository Root: %s
 Repository URL required when importing Repository UUID '%s' doesn't match expected UUID '%s' Repository UUID does not match expected UUID Repository UUID: %s
 Repository access is needed for this operation Repository access method not implemented Repository has been moved Repository has no UUID Resolved conflicted state of '%s'
 Restored '%s'
 Reverted '%s'
 Revision: %ld
 Root object must be a transaction root SQLite error Schedule: add
 Schedule: delete
 Schedule: normal
 Schedule: replace
 Second revision required Sending        %s
 Server SSL certificate untrusted Skipped '%s'
 Skipped missing target: '%s'
 Source '%s' is not a directory Source and destination URLs appear not to point to the same repository. String does not represent a node or node-rev-id Subcommand '%s' doesn't accept option '%s'
Type 'svn help %s' for usage.
 Subversion is a tool for version control.
For additional information, see http://subversion.apache.org/
 Svndiff has invalid header Symbolic links are not supported on this platform Text Last Updated The SQLite db is busy The file '%s' changed unexpectedly during diff The following repository access (RA) modules are available:

 The operation is forbidden by the server The operation was interrupted The repository at '%s' has uuid '%s', but the WC has '%s' The root directory cannot be deleted The specified diff option is not supported The specified transaction is not mutable Too many arguments given Too many arguments to import command Transaction is out of date Transmitting file data  Try 'svnadmin help' for more info URL '%s' contains a '..' element URL '%s' doesn't exist URL '%s' refers to a file, not a directory URL: %s
 Unable to make name for '%s' Unable to open repository '%s' Unexpected EOF on stream Unexpected end of svndiff input Unexpected node kind found Unknown FS type '%s' Unknown authorization method Unknown svn_node_kind Unrecognized binary data encoding; can't decode Unrecognized line ending style Unrecognized stream data Unsupported checksum type Unsupported encoding '%s' Unsupported working copy format Update complete.
 Updated to revision %ld.
 Username:  Version file format not correct When specifying working copy paths, only one target may be given Working Copy Root Path: %s
 Working copy is corrupt Working copy is not up-to-date Working copy not locked; this is probably a bug, please report Working copy text base is corrupt Write error Wrong or unexpected property value XML data was not well-formed disable automatic properties do no interactive prompting do not cache authentication tokens do not print differences for deleted files don't unlock the targets enable automatic properties maximum number of log entries operate on single directory only print extra information read user configuration files from directory ARG show help on a subcommand show program version information specify a password ARG specify a username ARG try operation but make no changes use ARG as diff command use ARG as external editor use a different EOL marker than the standard
                             system marker for files with the svn:eol-style
                             property set to 'native'.
                             ARG may be one of 'LF', 'CR', 'CRLF' Project-Id-Version: svn
Report-Msgid-Bugs-To:  
POT-Creation-Date: 2016-03-14 07:37+0000
PO-Revision-Date: 2012-08-19 05:42+0000
Last-Translator: Vladimir Doroshenko <vovktt@gmail.com>
Language-Team: Русский язык <ubuntu-l10n-ru@lists.ubuntu.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 17:07+0000
X-Generator: Launchpad (build 18115)
 
 
Глобальные опции:
 
Допустимые параметры:
 
Предупреждение: %s
  - Срок действия сертификата истёк.
  удаление '\r' из %s ... "%s": неизвестная команда.

 '%s' уже существует '%s' уже существует, и это не каталог '%s' не является правильно сформированным URL '%s' не существует '%s' не существует в ревизии %ld '%s' существует и он не пуст '%s' существует, и это не каталог '%s' имеет локальные изменения -- сначала зафиксируйте или отмените их '%s' это URL, а URL не может быть целью фиксации '%s' является зарезервированным именем и не может быть импортировано '%s' — существующий репозиторий '%s' не является именем файла или каталога '%s' не является каталогом '%s' не является директорией в файловой системе '%s' '%s' не является файлом '%s' не является файлом в файловой системе '%s' '%s' — не файл или каталог '%s' - не локальный путь '%s' — не относительный путь '%s' — не рабочая копия '%s' — не каталог рабочей копии '%s' — не корень рабочей копии '%s' не заблокирован '%s' не заблокирован в этой рабочей копии '%s' — не корень репозитория '%s' не находится под контролем системы управления версиями '%s' не в том же репозитории, что и '%s' '%s' заблокирован пользователем '%s'.
 '%s' должен быть из того же репозитория, что и '%s' '%s' не найден '%s' не найден в файловой системе '%s' путь не найден '%s' вернул %d '%s' разблокирован
 параметр 'incremental' может использоваться только в режиме XML параметр 'verbose' неприменим в режиме XML (без автора) (нет даты) (без ошибок) --allow-mixed-revisions нельзя использовать с --reintegrate --auto-props и --no-auto-props являются взаимоисключающими --depth и --set-depth являются взаимоисключающими --depth нельзя использовать с --reintegrate --diff-cmd и --internal-diff являются взаимоисключающими --force нельзя использовать с --reintegrate --message (-m) и --file (-F) являются взаимоисключающими --reintegrate нельзя использовать с --ignore-ancestry --reintegrate нельзя использовать с --record-only --relocate и --depth  являются взаимоисключающими --relocate и --non-recursive (-N) являются взаимоисключающими --with-all-revprops и --with-no-revprops являются взаимоисключающими --with-revprop и --with-no-revprops являются взаимоисключающими -c и -r являются взаимоисключающими -r и -c нельзя использовать с --reintegrate Несоответствие контрольной суммы Конфликт в рабочей копии препятствует текущей операции Фиксация прервана: '%s' находится в состоянии конфликта Доступ к '%s' запрещён Добавляю          %s
 Добавляю  (бинарный)  %s
 Аргумент --limit должен быть положительным Аргумент --strip должен быть положительным В редакции %ld.
 Попытка изменить неизменяемый узел в файловой системе Попытка удалить или заново создать корневую директорию FS Попытка выполнить lock директории, которая уже содержит lock Не удалось выполнить аутентификацию Ошибка авторизации Неправильный параментр pool передан svn_make_pool() Неправильное имя свойства Некорректное имя свойства: '%s' Ошибка Berkeley DB Поддельный URL Поддельный UUID Поддельная дата Подддельный MIME-тип Неправильная спецификация сервера Не могу сменить текущий каталог на '%s' Не удалось закрыть файл '%s' Не удалось закрыть поток Не удалось преобразовать строку из '%s' в '%s': Не удалось скопировать '%s' в '%s' Нельзя создать элемент с недопустимым именем Не удалось создать каталог '%s' Не удалось создать символическую ссылку '%s' Не удалось создать временный файл из шаблона '%s' Не удалось создать временный каталог Не удается найти путь к рабочей копии Не удалось найти элемент Не удалось получить имя файла Не удалось создать каталог '%s' Не удалось переместить '%s' в '%s' Не удалось открыть '%s' Не удалось открыть каталог '%s' Не удалось открыть файл '%s' Невозможно открыть stderr Не удалось открыть stdin Не удалось выполнить чтение каталога '%s' Не удалось выполнить чтение из файла '%s' Не удалось выполнить чтение из потока Не удалось удалить '%s' Не удалось удалить каталог '%s' Не удалось удалить файл '%s' Не удалось задать права доступа для '%s' Не удалось запустить процесс '%s' Не могу записать в '%s' Не удалось выполнить запись в файл '%s' Не удалось выполнить запись в поток Невозможно изменить тип узла Нельзя выполнить копирование между различными файловыми системами ('%s' и '%s') Невозможно скопировать путь '%s' в его собственный потомок '%s' Нельзя удалить файл лежащий за пределами Невозможно переместить URL '%s' сам в себя Нельзя переместить файл лежащий за пределами Невозможно скопировать путь '%s' сам в себя При операции перемещения нельзя указывать редакции ( кроме HEAD ) Информация о сертификате:
 - Имя хоста: %s
 - Действителен: с %s по %s
 - Издатель: %s
 - Отпечаток: %s
 Изменённые пути:
 Список изменений не соответствует Изменение каталога '%s' запрещено сервером Изменение файла '%s' запрещено сервером Получена редакция %ld.
 Получение завершено.
 Несовпадение контрольной суммы для '%s' Контрольная сумма: %s
 Не совпадает версия клиента/сервера Фиксация не удалась (подробности приведены ниже): Фиксация удалась, но возникли другие ошибки: Конфликт с текущим базовым файлом: %s
 Конфликт с предыдущим базовым файлом: %s
 Конфликт с предыдущим рабочим файлом: %s
 Разрешение конфликта не удалось Скопировано из редакции: %ld
 Скопировано с: %s
 Не удалось преобразовать '%s' в число Не могу запустить внешний редактор для получения журнальной записи. Установите переменную $SVN_EDITOR или воспользуйтесь параметрами --message (-m) или --file (-F) Не удалось определить абсолютный путь '%s' Не удалось найти репозиторий Не удалось открыть репозиторий Удаляю       %s
 Получить информацию по использованию этой программы или её подкоманд.
использование: help [ПОДКОМАНДА]
 Указанный каталог существует. Удалите каталог или используйте ключ --force для перезаписи. Каталог '%s' устарел Каталог должен быть пустым, а он не пуст Элемент уже существует Ошибка вызова внешней программы Ошибка при закрытии каталога Ошибка закрытия каталога '%s' Ошибка закрытия файловой системы Ошибка при получении UID процесса Ошибка при разборе аргументов Не удалось разблокировать заблокированный каталог (подробности приведены ниже): Ошибка записи потока: неожиданный EOF Ошибка записи в поток Ожидалось, что '%s' — каталог, а найден файл Не удалось выполнить WebDAV PROPPATCH Не удалось загрузить модуль для типа файловой системы '%s' Не удалось найти путь откуда копировать в рабочей копии Не удалось восстановить ( revert ) '%s' -- вместо этого попробуйте обновить.
 Файл '%s' уже существует Файл '%s' устарел Файл уже существует: файловая система '%s', ревизия %ld,  путь '%s' Файл уже существует: файловая система '%s', транзакция '%s', путь '%s' Файл не изменяемый: файловая система '%s', ревизия '%ld', путь '%s' Файл не найден: ревизия %ld, путь '%s' Файл не найден: транзакция '%s', путь '%s' Директория файловой системы не содержит такого элемента Файловая система не имеет элемента Файловая система не имеет такой копии Файловая система не имеет такого node-rev-id Файловая система не имеет такого представления Файловая система не имеет такой строки Файловая система уже открыта Файловая система испорчена Файловая система не открыта Первая строка '%s' содержит нецифровой символ Найден путь рабочей копии Общая ошибка файловой системы HTTP  путь не найден Недопустимый URL родительского каталога '%s' Недопустимая цель для запрошенной операции Несовместимая версия библиотеки Неполные данные Несовместимый стиль концов строк Задан неправильный параметр Ошибка ввода/вывода Неверный URL '%s' Неправильный URL: недопустимый символ в номере порта прокси Неправильный URL: отрицательный номер порта прокси Неправильный URL: номер порта прокси больше максимального номера порта TCP 65535 Недопустимый аргумент '%s' в параметрах diff В списке ревизий найден недопустимый символ '%c' Недопустимый символ в 16-ричной контрольной сумме Неправильное значение конфигурации Неверный синтаксис пути файловой системы Неверный номер ревизии файловой системы Неверное имя транзакции в файловой системе Некорректная блокировка Неверная операция над текущей рабочей директорией Недопустимая опция.

 Неверное перемещение Неверное расписание Неверный переключатель Неправильный синтаксис аргумента --config-option Элемент уже существует в файловой системе Автор последнего изменения: %s
 Дата последнего изменения Редакция последнего изменения: %ld
 Оставлены локально измененные файлы или файлы без версий Концы строк не такие, как ожидалось Вывести список содержимого каталога репозитария.
использование: list [ЦЕЛЬ[@редакция]..]

  Вывести список из всех файлов и содержимого каталогов, перечисленных в аргументе ЦЕЛЬ, 
  получая данные из репозитария. 
  Если ЦЕЛЬ является частью рабочей копии, URL репозитария будет извлечен из неё. 
  Если указана,  будет выведено состояние элементов в определенной редакции

  Целью по-умолчанию является текущий каталог, из которого извлекается соответствующий URL репозитария

  Если указан параметр --verbose, кроме имени файла будут выведены следующие атрибуты:
    Редакция последней фиксации
    Автор последней фиксации 
    Если наложена блокировка, будет выведена буква 'O' ( используйте 'svn info' для просмотра подробностей )
    Размер ( в байтах )
    Дата и время последней фиксации
 Локальный URL '%s' содержит только имя хоста, без пути Локальный URL '%s' содержит неподдерживаемое имя хоста Локальный URL '%s' не содержит префикса 'file://' Блокировка создана Блокировка истекает Владелец блокировки: %s
 Заблокировать элемент рабочей копии или репозитария, чтобы никто другой
не мог изменить его.
использование: lock ЦЕЛЬ...

  Используйте --force, чтобы присвоить чужую блокировку
 Отсутствует контрольная сумма MD5 Искаженная сети передачи данных Неправильно сформирован скелет данных Некорректные данные в потоке Конфликт слияний во время подтверждения Не совпадает версия модуля файловой системы для '%s': найдено %d.%d.%d%s, ожидалось %d.%d.%d%s Перемещение пути из одного списка изменений в другой Имя не ссылается на директорию в файловой системе Имя не ссылается на файл в файловой системе Имя: %s
 Внешний редактор недоступен Нет такого XML атрибута тэга Нет такой ревизии %ld Нет пользователя связанного в данный момент с файловой системой '%s' Вид узла: каталог
 Вид узла: файл
 Вид узла: нет
 Вид узла: неизвестный
 Указано недостаточно аргументов Указано слишком мало аргументов Объект не является корнем ревизии Объект не является корнем транзакции Затрудненное обновление Операция неприменима к двоичному файлу Операция неприменима к каталогу Операция не поддерживает несколько источников Вывести содержимое указанных файлов или URL-адресов
использование: cat ЦЕЛЬ[@редакция]...

  если указано, выводится определенная редакция файла ( иначе HEAD )
 Парольная фраза для '%s':  Пароль для связки ключей GNOME '%s':  Пароль для '%s':  Путь '%s' уже существует Путь '%s' уже существует, но это не каталог Путь '%s' содержит элемент '.' или '..' Путь '%s' не существует Путь '%s' не существует в редакции %ld Путь '%s' не существует в ревизии %ld Путь '%s' уже заблокирован пользователем '%s' в файловой системе '%s' Путь '%s' не является каталогом Путь '%s' не в кодировке UTF-8 Путь '%s' не в рабочей копии Путь '%s' должен быть прямым потомком каталога '%s' Путь '%s' не найден в ревизии %ld Указанный путь не является каталогом рабочей копии Путь не является файлом рабочей копии Синтаксис пути не поддерживается в данном контексте Путь: '%s'
 Проблема в первом элементе лога в рабочей копии Проблемы обработки лога Свойство не найдено Превышен предел уникальных имен Ошибка чтения в канале Чтение '%s' Чтение из stdin запрещено Заменяю      %s
 Корень репозитория: %s
 Для импорта требуется URL репозитария UUID репозитория '%s' не совпадает с ожидавшимся UUID '%s' UUID репозитория не соотвествует ожидавшемуся UUID UUID репозитория: %s
 Для этого действия требуется доступ к репозиторию Метод доступа к репозиторию не реализован Репозиторий был перемещён Репозиторий не имеет UUID Устранено конфликтное состояние '%s'
 Восстановлено '%s'
 Возвращено '%s'
 Редакция: %ld
 Корневой объект должен быть корнем транзакции Ошибка SQLite Задано: добавить
 Задано: удалить
 Задано: нормально
 Задано: заменить
 Необходимо указать вторую редакцию Посылаю         %s
 Недоверенный SSL сертификат сервера Пропущено '%s'
 Пропущена отсутствующая цель: '%s'
 Источник '%s' не является каталогом URL источника и цели не указывают на один и тот же репозиторий. Строка не представляет узел или node-rev-id Подкоманда '%s' не принимает параметр '%s'
Введите 'svn help %s' для ознакомления с параметрами вызова.
 Subversion — инструмент для управления версиями.
Дополнительную информацию смотрите на http://subversion.apache.org/
 Svndiff имеет недопустимый заголовок Символические ссылки не поддерживаются на этой платформе Последнее изменение текста База данных SQLite занята Файл '%s' неожиданно изменился во время diff Доступны следующие модули доступа к репозитарию (RA):

 Операция запрещена сервером Операция была прервана Репозитарий по адресу '%s' имеет идентификатор '%s', но в рабочей копии записан '%s' Нельзя удалить корневой каталог Указанная опция diff не поддерживается Указанная транзакция неизменяема Указано слишком много аргументов Передано слишком много аргументов команде импорта Транзакция устарела Передаю данные  Для дополнительной информации наберите 'svnadmin help' URL '%s' содержит элемент '..' URL '%s' не существует URL '%s' ведет к файлу, а не каталогу URL: %s
 Не удалось создать имя для '%s' Не удалось открыть репозиторий '%s' Неожиданный конец файла в потоке Неожиданный конец ввода svndiff Обнаружен неожиданный тип узла Незивестный тип файловой системы '%s' Неизвестный метод авторизации Неизвестный svn_node_kind Неизвестная кодировка бинарных данных; невзможно декодировать Не распознанный стиль концов строк Нераспознанные данные потока Не поддерживаемый тип контрольный суммы Неподдерживаемая кодировка '%s' Неподдерживаемый формат рабочей копии Обновление завершено.
 Обновлено до редакции %ld.
 Пользователь:  Формат версии файла не корректен Когда указываются пути рабочей копии, может быть задан только один объект Путь к корню рабочей копии: %s
 Рабочая копия повреждена Рабочая копия устарела Рабочая копия не заблокирована. Наверное это ошибка в программе. Пожалуйста сообщите о ней Текстовая база рабочей копии повреждена Ошибка записи Неправильное или неожиданное значение свойства Данные XML неверно сформированы запретить автоматические свойства не запрашивать данные у пользователя в интерактивном режиме не сохранять аутентификационные данные не показывать изменения для удаленных файлов не снимать блокировку с целей разрешить автоматические свойства максимальное количество журнальных записей обрабатывать только один каталог вывести дополнительную информацию читать пользовательские настройки из каталога ARG показать подсказку по подкоманде показать информацию о версии программы использовать пароль ARG использовать имя пользователя ARG попробовать операцию, но не применять изменения использовать ARG, как команду diff использовать ARG, как внешний редактор использовать нестандартный маркер конца строки ( EOL )
                             для файлов со значением свойства svn:eol-style
                             'native'.
                             ARG может принимать значение 'LF', 'CR'или 'CRLF' 