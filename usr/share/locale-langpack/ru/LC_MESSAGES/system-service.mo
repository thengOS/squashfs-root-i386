��          �      �       0  %   1     W     s     �     �  "   �  1   �  3     .   J  7   y  0   �  -   �  �    V   �  I     N   a  M   �  R   �  t   Q  q   �  ~   8  x   �  �   0  �   �  �   G	                                         	             
    Check if the package system is locked Get current global keyboard Get current global proxy Set current global keyboard Set current global proxy Set current global proxy exception System policy prevents querying keyboard settings System policy prevents querying package system lock System policy prevents querying proxy settings System policy prevents setting global keyboard settings System policy prevents setting no_proxy settings System policy prevents setting proxy settings Project-Id-Version: ubuntu-system-service
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-11-04 00:35+0000
PO-Revision-Date: 2012-09-19 15:40+0000
Last-Translator: Илья Калитко <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:35+0000
X-Generator: Launchpad (build 18115)
 Проверьте, не заблокирована ли система пакетов Получить текущую глобальную клавиатуру Получить текущий глобальный прокси-сервер Установить текущую глобальную клавиатуру Установить текущий глобальный прокси-сервер Установить исключение для текущего глобального прокси-сервера Системная политика препятствует запросу настроек клавиатуры Системная политика препятствует запросу блокировки системы пакетов Системная политика препятствует запросу настроек прокси-сервера Системная политика препятствует установке глобальных параметров клавиатуры Системная политика препятствует установке параметров без прокси-сервера Системная политика препятствует установке параметров прокси-сервера 