��          �   %   �      P     Q  <   q  9   �  3   �       /   9     i     r     �  2   �  '   �          (  $   7     \  0   s  0   �     �     �          +     =     X     q     x  �  {  N   3  h   �  b   �  X   N  1   �  h   �     B	  \   O	  =   �	  d   �	  Q   O
  @   �
     �
  ?   �
  .   :  >   i  B   �  >   �  E   *  0   p  *   �  3   �                $                                            
                                                                   	              -- Press any key to continue -- An Ispell program was not given in the configuration file %s Are you sure you want to throw away your changes? (y/n):  Conversion of '%s' to character set '%s' failed: %s Error initialising libvoikko Error initializing character set conversion: %s File: %s Incomplete spell checker entry Missing argument for option %s Parse error in file "%s" on line %d, column %d: %s Parse error in file "%s" on line %d: %s Parse error in file "%s": %s Replace with:  Unable to open configuration file %s Unable to open file %s Unable to open file %s for reading a dictionary. Unable to open file %s for writing a dictionary. Unable to open temporary file Unable to set encoding to %s Unable to write to file %s Unknown option %s Unterminated quoted string \ at the end of a string aiuqxr yn Project-Id-Version: tmispell-voikko
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2009-02-07 18:46+0200
PO-Revision-Date: 2008-03-08 22:08+0000
Last-Translator: Александр AldeX Крылов <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:10+0000
X-Generator: Launchpad (build 18115)
 --- Для продолжения нажмите любую клавишу --- Программа lspell не была указана в конфигурационном файле %s Вы действительно желаете не сохранять изменения? (y/n):  Ошибка конвертирования '%s' в набор символов '%s': %s Ошибка инициализации libvoikko Ошибка инициализации конвертирования набора символов: %s Файл: %s Неполный элемент программы проверки произношения Отсутствует аргумент параметра %s Синтаксическая ошибка в файле "%s", строка %d, столбец %d: %s Синтаксическая ошибка в файле "%s", строка %d: %s Синтаксическая ошибка в файле "%s": %s Заменить на:  Не удалось открыть файл настроек %s Не удалось открыть файл %s Не удалось открыть файл словаря %s. Не удалось сохранить файл словаря %s. Не удалось открыть временный файл Не удалось установить кодировку как %s Не удалось записать файл %s Неизвестный параметр %s Незакрытые кавычки в строке \ в конце строки aiuqxr yn 