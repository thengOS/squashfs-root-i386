��    �      �  �   ,      �     �     �  "   �  	          (   >  #   g     �     �  &   �     �     �       *   %     P     m     u  9   |  )   �     �  "   �          3  /   F  +   v     �     �     �     �  #   �  #     %   '      M     n     �     �     �     �     �  W   �     H  "   e     �  &   �  "   �     �          $     =     V  !   u     �  '   �  "   �     �       !   -  #   O     s  !   �  %   �     �     �  !         "  &   =  5   d  *   �  =   �       (     %   >  %   d  0   �  &   �     �  /   �     $  >   6     u     z     �  3   �  ,   �     	          3     I     `     t  '   �     �     �     �     �  +   	     5     U     s     �     �     �     �     �     �  	   �     �  %     /   =     m     �     �  &   �     �  !   �           7      =   1   D   /   v      �   !   �      �   ,   �   T   !     s!  
   �!      �!     "     ,"     :"     G"     U"     h"     �"  4   �"  $   �"     �"     #     9#     W#     s#     �#  *   �#  :   �#     $      +$  )   L$     v$  #   �$  #   �$  0   �$     %     %  
   .%     9%     <%     P%     h%     �%     �%  %   �%     �%     �%     &     0&  &   J&     q&     s&     �&     �&     �&  4   �&     �&     '     '  �  
'     �(     �(  E   �(  �  )  ?   �/  ?   0  M   ]0  5   �0  1   �0  9   1  2   M1      �1  /   �1  T   �1  <   &2     c2     t2  x   �2  R   �2  (   Q3  E   z3  8   �3  ,   �3  �   &4  b   �4  ,   5  !   <5  (   ^5  6   �5  L   �5  L   6  E   X6  O   �6     �6     
7  &   %7  %   L7  #   r7  1   �7  �   �7  8   a8  O   �8  5   �8  J    9  L   k9  4   �9  /   �9  4   :  4   R:  R   �:  G   �:  S   ";  i   v;  g   �;  -   H<  1   v<  Y   �<  >   =  %   A=  I   g=  k   �=  =   >  8   [>  H   �>  :   �>  W   ?  Z   p?  X   �?  T   $@     y@  u   �@  G   A  G   NA  x   �A  a   B  -   qB     �B  0   C  �   PC     �C  -   �C  :   D  x   PD  5   �D  &   �D  (   &E  (   OE  )   xE  &   �E  '   �E  ?   �E  >   1F  C   pF  -   �F  !   �F  C   G  :   HG  4   �G  !   �G  &   �G  '   H  .   )H  ,   XH  /   �H  -   �H     �H  3   I  E   6I  d   |I  H   �I  :   *J     eJ  K   �J  6   �J  :   K  J   >K  	   �K     �K  c   �K  [   L     _L  9   sL  9   �L  ]   �L  �   EM  �   �M  !   �N  C   �N     O  !   3O  !   UO  !   wO  &   �O  >   �O  B   �O  `   BP  V   �P  @   �P  O   ;Q  =   �Q  6   �Q  F    R  2   GR  W   zR  s   �R  T   FS  G   �S  \   �S  >   @T  T   T  S   �T  D   (U  $   mU  &   �U     �U     �U  ,   �U  8   V  G   :V  >   �V  6   �V  T   �V  8   MW  R   �W  R   �W  :   ,X  l   gX     �X     �X  /   �X  +   &Y     RY  4   pY     �Y     �Y     �Y        V         g   c      �   M          p   �       )      q           y   �   U   Y   �   W          �      �   0   t   ?   F   S   (   H   %       E   C   L   [   �       �   �   �   �   ,   �   O   ;   �   e   �   7   B          3   +   N   �              b      f   	                  j   }   �   �      �       �               h                           5          �               m   .   z   _   s   #   \       I   �   �   '   2   A       -   r   *   �       n           �   k   �   R   &   Q   "       G   �   �           !              x   u   �          `   6   $   �   1   v            @                 �   D   �   l       �   d   <   :                 ^   =   �           �                   �   o   X   �          �   Z   8   
   4      P       >   �   �       J   ]   �              {   �   i   9   �               /   �   T   �   a           �       w   ~   �            K   �       |       �   �   �        
 
(None) 
Error applying application rules. 
Usage: %(progname)s %(command)s

%(commands)s:
 %(enable)-31s enables the firewall
 %(disable)-31s disables the firewall
 %(default)-31s set default policy
 %(logging)-31s set logging to %(level)s
 %(allow)-31s add allow %(rule)s
 %(deny)-31s add deny %(rule)s
 %(reject)-31s add reject %(rule)s
 %(limit)-31s add limit %(rule)s
 %(delete)-31s delete %(urule)s
 %(insert)-31s insert %(urule)s at %(number)s
 %(route)-31s add route %(urule)s
 %(route-delete)-31s delete route %(urule)s
 %(route-insert)-31s insert route %(urule)s at %(number)s
 %(reload)-31s reload firewall
 %(reset)-31s reset firewall
 %(status)-31s show firewall status
 %(statusnum)-31s show firewall status as numbered list of %(rules)s
 %(statusverbose)-31s show verbose firewall status
 %(show)-31s show firewall report
 %(version)-31s display version information

%(appcommands)s:
 %(applist)-31s list application profiles
 %(appinfo)-31s show information on %(profile)s
 %(appupdate)-31s update %(profile)s
 %(appdefault)-31s set default application policy
  (skipped reloading firewall)  Attempted rules successfully unapplied.  Some rules could not be unapplied. %s is group writable! %s is world writable! '%(f)s' file '%(name)s' does not exist '%s' already exists. Aborting '%s' does not exist '%s' is not writable (be sure to update your rules accordingly) : Need at least python 2.6)
 Aborted Action Added user rules (see 'ufw status' for running firewall): Adding IPv6 rule failed: IPv6 not enabled Available applications: Backing up '%(old)s' to '%(new)s'
 Bad destination address Bad interface name Bad interface name: can't use interface aliases Bad interface name: reserved character: '!' Bad interface type Bad port Bad port '%s' Bad source address Cannot insert rule at position '%d' Cannot insert rule at position '%s' Cannot specify 'all' with '--add-new' Cannot specify insert and delete Checking ip6tables
 Checking iptables
 Checking raw ip6tables
 Checking raw iptables
 Checks disabled Command '%s' already exists Command may disrupt existing ssh connections. Proceed with operation (%(yes)s|%(no)s)?  Could not back out rule '%s' Could not delete non-existent rule Could not find '%s'. Aborting Could not find a profile matching '%s' Could not find executable for '%s' Could not find profile '%s' Could not find protocol Could not find rule '%d' Could not find rule '%s' Could not get listening status Could not get statistics for '%s' Could not load logging rules Could not normalize destination address Could not normalize source address Could not perform '%s' Could not set LOGLEVEL Could not update running firewall Couldn't determine iptables version Couldn't find '%s' Couldn't find parent pid for '%s' Couldn't find pid (is /proc mounted?) Couldn't open '%s' for reading Couldn't stat '%s' Couldn't update application rules Couldn't update rules file Couldn't update rules file for logging Default %(direction)s policy changed to '%(policy)s'
 Default application policy changed to '%s' Deleting:
 %(rule)s
Proceed with operation (%(yes)s|%(no)s)?  Description: %s

 Duplicate profile '%s', using last found ERROR: this script should not be SGID ERROR: this script should not be SUID Firewall is active and enabled on system startup Firewall not enabled (skipping reload) Firewall reloaded Firewall stopped and disabled on system startup Found exact match Found multiple matches for '%s'. Please use exact profile name From IPv6 support not enabled Improper rule syntax Improper rule syntax ('%s' specified with app rule) Insert position '%s' is not a valid position Invalid '%s' clause Invalid 'from' clause Invalid 'port' clause Invalid 'proto' clause Invalid 'to' clause Invalid IP version '%s' Invalid IPv6 address with protocol '%s' Invalid interface clause Invalid log level '%s' Invalid log type '%s' Invalid option Invalid policy '%(policy)s' for '%(chain)s' Invalid port with protocol '%s' Invalid ports in profile '%s' Invalid position ' Invalid position '%d' Invalid profile Invalid profile name Invalid token '%s' Logging disabled Logging enabled Logging:  Missing policy for '%s' Mixed IP versions for 'from' and 'to' Must specify 'tcp' or 'udp' with multiple ports Need 'from' or 'to' with '%s' Need 'to' or 'from' clause New profiles: No rules found for application profile Option 'log' not allowed here Option 'log-all' not allowed here Port ranges must be numeric Port: Ports: Profile '%(fn)s' has empty required field '%(f)s' Profile '%(fn)s' missing required field '%(f)s' Profile: %s
 Profiles directory does not exist Protocol mismatch (from/to) Protocol mismatch with specified protocol %s Resetting all rules to installed defaults. Proceed with operation (%(yes)s|%(no)s)?  Resetting all rules to installed defaults. This may disrupt existing ssh connections. Proceed with operation (%(yes)s|%(no)s)?  Rule added Rule changed after normalization Rule deleted Rule inserted Rule updated Rules updated Rules updated (v6) Rules updated for profile '%s' Skipped reloading firewall Skipping '%(value)s': value too long for '%(field)s' Skipping '%s': also in /etc/services Skipping '%s': couldn't process Skipping '%s': couldn't stat Skipping '%s': field too long Skipping '%s': invalid name Skipping '%s': name too long Skipping '%s': too big Skipping '%s': too many files read already Skipping IPv6 application rule. Need at least iptables 1.4 Skipping adding existing rule Skipping inserting existing rule Skipping malformed tuple (bad length): %s Skipping malformed tuple: %s Skipping unsupported IPv4 '%s' rule Skipping unsupported IPv6 '%s' rule Status: active
%(log)s
%(pol)s
%(app)s%(status)s Status: active%s Status: inactive Title: %s
 To Unknown policy '%s' Unsupported action '%s' Unsupported default policy Unsupported direction '%s' Unsupported policy '%s' Unsupported policy for direction '%s' Unsupported protocol '%s' WARN: '%s' is world readable WARN: '%s' is world writable Wrong number of arguments You need to be root to run this script n problem running problem running sysctl problem running ufw-init
%s running ufw-init uid is %(uid)s but '%(path)s' is owned by %(st_uid)s unknown y yes Project-Id-Version: ufw
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-17 18:51-0600
PO-Revision-Date: 2016-06-06 22:12+0000
Last-Translator: Eugene Marshal <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:31+0000
X-Generator: Launchpad (build 18115)
 
 
(Отсутствует) 
Ошибка применения правил приложения. 
Использование: %(progname)s %(command)s

%(commands)s:
 %(enable)-31s включает брандмауэр
 %(disable)-31s отключает брандмауэр
 %(default)-31s задаёт политику по умолчанию
 %(logging)-31s задаёт уровень регистрации событий: %(level)s
 %(allow)-31s добавить разрешение %(rule)s
 %(deny)-31s добавить запрещение %(rule)s
 %(reject)-31s добавить отказ %(rule)s
 %(limit)-31s добавить ограничение %(rule)s
 %(delete)-31s удалить %(urule)s
 %(insert)-31s вставить %(urule)s в %(number)s
 %(route)-31s добавить маршрут %(urule)s
 %(route-delete)-31s удалить маршрут %(urule)s
 %(route-insert)-31s вставить маршрут %(urule)s в %(number)s
 %(reload)-31s повторно загрузить брандмауэр
 %(reset)-31s выполнить сброс брандмауэра
 %(status)-31s показать состояние брандмауэра
 %(statusnum)-31s показать состояние брандмауэра в виде пронумерованного списка %(rules)s
 %(statusverbose)-31s показать подробное состояние брандмауэра
 %(show)-31s показать отчёт брандмауэра
 %(version)-31s показаться сведения о версии

%(appcommands)s:
 %(applist)-31s перечислить профили приложения
 %(appinfo)-31s показать сведения %(profile)s
 %(appupdate)-31s обновить %(profile)s
 %(appdefault)-31s задать политику приложения по умолчанию
  (пропущен перезапуск брандмауэра)  Пробные правила отменены успешно.  Некоторые правила не могут быть отменены. %s доступен для записи группе! %s доступен для записи всем! «%(f)s» файл «%(name)s» не существует «%s» уже существует. Пропуск «%s» не существует «%s» недоступен для записи (не забудьте соответственно обновить правила) : Необходим python версии не ниже 2.6)
 Прервано Действие Добавленные правила пользователя (см. 'ufw status' межсетевого экрана): Сбой при добавлении правила IPv6: IPv6 не включён Доступные приложения: Резервное копирование «%(old)s» в «%(new)s»
 Недопустимый адрес назначения Неверное имя интерфейса Неверное имя интерфейса: невозможно использовать псевдонимы интерфейса Неверное имя интерфейса: зарезервированный символ: '!' Неверный тип интерфейса Недопустимый порт Недопустимый порт «%s» Недопустимый адрес источника Не удалось вставить правило в позицию «%d» Не удалось добавить правило в позицию «%s» Нельзя указывать «all» вместе с «--add-new» Не удалось распознать вставку или удаление Проверка ip6tables
 Проверка iptables
 Проверка рядов ip6tables
 Проверка рядов iptables
 Проверки выключены Команда «%s» уже существует Команда может разорвать существующие соединения ssh. Продолжить операцию (%(yes)s|%(no)s)?  Не удалось вернуть правило «%s» Невозможно удалить несуществующее правило Не удалось найти «%s». Пропуск Не найдено профилей соответствующих «%s» Не удалось найти исполняемый файл для «%s» Не удалось найти профиль «%s» Не удалось найти протокол Не удалось найти правило «%d» Не удалось найти правило «%s» Не удалось получить состояние прослушивания Не удалось получить статистику для «%s» Не удалось загрузить правила ведения журнала Не удалось привести адрес назначения к стандартному виду Не удалось привести адрес источника к стандартному виду Не удалось выполнить «%s» Не удалось установить LOGLEVEL Не удалось обновить запущенный межсетевой экран Не удалось определить версию iptables Не удалось найти «%s» Не удалось найти родительский pid для «%s» Не удалось найти pid (убедитесь, что каталог /proc смонтирован) Не удалось открыть «%s» для чтения Не удалось получить статус «%s» Не удалось обновить правила приложения Не удалось обновить файл правил Не удалось обновить файл правил журналирования Правило по умолчанию %(direction)s изменено на «%(policy)s»
 Правило приложения по умолчанию изменено на «%s» Удаление:
 %(rule)s
Продолжить операцию (%(yes)s|%(no)s)?  Описание: %s

 Профиль «%s» дублируется, будет использован последний найденный Ошибка: этот сценарий не должен быть SGID Ошибка: этот сценарий не должен быть SUID Межсетевой экран включён и будет запускаться при запуске системы Межсетевой экран не включён (перезагрузка пропущена) Брандмауэр перезагружен Межсетевой экран отключён и не будет запускаться при запуске системы Найдено точное совпадение Найдены множественные совпадения для «%s». Используйте точное название профиля Из Поддержка IPv6 не включена Недопустимый синтаксис правила Неверный синтаксис правила («%s» указан с правилом для приложения) Неверная позиция вставки «%s» Неверное условие «%s» Неверное условие «from» Неверное условие «port» Неверное условие «proto» Неверное условие «to» Неверная версия IP «%s» Неверный адрес IPv6 с протоколом «%s» Неверное расположение интерфейса Неверный уровень журналирования «%s» Неверный тип журнала «%s» Неверный параметр Неверное правило «%(policy)s» для «%(chain)s» Неверный порт для протокола «%s» Неверные порты в профиле «%s» Неверная позиция ' Неверная позиция «%d» Неправильный профиль Неправильное имя профиля Некорректный символ «%s» Журналирование отключено Журналирование включено Журналирование:  Отсутствует правило для «%s» Указаны разные версии IP для «from» и «to» При нескольких портах должно быть указано «tcp» или «udp» Необходимо указать «from» или «to» для «%s» Необходимо условие «to» или «from» Новые профили: Не найдено правил для профиля приложения Параметр 'log' здесь недопустим Параметр 'log-all' здесь недопустим Интервалы портов должны быть численными Порт: Порты: Профиль «%(fn)s» содержит пустое обязательное поле «%(f)s» В профиле «%(fn)s» пропущено обязательное поле «%(f)s» Профиль: %s
 Каталог профилей не существует Несоответствие протокола (из/в) Несоответствие протокола с указанным протоколом %s Сброс всех правил к значениям по умолчанию. Продолжить операцию (%(yes)s|%(no)s)?  Сброс всех правил к значениям по умолчанию. Это может разорвать существующие соединения ssh. Продолжить операцию (%(yes)s|%(no)s)?  Правило добавлено Правило изменено после нормализации Правило удалено Правило вставлено Правило обновлено Правила обновлены Правила обновлены (v6) Правила для профиля «%s» обновлены Пропущена перезагрузка брандмауэра Пропуск «%(value)s»: значение слишком велико для «%(field)s»: Пропуск «%s»: информация уже находится в /etc/services Пропуск «%s»: невозможно обработать Пропуск «%s»: невозможно собрать статистику Пропуск «%s»: поле слишком большое Пропуск «%s»: недопустимое имя Пропуск «%s»: слишком длинное имя файла Пропуск «%s»: слишком велико Пропуск «%s»: слишком много файлов уже прочитано Пропуск правила приложения IPv6. Требуется iptables версии не ниже 1.4 Пропуск добавления уже существующего правила Пропуск вставки существующего правила Пропуск некорректных элементов (неверная длина): %s Пропуск некорректных элементов: %s Пропуск не поддерживаемого правила «%s» для IPv4 Пропуск неподдерживаемого правила «%s» для IPv6 Состояние: активен
%(log)s
%(pol)s
%(app)s%(status)s Состояние: активен%s Состояние: неактивен Заголовок: %s
 В Неизвестное правило «%s» Неподдерживаемое действие «%s» Неподдерживаемое правило по умолчанию Неподдерживаемое направление «%s» Неподдерживаемое правило «%s» Неподдерживаемое правило для направления «%s» Неподдерживаемый протокол «%s» Предупреждение: «%s» доступен для чтения всем Предупреждение: «%s» доступен для записи всем Неверное количество аргументов Для запуска этого сценария требуются права администратора n проблема запуска ошибка при выполнении sysctl проблема запуска ufw-init
%s выполнение ufw-init uid %(uid)s, но «%(path)s» занят %(st_uid)s неизвестный y да 