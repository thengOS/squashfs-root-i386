��    +      t  ;   �      �     �     �     �  "      '   #  -   K     y     �  $   �      �     �  =     o   L  B   �     �  $     $   >     c  "   �  /   �     �  ,   �  '     #   >  )   b  !   �  ?   �     �  .     <   7  9   t     �  +   �     �  +   	  ,   >	     k	  ,   	     �	  ?   �	     
     
  ,  /
  2   \  -   �  #   �  N   �  g   0  E   �  *   �  <   	  7   F  B   ~  Q   �  }     �   �  �   ?  /   �  B   �  D   <  %   �  >   �  y   �  '   `  �   �  I     Y   \  <   �  J   �  �   >  "   �  �     �   �  }   !  0   �  U   �  *   &  \   Q  H   �  (   �  f      #   �  ~   �  8   *  @   c        *   %   	                        !      #   "                                  )          '                  $       +                          (                  &                    
                 All upgrades installed Allowed origins are: %s An error occurred: '%s' Cache has broken packages, exiting Cache lock can not be acquired, exiting Download finished, but file '%s' not there?!? Error message: '%s' GetArchives() failed: '%s' Giving up on lockfile after %s delay Initial blacklisted packages: %s Installing the upgrades failed! Lock could not be acquired (another package manager running?) No '/usr/bin/mail' or '/usr/sbin/sendmail',can not send mail. You probably want to install the 'mailx' package. Package '%s' has conffile prompt and needs to be upgraded manually Package installation log: Packages that are auto removed: '%s' Packages that attempted to upgrade:
 Packages that were upgraded:
 Packages that will be upgraded: %s Packages with upgradable origin but kept back:
 Progress: %s %% (%s) Running unattended-upgrades in shutdown mode Simulation, download but do not install Starting unattended upgrades script The URI '%s' failed to download, aborting Unattended upgrade returned: %s

 Unattended-upgrade in progress during shutdown, sleeping for 5s Unattended-upgrades log:
 Unclean dpkg state detected, trying to correct Upgrade in minimal steps (and allow interrupting with SIGINT Warning: A reboot is required to complete this upgrade.

 Writing dpkg log to '%s' You need to be root to run this application dpkg --configure -a output:
%s dpkg returned a error! See '%s' for details dpkg returned an error! See '%s' for details error message: '%s' make apt/libapt print verbose debug messages package '%s' not upgraded package '%s' upgradable but fails to be marked for upgrade (%s) print debug messages print info messages Project-Id-Version: unattended-upgrades 0.77
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-02-21 04:07+0000
PO-Revision-Date: 2015-12-14 10:10+0000
Last-Translator: Aleksey Kabanov <Unknown>
Language-Team: Russian <debian-l10n-russian@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 17:52+0000
X-Generator: Launchpad (build 18115)
Language: ru
 Все обновления установлены Разрешённые источники: %s Возникла ошибка: '%s' В кэше сломанные пакеты, завершение работы Не удалось выполнить блокировку кэша, завершение работы Скачивание завершено, но файла %s нет?!? Сообщение об ошибке: '%s' GetArchives() завершилась с ошибкой: '%s' Отказ по lockfile после %s задержки Пакеты в чёрном списке изначально: %s Установка обновлений завершилась с ошибкой! Не удалось выполнить блокировку (запущен ещё один менеджер пакетов?) Нет '/usr/bin/mail' или '/usr/sbin/sendmail', невозможно отправить почту. Вероятно, нужно установить пакет 'mailx'. В пакете %s есть conffile с вводом от пользователя и его нужно обновлять вручную Журнал установки пакетов: Автоматически удаляемые пакеты: «%s» Пакеты, которые пытались обновиться:
 Обновлённые пакеты:
 Пакеты, которые будут обновлены: %s Пакеты, обновлённые в источнике, но оставленные теми же в системе:
 Ход выполнения: %s %% (%s) Выполнение автоматического обновления системы в режиме завершения работы Имитация, скачивать но не устанавливать Запускаются сценарии необслуживаемой установки Невозможно скачать URI '%s', останов Результат необслуживаемой установки: %s

 Выполняется автоматическое обновление системы в режиме завершения работы, приостановление на 5 с. Журнал unattended-upgrades:
 Обнаружено состояние незавершённости работы dpkg, попытаемся исправить Обновление с минимальным количеством этапов (можно прервать с помощью SIGINT Предупреждение: для завершения обновления требуется перезагрузка.

 Журнал dpkg записывается в %s Вы должны быть root для запуска этого приложения Результат dpkg --configure -a:
%s dpkg завершилась с ошибкой! Подробности смотрите в %s dpkg вернул ошибку! Подробнее смотрите '%s' сообщение об ошибке: %s установить для apt/libapt вывод подробных сообщений отладки пакет %s не обновлён пакет %s можно обновить, но его не удалось пометить как обновляемый (%s) выводить отладочные сообщения выводить информационные сообщения 