��          \      �       �   	   �   
   �      �      �   3   �   �   ,  !   �  �       �     �  *   �       �   /  r  �  D   4                                       Calculate Calculator Open in calculator Result Sorry, there are no results that match your search. This is an Ubuntu search plugin that enables Calculator results to be displayed in the Dash underneath the Info header. If you do not wish to search this content source, you can disable this search plugin. calculator;result;calculate;calc; Project-Id-Version: unity-scope-calculator
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-28 09:22+0000
PO-Revision-Date: 2014-04-11 09:12+0000
Last-Translator: ☠Jay ZDLin☠ <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:07+0000
X-Generator: Launchpad (build 18115)
 Рассчитать Калькулятор Открыть в калькуляторе Результат К сожалению, результаты, соответствующие вашим параметрам поиска, отсутствуют. Это поисковый модуль Ubuntu, который позволяет отображать результаты Калькулятора в главном меню под заголовком Информация. Если вы не хотите осуществлять поиск в данном ресурсе, отключите данный модуль. калькулятор;результат;рассчитать;calc; 