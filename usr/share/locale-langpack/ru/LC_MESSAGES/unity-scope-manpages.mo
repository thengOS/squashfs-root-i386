��    	      d      �       �      �      �      �      �   4        H  �   \     D  �  R  %   �       7   .  2   f  �   �  /   <  j  l     �        	                                      Manpages Open Search Manpages Search for Manpages Sorry, there are no Manpages that match your search. Technical Documents This is an Ubuntu search plugin that enables information from local manpages to be searched and displayed in the Dash underneath the Code header. If you do not wish to search this content source, you can disable this search plugin. manpages;man; Project-Id-Version: unity-scope-manpages
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-24 16:55+0000
PO-Revision-Date: 2014-04-11 09:23+0000
Last-Translator: ☠Jay ZDLin☠ <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:23+0000
X-Generator: Launchpad (build 18115)
 Справочные страницы Открыть Искать в Справочных страницах Искать Справочные страницы К сожалению, справочные страницы, соответствующие вашим параметрам поиска, отсутствуют. Техническая документация Это поисковый модуль Ubuntu, который позволяет выводить информацию из локальных справок в главном меню под заголовком Код. Если вы не хотите осуществлять поиск в данном ресурсе, отключите этот модуль. manpages;man; 