��    s      �  �   L      �	  1  �	     �
       &        >  ,   F     s     �     �  �   �  �   M            	        (  F   0  *   w  :   �     �     �            ,   :     g     s  &   �  &   �  !   �     �     �       O   4  .   �     �     �     �     �     �  �     A   �     �  K     "   c      �     �     �     �     �          "  %   B  G   h     �  2  �  O   �     B     S     _     k  #   �  "   �     �  8   �  '     )   C     m     �     �     �  2   �  0   �     "     B     V  (   l  �   �  I   B  ;   �  "   �  �   �  ,   �  Q   �  �         �  A     ,   H     u     �     �     �     �  Y   �  O   ,  4   |     �  A   �  
   �       �     V   �  D   �  @   3  P   t  A   �  L     "   T     w  	   �     �     �     �     �  
   �     �  6  �  ^  !  /   w#  	   �#  E   �#  &   �#  Y   $  !   x$     �$     �$  �   �$  �   �%  8   �&     �&     �&     '  |   '  H   �'  �   �'  "   x(     �(  /   �(  L   �(  G   2)     z)  ,   �)  G   �)  B    *  6   C*     z*  0   �*  3   �*  �   �*  K   x+     �+  C   �+  !   ,  ;   =,  *   y,    �,  �   �-     @.  �   V.  Q   �.  K   ,/  H   x/  =   �/  G   �/  /   G0  !   w0  8   �0  g   �0  S   :1  &   �1  U  �1  �   4  -   �4     �4     �4  =   5  ?   N5  P   �5  2   �5  [   6  {   n6  �   �6  +   l7  )   �7  )   �7  0   �7  S   8  v   q8  :   �8  %   #9  '   I9  V   q9  A  �9  �   
;  �   �;  C   2<    v<  k   �=  �   �=  �  �>  I   7@  �   �@  �   A  )   �A  9   �A  !   �A  #   B  '   CB  �   kB  �   C  �   �C     ;D  v   PD     �D     �D  �   �D  �   �E  i   �F  |   �F  b   uG  p   �G  �   IH  O   �H     BI     aI     wI  (   �I     �I  +   �I  "   
J     -J        +                  $   W   7          k   '                @   g   K      9   R           3      6       D   C               <   .   f      L   l       	   Q   X       e          s      Z   ?   N   P   &   F          4           a   B   ]   [               (                 "   p   ,   c   b   1   U   j   5       /   A   :          #   G      E      d       
   _          S      h   -      n              J       m   \   M       `               2       )       >   T       0      i   *   %   o   ;   V   8             r   !   ^       =       I   q                            H   Y      O    
A normal upgrade can not be calculated, please run:
  sudo apt-get dist-upgrade


This can be caused by:
 * A previous upgrade which didn't complete
 * Problems with some of the installed software
 * Unofficial software packages not provided by Ubuntu
 * Normal changes of a pre-release version of Ubuntu %(size).0f kB %(size).0f kB %.1f MB %i obsolete entries in the status file %s base %s needs to be marked as manually installed. %s will be downloaded. .deb package A file on disk An unresolvable problem occurred while calculating the upgrade.

Please report this bug against the 'update-manager' package and include the following error message:
 An unresolvable problem occurred while initializing the package information.

Please report this bug against the 'update-manager' package and include the following error message:
 Building Updates List Cancel Changelog Changes Changes for %s versions:
Installed version: %s
Available version: %s

 Check if a new Ubuntu release is available Check if upgrading to the latest devel release is possible Checking for updates… Connecting... Copy Link to Clipboard Could not calculate the upgrade Could not initialize the package information Description Details of updates Directory that contains the data files Do not check for updates when starting Do not focus on map when starting Download Downloading changelog Downloading list of changes... Failed to download the list of changes. 
Please check your Internet connection. However, %s %s is now available (you have %s). Install Install All Available Updates Install Now Install missing package. Installing updates… It is impossible to install or remove any software. Please use the package manager "Synaptic" or run "sudo apt-get install -f" in a terminal to fix this issue at first. It’s safer to connect the computer to AC power before updating. No longer downloadable: No network connection detected, you can not download changelog information. No software updates are available. Not all updates can be installed Not enough free disk space Obsolete dpkg status entries Obsolete entries in dpkg status Open Link in Browser Other updates Package %s should be installed. Please wait, this can take some time. Remove lilo since grub is also installed.(See bug #314004 for details.) Restart _Later Run a partial upgrade, to install as many updates as possible.

    This can be caused by:
     * A previous upgrade which didn't complete
     * Problems with some of the installed software
     * Unofficial software packages not provided by Ubuntu
     * Normal changes of a pre-release version of Ubuntu Run with --show-unsupported, --show-supported or --show-all to see more details Security updates Select _All Settings… Show all packages in a list Show all packages with their status Show and install available updates Show debug messages Show description of the package instead of the changelog Show supported packages on this machine Show unsupported packages on this machine Show version and exit Software Updater Software Updates Software index is broken Software updates are no longer provided for %s %s. Some software couldn’t be checked for updates. Support status summary of '%s': Supported until %s: Technical description Test upgrade with a sandbox aufs overlay The changelog does not contain any relevant changes.

Please use http://launchpad.net/ubuntu/+source/%s/%s/+changelog
until the changes become available or try again later. The computer also needs to restart to finish installing previous updates. The computer needs to restart to finish installing updates. The computer will need to restart. The list of changes is not available yet.

Please use http://launchpad.net/ubuntu/+source/%s/%s/+changelog
until the changes become available or try again later. The software on this computer is up to date. The update has already been downloaded. The updates have already been downloaded. The upgrade needs a total of %s free space on disk '%s'. Please free at least an additional %s of disk space on '%s'. Empty your trash and remove temporary packages of former installations using 'sudo apt-get clean'. There are no updates to install. This update does not come from a source that supports changelogs. To stay secure, you should upgrade to %s %s. Unimplemented method: %s Unknown download size. Unsupported Unsupported:  Update is complete Updated software has been issued since %s %s was released. Do you want to install it now? Updated software is available for this computer. Do you want to install it now? Updated software is available from a previous check. Updates Upgrade using the latest proposed version of the release upgrader Upgrade… Version %s: 
 When upgrading, if kdelibs4-dev is installed, kdelibs5-dev needs to be installed. See bugs.launchpad.net, bug #279621 for details. You are connected via roaming and may be charged for the data consumed by this update. You have %(num)s packages (%(percent).1f%%) supported until %(time)s You have %(num)s packages (%(percent).1f%%) that are unsupported You have %(num)s packages (%(percent).1f%%) that can not/no-longer be downloaded You may not be able to check for updates or download new updates. You may want to wait until you’re not using a mobile broadband connection. You stopped the check for updates. _Check Again _Continue _Deselect All _Partial Upgrade _Remind Me Later _Restart Now… _Try Again updates Project-Id-Version: update-manager
Report-Msgid-Bugs-To: sebastian.heinlein@web.de
POT-Creation-Date: 2016-04-12 03:58+0000
PO-Revision-Date: 2016-04-12 12:20+0000
Last-Translator: Launchpad Translations Administrators <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Launchpad-Export-Date: 2016-06-27 17:19+0000
X-Generator: Launchpad (build 18115)
Language: ru
 
Обновление не может быть выполнено в обычном режиме, пожалуйста, выполните в терминале:
  sudo apt-get dist-upgrade


Возможные причины:
 * Предыдущее обновление не завершилось
 * Проблемы с установленным программным обеспечением
 * Установка неофициальных пакетов, не поддерживаемых Ubuntu
 * Обычные изменения предварительной версии выпуска Ubuntu %(size).0f КБ %(size).0f КБ %(size).0f КБ %.1f МБ Устаревшие записи %i в файле состояния Базовые компоненты %s %s должен быть помечен как установленный вручную. %s будет загружено. .deb пакет Файл на диске При расчёте обновления произошла неразрешимая ошибка.

Пожалуйста, сообщите об ошибке в пакете  'update-manager', включив следующий отчёт:
 При работе с пакетом возникла неразрешимая ошибка.

Пожалуйста, сообщите об этой ошибке пакета «update-manager» и включите это сообщение:
 Формируется список обновлений Отмена Список изменений Изменения Изменения для %s версий:
Установленная версия: %s
Доступная версия: %s

 Проверить, доступен ли новый выпуск Ubuntu Проверка возможности обновления до последней нестабильной версии дистрибутива Поиск обновлений... Подключение... Копировать ссылку в буфер Не удалось рассчитать обновление системы Не удалось получить сведения о пакетах Описание Сведения об обновлениях Каталог, который содержит файлы данных Не проверять обновления при запуске Не выбирать карту при запуске Загрузка Загрузка списка изменений Загрузка списка изменений... Ошибка при загрузке списка изменений. 
Проверьте ваше сетевое соединение. Доступен выпуск %s %s (сейчас установлен %s). Установка Установить все доступные обновления Установить сейчас Установить отсутствующий пакет. Установка обновлений... Установка или удаление программ невозможна. Для исправления этой ситуации используйте менеджер пакетов Synaptic или запустите в терминале «sudo apt-get install -f». Безопаснее подключить компьютер к источнику питания перед обновлением. Недоступны: Не обнаружено сетевое соединение, невозможно скачать список изменений. Отсутствуют какие-либо обновления программ. Не все обновления могут быть установлены Недостаточно свободного места на диске Устарелое содержимое статусов dpkg Устаревшие записи в файле состояния dpkg Открыть ссылку в браузере Другие обновления Необходимо установить пакет %s. Пожалуйста, подождите. Это может занять некоторое время. Удалите lilo, если grub уже установлен. (См. bug #314004) Перезагрузить _позже Запустите частичное обновление, чтобы установить столько обновлений, сколько возможно.

    Возможные причины:
     * Предыдущее обновление не завершилось
     * Проблемы с установленным программным обеспечением
     * Установка неофициальных пакетов, не поддерживаемых Ubuntu
     * Обычные изменения предварительной версии выпуска Ubuntu Выполните с использованием --show-unsupported, --show-supported или --show-all для просмотра подробных сведений Обновления безопасности В_ыделить всё Настройки... Показать все пакеты в виде списка Показать все пакеты и их состояние Показать и установить доступные обновления Показать сообщения отладки Показать описание пакета вместо списка изменений Показать поддерживаемые пакеты, установленные на данный компьютер Показать неподдерживаемые пакеты, установленные на данном компьютере Показать версию и выйти Обновление приложений Обновление приложений Индекс программ повреждён Для %s %s обновления больше не предоставляются. Не удалось проверить наличие обновлений для некоторых программ. Сводка состояния поддержки «%s»: Поддерживаются до %s: Техническое описание Протестировать обновление в безопасном режиме Список изменений не содержит связанных с пакетом записей.

Пожалуйста, используйте http://launchpad.net/ubuntu/+source/%s/%s/+changelog
пока не станет доступен список изменений или попробуйте позже. Чтобы завершить установку предыдущих обновлений, необходимо перезагрузить компьютер. Необходимо перезагрузить компьютер для завершения установки обновлений. Необходимо перезагрузить компьютер. Список изменений пока не доступен.

Пожалуйста, посмотрите http://launchpad.net/ubuntu/+source/%s/%s/+changelog
пока изменения не станут доступны или попробуйте еще раз позднее. Отсутствуют какие-либо обновления для данного компьютера. Обновление уже загружено. Обновления уже загружены. Обновления уже загружены. Для обновления требуется %s свободного места на диске «%s». Пожалуйста, освободите по крайней мере %s дискового пространства на «%s». Очистите вашу корзину и удалите временные пакеты установок, выполнив в терминале команду «sudo apt-get clean». Нет доступных обновлений для установки. Это обновление поставляется источником, не поддерживающим списки изменений. Чтобы обеспечить безопасность компьютера, следует обновить %s до выпуска %s. Невыполненный метод: %s Загружаемый размер неизвестен. Не поддерживаются Не поддерживаются:  Обновление завершено С момента выхода %s %s были выпущены обновлённые версии программ. Установить их сейчас? Для этого компьютера доступны обновления программного обеспечения. Установить их сейчас? Обновлённое программное обеспечение доступно с момента последней проверки. Обновления Обновление с использованием последней версии Центра обновлений Обновить… Версия %s: 
 При обновлении, необходимо установить kdelibs5-dev, если установлен kdelibs4-dev. Это ошибка, о которой можно прочитать здесь - bugs.launchpad.net, bug #279621 Вы находитесь в роуминге и с вас может взиматься плата за данные, передаваемые при обновлении. Обнаружено %(num)s пакетов (%(percent).1f%%), поддерживаемых до %(time)s Здесь установлено %(num)s пакетов (%(percent).1f%%), которые не поддерживаются Обнаружено %(num)s пакетов (%(percent).1f%%), которые недоступны Нет возможности проверить наличие обновлений или скачать их. Возможно, вы захотите подождать, пока не используете мобильное широкополосное подключение. Вы остановили проверку наличия обновлений. _Проверить снова _Продолжить Сн_ять выделение _Частичное обновление _Напомнить позже _Перезагрузить сейчас... _Попробовать снова обновления 