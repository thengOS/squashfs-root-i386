��    v     �  �  |      H  (   I     r     �      �     �  $   �           	      &      A      S   F   g   F   �   B   �   7   8!  1   p!  @   �!  ?   �!  F   #"  �   j"  ,   �"  J   #  &  e#  �   �$  F   �%  <   �%  7   &  H   D&  1   �&  >   �&  ?   �&  <   >'  -   {'  1   �'  ,   �'     (     (  4   5(     j(     �(  ,   �(     �(  %   �(  ,   )  -   <)      j)  &   �)     �)     �)     �)     �)     *     *     *     6*     L*     \*     r*  ,   y*  !   �*  Z   �*  7   #+  *   [+  -   �+  S   �+  <   ,  S   E,  +   �,  Q   �,  !   -     9-  4   Q-  *   �-  ,   �-     �-     �-     .     .     ,.     @.  W   S.     �.     �.  	   �.  A   �.  9   #/     ]/     z/     �/     �/  0   �/     �/     �/     0  1   0  7   N0     �0     �0  	   �0  &   �0     �0  /   �0  #   1  %   ;1  .   a1  #   �1     �1     �1     �1     2     -2     =2     T2     \2     h2     n2  "   �2  ?   �2  ?   �2  5   ,3  G   b3     �3  (   �3  '   �3  (   4  :   C4  ?   ~4  ;   �4  I   �4     D5     ^5     y5  2   �5  &   �5  %   �5  &   6  '   >6     f6  1   �6  :   �6  1   �6  M   %7  ?   s7  1   �7  2   �7  2   8  >   K8  )   �8  0   �8  2   �8  -   9     F9  4   ]9  &   �9  .   �9  \   �9  X   E:     �:     �:  #   �:     �:  [   �:  @   [;  6   �;  R   �;  >   &<  1   e<  B   �<      �<  !   �<  "   =     @=      `=  *   �=  $   �=     �=     �=     >  L   >     b>  &   }>  >   �>  4   �>  ,   ?     E?     V?     ]?  2   }?  .   �?  ,   �?  %   @  6   2@  /   i@  '   �@  4   �@     �@     �@     A     A  ,   *A  +   WA     �A     �A  4   �A  6   �A     B     7B     GB     VB  ,   pB  -   �B  '   �B  &   �B  8   C     SC     kC  %   �C     �C     �C     �C     �C     �C     �C  (   D     ?D  ;   VD  ;   �D  :   �D     	E  0   E  0   ?E     pE  *   wE  +   �E     �E     �E  #   F  ]   %F  �   �F     #G  ,   8G  2   eG  %   �G     �G  N   �G     H     7H     FH  $   VH     {H     �H     �H     �H     �H     �H     �H     �H  $   I  -   =I     kI     {I     �I     �I  ;   �I  %   �I     J  '   &J  %   NJ  +   tJ  '   �J     �J     �J     �J     �J     �J     K     $K  (   =K  ^   fK  q   �K  <   7L  '   tL  L   �L     �L  ,   �L     M     -M     2M     NM     \M  "   yM     �M     �M  L   �M  =   N  )   VN  �   �N  !   BO     dO     pO     �O     �O     �O     �O  <   �O  '   P  9   0P  2   jP  0   �P  2   �P  &   Q  0   (Q  1   YQ  ?   �Q  ?   �Q  C   R  5   OR  6   �R  8   �R  I   �R  =   ?S  6   }S  ;   �S  L   �S  N   =T  K   �T  L   �T  .   %U  &   TU  ?   {U     �U  7   �U  "   V  3   &V  -   ZV  &   �V  V   �V  *   W  ,   1W  W   ^W  /   �W  &   �W  /   X  .   =X  ,   lX  1   �X  o   �X  
   ;Y     FY     YY     ^Y     cY     yY     �Y     �Y     �Y     �Y     �Y     �Y     �Y  F   �Y      Z     3Z     GZ     MZ     eZ     �Z     �Z     �Z     �Z     �Z     �Z     �Z  �  �Z  F   �\  .   �\  (   #]  6   L]     �]  -   �]     �]  (   �]  <   �]  -   6^  ,   d^  d   �^  p   �^  u   g_  x   �_  =   V`  �   �`  j   =a  u   �a  �   b  ?   �b  w   "c  �  �c  �  \e  s   �f  Z   hg  s   �g  w   7h  X   �h  m   i  y   vi  f   �i  S   Wj  Z   �j  <   k  "   Ck      fk  -   �k  4   �k  ,   �k  Z   l  5   rl  M   �l  Z   �l  Y   Qm  8   �m  N   �m  5   3n  5   in     �n     �n  +   �n     �n  2   �n  8   o     Qo  8   qo     �o  s   �o  X   2p  �   �p  n   fq  K   �q  j   !r  �   �r  e   s  �   {s  Y   ;t  �   �t  B   Ru  7   �u  s   �u  D   Av  T   �v  #   �v  <   �v     <w  !   Nw  '   pw     �w  �   �w  6   �x  D   �x  )   �x  �   )y  n   �y  P   z  '   lz  9   �z     �z  N   �z     5{  (   G{  >   p{  W   �{  [   |     c|     {|     �|  g   �|  9   }  b   V}  D   �}  _   �}  z   ^~  ;   �~  H     :   ^  :   �  F   �  .   �  (   J�     s�     ��     ��  .   ��  P   ߀  �   0�  x   ��  j   0�  �   ��  K   9�  R   ��  R   ؃  T   +�  b   ��  ^   �  Z   B�  �   ��  2   '�  4   Z�  :   ��  s   ʆ  H   >�  M   ��  O   Շ  O   %�  7   u�  _   ��  i   �  w   w�  �   �  u   ��  z   #�  }   ��  I   �  U   f�  H   ��  h   �  I   n�  D   ��  0   ��  t   .�  L   ��  l   ��  �   ]�  �   �  7   ̐  0   �  N   5�     ��  �   ��  h   K�  R   ��     �  c   ��  Q   �  f   =�  G   ��  H   �  C   5�  @   y�  B   ��  Z   ��  O   X�  ;   ��  3   �  $   �  �   =�  *   җ  H   ��  w   F�  m   ��  T   ,�     ��     ��  @   ��  q   �  �   Z�  [   �  I   @�  I   ��  K   ԛ  O    �  a   p�     Ҝ     �  8   �  7   *�  i   b�  i   ̝  6   6�     m�  Z   �  t   ڞ  V   O�  :   ��     �  A    �  f   B�  g   ��  f   �  Q   x�  �   ʡ  D   V�  @   ��  :   ܢ     �     2�     @�  4   L�  6   ��  2   ��  V   �      B�  o   c�  o   Ӥ  [   C�     ��  _   ��  |   �     ��  I   ��  n   ߦ  6   N�  2   ��  F   ��  �   ��  �   ר  "   ��  :   �  @   �  .   ^�     ��  �   ��  *   s�  &   ��      ū  r   �  "   Y�  "   |�  "   ��  5   ¬  "   ��  ?   �     [�  '   x�  K   ��  Y   �     F�     d�  <   z�  >   ��  �   ��  l   ��  ;   ��  k   0�  Y   ��  g   ��  @   ^�     ��     ��  %   ��  )   �     �  *   .�  (   Y�  >   ��  �   ��  �   g�  j   (�  <   ��  �   д     `�  T   o�     ĵ     ߵ  ;   �  #   "�  ;   F�  J   ��  8   Ͷ  4   �  �   ;�  a   ��  O   ]�  9  ��  J   �     2�     J�     g�     y�     ��  $   ��  _   ˺  1   +�  z   ]�  �   ػ  w   \�  p   Լ  W   E�  �   ��  a   �  {   ��  �   ��  �   ��  }   /�  m   ��  �   �  �   ��  �   B�  }   ��  �   M�  �   ��  �   ��  �   2�  �   ��  c   }�  D   ��  �   &�     ��  �   ��  <   `�  [   ��  V   ��  L   P�  �   ��  M   d�  S   ��  �   �  ^   ��  \   �  k   t�  j   ��  k   K�  d   ��  �   �     ��  6   �  
   <�     G�  9   T�     ��     ��  
   ��  0   ��  %   ��     �     &�     .�  ?   M�     ��      ��     ��  >   ��  0   �  @   K�     ��  !   ��     ��  0   ��  0   �     =�     �   �   %  7  �   /       '              1  _  R   [  r      g          �     �   $                 *   �             D  q   .      }   H          y   �   B   �      �   �   �   �       Q   �           J  �   x   :  �   �     �   �       �   v     p   �         n  �       >         �          W   G          Y       B  ]      T    %   �       @  J   s   l                 �       \                C          R  �     �   �   �                            �   �   d       Y            =        ;  L                    (  �   5   1       \       �   �   5                  �       �       >              K   �   d      H       ,              "       �   �   <  �   @   �   '   n   `   [   �       X  =   k   V      E   j    �   O  A   �   �      i  S   A  �   U   �   Q  �   �   2      �   p      N   s      3   �   8   c               �     �   �   �   �       �   o   !   �   _                      �   u   �   �      0  �   ?      4  6   �   �   {   l  X       �   �   �               -  �   �             E    �     b  q  9   �     0   �           �   �   4   N  +          �       �     V      v   /  �   �       �       *  W  I         �   S    	   K    �   F      �      �   +   t   �   z   �   I  h  O   F  8    D   �   �   �   )          3      �           f  C      
   M       e      �   ]       �   �   r   o  P       a          e   �   �   �   &  Z                   )       L  �   a      �   
    �                   G   �      �   |       <   �   ^  T   M  �   �             U        �      �   Z     t  �   	  �   �   k  "  �   j   m       �   �   ^   w             �   �   &   �   �   :   m  2   ;       -   ?   ,   ~       �   9  !      (   �   $      �   �   �   b   �       �   c           �   �   �   7     �   #              i       �      6      `  P  u  .   f       h       g   #  �   �        	-V Output version information and exit
 	Average bitrate: %.1f kb/s

 	Elapsed time: %dm %04.1fs
 	Encoding [%2dm%.2ds so far] %c  	Rate:         %.4f
 	[%5.1f%%] [%2dm%.2ds remaining] %c  
 
	File length:  %dm %04.1fs
 

Done encoding file "%s"
 

Done encoding.
 
Audio Device:   %s   --audio-buffer n        Use an output audio buffer of 'n' kilobytes
   -@ file, --list file    Read playlist of files and URLs from "file"
   -K n, --end n           End at 'n' seconds (or hh:mm:ss format)
   -R, --remote            Use remote control interface
   -V, --version           Display ogg123 version
   -Z, --random            Play files randomly until interrupted
   -b n, --buffer n        Use an input buffer of 'n' kilobytes
   -d dev, --device dev    Use output device "dev". Available devices:
   -f file, --file file    Set the output filename for a file device
                          previously specified with --device.
   -h, --help              Display this help
   -k n, --skip n          Skip the first 'n' seconds (or hh:mm:ss format)
   -l s, --delay s         Set termination timeout in milliseconds. ogg123
                          will skip to the next song on SIGINT (Ctrl-C),
                          and will terminate if two SIGINTs are received
                          within the specified timeout 's'. (default 500)
   -o k:v, --device-option k:v
                          Pass special option 'k' with value 'v' to the
                          device previously specified with --device. See
                          the ogg123 man page for available device options.
   -p n, --prebuffer n     Load n%% of the input buffer before playing
   -q, --quiet             Don't display anything (no title)
   -r, --repeat            Repeat playlist indefinitely
   -v, --verbose           Display progress and other status information
   -x n, --nth n           Play every 'n'th block
   -y n, --ntimes n        Repeat every played block 'n' times
   -z, --shuffle           Shuffle list of files before playing
  --bits, -b       Bit depth for output (8 and 16 supported)
  --help,  -h      Produce this help message.
  --quiet, -Q      Quiet mode. No console output.
  --version, -V    Print out version number.
  Input Buffer %5.1f%%  Output Buffer %5.1f%%  by the Xiph.Org Foundation (http://www.xiph.org/)

 %s: illegal option -- %c
 %s: invalid option -- %c
 %s: option `%c%s' doesn't allow an argument
 %s: option `%s' is ambiguous
 %s: option `%s' requires an argument
 %s: option `--%s' doesn't allow an argument
 %s: option `-W %s' doesn't allow an argument
 %s: option `-W %s' is ambiguous
 %s: option requires an argument -- %c
 %s: unrecognized option `%c%s'
 %s: unrecognized option `--%s'
 %sEOS %sPaused %sPrebuf to %.1f%% (NULL) (min %d kbps, max %d kbps) (min %d kbps, no max) (no min or max) (no min, max %d kbps) (none) --- Cannot open playlist file %s.  Skipped.
 --- Cannot play every 0th chunk!
 --- Cannot play every chunk 0 times.
--- To do a test decode, use the null output driver.
 --- Driver %s specified in configuration file invalid.
 --- Hole in the stream; probably harmless
 --- Prebuffer value invalid. Range is 0-100.
 255 channels should be enough for anyone. (Sorry, but Vorbis doesn't support more)
 === Cannot specify output file without specifying a driver.
 === Could not load default driver and no driver specified in config file. Exiting.
 === Driver %s is not a file output driver.
 === Error "%s" while parsing config option from command line.
=== Option was: %s
 === Incorrect option format: %s.
 === No such device %s.
 === Option conflict: End time is before start time.
 === Parse error: %s on line %d of %s (%s)
 === Vorbis library reported a stream error.
 AIFF/AIFC file reader Aspect ratio undefined
 Author:   %s Available codecs:  Available options:
 Avg bitrate: %5.1f BUG: Got zero samples from resampler: your file will be truncated. Please report this.
 Bad comment: "%s"
 Bad type in options list Bad value Big endian 24 bit PCM data is not currently supported, aborting.
 Bitrate hints: upper=%ld nominal=%ld lower=%ld window=%ld Bitstream error, continuing
 Cannot open %s.
 Cannot read header Category: %s
 Changed lowpass frequency from %f kHz to %f kHz
 Channels: %d
 Character encoding: %s
 Colourspace unspecified
 Colourspace: Rec. ITU-R BT.470-6 System M (NTSC)
 Colourspace: Rec. ITU-R BT.470-6 Systems B and G (PAL)
 Comment: Comments: %s Copyright Corrupt or missing data, continuing... Corrupt secondary header. Could not find a processor for stream, bailing
 Could not skip %f seconds of audio. Could not skip to %f in audio stream. Couldn't convert comment to UTF-8, cannot add
 Couldn't create directory "%s": %s
 Couldn't initialise resampler
 Couldn't open %s for reading
 Couldn't open %s for writing
 Couldn't parse cutpoint "%s"
 Decode options
 Decoding "%s" to "%s"
 Default Description Done. Downmixing stereo to mono
 ERROR - line %u: Syntax error: %s
 ERROR - line %u: end time must not be less than start time: %s
 ERROR: %s requires an output filename to be specified with -f.
 ERROR: An output file cannot be given for %s device.
 ERROR: Can only specify one input file if output filename is specified
 ERROR: Cannot open device %s.
 ERROR: Cannot open file %s for writing.
 ERROR: Cannot open input file "%s": %s
 ERROR: Cannot open output file "%s": %s
 ERROR: Could not allocate memory in malloc_buffer_stats()
 ERROR: Could not allocate memory in malloc_data_source_stats()
 ERROR: Could not allocate memory in malloc_decoder_stats()
 ERROR: Could not create required subdirectories for output filename "%s"
 ERROR: Decoding failure.
 ERROR: Device %s failure.
 ERROR: Device not available.
 ERROR: Failed to load %s - can't determine format
 ERROR: Failed to open input as Vorbis
 ERROR: Failed to open input file: %s
 ERROR: Failed to open output file: %s
 ERROR: Failed to write Wave header: %s
 ERROR: File %s already exists.
 ERROR: Input file "%s" is not a supported format
 ERROR: Input filename is the same as output filename "%s"
 ERROR: Multiple files specified when using stdin
 ERROR: Multiple input files with specified output filename: suggest using -n
 ERROR: No Ogg data found in file "%s".
Input probably not Ogg.
 ERROR: No input files specified. Use -h for help
 ERROR: No input files specified. Use -h for help.
 ERROR: Out of memory in create_playlist_member().
 ERROR: Out of memory in decoder_buffered_metadata_callback().
 ERROR: Out of memory in malloc_action().
 ERROR: Out of memory in new_audio_reopen_arg().
 ERROR: Out of memory in new_status_message_arg().
 ERROR: Out of memory in playlist_to_array().
 ERROR: Out of memory.
 ERROR: This error should never happen (%d).  Panic!
 ERROR: Unable to create input buffer.
 ERROR: Unsupported option value to %s device.
 ERROR: Wav file is unsupported subformat (must be 8,16, or 24 bit PCM
or floating point PCM
 ERROR: Wav file is unsupported type (must be standard PCM
 or type 3 floating point PCM
 ERROR: buffer write failed.
 Editing options
 Enabling bitrate management engine
 Encoded by: %s Encoding %s%s%s to 
         %s%s%s 
at approximate bitrate %d kbps (VBR encoding enabled)
 Encoding %s%s%s to 
         %s%s%s 
at average bitrate %d kbps  Encoding %s%s%s to 
         %s%s%s 
at quality %2.2f
 Encoding %s%s%s to 
         %s%s%s 
at quality level %2.2f using constrained VBR  Encoding %s%s%s to 
         %s%s%s 
using bitrate management  Error checking for existence of directory %s: %s
 Error opening %s using the %s module.  The file may be corrupted.
 Error opening comment file '%s'
 Error opening comment file '%s'.
 Error opening input file "%s": %s
 Error opening input file '%s'.
 Error opening output file '%s'.
 Error reading first page of Ogg bitstream. Error reading initial header packet. Error removing old file %s
 Error renaming %s to %s
 Error unknown. Error writing stream to output. Output stream may be corrupted or truncated. Error writing to file: %s
 Error: Could not create audio buffer.
 Error: Out of memory in decoder_buffered_metadata_callback().
 Error: Out of memory in new_print_statistics_arg().
 Error: path segment "%s" is not a directory
 FLAC file reader FLAC,  Failed to convert to UTF-8: %s
 Failed to set advanced rate management parameters
 Failed to set bitrate min/max in quality mode
 Failed to write comments to output file: %s
 Failed writing data to output stream
 Failed writing fisbone header packet to output stream
 Failed writing fishead packet to output stream
 Failed writing header to output stream
 Failed writing skeleton eos packet to output stream
 File: File: %s Frame aspect 16:9
 Frame aspect 4:3
 Frame offset/size invalid: height incorrect
 Frame offset/size invalid: width incorrect
 Framerate %d/%d (%.02f fps)
 Height: %d
 Input buffer size smaller than minimum size of %dkB. Input filename may not be the same as output filename
 Input is not an Ogg bitstream. Input not ogg.
 Input options
 Input truncated or empty. Internal error parsing command line options
 Internal error parsing command line options.
 Internal error parsing command options
 Internal error: Unrecognised argument
 Internal error: attempt to read unsupported bitdepth %d
 Invalid zero framerate
 Invalid/corrupted comments Kate stream %d:
	Total data length: % Key not found Language: %s
 Live: Logical stream %d ended
 Lower bitrate not set
 Lower bitrate: %f kb/s
 Memory allocation error in stats_init()
 Miscellaneous options
 Mode initialisation failed: invalid parameters for bitrate
 Mode initialisation failed: invalid parameters for quality
 Mode number %d does not (any longer) exist in this version Name New logical stream (#%d, serial: %08x): type %s
 No input files specified. "ogginfo -h" for help
 No key No module could be found to read from %s.
 No value for advanced encoder option found
 Nominal bitrate not set
 Nominal bitrate: %f kb/s
 Nominal quality setting (0-63): %d
 Note: Stream %d has serial number %d, which is legal but may cause problems with some tools.
 OPTIONS:
 General:
 -Q, --quiet          Produce no output to stderr
 -h, --help           Print this help text
 -V, --version        Print the version number
 Ogg FLAC file reader Ogg Speex stream: %d channel, %d Hz, %s mode Ogg Speex stream: %d channel, %d Hz, %s mode (VBR) Ogg Vorbis stream: %d channel, %ld Hz Ogg Vorbis.

 Ogg muxing constraints violated, new stream before EOS of all previous streams Opening with %s module: %s
 Out of memory
 Output options
 Page found for stream after EOS flag Pixel format 4:2:0
 Pixel format 4:2:2
 Pixel format 4:4:4
 Pixel format invalid
 Playing: %s Playlist options
 Processing failed
 Processing file "%s"...

 Processing: Cutting at %lld samples
 Quality option "%s" not recognised, ignoring
 RAW file reader Rate: %ld

 ReplayGain (Album): ReplayGain (Track): Requesting a minimum or maximum bitrate requires --managed
 Resampling input from %d Hz to %d Hz
 Scaling input to %f
 Set optional hard quality restrictions
 Setting advanced encoder option "%s"
 Setting advanced encoder option "%s" to %s
 Skipping chunk of type "%s", length %d
 Speex version: %s Speex,  Success Supported options:
 System error Target bitrate: %d kbps
 Text directionality: %s
 The file format of %s is not supported.
 The file was encoded with a newer version of Speex.
 You need to upgrade in order to play it.
 The file was encoded with an older version of Speex.
You would need to downgrade the version in order to play it. Theora headers parsed for stream %d, information follows...
 Theora stream %d:
	Total data length: % This version of libvorbisenc cannot set advanced rate management parameters
 Time: %s Total image: %d by %d, crop offset (%d, %d)
 Track number: Type Unknown character encoding
 Unknown error Unknown text directionality
 Unrecognised advanced option "%s"
 Upper bitrate not set
 Upper bitrate: %f kb/s
 Usage: ogg123 [options] file ...
Play Ogg audio files and network streams.

 Usage: oggdec [options] file1.ogg [file2.ogg ... fileN.ogg]

 Usage: oggenc [options] inputfile [...]

 Usage: ogginfo [flags] file1.ogg [file2.ogx ... fileN.ogv]

ogginfo is a tool for printing information about Ogg files
and for diagnosing problems with them.
Full help shown with "ogginfo -h".
 User comments section follows...
 Vendor: %s
 Vendor: %s (%s)
 Version: %d
 Version: %d.%d
 Version: %d.%d.%d
 Vorbis format: Version %d Vorbis headers parsed for stream %d, information follows...
 Vorbis stream %d:
	Total data length: % WARNING - line %d: failed to get UTF-8 glyph from string
 WARNING - line %u: missing data - truncated file?
 WARNING - line %u: text is too long - truncated
 WARNING: Can't downmix except from stereo to mono
 WARNING: Could not read directory %s.
 WARNING: Couldn't read endianness argument "%s"
 WARNING: Couldn't read resampling frequency "%s"
 WARNING: Failure in UTF-8 decoder. This should not be possible
 WARNING: Ignoring illegal escape character '%c' in name format
 WARNING: Insufficient titles specified, defaulting to final title.
 WARNING: Invalid bits/sample specified, assuming 16.
 WARNING: Invalid channel count specified, assuming 2.
 WARNING: Invalid sample rate specified, assuming 44100.
 WARNING: Multiple name format filter replacements specified, using final
 WARNING: Multiple name format filters specified, using final
 WARNING: Multiple name formats specified, using final
 WARNING: Multiple output files specified, suggest using -n
 WARNING: Raw bits/sample specified for non-raw data. Assuming input is raw.
 WARNING: Raw channel count specified for non-raw data. Assuming input is raw.
 WARNING: Raw endianness specified for non-raw data. Assuming input is raw.
 WARNING: Raw sample rate specified for non-raw data. Assuming input is raw.
 WARNING: Unknown option specified, ignoring->
 WARNING: no language specified for %s
 WARNING: quality setting too high, setting to maximum quality.
 WAV file reader Warning from playlist %s: Could not read directory %s.
 Warning: AIFF-C header truncated.
 Warning: Can't handle compressed AIFF-C (%c%c%c%c)
 Warning: Corrupted SSND chunk in AIFF header
 Warning: Could not read directory %s.
 Warning: INVALID format chunk in wav header.
 Trying to read anyway (may not work)...
 Warning: No SSND chunk found in AIFF file
 Warning: No common chunk found in AIFF file
 Warning: OggEnc does not support this type of AIFF/AIFC file
 Must be 8 or 16 bit PCM.
 Warning: Truncated common chunk in AIFF header
 Warning: Unexpected EOF in AIFF chunk
 Warning: Unexpected EOF in reading AIFF header
 Warning: Unexpected EOF in reading WAV header
 Warning: Unexpected EOF reading AIFF header
 Warning: Unrecognised format chunk in WAV header
 Warning: WAV 'block alignment' value is incorrect, ignoring.
The software that created this file is incorrect.
 Width: %d
 bad comment: "%s"
 bool char default output device double float int left to right, top to bottom no action specified
 none of %s ogg123 from %s %s ogg123 from %s %s
 by the Xiph.Org Foundation (http://www.xiph.org/)

 oggdec from %s %s
 ogginfo from %s %s
 other repeat playlist forever right to left, top to bottom shuffle playlist standard input standard output string top to bottom, left to right top to bottom, right to left utf-8 Project-Id-Version: vorbis-tools 1.1.1
Report-Msgid-Bugs-To: https://trac.xiph.org/
POT-Creation-Date: 2010-03-26 03:08-0400
PO-Revision-Date: 2013-05-28 13:37+0000
Last-Translator: Глория Хрусталёва <Unknown>
Language-Team: Russian <gnu@mx.ru>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:21+0000
X-Generator: Launchpad (build 18115)
 	-V Вывести информацию о версии и выйти
 	Средний битрейт: %.1f Кб/с

 	Время работы: %dм %04.1fс
 	Кодирование [готово %2dм%.2dс] %c  	Скорость:     %.4f
 	[%5.1f%%] [осталось %2dм%.2dс] %c  
 
	Длина файла:  %dм %04.1fс
 

Кодирование файла "%s" завершено
 

Кодирование завершено.
 
Звуковое Устройство:   %s   --audio-buffer n        Задать размер аудио-буфера в 'n' килобайт
   -@ file, --list file    Загрузить список файлов и потоков из файла "file"
   -K n, --end n           Остановить после 'n' секунд (или в формате чч:мм:сс)
   -R, --remote            Использовать интерфейс дистанционного управления
   -V, --version           Вывести версию ogg123
   -Z, --random            Воспроизводить файлы в случайном порядке
                          вплоть до прерывания
   -b n, --buffer n        Задать размер входного буфера в 'n' килобайт
   -d dev, --device dev    Выводить на устройство "dev". Доступные устройства:
   -f file, --file file    Имя файла вывода для файлового устройства,
                          указанного ранее в параметре --device.
   -h, --help              Вывести эту справку
   -k n, --skip n          Пропускать первые 'n' секунд (или в формате чч:мм:сс)
   -l s, --delay s         Задать время завершения в миллисекундах. ogg123
                          перейдёт к след. файлу при получении SIGINT (Ctrl-C),
                          и завершит работу, если два сигнала SIGINT получены
                          в течение промежутка времени 's'. (500 по умолчанию)
   -o k:v, --device-option k:v
                          Передать параметр 'k' со значением 'v' устройству,
                          указанному ранее в параметре --device.
                          Список параметров устройств доступен
                          в странице руководства ogg123.
   -p n, --prebuffer n     Загружать n%% входного буфера до воспроизведения
   -q, --quiet             Не выводить информацию (заголовки)
   -r, --repeat            Повторять воспроизведение композиций из списка
   -v, --verbose           Отображать прогресс и другие сведения о состоянии
   -x n, --nth n           Воспроизводить каждый 'n'-ный блок
   -y n, --ntimes n        Повторять каждый воспроизводимый блок 'n' раз
   -z, --shuffle           Воспроизводить файлы из списка в случайном порядке
  --bits, -b       Bit depth на выходе (поддерживаются значения 8 и 16)
  --help,  -h      Показать это справочное сообщение.
  --quiet, -Q      Тихий режим. Отключить вывод в консоль.
  --version, -V    Показать номер версии.
  Входной буфер %5.1f%%  Буфер вывода %5.1f%%  Xiph.Org Foundation (http://www.xiph.org/)

 %s: недопустимый параметр -- %c
 %s: неверный параметр -- %c
 %s: параметр `%c%s' не допускает указания аргументов
 %s: неоднозначный параметр `%s'
 %s: параметр `%s' требует указания аргумента
 %s: параметр `--%s' не допускает указания аргументов
 %s: параметр `-W %s' не допускает указания аргумента
 %s: неоднозначный параметр `-W %s'
 %s: параметр требует указания аргумента -- %c
 %s: неопознанный параметр `%c%s'
 %s: неопознанный параметр `--%s'
 %sEOS %sПауза %sПре-буферизация до %.1f%% (NULL) (мин. %d кбит/с, макс. %d кбит/с) (мин. %d кбит/с, макс. не огранич.) (без ограничений) (мин. не огранич., макс. %d кбит/с) (не указан) --- Не удаётся открыть файл списка воспроизведения %s.  Пропущен.
 --- Воспроизвести каждый 0-й фрагмент невозможно!
 --- Невозможно воспроизвести фрагмент 0 раз.
--- Для проведения тестового декодирования используйте выходной драйвер null.
 --- Указанный в файле конфигурации драйвер %s недействителен.
 --- Полость в потоке; возможно, безвредная
 --- Неправильное значение размера пре-буфера. Диапазон 0-100.
 255 каналов должно хватить любому. (Извините, но Vorbis не поддерживает больше)
 === Невозможно указать выходной файл, не указав драйвер.
 === Не удаётся загрузить драйвер по умолчанию; драйвер не указан в файле конфигурации. Завершение работы.
 === Драйвер %s не является драйвером записи в файл.
 === Ошибка "%s" во время разбора параметров конфигурации из командной строки.
=== Ошибку вызвал параметр: %s
 === Некорректный формат параметра: %s.
 === Устройство %s не существует.
 === Конфликт параметров: Время окончания раньше времени старта.
 === Ошибка разбора: %s в строке %d из %s (%s)
 === Библиотека Vorbis сообщила об ошибке в потоке.
 Чтение файлов AIFF/AIFC Соотношение размеров не указано
 Автор:    %s Доступные кодеки:  Доступные параметры:
 Срд битрейт: %5.1f ОШИБКА: От ресэмплера получено нулевое число кадров: ваш файл будет усечён. Пожалуйста, сообщите об этом автору.
 Неправильный комментарий: "%s"
 Некорректный тип в списке параметров Некорректное значение 24-битные данные PCM в режиме BIG-ENDIAN не поддерживаются. Действие прервано.
 Значения битрейта: верхнее=%ld номинальное=%ld нижнее=%ld окно=%ld Ошибка битового потока, продолжение работы
 Не удаётся открыть %s.
 Не удалось прочитать заголовок Категория: %s
 Изменение частоты lowpass-фильтра с %f до %f кГц
 Каналы: %d
 Кодировка символов: %s
 Цветовое пространство не указано
 Цветовое пространство: Rec. ITU-R BT.470-6 Система M (NTSC)
 Цветовое пространство: Rec. ITU-R BT.470-6 Системы B и G (PAL)
 Комментарий: Комментарии: %s Авторские права Повреждены или отсутствуют данные, продолжение работы... Повреждён вторичный заголовок. Не удалось найти обработчик потока. Задание отложено
 Не удалось пропустить %f секунд звука. Не удалось переместить воспроизведение потока на %f. Не удалось преобразовать комментарий в UTF-8, добавление невозможно
 Не удалось создать каталог "%s": %s
 Не удалось инициализировать ресэмплер
 Не удалось открыть %s для чтения
 Не удалось открыть %s для записи
 Не удалось обработать точку отреза "%s"
 Параметры декодирования
 Декодирование "%s" в "%s"
 По умолчанию Описание Завершено. Низведение стерео в моно
 ОШИБКА — строка %u: Синтаксическая ошибка: %s
 ОШИБКА - строка %u: время окончания не должно быть меньше времени начала: %s
 ОШИБКА: %s необходимо указать название для создаваемого файла с -f.
 ОШИБКА: Невозможно задать выходной файл для устройства %s.
 ОШИБКА: Нельзя установить больше одного исходного файла, если указан файл для записи.
 ОШИБКА: Невозможно открыть устройство %s.
 ОШИБКА: Не удаётся открыть файл %s для записи.
 ОШИБКА: Не удаётся открыть входной файл "%s": %s
 ОШИБКА: Не удаётся открыть выходной файл "%s": %s
 ОШИБКА: Невозможно распределить память в malloc_buffer_stats()
 ОШИБКА: Не удалось выделить память в malloc_data_source_stats()
 ОШИБКА: Не удалось выделить память в malloc_decoder_stats()
 ОШИБКА: Не удалось создать необходимые подкаталоги для выходного файла "%s"
 ОШИБКА: Сбой декодирования
 ОШИБКА: Ошибка устройства %s.
 ОШИБКА: Устройство не доступно.
 ОШИБКА: Не удалось загрузить %s — невозможно определить формат
 ОШИБКА: Не удалось открыть файл как Vorbis
 ОШИБКА: Не удалось открыть входной файл: %s
 ОШИБКА: Не удалось открыть выходной файл: %s
 ОШИБКА: Не удалось записать Wave заголовок: %s
 ОШИБКА: Файл %s уже существует.
 ОШИБКА: Входной файл "%s" в не поддерживаемом формате
 Имя входного файла совпадает с именем выходного файла "%s"
 ОШИБКА: Указано несколько файлов, в то время как используется stdin
 ОШИБКА: Несколько входных файлов при указанном имени выходного файла: предполагается использование - n
 ОШИБКА: Не найдено данных Ogg в файле «%s».
Вероятно, на входе не Ogg.
 ОШИБКА: Не указаны входные файлы. Для показа справки используйте -h
 ОШИБКА: Не указан входной файл. Используйте -h для получения справки.
 ОШИБКА: Нехватка памяти в create_playlist_member().
 ОШИБКА: Нехватка памяти в decoder_buffered_metadata_callback().
 ОШИБКА: Недостаточно памяти в malloc_action().
 ОШИБКА: Недостаточно памяти для выполнения new_audio_reopen_arg().
 ОШИБКА: Нехватка памяти в new_status_message_arg().
 ОШИБКА: Нехватка памяти в playlist_to_array().
 ОШИБКА: Не хватает памяти.
 ОШИБКА: Эта ошибка никогда не должна была произойти (%d).  Ахтунг!
 ОШИБКА: Невозможно создать входной буфер
 ОШИБКА: Не поддерживаемое значение опции для устройства %s.
 ОШИБКА: Файл wav неподдерживаемого подформата
(должен быть 8, 16 или 24 битный PCM 
или PCM с плавающей точкой)
 ОШИБКА: Файл wav неподдерживаемого типа (должен быть стандартный PCM
 или тип 3 PCM с плавающей точкой)
 ОШИБКА: Ошибка записи в буфер.
 Параметры редактирования
 Включение механизма управления битрейтом
 Кодирование: %s Кодирование %s%s%s в 
            %s%s%s 
с приблизительным битрейтом %d Кб/сек (включено кодирование с VBR)
 Кодирование %s%s%s в 
            %s%s%s 
со средним битрейтом %d Кб/с  Кодирование %s%s%s в 
            %s%s%s 
с качеством %2.2f
 Кодирование %s%s%s в 
            %s%s%s 
с уровнем качества %2.2f с ограничением VBR  Кодирование %s%s%s в 
            %s%s%s 
с управляемым битрейтом  Ошибка проверки существования каталога %s: %s
 Ошибка открытия %s модулем %s.  Файл может быть повреждён.
 Ошибка открытия файла комментариев '%s'
 Ошибка открытия файла комментариев '%s'.
 Ошибка открытия входного файла "%s": %s
 Ошибка открытия входного файла '%s'.
 Ошибка открытия выходного файла '%s'.
 Ошибка чтения первой страницы битового потока Ogg. Ошибка чтения начального заголовка пакета. Ошибка удаления старого файла %s
 Ошибка переименования %s в %s
 Неизвестная ошибка. Ошибка записи выходного потока. Выходной поток может быть повреждён или обрезан. Ошибка записи в файл: %s
 Ошибка: Не удалось создать аудио-буфер.
 Ошибка: Не достаточно памяти при выполнении decoder_buffered_metadata_callback().
 Ошибка: Не достаточно памяти при выполнении new_print_statistics_arg().
 Ошибка: сегмент пути "%s" не является каталогом
 Чтение файлов FLAC FLAC,  Не удалось сконвертировать в UTF-8: %s
 Ошибка при установке продвинутых параметров скорости потока
 Сбой при установке мин/макс значения битрейта в режиме установки качества
 Ошибка при записи комментариев в выходной файл: %s
 Сбой при записи данных в выходной поток
 Ошибка записи информации о потоке (fisbone)
 Ошибка записи информации о потоках (fishead)
 Сбой при записи заголовка в выходной поток
 Ошибка записи страницы информации о потоках (skeleton eos)
 Файловые: Файл: %s Соотношение размеров кадра 16:9
 Соотношение размеров кадра 4:3
 Недопустимое смещение/размер кадра: неправильная высота
 Недопустимое смещение/размер кадра: неправильная ширина
 Частота кадров %d/%d (%.02f кадр/с)
 Высота: %d
 Размер входного буфера меньше минимального - %dКб. Имя входного файла не может совпадать с именем выходного файла
 Исходные данные не являются битовым потоком Ogg. Исходные данные не в формате ogg.
 Параметры ввода
 Исходные данные усечены или пустые. Внутренняя ошибка разбора параметров командной строки
 Внутренняя ошибка разбора параметров командной строки.
 Внутренняя ошибка разбора параметров командной строки
 Внутренняя ошибка: Нераспознанный аргумент
 Внутренняя ошибка: попытка прочитать не поддерживаемую разрядность в %d бит
 Недопустимая нулевая частота кадров
 Неверные/поврежденные комментарии Поток Kate %d:
	Общая длина данных: % Ключ не найден Язык: %s
 Живые: Логический поток %d завершён
 Минимальный битрейт не задан
 Минимальный битрейт: %f Кб/с
 Ошибка выделения памяти при выполнении stats_init()
 Прочие параметры
 Сбой инициализации режима: некорректные параметры битрейта
 Сбой инициализации режима: некорректные параметры качества
 Режим %d (больше) не поддерживается данной версией. Имя Новый логический поток (#%d, серийный номер: %08x): тип %s
 Не указаны входные файлы. Для получения справки используйте "ogginfo -h"
 Нет ключа Не найдены модули для чтения из файла %s.
 Не найдено значение дополнительного параметра кодировщика
 Номинальный битрейт не задан
 Номинальный битрейт: %f Кб/с
 Значение номинального качества (0-63): %d
 Примечание: Поток %d имеет серийный номер %d, что допустимо, но может вызвать проблемы в работе некоторых инструментов.
 ОПЦИИ:
 Общие:
 -Q, --quiet          Не осуществлять вывод в stderr
 -h, --help           Вывести этот текст справки
 -V, --version        Вывести номер версии
 Чтение файлов Ogg FLAC Поток Ogg Speex: каналов - %d , %d Hz, %s mode Поток Ogg Speex: каналов - %d , %d Hz, %s mode (VBR) Поток Ogg Vorbis: %d канал, %ld Гц Ogg Vorbis.

 Нарушены ограничения мультиплексирования Ogg. Найден новый поток до получения маркеров конца всех предыдущих потоков Открытие с модулем %s: %s
 Недостаточно памяти
 Параметры вывода
 Найдена страница для потока после получения маркера его конца Формат пикселя 4:2:0
 Формат пикселя 4:4:2
 Формат пикселя 4:4:4
 Неправильный формат пикселя
 Воспроизведение: %s Параметры списка воспроизведения
 Сбой обработки
 Обработка файла "%s"...

 Обработка: Вырезание в позиции %lld кадров
 Параметр качества "%s" не распознан, игнорируется
 Чтение файлов RAW Частота: %ld

 УровеньВоспроизведения (Альбом): УровеньВоспроизведения (Дорожка): Для запроса минимального или максимального битрейта требуется параметр --managed
 Изменение частоты дискретизации входных файлов с %d до %d Гц
 Умножение входного сигнала на %f
 Установлены необязательные жёсткие ограничения качества
 Установка продвинутных настроек кодировщика "%s"
 Установка дополнительного параметра кодировщика "%s" в %s
 Пропуск фрагмента типа "%s" длиной %d
 Версия Speex: %s Speex,  Успешное завершение Поддерживаемые опции:
 Системная ошибка Целевой битрейт: %d Кб/с
 Направление текста: %s
 Формат файла %s не поддерживается.
 Файл был закодирован более новой версией Speex.
 Необходимо обновление для воспроизведения.
 Файл был закодирован более старой версией Speex.
Необходим откат до предыдущей версии для воспроизведения. Заголовки theora обработаны для потока %d, далее информация...
 Поток Theora %d:
	Общая длина данных: % Текущая версия libvorbisenc не поддерживает продвинутые параметры скорости потока
 Время: %s Полное изображение: %d на %d, сдвиг обрезки (%d, %d)
 Номер дорожки: Тип Неизвестная кодировка символов
 Неизвестная ошибка Неизвестное направление текста
 Неизвестный дополнительный параметр "%s"
 Максимальный битрейт не задан
 Максимальный битрейт: %f Кб/с
 Использование: ogg123 [параметры] файл ...
Воспроизведение потоковых источников и аудио-файлов в формате Ogg.

 Использование: oggdec [опции] файл1.ogg [файл2.ogg ... файлN.ogg]

 Использование: oggenc [опции] входной_файл [...]

 Использование: ogginfo [флаги] file1.ogg [file2.ogx ... fileN.ogv]

ogginfo — инструмент для вывода информации о файлах Ogg
и диагностики проблем в них.
Для показа полной справки наберите «ogginfo -h».
 Секция пользовательских комментариев...
 Поставщик: %s
 Поставщик: %s (%s)
 Версия: %d
 Версия: %d.%d
 Версия: %d.%d.%d
 Формат Vorbis: Версия %d Заголовки vorbis для потока %d обработаны, информация...
 Поток Vorbis %d:
	Всего данных: % ПРЕДУПРЕЖДЕНИЕ - строка %d: не удалось получить символ UTF-8 из строки
 ПРЕДУПРЕЖДЕНИЕ - строка %u: не хватает данных. Возможно Ваш файл обрезан.
 ПРЕДУПРЕЖДЕНИЕ - строка %u: текст слишком длинный, он будет урезан
 ВНИМАНИЕ: Смешение сигналов возможно только из стерео в моно
 ОШИБКА: не удалось выполнить чтение каталога %s.
 ВНИМАНИЕ: Невозможно прочитать аргумент "%s" в порядке следования байт
 ВНИМАНИЕ: Невозможно прочитать изменение частоты "%s"
 ПРЕДУПРЕЖДЕНИЕ: Сбой в декодере UTF-8. Этого не должно было случиться
 ВНИМАНИЕ: Некорректный управляющий символ '%c' в формате имени проигнорирован
 ВНИМАНИЕ: Указано недостаточное количество заголовков, установлены последние значения.
 ВНИМАНИЕ: Указано некорректное значение бит/кадр, предполагается 16.
 ВНИМАНИЕ: Указано неверное число каналов, предполагается 2.
 ВНИМАНИЕ: Указано некорректное значение частоты выборки, предполагается 44100.
 ВНИМАНИЕ: Указано несколько замен фильтров формата имени, используется последняя
 ВНИМАНИЕ: Указано несколько фильтров формата имени, используется последний
 ВНИМАНИЕ: Указано несколько форматов имени, используется последний
 ВНИМАНИЕ: Указано несколько выходных файлов, предполагается использование параметра -n
 ВНИМАНИЕ: Указано значение сырых бит/кадр для не-сырых данных. Предполагается сырой ввод.
 ВНИМАНИЕ: Указано число сырых каналов для не-сырых данных. Предполагается сырой ввод.
 ВНИМАНИЕ: Указан порядок байт сырых данных для не-сырых данных. Подразумеваем сырые данные.
 ВНИМАНИЕ: Указана сырая частота выборки для не-сырых данных. Предполагается сырой ввод.
 ВНИМАНИЕ: Указан неизвестный параметр, игнорируется->
 ПРЕДУПРЕЖДЕНИЕ: не указан язык для  %s
 ВНИМАНИЕ: установлено слишком высокое качество, ограничено до максимального
 Чтение файлов WAV Предупреждение от списка воспроизведения %s: Не удалось прочитать каталог %s.
 Внимание: Заголовок AIFF-C обрезан.
 Внимание: Невозможно обработать сжатый AIFF-C (%c%c%c%c)
 Внимание: В заголовке AIFF поврежден SSND-фрагмент
 Внимание: Не удалось прочитать каталог %s.
 Внимание: НЕКОРРЕКТНЫЙ формат фрагмента в заголовке wav.
 Будет выполнена попытка прочтения (возможны сбои)...
 Внимание: В файле AIFF не найден SSND-фрагмент
 Внимание: В файле AIFF не найден общий фрагмент
 Внимание: OggEnc не поддерживает данный тип файлов AIFF/AIFC
 Данные должны быть в формате PCM, 8 или 16 бит.
 Внимание: Обрезанный общий фрагмент в заголовке AIFF
 Внимание: Неожиданный конец файла во фрагменте AIFF
 Внимание: Неожиданный конец файла при чтении заголовка AIFF
 Внимание: Неожиданный конец файла при чтении заголовка WAV
 Внимание: Неожиданный конец файла при чтении заголовка AIFF
 Внимание: Формат фрагмента в заголовке WAV не распознан
 Внимание: Значение 'block alignment' в WAV-файле недействительно
и будет проигнорировано. Данный файл создан некорректным ПО.
 Ширина: %d
 неправильный комментарий: "%s"
 булев символ устройство вывода по умолчанию дв.точн. с пл.точкой целый слева направо, сверху вниз действие не указано
 не указан из %s ogg123 из пакета %s %s ogg123 из %s %s
 Xiph.Org Foundation (http://www.xiph.org/)

 oggdec из пакета %s %s
 ogginfo из пакета %s %s
 другой повторять список воспроизведения справа налево, сверху вниз перемешать список воспроизведения стандартный ввод стандартный вывод строка сверху вниз, слева направо сверху вниз, справа налево utf-8 