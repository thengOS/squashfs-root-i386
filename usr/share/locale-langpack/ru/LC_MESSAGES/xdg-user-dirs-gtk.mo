��          |      �             !     5  -   E  '   s  3   �  ,   �     �  �        �     �     �  y        �     �  i   �  F   *  m   q  n   �  8   N  �  �  9   	  +   C     o                            
      	                         Current folder name New folder name Note that existing content will not be moved. There was an error updating the folders Update common folders names to match current locale Update standard folders to current language? User folders update You have logged in in a new language. You can automatically update the names of some standard folders in your home folder to match this language. The update would change the following folders: _Don't ask me this again _Keep Old Names _Update Names Project-Id-Version: ru
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-04 03:00+0000
PO-Revision-Date: 2012-04-04 15:03+0000
Last-Translator: Leonid Kanter <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:21+0000
X-Generator: Launchpad (build 18115)
 Текущее имя папки Новое имя папки Учтите, что текущее содержимое папок не будет перенесено. При обновлении папок произошла ошибка Обновить имена общих папок для соответствия текущему языку Обновить стандартные папки в соответствии с текущим языком? Обновление папок пользователя Вы вошли в сеанс, выбрав новый язык. Вы можете автоматически обновить имена некоторых стандартных папок в вашей домашней папке для соответствия этому языку. При обновлении будут изменены имена следующих папок: _Не задавать больше этот вопрос _Сохранить старые имена _Обновить имена 