��          t      �                      .  "   A      d  7   �  c   �  )   !  R   K  0   �  �  �     b  )   h     �  0   �  3   �  ]     �   t  9     �   Y  W   �           	                              
                   %(2)s Internal error: %(1)s. Permission denied. The %(1)s handle %(2)s is invalid. The method %(1)s is unsupported. The method %(1)s takes %(2)s argument(s) (%(3)s given). The network you specified already has a PIF attached to it, and so another one may not be attached. This map already contains %(1)s -> %(2)s. Value "%(2)s" for %(1)s is not supported by this server.  The server said "%(3)s". You attempted an operation that was not allowed. Project-Id-Version: xen
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2008-03-31 17:40+0100
PO-Revision-Date: 2011-12-25 12:48+0000
Last-Translator: Nikita Putko <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:40+0000
X-Generator: Launchpad (build 18115)
 %(2)s Внутренняя ошибка: %(1)s. Доступ запрещен. %(1)s дескриптор %(2)s неверен. Метод %(1)s не поддерживается. Метод %(1)s принимает %(2)s аргумент(а/ов) (%(3)s передано). Операция не может быть выполнена потому, что указанная вами сеть уже имеет присоединенный PIF. Эта карта уже содержит %(1)s -> %(2)s. Значение "%(2)s" для %(1)s не поддерживается этим сервером. Сервер сообщил: "%(3)s". Вы попытались совершить недопустимую операцию. 