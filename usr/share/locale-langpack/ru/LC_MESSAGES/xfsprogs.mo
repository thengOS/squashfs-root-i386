��    .      �  =   �      �     �               :     Z  "   l  4   �  4   �  v   �     p     �  5   �  9   �  %   �  #   $     H  (   f  B   �  (   �  #   �          3     N     Z      s     �  ,   �  %   �  $   �  !        >  #   ^     �     �  
   �     �     �     �     �     �  #   		     -	      D	     e	     z	  �  �	  '   !  &   I  5   p  @   �  $   �  >     z   K  z   �  �   A  #        6  {   C  X   �  c     K   |  N   �  C     �   [  N   �  m   @  '   �  G   �       9   6  O   p     �  k   �  b   L  m   �  P     @   n  A   �     �  H         I     ]     u  c   �  
   �  U     W   W  /   �  X   �  (   8  -   a     '   
   *   -   %                               #   "      &                   ,   .      $                            !             )                                       (         	   +                         
fatal error --   resetting version number
  would reset version number
 %s: cannot set flags on %s: %s
 %s: unknown flag
 -%c %s option cannot have a value
 AGF geometry info conflicts with filesystem geometry AGI geometry info conflicts with filesystem geometry ALERT: The filesystem has valuable metadata changes in a log which is being
destroyed because the -L option was used.
 Cannot find %d
 Exiting now.
 Found unsupported filesystem features.  Exiting now.
 Internal error - process_misc_ino_types, illegal type %d
 No modify flag set, skipping phase 5
 Phase 2 - using external log on %s
 Phase 2 - using internal log
 Phase 4 - check for duplicate blocks...
 This filesystem contains features not understood by this program.
 WARNING:  unknown superblock version %d
 attempted to perform I/O beyond EOF bad blocksize field bad or unsupported version correcting
 couldn't get superblock
 couldn't initialize XFS library
 end of range ?  error - not enough free space in filesystem
 failed to create prefetch thread: %s
 failed to initialize prefetch mutex
 freeze filesystem of current file give advice about use of memory no files are open, try 'help open'
 non-sync prefetch corruption
 read-write resetting to zero
 resetting value
 root inode marked free,  sync uncertain inode list is --
 unfreeze filesystem of current file unknown option -%c %s
 value %d is out of range (0-%d)
 would reset to zero
 would reset value
 Project-Id-Version: xfsprogs
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-11-27 10:38+1100
PO-Revision-Date: 2011-04-28 08:56+0000
Last-Translator: AngorSTV <Unknown>
Language-Team: Russian <ru@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:23+0000
X-Generator: Launchpad (build 18115)
 
критическая ошибка--   сброс номера версии
  нужно сбросить номер версии
 %s: невозможно утсановить флаги %s: %s
 %s: неизвестный флаг
 -%c %s опции не могут иметь значение
 Информация о геометрии AGF противоречит геометрии файловой системы Информация о геометрии AGI противоречит геометрии файловой системы ТРЕВОГА: Файловая система содержащая полезные данные разрушена
(по данным логов) из-за использования параметра -L
 Невозможно найти %d
 Выход.
 Обнаружены не поддерживаемые возможности файловой системы. Выход.
 внутренняя ошибка - process_misc_ino_types, не верный тип %d
 Невозможно изменить флаг состояния, пропуск 5-ой фазы.
 Фаза 2 - использование внешнего журнала %s
 Фаза 2 - использование внутреннего журнала
 Фаза 4 - поиск повторяющихся блоков...
 Текущая файловая система имеет возможности, которые не распознаются программой.
 ВНИМАНИЕ: неизвестная версия суперблока %d
 попытка выполнить операцию чтения/записи после конца файла не верный размер поля не верная или не поддерживаемая версия исправление
 невозможно получить суперблок
 невозможно инициализировать XFS библиотеку
 конец диапазона?  ошибка - недостаточно свободного места в файловой системе
 неудача создания предварительной выборки в потоке: %s
 неудача инициализации предварительной выборки исключений
 заморозить файловую систему текущего файла дать совет по использованию памяти файлы не открыты, попробуйте 'help open'
 несинхр предварительная выборка не получилась
 ввод-вывод сброс в ноль
 сброс значения
 корневой индексный дескриптор помечен как свободный,  синхр список сомнительных индексных дескрипторов --
 разморозить файловую систему из текущего файла неизвестный параметр -%c %s
 значение %d превосходит допустимый диапазон (0-%d)
 нужно сбросить в ноль
 нужно сбросить значение
 