��    I      d  a   �      0     1     G     \     r     ~     �     �     �     �     �  n   �     ]     y     �  +   �  J   �  E   +  �   q  r   �  k   n	     �	     �	  5   �	  "   &
  <   I
     �
  #   �
     �
  )   �
     �
  <      3   =      q     �     �     �      �      �     �  
        "     @     P  �   g            ;   3  -   o  	   �  (   �  u   �  h   F     �  `   �     '     5  %   Q     w  
   �  D   �  	   �  
   �  6   �  6   ,  -   c     �     �  �   �  6   E  ;   |  &   �     �    �          1     G     [     i     w     �     �     �     �  e   �     :     S     p     �  F   �  F   �  y   5  s   �  X   #  
   |     �  1   �  -   �  7   �  $   $     I     f      m     �  A   �  /   �          "     )     2     G     d      �     �     �     �     �  �   �     �     �  >   �     �     �       b   "  k   �     �  [   
     f     s     �     �     �  A   �          -  2   >  2   q  3   �     �     �  g     6   v  :   �      �  &   	     *          %                          E      '   4   +       0          :   ;   1          &      I                         F      $   B   (   9      7   @   G       8   C   
      ?   2       3                   ,   )   5   H             .       =       #       A   /             "                  6              D   <   !   >         	   -               128-bit (most secure) 40-bit (less secure) <b>Authentication</b> <b>Echo</b> <b>General</b> <b>Misc</b> <b>Optional</b> <b>Security and Compression</b> Ad_vanced... All Available (Default) Allow MPPE to use stateful mode. Stateless mode is still attempted first.
config: mppe-stateful (when checked) Allow _BSD data compression Allow _Deflate data compression Allow st_ateful encryption Allow the following authentication methods: Allow/disable BSD-Compress compression.
config: nobsdcomp (when unchecked) Allow/disable Deflate compression.
config: nodeflate (when unchecked) Allow/disable Van Jacobson style TCP/IP header compression in both the transmit and the receive directions.
config: novj (when unchecked) Allow/disable authentication methods.
config: refuse-pap, refuse-chap, refuse-mschap, refuse-mschap-v2, refuse-eap Append the domain name <domain> to the local host name for authentication purposes.
config: domain <domain> Authenticate VPN CHAP Compatible with Microsoft and other PPTP VPN servers. Could not find pptp client binary. Could not find secrets (connection invalid, no vpn setting). Could not find the pppd binary. D-Bus name to use for this instance Default Don't quit when VPN connection terminates EAP Enable custom index for ppp<n> device name.
config: unit <n> Enable verbose debug logging (may expose passwords) Invalid or missing PPTP gateway. MSCHAP MSCHAPv2 Missing VPN gateway. Missing or invalid VPN password. Missing or invalid VPN username. Missing required option '%s'. NT Domain: No VPN configuration options. No VPN secrets! No cached credentials. Note: MPPE encryption is only available with MSCHAP authentication methods. To enable this checkbox, select one or more of the MSCHAP authentication methods: MSCHAP or MSCHAPv2. PAP PPTP Advanced Options PPTP server IP or name.
config: the first parameter of pptp Password passed to PPTP when prompted for it. Password: Point-to-Point Tunneling Protocol (PPTP) Require the use of MPPE, with 40/128-bit encryption or all.
config: require-mppe, require-mppe-128 or require-mppe-40 Send LCP echo-requests to find out whether peer is alive.
config: lcp-echo-failure and lcp-echo-interval Send PPP _echo packets Set the name used for authenticating the local system to the peer to <name>.
config: user <name> Show password Use TCP _header compression Use _Point-to-Point encryption (MPPE) Use custom _unit number: User name: You need to authenticate to access the Virtual Private Network '%s'. _Gateway: _Security: couldn't convert PPTP VPN gateway IP address '%s' (%d) couldn't look up PPTP VPN gateway IP address '%s' (%d) invalid boolean property '%s' (not yes or no) invalid gateway '%s' invalid integer property '%s' nm-pptp-service provides integrated PPTP VPN capability (compatible with Microsoft and other implementations) to NetworkManager. no usable addresses returned for PPTP VPN gateway '%s' no usable addresses returned for PPTP VPN gateway '%s' (%d) property '%s' invalid or not supported unhandled property '%s' type %s Project-Id-Version: NetworkManager-pptp master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=NetworkManager&keywords=I18N+L10N&component=VPN: pptp
POT-Creation-Date: 2016-04-14 07:51+0000
PO-Revision-Date: 2016-04-11 05:41+0000
Last-Translator: YunQiang Su <wzssyqa@gmail.com>
Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:03+0000
X-Generator: Launchpad (build 18115)
 128 位 (最安全的) 40 位 (不太安全) <b>身份验证</b> <b>回响</b> <b>一般</b> <b>其它</b> <b>可选项</b> <b>安全性及压缩</b> 高级(_V)... 全部可用 (默认) 允许 MPPE 使用状态模式，但首先尝试无状态模式。\n
配置：mppe-stateful  (选中) 允许 _BSD 数据压缩 允许 _Deflate 数据压缩 允许有状态的加密(_A) 允许以下认证方法: 允许/禁止 BSD-Compress 压缩。\n
配置 ：nobsdcomp (未选中) 允许/禁止 Deflate 数据压缩。\n
配置：nodeflate (未选中) 在发送和接收两个方向上允许/禁止 Van Jacobson 方式对 TCP/IP 头部压缩。\n
配置： novj (未选中) 允许/禁用 身份认证方法。\n
配置：refuse-pap, refuse-chap, refuse-mschap, refuse-mschap-v2, refuse-eap 在本地主机名称后添加域名 <域名> 用以认证。\n
配置：域名 <域名> 认证 VPN CHAP 与微软及其他的 PPTP VPN 服务器兼容。 无法找到 pptp 客户端二进制文件。 无法找到密钥 (连接失败，VPN 配置无效)。 无法找到 pppd 二进制文件。 此实例使用的 D-Bus 名 默认 在 VPN 连接终止时不退出 EAP 为 ppp<n> 设备名使用自定义索引号。
配置：unit <n> 启用详细的调试记录(可能暴露密码) PPTP 网关缺失或无效。 MSCHAP MSCHAPv2 缺少 VPN 网关。 VPN 密码缺失或无效。 VPN 用户名丢失或无效。 缺少需要的选项“%s”。 NT 域: 没有 VPN 配置选项。 未输入 VPN 密钥！ 没有缓存的证书。 注意：MPPE 加密仅在使用 MSCHAP 认证方式时可用。要启用此选项，请选择 MSCHAP 认证方式之中的一个或多个：MSCHAP 或 MSCHAPv2 PAP PPTP 高级选项 PPTP 服务 IP 或名称。\n
配置：pptp 的第一个参数 PPTP 密码。 密码: 点到点隧道协议(PPTP) 要求使用 40/128 位加密 MPPE。\n
配置：require-mppe, require-mppe-128 or require-mppe-40 发送 LCP 回声请求来查探设备是否有响应。\n
配置： lcp-echo-failure 和 lcp-echo-interval 发送 PPP 回响包(_E) 设置名称为 <名称> 来对本地系统到对端的认证。\n
配置：用户 <名称> 显示密码 使用 TCP 头压缩(_H) 使用点到点加密(MPPE)(_P) 使用自定义单元号(_U)： 用户名(_U)： 您需要进行身份验证才能访问虚拟专用网络 '%s'。 网关(_G)： 安全性(_S)： 无法转换 PPTP VPN 网关 IP 地址“%s”(%d) 无法查找 PPTP VPN 网关 IP 地址“%s”(%d) 无效的布尔型属性“%s”(不是 yes 或 no) 无效的网关“%s” 无效的整型属性“%s” nm-pptp-service 为网络管理器提供了集成的 PPTP VPN 功能(与微软和其他实现兼容)。 未返回对应 PPTP VPN 网关“%s”的可用地址 未返回对应 PPTP VPN 网关“%s”的可用地址(%d) 属性“%s”无效或不支持 无法处理的属性“%s”类型 %s 