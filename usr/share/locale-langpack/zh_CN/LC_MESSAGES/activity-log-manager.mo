��    D      <  a   \      �     �     �     �             8   5  �   n  �     #   �     �     �     �     �  	   �            9   ,  9   f     �  	   �     �     �  �   �     �	     �	     �	     �	     �	     �	     �	     �	  	   
     
     
     #
     )
     B
     T
     t
     }
     �
     �
     �
  !   �
     �
     �
                    1     D     _     |  /   �     �     �  O   �     ?     C  �   P     �     �     �          (     =  9   [  �  �     �     �  	   �  	   �     �     �  �   �  i   o     �     �     �          (     ;     H     [  2   q  '   �     �     �  	   �     �  �   �     �     �     �     �     �     �  	   	          )     6     =     D     K     [     h     �     �     �     �     �  $   �     �                     $     4  !   G  $   i      �  %   �     �     �  9   �     2     ?  �   N     �     �               &     @  k   `        !   C                  5   (                               3           $      <   @   ;          -   >   =   1   .   A   )                      4       	       D   *   6              '                  7   :                 
              B           #      8   0               %      "   +       2   &       9   /         ?      ,                       %d %B %Y %e %B %Y, %H:%M %u hour %u hours %u minute %u minutes %u second %u seconds 0 second (lock immediately) 0 seconds (lock immediately) <small>Error reports include information about what a program was doing when it failed. You always have the choice to send or cancel an error report.</small> <small>This includes things like how many programs are running, how much disk space the computer has, and what devices are connected.</small> Activities and Privacy Manager Tool Activity Activity Log Manager Add Application… Add Folder… Chat Logs Clear Usage Data Clear Usage Data… Configure what gets logged in your Zeitgeist activity log Delete records of which files and applications were used: Diagnostics Documents Exclude: Files & Applications Files and applications you’ve used recently can be shown in the Dash and elsewhere. If other people can see or access your user account, you may wish to limit which items are recorded. From all time From: In the past day In the past hour In the past week Include online search results Include: Invalid Timerange Last Used Music Name Never No description available Password Settings People using this computer can: Pictures Power Settings Presentations Privacy Policy Privacy and Activity Manager Record file and application usage Remove Item Require my password when: Search Security Security & Privacy Select Application Select a file to blacklist Select a folder to blacklist Send error reports to Canonical Send occasional system information to Canonical Show Previous Reports Spreadsheets This operation cannot be undone, are you sure you want to delete this activity? To: Today, %H:%M Ubuntu can collect anonymous information that helps developers improve it. All information collected is covered by our privacy policy. Videos When searching in the Dash: Yesterday, %H:%M _Returning from blank screen _Waking from suspend _if screen has been blank for privacy;activity;log;zeitgeist;diagnostics;error reports; Project-Id-Version: activity-log-manager
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-03-02 11:40+0000
PO-Revision-Date: 2015-04-13 01:14+0000
Last-Translator: Luo Lei <luolei@ubuntukylin.com>
Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2016-06-27 19:03+0000
X-Generator: Launchpad (build 18115)
Language: zh_CN
 %Y-%m-%d %Y-%m-%d, %H:%M %u 小时 %u 分钟 %u 秒 0 秒(立即锁定屏幕) <small>错误报告包括有关程序在出现故障时所产生的现象的信息。您始终可选择发送或取消错误报告。</small> 这些信息包括有多少程序正在运行，计算机有多少磁盘空间，连接哪些设备等。 活动与隐私管理工具 活动 活动日志管理器 添加应用程序... 添加文件夹... 聊天记录 清除使用记录 清除使用数据... 配置在 Zeitgeist 活动日志中记录的内容 删除文件和应用的使用记录： 诊断 文档 排除： 文件和应用 在 Dash 和其他地方能够显示您最近使用过的文件和应用程序。如果其他人能够看到或者访问您的用户帐号，您可能希望限制某些记录内容。 全部记录 发送方： 过去一天 过去一小时 过去一周 包含在线搜索结果 包括： 无效的时间范围 最近使用 音乐 名称 绝不 无可用描述 密码设置 使用此计算机的人可： 图片 电源设置 演示文稿 隐私政策 隐私与活动管理器 记录文件和应用的使用情况 删除项目 需要输入密码： 搜索 安全 安全和隐私 选择应用程序 选择要加入黑名单的文件 选择要加入黑名单的文件夹 向 Canonical 发送错误报告 发送少量系统信息给 Canonical 显示之前的报告 电子表格 此操作无法撤销，是否确定要删除此活动？ 接收方： 今天，%H:%M Ubuntu 可能收集部分匿名信息以帮助开发人员做出进一步改进。所有信息收集工作都会严格遵守我们的隐私政策。 视频 在 Dash 中搜索时： 昨天，%H:%M 从黑屏恢复(_R) 从挂起状态唤醒(_W) 如果屏幕一直是空白(_I) privacy;activity;log;zeitgeist;diagnostics;error reports;安全和隐私;活动;记录;错误报告;诊断; 