��    �     �  �  |-      �<  �   �<  c   b=  	   �=     �=     �=  �   �=  �   k>  �   4?  �   �?  !   �@     �@  	   �@     �@     A     A  "   A    4A     <D  #   WD  E   {D     �D  #   �D  !   �D  5   E  I   KE  8   �E  $   �E  3   �E  &   'F  +   NF     zF     �F  *   �F     �F  /   �F  ?   G  3   YG  2   �G  7   �G  /   �G  9   (H     bH  7   �H  .   �H  F   �H  D   .I  E   sI  C   �I  C   �I  =   AJ  <   J  8   �J  I   �J  A   ?K  #   �K  
   �K     �K     �K     �K  J   �K  B   9L  F   |L  A   �L  P   M     VM  R   sM  a   �M  G   (N  L   pN     �N     �N     �N     �N     O  *   O     GO     ZO     iO  	   rO  "   |O     �O     �O  (   �O  .   �O  /   P  V   @P  <   �P  9   �P  7   Q  !   FQ  )   hQ  &   �Q  +   �Q  )   �Q  9   R  Z   IR     �R  �   �R  M   >S  ?   �S  <   �S  >   	T  i   HT  k   �T  ]   U     |U  #   �U     �U     �U  �   �U  -   �V     �V     �V  C   �V  +   /W     [W  ?   nW     �W     �W  
   �W  "   �W      X     	X  n   X  n   �X     �X  3   Y     ?Y     LY     YY  2   qY  6   �Y  N   �Y  M   *Z  P   xZ  Q   �Z  	   [  
   %[  #   0[  %   T[     z[     �[  J   �[     �[  )   �[     \  r   \     �\     �\  )   �\     �\  
   �\     �\     �\  1   
]  �   <]     �]     �]  3   ^     ?^     X^     f^     z^     �^     �^     �^     �^  )   �^     �^     _  G   _     Z_     l_  :   y_  f   �_  S   `     o`     �`     �`  	   �`  $   �`     �`     �`  
   �`  %   �`     a     4a  1   Ma     a  	   �a     �a  	   �a     �a  :   �a  7   �a  :   ,b     gb     wb     �b     �b     �b     �b  (   �b     �b     c     c  "   !c     Dc     Jc     Pc     dc     tc     �c     �c     �c     �c     �c  
   �c     �c  /   �c  *   d  )   ;d  9   ed     �d     �d     �d  -   �d  
   e  [  e  E  hg     �h  �   �i  �  ^j  �  =l  �  n  �   �o  �   Zp  �   2q  �   r  �   �r    s  /  �t  �   �u    �v    �w  �   �x  �  ny  �   -{  �   �{  '  �|  �   �}  �   �~  �   O  (  A�    j�  T  ��  K  ކ  �   *�  �   �  ?  ˊ  �  �  �  ��  �  q�  K  h�  ~  ��  o  3�  >  ��  V  �  h  9�  �   ��  �   b�  �   ,�  )  )�  m  S�  m  ��  o  /�  q  ��  m  �  �  �  n  �  r  ��  q  ��  �  j�  �  �  �  �  �  z�  �  *�  �  ��  �  N�  �  �  �  ��  �  F�  �  ۽  U  r�  c  ��  M  ,�  \  z�  �  ��  �  ��  �  ��  �  ��  �  q�  �  `�  �  >�  �  "�  �  �  �  ��    ��  �  ��  �  ��    ��    ��  �  ��  .  ��  \  �    c�  |  w�  �   ��  �  ��  �  j�  �  ��  �  ��    0�    B�  �   R�    /�  %  =�    c�    t�    ��  V  ��  \  ��  <  P�    �  �   � �   M �   4   � 4  � �    R   Z  _	 �   �
 �   � �   E �  � �  � k  � `  � A  Z y  � �    �   � �   � 2  _ j  �   � ,   �   /      / ?  7  5  w! h  �" -  $   D& ;  \' �   �( �   Z) `  S* N  �+ �   .   �. �  �/ �  {1 �  E3 S  +5 �   6 �   ,7 I  �7 �   9 �  �9 ]  �; 4  = Q  K> �  �? U  'A ,  }B e  �C H  E �   YF   8G   OH E  WI �   �J   hK   qL �  �M �   �O   fP 3  ~Q ,  �R �   �S   �T   �U �  W j   �X i   Y i   �Y h   �Y I  WZ   �\ �  �^ �  Ba �   c   
d �  f �   �g �   �h �   �i �   =j �  k �   n   �n    �o �   	q �   �q �   �r N  {s @  �t F  v 9  Rw J  �x �   �y �   �z �   �{   m|   v} �   �~ +  R   ~�   �� �  �� �  c� f  =� ]  �� 5  � D  8� �  }� �  � �  Ҏ �  �� �  � f   ��     �    ?� @   _�    ��    �� 8   �� P   ��    H� '   ]� 1   ��    ��    Ε    �    ��    � #   !� :   E� *   ��    �� (   �� -   � 0   �    F�    X�    ^� Z   f� .   ��    �    �    -�    I�    g� /   ��    ��    Ƙ X   �� U   9�    ��    ��    ��    љ    � '   � *   /� 2   Z� 5   �� )   Ú    �    �     �    ;�    V�    s�    �� [   �� X   � [   e� X   ��    � 7   .� ?   f� $   ��    ˝ A   ٝ 9   � J   U� 1   �� J   Ҟ H   � i   f� >   П J   � D   Z� (   ��    Ƞ 
   � �   � 
   �� 
   ��    �� .   ˡ 3   �� .   .� 3   ]�    ��    ��    ��    �� L  ͢    �    .�    <�    C�    P� #   b� !   ��    ��    ��    Ȥ    פ    �    �    �     �    0�    D�    S�    Y�    _�    r� $   w� 
   ��    ��    ��    Х @   � A   (� B   j� B   �� A   � B   2� A   u� A   �� D   �� B   >� E   ��    Ǩ *   ب �   � 0   �� #   ݩ K   �     M� E   n� B   �� �   ��    �� 0   �� !   ֫ `   ��    Y� 7   y� F   �� 2   �� 0   +�    \� �   c� �   �� p   �� �   � m   �� �   � o   �� �   � l   �� a   � N   g� �   �� ;   d� 	   ��    �� A  ȳ `   
� 9   k� 0   �� o   ֵ �   F� �   � }   �� �   � �   �� Z   0� W   �� �   � �   �� :   x� *   �� B   ޻ �   !� {   �� �   s� �   	� {   �� �   � }   �� �   4� {   �� �   `� }   � �   �� {   <� �   ��    f� �   ��    �� �   � {   �� �   F� \   �� [   Q� E   �� F   �� �   :� �  �� Q   �� O   �� �   0� U   )�    � N   ��    ��    �� D   
� 	   O�    Y�    a� 	   j�    t�    ��    ��    ��    ��    �� E   �� 0   *� +   [� &   �� 3   ��    �� @   � F   B� ?   �� /   �� @   �� 5   :� V   p� H   �� e   �    v�    �� �   �� #   U�    y�    �� ?   ��    �� 0   ��    �    (�    .�    6� 	   :�    D�    P�    `�    f�    r�    w� 5   ��    ��    �� A   �� 6   2�    i�    l�    q� 
   v�     �� !   �� @   ��    � �  	� �   �� f   w�    ��    ��    �� `   �� �   Z� �   � �   ��    v�    ��    ��    �� 
   ��    �� "   �� �  ��    �� $   � ;   (�    d� %   t� %   �� 4   �� <   �� 3   2�     f� 5   ��    �� ,   ��    
�    "� !   =�    _� '   r� 9   �� =   �� ;   � 2   N� &   �� .   ��    �� 0   �� /   � E   K� E   �� D   �� D   � 9   a� /   �� *   ��    �� $   � B   :� %   }�    ��    ��    ��    �� A   �� 6   4� 6   k� 8   �� E   ��    !� 6   =� \   t� @   �� <   �    O�    k�    x�    ��    �� -   ��    ��    �� 
   ��    � #   �    B�    R� +   b� '   �� *   �� U   �� ?   7� 5   w� 5   �� !   �� +   � '   1� '   Y� %   �� <   �� N   ��    3� {   I� ?   �� K   � ?   Q� ?   �� T   �� Z   &� N   ��    �� -   ��    �    -� �   :� *   ��    ��    � 5   .� '   d�    �� >   ��    ��    ��    ��    �    �    "� c   8� c   ��     � -   �    B�    U�    h� -   x� 2   �� 9   �� 6   � <   J� <   ��    ��    �� $   �� !   �    %�    2� T   9�    �� -   ��    �� o   ��    O�    `�    g�    ��    ��    ��    �� 4   �� �   �     ��    �� 4   ��    ��    �    "�    ?�    L�    Y�    f�    m� '   }�    ��    �� 9   ��    ��    � 9   � b   M� W   ��    � 	   �    �    &� )   3�    ]�    m�    ��    ��    ��    �� 3   ��     �    �    �    '�    7� 0   I� #   z� 6   ��    ��    ��    ��    �    �    � "   )�    L�    _�    o� "   �    ��    ��    ��    ��    �� 	   ��    ��    �    � 	   �    )� 	   6� <   @� %   }� %   �� 1   ��    ��    �    � 8   +� 
   d� 6  o� J  ��   �  �   � �  � �  ? f  � �   ? �   � �   � �   |	 �   B
   �
 5  � �    �    �   � �   � �  � �   ( �   �   � �   � �    �   � �  u �  L    �   �    �   � E  � ^  �  j  Q# �  �% /  }' ?  �( >  �* 1  ,, '  ^- -  �. �   �/ �   �0 �   Q1   ;2 >  W3 8  �4 :  �5 <  
7 8  G8 D  �9 >  �: B  < @  G= �  �> �  @ w  �A �  6C G  �D D  F F  PG �  �H D  'J F  lK H  �L 6  �M ?  3O .  sP 7  �Q �  �R �  ~T �  V �  �W �  dY �  
[ �  �\ �  ?^ �  �_ �  �a �  .c �  �d �  �f �  Ch �  j �  �k �  �m >  �o �  �p u  �r    t |  u l  �v �  	x t  �y �  �z �  �| �   �~ �   �   ̀ �   ߁ �   ܂   փ _  ܄ y  <� E  �� �   �� �   ��   Ԋ �   � �   ��   z� �   �� )  y� /  �� �   Ӓ �   �� �   Z� {  � �  �� �   � J  � ]  [� �  �� �   S� �   �� �   Ӟ C  s�   ��   7� D  @� �   ��   Y� 
  i� F  t� =  �� y  ��   s� ?  �� +  Ϯ �   �� �   ǰ M  ��   �� �   �   ҵ -  Զ �  � �  �� +  s� �   �� �   ;� +  ֽ �   � �  �� B  J� ?  �� /  �� k  �� R  i� �   �� 0  �� S  �� �   9�   �   8� S  Q� �   �� �   g�   d� �  j� �   :�   � D  �   d� �   |� /  \� /  �� v  �� V   3� U   �� V   �� U   7� �  �� �  �� 	  C� �  M� �   �� �  �� �  �� �   8� �   .� �   *� �   �� }  �� �   � 	   � +  
� �   6� �   �� �   �� Q  �� E  '� I  m� =  �� N  �� �   D� �   8� �   ,�   �   2  �   I 9     Q   c   �   � �  �	 �  - P  � [   �  a �  
 �  � g  d �  � o   �    �     <   +    h    { +   � N   �     -    3   E    y    �    �    � 	   �    � -   � !   (    J !   Z    | !   �    �    �    � T   � 2   ? $   r $   � $   � &   �     B   &    i    � ^   � ]       b    |    �    �    � -   � .     8   O 9   � /   �    �         1     R "   s $   � #   � b   � a   B  a   �  `   !    g! >   �! 6   �! +   �!    #" I   6" E   �" U   �" :   # J   W# J   �# h   �# I   V$ J   �$ A   �$     -%    N%    g% �   t%    &    '&    4& ,   J& 1   w& ,   �& 1   �&    '    '    '    ,'   ?'    T(    m(    z(    �(    �(     �(     �(    �(    �(    )    )    ))    ?)    L)    Y)    h)    �) 	   �) 	   �)    �)    �) &   �)    �)    �)    * !   * =   8* >   v* ?   �* ?   �* >   5+ ?   t+ >   �+ >   �+ A   2, ?   t, B   �,    �, %   - �   *- @   �- $    . Z   %.    �. @   �. C   �. �   /    �/ $   �/    �/ A   �/    80 2   T0 0   �0 +   �0 .   �0 	   1 {   1 m   �1 a   2 k   i2 _   �2 k   53 _   �3 i   4 ]   k4 N   �4 7   5 �   P5 <   �5    6    &6 )  B6 Z   l7 7   �7 '   �7 g   '8 �   �8 w   /9 k   �9 q   : �   �: N   ; K   _; �   �; �   X< 5   
= /   @= M   p= �   �= b   y> �   �> Z   ^? m   �? �   '@ o   �@ �   (A m   �A �   )B o   �B �   *C m   �C �   +D q   �D �   .E v   �E �   :F m   �F �   =G \   �G [   +H E   �H F   �H �   I 9  �I @   �J @   ;K �   |K P   FL    �L O   �L    M    M ?   $M    dM    qM    xM 
   �M    �M 	   �M    �M    �M    �M    �M 2   N 2   7N 0   jN +   �N 5   �N !   �N <   O E   \O <   �O 5   �O B   P ;   XP K   �P B   �P {   #Q    �Q    �Q �   �Q    jR    �R    �R K   �R    �R 1   S    4S 
   KS 
   VS    aS    iS    wS    �S 
   �S    �S    �S    �S ,   �S     T    T A   /T /   qT    �T    �T    �T    �T    �T    �T 4   �T    +U    q  M  �   �  �         �  �   �  �   J       R    h  �     �       f  �  �  �   �  v  �       ;   �   3  
  �  E  N  �  �               �       z  �  �   f  �      G   �   �      o  |      �   �   �  z  e   �           �   �  �  i  �  �  �         }      m   �   �   %      V       �        `       4  �  �  �   >         4  �       P  /  �  p  _  g  �  M  Y   �  K      �                 �   �       (  �       R      �  �  �   U  C            �        �    �   .  �      i      �   T  H  ]   �   �  *  �   e  �  �  S    �  �  f       �     b  �   �          �      �   �               �  -                  �   =  -  t   �  .  5    �  o  j  z   �  y           9  !       �        =           �      �  �      �  �      �  B  )  ^          I  �  �  �  @   �     &      �  C      Z  =  )  �   h  ^              �   �      �   "         �  <       �   8   s             �   u      6      `  �  x  �      Q   �    �   F  �       �  �      Y      �          o   w      �   #       �   ~      �              u  Z  A       �      �   �   �  �  �  �          n      �  !          �    ;  A  �  G  �  m  �  �       �  �  �  �  �  M   �  �     �   �  �     �      #      �   �  F       �  V              '   D    �   ?              2           :      �              Q     5            3  �      �       ,  /   �  �  &   6  0  �   �  S   
   �  �  V    �     �       W      r  �   �  �         �  �   d  �    b       �   �   �  A      d  �  �       �  t  �      l   Y  �   �          �      �  �     �           �         �      b  "       w  �          �  B  ,   �  �      �  �   �   �  O   �     �  ^   �      +  �   �  ;  �   8          d   �      �  6   �        �      �  [  �           W   G  O  �   �     �  q   /  :      �  �  �              x   5  E       P  y  ,  �  {          �  �       �            �   �   �   �   �  |    �  9        \        >  �      H         J  �  _   8    �  �          x  (  3   �  %   D   ?  �      4   �    {  +       n  �       �  �   Z   �  �  n  �      �   [       	                   
  �          J  \   g   Q          2  �                          �  C  �  )   k  L     �   j   !      �  F  $      �   �  h       �  $           �   �  p  `  q  �   �   �  0   �      �      �   �       m  O    w   �       �       �  �       �   c  �  v       �   �  K   9         0  �   s   P       ]  �      I  �  7  <  7           \  �    +  �  �   �  	  1    �  �           �                   k      �   �     �   r  �       �  �  T     �   "         [      N       �  �    �  �   �  �                               �   B           �           �  �  �  �  }  �   �     �  X       $      H   a  �      L      j  -   1              l  �   �           �  U      �    �        �      �  �  �      E  W  �  2  �   c   �     �  K      .   '  �   �  c  �      �  �        v  y   X          �  �   �  �      *   �         �          >   �   �  t      k     #  g  �  �   �  �   �  a  @  s    _  u       �  }   �       ~                  �  �   �      �    �  {   |   �      S  �      �  �       �   I   �   ]    7  p       X  r   �           a   �  ?   �  D            N  �  :   l  �   �   �   U   ~         �  T     �  @  �           �  �       �  *  �  �   R   (       �  i   �  e             �    �  <  &            	      '    1       �         %  �     �   �       �             �           �  L       

Warning: Some tests could cause your system to freeze or become unresponsive. Please save all your work and close all other running applications before beginning the testing process.    Determine if we need to run tests specific to portable computers that may not apply to desktops.  Results   Run   Selection   Takes multiple pictures based on the resolutions supported by the camera and
 validates their size and that they are of a valid format.  Test that the system's wireless hardware can connect to a router using the
 802.11a protocol. This requires that you have a router pre-configured to only
 respond to requests on the 802.11a protocol.  Test that the system's wireless hardware can connect to a router using the
 802.11b protocol. This requires that you have a router pre-configured to only
 respond to requests on the 802.11b protocol.  Test that the system's wireless hardware can connect to a router using the
 802.11g protocol. This requires that you have a router pre-configured to only
 respond to requests on the 802.11g protocol. %(key_name)s key has been pressed &No &Previous &Skip this test &Test &Yes 10 tests completed out of 30 (30%) <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'Ubuntu'; font-size:10pt; font-weight:400; font-style:normal;">
<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=;">Please enter the e-mail address associated with your Launchpad account (if applicable)</span></p>
<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=;">and click the Submit Results button to submit these test results to Launchpad.</span></p></body></html> Abort signal from abort(3) All required keys have been tested! Archives the piglit-summary directory into the piglit-results.tar.gz. Are you sure? Attach log from fwts wakealarm test Attach log from rendercheck tests Attaches a copy of /var/log/dmesg to the test results Attaches a dump of the udev database showing system hardware information. Attaches a list of the currently running kernel modules. Attaches a report of CPU information Attaches a report of installed codecs for Intel HDA Attaches a report of sysfs attributes. Attaches a tarball of gcov data if present. Attaches dmidecode output Attaches info on DMI Attaches information about disk partitions Attaches lshw output Attaches the FWTS results log to the submission Attaches the audio hardware data collection log to the results. Attaches the bootchart log for bootchart test runs. Attaches the bootchart png file for bootchart runs Attaches the contents of /proc/acpi/sleep if it exists. Attaches the contents of the /etc/modules file. Attaches the contents of the various modprobe conf files. Attaches the firmware version Attaches the graphics stress results to the submission. Attaches the installer debug log if it exists. Attaches the log from the 250 cycle Hibernate/Resume test if it exists Attaches the log from the 250 cycle Suspend/Resume test if it exists Attaches the log from the 30 cycle Hibernate/Resume test if it exists Attaches the log from the 30 cycle Suspend/Resume test if it exists Attaches the log from the single suspend/resume test to the results Attaches the log generated by cpu/scaling_test to the results Attaches the output of udev_resource, for debugging purposes Attaches the screenshot captured in graphics/screenshot. Attaches the screenshot captured in graphics/screenshot_fullscreen_video. Attaches very verbose lspci output (with central database Query). Attaches very verbose lspci output. Audio Test Audio tests Automated CD write test Automated DVD write test. Automated check of the 30 cycle hibernate log for errors detected by fwts. Automated check of the hibernate log for errors discovered by fwts Automated check of the suspend log to look for errors reported by fwts Automated job to generate the PXE verification test for each NIC. Automated job to generate the Remote Shared IPMI verification test for each NIC. Automated optical read test. Automated test case to make sure that it's possible to download files through HTTP Automated test case to verify availability of some system on the network using ICMP ECHO packets. Automated test to store bluetooth device information in checkbox report Automated test to walk multiple network cards and test each one in sequence. Benchmark for each disk Benchmarks tests Bluetooth Test Bluetooth tests Bootchart information. Broken pipe: write to pipe with no readers Building report... CD write test. CPU Test CPU tests CPU utilization on an idle system. Camera Test Camera tests Check failed result from shell test case Check job is executed when dependency succeeds Check job is executed when requirements are met Check job result is set to "not required on this system" when requirements are not met Check job result is set to uninitiated when dependency fails Check logs for the stress poweroff (100 cycles) test case Check logs for the stress reboot (100 cycles) test case Check stats changes for each disk Check success result from shell test case Check that VESA drivers are not in use Check that hardware is able to run Unity 3D Check that hardware is able to run compiz Check the time needed to reconnect to a WIFI access point Check to see if CONFIG_NO_HZ is set in the kernel (this is just a simple regression check) Checkbox System Testing Checkbox did not finish completely.
Do you want to rerun the last test,
continue to the next test, or
restart from the beginning? Checks that a specified sources list file contains the requested repositories Checks the battery drain during idle.  Reports time until empty Checks the battery drain during suspend.  Reports time until Checks the battery drain while watching a movie.  Reports time Checks the length of time it takes to reconnect an existing wifi connection after a suspend/resume cycle. Checks the length of time it takes to reconnect an existing wired connection
 after a suspend/resume cycle. Checks the sleep times to ensure that a machine suspends and resumes within a given threshold Child stopped or terminated Choose tests to run on your system: Codec tests Collapse All Collect audio-related system information. This data can be used to simulate this computer's audio subsystem and perform more detailed tests under a controlled environment. Collect info on color depth and pixel format. Collect info on fresh rate. Collect info on graphic memory. Collect info on graphics modes (screen resolution and refresh rate) Combine with character above to expand node Command not found. Command received signal %(signal_name)s: %(signal_description)s Comments Common Document Types Test Components Configuration override parameters. Continue Continue if stopped Creates a mobile broadband connection for a CDMA based modem and checks the connection to ensure it's working. Creates a mobile broadband connection for a GSM based modem and checks the connection to ensure it's working.  DVD write test. Dependencies are missing so some jobs will not run. Deselect All Deselect all Detailed information... Detects and displays disks attached to the system. Detects and shows USB devices attached to this system. Determine whether the screen is detected as a multitouch device automatically. Determine whether the screen is detected as a non-touch device automatically. Determine whether the touchpad is detected as a multitouch device automatically. Determine whether the touchpad is detected as a singletouch device automatically. Disk Test Disk tests Disk utilization on an idle system. Do you really want to skip this test? Don't ask me again Done Dumps memory info to a file for comparison after suspend test has been run Email Email address must be in a proper format. Email: Ensure the current resolution meets or exceeds the recommended minimum resolution (800x600). See here for details: Enter text:
 Error Exchanging information with the server... Executing %(test_name)s Expand All ExpressCard Test ExpressCard tests FAIL: {}% packet loss is higherthan {}% threshold Failed to contact server. Please try
again or upload the following file name:
%s

directly to the system database:
https://launchpad.net/+hwdb/+submit Failed to open file '%s': %s Failed to process form: %s Failed to upload to server,
please try again later. Fingerprint reader tests Firewire Test Firewire disk tests Floating point exception Floppy disk tests Floppy test Form Further information: Gathering information from your system... Graphics Test Graphics tests Hangup detected on controlling terminal or death of controlling process Hibernation tests Hotkey tests I will exit automatically once all keys have been pressed. If a key is not present in your keyboard, press the 'Skip' button below it to remove it from the test. If your keyboard lacks one or more keys, press its number to skip testing that key. Illegal Instruction In Progress Info Info Test Information not posted to Launchpad. Informational tests Input Devices tests Input Test Internet connection fully established Interrupt from keyboard Invalid memory reference Jobs will be reordered to fix broken dependencies Key test Keys Test Kill signal LED tests List USB devices Lists the device driver and version for all audio devices. Make sure that the RTC (Real-Time Clock) device exists. Maximum disk space used during a default installation test Media Card Test Media Card tests Memory Test Memory tests Miscellanea Test Miscellaneous tests Missing configuration file as argument.
 Mobile broadband tests Monitor Test Monitor tests Move a 3D window around the screen Ne&xt Ne_xt Network Information Networking Test Networking tests Next No Internet connection Not Resolved Not Started Not Supported Not Tested Not required One of debug, info, warning, error or critical. Open and close 4 3D windows multiple times Open and close a 3D window multiple times Open, suspend resume and close a 3D window multiple times Optical Drive tests Optical Test Optical read test. PASS: {}% packet loss is within {}% threshold PASSWORD:  PURPOSE:
     Check that external line out connection works correctly
STEPS:
     1. Insert cable to speakers (with built-in amplifiers) on the line out port
     2. Open system sound preferences, 'Output' tab, select 'Line-out' on the connector list. Click the Test button
     3. On the system sound preferences, select 'Internal Audio' on the device list and click 'Test Speakers' to check left and right channel
VERIFICATION:
     1. Do you hear a sound in the speakers? The internal speakers should *not* be muted automatically
     2. Do you hear the sound coming out on the corresponding channel? PURPOSE:
    Audio Mute LED verification.
STEPS:
    Skip this test if your system does not have a special Audio Mute LED.
    1. Press the Mute key twice and observe the Audio LED to determine if it
    either turned off and on or changed colors.
VERIFICATION:
    Did the Audio LED turn on and off change color as expected? PURPOSE:
    Block cap keys LED verification
STEPS:
    1. Press "Block Cap Keys" to activate/deactivate cap keys blocking
    2. Cap Keys LED should be switched on/off every time the key is pressed
VERIFICATION:
    Did the Cap Keys LED light as expected? PURPOSE:
    Camera LED verification
STEPS:
    1. Select Test to activate camera
    2. Camera LED should light for a few seconds
VERIFICATION:
    Did the camera LED light? PURPOSE:
    Check that balance control works correctly on external headphone
STEPS:
    1. Check that moving the balance slider from left to right works smoothly
    2. Click the Test button to play an audio tone for 10 seconds.
    3. Move the balance slider from left to right and back.
    4. Check that actual headphone audio balance follows your setting.
VERIFICATION:
    Does the slider move smoothly, as well as being followed by the setting by the actual audio output? PURPOSE:
    Check that balance control works correctly on internal speakers
STEPS:
    1. Check that moving the balance slider from left to right works smoothly
    2. Click the Test button to play an audio tone for 10 seconds.
    3. Move the balance slider from left to right and back.
    4. Check that actual speaker audio balance follows your setting.
VERIFICATION:
    Does the slider move smoothly, as well as being followed by the setting by the actual audio output? PURPOSE:
    Check that external line in connection works correctly
STEPS:
    1. Use a cable to connect the line in port to an external line out source.
    2. Open system sound preferences, 'Input' tab, select 'Line-in' on the connector list. Click the Test button
    3. After a few seconds, your recording will be played back to you.
VERIFICATION:
    Did you hear your recording? PURPOSE:
    Check that the various audio channels are working properly
STEPS:
    1. Click the Test button
VERIFICATION:
    You should clearly hear a voice from the different audio channels PURPOSE:
    Check touchscreen drag & drop
STEPS:
    1. Double tap, hold, and drag an object on the desktop
    2. Drop the object in a different location
VERIFICATION:
    Does the object select and drag and drop? PURPOSE:
    Check touchscreen pinch gesture for zoom
STEPS:
    1. Place two fingers on the screen and pinch them together
    2. Place two fingers on the screen and move then apart
VERIFICATION:
    Does the screen zoom in and out? PURPOSE:
    Check touchscreen tap recognition
STEPS:
    1. Tap an object on the screen with finger. The cursor should jump to location tapped and object should highlight
VERIFICATION:
    Does tap recognition work? PURPOSE:
    Create jobs that use the CPU as much as possible for two hours. The test is considered passed if the system does not freeze. PURPOSE:
    DisplayPort audio interface verification
STEPS:
    1. Plug an external DisplayPort device with sound (Use only one HDMI/DisplayPort interface at a time for this test)
    2. Click the Test button
VERIFICATION:
    Did you hear the sound from the DisplayPort device? PURPOSE:
    Do some challenging operations then check for lockup on the GPU
STEPS:
    1. Create 2 glxgears windows and move them quickly
    2. Switch workspaces with wmctrl
    3. Launch an HTML5 video playback in firefox
VERIFICATION:
    After a 60s workload, check kern.log for reported GPU errors PURPOSE:
    HDD LED verification
STEPS:
    1. Select "Test" to write and read a temporary file for a few seconds
    2. HDD LED should light when writing to/reading from HDD
VERIFICATION:
    Did the HDD LED light? PURPOSE:
    HDMI audio interface verification
STEPS:
    1. Plug an external HDMI device with sound (Use only one HDMI/DisplayPort interface at a time for this test)
    2. Click the Test button
VERIFICATION:
    Did you hear the sound from the HDMI device? PURPOSE:
    Keep tester related information in the report
STEPS:
    1. Tester Information
    2. Please enter the following information in the comments field:
       a. Name
       b. Email Address
       c. Reason for this test run
VERIFICATION:
    Nothing to verify for this test PURPOSE:
    Manual detection of accelerometer.
STEPS:
    1. Look at the specifications for your system.
VERIFICATION:
    Is this system supposed to have an accelerometer? PURPOSE:
    Numeric keypad LED verification
STEPS:
    1. Press "Block Num" key to toggle numeric keypad LED
    2. Click on the "Test" button to open a window to verify your typing
    3. Type using the numeric keypad both when the LED is on and off
VERIFICATION:
    1. Numeric keypad LED status should toggle everytime the "Block Num" key is pressed
    2. Numbers should only be entered in the keyboard verification window when the LED is on PURPOSE:
    Power LED verification
STEPS:
    1. Power LED should be on while device is switched on
VERIFICATION:
    Does the power LED light as expected? PURPOSE:
    Power LED verification
STEPS:
    1. The Power LED should blink or change color while the system is suspended
VERIFICATION:
    Did the Power LED blink or change color while the system was suspended for the previous suspend test? PURPOSE:
    Suspend LED verification.
STEPS:
    Skip this test if your system does not have a dedicated Suspend LED.
    1. The Suspend LED should blink or change color while the system is
    suspended
VERIFICATION
    Did the Suspend LED blink or change color while the system was suspended? PURPOSE:
    Take a screengrab of the current screen (logged on Unity desktop)
STEPS:
    1. Take picture using USB webcam
VERIFICATION:
    Review attachment manually later PURPOSE:
    Take a screengrab of the current screen after suspend (logged on Unity desktop)
STEPS:
    1. Take picture using USB webcam
VERIFICATION:
    Review attachment manually later PURPOSE:
    Take a screengrab of the current screen during fullscreen video playback
STEPS:
    1. Start a fullscreen video playback
    2. Take picture using USB webcam after a few seconds
VERIFICATION:
    Review attachment manually later PURPOSE:
    Tests that the systems wireless hardware can connect to a router using
    no security and the 802.11b/g protocols.
STEPS:
    1. Open your routers configuration tool
    2. Change the settings to only accept connections on the B and G wireless bands
    3. Make sure the SSID is set to ROUTER_SSID
    4. Change the security settings to use no security
    5. Click the 'Test' button to create a connection to the router and test the connection
VERIFICATION:
    Verification is automated, do not change the automatically selected result. PURPOSE:
    Tests that the systems wireless hardware can connect to a router using
    no security and the 802.11n protocol.
STEPS:
    1. Open your routers configuration tool
    2. Change the settings to only accept connections on the N wireless band
    3. Make sure the SSID is set to ROUTER_SSID
    4. Change the security settings to use no security
    5. Click the 'Test' button to create a connection to the router and test the connection
VERIFICATION:
    Verification is automated, do not change the automatically selected result. PURPOSE:
    Tests that the systems wireless hardware can connect to a router using WPA
    security and the 802.11b/g protocols.
STEPS:
    1. Open your routers configuration tool
    2. Change the settings to only accept connections on the B and G wireless bands
    3. Make sure the SSID is set to ROUTER_SSID
    4. Change the security settings to use WPA2 and ensure the PSK matches that set in ROUTER_PSK
    5. Click the 'Test' button to create a connection to the router and test the connection
VERIFICATION:
    Verification is automated, do not change the automatically selected result. PURPOSE:
    Tests that the systems wireless hardware can connect to a router using WPA
    security and the 802.11n protocols.
STEPS:
    1. Open your routers configuration tool
    2. Change the settings to only accept connections on the N wireless band
    3. Make sure the SSID is set to ROUTER_SSID
    4. Change the security settings to use WPA2 and ensure the PSK matches that set in ROUTER_PSK
    5. Click the 'Test' button to create a connection to the router and test the connection
VERIFICATION:
    Verification is automated, do not change the automatically selected result. PURPOSE:
    This test checks that the manual plugin works fine
STEPS:
    1. Add a comment
    2. Set the result as passed
VERIFICATION:
    Check that in the report the result is passed and the comment is displayed PURPOSE:
    This test cycles through the detected video modes
STEPS:
    1. Click "Test" to start cycling through the video modes
VERIFICATION:
    Did the screen appear to be working for each mode? PURPOSE:
    This test tests the basic 3D capabilities of your video card
STEPS:
    1. Click "Test" to execute an OpenGL demo. Press ESC at any time to close.
    2. Verify that the animation is not jerky or slow.
VERIFICATION:
    1. Did the 3d animation appear?
    2. Was the animation free from slowness/jerkiness? PURPOSE:
    This test verifies that multi-monitor output works on your desktop system. This is NOT the same test as the external monitor tests you would run on your laptop.  You will need two monitors to perform this test.
STEPS:
    Skip this test if your video card does not support multiple monitors.
    1. If your second monitor is not already connected, connect it now
    2. Open the "Displays" tool (open the dash and search for "Displays")
    3. Configure your output to provide one desktop across both monitors
    4. Open any application and drag it from one monitor to the next.
VERIFICATION:
    Was the stretched desktop displayed correctly across both screens? PURPOSE:
    This test will check suspend and resume
STEPS:
    1. Click "Test" and your system will suspend for about 30 - 60 seconds
    2. Observe the Power LED to see if it blinks or changes color during suspend
    3. If your system does not wake itself up after 60 seconds, please press the power button momentarily to wake the system manually
    4. If your system fails to wake at all and must be rebooted, restart System Testing after reboot and mark this test as Failed
VERIFICATION:
    Did your system suspend and resume correctly?
    (NOTE: Please only consider whether the system successfully suspended and resumed. Power/Suspend LED verification will occur after this test is completed.) PURPOSE:
    This test will check that a DSL modem can be configured and connected.
STEPS:
    1. Connect the telephone line to the computer
    2. Click on the Network icon on the top panel.
    3. Select "Edit Connections"
    4. Select the "DSL" tab
    5. Click on "Add" button
    6. Configure the connection parameters properly
    7. Click "Test" to verify that it's possible to establish an HTTP connection
VERIFICATION:
    Did a notification show and was the connection correctly established? PURPOSE:
    This test will check that a USB audio device works correctly
STEPS:
    1. Connect a USB audio device to your system
    2. Click "Test", then speak into the microphone
    3. After a few seconds, your speech will be played back to you
VERIFICATION:
    Did you hear your speech played back through the USB headphones? PURPOSE:
    This test will check that bluetooth connection works correctly
STEPS:
    1. Enable bluetooth on any mobile device (PDA, smartphone, etc.)
    2. Click on the bluetooth icon in the menu bar
    3. Select 'Setup new device'
    4. Look for the device in the list and select it
    5. In the device write the PIN code automatically chosen by the wizard
    6. The device should pair with the computer
    7. Right-click on the bluetooth icon and select browse files
    8. Authorize the computer to browse the files in the device if needed
    9. You should be able to browse the files
VERIFICATION:
    Did all the steps work? PURPOSE:
    This test will check that headphones connector works correctly
STEPS:
    1. Connect a pair of headphones to your audio device
    2. Click the Test button to play a sound to your audio device
VERIFICATION:
    Did you hear a sound through the headphones and did the sound play without any distortion, clicks or other strange noises from your headphones? PURPOSE:
    This test will check that internal speakers work correctly
STEPS:
    1. Make sure that no external speakers or headphones are connected
       If testing a desktop, external speakers are allowed
    2. Click the Test button to play a brief tone on your audio device
VERIFICATION:
    Did you hear a tone? PURPOSE:
    This test will check that recording sound using an external microphone works correctly
STEPS:
    1. Connect a microphone to your microphone port
    2. Click "Test", then speak into the external microphone
    3. After a few seconds, your speech will be played back to you
VERIFICATION:
    Did you hear your speech played back? PURPOSE:
    This test will check that recording sound using the onboard microphone works correctly
STEPS:
    1. Disconnect any external microphones that you have plugged in
    2. Click "Test", then speak into your internal microphone
    3. After a few seconds, your speech will be played back to you.
VERIFICATION:
    Did you hear your speech played back? PURPOSE:
    This test will check that the built-in camera works
STEPS:
    1. Click on Test to display a still image from the camera for ten seconds.
VERIFICATION:
    Did you see the image? PURPOSE:
    This test will check that the built-in camera works
STEPS:
    1. Click on Test to display a video capture from the camera for ten seconds.
VERIFICATION:
    Did you see the video capture? PURPOSE:
    This test will check that the display is correct after suspend and resume
STEPS:
    1. Check that your display does not show up visual artifacts after resuming.
VERIFICATION:
    Does the display work normally after resuming from suspend? PURPOSE:
    This test will check that the system can switch to a virtual terminal and back to X
STEPS:
    1. Click "Test" to switch to another virtual terminal and then back to X
VERIFICATION:
    Did your screen change temporarily to a text console and then switch back to your current session? PURPOSE:
    This test will check that the system correctly detects
    the removal of a CF card from the systems card reader.
STEPS:
    1. Click "Test" and remove the CF card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of a MS card from the systems card reader.
STEPS:
    1. Click "Test" and remove the MS card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of a MSP card from the systems card reader.
STEPS:
    1. Click "Test" and remove the MSP card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of a SDXC card from the systems card reader.
STEPS:
    1. Click "Test" and remove the SDXC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of a xD card from the systems card reader.
STEPS:
    1. Click "Test" and remove the xD card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of an SD card from the systems card reader
    after the system has been suspended.
STEPS:
    1. Click "Test" and remove the SD card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of an SD card from the systems card reader.
STEPS:
    1. Click "Test" and remove the SD card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of an SDHC card from the systems card reader.
STEPS:
    1. Click "Test" and remove the SDHC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects
    the removal of the MMC card from the systems card reader.
STEPS:
    1. Click "Test" and remove the MMC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the insertion of
    a USB 3.0 storage device
STEPS:
    1. Click "Test" and insert a USB 3.0 storage device (pen-drive/HDD) in
       a USB 3.0 port. (Note: this test will time-out after 20 seconds.)
    2. Do not unplug the device after the test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the insertion of
    a USB 3.0 storage device after suspend and resume.
STEPS:
    1. Click "Test" and insert a USB 3.0 storage device (pen-drive/HDD) in
       a USB 3.0 port. (Note: this test will time-out after 20 seconds.)
    2. Do not unplug the device after the test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the insertion of
    a USB storage device
STEPS:
    1. Click "Test" and insert a USB storage device (pen-drive/HDD).
       (Note: this test will time-out after 20 seconds.)
    2. Do not unplug the device after the test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the insertion of
    a USB storage device after suspend and resume.
STEPS:
    1. Click "Test" and insert a USB storage device (pen-drive/HDD).
       (Note: this test will time-out after 20 seconds.)
    2. Do not unplug the device after the test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of a CF card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the CF card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of a MS card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the MS card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of a MSP card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the MSP card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of a SDXC card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the SDXC card from the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of a xD card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the xD card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of an MMC card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the MMC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal
    of an SDHC card from the systems card reader after the system has been suspended.
STEPS:
    1. Click "Test" and remove the SDHC card from the reader.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal of
    a USB 3.0 storage device
STEPS:
    1. Click "Test" and remove the USB 3.0 device.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal of
    a USB 3.0 storage device after suspend
STEPS:
    1. Click "Test" and remove the USB 3.0 device.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal of
    a USB storage device
STEPS:
    1. Click "Test" and remove the USB device.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the system correctly detects the removal of
    a USB storage device after suspend.
STEPS:
    1. Click "Test" and remove the USB device.
       (Note: this test will time-out after 20 seconds.)
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a CF card after the system has been suspended
STEPS:
    1. Click "Test" and insert a CF card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a Compact Flash (CF) media card
STEPS:
    1. Click "Test" and insert a CF card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a Extreme Digital (xD) media card
STEPS:
    1. Click "Test" and insert a xD card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a MS card after the system has been suspended
STEPS:
    1. Click "Test" and insert a MS card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a MSP card after the system has been suspended
STEPS:
    1. Click "Test" and insert a MSP card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a Memory Stick (MS) media card
STEPS:
    1. Click "Test" and insert a MS card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a Memory Stick Pro (MSP) media card
STEPS:
    1. Click "Test" and insert a MSP card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a Multimedia Card (MMC) media
STEPS:
    1. Click "Test" and insert an MMC card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a SDXC card after the system has been suspended
STEPS:
    1. Click "Test" and insert a SDXC card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a Secure Digital Extended Capacity (SDXC) media card
STEPS:
    1. Click "Test" and insert a SDXC card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a UNLOCKED Secure Digital High-Capacity
    (SDHC) media card
STEPS:
    1. Click "Test" and insert an UNLOCKED SDHC card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of a xD card after the system has been suspended
STEPS:
    1. Click "Test" and insert a xD card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of an MMC card after the system has been suspended
STEPS:
    1. Click "Test" and insert an MMC card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of an UNLOCKED SD card after the system
    has been suspended
STEPS:
    1. Click "Test" and insert an UNLOCKED SD card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of an UNLOCKED SDHC media card after the
    system has been suspended
STEPS:
    1. Click "Test" and insert an UNLOCKED SDHC card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that the systems media card reader can
    detect the insertion of an UNLOCKED Secure Digital (SD) media card
STEPS:
    1. Click "Test" and insert an UNLOCKED SD card into the reader.
       If a file browser opens up, you can safely close it.
       (Note: this test will time-out after 20 seconds.)
    2. Do not remove the device after this test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check that you can record and hear audio using a bluetooth audio device
STEPS:
    1. Enable the bluetooth headset
    2. Click on the bluetooth icon in the menu bar
    3. Select 'Setup new device'
    4. Look for the device in the list and select it
    5. In the device write the PIN code automatically chosen by the wizard
    6. The device should pair with the computer
    7. Click "Test" to record for five seconds and reproduce in the bluetooth device
VERIFICATION:
    Did you hear the sound you recorded in the bluetooth PURPOSE:
    This test will check that you can transfer information through a bluetooth connection
STEPS:
    1. Make sure that you're able to browse the files in your mobile device
    2. Copy a file from the computer to the mobile device
    3. Copy a file from the mobile device to the computer
VERIFICATION:
    Were all files copied correctly? PURPOSE:
    This test will check that you can use a BlueTooth HID device
STEPS:
    1. Enable either a BT mouse or keyboard
    2. Click on the bluetooth icon in the menu bar
    3. Select 'Setup new device'
    4. Look for the device in the list and select it
    5. For mice, perform actions such as moving the pointer, right and left button clicks and double clicks
    6. For keyboards, click the Test button to lauch a small tool. Enter some text into the tool and close it.
VERIFICATION:
    Did the device work as expected? PURPOSE:
    This test will check that you can use a USB HID device
STEPS:
    1. Enable either a USB mouse or keyboard
    2. For mice, perform actions such as moving the pointer, right and left button clicks and double clicks
    3. For keyboards, click the Test button to lauch a small tool. Type some text and close the tool.
VERIFICATION:
    Did the device work as expected? PURPOSE:
    This test will check that your system detects USB storage devices.
STEPS:
    1. Plug in one or more USB keys or hard drives.
    2. Click on "Test".
INFO:
    $output
VERIFICATION:
    Were the drives detected? PURPOSE:
    This test will check the system can detect the insertion of a FireWire HDD
STEPS:
    1. Click 'Test' to begin the test. This test will
       timeout and fail if the insertion has not been detected within 20 seconds.
    2. Plug a FireWire HDD into an available FireWire port.
VERIFICATION:
    The verification of this test is automated. Do not change the automatically
    selected result PURPOSE:
    This test will check the system can detect the insertion of an eSATA HDD
STEPS:
    1. Click 'Test' to begin the test. This test will
       timeout and fail if the insertion has not been detected within 20 seconds.
    2. Plug an eSATA HDD into an available eSATA port.
VERIFICATION:
    The verification of this test is automated. Do not change the automatically
    selected result PURPOSE:
    This test will check the system can detect the removal of a FireWire HDD
STEPS:
    1. Click 'Test' to begin the test. This test will timeout and fail if
       the removal has not been detected within 20 seconds.
    2. Remove the previously attached FireWire HDD from the FireWire port.
VERIFICATION:
    The verification of this test is automated. Do not change the automatically
    selected result PURPOSE:
    This test will check the system can detect the removal of an eSATA HDD
STEPS:
    1. Click 'Test' to begin the test. This test will timeout and fail if
       the removal has not been detected within 20 seconds.
    2. Remove the previously attached eSATA HDD from the eSATA port.
VERIFICATION:
    The verification of this test is automated. Do not change the automatically
    selected result PURPOSE:
    This test will check to make sure your system can successfully hibernate (if supported)
STEPS:
    1. Click on Test
    2. The system will hibernate and should wake itself within 5 minutes
    3. If your system does not wake itself after 5 minutes, please press the power button to wake the system manually
    4. If the system fails to resume from hibernate, please restart System Testing and mark this test as Failed
VERIFICATION:
    Did the system successfully hibernate and did it work properly after waking up? PURPOSE:
    This test will check your CD audio playback capabilities
STEPS:
    1. Insert an audio CD in your optical drive
    2. When prompted, launch the Music Player
    3. Locate the CD in the display of the Music Player
    4. Select the CD in the Music Player
    5. Click the Play button to listen to the music on the CD
    6. Stop playing after some time
    7. Right click on the CD icon and select "Eject Disc"
    8. The CD should be ejected
    9. Close the Music Player
VERIFICATION:
    Did all the steps work? PURPOSE:
    This test will check your DVD  playback capabilities
STEPS:
    1. Insert a DVD that contains any movie in your optical drive
    2. Click "Test" to play the DVD in Totem
VERIFICATION:
    Did the file play? PURPOSE:
    This test will check your DVI port.
STEPS:
    Skip this test if your system does not have a DVI port.
    1. Connect a display (if not already connected) to the DVI port on your system
VERIFICATION:
    Was the desktop displayed correctly on both screens? PURPOSE:
    This test will check your DisplayPort port.
STEPS:
    Skip this test if your system does not have a DisplayPort port.
    1. Connect a display (if not already connected) to the DisplayPort port on your system
VERIFICATION:
    Was the desktop displayed correctly on both screens? PURPOSE:
    This test will check your HDMI port.
STEPS:
    Skip this test if your system does not have a HDMI port.
    1. Connect a display (if not already connected) to the HDMI port on your system
VERIFICATION:
    Was the desktop displayed correctly on both screens? PURPOSE:
    This test will check your RCA port.
STEPS:
    Skip this test if your system does not have a RCA port.
    1. Connect a display (if not already connected) to the RCA port on your system
VERIFICATION:
    Was the desktop displayed correctly on both screens? PURPOSE:
    This test will check your S-VIDEO port.
STEPS:
    Skip this test if your system does not have a S-VIDEO port.
    1. Connect a display (if not already connected) to the S-VIDEO port on your system
VERIFICATION:
    Was the desktop displayed correctly on both screens? PURPOSE:
    This test will check your USB 3.0 connection.
STEPS:
    1. Plug a USB 3.0 HDD or thumbdrive into a USB 3.0 port in the computer.
    2. An icon should appear on the Launcher.
    3. Click "Test" to begin the test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check your USB connection.
STEPS:
    1. Connect a USB storage device to an external USB slot on this computer.
    2. An icon should appear on the Launcher.
    3. Confirm that the icon appears.
    4. Eject the device.
    5. Repeat with each external USB slot.
VERIFICATION:
    Do all USB slots work with the device? PURPOSE:
    This test will check your USB connection.
STEPS:
    1. Plug a USB HDD or thumbdrive into the computer.
    2. An icon should appear on the Launcher.
    3. Click "Test" to begin the test.
VERIFICATION:
    The verification of this test is automated. Do not change the
    automatically selected result. PURPOSE:
    This test will check your VGA port.
STEPS:
    Skip this test if your system does not have a VGA port.
    1. Connect a display (if not already connected) to the VGA port on your system
VERIFICATION:
    Was the desktop displayed correctly on both screens? PURPOSE:
    This test will check your lid sensors
STEPS:
    1. Click "Test".
    2. Close and open the lid.
VERIFICATION:
    Did the screen turn off while the lid was closed? PURPOSE:
    This test will check your lid sensors.
STEPS:
    1. Click "Test".
    2. Close the lid.
    3. Wait 5 seconds with the lid closed.
    4. Open the lid.
VERIFICATION:
    Did the system resume when the lid was opened? PURPOSE:
    This test will check your lid sensors.
STEPS:
    1. Close your laptop lid.
VERIFICATION:
   Does closing your laptop lid cause your system to suspend? PURPOSE:
    This test will check your monitor power saving capabilities
STEPS:
    1. Click "Test" to try the power saving capabilities of your monitor
    2. Press any key or move the mouse to recover
VERIFICATION:
    Did the monitor go blank and turn on again? PURPOSE:
    This test will check your system shutdown/booting cycle
STEPS:
    1. Select 'Test' to initiate a system shutdown.
    2. Power the system back on.
    3. From the grub menu, boot into the Xen Hypervisor.
    4. When the system has restarted, log in and restart checkbox-certification-server.
    5. Select 'Re-Run' to return to this test.
    6. Select 'Yes' to indicate the test has passed if the machine shut down
    successfully otherwise, Select 'No' to indicate there was a problem.
VERIFICATION:
    Did the system shutdown and boot correctly? PURPOSE:
    This test will check your system shutdown/booting cycle.
STEPS:
    1. Shutdown your machine.
    2. Boot your machine.
    3. Repeat steps 1 and 2 at least 5 times.
VERIFICATION:
    Did the system shutdown and rebooted correctly? PURPOSE:
    This test will check your wired connection
STEPS:
    1. Click on the Network icon in the top panel
    2. Select a network below the "Wired network" section
    3. Click "Test" to verify that it's possible to establish a HTTP connection
VERIFICATION:
    Did a notification show and was the connection correctly established? PURPOSE:
    This test will check your wireless connection.
STEPS:
    1. Click on the Network icon in the panel.
    2. Select a network below the 'Wireless networks' section.
    3. Click "Test" to verify that it's possible to establish an HTTP connection.
VERIFICATION:
    Did a notification show and was the connection correctly established? PURPOSE:
    This test will cycle through the detected display modes
STEPS:
    1. Click "Test" and the display will cycle trough the display modes
VERIFICATION:
    Did your display look fine in the detected mode? PURPOSE:
    This test will ensure that the AC is plugged back in after the battery.
    tests
STEPS:
    1. Plug laptop into AC.
VERIFICATION:
    Was the laptop plugged into AC? PURPOSE:
    This test will ensure that the AC is unplugged for the battery drain tests to run.
STEPS:
    1. Unplug laptop from AC.
VERIFICATION:
    Was the laptop unplugged from AC? PURPOSE:
    This test will send the image 'JPEG_Color_Image_Ubuntu.jpg' to a specified device
STEPS:
    1. Click "Test" and you will be prompted to enter the Bluetooth device name of a device that can accept file transfers (It may take a few moments after entering the name for the file to begin sending)
    2. Accept any prompts that appear on both devices
VERIFICATION:
    Was the data correctly transferred? PURPOSE:
    This test will send the image 'JPEG_Color_Image_Ubuntu.jpg' to a specified device
STEPS:
    1. Make sure Bluetooth is enabled by checking the Bluetooth indicator applet
    2. Click "Test" and you will be prompted to enter the Bluetooth device name of a device that can accept file transfers (It may take a few moments after entering the name for the file to begin sending)
    3. Accept any prompts that appear on both devices
VERIFICATION:
    Was the data correctly transferred? PURPOSE:
    This test will test changes to screen brightness
STEPS:
    1. Click "Test" to try to dim the screen.
    2. Check if the screen was dimmed approximately to half of the maximum brightness.
    3. The screen will go back to the original brightness in 2 seconds.
VERIFICATION:
    Was your screen dimmed approximately to half of the maximum brightness? PURPOSE:
    This test will test display rotation
STEPS:
    1. Click "Test" to test display rotation. The display will be rotated every 4 seconds.
    2. Check if all rotations (normal right inverted left) took place without permanent screen corruption
VERIFICATION:
    Did the display rotation take place without without permanent screen corruption? PURPOSE:
    This test will test the battery information key
STEPS:
    Skip this test if you do not have a Battery Button.
    1. Click Test to begin
    2. Press the Battery Info button (or combo like Fn+F3)
    3: Close the Power Statistics tool if it opens
VERIFICATION:
    Did the Battery Info key work as expected? PURPOSE:
    This test will test the battery information key after resuming from suspend
STEPS:
    Skip this test if you do not have a Battery Button.
    1. Click Test to begin
    2. Press the Battery Info button (or combo like Fn+F3)
    3: Close the Power Statistics tool if it opens
VERIFICATION:
    Did the Battery Info key work as expected after resuming from suspend? PURPOSE:
    This test will test the brightness key
STEPS:
    1. Press the brightness buttons on the keyboard
VERIFICATION:
    Did the brightness change following to your key presses? PURPOSE:
    This test will test the brightness key after resuming from suspend
STEPS:
    1. Press the brightness buttons on the keyboard
VERIFICATION:
    Did the brightness change following to your key presses after resuming from suspend? PURPOSE:
    This test will test the default display
STEPS:
    1. Click "Test" to display a video test.
VERIFICATION:
    Do you see color bars and static? PURPOSE:
    This test will test the media keys of your keyboard
STEPS:
    Skip this test if your computer has no media keys.
    1. Click test to open a window on which to test the media keys.
    2. If all the keys work, the test will be marked as passed.
VERIFICATION:
    Do the keys work as expected? PURPOSE:
    This test will test the media keys of your keyboard after resuming from suspend
STEPS:
    Skip this test if your computer has no media keys.
    1. Click test to open a window on which to test the media keys.
    2. If all the keys work, the test will be marked as passed.
VERIFICATION:
    Do the keys work as expected after resuming from suspend? PURPOSE:
    This test will test the mute key of your keyboard
STEPS:
    1. Click test to open a window on which to test the mute key.
    2. If the key works, the test will pass and the window will close.
VERIFICATION:
    Does the mute key work as expected? PURPOSE:
    This test will test the mute key of your keyboard after resuming from suspend
STEPS:
    1. Click test to open a window on which to test the mute key.
    2. If the key works, the test will pass and the window will close.
VERIFICATION:
    Did the volume mute following your key presses? PURPOSE:
    This test will test the sleep key
STEPS:
    1. Press the sleep key on the keyboard
    2. Wake your system up by pressing the power button
VERIFICATION:
    Did the system go to sleep after pressing the sleep key? PURPOSE:
    This test will test the sleep key after resuming from suspend
STEPS:
    1. Press the sleep key on the keyboard
    2. Wake your system up by pressing the power button
VERIFICATION:
    Did the system go to sleep after pressing the sleep key after resuming from suspend? PURPOSE:
    This test will test the super key of your keyboard
STEPS:
    1. Click test to open a window on which to test the super key.
    2. If the key works, the test will pass and the window will close.
VERIFICATION:
    Does the super key work as expected? PURPOSE:
    This test will test the super key of your keyboard after resuming from suspend
STEPS:
    1. Click test to open a window on which to test the super key.
    2. If the key works, the test will pass and the window will close.
VERIFICATION:
    Does the super key work as expected after resuming from suspend? PURPOSE:
    This test will test the volume keys of your keyboard
STEPS:
    Skip this test if your computer has no volume keys.
    1. Click test to open a window on which to test the volume keys.
    2. If all the keys work, the test will be marked as passed.
VERIFICATION:
    Do the keys work as expected? PURPOSE:
    This test will test the volume keys of your keyboard after resuming from suspend
STEPS:
    Skip this test if your computer has no volume keys.
    1. Click test to open a window on which to test the volume keys.
    2. If all the keys work, the test will be marked as passed.
VERIFICATION:
    Did the volume change following to your key presses? PURPOSE:
    This test will test the wireless key
STEPS:
    1. Press the wireless key on the keyboard
    2. Check that the wifi LED turns off or changes color
    3. Check that wireless is disabled
    4. Press the same key again
    5. Check that the wifi LED turns on or changes color
    6. Check that wireless is enabled
VERIFICATION:
    Did the wireless turn off on the first press and on again on the second?
    (NOTE: the LED functionality will be reviewed in a following test. Please
    only consider the functionality of the wifi itself here.) PURPOSE:
    This test will test the wireless key after resuming from suspend
STEPS:
    1. Press the wireless key on the keyboard
    2. Press the same key again
VERIFICATION:
    Did the wireless go off on the first press and on again on the second after resuming from suspend? PURPOSE:
    This test will test your accelerometer to see if it is detected
    and operational as a joystick device.
STEPS:
    1. Click on Test
    2. Tilt your hardware in the directions onscreen until the axis threshold is met.
VERIFICATION:
    Is your accelerometer properly detected? Can you use the device? PURPOSE:
    This test will test your keyboard
STEPS:
    1. Click on Test
    2. On the open text area, use your keyboard to type something
VERIFICATION:
    Is your keyboard working properly? PURPOSE:
    This test will test your pointing device
STEPS:
    1. Move the cursor using the pointing device or touch the screen.
    2. Perform some single/double/right click operations.
VERIFICATION:
    Did the pointing device work as expected? PURPOSE:
    This test will verify that the GUI is usable after manually changing resolution
STEPS:
    1. Open the Displays application
    2. Select a new resolution from the dropdown list
    3. Click on Apply
    4. Select the original resolution from the dropdown list
    5. Click on Apply
VERIFICATION:
    Did the resolution change as expected? PURPOSE:
    This test will verify that your system can successfully reboot.
STEPS:
    1. Select 'Test' to initiate a system reboot.
    2. When the grub boot menu is displayed, boot into Ubuntu (Or allow the
    system to automatically boot on its own).
    3. Once the system has restarted, log in and restart checkbox-certification-server.
    4. Select 'Re-Run' to return to this test.
    5. Select 'Yes' to indicate the test has passed if the system rebooted
    successfully, otherwise, select 'No' to indicate there was a problem.
VERIFICATION:
    Did the system reboot correctly? PURPOSE:
    This test will verify the default display resolution
STEPS:
    1. This display is using the following resolution:
INFO:
    $output
VERIFICATION:
    Is this acceptable for your display? PURPOSE:
    This will verify that an ExpressCard slot can detect inserted devices.
STEPS:
    Skip this test if you do not have an ExpressCard slot.
    1. Plug an ExpressCard device into the ExpressCard slot
VERIFICATION:
    Was the device correctly detected? PURPOSE:
    To make sure that stressing the wifi hotkey does not cause applets to disappear from the panel or the system to lock up
STEPS:
    1. Log in to desktop
    2. Press wifi hotkey at a rate of 1 press per second and slowly increase the speed of the tap, until you are tapping as fast as possible
VERIFICATION:
    Verify the system is not frozen and the wifi and bluetooth applets are still visible and functional PURPOSE:
    Touchpad LED verification
STEPS:
    1. Click on the touchpad button or press key combination to enable/disable touchpad button
    2. Slide your finger on the touchpad
VERIFICATION:
    1. Touchpad LED status should toggle everytime the button is clicked or the key combination is pressed
    2. When the LED is on, the mouse pointer should move on touchpad usage
    3. When the LED is off, the mouse pointer should not move on touchpad usage PURPOSE:
    Touchpad LED verification after resuming from suspend
STEPS:
    1. Click on the touchpad button or press key combination to enable/disable touchpad button
    2. Slide your finger on the touchpad
VERIFICATION:
    1. Touchpad LED status should toggle everytime the button is clicked or the key combination is pressed
    2. When the LED is on, the mouse pointer should move on touchpad usage
    3. When the LED is off, the mouse pointer should not move on touchpad usage PURPOSE:
    Touchpad horizontal scroll verification
STEPS:
    1. Select "Test" when ready and place your cursor within the borders of the displayed test window.
    2. Verify that you can move the horizontal slider by moving your finger right and left in the lower part of the touchpad.
VERIFICATION:
    Could you scroll right and left? PURPOSE:
    Touchpad manual detection of multitouch.
STEPS:
    1. Look at the specifications for your system.
VERIFICATION:
    Is the touchpad supposed to be multitouch? PURPOSE:
    Touchpad user-verify
STEPS:
    1. Make sure that touchpad is enabled.
    2. Move cursor using the touchpad.
VERIFICATION:
    Did the cursor move? PURPOSE:
    Touchpad vertical scroll verification
STEPS:
    1. Select "Test" when ready and place your cursor within the borders of the displayed test window.
    2. Verify that you can move the vertical slider by moving your finger up and down in the right part of the touchpad.
VERIFICATION:
    Could you scroll up and down? PURPOSE:
    Touchscreen manual detection of multitouch.
STEPS:
    1. Look at the specifications for your system.
VERIFICATION:
    Is the screen supposed to be multitouch? PURPOSE:
    Validate Wireless (WLAN + Bluetooth) LED operated the same after resuming from suspend
STEPS:
    1. Make sure WLAN connection is established and Bluetooth is enabled.
    2. WLAN/Bluetooth LED should light
    3. Switch WLAN and Bluetooth off from a hardware switch (if present)
    4. Switch them back on
    5. Switch WLAN and Bluetooth off from the panel applet
    6. Switch them back on
VERIFICATION:
    Did the WLAN/Bluetooth LED light as expected after resuming from suspend? PURPOSE:
    Validate that WLAN LED shuts off when disabled
STEPS:
    1. During the keys/wireless test you should have observed the WLAN LED
    while performing that test after turning wireless off.
    2. WLAN LED should turn off or change color when wireless is turned off
VERIFICATION:
    Did the WLAN LED turn off or change color as expected? PURPOSE:
    Validate that WLAN LED shuts off when disabled after resuming from suspend
STEPS:
    1. Connect to AP
    2. Use Physical switch to disable WLAN
    3. Re-enable
    4. Use Network-Manager to disable WLAN
VERIFICATION:
    Did the LED turn off then WLAN is disabled after resuming from suspend? PURPOSE:
    Validate that the Bluetooth LED turns on and off when BT is enabled/disabled
STEPS:
    1. Switch bluetooth off from a hardware switch (if present)
    2. Switch bluetooth back on
    3. Switch bluetooth off from the panel applet
    4. Switch bluetooth back on
VERIFICATION:
    Did the bluetooth LED turn off and on twice? PURPOSE:
    Validate that the Bluetooth LED turns on and off when BT is enabled/disabled after resuming from suspend
STEPS:
    1. Switch bluetooth off from a hardware switch (if present)
    2. Switch bluetooth back on
    3. Switch bluetooth off from the panel applet
    4. Switch bluetooth back on
VERIFICATION:
    Did the bluetooth LED turn off and on twice after resuming from suspend? PURPOSE:
    Validate that the Caps Lock key operates the same before and after resuming from suspend
STEPS:
    1. Press "Block Cap Keys" to activate/deactivate cap keys blocking
    2. Cap Keys LED should be switched on/off every time the key is pressed
VERIFICATION:
    Did the Cap Keys LED light as expected after resuming from suspend? PURPOSE:
    Validate that the External Video hot key is working as expected
STEPS:
    1. Plug in an external monitor
    2. Press the display hot key to change the monitors configuration
VERIFICATION:
    Check that the video signal can be mirrored, extended, displayed on external or onboard only. PURPOSE:
    Validate that the External Video hot key is working as expected after resuming from suspend
STEPS:
    1. Plug in an external monitor
    2. Press the display hot key to change the monitors configuration
VERIFICATION:
    Check that the video signal can be mirrored, extended, displayed on external or onboard only, after resuming from suspend. PURPOSE:
    Validate that the HDD LED still operates as expected after resuming from suspend
STEPS:
    1. Select "Test" to write and read a temporary file for a few seconds
    2. HDD LED should blink when writing to/reading from HDD
VERIFICATION:
    Did the HDD LED still blink with HDD activity after resuming from suspend? PURPOSE:
    Validate that the battery LED indicated low power
STEPS:
    1. Let system run on battery for several hours
    2. Monitor battery LED carefully
VERIFICATION:
    Does the LED light orange when battery is low? PURPOSE:
    Validate that the battery LED indicated low power after resuming from suspend
STEPS:
    1. Let system run on battery for several hours
    2. Monitor battery LED carefully
VERIFICATION:
    Does the LED light orange when battery is low after resuming from suspend? PURPOSE:
    Validate that the battery LED properly displays charged status
STEPS:
    1. Let system run on battery for a short time
    2. Plug in AC
    3. Let system run on AC
VERIFICATION:
    Does the orange battery LED shut off when system is fully charged? PURPOSE:
    Validate that the battery LED properly displays charged status after resuming from suspend
STEPS:
    1. Let system run on battery for a short time
    2. Plug in AC
    3. Let system run on AC
VERIFICATION:
    Does the orange battery LED still shut off when system is fully charged after resuming from suspend? PURPOSE:
    Validate that the battery light shows charging status
STEPS:
    1. Let system run on battery for a while
    2. Plug in AC plug
VERIFICATION:
    Did the battery indicator LED turn orange? PURPOSE:
    Validate that the battery light shows charging status after resuming from suspend
STEPS:
    1. Let system run on battery for a while
    2. Plug in AC plug
VERIFICATION:
    Did the battery indicator LED still turn orange after resuming from suspend? PURPOSE:
    Validate that the camera LED still works as expected after resuming from suspend
STEPS:
    1. Select Test to activate camera
    2. Camera LED should light for a few seconds
VERIFICATION:
    Did the camera LED still turn on and off after resuming from suspend? PURPOSE:
    Validate that the numeric keypad LED operates the same before and after resuming from suspend
STEPS:
    1. Press "Block Num" key to toggle numeric keypad LED
    2. Click on the "Test" button to open a window to verify your typing
    3. Type using the numeric keypad both when the LED is on and off
VERIFICATION:
    1. Numeric keypad LED status should toggle everytime the "Block Num" key is pressed
    2. Numbers should only be entered in the keyboard verification window when the LED is on PURPOSE:
    Validate that the power LED operated the same after resuming from suspend
STEPS:
    1. Power LED should be on while device is switched on
VERIFICATION:
    Does the power LED remain on after resuming from suspend? PURPOSE:
    Verify touchpad hotkey toggles touchpad functionality on and off
STEPS:
    1. Verify the touchpad is functional
    2. Tap the touchpad toggle hotkey
    3. Tap the touchpad toggle hotkey again
VERIFICATION:
    Verify the touchpad has been disabled and re-enabled. PURPOSE:
    Verify touchpad hotkey toggles touchpad functionality on and off after resuming from suspend
STEPS:
    1. Verify the touchpad is functional
    2. Tap the touchpad toggle hotkey
    3. Tap the touchpad toggle hotkey again
VERIFICATION:
    Verify the touchpad has been disabled and re-enabled. PURPOSE:
    WLAN LED verification
STEPS:
    1. During the keys/wireless test you should have observed the
    wireless LED while turning wireless back on.
    2. WLAN LED should light or change color when wireless is turned on
VERIFICATION:
    Did the WLAN LED turn on or change color as expected? PURPOSE:
    WLAN LED verification after resuming from suspend
STEPS:
    1. Make sure WLAN connection is established
    2. WLAN LED should light
VERIFICATION:
    Did the WLAN LED light as expected after resuming from suspend? PURPOSE:
    Wake up by USB keyboard
STEPS:
    1. Enable "Wake by USB KB/Mouse" item in BIOS
    2. Press "Test" to enter suspend (S3) mode
    3. Press any key of USB keyboard to wake system up
VERIFICATION:
    Did the system wake up from suspend mode when you pressed a keyboard key? PURPOSE:
    Wake up by USB mouse
STEPS:
    1. Enable "Wake by USB KB/Mouse" item in BIOS
    2. Press "Test" to enter suspend (S3) mode
    3. Press any button of USB mouse to wake system up
VERIFICATION:
    Did the system wake up from suspend mode when you pressed the mouse button? PURPOSE:
    Wireless (WLAN + Bluetooth) LED verification
STEPS:
    1. Make sure WLAN connection is established and Bluetooth is enabled.
    2. WLAN/Bluetooth LED should light
    3. Switch WLAN and Bluetooth off from a hardware switch (if present)
    4. Switch them back on
    5. Switch WLAN and Bluetooth off from the panel applet
    6. Switch them back on
VERIFICATION:
    Did the WLAN/Bluetooth LED light as expected? PURPOSE:
   This is an automated stress test that will force the system to hibernate/resume for 250 cycles PURPOSE:
   This is an automated stress test that will force the system to hibernate/resume for 30 cycles PURPOSE:
   This is an automated stress test that will force the system to suspend/resume for 250 cycles. PURPOSE:
   This is an automated stress test that will force the system to suspend/resume for 30 cycles. PURPOSE:
   This test will verify that a USB DSL or Mobile Broadband modem works
STEPS:
   1. Connect the USB cable to the computer
   2. Right click on the Network icon in the panel
   3. Select 'Edit Connections'
   4. Select the 'DSL' (for ADSL modem) or 'Mobile Broadband' (for 3G modem) tab
   5. Click on 'Add' button
   6. Configure the connection parameters properly
   7. Notify OSD should confirm that the connection has been established
   8. Select Test to verify that it's possible to establish an HTTP connection
VERIFICATION:
   Was the connection correctly established? PURPOSE:
   This test will verify that a fingerprint reader can be used to unlock a locked system.
STEPS:
   1. Click on the Session indicator (Cog icon on the Left side of the panel) .
   2. Select 'Lock screen'.
   3. Press any key or move the mouse.
   4. A window should appear that provides the ability to unlock either typing your password or using fingerprint authentication.
   5. Use the fingerprint reader to unlock.
   6. Your screen should be unlocked.
VERIFICATION:
   Did the authentication procedure work correctly? PURPOSE:
   This test will verify that a fingerprint reader will work properly for logging into your system. This test case assumes that there's a testing account from which test cases are run and a personal account that the tester uses to verify the fingerprint reader
STEPS:
   1. Click on the User indicator on the left side of the panel (your user name).
   2. Select "Switch User Account"
   3. On the LightDM screen select your username.
   4. Use the fingerprint reader to login.
   5. Click on the user switcher applet.
   6. Select the testing account to continue running tests.
VERIFICATION:
   Did the authentication procedure work correctly? PURPOSE:
   This test will verify that a network printer is usable
STEPS:
   1. Make sure that a printer is available in your network
   2. Click on the Gear icon in the upper right corner and then click on Printers
   3. If the printer isn't already listed, click on Add
   4. The printer should be detected and proper configuration values  should be displayed
   5. Print a test page
VERIFICATION:
   Were you able to print a test page to the network printer? PURPOSE:
   This test will verify that the desktop clock displays the correct date and time
STEPS:
   1. Check the clock in the upper right corner of your desktop.
VERIFICATION:
   Is the clock displaying the correct date and time for your timezone? PURPOSE:
   This test will verify that the desktop clock synchronizes with the system clock.
STEPS:
   1. Click the "Test" button and verify the clock moves ahead by 1 hour.
   Note: It may take a minute or so for the clock to refresh
   2. Right click on the clock, then click on "Time & Date Settings..."
   3. Ensure that your clock application is set to manual.
   4. Change the time 1 hour back
   5. Close the window and reboot
VERIFICATION:
   Is your system clock displaying the correct date and time for your timezone? PURPOSE:
   This test will verify that you can reboot your system from the desktop menu
STEPS:
   1. Click the Gear icon in the upper right corner of the desktop and click on "Shut Down"
   2. Click the "Restart" button on the left side of the Shut Down dialog
   3. After logging back in, restart System Testing and it should resume here
VERIFICATION:
   Did your system restart and bring up the GUI login cleanly? PURPOSE:
   This test will verify your system's ability to play Ogg Vorbis audio files.
STEPS:
   1. Click Test to play an Ogg Vorbis file (.ogg)
   2. Please close the player to proceed.
VERIFICATION:
   Did the sample play correctly? PURPOSE:
   This test will verify your system's ability to play Wave Audio files.
STEPS:
   1. Select Test to play a Wave Audio format file (.wav)
   2. Please close the player to proceed.
VERIFICATION:
   Did the sample play correctly? PURPOSE:
 If Recovery is successful, you will see this test on restarting checkbox, not
 sniff4.
STEPS:
 1. Click Yes
VERIFICATION:
 None Necessary, this is a bogus test PURPOSE:
 Simulates a failure by rebooting the machine
STEPS:
 1. Click test to trigger a reboot
 2. Select "Continue" once logged back in and checkbox is restarted
VERIFICATION:
 You won't see the user-verify PURPOSE:
 Some systems do not share IPMI over all NICs but instead have a dedicated management port directly connected to the BMC.  This test verifies that you have used that port for remote IPMI connections and actions.
STEPS:
 1. Prior to running the test, you should have configured and used the Dedicated Management Port to remotely power off/on this sytem.
VERIFICATION:
 Skip this test if this system ONLY uses shared management/ethernet ports OR if this system does not have a BMC (Management Console)
 1. Select Yes if you successfully used IPMI to remotely power this system off and on using the dedicated management port.
 2. Select No if you attempted to use the dedicated management port to remotely power this system off/on and it failed for some reason. PURPOSE:
 This test checks that gcalctool (Calculator) works.
STEPS:
 Click the "Test" button to open the calculator and perform:
  1. Cut
  2. Copy
  3. Paste
VERIFICATION:
 Did the functions perform as expected? PURPOSE:
 This test checks that gcalctool (Calculator) works.
STEPS:
 Click the "Test" button to open the calculator and perform:
  1. Memory set
  2. Memory reset
  3. Memory last clear
  4. Memory clear
VERIFICATION:
 Did the functions perform as expected? PURPOSE:
 This test checks that gcalctool (Calculator) works.
STEPS:
 Click the "Test" button to open the calculator and perform:
 1. Simple math functions (+,-,/,*)
 2. Nested math functions ((,))
 3. Fractional math
 4. Decimal math
VERIFICATION:
 Did the functions perform as expected? PURPOSE:
 This test checks that gcalctool (Calculator) works.
STEPS:
 Click the "Test" button to open the calculator.
VERIFICATION:
 Did it launch correctly? PURPOSE:
 This test checks that gedit works.
STEPS:
 1. Click the "Test" button to open gedit, and re-open the file you created previously.
 2. Edit then save the file, then close gedit.
VERIFICATION:
 Did this perform as expected? PURPOSE:
 This test checks that gedit works.
STEPS:
 1. Click the "Test" button to open gedit.
 2. Enter some text and save the file (make a note of the file name you use), then close gedit.
VERIFICATION:
 Did this perform as expected? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the AOL Instant Messaging (AIM) service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the Facebook Chat service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the Google Talk (gtalk) service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the Jabber service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Empathy messaging client works.
STEPS:
 1. Select Test to launch Empathy.
 2. Configure it to connect to the Microsoft Network (MSN) service.
 3. Once you have completed the test, please quit Empathy to continue here.
VERIFICATION:
 Were you able to connect correctly and send/receive messages? PURPOSE:
 This test will check that Evolution works.
STEPS:
 1. Click the "Test" button to launch Evolution.
 2. Configure it to connect to a IMAP account.
VERIFICATION:
 Were you able to receive and read e-mail correctly? PURPOSE:
 This test will check that Evolution works.
STEPS:
 1. Click the "Test" button to launch Evolution.
 2. Configure it to connect to a POP3 account.
VERIFICATION:
 Were you able to receive and read e-mail correctly? PURPOSE:
 This test will check that Evolution works.
STEPS:
 1. Click the "Test" button to launch Evolution.
 2. Configure it to connect to a SMTP account.
VERIFICATION:
 Were you able to send e-mail without errors? PURPOSE:
 This test will check that Firefox can play a Flash video. Note: this may
 require installing additional software to successfully complete.
STEPS:
 1. Select Test to launch Firefox and view a short flash video.
VERIFICATION:
 Did the video play correctly? PURPOSE:
 This test will check that Firefox can play a Quicktime (.mov) video file.
 Note: this may require installing additional software to successfully
 complete.
STEPS:
 1. Select Test to launch Firefox with a sample video.
VERIFICATION:
 Did the video play using a plugin? PURPOSE:
 This test will check that Firefox can render a basic web page.
STEPS:
 1. Select Test to launch Firefox and view the test web page.
VERIFICATION:
 Did the Ubuntu Test page load correctly? PURPOSE:
 This test will check that Firefox can run a java applet in a web page. Note:
 this may require installing additional software to complete successfully.
STEPS:
 1. Select Test to open Firefox with the Java test page, and follow the instructions there.
VERIFICATION:
 Did the applet display? PURPOSE:
 This test will check that Firefox can run flash applications. Note: this may
 require installing additional software to successfully complete.
STEPS:
 1. Select Test to launch Firefox and view a sample Flash test.
VERIFICATION:
 Did you see the text? PURPOSE:
 This test will check that Gnome Terminal works.
STEPS:
 1. Click the "Test" button to open Terminal.
 2. Type 'ls' and press enter. You should see a list of files and folder in your home directory.
 3. Close the terminal window.
VERIFICATION:
 Did this perform as expected? PURPOSE:
 This test will check that the file browser can copy a file.
STEPS:
 1. Click Test to open the File Browser.
 2. Right click on the file called Test File 1 and click Copy.
 3. Right click in the white space and click Paste.
 4. Right click on the file called Test File 1(copy) and click Rename.
 5. Enter the name Test File 2 in the name box and hit Enter.
 6. Close the File Browser.
VERIFICATION:
 Do you now have a file called Test File 2? PURPOSE:
 This test will check that the file browser can copy a folder
STEPS:
 1. Click Test to open the File Browser.
 2. Right click on the folder called Test Folder and click on Copy.
 3. Right Click on any white area in the window and click on Paste.
 4. Right click on the folder called Test Folder(copy) and click Rename.
 5. Enter the name Test Data in the name box and hit Enter.
 6. Close the File browser.
VERIFICATION:
 Do you now have a folder called Test Data? PURPOSE:
 This test will check that the file browser can create a new file.
STEPS:
 1. Click Select Test to open the File Browser.
 2. Right click in the white space and click Create Document -> Empty Document.
 3. Enter the name Test File 1 in the name box and hit Enter.
 4. Close the File browser.
VERIFICATION:
 Do you now have a file called Test File 1? PURPOSE:
 This test will check that the file browser can create a new folder.
STEPS:
 1. Click Test to open the File Browser.
 2. On the menu bar, click File -> Create Folder.
 3. In the name box for the new folder, enter the name Test Folder and hit Enter.
 4. Close the File browser.
VERIFICATION:
 Do you now have a new folder called Test Folder? PURPOSE:
 This test will check that the file browser can delete a file.
STEPS:
 1. Click Test to open the File Browser.
 2. Right click on the file called Test File 1 and click on Move To Trash.
 3. Verify that Test File 1 has been removed.
 4. Close the File Browser.
VERIFICATION:
  Is Test File 1 now gone? PURPOSE:
 This test will check that the file browser can delete a folder.
STEPS:
 1. Click Test to open the File Browser.
 2. Right click on the folder called Test Folder and click on Move To Trash.
 3. Verify that the folder was deleted.
 4. Close the file browser.
VERIFICATION:
 Has Test Folder been successfully deleted? PURPOSE:
 This test will check that the file browser can move a file.
STEPS:
 1. Click Test to open the File Browser.
 2. Click and drag the file called Test File 2 onto the icon for the folder called Test Data.
 3. Release the button.
 4. Double click the icon for Test Data to open that folder up.
 5. Close the File Browser.
VERIFICATION:
 Was the file Test File 2 successfully moved into the Test Data folder? PURPOSE:
 This test will check that the update manager can find updates.
STEPS:
 1. Click Test to launch update-manager.
 2. Follow the prompts and if updates are found, install them.
 3. When Update Manager has finished, please close the app by clicking the Close button in the lower right corner.
VERIFICATION:
 Did Update manager find and install updates (Pass if no updates are found,
 but Fail if updates are found but not installed) PURPOSE:
 This test will check the system's ability to power-off and boot.
STEPS:
 1. Select "Test" to begin.
 2. The machine will shut down.
 3. Power the machine back on.
 4. After rebooting, wait for the test prompts to inform you that the test is complete.
 5. Once the test has completed, restart checkbox and select 'Re-run' when prompted.
VERIFICATION:
 If the machine successfully shuts down and boots, select 'Yes', otherwise,
 select 'No'. PURPOSE:
 This test will check the system's ability to reboot cleanly.
STEPS:
 1. Select "Test" to begin.
 2. The machine will reboot.
 3. After rebooting, wait for the test prompts to inform you that the test is complete.
 4. Once the test has completed, restart checkbox and select Re-Run when prompted.
VERIFICATION:
 If the machine successfully reboots, select Yes then select Next. PURPOSE:
 This test will verify that the file browser can move a folder.
STEPS:
 1. Click Test to open the File Browser.
 2. Click and drag the folder called Test Data onto the icon called Test Folder.
 3. Release the button.
 4. Double click the folder called Test Folder to open it up.
 5. Close the File Browser.
VERIFICATION:
 Was the folder called Test Data successfully moved into the folder called Test Folder? PURPOSE:
 To sniff things out
STEPS:
 1. Click Yes
VERIFICATION:
 None Necessary, this is a bogus test Panel Clock Verification tests Panel Reboot Verification tests Parses Xorg.0.Log and discovers the running X driver and version Peripheral tests Piglit tests Ping ubuntu.com and restart network interfaces 100 times Play back a sound on the default output and listen for it on the  default input. Please choose (%s):  Please press each key on your keyboard. Please type here and press Ctrl-D when finished:
 Pointing device tests. Power Management Test Power Management tests Press any key to continue... Previous Print version information and exit. Provides information about displays attached to the system Provides information about network devices Quit from keyboard Record mixer settings before suspending. Record the current network before suspending. Record the current resolution before suspending. Rendercheck tests Rerun Restart Returns the name, driver name and driver version of any touchpad discovered on the system. Run Cachebench Read / Modify / Write benchmark Run Cachebench Read benchmark Run Cachebench Write benchmark Run Compress 7ZIP benchmark Run Compress PBZIP2 benchmark Run Encode MP3 benchmark Run Firmware Test Suite (fwts) automated tests. Run GLmark2 benchmark Run GLmark2-ES2 benchmark Run GiMark, a geometry instancing test (OpenGL 3.3) Fullscreen 1920x1080 no antialiasing Run GiMark, a geometry instancing test (OpenGL 3.3) Windowed 1024x640 no antialiasing Run GnuPG benchmark Run Himeno benchmark Run Lightsmark benchmark Run N-Queens benchmark Run Network Loopback benchmark Run Qgears2 OpenGL gearsfancy benchmark Run Qgears2 OpenGL image scaling benchmark Run Qgears2 XRender Extension gearsfancy benchmark Run Qgears2 XRender Extension image scaling benchmark Run Render-Bench XRender/Imlib2 benchmark Run Stream Add benchmark Run Stream Copy benchmark Run Stream Scale benchmark Run Stream Triad benchmark Run Unigine Heaven benchmark Run Unigine Santuary benchmark Run Unigine Tropics benchmark Run a stress test based on FurMark (OpenGL 2.1 or 3.2) Fullscreen 1920x1080 no antialiasing Run a stress test based on FurMark (OpenGL 2.1 or 3.2) Windowed 1024x640 no antialiasing Run a tessellation test based on TessMark (OpenGL 4.0) Fullscreen 1920x1080 no antialiasing Run a tessellation test based on TessMark (OpenGL 4.0) Windowed 1024x640 no antialiasing Run globs benchmark Run gtkperf to make sure that GTK based test cases work Run the graphics stress test. This test can take a few minutes. Run x264 H.264/AVC encoder benchmark Running %s... Runs a test that transfers 100 10MB files 3 times to a SDHC card. Runs a test that transfers 100 10MB files 3 times to usb. Runs all of the rendercheck test suites. This test can take a few minutes. Runs piglit tests for checking OpenGL 2.1 support Runs piglit tests for checking support for GLSL fragment shader operations Runs piglit tests for checking support for GLSL vertex shader operations Runs piglit tests for checking support for framebuffer object operations, depth buffer and stencil buffer Runs piglit tests for checking support for texture from pixmap Runs piglit tests for checking support for vertex buffer object operations Runs piglit_tests for checking support for stencil buffer operations Runs the piglit results summarizing tool SATA/IDE device information. SMART test SYSTEM TESTING: Please enter your password. Some tests require root access to run properly. Your password will never be stored and will never be submitted with test results. Select All Select all Server Services checks Shorthand for --config=.*/jobs_info/blacklist. Shorthand for --config=.*/jobs_info/blacklist_file. Shorthand for --config=.*/jobs_info/whitelist. Shorthand for --config=.*/jobs_info/whitelist_file. Skip Smoke tests Sniff Sniffers Software Installation tests Some new hard drives include a feature that parks the drive heads after a short period of inactivity. This is a power-saving feature, but it can have a bad interaction with the operating system that results in the drive constantly parked then activated. This produces excess wear on the drive, potentially leading to early failures. Space when finished Start testing Status Stop process Stop typed at tty Stress poweroff system (100 cycles) Stress reboot system (100 cycles) Stress tests Submission details Submit results Submit to HEXR Successfully finished testing! Suspend Test Suspend tests System 
Testing System Daemon tests System Testing Tab 1 Tab 2 Termination signal Test Test ACPI Wakealarm (fwts wakealarm) Test Again Test and exercise memory. Test cancelled Test for clock jitter. Test if the atd daemon is running when the package is installed. Test if the cron daemon is running when the package is installed. Test if the cupsd daemon is running when the package is installed. Test if the getty daemon is running when the package is installed. Test if the init daemon is running when the package is installed. Test if the klogd daemon is running when the package is installed. Test if the nmbd daemon is running when the package is installed. Test if the smbd daemon is running when the package is installed. Test if the syslogd daemon is running when the package is installed. Test if the udevd daemon is running when the package is installed. Test if the winbindd daemon is running when the package is installed. Test interrupted Test offlining CPUs in a multicore system. Test that the /var/crash directory doesn't contain anything. Lists the files contained within if it does, or echoes the status of the directory (doesn't exist/is empty) Test that the X is not running in failsafe mode. Test that the X process is running. Test the CPU scaling capabilities using Firmware Test Suite (fwts cpufreq). Test the network after resuming. Test to check that a Xen domU image can boot and run on Xen on Ubuntu Test to check that a cloud image boots and works properly with KVM Test to check that virtualization is supported and the test system has at least a minimal amount of RAM to function as an OpenStack Compute Node Test to detect audio devices Test to detect the available network controllers Test to detect the optical drives Test to determine if this system is capable of running hardware accelerated KVM virtual machines Test to output the Xorg version Test to see if we can sync local clock to an NTP server Test to see that we have the same resolution after resuming as before. Test to verify that the Xen Hypervisor is running. Test your system and submit results to Launchpad Tested Tests oem-config using Xpresser, and then checks that the user has been created successfully. Cleans up the newly created user after the test has passed. Tests that the systems wireless hardware can connect to a router using WPA security and the 802.11b/g protocols after the system has been suspended. Tests that the systems wireless hardware can connect to a router using WPA security and the 802.11b/g protocols. Tests that the systems wireless hardware can connect to a router using WPA security and the 802.11n protocol after the system has been suspended. Tests that the systems wireless hardware can connect to a router using WPA security and the 802.11n protocol. Tests that the systems wireless hardware can connect to a router using no security and the 802.11b/g protocols after the system has been suspended. Tests that the systems wireless hardware can connect to a router using no security and the 802.11b/g protocols. Tests that the systems wireless hardware can connect to a router using no security and the 802.11n protocol after the system has been suspended. Tests that the systems wireless hardware can connect to a router using no security and the 802.11n protocol. Tests the performance of a systems wireless connection through the iperf tool, using UDP packets. Tests the performance of a systems wireless connection through the iperf tool. Tests to see that apt can access repositories and get updates (does not install updates). This is done to confirm that you could recover from an incomplete or broken update. Tests whether the system has a working Internet connection. TextLabel The file to write the log to. The following report has been generated for submission to the Launchpad hardware database:

  [[%s|View Report]]

You can submit this information about your system by providing the email address you use to sign in to Launchpad. If you do not have a Launchpad account, please register here:

  https://launchpad.net/+login The generated report seems to have validation errors,
so it might not be processed by Launchpad. There is another checkbox running. Please close it first. This Automated test attempts to detect a camera. This attaches screenshots from the suspend/cycle_resolutions_after_suspend_auto test to the results submission. This is a fully automated version of mediacard/sd-automated and assumes that the system under test has a memory card device plugged in prior to checkbox execution. It is intended for SRU automated testing. This is an automated Bluetooth file transfer test. It sends an image to the device specified by the BTDEVADDR environment variable. This is an automated Bluetooth test. It emulates browsing on a remote device specified by the BTDEVADDR environment variable. This is an automated Bluetooth test. It receives the given file from a remote host specified by the BTDEVADDR environment variable This is an automated test to gather some info on the current state of your network devices. If no devices are found, the test will exit with an error. This is an automated test which performs read/write operations on an attached FireWire HDD This is an automated test which performs read/write operations on an attached eSATA HDD This is an automated version of usb/storage-automated and assumes that the server has usb storage devices plugged in prior to checkbox execution. It is intended for servers and SRU automated testing. This is an automated version of usb3/storage-automated and assumes that the server has usb 3.0 storage devices plugged in prior to checkbox execution. It is intended for servers and SRU automated testing. This is the automated version of suspend/suspend_advanced. This test checks cpu topology for accuracy This test checks that CPU frequency governors are obeyed when set. This test checks that the wireless interface is working after suspending the system. It disconnects all interfaces and then connects to the wireless interface and checks that the connection is working as expected. This test checks the amount of memory which is reporting in meminfo against the size of the memory modules detected by DMI. This test disconnects all connections and then connects to the wireless interface. It then checks the connection to confirm it's working as expected. This test grabs the hardware address of the bluetooth adapter after suspend and compares it to the address grabbed before suspend. This test is automated and executes after the mediacard/cf-insert test is run. It tests reading and writing to the CF card. This test is automated and executes after the mediacard/cf-insert-after-suspend test is run. It tests reading and writing to the CF card after the system has been suspended. This test is automated and executes after the mediacard/mmc-insert test is run. It tests reading and writing to the MMC card. This test is automated and executes after the mediacard/mmc-insert-after-suspend test is run. It tests reading and writing to the MMC card after the system has been suspended. This test is automated and executes after the mediacard/ms-insert test is run. It tests reading and writing to the MS card. This test is automated and executes after the mediacard/ms-insert-after-suspend test is run. It tests reading and writing to the MS card after the system has been suspended. This test is automated and executes after the mediacard/msp-insert test is run. It tests reading and writing to the MSP card. This test is automated and executes after the mediacard/msp-insert-after-suspend test is run. It tests reading and writing to the MSP card after the system has been suspended. This test is automated and executes after the mediacard/sd-insert test is run. It tests reading and writing to the SD card. This test is automated and executes after the mediacard/sd-insert-after-suspend test is run. It tests reading and writing to the SD card after the system has been suspended. This test is automated and executes after the mediacard/sdhc-insert test is run. It tests reading and writing to the SDHC card. This test is automated and executes after the mediacard/sdhc-insert-after-suspend test is run. It tests reading and writing to the SDHC card after the system has been suspended. This test is automated and executes after the mediacard/sdxc-insert test is run. It tests reading and writing to the SDXC card. This test is automated and executes after the mediacard/sdxc-insert-after-suspend test is run. It tests reading and writing to the SDXC card after the system has been suspended. This test is automated and executes after the mediacard/xd-insert test is run. It tests reading and writing to the xD card. This test is automated and executes after the mediacard/xd-insert-after-suspend test is run. It tests reading and writing to the xD card after the system has been suspended. This test is automated and executes after the suspend/usb3_insert_after_suspend test is run. This test is automated and executes after the suspend/usb_insert_after_suspend test is run. This test is automated and executes after the usb/insert test is run. This test is automated and executes after the usb3/insert test is run. This test will check to make sure supported video modes work after a suspend and resume. This is done automatically by taking screenshots and uploading them as an attachment. This test will verify that the volume levels are at an acceptable level on your local system.  The test will validate that the volume is greater than or equal to minvol and less than or equal to maxvol for all sources (inputs) and sinks (outputs) recognized by PulseAudio.  It will also validate that the active source and sink are not muted.  You should not manually adjust the  volume or mute before running this test. This will attach any logs from the power-management/poweroff test to the results. This will attach any logs from the power-management/reboot test to the results. This will check to make sure that your audio device works properly after a suspend and resume.  This may work fine with speakers and onboard microphone, however, it works best if used with a cable connecting the audio-out jack to the audio-in jack. This will run some basic connectivity tests against a BMC, verifying that IPMI works. Timer signal from alarm(2) To fix this, close checkbox and add the missing dependencies to the whitelist. Touchpad tests Touchscreen tests Try to enable a remote printer on the network and print a test page. Type Text UNKNOWN USB Test USB tests Unknown signal Untested Usage: checkbox [OPTIONS] User Applications User-defined signal 1 User-defined signal 2 Validate that the Vector Floating Point Unit is running on ARM device Verifies that DNS server is running and working. Verifies that Print/CUPs server is running. Verifies that Samba server is running. Verifies that Tomcat server is running and working. Verifies that sshd is running. Verifies that the LAMP stack is running (Apache, MySQL and PHP). Verify USB3 external storage performs at or above baseline performance Verify system storage performs at or above baseline performance Verify that all CPUs are online after resuming. Verify that all memory is available after resuming from suspend. Verify that all the CPUs are online before suspending Verify that an installation of checkbox-server on the network can be reached over SSH. Verify that mixer settings after suspend are the same as before suspend. Verify that storage devices, such as Fibre Channel and RAID can be detected and perform under stress. View results Virtualization tests Welcome to System Testing!

Checkbox provides tests to confirm that your system is working properly. Once you are finished running the tests, you can view a summary report for your system. Whitelist not topologically ordered Wireless Test Wireless networking tests Wireless scanning test. It scans and reports on discovered APs. Working You can also close me by pressing ESC or Ctrl+C. _Deselect All _Exit _Finish _No _Previous _Select All _Skip this test _Test _Test Again _Yes and capacity as well. attaches the contents of various sysctl config files. eSATA disk tests empty and capacity as well. https://help.ubuntu.com/community/Installation/SystemRequirements installs the installer bootchart tarball if it exists. no skip test test again tty input for background process tty output for background process until empty and capacity as well.  Requires MOVIE_VAR to be set. yes Project-Id-Version: checkbox
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-01-27 17:46+0000
PO-Revision-Date: 2016-04-15 17:18+0000
Last-Translator: YunQiang Su <wzssyqa@gmail.com>
Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:35+0000
X-Generator: Launchpad (build 18115)
Language: zh_CN
 

警告：一些测试可能导致您的系统失去响应或响应缓慢。开始此测试过程前，请保存您的所有工作，并关闭其他所有正在运行的应用程序。    确定是否需要运行特定于便携式计算机的测试，这些测试不能用于台式机。  结果   运行   选择   基于摄像头支持的分辨率拍摄多张图形并
 验证他们的大小和格式有效。  测试系统的无线硬件是否能够连接到使用
802.11a 协议的路由器。此测试要求您有一个预先配置只
回应 802.11a 协议发出的请求的路由器。  测试系统的无线硬件是否能够连接到使用
802.11b 协议的路由器。此测试要求您有一个预先配置只
回应 802.11b 协议发出的请求的路由器。  测试系统的无线硬件是否能够连接到使用
802.11g 协议的路由器。此测试要求您有一个预先配置只
回应 802.11g 协议发出的请求的路由器。 %(key_name)s 按键已按 否(&N) 上一个(&P) 跳过此测试(&S) 测试(&T) 是(&Y) 您已完成30项测试中的10项 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">
<html><head><meta name="qrichtext" content="1" /><style type="text/css">
p, li { white-space: pre-wrap; }
</style></head><body style=" font-family:'Ubuntu'; font-size:10pt; font-weight:400; font-style:normal;">
<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=;">请输入与您的 Launchpad 账户关联的电子邮箱地址 (如适用)</span></p>
<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=;">并点击 “提交结果” 按钮提交测试结果至 Launchpad。</span></p></body></html> 来自 abort(3) 的取消信号 所有要求的按键已被测试！ 将 piglit-summary 目录归档至 piglit-results.tar.gz。 您确定吗？ 附加 fwts wakealarm 测试的日志 附加 rendercheck 测试中的日志 将 /var/log/dmesg 的副本附加到测试结果中 附加表示系统硬件信息的 udev 数据库的转储。 附加当前正在运行的内核模块的列表。 附加有关 CPU 信息的报告 附加已安装的英特尔 HDA 编解码器的报告 附加 sysfs 属性的报告。 附加 gcov 数据(如果存在)的归档。 附加 dmidecode 输出 附加有关 DMI 的信息 提供关于磁盘分区的信息 附加 lshw 输出 附加 FWTS 结果日志至结果提交 将音频硬件数据收集的日志附加到结果中。 附加针对 bootchart 测试运行情况的 bootchart log。 附加针对 bootchart 运行情况的 bootchart png 文件 附加 /proc/acpi/sleep 的内容(如果存在)。 附加 /etc/modules 文件的内容。 附加各个 modprobe 配置文件的内容。 附加固件版本 附加图像压力测试结果至结果提交。 附加安装程序调试日志(如果存在)。 附加 “250 次休眠/唤醒循环” 测试的日志(如果存在) 附加 “250 次挂起/唤醒循环” 测试的日志(如果存在) 附加 “30 次休眠/唤醒循环” 测试的日志(如果存在) 附加 “30 次挂起/唤醒循环” 测试的日志(如果存在) 附加单次挂起或唤醒测试的日志至测试结果 将 CPU 变频测试的日志附加到结果中 附加 udev_resource 输出，以供调试 附加捕获的屏幕截图。 附加捕获的全屏视频截图。 附加极为冗长的 lspci 输出(包含中央数据库查询)。 附加极为冗长的 lspci 输出。 音频测试 音频测试 自动 CD 写入测试。 自动 DVD 写入测试。 针对 fwts 发现的错误，自动检查30次休眠循环日志 针对 fwts 发现的错误，自动检查休眠日志 针对 fwts 发现的错误，自动检查挂起日志 为每个网卡生成 PXE 确认测试的自动作业。 为每个网卡生成远程共享 IPMI 确认测试的自动作业。 自动光驱读取测试。 确保可通过 HTTP 下载文件的自动测试用例 使用 ICMP ECHO 数据包验证某个系统在网络上的可用性的自动测试用例。 将蓝牙设备信息保存在 checkbox 报告中的自动测试 自动使用多个网卡以及按顺序测试每个网卡。 每个磁盘的基准测试 基准测试 蓝牙测试 蓝牙测试 Bootchart 信息。 管道中断：写向管道时没有读入者 正在创建报告... CD 写入测试。 CPU 测试 中央处理器测试 空闲系统时的 CPU 利用率。 摄像头测试 摄像头测试 检查 shell 测试用例中的失败结果 依赖满足时，检查作业被执行 要求被满足时，检查作业被执行 要求未被满足时，检查作业结果被设置为 “在此系统未被满足” 依赖未被满足时，检查作业结果被设置为不启动 检查压力关机(100次循环)测试用例的日志 检查压力重启(100次循环)测试用例的日志 检查每个磁盘的状态变化 检查 shell 测试用例中的成功结果 检查是否未使用 VESA 驱动程序 检查硬件是否能够运行 Unity 3D 检查硬件是否能够运行 compiz 检查需要重新连接到一个 WIFI 接入点的时间。 查看内核中是否设置了 CONFIG_NO_HZ(这只是一个简单回归检查) Checkbox 系统测试 Checkbox 未完全结束。
您确定重新运行上一个测试，
或继续下一个测试，还是
从头重新开始？ 检查指定的源列表文件是否包括要求的软件仓库 检查系统空闲时电池耗竭的情况。电源为空时，报告时间 检查系统挂起时电池耗竭的情况。电源为空时， 检查观看电影时电池耗竭的情况。电源为空时， 检查系统从挂起中恢复后，重新连接现有的 wifi 所花费的时间。 检查系统从挂起中恢复后，重新连接现有的有线连接所花费的时间。 检查机器从挂起中唤醒所花费的睡眠时候在给定的阈值之内 子进程停止或被终止 选择将要运行在您系统上的测试： 编解码器测试 全部折叠 收集音频相关的系统信息。这些数据可用来模拟此计算机的音频系统并在一个受控环境下执行更细致的测试。 收集颜色深度和像素格式信息。 收集刷新率信息。 收集图形储存器信息。 收集图形模式(屏幕分辨率和刷新率)信息 使用以上字符连接来展开节点 未找到命令。 命令接收的信号 %(signal_name)s：%(signal_description)s 注释 常用文档类型测试 组件 配置覆盖参数。 继续 若已停止就继续 为基于 CDMA 的调制解调器创建一个移动宽带连接并检查连接确认正在运行。 为基于 GSM 的调制解调器创建一个移动宽带连接并检查连接确认正在运行。  DVD 写入测试。 依赖缺失，某些作业将不会运行。 取消全部选中 取消全部选中 详细信息... 检测并显示连接到该系统的磁盘。 检测并显示连接到该系统的 USB 设备。 确定屏幕是否被自动检测为多点触控设备。 确定屏幕是否被自动检测为非触摸设备。 确定触控板是否被自动检测为多点触控设备。 确定触控板是否被自动检测为单点触摸设备。 硬盘测试 磁盘测试 空闲系统时的磁盘利用率。 确定要跳过这个测试吗？ 不再询问 完成 将内存信息转储到一个文件中，以便运行了挂起测试后进行比较 电子邮件 电子邮件地址必须有正确的格式。 电子邮件地址： 确保当前分辨率达到或超过建议的最小分辨率(800x600)。有关详细信息，请参见这里： 输入文本：
 错误 和服务器交换信息... 正在执行 %(test_name)s 全部展开 ExpressCard 测试 ExpressCard 测试 失败： 数据包丢失率为 {}% 超过阈值 {}% 联结服务器失败。请再试试
或上载下列文件名：
%s

直接到系统数据库：
https://launchpad.net/+hwdb/+submit 无法打开文件 “%s”：%s 处理表单失败：%s 上传到服务器失败，
请稍后重新尝试。 指纹阅读器测试 火线接口测试 火线(Firewire)磁盘测试 浮点异常 软盘测试 软盘测试 表单 更多信息： 正在从您的系统上收集信息... 显卡测试 显卡测试 在控制终端或控制进程死掉时检测到了挂起 休眠测试 热键测试 当所有的按键已被按下，测试将自动退出。 如果按键不存在于您的键盘，请按下方的 “跳过” 按钮将其从测试移除。 如果您的键盘缺失一个或多个按键，请按下它的数字跳过其测试。 非法指令 测试中 信息 信息测试 不会发布到 Launchpad 上的信息。 信息性测试 输入设备测试 输入测试 互联网连接完整建立 从键盘中断 无效内存调用 作业将会重新安排以修复受损的依赖。 按键测试 按键测试 杀死信号 指示灯测试 列出 USB 设备 列出所有音频设备的设备驱动和版本 确认实时时钟(RTC)设备存在 默认安装测试过程中使用的最大磁盘空间 媒体卡测试 媒体卡测试 内存测试 内存测试 其它测试 杂项测试 丢失配置文件作为参数。
 移动宽带测试 显示器测试 显示器测试 在屏幕上移动一个 3D 窗口 下一个(&T) 下一步(_X) 网络信息 网络测试 联网测试 下一步 没有互联网连接 未能解决 未能开始 不支持 尚未测试 不需要 调试，信息，警告，错误或者致命错误之一。 多次打开和关闭 4 个 3D 窗口 多次打开和关闭一个 3D 窗口 多次打开、挂起唤醒以及关闭 3D 窗口 光驱测试 光驱测试 光驱读取测试。 通过： 数据包丢失率为 {}% 在阈值 {}% 之内 密码：  目的：
    检查外部输出连接是否正常工作
步骤：
    1. 将（带内置放大器的）扬声器电缆插入输出接口
    2. 打开系统声音首选项，选中 “输出” 标签页，在连接器列表中选择 “线路输出”。单击 “测试” 按钮
    3. 打开系统声音首选项，在设备列表中选择 “内部音频”，单击“测试声音”，检查左右声道
验证：
    1. 您听到扬声器发出声音了吗？内置扬声器的声音不应该被自动静音
    2. 您是否从正确的声道听到声音？ 目的：
   音频静音指示灯确认
步骤：
    如果您的系统没有一个特殊的音频静音指示灯，请跳过此测试。
    1. 连续两次按下静音键，观察音频指示灯确认它是否熄灭以及亮起或者更换颜色。
验证：
    音频指示灯是否正常亮起、熄灭以及更换颜色？ 目的：
   大写锁定键指示灯验证
步骤：
    1. 按 “大写锁定键”激活或取消激活大写锁定键
    2. 每次按下按键时，大写锁定键指示灯应该亮起或熄灭
验证：
    大写锁定键指示灯是否正常工作？ 目的：
   摄像头指示灯确认
步骤：
    1. 选择 “测试” 激活摄像头
    2. 摄像头指示灯应该亮几秒钟
验证：
    摄像头指示灯是否点亮？ 目的：
    此测试将检查外置耳机中的均衡控制是否正常工作
步骤：
    1. 检查从左至右移动均衡滑动条是否顺畅
    2. 单击 “测试” 按钮，播放一段10秒的音频。
    3. 来回移动均衡滑动条。
    4. 检查您设置后，实际的耳机音频均衡
验证：
    滑动条移动以及您设置后，实际的音频均衡输出是否顺畅？ 目的：
    此测试将检查内置扬声器中的均衡控制是否正常工作
步骤：
    1. 检查从左至右移动均衡滑动条是否顺畅
    2. 单击 “测试” 按钮，播放一段10秒的音频。
    3. 来回移动均衡滑动条。
    4. 检查您设置后，实际的扬声器音频平衡
验证：
    滑动条移动以及您设置后，实际的音频均衡输出是否顺畅？ 目的：
    检查外部输入连接是否正常工作
步骤：
    1. 使用线缆连接输入端口和外部输出源。
    2. 打开系统声音首选项，选中 “输入” 选项卡，在连接器列表里选择 “线路输入”。点击 “测试” 按钮
    3. 几秒钟将向您回放您的录音。
验证：
   听到您的录音了吗？ 目的：
    检查各声道是否正常工作
步骤：
    1. 单击 “测试” 按钮
验证：
    您应该清楚地听到来自不同声道的声音。 目的：
    检查触屏拖拽与拖放
步骤：
    1. 双击、选中并拖曳桌面上的一个对象
    2. 将对象拖放至不同的位置
验证：
    对象是否被选中、拖曳以及拖放？ 目的：
    检查触屏缩放
步骤：
    1. 将两个手指放置于屏幕并将他们聚集
    2. 将两个手指放置于屏幕并将他们分开
验证：
    屏幕是否被缩小和放大？ 目的：
    检查触屏单击反应
步骤：
    1. 使用手指单击屏幕上的对象。指针应该跳至您点击的位置，对象高亮。
验证：
    点击反应是否正常工作？ 目的：
    创建尽可能使用 CPU 的作业，运行两个小时。如果该系统没有失去响应，则认为此测试已通过。 目的：
    DisplayPort 音频接口验证
步骤：
    1. 插入一个支持声音的外置 DisplayPort 设备(测试一次只使用一个 HDMI 或 DisplayPort 接口)
    2. 单击 “测试” 按钮
验证：
    您通过 DisplayPort 设备听到声音了吗？ 目的：
    执行某些具有挑战性的操作，然后检查 GPU 锁定
步骤：
    1. 创建2个 glxgears 窗口然后快速移动它们
    2. 使用 wmctrl 切换工作区
    3. 在火狐浏览器中启动 HTML5 视频回放
验证：
    60秒的负荷后，检查 kern.log 是否报告 GPU 错误 目的：
   硬盘指示灯确认
步骤：
    1. 选择 “测试”，花几秒钟写入和读取一个临时文件
    2. 向硬盘写入或从硬盘读取时，硬盘指示灯应该点亮
验证：
    硬盘指示灯是否点亮？ 目的：
    HDMI 音频接口验证
步骤：
    1. 插入一个支持声音的外置 HDMI 设备(测试一次只使用一个 HDMI 或 DisplayPort 接口)
    2. 单击 “测试” 按钮
验证：
    您通过 HDMI 设备听到声音了吗？ 目的：
    保存报告中测试人相关的信息
步骤：
    1. 测试人信息
    2. 请在注释区域输入以下信息：
       a. 姓名
       b. 电子邮箱地址
       c. 运行测试原因
验证：
    此测试无需验证 目的：
    手动检测加速度计。
步骤：
    1. 查看您系统的说明书。
验证：
    您系统是否应该存在加速度计？ 目的：
   数字键盘指示灯确认
步骤：
    1. 按 “Block Num/Num Lock” 按键切换数字键盘指示灯
    2. 点击 “测试” 按钮打开窗口确认您的录入
    3. 当指示灯亮和熄灭时，使用数字键盘录入
验证：
    1. 当每次 “Block Num/Num Lock” 被按下时，数字键盘状态都应该切换
    2. 只有当指示灯亮时，键盘确认窗口中才能输入数字 目的：
    电源指示灯确认
步骤：
    1. 设备打开时，电源指示灯应该是亮的
验证：
    电源指示灯是否工作正常？ 目的：
    电源指示灯确认
步骤：
    1. 系统挂起时，电源指示灯应该是闪烁或更换颜色
验证：
    系统挂起时，电源指示灯是否闪烁或更换颜色？ 目的：
   挂起指示灯确认
步骤：
    如果您的系统不存在挂起指示灯，请跳过此测试。
    1. 系统挂起时，挂起指示灯应该是闪烁或更换颜色
验证：
    系统挂起时，电源指示灯是否闪烁或更换颜色？ 目的：
    抓取屏幕( Unity 桌面登录)
步骤：
    1. 使用 USB 网络摄像头抓取图片
验证：
    稍后手动查看附件 目的：
    挂起系统后，抓取屏幕( Unity 桌面登录)
步骤：
    1. 使用 USB 网络摄像头抓取图片
验证：
    稍后手动查看附件 目的：
    播放全屏视频时，抓取屏幕
步骤：
    1. 播放全屏视频
    2. 几秒后，使用 USB 网络摄像头抓取图片
验证：
    稍后手动查看附件 目的：
    测试系统无线硬件是否能够连接到不使用安全的 802.11b/g 协议的路由器。
步骤：
    1. 打开您的路由器配置工具
    2. 更改设置，只接收 B 和 G 无线频带上的连接
    3. 确保 SSID 设置为 ROUTER_SSID
    4. 更改安全设置，不使用安全
    5. 点击 “测试” 按钮创建连接至路由器并测试该连接
验证：
    此测试的验证自动进行。不要更改自动选择的结果。 目的：
    测试系统无线硬件是否能够连接到不使用安全的 802.11n 协议的路由器。
步骤：
    1. 打开您的路由器配置工具
    2. 更改设置，只接收 N 无线频带上的连接
    3. 确保 SSID 设置为 ROUTER_SSID
    4. 更改安全设置，不使用安全
    5. 点击 “测试” 按钮创建连接至路由器并测试该连接
验证：
    此测试的验证自动进行。不要更改自动选择的结果。 目的：
    测试系统无线硬件是否能够连接到使用 WPA 安全和 802.11b/g 协议的路由器。
步骤：
    1. 打开您的路由器配置工具
    2. 更改设置，只接收 B 和 G 无线频带上的连接
    3. 确保 SSID 设置为 ROUTER_SSID
    4. 更改安全设置，使用 WPA2 并确保 PSK 和 ROUTER_PSK 设置的匹配
    5. 点击 “测试” 按钮创建连接至路由器并测试该连接
验证：
    此测试的验证自动进行。不要更改自动选择的结果。 目的：
    测试系统无线硬件是否能够连接到使用 WPA 安全和 802.11n 协议的路由器。
步骤：
    1. 打开您的路由器配置工具
    2. 更改设置，只接收 N 无线频带上的连接
    3. 确保 SSID 设置为 ROUTER_SSID
    4. 更改安全设置，使用 WPA2 并确保 PSK 和 ROUTER_PSK 设置的匹配
    5. 点击 “测试” 按钮创建连接至路由器并测试该连接
验证：
    此测试的验证自动进行。不要更改自动选择的结果。 目的：
    此测试将检查手工插件是否工作正常
步骤：
    1. 添加一个注释
    2. 设置结果已通过
验证：
    检查报告中的结果是否通过，注释是否显示？ 目的：
    此测试将循环进入检测到的视频模式
步骤：
    1. 单击 “测试”，开始循环进入这些视频模式
验证：
    屏幕是否在每个模式下都正常工作？ 目的：
    将测试您显卡的基本 3D 功能
步骤：
    1. 单击 “测试”，执行 OpenGL 演示。任何时候按 ESC 都可关闭。
    2. 验证此动画是否没有出现不流畅或缓慢现象。
验证：
    1. 是否显示此 3D 动画？
    2. 此动画是否没有出现缓慢或不流畅现象？ 目的：
    此测试验证您桌面系统的多显示器输出。这个与在您的笔记本上运行的外接显示测试不同。您需要两台显示器以执行此测试。
步骤：
    如果您的显卡不支持多显示器，请跳过此测试。
    1. 如果您第二台显示器还未连接，请现在连接
    2. 打开 “显示” 工具(打开 dash 搜索 “显示”)
    3. 配置您的输出以在两个显示器上显示一个桌面
    4. 打开任一应用程序并将它从一台显示器拖曳到另一台上。
验证：
    扩展桌面是否在两个屏幕上显示正常？ 目的：
    此测试将检查挂起和唤醒
步骤：
    1. 点击 “测试” ，您的系统将会挂起约30到60秒
    2. 挂起时， 观察电源指示灯是否闪烁或更换颜色
    3. 如果您的系统60秒后未自行唤醒，请立即按电源按钮，手动唤醒系统
    4. 如果您的系统无法唤醒，必须要重启。重启系统后，重新启动 “系统测试” 并标记此测试为 “失败”
验证：
    您的系统是否正常挂起和唤醒？
    (注意：请只关注系统是否成功挂起和唤醒。电源和挂起指示灯确认将在此测试完成后执行。) 目的：
    此测试将检查是否能够配置和连接 DSL 调制解调器。
步骤：
    1. 将电话线连接到此计算机
    2. 单击顶部面板中的网络图标。
    3. 选择 “编辑连接”
    4. 选择 “DSL” 标签页
    5. 添加 “添加” 按钮
    6. 正确配置连接参数
    7. 单击 “测试”，验证是否可建立 HTTP 连接
验证：
    是否显示通知以及是否正确建立了该连接？ 目的：
    此测试将检查 USB 音频设备是否正常工作
步骤：
    1. 将一个USB 音频设备连接到您的系统
    2. 单击 “测试”，然后对着麦克风说话
    3. 几秒钟后将向您回放您说的话
验证：
    通过 USB 耳机是否听到了回放您说的话？ 目的：
    此测试将检查蓝牙连接是否正常工作
步骤：
    1. 启用任何移动设备(PDA、智能手机等)上的蓝牙
    2. 单击菜单栏中的蓝牙图标
    3. 选择 “设置新设备”
    4. 在列表中查找该设备，并选择
    5. 在该设备中输入由向导自动选择的 PIN 码
    6. 该设备应与此计算机配对
    7. 右键单击蓝牙图标，选择浏览文件
    8. 需要时授权此计算机浏览该设备中的文件
    9. 您应能够浏览这些文件
验证：
    所有这些步骤是否均能够执行？ 目的：
    此测试将检查耳机接口是否工作正常
步骤：
    1. 将一对耳机连接到您的音频设备
    2. 单击 “测试” 按钮，让音频设备播放声音
验证：
    您是否通过耳机听到了声音，从耳机播放的声音是否没有失真、咔嗒声或其他奇怪噪声？ 目的：
    此测试将检查内置扬声器是否正常工作
步骤：
    1. 确保未连接外置扬声器或耳机
       如果测试台式机，则允许使用外置扬声器
    2. 单击 “测试” 按钮，在您的音频设备上播放简短的声音
验证：
    您听到声音了吗？ 目的：
    此测试将检查使用外置麦克风录音时是否正常工作
步骤：
    1. 将麦克风连接到麦克风端口
    2. 单击 “测试”，然后对着外置麦克风说话
    3. 几秒钟后将向您回放您说的话
验证：
    是否听到回放了您说的话？ 目的：
    此测试将检查使用内置麦克风录音时是否正常工作
步骤：
    1. 断开已插入的任何外置麦克风
    2. 单击 “测试”，然后对着内置麦克风说话
    3. 几秒钟后将向您回放您说的话。
验证：
    是否听到回放了您说的话？ 目的：
    此测试将检查内置摄像头是否正常工作
步骤：
    1. 单击 “测试”，使从该摄像头捕获的静态图像显示十秒钟。
验证：
    您是否看到了图像？ 目的：
    此测试将检查内置摄像头是否正常工作
步骤：
    1. 单击 “测试”，使从该摄像头捕获的视频显示十秒钟。
验证：
    您是否看到了捕获的视频？ 目的：
    此测试验证在从挂起状态中恢复后显示是否正常。
步骤：
    1. 验证在从挂起状态中恢复后您的显示未出现残影。
验证：
    从挂起状态中恢复后显示是否工作正常？ 目的：
    此测试将检查该系统是否能够切换到虚拟终端，以及切回到 X
步骤：
    1. 单击 “测试”，切换到其他虚拟终端，然后切回到 X
验证：
    您的屏幕是否临时变为文本控制台，然后又切回到您当前的会话？ 目的：
    此测试将检查您的系统是否能够正确检测到 CF 卡从系统读卡器移除
步骤：
    1. 单击 “测试”，从系统读卡器移除 CF 卡。
       (注意：此测试将在 20 秒后超时。)
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统是否能够正确检测到 MS 卡从系统读卡器移除
步骤：
    1. 单击 “测试”，从系统读卡器移除 MS 卡。
       (注意：此测试将在 20 秒后超时。)
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统是否能够正确检测到 MSP 卡从系统读卡器移除
步骤：
    1. 单击 “测试”，从系统读卡器移除 MSP 卡。
       (注意：此测试将在 20 秒后超时。)
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统是否能够正确检测到 SDXC 卡从系统读卡器移除
步骤：
    1. 单击 “测试”，从系统读卡器移除 SDXC 卡。
       (注意：此测试将在 20 秒后超时。)
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统是否能够正确检测到 xD 卡从系统读卡器移除
步骤：
    1. 单击 “测试”，从系统读卡器移除 xD 卡。
       (注意：此测试将在 20 秒后超时。)
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统挂起后，是否能够正确检测到 SD 卡从系统读卡器移除
步骤：
    1. 单击 “测试”，从系统读卡器移除 SD 卡。
       (注意：此测试将在 20 秒后超时。)
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查您的系统是否能够正确检测到 SD 卡从系统读卡器移除
步骤：
    1. 单击 “测试”，从系统读卡器移除 SD 卡。
       (注意：此测试将在 20 秒后超时。)
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查您的系统是否能够正确检测到 SDHC 卡从系统读卡器移除
步骤：
    1. 单击 “测试”，从系统读卡器移除 SDHC 卡。
       (注意：此测试将在 20 秒后超时。)
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查您的系统是否能够正确检测到 MMC 卡从系统读卡器移除
步骤：
    1. 单击 “测试”，从系统读卡器移除 MMC 卡。
       (注意：此测试将在 20 秒后超时。)
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查您的系统是否能够正确检测到 USB 3.0 存储设备插入
步骤：
    1. 单击 “测试”，将 USB 3.0 存储设备(笔式驱动器或硬盘驱动器)插入 USB 3.0 接口。
       (注意：此测试将在 20 秒后超时。)
    2. 测试后不要拔出设备。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统从挂起中唤醒后，是否能够正确检测到 USB 3.0 存储设备插入
步骤：
    1. 单击 “测试”，将 USB 3.0 存储设备(笔式驱动器或硬盘驱动器)插入 USB 3.0 接口。
       (注意：此测试将在 20 秒后超时。)
    2. 测试后不要拔出设备。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查您的系统是否能够正确检测到 USB 存储设备插入
步骤：
    1. 单击 “测试”，插入 USB 存储设备(笔式驱动器或硬盘驱动器)。
       (注意：此测试将在 20 秒后超时。)
    2. 测试后不要拔出设备。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统从挂起中唤醒后，是否能够正确检测到 USB 存储设备插入
步骤：
    1. 单击 “测试”，将 USB 存储设备(笔式驱动器或硬盘驱动器)插入。
       (注意：此测试将在 20 秒后超时。)
    2. 测试后不要拔出设备。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统被挂起后，是否能够正确检测到 CF 卡从系统读卡器移除
步骤：
    1. 单击 “测试”，从系统读卡器移除 CF 卡。
       (注意：此测试将在 20 秒后超时。)
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统挂起后，是否能够正确检测到 MS 卡从系统读卡器移除
步骤：
    1. 单击 “测试”，从系统读卡器移除 MS 卡。
       (注意：此测试将在 20 秒后超时。)
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统挂起后，是否能够正确检测到 MSP 卡从系统读卡器移除
步骤：
    1. 单击 “测试”，从系统读卡器移除 MSP 卡。
       (注意：此测试将在 20 秒后超时。)
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统被挂起后，是否能够正确检测到 SDXC 卡从系统读卡器移除
步骤：
    1. 单击 “测试”，从系统读卡器移除 SDXC 卡。
       如果文件浏览器开启，您可以安全地关闭它。
       (注意：此测试将在 20 秒后超时。)
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统挂起后，是否能够正确检测到 xD 卡从系统读卡器移除
步骤：
    1. 单击 “测试”，从系统读卡器移除 xD 卡。
       (注意：此测试将在 20 秒后超时。)
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统挂起后，是否能够正确检测到 MMC 卡从系统读卡器移除
步骤：
    1. 单击 “测试”，从系统读卡器移除 MMC 卡。
       (注意：此测试将在 20 秒后超时。)
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统挂起后，是否能够正确检测到 SDHC 卡从系统读卡器移除
步骤：
    1. 单击 “测试”，从系统读卡器移除 SDHC 卡。
       (注意：此测试将在 20 秒后超时。)
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查您的系统是否能够正确检测到 3.0 USB 存储设备移除
步骤：
    1. 单击 “测试”，移除 USB 3.0 存储设备。
       (注意：此测试将在 20 秒后超时。)
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统挂起后，是否能够正确检测到 USB 3.0 储存设备被移除
步骤：
    1. 单击 “测试”，移除 USB 3.0 储存设备。
       (注意：此测试将在 20 秒后超时。)
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查您的系统是否能够正确检测到 USB 存储设备移除
步骤：
    1. 单击 “测试”，移除 USB 存储设备。
       (注意：此测试将在 20 秒后超时。)
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统挂起后，是否能够正确检测到 USB 储存设备被移除
步骤：
    1. 单击 “测试”，移除 USB 储存设备。
       (注意：此测试将在 20 秒后超时。)
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统挂起后，系统读卡器是否能够检测到 CF 卡被插入
步骤：
    1. 单击 “测试” 并将 CF 卡插入读卡器。
       如果文件浏览器开启，您可以安全地关闭它。
       (注意：此测试将在 20 秒后超时。)
    2. 测试后请勿移除设备。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统读卡器是否能够检测到闪存(CF)存储卡被插入
步骤：
    1. 单击 “测试” 并将 CF 卡插入读卡器。
       如果文件浏览器开启，您可以安全地关闭它。
       (注意：此测试将在 20 秒后超时。)
    2. 测试后请勿移除设备。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统读卡器是否能够检测到尖端数码(xD)储存卡被插入
步骤：
    1. 单击 “测试” 并将 xD 卡插入读卡器。
       如果文件浏览器开启，您可以安全地关闭它。
       (注意：此测试将在 20 秒后超时。)
    2. 测试后请勿移除设备。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统挂起后，系统读卡器是否能够检测到 MS 卡被插入
步骤：
    1. 单击 “测试” 并将 MS 卡插入读卡器。
       如果文件浏览器开启，您可以安全地关闭它。
       (注意：此测试将在 20 秒后超时。)
    2. 测试后请勿移除设备。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统挂起后，系统读卡器是否能够检测到 MSP 卡被插入
步骤：
    1. 单击 “测试” 并将 MSP 卡插入读卡器。
       如果文件浏览器开启，您可以安全地关闭它。
       (注意：此测试将在 20 秒后超时。)
    2. 测试后请勿移除设备。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统读卡器是否能够检测到记忆棒(MS)存储卡被插入
步骤：
    1. 单击 “测试” 并将 MS 卡插入读卡器。
       如果文件浏览器开启，您可以安全地关闭它。
       (注意：此测试将在 20 秒后超时。)
    2. 测试后请勿移除设备。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统读卡器是否能够检测到 MSP 卡被插入
步骤：
    1. 单击 “测试” 并将 MSP 卡插入读卡器。
       如果文件浏览器开启，您可以安全地关闭它。
       (注意：此测试将在 20 秒后超时。)
    2. 测试后请勿移除设备。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统读卡器是否能够检测到多媒体卡(MMC)被插入
步骤：
    1. 单击 “测试” 并将 MMC 卡插入读卡器。
       如果文件浏览器开启，您可以安全地关闭它。
       (注意：此测试将在 20 秒后超时。)
    2. 测试后请勿移除设备。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统挂起后，系统读卡器是否能够检测到 SDXC 卡被插入
步骤：
    1. 单击 “测试” 并将 SDXC 卡插入读卡器。
       如果文件浏览器开启，您可以安全地关闭它。
       (注意：此测试将在 20 秒后超时。)
    2. 测试后请勿移除设备。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统读卡器是否能够检测到超大容量SD(SDXC)存储卡被插入
步骤：
    1. 单击 “测试” 并将 SDXC 卡插入读卡器。
       如果文件浏览器开启，您可以安全地关闭它。
       (注意：此测试将在 20 秒后超时。)
    2. 测试后请勿移除设备。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统读卡器是否能够检测到未锁定的高容量SD(SDHC)存储卡被插入
步骤：
    1. 单击 “测试” 并将未锁定的 SDHC 卡插入读卡器。
       如果文件浏览器开启，您可以安全地关闭它。
       (注意：此测试将在 20 秒后超时。)
    2. 测试后请勿移除设备。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统挂起后，系统读卡器是否能够检测到 xD 卡被插入
步骤：
    1. 单击 “测试” 并将 xD 卡插入读卡器。
       如果文件浏览器开启，您可以安全地关闭它。
       (注意：此测试将在 20 秒后超时。)
    2. 测试后请勿移除设备。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统从挂起中唤醒后，系统读卡器是否能够检测到 MMC 卡被插入
步骤：
    1. 单击 “测试” 并将 MMC 卡插入读卡器。
       如果文件浏览器开启，您可以安全地关闭它。
       (注意：此测试将在 20 秒后超时。)
    2. 测试后请勿移除设备。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统从挂起中唤醒后，系统读卡器是否能够检测到未锁定 SD 卡被插入
步骤：
    1. 单击 “测试” 并将未锁定 SD 卡插入读卡器。
       如果文件浏览器开启，您可以安全地关闭它。
       (注意：此测试将在 20 秒后超时。)
    2. 测试后请勿移除设备。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统从挂起中唤醒后，系统读卡器是否能够检测到未锁定 SDHC 卡被插入
步骤：
    1. 单击 “测试” 并将未锁定 SDHC 卡插入读卡器。
       如果文件浏览器开启，您可以安全地关闭它。
       (注意：此测试将在 20 秒后超时。)
    2. 测试后请勿移除设备。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查系统读卡器是否能够检测到未锁定的安全数码(SD)储存卡被插入
步骤：
    1. 单击 “测试” 并将未锁定的 SD 卡插入读卡器。
       如果文件浏览器开启，您可以安全地关闭它。
       (注意：此测试将在 20 秒后超时。)
    2. 测试后请勿移除设备。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查您是否能够使用蓝牙音频设备录制和播放音频
步骤：
    1. 启用蓝牙耳机
    2. 单击菜单栏中的蓝牙图标
    3. 选择 “设置新设备”
    4. 在列表中查找该设备，并选择
    5. 在该设备中输入由向导自动选择的 PIN 码
    6. 该设备应与此计算机配对
    7. 单击 “测试”，在该蓝牙设备中录音五秒钟，然后播放
验证：
    您是否听到了您在该蓝牙设备中录制的声音 目的：
    此测试将检查您是否能够通过蓝牙连接传输信息
步骤：
    1. 确保您能够浏览您移动设备上的文件
    2. 将一个文件从此计算机复制到该移动设备
    3. 将一个文件从该移动设备复制到此计算机
验证：
    所有文件是否均正确复制？ 目的：
    此测试将检查您是否能够使用蓝牙 HID 设备
步骤：
    1. 启用蓝牙鼠标或者键盘
    2. 单击菜单栏中的蓝牙图标
    3. 选择 “设置新设备”
    4. 在列表中查找该设备，并选择
    5. 测试鼠标时，执行移动指针、单击左右键以及双击等操作
    6. 测试键盘时，请点击 “测试” 按钮启动一款小工具。在工具内输入文本并关闭它。
验证：
    设备是否正常工作？ 目的：
    此测试将检查您的系统是否可使用 USB HID 设备。
步骤：
    1. 插入一个 USB 鼠标或键盘。
    2. 测试鼠标时，执行移动指针、单击左右键以及双击等操作
    3. 测试键盘时，请点击 “测试” 按钮启动一款小工具。在工具内输入文本并关闭它。
验证：
    设备是否正常工作？ 目的：
    此测试将检查您的系统是否可检测到 USB 存储设备。
步骤：
    1. 插入一个或多个 U 盘或 USB 硬盘驱动器。
    2. 单击 “测试”。
信息：
    $output
验证：
    是否检测到了这些驱动器？ 目的：
    此测试将检查您的系统是否能够检测到 FireWire HDD 被插入
步骤：
    1. 单击 “测试” 开始测试。
       如插入 FireWire HDD  在 20 秒内未被检测到，此测试将超时。
    2. 将FireWire HDD  插入一个可用的 FireWire  端口。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查您的系统是否能够检测到 eSATA HDD 被插入
步骤：
    1. 单击 “测试” 开始测试。
       如插入 eSATA HDD 在20秒内未被检测到，此测试将超时。
    2. 将 eSATA HDD 插入一个可用的 eSATA 端口。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查您的系统是否能够检测到 FireWire HDD 被移除
步骤：
    1. 单击 “测试” 开始测试。
       如移除 FireWire HDD 在 20 秒内未被检测到，此测试将超时。
    2. 将 FireWire HDD 从原先插入的 FireWire 端口上移除。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查您的系统是否能够检测到 eSATA HDD 被移除
步骤：
    1. 单击 “测试” 开始测试。
       如移除 eSATA HDD 在 20 秒内未被检测到，此测试将超时。
    2. 将 eSATA HDD 从原先插入的 eSATA 端口上移除。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查并确保您的系统能够成功休眠(如果支持)
步骤：
    1. 单击 “测试”
    2. 该系统将休眠，并且应在 5 分钟内自动唤醒
    3. 如果您的系统在 5 分钟后没有自动唤醒，请按电源按钮手动唤醒该系统
    4. 如果该系统没有从休眠状态中恢复，请手动重新启动 “系统测试”，并将此测试标记为失败
验证：
    该系统是否成功休眠，唤醒后其是否正常工作？ 目的：
    此测试将检查您系统的 CD 音频播放功能。
步骤：
    1. 将一张音频 CD 放入您的光驱中
    2. 有提示时，启动音乐播放器。
    3. 在音乐播放器的显示中，定位到 CD
    4. 在音乐播放器中选择此 CD
    5. 点击播放按钮欣赏 CD 上的音乐
    6. 一段时间后，停止播放
    7. 在 CD 图标上右击，并选择“弹出光盘”
    8. 会弹出 CD
    9. 关闭音乐播放器
验证：
    所以步骤都可以工作吗？ 目的：
    此测试将检查您的 DVD 播放功能
步骤：
    1. 将一张包含任何电影的 DVD 插入您的光驱中
    2. 单击 “测试”，在 Totem 中播放该 DVD
验证：
    是否播放了该文件？ 目的：
    此测试将检查您的 DVI 接口。
步骤：
    如果您的系统没有 DVI 接口，请跳过此测试。
    1. 将显示器连接至您系统上的 DVI 接口(如果还未连接)
验证：
    桌面双屏显现是否正常？ 目的：
    此测试将检查您的 DisplayPort 接口。
步骤：
    如果您的系统没有 DisplayPort 接口，请跳过此测试。
    1. 将显示器连接至您系统上的 DisplayPort 接口(如果还未连接)
验证：
    桌面双屏显现是否正常？ 目的：
    此测试将检查您的 HDMI 接口。
步骤：
    如果您的系统没有 HDMI 接口，请跳过此测试。
    1. 将显示器连接至您系统上的 HDMI 接口(如果还未连接)
验证：
    桌面双屏显现是否正常？ 目的：
    此测试将检查您的 RCA 接口。
步骤：
    如果您的系统没有 RCA 接口，请跳过此测试。
    1. 将显示器连接至您系统上的 RCA 接口(如果还未连接)
验证：
    桌面双屏显现是否正常？ 目的：
    此测试将检查您的 S-VIDEO 接口。
步骤：
    如果您的系统没有 S-VIDEO 接口，请跳过此测试。
    1. 将显示器连接至您系统上的 S-VIDEO 接口(如果还未连接)
验证：
    桌面双屏显现是否正常？ 目的：
    此测试将检查您的 USB 3.0 连接。
步骤：
    1. 将一个 USB 3.0 移动硬盘或 U 盘插入此计算机中的 USB 3.0 接口。
    2. 此时在启动器上应显示一个图标。
    3. 单击 “测试”，开始进行此测试。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查您的 USB 连接。
步骤：
    1. 将一个 USB 存储设备连接到此计算机上的外置 USB 插槽。
    2. 此时在启动器上应显示一个图标。
    3. 确认该图标能够显示。
    4. 弹出该设备。
    5. 使用每个外置 USB 插槽重复这些步骤。
验证：
    该设备是否可使用所有 USB 插槽？ 目的：
    此测试将检查您的 USB 连接。
步骤：
    1. 将一个 USB 移动硬盘或 U 盘插入此计算机中。
    2. 此时在启动器上应显示一个图标。
    3. 单击 “测试”，开始进行此测试。
验证：
    此测试的验证自动进行。
    不要更改自动选择的结果。 目的：
    此测试将检查您的 VGA 接口。
步骤：
    如果您的系统没有 VGA 接口，请跳过此测试。
    1. 将显示器连接至您系统上的 VGA 接口(如果还未连接)
验证：
    桌面双屏显现是否正常？ 目的：
    此测试将检查您的笔记本盖子传感器。
步骤：
    1. 点击 “测试”。
    2. 关闭和打开您的笔记本盖子。
验证：
    您的笔记本盖子关闭时，屏幕是否关闭？ 目的：
    此测试将检查您的笔记本盖子传感器。
步骤：
    1. 点击 “测试”。
    2. 关闭笔记本盖子。
    3. 关闭盖子，等待5秒。
    4. 打开笔记本盖子。
验证：
    您的笔记本打开时，系统是否唤醒？ 目的：
    此测试将检查您的笔记本盖子传感器。
步骤：
    1. 关闭您的笔记本盖子。
验证：
    您的笔记本关闭时，系统是否挂起？ 目的：
    此测试将检查您的显示器省电能力
步骤：
    1. 点击 “测试” 尝试您的显示器省电能力
    2. 按下任一按钮或移动鼠标唤醒
验证：
    显示器是否变白并重新打开？ 目的：
    此测试将检查您的系统关闭和启动循环。
步骤：
    1. 选择 “测试” 启动系统关机。
    2. 启动系统。
    3. grub 启动菜单显示时，启动进入 Xen Hypervisor。
    4.系统重启时，登录并重启 checkbox-certification-server。
    4. 选择 “重新运行” 返回到此测试。
    5. 如果系统成功关机，请选择 “是”，表明测试已通过；
    反之，选择 “否”，则表明存在问题。
验证：
    系统是否正常关机和启动？ 目的：
    此测试将检查您的系统关机和启动循环。
步骤：
    1. 关闭您的机器。
    2. 重启您的机器。
    3. 重复步骤1和步骤2至少5次。
验证：
    系统是否正常关机和重启？ 目的：
    此测试将检查您的有线连接
步骤：
    1. 单击顶部面板中的网络图标
    2. 在 “有线网络” 部分下选择一个网络
    3. 单击 “测试”，验证是否可建立 HTTP 连接
验证：
    是否显示通知以及是否正确建立了该连接？ 目的：
    此测试将检查您的无线连接。
步骤：
    1. 单击面板中的网络图标。
    2. 在 “无线网络” 部分下选择一个网络。
    3. 单击 “测试”，验证是否可建立 HTTP 连接。
验证：
    是否显示通知以及是否正确建立了该连接？ 目的：
    此测试将在检测到的显示模式间循环
步骤：
    1. 单击 “测试”，显示器将在检测到的显示模式间循环
验证：
    您的显示器在检测到的模式下是否正常显示？ 目的：
    此测试确保电池测试后，电源被插回。
步骤：
    1. 插入笔记本的电源。
验证：
    您的笔记本电源是否被插入？ 目的：
    此测试确保电源被拔出，以运行电池耗竭测试。
步骤：
    1. 拔出笔记本的电源。
验证：
    您的笔记本电源是否被拔出？ 目的：
    此测试将向指定的设备发送图像 'JPEG_Color_Image_Ubuntu.jpg'
步骤：
    1. 单击 “测试”，此时将提示您输入能够接受文件传输的设备的蓝牙设备名称(输入该文件的名称后可能需要等待片刻才能开始发送)
    2. 接受这两个设备上显示的任何提示
验证：
    是否正确发送了这些数据？ 目的：
    此测试将 “JPEG_Color_Image_Ubuntu.jpg” 图像传送到指定设备
步骤：
    1. 通过检查蓝牙指示器程序确认蓝牙被启用。
    2. 点击 “测试” ，将提示您需要输入可传输文件的蓝牙设备名称(输入文件名称到开始传送可能需要一会儿)
    3. 接受两个设备上出现的任意弹窗
验证：
    数据是否正确传输？ 目的：
    此测试将测试更改屏幕亮度
步骤：
    1. 点击 “测试” 尝试使屏幕变暗
    2. 检查。
    3. 两秒后，屏幕将恢复先前亮度。
验证：
    刚才屏幕亮度是否变为最大亮度的一半？ 目的：
    此测试测试显示旋转
步骤：
    1. 单击 “测试”，测试显示旋转。显示会每4秒旋转一次。
     2. 检测是否所有的旋转(普通的左右颠倒)都正常显示，不存在永久性的屏幕毁坏
验证：
    显示旋转是否都正常运行，不存在永久性的屏幕毁坏？ 目的：
    此测试将测试电池信息键是否工作正常
步骤：
    如果您的电脑无电源按钮，请跳过此测试。
    1. 单击 “测试” 开始测试
    2. 按电源信息按钮(或者类似 Fn+F3 的组合键)
    3. 关闭电源统计工具，如果它打开了。
验证：
    电池信息按键是否工作正常？ 目的：
    此测试将测试系统从挂起中恢复后，电池信息键是否工作正常
步骤：
    如果您的电脑无电源按钮，请跳过此测试。
    1. 单击 “测试” 开始测试
    2. 按电源信息按钮(或者类似 Fn+F3 的组合键)
    3. 关闭电源数据工具，如果它打开了。
验证：
    系统从挂起中恢复后，电池信息按键是否工作正常？ 目的：
    此测试将测试亮度键是否工作正常
步骤：
    1. 按键盘上的亮度按钮
验证：
    在您按下该键后亮度是否改变？ 目的：
    此测试将测试系统从挂起中恢复后，亮度键是否工作正常
步骤：
    1. 按键盘上的亮度按钮
验证：
    系统从挂起中恢复后，在您按下该键后亮度是否改变？ 目的： 
    此测试将检测默认显示器
步骤：
    1. 点击“测试”按钮以显示测试视频。
验证：
    是否看到色条和雪花？ 目的：
    此测试将测试您键盘上的媒体按键是否工作正常
步骤：
    如果您的电脑无媒体按键，请跳过此测试。
    1. 单击 “测试” 打开测试媒体按键的窗口
    2. 如果所有按键工作正常，测试将被标记为通过。
验证：
    按键是否工作正常？ 目的：
    此测试将测试系统从挂起中恢复后，您键盘上的媒体按键是否工作正常
步骤：
    如果您的电脑无媒体按键，请跳过此测试。
    1. 单击 “测试” 打开测试媒体按键的窗口
    2. 如果所有按键工作正常，测试将被标记为通过。
验证：
    系统从挂起中恢复后，按键是否工作正常？ 目的：
    此测试将测试您键盘上的静音键是否工作正常
步骤：
    1. 单击 “测试” 打开测试静音键的窗口
    2. 如果按键工作正常，测试将通过以及窗口将关闭。
验证：
    静音按键是否工作正常？ 目的：
    此测试将测试系统从挂起中恢复后，您键盘上的静音键是否工作正常
步骤：
    1. 单击 “测试” 打开测试静音键的窗口
    2. 如果按键工作正常，测试将通过以及窗口将关闭。
验证：
    系统从挂起中恢复后，静音按键是否工作正常？ 目的：
    此测试将测试睡眠键是否工作正常
步骤：
    1. 按键盘上的睡眠键
    2. 按电源按钮唤醒您的系统
验证：
    按下睡眠键后该系统是否进入睡眠状态？ 目的：
    此测试将测试系统从挂起中恢复后，睡眠键是否工作正常
步骤：
    1. 按键盘上的睡眠键
    2. 按电源按钮唤醒您的系统
验证：
    系统从挂起中恢复后，按下睡眠键后该系统是否进入睡眠状态？ 目的：
    此测试将测试您键盘上的 super 键是否工作正常
步骤：
    1. 单击 “测试” 打开测试 super 键的窗口
    2. 如果按键工作正常，测试将通过以及窗口将关闭。
验证：
    super 按键是否工作正常？ 目的：
    此测试将测试系统从挂起中恢复后，您键盘上的 super 键是否工作正常
步骤：
    1. 单击 “测试” 打开测试 super 键的窗口
    2. 如果按键工作正常，测试将通过以及窗口将关闭。
验证：
    系统从挂起中恢复后，super 按键是否工作正常？ 目的：
    此测试将测试您键盘上的音量键是否工作正常
步骤：
    如果您的电脑无音量键，请跳过此测试。
    1. 单击 “测试” 打开测试音量键的窗口
    2. 如果所有的按键工作正常，测试将被标记为通过。
验证：
    按键是否工作正常？ 目的：
    此测试将测试系统从挂起中恢复后，您键盘上的音量键是否工作正常
步骤：
    如果您的电脑无音量键，请跳过此测试。
    1. 单击 “测试” 打开测试音量键的窗口
    2. 如果所有的按键工作正常，测试将被标记为通过。
验证：
    系统从挂起中恢复后，按键是否工作正常？ 目的：
    此测试将测试无线按键是否工作正常
步骤：
    1. 按键盘上的无线按键
    2. 检查 wifi 指示灯是否熄灭或变换颜色
    3. 检查无线是否被禁用
    4. 再次按下相同的按键
    5. 检查 wifi 指示灯是否亮或变换颜色
    6. 检查无线是否被启用
验证：
    第一次按下无线按键时，无线是否被关闭，再次按按键时，无线是否被开启？
    (注意：LED 功能将会在接下一个测试复查。在此请只考虑 wifi 本身的功能) 目的：
    此测试将测试系统从挂起中恢复后，无线按键是否工作正常
步骤：
    1. 按键盘上的无线按键
    2. 再次按下相同的按键
验证：
    系统从挂起中恢复后，第一次按下无线按键时，无线是否被关闭，再次按按键时，无线是否被开启？ 目的：
    测试您的加速度计，查看设备是否被检测和操作为操纵杆装置。
步骤：
    1. 点击 “测试”
    2. 将您的硬件按屏幕显示的方向倾斜，直到满足轴的阈值。
验证：
    您加速度计是否被检测到？您可以使用该设备吗？ 目的：
    此测试将测试您的键盘
步骤：
    1. 单击 “测试”
    2. 在打开的文本区域上，使用您的键盘键入一些内容
验证：
    您的键盘是否正常工作？ 目的：
    此测试将测试您的指点设备
步骤：
    1. 使用指点设备移动光标，或者触摸屏幕。
    2. 执行一些单击/双击/右键单击操作。
验证：
    该指点设备是否正常工作？ 目的：
    此测试将验证在手动更改分辨率后，GUI 是否可用
步骤：
    1. 打开 “显示” 应用程序
    2. 从下拉列表中选择一个新的分辨率
    3. 单击 “应用”
    4. 从下拉列表中选择原始分辨率
    5. 单击 “应用”
验证：
    分辨率是否如希望地改变？ 目的：
    此测试将确认您的系统是否可以成功重启。
步骤：
    1. 选择 “测试” 启动系统重启。
    2. grub 启动菜单显示时，启动进入 Ubuntu (或允许系统自己自动开机)。
    3.系统重启时，登录并重启 checkbox-certification-server。
    4. 选择 “重新运行” 返回到此测试。
    5. 如果系统成功重启，请选择 “是”，表明测试已通过；
    反之，选择 “否”，则表明存在问题。
验证：
    系统是否正常重启？ 目的：
    此测试将验证默认显示分辨率
步骤：
    1. 此显示器正在使用以下分辨率：
信息:
    $output
验证：
    对于您的显示器，这是否可接受？ 目的：
    此测试将验证 ExpressCard 接口可以检测到已插入的设备。
步骤：
    如果您没有 ExpressCard 接口，请跳过此测试。
    1. 将 ExpressCard 插入 ExpressCard 接口。
验证：
    设备是否被正确检测。 目的：
    确保加压 wifi 热键不会导致程序从面板消失或系统被锁定
步骤：
    1. 登入桌面
    2. 以每秒一次的频率按 wifi 热键，慢慢地加快单击速度，直到不能再快
验证：
    确认系统未被冻结且 wifi 和蓝牙程序仍然可见并可用 目的：
   触控板指示灯确认
步骤：
    1. 单击触控板按钮或按下组合键启用或禁用触控板按钮
    2. 在触控板上滑动您的手指
验证：
    1. 每次单击触控板按钮或按下组合键时，触控板指示灯状态应该都会切换
    2. 指示灯亮起时，鼠标指针应该可以通过触控板移动
    3. 指示灯熄灭时，鼠标指针不可以通过到触控板移动 目的：
    系统从挂起中唤醒后，确认触控板指示灯
步骤：
    1. 单击触控板按钮或按下组合键启用或禁用触控板按钮
    2. 在触控板上滑动您的手指
验证：
    1. 每次单击触控板按钮或按下组合键时，触控板指示灯状态应该都会切换
    2. 指示灯亮起时，鼠标指针应该可以通过触控板移动
    3. 指示灯熄灭时，鼠标指针不可以通过到触控板移动 目的：
    触控板水平滚动验证
步骤：
    1. 准备好测试并将您的指针放置于显示的测试窗口内时，请选择 “测试”。
    2. 确认您可以通过在触控板的下方左右移动您的手指，移动水平滑动条。
验证：
    您可以左右滑动吗？ 目的：
    多点触控的触控板手动检测。
步骤：
    1. 查看您系统的说明书。
验证：
    触控板应该是多点触控的吗？ 目的：
    触控板用户验证
步骤：
    1. 确认触控板已启用。
    2. 使用触控板移动指针。
验证：
    指针移动了吗？ 目的：
    触控板垂直滚动验证
步骤：
    1. 准备好测试并将您的指针放置于显示的测试窗口内时，请选择 “测试”。
    2. 确认您可以通过在触控板的右边上下移动您的手指，移动垂直滑动条。
验证：
    您可以上下滑动吗？ 目的：
    多点触控的触屏手动检测。
步骤：
    1. 查看您系统的说明书。
验证：
    触屏应该是多点触控的吗？ 目的：
   系统从挂起中唤醒后，确认无线(WLAN + 蓝牙)指示灯
步骤：
    1. 确保 WLAN 连接已建立以及蓝牙已启用。
    2. WLAN 以及蓝牙指示灯应该亮起
    3. 从硬件开关关闭 WLAN 以及蓝牙(如果存在)
    4. 重新打开
    5. 从面板程序关闭 WLAN 以及蓝牙
    6. 重新打开
验证：
   系统从挂起中唤醒后，WLAN 以及蓝牙指示灯是否正常亮起？ 目的：
   当 WLAN 被禁用时，检验指示灯是否熄灭
步骤：
    1. 进行按键或无线测试，关闭无线之后执行测试时，您应该已观察了 WLAN 指示灯
    2. 无线关闭时，WLAN 指示灯应该熄灭或者更换颜色
验证：
    WLAN 指示灯是否正常熄灭或者更换颜色？ 目的：
   系统从挂起中唤醒后，当 WLAN 被禁用时，检验指示灯是否熄灭
步骤：
    1. 连接至 AP
    2. 使用物理开关禁用 WLAN
    3. 重新开启
    4. 使用网络管理器禁用 WLAN
验证：
   系统从挂起中唤醒后，当 WLAN 被禁用时，指示灯是否正常熄灭？ 目的：
   当蓝牙被启用或禁用时，检验蓝牙指示灯是否亮起或熄灭
步骤：
    1. 从硬件开关关闭蓝牙(如果存在)
    2. 重新开启蓝牙
    3. 从面板程序关闭蓝牙
    4. 重新开启蓝牙
验证：
    蓝牙指示灯是否连续两次亮起然后熄灭？ 目的：
    系统从挂起中唤醒后，当蓝牙被启用或禁用时，检验蓝牙指示灯是否亮起或熄灭
步骤：
    1. 从硬件开关关闭蓝牙(如果存在)
    2. 重新开启蓝牙
    3. 从面板程序关闭蓝牙
    4. 重新开启蓝牙
验证：
      系统从挂起中唤醒后，蓝牙指示灯是否连续两次亮起和熄灭？ 目的：
    系统从挂起中唤醒后，验证大写锁定键指示灯是否正常工作
步骤：
    1. 按 “大写锁定键”激活或取消激活大写锁定键
    2. 每次按下按键时，大写锁定键指示灯应该亮起或熄灭
验证：
    系统从挂起中唤醒后，大写锁定键指示灯是否正常工作？ 目的：
    检验外部视频热键是否工作正常
步骤：
    1. 插入外接显示器
    2. 按下显示热键以更改显示器配置
验证：
    检查视频信号能否可以被镜像、扩展还是只在外部或板载上显示。 目的：
    检验系统从挂起中恢复后，外部热键是否工作正常
步骤：
    1. 插入外接显示器
    2. 按下显示热键以更改显示器配置
验证：
    检查统从挂起中恢复后，视频信号能否被设置为可镜像、扩展还是只在外部或板载上显示。 目的：
   系统从挂起中唤醒后，验证硬盘指示灯是否能够继续正常工作
步骤：
    1. 选择 “测试”，花几秒钟写入和读取一个临时文件
    2. 向硬盘写入或从硬盘读取时，硬盘指示灯应该闪烁
验证：
   系统从挂起中唤醒后，使用硬盘时，指示灯是否闪烁？ 目的：
   验证电源指示灯是否提示低电量
步骤：
    1. 让您的系统使用电池运行几个小时
    2. 仔细观察电池指示灯
验证：
    电池低电量时，电源指示灯是否显示橘色？ 目的：
   系统从挂起中唤醒后，验证电源指示灯是否提示低电量
步骤：
    1. 让您的系统使用电池运行几个小时
    2. 仔细观察电池指示灯
验证：
   系统从挂起中唤醒后，电池低电量时，电源指示灯是否显示橘色？ 目的：
   验证电源指示灯是否正确显示充电状态
步骤：
    1. 让您的系统使用电池运行一段时间
    2. 插入 AC 电源线
    3. 让您的系统使用 AC 电源线运行
验证：
    系统全部充满电时，橘色电源指示灯是否熄灭？ 目的：
   系统从挂起中唤醒后，验证电源指示灯是否正确显示充电状态
步骤：
    1. 让您的系统使用电池运行一段时间
    2. 插入 AC 电源线
    3. 让您的系统使用 AC 电源线运行
验证：
   系统从挂起中唤醒后，系统全部充满电时，橘色电源指示灯是否熄灭？ 目的：
   验证电源灯是否显示充电状态
步骤：
    1. 让您的系统使用电池运行一段时间
    2. 插入 AC 电源线
验证：
    电源指示灯是否转为橘色？ 目的：
   系统从挂起中唤醒后，验证电源灯是否显示充电状态
步骤：
    1. 让您的系统使用电池运行一段时间
    2. 插入 AC 电源线
验证：
   系统从挂起中唤醒后，电源指示灯是否转为橘色？ 目的：
    系统从挂起中唤醒后，确认摄像头指示灯是否工作正常
步骤：
    1. 选择 “测试” 激活摄像头
    2. 摄像头指示灯应该亮几秒钟
验证：
    系统从挂起中唤醒后，摄像头指示灯是否点亮？ 目的：
   系统从挂起中唤醒后，确认数字键盘指示灯是否工作正常
步骤：
    1. 按 “Block Num/Num Lock” 按键切换数字键盘指示灯
    2. 点击 “测试” 按钮打开窗口确认您的录入
    3. 当指示灯亮和熄灭时，使用数字键盘录入
验证：
    1. 当每次 “Block Num/Num Lock” 被按下时，数字键盘状态都应该切换
    2. 当指示灯亮时，键盘确认窗口中只能输入数字 目的：
   系统从挂起中唤醒后，验证电源指示灯是否正常工作
步骤：
    1. 设备打开时，电源指示灯应该是亮的
验证：
   系统从挂起中唤醒后，电源指示灯是否仍然亮？ 目的：
    确认触控板热键切换触控板功能的开启和关闭
步骤：
    1. 确认触控板是可用的
    2. 单击触控板切换热键
    3. 再次单击触控板切换热键
验证：
    确认触控板是否被禁用并重新开启。 目的：
    确认系统从挂起中唤醒后，触控板热键能够切换触控板功能的开启和关闭
步骤：
    1. 确认触控板是可用的
    2. 单击触控板切换热键
    3. 再次单击触控板切换热键
验证：
    确认系统从挂起中唤醒后，触控板是否被禁用并重新开启。 目的：
   WLAN 指示灯验证
步骤：
    1. 进行按键或无线测试，重新开启无线时，您应该已观察了无线指示灯
    2. 无线开启时，WLAN 指示灯应该亮起或者更换颜色
验证：
    WLAN 指示灯是否正常亮起或者更换颜色？ 目的：
   系统从挂起中唤醒后，验证 WLAN 指示灯
步骤：
    1. 确认 WLAN 连接已建立
    2. WLAN 指示灯应该亮起
验证：
    系统从挂起中唤醒后，WLAN 指示灯是否正常亮起？ 目的：
    通过 USB 键盘唤醒
步骤：
    1. 启用 BIOS 中的 “通过 USB 键盘/键盘唤醒” 项目
    2. 点击 “测试” 进入挂起(S3)模式
    3. 点击 USB 键盘上的任一按键唤醒系统
验证：
    您按下键盘按键时，系统是否从挂起模式中唤醒？ 目的：
    通过 USB 鼠标唤醒
步骤：
    1. 启用 BIOS 中的 “通过 USB 键盘/键盘唤醒” 项目
    2. 点击 “测试” 进入挂起(S3)模式
    3. 点击 USB 鼠标上的任一按钮唤醒系统
验证：
    您按下鼠标按钮时，系统是否从挂起模式中唤醒？ 目的：
   无线(WLAN + 蓝牙)指示灯确认
步骤：
    1. 确保 WLAN 连接已建立以及蓝牙已启用。
    2. WLAN 以及蓝牙指示灯应该亮起
    3. 从硬件开关关闭 WLAN 以及蓝牙(如果存在)
    4. 重新打开
    5. 从面板程序关闭 WLAN 以及蓝牙
    6. 重新打开
验证：
    WLAN 以及蓝牙指示灯是否行为正常？ 目的：
    这是一个自动压力测试，迫使系统休眠和唤醒250次循环 目的：
    这是一个自动压力测试，迫使系统休眠和唤醒30次循环 目的：
    这是一个自动压力测试，迫使系统挂起和唤醒250次循环 目的：
    这是一个自动压力测试，迫使系统挂起和唤醒30次循环 目的：
   此测试将验证 USB DSL 或移动宽带调制解调器是否可用
步骤：
   1. 将 USB 线连接至电脑
   2. 右击面板上的网络图标
   3. 选择 “编辑连接”
   4. 选择 “DSL” (ADSL 调制解调器)或 “移动宽带” (3G 调制解调器)标签
   5. 单击 “添加” 按钮
   6. 正确配置连接参数
   7. 注意 OSD 应该确认连接已被建立
   8. 选择 “测试” 确认是否可能建立 HTTP 连接
验证：
   连接是否正确建立？ 目的：
    此测试将验证指纹阅读器可以用来解锁被锁定的系统。
步骤：
   1. 单击会话指示器(面板右侧的锯齿图标)。
   2. 选择 “锁定屏幕”
   3. 按下任意键或移动鼠标。
   4. 解锁窗口将弹出，可以通过输入您的密码或者使用指纹认证解锁。
   5. 使用指纹阅读器解锁。
   6. 您的屏幕应该被解锁。
验证：
   认证过程是否正常工作。 目的：
    此测试将验证指纹阅读器可以用来登入您的系统。此测试用例假设存在运行用例的测试账户以及测试人员用来验证指纹阅读器的个人账户。
步骤：
   1. 单击面板右侧的用户指示器(您的用户名)。
   2. 选择 “切换账户”
   3. 在 LightDM 界面选择您的用户名。
   4. 使用指纹阅读器登入。
   5. 单击用户切换小程序。
   6. 选择测试账户以继续运行测试。
验证：
   认证过程是否正常工作。 目的：
   此测试将验证网络打印机是否可用
步骤：
   1. 确保可在您的网络中获得打印机
   2. 单击右上角中的齿轮图标，然后单击 “打印机”
   3. 如果尚未列出该打印机，则单击 “添加”
   4. 应检测到该打印机，并且应显示正确的配置值
   5. 打印测试页
验证：
   您是否能够用网络打印机打印测试页？ 目的：
    本测试将验证您的桌面时钟是否显示正确的日期和时间。
步骤：
    1. 检查桌面右上角的时钟。
验证：
    您的时区下，时钟显示的日期和时间是否正确？ 目的：
    本测试将检测您的桌面时钟与系统时钟是否保持同步。
步骤：
    1. 开始测试，确认时钟被程序自动调快了一小时。(注意时钟同步可能会花去一分钟左右时间)
    2. 右键单击时钟，点击 “时间日期设置”
    3. 确保时钟程序被设定为 “手动设置时间”
    4. 将时钟回调一小时。
    5. 关闭窗口，重启电脑。
验证：
    您的时区下，系统时钟显示的日期和时间是否正确？ 目的：
   此测试将验证您是否能够从桌面菜单重新启动系统
步骤：
   1. 单击桌面右上角的齿轮图标，然后单击 “关机”
   2. 单击 “关机” 对话左侧的 “重新启动” 按钮
   3. 重新登录后，重新启动系统测试，其应继续回到此处
验证：
   您的系统是否重新启动并且干净地回到了 GUI 登录？ 目的：
   此测试将验证您的系统是否能够播放 Ogg Vorbis 音频文件。
步骤：
   1. 单击 “测试”，播放 Ogg Vorbis 文件 (.ogg)
   2. 请关闭播放器，继续。
验证：
   该示例文件是否正确播放？ 目的：
   此测试将验证您的系统是否能够播放 Wave Audio 文件。
步骤：
   1. 选择 “测试”，播放一个 Wave Audio 格式文件 (.wav)
   2. 请关闭播放器，继续。
验证：
   该示例文件是否正确播放？ 目的：
 如果成功修复，您将看到重启 checkbox 的测试，而不是 sniff4
步骤：
 1. 点击 “是”
验证：
 不是必须的，这是个虚假的测试。 目的：
模拟重启机器时产生的故障
步骤：
 1. 点击 “测试” 触发重启
 2. 重新登陆并重启 checkbox 时，选择 “继续”
验证：
您不会看见用户认证 目的：
 有些系统不在所有的网卡共享 IPMI 但是存在直接连接至 BMC 的专用管理接口。此测试证实您已使用了远程 IPMI 连接和行为的接口。
步骤：
 1. 运行测试前，您应该已配置和使用了 “专用管理接口” 远程关闭或开启系统。
验证：
 如果此系统只使用共享管理/以太网接口或者此系统无 BMC(管理控制台)，请跳过此测试
 1. 如果您通过专用管理接口已成功使用 IPMI 去远程开启或关闭系统，请选择 “是”。
 2. 如果您尝试专用管理接口去远程开启或关闭系统失败，请选择 “否”。 目的：
 此测试将检查 gcalctool(计算器)是否正常工作。
步骤：
 单击 “测试” 按钮，打开该计算器，然后执行：
  1. 剪切
  2. 复制
  3. 粘贴
验证：
 这些功能是否正常执行？ 目的：
 此测试将检查 gcalctool(计算器)是否正常工作。
步骤：
 单击 “测试” 按钮，打开该计算器，然后执行：
  1. 记忆设置
  2. 记忆重置
  3. 记忆上次清除
  4. 记忆清除
验证：
 这些是否正常执行？ 目的：
 此测试将检查 gcalctool(计算器)是否正常工作。
步骤：
 单击 “测试” 按钮，打开该计算器，然后执行：
 1. 简单的数学运算(+、-、/、*)
 2. 嵌套的数学运算((,))
 3. 分数数学
 4. 小数数学
验证：
 这些功能是否正常执行？ 目的：
 此测试将检查 gcalctool(计算器)是否正常工作。
步骤：
 单击 “测试” 按钮，打开该计算器。
验证：
 该计算器是否正确启动？ 目的：
 此测试将检查 gedit 是否正常工作。
步骤：
 1. 单击 “测试” 按钮，打开 gedit，然后重新打开您先前创建的文件。
 2. 编辑并保存该文件，然后关闭 gedit。
验证：
 是否正常执行？ 目的：
 此测试将检查 gedit 是否正常工作。
步骤：
 1. 单击 “测试” 按钮，打开 gedit。
 2. 输入一些文字，保存该文件(记下您使用的文件名)，然后关闭 gedit。
验证：
 是否正常执行？ 目的：
 此测试将检查 Empathy 消息客户端是否正常工作。
步骤：
 1. 选择 “测试”，启动 Empathy。
 2. 对其进行配置，使其连接到 AOL Instant Messaging (AIM)服务。
 3. 完成了此测试后，请退出 Empathy，继续在这里操作。
验证：
 您是否能够正确连接并收/发消息？ 目的：
 此测试将检查 Empathy 消息客户端是否正常工作。
步骤：
 1. 选择 “测试”，启动 Empathy。
 2. 对其进行配置，使其连接到 Facebook 聊天服务。
 3. 完成了此测试后，请退出 Empathy，继续在这里操作。
验证：
 您是否能够正确连接并收/发消息？ 目的：
 此测试将检查 Empathy 消息客户端是否正常工作。
步骤：
 1. 选择 “测试”，启动 Empathy。
 2. 对其进行配置，使其连接到 Google Talk (gtalk)服务。
 3. 完成了此测试后，请退出 Empathy，继续在这里操作。
验证：
 您是否能够正确连接并收/发消息？ 目的：
 此测试将检查 Empathy 消息客户端是否正常工作。
步骤：
 1. 选择 “测试”，启动 Empathy。
 2. 对其进行配置，使其连接到 Jabber 服务。
 3. 完成了此测试后，请退出 Empathy，继续在这里操作。
验证：
 您是否能够正确连接并收/发消息？ 目的：
 此测试将检查 Empathy 消息客户端是否正常工作。
步骤：
 1. 选择 “测试”，启动 Empathy。
 2. 对其进行配置，使其连接到 Microsoft Network (MSN) 服务。
 3. 完成了此测试后，请退出 Empathy，继续在这里操作。
验证：
 您是否能够正确连接并收/发消息？ 目的：
 此测试将检查 Evolution 是否正常工作。
步骤：
 1. 单击 “测试” 按钮，启动 Evolution。
 2. 对其进行配置，使其连接到 IMAP 帐户。
验证：
 您是否能够正确接收并阅读电子邮件？ 目的：
 此测试将检查 Evolution 是否正常工作。
步骤：
 1. 单击 “测试” 按钮，启动 Evolution。
 2. 对其进行配置，使其连接到 POP3 帐户。
验证：
 您是否能够正确接收并阅读电子邮件？ 目的：
 此测试将检查 Evolution 是否正常工作。
步骤：
 1. 单击 “测试” 按钮，启动 Evolution。
 2. 对其进行配置，使其连接到 SMTP 帐户。
验证：
 您是否能够无错误地发送电子邮件？ 目的：
 此测试将检查 Firefox 是否能够播放 Flash 视频。注意：这可能
 需要安装额外软件才能成功完成。
步骤：
 1. 选择 “测试”，启动 Firefox，然后观看一段简短的 Flash 视频。
验证：
 该视频是否正确播放？ 目的：
 此测试将检查 Firefox 是否能够播放 Quicktime(.mov)视频文件。
 注意：这可能需要安装额外软件才能成功完成。
步骤：
 1. 选择 “测试”，启动具有视频示例的 Firefox。
验证：
 是否使用了插件播放该视频？ 目的：
 此测试将检查 Firefox 是否能够呈现基本网页。
步骤：
 1. 选择 “测试”，启动 Firefox，然后查看测试网页。
验证：
 是否正确加载了 Ubuntu 测试页？ 目的：
 此测试将检查 Firefox 是否能够在网页中运行 java 小程序。注意：
 这可能需要安装额外软件才能成功完成。
步骤：
 1. 选择 “测试”，打开具有 Java 测试页的 Firefox，然后按照其中的说明进行操作。
验证：
 是否显示了该小程序？ 目的：
 此测试将检查 Firefox 是否能够运行 Flash 应用程序。注意：这可能
 需要安装额外软件才能成功完成。
步骤：
 1. 选择 “测试“，启动 Firefox，然后查看 Flash 测试示例。
验证：
 您是否看到了该文本？ 目的：
 此测试将检查 Gnome 终端是否正常工作。
步骤：
 1. 单击 “测试” 按钮，打开终端。
 2. 键入 “ls”，然后按 Enter。您应看到您的主目录下文件和文件夹的列表。
 3. 关闭终端窗口。
验证：
 这是否正常执行？ 目的：
 此测试将检查文件浏览器是否能够复制文件。
步骤：
 1. 单击 “测试”，打开文件浏览器。
 2. 右键单击名为 Test File 1 的文件，然后单击 “复制”。
 3. 右键单击白色空间，然后单击 “粘贴”。
 4. 右键单击名为 Test File 1(复件)的文件，然后单击 “重命名”。
 5. 在名称框中输入名称 Test File 2，然后按回车。
 6. 关闭文件浏览器。
验证：
 您现在是否有了一个名为 Test File 2 的文件？ 目的：
 此测试将检查文件浏览器是否能够复制文件夹
步骤：
 1. 单击 “测试”，打开文件浏览器。
 2. 右键单击名为 Test Folder 的文件夹，然后单击 “复制”。
 3. 右键单击窗口中的任何白色区域，然后单击 “粘贴”。
 4. 右键单击名为 Test Folder(复件)的文件夹，然后单击 “重命名”。
 5. 在名称框中输入名称 Test Data，然后按 Enter。
 6. 关闭文件浏览器。
验证：
 您现在是否有了一个名为 Test Data 的文件夹？ 目的：
 此测试将检查文件浏览器是否能够创建新文件。
步骤：
 1. 单击 “选择测试”，打开文件浏览器。
 2. 右键单击白色空间，然后单击 “创建文档” -> “空文档”。
 3. 在名称框中输入名称 Test File 1，然后按 Enter。
 4. 关闭文件浏览器。
验证：
 您现在是否有了一个名为 Test File 1 的文件？ 目的：
 此测试将检查文件浏览器是否能够创建新文件夹。
步骤：
 1. 单击 “测试”，打开文件浏览器。
 2. 在菜单栏上，单击 “文件” -> “创建文件夹”。
 3. 在新文件夹的名称框中，输入 Test Folder，然后按 Enter。
 4. 关闭文件浏览器。
验证：
 您现在是否有了一个名为 Test Folder 的新文件夹？ 目的：
 此测试将检查文件浏览器是否能够删除文件。
步骤：
 1. 单击 “测试”，打开文件浏览器。
 2. 右键单击名为 Test File 1 的文件，然后单击 "移动到回收站"。
 3. 验证 Test File 1 是否已删除。
 4. 关闭文件浏览器。
验证：
  Test File 1 现在是否已消失？ 目的：
 此测试将检查文件浏览器是否能够删除文件夹。
步骤：
 1. 单击 “测试”，打开文件浏览器。
 2. 右键单击名为 Test Folder 的文件夹，然后单击 “移动到回收站”。
 3. 验证该文件夹是否已删除。
 4. 关闭文件浏览器。
验证：
 是否已成功删除了 Test Folder？ 目的：
 此测试将检查文件浏览器是否能够移动文件。
步骤：
 1. 单击 “测试”，打开文件浏览器。
 2. 单击名为 Test File 2 的文件，然后将其拖到名为 Test Data 的文件夹的图标上。
 3. 释放按钮。
 4. 双击 Test Data 的图标，打开该文件夹。
 5. 关闭文件浏览器。
验证：
 是否将 Test File 2 文件成功移动到了 Test Data 文件夹中？ 目的：
 此测试将检查更新管理器是否能够找到更新。
步骤：
 1. 单击 “测试”，启动更新管理器。
 2. 按照提示操作，如果找到更新，则安装它们。
 3. 当更新管理器已完成时，请单击右下角中的 “关闭” 按钮关闭该应用程序。
验证：
 更新管理器是否找到并安装了更新(如果未找到更新，则此测试通过，
 但如果找到了更新但没有安装，则此测试失败) 目的：
此测试将检查您系统关机和重启的能力。
步骤：
1. 选择 “测试” 开始。
2. 您的机器将会关闭。
3. 重启机器。
4. 重启后，等待测试弹窗提醒您测试完毕。
5. 测试完成时，重启 checkbox 并弹窗时，选择 “重新运行”
验证：
如果机器成功关闭并重启，选择 “是”，反之，选择 “否”。 目的：
此测试将检查您系统干净重启的能力。
步骤：
1. 选择 “测试” 开始。
2. 您的机器将会重启。
3. 重启后，等待测试弹窗提醒您测试完毕。
4. 测试完成时，重启 checkbox 并弹窗时，选择 “重新运行”
验证：
如果机器成功重启，选择 “是”，然后选择 “下一步”。 目的：
 此测试将验证文件浏览器是否能够移动文件夹。
步骤：
 1. 单击 “测试”，打开文件浏览器。
 2. 单击名为 Test Data 的文件夹，然后将其拖到名为 Test Folder 的图标上。
 3. 释放按钮。
 4. 双击名为 Test Folder 的文件夹，将其打开。
 5. 关闭文件浏览器。
验证：
 是否将名为 Test Data 的文件夹成功移动到了名为 Test Folder 的文件夹中？ 目的：
 发现东西
步骤：
 1. 点击 “是”
验证：
 不是必须的，这是个虚假的测试。 面板时钟验证测试 面板重启验证测试 解析 Xorg.0.Log 并发现正在运行的 X 驱动和版本 外围设备测试 Piglit 测试 ping ubuntu.com 并重启网络接口100次 通过默认输出设备回放一段音频并使用默认输入设备试听。 请选择 (%s)：  请按下您的键盘上的每一个按键。 请在这里输入并在结束时按下 Ctrl-D ：
 指点设备测试 电量管理测试 电源管理测试 按下任何一个键继续… 上一个 显示版本信息并退出。 提供关于连接到该系统的显示信息 提供关于网络设备的信息 从键盘退出 挂起前记录混音器设置。 挂起前记录当前网络。 挂起前记录当前分辨率。 Rendercheck 测试 重新运行 重新启动 返回此系统可用的任一触控板的名称、驱动名称以及驱动版本。 运行 Cachebench 读取/变更/写入基准测试 运行 Cachebench 读取基准测试 运行 Cachebench 写入基准测试 运行 7ZIP 压缩软件基准测试 运行 PBZIP2 压缩软件基准测试 运行 MP3 编码基准测试 运行固件测试用例(Firmware Test Suite, fwts)自动测试。 运行 GLmark2 基准测试 运行 GLmark2-ES2 基准测试 运行 GiMark，一款几何体实例化测试程序(OpenGL 3.3) 全屏 1920x1080 无反锯齿 运行 GiMark，一款几何体实例化测试程序(OpenGL 2.3) 窗口 1024x640 无反锯齿 运行 GnuPG 基准测试 运行 Himeno 基准测试 运行 Lightsmark 基准测试 运行 N-Queens 基准测试 运行网络回路基准测试 运行 Qgears2 OpenGL gearsfancy 基准测试 运行 Qgears2 OpenGL 图像缩放基准测试 运行 Qgears2 XRender Extension gearsfancy 基准测试 运行 Qgears2 XRender Extension 图像缩放基准测试 运行 Render-Bench XRender/Imlib2 基准测试 运行 Stream Add 基准测试 运行 Stream Copy 基准测试 运行 Stream Scale 基准测试 运行 Stream Triad 基准测试 运行 Unigine Heaven 基准测试 运行 Unigine Santuary 基准测试 运行 Unigine Tropics 基准测试 运行一个压力测试程序，基于 FurMark (OpenGL 2.1 或 3.2) 全屏 1920x1080 无反锯齿 运行一个压力测试程序，基于 FurMark (OpenGL 2.1 或 3.2) 窗口 1024x640 无反锯齿 运行一个细分曲面测试程序，基于 TessMark (OpenGL 4.0) 全屏 1920x1080 无反锯齿 运行一个细分曲面测试程序，基于 TessMark (OpenGL 4.0) 窗口 1024x640 无反锯齿 运行 globs 基准测试 运行 gtkperf 以确保基于 GTK 的测试用例正常工作 运行图像压力测试。测试会花费几分钟。 运行 x264 H.264/AVC 编码器基准测试 正在运行 %s... 运行一个传输3次100个 10MB 大小的文件至 SDHC 卡的测试。 运行一个传输3次100个 10MB 大小的文件至 usb 的测试。 运行 rendercheck 测试套件中所有的测试。此测试将会花费几分钟。 运行 piglit 测试以检查系统是否支持 OpenGL 2.1 运行 piglit 测试以检查系统是否支持 GLSL 片段着色器操作 运行 piglit 测试以检查系统是否支持 GLSL 顶点着色器操作 运行 piglit 测试以检查系统是否支持帧缓冲区对象操作、深度缓冲以及模板缓冲 运行 piglit 测试以检查系统是否支持使用 pixmap 实现纹理 运行 piglit 测试以检查系统是否支持顶点缓存区对象操作 运行 piglit 测试以检查系统是否支持模板缓冲操作 运行 piglit 结果总结工具 SATA/IDE 设备信息。 SMART 测试 系统测试：请输入您的密码。某些测试需要 root 用户权限正常运行测试。您的密码将永不会被保存或与测试结果一起被提交。 全部选中 全部选中 服务器服务检查 --config=.*/jobs_info/blacklist 的简写。 --config=.*/jobs_info/blacklist_file 的简写。 --config=.*/jobs_info/whitelist 的简写。 --config=.*/jobs_info/whitelist_file 的简写。 跳过 冒烟测试 发现探测器 软件安装测试 某些新的硬盘存在一个特性：硬盘短期不活动就会减速。这是一个省电特性，但是硬盘持续减速然后被激活时，会造成与操作系统一个不好的相互作用。此过程会导致驱动器过度磨损，可能导致过早出现故障。 完成后的空间大小 开始测试 状态 停止进程 在 tty 上输入了 Stop 压力关闭系统(100次循环) 压力重启系统(100次循环) 压力测试 提交的详细信息 提交结果 提交至 HEXR 成功完成测试！ 休眠测试 挂起测试 系统 
测试 系统守护进程测试 系统测试 标签一 标签二 终止信号 测试 测试 ACPI Wakealarm (fwts wakealarm) 重新测试 测试和使用内存。 被取消的测试 测试是否出现时钟抖动。 测试在安装软件包后，atd 守护程序是否运行。 测试在安装软件包后，cron 守护程序是否运行。 测试在安装软件包后，cupsd 守护程序是否运行。 测试在安装软件包后，getty 守护程序是否运行。 测试在安装软件包后，init 守护程序是否运行。 测试在安装软件包后，klogd 守护程序是否运行。 测试在安装软件包后，nmbd 守护程序是否运行。 测试在安装软件包后，smbd 守护程序是否运行。 测试在安装软件包后，syslogd 守护程序是否运行。 测试在安装软件包后，udevd 守护程序是否运行。 测试在安装软件包后，winbindd 守护程序是否运行。 测试中断 测试在多核系统上屏蔽 CPU。 测试 /var/crash 目录是否没有包含任何内容。列出其中包含的文件(如果有)，或者回显该目录的状态(不存在或为空) 测试 X 是否没有在故障安全(failsafe)模式下运行。 测试 X 进程是否正在运行。 使用 固件测试用例(Firmware Test Suite, fwts cpufreq) 测试 CPU 的扩展能力。 恢复后测试网络。 测试检查 Xen domU 镜像能够在 Ubuntu 引导和运行 Xen 测试检查使用 KVM，云镜像是否能够正常引导和工作 测试检查测试系统是否支持虚拟化，以及该内存是否满足作为 OpenStack 计算节点所要求的最低内存 检测音频设备的测试 检测可用网络控制器的测试 检测光驱的测试 测试确定系统是否能够运行 KVM 虚拟机的硬件加速 输出 Xorg 版本的测试 测试能否将本地时钟与 NTP 服务器同步 测试恢复后分辨率是否与之前相同。 测试确认 Xen Hypervisor 正在运行。 测试您的系统并提交结果至 Launchpad 已测试 测试使用 Xpresser 的 oem-conifg，并检查用户是否被成功创建。测试通过后，清除新创建的用户。 测试系统挂起后，无线硬件是否能够连接到使用 WPA 安全和 802.11b/g 协议的路由器。 测试系统无线硬件是否能够连接到使用 WPA 安全和 802.11b/g 协议的路由器。 测试系统挂起后，无线硬件是否能够连接到使用 WPA 安全和 802.11n 协议的路由器。 测试系统无线硬件是否能够连接到使用 WPA 安全和 802.11n 协议的路由器。 测试系统挂起后，无线硬件是否能够连接到不使用安全的 802.11b/g 协议的路由器。 测试系统无线硬件是否能够连接到不使用安全和 802.11b/g 协议的路由器。 测试系统挂起后，无线硬件是否能够连接到不使用安全的 802.11n 协议的路由器。 测试系统无线硬件是否能够连接到不使用安全和 802.11n 协议的路由器。 通过 iperf 工具，使用 UDP 数据包测试系统无线连接的性能。 通过 iperf 工具测试系统无线连接的性能。 测试 apt 能否访问软件仓库并获得更新(不安装更新)。这样做是确认您能够从不完整或受损的更新中恢复。 测试该系统是否具有正在工作的互联网连接。 文本标签 写入到日志的文件。 以下报告已生成，并将被提交至 Launchpad 硬件数据库：

  [[%s|查看报告]]

您可以通过提供您注册 Launchpad 所使用的电子邮件地址来提交有关您系统的这些信息。如果您没有 Launchpad 帐号，请在这里注册：

  https://launchpad.net/+login 生成的报告似乎具有确认错误，
因此 Launchpad 可能不会处理该报告。 有另一个 Checkbox 正在运行。请先关闭它。 此自动测试尝试检测摄像头。 附加 suspend/cycle_resolutions_after_suspend_auto 测试中捕获的屏幕截图到提交的结果。 此测试为 mediacard/sd-automated 的全自动化版本，且假设在 checkbox 执行之前就有记忆卡设备插入系统。供 SRU 自动测试使用。 这是一个自动蓝牙文件传输测试。此测试将向 BTDEVADDR 环境变量指定的设备发送一个图像。 这是一个自动蓝牙测试。此测试模拟在 BTDEVADDR 环境变量指定的远程设备上浏览。 这是一个自动蓝牙测试。此测试接收由 BTDEVADDR 环境变量指定的远程设备发送的文件。 这是一个收集有关您网络设备当前状态的一些信息的自动测试。如果未发现设备，此测试将退出并报错。 这是一个在已连接的 FireWire HDD 上执行读/写操作的自动测试 这是一个在已连接的 eSATA HDD 上执行读/写操作的自动测试 此测试为 usb/storage-automated 的自动化版本，且假设在 checkbox 执行之前就有 USB 储存设备插入服务器。供服务器和 SRU 自动测试使用。 此测试为 usb3/storage-automated 的自动化版本，且假设在 checkbox 执行之前就有 USB 3.0 储存设备插入服务器。供服务器和 SRU 自动测试使用。 这是 suspend/suspend_advanced 的自动化版本。 本测试将检测 CPU 的拓扑结构精度。 此测试检查在设置了 CPU 频率管理器时，其是否得到遵守。 此测试将检查在该系统挂起后无线接口是否正常工作。此测试将断开所有接口的连接，然后连接到无线接口，并且检查此连接是否正常工作。 此测试检查对比由 DMI 检测的内存模块的大小和 meminfo 中报告的内存容量。 此测试将断开所有连接，然后连接到无线接口。然后，此测试将检查该连接，以确认其正常工作。 本测试将获取休眠之后的蓝牙适配器地址，并于休眠之前进行对比。 此测试自动进行，并且在 mediacard/cf-insert 测试运行后执行。其测试对 CF 卡的读写。 此测试自动进行，并且在 mediacard/cf-insert-after-suspend 测试运行后执行。其测试在系统已挂起后对 CF 卡的读写。 此测试自动进行，并且在 mediacard/mmc-insert 测试运行后执行。其测试对 MMC 卡的读写。 此测试自动进行，并且在 mediacard/mmc-insert-after-suspend 测试运行后执行。其测试在系统已挂起后对 MMC 卡的读写。 此测试自动进行，并且在 mediacard/ms-insert 测试运行后执行。其测试对 MS 卡的读写。 此测试自动进行，并且在 mediacard/ms-insert-after-suspend 测试运行后执行。其测试在系统已挂起后对 MS 卡的读写。 此测试自动进行，并且在 mediacard/msp-insert 测试运行后执行。其测试对 MSP 卡的读写。 此测试自动进行，并且在 mediacard/msp-insert-after-suspend 测试运行后执行。其测试在系统已挂起后对 MSP 卡的读写。 此测试自动进行，并且在 mediacard/sd-insert 测试运行后执行。其测试对 SD 卡的读写。 此测试自动进行，并且在 mediacard/sd-insert-after-suspend 测试运行后执行。其测试在系统已挂起后对 SD 卡的读写。 此测试自动进行，并且在 mediacard/sdhc-insert 测试运行后执行。其测试对 SDHC 卡的读写。 此测试自动进行，并且在 mediacard/sdhc-insert-after-suspend 测试运行后执行。其测试在系统已挂起后对 SDHC 卡的读写。 此测试自动进行，并且在 mediacard/sdxc-insert test 测试运行后执行。其测试对 SDXC 卡的读写。 此测试自动进行，并且在 mediacard/sdxc-insert-after-suspend 测试运行后执行。其测试在系统已挂起后对 SDXC 卡的读写。 此测试自动进行，并且在 mediacard/xd-insert 测试运行后执行。其测试对 xD 卡的读写。 此测试自动进行，并且在 mediacard/xd-insert-after-suspend 测试运行后执行。其测试在系统已挂起后对 xD 卡的读写。 此测试自动进行，并且在 suspend/usb3_insert_after_suspend 测试运行后执行。 此测试自动进行，并且在 suspend/usb_insert_after_suspend 测试运行后执行。 此测试自动进行，并且在 usb/insert 测试运行后执行。 此测试自动进行，并且在 usb3/insert 测试运行后执行。 此测试将检查并确保在挂起和恢复后支持的显示模式正常工作。这是通过捕获屏幕截图并将它们作为附件进行上传来自动进行的。 此测试将验证音量在您的本地系统中处于一个合理水平，证实针对被脉冲音频认可的所有信源(输入)和信宿(输出)，音量都介于最小音量和最大音量之间。它也将验证活跃的信源和信宿没有被静音。运行测试前，请勿手动调节音量或静音。 附加电源管理/关机测试中的任何日志到测试结果 附加电源管理/重启测试中的任何日志到测试结果 这将检查并确保在挂起和恢复后您的音频设备正常工作。此测试可以使用扬声器和内置麦克风，但最好使用将音频输出插孔与音频输入插孔相连的电缆。 本测试将检测 BMC 的联通性，以确认 IPMI 是否能够正常运行。 来自 alarm(2) 的时间信号 请关闭 checkbox 并添加缺失的依赖至白名单以修复这个问题。 触控板测试 触屏测试 尝试在网络上启用远程打印机，并打印测试页。 键入文本 未知 USB 接口测试 USB 测试 未知信号 未测试 用法： checkbox [选项] 用户应用程序 用户定义信号 1 用户定义信号 2 验证矢量浮点单元正在 ARM 设备上运行 验证 DNS 服务器是否正在运行和工作。 验证打印/CUPS 服务器是否正在运行。 验证 Samba 服务器是否正在运行。 验证 Tomcat 服务器是否正在运行和工作。 验证 sshd 是否正在运行。 验证 LAMP 栈是否正在运行(Apache、MySQL 和 PHP)。 确认 USB3 外部存储器的表现是否达到或高于基线性能 验证系统存储的表现是否达到或高于基线性能 确认恢复之后所有 CPU 都处于运行状态。 验证在从挂起状态中恢复后所有内存是否均可用。 确认电脑休眠之前所有 CPU 都处于运行状态。 验证是否可以通过 SSH 达到在网络上安装的 checkbox-server。 验证在挂起之后混合器设置是否与挂起之前相同。 验证是否能够检测到光纤通道和 RAID 等存储设备，以及这些存储设备是否能够在压力下使用。 查看结果 虚拟化测试 欢迎使用系统测试程序！

Checkbox 提供一系列测试来验证您的系统工作正常。当您完成这些测试后，可以查看针对系统的总结报告。 未被拓扑安排的白名单 无线测试 无线联网测试 无线网络扫描测试。将自动扫描和报告可用的无线网络。 正在工作 您也可通过按 ESC 或 Ctrl+C 关闭测试。 取消全部选中(_D) 退出(_E) 完成(_F) 否(_N) 上一步(_P) 全部选中(_S) 跳过这个测试(_S) 测试(_T) 再次测试(_T) 是(_Y) 以及性能。 附加各个 sysctl 配置文件的内容。 eSATA 磁盘测试 报告时间以及性能。 https://help.ubuntu.com/community/Installation/SystemRequirements 安装启动流程检测程序(如果存在)。 否 跳过 测试 再次测试 后台进程的 tty 输入 后台进程的 tty 输出 报告时间以及性能。需要设置 MOVIE_VAR。 是 