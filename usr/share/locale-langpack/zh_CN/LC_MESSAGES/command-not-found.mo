��          �   %   �      p  $   q     �     �  -   �  1   �  :   +      f     �  4   �  $   �  9   �     2  B   Q  _   �  8   �  -   -  f   [  W   �          5  -   S  1   �     �  /   �  8     #   :     ^  �  `  #   	     4	     L	  '   a	  1   �	  -   �	     �	     
  -   
  3   ?
  $   s
     �
  H   �
  A      -   B     p  E   �  M   �  #   #  $   G     l     �     �  0   �  3   �  !   !     C                                                 
                                                                      	     Command '%s' from package '%s' (%s) %prog [options] <command-name> %s: command not found Ask your administrator to install one of them Command '%(command)s' is available in '%(place)s' Command '%(command)s' is available in the following places Do you want to install it? (N/y) Exception information: No command '%s' found, but there are %s similar ones No command '%s' found, did you mean: Please include the following information with the report: Python version: %d.%d.%d %s %d Sorry, command-not-found has crashed! Please file a bug report at: The command could not be located because '%s' is not included in the PATH environment variable. The program '%s' can be found in the following packages: The program '%s' is currently not installed.  This is most likely caused by the lack of administrative privileges associated with your user account. To run '%(command)s' please ask your administrator to install the package '%(package)s' Try: %s <selected package> You can install it by typing: You will have to enable component called '%s' You will have to enable the component called '%s' command-not-found version: %s don't print '<command-name>: command not found' ignore local binaries and display the available packages use this path to locate data fields y Project-Id-Version: command-not-found
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-06-20 16:54+0000
PO-Revision-Date: 2009-10-26 08:37+0000
Last-Translator: Kyle WANG <waxaca@163.com>
Language-Team: Chinese (China) <zh_CN@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:59+0000
X-Generator: Launchpad (build 18115)
  命令 '%s' 来自于包 '%s' (%s) %prog [选项] <命令> %s：未找到命令 请要求管理员安装其中的一个 命令 '%(command)s' 可在 '%(place)s' 处找到 命令 '%(command)s' 可在以下位置找到 您要安装吗？(N/y) 额外信息： 未找到 '%s' 命令，有%s 个相似命令 未找到 '%s' 命令，您要输入的是否是： 请在报告中包含以下信息： Python 版本：%d.%d.%d %s %d 抱歉，command-not-found 崩溃了！请在以下地址报告错误： 由于%s 不在PATH 环境变量中，故无法找到该命令。 程序 '%s' 已包含在下列软件包中： 程序“%s”尚未安装。  这很可能是由您的用户账户没有管理员权限造成的。 如需运行 '%(command)s'，请要求管理员安装 '%(package)s' 软件包 请尝试：%s <选定的软件包> 您可以使用以下命令安装： 您必须启用%s 组件 您必须启用%s 组件 command-not-found 版本：%s 不输出 “<command-name>：未找到命令” 忽略本地二进制文件并显示可用软件包 使用该路径以定位数据域 是 