��          �   %   �      P      Q     r     �  #   �  =   �  @   �     5     B     O     \     l  '   �     �  #   �  1   �  2        A  F   P  $   �  "   �  [   �  :   ;  X   v  S   �  '   #  �  K          !     1     >  -   Z  '   �     �     �     �     �     �     �     	     	  '   9	  3   a	     �	  C   �	     �	     
  B   
  %   b
  6   �
  :   �
  +   �
                                                                                                    	               
           - Edit Web credentials settings Add account… All applications Another instance is already running Are you sure that you wish to remove this Ubuntu Web Account? Control whether this application integrates with Online Accounts Edit Options Grant access Legal notice Online Accounts Online Accounts preferences Online account credentials and settings Options Output version information and exit Please authorize Ubuntu to access your %s account Please authorize Ubuntu to access your %s account: Remove Account Run '%s --help' to see a full list of available command line options.
 Select to configure a new %s account Show accounts that integrate with: The Web Account which manages the integration of %s with your applications will be removed. The following applications integrate with your %s account: There are currently no account providers available which integrate with this application There are currently no applications installed which integrate with your %s account. Your online %s account is not affected. Project-Id-Version: gnome-control-center-signon
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-02-01 12:56+0000
PO-Revision-Date: 2013-05-02 07:30+0000
Last-Translator: Wang Dianjin <Unknown>
Language-Team: Chinese (Simplified) <zh_CN@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:14+0000
X-Generator: Launchpad (build 18115)
  - 编辑 Web 证书设置 添加帐号… 所有程序 另一个实例已在运行 您确定您想移除此 Ubuntu Web 账户？ 设置此软件是否集成在线帐号 编辑选项 授权访问 法律提示 在线帐户 在线账户首选项 在线账户证书与设置 选项 输出版本信息并退出 请授权 Ubuntu 访问您的 %s 帐号 请认证以便于 Ubuntu 访问您的 %s 账户： 移除帐号 运行“%s --help”可查看可用命令行选项的完整列表
 选择以便配置新 %s 账户 显示集成的帐号： 管理你的程序与 %s 的整合的 Web 帐号将会被移除。 以下程序与你的 %s 帐号整合 现在还没有帐号提供者与此应用程序整合 目前没有安装能与您的 %s 账户整合的应用。 你的在线帐号 %s 没有受到影响。 