��    5     �	  �  l      �     �               )     ,     9     J     R  6   q     �  	   �     �     �  Q   �  �   K  *   �       	     &   '     N     W  0   g  �   �  1     �   L     �     �     �       "        7     ?  
   U     `     q     v     �     �     �     �  F   �     �       1   !     S  
   o  '   z     �     �     �  .   �  -         L      a   (   ~      �      �   
   �   -   �      �      !  �   !     �!  %   �!  (   �!      "     ="     B"  !   T"     v"     �"     �"     �"     �"     �"     �"  	   �"  %   #     .#     J#     Q#     b#     q#     ~#     �#     �#     �#     �#     �#     �#  $   �#  #   $  #   %$  '   I$     q$  
   y$     �$     �$    �$  �   �%    T&  z   Z'  �   �'  \   �(  1   �(  -   )     G)  
   M)     X)     k)     |)     �)  
   �)     �)  X   �)  
   *  U   *     u*     �*     �*  	   �*     �*  �   �*  	   ^+     h+  
   �+     �+     �+     �+     �+     �+     �+     �+     �+     �+     ,     ,     ,  
   (,  
   3,  
   >,  K   I,     �,     �,     �,     �,     �,     �,  
    -     -     )-  8   E-     ~-     �-     �-     �-     �-     �-     �-  
   �-     �-     �-     �-     �-     .  '   !.  (   I.  E   r.  $   �.  
   �.     �.  
   �.     �.     /  *   /     ;/     U/     m/     /     �/     �/     �/     �/  )   �/  <   0  0   J0  !   {0     �0     �0  ,   �0  #   
1     .1  
   ?1     J1  
   O1  
   Z1  	   e1     o1     �1  z   �1     '2  7   -2     e2     {2     2     �2  �   �2  �   �3     ^4  �   v4  &   5  Y   =5  [   �5  2   �5  <   &6  �   c6  �   `7  �   �7  �   �8     9     9     9  !   19     S9     f9     }9     �9     �9     �9     �9     �9     �9     �9     �9     �9  X   :  3   ]:  �   �:  �   ;  L   �;  L   �;  4   :<  B   o<     �<     �<     �<  	   �<     �<  ;   �<     '=  	   0=     :=  	   J=     T=     [=  	   r=     |=     �=     �=  	   �=     �=     �=     �=     �=     �=     �=     �=     �=     �=     >     >     >     >     '>     3>     E>     L>     [>     a>     m>     z>     �>     �>     �>  	   �>     �>  	   �>     �>     �>      �>     ?     ?      ?     &?  	   2?     <?  
   O?     Z?     p?     v?     |?     �?     �?     �?     �?     �?     �?     �?  !  �?     �A     �A     B     #B     &B     8B     KB     cB     sB     �B  	   �B     �B     �B  S   �B  }   "C     �C     �C     �C     �C     �C     �C  0   D  >   4D  *   sD  8   �D     �D     �D     �D     E     /E  	   NE     XE     kE     rE     E     �E     �E  
   �E     �E     �E  0   �E     �E     F  3   #F     WF  	   mF  $   wF     �F     �F     �F  $   �F  $   G     ;G     NG     gG     �G     �G     �G  $   �G  	   �G     �G  �   �G     �H  "   �H  #   �H  "   �H     I     I     I     =I     TI     mI     tI     �I     �I     �I     �I     �I     �I     �I     J     J     (J  	   /J  	   9J     CJ     PJ     cJ  
   yJ     �J     �J  !   �J     �J     �J  	   
K     K     K     K  �   0K  �   L  �   �L  W   �M  �   $N  K   �N  3   O  -   ;O     iO     pO     }O     �O     �O     �O     �O     �O  K   �O      P  i   -P     �P     �P     �P     �P     �P  y   �P  	   [Q     eQ     ~Q  
   �Q  	   �Q     �Q     �Q     �Q     �Q  	   �Q     �Q     �Q     R     R     R     %R     2R     CR  `   TR     �R  !   �R     �R     S     (S     /S     6S     CS  3   bS  5   �S     �S     �S     �S  	   �S     �S  	   �S  
   T     T     T     T     /T     HT     XT     lT     �T  E   �T  "   �T     U  	   #U     -U     :U     HU  /   YU  )   �U     �U     �U     �U     �U     �U     V     1V  $   JV  9   oV  .   �V  "   �V     �V     W  '   ;W     cW     W     �W     �W     �W     �W     �W     �W     �W  c   	X     mX  -   qX     �X     �X     �X     �X  �   �X  �   �Y     bZ  �   {Z  !   �Z  Q    [  E   r[     �[  *   �[  �   \  L   �\  �   ]  |   �]     ^     ^     ^     *^     F^     Y^     l^  '   y^     �^  	   �^     �^     �^     �^     �^     �^     _  W   _  $   ^_  f   �_  f   �_  3   Q`  ?   �`  '   �`  *   �`  	   a     "a     +a  
   4a     ?a  *   Pa  
   {a  
   �a     �a     �a  
   �a     �a     �a  
   �a  
   �a  
   �a  
   �a  
   b     b  
   b     *b  
   Ab     Lb     cb  
   wb  
   �b     �b  
   �b  
   �b     �b     �b     �b  
   �b  
   �b      c     c     "c  
   3c     >c     Xc     fc     tc     �c     �c  
   �c     �c     �c  
   �c     �c  
   �c     
d     d     )d     @d     Nd  
   kd  
   vd     �d  
   �d     �d     �d  
   �d     �d     �d  �  �d     �   5          U   T       �   4  �       `   f   �   �      �   �          �   z   �   �   �   �     �   {     �   �       �   x   n   G          �       �   �             /  @   �              0  a       #      �           q              �   �     �              �   �                   H   �   �   �   �   
  ;                �           -  �   �       
       �   �          Z       �   �   �       ,         *  E   �   8      �       >       *             �   $  �   A             ?   �       ,  V   �   e   )   o   c   �              �   �   &      w           �     3      &   �           �        �         3   _   �   �   +  �   :         +          �   }   �      '               �          D   �   S          "      	                �      �   �       F   �   �   �   )  O   2  m   �   �   �   5   ^   �                         �         �   1   =       0   �   4     �   2   �       �           r           �   d   6   v     �   �       �   �   �      B   C   N           �       �   P       !  K   �   �   �       �   �           �           9   %  g   �   $   �   u   �   s   i      Q          p   (   �       "           �       .   X   !       #   	   �       �   \   �   M          %       �   <       �   �                 �   �   �   /   �   �         I   �   j   �   �   �       h      �   �       7     �   k   Y     �   �   ]       t       ~       �   W   �   �   �   �   L           .  �   -     �   �       �   �       �   J           y   (       �      [           l         b          �   '        R       1  �                �   |     (invalid Unicode) %.1f (35mm film) %.1f (lens) %X %a, %d %B %Y %a, %d %B %Y  %X %d / %d %i × %i pixel %i × %i pixels %i × %i pixel  %s    %i%% %i × %i pixels  %s    %i%% %lu second %lu seconds %s (*.%s) <b>%f:</b> original filename <b>%n:</b> counter A trash for "%s" couldn't be found. Do you want to remove this image permanently? A value greater than 0 determines the seconds an image stays on screen until the next one is shown automatically. Zero disables the automatic browsing. Activate fullscreen mode with double-click Active plugins All files Allow zoom greater than 100% initially Aperture Aperture Value: Are you sure you want to move
"%s" to the trash? Are you sure you want to move
the selected image to the trash? Are you sure you want to move
the %d selected images to the trash? Are you sure you want to remove
"%s" permanently? Are you sure you want to remove
the selected image permanently? Are you sure you want to remove
the %d selected images permanently? As _background As check _pattern As custom c_olor: As custom color: At least two file names are equal. Author: Automatic orientation Background Background Color Both Browse and rotate images Bytes: C_enter: Camera Camera Model: Changes the visibility of the image gallery pane in the current window Choose a folder Close _without Saving Close main window without asking to save changes. Color for Transparent Areas Copyright: Could not display help for Image Viewer Could not load image '%s'. Could not save image '%s'. Couldn't access trash. Couldn't allocate memory for loading JPEG file Couldn't create temporary file for saving: %s Couldn't delete file Couldn't retrieve image file Couldn't retrieve image file information Date Date in statusbar Date/Time: Delay in seconds until showing the next image Description: Details Determines how transparency should be indicated. Valid values are CHECK_PATTERN, COLOR and NONE. If COLOR is chosen, then the trans-color key determines the color value used. Disable image gallery Do _not ask again during this session EXIF not supported for this file format. E_xpand images to fit screen East Enlarge the image Error launching System Settings:  Error on deleting image %s Error printing file:
%s Exposure Exposure Time: Extrapolate Image File Name Preview File Path Specifications File Size File format is unknown or unsupported Fit the image to the window Flash: Flip _Horizontal Flip _Vertical Focal Length Focal Length: Folder Folder: Fullscreen with double-click GNOME Image Viewer GPS Data General Go to the first image of the gallery Go to the last image of the gallery Go to the next image of the gallery Go to the previous image of the gallery Height: Horizontal ISO ISO Speed Rating: If activated and no image is loaded in the active window, the file chooser will display the user's pictures folder using the XDG special user directories. If deactivated or the pictures folder has not been set up, it will show the current working directory. If activated, Eye of GNOME won't ask for confirmation when moving images to the trash. It will still ask if any of the files cannot be moved to the trash and would be deleted instead. If activated, the detailed metadata list in the properties dialog will be moved to its own page in the dialog. This should make the dialog more usable on smaller screens, e.g. as used by netbooks. If disabled, the widget will be embedded on the "Metadata" page. If the transparency key has the value COLOR, then this key determines the color which is used for indicating transparency. If this is active, the color set by the background-color key will be used to fill the area behind the image. If it is not set, the current GTK+ theme will determine the fill color. If this is set to FALSE small images will not be stretched to fit into the screen initially. If you don't save, all your changes will be lost. If you don't save, your changes will be lost. Image Image Data Image Enhancements Image Properties Image Settings Image Taking Conditions Image View Image Viewer Image Viewer could not determine a supported writable file format based on the filename. Image Zoom Image gallery pane position. Set to 0 for bottom; 1 for left; 2 for top; 3 for right. Image loading failed. Inches Interpolate Image Keywords: Leave fullscreen mode List of active plugins. It doesn't contain the "Location" of the active plugins. See the .eog-plugin file for obtaining the "Location" of a given plugin. Location: Loop through the image sequence Maker Note MessageAreaHi_de Metadata Metering Metering Mode: Millimeters Move to _Trash Name: No image loaded. No images found in '%s'. None North Open Folder Open Image Open _With Open _with Open in a single window, if multiple windows are open the first one is used Open in fullscreen mode Open in slideshow mode Open with _Document Viewer Opening image "%s" Options Other Page Setup Pause or resume the slideshow Picture;Slideshow;Graphics; Please try a different file extension like .png or .jpg. Plugins Position Prefere_nces Preferences Preview Print… Prope_rties Properties Question Reload Image Reload current image Rename from: Rotate Counter_clockwise Rotate the image 90 degrees to the left Rotate the image 90 degrees to the right Run '%s --help' to see a full list of available command line options. S_elect the images you want to save: S_ide Pane Save As Save Image Save _As Save _As… Save changes to image "%s" before closing? Saving image "%s" (%u/%u) Saving image locally… Scroll wheel zoom Sequence Set as Wa_llpaper Set as _Wallpaper Show Containing _Folder Show the application's version Show the current image in fullscreen mode Show the folder which contains this file in the file manager Show/Hide the image gallery pane scroll buttons. Show/Hide the image gallery pane. Show/Hide the window side pane. Show/Hide the window statusbar. Shows the image date in the window statusbar Shrink or enlarge the current image Shrink the image Side _Pane Size Sli_deshow Slide Show Slideshow Smooth images when zoomed _in Smooth images when zoomed _out Some of the selected images can't be moved to the trash and will be removed permanently. Are you sure you want to proceed? South Start a new instance instead of reusing an existing one Supported image files Tag Taken on Temporary file creation failed. The Eye of GNOME also allows to view the images in a fullscreen slideshow mode or set an image as the desktop wallpaper. It reads the camera tags to automatically rotate your images in the correct portrait or landscape orientation. The Eye of GNOME is the official image viewer for the GNOME desktop. It integrates with the GTK+ look and feel of GNOME, and supports many image formats for viewing single images or images in a collection. The GNOME image viewer. The color that is used to fill the area behind the image. If the use-background-color key is not set, the color is determined by the active GTK+ theme instead. The given locations contain no images. The image "%s" has been modified by an external application.
Would you like to reload it? The image "%s" has been set as Desktop Background.
Would you like to modify its appearance? The image whose printing properties will be set up The information for the page where the image will be printed The multiplier to be applied when using the mouse scroll wheel for zooming. This value defines the zooming step used for each scroll event. For example, 0.05 results in a 5% zoom increment for each scroll event and 1.00 result in a 100% zoom increment. There is %d image with unsaved changes. Save changes before closing? There are %d images with unsaved changes. Save changes before closing? This image contains multiple pages. Image Viewer displays only the first page.
Do you want to open the image with the Document Viewer to see all pages? This image contains multiple pages. Image Viewer displays only the first page.
You may want to install the Document Viewer to see all pages. Time To: Transformation failed. Transformation on unloaded image. Transparency color Transparency indicator Transparent Parts Trash images without asking Type Type: Unknown Use a custom background color Value Vertical Viewing a slideshow West Whether the file chooser should show the user's pictures folder if no images are loaded. Whether the image gallery pane should be resizable. Whether the image should be extrapolated on zoom-in. This leads to blurry quality and is somewhat slower than non-extrapolated images. Whether the image should be interpolated on zoom-out. This leads to better quality but is somewhat slower than non-interpolated images. Whether the image should be rotated automatically based on EXIF orientation. Whether the metadata list in the properties dialog should have its own page. Whether the scroll wheel should be used for zooming. Whether the sequence of images should be shown in an endless loop. Width: XMP Exif XMP IPTC XMP Other XMP Rights Management You do not have the permissions necessary to save the file. Zoom _In Zoom _Out Zoom multiplier [FILE…] _About _Automatic orientation _Best Fit _Bottom: _Cancel _Close _Contents _Copy _Copy Image _Delete _Destination folder: _Edit _Filename format: _First Image _Fullscreen _Go _Height: _Help _Image _Image Gallery _Last Image _Leave Fullscreen _Left: _Loop sequence _Next _Next Image _Normal Size _Open _Open Background Preferences _Open… _Preferences _Previous _Previous Image _Print… _Quit _Reload _Replace spaces with underscores _Right: _Rotate Clockwise _Save _Save As… _Scaling: _Start counter at: _Statusbar _Time between images: _Top: _Undo _Unit: _View _Width: _Yes _Zoom In as is pixel pixels translator-credits Project-Id-Version: eog master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=eog&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-03-10 04:21+0000
PO-Revision-Date: 2016-03-24 10:06+0000
Last-Translator: YunQiang Su <wzssyqa@gmail.com>
Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2016-06-27 16:14+0000
X-Generator: Launchpad (build 18115)
  (无效的 Unicode) %.1f (35mm 电影胶片) %.1f (镜头) %X %Y年%m月%d日%A %Y年%m月%d日 %X 第 %d 张，共 %d 张 %i × %i 像素 %i × %i 像素  %s    %i%% %lu 秒 %s (*.%s) <b>%f:</b> 原始文件名 <b>%n:</b> 计数 无法找到可用于“%s”的回收站。您想要将此图像永久移除吗？ 大于 0 的值将决定自动显示下张图像之前当前图像要在屏幕上停留几秒。0 代表禁用自动浏览。 双击启用全屏模式 活动插件 全部文件 允许放大至超过原大 光圈 光圈值： 您真的想要将“%s”
移至回收站吗？ 您真的想要将 %d 个
选中的图像移至回收站吗？ 您要删除“%s”吗？
永久删除！ 您确定要永久删除选中的
这 %d 张图像吗？ 与背景相同(_B) 为十字图案(_P) 为自定义颜色(_O)： 为自定义颜色： 至少两个文件名相同。 作者： 自动调整方向 背景 背景颜色 双向 浏览及旋转图像 字节数： 中(_E)： 相机 相机型号： 更改当前窗口图像收藏面板的可见性 选择文件夹 关闭而不保存(_W) 关闭主窗口而不询问是否要保存修改。 透明区域的颜色 版权： 无法显示图像查看器的帮助 无法载入图像“%s”。 无法保存图像“%s”。 无法访问回收站。 无法分配内存载入 JPEG 文件 无法创建临时文件来保存 %s 无法删除文件 无法检索图像文件 无法获取图像文件信息 日期 在状态栏中显示日期 日期/时间： 显示下张图像前延迟的秒数 描述： 细节 决定如何指示透明区域。可用的值有 CHECK_PATTERN、COLOR 和 NONE。如果选择 COLOR，则使用的颜色值将由 trans_color 键值决定。 禁用图像收藏 在该会话期间不再询问(_N) 这种文件格式不支持 EXIF。 将图像扩展以适合屏幕(_X) 东 放大图片 启动系统设置时出错：  删除图像 %s 出错 打印文件出错：
%s 曝光 曝光时间： 外插补图像 文件名预览 文件路径规范 文件大小 文件格式未知或不支持 使图像适合窗口 闪光灯： 水平翻转(_H) 垂直翻转(_V) 焦距 焦距： 文件夹 文件夹： 双击切换全屏 GNOME 图像查看器 GPS 数据 常规 转到收藏中第一个图像 转到收藏中最后一张图像 转到收藏中下一个图像 转到收藏中上一个图像 高度： 水平 ISO ISO 速度值： 如果启用，并且没有在活动窗口中加载图像，文件选择器将使用 XDG 指定的用户目录来显示用户的图像文件夹。如果禁用或没有设置图像文件夹，那么它将显示当前的工作目录。 如果启用，那么 GNOME 之眼将不会在移动图像到回收站的时候要求确认。但是如果文件不能被移动到回收站而是被删除的话，那么它仍将询问。 如果启用，属性对话框中的详细的元数据列表将移动对话框到其单独的页面中。这可提高此对话框在小屏幕上的可用性，例如用于上网本。如果禁用，此控件将被嵌入到“Metadata”页中。 如果透明键值是 COLOR，这个键值则确定用来表示透明区域的颜色。 如果启用，将使用背景色键值所设的颜色填充图像后的区域。如果未设置它，则由当前的 GTK+ 主题决定填充颜色。 如果设置为 FALSE，则小图像将不会放大至适合屏幕大小。 如果不保存，您的所有更改都会丢失。 如果不保存，您的更改将会丢失。 图像 图像数据 图像增强 图像属性 图像设置 图像获取条件 图像查看 图像查看器 图像查看器无法根据文件名推测出支持写入的文件格式。 图像缩放 图像收藏面板位置。设定为 0 代表下侧，1 代表左侧，2 代表上侧，3 代表右侧。 图像载入失败。 英寸 插补图像 关键字： 退出全屏模式 已激活插件列表。它并不包含激活插件的位置。参见 .eog-plugin 文件以获得相应插件的位置。 位置： 图像序列循环播放 制作者备忘 隐藏(_D) 元数据 测光 测光模式： 毫米 移至回收站(_T) 名称： 没有载入图像。 “%s”中无图像。 无 北 打开文件夹 打开图像 打开方式(_W) 打开方式(_W) 在单个窗口中打开，如果已经打开了多个窗口那么会在第一个窗口中打开 在全屏模式中打开 在幻灯片放映模式中打开 用文档查看器打开(_D) 正在打开图像“%s” 选项 其它 页面设置 暂停或继续幻灯片放映 Picture;Slideshow;Graphics;图片;幻灯片;图像; 请尝试一个不同的扩展名如 .png 或 .jpg。 插件 位置 首选项(_N) 首选项 预览 打印... 属性(_R) 属性 问题 重新载入图像 重新载入当前图像 重命名自： 逆时针旋转(_C) 将图片逆时针旋转 90 度 将图片顺时针旋转 90 度 运行“%s --help”来查看可用命令行选项的完整列表。 选择您想保存的图像(_E)： 侧边栏(_I) 另存为 保存图像 另存为(_A) 另存为(_A)... 在关闭之前保存更改到图像“%s”？ 保存图像“%s”(第%u个，共%u个) 本地保存图像... 使用滚轮缩放 序列 设为桌面壁纸(_L) 设为桌面壁纸(_W) 显示图像所在文件夹(_F) 显示应用程序版本 在全屏模式中显示当前图像 在文件管理器中显示包含这个文件的文件夹 显示/隐藏图像收藏面板滚动按钮。 显示/隐藏图像收藏面板。 显示/隐藏窗口侧边栏。 显示/隐藏窗口状态栏。 在窗口状态栏中显示图像日期 缩小或放大当前图像 缩小图像 侧边栏(_P) 大小 幻灯片放映(_D) 幻灯片放映 幻灯片放映 放大时平滑图像(_I) 缩小时平滑图像(_O) 有些所选择的图像无法移动到回收站并将永久移除。您确定要这样进行吗？ 南 启动新例程，而不是重用现有例程 支持的图像文件 标记 拍摄时间 临时文件创建失败。 GNOME 之眼也可以全屏幻灯片播放的方式查看图片，或者将图片设置为桌面壁纸。它会自动读取相机的标签来自动旋转您的照片，从而正确的横向/纵向显示它们。 GNOME 之眼(Eye of GNOME)是 GNOME 桌面环境的官方图片查看器。它融合了GNOME 的 GTK+ 外观风格，支持多种图片格式的单图及相册查看。 GNOME 图像查看器。 用于填充图像背后区域的颜色。如果未设置 use-background-color 键，则根据所启用的 GTK+ 主题决定颜色 给定的位置不包含图像。 图像“%s”这张图已经被外部程序修改。
您想重新载入它吗？ 图像“%s”已设为桌面背景。
您想修改它的外观吗？ 要设置打印属性的图像 即将用来打印图像的页面的信息 使用鼠标滚轮缩放时的比例系数。该值定义了每一次滚动事件的缩放步幅。例如：0.05 会在每次缩放事件时放大 5% 而 1.00 则会放大 100%。 还有 %d 幅图像有没有保存的更改。在关闭之前保存更改？ 该图像包含多个页面。图像查看器只能显示第一页。
您想要用文档查看器打开该图像以查看所有页面吗？ 该图像包含多个页面。图像查看器只能显示第一页。
您可以安装文档查看器来查看所有页面。 时间 到： 转换失败。 转换未加载的图像。 透明区域颜色 透明区域指示 透明部分 把图像移除到回收站而不询问 类型 类型： 未知 使用自定义的背景颜色 值 垂直 正在以幻灯片方式查看 西 如果没有加载图像，文件选择器是否应当显示用户的图像文件夹。 图像收藏面板是否可缩放。 在放大时是否对图像进行外插补。这会使图像更平滑，不过比不插补时稍慢。 在缩小时是否对图像进行内插补。这会提升图像质量，不过比不插补时稍慢。 是否应该根据 EXIF 方向自动旋转图像。 属性对话框中的元数据列表是否有自己的页面。 是否使用鼠标滚轮控制缩放。 图像序列是否要无穷循环显示。 宽度： XMP Exif XMP IPTC XMP 其它 XMP 权限管理 您没有保存此文件必须的权限。 放大(_I) 缩小(_O) 缩放比例 [文件...] 关于(_A) 自动旋转(_A) 最佳匹配(_B) 下(_B)： 取消(_C) 关闭(_C) 内容(_C) 复制(_C) 复制图像(_C) 删除(_D) 目标文件夹(_D)： 编辑(_E) 文件名格式(_F)： 第一个图像(_F) 全屏(_F) 转到(_G) 高度(_H)： 帮助(_H) 图像(_I) 图像收藏(_I) 最后一个图像(_L) 退出全屏(_L) 左(_L)： 循环(_L) 下一张(_N) 下一个图像(_N) 正常大小(_N) 打开(_O) 打开背景首选项(_O) 打开(_O)... 首选项(_P) 上一张(_P) 上一个图像(_P) 打印(_P)... 退出(_Q) 重新载入(_R) 将空格替换为下划线(_R) 右(_R)： 顺时针旋转(_R) 保存(_S) 另存为(_S)... 缩放(_S)： 计数初始值(_S)： 状态栏(_S) 图像之间的时间(_T)： 上(_T)： 撤销(_U) 单位(_U)： 查看(_V) 宽度(_W)： 是(_Y) 放大(_Z) 原样 像素 开源软件国际化之简体中文组
YangZhang <zyangmath@gmail.com>, 2008
Xiyue Deng <manphiz@gmail.com>, 2009
YunQiang Su <wzssyqa@gmail.com>, 2010
朱涛 <bill_zt@sina.com>, 2010
Aron Xu <aronxu@gnome.org>, 2010
李炜 <lw124124@gmail.com>, 2011
Julius Wong<villahwong@gmail.com>, 2011
tuhaihe <1132321739qq@gmail.com>, 2013

Launchpad Contributions:
  Aron Xu https://launchpad.net/~happyaron
  Dean Lee https://launchpad.net/~xslidian-lidian
  EL8LatSPQ https://launchpad.net/~el8latspq
  Eric Shan https://launchpad.net/~ericalways
  Feng Chao https://launchpad.net/~chaofeng
  Funda Wang https://launchpad.net/~fundawang
  Funda Wang https://launchpad.net/~fundawang-gmail
  Heling Yao https://launchpad.net/~hyao
  Kyle WANG https://launchpad.net/~osfans
  Lele Long https://launchpad.net/~schemacs
  Qiu Haoyu https://launchpad.net/~timothyqiu
  SunMozilla https://launchpad.net/~sunmozilla
  Tao Wei https://launchpad.net/~weitao1979
  Tong Hui https://launchpad.net/~tonghuix
  Wang Dianjin https://launchpad.net/~tuhaihe
  Wylmer Wang https://launchpad.net/~wantinghard
  Xiyue Deng https://launchpad.net/~manphiz
  YunQiang Su https://launchpad.net/~wzssyqa
  hjhee https://launchpad.net/~hjhee7
  朱涛 https://launchpad.net/~bill-zt 