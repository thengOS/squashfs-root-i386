��    p     �  �        �  #   �     �            0   /     `     s  
   �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �  &      '   .   &   V   -   }   '   �   &   �   $   �      !  	   -!  *   7!  %   b!     �!     �!     �!     �!     �!     �!     �!     "  !   "  	   2"     <"  G   L"     �"     �"     �"     �"     �"     �"     �"     �"      #  $   #     1#  .   Q#  !   �#  %   �#  %   �#     �#     �#      $     $     $  !   $     ;$     U$  "   d$     �$     �$     �$     �$  t   �$  j   4%      �%  ,   �%     �%     &     &     9&     B&     R&     k&  #   t&     �&     �&     �&  N   �&     '  �   '  �    (     �(  *   �(  %   )      A)     b)     ~)     �)     �)      �)     �)     �)  "   *     .*  %   3*  )   Y*  -   �*     �*  
   �*  	   �*  
   �*     �*     �*     �*     �*     +     $+     8+     Q+  #   g+     �+  !   �+     �+     �+     �+     �+     ,     ,  
   ,,     7,     E,     a,     u,     �,     �,  	   �,     �,  @   �,     �,     �,     -     -     +-  �   B-     �-     �-  	   �-  	   �-  	   �-      .     .  
   &.  	   1.  )   ;.  /   e.     �.  	   �.     �.     �.     �.  	   �.     �.     �.      /     /     &/     +/     J/  )   W/     �/     �/     �/     �/     �/     �/     �/     �/     �/  
   0     0  E   .0     t0     y0     �0     �0     �0     �0     �0     �0     �0     �0  	   �0     �0     1     1     11     F1     T1     i1  �   1     R2     j2     x2     ~2     �2     �2     �2     �2     �2     �2  	   �2  
   3     3     "3     =3  $   O3     t3     {3  2   �3     �3     �3  �   �3     m4     �4     �4     �4     �4     �4  !   �4  
   5     &5     25  0   ?5     p5     �5     �5  �  �5     @7  	   L7     V7     g7     v7  	   �7     �7     �7  )   �7     �7  N   �7     ,8     E8     \8     p8  
   �8     �8     �8  
   �8     �8     �8     �8     �8     �8     �8  =   9  4   @9  "   u9  *   �9  <   �9      :  &   :  6   F:  Y   }:  P   �:  (   (;     Q;  V   o;  *   �;  +   �;  ,   <  *   J<  �   u<  %   =  �   -=  N   >  0   Z>  !   �>  
   �>     �>     �>     �>     �>     �>     �>     �>     �>     ?  &   ?     ;?  !   U?     w?  	   �?     �?     �?     �?     �?     �?      @     @     #@     =@  6   J@     �@  T   �@     �@     �@  �   �@  5   �A     �A  	   �A  	    B     
B     B  
   B  
   *B     5B     <B     HB     NB     aB     gB     mB     uB     {B     �B     �B     �B     �B     �B     �B  
   �B     �B  
   �B     �B     �B     �B  
   C     C  
   !C     ,C  	   ;C     EC     MC     ^C     oC     �C     �C     �C     �C     �C  
   �C     �C  :   �C     D  !   0D  S  RD  !   �F     �F     �F     �F  (   
G     3G     CG     SG     mG     rG     wG     |G     �G     �G     �G     �G     �G     �G     �G     �G     �G     �G     �G     �G     �G  &   �G  '   H  &   /H  -   VH  '   �H  &   �H  $   �H     �H     I  0   I     CI     bI     rI     I     �I     �I  	   �I     �I     �I     �I     �I     �I  E   �I     BJ     \J     cJ     jJ     ~J  	   �J     �J     �J     �J     �J     �J  *   K     ,K     GK     gK     �K     �K     �K     �K  
   �K     �K     �K     �K     �K     L     L     #L     0L  ]   @L  c   �L     M  3   !M     UM     nM     {M     �M     �M     �M     �M  $   �M     �M     �M  	   N  =   N     MN  �   TN  �   DO     
P     P     1P     QP     lP     �P     �P     �P     �P     �P     �P     	Q     'Q  !   .Q  '   PQ  '   xQ     �Q  	   �Q     �Q     �Q     �Q     �Q     �Q     �Q  	   R     R     +R     AR  !   WR     yR  (   �R     �R     �R     �R     �R      S     S     )S     9S     JS     cS     sS     �S     �S     �S  	   �S  Q   �S     �S     T     T     !T     1T  v   GT     �T     �T     �T     �T  	   �T     �T     �T     U  	   +U     5U  !   QU     sU     �U     �U     �U  	   �U  	   �U     �U     �U     �U  	   V     V  ,   V  	   <V  '   FV     nV  	   uV     V     �V     �V     �V     �V     �V     �V  	   �V     W  3   W     LW  
   SW  
   ^W     iW  
   pW  
   {W     �W     �W     �W     �W     �W     �W     �W     �W     �W  
   X     X     0X  �   FX     Y  	   Y     %Y     ,Y     9Y     FY     YY     iY  "   �Y  	   �Y     �Y     �Y     �Y     �Y     �Y     Z      Z     'Z  *   4Z     _Z     pZ  ~   �Z  "    [     #[     @[     ][  	   s[     }[  "   �[     �[     �[     �[  ,   �[     \     \     7\  l  P\     �]     �]     �]     �]     �]     
^     ^  
   $^  *   /^     Z^  ?   k^     �^     �^     �^     �^     �^  	   _  	   _  	   _  	   #_  	   -_  	   7_     A_     W_     c_  .   s_  %   �_     �_  #   �_  A   `     D`     c`  $   �`  W   �`  ;   �`      ;a     \a  T   ra  !   �a  !   �a  &   b  $   2b  F   Wb     �b  �   �b  H   �c  1   �c     d  	   %d  	   /d  	   9d     Cd     Ld     [d     bd     od     wd     �d     �d     �d     �d     �d  	   �d     �d     e     e     ,e     Fe     `e     ze     �e     �e  2   �e     �e  K   �e  
   -f     8f  �   <f  -   g  
   =g  
   Hg     Sg  
   _g     jg     {g  
   �g  
   �g  
   �g  
   �g     �g  
   �g  
   �g  
   �g  
   �g  
   �g     h  
   h  
   h  
   &h  
   1h  
   <h     Gh     Xh     ih     wh     �h     �h     �h     �h     �h     �h     �h     �h     
i     i     /i     Fi     ]i     qi  
   �i     �i  
   �i  
   �i  O   �i  <  
j     Gp        �   &   !  �       k     T   $             \   �   +              �   7   <  b   �   1       �   �       �   k   �   �   P   T  n           �              	  �   @   h       �   I   �   �   �                      =   2   %  V            �   9  z      �       x       G             Z   �   ;       �   "                     �       ?   �   C  �   5      W  0      �   �               �       A      p  &  H            R             i  }      �       '      �   .  (   �           B  �   X     f  )               8        "                 q   V   �   +  �       �        %         R  �   7    !   {   s   6          �   �                �   �   �   t   U   �   �       �   `   H  o  �   �       �     �      <           0   �   �   l   r   �   N  y   5      �       =    
       .           �       G       �   9   (       �   �   ~       ;    o                     m   �           B       �       �   P      1  �   Z        �   j   �       �   �       �   �   E   �          �   �   �       >       �   :      p   w   Q  �   ^  D  4       u         J  �       n  ,  �   @      ^   E  #   �   �   �   �           '   �   M   c       a   c  ]         e  �   *   v   >  [  �     C   )        �      h  g        Q                O  
  _   �     4      �              �       �   D   �   -   #  8  �   d       M  �   m  Y   �   -         $       �     �   W       �   �       �     �      K  �   Y  �   �   O   �   /   S  L       N   �   �       b  :   l    �       X   �              �       2  ,   �       6   	           L  �       �   [   �   �   A   `  J   e       �   �   I  j  �   3   �   �         |   \  �   �   g   �   �   f   ]   �   �   �   *      �   3      ?        K   F  �   F             �   �   �           d  �   �                 S      _    U  a  /            �     i     (Not one of the Standard 14 Fonts)  (One of the Standard 14 Fonts) %.0f × %.0f mm %.2f × %.2f inch %d pending job in queue %d pending jobs in queue %s, Landscape (%s) %s, Portrait (%s) (%d of %d) 100% 125% 150% 1600% 175% 200% 300% 3200% 400% 50% 6400% 70% 800% 85% Add Add highlight annotation Add text annotation Adds support for reading DVI documents Adds support for reading DjVu documents Adds support for reading PDF Documents Adds support for reading PostScript documents Adds support for reading TIFF documents Adds support for reading XPS documents Adds support for reading comic books All Documents All Files All fonts are either standard or embedded. Allow links to change the zoom level. Annotate the document Annotation Properties Annotation Properties… Annotations Attachments Author: Auto Rotate and Center Auto_scroll Automatically reload the document Bookmarks C_ase Sensitive Can't find an appropriate command to decompress this type of comic book Cancel _print and Close Circle Close Close _after Printing Close _without Saving Color: Comic Books Comment Copy _Image Could not open the containing folder Could not send current document Couldn't find appropriate format to save image Couldn't open attachment “%s” Couldn't open attachment “%s”: %s Couldn't save attachment “%s”: %s Created: Creator: Cross DEST DVI Documents DVI document has incorrect format Delete the temporary file DjVu Documents DjVu document has incorrect format Document Document License Document View Document Viewer Document contains form fields that have been filled out. If you don't save a copy, changes will be permanently lost. Document contains new or modified annotations. If you don't save a copy, changes will be permanently lost. Document contains no annotations Document viewer for popular document formats Don't show this message again Download document Downloading document (%d%%) Embedded Embedded subset Enable caret navigation? Encoding End of presentation. Click to exit. Enlarge the document Enter password Error %s Error launching the command “%s” in order to decompress the comic book: %s Evince Evince is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 Evince is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
 FILE Failed to create a temporary directory: %s Failed to create a temporary file: %s Failed to load document “%s” Failed to load remote file. Failed to print document Failed to print page %d: %s Failed to reload document. Failed to save document “%s” File corrupted File options File type %s (%s) is not supported Find Find a word or phrase in the document Find next occurrence of the search string Find previous occurrence of the search string Finishing… First Page Fit Pa_ge Fit _Width Fit to Printable Area Font Fonts Forget password _immediately Format: Further Information GNOME Document Previewer GNOME Document Viewer Gathering font information… %3d%% General Generating preview: page %d of %d Go to %s on file “%s” Go to file “%s” Go to first page Go to last page Go to next history item Go to next page Go to page Go to page %s Go to previous history item Go to previous page Go to the next page Go to the previous page Help Highlight Icon: If you close the window, pending print jobs will not be printed. Index Initial window state: Insert Invalid document Invalid page selection It supports the following document formats: PDF, PS, EPS, XPS, DjVu, TIFF, DVI (with SyncTeX), and Comic Books archives (CBR, CBT, CBZ, CB7). Jump to page: Key Keywords: Last Page Launch %s Layers Loading document from “%s” Loading… Location: Make the current document fill the window Make the current document fill the window width Markup type: Modified: NUMBER Named destination to display. New Paragraph Next Page No No files in archive No images found in archive %s No name None Not a comic book MIME type: %s Not embedded Not found, click to change search options Note Number of Pages: Op_en a Copy Opaque Open Open Containing _Folder Open Document Open an existing document Open in New _Window Optimized: Override document restrictions Override document restrictions, like restriction to copy or to print. PAGE PDF Documents P_roperties… Page Page %d Page %s Page Handling Page Scaling: Page cache size in MiB Paper Size: Paragraph Password Required Password for document %s Password required PostScript Documents Pre_sentation Preparing preview… Preparing to print… Pressing F7 turns the caret navigation on or off. This feature places a moveable cursor in text pages, allowing you to move around and select text with your keyboard. Do you want to enable the caret navigation? Preview before printing Previous Page Print Print Preview Print document Print settings file Print this document Printing job “%s” Printing page %d of %d… Print… Producer: Properties Recent Documents Reloading document from %s Remember _forever Remember password until you _log out Remove Remove Annotation Requested format is not supported by this printer. Rotate _Left Rotate _Right Rotate printer page orientation of each page to match orientation of each document page. Document pages will be centered within the printer page. Run evince as a previewer Run evince in fullscreen mode Run evince in presentation mode Running in presentation mode STRING Save Attachment Save Current Settings as _Default Save Image Save a Copy Save a _Copy Save a copy of document “%s” before closing? Saving attachment to %s Saving document to %s Saving image to %s Scale document pages to fit the selected printer page. Select from one of the following:

• "None": No page scaling is performed.

• "Shrink to Printable Area": Document pages larger than the printable area are reduced to fit the printable area of the printer page.

• "Fit to Printable Area": Document pages are enlarged or reduced as required to fit the printable area of the printer page.
 Scroll Down Scroll Up Scroll View Down Scroll View Up Search options Security: Select Page Select _All Select page size using document page size Send _To… Show a dialog to confirm that the user wants to activate the caret navigation. Show the entire document Show two pages at once Shrink the document Shrink to Printable Area Side _Pane Size: Squiggly Strike out Style: Subject: Substituting with Supported Image Files TIFF Documents Text License The URI of the directory last used to open or save a document The URI of the directory last used to save a picture The attachment could not be saved. The command “%s” did not end normally. The command “%s” failed at decompressing the comic book. The document contains no pages The document contains only empty pages The document is automatically reloaded on file change. The document is composed of several files. One or more of these files cannot be accessed. The document “%s” is locked and requires a password before it can be opened. The file could not be saved as “%s”. The image could not be saved. The maximum size that will be used to cache rendered pages, limits maximum zoom level. The page label of the document to display. The page number of the document to display. The selected printer '%s' could not be found The word or phrase to find in the document There is %d print job active. Wait until print finishes before closing? There are %d print jobs active. Wait until print finishes before closing? There was an error deleting “%s”. This document contains non-embedded fonts that are not from the PDF Standard 14 fonts. If the substitute fonts selected by fontconfig are not the same as the fonts used to create the PDF, the rendering may not be correct. This document is locked and can only be read by entering the correct password. This is a document viewer for the GNOME desktop. This work is in the Public Domain Thumbnails Title: Transparent TrueType TrueType (CID) Type 1 Type 1 (CID) Type 1C Type 1C (CID) Type 3 Unable to launch external application. Unable to open attachment Unable to open document “%s”. Unable to open external link Underline Unknown Unknown MIME Type Unknown font type Uploading attachment (%d%%) Uploading document (%d%%) Uploading image (%d%%) Usage terms View multi-page documents View options Wait until print job “%s” finishes before closing? Warning When enabled, each page will be printed on the same size paper as the document page. XPS Documents Yes You should have received a copy of the GNU General Public License along with Evince; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 Your print range selection does not include any pages Zoom _In Zoom _Out [FILE…] _About _Add Bookmark _Automatic _Bookmarks _Close _Continuous _Copy _Copy Link Address _Dual _Edit _Enable _File _Find _First Page _Fullscreen _Go _Go To _Help _Inverted Colors _Last Page _New Window _Next Page _Odd Pages Left _Open Attachment _Open Bookmark _Open Link _Open… _Password: _Previous Page _Print… _Reload _Remove Bookmark _Rename Bookmark _Save Attachment As… _Save Image As… _Save a Copy… _Unlock Document _View _Whole Words Only default:mm of %d pdf;ps;postscript;dvi;xps;djvu;tiff;document;presentation; translator-credits © 1996–2014 The Evince authors Project-Id-Version: evince master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=evince&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-03-08 21:45+0000
PO-Revision-Date: 2016-03-25 07:50+0000
Last-Translator: YunQiang Su <wzssyqa@gmail.com>
Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2016-06-27 17:26+0000
X-Generator: Launchpad (build 18115)
Language: zh_CN
X-DamnedLies-Scope: partial
  (14 种标准字体范围之外)  (14 种标准字体之一) %.0f x %.0f 毫米 %.2f x %.2f 英寸 队列中有 %d 个等待执行的任务 %s，横向(%s) %s，纵向(%s) (第 %d 页，共 %d 页) 100% 125% 150% 1600% 175% 200% 300% 3200% 400% 50% 6400% 70% 800% 85% 添加 添加高亮批注 添加文本注释 增加支持阅读 DVI 文档的功能 增加支持阅读 DjVu 文档的功能 增加支持阅读 PDF 文档的功能 增加支持阅读 PostScript 文档的功能 增加支持阅读 TIFF 文档的功能 增加支持阅读 XPS 文档的功能 增加支持阅读动画书的功能 全部文档 全部文件 全部字体皆为标准字体或嵌入字体。 允许链接调整缩放级别 给文档批注 注释属性 注释属性... 注释 附件 作者： 自动旋转并居中 自动滚屏(_S) 自动刷新文档 书签 区分大小写(_A) 无法找到合适的命令来解压这种类型的 Comic Book 文档 取消打印并关闭(_P) 圆圈 关闭 打印后关闭(_A) 关闭且不保存(_W) 颜色： Comic Book 文档 评论 复制图像(_I) 无法打开所在的文件夹 无法保存当前文档 无法找到恰当的格式以保存图像 无法打开附件“%s” 无法打开附件“%s”：%s 无法保存附件“%s”：%s 创建于： 创建者： 交叉 DEST DVI 文档 DVI 文档的格式不对 删除临时文件 DjVu 文档 DjVu 文档的格式不对 文档 文档许可证 文档视图 文档查看器 文档包含了已经填写的表单。如果您不保存副本，修改将会永久丢失。 文档包含了新的或修改过的注释。如果您不保存副本，修改将会永久丢失。 此文档不包含任何注释 支持查看流行的文档格式的文档查看器 不再显示这条信息 下载文档 正在下载文档 (%d%%) 嵌入 嵌入子集 启用光标浏览？ 编码 演示文稿结束。单击退出。 放大文档 输入密码 错误 %s 加载命令“%s”来解压 Comic Book 文档时出错：%s Evince 发布 Evince 的目的是希望它能够在一定程度上帮到您。但我们并不为
它提供任何形式的担保，也无法保证它可以在特定用途中得到您希望的
结果。请参看 GNU GPL 许可中的更多细节。
 Evince 是自由软件；您可以按照自由软件基金会所发布的 GNU GPL 许可对其再发放和/或修改它；至于 GPL 的版本，您可以使用第二版或任何更新的版本。
 文件 创建临时目录 %s 失败。 创建临时文件 %s 失败。 载入文档“%s”失败 载入远程文档失败。 打印文档失败 打印页面 %d 失败： %s 重新载入文档失败。 保存文档“%s”失败 文件损坏 文件选项 不支持文件类型 %s (%s) 查找 在文档中查找单词或短语 查找被搜索字符串的下次出现 查找被搜索字符串的上次出现 完成中... 第一页 适合页面(_G) 适合宽度(_W) 适合于打印区域 字体 字体 立即忘记密码(_I) 格式： 更多信息 GNOME 文档预览器 GNOME 文档查看器 正在搜集字体信息... %3d%% 常规 正在生成预览：第%d页，共%d页 转到文件“%2$s”的 %1$s 转到文件“%s” 转到第一页 转到最后一页 转到下一历史条目 转到下一页 转到指定页 转到第 %s 页 转到上一历史条目 转到上一页 转到下一页 转到上一页 帮助 高亮 图标： 如果你关闭该窗口，正在等待执行的打印任务将不会被打印。 索引 初始窗口状态： 插入 无效的文档 无效的页面选择 它支持以下的文档格式: PDF, PS, EPS, XPS, DjVu, TIFF, DVI(和SyncTex), 和动画书档案(CBR, CBT, CBZ, CB7) 跳到页面： 关键 关键词： 最后一页 调用 %s 层 正在从“%s”载入文档 正在载入... 位置： 让当前文档适合窗口 让当前文档适合窗口宽度 标记类型： 修改于： 页号 显示指定名称的目标。 新段落 下一页 否 存档中没有文件 存档 %s 中未找到图像 无名称 无 不是 Comic Book 文档的 MIME 类型：%s 未嵌入 未找到，单击以更改搜索选项 笔记 页数： 打开副本(_E) 不透明的 打开 打开所在的文件夹(_F) 打开文档 打开已有文档 在新窗口中打开(_W) 优化： 忽略文档限制 忽略文档限制，比如限制复制或打印。 页面 PDF 文档 属性(_R) 页面 第 %d 页 第 %s 页 页面处理 页面缩放： 页缓存大小(MiB) 纸张大小： 段落 需要密码 文档 %s 的密码 需要密码 PostScript 文档 放映(_S) 正在准备预览... 正在准备打印... 按下 F7 可以打开或关闭光标浏览功能。这个功能会在文字页面中显示一个可移动光标，允许您用键盘四处移动并选择文字。您想要启用光标浏览吗？ 打印前预览 上一页 打印 打印预览 打印文档 打印设置文件 打印此文档 正在打印任务“%s” 正在打印第%d页，共%d页... 打印... 制作者： 属性 最近文档 正在从 %s 重新载入文档 永远记住(_F) 记住密码直到注销(_L) 移除 去掉注释 该打印机不支持此要求的格式。 向左旋转(_L) 向右旋转(_R) 旋转每个页面的打印机页面方向以匹配每个文档页面的方向。文档页面将在打印机页面内居中。 以预览程序模式运行 evince 以全屏模式运行 evince 以放映模式运行 evince 以放映模式运行 字符串 保存附件 将当前设置设为默认值(_D) 保存图像 保存副本 保存副本(_C) 关闭前保存文档“%s”的副本吗？ 正在保存附件到 %s 正在保存文档到 %s 正在保存图片到 %s 缩放文档页面以适合于选定的打印机页面。从下列中选定一个：

• “否”：不执行页面缩放

• “缩小至打印区域”：缩小大于可打印区域的文档页面以适合打印机页面的可打印区域。

• “适合于打印区域”：按要求缩小或放大文档页面以适合打印机页面的可打印区域。
 向下滚动 向上滚动 向下滚动视图 向上滚动视图 搜索选项 安全性： 选择页面 全选(_A) 使用文档页面大小选定页面大小 发送至(_T)... 显示一个对话框以确认用户想要使用键盘浏览。 显示整篇文档 一次显示两页 缩小文档 缩小至可打印区域 侧边栏(_P) 大小： 波浪线 删除线 样式： 主题： 替换为 支持的图像文件 TIFF 文档 文本许可证 最近用来打开或保存文档的目录 URI 最近用来保存图片的目录 URI 无法保存附件。 命令“%s”没有正常结束。 在解压这个 Comic Book 文档时使用命令“%s”失败。 此文档不包含任何页面 这个文档只包含空页面 文件发生变化时自动刷新。 此文档由多个文件组成，而这些文件中的一个或多个无法被访问。 文档“%s”已载入，但在打开之前需要密码。 文件无法另存为“%s”。 无法保存图像。 将用于缓存渲染了的页面的最大大小，此会限制最大缩放级别。 要显示的文档的页标签。 要显示的文档的页编号。 选择的打印机“%s”没有找到 文档中待查找的单词或短语 有 %d 项活动的打印任务。等待打印完成后再关闭吗？ 删除“%s”时出错。 该文档包含了未嵌入的字体，且字体并不是 PDF 原生支持的 14 种标准字体。如果由 fontconfig 选择的替代字体与创建 PDF 时所用字体不一致，渲染可能不太正确。 文档已被锁定，只能在输入正确的密码之后才能读取。 这是 GNOME 桌面环境下的文档查看器。 这项工作是在公共领域 缩略图 标题： 透明的 TrueType TrueType (CID) Type 1 Type 1 (CID) Type 1C Type 1C (CID) Type 3 无法启动外部程序。 无法打开附件 无法打开文档“%s”。 无法打开外部链接 下划线 未知 未知的 MIME 类型 未知字体类型 正在上传附件 (%d%%) 正在上传文档 (%d%%) 上载上传图片 (%d%%) 使用条款 查看多页文档 查看选项 等待打印任务“%s”完成后再关闭吗？ 警告 当启用时，每页将打印于跟文档页面同样大小的纸张上。 XPS 文档 是 您应该收到和 Evince 一起的 GNU GPL 协议副本；如果没有收到该协议
的话，您可以写信给自由软件基金会，地址是：
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 您选择的打印范围不包括任何页面 放大(_I) 缩小(_O) [文件...] 关于(_A) 添加书签(_A) 自动（_A) 书签(_B) 关闭(_C) 连续(_C) 复制(_C) 复制链接地址(_C) 双页(_D) 编辑(_E) 启用(_E) 文件(_F) 查找(_F) 第一页(_F) 全屏(_F) 转到(_G) 转到(_G) 帮助(_H) 反色(_I) 最后一页(_L) 新建窗口(_N) 下一页(_N) 奇数页在左侧(_O) 打开附件(_O) 打开书签(_O) 打开链接(_O) 打开(_O)... 密码(_P)： 上一页(_P) 打印(_P)... 重新载入(_R) 移除书签(_R) 重命名书签(_R) 保存附件为(_S)... 图像另存为(_S)... 保存副本(_S)... 取消文档锁定(_U) 查看(_V) 全字匹配(_W) default:mm 共 %d 页 pdf;ps;postscript;dvi;xps;djvu;tiff;document;presentation;文档;演示;幻灯; Aron Xu <happyaron.xu@gmail.com>, 2009, 2010, 2011
樊栖江(Fan Qijiang) <fqj1994@gmail.com>, 2009
甘露(Gan Lu) <rhythm.gan@gmail.com>, 2009
Deng Xiyue <manphiz@gmail.com>, 2009
杨章 <zyangmath@gmail.com>, 2010
朱涛 <bill_zt@sina.com>, 2010
Tao Wang <dancefire@gmail.com>, 2010
Xhacker Liu <liu.dongyuan@gmail.com>, 2010
tuhaihe <1132321739qq@gmail.com>, 2013
eternalhui <www.eternalhui@gmail.com>, 2013
Launchpad Contributions:
  Aron Xu https://launchpad.net/~happyaron
  Carlos Gong https://launchpad.net/~bfsugxy
  Chen Ming https://launchpad.net/~chenming
  DBLobster https://launchpad.net/~db.lobster
  Dingyuan Wang https://launchpad.net/~gumblex
  EL8LatSPQ https://launchpad.net/~el8latspq
  Eleanor Chen https://launchpad.net/~chenyueg
  Funda Wang https://launchpad.net/~fundawang
  Funda Wang https://launchpad.net/~fundawang-gmail
  Heling Yao https://launchpad.net/~hyao
  Liu Qishuai https://launchpad.net/~lqs
  Tao Wei https://launchpad.net/~weitao1979
  WEI Kun https://launchpad.net/~conkty
  Wang Dianjin https://launchpad.net/~tuhaihe
  Wylmer Wang https://launchpad.net/~wantinghard
  Xhacker Liu https://launchpad.net/~xhacker
  Xiyue Deng https://launchpad.net/~manphiz
  YunQiang Su https://launchpad.net/~wzssyqa
  ZhangCheng https://launchpad.net/~xxzc
  fqj1994 https://launchpad.net/~fqj1994
  kevinchou https://launchpad.net/~kevinchou-c-gmail
  shijing https://launchpad.net/~shijing
  zhangmiao https://launchpad.net/~mymzhang
  张天鹄 https://launchpad.net/~koc
  朱涛 https://launchpad.net/~bill-zt
  甘露 (Lu Gan) https://launchpad.net/~rhythm-gan © 1996–2014 Evince 作者 