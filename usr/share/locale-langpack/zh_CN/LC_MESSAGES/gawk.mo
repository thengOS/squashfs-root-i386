��         �  u  �      p     q     ~     �     �     �     �          &  %   A  y   g  +   �  "     -   0     ^  :   u  3   �     �     �  ,     ,   C  -   p      �  )   �  &   �  (     :   9     t     �  '   �  +   �  (    ]   1     �     �     �     �  $   �          .     I     Z     s     |     �  !   �  "   �  #   �  #        B     ]  '   e  �   �     y      �      �      �   A   �   A   �   �   A!  8   �!  5    "     6"  3   M"     �"  !   �"  5   �"      �"  #   #  8   7#     p#  !   �#      �#  *   �#  0   �#     -$     M$  *   i$  *   �$  #   �$  &   �$     
%  *   %  +   H%  1   t%  -   �%  0   �%     &  $    &     E&     d&  4   �&  #   �&  $   �&  ,   '  2   /'     b'     �'     �'  $   �'  #   �'  $   (  /   *(  &   Z(     �(     �(  !   �(     �(     �(  *   )     >)  #   ])  $   �)  5   �)  E   �)  *   "*  -   M*  
   {*  -   �*  $   �*  "   �*  $   �*  $   !+  #   F+     j+  &   �+     �+  .   �+  $   �+     ,  "   7,  #   Z,  "   ~,  (   �,  /   �,  "   �,  @   -  !   ^-  "   �-     �-  +   �-      �-  "   �-  5   .  5   Q.  '   �.  8   �.  (   �.  0   /  7   B/      z/  .   �/  .   �/     �/  %   0  +   ;0     g0  4   o0  ?   �0  ?   �0     $1  F   D1     �1     �1  &   �1  6   �1  W   
2  9   b2  =   �2  >   �2  8   3  F   R3  #   �3  <   �3  5   �3  %   04  t   V4  G   �4  )   5  *   =5  "   h5  "   �5  $   �5     �5     �5  $   6      56  "   V6  "   y6  +   �6  ,   �6  )   �6  %   7  $   E7  .   j7  /   �7  3   �7  4   �7  0   28  '   c8  -   �8  '   �8  '   �8  )   	9  %   39     Y9     m9  4   �9  (   �9  0   �9  8   :  &   M:  '   t:  1   �:  2   �:  <   ;  (   >;  )   g;     �;  @   �;     �;  K   <     S<  (   b<  &   �<  (   �<  9   �<  9   =  6   O=  2   �=  )   �=  )   �=  1   >  )   ?>  *   i>  +   �>  ,   �>  0   �>  !   ?     @?  "   ]?     �?  #   �?  4   �?  &   �?  &   @  #   A@  $   e@     �@  &   �@  .   �@  ,   �@  J   A     hA     �A  ?   �A  /   �A  4   B  $   IB  *   nB  ,   �B  $   �B  2   �B     C  %   =C  -   cC  "   �C  %   �C  0   �C  #   D     /D  2   CD     vD  "   �D  0   �D     �D  1   �D  @   $E  A   eE  	   �E  �  �E     jG     vG     �G     �G     �G  &   �G     H     $H     ;H  �   YH     &I  !   FI  *   hI     �I  J   �I  :   �I     1J     FJ  '   dJ  '   �J  %   �J     �J  +   �J  #   &K  )   JK  C   tK  !   �K  !   �K  7   �K  *   4L  �   _L  [   5M     �M     �M     �M     �M     �M     N     N     6N     CN  	   YN     cN     N      �N      �N  !   �N     �N     O     0O     7O  �   QO     AP     TP     jP     �P  H   �P  C   �P  �   Q  )   �Q  7   �Q     R  ,   R     BR  &   ZR  7   �R     �R  &   �R  A   �R     <S      YS     zS  *   �S  1   �S     �S     T  *   +T  *   VT  "   �T     �T     �T  "   �T  "   �T  $   U  ,   CU  ,   pU     �U  '   �U  "   �U     V  3   $V  '   XV  '   �V  -   �V  4   �V  !   W     -W     LW  7   kW  1   �W  1   �W  *   X  *   2X     ]X     |X  '   �X     �X     �X  '   �X     Y  +   1Y  +   ]Y  D   �Y  J   �Y  %   Z  4   ?Z  
   tZ  0   Z     �Z     �Z  !   �Z  &   
[  $   1[  )   V[  /   �[     �[  7   �[  -   \  
   1\     <\     U\     o\  #   �\  0   �\     �\  ?   �\  ,   9]  &   f]     �]  /   �]     �]     �]  /   �]  /   -^  (   ]^  ?   �^  (   �^  .   �^  :   _     Y_  =   t_  =   �_     �_  %   `  %   1`     W`  C   f`  E   �`  E   �`  $   6a  M   [a     �a     �a  )   �a  >   �a  U   )b  6   b  @   �b  C   �b  4   ;c  B   pc  &   �c  E   �c  3    d      Td  m   ud  :   �d  %   e  %   De     je  #   �e  $   �e  #   �e     �e      f  '   ,f     Tf     of  #   �f  #   �f  %   �f  "   �f      g  9   <g  9   vg  ;   �g  ;   �g  )   (h     Rh  #   qh     �h     �h     �h  ,   �h     i     <i  9   Xi  ,   �i     �i  7   �i  "   j  #   8j  4   \j  &   �j  7   �j  !   �j  "   k  $   5k  I   Zk     �k  A   �k     �k  #   l  $   ,l  #   Ql  F   ul  F   �l  7   m  3   ;m  '   om  '   �m  +   �m  (   �m  (   n  #   =n  #   an  >   �n  !   �n  !   �n     o     #o     ;o  +   Zo  "   �o     �o     �o     �o     �o  "   p  %   .p  (   Tp  R   }p     �p     �p  <   q  *   Cq  0   nq     �q  )   �q  -   �q      r  B   7r     zr  !   �r  $   �r  !   �r  !   s  1   $s  $   Vs     {s  @   �s     �s  -   �s  =   t     ]t  -   st  ?   �t  @   �t     "u         �   C           V   �   �       h       |   �           �   �           w                  b               Q       �   f   8           �   <     A       �   #      &   �       ]   �   M       �   �   Z       �   �   I           H   �   P   �   F   �       ,          �          �   �   n   �   �          �       �   
  p   �   �      �   /   :   	               .   '   %                  �   �   )   �   �   o   (   �           �   �   =   �   �   �   K   �   2   >   �      �   �   �   }   �       *          �       �       �   �   �   J   �   �             a      `   �   �       4   U   �       �   L   �   �   t       s            q   �           �       x      �   �       3   c   5   _   �   +          �   g   -      �          1       �   v       �   �           6   X              {   �   ~             !   �         Y   �   m              �   �           i   �   "   �       �         �   �   �   e       �   �           �   �   G   �   �   �             �   k   �   �   9   �   �       �   @   R           ^       r   y      �   �   0   �      ;        �     d   �   W   �       O   �       �   S     $   �   l     �           \   �   �   
   �   �   [   �             �       �                 �   ?   	            T   E   �   �   �          B      �   D   z       �       �       �       �   �   �   �          �   u       j          7          �   �   �   N   �                  �   �   �    	# Rule(s)

 	# gawk profile, created %s
 	-F fs			--field-separator=fs
 	-O			--optimize
 	-W nostalgia		--nostalgia
 	-f progfile		--file=progfile
 	-v var=val		--assign=var=val
 
	# Function Call Stack:

 
	# Functions, listed alphabetically
 
To report bugs, see node `Bugs' in `gawk.info', which is
section `Reporting Problems and Bugs' in the printed version.

 %d is invalid as number of arguments for %s %s blocks must have an action part %s third parameter is not a changeable object %s to "%s" failed (%s) %s: %d: tawk regex modifier `/.../%c' doesn't work in gawk %s: `%s' argument to `-v' not in `var=value' form

 %s: close failed (%s) %s: invalid option -- '%c'
 %s: option '%c%s' doesn't allow an argument
 %s: option '--%s' doesn't allow an argument
 %s: option '-W %s' doesn't allow an argument
 %s: option '-W %s' is ambiguous
 %s: option `-W %s' unrecognized, ignored
 %s: option requires an argument -- %c
 %s: option requires an argument -- '%c'
 %s: string literal as last arg of substitute has no effect %s: unrecognized option '%c%s'
 %s: unrecognized option '--%s'
 -Ft does not set FS to tab in POSIX awk BINMODE value `%s' is invalid, treated as 3 Copyright (C) 1989, 1991-%d Free Software Foundation.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

 Examples:
	gawk '{ sum += $1 }; END { print sum }' file
	gawk -F: '{ print $1 }' /etc/passwd
 Invalid back reference Invalid character class name Invalid collation character Invalid content of \{\} Invalid preceding regular expression Invalid range end Invalid regular expression Memory exhausted NF set to negative value No match No previous regular expression POSIX does not allow `%s' POSIX does not allow `\x' escapes POSIX does not allow operator `**' POSIX does not allow operator `**=' Premature end of regular expression Regular expression too big Success TCP/IP communications are not supported This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

 Trailing backslash Unmatched ( or \( Unmatched ) or \) Unmatched \{ Usage: %s [POSIX or GNU style options] -f progfile [--] file ...
 Usage: %s [POSIX or GNU style options] [--] %cprogram%c file ...
 You should have received a copy of the GNU General Public License
along with this program. If not, see http://www.gnu.org/licenses/.
 [s]printf: format specifier does not have control letter [s]printf: value %g is out of range for `%%%c' format ^ ran out for this one `%s' is a built-in function, it cannot be redefined `%s' is a gawk extension `%s' is not a legal variable name `%s' is not a variable name, looking for file `%s=%s' `%s' is not supported in old awk `--posix' overrides `--traditional' `--posix'/`--traditional' overrides `--non-decimal-data' `BINMODE' is a gawk extension `FIELDWIDTHS' is a gawk extension `IGNORECASE' is a gawk extension `L' is meaningless in awk formats; ignored `delete(array)' is a non-portable tawk extension `extension' is a gawk extension `goto' considered harmful!
 `h' is meaningless in awk formats; ignored `l' is meaningless in awk formats; ignored `length(array)' is a gawk extension `return' used outside function context `|&' not supported atan2: received non-numeric first argument atan2: received non-numeric second argument attempt to field reference from non-numeric value attempt to use array `%s' in a scalar context attempt to use scalar parameter `%s' as an array backslash at end of string backslash not last character on line bad `%sFMT' specification `%s' buffer overflow in genflags2str call of `length' without parentheses is not portable can't open pipe `%s' for input (%s) can't open pipe `%s' for output (%s) can't open source file `%s' for reading (%s) can't open two way pipe `%s' for input/output (%s) can't read sourcefile `%s' (%s) can't redirect from `%s' (%s) can't redirect to `%s' (%s) can't set binary mode on stderr (%s) can't set binary mode on stdin (%s) can't set binary mode on stdout (%s) cannot create child process for `%s' (fork: %s) cannot open file `%s' for reading (%s) cannot open pipe `%s' (%s) close of `%s' failed (%s). close of fd %d (`%s') failed (%s) close of master pty failed (%s) close of pipe failed (%s) close of redirection that was never opened close of slave pty failed (%s) close of stdin in child failed (%s) close of stdout in child failed (%s) close: `%.*s' is not an open file, pipe or co-process close: redirection `%s' not opened with `|&', second argument ignored close: second argument is a gawk extension close: second argument must be `to' or `from' cmd. line: co-process flush of pipe to `%s' failed (%s). compl: received non-numeric argument cos: received non-numeric argument could not allocate more input memory could not open `%s' for writing (%s) could not open `%s' for writing: %s could not open `%s', mode `%s' could not pre-open /dev/null for fd %d data file `%s' is empty dcgettext: `%s' is not a valid locale category delete: index `%s' not in array `%s' division by zero attempted division by zero attempted in `%%' division by zero attempted in `%%=' division by zero attempted in `/=' duplicate case values in switch body: %s each rule must have a pattern or an action part empty program text on command line environment variable `POSIXLY_CORRECT' set: turning on `--posix' error writing standard error (%s) error writing standard output (%s) error:  escape sequence `\%c' treated as plain `%c' exp: argument %g is out of range exp: received non-numeric argument expression for `%s' redirection has null string value expression in `%s' redirection only has numeric value extension: can't redefine function `%s' extension: can't use gawk built-in `%s' as function name extension: function `%s' already defined extension: function name `%s' previously defined extension: illegal character `%c' in function name `%s' extension: missing function name failure status (%d) on file close of `%s' (%s) failure status (%d) on pipe close of `%s' (%s) fatal error: internal error fatal error: internal error: segfault fatal error: internal error: stack overflow fatal:  fflush: `%s' is not an open file, pipe or co-process fflush: cannot flush: file `%s' opened for reading, not writing fflush: cannot flush: pipe `%s' opened for reading, not writing file flush of `%s' failed (%s). filename `%s' for `%s' redirection may be result of logical expression floating point exception from %s function `%s' called but never defined function `%s' called with more arguments than declared function `%s' called with space between name and `(',
or used as a variable or an array function `%s' defined to take no more than %d argument(s) function `%s': argument #%d: attempt to use array as a scalar function `%s': argument #%d: attempt to use scalar as an array function `%s': can't use function name as parameter name function `%s': can't use special variable `%s' as a function parameter function `%s': missing argument #%d function `%s': parameter #%d, `%s', duplicates parameter #%d function `%s': parameter `%s' shadows global variable function name `%s' previously defined gawk is a pattern scanning and processing language.
By default it reads standard input and writes standard output.

 ignoring unknown format specifier character `%c': no argument converted index: received non-string first argument index: received non-string second argument int: received non-numeric argument internal error: %s with null vname invalid FIELDWIDTHS value, near `%s' invalid char '%c' in expression invalid subscript expression length: received non-string argument local port %s invalid in `/inet' log: received negative argument %g log: received non-numeric argument lshift: received non-numeric first argument lshift: received non-numeric second argument match: third argument is a gawk extension match: third argument is not an array mktime: received non-string argument moving pipe to stdin in child failed (dup: %s) moving pipe to stdout in child failed (dup: %s) moving slave pty to stdin in child failed (dup: %s) moving slave pty to stdout in child failed (dup: %s) multicharacter value of `RS' is a gawk extension multistage two-way pipelines don't work no explicit close of co-process `%s' provided no explicit close of file `%s' provided no explicit close of pipe `%s' provided no explicit close of socket `%s' provided no hex digits in `\x' escape sequence no pre-opened fd %d no program text at all! non-redirected `getline' undefined inside END action null string for `FS' is a gawk extension old awk does not support multidimensional arrays old awk does not support multiple `BEGIN' or `END' rules old awk does not support operator `**' old awk does not support operator `**=' old awk does not support regexps as value of `FS' old awk does not support the `\%c' escape sequence old awk does not support the keyword `in' except after `for' operator `^' is not supported in old awk operator `^=' is not supported in old awk pipe flush of `%s' failed (%s). plain `print' in BEGIN or END rule should probably be `print ""' printf: no arguments reached system limit for open files: starting to multiplex file descriptors reason unknown reference to uninitialized argument `%s' reference to uninitialized field `$%d' reference to uninitialized variable `%s' regexp constant `/%s/' looks like a C comment, but is not regexp constant `//' looks like a C++ comment, but is not regexp constant for parameter #%d yields boolean value regular expression on left of `~' or `!~' operator regular expression on right of assignment regular expression on right of comparison remote host and port information (%s, %s) invalid restoring stdin in parent process failed
 restoring stdout in parent process failed
 rshift: received non-numeric first argument rshift: received non-numeric second argument running %s setuid root may be a security problem sending profile to standard error shadow_funcs() called twice! sin: received non-numeric argument source file `%s' is empty source file does not end in newline split: null string for third arg is a gawk extension split: second argument is not an array sqrt: called with negative argument %g sqrt: received non-numeric argument srand: received non-numeric argument standard output strftime: received empty format string strftime: received non-numeric second argument strftime: received non-string first argument substr: length %g at start index %g exceeds length of first argument (%lu) substr: length %g is not >= 0 substr: length %g is not >= 1 substr: length %g too big for string indexing, truncating to %g substr: non-integer length %g will be truncated substr: non-integer start index %g will be truncated substr: source string is zero length substr: start index %g is invalid, using 1 substr: start index %g is past end of string system: received non-string argument tawk regex modifier `/.../%c' doesn't work in gawk there were shadowed variables. tolower: received non-string argument too many arguments supplied for format string too many pipes or input files open toupper: received non-string argument turning off `--lint' due to assignment to `LINT' unexpected newline or end of string unknown nodetype %d unnecessary mixing of `>' and `>>' for file `%.*s' unterminated regexp unterminated regexp at end of file unterminated regexp ends with `\' at end of file unterminated string use of `\ #...' line continuation is not portable use of dcgettext(_"...") is incorrect: remove leading underscore use of dcngettext(_"...") is incorrect: remove leading underscore warning:  Project-Id-Version: gawk 3.1.6c
Report-Msgid-Bugs-To: arnold@skeeve.com
POT-Creation-Date: 2015-05-19 16:06+0300
PO-Revision-Date: 2010-05-11 09:48+0000
Last-Translator: LI Daobing <lidaobing@gmail.com>
Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:25+0000
X-Generator: Launchpad (build 18115)
 	# 规则

 	# gawk 配置, 创建 %s
 	-F fs			--field-separator=fs
 	-O			--optimize
 	-W nostalgia		--nostalgia
 	-f 脚本文件		--file=脚本文件
 	-v var=val		--assign=var=val
 
	# 函数调用栈:

 
	# 函数列表，字典序
 
提交错误报告请参考“gawk.info”中的“Bugs”页，它位于打印版本中的“Reporting
Problems and Bugs”一节

翻译错误请发信至 translation-team-zh-cn@lists.sourceforge.net

 %d 是 %s 的无效参数个数 %s 块必须有一个行为部分 %s 第三个参数不是一个可变对象 %s 到 "%s" 失败 (%s) %s: %d: tawk 正则表达式修饰符“/.../%c”无法在 gawk 中工作 %s: “-v”的参数“%s”不是“var=value”形式

 %s: 关闭失败(%s) %s: 无效选项 -- “%c”
 %s: 选项“%c%s”不允许有参数
 %s: 选项“--%s”不允许有参数
 %s: 选项“-W %s”不允许参数
 %s: 选项“-W %s”有歧义
 %s: 选项“-W %s”无法识别，忽略
 %s: 选项需要一个参数 -- %c
 %s: 选项需要一个参数 -- “%c”
 %s: 字符串作为 substitute 的最后一个参数无任何效果 %s: 无法识别选项“%c%s”
 %s: 无法识别选项“--%s”
 在 POSIX awk 中 -Ft 不会将 FS 设为制表符(tab) BINMODE 值 “%s” 非法，按 3 处理 版权所有 © 1989, 1991-%d 自由软件基金会(FSF)。

该程序为自由软件，你可以在自由软件基金会发布的 GNU 通用公共许可证(GPL)第
3版或以后版本下修改或重新发布。

 范例:
	gawk '{ sum += $1 }; END { print sum }' file
	gawk -F: '{ print $1 }' /etc/passwd
 无效的回引用 无效的字符类型名 无效的收集字符 \{\} 中内容无效 无效的前置正则表达式 无效的范围结束 无效的正则表达式 内存耗尽 NF 被设置为负值 无匹配 前面没有正则表达式 POSIX 不允许“%s” POSIX 不允许“\x”转义符 POSIX 不允许操作符“**” POSIX 不允许操作符“**=” 正则表达式非正常结束 正则表达式过大 成功 TCP/IP 通讯不被支持 该程序之所以被发布是因为希望他能对你有所用处，但我们不作任何担保。这包含
但不限于任何商业适售性以及针对特定目的的适用性的担保。详情参见 GNU 通用公
共许可证(GPL)。

 尾端的反斜杠 未匹配的 ( 或 \( 未匹配的 ) 或 \) 未匹配的 \{ 用法: %s [POSIX 或 GNU 风格选项] -f 脚本文件 [--] 文件 ...
 用法: %s [POSIX 或 GNU 风格选项] [--] %c程序%c 文件 ...
 你应该收到程序附带的一份 GNU 通用公共许可证(GPL)。如果没有收到，请参看 http://www.gnu.org/licenses/ 。
 [s]printf: 指定格式不含控制字符 [s]printf: 值 %g 对“%%%c”格式来说超出范围 ^ 跑出范围 “%s”是内置函数，不能被重定义 “%s”是 gawk 扩展 “%s”不是一个合法的变量名 “%s”不是一个变量名，查找文件“%s=%s” 老 awk 不支持“%s” “--posix”覆盖“--traditional” “--posix”或“--traditional”覆盖“--non-decimal-data” “BINMODE”是 gawk 扩展 “FIELDWIDTHS”是 gawk 扩展 “IGNORECASE”是 gawk 扩展 “L”在 awk 格式中无意义；忽略 “delete(array)”是不可移植的 tawk 扩展 “extension”是 gawk 扩展 “goto”有害！
 “h”在 awk 格式中无意义；忽略 “l”在 awk 格式中无意义；忽略 “length(array)”是 gawk 扩展 “return”在函数外使用 “|&”不被支持 atan2: 第一个参数不是数字 atan2: 第二个参数不是数字 试图从非数值引用字段编号 试图在标量环境中使用数组“%s” 试图把标量参数“%s”当数组使用 字符串尾部的反斜杠 反斜杠不是行的最后一个字符 错误的“%sFMT”实现“%s” genflags2str 时缓冲区溢出 不带括号调用“length”是不可以移植的 无法为输入打开管道“%s”(%s) 无法为输出打开管道“%s”(%s) 无法以读模式打开源文件“%s”(%s) 无法为输入/输出打开双向管道“%s”(%s) 无法读取源文件“%s”(%s) 无法自“%s”重定向(%s) 无法重定向到“%s”(%s) 无法在标准错误输出上设置二进制模式(%s) 无法在标准输入上设置二进制模式(%s) 无法在标准输出上设置二进制模式(%s) 无法为“%s”创建子进程(fork: %s) 无法以读模式打开文件“%s”(%s) 无法打开管道“%s”(%s) 关闭“%s”失败(%s)。 关闭文件号 %d (“%s”)失败(%s) 关闭主 pty 失败(%s) 关闭管道失败(%s) 关闭一个从未被打开的重定向 关闭从 pty 失败(%s) 在子进程中关闭标准输入失败(%s) 在子进程中关闭标准输出失败(%s) close: “%.*s”不是一个已打开文件、管道或合作进程 close: 重定向“%s”不是用“|&”打开，第二个参数被忽略 close: 第二个参数是 gawk 扩展 close: 第二个参数必须是“to”或“from” 命令行: 刷新合作进程管道“%s”时出错(%s)。 compl: 收到非数字参数 cos: 收到非数字参数 无法分配更多的输入内存 无法以写模式打开“%s”（%s) 无法以写模式打开“%s”: %s 无法以模式“%2$s”打开“%1$s” 无法为文件描述符 %d 预打开 /dev/null 数据文件“%s”为空 dcgettext: “%s”不是一个合理的本地化目录 删除: 索引“%s”不在数组“%s”中 试图除0 在“%%”中试图除0 在“%%=”中试图除0 在“/=”中试图除0 switch 中有重复的 case 值: %s 每个规则必须有一个模式或行为部分 命令行中程序体为空 环境变量“POSIXLY_CORRECT”被设置: 打开“--posix” 向标准错误输出写时发生错误 (%s) 向标准输出写时发生错误 (%s) 错误:  转义序列“\%c”被当作单纯的“%c” exp: 参数 %g 超出范围 exp: 收到非数字参数 “%s”重定向中的表达式是空字符串 “%s”重定向中的表达式只有数字值 extension: 无法重定义函数“%s” extension: 无法使用 gawk 内置的 “%s” 作为函数名 extension: 函数“%s”已经被定义 extension: 函数名“%s”前面已被定义 extension: 函数名“%2$s”中有非法字符“%1$c” extension: 缺少函数名 关闭文件“%2$s”时失败，失败状态为(%1$d)(%3$s) 关闭管道“%2$s”时失败，失败状态为(%1$d)(%3$s) 致命错误: 内部错误 致命错误: 内部错误: 段错误 致命错误: 内部错误: 栈溢出 致命错误:  fflush: “%s”不是一个已打开文件、管道或合作进程 fflush: 无法使用: 文件“%s”以只读方式打开，不可写 fflush: 无法使用: 管道“%s”以只读方式打开，不可写 刷新文件“%s”时出错(%s)。 “%2$s”重定向中的文件名“%1$s”可能是逻辑表达式的结果 浮点数异常 从 %s 函数“%s”被调用但重未被定义 函数“%s”被调用时提供了比声明时更多的参数 函数“%s”被调用时名字与“(”间有空格，
或被用作变量或数组 函数“%s”被定义为支持不超过 %d 个参数 函数“%s”: 第 %d 个参数: 试图把数组当标量使用 函数“%s”: 第 %d 个参数: 试图把标量当作数组使用 函数“%s”: 无法使用函数名作为参数名 函数“%s”: 无法使用特殊变量“%s”作为函数参数 函数“%s”: 缺少第 %d 个参数 函数“%s”: 第 %d 个参数, “%s”, 与第 %d 个参数重复 函数“%s”: 参数“%s”掩盖了公共变量 函数名“%s”前面已定义 gawk 是一个模式扫描及处理语言。缺省情况下它从标准输入读入并写至标准输出。

 忽略位置的格式化字符“%c”: 无参数被转化 index: 第一个参数不是字符串 index: 第二个参数不是字符串 int: 收到非数字参数 内部错误: %s 带有空的 vname “%s”附近 FIELDWIDTHS 值无效 表达式中的无效字符“%c” 无效的下标表达式 length: 收到非字符串参数 本地端口 %s 在“/inet”中无效 log: 收到负数参数 %g log: 收到非数字参数 lshift: 第一个参数不是数字 lshift: 第二个参数不是数字 match: 第三个参数是 gawk 扩展 match: 第三个参数不是数组 mktime: 收到非字符串参数 在子进程中将管道改为标准输入失败(dup: %s) 在子进程中将管道改为标准输出失败(dup: %s) 在子进程中将从 pty 改为标准输入失败(dup: %s) 在子进程中将从 pty 改为标准输出失败(dup: %s) “RS”设置为多字符是 gawk 扩展 多阶双向管道无法工作 未显式关闭合作进程“%s” 未显式关闭文件“%s” 未显式关闭管道“%s” 未显式关闭端口“%s” “\x”转义序列中没有十六进制数 文件描述符 %d 未被打开 完全没有程序正文！ 在 END 环境中，非重定向的“getline”未定义 把“FS”设为空字符串是 gawk 扩展 老 awk 不支持多维数组 老的 awk 不支持多个“BEGIN”或“END”规则 老 awk 不支持操作符“**” 老 awk 不支持操作符“**=” 老 awk 不支持把“FS”设置为正则表达式 老 awk 不支持“\%c”转义序列 老 awk 只支持关键词“in”在“for”的后面 老 awk 不支持操作符“^” 老 awk 不支持操作符“^=” 刷新管道“%s”时出错(%s)。 在 BEGIN 或 END 规则中，“print”也许应该写做“print ""” printf: 没有参数 打开的文件数达到系统限制: 开始复用文件描述符 未知原因 引用未初始化的参数“%s” 引用未初始化的字段“$%d” 引用未初始化的变量“%s” 正则表达式常量“/%s/”看起来像 C 注释，但其实不是 正则表达式常量“//”看起来像 C++ 注释，但其实不是 第 %d 个参数的正则表达式常量产生布尔值 正则表达式在“~”或“!~”算符的左边 正则表达式在赋值算符的右边 正则表达式在比较算符的右边 远程主机和端口信息 (%s, %s) 无效 在父进程中恢复标准输入失败
 在父进程中恢复标准输出失败
 rshift: 第一个参数不是数字 rshift: 第二个参数不是数字 以设置 root ID 方式运行“%s”可能存在安全漏洞 发送配置到标准错误输出 shadow_funcs() 被调用两次！ sin: 收到非数字参数 源文件“%s”为空 源文件不以换行符结束 split: 第三个参数为空是 gawk 扩展 split: 第二个参数不是数组 sqrt: 收到负数参数 %g sqrt: 收到非数字参数 srand: 收到非数字参数 标准输出 strftime: 收到空格式字符串 strftime: 第二个参数不是数字 strftime: 第一个参数不是字符串 substr: 在开始坐标 %2$g 下长度 %1$g 超出第一个参数的长度 (%3$lu) substr: 长度 %g 小于 0 substr: 长度 %g 小于 1 substr: 长度 %g 作为字符串坐标过大，截断至 %g substr: 非整数的长度 %g 会被截断 substr: 非整数的开始坐标 %g 会被截断 substr: 源字符串长度为0 substr: 开始坐标 %g 无效，使用 1 substr: 开始坐标 %g 超出字符串尾部 system: 收到非字符串参数 tawk 正则表达式修饰符“/.../%c”无法在 gawk 中工作 那里有被掩盖的变量。 tolower: 收到非字符串参数 相对格式来说参数个数过多 打开过多管道或输入文件 toupper: 收到非字符串参数 由于对“LINT”赋值所以关闭“--lint” 未预期的新行或字符串结束 未知的结点类型 %d 在文件“%.*s”中不必要的混合使用“>”和“>>” 未终止的正则表达式 未终止的正则表达式在文件结束处 未终止的正则表达式在文件结束处以“\”结束 未结束的字符串 使用“\ #...”来续行是不可移植的 使用 dcgettext(_"...") 是错误的: 去掉开始的下划线 使用 dcngettext(_"...") 是错误的: 去掉开始的下划线 警告:  