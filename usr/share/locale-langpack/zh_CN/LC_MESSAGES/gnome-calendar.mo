��    x      �  �   �      (
     )
     I
     O
     U
     k
     q
     z
     |
     
     �
     �
     �
     �
     �
  *   �
     �
               !     <     M     _     n     �     �     �  	   �     �     �     �     �  $   �  )        :     >     K     \     s     x     �     �  �   �     Q     _     l  �   x     W     ^     {     �     �     �     �     �     �     �  	   �                          $     '     7      <  &   ]     �     �     �     �     �     �     �     �     �            '   &     N     U     [     a     x  >   �     �     �     �  )   �       �   "     �     �     
          5     A     a     f     m  
   s     ~     �     �     �     �     �     �          /     J     d     ~     �     �     �     �               1  D  D     �     �     �     �     �     �     �     �     �     �     �     �            0   /     `     g     �     �     �     �     �     �     �     �  -   �           '     .     ;     B  !   I  (   k  	   �     �     �     �     �     �     �     �  {   �     u     �     �  �   �     &     -     @     G     T     d     k     r     �     �     �     �     �     �     �     �     �     �  !   �  !   !     C     \     k     x          �     �     �     �     �     �  $   �     �                     $  6   7     n     u     �  $   �     �  �   �     d     t     �      �     �      �     �  
   �  
   �       
             "     +     8     ?     F  	   M  
   W  	   b     l     y     �  	   �     �     �     �  	   �  �   �                   C   .       p   a   #   i   E      "   2   9   8                 &   +       *   n                       t      N   P      V   K   
          ]   G   m                      A   o   5   \   U              -   l   w   s              7      x       ?           g   Z           /   1   >   (      r         M       W              '   _   ^      e       [           B   Y      @           %       k      X   :   L   Q   F   b   )   c   J   v       O       H   $       <       f   0   ;       6   `   !   S               	   ,   j   R           =   q      3      T      D       d                 4   u   I   h    %s (this calendar is read-only) %s AM %s PM - Calendar management 00:00 00:00 PM : AM Access, and manage calendar Account Add Add Calendar Add Eve_nt… Add Event… Add new events to this calendar by default All day Another event deleted Calendar Calendar <b>%s</b> removed Calendar Address Calendar Settings Calendar files Calendar for GNOME Calendar management Calendar name Calendar;Event;Reminder; Calendars Cancel Click to set up Color Connect Copyright © %d The Calendar authors Copyright © %d–%d The Calendar authors Day Delete event Display calendar Display version number Done Edit Calendar Edit Details… Ends Enter the address of the calendar that you want to add. If the calendar belongs to one of your online accounts, you can add it through the <a href="GOA">online account settings</a>. Event deleted From File… From Web… GNOME Calendar is a simple and beautiful calendar application designed to perfectly fit the GNOME desktop. By reusing the components which the GNOME desktop is build on, Calendar nicely integrates with the GNOME ecosystem. Google List of the disabled sources Location Manage your calendars Microsoft Exchange Midnight Month New Event from %s to %s New Event on %s New Local Calendar… No events No results found Noon Notes Off On Online Accounts Open Open calendar on the passed date Open calendar showing the passed event Open online account settings Other event Other %d events Other events Overview PM Password Remove Calendar Save Search for events Select a calendar file Settings Sources disabled last time Calendar ran Starts Title Today Try a different search Type of the active view Type of the active window view, default value is: monthly view Undo Unnamed Calendar Unnamed event Use the entry above to search for events. User We aim to find the perfect balance between nicely crafted features and user-centred usability. No excess, no lacks. You'll feel comfortable using Calendar, like you've been using it for ages! Window maximized Window maximized state Window position Window position (x and y). Window size Window size (width and height). Year _About _Quit _Search… _Synchronize event date format%B %d ownCloud shortcut windowClose window shortcut windowGeneral shortcut windowGo back shortcut windowGo forward shortcut windowMonth view shortcut windowNavigation shortcut windowNew event shortcut windowNext view shortcut windowPrevious view shortcut windowSearch shortcut windowShortcuts shortcut windowShow help shortcut windowShow today shortcut windowView shortcut windowYear view translator-credits Project-Id-Version: gnome-calendar master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-calendar&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-05-18 08:25+0000
PO-Revision-Date: 2016-04-18 21:49+0000
Last-Translator: shijing <jingshi@ubuntukylin.com>
Language-Team: Chinese (China) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2016-06-27 19:34+0000
X-Generator: Launchpad (build 18115)
Language: zh_CN
 %s (这个日历是只读的) %s AM %s PM - 日历管理 00:00 00:00 PM : 上午 访问并管理日历 账户 添加 添加日历 添加事件(_N)... 添加事件... 默认情况下将新事件添加到该日历中 全天 另一个事件已删除 日历 日历<b>%s</b> 已被删除 日历地址 日历设置 日历文件 GNOME 日历 日历管理 日历名称 Calendar;Event;Reminder;日历;事件;提醒; 日历 取消 点击设置 颜色 连接 版权所有© %d 个日历作者 版权所有 © %d–%d  个日历作者 工作日 删除事件 显示日历 显示版本号 完成 编辑日历 编辑细节… 结束时间 输入您要添加的日历地址。如果您是在线账户，可以通过<a href="GOA">在线帐户设置</a>来添加。 活动已删除 从文件… 从网络… GNOME 日历设计简洁美观，符合 GNOME 桌面风格。通过重用组件，日历能够很好地与 GNOME 生态系统结合。 Google 已禁用源列表 位置 管理日程 微软 Exchange 午夜 月份 新建活动：%s~%s 新建活动时间：%s 新本地日历... 没有事件 未发现结果 中午 备注 关闭 开启 在线账户 打开 打开日历显示过去的日期 打开日历显示过去的事件 打开在线账户设置 其他%d事件 其他事件 概述 下午 密码 删除日历 保存 搜索事件 选择一个日历文件 设置 上次运行日历时所禁用的源 开始时间 标题 今天 尝试不同的搜索 活动视图类型 活动窗口视图类型，默认值为：月份视图 撤消 未命名日历 未命名事件 使用上面的条目搜索事件。 用户 我们的目标是找到很好的功能和用户为中心的易用性之间的完美平衡。既不多余，也不缺乏。使用日历，您会感觉很舒服！ 窗口最大化 窗口最大化状态 窗口位置 窗口位置(水平和竖直)。 窗口大小 窗口大小(宽度和高度)。 年份 关于(_A) 退出(_Q) 搜索(_S)... 同步(_S) %B %d ownCloud 关闭窗口 通用 返回 前进 月视图 导航 、 新事件 下一视图 上一视图 搜索 快捷键 显示帮助 显示今天 视图 年视图 tuhaihe <1132321739qq@gmail.com>, 2013

Launchpad Contributions:
  MR.wangmaoxin https://launchpad.net/~ddllkl
  Wang Dianjin https://launchpad.net/~tuhaihe
  YunQiang Su https://launchpad.net/~wzssyqa
  shijing https://launchpad.net/~shijing 