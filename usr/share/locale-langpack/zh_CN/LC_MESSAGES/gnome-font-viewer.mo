��            )         �     �  	   �     �  	   �     �     �     �     �  �     �   �     �     �     �  	   �     �     �  E   �     �          !     '     ,  !   L     n     �     �     �  	   �     �     �    �                          "     )     C     S  �   i  �   ,	     �	     �	     �	  	   �	      
     
  E   
     T
     [
     n
     u
  +   |
     �
     �
     �
     �
     �
  	   	       F  *                                                                                  	      
                                                About All Fonts Back Copyright Description FONT-FILE OUTPUT-FILE Font Viewer GNOME Font Viewer GNOME Font Viewer also supports installing new font files downloaded in the .ttf and other formats. Fonts may be installed only for your use or made available to all users on the computer. GNOME Font Viewer shows you the fonts installed on your computer for your use as thumbnails. Selecting any thumbnails shows the full view of how the font would look under various sizes. Info Install Install Failed Installed Name Quit Run '%s --help' to see a full list of available command line options. SIZE Show the application's version Style TEXT Text to thumbnail (default: Aa) This font could not be displayed. Thumbnail size (default: 128) Type Version View fonts on your system [FILE...] fonts;fontface; translator-credits Project-Id-Version: gnome-utils master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-font-viewer&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 22:21+0000
PO-Revision-Date: 2015-12-04 14:44+0000
Last-Translator: Cheng Lu <chenglu1990@gmail.com>
Language-Team: Chinese (China) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:16+0000
X-Generator: Launchpad (build 18115)
Language: zh_CN
 关于 全部字体 返回 版权 描述 字体文件 输出文件 字体查看器 GNOME 字体查看器 GNOME 字体查看器还支持安装新字体，支持下载的 ttf 或其他格式的字体文件。字体可以只为您一人安装使用，亦可为使用此计算机的所有用户安装。 GNOME 字体查看器以缩略图的形式显示了安装在您系统中的字体。选择任意一个缩略图可以完整查看该字体在不同字号下的显示效果。 信息 安装 安装失败 已安装 名称 退出 运行“%s --help”来查看可用命令行选项的完整列表。 大小 显示应用版本 样式 文字 生成缩略图所用的文字(默认：Aa) 该字体无法显示。 缩略图大小(默认：128) 类型 版本 查看您系统中的字体 [FILE...] fonts;fontface;字体; Cheng Lu <chenglu1990@gmail.com>, 2012, 2014
tuhaihe <1132321739qq@gmail.com>, 2013
Tong Hui <tonghuix@gmail.com>, 2013, 2014

Launchpad Contributions:
  Aron Xu https://launchpad.net/~happyaron
  Cheng Lu https://launchpad.net/~dawndiy
  Dingyuan Wang https://launchpad.net/~gumblex
  Tong Hui https://launchpad.net/~tonghuix 