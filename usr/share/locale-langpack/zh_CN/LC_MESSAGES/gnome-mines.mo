��    L      |  e   �      p  $   q     �     �  	   �     �  
   �     �  #   �  #        :     @     G      L     m  !        �    �     �  <   �     	  
    	     +	     8	     D	     J	  "   S	     v	     �	     �	     �	     �	     �	     �	     �	  ;   
  S   A
  2   �
  [   �
  /   $     T     Z  
   f     q     }  $   �     �  #   �     �     �     �          -  �   K                    "  	   )     3     ;     A  	   H     R     V     ]  
   i     t     z     �     �     �     �     �     �     �  4  �     )     =  
   N     Y     f     s     �     �  !   �     �  	   �     �     �     	          /  �   <  !   '  3   I     }     �     �     �     �  	   �     �     �  	   �               ,     H     `     h  4   u  H   �  '   �  T     '   p  	   �     �     �     �     �     �     �  !        1     8     I     \  !   r  �   �  
   %     0  
   A  
   L  
   W  
   b  
   m  
   x     �  
   �  
   �     �     �  
   �  
   �  
   �     �     �  
          @  8      y     C   D                !   *   <   E          J      	       G   :   L              +      &      ,                    #   )   3   $               "         @   /             2   H       A      1              '      I   F   .   >                    0   K   ?   6            (               7   9                     B       ;   =       4   5       
             %   -   8    %u × %u, %u mine %u × %u, %u mines <b>%d</b> mine <b>%d</b> mines A_ppearance Big board Big game Board size Change _Difficulty Clear explosive mines off the board Clear hidden mines from a minefield Close Custom Date Do you want to start a new game? Enable animations Enable automatic placing of flags GNOME Mines GNOME Mines is a puzzle game where you search for hidden mines. Flag the spaces with mines as quickly as possible to make the board a safer place. You win the game when you've flagged every mine on the board. Be careful not to trigger one, or the game is over! Height of the window in pixels If you start a new game, your current progress will be lost. Keep Current Game Main game: Medium board Medium game Mines New Game Number of columns in a custom game Number of rows in a custom game Paused Percent _mines Play _Again Print release version and exit Resizing and SVG support: Score: Select Theme Set to false to disable theme-defined transition animations Set to true to automatically flag squares as mined when enough squares are revealed Set to true to be able to mark squares as unknown. Set to true to enable warning icons when too many flags are placed next to a numbered tile. Size of the board (0-2 = small-large, 3=custom) Size: Small board Small game St_art Over Start New Game The number of mines in a custom game The theme to use The title of the tile theme to use. Time Use _animations Use the unknown flag Warning about too many flags Width of the window in pixels You can select the size of the field you want to play on at the start of the game. If you get stuck, you can ask for a hint: there's a time penalty, but that's better than hitting a mine! _About _Best Times _Cancel _Close _Contents _Height _Help _Mines _New Game _OK _Pause _Play Again _Play Game _Quit _Resume _Scores _Show Warnings _Use Question Flags _Width minesweeper; translator-credits true if the window is maximized Project-Id-Version: gnome-games master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-mines&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-02-11 17:13+0000
PO-Revision-Date: 2016-04-14 09:00+0000
Last-Translator: liushuyu <Unknown>
Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2016-06-27 19:21+0000
X-Generator: Launchpad (build 18115)
Language: zh_CN
 %u × %u, %u 颗雷 <b>%d</b> 个雷 外观(_P) 大号版面 大型游戏 盘面大小 改变难度(_D) 从板上清除爆炸的地雷 扫除雷区里的隐藏的地雷 关闭 自定义 日期 您想要启动新游戏吗？ 启用动画 启用旗标自动放置 GNOME 扫雷 GNOME 扫雷是一款寻找隐藏地雷的解谜游戏。以最快的速度给棋盘上有雷的地方插上旗，让棋盘变安全。棋盘上所有雷都插上旗时游戏胜利。小心别点到雷哦，点到游戏就结束了！ 窗口的高度，以像素计。 如果开始新游戏，您当前进度将丢失。 保存当前游戏 主游戏： 中等版面 中等游戏 扫雷 新游戏 自定义游戏的列数 自定义游戏的行数 已暂停 地雷百分比(_M)： 再玩一遍(_A) 显示发行版本并推出 缩放和 SVG 支持： 得分: 选择主题 设置为 false 以禁用主题定义的过渡动画 设定为 True 可在已揭开足够的方块时自动标记方块为雷 设为 true 可将方块标为未知。 设为 true 可在某个数字块附近放置了太多旗标时启用警告图标。 盘面大小(0-2=小-大，3=自定义) 大小： 小号板面 小型游戏 重开一局(_A) 开始新游戏 自定义游戏中雷的数目 要使用的主题 要使用的牌面主题名称。 时间 使用动画(_A) 使用未知旗标 旗标过多时警告 窗口的宽度，以像素计。 您可以在游戏启动时选择棋盘大小。陷入僵局时，您可以寻求提示：使用提示会有惩罚，但总比踩到雷好嘛！ 关于(_A) 最快时间(_B) 取消(_C) 关闭(_C) 目录(_C) 高度(_H) 帮助(_H) 扫雷(_M) 新建游戏(_N) 确认(_O) 暂停(_P) 再玩一遍(_P) 玩游戏(_P) 退出(_Q) 继续(_R) 得分(_S) 显示警告(_S) 使用问号旗标(_U) 宽度(_W) minesweeper;挖雷;扫雷; Yang Zhang <zyangmath@gmail.com>, 2007
Ping Z <zpsigma@gmail.com>, 2007
Xhacker Liu <liu.dongyuan@gmail.com>, 2010
Aron Xu <aronxu@gnome.org>, 2011
YunQiang Su <wzssyqa@gmail.com>, 2011, 2012
甘露(Gan Lu) <rhythm.gan@gmail.com>, 2013
Tong Hui (tonghuix@gmail.com), 2014

Launchpad Contributions:
  Rockworld https://launchpad.net/~rockrock2222222
  Wylmer Wang https://launchpad.net/~wantinghard
  YunJian https://launchpad.net/~tld5yj
  YunQiang Su https://launchpad.net/~wzssyqa
  liushuyu https://launchpad.net/~liushuyu-011-y
  tuhaihe https://launchpad.net/~wangdianjin 如果窗口最大化则为 true 