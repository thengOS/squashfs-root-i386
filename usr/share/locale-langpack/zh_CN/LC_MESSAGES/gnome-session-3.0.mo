��    I      d  a   �      0     1  4  N     �  R   �  g   �  Z   W	  '   �	     �	     �	     �	     
  	   '
  1   1
  	   c
  	   m
  (   w
  )   �
     �
     �
     �
  #      '   $  "   L     o     �     �     �     �     �     �     �      �          	             !   /  '   Q  %   y  	   �     �  '   �     �  P   �     1     H     X     e  
   t  ;     3   �  Q   �     A     P  &   g     �      �  #   �      �  *   	      4      U  /   v     �  =   �  	         
               #  (   0     Y  �  i       �  0     ,  ?   C  K   �  9   �      	     *     =     P     m  	   z  '   �     �     �     �  %   �       	   &     0     @  $   Y     ~     �     �     �     �     �     �     �     �            	   %  	   /  	   9     C  !   Y  *   {     �     �  !   �     �  4   �          (     <     I  	   V  (   `  %   �  ;   �     �     �  *        <     O     k     �  -   �     �     �  0        4  4   M  
   �  
   �  
   �     �     �  %   �     �         )      +   I   E   A       #   D   =      "          ;          C   *           0      /              H            9   4      	      
              >   <   8       $           -                   B   ,          3       ?              :   &          5   2   7                @          6                           G   1      '   %   F   .   !       (     - the GNOME session manager %s [OPTION...] COMMAND

Execute COMMAND while inhibiting some session functionality.

  -h, --help        Show this help
  --version         Show program version
  --app-id ID       The application id to use
                    when inhibiting (optional)
  --reason REASON   The reason for inhibiting (optional)
  --inhibit ARG     Things to inhibit, colon-separated list of:
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    Do not launch COMMAND and wait forever instead

If no --inhibit option is specified, idle is assumed.
 %s requires an argument
 A problem has occurred and the system can't recover.
Please log out and try again. A problem has occurred and the system can't recover. All extensions have been disabled as a precaution. A problem has occurred and the system can't recover. Please contact a system administrator A session named ‘%s’ already exists AUTOSTART_DIR Add Startup Program Additional startup _programs: Allow logout Browse… Choose what applications to start when you log in Co_mmand: Comm_ent: Could not connect to the session manager Could not create ICE listening socket: %s Could not display help document Custom Custom Session Disable hardware acceleration check Do not load user-specified applications Don't prompt for user confirmation Edit Startup Program Enable debugging code Enabled Failed to execute %s
 GNOME GNOME dummy GNOME on Wayland Icon Ignoring any existing inhibitors Log out No description No name Not responding Oh no!  Something has gone wrong. Override standard autostart directories Please select a custom session to run Power off Program Program called with conflicting options Reboot Refusing new client connection because the session is currently being shut down
 Remembered Application Rena_me Session SESSION_NAME Select Command Session %d Session names are not allowed to contain ‘/’ characters Session names are not allowed to start with ‘.’ Session names are not allowed to start with ‘.’ or contain ‘/’ characters Session to use Show extension warning Show the fail whale dialog for testing Startup Applications Startup Applications Preferences The startup command cannot be empty The startup command is not valid This entry lets you select a saved session This program is blocking logout. This session logs you into GNOME This session logs you into GNOME, using Wayland Version of this application _Automatically remember running applications when logging out _Continue _Log Out _Log out _Name: _New Session _Remember Currently Running Applications _Remove Session Project-Id-Version: gnome-session master
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-05-18 08:40+0000
PO-Revision-Date: 2015-07-10 03:30+0000
Last-Translator: Tong Hui <Unknown>
Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 16:40+0000
X-Generator: Launchpad (build 18115)
Language: 
  - GNOME 会话管理器 %s [选项...] 命令

执行 命令 同时禁用一些会话功能。

  -h, --help        显示此帮助
  --version         显示程序版本
  --app-id ID       禁用时使用的应用程序 ID(可选)
  --reason 原因     禁用原因(可选)
  --inhibit 参数    要禁用的功能，以英文冒号分隔，可包括：
                    logout, switch-user, suspend, idle, automount
  --inhibit-only    不启动 命令，而是无限等待

若未指定 --inhibit 参数，将假设为 idle
 %s 需要一个参数
 系统出错并无法恢复，请尝试注销并重新登录。 系统出错且无法恢复。为预防起见，已禁用了所有扩展。 系统出错且无法恢复。请联系系统管理员。 名为“%s”的会话已存在 自动启动目录 添加启动程序 额外的启动程序(_P)： 允许注销 浏览... 选择登录时要启动的应用程序 命令(_M)： 注释(_E)： 无法连接到会话管理器 无法创建 ICE 监听套接字：%s 不能显示帮助文档 自定义 自定义会话 禁用硬件加速检测 不装入用户指定的应用程序 不提示用户确认 编辑启动程序 启用调试代码 启用 执行 %s 失败
 GNOME GNOME dummy 基于 Wayland 的 GNOME 图标 忽略现有限制因素 注销 无描述 无名称 无响应 糟糕！出错啦！ 取代默认的自动启动目录 请选择一个要运行的自定义会话 关机 程序 调用程序的选项互相冲突 重启 拒绝新客户连接，原因是会话正在关闭
 记住的应用程序 重命名会话(_M) 会话名称 选择命令 会话 %d 会话名称不允许包含“/”字符 会话名称不允许以“.”开头 会话名称不允许以“.”开头或包含“/”字符 要使用的会话 显示扩展警告 显示用于测试的鲸鱼失败对话框 启动应用程序 启动应用程序首选项 启动命令不能为空 启动命令无效 此项允许您选择一个已保存的会话 此程序正在阻止注销。 此会话将您登录到 GNOME 此会话将您登录到使用 Wayland 的 GNOME 此应用程序的版本 注销时自动记住正在运行的应用程序(_A) 继续(_C) 注销(_L) 注销(_L) 名称(_N)： 新建会话(_N) 记录当前运行的应用程序(_R) 移除会话(_R) 