Þ    C      4  Y   L      °     ±     Æ  ?   à           %  ,   6     c     h     {                    ¶  4   Ó          #     >     N     _     o     |               °     ¸     ¼  .   Ñ                   C   %     i     }  `   	     x	     	  w   	     
  P   "
     s
     
  î   °
          ¸     Ê  	   é     ó           
     +     E     U     b     t            	   ¤     ®     ¶  
   ½     È     Í     Ô     Ý     ã     ç  ´  ë           ­  <   º     ÷     þ  *        E     J     `     t                 -   ¢     Ð     æ     ü          
                    2  	   E     O     S  8   j  "   £     Æ     Ê  E   Î          %  Q   ®             x          T         õ       ö   0     '     :     M  	   i  	   s     }                ³     Ã  	   Ê     Ô     Û     ø  	   ý            
              %     ,     5     <     C            0   @   5                C       B   #       +                 :   $   7       9          	          ?            '          4          "      =                  &   6           ,                !       
   -   3   ;   8           A      <       2       /      *               (       1   .      >      %         )    Add phrases in front Add phrases in the front. Always input numbers when number keys from key pad is inputted. Auto Auto move cursor Automatically move cursor to next character. Big5 Candidate per page Chewing Chewing component Chi Chinese chewing input method Choose phrases from backward Choose phrases from the back, without moving cursor. Click to switch to Chinese Click to switch to English ConfigureApply ConfigureCancel ConfigureClose ConfigureOk ConfigureSave Easy symbol input Easy symbol input. Editing Eng Esc clean all buffer Escape key cleans the text in pre-edit-buffer. Force lowercase in En mode Full Half Hsu's keyboard selection keys, 1 for asdfjkl789, 2 for asdfzxcv89 . Hsu's selection key Ignore CapsLock status and input lowercase by default.
It is handy if you wish to enter lowercase by default.
Uppercase can still be inputted with Shift. In plain Zhuyin mode, automatic candidate selection and related options are disabled or ignored. Keyboard Keyboard Type Keys used to select candidate. For example "asdfghjkl;", press 'a' to select the 1st candidate, 's' for 2nd, and so on. Maximum Chinese characters Maximum Chinese characters in pre-edit buffer, including inputing Zhuyin symbols Number of candidate per page. Number pad always input number Occasionally, the CapsLock status does not match the IM, this option determines how these status be synchronized. Valid values:
"disable": Do nothing.
"keyboard": IM status follows keyboard status.
"IM": Keyboard status follows IM status. Peng Huang, Ding-Yi Chen Plain Zhuyin mode Select Zhuyin keyboard layout. Selecting Selection keys Setting Shift key to toggle Chinese Mode Shift toggle Chinese Mode Space to select Syncdisable Syncinput method Synckeyboard Sync between CapsLock and IM UTF8 dachen_26 default dvorak dvorak_hsu eten eten26 gin_yieh hanyu hsu ibm Project-Id-Version: ibus-chewing 1.4.11
Report-Msgid-Bugs-To: Ding-Yi Chen <dchen at redhat.com>
POT-Creation-Date: 2016-01-21 05:14+0000
PO-Revision-Date: 2015-09-04 18:12+0000
Last-Translator: Pany <Unknown>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:39+0000
X-Generator: Launchpad (build 18115)
Language: zh-CN
 åæ¹å è¯ åæ¹å è¯ å¨ä½¿ç¨æ°å­é®ççæé®è¾å¥æ¶æ»æ¯è¾å¥æ°å­ã èªå¨ èªå¨ç§»æ¸¸æ ä¸ä¸ä¸ªå­ èªå¨å°åæ ç§»å¨å°ä¸ä¸ä¸ªå­ç¬¦ã Big5 æ¯é¡µä¸­çåéè¯ é·é³ï¼Chewingï¼ é·é³ç»ä»¶ ä¸­ é·é³è¾å¥æ³ åååè¯ å¨ä¸ç§»å¨åæ çæåµä¸åååè¯ã åæ¢å°ä¸­ææ¨¡å¼ åæ¢å°è±ææ¨¡å¼ åºç¨ åæ¶ å³é­ ç¡®è®¤ ä¿å­ è½»æ¾è¾å¥ç¬¦å· è½»æ¾è¾å¥ç¬¦å· ç¼è¾ä¸­ è± Esc æ¸é¤ææç¼å­ æ Esc é®å¯æ¸é¤ææé¢ç¼è¾ç¼å²ä¸­çææ¬ã å¨ En æ¨¡å¼ä¸­å¼ºå¶ä½¿ç¨å°å å¨ å Hsu çé®çéæ©é®ï¼1 ä»£è¡¨ asdfjkl789ï¼2 ä»£è¡¨ asdfzxcv89ã Hsu çéæ©é® å¿½ç¥ CapsLock ç¶æï¼é»è®¤è¾å¥å°åã
å¦ææ¨å¸æé»è®¤è¾å¥å°åå°±å¾æ¹ä¾¿ã
æ¨ä»å¯ä½¿ç¨ Shift é®è¾å¥å¤§åã å¨ç®åæ³¨é³æ¨¡å¼ä¸­ï¼ç¦ç¨æå¿½ç¥èªå¨åéè¯éæ©åç¸å³éé¡¹ã é®ç é®çç±»å ç¨æ¥éæ©åéè¯çé®ãä¾å¦ï¼"asdfghjkl;"ï¼æ 'a' éæ©ç¬¬ä¸ä¸ªè¯ï¼'s' ä»£è¡¨ç¬¬äºä¸ªï¼ä¾æ¬¡ç±»æ¨ã æå¤ä¸­æå­ç¬¦ å¨é¢ç¼è¾ç¼å²ä¸­æå¤çä¸­æå­ç¬¦æ°ï¼å¶ä¸­åæ¬è¾å¥çæ³¨é³ç¬¦å·ã æ¯é¡µä¸­çåéè¯æ°ã æ°å­é®çæ»æ¯è¾å¥æ°å­ å¶å°ä¹æ CapsLock ç¶æä¸è¾å¥æ³ä¸ç¬¦çæåµï¼è¿ä¸ªéé¡¹å¯å³å®å¦ä½åæ­¥è¿äºç¶æãææå¼ä¸ºï¼
"disable"ï¼ä»ä¹é½ä¸åã
"keyboard"ï¼è¾å¥æ³ç¶æéµå¾ªé®çç¶æã
"IM"ï¼é®çç¶æéµå¾ªè¾å¥æ³ç¶æã é»é¹ï¼é³å®å½ ç®åæ³¨é³æ¨¡å¼ éæ©æ³¨é³é®çå¸å±ã éæ©ä¸­ éæ©é® è®¾ç½® Shift é®åæ¢ä¸­ææ¨¡å¼ Shift åæ¢ä¸­æ æç©ºæ ¼éæ© ç¦ç¨ è¾å¥æ³ é®ç åæ­¥ CapsLock åè¾å¥æ³ UTF8 dachen_26 é»è®¤ dvorak dvorak_hsu eten eten26 gin_yieh ä¸­æ è®¸æ° ibm 