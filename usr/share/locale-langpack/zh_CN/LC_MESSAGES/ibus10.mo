��    �      �  �         �     �     �     �     �  #   �          0     J  k   Y  �   �     v  <   |     �     �     �  	   �     �     �            
   6  H   A     �     �     �  #   �     �     �               !     -     2  (   E  (   n  @   �     �  D   �     <     O     d     l     z  9   �     �     �     �  
   �  
             "  .   .  �   ]  0   !  �   R     �     �  	   �  M   �     <     Q     d     |     �  !   �     �  %   �  (     3   *  
   ^     i  @   q  C   �     �          #     6  9   R     �  )   �  ^   �  0        L  .   X     �  %   �     �     �  &   �               1     O  ?   o     �     �     �     �  J   �  (   B     k     �     �     �     �     �  �   �  9   �  1   �  2   +     ^     u     �  -   �     �  &   �  @     '   R     z  '   �     �  "   �     �            &   /  O   V  �   �  '   L   Q   t   �   �      Q!  �   q!  @   �!  D   @"  D   �"  <   �"  .   #  -   6#  4   d#     �#     �#     �#  /   �#  C    $     D$     d$  '   t$     �$     �$  >   �$      %     %%     @%     L%     U%      t%  �  �%     }'     �'     �'     �'     �'     �'     �'     �'     �'     �'     �'     �'  �   �'     u(     �(     �(  �  �(     �*     �*     �*     �*     �*     �*     �*     
+  c   +  |   |+     �+  0    ,     1,     8,     ?,     L,  	   Y,  	   c,     m,     �,  
   �,  J   �,     �,  	   -     -     -     8-  
   Q-     \-  	   l-     v-     �-     �-  0   �-  0   �-  <   �-     <.  9   R.  	   �.     �.  	   �.     �.     �.  3   �.     /     /     (/     5/  
   </     G/     V/  $   b/  �   �/  V   A0  u   �0  	   1     1  	   1  :   &1     a1  	   t1     ~1     �1     �1     �1     �1  #   �1  #   2  ,   )2     V2  	   c2     m2     �2     �2     �2     �2     �2  /   �2     %3  6   ,3  p   c3  *   �3  	   �3  +   	4     54      E4     f4     4  ,   �4     �4     �4     �4     �4  0   5     E5     X5     h5     u5  Q   �5  *   �5     6     6     +6     B6     X6     u6  �   �6  -   N7     |7  0   �7     �7     �7     �7     8     -8  !   C8  6   e8  $   �8     �8  $   �8     �8     9     (9     89     H9  *   ]9  O   �9  �   �9  -   o:  Q   �:  _   �:     O;  `   f;  !   �;  !   �;  -   <  -   9<     g<     �<  !   �<  	   �<  	   �<     �<  )   �<  <   =      L=     m=  !   �=     �=     �=  =   �=  !   >     4>     M>     \>     c>     ~>  �  �>  
   s@  
   ~@  
   �@  
   �@  
   �@  
   �@  
   �@  
   �@     �@  
   �@  
   �@     �@  w   �@     sA     �A    �A     A   (   ?   &   �       I   �   H   y   D      g   E          �   1   �       �          *   r   �   z       �       �   q       3       {   5           �   _   #   T   [          
   +   �   F   `   n   d   �       �   >       @   %   -   u   �      x       �              )   ,          L   �   8   m   �   !      �       Q   �   �           p       �       7   X                     ]   U   w      W   O   2   �   C   N   ;      c   =   �       �           �           �       �      v      M   �      :   }       �           G   �   0       ^      e   P   �                      �   J                      �   o   Y       Z   S           k   ~       �      |         �   �   �          R   �       /   i       \   "   <   l           6   B   �   �       .         j   h   4   b   t   K             s      9   �   �       $          �   �   �   �   �   V   �       a           	      �   f   '    %s is unknown command!
 (Not implemented) ... <b>Font and Style</b> <b>Global input method settings</b> <b>Keyboard Layout</b> <b>Keyboard Shortcuts</b> <b>Startup</b> <big><b>IBus</b></big>
<small>The intelligent input bus</small>
Homepage: http://code.google.com/p/ibus



 <small><i>The active input method can be switched around from the selected ones in the above list by pressing the keyboard shortcut keys or clicking the panel icon.</i></small> About Add the selected input method into the enabled input methods Advanced Always Author: %s
 Auto hide Bottom left corner Bottom right corner Can't connect to IBus.
 Candidates orientation: Commands:
 Copyright (c) 2007-2010 Peng Huang
Copyright (c) 2007-2010 Red Hat, Inc. Create registry cache Custom Custom font Custom font name for language panel DConf preserve name prefixes Description:
 Disable shortcut keys Disable: Do not show Done Embed Preedit Text Embed Preedit Text in Application Window Embed preedit text in application window Embed the preedit text of input method in the application window Enable input method by default Enable input method by default when the application gets input focus Enable or disable: Enable shortcut keys Enable: Engines order Exit ibus-daemon Follow the input cursor in case the panel is always shown General Get global engine failed.
 Hide automatically Horizontal IBus Panel IBus Preferences IBus Update IBus daemon could not be started in %d seconds IBus has been started! If you can not use IBus, please open System Menu -&gt; System Settings -&gt; Language Support and set the "Keyboard Input Method" to "ibus", then log out and back in again. IBus is an intelligent input bus for Linux/Unix. If true, the panel follows the input cursor in case the panel is always shown. If false, the panel is shown at a fixed location. Input Method Kbd Key code: Keyboard Input Methods (IBus Daemon) is not running. Do you wish to start it? Keyboard layout: %s
 Keyboard shortcuts Language panel position Language panel position: Language: %s
 Latin layouts which have no ASCII List engine name only List of Asian languages on ibus-setup List of European languages on ibus-setup List of system keyboard layout groups on ibus-setup Modifiers: More… Move down the selected input method in the enabled input methods Move up the selected input method in the enabled input methods list Next engine shortcut keys Next input method: No engine is set.
 Orientation of lookup table Orientation of lookup table. 0 = Horizontal, 1 = Vertical Other Please press a key (or a key combination) Please press a key (or a key combination).
The dialog will be closed when the key is released. Popup delay milliseconds for IME switcher window Preferences Prefixes of DConf keys to stop name conversion Preload engines Preload engines during ibus starts up Prev engine shortcut keys Previous input method: Print the D-Bus address of ibus-daemon Quit RGBA value of XKB icon Read the registry cache FILE. Read the system registry cache. Remove the selected input method from the enabled input methods Reset the configuration values Resetting… Restart Restart ibus-daemon Run xmodmap if .xmodmap or .Xmodmap exists when ibus engines are switched. Saved engines order in input method list Saved version number Select an input method Select keyboard shortcut for %s Set IBus Preferences Set global engine failed.
 Set or get engine Set popup delay milliseconds to show IME switcher window. The default is 400. 0 = Show the window immediately. 0 &lt; Delay milliseconds. 0 &gt; Do not show the window and switch prev/next engines. Set the behavior of ibus how to show or hide language bar Set the orientation of candidates in lookup table Share the same input method among all applications Show all input methods Show available engines Show icon on system tray Show information of the selected input method Show input method name Show input method name on language bar Show input method's name on language bar when check the checkbox Show only input methods for your region Show property panel: Show setup of the selected input method Show the configuration values Show the content of registry cache Show this information Show version Start ibus on login Super+space is now the default hotkey. The behavior of property panel. 0 = Do not show, 1 = Auto hide, 2 = Always show The group list is used not to show all the system keyboard layouts by default. The list item will be appended at the end of gconf key. e.g. .../xkblayoutconfig/item1 The milliseconds to show property panel The milliseconds to show property panel after focus-in or properties are changed. The position of the language panel. 0 = Top left corner, 1 = Top right corner, 2 = Bottom left corner, 3 = Bottom right corner, 4 = Custom The registry cache is invalid.
 The saved version number will be used to check the difference between the version of the previous installed ibus and one of the current ibus. The shortcut keys for switching to next input method in the list The shortcut keys for switching to previous input method in the list The shortcut keys for switching to the next input method in the list The shortcut keys for switching to the previous input method The shortcut keys for turning input method off The shortcut keys for turning input method on The shortcut keys for turning input method on or off Top left corner Top right corner Trigger shortcut keys Trigger shortcut keys for gtk_accelerator_parse US layout is appended to the latin layouts. variant can be omitted. Usage: %s COMMAND [OPTION...]

 Use custom font Use custom font name for language panel Use custom font: Use global input method Use shortcut with shift to switch to the previous input method Use system keyboard (XKB) layout Use system keyboard layout Use xmodmap Vertical Write the registry cache FILE. Write the system registry cache. XKB icon shows the layout string and the string is rendered with the RGBA value. The RGBA value can be 1. a color name from X11, 2. a hex value in form '#rrggbb' where 'r', 'g' and 'b' are hex digits of the red, green, and blue, 3. a RGB color in form 'rgb(r,g,b)' or 4. a RGBA color in form 'rgba(r,g,b,a)' where 'r', 'g', and 'b' are either integers in the range 0 to 255 or precentage values in the range 0% to 100%, and 'a' is a floating point value in the range 0 to 1 of the alpha. _About _Add _Apply _Cancel _Close _Delete _Down _OK _Preferences _Remove _Up default:LTR ibus-setup shows the languages only in input method list when you run ibus-setup on one of the languages. Other languages are hidden under an extended button. language: %s
 switching input methods translator-credits Project-Id-Version: IBus
Report-Msgid-Bugs-To: https://github.com/ibus/ibus/issues
POT-Creation-Date: 2015-06-20 22:35+0000
PO-Revision-Date: 2016-03-29 14:46+0000
Last-Translator: YunQiang Su <wzssyqa@gmail.com>
Language-Team: Chinese (China) <trans-zh_cn@lists.fedoraproject.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:36+0000
X-Generator: Launchpad (build 18115)
Language: zh_CN
 %s 是未知命令！
 (暂不可用) ... <b>字体和风格</b> <b>全局输入法设置</b> <b>键盘布局</b> <b>快捷键</b> <b>启动</b> <big><b>IBus</b></big>
<small>智能输入平台</small>
主页：http://code.google.com/p/ibus



 <small><i>可以通过敲击键盘快捷键或者点击面板图标来在上述列表中切换选中的输入法</i></small> 关于 添加选中的输入法到启用输入法列表 高级 总是 作者：%s
 自动隐藏 左下角 右下角 无法连接 IBus。
 候选词排列方向： 命令：
 版权所有 (c) 2007-2010 黄鹏
版权所有 (c) 2007-2010 Red Hat, Inc. 创建注册缓存 自定义 自定义字体 自定义语言栏字体 DConf 保留名称前缀 描述：
 禁用快捷键 禁用： 不再显示 完成 内嵌编辑模式 在应用程序窗口中启用内嵌编辑模式 在应用程序窗口中启用内嵌编辑模式 在应用程序窗口中启用输入法的内嵌编辑模式 默认启动输入法 当应用程序需要使用输入时自动启用输入法 开关： 应用快捷键 启用： 引擎顺序 退出 ibus-daemon 在始终显示的情况下面板跟随输入光标 常规 获取全局引擎失败。
 自动隐藏 水平 IBus面板 IBus 首选项 IBus 更新 IBus 守护在 %d 秒内无法启动 IBus 已经启动！如果仍然无法使用 IBus，请打开 系统菜单 -&gt; 系统设置 -&gt; 语言支持并将“键盘输入法”设置为“ibus”，然后注销再登录。 IBus 是一个用于 Linux/Unix 平台上的智能输入法(intelligent input bus) 。 若为真，当面板设为始终显示时面板将跟随输入光标。若为假，面板将在固定位置显示。 输入法 Kbd 按键： 键盘输入法(IBus Daemon)没有运行，是否启动？ 键盘布局：%s
 快捷键 语言栏位置 语言栏位置： 语言：%s
 无 ASCII 的拉丁语布局 仅列出引擎名称 ibus-setup 中的亚洲语言列表 ibus-setup 中的欧洲语言列表 ibus-setup 中的系统键盘布局组列表 修饰符： 更多… 下移选中的输入法 上移选中的输入法 下一个引擎快捷键 下一输入法： 尚未设置引擎。
 候选词表方向 候选词表方向。0 = 水平，1 = 竖直。 其他 请按一个键盘按键（或者一个组合按键） 请按一个键盘按键（或者一个组合按键）
当您松开任意按键时，对话框会自动关闭。 输入法切换窗口弹出延时微秒数 首选项 用来停止名称转换的 DConf 键前缀 预加载引擎 IBus 启动时预加载的引擎 上一个引擎快捷键 上一输入法： 输出 ibus-daemon 位于 D-Bus 中的地址 退出 XKB 图标的 RGBA 值 读取注册缓存 FILE。 读取系统注册缓存。 从启用输入法列表删除选中的输入法 重置配置参数 正在重置... 重新启动 重启 ibus-daemon IBus 引擎切换时，如果存在  .xmodmap 或 .Xmodmap，则运行 xmodmap。 保存在输入法选单中的引擎顺序 已储存的版本号 选择输入法 选择 %s 的快捷键 设定 IBus 首选项 设定全局引擎失败。
 设定或获取引擎 设定输入法切换窗口弹出的延时微秒数。默认为 400。0 代表立即显示窗口。 0 &lt; 延迟微秒数 0 &gt; 当切换上一个/下一个输入法引擎时不显示窗口。 设置如何显示或隐藏语言栏的方式 设置候选词表方向 在所有应用程序中共享同一个输入法 显示所有输入法 显示可用引擎 在系统托盘上显示图标 显示选中输入法的信息 显示输入法名称 语言栏上显示输入法名称 选中该项将在语言栏上显示输入法的名字 只显示您所在区域的输入法 显示属性栏： 显示选中输入法的设置界面 显示配置参数 显示注册缓存内容。 显示本信息 显示版本号 登录时运行 IBus Super+space 是目前默认的快捷键。 属性面板的行为。 0 = 不再显示，1 = 自动隐藏，2 = 始终显示 默认情况下组列表将不显示所有系统键盘布局。列表中的项目将被添加到 gconf 键的末尾。例如 .../xkblayoutconfig/item1 以毫秒为单位显示属性面板的时间 在离开焦点或属性变更后，以毫秒为单位显示属性面板的时间 语言栏位置。0 = 左上角，1 = 右上角，2 = 左下角，3 = 右下角，4 = 自定义 注册缓存无效。
 已保存的版本号将被用作检查之前安装的 ibus 版本和当前版本间的区别。 切换下一个输入法快捷键 切换上一个输入法快捷键 切换到列表中下一个输入法快捷键 切换到列表中上一个输入法快捷键 关闭输入法的快捷键 开启输入法的快捷键 打开关闭输入法的快捷键 左上角 右上角 触发快捷键 为 gtk_accelerator_parse 触发快捷键 美国布局跟在拉丁布局之后。变种可以忽略。 用法：%s 命令 [选项...]

 使用自定义字体 语言栏上使用自定义字体 使用自定义字体： 使用全局输入法 和 SHIFT 一起使用快捷键来切换到先前的输入法 使用系统键盘（XKB）布局 使用系统键盘布局 使用 xmodmap 竖直 写入注册缓存 FILE。 写入系统注册缓存。 XKB 图标显示键盘布局字符串并依据 RGBA 值进行渲染。RGBA 值可以是 1. X11 中的颜色名；2. '#rrggbb' 形式的十六进制数，其中 'r', 'g' 和 'b' 分别代表十六进制数字的红、绿和蓝；3. 'rgb(r,g,b)' 形式的 RGB 颜色；或者 4. 'rgba(r,g,b,a)' 形式的 RGBA 颜色，其中 'r', 'g' 和 'b' 分别为 0 到 255 之间的整数值，或者 0% 到 100% 之间的百分比值，而 'a' 为 0 到 1 之间的浮点透明度。 关于(_A) 添加(_A) 应用(_A) 取消(_C) 关闭(_C) 删除(_D) 向下(_D) 确定(_O) 首选项(_P) 删除(_R) 向上(_U) default:LTR ibus-setup 仅显示您运行 ibus-setup 时所用语言的输入法列表。其他语言被隐藏在扩展按钮下。 语言： %s
 切换输入法 黄鹏 <shawn.p.huang@gmail.com>
Fedora 简体中文组 <trans-zh_cn@lists.fedoraproject.org>

Launchpad Contributions:
  Aron Xu https://launchpad.net/~happyaron
  Boyuan Yang https://launchpad.net/~hosiet
  Christopher Meng https://launchpad.net/~cicku
  Heling Yao https://launchpad.net/~hyao
  Leah Liu https://launchpad.net/~lliu-redhat
  Luo Lei https://launchpad.net/~luolei
  Peng Huang https://launchpad.net/~shawn-p-huang
  Richard Ma https://launchpad.net/~richard-ma
  TeliuTe https://launchpad.net/~teliute
  YunQiang Su https://launchpad.net/~wzssyqa
  lovenemesis https://launchpad.net/~lovenemesis
  peaceman https://launchpad.net/~zengchang1985
  simonyanix https://launchpad.net/~simonyan
  sxdhaoren https://launchpad.net/~hupengnihao
  wangyu https://launchpad.net/~nameiswangyu 