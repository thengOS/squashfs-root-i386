��    (      \  5   �      p  *   q  
   �  )   �     �     �  z   �     l     {  ,   �     �     �  3   �     %     6  7   O  (   �  a   �          1     P     m     �  -   �     �  (   �       &        :  -   O  '   }     �     �     �     �     �     �  	     
        )  0  B  $   s
     �
  *   �
     �
     �
  u   �
     `     m  '   z     �     �  +   �     �  !     6   #  $   Z  Q        �     �               3     @  	   _     i     �     �  	   �     �     �     �     �     
          $     3  	   N  	   X     b                 
      	           '                    $         %                             !                                             #       "             &            (       Default group, assigned on window creation Indicator: Keep and manage separate group per window Keyboard Layout Keyboard layout Keyboard layout "%s"
Copyright &#169; X.Org Foundation and XKeyboardConfig contributors
For licensing see package metadata Keyboard model Keyboard options Load exotic, rarely used layouts and options Load extra configuration items Preview keyboard layouts Save/restore indicators together with layout groups Secondary groups Show flags in the applet Show flags in the applet to indicate the current layout Show layout names instead of group names Show layout names instead of group names (only for versions of XFree supporting multiple layouts) The Keyboard Preview, X offset The Keyboard Preview, Y offset The Keyboard Preview, height The Keyboard Preview, width The background color The background color for the layout indicator The font family The font family for the layout indicator The font size The font size for the layout indicator The foreground color The foreground color for the layout indicator There was an error loading an image: %s Unknown XKB initialization error keyboard layout keyboard model layout "%s" layouts "%s" model "%s", %s and %s no layout no options option "%s" options "%s" Project-Id-Version: libgnomekbd master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=libgnomekbd&keywords=I18N+L10N&component=general
POT-Creation-Date: 2016-01-28 14:10+0000
PO-Revision-Date: 2011-05-18 05:08+0000
Last-Translator: Lele Long <schemacs@gmail.com>
Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2016-06-27 18:12+0000
X-Generator: Launchpad (build 18115)
 窗口创建时赋予的默认的组 指示器： 为每个窗口保持和管理单独的组 键盘布局 键盘布局 键盘布局“%s”
版权所有&#169 X.Org 基金会及 XKeyboardConfig 贡献者
许可请查看软件包元数据 键盘型号 键盘选项 部分加载，仅使用布局和选项 加载额外配置条目 预览键盘布局 与布局分组一起保存/恢复指示器 次组 在面板小程序中显示标记 在面板小程序中显示指示当前布局的标记 显示布局名称而不是组名称 显示布局名称而不是组名称(只针对支持多种布局的 XFree 版本) 键盘预览，X 偏移量 键盘预览，Y 偏移量 键盘预览，高度 键盘预览，宽度 背景颜色 布局指示器的背景颜色 字体族 布局指示器的字体族 字体大小 布局指示器的字体大小 前景色 布局指示器的前景色 加载图片出错：%s 未知 XKB 初始化出错 键盘布局 键盘型号 布局“%s” 型号“%s”，%s 和 %s 无布局 无选项 选项“%s” 