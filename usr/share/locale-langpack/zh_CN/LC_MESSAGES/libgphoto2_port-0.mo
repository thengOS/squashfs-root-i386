��    :      �  O   �      �     �               *     I     d  y   �  j   �  ,   h  3   �     �  #   �       &   #     J     e       $   �     �  )   �           /  6   L  :   �  <   �  "   �     	  /   >	     n	     �	     �	  #   �	      �	     
     
     7
     D
     P
     n
     w
     �
     �
     �
     �
  !   �
  2   �
  %   0  ;   V  +   �     �  0   �               $     1     C     Y  �  n     O     ^     n     ~     �     �  h   �  `   ;  %   �  )   �     �  '        0  "   J     m     �     �     �     �  0   �  $   (     M  .   b  0   �  Q   �       6   2  ,   i     �     �     �     �     �     �          *  
   7  (   B  	   k     u     �     �  	   �     �  -   �      �       9   -     g     �  *   �     �     �     �     �               5         4      9   
      -                         "   #      0       .       %      '                       6   +       )   3   (       2       8                       ,                     &                 !      	      $            :   1       /   *                7            '%s' already loaded Bad parameters Clear halt... Could not claim the USB device Could not close '%s' (%m). Could not close USB port (%m). Could not find USB device (class 0x%x, subclass 0x%x, protocol 0x%x). Make sure this device is connected to the computer. Could not find USB device (vendor 0x%x, product 0x%x). Make sure this device is connected to the computer. Could not find some functions in '%s': '%s'. Could not find the requested device on the USB port Could not flush '%s' (%m). Could not get level of pin %i (%m). Could not load '%s': '%s'. Could not load port driver list: '%s'. Could not lock device '%s' Could not lock the device Could not open USB device (%m). Could not release interface %d (%m). Could not set config %d/%d (%m) Could not set level of pin %i to %i (%m). Could not set the baudrate to %d Could not write to port (%m) Device '%s' could not be locked (dev_lock returned %d) Device '%s' could not be unlocked (dev_unlock returned %d) Device '%s' could not be unlocked as it is locked by pid %d. Device '%s' could not be unlocked. Device '%s' is locked by pid %d Error clearing a halt condition on the USB port Error initializing the port Error loading a library Error reading from the port Error setting the serial port speed Error updating the port settings Error writing to the port Failed to open '%s' (%m). Generic Port I/O problem Loaded '%s' ('%s') from '%s'. No error No error description available Out of memory Parity error. Serial Port %i Serial port not supported Starting regex search for '%s'... The operation '%s' is not supported by this device The port has not yet been initialized The supplied vendor or product id (0x%x,0x%x) is not valid. Timeout reading from or writing to the port USB port not supported Unexpected parity response sequence 0xff 0x%02x. Unknown error Unknown pin %i. Unknown port Unspecified error Unsupported operation re_match failed (%i) Project-Id-Version: libgphoto2_port
Report-Msgid-Bugs-To: gphoto-devel@lists.sourceforge.net
POT-Creation-Date: 2012-03-31 00:03+0200
PO-Revision-Date: 2010-03-24 09:33+0000
Last-Translator: Jianle Ma <Unknown>
Language-Team: zh_CN <i18n-translation@lists.linux.net.cn>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2016-06-27 16:59+0000
X-Generator: Launchpad (build 18115)
 '%s' 已装入 错误的参数 清除中止... 无法请求 USB 设备 无法关闭‘%s’(%m)。 无法关闭 USB 端口 (%m)。 无法找到 USB 设备 (类 0x%x、子类 0x%x、协议 0x%x)。确信该设备已连接到计算机。 无法找到 USB 设备 (供应商 0x%x、产品 0x%x)。确信该设备已连接到计算机。 在'%s': '%s'中无法找到函数。 无法在 USB 端口找到请求的设备 无法刷新‘%s’(%m)。 无法获取插脚 %i 的级别 (%m)。 无法装入'%s': '%s'。 无法装入端口驱动表'%s'。 无法锁定设备‘%s’ 无法锁定设备 无法打开 USB 设备 (%m)。 无法释放界面 %d (%m)。 无法设定配置 %d/%d (%m) 无法将插脚 %i 的级别设置为 %i (%m)。 无法设置将波特率设置为 %d 无法写端口 (%m) 无法锁定设备‘%s’(dev_lock 返回 %d) 无法解锁设备‘%s’(dev_unlock 返回 %d) 由于设备‘%s’已被进程号为 %d 的进程锁定，所以无法锁定。 无法解锁设备‘%s’。 设备‘%s’已经被进行号为 %d 的进程锁定 清除 USB 端口的一个中止条件出错 初始化端口出错 装入库出错 读端口出错 设置串口速度出错 更新端口设置出错 写端口出错 打开‘%s’(%m)失败。 通用端口 I/O 问题 从 '%3$s' 加载了 '%1$s' （'%2$s'） 无错误 没有可用的错误描述 内存耗尽 奇偶错误。 串口 %i 不支持串口 正在为 '%s' 开始正则表达式搜索... 该设备不支持操作‘%s’ 端口尚未初始化 给出的供应商或产品 id (0x%x、0x%x) 不合法。 读取或写入端口超时 不支持 USB 接口 意外的奇偶应答序列 0xff 0x%02x。 未知的错误 未知插脚 %i。 未知端口 未指明的错误 不支持的操作 再匹配失败 (%i)(_M) 