��    :      �  O   �      �  /   �  �   )  &     %   4  #   Z  %   ~  
   �     �     �  	   �     �     �     �        C        K     [  	   a  	   k     u     ~     �     �     �     �     �     �     �  �     �   �  �   #	     �	     �	     �	     �	     �	     
     
  f   %
  `   �
  d   �
     R     X  
   ]     h     u     �  	   �     �     �     �     �     �     �     �     
       �  #  /   �  �   �  &   �  #   �  #     '   '  
   O     Z     a     n     {     �     �     �  ?   �     �     �  	   �  	                  !     0  	   B     L  	   `     j     q  �   �  �     �   �     ;     K     ]     m     }     �     �  Y   �  Y   �  Y   N     �     �     �  	   �     �     �     �               ,     :     @     N     a     n     {     -      :                                    8   2                   7       4                 9   %                              
       (      6      '   +                !         5   #   .   $   1          3       /   0               ,       "                &   *      )   	       * You should restart nabi to apply above option <span size="x-large" weight="bold">Can't load tray icons</span>

There are some errors on loading tray icons.
Nabi will use default builtin icons and the theme will be changed to default value.
Please change the theme settings. <span weight="bold">Connected</span>:  <span weight="bold">Encoding</span>:  <span weight="bold">Locale</span>:  <span weight="bold">XIM name</span>:  About Nabi Advanced Automatic reordering BackSpace Choseong Commit by word English keyboard Hangul Hangul input method: Nabi - You can input hangul using this program Hangul keyboard Hanja Jongseong Jungseong Keyboard Keypress Statistics Nabi Preferences Nabi keypress statistics Nabi: %s Nabi: error message Off keys Orientation Preedit string font:  Press any key which you want to use as candidate key. The key you pressed is displayed below.
If you want to use it, click "Ok", or click "Cancel" Press any key which you want to use as off key. The key you pressed is displayed below.
If you want to use it, click "Ok" or click "Cancel" Press any key which you want to use as trigger key. The key you pressed is displayed below.
If you want to use it, click "Ok" or click "Cancel" Select candidate key Select hanja font Select off key Select trigger key Shift Space The orientation of the tray. This key is already registered for <span weight="bold">candidate key</span>
Please  select another key This key is already registered for <span weight="bold">off key</span>
Please  select another key This key is already registered for <span weight="bold">trigger key</span>
Please  select another key Total Tray Tray icons Trigger keys Use simplified chinese XIM Server is not running XIM name: _Hide palette _Show palette hangul(hanja) hanja hanja(hangul) per application per context per desktop per toplevel Project-Id-Version: nabi
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2013-01-26 17:31+0900
PO-Revision-Date: 2009-10-29 08:00+0000
Last-Translator: Wylmer Wang <Unknown>
Language-Team: Simplified Chinese <zh_CN@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:12+0000
X-Generator: Launchpad (build 18115)
 * 您要重启 nabi 才能应用上面的选项 <span size="x-large" weight="bold">无法载入托盘图标</span>

载入托盘图标时出现了一些错误。
Nabi 将使用默认图标，主题将改为默认值。
请修改主题设置。 <span weight="bold">已连接</span>:  <span weight="bold">编码</span>:  <span weight="bold">本地</span>:  <span weight="bold">XIM 名称</span>:  关于Nabi 高级 自动排序 退后一格 Choseong 按词上屏 英语键盘 韩文 韩语输入法: Nabi - 你可以使用这个程序输入韩语 韩语键盘 汉字 Jongseong Jungseong 键盘 击键统计 Nabi 首选项 Nabi 击键统计 Nabi：%s Nabi：错误信息 关闭键 方向 预编辑字符串字体：  按下您想设为候选键的按键。按下的键会显示在下面。
如果您想使用它，点“确定”，否则点“取消” 按下您想设为关闭键的按键。您按下的键将显示在下面。
如果您想使用它就点“确定”，不想用则点“取消” 按下您想设为激活键的按键。您按下的键将显示在下面。
如果您想使用它就点“确定”，不想用则点“取消” 选择候选键 选择hanja字体 选择关闭键 选择激活键 Shift 空格 托盘的方向 此按键已被注册用于 <span weight="bold">候选键</span>
请另选一个的按键 此按键已被注册用于 <span weight="bold">关闭键</span>
请另选一个的按键 此按键已被注册用于 <span weight="bold">激活键</span>
请另选一个的按键 总数 托盘 托盘图标 激活键 使用简体中文 XIM服务停止运行 XIM 名称： 隐藏调色板(_H) 显示调色板(_S) hangul(hanja) hanja hanja(hangul) 每个应用程序 每种环境 每个桌面 每个顶层窗口 