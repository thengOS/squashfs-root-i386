��    	      d      �       �      �   )   �   2        F  ,   T  #   �  "   �  0   �  ,  �     &  !   -  .   O     ~  1   �     �  #   �  7                                             	    Archive Could not parse command-line options: %s
 Expects URIs or filenames to be passed as options
 Files to send No mail client installed, not sending files
 Output version information and exit Run from build directory (ignored) Use XID as parent to the send dialogue (ignored) Project-Id-Version: nautilus-sendto master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=nautilus-sendto&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-12-03 23:48+0000
PO-Revision-Date: 2015-12-04 21:59+0000
Last-Translator: 甘露 (Lu Gan) <rhythm.gan@gmail.com>
Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:35+0000
X-Generator: Launchpad (build 18115)
Language: zh_CN
 存档 无法解析命令行选项：%s
 需要网址或者文件名称来作为选项
 要发送的文件 没有安装邮件客户端，将不发送文件
 输出版本信息并退出 从构建目录中运行(已忽略) 使用 XID 作为发送对话框的父对象(已忽略) 