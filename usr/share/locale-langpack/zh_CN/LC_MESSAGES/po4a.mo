��    +      t  ;   �      �      �     �     �            .   5  5   d     �  &   �  "   �  K   �     F  9   ]     �     �  "   �     �       .     ]   D  d   �  a     Z   i  m   �  /   2     b     x     �     �     �     �     �  <   	     ?	  !   T	  &   v	  ,   �	     �	     �	     
     
  .   1
  �  `
  $   (     M     k     �     �  *   �  8   �       $   )     N  [   l     �  2   �          )  !   <     ^     v  -   �  @   �  [   �  X   N  O   �  j   �  0   b     �     �     �     �     �     �       6   7     n     �     �  (   �     �          !     5  *   L             *             '          &      	                                           $                            %   
          #   (                      +                    )   "                   !    Can't close %s after reading: %s Can't close tempfile: %s Can't move %s to %s: %s. Can't open %s: %s Can't read from %s: %s Can't read from file without having a filename Can't write PO files with more than two plural forms. Can't write to %s: %s Can't write to a file without filename Cannot parse command arguments: %s Invalid 'groff_code' value. Must be one of 'fail', 'verbatim', 'translate'. List of valid formats: Messages with more than 2 plural forms are not supported. Module loading error: %s Need to provide a module name Please provide a non-null filename Strange line: -->%s<-- Syntax error The .ie macro must be followed by a .el macro. This file was generated with %s. You should translate the source file, but continuing anyway. This file was generated with docbook-to-man. Translate the source file with the sgml module of po4a. This file was generated with docbook2man. Translate the source file with the sgml module of po4a. This file was generated with help2man. Translate the source file with the regular gettext. Translations don't match for:
%s
-->First translation:
%s
 Second translation:
%s
 Old translation discarded. Unknown '<' or '>' sequence. Faulty message: %s Unknown command: '%s' Unknown format type: %s. Unknown option: %s Unknown tag %s Unparsable line: %s Unsupported font in: '%s'. dia: uncompressed Dia diagrams. kernelhelp: Help messages of each kernel compilation option. latex: LaTeX format. man: Good old manual page format. pod: Perl Online Documentation format. tex: generic TeX documents (see also latex). texinfo: The info page format. text: simple text document. wml: WML documents. xhtml: XHTML documents. xml: generic XML documents (see also docbook). Project-Id-Version: po4a
Report-Msgid-Bugs-To: po4a@packages.debian.org
POT-Creation-Date: 2016-01-05 23:11+0000
PO-Revision-Date: 2010-04-14 14:25+0000
Last-Translator: Wylmer Wang <Unknown>
Language-Team: Simplified Chinese <debian-l10n-chinese@lists.debian.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 17:35+0000
X-Generator: Launchpad (build 18115)
Language: zh_CN
 无法关闭 %s，之前已读取 %s 无法关闭模板文件：%s 移动 %s 到 %s：%s。 无法打开 %s: %s 无法从 %s: %s 读取 没有指定文件名，无法读取文件 无法写入 带多于 2 种复数形式的 PO 文件。 无法写入 %s：%s 没有输入文件名，无法保存 无法解析命令参数：%s 无效的 groff_code 值。必须是下面几种中的某个：fail, verbatim, translate。 可用格式列表： 不支持带 超过 2 种复数形式 的消息。 模块加载错误：%s 需提供模块名 请提供一个非空的文件名 特殊线条：-->%s<-- 语法错误 .ie 宏后面必须跟带有一个 .el 宏。 本文件是用 %s 生成的。但是你需要翻译源文件。 本文件是用 docbook-to-man 生成的。请用 po4a 的 sgml 模块来翻译源文件。 本文件是用 docbook2man 生成的。请用 po4a 的 sgml 模块来翻译源文件。 本文件是用 help2man 生成的。请用 gettext 命令来翻译源文件。 Copy text   	
翻译不吻合：
%s
-->第一次翻译：
%s
 第二次翻译：
%s
 忽略旧的翻译。 未知的  '<' or '>' 序列。出错消息：%s 未知的命令：%s 未知格式类型：%s 未知选项: %s 未知的标签 %s 无法解析的行：%s 在 '%s' 中不支持字体： dia：未解压缩Dia表格 内核帮助：每个内核编译选项的帮助信息 latex: LaTeX 格式。 man：优秀的旧手册格式 pod：Perl 在线文档格式 tex: 常规 TeX 文档 (参考 latex)。 texinfo:  info 页格式. text: 简单文本文档。 wml：WML 文档。 xhtml: XHTML 文档。 xml: 常规 XML 文档 (参考 docbook)。 