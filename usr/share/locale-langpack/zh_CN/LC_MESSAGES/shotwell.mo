��    �     �  �  ;      �N      �N  '   �N     O  	   O     O     "O     /O     2O     5O     >O     KO     ^O     qO     �O     �O     �O     �O     �O     �O     �O     �O  	   P     P     *P  *   =P     hP     zP  >   �P     �P     �P     �P     �P     �P  L   �P  Y   8Q  L   �Q  c   �Q  �   CR  a   �R  u   ?S  H   �S  7   �S  h   6T  �   �T  c   ;U  w   �U  K   V  A   cV  u   �V  �   W  p   �W  �   5X  X   �X  N   Y  =   bY  h   �Y  �   	Z  c   �Z  w   	[  K   �[  A   �[     \     !\     -\  
   ?\     J\     ]\  
   i\     t\  
   �\     �\     �\  
   �\  
   �\     �\     �\  	   �\     �\  	   ]     ]     ]     /]  
   A]     L]     d]     ~]     �]  X   �]  U   �]  S   O^  S   �^  T   �^  5   L_     �_     �_     �_     �_     �_     �_     �_     �_     �_     `     `  !   "`  !   D`     f`     ~`     �`     �`  !   �`     �`     �`  #   �`     a     %a  F   <a     �a     �a  5   �a     �a     �a  :   �a     +b     Bb  ,   [b     �b     �b     �b     �b     �b  
   �b  	   �b     �b     �b     �b     �b     �b  c   �b  o   ac  O   �c  M   !d     od     �d     �d  <   �d     �d     �d     �d     e     e     .e  "   ;e     ^e     fe  	   oe  ,   ye     �e     �e     �e  /   �e  $   f  D   <f  2   �f     �f  
   �f     �f     �f     g     ,g     ?g  
   Ug     `g     {g     �g     �g     �g     �g     �g     �g     �g     �g  	   �g     h     h     h     .h     =h     Bh  	   Hh     Rh  '   bh  ,   �h     �h     �h     �h     �h  
   �h     �h     i  B   ,i  B   oi  O   �i  B   j     Ej     Uj  
   aj     lj     ~j  	   �j     �j  
   �j     �j  +   �j     �j     k  .   k  !   Nk  !   pk     �k     �k     �k  7   �k  $   l  -   :l  	   hl  %   rl     �l  	   �l     �l     �l     �l     �l     m     m  
   3m     >m     Om     ^m     km     wm     �m  	   �m  �   �m  Z   !n  D   |n  2   �n     �n     o     .o     6o  
   ?o     Jo     Qo     Zo     co     jo     wo     �o     �o     �o     �o     �o  	   �o     �o     �o     �o  I   	p  H   Sp     �p  	   �p     �p     �p     �p     �p     q     	q     q     5q     Pq  !   hq     �q  
   �q  	   �q  
   �q     �q  G   �q     #r     8r     Fr  =   Kr  )   �r     �r     �r     �r  
   �r     �r     �r     s     s      s     2s     Cs     Ss     is     }s     �s     �s     �s     �s     �s     �s     �s     �s  %   �s     t     (t     At     ^t     pt     wt     �t  
   �t     �t  6   �t     �t     �t     u     %u     8u     Tu     ju     vu  '   �u     �u  "   �u  "   �u  ,   	v     6v     Hv     Tv  	   iv  ;   sv     �v     �v  '   �v  ,   �v     !w     4w     @w     ^w     nw     uw     zw     �w     �w     �w     �w     �w     �w     �w     x  6   x     Ix  	   Zx     dx     rx     xx     x  
   �x     �x     �x     �x  2   �x     "y  
   1y     <y     Jy     ey     ky     sy     |y     �y     �y     �y     �y     �y     �y     �y  %   z  .   -z     \z     wz     �z     �z  	   �z     �z     �z  
   �z  
   �z     �z  	   �z     �z     �z     {     -{     D{  
   I{     T{     f{  [   {{  ^   �{     6|  /   C|  $   s|  .   �|     �|     �|  7   �|  3   0}     d}     r}  %   �}     �}  
   �}     �}     �}     �}     �}     ~     ~     ~     -~     :~     N~     [~     g~     �~     �~     �~     �~  ,   �~  .   �~     '     >  5   Z  P   �  Q   �  0   3�  -   d�     ��     ��     ��  D   ��     �     �  4   �     L�     g�     o�     ��  "   ��     ��     с     �     ��     �  7   �     Q�     ]�     e�     q�     x�     ��     ��     ��     ��     ǂ     ւ     �     �     '�     C�  !   V�     x�     ��  
   ��  :   Ã     ��     �  
   �  
   �     &�     /�  #   D�     h�     o�     w�     ��     ��     ��     ��  
   ��     Ʉ     ߄     ��      
�     +�     3�  4   8�     m�     v�     ��     ��     ��     ��     ܅     ��     �     /�     L�  1   _�  +   ��  )   ��     �     ��     �     /�  "   J�     m�     z�     ��     ��     ��  '   Ǉ     �  /   	�     9�      U�     v�  "   ��     ��     ʈ     ��     ��     
�  	   %�     /�     6�     B�     O�     \�     j�  3   ��     ��  F   ��     �     �  
   �  
   *�     5�     A�     I�     V�     f�  
   r�      }�     ��     ��     ��     ��     Ȋ     Ԋ     �     �     ��     �  
   �  #   �     B�     [�     v�     ��     ��  3   ��     �      �     ,�     B�     V�     i�     ~�     ��     ��     ��     ��  "   ��     Ռ  \   �  T   G�  �   ��  i   ��  �   ��  �   �  Z   ��  2   ��  �   )�     ő  #   Ց     ��     �     #�     =�     I�     h�     n�  	   t�     ~�     ��  	   ��     ��     Ē     ђ     �     �  !   �  !   ;�     ]�     d�     l�     ��     ��  
   ��     Ó     ؓ     ��  �   �     ��     ��     ��     ��     ��     ��     Δ     �      �      �     %�     A�     M�  �   Z�  I   �  �   N�  .   �  0   �  7   H�  .   ��  0   ��  �   ��  o   h�  &  ؘ  3  ��  2   3�  q   f�  &  ؛     ��  �   �     ��     ��     ��     ��     ��  3   ��  F   �  E   3�     y�     }�     ��     ��     ��  J   ��     ��     ��  '   �  &   +�  )   R�  +   |�     ��     ��     >�     X�  b   s�     ֠  %   �  ;   �  ,   P�  .   }�  %   ��     ҡ     �  *   �  &   3�  "   Z�  6   }�  '   ��     ܢ  (   ��      �  B   2�  <   u�  K   ��  +   ��     *�     8�     @�     E�      c�     ��     ��     ��     Ƥ     դ     �     �     �  :   �  	   X�  
   b�     m�     {�     ��     ��     ��     ��     Х     �  
   ��     �  2   �     L�     d�     k�  (   �     ��     æ     ڦ     �     �     )�     <�  �   Q�  U   �     r�  4   {�     ��     ��  6   ƨ     ��     �     &�     7�     G�  	   V�  %   `�  #   ��  ,   ��  #   ש  "   ��  )  �  �   H�  �   ��  /   ��  �   �  0   �  �   �  �   �  �   ��  :   F�  �   ��  �   �  �   �  
   ��  
   ��     ʲ  	   Ӳ  $   ݲ  $   �  #   '�     K�     R�     Y�     f�     n�     ��     ��  
   ��  
   ��     ��     γ     �     �  	   �  	   ��     �     �  	   "�     ,�     4�     <�  
   M�     X�     n�     ��  
   ��     ��     ��     ��     ɴ  
   Ҵ     ݴ     �     �     ��     �  "   �     *�     3�     O�  :   U�     ��     ��     ��  "   ��     �  	   ��      �     �     �     �     �     6�     D�     Z�     q�     ��     ��     ��     ʶ  
   ϶     ڶ     �  	   ��     �  	   �     %�     ,�     4�     @�     M�  	   ]�     g�  	   p�     z�     ��     ��     ��  	   ��     ��     ��  L   ��  
   
�     �     �     3�     :�     C�     I�     V�     k�     w�  '   ��     ��     ��     ��     ʸ     ܸ     �     ��  !   �     3�     9�     B�     K�  &   Q�     x�     ��     ��     ��  
   ��  	   ��     ��  	   ��     ��     ��     ��     ɹ     �     �     ��  	   �     $�     3�     J�     R�     V�  
   ]�     h�     l�     ��     ��  	   ��  
   ��  
   ��     ��  
   ��     ��     Ǻ     ֺ     �     ��     �     ��     �     �     $�     4�     C�     K�     W�     j�     s�     y�    ��     ��     ��     ͽ  	   ٽ     �     �     ��     ��     ��     �     �     +�     8�     L�  	   Y�  	   c�     m�     t�     y�     ��     ��  	   ��     ��     ƾ  "   ־     ��     �  0   �     P�     Y�     _�     s�     u�  %   {�  ,   ��  %   ο  %   ��  =   �  +   X�  4   ��  (   ��      ��  .   �  7   2�  .   j�  7   ��  )   ��     ��  5   �  >   N�  5   ��  >   ��  0   �  #   3�  #   W�  .   {�  7   ��  .   ��  7   �  )   I�     s�     ��     ��     ��     ��     ��     ��     ��     ��     �     �  	   -�     7�     F�     U�     c�     p�     }�     ��     ��     ��     ��     ��     ��     ��     �     �  M   &�  K   t�  I   ��  I   
�  J   T�  '   ��     ��     ��     ��     ��     �     �     +�     B�     V�     o�     v�     ��     ��     ��  	   ��     ��     ��  '   �     >�     L�  !   `�     ��     ��  5   ��  	   ��     ��  0   �     9�     L�     Y�     v�     ��     ��     ��     ��  	   ��  	   ��     ��     ��      �     �     �     %�     5�     E�  X   L�  ^   ��  A   �  G   F�     ��     ��     ��  >   ��     	�     �     �     �     5�     H�     Y�     x�     �  
   ��  !   ��     ��     ��     ��  $   ��  $   �  D   7�  5   |�     ��  	   ��     ��     ��     �     �     -�     K�  "   X�     {�     ��     ��     ��     ��     ��     ��     ��  	   ��  	   ��  
   �     �     �     /�     B�  	   I�     S�     a�     n�     ~�     ��     ��     ��     ��     ��     ��  $   ��     �     !�  &   A�  "   h�     ��  	   ��     ��     ��     ��     ��     ��  	   ��     ��     ��     �     :�     V�     u�     ��     ��     ��     ��  -   ��     �  "   0�     S�  ,   Z�     ��     ��     ��     ��     ��     ��     ��     �     /�     <�     P�     d�     x�     ��     ��     ��  ]   ��  D   �  5   X�  2   ��  "   ��     ��  
   ��  	   �     �     �  	   &�  	   0�     :�     A�     N�     b�     o�     ��     ��     ��     ��     ��     ��     ��  H   ��  H   (�     q�  	   �     ��     ��     ��     ��     ��     ��     ��     �     �  %   )�     O�     n�  	   {�     ��     ��  F   ��     ��     �     �  $   �  !   ;�     ]�     n�     u�     ��  	   ��     ��     ��     ��     ��     ��     ��     �     �  	   '�  	   1�     ;�     B�     X�     e�  
   l�     w�     ��     ��     ��     ��     ��     ��     �     �  	   0�     :�     A�  0   I�     z�     ��     ��     ��     ��     ��     ��     ��  $   �  "   :�  !   ]�  "   �  '   ��     ��     ��     ��  	   ��  '   �  	   -�     7�     D�     T�     d�     q�     �     ��  	   ��     ��  	   ��  %   ��     ��     ��     �     �     )�  	   B�     L�  -   \�     ��  	   ��     ��     ��     ��     ��  	   ��     ��     �     (�  8   ;�     t�     ��  	   ��     ��     ��     ��  	   ��     ��     ��     ��     �     �     '�     @�     P�  "   o�  !   ��     ��  !   ��  	   ��     ��     �     &�     :�     A�     Q�     a�     n�     {�     ��     ��     ��     ��     ��     ��     �  '    �  !   H�     j�     x�     ��  (   ��     ��     ��  -   �  -   D�     r�     �     ��     ��  
   ��     ��     ��     ��     ��     ��     �     �     *�     :�     T�     h�     x�     ��     ��     ��     ��  *   ��  $   �     +�     A�  5   U�  E   ��  N   ��  /    �  2   P�     ��     ��     ��  -   ��     ��     ��  *   �     ,�     ?�     F�     `�     m�     ��     ��     ��     ��     ��  0   ��     �     !�     (�     6�     =�     J�     Z�     a�     n�     ��     ��     ��     ��  '   ��     
�     �     =�     Z�     w�  7   ~�     ��     ��  
   ��  
   ��     ��     ��  %   �     '�  	   .�     8�  	   E�     O�     V�     p�     ��     ��     ��     ��     ��     ��      �  '   �     /�     6�     D�     R�     e�     ~�     ��     ��     ��     ��     �  -   �     F�  !   e�     ��     ��     ��     ��  "   ��     �     �     8�     R�     c�  '   p�     ��  (   ��     ��     ��     �     *�     C�     Z�     n�     ��     ��     ��     ��     ��     ��     ��     ��     ��  ,   �     5�  D   B�     ��     ��     ��  	   ��     ��  	   ��     ��     ��     
�     �     (�     D�     Q�     a�     h�     o�     ��     ��  	   ��     ��     ��     ��  ,   ��     �     %�     ?�     _�     r�  '   ��     ��  $   ��     ��     �     �     )�     <�     C�  	   J�     T�     ]�     m�     ��  T   ��  N   ��  �   @�  ^   �  �   r�  �   A�  H   ��  '   ?�  �   g�     ��  !    �     "�     8�     U�     n�     �  	   ��     ��     ��     ��     ��  	   ��     ��      �     �     *�     =�     P�     i�     ��     ��     ��  !   ��     ��     ��     ��     ��     ��  ?   ��     :�  
   ?�  
   J�     U�     s�     z�     ��  !   ��  !   ��     ��     ��     �  	   �  |   #�  "   ��  O   ��  $   �  !   8�  +   Z�  $   ��  $   ��  F   ��  :   �  �   R�  �   ��  0   e�  :   ��  �   ��     W�  -   j�  	   ��     ��  	   ��     ��  	   ��  ?   ��  H   �  E   M�     ��     ��     ��  	   ��     ��  P   ��           '      8  #   Y  &   }  #   �     �  ;   �         3 .   I    x     � /   � !   � $     6   %    \    w (   �    � &   � 7   �     7    X    r    � 6   � 6   � B    '   N 	   v    �    �    � !   �    �    �    �        !    4    A    Z 3   y    �    � 	   � 	   �    �    �        !    4    N    k    x '   �    �    �    � (   �        /    E    b    }    �    � �   � W   e 	   � $   �    �    � :   	    A	    ]	    f	    t	    �	    �	 '   �	 (   �	 0   �	 (   
 $   ?
   d
 �   h �    %   � �   � 0   � �   � �   � �   J 3   � i    �   � �   P         
    
   )    4    P    l    � 
   �    � 
   �    �    �    � 
               ) 
   : 
   E 
   P 
   [    f 
   } 
   �    � 
   �    �    �    �    �    � 
    
       *    8 
   O    Z 
   h    s 
   �    � 
   �    �    �    � 
   � :   � 
   *    5    O &   i    � 
   � 
   � 
   � 
   � 
   �    �    �            4    Q    n    � 
   �    �    �    � 	   �    �    � 
    
       "    3    A    U 
   c    n 
   | 
   � 
   � 
   � 
   �    � 
   � U   �    % 
   6    A 
   U 
   ` 
   k    v    �    �    � )   � 
   �    � 
         
       *    >    R 
   r 
   }    � 
   � +   �    �    �    �    � 	   � 	                   #    *    1    P 	   T !   ^ 	   �    �    � 	   �    �    �    �    �    �    �    �    �    � 	   �    � 	   �    	                % 	   )    3    C 	   G    Q    d    h 	   l   v 	   �               z                   �              H  �      '  �              �  �  �  s  R   [  N  �  i          �    �  �   ^      �    m       '   �              !   &  �   �  J   N  �      H  :  E      �      �      �  �      o    �      \   �  [       T   �  �   �   �  �  m      �  �      {   y   �   �        �      ;  �  �  z    �  �  �              !  �   �  �  �  ;   ;      @  A       �        K           G  �  \  l  �   "  �                 #           1       =  �    "      �  �  �  +  �  B            G  �              �  �  �  �  �  �   �  w  o  C     �  �  �  �      �         ,      0       9  �   r  �   �      �      M  �   "  d          b  �  �  u      �  �  �  �  �  B      F      n      &  �   V      _    �  �         }  �   �  �   0  #  v  d  <   �  �  �   4      �   �  3      k   �          �  �  �  �  �      �  J  �      c  -        �   p  :  x  �   �          �      +   8      w  V   c  �      b   �  �  {  k  �          )  h  ;  	  /  Z          �  �      R  D  W  �  y      k  �   �         �                L  Y  <  L      �  �  �  �       �   �   �      �      S      m  2      �  �       U                 �              �   �    =   r   �  �     �  <  �   �  �  �  L  O  (   �    $     V     �  �   �  �  5        �  _     $         $      �      �   �      6      Q  �   �   |   c  $  �  q   �       K          |  E      �      �  �  �     :   �  �   0  B      X  �   3  �      �   G  �  �      �   �      �      y      �   �   �              �  �  P  {  �  �  �  1  )      ]       ^    �      �   k      �   �      �   +         �  �    =    �     �  �          �       �      �      l           s           I   �  e  �  �           �   �  M      2                   4  �  �           i  �  7     S    �            Z                "   �  �  �  �   !  I  �  �  �  �   �  �  �  �  #  �          �      �       �   �   9  �   Y   �      �       �      �       ,   �    �  5   �   �  u  n   u       �   3       �   r  �   �   �      �      d  q  Y  �   (  �  �  �  t  �        J  g      ~       >    S  C  �         �          �  �  �     �          �  �  [  P  !    �  �   �   ]    �  w     �   |  
  [  D   X  L   �   C  *      �                      �      �   �  �   5  �      �          �  �   #  �   %  /         j   �   w  �    �    �  �       1  }  `   	   Z       P   e  -  v  �  8  �                   i   Q          �  f  >   M   �  �  '      �   �  �       �          i  �          u     =          �      �  E  7  r      �  +  �  3      B           W      �  �    :              �  p  z  �  |       6      �      Q      O  A  �  5  �       �      2  �  a      U  �   v   �  �  s  -        ^          @  �  �   .      �  n          �  J          ,  @  �    �  A      �   �        �                 ~  �   o  �   )   �  �       K  T  ,      �      �  a  �  I      e   f  C  E  /    F   .      b  �  �  q  �  �  �      �       �       �   �  l  �   �            �  �    �  �   �   �  <  F  A  N         n  �   �  �  �  �          �           �  �      s  (  �        ?        _  �    �  O      �  �   �  �       ?  f      �      W  �  O           �  �    �   �  N  ]                �  �  �      �          D  �               p   �      8      g   %       '          �     �  �  �   8  �  F  �   �  �  x   �   X   �       �       `  f   
  /   h       y              �                  t   �  �   �  ~     2  �       �  �  �  �              �  ?   �  �          �      �     h  j        ?  �   �     �  g  �      6  �   �  >  �       
   �       �  Y  z      %          	      &  �  m          �  �  �   R  �  *   }   7          �            U  h  l  �  }      x  �  �   �   H   �  �  1  Z  �        �       *  �  �          P  {  �   \  �              `      @   ]      �  6       H  �          �                      �       a   �  X    j  �                  �       �   _     >                9           �  G   �       �  S   x      D  t          e  c   �  �  4  q  �  �       Q   �   �      `  U  �  �     �      �  a      �  W    �  t      0         (  p  v    �    �  �   &       j  b  )  g      \  4   �   �       �      I     R  7      �  �    	    o   K  �  �  �   .  T  �    ~  %  �   T        �           ^   M  9      d       -   *  �  �          �  
     V  �  .    �  �       �   

And %d other. 

And %d others. 

Would you like to continue exporting?  _pixels %-I:%M %p %-I:%M:%S %p %.1f seconds %B %Y %a %b %d %a %b %d, %Y %d Event %d Events %d Photo %d Photos %d Photo/Video %d Photos/Videos %d Video %d Videos %d hour %d hours %d minute %d minutes %d second %d seconds %d%% %d, %Y %m/%d/%Y, %H:%M:%S %m/%d/%Y, %I:%M:%S %p %s (%d%%) %s Database %s does not exist. %s does not support the file format of
%s. %s is not a file. %s or Better '%s' isn't a valid response to an OAuth authentication request (Help) (None) (and %d more)
 - 1 day 1 duplicate photo was not imported:
 %d duplicate photos were not imported:
 1 duplicate photo/video was not imported:
 %d duplicate photos/videos were not imported:
 1 duplicate video was not imported:
 %d duplicate videos were not imported:
 1 file failed to import because it was corrupt:
 %d files failed to import because it was corrupt:
 1 file failed to import because the photo library folder was not writable:
 %d files failed to import because the photo library folder was not writable:
 1 file failed to import due to a camera error:
 %d files failed to import due to a camera error:
 1 file failed to import due to a file or hardware error:
 %d files failed to import due to a file or hardware error:
 1 file skipped due to user cancel:
 %d file skipped due to user cancel:
 1 non-image file skipped.
 %d non-image files skipped.
 1 photo failed to import because it was corrupt:
 %d photos failed to import because they were corrupt:
 1 photo failed to import because the photo library folder was not writable:
 %d photos failed to import because the photo library folder was not writable:
 1 photo failed to import due to a camera error:
 %d photos failed to import due to a camera error:
 1 photo failed to import due to a file or hardware error:
 %d photos failed to import due to a file or hardware error:
 1 photo skipped due to user cancel:
 %d photos skipped due to user cancel:
 1 photo successfully imported.
 %d photos successfully imported.
 1 photo/video failed to import because it was corrupt:
 %d photos/videos failed to import because they were corrupt:
 1 photo/video failed to import because the photo library folder was not writable:
 %d photos/videos failed to import because the photo library folder was not writable:
 1 photo/video failed to import due to a camera error:
 %d photos/videos failed to import due to a camera error:
 1 photo/video failed to import due to a file or hardware error:
 %d photos/videos failed to import due to a file or hardware error:
 1 photo/video skipped due to user cancel:
 %d photos/videos skipped due to user cancel:
 1 photo/video successfully imported.
 %d photos/videos successfully imported.
 1 unsupported photo skipped:
 %d unsupported photos skipped:
 1 video failed to import because it was corrupt:
 %d videos failed to import because they were corrupt:
 1 video failed to import because the photo library folder was not writable:
 %d videos failed to import because the photo library folder was not writable:
 1 video failed to import due to a camera error:
 %d videos failed to import due to a camera error:
 1 video failed to import due to a file or hardware error:
 %d videos failed to import due to a file or hardware error:
 1 video skipped due to user cancel:
 %d videos skipped due to user cancel:
 1 video successfully imported.
 %d videos successfully imported.
 1024 x 768 pixels 11 x 14 in. 1280 x 853 pixels 13 x 18 cm 16 images per page 16 x 20 in. 18 x 24 cm 2 images per page 20 x 30 cm 2048 x 1536 pixels 24 Hr 24 x 40 cm 30 x 40 cm 32 images per page 4 images per page 4 x 6 in. 4096 x 3072 pixels 5 x 7 in. 500 x 375 pixels 6 images per page 8 images per page 8 x 10 in. <b>Pixel Resolution</b> <b>Printed Image Size</b> <b>Titles</b> A _new album named: A fatal error occurred when accessing Shotwell's library.  Shotwell cannot continue.

%s A file required for publishing is unavailable. Publishing to Facebook can't continue. A file required for publishing is unavailable. Publishing to Flickr can't continue. A file required for publishing is unavailable. Publishing to Picasa can't continue. A file required for publishing is unavailable. Publishing to Youtube can't continue. A temporary file needed for publishing is unavailable A3 (297 x 420 mm) A4 (210 x 297 mm) AM Access _type: Add Tag "%s" Add Tags Add Tags "%s" and "%s" Add _Tags... Add more accounts... Adjust Adjust Date and Time Adjust the photo's color and tone Adjust the size of the thumbnails Adjusting Date and Time Admins Admins, Family Admins, Family, Friends Admins, Family, Friends, Contacts Album comment: All + _Rejected All photo source files are missing. An _existing album: An _existing category: An error message occurred when publishing to Piwigo. Please try again. Angle: Any text Apply copied color adjustments to the selected photos Applying Color Transformations Artist: Attempted to import %d file. Attempted to import %d files. Authorization _Number: Auto-importing photos... Automatically improve the photo's appearance BMP Back Blinds Blogs: By Exposure _Date By _Rating By _Title Camera Camera error Camera make: Camera model: Cameras Cannot open the selected F-Spot database file: the file does not exist or is not an F-Spot database Cannot open the selected F-Spot database file: this version of the F-Spot database is not supported by Shotwell Cannot read the selected F-Spot database file: error while reading photos table Cannot read the selected F-Spot database file: error while reading tags table Change slideshow settings Change the rating of your photo Chess Choose <span weight="bold">File %s Import From Folder</span> Circle Circles Clock Close _without Saving Close the red-eye tool Co_py Photos Combine events into a single event Comment Comment: Con_tinue Connect a camera to your computer and import Continue the slideshow Contrast Expansion Copy Color Adjustments Copy the color adjustments applied to the photo Copyright 2009-2014 Yorba Foundation Copyright 2010 Maxim Kartashev, Copyright 2011-2014 Yorba Foundation Copyright 2010+ Evgeniy Polyakov <zbr@ioremap.net> Copyright 2012 BJA Electronics Copyright: Core Data Import Services Core Publishing Services Core Slideshow Transitions Corrupt image file Could not load UI: %s Create Tag Create a _new album named: Creating New Event Creating album %s... Creating album... Crop Crop the photo's size Crumble Current Current Development: Custom DIRECTORY D_escending Data Imports Database error Database file: Date Date: De_fault: Decrease Rating Decrease the magnification of the photo Decrease the magnification of the thumbnails Decreasing ratings Delete Delete Search Delete Search "%s" Delete Tag Delete Tag "%s" Delete all photos in the trash Delete these files from camera? Delete these %d files from camera? Delete this photo from camera? Delete these %d photos from camera? Delete this photo/video from camera? Delete these %d photos/videos from camera? Delete this video from camera? Delete these %d videos from camera? Deleting Photos Deleting... Developer: Disable _comments Disk failure Disk full Display Display %s Display %s or Better Display basic information for the selection Display each photo's rating Display each photo's tags Display extended information for the selection Display the comment of each event Display the comment of each photo Display the search bar Display the sidebar Display the title of each photo Do not monitor library directory at runtime for changes Don't display startup progress meter Drag and drop photos onto the Shotwell window Duplicate Duplicate Photos/Videos Not Imported: Duplicating photos Duration: E_xtended Information E_xternal photo editor: Edit Comment Edit Event Comment Edit Event _Comment... Edit Photo/Video Comment Edit Title Edit _Comment... Edit _Title... Empty T_rash Empty Trash Emptying Trash... Enhance Enhancing Enter the URL of your Piwigo photo library as well as the username and password associated with your Piwigo account for that library. Enter the confirmation number which appears after you log into Flickr in your Web browser. Enter the username and password associated with your Tumblr account. Error accessing database file:
 %s

Error was: 
%s Error loading UI file %s: %s Error while saving to %s: %s Even_ts Event %s Event name Events Everyone Example: Export Export Photo Export Photo/Video Export Photos Export Photos/Videos Export Video Export Videos Export metadata Exporting Exposure Exposure bias: Exposure date: Exposure time will be shifted backward by
%d %s, %d %s, %d %s, and %d %s. Exposure time will be shifted forward by
%d %s, %d %s, %d %s, and %d %s. Exposure time: Exposure: Extended Information External Editors External _RAW editor: F-Spot library: %s Fade Family only Fetching account information... Fetching photo information Fetching preview for %s File %s already exists.  Replace? File already exists in database File error File name File size: File write error Files Not Imported Because They Weren't Recognized as Photos or Videos: Fill the entire page Filter Photos Find Find an image by typing text that appears in its name or tags Find photos and videos by search criteria Fit to _Page Flag Flag selected photos Flag state Flagged Flagging selected photos Flash: Flip Hori_zontally Flip Horizontally Flip Verti_cally Flip Vertically Flipping Horizontally Flipping Vertically Focal length: Folders Friends Friends & family only Friends only From: Fulls_creen GPS latitude: GPS longitude: Generate desktop background slideshow Go to the next photo Go to the previous photo Google+ (2048 x 1536 pixels) HD Video (16 : 9) Height Hide photos already imported High (%d%%) Highlights Highlights: How long each photo is shown on the desktop background Image Settings Import Complete Import From Application Import From Folder Import From _Application... Import Results Report Import _All Import _Selected Import all the photos into your library Import media _from: Import photos from disk to library Import photos from your %s library Import the selected photos into your library Import to Library Imported %s Imported failed (%d) Importing Importing from %s can't continue because an error occurred: Importing... Increase Rating Increase the magnification of the photo Increase the magnification of the thumbnails Increasing ratings Invalid URL Invalid User Name or Password Invalid pattern Items: JPEG Just me L_ist album in public gallery Large (2048 pixels) Last Import Leave _Fullscreen Leave fullscreen Letter (8.5 x 11 in.) Library Library Location Limit the number of photos displayed based on a filter Loading Shotwell Location: Logging in... Login Logout Lose changes to %s? Low (%d%%) Make Key Photo for Event Make _Key Photo for Event Make a duplicate of the photo Manually select an F-Spot database file to import: Maximum (%d%%) Media type Medium (%d%%) Medium (1024 x 768 pixels) Merge Merging Metadata Metric Wallet (9 x 13 cm) Missing Files Modif_y Tags... Modify Tags Move Photos Move Photos to Trash Move Tag "%s" Move photos to an event Move the photos to the Shotwell trash Move the selected photos back into the library Moving Photos to New Event Moving Photos to Trash Name: Ne_w Saved Search... New Event New _Tag... Next Next Photo Next photo No Event No events No events found No photos or videos imported.
 No photos/videos No photos/videos found None Not a file Not an image file Notecard (3 x 5 in.) Of these, %d file was successfully imported. Of these, %d files were successfully imported. One original photo could not be adjusted. The following original photos could not be adjusted. Only _Remove Only display photos that have not been imported Only show photos with a rating of %s Only show photos with a rating of %s or better Open With E_xternal Editor Open With RA_W Editor Open the selected photo's directory in the file manager Open the selected videos in the system video player Original Size Original dimensions: Original photo could not be adjusted. Original size Original:  PM PNG Paste Color Adjustments Path to Shotwell's private data Pause Pause the slideshow Photo Manager Photo Viewer Photo _size preset: Photo _size: Photo size: Photo source file missing: %s Photo state Photos Photos _visible to: Photos and videos _visible to: Photos cannot be exported to this directory. Photos cannot be imported from this directory. Photos will appear in: Photos will be _visible by: Photos/Videos Not Imported Because Files Are Corrupt: Photos/Videos Not Imported Because Shotwell Couldn't Copy Them into its Library: Photos/Videos Not Imported Because They Weren't in a Format Shotwell Understands: Photos/Videos Not Imported Due to Camera Errors: Photos/Videos Not Imported for Other Reasons: Pictures Pin Toolbar Pin the toolbar open Pivot the crop rectangle between portrait and landscape orientations Play Play a slideshow Please close any other application using the camera. Please unmount the camera. Plugins Postcard (10 x 15 cm) Preparing for upload Preparing to auto-import photos... Preparing to import Preparing to import... Previous Photo Previous photo Print image _title Print the photo to a printer connected to your computer Printing... Private Pu_blish... Public Public listed Public unlisted Publish Publish Photos Publish Photos and Videos Publish Videos Publish photos _to: Publish photos and videos _to Publish to an e_xisting album: Publish to various websites Publish videos _to Publish your pictures to Facebook Publish your pictures to Flickr Publish your pictures to Picasa Publishing Publishing to %s can't continue because an error occurred: RAW RAW Developer RAW Photos RAW photos RAW+JPEG R_emove From Library R_ename imported files to lowercase Random Rate %s Rate Rejected Rate Unrated Rating Re_name Event... Re_name Tag "%s"... Re_name... Re_vert External Edit Re_vert External Edits Re_vert to Original Recommended (1600 x 1200 pixels) Red-eye Redo Reduce or eliminate any red-eye effects in the photo Rejected Rejected Only Rejected _Only Remove From Library Remove Photo From Library Remove Photos From Library Remove Tag "%s" From Photo Remove Tag "%s" From Photos Remove Tag "%s" From _Photo Remove Tag "%s" From _Photos Remove any ratings Remove any red-eye effects in the selected region Remove the selected photos from the library Remove the selected photos from the trash Removing Event Removing Photo From Library Removing Photos From Library Removing duplicated photos Removing photos/videos from camera Rename Event Rename Search "%s" to "%s" Rename Tag "%s" to "%s" Replace _All Reset Colors Reset all color adjustments to original Restore Photos from Trash Restore the photos back to the Shotwell library Restoring Photos From Trash Restoring previous RAW developer Restoring previous rating Return to current photo dimensions Revert External E_dits Revert External Edit? Revert External Edits? Revert to Original Revert to the master photo Reverting Rotate Rotate Left Rotate Right Rotate _Left Rotate _Right Rotate the photos left Rotate the photos right (press Ctrl to rotate left) Rotating Run '%s --help' to see a full list of available command line options.
 SD Video (4 : 3) S_idebar S_lideshow Saturation Saturation: Save As Save Details Save Details... Save _As... Save photo Save photo with a different name Saved Search Saved Searches Screen Search Select _All Select all items Send T_o... Send To Send _To... Set Developer Set Rating Set _all photos/videos to this time Set as Desktop Slideshow Set as _Desktop Background Set as _Desktop Slideshow... Set rating to %s Set rating to rejected Set selected image to be the new desktop background Set the crop for this photo Setting Photos to Previous Event Setting RAW developer Setting as rejected Setting as unrated Setting rating to %s Settings Shadows Shadows: Shotwell Shotwell Connect Shotwell Extra Publishing Services Shotwell Preferences Shotwell can copy the photos into your library folder or it can import them without copying. Shotwell cannot contact your Piwigo photo library. Please verify the URL you entered Shotwell cannot publish the selected items because you do not have a compatible publishing plugin enabled. To correct this, choose <b>Edit %s Preferences</b> and enable one or more of the publishing plugins on the <b>Plugins</b> tab. Shotwell couldn't create a file for editing this photo because you do not have permission to write to %s. Shotwell has found %d photos in the F-Spot library and is currently importing them. Duplicates will be automatically detected and removed.

You can close this dialog and start using Shotwell while the import is taking place in the background. Shotwell is configured to import photos to your home directory.
We recommend changing this in <span weight="bold">Edit %s Preferences</span>.
Do you want to continue importing photos? Shotwell needs to unmount the camera from the filesystem in order to access it.  Continue? Shotwell was unable to play the selected video:
%s Shotwell was unable to upgrade your photo library from version %s (schema %d) to %s (schema %d).  For more information please check the Shotwell Wiki at %s Show all photos Show all photos, including rejected Show each photo for Show in File Mana_ger Show only rejected photos Show t_itle Show the application's version Size: Slide Slideshow Slideshow Transitions Small (640 x 480 pixels) Software: Sort _Events Sort _Photos Sort photos by exposure date Sort photos by rating Sort photos by title Sort photos in a descending order Sort photos in an ascending order Square Squares Standard (720 pixels) Starting import, please wait... Stop importing photos Straighten Straighten the photo Stripes Success Switching developers will undo all changes you have made to this photo in Shotwell Switching developers will undo all changes you have made to these photos in Shotwell TIFF T_ools Ta_gs Tabloid (11 x 17 in.) Tag Tag Photo as "%s" Tag Photos as "%s" Tag the selected photo as "%s" Tag the selected photos as "%s" Tags Tags (separated by commas): Temperature Temperature: The camera is locked by another application.  Shotwell can only access the camera when it's unlocked.  Please close any other application using the camera and try again. The photo or video cannot be deleted. %d photos/videos cannot be deleted. The photo or video cannot be moved to your desktop trash.  Delete this file? %d photos/videos cannot be moved to your desktop trash.  Delete these files? The selected photo was successfully published. The selected photos were successfully published. The selected photos/videos were successfully published. The selected video was successfully published. The selected videos were successfully published. This will destroy all changes made to the external file.  Continue? This will destroy all changes made to %d external files.  Continue? This will remove the photo from the library.  Continue? This will remove %d photos from the library.  Continue? This will remove the photo from your Shotwell library.  Would you also like to move the file to your desktop trash?

This action cannot be undone. This will remove %d photos from your Shotwell library.  Would you also like to move the files to your desktop trash?

This action cannot be undone. This will remove the photo/video from your Shotwell library.  Would you also like to move the file to your desktop trash?

This action cannot be undone. This will remove %d photos/videos from your Shotwell library.  Would you also like to move the files to your desktop trash?

This action cannot be undone. This will remove the saved search "%s".  Continue? This will remove the tag "%s" from one photo.  Continue? This will remove the tag "%s" from %d photos.  Continue? This will remove the video from your Shotwell library.  Would you also like to move the file to your desktop trash?

This action cannot be undone. This will remove %d videos from your Shotwell library.  Would you also like to move the files to your desktop trash?

This action cannot be undone. Time Adjustment Error Time adjustments could not be undone on the following photo file. Time adjustments could not be undone on the following photo files. Time: Tint Tint: Title Title: To get started, import photos in any of these ways: To try importing from another service, select one from the above menu. To try publishing to another service, select one from the above menu. To: Today Transition d_elay: Trash Trash is empty TumblrPublisher: start( ): can't start; this publisher is not restartable. Type Un_flag Unable to create cache directory %s: %s Unable to create data directory %s: %s Unable to create data subdirectory %s: %s Unable to create temporary directory %s: %s Unable to decode file Unable to delete %d photo/video from the camera due to errors. Unable to delete %d photos/videos from the camera due to errors. Unable to display FAQ: %s Unable to display help: %s Unable to duplicate one photo due to a file error Unable to duplicate %d photos due to file errors Unable to export %s: %s Unable to export background to %s: %s Unable to export the following photo due to a file error.

 Unable to fetch previews from the camera:
%s Unable to generate a temporary file for %s: %s Unable to launch Nautilus Send-To: %s Unable to launch editor: %s Unable to lock camera: %s Unable to monitor %s: Not a directory (%s) Unable to navigate to bug database: %s Unable to open in file manager: %s Unable to open/create photo database %s: error code %d Unable to prepare desktop slideshow: %s Unable to print photo:

%s Unable to process monitoring updates: %s Unable to publish Unable to rename search to "%s" because the search already exists. Unable to rename tag to "%s" because the tag already exists. Unable to unmount camera.  Try unmounting the camera from the file manager. Unable to write to photo database file:
 %s Unconstrained Undated Undo Undoing Color Transformations Undoing Date and Time Adjustment Undoing Enhance Undoing Flip Horizontally Undoing Flip Vertically Undoing Revert Undoing Rotate Unflag Unflag selected photos Unflagging selected photos Unknown error attempting to verify Shotwell's database: %s Unmerging Unmodified Unmounting... Unrated Unsupported file format Updating library... Upload _size: Uploading %d of %d Use a _standard size: Use a c_ustom size: User _name User aborted import Username and/or password invalid. Please try again Video privacy _setting: Videos Videos _visible to: Videos and new photo albums _visible to: Videos will appear in '%s' Videos will appear in: View Eve_nt for Photo Visit the Yandex.Fotki web site Visit the Yorba web site Wallet (2 x 3 in.) Welcome to Shotwell! Welcome to the F-Spot library import service.

Please select a library to import, either by selecting one of the existing libraries found by Shotwell or by selecting an alternative F-Spot database file. Welcome to the F-Spot library import service.

Please select an F-Spot database file. Welcome! Where would you like to publish the selected photos? Width Width or height Write tags, titles, and other _metadata to photo files Writing metadata to files... Year%sMonth Year%sMonth%sDay Year%sMonth-Day Year-Month-Day Yesterday You are logged into Facebook as %s.

 You are logged into Flickr as %s.

 You are logged into Picasa Web Albums as %s. You are logged into Tumblr as %s.

 You are logged into YouTube as %s. You are not currently logged into Facebook.

If you don't yet have a Facebook account, you can create one during the login process. During login, Shotwell Connect may ask you for permission to upload photos and publish to your feed. These permissions are required for Shotwell Connect to function. You are not currently logged into Flickr.

Click Login to log into Flickr in your Web browser.  You will have to authorize Shotwell Connect to link to your Flickr account. You are not currently logged into Picasa Web Albums.

Click Login to log into Picasa Web Albums in your Web browser. You will have to authorize Shotwell Connect to link to your Picasa Web Albums account. You are not currently logged into Yandex.Fotki. You are not currently logged into YouTube.

You must have already signed up for a Google account and set it up for use with YouTube to continue. You can set up most accounts by using your browser to log into the YouTube site at least once. You can also import photos in any of these ways: You do not have any data imports plugins enabled.

In order to use the Import From Application functionality, you need to have at least one data imports plugin enabled. Plugins can be enabled in the Preferences dialog. You have already logged in and out of Flickr during this Shotwell session.
To continue publishing to Flickr, quit and restart Shotwell, then try publishing again. You have already logged in and out of a Google service during this Shotwell session.

To continue publishing to Google services, quit and restart Shotwell, then try publishing again. Your Flickr Pro account entitles you to unlimited uploads. Your free Flickr account limits how much data you can upload per month.
This month, you have %d megabytes remaining in your upload quota. Your photo library is not compatible with this version of Shotwell.  It appears it was created by Shotwell %s (schema %d).  This version is %s (schema %d).  Please clear your library by deleting %s and re-import your photos. Your photo library is not compatible with this version of Shotwell.  It appears it was created by Shotwell %s (schema %d).  This version is %s (schema %d).  Please use the latest version of Shotwell. Zoom _100% Zoom _200% Zoom _In Zoom _Out Zoom the photo to 100% magnification Zoom the photo to 200% magnification Zoom the photo to fit on the screen [FILE] _About _Add Tags... _Adjust _Adjust Date and Time... _Albums (or write new): _All Photos _Ascending _Autosize: _Background: _Basic Information _Cancel _Close _Comments _Contents _Copy Color Adjustments _Crop _Decrease _Delay: _Delete _Delete Tag "%s" _Developer _Directory structure: _Do no upload tags _Don't show this message again _Duplicate _Edit _Edit... _Email address _Enhance _Export... _File _Filter Photos _Find _Find... _Flag _Forbid downloading original photo _Format: _Frequently Asked Questions _Help _If a title is set and comment unset, use title as comment _Import _Import From Folder... _Import in Place _Import photos from your %s folder _Import photos to: _Increase _Keep _Login _Logout _Match _Match photo aspect ratio _Merge Events _Modify original file _Modify original files _Modify original photo file _Modify original photo files _Move to Trash _Name of search: _New _New Event _Next Photo _Output photo at: _Password _Paste Color Adjustments _Pattern: _Photo _Photos _Play Video _Preferences _Previous Photo _Print... _Publish _Quality: _Quit _Ratings _Red-eye _Redo _Rejected _Remember Password _Remove _Remove location, camera, and other identifying information before uploading _Rename... _Replace _Report a Problem... _Reset _Restore _Save _Save a Copy _Scaling constraint: _Search Bar _Set Rating _Shift photos/videos by the same amount _Skip _Stop Import _Straighten _Switch Developer _Titles _Transition effect: _Trash File _Trash Files _URL of your Piwigo photo library _Undo _Unmount _Unrated _View _Watch library directory for new files a raw photo a video all and and higher and lower any any photo black cm contains couldn't copy %s
	to %s day days does not contain duplicates existing media item ends with error message: external modifications flagged has has no hour hours in. internal modifications is is after is before is between is exactly is not is not set label minute minutes modifications modified none not flagged of the following: only period of time pixels per inch second seconds seconds starts with translator-credits untitled white within category: Project-Id-Version: shotwell-0.15.1
Report-Msgid-Bugs-To: shotwell@yorba.org
POT-Creation-Date: 2014-05-15 08:54+0000
PO-Revision-Date: 2014-04-10 14:09+0000
Last-Translator: Luo Lei <luolei@ubuntukylin.com>
Language-Team: Chinese (China) (http://www.transifex.com/projects/p/shotwell/language/zh_CN/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2016-06-27 18:45+0000
X-Generator: Launchpad (build 18115)
Language: zh_CN
 

和另外 %d 个。 

您想要继续导出吗?  像素(_P) %-I:%M %p %-I:%M:%S %p %.1f 秒 %m %Y %m月%d日%A %Y年%m月%d日%A %d 个事件 %d 个照片 %d 个照片/视频 %d 个视频 %d 小时 %d 分钟 %d 秒 %d%% %Y年%m月%d日 %Y/%m/%d %H:%M:%S %Y/%m/%d %p%I:%M:%S %s (%d%%) %s 数据库 %s 不存在。 %s 不支持 %s 类型的文件。 %s 不是一个文件。 %s 或更高 “%s”不是 OAuth 认证请求的有效响应 (帮助) (无) (和另外 %d 个)
 - 1 天 %d 个重复的照片没有导入：
 %d 个重复的照片/视频没有导入：
 %d 个重复的视频没有导入：
 %d 文件由于损坏导入失败：
 %d 文件导入失败因为照片库文件夹不可写入：
 %d 文件导入失败由于相机错误：
 %d 文件导入失败由于文件或硬件错误：
 %d 文件被跳过由于用户取消：
 跳过 %d 个非照片文件。
 %d 个照片由于文件损坏导入失败：
 %d 个照片由于无法写入媒体库导入失败：
 %d 个照片由于相机错误导入失败：
 %d 个照片由于文件或硬件错误导入失败：
 由于用户取消跳过 %d 个照片：
 %d 个照片成功导入。
 %d 个照片/视频由于文件损坏导入失败：
 %d 个照片/视频由于无法写入媒体库导入失败：
 %d 个照片/视频由于相机错误导入失败：
 %d 个照片/视频由于文件或硬件错误导入失败：
 由于用户取消跳过 %d 个照片/视频：
 %d 个照片/视频成功导入。
 跳过 %d 个不支持的照片：
 %d 个视频由于文件损坏导入失败：
 %d 个视频由于无法写入媒体库导入失败：
 %d 个视频由于相机错误导入失败：
 %d 个视频由于文件或硬件错误导入失败：
 由于用户取消跳过 %d 个视频：
 %d 个视频成功导入。
 1024 x 768 像素 11 x 14 英寸 1280 x 853 像素 13 x 18 厘米 每页 16 张 16 x 20 英寸 18 x 24 厘米 每页 2 张 20 x 30 厘米 2048 x 1536 像素 24 小时 24 x 40 厘米 30 x 40 厘米 每页 32 张 每页 4 张 4 x 6 英寸 4096 x 3072 像素 5 x 7 英寸 500 x 375 像素 每页 6 张 每页 8 张 8 x 10 英寸 <b>像素分辨率</b> <b>印刷尺寸</b> <b>标题</b> 一个新相册(_N)： 访问 Shotwell 媒体库时出现致命错误。Shotwell 无法继续。

%s 一个发布所需要的文件不存在。无法继续发布至 Facebook。 一个发布所需要的文件不存在。无法继续发布至 Flickr。 一个发布所需要的文件不存在。无法继续发布至 Picasa。 一个发布所需要的文件不存在。无法继续发布至 YouTube。 发布所需要的临时文件不存在 A3 (297 x 420 毫米) A4 (210 x 297 毫米) 上午 访问类型(_T)： 添加标签 %s 添加标签 添加标签 %s 和 %s 添加标签(_T)... 添加更多的帐号... 调整 修改日期和时间 调整照片的色彩和色调 改变缩略图大小 正在调整日期和时间 管理员 管理员，家人 管理员，家人，朋友 管理员，家人，朋友，联系人 相册备注: 全部为次品(_R) 所有的照片源文件丢失。 一个已有的相册(_E)： 一个已有的分类(_)： 在发布至 Piwigo 时发生了错误，请重试。 角度： 任意文字 将复制的颜色调整应用于选定的照片 应用颜色变换 艺术家： 尝试导入 %d 个文件。 认证码(_N)： 自动导入照片... 自动增强照片的观感 BMP 后退 百叶窗 博客： 按照曝光日期(_D) 按照评分(_R) 按照标题(_T) 相机 相机错误 相机厂商： 相机型号： 相机 无法打开选定的 F-Spot 数据库文件：文件不存在或不是 F-Spot 数据库 无法打开选定的 F-Spot 数据库文件：Shotwell 不支持此版本的 F-Spot 数据库 无法读取选定的 F-Spot 数据库文件：读取照片出错 无法读取选定的 F-Spot 数据库文件：读取标签列表出错 改变幻灯片演示设置 改变您照片的评分 茄子 选择 <span weight="bold">文件 %s 从文件夹导入</span> 圈子 圆圈 时钟 关闭但不保存(_W) 关闭红眼工具 复制照片(_P) 将多个事件合并为一个 备注 备注: 继续(_T) 链接相机到电脑上并导入 继续幻灯片演示 对比度扩展 复制颜色调整 复制应用于照片的颜色调整 Copyright 2009-2014 Yorba Foundation Copyright 2010 Maxim Kartashev, Copyright 2011-2014 Yorba Foundation 版权所有 2010+ Evgeniy Polyakov <zbr@ioremap.net> Copyright 2012 BJA Electronics 版权： 核心数据导入服务 主要上传服务 主要幻灯片变换 损坏的图片 无法加载图形界面：%s 创建标签 创建一个新相册名为(_N)： 正在创建新的事件 正在创建相册 %s… 创建相册... 剪裁 剪裁照片尺寸 碎片 当前 当前开发: 自定义 文件夹 降序(_D) 数据导入 数据库错误 数据库文件： 日期 日期： 默认(_F)： 降低评分 缩小缩略图 缩小缩略图 降低评分 刪除 删除搜索 删除搜索 %s 删除标签 删除标签 %s 删除在回收站中的全部照片 从相机删除 %d 个文件？ 从相机删除 %d 张照片？ 从相机删除 %d 个照片/视频？ 从相机删除这 %d 个视频？ 删除照片 删除... 开发者： 禁用评论(_C) 磁盘错误 磁盘已满 显示 显示 %s 显示 %s 或更高 显示所选项的基本信息 显示每张照片的评分 显示每张照片的标签 显示所选项的扩展信息 显示每个事件的备注 显示每张照片的备注 显示搜索栏 显示侧边栏 显示每张照片的标题 请勿在程序运行时监视文件夹变化 不显示启动进程条 拖放照片到 Shotwell 窗口中 副本 以下重复的照片/视频没有被导入: 正在创建照片副本 持续时长： 扩展信息(_X) 外部照片编辑器(_X)： 修改备注 修改事件备注 修改事件备注(_C) 修改照片/视频备注 编辑标题 编辑备注(_C)... 编辑标题(_T)... 清空回收站(_R) 清空回收站 正在清空回收站... 增强 正在增强 输入您 Piwigo 照片库的 URL 以及对应 Piwigo 照片库相关的用户名和密码。 输入您使用网页浏览器登录 Flickr 后出现的确认码。 输入与您 Tumblr 帐号关联的用户名和密码 访问数据库文件错误：
%s

错误是：
%s 载入 UI 文件 %s 时出错：%s 保存 %s 时出错：%s 活动(_T) 事件 %s 事件名称 事件 所有人 例如： 导出 导出照片 导出照片/视频 导出照片 导出照片/视频 导出视频 导出视频 导出元数据 正在导出 曝光 曝光偏差： 曝光日期: 曝光时间将会向较晚时改变 
%d %s、%d %s、%d %s 和 %d %s。 曝光时间将会向较早时改变 
%d %s、%d %s、%d %s 和 %d %s。 曝光时间: 曝光： 扩展信息 外部编辑器 外部 RAW 编辑器(_R)： F-Spot 媒体库：%s 渐出 仅限家人 获取帐号信息... 获取照片信息 获取 %s 的预览 文件 %s 已存在。是否替换？ 文件已存在于数据库中 文件错误 文件名 文件大小： 文件写入错误 以下文件因为它们不被识别为照片或视频而未被导入: 填充整个页面 过滤照片 查找 依据名称或者标签查找图片 按照规则查找照片和视频 适合页面(_P) 标注 标注选定照片 标注状态 已标注 正在标注选定照片 闪光灯： 水平翻转(_Z) 水平翻转 竖直翻转(_C) 竖直翻转 正在水平翻转 正在垂直翻转 焦距： 文件夹 朋友 仅有朋友和家人 仅限朋友 从： 全屏(_C) GPS 纬度： GPS 经度： 生成桌面背景幻灯片 跳到下一张照片 跳到上一张照片 Google+ (2048 x 1536 像素) 高清视频(16 : 9) 高度 隐藏已经导入的照片 高(%d%%) 高亮 高亮: 每张照片在桌面背景上出现多长时间 图像设置 导入完成 从应用程序导入 从文件夹导入 从应用程序导入(_A)... 导入结果报告 导入全部(_A) 导入选中项目(_S) 导入所有照片到您的媒体库 从以下位置导入媒体(_F)： 从磁盘导入照片到媒体库 从您的 %s 照片库导入照片 导入选定的照片到您的媒体库 导入到媒体库 已导入 %s 导入失败(%d) 导入中 发生错误，无法继续导入 %s： 导入... 提高评分 放大缩略图 放大缩略图 提高评分 无效的 URL 无效的用户名或密码 无效的样式 项目： JPEG 仅有我 显示公开相片库里的相册(_I) 大型(2048 像素) 最近导入 退出全屏(_F) 退出全屏 书信 (8.5 x 11 英寸) 媒体库 媒体库位置 限制在过滤条件下显示的照片数量 正在载入 Shotwell 地点： 正在登录…… 登录 登出 放弃对 %s 的更改？ 低(%d%%) 创建事件关键照片 为事件创建关键照片(_K) 创建照片副本 手工选择一个要导入的 F-Spot 数据库文件： 最大(%d%%) 媒体类型 中(%d%%) 中型(1024 x 768 像素) 合并 正在合并 元数据 用于皮夹(9 x 13 厘米) 丢失文件 修改标签(_Y)... 修改标签 移动照片 移动照片至回收站 移动标签 %s 将照片移动到一个事件 将照片移至 Shotwell 回收站 将选定照片移回至媒体库 正在移动照片到新事件 正在将照片移动到回收站 名称： 新建保存搜索(_W)... 新建活动 新建标签(_T)... 前进 下一张照片 下一张照片 没有事件 没有事件 没有找到事件 没有导入照片或视频。
 没有照片/视频 没有找到照片或视频 无 不是一个文件 不是照片文件 留言卡(3 x 5 英寸) 其中，%d 个文件被成功导入。 无法修改下列原始照片。 只移除(_R) 只显示尚未导入的照片 只显示评分为 %s 的照片 只显示评分为 %s 或更高的照片 用外部编辑器打开(_X) 用 RA_W 编辑器打开 在文件管理器中打开选定照片目录 在系统视频播放器中打开选定视频 原始尺寸 原始尺寸： 无法修改原始照片。 原始尺寸 原始：  下午 PNG 粘贴颜色调整 Shotwell 程序数据目录 暂停 暂停幻灯片演示 照片管理器 照片查看器 预置照片尺寸(_S)： 照片尺寸(_S)： 照片尺寸： 照片源文件丢失：%s 照片状态 照片 照片可见(_V)： 照片和视频可见于(_V)： 照片不可以被导出到该文件夹。 无法从此文件夹导入照片。 照片将出现在： 照片可见(_V)： 以下照片/视频因为文件损坏而未被导入: 以下照片/视频因为 Shotwell 无法拷贝它们而未被导入: 以下照片/视频因为它们不是 Shotwell 识别的格式而未被导入: 照片/视频因为相机错误而未被导入: 因为其他原因而未被导入的照片/视频: 图片 固定工具栏 将工具栏固定为打开 在肖像和风景方向上旋转剪裁矩形 播放 开始幻灯片播放 请关闭其他使用该相机的程序。 请卸载相机。 插件 明信片(10 x 15 厘米) 准备上传 准备自动导入照片... 正在准备导入 准备导入... 前一张照片 上一张照片 打印图像标题(_T) 使用连接到此电脑的打印机打印照片 打印中... 私人 发布(_B)... 公开 公开列出 公开未列出 发布 发布照片 发布照片和视频 发布视频 发布照片到(_T)： 发布照片和视频到(_T) 发布到已有相册(_X)： 将所选照片发布到各种网站上 发布视频到(_T) 您的照片发布到 Facebook 您的照片发布到 Flickr 您的照片发送到 Picasa 发布 无法继续发布到 %s 因为发生了一个错误： RAW RAW 开发者： RAW 照片 RAW 照片 RAW+JPEG 从媒体库移除(_E) 将导入文件以小写重命名(_E) 随机 评分 %s 评为次品 未评分 评分 重新命名事件(_N)... 重命名标签 %s (_N)... 重命名(_N)... 恢复额外修改(_V) 恢复额外修改(_V) 恢复到原始(_V) 推荐(1600 x 1200 像素) 红眼 重做 降低或去除照片上的红眼效果 次品 仅次品(_R) 仅次品(_R) 从媒体库移除 从照片库移除照片 从照片库移除照片 从照片移除标签 %s 从照片移除标签 %s 从照片移除标签 %s (_P) 从照片移除标签 %s (_P) 移除任何评分 移除在选定区域内的所有红眼效果 从媒体库移除选定照片 从回收站中移除选中照片 正在移除事件 正在从媒体库移除照片 从照片库移除照片 正在移除照片副本 正在从相机移除照片/视频 重命名事件 重命名搜索 %s 为 %s 重命名标签 %s 为 %s 全部替换(_A) 重置颜色 将所有颜色修正重置至原始值 从回收站还原照片 恢复这些照片到 Shotwell 媒体库 正在从回收站还原照片 恢复原先的 RAW 开发者 恢复先前评分 回到当前照片尺寸 恢复额外修改(_D) 恢复额外修改? 恢复额外修改? 恢复到原始 恢复到主照片 正在还原 旋转 向左旋转 向右旋转 向左旋转(_L) 向右旋转(_R) 向左旋转照片 向右旋转照片(按住 Ctrl 向左旋转) 正在旋转 运行 '%s --help' 来查看可用命令行选项的完整列表。
 标清视频(4 : 3) 侧边栏(_S) 幻灯片演示(_L) 饱和度 饱和度： 另存为 保存详细信息... 保存详细信息... 另存为(_A)... 保存照片 以其他名字保存照片 保存搜索 已保存搜索 屏幕 搜索 选择全部(_A) 选择全部项目 发送至(_O)… 发送至 发送至(_T)... 设置开发人员 设置评分 设置所有照片/视频到这个时间(_A) 设置为桌面幻灯片 设置为桌面背景(_D) 设置为桌面幻灯片(_D)... 设置评分为 %s 设置评分为次品 设置选定照片为新的桌面背景 对这张照片进行剪裁 正在设置照片至先前的事件 设置 RAW 开发者 设置为次品 设置为未评分 设置评分为 %s 设置 阴影 阴影： Shotwell Shotwell 连接 Shotwell 附加发布服务 Shotwell 首选项 Shotwell 可以复制照片到您的媒体库，或者可以导入但并不复制。 Shotwell 无法连接至您的 Piwigo 照片库。请检查您输入的 URL。 Shotwell 无法发布选定项目，因为您尚未启用兼容的发布插件。要修复此问题，请选择<b>编辑 %s 首选项</b>，并且在<b>插件</b>标签页中启用一个或多个发布插件。 Shotwell 无法为编辑此照片创建必需的文件，因为您没有写入 %s 的权限。 Shotwell 在 F-Spot 媒体库中找到 %d 个照片并正在导入它们。重复的文件会被自动去除。

您可以关闭这个会话并开始使用 Shotwell，导入工作将在后台继续进行。 Shotwell 被设置为自动从您的主目录导入照片。
我们建议您在<span weight="bold">编辑 %s 首选项</span>中修改该设置。
您仍要继续导入照片吗？ Shotwell 需要从文件系统卸载相机来访问它。是否继续？ Shotwell 无法播放选中视频：
%s Shotwell 无法将您的媒体库从 %s (schema %d)版本升级到 %s (schema %d)版本。更多的信息请查询 Shotwell 维基 %s 显示所有照片 显示所有照片，包括次品 显示每张照片为 在文件管理器显示(_G) 只显示次要的照片 显示标题(_I) 显示程序版本 大小： 幻灯片演示 幻灯片放映 幻灯片过渡 小型(640 x 480 像素) 软件： 排列事件(_E) 照片排序(_P) 按照曝光日期排序 按照评分排序 按照标题排序 按照降序排列照片 按照升序排列照片 方形 方格 标准(720 像素) 正在开始导入，请稍候... 停止导入照片 校正 校正照片 条纹 成功 切换开发者将撤销您对于此照片所作的全部更改 TIFF 工具(_O) 标签(_G) 小型海报 (11 x 17 英寸) 标签 为照片添加标签 %s 为照片添加标签 %s 为选中的照片添加标签 %s 为选中的照片添加标签 %s 标签 标签(由逗号分割)： 色温 色温： 该相机被其他程序锁定，仅在解锁后 Shotwell 才能访问。请关闭其他使用该相机的程序后重试。 %d 照片/视频无法被删除。 %d 个照片/视频无法移动到您的桌面回收站。删除这些文件？ 选中的照片已经成功发布。 选中照片已经成功发布。 选中的照片/视频已经成功发布。 选中的视频已经成功发布。 选中的视频已经成功发布。 这将毁掉对于 %d 个外部文件所做的全部修改。继续？ 这将从媒体库中移除 %d 张照片中。继续么？ 将从 Shotwell 媒体库移除 %d 个照片。你也想要从将这些文件移至桌面垃圾箱么？

该操作将不可撤销。 将从 Shotwell 媒体库移除 %d 个照片/视频。你也想要从将这些文件移至桌面垃圾箱么？

该操作将不可撤销。 这将移除已保存搜索 %s。是否继续？ 这将移除标签 "%s" 从 %d 张照片中。继续么？ 将从 Shotwell 媒体库移除 %d 个视频。你也想要从将这些文件移至桌面垃圾箱么？

该操作将不可撤销。 时间调整出错 时间修改无法在以下照片上取消。 时间： 色彩 色彩： 标题 标题： 要开始使用，请用以下任意一种方式导入照片： 要尝试从其他服务导入，请从上面的菜单中选择一个。 尝试发布到其他的服务，从上面的菜单中选择一个。 到： 今天 过渡延时(_E)： 回收站 回收站为空 TumblrPublisher：start( )：无法启动；该发布工具不可重新启动。 类型 取消标注(_F) 无法创建缓存目录 %s：%s 无法创建数据文件夹 %s：%s 无法创建子数据文件夹 %s：%s 无法创建临时文件夹 %s：%s 无法解码文件 由于错误无法从相机中删除 %d 个照片/视频。 无法显示常见问题 %s 无法显示帮助 %s 由于文件错误无法创建 %d 照片副本 无法导出 %s：%s 不能输出为背景到 %s：%s 由于文件错误无法导出以下照片。

 无法从相机获取预览：
%s 无法为 %s 生成临时文件：%s 无法启动 Nautilus 文件管理器的发送至：%s 无法启动编辑器：%s 无法锁定相机：%s 无法监视 %s：不是一个目录(%s) 无法转向 Bug 数据库：%s 无法在文件管理器中打开：%s 无法打开/创建照片数据库 %s：错误代码 %d 无法准备桌面幻灯片：%s 无法打印照片：

%s 无法进行监控更新：%s 无法发布 无法重命名搜索为 %s，同名搜索已存在。 无法重命名标签为 %s，同名标签已存在。 不能卸载相机。请尝试从文件管理器中卸载相机。 无法写入照片数据库文件：
%s 不限制 取消隐藏 取消 撤销颜色变换 正在取消日期和时间调整 正在取消增强 正在取消水平翻转 正在取消垂直翻转 正在取消还原 正在取消旋转 取消标注 取消标注选定照片 正在取消标注选定照片 验证 Shotwell 数据库时发生未知错误：%s 正在取消合并 未修改的 卸载... 未评分 不支持的文件格式 更新媒体库... 上传大小(_S)： 正在上传 %d/%d 使用标准尺寸(_S)： 使用自定义尺寸(_U)： 用户名(_) 用户中止导入 无效的用户名或密码。请重试 视频隐私设置(_S)： 视频 视频可见于(_V)： 视频和新照片相册可见于(_V)： 视频将出现在“%s” 视频将出现在： 查看照片所属活动(_N) 访问 Yandex.Fotki 网站 访问 Yorba 网站 用于皮夹(2 x 3 英寸) 欢迎使用 Shotwell ！ 欢迎使用 F-Spot 媒体库导入服务。

请选择一个要导入的数据，既可以是 Shotwell 自动找到的，也可以是手工制定的文件。 欢迎使用 F-Spot 媒体库导入服务。

请选择一个 F-Spot 数据库文件。 欢迎！ 想在哪里发布选中的照片？ 宽度 宽度或者高度 写入标签、标题和其他元数据到照片文件(_M) 写入元数据到文件... 年%s月 年%s月%s日 年%s月-日 年-月-日 昨天 您已经作为 %s 登录 Facebook。

 您已经作为 %s 登录到 Flickr。

 您已经作为 %s 登录 Picasa 网络相册。 您以 %s 的身份登陆了 Tumblr。

 您已经作为 %s 登录 YouTube。 您尚未登录 Facebook.

如果您没有 Facebook 帐号，可以在登录过程中创建一个。在登陆过程中， Shotwell Connect 将请求您上传照片和发布到订阅列表的权限。这些权限对于 Shotwell Connect 的工作是必须的。 您尚未登录 Flickr。

您必需在注册一个 Flickr 帐号才能完成登录过程。在登录过程中您需要特别授权 Shotwell 连接到您的 Flickr 帐号。 您当前尚未登录 Picasa 网络相册。

在您的浏览器中点击登录来完成 Picasa 网络相册等各路。您必须授权 Shotwell Connect 来连接至您的 Picasa 网络相册。 您当前未登陆至 Yandex.Fotki。 您尚未登录 YouTube。

您必须已经注册了一个 Google 帐号并设置为使用 YouTube 才能继续。您可以通过浏览器设置绝大多数帐号并至少登录 YouTube 一次。 您可以使用以下任意方式导入照片： 您没有启用任何数据导入插件。

要使用从应用程序导入数据的功能，您需要至少启用一个数据导入插件。您可在首选想对话框中启用插件。 您已经登录 Flickr 但在本次 Shotwell 进程中已离开。
要继续发布到 Flickr，请退出并重新启动 Shotwell，然后再尝试发布。 您已经登录 Google 但在本次 Shotwell 进程中已离开。
要继续发布到 Google，请退出并重新启动 Shotwell，然后再尝试发布。 您的 Flickr Pro 帐号让您可以无限上传。 您的 Flickr 免费帐号限制您每月可以上传的数据。
本月您的流量限额还剩 %d MB。 您的媒体库和当前版本的 Shotwell 不兼容。它似乎是由 Shotwell %s (schema %d)创建的。当前版本是 %s (schema %d)。请删除 %s 来释放您的照片库然后重新导入照片。 您的媒体库和当前版本的 Shotwell 不兼容。它似乎是由 Shotwell %s (schema %d)创建的，而当前版本是 %s (schema %d) 。请使用最新版本的 Shotwell。 缩放 _100% 缩放 _200% 放大(_I) 缩小(_O) 缩放照片至 100% 比例 缩放照片至 200% 比例 缩放照片以适合屏幕 [文件] 关于(_A) 添加标注(A)… 调整(_A) 修改日期和时间(_A)... 已有或新相册(_A)： 所有照片(_A) 升序(_A) 自动(_A)： 背景(_B)： 基本信息(_B) 取消(_C) 关闭(_C) 备注(_C) 目录(_C) 复制颜色调整(_C) 剪裁(_C) 降低(_D) 延时(_D)： 删除(_D) 删除标签 %s (_D) 开发人员(_D) 目录结构(_D)： 不上传标签(_D) 不再显示该消息(_D) 副本(_D) 编辑(_E) 编辑(_E)... 电子邮件地址(_E) 增强(_E) 导出(_E)... 文件(_F) 过滤照片(_F) 查找(_F) 查找(_F)... 标注(_F) 禁止下载原始照片(_F) 格式(_F)： 常见问题(_F) 帮助(_H) 如果有标题而没有备注，将标题作为备注(_I) 导入(_I) 从文件夹导入(_I)... 保留原位置导入(_I) 从您的 %s 文件夹导入照片(_I) 导入照片到(_I)： 提高(_I) 保留(_K) 登录(_L) 注销(_L) 匹配(_M) 匹配照片比例(_M) 合并事件(_M) 修改原始文件(_M) 修改原始文件(_M) 修改原始照片文件(_M) 修改原始照片文件(_M) 移至回收站(_M) 搜索名称(_N)： 新建(_N) 新建活动(_N) 下一张照片(_N) 输出照片为(_O)： 密码(_) 粘贴颜色调整(_P) 范例(_P)： 照片(_P) 照片(_P) 播放视频(_P) 首选项(_P) 前一张照片(_P) 打印(_P)... 发布(_P) 质量(_Q)： 退出(_Q) 评分(_R) 红眼(_R) 重做(_R) 次品(_R) 记住密码(_R) 移除(_R) 上传前去除照片的地理位置、相机型号和其他可供辨认的信息(_R) 重命名(_R)... 替换(_R) 汇报问题(_R)… 重置(_R) 还原(_R) 保存(_S) 保存副本(_S) 缩放限制(_S)： 搜索栏(_S) 设置评分(_S) 按照同样大小调整照片/视频(_S) 跳过(_S) 停止导入(_S) 校正(_S) 切换开发者(_S) 标题(_T) 过渡效果(_T)： 移至垃圾箱(_T) 您的 Piwigo 照片库 _URL(_) 取消(_U) 卸载(_U) 未评分(_U) 查看(_V) 监视媒体库文件夹中的新文件(_W) 一个 RAW 照片 一个视频 全部 和 及更高 及更低 任何 任意照片 黑 厘米 包含 无法将 %s⏎ »拷贝到 %s 日 不包含 创建现有媒体文件的副本 结束以 错误信息: 外部修改 已标注 有 没有 小时 英寸 内部修改 是 晚于 先于 介于 必须有 不是 未设为 标签 分钟 修改 修改 无 未标注 以下这些： 仅 时间段 每英寸像素数 秒 秒 以开始 Tommy He <tommy.he@linux.com>, 2011
zheng7fu2 <zheng7fu2@gmail.com>, 2011
Xhacker Liu <liu.dongyuan@gmail.com>, 2011
b6i <baddogai@gmail.com>, 2011
YunQiang Su <wzssyqa@gmail.com>, 2012
Aron Xu <happyaron.xu@gmail.com>, 2012
Jason Lau, 刘家昌 <i@dotkrnl.com>, 2013

Launchpad Contributions:
  Abel Liu https://launchpad.net/~guiltyritter
  Aron Xu https://launchpad.net/~happyaron
  Carlos Gong https://launchpad.net/~bfsugxy
  David Gao https://launchpad.net/~davidgao1001
  Jason Lau https://launchpad.net/~dotkrnl
  Luo Lei https://launchpad.net/~luolei
  Meng Zhuo https://launchpad.net/~mengzhuo1203
  Qiu Haoyu https://launchpad.net/~timothyqiu
  Tommy He https://launchpad.net/~tommy-he
  Vera Yin https://launchpad.net/~vera-yorba
  Wang Dianjin https://launchpad.net/~tuhaihe
  Wylmer Wang https://launchpad.net/~wantinghard
  Xhacker Liu https://launchpad.net/~xhacker
  YunQiang Su https://launchpad.net/~wzssyqa
  ZHOU Qi https://launchpad.net/~esperisto
  lovenemesis https://launchpad.net/~lovenemesis
  mike2718 https://launchpad.net/~mike2718
  slgray https://launchpad.net/~slgray-chan
  丁威扬 https://launchpad.net/~dingvyoung 未命名 白 在以下分类中: 