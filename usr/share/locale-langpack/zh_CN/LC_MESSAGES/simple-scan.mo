Þ    £      4  ß   L
      È     É     Ð     á     ð     	  â        ñ     õ     ù     ý            	   0     :  	   Q     [     `     s                    µ  	   Æ     Ð     Õ     ì     ó          "     2     ;  _   L  _   ¬  [        h             *   ²     Ý     ø            :   (     c     i  !   x       4        Ô     à     ð               '     .     3     ;     C     K  	   P  
   Z     e     i  1   v     ¨  4   ½     ò       
        &     8  5   >     t            )   ¢  *   Ì     ÷                     '     B     \     d     p     }       /     (   È  E   ñ  #   7     [  
   `     k          ¡     ¹     Ì     å     ë     ð     ÿ  
     #        <     Q     f  F   r     ¹     ×     ä     ù     þ       :     8   T  M     O   Û     +  <   B  :     N   º  c  	     m  h        õ       (   +      T     u  -         ¸  %   Ù  %   ÿ  $   %  !   J  5   l  &   ¢     É  
   è     ó     û  	               	        $     +     1     B     J     P  
   V     a     t       
             ¤     ¼     Ö  Ã  é     ­!     ´!     Ä!     Ô!     ê!  ã   ï!     Ó"     ×"     Û"     ß"     æ"     ö"     #     #     9#     @#     G#  	   T#     ^#  	   r#     |#     #     #     «#     ²#     È#     Ï#     Ü#     ø#     $     $  S   $  T   p$  T   Å$     %     6%     F%  )   _%     %     %     ²%     ¿%  )   Ï%     ù%      &     &     0&  -   7&     e&     r&     &     &     £&  
   °&     »&     Â&  	   É&  	   Ó&     Ý&     ä&     ë&     ò&  	   ù&  6   '     :'  0   P'     '     '     ¡'     ±'     Ç'  0   Î'  	   ÿ'     	(  	   (     &(     8(  	   J(     T(     [(     n(     (     (     ­(     ´(     Ä(     Ô(     å(     ö(     )  E   ()     n)     )     )     ¡)     ·)     Ð)     é)     ÿ)  
   *     !*     (*     5*     I*     Y*     x*     *  	   *  ?   ©*     é*  
   +     +     )+     0+     C+  7   J+  :   +  S   ½+  <   ,     N,  1   d,  .   ,  <   Å,  A  -     D/  W   Z/     ²/     Ë/     ä/  !   0     %0  &   80     _0  '   0  '   §0  %   Ï0  "   õ0  *   1     C1     ]1  
   x1  
   1  
   1  
   1  
   ¤1     ¯1  
   ½1     È1  
   Ù1     ä1  
   õ1      2  
   2     2     !2     82  
   I2     T2     e2     |2  *   2  6  ¾2               @       t   ¢   3                 u   U   7      w                 I   z           ~   ;   _   >                 P   Q   H                           e         N       #   E   }   K   F   W      `       X              v         \   *   =   x              ?                              5   j   <          r          B         T   g               0   :               9      2   "       ]          A       1   +                              [   q       k           o   ,   /          -          C   c   n   f   Z              i   y      8          	   
      D   M          S   4              h   G       '         £       s   )              J   &   ^      ¡   O      L           %         b   !   V   |      m   $       a       6            Y                             p   .   l          R   d      (          {       %d dpi %d dpi (default) %d dpi (draft) %d dpi (high resolution) 4Ã6 A really easy way to scan both documents and photos. You can crop out the bad parts of a photo and rotate it if it is the wrong way round. You can print your scans, export them to pdf, or save them in a range of image formats. A_4 A_5 A_6 About About Simple Scan Additional software needed All Files All Pages From _Feeder Automatic Back Brightness of scan Brightness: Change _Scanner Combine sides Combine sides (reverse) Contrast of scan Contrast: Crop Crop the selected page Darker Device to scan from Directory to save files to Discard Changes Document Document Scanner Drivers for this are available on the <a href="http://samsung.com/support">Samsung website</a>. Drivers for this are available on the <a href="http://support.brother.com">Brother website</a>. Drivers for this are available on the <a href="http://support.epson.com">Epson website</a>. Drivers installed successfully! Email... Error communicating with scanner Failed to install drivers (error code %d). Failed to install drivers. Failed to save file Failed to scan File format: Fix PDF files generated with older versions of Simple Scan Front Front and Back Height of paper in tenths of a mm Help If you don't save, changes will be permanently lost. Image Files Install drivers Installing drivers... JPEG (compressed) Keep unchanged Le_gal Less Lighter Maximum Minimum More Move Left Move Right New New Document No scanners available.  Please connect a scanner. No scanners detected Once installed you will need to restart Simple Scan. PDF (multi-page document) PNG (lossless) Page Size: Page side to scan Photo Please check your scanner is connected and powered on Preferences Print debugging messages Print... Quality value to use for JPEG compression Quality value to use for JPEG compression. Quality: Quit Quit without Saving Reorder Pages Resolution for photo scans Resolution for text scans Reverse Rotate Left Rotate Right Rotate _Left Rotate _Right Rotate the page to the left (counter-clockwise) Rotate the page to the right (clockwise) Run '%s --help' to see a full list of available command line options. SANE device to acquire images from. Save Save As... Save current document? Save document before quitting? Save document to a file Saving document... Saving page %d out of %d Sc_an Scan Scan Documents Scan S_ource: Scan Side: Scan a single page from the scanner Scanned Document.pdf Show release version Simple Scan Simple Scan uses the SANE framework to support most existing scanners. Simple document scanning tool Single _Page Start a new document Stop Stop the current scan Text The brightness adjustment from -100 to 100 (0 being none). The contrast adjustment from -100 to 100 (0 being none). The directory to save files to. Defaults to the documents directory if unset. The height of the paper in tenths of a mm (or 0 for automatic paper detection). The page side to scan. The resolution in dots-per-inch to use when scanning photos. The resolution in dots-per-inch to use when scanning text. The width of the paper in tenths of a mm (or 0 for automatic paper detection). This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. Type of document being scanned Type of document being scanned. This setting decides on the scan resolution, colors and post-processing. Unable to connect to scanner Unable to open help file Unable to open image preview application Unable to save image for preview Unable to start scan Username and password required to access '%s' Width of paper in tenths of a mm You appear to have a Brother scanner. You appear to have a Samsung scanner. You appear to have an Epson scanner. You appear to have an HP scanner. You need to install driver software for your scanner. You need to install the %s package(s). [DEVICE...] - Scanning utility _Authorize _Cancel _Close _Contents _Crop _Custom _Document _Email _Help _Install Drivers _Letter _None _Page _Password: _Photo Resolution: _Rotate Crop _Save _Stop Scan _Text Resolution: _Username for resource: scan;scanner;flatbed;adf; translator-credits Project-Id-Version: simple-scan
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2016-03-20 21:13+0000
PO-Revision-Date: 2016-04-11 05:38+0000
Last-Translator: YunQiang Su <wzssyqa@gmail.com>
Language-Team: Chinese (Simplified) <zh_CN@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 18:48+0000
X-Generator: Launchpad (build 18115)
Language: zh_CN
 %d dpi %d dpi (é»è®¤) %d dpi (èç¨¿) %d dpi (é«åè¾¨ç) 4Ã6 ä¸ä¸ªéå¸¸ç®åçæ¹æ³æ¥åæ¶æ«æææ¡£åç§çãå¦æå¾çæ¾ç¤ºæè¯¯ï¼æ¨å¯ä»¥æ ¹æ®éè¦éè¿è£åªãæè½¬ç­æ¹å¼è°æ´ç§çãæ¨ä¹å¯ä»¥æå°æ«æä»¶ãå¯¼åº PDF æ ¼å¼ãæèä¿å­ä¸ºå¾çã A_4 A_5 A_6 å³äº å³äºæ«ææ éè¦å®è£å¶ä»è½¯ä»¶ æææä»¶ ä¼ éæ¥çææé¡µé¢(_F) èªå¨ èé¢ æ«æäº®åº¦ äº®åº¦ï¼ æ´æ¢æ«æä»ª(_S) åå¹¶é¡µ åå¹¶é¡µ(åå) æ«æå¯¹æ¯åº¦ å¯¹æ¯åº¦ï¼ åªè£ è£åªéä¸­çé¡µé¢ è¾æ·± æ«æè®¾å¤ ä¿å­æä»¶çæä»¶å¤¹ï¼ æ¾å¼æ´æ¹ ææ¡£ ææ¡£æ«æä»ª è¯¥é©±å¨ç¨åºå¯å¨<a href="http://samsung.com/support">ä¸æç½ç«</a>è·å¾ã è¯¥é©±å¨ç¨åºå¯å¨ <a href="http://support.brother.com">åå¼ç½ç«</a>è·å¾ã è¯¥é©±å¨ç¨åºå¯å¨<a href="http://support.epson.com">ç±æ®çç½ç«</a>è·å¾ã æåå®è£é©±å¨ç¨åºï¼ çµå­é®ä»¶... ä¸æ«æä»ªéä¿¡éè¯¯ æ æ³å®è£é©±å¨ç¨åº(éè¯¯ç  %d)ã å®è£é©±å¨å¤±è´¥ã ä¿å­æä»¶éè¯¯ æ«æå¤±è´¥ æä»¶æ ¼å¼ï¼ ä¿®å¤æ§çæ«ææçæç PDF æä»¶ æ­£é¢ æ­£é¢åèé¢ çº¸å¼ çé«åº¦ (ä»¥ 0.1mm è®¡) å¸®å© å¦ææ¨ä¸ä¿å­ï¼åæ´å°æ°¸ä¹ä¸¢å¤±ã å¾åæä»¶ å®è£é©±å¨ å®è£é©±å¨ç¨åºâ¦ JPEG (åç¼©) ä¿æä¸å æ³å®(_G) è¾å° ç¨æµ æå¤§å¼ æå°å¼ æ´å¤ å·¦ç§» å³ç§» æ°å»º æ°ææ¡£ æ²¡æå¯ç¨çæ«æä»ªãè¯·è¿æ¥ä¸ä¸ªæ«æä»ªã æ²¡æåç°æ«æä»ª å®è£å®æåï¼éè¦éæ°å¯å¨æ«ææã PDF (å¤é¡µææ¡£) PNG (æ æ) é¡µé¢å¤§å°ï¼ æ«æçº¸å¼ çåªé¢ ç§ç è¯·æ£æ¥æ¨çæ«æä»ªå·²ç»è¿æ¥å¹¶ä¸æå¼ é¦éé¡¹ æ¾ç¤ºè°è¯ä¿¡æ¯ æå°... JPEG åç¼©è´¨é JPEG åç¼©è´¨é è´¨éï¼ éåº éåºä¸ä¸ä¿å­ éæ°æåºé¡µé¢ ç§çæ«æåè¾¨ç ææ¬æ«æåè¾¨ç åå éæ¶éæè½¬ é¡ºæ¶éæè½¬ åå·¦æè½¬(_L) åå³æè½¬(_R) å·¦æé¡µé¢ (éæ¶é) å³æé¡µé¢ (é¡ºæ¶é) è¿è¡â%s --helpâæ¥æ¥çå¯ç¨å½ä»¤è¡éé¡¹çå®æ´åè¡¨ã ä» SANE è®¾å¤è·åå¾åã ä¿å­ ä¿å­ä¸º... ä¿å­å½åææ¡£ï¼ éåºåä¿å­ææ¡£ï¼ å°ææ¡£ä¿å­ä¸ºæä»¶ æ­£å¨ä¿å­ææ¡£... æ­£å¨ä¿å­ %d/%d é¡µ æ«æ(_A) æ«æ æ«æææ¡£ æ«ææ¥æº(_O)ï¼ æ«æé¡µé¢ï¼ ä»æ«æä»ªæ«æä¸ä¸ªåé¡µ æ«æçæä»¶.pdf æ¾ç¤ºçæ¬ä¿¡æ¯ æ«ææ æ«ææä½¿ç¨ SANE æ¡æ¶æ¯æå¤§å¤æ°ç°æçæ«æä»ªã ç®æçæä»¶æ«æå·¥å· åé¡µ(_P) å¼å§ä¸ä¸ªæ°çææ¡£ åæ­¢ åæ­¢å½åæ«æ ææ¬ ä» -100 å° 100 çäº®åº¦è°èï¼0 ä¸ºä¸ä½è°èï¼ ä» -100 å° 100 çå¯¹æ¯åº¦è°èï¼0 ä¸ºä¸ä½è°èï¼ ç¨äºä¿å­æä»¶çæä»¶å¤¹ãå¦æä¸è®¾ç½®ï¼é»è®¤ä½¿ç¨ ææ¡£ æä»¶å¤¹ã çº¸å¼ çé«åº¦ (ä»¥ 0.1mm è®¡ï¼0 ä»£è¡¨èªå¨æ£æ¥çº¸å¼ ) è¦æ«æçé¡µé¢ã æ«æç§çæ¶ä½¿ç¨çåè¾¨ç (ç¹/è±å¯¸)ã ä¸æ¬¡æ«æææ¡£çåè¾¨ç (ç¹/è±å¯¸)ã çº¸å¼ çå®½åº¦ (ä»¥ 0.1mm è®¡ï¼0 ä»£è¡¨èªå¨æ£æ¥çº¸å¼ ) æ¬ç¨åºä¸ºèªç±è½¯ä»¶ï¼æ¨å¯ä¾æ®èªç±è½¯ä»¶åºéä¼æåå¸ç GNU éç¨å¬å±æææ¡æ¬¾ï¼
å¯¹æ¬ç¨åºè¿è¡éæ°ååå/æä¿®æ¹ï¼æ è®ºæ¨ä¾æ®çæ¯æ¬ææçç¬¬ä¸çï¼æï¼æ¨èªç±
éæ©çï¼ä»»ä¸æ¥ååè¡ççæ¬ã

æ¬ç¨åºæ¯åºäºä½¿ç¨ç®çèå ä»¥åå¸ï¼ç¶èä¸æ¿æä½æä¿è´£ä»»ï¼äº¦æ å¯¹éå®æ§æç¹
å®ç®çéç¨æ§æä¸ºçé»ç¤ºæ§æä¿ãè¯¦æè¯·åç§ GNU éç¨å¬å±ææã

æ¨åºå·²æ¶å°ééäºæ¬ç¨åºç GNU éç¨å¬å±ææçå¯æ¬ï¼å¦ææ²¡æï¼è¯·åç§
<http://www.gnu.org/licenses/>. æ«æææ¡£çç±»å è¦æ«æçæä»¶ç±»åãæ­¤è®¾ç½®å°å³å®æ«æçåè¾¨çãè²å½©åé¢å¤çã æ æ³è¿æ¥å°æ«æä»ª æ æ³æå¼å¸®å©æä»¶ æ æ³æå¼å¾åé¢è§è½¯ä»¶ æ æ³ä¿å­ç¨ä»¥é¢è§çå¾å æ æ³å¼å§æ«æ ä½¿ç¨â%sâéè¦ç¨æ·ååå¯ç  çº¸å¼ çå®½åº¦ (ä»¥ 0.1mm è®¡) æ¨ä¼¼ä¹æä¸ä¸ª Brother æ«æä»ªã æ¨ä¼¼ä¹æä¸ä¸ª Samsung æ«æä»ªã æ¨ä¼¼ä¹æä¸ä¸ª Epson æ«æä»ªã æ¨ä¼¼ä¹æä¸ä¸ª HP æ«æä»ªã æ¨éè¦ä¸ºæ«æä»ªå®è£é©±å¨ç¨åºã æ¨éè¦å®è£ %s åã [è®¾å¤...] - æ«æå·¥å· è®¤è¯(_A) åæ¶(_C) å³é­(_C) åå®¹(_C) åªè£(_C) èªå®ä¹(_C) ææ¡£(_D) çµå­é®ä»¶(_E) å¸®å©(_H) å®è£é©±å¨(_I) ä¿¡ä»¶(_L) æ (_N) é¡µé¢(_P) å¯ç (_P)ï¼ å¾ååè¾¨ç(_P)ï¼ æè½¬åªè£(_R) ä¿å­(_S) åæ­¢æ«æ(_S) æå­åè¾¨ç(_T)ï¼ èµæºç¨æ·å(_U)ï¼ scan;scanner;flatbed;adf;æ«æ;æ«æä»ª; Launchpad Contributions:
  Alexey Kotlyarov https://launchpad.net/~koterpillar
  Aron Xu https://launchpad.net/~happyaron
  Carlos Gong https://launchpad.net/~bfsugxy
  David Gao https://launchpad.net/~davidgao1001
  Dennis Kuang https://launchpad.net/~denniskrq
  Dingyuan Wang https://launchpad.net/~gumblex
  Heling Yao https://launchpad.net/~hyao
  Jimhu https://launchpad.net/~huyiwei
  MR.wangmaoxin https://launchpad.net/~ddllkl
  Wang Dianjin https://launchpad.net/~tuhaihe
  Wylmer Wang https://launchpad.net/~wantinghard
  Xhacker Liu https://launchpad.net/~xhacker
  YunJian https://launchpad.net/~tld5yj
  YunQiang Su https://launchpad.net/~wzssyqa
  ZhangCheng https://launchpad.net/~xxzc
  otm123 https://launchpad.net/~otm123
  shijing https://launchpad.net/~shijing
  zixue.liu https://launchpad.net/~zixue 