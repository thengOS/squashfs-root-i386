��    	      d      �       �      �      �      �      �   4        H  �   \     D  �  R                    +  3   8     l  �   y     1        	                                      Manpages Open Search Manpages Search for Manpages Sorry, there are no Manpages that match your search. Technical Documents This is an Ubuntu search plugin that enables information from local manpages to be searched and displayed in the Dash underneath the Code header. If you do not wish to search this content source, you can disable this search plugin. manpages;man; Project-Id-Version: unity-scope-manpages
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-03-24 16:55+0000
PO-Revision-Date: 2014-04-10 14:06+0000
Last-Translator: Luo Lei <luolei@ubuntukylin.com>
Language-Team: Chinese (Simplified) <zh_CN@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-06-27 19:23+0000
X-Generator: Launchpad (build 18115)
 手册 打开 搜索手册 搜索手册 对不起，没有搜索到相关的手册内容。 技术文档 这是一个 Ubuntu 搜索插件，能够在 Dash 的”代码“栏目中搜索来自本地手册的信息 。如果您不希望搜索这个内容的来源，可以禁用此插件。 manpages;man;手册;手册页; 