��    s      �  �   L      �	  1  �	     �
       &        >  ,   F     s     �     �  �   �  �   M            	        (  F   0  *   w  :   �     �     �            ,   :     g     s  &   �  &   �  !   �     �     �       O   4  .   �     �     �     �     �     �  �     A   �     �  K     "   c      �     �     �     �     �          "  %   B  G   h     �  2  �  O   �     B     S     _     k  #   �  "   �     �  8   �  '     )   C     m     �     �     �  2   �  0   �     "     B     V  (   l  �   �  I   B  ;   �  "   �  �   �  ,   �  Q   �  �         �  A     ,   H     u     �     �     �     �  Y   �  O   ,  4   |     �  A   �  
   �       �     V   �  D   �  @   3  P   t  A   �  L     "   T     w  	   �     �     �     �     �  
   �     �  �  �  �   �      �!     �!  (   �!     "  !   &"     H"     ["     j"  �   }"  �   �"     �#     �#     �#     �#  :   �#  ,   $  '   4$     \$     r$     �$     �$     �$     �$     �$     �$      %  $   %     >%     E%     ^%  ;   z%  @   �%     �%     �%     &     *&     F&  �   \&  -   �&     '  9   +'     e'  !   �'     �'     �'     �'     �'     (     (  '   6(  D   ^(     �(    �(  ]   �)     *     ,*  	   =*     G*     W*     s*     �*  '   �*  $   �*  '   �*     +     -+     =+     J+  (   c+  !   �+     �+     �+     �+  -   �+  �    ,  9   �,  -   �,     -  �   1-  *   �-     �-  �   �-     �.  0   �.  0   /     ?/     Z/     p/     }/     �/  I   �/  <   �/  !   "0     D0  .   K0  	   z0     �0  �   �0  B   +1  D   n1  /   �1  ;   �1  3   2  ?   S2     �2     �2  
   �2     �2     �2     �2     �2     3     #3        +                  $   W   7          k   '                @   g   K      9   R           3      6       D   C               <   .   f      L   l       	   Q   X       e          s      Z   ?   N   P   &   F          4           a   B   ]   [               (                 "   p   ,   c   b   1   U   j   5       /   A   :          #   G      E      d       
   _          S      h   -      n              J       m   \   M       `               2       )       >   T       0      i   *   %   o   ;   V   8             r   !   ^       =       I   q                            H   Y      O    
A normal upgrade can not be calculated, please run:
  sudo apt-get dist-upgrade


This can be caused by:
 * A previous upgrade which didn't complete
 * Problems with some of the installed software
 * Unofficial software packages not provided by Ubuntu
 * Normal changes of a pre-release version of Ubuntu %(size).0f kB %(size).0f kB %.1f MB %i obsolete entries in the status file %s base %s needs to be marked as manually installed. %s will be downloaded. .deb package A file on disk An unresolvable problem occurred while calculating the upgrade.

Please report this bug against the 'update-manager' package and include the following error message:
 An unresolvable problem occurred while initializing the package information.

Please report this bug against the 'update-manager' package and include the following error message:
 Building Updates List Cancel Changelog Changes Changes for %s versions:
Installed version: %s
Available version: %s

 Check if a new Ubuntu release is available Check if upgrading to the latest devel release is possible Checking for updates… Connecting... Copy Link to Clipboard Could not calculate the upgrade Could not initialize the package information Description Details of updates Directory that contains the data files Do not check for updates when starting Do not focus on map when starting Download Downloading changelog Downloading list of changes... Failed to download the list of changes. 
Please check your Internet connection. However, %s %s is now available (you have %s). Install Install All Available Updates Install Now Install missing package. Installing updates… It is impossible to install or remove any software. Please use the package manager "Synaptic" or run "sudo apt-get install -f" in a terminal to fix this issue at first. It’s safer to connect the computer to AC power before updating. No longer downloadable: No network connection detected, you can not download changelog information. No software updates are available. Not all updates can be installed Not enough free disk space Obsolete dpkg status entries Obsolete entries in dpkg status Open Link in Browser Other updates Package %s should be installed. Please wait, this can take some time. Remove lilo since grub is also installed.(See bug #314004 for details.) Restart _Later Run a partial upgrade, to install as many updates as possible.

    This can be caused by:
     * A previous upgrade which didn't complete
     * Problems with some of the installed software
     * Unofficial software packages not provided by Ubuntu
     * Normal changes of a pre-release version of Ubuntu Run with --show-unsupported, --show-supported or --show-all to see more details Security updates Select _All Settings… Show all packages in a list Show all packages with their status Show and install available updates Show debug messages Show description of the package instead of the changelog Show supported packages on this machine Show unsupported packages on this machine Show version and exit Software Updater Software Updates Software index is broken Software updates are no longer provided for %s %s. Some software couldn’t be checked for updates. Support status summary of '%s': Supported until %s: Technical description Test upgrade with a sandbox aufs overlay The changelog does not contain any relevant changes.

Please use http://launchpad.net/ubuntu/+source/%s/%s/+changelog
until the changes become available or try again later. The computer also needs to restart to finish installing previous updates. The computer needs to restart to finish installing updates. The computer will need to restart. The list of changes is not available yet.

Please use http://launchpad.net/ubuntu/+source/%s/%s/+changelog
until the changes become available or try again later. The software on this computer is up to date. The update has already been downloaded. The updates have already been downloaded. The upgrade needs a total of %s free space on disk '%s'. Please free at least an additional %s of disk space on '%s'. Empty your trash and remove temporary packages of former installations using 'sudo apt-get clean'. There are no updates to install. This update does not come from a source that supports changelogs. To stay secure, you should upgrade to %s %s. Unimplemented method: %s Unknown download size. Unsupported Unsupported:  Update is complete Updated software has been issued since %s %s was released. Do you want to install it now? Updated software is available for this computer. Do you want to install it now? Updated software is available from a previous check. Updates Upgrade using the latest proposed version of the release upgrader Upgrade… Version %s: 
 When upgrading, if kdelibs4-dev is installed, kdelibs5-dev needs to be installed. See bugs.launchpad.net, bug #279621 for details. You are connected via roaming and may be charged for the data consumed by this update. You have %(num)s packages (%(percent).1f%%) supported until %(time)s You have %(num)s packages (%(percent).1f%%) that are unsupported You have %(num)s packages (%(percent).1f%%) that can not/no-longer be downloaded You may not be able to check for updates or download new updates. You may want to wait until you’re not using a mobile broadband connection. You stopped the check for updates. _Check Again _Continue _Deselect All _Partial Upgrade _Remind Me Later _Restart Now… _Try Again updates Project-Id-Version: update-manager HEAD
Report-Msgid-Bugs-To: sebastian.heinlein@web.de
POT-Creation-Date: 2016-04-12 03:58+0000
PO-Revision-Date: 2016-04-12 12:09+0000
Last-Translator: Yiding He <yiding.he@gmail.com>
Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2016-06-27 17:19+0000
X-Generator: Launchpad (build 18115)
Language: zh_CN
 
无法进行正常升级，请运行：
  sudo apt-get dist-upgrade


这可能由下面原因引起：
 * 上一次升级不完整
 * 部分已安装软件的问题
 * 不是 Ubuntu 提供的非官方软件包
 * Ubuntu 预览版本的正常更新 %(size).0f kB %.1f MB 状态文件中包含 %i 项过期条目 %s 核心组件 %s 需要标记为手动安装。 将会下载 %s。 .deb 软件包 磁盘上的文件 计算升级时发生无法解决的问题。

请报告这个错误，针对“更新管理器”并包括以下错误消息：
 初始化包信息时遇到无法解决的问题。

请汇报这个“update-manager”软件包的错误，并且将如下信息包含在报告中：
 正在建立更新列表 取消 变更日志 变更 %s的版本变化:
已安装版本:%s
可用的版本:%s

 检查是否有新的 Ubuntu 发行版可用 验证是否能够升级到最新版本 正在检查更新... 正在连接... 复制链接到剪贴板 无法计算升级 无法初始化软件包信息 描述 更新详情 包含数据文件的文件夹 启动时不检查更新 开始时焦点不要定位在图上 下载 正在下载变更日志 正在下载更新列表... 无法下载更新列表。 
请检查您的网络连接。 然而， %s %s 现在可以安装了（您正在使用 %s）。 安装 安装全部可利用的更新 立即安装 安装缺失的软件包。 正在安装更新... 无法安装或删除任何软件。请使用新立得软件包管理器或在终端运行 "sudo apt-get install -f" 来修正这个问题。 在更新前最好将计算机连接电源。 已不能下载： 未检测到网络连接，您无法下载更新日志。 没有可用的软件更新。 不是全部更新都可以安装 磁盘空间不足 过期的 dpkg 状态条目 dpkg 状态中的过期条目 在浏览器中打开链接 其他更新 包 %s 应当被安装。 请稍候，这需要花一些时间。 由于 grub 已安装，移除 lilo。(详情参见 bug #314004 。) 稍后重启(_L) 运行部分的升级，安装尽可能多的更新。

    这可能由以下原因造成：
     *先前未完成的升级
     *已安装的软件的一些问题
     *非官方的软件不是由Ubuntu提供的软件包
     *正常变化的预发行版本的Ubuntu 通过 --show-unsupported、--show-supported 或 --show-all 运行以查看更多详细信息 安全更新 选择全部(_A) 设置... 列出所有包 显示所有包及其状态 显示并安装可用更新 显示调试消息 显示包的说明而不是变更日志 显示此计算机中受支持的包 显示此计算机中不受支持的包 显示版本并退出 软件更新器 软件更新 软件索引已经损坏 已不再为 %s %s 提供软件更新。 无法为某些软件检查更新 '%s' 的支持状态摘要： 支持截止时间 %s： 技术说明 进行带沙盒 aufs 隔离层的测试升级 变更日志未包含任何有关的更改。

请访问 http://launchpad.net/ubuntu/+source/%s/%s/+changelog
直到变更可用或再试一遍。 需要重启计算机才能完成安装以前的更新。 计算机需要重启以完成安装更新。 需要重启计算机 变更列表尚无法获取。

请使用 http://launchpad.net/ubuntu/+source/%s/%s/+changelog
直到能够访问该列表或稍后再试。 此计算机中的软件是最新软件。 更新已经下载。 这个更新需要花去 %s 磁盘上总计 %s 的空间。请在 %s 磁盘上留出 %s 空间。清空您的回收站和临时文件，用“sudo apt-get clean”清理以前的安装文件。 没有更新需要安装。 更新不是来源于支持日志变更的源。 为了保持安全，您应该升级到 %s %s。 尚未实现的方法：%s 未知下载大小。 不受支持 不受支持：  更新完成 已有在 %s %s 发布后发行的软件更新。您想现在安装吗？ 这台计算机有可用的升级。你想现在安装吗？ 上次检查发现有可用升级 更新 使用最新 proposed 版本的升级器升级 升级... 版本 %s： 
 在升级时， 如果已经安装了 kdelibs4-dev， 那么需要安装 kdelibs5-dev。参阅 bugs.launchpad.net 上的 bug #279621 来了解详情。 您正在使用漫游连接，更新可能会耗费很多流量。 有 %(num)s 个包(%(percent).1f%%)的支持截止时间是 %(time)s 有 %(num)s 个包(%(percent).1f%%)不受支持 有 %(num)s 个包(%(percent).1f%%) (已)不能不能下载 您可能无法检查更新或下载新的更新。 您可能需要等待直到您不再使用移动宽带连接。 您停止了更新检查。 重新检查(_C) 继续(_C) 全部不选(_D) 部分升级(_P) 稍后提醒(_R) 立即重启(_R)... 再试一次(_T) 更新 