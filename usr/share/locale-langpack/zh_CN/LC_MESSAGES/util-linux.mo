��    �     <  �  \,      (;  	   );  #   3;     W;  �   p;     �;     <  (   (<     Q<     _<     m<     �<     �<  �   �<  
   2=     ==     Q=     d=      =  '   �=  %   �=     �=  :   �=  )   5>     _>  1   ~>  9   �>     �>     �>     �>  
    ?     ?     ?     1?     J?     V?     b?  	   h?  &   r?     �?     �?     �?     �?  /   �?      
@     +@     C@     S@  4   b@     �@     �@  9   �@     �@     �@     A  #   9A  %   ]A     �A     �A  D   �A     �A     B     8B     LB     kB     B  .   �B  >   �B     �B     C     ,C     HC     UC  (   rC      �C  A   �C  (   �C  L   'D     tD     �D     �D     �D     �D     �D     �D     �D  	   �D     �D     �D     E     E  $   ?E  6   dE  !   �E  1   �E  !   �E  1   F  !   CF  9   eF  %   �F  6   �F  '   �F  %   $G     JG  W   _G     �G     �G     �G     �G     H     H     &H     <H     QH  *   lH     �H  &   �H  l  �H  3   KM     M     �M     �M     �M  
   �M  ,   �M     �M     �M     �M  	   �M     �M  +   N  -   /N     ]N     lN     qN     �N     �N     �N     �N     �N     �N     �N     �N     �N  $   �N     O     -O     IO  "   OO     rO     �O     �O     �O     �O  O   �O  E   P     cP  $   P     �P  	   �P     �P     �P  
   �P     �P     Q     Q     -Q  
   @Q     KQ     WQ     ^Q     {Q     �Q     �Q     �Q     �Q     �Q     �Q  .   �Q     !R     4R     =R     CR     GR     bR     �R     �R  '   �R     �R     �R  
   �R     �R     �R     S     S  +   >S     jS  4   S     �S      �S  "   �S     T     T      $T     ET     UT     ]T     nT     rT     �T     �T  
   �T  
   �T     �T     �T     �T     �T     �T     U     U     +U     BU     SU  
   jU  F   uU     �U     �U  
   �U     �U  /   V     2V     OV  @   aV     �V  '   �V  ?   �V      W  )   :W     dW     lW     ~W     �W  0   �W  5   �W     X      X     .X  	   4X  
   >X     IX     XX     eX     uX  
   �X     �X     �X     �X     �X     �X  *   �X  "   $Y     GY     ]Y     oY     vY     ~Y     �Y     �Y     �Y     �Y  A   �Y     Z     Z     Z  	   -Z     7Z  %   DZ     jZ     {Z  !   �Z     �Z  |   �Z  T   B[     �[     �[  7   �[     �[  3   �[  C   /\     s\     �\     �\     �\     �\     �\     �\     
]     ]     !]     (]  	   5]  
   ?]     J]     []  
   l]     w]     �]     �]     �]     �]     �]  
   �]     �]     �]     �]     ^  #   ^     A^     H^     X^     h^  ,   m^  
   �^  9   �^  	   �^     �^     �^     _  #   !_  1   E_     w_     ~_     �_     �_  "   �_     �_     �_     �_     �_     `     `  	   `     `     *`     3`  
   ?`  
   J`     U`  
   ]`     h`     p`     x`     �`     �`     �`     �`     �`     �`     �`  B   a     Ga     ]a     ma     ta     |a  	   �a  	   �a     �a     �a  
   �a     �a  
   �a     �a  
   �a  	   �a  	   b     b     %b     7b     >b  �   Sb  ?   �b     4c     Qc  <   ec  -   �c     �c     �c  )   �c  '   d  $   +d  (   Pd  &   yd     �d     �d     �d  =   �d     %e  -   Be  '   pe     �e     �e     �e  
   �e     �e     �e     
f  2   f     Df     Uf     hf     tf  
   �f     �f     �f     �f     �f     �f     �f     �f  	   �f     g     g  +   3g  %   _g     �g  (   �g  
   �g  *   �g  %   h     )h  $   /h  7   Th  
   �h  	   �h  ,   �h  /   �h     �h     i  !   "i     Di     ci     pi     �i  '   �i     �i  2   �i     j     *j     Ej     Xj     lj     j     �j     �j     �j     �j     �j     �j     �j  &   k  &   ;k     bk     pk     �k  '   �k  2   �k  &   �k     l     .l     @l     Fl     Sl     _l     pl     �l     �l     �l  %   �l     �l     m  %   m     Bm     Gm     Nm     dm     lm     |m     �m     �m      �m     �m  	   �m     �m     �m     n     0n     Bn  	   Gn     Qn     ^n  2   sn     �n     �n     �n  	   �n  5   �n     o     o  -   o     Ho     Yo     oo     �o     �o     �o  (   �o     �o     	p     #p     0p     Pp  !   ep     �p     �p     �p     �p     �p  !   q     -q     Kq  ,   fq  ,   �q     �q     �q     �q     �q     r  
   (r     3r     Hr     Mr     Yr     gr     �r     �r     �r     �r      �r     s     s     #s     5s  
   9s  (   Ds  (   ms     �s     �s  5   �s  
   t     t  #    t     Dt  
   ]t     ht     tt     �t     �t  1   �t     �t  2   u  >   Au  "   �u  !   �u     �u     �u  )   �u  %   v  (   9v     bv     fv     sv     {v     �v     �v     �v     �v     �v     �v     �v     �v     �v     w      'w     Hw     Qw     lw     }w     �w     �w     �w  T  �w     +y     1y     8y  %   ?y     ey  
   hy     sy     �y     �y     �y     �y     �y  +   �y     z     !z      *z     Kz     ]z     cz     hz     xz     �z     �z     �z     �z  
   �z     �z     �z     �z      {     %{     A{     ^{     b{     g{  "   m{     �{     �{  
   �{     �{     �{     �{     �{      |  ,   6|     c|     i|     p|     u|     �|     �|     �|     �|     �|     �|     �|     �|     }     }  $   7}     \}     q}     x}     �}     �}     �}     �}  '   �}     '~     A~     ^~     d~     x~  	   �~     �~     �~  %   �~  "   �~  $   �~           >     W     r     �  %   �     �     �     �  *   �     8�     G�  �  g�     0�     B�  
   U�     `�     q�     w�     ��  :   ��     փ     ܃     �     �     �     �  �  �  
   �  #   �     B�  �   [�     �     �  ,   �     J�     Z�     f�     ��     ��  �   ��     1�     =�     O�     f�     ��  $   ��  )   ��  	   �  3   ��     )�     I�  +   f�  .   ��     ��  
   ɉ     ԉ  
   ى     �     ��     �     ,�     5�     A�     G�     T�     n�     z�     ��     ��  3   ��     �     �     #�     3�  4   ?�     t�     ��  3   ��     Ӌ     ܋  !   �  ,   �  ,   C�  #   p�     ��  F   ��  #   ��     �     7�      L�     m�     ��  3   ��  6   ˍ     �     �     /�     J�  &   Y�  &   ��  !   ��  A   Ɏ  &   �  T   2�     ��     ��     ��     Ï     ҏ     �  	   ��  	   �  	   �  	   �  	    �     *�  #   I�  '   m�  B   ��  &   ؐ  <   ��  &   <�  :   c�  &   ��  E   ő  +   �  A   7�  ,   y�  +   ��     Ғ  ;   �  
   #�     .�     J�     ^�     o�  
   ��  
   ��  
   ��     ��  *   ��     �  -   �  (  1�  1   Z�     ��     ��     ��     ��  
   ��  ,   ��     �     �     ��  	   �     �  +   �  -   ?�     m�     }�     ��     ��  	   ��     ��     ��  
   ��     ʙ     ؙ  	   �     ��  %   �     :�  "   X�     {�     ��     ��  !   ��     ښ     �     ��  :   �  1   @�     r�  !   ��     ��     ̛     ӛ     �  
   �  
   ��     �     �     0�  
   C�     N�     \�     c�     v�     ��     ��     ��     ��     Ɯ     ٜ  (   ��     	�     �     %�     )�     0�  '   A�     i�     p�  '   ��     ��     ��  
   Ý     Ν     �     ��  (   �  1   >�     p�  1   ��     ��     Ҟ  "   �  	   �     �  ,   !�     N�     c�     k�     |�     ��     ��     ��  
   ß  
   Ο     ٟ     ��     ��     �     �     "�     7�     K�     e�     y�     ��  I   ��     �      �     �      �  2   -�  &   `�     ��  .   ��     á  5   ԡ  5   
�     @�  $   _�     ��     ��     ��     ��  *   Ȣ  0   �     $�  	   9�     C�  	   I�  
   S�     ^�     k�     x�     ��  
   ��     ��     £     ף     ��     �  2   �  &   M�     t�     ��  	   ��     ��     ��  *   ��     �     ��     ��  4   �     9�     @�     G�  	   W�     a�     t�     ��     ��  "   ť     �  e   ��  I   d�     ��  	   ��  =   Ŧ     �  @   �  *   W�     ��     ��     ��  	   ��  	   ȧ     ҧ  	   �     �     �     �     �  	   �  
   $�     /�     @�  
   Q�     \�     q�     y�     �  	   ��     ��  
   ��     ��     Ȩ     Ϩ     ۨ  %   �     �     �     -�     ?�  !   F�     h�  >   |�  
   ��     Ʃ     ש     �  '   �  6   )�     `�  	   f�     p�     ��  )   ��     ��     ժ     ٪     �     �     �  	   ��     �     �     �  
   %�  
   0�     ;�  
   C�     N�     V�     ^�     |�     ��     ��     ��     ī     ׫     ޫ  E   ��     >�     T�     h�     o�     w�  	   ��  	   ��     ��     ��  
   ��     Ŭ  	   Ҭ     ܬ     �  	   ��  	   ��     	�     !�     5�     <�  �   R�  C   ݭ     !�     =�  >   V�  &   ��     ��     î  %   Ǯ  #   ��      �  $   2�  "   W�     z�     ��     ��  B   ��     ��  ,   �  $   ?�     d�     x�     ��  	   ��     ��     ��  	   ɰ  (   Ӱ     ��     �     )�     5�     D�     Q�     ]�     o�     ��     ��     ��     ��  	   ��     ȱ     ر  ,   ��  $   "�     G�  )   d�     ��  .   ��  "   Ĳ     �  '   �  ,   �  
   C�  	   N�  3   X�  6   ��     ó     ׳  *   �  +   �     E�     R�     l�  *   ��     ��  9   δ     �     !�     =�     M�     ]�  	   t�     ~�     ��     ��     ��  !   ǵ     �     ��  -   �  -   B�     p�     ��     ��      ��  (   ζ  #   ��     �     6�     G�     N�     \�     i�     ��     ��     ��     ŷ     ߷     ��     �     "�     B�     G�     N�  	   e�     o�     ~�     ��     ��  $   ��     ۸  
   �     �     �     �     3�     E�     J�     Q�     ^�  3   n�     ��     ��  	   ��     ʹ  .   ѹ      �     �  )   �     A�     R�     e�     y�     ��     ��  #   ��     ֺ     �     ��  #   	�     -�     >�     \�     z�     ��     ��     ��     ׻     ��     �  )   1�  )   [�     ��     ��     ��     ��      Ƽ     �     ��     �     �     �     .�     L�  $   j�     ��     ��     ��     ݽ     �     ��     �     �  3   �  0   S�  !   ��  #   ��  1   ʾ     ��     �  !   �     2�  	   N�  	   X�     b�     {�     ��  ,   ��     ο  *   �  0   �  *   ?�  )   j�     ��     ��  "   ��  %   ��  (   �     *�     .�     ;�     H�     [�  	   b�  
   l�     w�     |�     ��     ��  *   ��     ��  *   ��     �     5�     <�     T�     f�     x�     ��     ��  +  ��     ��  	   ��     ��  $   �     )�     -�     ;�  	   T�     ^�     p�     ��  #   ��  '   ��     ��  	   ��     ��     	�  	   �     %�     )�     9�     P�     h�     o�     s�  	   ��     ��     ��     ��     ��     ��     ��     �     �  	   #�  &   -�     T�     b�     p�     }�     ��  '   ��     ��  %   ��  +   �     =�     C�     J�     Q�     h�     ��     ��     ��     ��     ��     ��     ��  	   ��     �  "   %�     H�     [�     b�     ��     ��     ��     ��  $   ��     �     �     3�     :�     J�     X�     e�     ~�  !   ��  "   ��  *   ��     ��     �     +�     >�      S�  '   t�     ��     ��     ��  *   ��     ��     �    +�     7�      M�     n�     ~�     ��     ��     ��  ?   ��     ��     ��     �     �     �      �     O      1         �      �   V    Z   �  S   �              U     ,   �  �   h      e   *  �      %         �        �        �  �  �  $   ,  ~   I   H   M  �      �  2  �          x     �   q   �   8  �  &  �      �         �  �   �     .   �  �  �   6  �  �  �   c  �  �     �  �     �  k  M  M   >  �      _   E  �  �  �  �               �  �  �       �                7      i  �      �     �  4  C  T  �   w   /  �  0        `  a   �   V        �  �   /   �  �     '       �      p      r          h   o  &   `  �      �   �      v  P     �   [      R       u  g       e  =         b  �      d      �  �   2  �  l   �  �      �      L  �   �  �  �          �  �  n  E  �           �      n           �  �  )   �  6                      @  q      �   �      �          �   �     <   �  �   d   1  k  �       
              H      �  �  �  �       0   f      �   �  Y  �  3  t      3                 �  K        �       B   �   h  8   �   k   )  �     1  .  �          J  �        q        �   �       b   (   L   �           ]          �  %       �   \          {         :  z          R  �  �  �      �  �  �             �  <  7   �      �  �         �  �  �              N   t      s        �                     }      5  X   �   !   �   -  S          �  e          i      (  �   *   N  �   �      s   �   �  D  �         �   '  ^       �  �       y       b  �       �       �       
                  Q               �      �  
   j   N      w      �   �    O  �      �   �      %      �          C   �    �   	  �       t       #  Y     @   @    �       �  �  �      F            �           a      >   �   (      `   $  Z            �   �  �  �   �       �  Y                     [    �    \  �          w          m      f  �      �   �  r         �       �  �      	       A  �  m   �   G  �   �   B  ,  &      ~     z      K          �  U       |  v   �  7  �  ]      �  �   4  ;  �     X  �      �        A      �   u       �   �   �  �      �  �  .      �      �     �   �   ;  z   =  �   W  y          �     �   ?       H  �  �  3  �  c   �         �   �      �    �   0  �  �  �      I        �           �  �      �                �   ?  �  �  :       �  9      �  �  J  d  m  �   �  |                  E       5   �  �   �   A   g  u  }  �   �  "   l      /  �   i   �  �  Q      v  �   W       �  �  �  �  {      P      s        j  y  �         +    #   O       �  "  �  8  K   �   �              �  <                 a  �   �  W  #  n  X  :  ^  �  �      L  �  �  p     ^          �   f   �   �  �     �  �  �   �   S  Z  �       �       �   -   �      ?  �  o  �  p   !  P  �   4   '      !  D   �  �  $      �  �       =  �    _  G   9  �  �      l  �  �  �          �   �   �   �       �  �       ~  �   )      F          �   �  �                    D            �   �   �  V       �      �   _  c      �   �   �      �   �   �      �  �   B  �  �   �  �           �  �  �   ;   �       x   �  |   �      �  �   j  \                       �  �   R  T        �  -  2   �      x  �         �  g  }   �  {     �  �   �          "  �     r  �  T  �  �   I  	  Q       �  �             �  6   �   �       5      �  �  o   >  �        �   *  �       J   �   F  9       +  G          �   C  U  [   �  �    +               ]    	UTC: %s
 	tv.tv_sec = %ld, tv.tv_usec = %ld
 	tz.tz_minuteswest = %d
 
%6d regular files
%6d directories
%6d character device files
%6d block device files
%6d links
%6d symbolic links
------
%6d files
 
%6ld inodes used (%ld%%)
 
*** %s: directory ***

 
******** %s: Not a text file ********

 
***Back***

 
...Skipping  
Known <ldisc> names:
 
Login incorrect
 
Message Queue msqid=%d
 
Most commands optionally preceded by integer argument k.  Defaults in brackets.
Star (*) indicates argument becomes new default.
 
Options:
 
Pattern not found
 
Script done on %s 
Semaphore Array semid=%d
 
Shared memory Segment shmid=%d
         (%s partition table detected).          (compiled without libblkid).    Overflow
   b          Toggle bootable flag of the current partition   d          Delete the current partition   h          Print this screen   n          Create new partition from free space   q          Quit program without writing partition table  Remove  badsect  ecc  removable "%s" line %d %6.2f%% (%+ld bytes)	%s
 %6ld zones used (%ld%%)
 %ld blocks
 %ld inodes
 %s %d %s cache: %s does not have interrupt functions.  %s failed.
 %s from %s
 %s is clean, no check.
 %s is mounted.	  %s is mounted; will not make a filesystem here! %s is not a block special device %s is not a serial line %s status is %d %s succeeded.
 %s takes no non-option arguments.  You supplied %d.
 %s using IRQ %d
 %s using polling
 %s-sided, %d tracks, %d sec/track. Total capacity %d kB.
 %s: OK
 %s: Unrecognized architecture %s: assuming RTC uses UTC ...
 %s: bad directory: '.' isn't first
 %s: bad directory: '..' isn't second
 %s: bad directory: size < 32 %s: can't exec %s: %m %s: cannot add inotify watch (limit of inotify watches was reached). %s: cannot add inotify watch. %s: cannot read inotify events %s: dup problem: %m %s: exceeded limit of symlinks %s: get size failed %s: input overrun %s: insecure permissions %04o, %04o suggested. %s: last_page 0x%08llx is larger than actual size of swapspace %s: lseek failed %s: not open for read/write %s: read swap header failed %s: read: %m %s: reinitializing the swap. %s: skipping - it appears to have holes. %s: skipping nonexistent device
 %s: software suspend data detected. Rewriting the swap signature. %s: swap format pagesize does not match. %s: swap format pagesize does not match. (Use --fixpgsz to reinitialize it.) %s: swapoff failed %s: swapon failed %s: write signature failed (EOF) (Next file:  (Next file: %s) , busy , error , on-line , out of paper , ready -- line already flushed ------ Message Queues --------
 ------ Message Queues PIDs --------
 ------ Message Queues Send/Recv/Change Times --------
 ------ Semaphore Arrays --------
 ------ Semaphore Arrays Creators/Owners --------
 ------ Semaphore Limits --------
 ------ Semaphore Operation/Change Times --------
 ------ Semaphore Status --------
 ------ Shared Memory Attach/Detach/Change Times --------
 ------ Shared Memory Limits --------
 ------ Shared Memory Segment Creators/Owners --------
 ------ Shared Memory Segments --------
 ------ Shared Memory Status --------
 -------      ------- ----------------------------
FILE SYSTEM HAS BEEN CHANGED
----------------------------
 --More-- --waiting-- (pass %d)
 ...Skipping back to file  ...Skipping to file  ...got clock tick
 ...skipping
 ...skipping backward
 ...skipping forward
 ...synchronization failed
 /dev/%s: cannot open as standard input: %m /dev/%s: not a character device : !command not allowed in rflag mode.
 <space>                 Display next k lines of text [current screen size]
z                       Display next k lines of text [current screen size]*
<return>                Display next k lines of text [1]*
d or ctrl-D             Scroll k lines [current scroll size, initially 11]*
q or Q or <interrupt>   Exit from more
s                       Skip forward k lines of text [1]
f                       Skip forward k screenfuls of text [1]
b or ctrl-B             Skip backwards k screenfuls of text [1]
'                       Go to place where previous search started
=                       Display current line number
/<regular expression>   Search for kth occurrence of regular expression [1]
n                       Search for kth occurrence of last r.e [1]
!<cmd> or :!<cmd>       Execute <cmd> in a subshell
v                       Start up /usr/bin/vi at current line
ctrl-L                  Redraw screen
:n                      Go to kth next file [1]
:p                      Go to kth previous file [1]
:f                      Display current file name and line number
.                       Repeat previous command
 AIEEE: block "compressed" to > 2*blocklength (%ld)
 AIX AIX bootable AST SmartSleep Amoeba Amoeba BBT Assuming hardware clock is kept in %s time.
 BBT BSD/OS BSDI fs BSDI swap BeOS fs Block %d in file `%s' is marked not in use. Block has been used before. Now in file `%s'. BlockSize: %d
 Boot Boot Wizard hidden BootIt Bootable CP/M CP/M / CTOS / ... CPU MHz: CPU family: CPU op-mode(s): CRC: %x
 Calling settimeofday:
 Changing finger information for %s.
 Changing shell for %s.
 Checking all file systems.
 Clear Clock not changed - testing only.
 Command      Meaning Command (m for help):  Compaq diagnostics Core(s) per socket: Correct Could not open file with the clock adjustment parameters in it (%s) for writing Could not update file with the clock adjustment parameters (%s) in it Couldn't initialize PAM: %s Create new partition from free space Current system time: %ld = %s
 Cylinders DIALUP AT %s BY %s DOS R/O DOS access DOS secondary DRDOS/sec (FAT-12) DRDOS/sec (FAT-16 < 32M) DRDOS/sec (FAT-16) Darwin UFS Darwin boot Delete Delete the current partition Dell Utility Device Device: %s
 Directory data: %zd bytes
 DiskSecure Multi-Boot Do you really want to continue Double Down Arrow   Move cursor to the next partition EFI (FAT-12/16/32) EZ-Drive Empty End Everything: %zd kilobytes
 Expert command (m for help):  Extended Extra sectors per cylinder FAILED LOGIN SESSION FROM %s FOR %s, %s FAT12 FAT16 FAT16 <32M FATAL: %s is not a terminal FATAL: bad tty FSname: <%-6s>
 Failed to set personality to %s Filesystem on %s is dirty, needs checking.
 Filesystem state=%d
 Finger information *NOT* changed.  Try again later.
 Finger information changed.
 Finger information not changed.
 Finished with %s (exit status %d)
 First %s Flags Forcing filesystem check on %s.
 Formatting ...  FreeBSD GNU HURD or SysV GPT Generated random UUID: %s
 Generated time UUID: %s
 Geometry Golden Bow HFS / HFS+ Hardware clock is on %s time
 Heads Help Hidden FAT12 Hidden FAT16 Hidden FAT16 <32M Hidden HPFS/NTFS Hidden W95 FAT16 (LBA) Hidden W95 FAT32 Hidden W95 FAT32 (LBA) Home Phone Hw clock time : %4d/%.2d/%.2d %.2d:%.2d:%.2d = %ld seconds since 1969
 Hypervisor vendor: IBM Thinkpad hibernation Id  Name

 Including: %s
 Inode %d marked unused, but used for file '%s'
 Inode end: %d, Data end: %d
 Interleave factor Internal error: trying to write bad block
Write request ignored
 Invalid operation %d
 Invalid user name "%s" in %s:%d. Abort. Invalid values in hardware clock: %4d/%.2d/%.2d %.2d:%.2d:%.2d
 Issuing date command: %s
 Kernel is assuming an epoch value of %lu
 LANstep LOGIN ON %s BY %s LOGIN ON %s BY %s FROM %s LPGETIRQ error Last calibration done at %ld seconds after 1969
 Last drift adjustment done at %ld seconds after 1969
 Last login: %.*s  Line too long Linux Linux LVM Linux RAID Linux extended Linux native Linux plaintext Linux raid autodetect Linux swap Linux swap / Solaris Linux/PA-RISC boot Logging in with home = "/".
 Login incorrect

 Mark in use Message from %s@%s (as %s) on %s at %s ... Message from %s@%s on %s at %s ... Message queue id: %d
 Minix / old Linux Model: NEC DOS NTFS volume set NULL user name in %s:%d. Abort. NUMA node(s): Name NeXTSTEP Needed adjustment is less than one second, so not setting clock.
 NetBSD New New beginning of data New shell No next file No previous command to substitute for No previous file No remembered search string No usable clock interface found.
 Non-FS data Not adjusting drift factor because last calibration time is zero,
so history is bad and calibration startover is necessary.
 Not adjusting drift factor because the Hardware Clock previously contained garbage.
 Not enough arguments Not set Not setting system clock because running in test mode.
 Not superuser. Not updating adjtime file because of testing mode.
 Note: All of the commands can be entered with either upper or lower Novell Netware 286 Novell Netware 386 Number of alternate cylinders Number of cylinders Number of heads Number of physical cylinders Number of sectors OPUS OS/2 Boot Manager Office Office Phone Old Minix OnTrack DM OnTrack DM6 Aux1 OnTrack DM6 Aux3 OnTrackDM6 Only 1k blocks/zones supported OpenBSD PC/IX PPC PReP Boot Partition number PartitionMagic recovery Password:  Pattern not found Plan 9 Priam Edisk Print help screen Probably you need root privileges.
 QNX4.x QNX4.x 2nd part QNX4.x 3rd part Quit Quit program without writing partition table RE error:  RO    RA   SSZ   BSZ   StartSec            Size   Device
 ROM image ROM image map ROOT LOGIN ON %s ROOT LOGIN ON %s FROM %s Read error: bad block in file '%s'
 Read error: unable to seek to block in file '%s'
 Read:  Remove block Rotation speed (rpm) Ruffian BCD clock
 SCHED_%s min/max priority	: %d/%d
 SCHED_%s not supported?
 SFS SGI bsd SGI efs SGI lvol SGI raw SGI rlvol SGI secrepl SGI sysv SGI trkrepl SGI volhdr SGI volume SGI xfs SGI xfslog SGI xlv SGI xvm Script done, file is %s
 Script started on %s Script started, file is %s
 Sectors Sectors/track Semaphore id: %d
 Set Set i_nlinks to count Setting Hardware Clock to %.2d:%.2d:%.2d = %ld seconds since 1969
 Shared memory id: %d
 Shell changed.
 Single Solaris Solaris boot SpeedStor Stepping: SunOS alt sectors SunOS cachefs SunOS home SunOS reserved SunOS root SunOS stand SunOS swap SunOS usr SunOS var Super block: %zd bytes
 Switching on %s.
 Syrinx TIOCSCTTY failed: %m The date command issued by %s returned something other than an integer where the converted time value was expected.
The command was:
  %s
The response was:
 %s
 The directory '%s' contains a bad inode number for file '%.*s'. The file `%s' has mode %05o
 Thread(s) per core: Time read from Hardware Clock: %4d/%.2d/%.2d %02d:%02d:%02d
 Toggle bootable flag of the current partition Type UTC Unable to allocate buffer for inode count Unable to allocate buffer for inode map Unable to allocate buffer for inodes Unable to allocate buffer for zone count Unable to allocate buffer for zone map Unable to read inode map Unable to read inodes Unable to read zone map Unable to run 'date' program in /bin/sh shell. popen() failed Unable to set system clock.
 Unable to set the epoch value in the kernel.
 Unable to set up swap-space: unreadable Unable to write inode map Unable to write inodes Unable to write zone map Unassigned Unknown Unknown user context Unmark Up Arrow     Move cursor to the previous partition Using UTC time.
 Using local time.
 VMware VMFS VMware VMKCORE Vendor ID: Venix 80286 Verifying ...  Virtualization type: Virtualization: Volume: <%-6s>
 W95 Ext'd (LBA) W95 FAT16 (LBA) W95 FAT32 W95 FAT32 (LBA) Waiting for clock tick...
 Waiting in loop for time from %s to change
 Warning: Firstzone != Norm_firstzone
 Warning: inode count too big.
 Weird values in do_check: probably bugs
 Whole disk Would have written the following to %s:
%s Would you like to edit %s now [y/n]?  Write Write error: bad block in file '%s'
 Write partition table to disk (this might destroy data) XENIX root XENIX usr You are using shadow groups on this system.
 You are using shadow passwords on this system.
 You have mail.
 You have new mail.
 Zone nr < FIRSTZONE in file `%s'. Zone nr >= ZONES in file `%s'. Zonesize=%d
 [Not a file] line %d [Press 'h' for instructions.] [Press space to continue, 'q' to quit.] [Use q or Q to quit] alarm %ld, sys_time %ld, rtc_time %ld, seconds %u
 allocated queues = %d
 allocated semaphores = %d
 already removed id already removed key att_time=%-26.24s
 attached bad arguments bad filename length bad inode offset bad inode size bad magic number in super-block bad response length bad root offset (%lu) bad s_imap_blocks field in super-block bad s_zmap_blocks field in super-block bad speed: %s bad timeout value: %s bad v2 inode size badblock number input error on line %d
 block size smaller than physical sector size of %s blocks argument too large, max is %llu bogus mode: %s (%o) booted from MILO
 bytes bytes/sector can't fork
 cannot daemonize cannot determine size of %s cannot find the device for %s cannot fork cannot get size of %s cannot get terminal attributes for %s cannot open %s cannot set line discipline cannot set terminal attributes for %s cgid change change_time=%-26.24s
 changed check aborted.
 chown failed: %s clockport adjusted to 0x%x
 connect couldn't compute selinux context cpid crc error create message queue failed create semaphore failed create share memory failed ctime = %-26.24s
 cuid cylinders cylinderskew data block too large date string %s equates to %ld seconds since 1969.
 dest det_time=%-26.24s
 detached different directory inode has zero offset and non-zero size: %s divisor '%s' done
 empty long option after -l or --long argument error closing %s error writing . entry error writing .. entry error writing inode error writing root inode error writing superblock error: %s: probing initialization failed error: uname failed excessively long line arg exec failed
 expected a number, but got '%s' failed to execute %s failed to get pid %d's attributes failed to get pid %d's policy failed to parse pid failed to parse priority failed to read symlink: %s failed to read timing file %s failed to read typescript file %s failed to set pid %d's policy fifo has non-zero size: %s file inode has zero offset and non-zero size file inode has zero size and non-zero offset file length too short filename length is zero flush buffers fork failed fork() failed, try again later
 from %.*s
 fsname name too long full funky TOY!
 get blocksize get filesystem readahead get logical block (sector) size get max sectors per request get minimum I/O size get optimal I/O size get physical block (sector) size get read-only get readahead get size in bytes gid headswitch ignoring given class data for idle class ignoring given class data for none class illegal day value: use 1-%d illegal month value: use 1-12 incomplete write to "%s" (written %zd, expected %zd)
 interleave internal error internal error, contact the author. invalid file data offset invalid id invalid key invalid length value specified invalid offset value specified invalid option ioctl failed: unable to determine device size: %s ioctl(%s) was successful.
 ioctl() to %s to turn off update interrupts failed ioctl() to %s to turn on update interrupts failed unexpectedly ioctl(RTC_EPOCH_READ) to %s failed ioctl(RTC_EPOCH_SET) to %s failed ioprio_get failed ioprio_set failed kernel not configured for message queues
 kernel not configured for semaphores
 kernel not configured for shared memory
 key last-changed last-op lchown failed: %s local locked login:  lpid lrpid lspid max number of arrays = %d
 max ops per semop call = %d
 max queues system wide = %d
 max semaphores per array = %d
 max semaphores system wide = %d
 messages missing optstring argument mkdir failed: %s mknod failed: %s mode=%#o	access_perms=%#o
 mode=%#o, access_perms=%#o
 mount table full mount: %s does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
 msqid nattch ncount need terminal for interactive repairs no no label,  no length argument specified no uuid
 non-block (%ld) bytes non-size (%ld vs %ld) bytes none not a block device or file: %s not enough space, need at least %llu blocks nsems on %.*s
 operation %d, incoming num = %d
 otime = %-26.24s
 owner para past first line permission denied for id permission denied for key perms pid rcv_time=%-26.24s
 read count recv reread partition table resource(s) deleted
 response from date command = %s
 root inode is not directory root inode isn't a directory rpm same saved sector count: %d, sector size: %d
 sectors/cylinder sectors/track seek error seek failed seek failed in bad_zone seek failed in check_blocks seek failed in write_block seek failed in write_super_block select() to %s to wait for clock tick failed semid semnum send send_time=%-26.24s
 set filesystem readahead set read-only set read-write set readahead settimeofday() failed setuid() failed shmid size error in symlink: %s socket socket has non-zero size: %s special file has non-zero offset: %s speed %d unsupported status superblock magic not found superblock size (%d) too small symbolic link has zero offset symbolic link has zero size symlink failed: %s timings file %s: %lu: unexpected format too many alternate speeds too many inodes - max is 512 total track-to-track seek tracks/cylinder trackskew trouble reading terminfo uid unable to alloc buffer for superblock unable to alloc new libblkid probe unable to create new selinux context unable to erase bootbits sectors unable to matchpathcon() unable to read super block unable to resolve '%s' unable to rewind swap-device unable to test CRC: old cramfs format unable to write super-block unexpected end of file on %s unknown unknown shell after -s or --shell argument unshare failed unsupported filesystem features usage: %s [-h] [-v] [-b blksize] [-e edition] [-N endian] [-i file] [-n name] dirname outfile
 -h         print this help
 -v         be verbose
 -E         make all warnings errors (non-zero exit status)
 -b blksize use this blocksize, must equal page size
 -e edition set edition number (part of fsid)
 -N endian  set cramfs endianness (big|little|host), default host
 -i file    insert a file image into the filesystem (requires >= 2.4.0)
 -n name    set name of cramfs filesystem
 -p         pad by %d bytes for boot code
 -s         sort directory entries (old option, ignored)
 -z         make explicit holes (requires >= 2.3.39)
 dirname    root of the filesystem to be compressed
 outfile    output file
 used arrays = %d
 used headers = %d
 used-bytes utime failed: %s value volume name too long waitpid failed we have read epoch %ld from %s with RTC_EPOCH_READ ioctl.
 write write failed: %s write to stdout failed yes zcount zero file count Project-Id-Version: util-linux-ng-2.14.1-rc2
Report-Msgid-Bugs-To: Karel Zak <kzak@redhat.com>
POT-Creation-Date: 2015-11-02 11:43+0100
PO-Revision-Date: 2010-11-18 23:02+0000
Last-Translator: Ray Wang <wanglei1123@gmail.com>
Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Launchpad-Export-Date: 2016-06-27 16:06+0000
X-Generator: Launchpad (build 18115)
 	UTC：%s
 	tv.tv_sec = %ld, tv.tv_usec = %ld
 	tz.tz_minuteswest = %d
 
%6d 个常规文件
%6d 个目录
%6d 个字符设备文件
%6d 个块设备文件
%6d 个链接
%6d 个符号链接
------
%6d 个文件
 
已使用 %6ld inodes(%ld%%)
 
*** %s：目录 ***

 
******** %s：不是文件文件 ********

 
***后退***

 
...跳过  
已知<线路规则>名称：
 
登录不正确
 
消息队列 msqid=%d
 
多数命令可在前面加整数参数 k。方括号中为参数的默认值
星号(*)表示参数将成为新的默认值。
 
选项：
 
未找到模式
 
脚本完成，于 %s 
信号量数组 semid=%d
 
共享内存段 shmid=%d
         (检测到 %s 分区表)。          (编译时未链接 libblkid)。    上溢
   b          切换当前分区的 可启动 标志   d          删除当前分区   h          打印此屏幕   n          从空闲空间创建新分区   q          退出程序但不写入分区表  删除  坏扇区  ecc  可移动 “%s”第 %d 行 %6.2f%% (%+ld 字节)	%s
 已使用 %6ld 区(%ld%%)
 %ld 块
 %ld inodes
 %s %d %s 缓存： %s 没有中断函数。  %s 失败.
 %s，来自 %s
 %s 是干净的，不检查。
 %s 已经挂载.	  %s 已挂载；将不会在此创建文件系统！ %s 不是一个块特殊设备 %s 不是串行线 %s 状态为 %d %s 成功.
 %s 不接受无选项的参数。您提供了 %d。
 %s 使用中断请求 %d
 %s 使用轮询
 %s-面, %d 磁道, %d 秒/磁道. 总容量 %d kB.
 %s：OK
 %s：未识别的架构 %s：将假设 RTC 使用 UTC...
 %s: 不正确的目录: '.' 不是第一个
 %s: 不正确的目录: '.' 不是第二个
 %s: 不正确的目录: 大小 < 32 %s：无法执行 %s：%m %s：无法添加 inotify 观察(达到了 inotify 观察数限制)。 %s：无法添加 inotify 观察。 %s：无法读 inotify 事件 %s：dup 问题：%m %s：超过了符号链接限制 %s：获取大小失败 %s：输入过载 %s：不安全的权限 %04o，建议使用 %04o。 %s：末页 0x%08llx 大于交换空间的实际大小 %s：lseek 失败 %s：未打开(就)读/写 %s：读交换区头失败 %s：read：%m %s：正在重新初始化交换区。 %s：将跳过 - 它似乎有空洞。 %s：将跳过不存在的设备
 %s：检测到软件挂起数据。将重新写交换区签名。 %s：交换格式页大小不匹配。 %s：交换格式页大小不匹配。(使用 --fixpgsz 来重新对它初始化。) %s：swapoff 失败 %s：swapon 失败 %s：写签名失败 (文件末尾) (下个文件：  (下个文件：%s) ，忙碌 ，错误 ，在线 ，缺页 ，就绪 -- 行已经被冲洗(flushed) --------- 消息队列 -----------
 --------- 消息队列 PID -----------
 ----------- 消息队列 发送/接收/更改 时间 ------------
 --------- 信号量数组 -----------
 ----------- 信号量数组创建者/拥有者 ------------
 --------- 信号量限制 -----------
 ------------ 信号量 操作/更改 时间 -------------
 --------- 信号量状态 -----------
 ------------ 共享内存 添加/脱离/更改 时间 --------------
 ---------- 同享内存限制 ------------
 ------------- 共享内存段创建者/拥有者 ---------------
 ------------ 共享内存段 --------------
 ---------- 共享内存状态 ------------
 -------      ------- ----------------
文件系统已经更改
----------------
 --更多-- --等待中-- (第 %d 遍)
 ...将跳回文件  ...跳到文件  ...已获取时钟滴答
 ...跳过
 ...后退
 ...前进
 ...同步失败
 /dev/%s：无法以标准输入打开：%m /dev/%s：不是字符设备 : !命令 在 rflag 模式下不许使用。
 <空格键>                显示下 k 行文本[当前屏幕尺寸]
z                       显示下 k 行文本[当前屏幕尺寸]*
<回车键>                显示下 k 行文本[1]*
d 或 ctrl-D             滚动 k 行[当前滚动尺寸，初始值为 11]*
q 或 Q 或 <interrupt>   退出 more
s                       跳过下面 k 行文本[1]
f                       跳过下面 k 屏文本[1]
b 或 ctrl-B             跳过上面 k 屏文本 [1]
'                       转到上次搜索开始处
=                       显示当前行号
/<正则表达式>           搜索正则表达式第 k 次出现处[1]
n                       搜索前一正则表达式第 k 次出现处[1]
!<cmd> 或 :!<cmd>       在子 shell 中执行 <cmd> 命令
v                       在当前行启动 /usr/bin/vi
ctrl-L                  重绘屏幕
:n                      转到后面第 k 个文件[1]
:p                      转到前面第 k 个文件 [1]
:f                      显示当前文件名和行号
.                       重复前一命令
 AIEEE：块已“压缩”到 > 2*块长度(%ld)
 AIX AIX 可启动 AST 智能睡眠 Amoeba Amoeba BBT 将假设硬件时钟保持为 %s 时间。
 BBT BSD/OS BSDI fs BSDI swap BeOS fs 文件中块 %d `%s' 标记为没有使用. 块已经被使用, 现在在文件 `%s' 中. 块大小：%d
 启动 Boot Wizard 隐藏 BootIt 可启动 CP/M CP/M / CTOS / ... CPU MHz： CPU 系列： CPU 运行模式： CRC：%x
 正在调用 settimeofday：
 正在更改 %s 的 finger 信息。
 正在更改 %s 的 shell。
 正在检查所有文件系统。
 清除 时钟未更改 - 纯测试。
 命令         意义 命令(输入 m 获取帮助)：  Compaq 诊断 每个座的核数： 正确 无法打开包含时钟调整参数(%s)的文件来写入 无法更新包含时钟调整参数(%s)的文件 无法初始化 PAM：%s 在剩余空间上创建新分区 当前系统时间：%ld = %s
 柱面 DIALUP AT %s BY %s DOS R/O DOS 访问 DOS 次要 DRDOS/sec (FAT-12) DRDOS/sec (FAT-16 < 32M) DRDOS/sec (FAT-16) Darwin UFS Darwin 启动 删除 删除当前分区 Dell 工具 设备 设备：%s
 目录数据：%zd 字节
 DiskSecure 多启动 你确认要继续 双面 下箭头       光标移到下一分区 EFI (FAT-12/16/32) EZ-Drive 空 末尾 全部：%zd KB
 专家命令(输入 m 显示帮助)：  扩展 每个柱面的额外扇区数 FAILED LOGIN SESSION FROM %s FOR %s, %s FAT12 FAT16 FAT16 <32M 致命：%s 不是终端 致命：坏的 tty 文件系统名：<%-6s>
 无法将人格(personality)设置为 %s %s 上的文件系统有问题，需要检查。
 Filesystem state=%d
 Finger 信息*没有*更改。请稍后重试。
 Finger 信息已更改。
 Finger 信息未更改。
 以 %s 结束(退出状态为 %d)
 起始 %s 标志 强制在 %s 上进行文件系统检查。
 正在格式化 ...  FreeBSD GNU HURD or SysV GPT 已生成随机 UUID：%s
 已生成时间 UUID：%s
 几何属性 Golden Bow HFS / HFS+ 硬件时钟为 %s 时间
 磁头 帮助 隐藏的 FAT12 隐藏的 FAT16 隐藏的 FAT16 <32M 隐藏的 HPFS/NTFS 隐藏的 W95 FAT16 (LBA) 隐藏的 W95 FAT32 隐藏的 W95 FAT32 (LBA) 住宅电话 硬件时钟时间：%4d/%.2d/%.2d %.2d:%.2d:%.2d = 1969(年)后 %ld 秒
 超管理器厂商： IBM Thinkpad 休眠 Id 名称

 包含：%s
 Inode %d 标记为未使用, 但用于文件 '%s'
 Inode 终点：%d，数据终点：%d
 交错系数 内部错误: 试图写坏块
写请求忽略
 无效操作 %d
 %2$s:%3$d 中是无效用户名“%1$s”。中止。 硬件时钟值无效：%4d/%.2d/%.2d %.2d:%.2d:%.2d
 正在发出 date 命令：%s
 内核假设纪元(epoch)值为 %lu
 LANstep LOGIN ON %s BY %s LOGIN ON %s BY %s FROM %s LPGETIRQ 出错 上次校准完成于1969(年)后 %ld 秒
 上次漂移调整完成于1969(年)后 %ld 秒
 上次登录：%.*s  行过长 Linux Linux LVM Linux RAID Linux 扩展 Linux native Linux 纯文本 Linux raid 自动检测 Linux swap Linux 交换 / Solaris Linux/PA-RISC 启动 以 home = “/” 登录。
 登录不正确

 标记为已使用 %s@%s (作为 %s) 从 %s 上在 %s 发来消息... %s@%s 从 %s 上在 %s 发来消息... 消息队列 id：%d
 Minix / 旧 Linux 型号： NEC DOS NTFS 卷集 %s:%d 中是空(NULL)用户名。中止。 NUMA 节点： 名称 NeXTSTEP 需要的调整不到一秒，将不设置时钟。
 NetBSD 新建 新数据起点 新 shell 无下一个文件 没有要替换的前一命令 无上一个文件 没有记忆的搜索字符串 未找到可用的时钟接口。
 非文件系统数据 将不调整漂移系数，因为上次校准时间为零，
历史有问题；需要启动校准。
 将不调整漂移系数，因为之前硬件时钟包含垃圾内容。
 参数不够 未设置 将不设置系统时钟，因为正以测试方式运行。
 非超级用户。 将不更新 adjtime 文件，因为正以测试方式运行。
 注：所有命令均可以大写或小写 Novell Netware 286 Novell Netware 386 备用柱面的数量 柱面数 磁头数 物理柱面数 扇区数 OPUS OS/2 启动管理器 办公 办公电话 旧 Minix OnTrack DM OnTrack DM6 Aux1 OnTrack DM6 Aux3 OnTrackDM6 只支持 1k 块/区 OpenBSD PC/IX PPC PReP Boot 分区号 PartitionMagic 恢复 密码：  找不到模式 Plan 9 Priam Edisk 打印帮助界面 您可能需要 root 用户权限。
 QNX4.x QNX4.x 第2部分 QNX4.x 第3部分 退出 退出程序而不写入分区表 表达式错误：  RO    RA   SSZ   BSZ   开始段              大小   设备
 ROM 镜像 ROM 镜像映射 ROOT LOGIN ON %s ROOT LOGIN ON %s FROM %s 读取错误: 文件 '%s' 中有坏块
 读取错误: 不能在文件 '%s' 中定位到该块
 读:  删除块 转速(转/分) Ruffian BCD 时钟
 SCHED_%s 最小/最大优先级	：%d/%d
 SCHED_%s 不支持？
 SFS SGI bsd SGI efs SGI lvol SGI raw SGI rlvol SGI secrepl SGI sysv SGI trkrepl SGI volhdr SGI volume SGI xfs SGI xfslog SGI xlv SGI xvm 脚本完成，文件为  %s
 脚本启动于 %s 脚本已启动，文件为 %s
 扇区 扇区/磁道 信号量 id：%d
 设置 将 i_nlinks 设为计数 正在将硬件时钟设置为 %.2d:%.2d:%.2d = 1969(年)后 %ld 秒
 共享内存 id：%d
 shell 已更改。
 单面 Solaris Solaris 启动 SpeedStor 步进： SunOS alt sectors SunOS cachefs SunOS home SunOS 保留 SunOS 根 SunOS stand SunOS 交换 SunOS usr SunOS var 超级块：%zd 字节
 正在打开 %s。
 Syrinx TIOCSCTTY 失败：%m %s 发出的 date 命令返回了意外结果，本应返回包含已转换时间值的一个整数。
命令为：
  %s
响应为：
 %s
 目录 '%s' 包含了一个不正确的 inode number 文件 '%.*s'. 文件 `%s' 的模式 %05o
 每个核的线程数： 从硬件时钟读取的时间：%4d/%.2d/%.2d %02d:%02d:%02d
 开关当前分区的 可启动 标志 类型 UTC 不能为 inode count 分配缓冲区 不能为 inode map 分配缓冲区 不能为 inodes 分配缓冲区 不能为 zone count 分配缓冲区 不能为 zone map 分配缓冲区 不能读 inode map 不能读 inodes 不能读 zone map 无法在 /bin/sh shell 中运行“date”程序。popen() 失败 无法设置系统时钟。
 无法设置内核中的纪元(epoch)值。
 无法设置交换空间：不可读 不能写 inode map 不能写 inodes 不能写 zone map 未分配 未知 未知用户环境(context) 清标记 上箭头       光标移到上一分区 使用 UTC 时间。
 使用当地时间。
 VMware VMFS VMware VMKCORE 厂商 ID： Venix 80286 正在验证 ...  虚拟化类型： 虚拟化： 卷：<%-6s>
 W95 扩展 (LBA) W95 FAT16 (LBA) W95 FAT32 W95 FAT32 (LBA) 正在等等时钟滴答...
 正在循环等待来自 %s 的时间更改
 警告: Firstzone != Norm_firstzone
 警告: inode 计数太大.
 do_check 中的值异常：可能有 bug
 整盘 若执行，将向 %s 写入以下内容：
%s 您想现在编辑 %s 吗[y/n]？  写入 写入错误: 文件 '%s' 中有坏块
 将分区表写入磁盘(可能损坏数据) XENIX root XENIX usr 您正在使用此系统上的遮蔽(shadow)组。
 您正在使用此系统上的遮蔽(shadow)密码。
 有您的邮件。
 有您的新邮件。
 nr 区 < 文件“%s”中的 FIRSTZONE。 nr 区 >= 文件“%s”中的 FIRSTZONE。 Zonesize=%d
 [不是文件] 第 %d 行 [按“h”键获取说明。] [按空格键继续，“q”键退出。] [使用 q 或 Q 退出] 闹钟 %ld，sys 时间 %ld，rtc 时间 %ld，秒数 %u
 已分配队列数 = %d
 已分配信号量数 = %d
 已移除的 id 已移除的键 附加时间=%-26.24s
 已连上 参数错误 文件名长度有误 inode 偏移有误 inode 大小有误 超级块中的魔数数字损坏 响应长度错误 错误的根偏移(%lu) 不正确的 s_imap_blocks 域在超级块中 不正确的 s_zmap_blocks 域在超级块中 速度有误：%s 超时值有误：%s v2 inode 大小有误 第 %d 行坏块数输入错误
 块尺寸比 %s 的物理扇区尺寸小 块参数过大，最大值是 %llu 伪(bogus)模式：%s (%o) 从 MILO 启动
 字节 字节/扇区 无法 fork
 无法转为守护进程 无法确定 %s 的大小 找不到对应 %s 的设备 无法 fork 无法获得 %s 的大小 无法获取 %s 的终端属性 打不开 %s 无法设置行规则 无法设置 %s 的终端属性 cgid 更改 更改时间=%-26.24s
 已更改 检查失败.
 chown 失败：%s 时钟端口已调整为 0x%x
 连接 无法计算 selinux 环境(context) cpid crc 错误 创建消息队列失败 创建信号量失败 创建共享内存失败 ctime = %-26.24s
 cuid 柱面 柱头螺距 数据块过大 数据字符串 %s 等价于 1969年后 %ld 秒。
 目标 脱离时间=%-26.24s
 已断开 不同 目录 inode 偏移为 0，大小不为 0：%s 除数“%s” 完成
 -l 或 --long 参数后的长选项为空 关闭 %s 出错 写 . 记录出错 写 .. 记录出错 写 inode 出错 写 根 inode 出错 写超级块出错 错误：%s：探测初始化失败 错误：uname 失败 参数行超长 exec 失败
 应为数字，但收到了“%s” 执行 %s 失败 获取 pid %d 的属性失败 无法获得 pid %d 的策略 解析 pid 失败 解析优先级失败 读符号链接失败：%s 读时序文件 %s 失败 读 typescript 文件 %s 失败 设置 pid %d 的策略失败 FIFO 的大小不为 0：%s 文件 inode 偏移为 0，大小不为 0 文件 inode 大小为 0，偏移不为 0 文件长度过短 文件名长度为 0 刷新缓存 fork 失败 fork() 失败，请稍后重试
 来自 %.*s
 fsname 名过长 完全 烦人的 TOY！
 获得块大小 获取 文件系统 readahead 获得逻辑块(扇区)大小 获得每次请求的最大扇区数 获得最小 I/O 大小 获得最优 I/O 大小 获得物理块(扇区)大小 获得只读 获取 readahead 获得字节大小 gid 磁头切换 将忽略为“空闲”类别指定的类别数据 将忽略为“无”类别指定的类别数据 日 值不合法：请使用 1-%d 月份值不合法：请使用 1-12 不完整写入“%s”(写入 %zd，应为 %zd)
 交错 内部错误 内部错误，请联系作者。 无效的文件数据偏移 无效 id 无效键 指定的长度值无效 指定的偏移值无效 无效选项 ioctl 失败：无法确定设备大小：%s ioctl(%s) 成功。
 对 %s ioctl() 以关闭更新中断失败 对 %s ioctl() 以打开更新中断意外失败 对 %s 进行 ioctl(RTC_EPOCH_READ) 失败 对 %s 进行 ioctl(RTC_EPOCH_SET) 失败 ioprio_get 失败 ioprio_set 失败 内核未做消息的相关配置
 内核未做信号量的相关配置
 内核未做共享内存的相关配置
 键 上次更改 上一操作 lchown 失败：%s 当地 已锁定 登录：  lpid lrpid lspid 最大数组数量 = %d
 每次信号量调用最大操作数 = %d
 系统最大队列数量 = %d
 每个数组的最大信号量数目 = %d
 系统最大信号量数 = %d
 消息 缺少 optstring 参数 mkdir 失败：%s mknod 失败：%s 模式=%#o	访问权限=%#o
 模式=%#o，访问权限=%#o
 挂载表满 mount：%s 不包含 SELinux 标志
       您刚将一个支持标签但未包含标签的文件系统挂载到了 SELinux 机器上。
       受限制的应用程序可能会生成 AVC 消息，并被阻止访问该文件系统。
       更多详细信息请参阅 restorecon(8) 和 mount(8)。
 msqid 连接数 ncount 需要终端来进行交互式修复 否 无标签，  没有指定长度参数 无 uuid
 非块(%ld)字节 无大小(%ld ： %ld)字节 无 不是一个块设备或文件：%s 空间不足，至少需要 %llu 个块 nsems 于 %.*s
 操作 %d，接收数 = %d
 otime = %-26.24s
 拥有者 半 越过第一行 对 id 的权限不足 对 键 的权限不足 权限 pid 接收时间=%-26.24s
 读计数 接收 重新读取分区表 资源已删除
 date 命令的响应为 %s
 根 inode 不是一个目录 根 inode 不是一个目录 rpm 相同 已保存 扇区计数：%d，扇区大小：%d
 扇区/柱面 扇区/磁道 定位出错 定位失败 bad_zone 中定位失败 check_blocks (检查块)时定位失败 在 write_block 中定位失败 在 write_super_block 中定位失败 对 %s select() 以等待时钟嘀嗒失败 semid semnum 发送 发送时间=%-26.24s
 设置 文件系统 readahead 设置只读 设置读写 设置 readahead settimeofday() 失败 setuid() 失败 shmid 符号链接大小出错：%s 套接字 套接字的大小不为 0：%s 特殊文件的偏移不为 0：%s 不支持速度 %d 状态 超级块魔数(magic)未找到 超级块尺寸(%d)过小 符号链接偏移为 0 符号链接大小为 0 symlink 失败：%s 时序文件 %s：%lu：格式异常 备选速度过多 inodes 过多 - 最多 512 总计 跨磁道定位 磁道/柱面 磁道螺距 读取终端信息出错 uid 无法为超级块分配缓冲区 无法分配新的 libblkid 探针 无法创建新的 selinux 环境(context) 不能擦除 bootbits 扇区 无法执行 matchpathcon() 不能读超级块 不能解析“%s” 无法倒回(rewind)交换设备 无法检测 CRC：旧的 cramfs 格式 不能写超级块 %s 上的文件结尾异常 未知 -s 或 --shell 参数后是未知的 shell unshare 失败 不支持的文件系统特性 用法：%s [-h] [-v] [-b 块大小] [-e 版本] [-N 大小端] [-i 文件] [-n 名称] 目录名 输出文件
 -h         打印此帮助
 -v         详尽模式
 -E         将所有警告视为错误(退出状态码非 0)
 -b 块大小  使用此 块大小，必须与页大小相同
 -e 版本    设置修订版本号(fsid 的一部分)
 -N 大小端  设置 cramfs 大小端(big|little|host)，默认为 host
 -i 文件    将文件镜像插入文件系统(要求 >= 2.4.0)
 -n 名称    设置cramfs 文件系统名称
 -p         用于启动代码的 %d 字节的 pad 区
 -s         对目录记录进行排序(旧选项，会被忽略)
 -z         产生显式空洞 (要求 >= 2.3.39)
 目录名     要压缩的文件系统的根目录
 输出文件   输出文件
 已使用数组 = %d
 已用消息头(header)数 = %d
 已用字节数 utime 失败：%s 值 卷名过长 waitpid 失败 我们从 %2$s 通过 ioctl 读取到纪元(epoch)值 %1$ld。
 写 写失败：%s 写 stdout 失败 是 zcount 零文件计数 