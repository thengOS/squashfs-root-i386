��    ,      |  ;   �      �     �     �  '   �               <     Z     a     m     s     �     �     �     �     �  '   �       
   
          #  	   '     1     Q     a     z     �  $   �     �     �          *     I  "   [     ~     �     �  *   �  ,     '   0      X  $   y  3   �  !   �    �     �	     
  2   
     O
  )   T
  &   ~
     �
  
   �
     �
     �
     �
     �
                     1     L     U     c     q     u     }     �     �     �     �     �     �     
     %     :     Y     g     �     �     �  #   �  2   �  *   #     N  )   h  -   �  (   �           $   )                       "         #                   !                      	       &                        +                             ,   '   
         *      (                 %           A pointer to a sqlite3_stmt. Adapter Cannot save resource, no repository set Count Failed to access SQLite handle. Failed to open database at %s Filter Is Writable Limit Many-to-Many Table Many-to-Many type Many-to-many table Many-to-many type Mode No resources were found. No result was returned from the cursor. Offset Repository Resource Type SQL Statement The GomAdapter for the command. The GomAdapter. The SQL for the command. The SQL for the filter. The adapter for the repository. The command does not contain any SQL The filter for the command. The maximum number of results. The mode of the filter. The number of results to skip. The query filter. The repository for object storage. The resource type to query for. The resources repository. The size of the resource group. The table to use for many-to-many queries. The table used to join a Many to Many query. The type for the join within m2m-table. The type of resources contained. The type used in the m2m-table join. Whether the group contains resources to be written. sqlite3_prepare_v2 failed: %s: %s Project-Id-Version: bosnianuniversetranslation
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gom&keywords=I18N+L10N&component=general
POT-Creation-Date: 2015-02-26 21:34+0000
PO-Revision-Date: 2015-02-05 00:32+0000
Last-Translator: Selma Glavić <sglavic1@etf.unsa.ba>
Language-Team: Bosnian <bs@li.org>
Language: bs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2015-02-05 07:34+0000
X-Generator: Launchpad (build 17331)
 Pokazivač na sqlite3_stmt. Adapter Ne može spremiti resurs, ni podešenje spremišta Broj Nije moguće pristupiti SQLite rukovanju. Nije moguće otvoriti bazu podataka %s Filter Upisivo je Ograničenje Many-to-Many tabele Many-to-Many vrsta Više u više tabela VIše u više vrsta Režim Resursi nisu nađeni. Nema rezultata od kursora. Pomjeraj Repozitorijum Vrsta resursa SQL Naredba Naredbe za GomAdapter. GomAdapter. SQL. za naredbu. SQL za filter. Adapter za spremište. Naredba ne sadrži ni jedan SQL Filter za naredbu. Maksimalni broj rezultata. Način rada filtera. Broj rezultata ua preskoćiti. Filter upita. Spremište za čuvanje objekta. Vrsta resursa za upit. Spremište resursa. Veličina grupe rersursa. Tabele koriste više u više upite. Tabele korištene za pristup Many to Many upitima. Vrsta za pridruživanje unutar m2m-tabela. Vrsta sadržanih resursa. Vrsta korištena u m2m spajanjima tabela. Da li grupa sadrži izvorišta za upisivanje. sqlite3_pripremi_v2 nije moguće: %s: %s 