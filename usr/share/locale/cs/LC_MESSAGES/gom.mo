��    ,      |  ;   �      �     �     �  '   �               <     Z     a     m     s     �     �     �     �     �  '   �       
   
          #  	   '     1     Q     a     z     �  $   �     �     �          *     I  "   [     ~     �     �  *   �  ,     '   0      X  $   y  3   �  !   �    �     
     
  ?   &
     f
  &   m
  !   �
     �
     �
     �
     �
     �
     �
     �
     �
  %     +   ,     X     ^     j     z     ~     �     �     �     �     �      �               9  /   H     x  %   �     �     �     �  #      -   $     R     r  '   �  <   �  )   �           $   )                       "         #                   !                      	       &                        +                             ,   '   
         *      (                 %           A pointer to a sqlite3_stmt. Adapter Cannot save resource, no repository set Count Failed to access SQLite handle. Failed to open database at %s Filter Is Writable Limit Many-to-Many Table Many-to-Many type Many-to-many table Many-to-many type Mode No resources were found. No result was returned from the cursor. Offset Repository Resource Type SQL Statement The GomAdapter for the command. The GomAdapter. The SQL for the command. The SQL for the filter. The adapter for the repository. The command does not contain any SQL The filter for the command. The maximum number of results. The mode of the filter. The number of results to skip. The query filter. The repository for object storage. The resource type to query for. The resources repository. The size of the resource group. The table to use for many-to-many queries. The table used to join a Many to Many query. The type for the join within m2m-table. The type of resources contained. The type used in the m2m-table join. Whether the group contains resources to be written. sqlite3_prepare_v2 failed: %s: %s Project-Id-Version: gom master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gom&keywords=I18N+L10N&component=general
POT-Creation-Date: 2014-11-04 18:56+0000
PO-Revision-Date: 2014-11-04 21:19+0100
Last-Translator: Marek Černocký <marek@manet.cz>
Language-Team: Czech <gnome-cs-list@gnome.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Gtranslator 2.91.6
 Ukazatel na sqlite3_stmt. Adaptér Prostředek nelze uložit, není nastaven žádný repozitář. Počet Selhal přístup k popisovači SQLite. Selhalo otevření databáze v %s Filtr Je zapisovatelné Omezení Tabulka M:N Typ M:N Tabulka M:N Typ M:N Režim Žádné prostředky nebyly nalezeny. Z kurzoru nebyl vrácen žádný výsledek. Posun Repozitář Typ prostředku SQL Výraz GomAdapter pro příkaz. GomAdapter. SQL pro příkaz. SQL pro filtr. Adaptér pro repozitář. Příkaz neobsahuje žádné SQL Filtr pro příkaz. Maximální počet výsledků. Režim filtru. Počet výsledků, které se mají přeskočit. Filtr dotazu. Repozitář pro úložiště objektu. Typ prostředku pro dotaz. Repozitář prostředků. Velikost skupiny prostředků. Tabulka pro použití v dotazu M:N. Tabulka použitá v dotazu s propojením M:N. Typ pro propojení tabulek M:N. Typ obsažených prostředků. Typ použitý v propojení tabulek M:N. Zda skupina obsahuje prostředky, které se mají zapisovat. Selhala funkce sqlite3_prepare_v2: %s: %s 