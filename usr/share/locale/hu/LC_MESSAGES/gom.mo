��    ,      |  ;   �      �     �     �  '   �               <     Z     a     m     s     �     �     �     �     �  '   �       
   
          #  	   '     1     Q     a     z     �  $   �     �     �          *     I  "   [     ~     �     �  *   �  ,     '   0      X  $   y  3   �  !   �  �  �  &   �	     
  ?   "
  
   b
  0   m
  /   �
     �
     �
     �
     �
     �
          1     J     O  1   o     �     �     �     �  
   �     �     �               2  )   O     y  !   �     �  !   �     �  #   �  $   "     G     d  9   �  D   �  :     %   =  D   c  H   �  %   �           $   )                       "         #                   !                      	       &                        +                             ,   '   
         *      (                 %           A pointer to a sqlite3_stmt. Adapter Cannot save resource, no repository set Count Failed to access SQLite handle. Failed to open database at %s Filter Is Writable Limit Many-to-Many Table Many-to-Many type Many-to-many table Many-to-many type Mode No resources were found. No result was returned from the cursor. Offset Repository Resource Type SQL Statement The GomAdapter for the command. The GomAdapter. The SQL for the command. The SQL for the filter. The adapter for the repository. The command does not contain any SQL The filter for the command. The maximum number of results. The mode of the filter. The number of results to skip. The query filter. The repository for object storage. The resource type to query for. The resources repository. The size of the resource group. The table to use for many-to-many queries. The table used to join a Many to Many query. The type for the join within m2m-table. The type of resources contained. The type used in the m2m-table join. Whether the group contains resources to be written. sqlite3_prepare_v2 failed: %s: %s Project-Id-Version: gom master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gom&keywords=I18N+L10N&component=general
POT-Creation-Date: 2014-11-04 18:56+0000
PO-Revision-Date: 2014-12-07 12:48+0100
Last-Translator: Balázs Úr <urbalazs@gmail.com>
Language-Team: Hungarian <openscope at googlegroups dot com>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.2
 Mutató egy sqlite3_stmt utasításra. Csatlakozó Nem lehet elmenteni az erőforrást, nincs tároló beállítva Darabszám Nem sikerült hozzáférni az SQLite leíróhoz. Az adatbázis megnyitása nem sikerült itt: %s Szűrő Írható Határ Több-a-többhöz tábla Több-a-többhöz típus Több-a-többhöz tábla Több-a-többhöz típus Mód Nem találhatók erőforrások. Nem került eredmény visszaadásra a kurzortól. Eltolás Tároló Erőforrástípus SQL Utasítás A Gom csatlakozó a parancshoz. A Gom csatlakozó. Az SQL a parancshoz. Az SQL a szűrőhöz. A csatlakozó a tárolóhoz. A parancs nem tartalmaz SQL lekérdezést A szűrő a parancshoz. Az eredmények legnagyobb száma. A szűrő módja. A kihagyandó eredmények száma. A lekérdezésszűrő. A tároló az objektumtároláshoz. Erőforrástípus a lekérdezéshez. Az erőforrások tárolója. Az erőforráscsoport mérete. Több-a-többhöz lekérdezésekhez használandó tábla. Egy több-a-többhöz lekérdezés illesztéséhez használt tábla. Típus a több-a-többhöz táblán belüli illesztéshez. A tartalmazott erőforrások típusa. A több-a-többhöz táblán belüli illesztésben használt típus. Az erőforrásokat tartalmazó csoportoknak írhatónak kell-e lenniük. sqlite3_prepare_v2 sikertelen: %s: %s 