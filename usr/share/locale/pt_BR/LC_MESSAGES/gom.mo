��    ,      |  ;   �      �     �     �  '   �               <     Z     a     m     s     �     �     �     �     �  '   �       
   
          #  	   '     1     Q     a     z     �  $   �     �     �          *     I  "   [     ~     �     �  *   �  ,     '   0      X  $   y  3   �  !   �    �  "   �	  	   
  A   &
     h
  /   q
  +   �
     �
     �
     �
     �
               4     L     Q  +   p     �     �     �     �     �     �     �                2      S     t      �     �  %   �     �  ,   �  !   )     K     g  7   �  9   �  9   �     2  ,   O  -   |  !   �           $   )                       "         #                   !                      	       &                        +                             ,   '   
         *      (                 %           A pointer to a sqlite3_stmt. Adapter Cannot save resource, no repository set Count Failed to access SQLite handle. Failed to open database at %s Filter Is Writable Limit Many-to-Many Table Many-to-Many type Many-to-many table Many-to-many type Mode No resources were found. No result was returned from the cursor. Offset Repository Resource Type SQL Statement The GomAdapter for the command. The GomAdapter. The SQL for the command. The SQL for the filter. The adapter for the repository. The command does not contain any SQL The filter for the command. The maximum number of results. The mode of the filter. The number of results to skip. The query filter. The repository for object storage. The resource type to query for. The resources repository. The size of the resource group. The table to use for many-to-many queries. The table used to join a Many to Many query. The type for the join within m2m-table. The type of resources contained. The type used in the m2m-table join. Whether the group contains resources to be written. sqlite3_prepare_v2 failed: %s: %s Project-Id-Version: gom master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gom&keywords=I18N+L10N&component=general
POT-Creation-Date: 2014-11-04 18:56+0000
PO-Revision-Date: 2015-01-26 13:56-0300
Last-Translator: Enrico Nicoletto <liverig@gmail.com>
Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 1.7.3
 Um ponteiro para uma sqlite3_stmt. Adaptador Não foi possível salvar o recurso, nenhum repositório definido Contagem Ocorreu falha ao acessar manipulador do SQLite. Ocorreu falha ao abrir banco de dados em %s Filtro É gravável Limite Tabela muitos-para-muitos Tipo muitos-para-muitos Tabela muitos-para-muitos Tipo muitos-para-muitos Modo Nenhum recurso foi localizado. Nenhum resultado foi retornado pelo cursor. Deslocamento Repositório Tipo do recurso SQL Instrução O adaptador Gom para o comando. O adaptador Gom. O SQL para o comando. O SQL para o filtro. O adaptador para o repositório. O comando não possui nenhum SQL O filtro para o comando. O número máximo de resultados. O modo do filtro. O número de resultados para ignorar. O filtro da consulta. O repositório para armazenamento de objeto. O tipo do recurso a se consultar. O repositório de recursos. O tamanho do grupo do recurso. A tabela a ser usada para consultas muitas-para-muitas. A tabela usada para unir uma consulta muitas-para-muitas. O tipo a ser usado na união (join) dentro da tabela m-m. O tipo de recursos contidos. O tipo usado na união (join) da tabela m-m. Se o grupo contém recursos a serem gravados. sqlite3_prepare_v2 falhou: %s: %s 