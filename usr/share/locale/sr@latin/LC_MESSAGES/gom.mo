��    ,      |  ;   �      �     �     �  '   �               <     Z     a     m     s     �     �     �     �     �  '   �       
   
          #  	   '     1     Q     a     z     �  $   �     �     �          *     I  "   [     ~     �     �  *   �  ,     '   0      X  $   y  3   �  !   �  G  �     <
     [
  6   j
     �
  )   �
  0   �
  	     
             '     =     R     h     }     �     �     �     �     �     �     �     �               ,     @     [     {     �     �  )   �     �     �  )        @     T  5   p  2   �  /   �     	  =   &  -   d  (   �           $   )                       "         #                   !                      	       &                        +                             ,   '   
         *      (                 %           A pointer to a sqlite3_stmt. Adapter Cannot save resource, no repository set Count Failed to access SQLite handle. Failed to open database at %s Filter Is Writable Limit Many-to-Many Table Many-to-Many type Many-to-many table Many-to-many type Mode No resources were found. No result was returned from the cursor. Offset Repository Resource Type SQL Statement The GomAdapter for the command. The GomAdapter. The SQL for the command. The SQL for the filter. The adapter for the repository. The command does not contain any SQL The filter for the command. The maximum number of results. The mode of the filter. The number of results to skip. The query filter. The repository for object storage. The resource type to query for. The resources repository. The size of the resource group. The table to use for many-to-many queries. The table used to join a Many to Many query. The type for the join within m2m-table. The type of resources contained. The type used in the m2m-table join. Whether the group contains resources to be written. sqlite3_prepare_v2 failed: %s: %s Project-Id-Version: gom master
Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gom&keywords=I18N+L10N&component=general
POT-Creation-Date: 2014-11-28 21:14+0000
PO-Revision-Date: 2014-11-29 10:33+0200
Last-Translator: Miroslav Nikolić <miroslavnikolic@rocketmail.com>
Language-Team: Serbian <gnom@prevod.org>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=n==1? 3 : n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Project-Style: gnome
 Pokazivač ka skulajt3_stanju. Prilagođivač Ne mogu da sačuvam izvorište, riznice nisu podešene Ukupnost Nisam uspeo da pristupim ručku SKuLajta. Nisam uspeo da otvorim bazu podataka na „%s“ Propusnik Upisivo je Ograničenje Tabela mnogo-za-mnogo Vrsta mnogo-za-mnogo Tabela mnogo-za-mnogo Vrsta mnogo-za-mnogo Režim Nisam pronašao izvorišta. Nema rezultata sa kurzora. Pomeraj Riznica Vrsta izvorišta SKuL Stanje Gom prilagođivač za naredbu. Gom prilagođivač. SKuL za naredbu. SKuL za propusnika. Prilagođivač za riznicu. Naredba ne sadrži nijedan SKuL Propusnik za naredbu. Najveći broj rezultata. Režim propusnika. Broj rezultata koji će biti preskočeni. Propusnik upita. Riznica za smeštaj predmeta. Vrsta izvorišta koja će biti propitana. Riznica izvorišta. Veličina grupe izvorišta. Tabela koja će se koristiti za upite mnogo-za-mnogo. Tabela za pridruživanje sa upitom mnogo za mnogo. Vrsta pridruživanja sa tabelom mnogo-za-mnogo. Vrsta sadržanih izvorišta. Vrsta koja se koristi u pridruživanju tabele mnogo za mnogo. Da li grupa sadrži izvorišta za upisivanje. skulajt3_pripremi_i2 nije uspelo: %s: %s 