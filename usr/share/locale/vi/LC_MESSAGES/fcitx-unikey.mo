��    #      4  /   L           	                    &  $   C     h     q     y     �     �     �     �     �     �     �     �                         .     =     I     O     U     ]     d     x     �  #   �     �     �     �  �  �     �     �     �     �     �  <   �     �                     ,     8     D     a      m  
   �     �     �     �     �     �  
   �     �                               -     K  6   g     �     �     �                                                  	                          #   
             "                       !                                          &Add &Delete &Export &Import Allow type with more freedom Auto restore keys with invalid words BK HCM 2 CString Choose input method Choose output charset De&lete All Enable Macro Enable Spell Check Enable macro Enable spell check Input Method Macro NCR Decimal NCR Hex No Macro No Spell Check Output Charset Spell Check TCVN3 Telex Unicode Unikey Unikey Input Method Unikey Macro Editor Unikey Wrapper For Fcitx Use oà, _uý (instead of òa, úy) VIQR VNI Win Word Project-Id-Version: fcitx
Report-Msgid-Bugs-To: fcitx-dev@googlegroups.com
POT-Creation-Date: 2014-04-30 20:02+0200
PO-Revision-Date: 2014-04-30 17:03+0000
Last-Translator: Xuetian Weng <wengxt@gmail.com>
Language-Team: Vietnamese (http://www.transifex.com/projects/p/fcitx/language/vi/)
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 &Thêm &Xóa &Xuất &Nhập Cho phép gõ tự do Tự động khôi phục phím với từ không hợp lệ BK HCM 2 CString Chọn kiểu gõ Chọn bảng mã Xóa &hết Bật Macro Bật kiểm tra chính tả Bật macro Cho phép kiểm tra chính tả Kiểu gõ Macro NCR Decimal NCR Hex Không Macro Không kiểm tra chính tả Bảng mã Kiểm tra chính tả TCVN3 Telex Unicode Unikey Unikey Kiểu gõ Bộ chỉnh macro cho Unikey Bộ Unikey dành cho Fcitx Đặt dấu theo dạng oà , uý (thay vì òa, úy) VIQR VNI Win Từ 