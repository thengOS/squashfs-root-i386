��    %      D  5   l      @     A     F     N     V     ^  $   {     �     �     �     �     �     �     �               '     4     :     F     N     W     f     u     �     �     �     �     �     �     �     �  #   �               "     *  �  /  
   �  
   �  
   �  
             +     G     P     X     h     ~  	   �     �  	   �     �  	   �     �     �     �  	   �     �               0     =     C     I     Q     X     i     }  !   �     �     �     �     �                                                                              
           %   $           !                                                            #      "      	    &Add &Delete &Export &Import Allow type with more freedom Auto restore keys with invalid words BK HCM 2 CString Choose input method Choose output charset De&lete All Enable Macro Enable Spell Check Enable macro Enable spell check Input Method Macro NCR Decimal NCR Hex No Macro No Spell Check Output Charset Process W at word begin Spell Check TCVN3 Telex Unicode Unikey Unikey Input Method Unikey Macro Editor Unikey Wrapper For Fcitx Use oà, _uý (instead of òa, úy) VIQR VNI VNI Win Word Project-Id-Version: fcitx
Report-Msgid-Bugs-To: fcitx-dev@googlegroups.com
POT-Creation-Date: 2014-05-08 07:01+0200
PO-Revision-Date: 2014-05-08 04:53+0000
Last-Translator: Xuetian Weng <wengxt@gmail.com>
Language-Team: Chinese (China) (http://www.transifex.com/projects/p/fcitx/language/zh_CN/)
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 添加(&A) 删除(&D) 导出(&E) 导入(&I) 允许更自由的输入 自动恢复无效词的键 BK HCM 2 CString 选择输入法 选择输出字符集 全部删除(&L) 开启宏 启用拼写检查 启用宏 启用拼写检查 输入法 宏 NCR Decimal NCR Hex 禁用宏 禁用拼写检查 输出字符集 在单词开头处理 W 拼写检查 TCVN3 Telex Unicode Unikey Unikey 输入法 Unikey 宏编辑器 Fcitx 的 Unikey 封装 使用 oà, _uý (替换òa, úy) VIQR VNI VNI Win 单词 