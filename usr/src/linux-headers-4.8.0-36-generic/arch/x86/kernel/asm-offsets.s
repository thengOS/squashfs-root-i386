	.file	"asm-offsets.c"
# GNU C89 (Ubuntu 5.4.0-6ubuntu1~16.04.4) version 5.4.0 20160609 (i686-linux-gnu)
#	compiled by GNU C version 5.4.0 20160609, GMP version 6.1.0, MPFR version 3.1.4, MPC version 1.0.3
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -nostdinc -I /usr/src/linux-headers-lbm-
# -I /build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/include
# -I ./arch/x86/include/generated/uapi -I ./arch/x86/include/generated
# -I /build/linux-hwe-xlPxPU/linux-hwe-4.8.0/include -I ./include
# -I /build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/include/uapi
# -I /build/linux-hwe-xlPxPU/linux-hwe-4.8.0/include/uapi
# -I ./include/generated/uapi -I ubuntu/include
# -I /build/linux-hwe-xlPxPU/linux-hwe-4.8.0/ubuntu/include
# -I /build/linux-hwe-xlPxPU/linux-hwe-4.8.0/. -I .
# -imultiarch i386-linux-gnu -D __KERNEL__ -D CONFIG_AS_CFI=1
# -D CONFIG_AS_CFI_SIGNAL_FRAME=1 -D CONFIG_AS_CFI_SECTIONS=1
# -D CONFIG_AS_SSSE3=1 -D CONFIG_AS_CRC32=1 -D CONFIG_AS_AVX=1
# -D CONFIG_AS_AVX2=1 -D CONFIG_AS_SHA1_NI=1 -D CONFIG_AS_SHA256_NI=1
# -D CC_HAVE_ASM_GOTO -D KBUILD_BASENAME="asm_offsets"
# -D KBUILD_MODNAME="asm_offsets"
# -isystem /usr/lib/gcc/i686-linux-gnu/5/include
# -include /build/linux-hwe-xlPxPU/linux-hwe-4.8.0/include/linux/kconfig.h
# -MD arch/x86/kernel/.asm-offsets.s.d
# /build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c
# -mno-sse -mno-mmx -mno-sse2 -mno-3dnow -mno-avx -m32 -msoft-float
# -mregparm=3 -mpreferred-stack-boundary=2 -march=i686 -mtune=generic
# -maccumulate-outgoing-args -auxbase-strip arch/x86/kernel/asm-offsets.s
# -O2 -Wall -Wundef -Wstrict-prototypes -Wno-trigraphs
# -Werror=implicit-function-declaration -Wno-format-security
# -Wno-sign-compare -Wno-maybe-uninitialized -Wframe-larger-than=1024
# -Wno-unused-but-set-variable -Wdeclaration-after-statement
# -Wno-pointer-sign -Werror=implicit-int -Werror=strict-prototypes
# -Werror=date-time -Werror=incompatible-pointer-types -std=gnu90 -p
# -fno-strict-aliasing -fno-common -freg-struct-return -fno-pic
# -ffreestanding -fno-asynchronous-unwind-tables
# -fno-delete-null-pointer-checks -fstack-protector-strong
# -fno-omit-frame-pointer -fno-optimize-sibling-calls
# -fno-var-tracking-assignments -fno-inline-functions-called-once
# -fno-strict-overflow -fconserve-stack -fverbose-asm
# --param allow-store-data-races=0
# options enabled:  -faggressive-loop-optimizations -falign-labels
# -fauto-inc-dec -fbranch-count-reg -fcaller-saves
# -fchkp-check-incomplete-type -fchkp-check-read -fchkp-check-write
# -fchkp-instrument-calls -fchkp-narrow-bounds -fchkp-optimize
# -fchkp-store-bounds -fchkp-use-static-bounds
# -fchkp-use-static-const-bounds -fchkp-use-wrappers
# -fcombine-stack-adjustments -fcompare-elim -fcprop-registers
# -fcrossjumping -fcse-follow-jumps -fdefer-pop -fdevirtualize
# -fdevirtualize-speculatively -fdwarf2-cfi-asm -fearly-inlining
# -feliminate-unused-debug-types -fexpensive-optimizations
# -fforward-propagate -ffunction-cse -fgcse -fgcse-lm -fgnu-runtime
# -fgnu-unique -fguess-branch-probability -fhoist-adjacent-loads -fident
# -fif-conversion -fif-conversion2 -findirect-inlining -finline
# -finline-atomics -finline-small-functions -fipa-cp -fipa-cp-alignment
# -fipa-icf -fipa-icf-functions -fipa-icf-variables -fipa-profile
# -fipa-pure-const -fipa-reference -fipa-sra -fira-hoist-pressure
# -fira-share-save-slots -fira-share-spill-slots
# -fisolate-erroneous-paths-dereference -fivopts -fkeep-static-consts
# -fleading-underscore -flifetime-dse -flra-remat -flto-odr-type-merging
# -fmath-errno -fmerge-constants -fmerge-debug-strings
# -fmove-loop-invariants -foptimize-strlen -fpartial-inlining -fpeephole
# -fpeephole2 -fprefetch-loop-arrays -fprofile -free -freg-struct-return
# -freorder-blocks -freorder-blocks-and-partition -freorder-functions
# -frerun-cse-after-loop -fsched-critical-path-heuristic
# -fsched-dep-count-heuristic -fsched-group-heuristic -fsched-interblock
# -fsched-last-insn-heuristic -fsched-rank-heuristic -fsched-spec
# -fsched-spec-insn-heuristic -fsched-stalled-insns-dep -fschedule-fusion
# -fschedule-insns2 -fsemantic-interposition -fshow-column -fshrink-wrap
# -fsigned-zeros -fsplit-ivs-in-unroller -fsplit-wide-types -fssa-phiopt
# -fstack-protector-strong -fstdarg-opt -fstrict-volatile-bitfields
# -fsync-libcalls -fthread-jumps -ftoplevel-reorder -ftrapping-math
# -ftree-bit-ccp -ftree-builtin-call-dce -ftree-ccp -ftree-ch
# -ftree-coalesce-vars -ftree-copy-prop -ftree-copyrename -ftree-cselim
# -ftree-dce -ftree-dominator-opts -ftree-dse -ftree-forwprop -ftree-fre
# -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pre
# -ftree-pta -ftree-reassoc -ftree-scev-cprop -ftree-sink -ftree-slsr
# -ftree-sra -ftree-switch-conversion -ftree-tail-merge -ftree-ter
# -ftree-vrp -funit-at-a-time -fverbose-asm -fzero-initialized-in-bss -m32
# -m96bit-long-double -maccumulate-outgoing-args -malign-stringops
# -mavx256-split-unaligned-load -mavx256-split-unaligned-store -mglibc
# -mieee-fp -mlong-double-80 -mno-fancy-math-387 -mno-red-zone -mno-sse4
# -mpush-args -msahf -mtls-direct-seg-refs -mvzeroupper

	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4,,15
	.globl	foo
	.type	foo, @function
foo:
	pushl	%ebp	#
	movl	%esp, %ebp	#,
1:	call	mcount
#APP
# 20 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->CPUINFO_x86 $0 offsetof(struct cpuinfo_x86, x86)	#
# 0 "" 2
# 21 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->CPUINFO_x86_vendor $1 offsetof(struct cpuinfo_x86, x86_vendor)	#
# 0 "" 2
# 22 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->CPUINFO_x86_model $2 offsetof(struct cpuinfo_x86, x86_model)	#
# 0 "" 2
# 23 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->CPUINFO_x86_mask $3 offsetof(struct cpuinfo_x86, x86_mask)	#
# 0 "" 2
# 24 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->CPUINFO_cpuid_level $16 offsetof(struct cpuinfo_x86, cpuid_level)	#
# 0 "" 2
# 25 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->CPUINFO_x86_capability $20 offsetof(struct cpuinfo_x86, x86_capability)	#
# 0 "" 2
# 26 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->CPUINFO_x86_vendor_id $96 offsetof(struct cpuinfo_x86, x86_vendor_id)	#
# 0 "" 2
# 27 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->
# 0 "" 2
# 29 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->PT_EBX $0 offsetof(struct pt_regs, bx)	#
# 0 "" 2
# 30 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->PT_ECX $4 offsetof(struct pt_regs, cx)	#
# 0 "" 2
# 31 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->PT_EDX $8 offsetof(struct pt_regs, dx)	#
# 0 "" 2
# 32 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->PT_ESI $12 offsetof(struct pt_regs, si)	#
# 0 "" 2
# 33 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->PT_EDI $16 offsetof(struct pt_regs, di)	#
# 0 "" 2
# 34 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->PT_EBP $20 offsetof(struct pt_regs, bp)	#
# 0 "" 2
# 35 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->PT_EAX $24 offsetof(struct pt_regs, ax)	#
# 0 "" 2
# 36 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->PT_DS $28 offsetof(struct pt_regs, ds)	#
# 0 "" 2
# 37 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->PT_ES $32 offsetof(struct pt_regs, es)	#
# 0 "" 2
# 38 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->PT_FS $36 offsetof(struct pt_regs, fs)	#
# 0 "" 2
# 39 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->PT_GS $40 offsetof(struct pt_regs, gs)	#
# 0 "" 2
# 40 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->PT_ORIG_EAX $44 offsetof(struct pt_regs, orig_ax)	#
# 0 "" 2
# 41 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->PT_EIP $48 offsetof(struct pt_regs, ip)	#
# 0 "" 2
# 42 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->PT_CS $52 offsetof(struct pt_regs, cs)	#
# 0 "" 2
# 43 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->PT_EFLAGS $56 offsetof(struct pt_regs, flags)	#
# 0 "" 2
# 44 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->PT_OLDESP $60 offsetof(struct pt_regs, sp)	#
# 0 "" 2
# 45 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->PT_OLDSS $64 offsetof(struct pt_regs, ss)	#
# 0 "" 2
# 46 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->
# 0 "" 2
# 48 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->saved_context_gdt_desc $41 offsetof(struct saved_context, gdt_desc)	#
# 0 "" 2
# 49 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->
# 0 "" 2
# 52 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->TSS_sysenter_sp0 $-8556 offsetof(struct tss_struct, x86_tss.sp0) - offsetofend(struct tss_struct, SYSENTER_stack)	#
# 0 "" 2
# 56 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->CPU_TSS_SYSENTER_stack $8304 offsetof(struct tss_struct, SYSENTER_stack)	#
# 0 "" 2
# 58 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->SIZEOF_SYSENTER_stack $256 sizeof(((struct tss_struct *)0)->SYSENTER_stack)	#
# 0 "" 2
# 77 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->
# 0 "" 2
# 78 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->__NR_syscall_max $382 sizeof(syscalls) - 1	#
# 0 "" 2
# 79 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets_32.c" 1
	
->NR_syscalls $383 sizeof(syscalls)	#
# 0 "" 2
#NO_APP
	popl	%ebp	#
	ret
	.size	foo, .-foo
	.section	.text.unlikely
.LCOLDE0:
	.text
.LHOTE0:
	.section	.text.unlikely
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4,,15
	.globl	common
	.type	common, @function
common:
	pushl	%ebp	#
	movl	%esp, %ebp	#,
1:	call	mcount
#APP
# 31 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->
# 0 "" 2
# 32 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->TI_flags $4 offsetof(struct thread_info, flags)	#
# 0 "" 2
# 33 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->TI_status $8 offsetof(struct thread_info, status)	#
# 0 "" 2
# 35 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->
# 0 "" 2
# 36 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->TASK_addr_limit $1764 offsetof(struct task_struct, thread.addr_limit)	#
# 0 "" 2
# 38 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->
# 0 "" 2
# 39 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->crypto_tfm_ctx_offset $40 offsetof(struct crypto_tfm, __crt_ctx)	#
# 0 "" 2
# 41 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->
# 0 "" 2
# 42 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->pbe_address $0 offsetof(struct pbe, address)	#
# 0 "" 2
# 43 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->pbe_orig_address $4 offsetof(struct pbe, orig_address)	#
# 0 "" 2
# 44 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->pbe_next $8 offsetof(struct pbe, next)	#
# 0 "" 2
# 47 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->
# 0 "" 2
# 48 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->IA32_SIGCONTEXT_ax $44 offsetof(struct sigcontext_32, ax)	#
# 0 "" 2
# 49 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->IA32_SIGCONTEXT_bx $32 offsetof(struct sigcontext_32, bx)	#
# 0 "" 2
# 50 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->IA32_SIGCONTEXT_cx $40 offsetof(struct sigcontext_32, cx)	#
# 0 "" 2
# 51 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->IA32_SIGCONTEXT_dx $36 offsetof(struct sigcontext_32, dx)	#
# 0 "" 2
# 52 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->IA32_SIGCONTEXT_si $20 offsetof(struct sigcontext_32, si)	#
# 0 "" 2
# 53 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->IA32_SIGCONTEXT_di $16 offsetof(struct sigcontext_32, di)	#
# 0 "" 2
# 54 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->IA32_SIGCONTEXT_bp $24 offsetof(struct sigcontext_32, bp)	#
# 0 "" 2
# 55 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->IA32_SIGCONTEXT_sp $28 offsetof(struct sigcontext_32, sp)	#
# 0 "" 2
# 56 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->IA32_SIGCONTEXT_ip $56 offsetof(struct sigcontext_32, ip)	#
# 0 "" 2
# 58 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->
# 0 "" 2
# 59 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->IA32_RT_SIGFRAME_sigcontext $164 offsetof(struct rt_sigframe, uc.uc_mcontext)	#
# 0 "" 2
# 63 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->
# 0 "" 2
# 64 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->PARAVIRT_PATCH_pv_cpu_ops $12 offsetof(struct paravirt_patch_template, pv_cpu_ops)	#
# 0 "" 2
# 65 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->PARAVIRT_PATCH_pv_irq_ops $152 offsetof(struct paravirt_patch_template, pv_irq_ops)	#
# 0 "" 2
# 66 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->PV_IRQ_irq_disable $8 offsetof(struct pv_irq_ops, irq_disable)	#
# 0 "" 2
# 67 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->PV_IRQ_irq_enable $12 offsetof(struct pv_irq_ops, irq_enable)	#
# 0 "" 2
# 68 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->PV_CPU_iret $124 offsetof(struct pv_cpu_ops, iret)	#
# 0 "" 2
# 69 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->PV_CPU_read_cr0 $12 offsetof(struct pv_cpu_ops, read_cr0)	#
# 0 "" 2
# 70 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->PV_MMU_read_cr2 $0 offsetof(struct pv_mmu_ops, read_cr2)	#
# 0 "" 2
# 74 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->
# 0 "" 2
# 75 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->XEN_vcpu_info_mask $1 offsetof(struct vcpu_info, evtchn_upcall_mask)	#
# 0 "" 2
# 76 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->XEN_vcpu_info_pending $0 offsetof(struct vcpu_info, evtchn_upcall_pending)	#
# 0 "" 2
# 79 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->
# 0 "" 2
# 80 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->BP_scratch $484 offsetof(struct boot_params, scratch)	#
# 0 "" 2
# 81 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->BP_loadflags $529 offsetof(struct boot_params, hdr.loadflags)	#
# 0 "" 2
# 82 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->BP_hardware_subarch $572 offsetof(struct boot_params, hdr.hardware_subarch)	#
# 0 "" 2
# 83 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->BP_version $518 offsetof(struct boot_params, hdr.version)	#
# 0 "" 2
# 84 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->BP_kernel_alignment $560 offsetof(struct boot_params, hdr.kernel_alignment)	#
# 0 "" 2
# 85 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->BP_init_size $608 offsetof(struct boot_params, hdr.init_size)	#
# 0 "" 2
# 86 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->BP_pref_address $600 offsetof(struct boot_params, hdr.pref_address)	#
# 0 "" 2
# 87 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->BP_code32_start $532 offsetof(struct boot_params, hdr.code32_start)	#
# 0 "" 2
# 89 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->
# 0 "" 2
# 90 "/build/linux-hwe-xlPxPU/linux-hwe-4.8.0/arch/x86/kernel/asm-offsets.c" 1
	
->PTREGS_SIZE $68 sizeof(struct pt_regs)	#
# 0 "" 2
#NO_APP
	popl	%ebp	#
	ret
	.size	common, .-common
	.section	.text.unlikely
.LCOLDE1:
	.text
.LHOTE1:
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
