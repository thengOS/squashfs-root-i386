;;; This file is part of the dictionaries-common package.
;;; It has been automatically generated.
;;; DO NOT EDIT!

;; Adding aspell dicts

(add-to-list 'debian-aspell-only-dictionary-alist
  '("english"
     "[a-zA-Z]"
     "[^a-zA-Z]"
     "[']"
     nil
     ("-d" "en")
     nil
     iso-8859-1))
(add-to-list 'debian-aspell-only-dictionary-alist
  '("canadian"
     "[a-zA-Z]"
     "[^a-zA-Z]"
     "[']"
     nil
     ("-d" "en_CA")
     nil
     iso-8859-1))
(add-to-list 'debian-aspell-only-dictionary-alist
  '("british"
     "[a-zA-Z]"
     "[^a-zA-Z]"
     "[']"
     nil
     ("-d" "en_GB")
     nil
     iso-8859-1))
(add-to-list 'debian-aspell-only-dictionary-alist
  '("american"
     "[a-zA-Z]"
     "[^a-zA-Z]"
     "[']"
     nil
     ("-d" "en_US")
     nil
     iso-8859-1))


;; Adding hunspell dicts

(add-to-list 'debian-hunspell-only-dictionary-alist
  '("russian"
     "[АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯабвгдеёжзийклмнопрстуфхцчшщьыъэюя]"
     "[^АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЫЪЭЮЯабвгдеёжзийклмнопрстуфхцчшщьыъэюя]"
     "^$"
     nil
     ("-d" "ru_RU")
     nil
     utf-8))
(add-to-list 'debian-hunspell-only-dictionary-alist
  '("portugues"
     "[a-zA-Z]"
     "[^a-zA-Z]"
     "[']"
     nil
     ("-d" "pt_PT")
     nil
     iso-8859-1))
(add-to-list 'debian-hunspell-only-dictionary-alist
  '("castellano8"
     "[a-z\340\341\350\351\354\355\362\363\371\372\374\347\361A-Z\300\301\310\311\314\315\322\323\331\332\334\307\321]"
     "[^a-z\340\341\350\351\354\355\362\363\371\372\374\347\361A-Z\300\301\310\311\314\315\322\323\331\332\334\307\321]"
     "[']"
     nil
     ("-B" "-d" "es")
     nil
     iso-8859-1))
(add-to-list 'debian-hunspell-only-dictionary-alist
  '("brasileiro"
     "[a-zA-Z]"
     "[^a-zA-Z]"
     "[---']"
     nil
     ("-d" "pt_BR")
     nil
     iso-8859-1))
(add-to-list 'debian-hunspell-only-dictionary-alist
  '("australian"
     "[A-Za-z]"
     "[^A-Za-z]"
     "[']"
     nil
     ("-d" "en_AU")
     nil
     iso-8859-1))



;; No emacsen-aspell-equivs entries were found


;; An alist that will try to map hunspell locales to emacsen names

(setq debian-hunspell-equivs-alist '(
     ("en" "australian")
     ("en_AU" "australian")
     ("es" "castellano8")
     ("es_ES" "castellano8")
     ("pt" "portugues")
     ("pt_BR" "brasileiro")
     ("pt_PT" "portugues")
))

;; Get default value for debian-hunspell-dictionary. Will be used if
;; spellchecker is hunspell and ispell-local-dictionary is not set.
;; We need to get it here, after debian-hunspell-equivs-alist is loaded

(setq debian-hunspell-dictionary (debian-ispell-get-hunspell-default))

